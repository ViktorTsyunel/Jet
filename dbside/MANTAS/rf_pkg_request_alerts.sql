create or replace package mantas.rf_pkg_request_alerts is

  /* ������� ��������� ������� ��� ������� 
   *   AlertList read
   *   AlertAction readById
   *   AlertAction readByFilter
   *   AlertAction execById
   *   AlertAction execByFilter
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number, p_response out clob ) return rf_json;
end;
/
create or replace package body mantas.rf_pkg_request_alerts is

  /* ������� ����� �� mantas.kdd_queue_master ����� ������� */
  function get_queue_query( p_id number ) return varchar2 is
    v_queue_query mantas.kdd_queue_master.queue_query%type;
  begin
    select max(k.queue_query)
      into v_queue_query
      from mantas.kdd_queue_master k
     where k.queue_seq_id = p_id;
    return v_queue_query;
  end;

  /* ������ ������ �� json_value � ���������� ��� sql �������:
   *   Number -> Number
   *   Array -> ��������� ������� - ������� [], '"' �������� �� '
   *   String -> ����������� �� ����:
   *               ���� ����, �� to_date('***', 'YYYY-MM-DD HH24:MI:SS')
   *               ����� ��������� ������ � �������
   * 
   * ��������� ������
   *   p_json_value - ���� �������� json
   *   p_is_like - ��������, ����������� ����� �� ��������� �������� � "%" ��� �������� like
   * ��������� ������
   *   varchar2 - �������������� ������ ��������� ��� sql ������� 
   */
  function modify_value( p_json_value in rf_json_value, p_is_like in boolean default false ) return varchar2
  is
    v_value varchar2(512) := null;
  begin
    if p_json_value.is_array then 
      v_value := translate( p_json_value.to_char, '[]"', '  ''' );
    else 
      if p_json_value.is_number 
        then v_value := p_json_value.get_number;
      elsif p_json_value.is_string 
        then v_value := p_json_value.get_string;
      end if;

      if p_is_like = true then
        v_value := '''%'||v_value||'%''';
      else 
        v_value := ''''||v_value||'''';
      end if;  
    end if;  

    return v_value;
  end;
  
  /* �������� json_value, ��� ��� ���������� � ��� �������� �� ������. 
   * ���������� ������� � sql: 
   *   Value - �������������� �������� �� json_value  
   *   Value_Like - �������������� �������� �� json_value � ����������� �������� ������� like
   *   Value_OsbID - ���������� tb_id � osb_id
   *   Value_Where - ������������� ������� where �� ��������� ����� json. ���� �������� ����������, ������������ ������ ������. 
   *
   * ��������� ������
   *   p_condition - ������ + ������� ��� ��������������� �������
   *   p_json_value - ������ json_value
   * ��������� ������
   *   varchar2 - �������������� ������ sql �������
   */
  function get_sql_condition( p_condition in varchar2, p_json_value in rf_json_value ) return varchar2
  is
    v_str  varchar2(512) := null;
    v_tmp  varchar2(512) := null;
    v_clob clob;
  begin
    if ( p_json_value is null ) or ( p_json_value.is_null )
      then return '';
      else 
        v_str := p_condition;
        
        if instr( v_str, '##VALUE##') > 0 then
          v_str := replace( v_str, '##VALUE##', modify_value ( p_json_value ) );
        end if;  
        
        if instr( p_condition, '##VALUE_LIKE##') > 0 then
          v_str := replace( v_str, '##VALUE_LIKE##', modify_value ( p_json_value, true ) ); 
        end if;  
        
        if instr( p_condition, '##VALUE_OSBID##') > 0 then
          v_str := replace( v_str, 
                            '##VALUE_OSBID##', 
                            '( ( rv.rv_tb_id = '|| replace( replace( trim( translate( modify_value ( p_json_value ), '() ''', ' ' ) ),
                                                                  '-',
                                                                  ' and rv.rv_osb_id = '
                                                                ),
                                                         ',',
                                                         ' ) or ( rv.rv_tb_id = '
                                                       ) ||' ) )' 
                          );
        end if;
        
        if instr( v_str, '##VALUE_WHERE##') > 0 then
          v_tmp := trim( get_queue_query( p_json_value.get_number ) );
          if v_tmp is not null 
            then v_str := replace( v_str, '##VALUE_WHERE##', v_tmp );
            else v_str := '';
          end if;
        end if;  
        
        if instr( v_str, '##ALERTIDS##') > 0 then
          dbms_lob.createtemporary(v_clob, true);
          p_json_value.to_clob( v_clob );
          v_clob := regexp_replace(v_clob, '[][" ]');
          if instr( v_clob, ',' ) > 0 then 
            delete from mantas.rf_id;
            insert into mantas.rf_id( column_value ) 
            select * 
              from table( mantas.rf_pkg_scnro.list_to_tab( v_clob ) );
            v_str := replace( v_str, '##ALERTIDS##', 'in (select column_value from mantas.rf_id)' );
          else 
            v_str := replace( v_str, '##ALERTIDS##', '= '||v_clob );
          end if;
        end if;
        
        return v_str || chr(13);
    end if;
  end;
  
  /* ������� ���������� ��������� ��� ������� ������ �����.
   *
   * ��������� ������
   *   p_json - json ������ �������
   *   p_tag_name - ��� ���� �� �������� �������� ��������� ������
   *
   * ��������� ������
   *   varchar2 - ������� ����� ��� �����
   *     * : �����
   *     ��: ���� ���������
   *     ��: ���� ��������
   *     - : �����
   */
  function get_acct_area_condition(p_json in rf_json, p_tag_name in varchar2) return varchar2
  is
    v_json_list rf_json_list;
    v_return varchar2(4);
  begin
    v_json_list := rf_json_ext.get_json_list(p_json, p_tag_name);
    if v_json_list is null then
      v_return := '-';
    else
      if rf_json_helper.contains(v_json_list, '��') and rf_json_helper.contains(v_json_list, '��') then
        v_return := '*';
      elsif rf_json_helper.contains(v_json_list, '��') then 
        v_return := '��';
      elsif rf_json_helper.contains(v_json_list, '��') then
        v_return := '��';
      else
        v_return := '-';
      end if;
    end if;

    return v_return;
  end;
  
  /* ������ ������ ������ �� ������ �� json filetered
   *
   * ��������� ������
   *   p_json - json ������ �������
   * ��������� ������
   *   varchar2 - �������������� ������ ��� sql �������
   */
  function get_filter(p_json in rf_json) return varchar2
  is
    v_str varchar2(4000) := ''; 
  begin
    if p_json is null then 
      v_str := v_str || q'{and rv.rv_partition_key = 'ACT'}';
    else
      v_str := v_str || get_sql_condition( 'and rv.rv_owner_seq_id in (##VALUE##)', rf_json_ext.get_json_value( p_json, 'ownerIds' ) );
      v_str := v_str || get_sql_condition( 'and rv.rv_tb_id in (##VALUE##)', rf_json_ext.get_json_value( p_json, 'tbIds' ) );
      v_str := v_str || get_sql_condition( 'and ##VALUE_OSBID##', rf_json_ext.get_json_value( p_json, 'osbIds' ) );
      v_str := v_str || get_sql_condition( 'and rv.rv_status_cd in (##VALUE##)', rf_json_ext.get_json_value( p_json, 'status' ) );
      v_str := v_str || get_sql_condition( 'and rv.review_id = ##VALUE##', rf_json_ext.get_json_value( p_json, 'operationNumber' ) );
      v_str := v_str || get_sql_condition( 'and rv.rv_trxn_base_am >= ##VALUE##', rf_json_ext.get_json_value( p_json, 'amountFrom' ) );
      v_str := v_str || get_sql_condition( 'and rv.rv_trxn_base_am <= ##VALUE##', rf_json_ext.get_json_value( p_json, 'amountTo' ) );
      v_str := v_str || get_sql_condition( 'and rv.rv_trxn_dt >= to_date(##VALUE##, '''||rf_json_ext.format_string||''')', rf_json_ext.get_json_value( p_json, 'dtFrom' ) );
      v_str := v_str || get_sql_condition( 'and rv.rv_trxn_dt <= to_date(##VALUE##, '''||rf_json_ext.format_string||''') + 1 - 1/86400', rf_json_ext.get_json_value( p_json, 'dtTo' ) );

      if rf_json_ext.get_json_value( p_json, 'opokType' ) is not null then
        case rf_json_ext.get_json_value( p_json, 'opokType' ).get_string
          when 'OPOK' then v_str := v_str || get_sql_condition( 'and rv.opok_nb = ##VALUE##', rf_json_ext.get_json_value( p_json, 'opok' ) );
          when 'ADD_OPOK' then v_str := v_str || get_sql_condition( 'and rv.add_opoks_tx like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'opok' ) );
          when 'ALL' then v_str := v_str || get_sql_condition( 'and ( rv.opok_nb = ##VALUE## or rv.add_opoks_tx like ##VALUE_LIKE## )', rf_json_ext.get_json_value( p_json, 'opok' ) );
          else null;
        end case;
      end if;
    
      if ( rf_json_ext.get_json_value( p_json, 'oldAlert' ) is null ) or ( rf_json_ext.get_json_value( p_json, 'oldAlert' ).get_string = 'N' ) then
        v_str := v_str || q'{and rv.rv_partition_key = 'ACT'}';
      end if;
      
      -- �������������� �������
      
      -- ��� �����������
      v_str := v_str || get_sql_condition( 'and rv.orig_inn_nb like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'origInnNumber' ) );
      -- ���� � ������� ������ �����������
      case get_acct_area_condition(p_json, 'origAcctArea')
        when '*'  then v_str := v_str ||'and ( '||get_sql_condition( 'rv.orig_acct_nb like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'origAcctNumber' ) )||
                                          ' or '||get_sql_condition( 'rv.debit_cd like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'origAcctNumber' ) )||') ';
        when '��' then v_str := v_str || get_sql_condition( 'and rv.orig_acct_nb like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'origAcctNumber' ) );
        when '��' then v_str := v_str || get_sql_condition( 'and rv.debit_cd like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'origAcctNumber' ) );
        else null;
      end case;
      -- ��� ����������
      v_str := v_str || get_sql_condition( 'and rv.benef_inn_nb like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'benefInnNumber' ) );
      -- ���� � ������� ������ ����������
      case get_acct_area_condition(p_json, 'benefAcctArea')
        when '*'  then v_str := v_str ||'and ( '||get_sql_condition( 'rv.benef_acct_nb like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'benefAcctNumber' ) )||
                                          ' or '||get_sql_condition( 'rv.credit_cd like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'benefAcctNumber' ) )||') ';
        when '��' then v_str := v_str || get_sql_condition( 'and rv.benef_acct_nb like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'benefAcctNumber' ) );
        when '��' then v_str := v_str || get_sql_condition( 'and rv.credit_cd like ##VALUE_LIKE##', rf_json_ext.get_json_value( p_json, 'benefAcctNumber' ) );
        else null;
      end case;
      
      -- ��������
      v_str := v_str || get_sql_condition( 'and rv.trxn_doc_id = ##VALUE##', rf_json_ext.get_json_value( p_json, 'trxnDocNumber' ) );
      v_str := v_str || get_sql_condition( 'and rv.trxn_doc_dt = to_date(##VALUE##, '''||rf_json_ext.format_string||''')', rf_json_ext.get_json_value( p_json, 'trxnDocDate' ) );
      -- ������� ��������
      v_str := v_str || get_sql_condition( 'and rv.src_sys_cd in (##VALUE##)', rf_json_ext.get_json_value( p_json, 'srcSysCd' ) );
      -- ���������� �������
      v_str := v_str || get_sql_condition( 'and mantas.rf_pkg_scnro.check_lexeme(rv.trxn_desc, ##VALUE##) = 1', rf_json_ext.get_json_value( p_json, 'trxnDesc' ) );
    end if;
    
    return v_str;
  end;


  /* ��������� �������������� ������� filtered ����� ��������� json ������� � ������
   *
   * ��������� ������
   *   p_json_in - json �������
   *   p_json_out - json ������
   * ��������� ������
   *   rf_json_list - ������ ������, �������� � ������
   */
  function get_filetered_keys( p_json_in in rf_json, p_json_out in rf_json ) return rf_json_list
  is
    v_alert_ids   clob;
    v_id          number;
    v_json_list   rf_json_list := null;
    v_filter_list rf_json_list;
    v_json_value  rf_json_value;
  begin
    dbms_lob.createtemporary(v_alert_ids, true);
    v_json_value := rf_json_ext.get_json_value( p_json_in, 'alertIds' );
    v_json_value.to_clob( v_alert_ids );
    /* ������� �������, ����������� �������� */
    v_alert_ids := ','||regexp_replace(v_alert_ids, '[][" ]')||',';

    /* ������ json ������ � ����� �� ����� data json ������ */
    for i in 1..rf_json_ext.get_json_list( p_json_out, 'data' ).count loop
      v_id := rf_json_ext.get_number( p_json_out, 'data['||i||'].REVIEW_ID' );
      v_alert_ids := replace( v_alert_ids, ','||v_id||',', ',' );
    end loop;
    
    /* �������������� ������ � ������� � rf_json_list */
    v_alert_ids := trim( both ',' from v_alert_ids );
    if v_alert_ids is not null then 
      v_json_list := rf_json_list();
      for cur in ( select to_number( column_value ) cv from table( mantas.rf_pkg_scnro.list_to_tab( v_alert_ids ) ) ) loop
        v_json_list.append( cur.cv );
      end loop;
    end if;
    
    return v_json_list;
  end;

  
  /* ����� ���������� ����������� ����� � ����������� �� �������
   *
   * ��������� ������
   *   p_json - json �������
   *   p_supervisor - ����, �������� �� ������������ ������������: 1-��, 0-���
   * ��������� ������
   *   varchar2 - �����, ������� ���������� ������� � �������
   */
  function get_hint( p_json in rf_json, p_supervisor in number ) return varchar2
  is
    v_hint           varchar2(512) := null;
    is_where_exist   number;
    is_filter_exist  number;
    is_alertid_exist number;
  begin
    is_where_exist := nvl(rf_json_ext.get_number(p_json, 'where'), 0);
    is_filter_exist := nvl(rf_json_ext.get_number(p_json, 'filters'), 0);
    is_alertid_exist := nvl(rf_json_ext.get_number(p_json, 'alertIds'), 0);
    
    /* ���� ����������, ������ where = 9001 (��������� ���������), �� ������� ��������������� � ��������, */
    if p_supervisor = 1 and is_where_exist = 9001 and is_alertid_exist = 0 and is_filter_exist = 0 then
      v_hint := '/*+ FIRST_ROWS(1) INDEX(rv.rv RF_KDD_REVIEW_WRK_SPVR_I) */';
    /* ���� ������� (������������), ������ where = 9001 (��������� ���������), �� ������� ��������������� � �������� */
    elsif p_supervisor = 0 and is_where_exist = 9001 and is_alertid_exist = 0 and is_filter_exist = 0 then
      v_hint := '/*+ FIRST_ROWS(1) INDEX(rv.rv RF_KDD_REVIEW_WRK_CTRL_I) */';
    /* ���� ���������� � �� ������� �� where, �� ��������������, �� ������� */
    elsif p_supervisor = 1 and is_where_exist = 0 and is_alertid_exist = 0 and is_filter_exist = 0 then
      v_hint := '/*+ FIRST_ROWS(1) */';
    else
      v_hint := '/*+ FIRST_ROWS(1) INDEX(rv.rv) */';
    end if;
    
    return v_hint;
  end;

  /* ��������� ������� AlertList countAlert
   *
   * ��������� �������
   *   p_json - json ������  {"form":"AlertList","action":"countAlert","filters":{ ... },"where":9001}
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� �������
   *   rf_json - json ����� {"success":"true","count": 123456}
   */
  FUNCTION get_alert_count(
    p_json       IN rf_json,
    p_user       IN VARCHAR2,
    p_id_request IN NUMBER,
    p_response OUT CLOB ) -- �� ����������� ��������� ��� �������������
    RETURN rf_json IS
      v_sql VARCHAR2(32000)    := NULL;
      v_json rf_json           := NULL;
      v_json_filter rf_json    := NULL;
      v_json_list rf_json_list := NULL;
      v_owner_seq_id mantas.kdd_review_owner.owner_seq_id%type;
      v_is_all_alerts NUMBER := 0;
      v_bind_json rf_json  := rf_json;
    BEGIN
      v_sql := q'{ SELECT ##HINT## COUNT(*) AS "count", to_char(round(nvl(sum(rv_trxn_base_am), 0), 2), 'FM999G999G999G999G990D00', 'NLS_NUMERIC_CHARACTERS = ''. ''') as "sume"
        FROM mantas.rfv_opok_review_light rv       
        WHERE rv.rv_prcsng_batch_cmplt_fl = 'Y'             
        ##USER##  ##WHERE## ##FILTER##}';
      
      /* ���� ������������ �� ����������, �� ��������� ������� �� id ������������ */
      v_is_all_alerts   := mantas.rf_pkg_adm.is_all_alerts( p_user );
      IF v_is_all_alerts  = 0
        THEN  -- ���� ������ ������ ����
          BEGIN 
            SELECT owner_seq_id INTO v_owner_seq_id
              FROM mantas.kdd_review_owner
              WHERE owner_id = p_user;
          EXCEPTION
            WHEN no_data_found THEN
              rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
              RETURN rf_json('{"success": "false", "message" : "���������� ������: ������������ �� ������"}');
          END;
          v_sql := REPLACE( v_sql, '##USER##', 'and rv.rv_owner_seq_id = :USERID' );
          v_bind_json.put('USERID', v_owner_seq_id);
        ELSE -- ���������� ������ ���
          v_sql := REPLACE( v_sql, '##USER##', '' );
      END IF;
      
      v_sql := REPLACE( v_sql, '##WHERE##', get_sql_condition( 'and ##VALUE_WHERE##', rf_json_ext.get_json_value( p_json, 'where' ) ) );
      v_sql := REPLACE( v_sql, '##FILTER##', get_filter( rf_json_ext.get_json( p_json, 'filters' ) ) );
      v_sql := REPLACE( v_sql, '##HINT##', get_hint( p_json, v_is_all_alerts  ) );
 
     /* ���������� ��������������� ������� */    
      BEGIN
        IF v_bind_json IS NOT NULL AND v_bind_json.count>0
          THEN v_json_list := rf_json_dyn.executelist( v_sql, v_bind_json );
          ELSE v_json_list := rf_json_dyn.executelist( v_sql );
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
          RETURN rf_json('{"success": "false", "message" : "���������� ������: ����������� SQL ��������"}');
      END;

      v_json := rf_json(v_json_list.get(1));
      v_json.put( 'success', true,1 );
      RETURN v_json;
    END get_alert_count;

  /* ��������� ������� AlertList Read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_alert_list( p_json in rf_json, p_user in varchar2, p_id_request in number, p_response out clob ) return rf_json
  is
    v_sql             varchar2(32000) := null;
    v_json            rf_json := null;
    v_json_filter     rf_json := null;
    v_json_list       rf_json_list := null;
    v_clob            clob := null;
    v_owner_seq_id    mantas.kdd_review_owner.owner_seq_id%type;
    v_is_all_alerts      number := 0;
    v_bind_json       rf_json := rf_json;
    v_cancel_fetch_id mantas.rf_request_fetch.fetch_id%type;
  begin
    v_sql := q'{
      select ##HINT##
             rv.review_id as "_id_",
             rv.review_id as review_id,
             rv.rv_status_nm as rv_status_nm,
             rv.opok_nb as opok_nb,
             rv.add_opoks_tx as add_opoks_tx,
             rv.trxn_base_am as trxn_base_am,
             rv.trxn_crncy_am as trxn_crncy_am,
             rv.trxn_crncy_cd as trxn_crncy_cd,
             rv.trxn_dt as trxn_dt,
             rv.rv_due_dt as rv_due_dt,
             rv.rv_owner_id as rv_owner_id,
             mantas.rf_pkg_review.get_last_note( rv.review_id ) as rv_note_tx,
             case op_cat_cd
               when 'WT' then  '�/���'
               when 'CT' then  '���'
               when 'BOT' then '�����'
             end as op_cat_cd,
             mantas.rf_pkg_scnro.get_explanation( rv.review_id ) as explanation_tx,
             rv.trxn_desc as trxn_desc,
             trim( to_char( rv.nsi_op_id ) || ' ' || rv.nsi_op_type_nm ) as nsi_op,
             rv.orig_nm as orig_nm,
             rv.orig_inn_nb as orig_inn_nb,
             business.rf_pkg_util.format_account( nullif( rv.orig_acct_nb, '-1' ) ) as orig_acct_nb,
             rv.send_instn_nm || decode( rv.send_instn_id, null, null, ' (' || rv.send_instn_id || ')') as send_instn,
             business.rf_pkg_util.format_account( rv.send_instn_acct_id ) as send_instn_acct_id,
             business.rf_pkg_util.format_account( rv.debit_cd ) as debit_cd,
             rv.orig_addr_tx as orig_addr_tx,
             rv.orig_iddoc_tx as orig_iddoc_tx,
             rv.benef_nm as benef_nm,
             rv.benef_inn_nb as benef_inn_nb,
             business.rf_pkg_util.format_account( nullif( rv.benef_acct_nb, '-1' ) ) as benef_acct_nb,
             rv.rcv_instn_nm || decode( rv.rcv_instn_id, null, null, ' (' || rv.rcv_instn_id || ')' ) as rcv_instn,
             business.rf_pkg_util.format_account( rv.rcv_instn_acct_id ) as rcv_instn_acct_id,
             business.rf_pkg_util.format_account( rv.credit_cd ) as credit_cd,
             rv.benef_addr_tx as benef_addr_tx,
             rv.benef_iddoc_tx as benef_iddoc_tx,
             nvl(rv.tb_id,rv.rv_tb_id) as tb_id,
             rv.tb_nm as tb_nm,
             nvl(rv.osb_id,rv.rv_osb_id) as osb_id,
             rv.osb_nm as osb_nm,
             mantas.rf_pkg_review.get_review_scrty_desc( rv.rv_op_cat_cd, rv_trxn_seq_id) as rv_scrty_desc,
             mantas.rf_pkg_review.get_review_trxn_change(rv.review_id) as trxn_change,
             mantas.rf_pkg_review.get_opok_change(rv.review_id) as opok_change,
             mantas.rf_pkg_scnro.get_lexeme_list( rv.review_id, '��' ) as trxn_desc_lex,
             mantas.rf_pkg_scnro.get_lexeme_list( rv.review_id, '�' ) as orig_lex,
             mantas.rf_pkg_scnro.get_lexeme_list( rv.review_id, '�2' ) as benef_lex,
             case 
               when ','||replace( mantas.rf_pkg_adm.get_install_param( 29 ), ' ')||',' like '%,'||rv.rv_status_cd||',%' then 1 
               else 0 
             end as rv_status_highlight,
             case
               when rv.rv_status_cd not in ('RF_CANCEL', 'RF_DLTD-', 'RF_DONE') and trunc(sysdate) > rv.rv_due_dt then 'OVERDUE'
               when rv.rv_status_cd not in ('RF_CANCEL', 'RF_CREADY+', 'RF_READY=', 'RF_DLTD-', 'RF_DONE', 'RF_READY', 'RF_READY+') 
                    and mantas.rf_add_work_days( trunc ( sysdate ), nvl( to_number( mantas.rf_pkg_adm.get_install_param(14, 3 ) ), 1 ) ) >= rv.rv_due_dt then 'NEARDUE' 
               else 'NORMAL'
             end as rv_due_dt_highlight,
             rv.rv_status_cd, 
             rv.rv_object_tp_cd,
             rv.rv_op_cat_cd,
             rv.rv_trxn_seq_id,
             rv.rv_trxn_scrty_seq_id,
             rv.rv_repeat_nb, 
             rv.rv_data_dump_dt,
             rv.rv_cls_dt,
             rv.src_sys_cd, 
             rv.canceled_fl,
             rv.scrty_canceled_fl,
             rv.oes_id oes321id,
             decode( rv.oes_send_fl, 'Y', 'S', rv.oes_check_status ) check_status,
             case 
               when rv.oes_send_fl = 'Y' then '����������'
               when upper(rv.oes_check_status) = 'E' then '������� ���������'
               when upper(rv.oes_check_status) = 'W' then '���� ��������������'
               when upper(rv.oes_check_status) = 'O' then '�������� �������'
               when rv.oes_check_status is null and rv.oes_id is not null then '�� �����������'
               else ''
             end check_status_nm,
             to_char(rv.orig_reg_dt, 'DD.MM.YYYY') orig_reg_dt,
             to_char(rv.benef_reg_dt, 'DD.MM.YYYY') benef_reg_dt,
             rv.trxn_doc_id,
             rv.trxn_doc_dt,
             nvl2(rv.trxn_doc_id, '� '||rv.trxn_doc_id||nvl2(rv.trxn_doc_dt, ' �� '||to_char(rv.trxn_doc_dt,'DD.MM.YYYY'), null), null) trxn_doc,
             nvl( to_char( rv.rv_due_dt, 'YYYYMMDD' ), '99991231' )||
                  nvl( rv.opok_nb, 9999 )||
                  lpad( rv.review_id, 10, '0' ) sort_key,
             mantas.rf_pkg_adm.allow_alert_editing(:p_alert_user,rv.rv_status_cd) allow_alert_editing  
            ,rv.send_kgrko_id AS send_kgrko_id
            ,rv.send_kgrko_nm AS send_kgrko_nm
            ,rv.rcv_kgrko_id AS rcv_kgrko_id
            ,rv.rcv_kgrko_nm AS rcv_kgrko_nm
            ,rv.rv_kgrko_id AS rv_kgrko_id
            ,rv.rv_kgrko_nm||case rv.rv_kgrko_party_cd
                               when 1 then ' (������ �����������)'
                               when 2 then ' (������ ����������)'
                             end AS rv_kgrko_nm
            ,mantas.rf_pkg_review.has_attachments(rv.review_id) has_attachments
            ,case 
               when rv.send_kgrko_id <> rv.rcv_kgrko_id then 1 
               else 0 
             end is_inter_kgrko
             ,rv.rv_creat_ts
             ,trim(rv.orig_goz_op_cd ||' '|| rv.orig_goz_op_nm) as orig_goz_op_cd
             ,trim(rv.benef_goz_op_cd ||' '|| rv.benef_goz_op_nm) as benef_goz_op_cd 
             ,rv.scrty_nb 
             ,rv.scrty_base_am 
             ,case
               when rv.org_desc is not null then rv.org_desc||nvl2(rv.src_user_nm, ' ('||rv.src_user_nm||')', '')
               else rv.src_user_nm 
              end trxn_org_desc 
             ##COUNT##
        from mantas.rfv_opok_review_list rv
       where rv.rv_prcsng_batch_cmplt_fl = 'Y'
             ##USER##
             ##ALERTIDS##
             ##WHERE##
             ##FILTER##
       order by nvl(rv.rv_due_dt, to_date('31.12.9999', 'dd.mm.yyyy'))  asc,
                nvl(rv.opok_nb, 9999) asc,
                rv.review_id asc
    }';

    /* ������������� �������� ������ ��� ����� ��������� ������� */
    v_cancel_fetch_id := nvl(rf_json_ext.get_number(p_json, 'cancelFetchId'), 0);
    if v_cancel_fetch_id > 0 then
      mantas.rf_pkg_request_fetch.update_page_info (
        p_fetch_id  => v_cancel_fetch_id,
        p_status_cd => 12
      );
    end if;
    
    v_bind_json.put('p_alert_user', p_user);
    
    /* ���� ������������ �� ����������, �� ��������� ������� �� id ������������ */
    v_is_all_alerts := mantas.rf_pkg_adm.is_all_alerts( p_user );
    if v_is_all_alerts  = 0 then
      select max(owner_seq_id) into v_owner_seq_id 
        from mantas.kdd_review_owner
       where owner_id = p_user;
      v_sql := replace( v_sql, '##USER##', 'and rv.rv_owner_seq_id = :USERID' );
      v_bind_json.put('USERID', nvl(v_owner_seq_id,0));
    /* ���������� ������ ��� */
    else
      v_sql := replace( v_sql, '##USER##', '' );
    end if;
   
    v_sql := replace( v_sql, '##WHERE##', get_sql_condition( 'and ##VALUE_WHERE##', rf_json_ext.get_json_value( p_json, 'where' ) ) );
    v_sql := replace( v_sql, '##FILTER##', get_filter( rf_json_ext.get_json( p_json, 'filters' ) ) );
    v_sql := replace( v_sql, '##HINT##', get_hint( p_json, v_is_all_alerts  ) );

    /* ������������ �������� */
    if ( rf_json_ext.get_json_value( p_json, 'alertIds' ) is null ) then
      v_sql := replace( v_sql, '##COUNT##', ',  to_number(null) as cnt' );
      v_sql := replace( v_sql, '##ALERTIDS##', '' );
      /* ������������ ������� � ������ ��������� ������ �������� */
      begin
        v_sql := v_sql||'###BIND_JSON###'||v_bind_json.to_char;
        p_response := mantas.rf_pkg_request_fetch.run( v_sql, 'cnt', p_user );
        v_json := null;
        /* ����������� ������ ����������� ������� */
        exception
          when others then
            rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
            v_clob := rf_pkg_request.exec_exception( '��������� ���������� ������.'||chr(13)||sqlerrm() );
            v_json := rf_json(v_clob);
      end;  
    else
    /* ������� �������� */
      v_sql := replace( v_sql, '##COUNT##', '' );
      v_sql := replace( v_sql, '##ALERTIDS##', get_sql_condition( 'and rv.review_id ##ALERTIDS##', rf_json_ext.get_json_value( p_json, 'alertIds' ) ) );
      /* ��������� ��������������� ������� */
      v_json := rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
      /* ���������� ������ � ���� filtered */
      v_json_list := get_filetered_keys( p_json, v_json );
      if v_json_list is not null then
        v_json.put( 'filtered', v_json_list );
      end if;
    end if;  

    return v_json;
  end;

  /* ����� ��������� ������ ��� ��������� ��� ���� ����������� �������
   *
   * ��������� ������
   *   p_json - json ������
   *   p_action - ��������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����

   */
  function get_alert_list_page( p_json in rf_json, p_action in varchar, p_user in varchar2, p_id_request in number, p_response out clob ) return rf_json
  is
    v_json rf_json := null;
    v_fetch_id number;
    v_clob clob;
  begin
    v_fetch_id := rf_json_ext.get_number( p_json, 'fetchId' );
    if v_fetch_id > 0 then
      /* ��������� ��������� �������� */
      if p_action = 'readNext' then 
        p_response := rf_pkg_request_fetch.get_next_page( v_fetch_id, p_user );
      /* ��������� ��� ���������� �������� */  
      elsif p_action = 'readLast' then 
        /* ������������� ������ � FORCE FETCH */
        mantas.rf_pkg_request_fetch.update_page_info (
          p_fetch_id  => v_fetch_id,
          p_status_cd => 11
        );
        p_response := rf_pkg_request_fetch.get_all_page( v_fetch_id, p_user );
      end if;
    end if;
    
    return v_json;
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        v_clob := rf_pkg_request.exec_exception( '��������� ���������� ������.'||chr(13)||sqlerrm() );
        v_json := rf_json(v_clob);
  end;

  /* ��������� ������� AlertAction readById, readByFilter
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_alert_action( p_json in out rf_json, p_user in varchar2, p_id_request in number, p_by_id in boolean ) return rf_json
  is
    v_sql           varchar2(32000) := null;
    v_json          rf_json := null;
    v_json_data     rf_json := null;
    v_json_list     rf_json_list := null;
    v_clob          clob := null;
    v_cursor        sys_refcursor;
    v_actvy_type_cd mantas.kdd_activity_type_cd.actvy_type_cd%type;
    v_actvy_type_nm mantas.kdd_activity_type_cd.actvy_type_nm%type;
    v_alert_edit_fl varchar2(1);
    v_req_cmmnt_fl  mantas.kdd_activity_type_cd.req_cmmnt_fl%type;
    v_alert_count   number;
    v_total_count   number;
  begin
    v_sql := q'{
      with
        status as (
          select distinct rv.rv_status_cd as status_cd, 
                 count(distinct rv.rv_status_cd) over() as cnt, 
                 count(*) over() as rv_cnt 
            from mantas.rfv_opok_review_light rv
           where rv.rv_prcsng_batch_cmplt_fl = 'Y'
                 ##ALERTIDS##
                 ##FILTER##
                 ##WHERE##
        ), 
        actvy as (
          select act.actvy_type_cd, act.actvy_type_nm, act.displ_order_nb, act.actvy_cat_cd, 
                 case 
                   when act.actvy_type_cd in ('RF_SETOPOK', 'MTS003', 'MTS018') then 'Y'
                   else 'N'
                 end as alert_edit_fl,
                 act.req_cmmnt_fl,
                 rv_cnt
            from status
            join mantas.kdd_actvy_type_review_status act_st on act_st.status_cd = status.status_cd
            join mantas.kdd_activity_type_cd act on act.actvy_type_cd = act_st.actvy_type_cd
           group by act.actvy_type_cd, status.cnt, act.actvy_type_nm, act.displ_order_nb, act.actvy_cat_cd, act.req_cmmnt_fl, rv_cnt
          having count(*) = status.cnt
        )
      
      select act.actvy_type_cd,
             act.actvy_type_nm,
             act.alert_edit_fl,
             act.req_cmmnt_fl,
             rv_cnt alertCount
        from actvy act
       where act.actvy_cat_cd = 'ACT' 
         and exists ( select null
                        from mantas.kdd_scnro_class_actvy_type scat
                       where scat.actvy_type_cd = act.actvy_type_cd 
                         and scat.scnro_class_cd = 'ML' )
         and exists( select null
                       from mantas.kdd_role_activity_type rat
                      where rat.actvy_type_cd = act.actvy_type_cd 
                        and mantas.rf_pkg_adm.has_role('##USER##', rat.role_cd) = 1
                   )
         -- ��� ����� ������� �� �������� ������ ORA-07445
         and rownum >=0
       order by act.displ_order_nb,
                act.actvy_type_nm,
                act.actvy_type_cd
    }';
    
    /* ��� ������� readById ������ ������������� ������������� � ������ ������������ ������� */
    if p_by_id = true then
      p_json.path_put('filters.oldAlert', 'Y');
    end if;

    v_sql := replace( v_sql, '##USER##', p_user);
    v_sql := replace( v_sql, '##ALERTIDS##', get_sql_condition( 'and rv.review_id ##ALERTIDS##', rf_json_ext.get_json_value( p_json, 'alertIds' ) ) );
    v_sql := replace( v_sql, '##FILTER##', get_filter( rf_json_ext.get_json( p_json, 'filters' ) ) );
    v_sql := replace( v_sql, '##WHERE##', get_sql_condition( 'and ##VALUE_WHERE##', rf_json_ext.get_json_value( p_json, 'where' ) ) );

    /* ������ ������������ ��������� rf_pkg_request.exec_sql_read(v_sql, p_id_request) �������������� ��������� json */
    v_json := rf_json();
    begin
      v_total_count := 0;
      v_json_list := rf_json_list();
      v_json.put( 'success', true );
      open v_cursor for v_sql;
      loop
        fetch v_cursor into v_actvy_type_cd, v_actvy_type_nm, v_alert_edit_fl, v_req_cmmnt_fl, v_alert_count;
        exit when v_cursor%notfound;
        v_total_count := v_total_count + 1;
        v_json_data := rf_json();
        v_json_data.put( 'ACTVY_TYPE_CD', v_actvy_type_cd );
        v_json_data.put( 'ACTVY_TYPE_NM', v_actvy_type_nm );
        v_json_data.put( 'ALERT_EDIT_FL', v_alert_edit_fl );
        v_json_data.put( 'REQ_CMMNT_FL', v_req_cmmnt_fl );
        v_json_list.append( v_json_data.to_json_value() );
      end loop;
      close v_cursor;
      v_json.put( 'totalCount', v_total_count );
      v_json.put( 'data', v_json_list );
      v_json.put( 'alertCount', v_alert_count );
      exception
        when others then
          rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
          v_clob := rf_pkg_request.exec_exception( 
            '�� ������� ��������� ������!'||chr(13)||'��������� ���������� ������.'||chr(13)||sqlerrm()
          );
          v_json := rf_json(v_clob);
    end;
    
    return v_json;
  end;

  /* ��������� ������� AlertAction execById, execByFilter
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   *   p_by_id - true ���� ������ �� ������ alertIds (execById), false ���� ������ �� ������� (execByFilter)
   * ��������� ������
   *   rf_json - json �����
   */
  function exec_action( p_json in rf_json, p_user in varchar2, p_id_request in number, p_by_id in boolean ) return rf_json
  is
    v_alert_ids  clob;
    v_ids        mantas.rf_tab_number;
    v_type_cds   varchar2(4000) := null;
    v_types      mantas.rf_tab_varchar;

    v_message    clob := null;
    v_out_ids    mantas.rf_tab_number;
    v_json       rf_json := null;
    v_json_list  rf_json_list := rf_json_list();
    v_sql        varchar2(32000);
    v_json_value rf_json_value;

    v_oes_321_id   rf_oes_321.oes_321_id%type;
    v_object_tp_cd kdd_review.rf_object_tp_cd%type;
    e_copy_error   exception;
    pragma exception_init(e_copy_error, -20100);
  begin
    /* ������ review_id �� json */
    if p_by_id = true then
      /* �������������� ������ � ������� � rf_tab_number */
      v_json_value := rf_json_ext.get_json_value( p_json, 'alertIds' );
      dbms_lob.createtemporary(v_alert_ids, true);
      v_json_value.to_clob( v_alert_ids );
      v_alert_ids := translate( v_alert_ids, '[]"', ' ' );
      /* ���� ������ ������ ������ */
      if instr( v_alert_ids, ',' ) > 0 then 
        select to_number( column_value ) bulk collect into v_ids
          from table( mantas.rf_pkg_scnro.list_to_tab( v_alert_ids ) );
      else
        v_ids := mantas.rf_tab_number(to_number(v_alert_ids));
      end if ;
    /* ������ review_id �� ������� */    
    else
      v_sql := q'{
        select rv.review_id
          from mantas.rfv_opok_review_light rv
         where rv.rv_prcsng_batch_cmplt_fl = 'Y'
               ##FILTER##
               ##WHERE##
      }';
      v_sql := replace( v_sql, '##FILTER##', get_filter( rf_json_ext.get_json( p_json, 'filters' ) ) );
      v_sql := replace( v_sql, '##WHERE##', get_sql_condition( 'and ##VALUE_WHERE##', rf_json_ext.get_json_value( p_json, 'where' ) ) );
      execute immediate v_sql bulk collect into v_ids;
    end if;
    
    /* �������������� ������ � ������� � rf_tab_varchar */
    v_type_cds := trim( translate( rf_json_ext.get_json_value( p_json, 'actvyTypeCds' ).to_char, '[]"', ' ' ) );
    v_types := mantas.rf_pkg_scnro.list_to_tab( v_type_cds );

    /* ���� �������� - ����� ��� (RF_OESCOPY) */
    if instr(v_type_cds, 'RF_OESCOPY') > 0 then
      if v_ids.count <> 1 then 
        raise_application_error(-20100, '����������� ��� �������� ������ ��� ����� ��������� ��������.');
      else
        /* ���������, ��� ���� �������� ��������� ��� */
        begin
          select o.oes_321_id, r.rf_object_tp_cd 
            into v_oes_321_id, v_object_tp_cd
            from rf_oes_321 o
            join kdd_review r on r.review_id = o.review_id 
           where o.current_fl = 'Y' 
             and r.review_id = v_ids(1);
          exception
            when no_data_found then
              raise_application_error(-20100, '��� �������� � '||v_ids(1)||' ��� ��������� ���.');
            when too_many_rows then
              raise_application_error(-20100, '��� �������� � '||v_ids(1)||' ������� ������ ������ �������� ���.');
        end;
        /* ���������, ��� ��������� ������� �������. */
        if upper(v_object_tp_cd) <> 'OES321' then
          raise_application_error(-20100, '�������� ��������� ������ ��� ���, ��������� �������');
        end if;
      end if;
    end if;

    /* ����� ������ ��� ���������� �������� */
    v_message := rf_pkg_review.do_actions(
      par_review_ids          => v_ids,
      par_actvy_type_cds      => v_types,
      par_new_review_owner_id => case 
                                   when rf_json_ext.get_json_value( p_json, 'ownerId' ) is not null 
                                     then rf_json_ext.get_json_value( p_json, 'ownerId' ).get_number
                                   else null
                                 end,  
      par_due_dt              => case 
                                   when rf_json_ext.get_json_value( p_json, 'dueDt' ) is not null 
                                     then to_date( rf_json_ext.get_json_value( p_json, 'dueDt' ).get_string, rf_json_ext.format_string )
                                   else null
                                 end,  
      par_note_tx             => case
                                   when rf_json_ext.get_json_value( p_json, 'noteTx' ) is not null 
                                     then rf_json_ext.get_json_value( p_json, 'noteTx' ).get_string
                                   else null
                                 end,
      par_owner_id            => p_user,
      par_cmmnt_id            => case 
                                   when rf_json_ext.get_json_value( p_json, 'cmmntId' ) is not null 
                                     then rf_json_ext.get_json_value( p_json, 'cmmntId' ).get_number
                                   else null
                                 end,
      par_opok_nb             => case 
                                   when rf_json_ext.get_json_value( p_json, 'opok' ) is not null 
                                     then rf_json_ext.get_json_value( p_json, 'opok' ).get_number
                                   else null
                                 end,
      par_add_opoks_tx        => case
                                   when rf_json_ext.get_json_value( p_json, 'addOpoks' ) is not null 
                                     then rf_json_ext.get_json_value( p_json, 'addOpoks' ).get_string
                                   else null
                                 end,
      par_act_review_ids      => v_out_ids
    );

    /* ���� ������ �� �������, �� � ��������� ��������� ��������� ��������� �� ���������� */
    if p_by_id = false then
      v_message := v_message||'</br><b>����� ������� ��������� ������������ ��������(-��) ���������� �������� ������ ��������!</b>';
    end if;

    /* ����� json � ������� ���������, ���� �� ���� � ������� alertIds */
    v_json := rf_json();
    v_json.put('success', true);
    if v_message is not null 
      then v_json.put('message', v_message);
    end if;  
    
    /* ������ ������ ��������� ������ ��� ������� execById */
    if p_by_id = true and v_out_ids.count > 0 then
      for i in 1..v_out_ids.count loop
        v_json_list.append(v_out_ids(i));
      end loop;
    end if;
    v_json.put('alertIds', v_json_list);

    return v_json;
    
    exception
      when e_copy_error then
        v_json := rf_json();
        v_json.put('success', false);
        v_json.put('message', trim(regexp_replace(sqlerrm, '^ORA-[0-9]{5}:')));
        return v_json;
  end;

  /* ��������� ������� AlertRuleSnapshot read
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_alert_rule_snapshot( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_json          rf_json := null;
    v_json_elem     rf_json := null;
    v_json_data     rf_json_list := null;
    v_json_child    rf_json := null;
    v_json_children rf_json_list := null;
    v_json_leaf     rf_json := null;  
    v_json_leafs    rf_json_list := null;
    
    v_alert_id      number := null;
    v_repeat_nb     number := null;
    v_clob          clob := null;
    
    cursor cur_snapshots (p_repaet_nb number, p_alert_id number ) is
      select --������������ ������� 
             to_char(snp.opok_nb)||', ������� ���������'||nvl2(rl.rule_seq_id, ' � '||to_char(rl.rule_cd)||', ������ '||to_char(rl.vers_number), ' (id='||to_char(snp.rule_seq_id)||')') as rule_nm,
           
             /* ���� "��������" */
             --���������
             case snp.op_cat_cd
               when 'CT' then '��������'
               when 'WT' then '�����������'
               when 'BOT' then '����������'
             end as op_cat,
             --�����������
             case 
               when snp.dbt_cdt_cd is not null then case snp.dbt_cdt_cd
                                                      when 'C' then '����������'
                                                      when 'D' then '��������'
                                                    end
             end as dbt_cdt,
             --��������� ����� 
             case
               when snp.limit_am <> 0 then to_char( snp.limit_am, 'FM9G999G999G999G999G999G999D00', 'NLS_NUMERIC_CHARACTERS=''. ''' )
             end as limit_am,
             --�������
             case
               when snp.tb_id <> 0 then nvl(tb.org_nm, '� '||to_char(snp.tb_id))
             end as tb,
             --������������� ��������
             case
               when snp.kgrko_party_cd = 0 then '� ����� ��������'
               when snp.kgrko_party_cd = 1 then '� ������� �� ������'
               when snp.kgrko_party_cd = 2 then '� ������� �� �������'
             end as kgrko_party_tx,
      
             /* ���� "���� ��������� 1" */
             --������������ �����
             case 
               when snp.op_cat_cd = 'WT' or snp.dbt_cdt_cd = 'D' then '���� �����������'
               when snp.op_cat_cd <> 'WT' and snp.dbt_cdt_cd = 'C' then '���� ����������'
               else '���� ���������' 
             end as acct_header,
             --�������
             snp.acct_crit_tx,
             --���������� �������
             case 
               when snp.acct_crit_tx is not null and snp.acct_subtle_crit_tx is not null then snp.acct_subtle_crit_tx
               else null
             end acct_subtle_crit_tx,
             --������� ������
             case 
               when snp.acct_crit_tx is not null then replace( replace( replace( replace( replace( nvl( snp.acct_area_tx, '��' ), 
                                                                '��', '���� ���������' ),
                                                                '��', '���� ��������' ),
                                                                '��', '������������ ���������' ),
                                                                '��', '���������� �������' ), 
                                                                ',',', ' )
               else null
             end as acct_area_tx,
             --����������� ����
             case 
               when snp.acct_crit_tx is not null then case nvl(snp.acct_null_fl, 'N')
                                                        when 'Y' then '��'
                                                        when 'N' then '���'
                                                      end
               else null 
             end as acct_null_fl,

             /* ���� ���������� */
             -- ��������
             case
               when snp.cust_watch_list_cd is not null then wl1.watch_list_cd||' - '||wl1.watch_list_nm
               else null
             end as watch_list_nm_1,
             -- ���������� �� �������
             case 
               when snp.cust_watch_list_cd is not null then decode(nvl(snp.cust_not_watch_list_fl, 'N'),
                                                                   'Y', '<b>��</b> ����������� �������',
                                                                   'N', '����������� �������',
                                                                    null) 
               else null
             end watch_list_caption_1,

             /* ���� "���� ��������� 2" */
             --������������ �����
             decode( snp.op_cat_cd, 
                     'WT', '���� ����������'
                   ) as acct2_header,
             --�������
             snp.acct2_crit_tx,
             --���������� �������
             case 
               when snp.acct2_crit_tx is not null and snp.acct2_subtle_crit_tx is not null then snp.acct2_subtle_crit_tx
               else null
             end as acct2_subtle_crit_tx,
             --������� ������
             case 
               when snp.acct2_crit_tx is not null then replace( replace( replace( replace( replace( nvl( snp.acct2_area_tx, '��' ), 
                                                                 '��', '���� ���������' ),
                                                                 '��', '���� ��������' ),
                                                                 '��', '������������ ���������' ),
                                                                 '��', '���������� �������'),
                                                                 ',',', ' ) 
               else null
             end as acct2_area_tx,
             --����������� ����
             case 
               when snp.acct2_crit_tx is not null then case nvl(snp.acct2_null_fl, 'N')
                                                         when 'Y' then '��'
                                                         when 'N' then '���'
                                                       end
               else null
             end as acct2_null_fl,
      
             /* ���� "�������� ������� �� �����" */
             decode( snp.acct_reverse_fl,
                     'Y', '��',
                     null
                   ) as acct_reverse, 
             
             /* ���� ���������� */
             -- ��������
             case
               when snp.cust2_watch_list_cd is not null then wl2.watch_list_cd||' - '||wl2.watch_list_nm
               else null
             end as watch_list_nm_2,
             -- ���������� �� �������
             case 
               when snp.cust2_watch_list_cd is not null then decode(nvl(snp.cust2_not_watch_list_fl, 'N'),
                                                                    'Y', '<b>��</b> ����������� �������',
                                                                    'N', '����������� �������',
                                                                    null) 
               else null
             end watch_list_caption_2,
             
             /* ���� "�������" */
             --���������� �������
             snp.lexeme_purpose_tx,
             --������������ 1 - ���������
             case 
               when snp.lexeme_cust_tx is not null and (snp.op_cat_cd = 'WT' or snp.dbt_cdt_cd = 'D') then '������������ �����������'
               when snp.lexeme_cust_tx is not null and snp.op_cat_cd <> 'WT' and snp.dbt_cdt_cd = 'C' then '������������ ����������'
               when snp.lexeme_cust_tx is not null then '������������ ���������'
             end as lex1_cust1_desc,
             --����������� 1
             snp.lexeme_cust_tx,
             --������������ 2 - ���������
             case 
               when snp.lexeme_cust2_tx is not null and snp.op_cat_cd = 'WT' then '������������ ����������'
             end as lex1_cust2_desc,
             --������������ 2
             snp.lexeme_cust2_tx,
      
             -- ���� "������� - 2"
             --���������� �������
             snp.lexeme2_purpose_tx,
             --������������ 1 - ���������
             case 
               when snp.lexeme2_cust_tx is not null and (snp.op_cat_cd = 'WT' or snp.dbt_cdt_cd = 'D') then '������������ �����������'
               when snp.lexeme2_cust_tx is not null and snp.op_cat_cd <> 'WT' and snp.dbt_cdt_cd = 'C' then '������������ ����������'
               when snp.lexeme2_cust_tx is not null then '������������ ���������'
             end as lex2_cust1_desc,
             --����������� 1
             snp.lexeme2_cust_tx,
             --����������� 2 - ���������
             case 
               when snp.lexeme2_cust2_tx is not null and snp.op_cat_cd = 'WT' then '������������ ����������'
             end as lex2_cust2_desc,
             --����������� 2
             snp.lexeme2_cust2_tx,
            
             /* ���� "������ �������" */
             --���� �������� ��� ���
             case 
               when trim( snp.optp_crit_tx ) is not null then ( select listagg( trim( trim( t.column_value )||' '||optp.op_type_nm ), ', ' ) within group( order by optp.nsi_op_id, trim( t.column_value ) )
                                                                  from table( mantas.rf_pkg_scnro.list_to_tab( trim( snp.optp_crit_tx ) ) ) t
                                                                  left join business.rf_nsi_op_types optp on optp.nsi_op_id = case 
                                                                                                                                 when regexp_like( trim( t.column_value ), '^\d+$' )  then to_number( trim( t.column_value ) )
                                                                                                                               end
                                                              )
             end as op_type_nms,
             --�������-���������
             trim( snp.src_crit_tx ) as src_crit_tx,
             --SQL-�������
             trim( snp.custom_crit_sql_tx ) as custom_crit_sql_tx,
             --�������� SQL-�������
             trim( snp.custom_crit_param_tx ) as custom_crit_param_tx,
             --���� ��������
             case 
               when snp.start_dt is not null or snp.end_dt is not null 
                 then trim( nvl2( snp.start_dt, '� '||to_char( snp.start_dt, 'dd.mm.yyyy' ), null )||
                            nvl2( snp.end_dt, ' �� '||to_char( snp.end_dt, 'dd.mm.yyyy' ), null ) )
               else null
             end as dates,
      
             /* ���� "���������� � �������" */
             --��������
             trim( snp.note_tx ) as note_tx,
             --���������
             to_char( snp.priority_nb ) as priority_nb,
             --��� �������
             snp.query_cd,
             --���������� ������
             decode( snp.actual_fl, 
                     'Y', '��', 
                     '���'
                   ) actual_fl
        from mantas.kdd_review rv
        join mantas.rf_kdd_review_opok rvo on rvo.review_id = rv.review_id and rvo.repeat_nb = nvl(p_repaet_nb, rv.rf_repeat_nb)
        join mantas.rf_opok_rule_snapshot snp on snp.snapshot_id = rvo.snapshot_id
        left join mantas.rf_opok_rule rl on rl.rule_seq_id = snp.rule_seq_id
        left join business.org tb on tb.org_intrl_id = to_char(snp.tb_id)
        left join mantas.rf_watch_list wl1 on wl1.watch_list_cd = snp.cust_watch_list_cd 
        left join mantas.rf_watch_list wl2 on wl2.watch_list_cd = snp.cust2_watch_list_cd 
       WHERE rv.review_id = p_alert_id;


    /* ����� ��������� ������������ �������. ������� ������ �������.
     * ������ �������� � ����������� ������������� ������ 
     */
    procedure add_json_parent( p_name in varchar2 default null )
    is
    begin
      v_json_child := rf_json();
      v_json_leafs := rf_json_list();
      if p_name is not null then
        v_json_child.put( 'COLUMN_NAME', p_name );
        v_json_child.put( 'COLUMN_VALUE', '' );
        v_json_child.put( 'expanded', true );
      end if;  
    end;
    
    /* ����� ��������� �������� �������.
     * ������ �������� � ����������� ������������� ������ 
     */
    procedure add_json_leaf( p_name in varchar2, p_value in varchar2 )
    is
    begin
      v_json_leaf := null;
      if p_value is not null then
        v_json_leaf := rf_json();
        v_json_leaf.put( 'COLUMN_NAME' , p_name );
        v_json_leaf.put( 'COLUMN_VALUE', p_value );
        v_json_leaf.put( 'leaf', true );
        v_json_leafs.append( v_json_leaf.to_json_value );
      end if;  
    end;
    
    /* ����� ��������� ������������ ������� json � ��������� json.
     *   ALL - ��������� ���� ������ �������� ���������
     *   LAST - ��������� ��������� �������� �������
     * ������ �������� � ����������� ������������� ������ 
     */
    procedure append_json( p_last_leaf in varchar2 default 'ALL' )
    is
    begin
      if p_last_leaf = 'ALL' then
        if v_json_leafs.count > 0 then
          v_json_child.put( 'children', v_json_leafs );
          v_json_children.append(v_json_child.to_json_value);
        end if;
      elsif p_last_leaf = 'LAST' then
        if v_json_leaf is not null 
          then v_json_children.append(v_json_leaf.to_json_value);
        end if;  
      end if;
    end;
    
  begin
  
    if rf_json_ext.get_json_value( p_json, 'repeatNb' ) is not null 
      then v_repeat_nb := rf_json_ext.get_json_value( p_json, 'ownerId' ).get_number;
      else v_repeat_nb := null;
    end if;
    
    if rf_json_ext.get_json_value( p_json, 'alertId' ) is not null 
      then v_alert_id := rf_json_ext.get_json_value( p_json, 'alertId' ).get_number;
      else v_alert_id := null;
    end if;
    
    v_json := rf_json();
    v_json_data := rf_json_list();
    v_json.put('success', true);
  
    for cur in cur_snapshots(v_repeat_nb, v_alert_id) loop
      /* ���������� ������ ������ � ������� json */
      v_json_elem := rf_json();
      --������������ ������� 
      v_json_elem.put( 'COLUMN_NAME', cur.rule_nm );
      v_json_elem.put( 'COLUMN_VALUE', '' );
      v_json_elem.put( 'expanded', true );

      v_json_children := rf_json_list();
      
      --���� "��������"
        add_json_parent( '��������' );
          add_json_leaf( '���������', cur.op_cat );
          add_json_leaf( '�����������', cur.dbt_cdt);
          add_json_leaf( '��������� �����', cur.limit_am);
          add_json_leaf( '�������', cur.tb);
          add_json_leaf( '������������� �������� �������� ��������', cur.kgrko_party_tx);
        append_json;
          
      --���� "���� ��������� 1"
        add_json_parent( cur.acct_header );
          add_json_leaf( '�������', cur.acct_crit_tx );
          add_json_leaf( '���������� �������', cur.acct_subtle_crit_tx);
          add_json_leaf( '������� ������', cur.acct_area_tx);
          add_json_leaf( '����������� ����', cur.acct_null_fl);
        append_json;
       
        --���� "����������"
        if cur.watch_list_nm_1 is not null then
          add_json_parent( '����������' );
            add_json_leaf( cur.watch_list_caption_1, cur.watch_list_nm_1 );
          append_json;  
        end if;
        
      --���� "���� ��������� 2"
        add_json_parent( cur.acct2_header );
          add_json_leaf( '�������', cur.acct2_crit_tx );
          add_json_leaf( '���������� �������', cur.acct2_subtle_crit_tx);
          add_json_leaf( '������� ������', cur.acct2_area_tx);
          add_json_leaf( '����������� ����', cur.acct2_null_fl);
        append_json;

        --���� "����������"
        if cur.watch_list_nm_1 is not null then
          add_json_parent( '����������' );
            add_json_leaf( cur.watch_list_caption_2, cur.watch_list_nm_2 );
          append_json;  
        end if;

      --���� "�������� ������� �� �����"  
        add_json_parent();
          add_json_leaf( '�������� ������� �� �����', cur.acct_reverse );
        append_json( 'LAST' );
      
      --���� "�������"
        add_json_parent( '�������' );
          add_json_leaf( '���������� �������', cur.lexeme_purpose_tx );
          add_json_leaf( cur.lex1_cust1_desc, cur.lexeme_cust_tx);
          add_json_leaf( cur.lex1_cust2_desc, cur.lexeme_cust2_tx);
        append_json;
        
      --���� "������� - 2"  
        add_json_parent( '������� - 2' );
          add_json_leaf( '���������� �������', cur.lexeme2_purpose_tx );
          add_json_leaf( cur.lex2_cust1_desc, cur.lexeme2_cust_tx);
          add_json_leaf( cur.lex2_cust2_desc, cur.lexeme2_cust2_tx);
        append_json;
      
      --���� "������ �������"
        add_json_parent( '������ �������' );
          add_json_leaf( '���� �������� ��� ���', cur.op_type_nms );
          add_json_leaf( '�������-���������', cur.src_crit_tx);
          add_json_leaf( 'SQL-�������', cur.custom_crit_sql_tx);
          add_json_leaf( '�������� SQL-�������', cur.custom_crit_param_tx);
          add_json_leaf( '���� ��������', cur.dates);
        append_json;
        
      --���� "���������� � �������"
        add_json_parent( '���������� � �������' );
          add_json_leaf( '��������', cur.note_tx );
          add_json_leaf( '���������', cur.priority_nb);
          add_json_leaf( '��� �������', cur.query_cd);
          add_json_leaf( '���������� ������', cur.actual_fl);
        append_json;
        
      v_json_elem.put('children', v_json_children);
      v_json_data.append(v_json_elem.to_json_value);
    end loop;
    
    v_json.put('data', v_json_data);

    return v_json;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        v_clob := rf_pkg_request.exec_exception( 
          '�� ������� ��������� ������!'||chr(13)||'��������� ���������� ������.'||chr(13)||sqlerrm()
        );
        return rf_json( v_clob );
  end;

  /* ��������� ������� AlertStatistics read
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_alert_statistics(  p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_sql          varchar2(4000);
    v_owner_seq_id mantas.kdd_review_owner.owner_seq_id%type;
  begin
    v_sql := q'{
      select rownum as "v_$0", rownum as "_id_", z.* 
        from ( select sum( case 
                             when rf_wrk_flag = 1 then 1 
                             else 0 
                           end ) as work_qty,
                      sum( case
                             when rf_wrk_flag = 1
                                  and t.neardue_dt >= due_dt 
                                  and due_dt >= trunc( sysdate ) then 1 
                             else 0 
                           end ) as neardue_qty, 
                      sum( case 
                             when status_cd not in ( 'RF_CANCEL', 'RF_DLTD-', 'RF_DONE' ) and trunc( sysdate ) > due_dt then 1 
                             else 0 
                           end ) as overdue_qty
                 from mantas.kdd_review rv 
                 left join mantas.kdd_review_owner rvo on rvo.owner_seq_id = rv.owner_seq_id
                 left join (select mantas.rf_add_work_days( trunc( sysdate ), nvl( to_number( mantas.rf_pkg_adm.get_install_param( 14, 3 ) ), 1 ) ) as neardue_dt
                              from dual) t on 1=1
                where rv.prcsng_batch_cmplt_fl = 'Y' 
                  and rf_partition_key = 'ACT'
                  and rf_alert_type_cd = 'O'
                  ##USER##
             ) z
    }';
    /* ���� ������������ �� ����������, �� ��������� ������� �� id ������������ */
    if mantas.rf_pkg_adm.is_all_alerts ( p_user ) = 0 then
      select max(owner_seq_id) into v_owner_seq_id 
        from mantas.kdd_review_owner
       where owner_id = p_user;
      v_sql := replace( v_sql, '##USER##', 'and rv.owner_seq_id = '||nvl(v_owner_seq_id,0) );
    /* ���������� ������ ��� */  
    else
      v_sql := replace( v_sql, '##USER##', '' );
    end if;

    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  end;
  
  
  /* ��������� ������� AlertHistory readByAlertId
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_alert_history( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is 
    v_sql varchar2(4000);
  begin
    v_sql := q'{
      select rownum "_id_",
             ka.start_dt,
             kat.actvy_type_nm,
             kro.owner_id,
             kct.code_disp_tx new_status_tx,
             nvl( trim( knh.note_tx||nvl2( knh.note_tx, nvl2( kc.cmmnt_tx, '; ', null )||kc.cmmnt_tx, kc.cmmnt_tx ) ), '--' ) cmmnt,
             ra.attch_nm,
             ra.file_nm,
             case
               when oes.oes_321_id is not null then '� '||nvl( to_char( oes.numb_p ), '...' )||' �� '||nvl( to_char( oes.date_p, 'DD.MM.YYYY HH24:MI:SS' ), '...' )||'('||to_char( oes.oes_321_id )||')' 
               else null
             end oes_desc,
             ra.content_type_tx,
             ra.attch_seq_id,
             nvl2( ra.content_data,
                   case 
                     when dbms_lob.getlength(ra.content_data) < 1024 then dbms_lob.getlength(ra.content_data) || ' �'
                     when dbms_lob.getlength(ra.content_data) < 1048576 then round( dbms_lob.getlength(ra.content_data)/1024, 1)||' ��'
                     else round( dbms_lob.getlength(ra.content_data)/1024/1024, 1)||' ��'
                   end,
                   null 
                 ) file_size,
             ra.deleted_fl,
             oes.oes_321_id
        /* ������ �������� */
        from mantas.kdd_activity ka
        /* ��� �������� */
        join mantas.kdd_activity_type_cd kat on kat.actvy_type_cd = ka.actvy_type_cd
        /* ��� ������������ */
        join mantas.kdd_review_owner kro on kro.owner_seq_id = ka.creat_id
        /* ��� ������� */
        join mantas.kdd_code_set_trnln kct on kct.code_val = ka.new_review_status_cd and kct.code_set = 'AlertStatus'
        /* ����������� ����������� */
        left join mantas.kdd_activity_cmmnt kac on kac.actvy_id = ka.actvy_id
        left join mantas.kdd_cmmnt kc on kc.cmmnt_id = kac.cmmnt_id
        /* ����������� � ��������� ����� */
        left join mantas.kdd_activity_note kan on kan.actvy_id = ka.actvy_id
        left join mantas.kdd_note_hist knh on knh.note_id = kan.note_id
        /* ����������� ��������� ������ */
        left join mantas.kdd_activity_doc kad on kad.actvy_id = ka.actvy_id
        left join mantas.rf_attachment ra on ra.attch_seq_id = kad.doc_id
        /* ��������� */
        left join mantas.rf_oes_321 oes on oes.oes_321_id = ka.rf_oes_id
       where ka.review_id = ##ALERTID##
    }';
    v_sql := replace( v_sql, '##ALERTID##', rf_json_ext.get_number( p_json, 'alertId' ) );
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  end;
  
  
  /* ��������� ������� AlertAttachments readByAlertId
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_alert_attachments( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is 
    v_sql varchar2(4000);
  begin
    v_sql := q'{
      select ra.attch_seq_id "_id_", 
             ra.attch_seq_id,
             ra.attch_nm, ra.file_nm, 
             nvl( trim( knh.note_tx||nvl2( knh.note_tx, nvl2( kc.cmmnt_tx, '; ', null )||kc.cmmnt_tx, kc.cmmnt_tx ) ), '--' ) cmmnt,
             ra.created_by, ra.created_date, ra.modified_by, ra.modified_date,
             nvl2( ra.content_data,
                   case 
                     when dbms_lob.getlength(ra.content_data) < 1024 then dbms_lob.getlength(ra.content_data) || ' �'
                     when dbms_lob.getlength(ra.content_data) < 1048576 then round( dbms_lob.getlength(ra.content_data)/1024, 1)||' ��'
                     else round( dbms_lob.getlength(ra.content_data)/1024/1024, 1)||' ��'
                   end,
                   null 
                 ) file_size
        from mantas.rf_attachment ra
        join mantas.kdd_activity_doc kad on kad.doc_id = ra.attch_seq_id
        join mantas.kdd_activity ka on ka.actvy_id = kad.actvy_id
        /* ����������� ����������� */
        left join mantas.kdd_activity_cmmnt kac on kac.actvy_id = ka.actvy_id
        left join mantas.kdd_cmmnt kc on kc.cmmnt_id = kac.cmmnt_id
        /* ����������� � ��������� ����� */
        left join mantas.kdd_activity_note kan on kan.actvy_id = ka.actvy_id
        left join mantas.kdd_note_hist knh on knh.note_id = kan.note_id
       where ka.review_id = ##ALERTID## 
         and actvy_type_cd = 'MTS008'
         and ra.deleted_fl = 'N'
    }';
    v_sql := replace( v_sql, '##ALERTID##', rf_json_ext.get_number( p_json, 'alertId' ) );
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  end;
  
  
  /* ��������� ������� AlertAttachments update
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function update_alert_attachment( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is 
    v_json_data rf_json;
    v_json      rf_json;
    v_sql  varchar2(32000);
    v_id   number;
    v_allow NUMBER;
  begin
    v_json_data := rf_json_ext.get_json( p_json, 'data[1]' );
    v_id := rf_json_ext.get_number( v_json_data, '_id_' );
    
    -- �������� �� �����
    SELECT NVL(MAX(mantas.rf_pkg_adm.allow_alert_editing(p_user,kr.status_cd)),0)
      INTO v_allow
      FROM mantas.rf_attachment ra
        JOIN mantas.kdd_activity_doc kad ON kad.doc_id = ra.attch_seq_id
        JOIN mantas.kdd_activity ka ON ka.actvy_id = kad.actvy_id
        JOIN mantas.kdd_review kr ON kr.review_id = ka.review_id
      WHERE ra.ATTCH_SEQ_ID=v_id;
    IF  v_allow=0 THEN
      v_json := rf_json();
      v_json.put('success', false);
      v_json.put('message','��������� ������ ���������� �������� � ���� �������!');
      RETURN v_json;
    END IF;
    
    v_json_data.put('MODIFIED_DATE', to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS'));
    v_json_data.put('MODIFIED_BY', p_user);

    /* ��������� ���������� */
    v_json := rf_json( rf_pkg_request.exec_sql_update(
                         p_sql        => rf_pkg_request.update_generate(
                                           p_schema     => 'MANTAS',
                                           p_table_name => 'RF_ATTACHMENT',
                                           p_key_name   => 'ATTCH_SEQ_ID',
                                           p_data       => v_json_data
                                         ),
                         p_bind_param => v_id,
                         p_id_req     => p_id_request,
                         p_schema     => 'MANTAS',
                         p_table      => 'RF_ATTACHMENT',
                         p_id_name    => 'ATTCH_SEQ_ID',
                         p_id_type    => 'NUMBER'
                       )
                     );  

    return v_json;
  end;
  
  
  /* ��������� ������� AlertAttachments delete
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function delete_alert_attachment( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is 
    v_json rf_json;
    v_id   number;
    v_allow NUMBER;    
  begin
    v_json := rf_json_ext.get_json( p_json, 'data[1]' );
    v_id:=rf_json_ext.get_number( v_json, '_id_' );
    
    -- �������� �� �����
    SELECT NVL(MAX(mantas.rf_pkg_adm.allow_alert_editing(p_user,kr.status_cd)),0)
      INTO v_allow
      FROM mantas.rf_attachment ra
        JOIN mantas.kdd_activity_doc kad ON kad.doc_id = ra.attch_seq_id
        JOIN mantas.kdd_activity ka ON ka.actvy_id = kad.actvy_id
        JOIN mantas.kdd_review kr ON kr.review_id = ka.review_id
      WHERE ra.ATTCH_SEQ_ID=v_id;
    IF  v_allow=0 THEN
      v_json := rf_json();
      v_json.put('success', false);
      v_json.put('message','��������� ������ ���������� �������� � ���� �������!');
      RETURN v_json;
    END IF;
    
    mantas.rf_pkg_review.delete_attachment(
      p_attch_id => v_id,
      p_user => p_user
    );
    
    /* ��������� � ������� json */
    v_json := rf_json();
    v_json.put('success', true);
    return v_json;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        v_json := rf_json();
        v_json.put('success', false);
        v_json.put('message', '������ �������� �����. '||sqlerrm());
        return v_json;
  end;
  
  
  /* ��������� ������� AlertNarrative readByAlertId
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_alert_narrative( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_sql varchar2(1024);
  begin
    v_sql := q'{
      select krn.review_id "_id_", knh.note_tx, kro.owner_id modified_by, knh.creat_ts modified_date
        from mantas.kdd_review_narrative krn
        join mantas.kdd_note_hist knh on knh.note_id = krn.note_id
        join mantas.kdd_review_owner kro on kro.owner_seq_id = knh.creat_id
       where krn.review_id = ##ALERTID##
         and krn.actv_fl = 'Y'
    }';
    v_sql := replace( v_sql, '##ALERTID##', rf_json_ext.get_number( p_json, 'alertId' ) );

    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  end;
  
  
  /* ��������� ������� AlertNarrative updateByAlertId
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function update_alert_narrative( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_json rf_json;
    v_narrative_id number := -1;
    v_clob clob;
    v_id   number;
    v_allow NUMBER;    
  begin
    v_json := rf_json_ext.get_json( p_json, 'data[1]' );
    v_id:=rf_json_ext.get_number( v_json, '_id_' );
     
    -- �������� �� �����
    SELECT NVL(MAX(mantas.rf_pkg_adm.allow_alert_editing(p_user,kr.status_cd)),0)
      INTO v_allow
      FROM mantas.kdd_review kr 
      WHERE kr.review_id=v_id;
    IF  v_allow=0 THEN
      v_json := rf_json();
      v_json.put('success', false);
      v_json.put('message','��������� ������ ���������� �������� � ���� �������!');
      RETURN v_json;
    END IF;
    
    v_narrative_id := mantas.rf_pkg_review.create_narrative(
      p_alert_id  => v_id,
      p_narrative => to_clob( rf_json_ext.get_string( v_json, 'NOTE_TX' ) ),
      p_user      => p_user
    );
    
    v_json := rf_json;
    v_json.put('success', true);
    return v_json;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        v_clob := rf_pkg_request.exec_exception( '�� ������� �������� ������! '||chr(13)||sqlerrm() );
        return rf_json(v_clob);
  end;  


  /* ��������� ������� AlertAction setKGRKO
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function set_kgrko( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_op_cat_cd varchar2(64) := null;
    v_trxn_seq_id number := null;
    v_sql varchar2(2000);
    v_bind_json rf_json;
    v_review_ids mantas.rf_tab_number;
    v_json rf_json;
    v_json_list rf_json_list;
  begin
    /* ������ �������� �� ������� */
    v_op_cat_cd := rf_json_ext.get_string( p_json, 'op_cat_cd' );
    v_trxn_seq_id := rf_json_ext.get_number( p_json, 'trxn_seq_id' );
    /* ���� �� ������ ��������� ��� � ����� ��������, �� ������ */
    if v_op_cat_cd is null or v_trxn_seq_id is null then
      raise_application_error(-20001, '�� ���������� ������ ��������� ��������.');
    end if;    

    /* ��������� ������� ����� */
    v_review_ids := rf_pkg_review.set_trxn_kgrko (
      p_op_cat_cd       => v_op_cat_cd,
      p_trxn_seq_id     => v_trxn_seq_id,
      p_send_oes_org_id => /* ���� ��������� ����, ��� ������ �������� send_oes_org_id, ����� ���������� �������� � null */
                           case 
                             when rf_json_ext.get_number( p_json, 'send_filial_fl' ) = 1 then rf_json_ext.get_number( p_json, 'send_oes_org_id' )
                             else null
                           end,
      p_rcv_oes_org_id  => /* ���� ��������� ����, ��� ������ �������� rcv_oes_org_id, ����� ���������� �������� � null */
                           case 
                             when rf_json_ext.get_number( p_json, 'rcv_filial_fl' ) = 1 then rf_json_ext.get_number( p_json, 'rcv_oes_org_id' )
                             else null
                           end,
      p_alert_party_fl  => rf_json_ext.get_number( p_json, 'alert_party_fl' ),
      p_user            => p_user
    );

    /* ������ ��� ��������� ����������� ������ �� business.rf_trxn_extra */
    v_sql := q'{
      select op_cat_cd, fo_trxn_seq_id as trxn_seq_id, send_oes_org_id, rcv_oes_org_id, created_by, created_date, modified_by, modified_date
        from business.rf_trxn_extra
       where op_cat_cd = :OP_CAT_CD
         and fo_trxn_seq_id = :TRXN_SEQ_ID
    }';
    v_bind_json := rf_json();
    v_bind_json.put('OP_CAT_CD', v_op_cat_cd);
    v_bind_json.put('TRXN_SEQ_ID', v_trxn_seq_id);
    
    /* ����� json � ������� ���������, ���� �� ���� � ������� alertIds */
    v_json_list := rf_json_list();
    for i in 1..v_review_ids.count loop
      v_json_list.append(v_review_ids(i));
    end loop;
    v_json := rf_json();
    v_json.put('success', true);
    v_json.put('data', rf_json_dyn.executelist( v_sql, v_bind_json ) );
    v_json.put('alertIds', v_json_list);
    return v_json;

    exception
      when others then
        v_json := rf_json();
        v_json.put('success', false);
        v_json.put('message', '������ ���������� �������� �����. '||trim(regexp_replace(sqlerrm, '^ORA-[0-9]{5}:')));
        return v_json;
  end;
  
    /* ��������� ������� AlertAction doubleOES
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  FUNCTION double_oes(p_json IN rf_json, p_user IN VARCHAR2, p_id_request IN NUMBER) RETURN rf_json
  IS
    v_first_review_id   mantas.kdd_review.review_id%TYPE;
    v_opok_nb           mantas.kdd_review.rf_opok_nb%TYPE;
    v_add_opoks_tx      mantas.kdd_review.rf_add_opoks_tx%TYPE;
    v_due_dt            mantas.kdd_review.due_dt%TYPE;
    v_owner_id          mantas.kdd_review.owner_seq_id%TYPE;
    v_cmmnt_id          mantas.kdd_cmmnt.cmmnt_id%TYPE;
    v_note_tx           mantas.kdd_note_hist.note_tx%TYPE;
    v_creator_id        mantas.kdd_review_owner.owner_seq_id%TYPE;     
    v_success           BOOLEAN;
    v_message           VARCHAR2(2000);
    v_second_review_id  mantas.kdd_review.review_id%TYPE;
    v_second_oes_321_id mantas.rf_oes_321.oes_321_id%TYPE;
    v_json rf_json;
  BEGIN
  
    -- ������ ��������� �� ������� 
    v_first_review_id := rf_json_ext.get_number( p_json, 'first_review_id' );
    v_opok_nb         := rf_json_ext.get_number( p_json, 'opok_nb' );
    v_add_opoks_tx    := rf_json_ext.get_string( p_json, 'add_opoks_tx' );
    v_due_dt          := TO_DATE(rf_json_ext.get_string( p_json, 'due_dt'), rf_json_ext.format_string);          
    v_owner_id        := rf_json_ext.get_number( p_json, 'owner_id' );
    v_cmmnt_id        := rf_json_ext.get_number( p_json, 'cmmnt_id' );
    v_note_tx         := rf_json_ext.get_string( p_json, 'note_tx' );
    
    -- ���� �� ��� ��������� ������
    IF v_first_review_id IS NULL OR v_opok_nb IS NULL OR v_due_dt IS NULL OR v_owner_id IS NULL
      THEN raise_application_error(-20001, '���������� ������: ��������� ������������ ��������� ������ doubleOES.');
    END IF;    

    BEGIN 
      SELECT max(owner_seq_id) INTO v_creator_id
        FROM mantas.kdd_review_owner
        WHERE owner_id = p_user;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        raise_application_error(-20001, '���������� ������: ������������ �� ������');
    END;
    
    -- �������� ����������
    v_success:=rf_pkg_kgrko.create_second_oes(
      v_first_review_id
     ,v_opok_nb
     ,v_add_opoks_tx
     ,v_due_dt
     ,v_owner_id
     ,v_creator_id
     ,v_cmmnt_id
     ,v_note_tx
     ,v_message
     ,v_second_review_id
     ,v_second_oes_321_id
    );
    
    -- �������� JSON
    v_json:=RF_JSON();
    v_json.put('success',v_success);
    v_json.put('message',v_message);
    IF v_success THEN
      v_json.put('first_review_id',v_first_review_id);
      v_json.put('second_review_id',v_second_review_id);
      v_json.put('v_second_oes_321_id',v_second_oes_321_id);
    END IF;
    
    RETURN v_json;
  END double_oes;
  
  /* ��������� ������� GOZChanges readByTrxnId
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  
  FUNCTION gozchanges(p_json IN rf_json, p_user IN VARCHAR2, p_id_request IN NUMBER) RETURN rf_json
  IS
    v_bind_json rf_json:=rf_json(); 
    v_json rf_json:=rf_json();
    v_op_cat_cd business.rf_trxn_changes.op_cat_cd%type;
    v_trxn_id business.rf_trxn_changes.fo_trxn_seq_id%type;
    v_sql varchar2(32000);
  BEGIN 
    -- ������ ��������� �� ������� 
    v_op_cat_cd := rf_json_ext.get_string( p_json, 'opCatCd' );
    v_trxn_id   := rf_json_ext.get_number( p_json, 'trxnId' );
    
    if ( v_trxn_id is null or v_op_cat_cd is null ) then 
       raise_application_error(-20001, '���������� ������: ��������� ������������ ��������� ������ GOZchanges.');
    end if;
    
    v_sql:= q'{ select ch.change_dt, 
                       decode(ch.record_type_cd, 'SRC', '������� ��������', 'AML', 'AML') as record_type_cd, 
                       ch.old_orig_goz_op_cd,  
                       ch.new_orig_goz_op_cd, 
                       ch.change_reason_tx,
                       ch.src_user_nm,
                       ch.src_cd
                  from business.rf_trxn_changes ch 
                 where ch.fo_trxn_seq_id = :TRXNID 
                   and upper( ch.op_cat_cd ) = upper( :OPCATCD )
                   and rownum <= 1000
                 order by 1 desc    
                }';
     v_bind_json.put('OPCATCD', v_op_cat_cd);
     v_bind_json.put('TRXNID', v_trxn_id);
     v_json:= rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
    RETURN v_json;
  END gozchanges;
   
  
  /* ������� ��������� ������� ��� ������� 
   *   AlertList read
   *   AlertAction readById
   *   AlertAction readByFilter
   *   AlertAction execById
   *   AlertAction execByFilter
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number, p_response out clob ) return rf_json
  is
    v_request    clob := null;
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
  begin
    /* ������ ���� � ������ ��� "T" */
    v_request := regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2');
    
    v_json_in := rf_json(v_request);
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');

    if v_form_name = 'AlertList' and v_action = 'read' 
      then v_json_out := get_alert_list( v_json_in, p_user, p_id_request, p_response );
    /* ��� ������ � ������������ ��������� ���������� clob, �.�. json �� clob ����� ����� �������� */  
    elsif v_form_name = 'AlertList' and ( v_action = 'readNext' or v_action = 'readLast' ) then 
      v_json_out := get_alert_list_page( v_json_in, v_action, p_user, p_id_request, p_response );
    elsif v_form_name = 'AlertList' and v_action = 'countAlert'  then 
      v_json_out := get_alert_count( v_json_in, p_user, p_id_request, p_response );
    elsif v_form_name = 'AlertAction' and ( v_action = 'readById' or v_action = 'readByFilter' ) 
      then v_json_out := get_alert_action( v_json_in, p_user, p_id_request, case v_action when 'readById' then true else false end );
    elsif v_form_name = 'AlertAction' and ( v_action = 'execById' or v_action = 'execByFilter' )
      then v_json_out := exec_action( v_json_in, p_user, p_id_request, case v_action when 'execById' then true else false end );
    elsif v_form_name = 'AlertRuleSnapshot' and v_action = 'read' 
      then v_json_out := get_alert_rule_snapshot( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertsStatistics' and v_action = 'read' 
      then v_json_out := get_alert_statistics( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertHistory' and v_action = 'readByAlertId' 
      then v_json_out := get_alert_history( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertAttachments' and v_action = 'readByAlertId'
      then v_json_out := get_alert_attachments( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertAttachments' and v_action = 'update'
      then v_json_out := update_alert_attachment( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertAttachments' and v_action = 'delete'
      then v_json_out := delete_alert_attachment( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertNarrative' and v_action = 'readByAlertId'
      then v_json_out := get_alert_narrative( v_json_in, p_user, p_id_request );  
    elsif v_form_name = 'AlertNarrative' and v_action = 'updateByAlertId'
      then v_json_out := update_alert_narrative( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertAction' and v_action = 'setKGRKO'
      then v_json_out := set_kgrko( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'AlertAction' and v_action = 'doubleOES'
      then v_json_out := double_oes( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'GOZChanges' and v_action = 'readByTrxnId'   
      then v_json_out := gozchanges( v_json_in, p_user, p_id_request );
    end if;
    
    return v_json_out;
  end;

end;
/
