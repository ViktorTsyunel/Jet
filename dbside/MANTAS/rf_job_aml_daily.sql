declare
  v_job_action varchar2(1024) := q'{
begin 
  MANTAS.RF_PKG_REVIEW.ARCHIVE_OLD_ALERTS;
end;}';

  v_count number;
begin
  select count(*) into v_count
    from all_scheduler_jobs 
   where job_name = 'RF_JOB_AML_DAILY';
  
  if v_count = 0 then
    /* ������� ���� */
    dbms_scheduler.create_job(
      job_name        => 'MANTAS.RF_JOB_AML_DAILY',
      job_type        => 'PLSQL_BLOCK',
      job_action      => v_job_action,
      start_date      => trunc(systimestamp, 'DD') + numtodsinterval(22, 'HOUR'), 
      repeat_interval => 'freq=DAILY',
      enabled         => TRUE,
      auto_drop       => FALSE,
      comments        => '���� ��������� ������ ��� AML �� ���������� ���� ��� � ����, � 22 ����.'
    );
  else
    /* ��������� ������� job_action */
    if v_job_action is not null then
      dbms_scheduler.set_attribute(
        name => 'MANTAS.RF_JOB_AML_DAILY',
        attribute => 'job_action', 
        value => v_job_action
      );  
    end if;  
  end if;
end;
/

/* ������ ����� (��������) */
--begin
--  dbms_scheduler.run_job(
--    job_name            => 'MANTAS.RF_JOB_AML_DAILY',
--    use_current_session => FALSE
--  );
--end;
--/

/* �������� ����� */
--begin
--  dbms_scheduler.drop_job(
--    job_name => 'MANTAS.RF_JOB_AML_DAILY',
--    force    => TRUE
--  );
--end;
--/

/* �������� ������ ����� */
--select * from all_scheduler_jobs where job_name = 'RF_JOB_AML_DAILY';
--select * from all_scheduler_job_run_details where job_name = 'RF_JOB_AML_DAILY';
--select * from all_scheduler_running_jobs where job_name = 'RF_JOB_AML_DAILY';
