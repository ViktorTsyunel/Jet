grant EXECUTE on BUSINESS.rfv_trxn to MANTAS with grant option;

CREATE OR REPLACE FORCE VIEW mantas.rfv_opok_review as
SELECT /* ORDERED USE_NL(trxn trxn_scrty oes_org) */
       rv.review_id             as review_id, 
       rv.status_cd             as rv_status_cd,
       rv.status_dt             as rv_status_dt,
       rv.creat_ts              as rv_creat_ts,
       rv.creat_id              as rv_creat_id,
       rv.due_dt                as rv_due_dt,
       rv.owner_seq_id          as rv_owner_seq_id, 
       rv.owner_org             as rv_owner_org,
       rv.orig_owner_seq_id     as rv_orig_owner_seq_id, 
       rv.rf_object_tp_cd       as rv_object_tp_cd, 
       rv.rf_op_cat_cd          as rv_op_cat_cd,
       rv.rf_trxn_seq_id        as rv_trxn_seq_id,
       rv.rf_trxn_scrty_seq_id  as rv_trxn_scrty_seq_id,
       rv.rf_repeat_nb          as rv_repeat_nb,
       rv.rf_data_dump_dt       as rv_data_dump_dt,
       rv.rf_branch_id          as rv_branch_id,
       trunc(rv.rf_branch_id / 100000)              as rv_tb_id, 
       rv.rf_branch_id - round(rv.rf_branch_id, -5) as rv_osb_id, 
       oes_org.oes_321_org_id   as rv_oes_org_id,
       oes_org.tb_name          as rv_kgrko_nm,
       case when oes_org.oes_321_org_id is not null  
            then nvl(decode(oes_org.branch_fl, '0', nullif(oes_org.numbf_s, '0'), 
                                               '1', nullif(oes_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
       end as rv_kgrko_id,       
       rv.rf_kgrko_party_cd     as rv_kgrko_party_cd,
       rv.last_actvy_type_cd    as rv_last_actvy_type_cd,
       rv.prcsng_batch_cmplt_fl as rv_prcsng_batch_cmplt_fl,
       rv.rf_cls_dt             as rv_cls_dt,
       rv.cls_id                as rv_cls_id,       
       rv.cls_actvy_type_cd     as rv_cls_actvy_type_cd,
       rv.cls_class_cd          as rv_cls_class_cd,
       rv.review_type_cd        as rv_review_type_cd, 
       rv.rf_wrk_flag           as rv_wrk_flag,
       rv.rf_trxn_dt            as rv_trxn_dt,
       rv.rf_trxn_base_am       as rv_trxn_base_am,
       rv.rf_partition_key      as rv_partition_key,
       rv.rf_subpartition_date  as rv_subpartition_date,
       rv.rf_opok_nb            as opok_nb, 
       rv.rf_add_opoks_tx       as add_opoks_tx,
       trxn.*, 
       trxn_scrty.scrty_type_cd    as scrty_type_cd,
       trxn_scrty.scrty_id         as scrty_id,
       trxn_scrty.scrty_nb         as scrty_nb,
       trxn_scrty.scrty_base_am    as scrty_base_am,
       trxn_scrty.scrty_nominal    as scrty_nominal,
       trxn_scrty.nominal_crncy_cd as scrty_nominal_crncy_cd,
       trxn_scrty.scrty_price      as scrty_price,
       trxn_scrty.price_crncy_cd   as scrty_price_crncy_cd,
       trxn_scrty.scrty_seq_id     as scrty_seq_id,
       trxn_scrty.canceled_fl      as scrty_canceled_fl
  FROM mantas.kdd_review rv
       LEFT JOIN table(business.rfv_trxn(rv.rf_op_cat_cd, rv.rf_trxn_seq_id, rv.rf_data_dump_dt)) trxn ON rv.rf_object_tp_cd like 'TRXN%'
       LEFT JOIN business.rfv_trxn_scrty trxn_scrty ON trxn_scrty.op_cat_cd = rv.rf_op_cat_cd and 
                                                       trxn_scrty.trxn_scrty_seq_id = rv.rf_trxn_scrty_seq_id and
                                                       rv.rf_object_tp_cd in ('TRXN_SCRTY', 'TRXN_SCRTY_JOINT')
       LEFT JOIN mantas.rf_oes_321_org oes_org ON oes_org.oes_321_org_id = mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(rv.rf_branch_id/100000), rv.rf_branch_id - round(rv.rf_branch_id, -5), trxn.trxn_dt)
 WHERE rv.rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/;

GRANT SELECT on mantas.rfv_opok_review to KDD_ALGORITHM;
GRANT SELECT on mantas.rfv_opok_review to KDD_MINER;
GRANT SELECT on mantas.rfv_opok_review to MANTAS_LOADER;
GRANT SELECT on mantas.rfv_opok_review to MANTAS_READER;
GRANT SELECT on mantas.rfv_opok_review to RF_RSCHEMA_ROLE;
GRANT SELECT on mantas.rfv_opok_review to BUSINESS;
GRANT SELECT on mantas.rfv_opok_review to CMREVMAN with grant option;
GRANT SELECT on mantas.rfv_opok_review to KDD_MNR with grant option;
GRANT SELECT on mantas.rfv_opok_review to KDD_REPORT with grant option;
GRANT SELECT on mantas.rfv_opok_review to KYC;
GRANT SELECT on mantas.rfv_opok_review to STD;
GRANT SELECT on mantas.rfv_opok_review to KDD_ALG;

