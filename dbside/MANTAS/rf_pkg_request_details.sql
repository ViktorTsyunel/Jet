create or replace package mantas.rf_pkg_request_details is

  /* ������� ��������� ������� ��� �������� 
   *   TrxnDetails
   *   TrxnParty
   *   CustAddrList
   *   CustIdDocList
   *   CustPhonList
   *   CustEmailList
   *   CustCustList
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json;

end;
/
create or replace package body mantas.rf_pkg_request_details is

  /* ����� ��� ����������� ���������� alert_id, op_cat_cd, trxn_seq_id, party_cd.
   * ���� ��� ������� readByAlertId �� alertId ��������� trxn_id, op_cat_cd, rf_data_dump_dt.
   * ���� ��� ������� readByTrxnId �������� trxn_id, op_cat_cd ����� �� json, � alert_id, rf_data_dump_dt ������������� � null.
   * ��� ���� ����� �������� party_cd ����� �� json.
   *
   * ��������� ������
   *   p_json - ������ json
   *   p_alert_id - �������� review_id
   *   p_op_cat_cd - �������� op_cat_cd
   *   p_trxn_id - �������� trxn_seq_id
   *   p_party_cd - �������� party_cd
   *   p_data_dump_dt - �������� rf_data_dump_dt
   */
  procedure get_key_values( 
    p_json in rf_json, 
    p_alert_id out number, 
    p_op_cat_cd out varchar2, 
    p_trxn_id out number,
    p_party_cd out varchar2,
    p_data_dump_dt out date,
    p_cust_id out number )
  is
    v_action varchar2(64);
  begin
    v_action := rf_json_ext.get_string(p_json, 'action');
    p_party_cd := rf_json_ext.get_string(p_json, 'partyCd');
    
    /* ������������ ������ readByAlertId */
    if v_action = 'readByAlertId' then
      p_alert_id := rf_json_ext.get_number( p_json, 'alertId' );
      select rf_trxn_seq_id, rf_op_cat_cd, rf_data_dump_dt
        into p_trxn_id, p_op_cat_cd, p_data_dump_dt
        from mantas.kdd_review 
       where review_id = p_alert_id;
      p_cust_id := null; 

    /* ������������ ������ readByTrxnId */
    elsif v_action = 'readByTrxnId' then
      p_trxn_id := rf_json_ext.get_number( p_json, 'trxnId' );
      p_op_cat_cd := rf_json_ext.get_string( p_json, 'opCatCd' );
      p_alert_id := null;
      p_data_dump_dt := null;
      p_cust_id := null;
    
    /* ������������ ������ readByCustId */
    elsif v_action = 'readByCustId' then
      p_cust_id := rf_json_ext.get_number( p_json, 'custSeqId' );
      p_op_cat_cd := null;
      p_alert_id := null;
      p_data_dump_dt := null;
    end if;
    
  end;


  /* ����� ��� ����������� ��������� cust_seq_id
   *
   * ��������� ������
   *   p_trxn_id - �������� trxn_seq_id
   *   p_op_cat_cd - �������� op_cat_cd
   *   p_party_cd - �������� party_cd
   * ��������� ������
   *   number - �������� cust_seq_id
   */
  function get_cust_id( p_trxn_id in number, p_op_cat_cd in varchar2, p_party_cd in varchar2 ) return number
  is
    v_cust_id number;
  begin
    select cust_seq_id into v_cust_id
      from table( business.rfv_trxn_party( p_op_cat_cd, p_trxn_id ) )
     where party_cd = p_party_cd;

    return v_cust_id;
    
    exception
      when no_data_found then
        return null;
  end;


  /* ����� ���������� "������" json - json ��� ������ */
  function get_empty_json return rf_json
  is
    v_json rf_json;
  begin
    v_json := rf_json();
    v_json.put( 'success', true );
    v_json.put( 'totalCount', 0 );
    v_json.put( 'data', rf_json_list() );
    return v_json;
  end;


  /* ��������� ������� TrxnDetails. ������ �������������� ���� � ������ process_request.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_details( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000);
    v_alert_id number;
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_party_cd varchar2(32);
    v_data_dump_dt date;
    v_cust_id number;
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select to_char(trxn.trxn_dt, 'dd.mm.yyyy') as trxn_dt,
             trxn.trxn_doc_id as trxn_doc_id,
             to_char(trxn.trxn_doc_dt, 'dd.mm.yyyy') as trxn_doc_dt,
             case trxn.op_cat_cd
               when 'WT' then  '�����������'
               when 'CT' then  '��������'
               when 'BOT' then '����������'
             end as op_cat_cd,
             nvl(trxn.trxn_desc, optp.op_prim_tx) as trxn_desc,
             trxn.trxn_crncy_am as trxn_crncy_am,
             trxn.trxn_base_am as trxn_base_am,
             trxn.trxn_crncy_cd as trxn_crncy_cd,
             trxn.trxn_crncy_rate_am as trxn_crncy_rate_am,
             trxn.trxn_crncy_old_rate_fl as trxn_crncy_old_rate_fl,
             trxn.orig_nm as orig_nm,
             trxn.orig_inn_nb as orig_inn_nb,
             trxn.orig_kpp_nb as orig_kpp_nb,
             business.rf_pkg_util.format_account(trxn.orig_acct_nb) as orig_acct_nb,
             trxn.send_instn_nm as send_instn_nm,
             trxn.send_instn_cntry_cd as send_instn_cntry_cd,
             trxn.send_instn_id as send_instn_id,
             business.rf_pkg_util.format_account(trxn.send_instn_acct_id) as send_instn_acct_id,
             business.rf_pkg_util.format_account(trxn.debit_cd) as debit_cd,
             trxn.benef_nm as benef_nm,
             trxn.benef_inn_nb as benef_inn_nb,
             trxn.benef_kpp_nb as benef_kpp_nb,
             business.rf_pkg_util.format_account(trxn.benef_acct_nb) as benef_acct_nb,
             trxn.rcv_instn_nm as rcv_instn_nm,
             trxn.rcv_instn_cntry_cd as rcv_instn_cntry_cd,
             trxn.rcv_instn_id as rcv_instn_id,
             business.rf_pkg_util.format_account(trxn.rcv_instn_acct_id) as rcv_instn_acct_id,
             business.rf_pkg_util.format_account(trxn.credit_cd) as credit_cd,
             trim(optp.nsi_op_id || ' ' || optp.op_type_nm) as nsi_op_id,
             trxn.trxn_conv_crncy_cd as trxn_conv_crncy_cd,
             trxn.trxn_conv_am as trxn_conv_am,
             trxn.trxn_conv_base_am as trxn_conv_base_am,
             trxn.trxn_cash_symbols_tx as trxn_cash_symbols_tx,       
             trxn.bank_card_id_nb as bank_card_id_nb,
             to_char(trxn.bank_card_trxn_dt, 'dd.mm.yyyy hh24:mi:ss') as bank_card_trxn_dt,
             trxn.atm_id as atm_id,
             trxn.merchant_no as merchant_no,
             trxn.tb_id as tb_id,
             trxn.osb_id as osb_id,
             tb.org_nm as tb_nm,
             osb.org_nm as osb_nm,
             trxn.src_sys_cd as src_sys_cd,
             to_char(trxn.src_change_dt, 'dd.mm.yyyy hh24:mi:ss') as src_change_dt,
             trxn.src_user_nm as src_user_nm,
             trxn.src_cd as src_cd,
             trxn.canceled_fl as canceled_fl,
             mantas.rf_pkg_scnro.get_lexeme_list(:ALERTID, '��') as trxn_desc_lex,
             mantas.rf_pkg_scnro.get_lexeme_list(:ALERTID, '�') as orig_lex,
             mantas.rf_pkg_scnro.get_lexeme_list(:ALERTID, '�2') as benef_lex,
             
             trxn.send_kgrko_id as send_kgrko_id,
             trxn.send_kgrko_nm as send_kgrko_nm,
             trxn.rcv_kgrko_id as rcv_kgrko_id,
             trxn.rcv_kgrko_nm as rcv_kgrko_nm,
             oes_org.tb_name as rv_kgrko_nm,
             case 
               when oes_org.oes_321_org_id is not null then nvl(decode(oes_org.branch_fl, 
                                                                       '0', nullif(oes_org.numbf_s, '0'),
                                                                       '1', nullif(oes_org.numbf_ss, '0')), 
                                                                '-') -- ������� �������� �������� �� (��� ������ �������)
              end as rv_kgrko_id, 
            trim(trxn.benef_goz_op_cd ||' '|| nvl(goz_benef.code_val2_nm, goz_benef.code_desc_tx)) as benef_goz_op_cd, 
            trim(trxn.orig_goz_op_cd ||' '|| nvl(goz_orig.code_val2_nm, goz_orig.code_desc_tx)) as orig_goz_op_cd,    
            case 
              when nvl2(trxn.org_intrl_id, business.rf_pkg_util.get_trxn_org_desc(trxn.org_intrl_id,trxn.branch_id), null) is not null
              then business.rf_pkg_util.get_trxn_org_desc(trxn.org_intrl_id,trxn.branch_id)||nvl2(trxn.src_user_nm, ' ('||trxn.src_user_nm||')', '')
              else trxn.src_user_nm
            end as trxn_org_desc      
        from table(business.rfv_trxn(:OPCATCODE, :TRXNID, to_date(:DUMPDT, 'dd.mm.yyyy hh24:mi:ss'))) trxn
        left join business.rf_nsi_op_types optp on optp.nsi_op_id = trxn.nsi_op_id
        left join business.org tb on tb.org_intrl_id = to_char(trxn.tb_id)
        left join business.org osb on osb.org_intrl_id = to_char(trxn.tb_id) || '-' || to_char(trxn.osb_id)
        left join mantas.rf_oes_321_org oes_org on oes_org.oes_321_org_id = mantas.rf_pkg_kgrko.get_oes_321_org_id(
                                                                              trunc(trxn.branch_id/100000), 
                                                                              trxn.branch_id - round(trxn.branch_id, -5), 
                                                                              trxn.trxn_dt
                                                                            )
        left join business.ref_table_detail goz_orig ON goz_orig.code_set_id = 'RF_GOZ_OP_TYPE_DBT' and goz_orig.code_val1_nm = trxn.orig_goz_op_cd
        left join business.ref_table_detail goz_benef ON goz_benef.code_set_id = 'RF_GOZ_OP_TYPE_CDT' and goz_benef.code_val1_nm = trxn.benef_goz_op_cd                                                                    
    }';
    
    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    v_bind_json.put( 'ALERTID', v_alert_id );
    v_bind_json.put( 'OPCATCODE', v_op_cat_cd );
    v_bind_json.put( 'TRXNID', v_trxn_id );
    v_bind_json.put( 'DUMPDT', to_char(v_data_dump_dt, 'dd.mm.yyyy hh24:mi:ss') );
    
    return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
  end;
  
  
  /* ��������� ������� TrxnParty. ������ �������������� ���� � ������ process_request.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_party( p_json rf_json, p_id_request in number) return rf_json
  is
    v_sql varchar2(32000);
    v_alert_id number;
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_party_cd varchar2(32);
    v_data_dump_dt date;
    v_cust_id number;
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select -- �������� �� ���������
             decode(p.cust_type_cd, 'ORG', '����������� ����',
                                    'IND', '���������� ����',
                                    'IND_ORG', '�������������� ���������������',
                                    'FIN', '���������� �����������') as cust_type_cd,
             p.cust_nm as cust_nm,
             p.inn_nb as inn_nb,
             p.kpp_nb as kpp_nb,
             p.okved_nb as okved_nb,
             p.okpo_nb as okpo_nb,
             p.cust_fl_nm as cust_fl_nm,
             decode(p.resident_fl, 'Y', '��', 'N', '���') as resident_fl,
             decode(p.gender_cd, 'A', '���',
                                 'B', '���',
                                 'C', '�� ��������') as gender_cd,
             to_char(birth_dt, 'dd.mm.yyyy') as birth_dt,
             p.birthplace_tx as birthplace_tx,
             ctzn.cntry_nm as citizenship_cd,
             to_char(reg_dt, 'dd.mm.yyyy') as reg_dt,
             p.ogrn_nb as ogrn_nb,
             p.reg_org_nm as reg_org_nm,
             -- ����� �����������
             adr_cntry.cntry_nm as adr_country_cd,
             trim(p.adr_state_type_cd || ' ' || p.adr_state_nm) as adr_state_nm,
             trim(p.adr_district_type_cd || ' ' || p.adr_district_nm) as adr_district_nm,
             trim(p.adr_city_type_cd || ' ' || p.adr_city_nm) as adr_city_nm,
             trim(p.adr_settlm_type_cd || ' ' || p.adr_settlm_nm) as adr_settlm_nm,
             p.adr_unstruct_tx as adr_unstruct_tx,
             substr(adr_state.okato_cd, 1, 2) as adr_okato_cd,
             trim(p.adr_street_type_tx || ' ' || p.adr_street_name_tx) as adr_street_name_tx,
             p.adr_house_number_tx as adr_house_number_tx,
             p.adr_building_number_tx as adr_building_number_tx,
             p.adr_flat_number_tx as adr_flat_number_tx,
             -- ����� ����� ����������
             adh_cntry.cntry_nm as adh_country_cd,
             trim(p.adh_state_type_cd || ' ' || p.adh_state_nm) as adh_state_nm,
             trim(p.adh_district_type_cd || ' ' || p.adh_district_nm) as adh_district_nm,
             trim(p.adh_city_type_cd || ' ' || p.adh_city_nm) as adh_city_nm,
             trim(p.adh_settlm_type_cd || ' ' || p.adh_settlm_nm) as adh_settlm_nm,
             p.adh_unstruct_tx as adh_unstruct_tx,
             substr(adh_state.okato_cd, 1, 2) as adh_okato_cd,
             trim(p.adh_street_type_tx || ' ' || p.adh_street_name_tx) as adh_street_name_tx,
             p.adh_house_number_tx as adh_house_number_tx,
             p.adh_building_number_tx as adh_building_number_tx,
             p.adh_flat_number_tx as adh_flat_number_tx,
             -- �������������� ��������
             dt.doc_type_nm as doc_type_cd,
             p.document_series as document_series,
             p.document_issued_dpt_tx as document_issued_dpt_tx,
             p.document_issued_dpt_cd as document_issued_dpt_cd,
             p.document_no as document_no,
             to_char(p.document_issued_dt, 'dd.mm.yyyy') as document_issued_dt,
             to_char(p.document_end_dt, 'dd.mm.yyyy') as document_end_dt,
             -- ��������� ����
             p.cust_seq_id as cust_seq_id       
        from table(business.rfv_trxn_party(:OPCATCODE, :TRXNID, to_date(:DUMPDT, 'dd.mm.yyyy hh24:mi:ss'))) p 
        left join business.rf_country ctzn on ctzn.cntry_cd = p.citizenship_cd
        left join business.rf_country adr_cntry on adr_cntry.cntry_cd = p.adr_country_cd
        left join business.rf_country adh_cntry on adh_cntry.cntry_cd = p.adh_country_cd
        left join business.rf_addr_object adr_state on adr_state.addr_seq_id = p.adr_state_seq_id
        left join business.rf_addr_object adh_state on adh_state.addr_seq_id = p.adh_state_seq_id
        left join business.rf_cust_id_doc_type dt on dt.doc_type_cd = p.doc_type_cd
       where p.party_cd = :PARTYCD
    }';

    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    
    v_bind_json.put( 'OPCATCODE', v_op_cat_cd );
    v_bind_json.put( 'TRXNID', v_trxn_id );
    v_bind_json.put( 'PARTYCD', v_party_cd );
    v_bind_json.put( 'DUMPDT', to_char(v_data_dump_dt, 'dd.mm.yyyy hh24:mi:ss') );

    return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json );
  end;

  
  /* ��������� ������� CustAddrList. ������ �������������� ���� � ������ process_request.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_cust_addresses( p_json rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000);
    v_alert_id number;
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_party_cd varchar2(32);
    v_cust_id number;
    v_data_dump_dt date;
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select decode(a.rf_cust_seq_id, c.rf_ind_cust_seq_id, '��: ')||
             decode(a.addr_usage_cd, 'L', '����� �����������',
                                     'H', '����� ����������',
                                     'O', '������ �����',
                                     'B', '������� �����',
                                     'M', '�������� �����',
                                     '������ �����') as usage_cd,
             cntry.cntry_nm as country_cd,
             trim(a.rf_state_type_cd || ' ' || a.rf_state_nm) as state_nm,
             trim(a.rf_rgn_type_cd || ' ' || a.addr_rgn_nm) as district_nm,
             trim(a.rf_city_type_cd || ' ' || a.addr_city_nm) as city_nm,
             trim(a.rf_settlm_type_cd || ' ' || a.rf_settlm_nm) as settlm_nm,
             trim(a.addr_strt_line2_tx || ' ' || a.addr_strt_line1_tx) as street_name_tx,
             trim(a.addr_strt_line3_tx) as house_number_tx,
             a.addr_strt_line4_tx as building_number_tx,
             a.addr_strt_line5_tx as flat_number_tx,
             decode(a.rf_active_fl, 'Y', '�����������',
                                    'N', '�� �����������',
                                    '����������') as status_tx,
             to_char(a.rf_start_dt, 'dd.mm.yyyy') as start_dt,
             to_char(a.rf_end_dt, 'dd.mm.yyyy') as end_dt,
             a.src_sys_cd as src_sys_cd,
             to_char(a.rf_src_change_dt, 'dd.mm.yyyy hh24:mi:ss') as src_change_dt,
             a.rf_src_user_nm as src_user_nm,
             a.cust_addr_seq_id as cust_addr_seq_id,
             cntry.iso_numeric_cd iso_numeric_cd,
             substr(ao.okato_cd, 1, 2) okato_cd
        from business.cust c
        join business.cust_addr a on a.rf_cust_seq_id = c.cust_seq_id or a.rf_cust_seq_id = c.rf_ind_cust_seq_id
        left join business.rf_country cntry on cntry.cntry_cd = a.addr_cntry_cd
        left join business.rf_addr_object ao on ao.addr_seq_id = a.rf_state_addr_seq_id
       where c.cust_seq_id = :CUSTID    
       order by decode(a.rf_cust_seq_id, c.cust_seq_id, 1, 2),
             decode(a.rf_active_fl, 'Y', 1, null, 2, 3),
             decode(a.rf_primary_fl, 'Y', 1, 2),
             a.rf_start_dt desc nulls last,
             a.rf_end_dt desc nulls last,
             a.cust_addr_seq_id desc
    }';
    
    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    if v_cust_id is null then 
      v_cust_id := get_cust_id( v_trxn_id, v_op_cat_cd, v_party_cd );
    end if;  
    
    v_bind_json.put( 'CUSTID', v_cust_id );
    
    /* ���� �������� v_cust_id = null, �� �� ��������� �������� ������ � ��������� ������ json ����� */
    if v_cust_id is not null then
      return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
    else
      return get_empty_json;
    end if;  
  end;


  /* ��������� ������� CustIdDocList. ������ �������������� ���� � ������ process_request.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_cust_docs( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000);
    v_alert_id number;
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_party_cd varchar2(32);
    v_cust_id number;
    v_data_dump_dt date;
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select decode(d.cust_seq_id, c.rf_ind_cust_seq_id, '��: ')||dt.doc_type_nm as doc_type_cd,
             decode(d.valid_fl, 'Y', '�����������',
                                'N', '�� �����������',
                                     '����������') as status_tx,
             d.series_id as series_id,
             d.no_id as no_id,
             to_char(issue_dt, 'dd.mm.yyyy') as issue_dt,
             to_char(end_dt, 'dd.mm.yyyy') as end_dt,
             d.issued_by_tx as issued_by_tx,
             d.issue_dpt_cd as issue_dpt_cd,
             d.src_sys_cd as src_sys_cd,
             to_char(d.src_change_dt, 'dd.mm.yyyy hh24:mi:ss') as src_change_dt,
             d.src_user_nm as src_user_nm,
             d.cust_doc_seq_id as cust_doc_seq_id,
             dt.cbrf_cd cbrf_doc_type_cd
        from business.cust c
        join business.rf_cust_id_doc d on d.cust_seq_id = c.cust_seq_id or d.cust_seq_id = c.rf_ind_cust_seq_id
        left join business.rf_cust_id_doc_type dt on dt.doc_type_cd = d.doc_type_cd
       where c.cust_seq_id = :CUSTID   
       order by decode(d.cust_seq_id, c.cust_seq_id, 1, 2),
             decode(d.valid_fl, 'Y', 1, 2),
             decode(d.identity_prm_fl, 'Y', 1, null, 2, 3),
             d.issue_dt desc nulls last,
             d.start_dt desc nulls last,
             d.end_dt desc nulls last,
             d.cust_doc_seq_id desc    
    }';
    
    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    if v_cust_id is null then 
      v_cust_id := get_cust_id( v_trxn_id, v_op_cat_cd, v_party_cd );
    end if;  
    
    v_bind_json.put( 'CUSTID', v_cust_id );

    /* ���� �������� v_cust_id = null, �� �� ��������� �������� ������ � ��������� ������ json ����� */
    if v_cust_id is not null then
      return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
    else
      return get_empty_json;
    end if;  
  end;
  
  
  /* ��������� ������� CustPhonList. ������ �������������� ���� � ������ process_request.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_cust_phones( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000);
    v_alert_id number;
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_party_cd varchar2(32);
    v_cust_id number;
    v_data_dump_dt date;
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select decode(p.rf_cust_seq_id, c.rf_ind_cust_seq_id, '��: ') ||
             decode(p.phon_usage_cd, 'H', '��������',
                                     'B', '�������',
                                     'C', '���������',
                                     'O', '������',
                                          '������') as usage_cd,
             decode(p.rf_active_fl, 'Y', '�����������',
                                    'N', '�� �����������',
                                    '����������') as status_tx,
             p.phon_nb as phon_nb,
             to_char(p.rf_start_dt, 'dd.mm.yyyy') as start_dt,
             to_char(p.rf_end_dt, 'dd.mm.yyyy') as end_dt,
             p.rf_src_cd as src_cd,
             to_char(p.rf_src_change_dt, 'dd.mm.yyyy hh24:mi:ss') as src_change_dt,
             p.rf_src_user_nm as src_user_nm,
             p.cust_phon_seq_id as cust_phon_seq_id
        from business.cust c
        join business.cust_phon p on p.rf_cust_seq_id = c.cust_seq_id or p.rf_cust_seq_id = c.rf_ind_cust_seq_id
       where c.cust_seq_id = :CUSTID    
       order by decode(p.rf_cust_seq_id, c.cust_seq_id, 1, 2),
             decode(p.rf_active_fl, 'Y', 1, null, 2, 3),
             p.rf_start_dt desc nulls last,
             p.rf_end_dt desc nulls last,
             p.cust_phon_seq_id desc
    }';
    
    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    if v_cust_id is null then
      v_cust_id := get_cust_id( v_trxn_id, v_op_cat_cd, v_party_cd );
    end if;  
    
    v_bind_json.put( 'CUSTID', v_cust_id );

    /* ���� �������� v_cust_id = null, �� �� ��������� �������� ������ � ��������� ������ json ����� */
    if v_cust_id is not null then
      return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
    else
      return get_empty_json;
    end if;  
  end;


  /* ��������� ������� CustEmailList. ������ �������������� ���� � ������ process_request.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_cust_emails( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000);
    v_alert_id number;
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_party_cd varchar2(32);
    v_cust_id number;
    v_data_dump_dt date;
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select decode(e.rf_cust_seq_id, c.rf_ind_cust_seq_id, '��: ') ||
             decode(e.rf_active_fl, 'Y', '�����������',
                                    'N', '�� �����������',
                                    '����������') as status_tx,
             e.email_addr_tx as email_addr_tx,
             to_char(e.rf_start_dt, 'dd.mm.yyyy') as start_dt,
             to_char(e.rf_end_dt, 'dd.mm.yyyy') as end_dt,
             e.rf_src_cd as src_cd,
             to_char(e.rf_src_change_dt, 'dd.mm.yyyy hh24:mi:ss') as src_change_dt,
             e.rf_src_user_nm as src_user_nm,
             e.cust_email_seq_id as cust_email_seq_id
        from business.cust c
        join business.cust_email_addr e on e.rf_cust_seq_id = c.cust_seq_id or e.rf_cust_seq_id = c.rf_ind_cust_seq_id
       where c.cust_seq_id = :CUSTID
       order by decode(e.rf_cust_seq_id, c.cust_seq_id, 1, 2),
             decode(e.rf_active_fl, 'Y', 1, null, 2, 3),
             e.rf_start_dt desc nulls last,
             e.rf_end_dt desc nulls last,
             e.cust_email_seq_id desc
    }';
    
    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    if v_cust_id is null then
      v_cust_id := get_cust_id( v_trxn_id, v_op_cat_cd, v_party_cd );
    end if;  
    
    v_bind_json.put( 'CUSTID', v_cust_id );

    /* ���� �������� v_cust_id = null, �� �� ��������� �������� ������ � ��������� ������ json ����� */
    if v_cust_id is not null then
      return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
    else
      return get_empty_json;
    end if;  
  end;


  /* ��������� ������� CustCustList. ������ �������������� ���� � ������ process_request.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_cust_custs( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000);
    v_alert_id number;
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_party_cd varchar2(32);
    v_cust_id number;
    v_data_dump_dt date;
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select decode(cc.rf_cust_seq_id, c.rf_ind_cust_seq_id, '��: ')||rtp.rlshp_tp_nm as rlshp_tp_nm,  -- ��� �����
             cc.rf_rltd_cust_job_titl_nm as job_titl_nm,                -- ���������    
             nvl(rltd.full_nm, cc.rf_rltd_cust_nm) as full_nm,          -- ������������ ����
             cc.rf_rltd_cust_dt as birth_dt,                            -- ���� ��������/�����������
             cc.rf_rlshp_reason_tx as rlshp_reason_tx,                  -- ��������� ����� 
             decode(cc.rf_active_fl, 'Y', '�����������',
                                     'N', '�� �����������',
                                     '����������') as status_tx,       -- ������
             to_char(cc.rlshp_efctv_dt, 'dd.mm.yyyy') as start_dt,      -- ������ ��������
             to_char(cc.rlshp_exprn_dt, 'dd.mm.yyyy') as end_dt,        -- ��������� ��������
             endrs.code_desc_tx as end_reason_tx,                       -- ������� �����������
             cc.rf_src_cd as src_cd,                                    -- ������� ��������
             to_char(cc.rf_src_change_dt, 'dd.mm.yyyy hh24:mi:ss') as src_change_dt, -- ���� ���������� ���������
             cc.rf_src_user_nm as src_user_nm,                          -- ������������, ������� ���������
             cc.cust_cust_seq_id as cust_cust_seq_id,
             cc.rf_rltd_cust_seq_id as rltd_cust_seq_id
        from business.cust c
        join business.cust_cust cc on cc.rf_cust_seq_id = c.cust_seq_id or cc.rf_cust_seq_id = c.rf_ind_cust_seq_id
        left join business.cust rltd on rltd.cust_seq_id = cc.rf_rltd_cust_seq_id
        left join business.rf_cust_rlshp_type rtp on rtp.rlshp_tp_seq_id = cc.rf_rlshp_tp_seq_id
        left join business.ref_table_detail endrs on endrs.code_set_id = 'RF_CUST_RLSHP_ENDRSN' and endrs.code_val1_nm = cc.rf_end_reason_cd
       where c.cust_seq_id = :CUSTID
       order by decode(cc.rf_cust_seq_id, c.cust_seq_id, 1, 2),
             decode(cc.rf_active_fl, 'Y', 1, null, 2, 3),
             case 
               when rtp.src_cd like 'EKSP|CHIEF%' then 1
               when rtp.src_cd like 'EKSP|GA%' then 2
               else 3
             end,     
             nvl(rltd.full_nm, cc.rf_rltd_cust_nm),    
             cc.rlshp_efctv_dt desc nulls last,
             cc.rlshp_exprn_dt desc nulls last,
             cc.cust_cust_seq_id desc
    }';
    
    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    if v_cust_id is null then 
      v_cust_id := get_cust_id( v_trxn_id, v_op_cat_cd, v_party_cd );
    end if;  
    
    v_bind_json.put( 'CUSTID', v_cust_id );

    /* ���� �������� v_cust_id = null, �� �� ��������� �������� ������ � ��������� ������ json ����� */
    if v_cust_id is not null then
      return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
    else
      return get_empty_json;
    end if;  
  end;


  /* ����� ��� ��������� ������� SearchParty readById
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_cust_details( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql         varchar2(32000);
    v_cust_seq_id number;
    v_bind_json   rf_json := rf_json;
  begin
    v_sql := q'{
      select decode(orig.cust_type_cd, 
                    'ORG', '����������� ����',
                    'IND', '���������� ����',
                    'IND_ORG', '�������������� ���������������',
                    'FIN', '���������� �����������'
                   ) as cust_type_cd,
             orig.full_nm, 
             orig.tax_id,
             orig.rf_kpp_nb,
             business.rf_pkg_util.get_list_clsf_code(orig.cust_seq_id,'1') as okved_nb,
             orig.rf_okpo_nb,
             orig_fl.full_nm as cust_fl_nm,
             decode(orig.rf_resident_fl, 'Y', '��', 'N', '���') as resident_fl,
             decode(coalesce(orig_fl.cust_gndr_cd, orig.cust_gndr_cd), 
                    'A', '���',
                    'B', '���',
                    'C', '�� ��������'
                   ) as gender_cd,
             to_char(coalesce(orig_fl.birth_dt, orig.birth_dt), 'dd.mm.yyyy') as birth_dt,
             coalesce(orig_fl.rf_birthplace_tx, orig.rf_birthplace_tx) as birthplace_tx,
             ctzn.cntry_nm as citizenship_cd,
             to_char(orig.rf_reg_dt, 'dd.mm.yyyy') as reg_dt,
             orig.rf_ogrn_nb ogrn_nb,
             orig.rf_reg_org_nm reg_org_nm,
             -- ����� �����������
             adr_cntry.cntry_nm as adr_country_cd,
             trim(nvl(orig_addr_state.addr_type_cd, orig_addr.rf_state_type_cd) ||' '|| nvl(orig_addr_state.object_nm, orig_addr.rf_state_nm)) as adr_state_nm, 
             trim(orig_addr.rf_rgn_type_cd ||' ' || orig_addr.addr_rgn_nm) as adr_district_nm,
             trim(orig_addr.rf_city_type_cd ||' '|| orig_addr.addr_city_nm) as adr_city_nm,
             trim(orig_addr.rf_settlm_type_cd ||' '|| orig_addr.rf_settlm_nm) as adr_settlm_nm,
             orig_addr.rf_addr_tx adr_unstruct_tx,
             substr(adr_state.okato_cd, 1, 2) as adr_okato_cd,
             trim(orig_addr.addr_strt_line2_tx ||' '|| orig_addr.addr_strt_line1_tx) as adr_street_name_tx,
             orig_addr.addr_strt_line3_tx as adr_house_number_tx,
             orig_addr.addr_strt_line4_tx as adr_building_number_tx,
             orig_addr.addr_strt_line5_tx as adr_flat_number_tx,
             -- ����� ����� ����������
             adh_cntry.cntry_nm as adh_country_cd,
             trim(nvl(orig_addr_state_h.addr_type_cd, orig_addr_h.rf_state_type_cd) ||' '|| nvl(orig_addr_state_h.object_nm, orig_addr_h.rf_state_nm)) as adh_state_nm,
             trim(orig_addr_h.rf_rgn_type_cd ||' '|| orig_addr_h.addr_rgn_nm) as adh_district_nm,
             trim(orig_addr_h.rf_city_type_cd ||' '|| orig_addr_h.addr_city_nm) as adh_city_nm,
             trim(orig_addr_h.rf_settlm_type_cd ||' '|| orig_addr_h.rf_settlm_nm) as adh_settlm_nm,
             orig_addr_h.rf_addr_tx as adh_unstruct_tx,
             substr(adh_state.okato_cd, 1, 2) as adh_okato_cd,
             trim(orig_addr_h.addr_strt_line2_tx ||' '||orig_addr_h.addr_strt_line1_tx) as adh_street_name_tx,
             orig_addr_h.addr_strt_line3_tx as adh_house_number_tx,
             orig_addr_h.addr_strt_line4_tx as adh_building_number_tx,
             orig_addr_h.addr_strt_line5_tx as adh_flat_number_tx,
             -- �������������� ��������
             dt.doc_type_nm as doc_type_cd,
             orig_doc.series_id as document_series,
             orig_doc.issued_by_tx as document_issued_dpt_tx,
             orig_doc.issue_dpt_cd as document_issued_dpt_cd,
             orig_doc.no_id as document_no,
             to_char(orig_doc.issue_dt, 'dd.mm.yyyy') as document_issued_dt,
             to_char(orig_doc.end_dt, 'dd.mm.yyyy') as document_end_dt,
             -- ��������� ����
             orig.cust_seq_id
        from business.cust orig
        left join business.cust orig_fl on orig_fl.cust_seq_id = orig.rf_ind_cust_seq_id
        left join business.cust_addr orig_addr on orig_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(orig.cust_seq_id, 'L')
        left join business.rf_addr_object orig_addr_state on orig_addr_state.addr_seq_id = coalesce(orig_addr.rf_state_addr_seq_id,
                                                                                                    case 
                                                                                                      when orig_addr.rf_state_nm is not null then std.aml_tools.recognise_region2(orig_addr.rf_state_nm)
                                                                                                     end,
                                                                                                     case 
                                                                                                       when orig_addr.addr_city_nm is not null then std.aml_tools.recognise_region2(orig_addr.addr_city_nm)
                                                                                                     end
                                                                                                    )
        left join business.cust_addr orig_addr_h on orig_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(orig.cust_seq_id, 'H')
        left join business.rf_addr_object orig_addr_state_h on orig_addr_state_h.addr_seq_id = coalesce(orig_addr_h.rf_state_addr_seq_id,
                                                                                                        case 
                                                                                                          when orig_addr_h.rf_state_nm is not null then std.aml_tools.recognise_region2(orig_addr_h.rf_state_nm)
                                                                                                        end,
                                                                                                        case 
                                                                                                          when orig_addr_h.addr_city_nm is not null then std.aml_tools.recognise_region2(orig_addr_h.addr_city_nm)
                                                                                                        end
                                                                                                       )
        left join business.rf_cust_id_doc orig_doc on orig_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(orig.cust_seq_id)
        
        left join business.rf_country ctzn on ctzn.cntry_cd = orig.ctzshp_cntry1_cd
        left join business.rf_country adr_cntry on adr_cntry.cntry_cd = nvl(orig_addr_state.cntry_cd, orig_addr.addr_cntry_cd)
        left join business.rf_country adh_cntry on adh_cntry.cntry_cd = nvl(orig_addr_state_h.cntry_cd, orig_addr_h.addr_cntry_cd)
        left join business.rf_addr_object adr_state on adr_state.addr_seq_id = orig_addr_state.addr_seq_id
        left join business.rf_addr_object adh_state on adh_state.addr_seq_id = orig_addr_state_h.addr_seq_id
        left join business.rf_cust_id_doc_type dt on dt.doc_type_cd = orig_doc.doc_type_cd
      
      where orig.cust_seq_id = :CUST_SEQ_ID
    }';
    
    v_bind_json.put('CUST_SEQ_ID', rf_json_ext.get_number(p_json, 'cust_seq_id'));

    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;

  /* ����� ��� ��������� ������� CustAcctList readByAlertId readByTrxnId readByCustId
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
   
  function get_acct_details ( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql         varchar2(32000);
    v_cust_seq_id number;
    v_bind_json   rf_json := rf_json;
  begin
    
  v_sql := q'{select a.acct_intrl_id as acct_id
                     , a.alt_acct_id as acct_nb
                     , a.acct_rptng_crncy_cd as acct_corrency_cd
                     , case a.acct_stat_cd
                     when 'A' then '�����������'
                     when 'C' then '��������'
                     when 'I' then '����������'
                     when 'X' then '���������'
                     else null
                     end as acct_status_cd
                     , a.acct_open_dt as acct_open_dt
                     , a.acct_close_dt as acct_close_dt
                     , trim(regexp_substr(a.firm_acct_org_intrl_id, '^[0-9]*')||' 
                     '||tb.org_nm) as acct_tb_nm
                     , trim(regexp_substr(a.firm_acct_org_intrl_id, '[0-9]*$')||' 
                     '||osb.org_nm) as acct_osb_nm
                     , a.rf_first_actvy_dt as acct_first_dt
                     , a.rf_goz_first_dt as acct_goz_first_dt
                     , a.src_sys_cd as src_sys_cd
               from business.acct a
               left join business.org tb on tb.org_intrl_id = regexp_substr(a.firm_acct_org_intrl_id, '[0-9]*')
               left join business.org osb on osb.org_intrl_id = a.firm_acct_org_intrl_id
              where a.rf_prmry_cust_seq_id = :CUST_SEQ_ID
              order by acct_open_dt desc}';
  
    get_key_values( p_json, v_alert_id, v_op_cat_cd, v_trxn_id, v_party_cd, v_data_dump_dt, v_cust_id );
    
    if v_cust_id is null then 
      v_cust_id := get_cust_id( v_trxn_id, v_op_cat_cd, v_party_cd );
    end if;  
    
    v_bind_json.put( 'CUST_SEQ_ID', v_cust_id );

    /* ���� �������� v_cust_id = null, �� �� ��������� �������� ������ � ��������� ������ json ����� */
    if v_cust_id is not null then
      return rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
    else
      return get_empty_json;
    end if;  
  end;

  /* ������� ��������� ������� ��� �������� 
   *   TrxnDetails
   *   TrxnParty
   *   CustAddrList
   *   CustIdDocList
   *   CustPhonList
   *   CustEmailList
   *   CustCustList
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_request    clob := null;
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
  begin
    /* ������ ���� � ������ ��� "T" */
    v_request := regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2');
    
    v_json_in := rf_json(v_request);
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');

    if v_form_name = 'TrxnDetails'
      then v_json_out := get_details( v_json_in, p_id_request );
    elsif v_form_name = 'TrxnParty' 
      then v_json_out := get_party( v_json_in, p_id_request );
    elsif v_form_name = 'CustAddrList' 
      then v_json_out := get_cust_addresses( v_json_in, p_id_request );
    elsif v_form_name = 'CustIdDocList' 
      then v_json_out := get_cust_docs( v_json_in, p_id_request );      
    elsif v_form_name = 'CustPhonList' 
      then v_json_out := get_cust_phones( v_json_in, p_id_request );      
    elsif v_form_name = 'CustEmailList' 
      then v_json_out := get_cust_emails( v_json_in, p_id_request );      
    elsif v_form_name = 'CustCustList' 
      then v_json_out := get_cust_custs( v_json_in, p_id_request );        
    elsif v_form_name = 'CustDetails' and v_action = 'readByCustId' 
      then v_json_out := get_cust_details( v_json_in, p_id_request );
    elseif v_form_name = 'CustAcctList' and v_action in ( 'readByAlertId','readByTrxnId','readByCustId' )
      then v_json_out := get_acct_details( v_json_in, p_id_request);
    end if;
    
    return v_json_out;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        return rf_json( rf_pkg_request.exec_exception( 
          '�� ������� ��������� ������!'||chr(13)||'��������� ���������� ������.'||chr(13)||sqlerrm()
        ) );
  end;

end;
/
