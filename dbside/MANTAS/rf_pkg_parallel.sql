begin
  EXECUTE IMMEDIATE ('DROP TYPE mantas.RF_TAB_NUMBER_PAIR');
exception when others then null; end;
/

CREATE OR REPLACE TYPE mantas.RF_TYPE_NUMBER_PAIR AS OBJECT
(
  num_1  NUMBER,
  num_2  NUMBER
)
/

CREATE OR REPLACE TYPE mantas.RF_TAB_NUMBER_PAIR AS TABLE OF mantas.RF_TYPE_NUMBER_PAIR
/  

GRANT UPDATE on std.mark_data_list to MANTAS;
--
-- ����� �� ���������������� ���������, ��������������� "������" ����������������� �������� (����� dbms_parallel_execute)
--
CREATE OR REPLACE PACKAGE mantas.rf_pkg_parallel IS
--
-- ��������� ��������� ������ SQL/PLSQL ����������� (����� dbms_parallel_execute), 
-- ��� ���� ��������� �� ����� (chunks) ������ ��������/����������� ��������� �������
-- ������� ��������� ��������� ��� ������� ����������� (AUTHID CURRENT_USER) �� ������� - � ����������� ����� ������ ���� ����� �� dbms_scheduler, ��� �� ����
-- ������� ����������, ����� � ������������ MANTAS ���� ����� �� �� �������� (������ ��������, ��������� ������), ������� ����������� � ����������� SQL/PLSQL
--
FUNCTION exec_in_parallel
(
  par_sql               CLOB,                                                    -- SQL/PLSQL ��� ����������, ������ ��������� �� ��� ��������/����������� ����� ������ 
                                                                                 -- #PART_SUBPART# - ���������� ���� �� PARTITION (<��� ��������>), ���� SUBPARTITION (<��� �����������>)
                                                                                 -- ����� ����� ��������� �� ������ #PARTITION_NAME# �/��� #SUBPARTITION_NAME# - ���������� �� ����� ��������/�����������
  par_degree            INTEGER,                                                 -- ������� ������������
  par_task_name         VARCHAR2,                                                -- ��� ������ ��� dbms_parallel_execute
  par_table_owner       dba_tab_subpartitions.table_owner%TYPE,                  -- ����� �������, �������� ��������� �� chunks 
  par_table_name        dba_tab_subpartitions.table_name%TYPE,                   -- ������������ �������, �������� ��������� �� chunks
  par_partition_name    dba_tab_subpartitions.partition_name%TYPE  DEFAULT null, -- ������������ ��������, �������������� �������� chunks (� ������ �����������)
  par_chunk_type        VARCHAR2 DEFAULT 'AUTO',  -- ���� ������������� ���� chunk: P - ��������, SP - �����������, 'AUTO' - ���������� �������������, � ������������ � ���������� ���������� ��������� ������� 
  par_log_level         VARCHAR2 DEFAULT 'ERROR',                                -- ������� ������������ � ��� ��������� �� �������: WARN, ERROR, FATAL
  par_repair_sql        CLOB     DEFAULT null                                    -- SQL/PLSQL ��� ����������, � ������, ���� ��������/����������� �������� �������������� ��� ��� ��������� ��������� ������ 
                                                                                 -- ����� ��������� �� �� �� �������, ��� � par_sql
) 
RETURN INTEGER;  -- ���������� ������ (chunk), ������������� � �������� (���� ������ ������������ � ������� ��� std.log_items) 
--
-- ��������� ��������� ������ SQL/PLSQL ����������� (����� dbms_parallel_execute), 
-- ��� ���� ��������� �� ����� (chunks) ������ ��� ������, ���������� � ���� ���������� ����� ����� #PORTION_NUMBER#
-- (�������������� ��������� ���� ������� � ��������, ����� ����������������� ��� ��� ��� �� �������� � �������, 
--  �� ��������� ������ ��������� �� ����� ����� ������� ���� ora_hash(id, 63) + 1 = #PORTION_NUMBER#)
--
FUNCTION exec_in_parallel2
(
  par_sql               CLOB,                                                    -- SQL/PLSQL ��� ����������, ������ ��������� �� ����� ����� #PORTION_NUMBER# - ���������� �� ����� �� 1 �� par_degree ������������
  par_degree            INTEGER,                                                 -- ������� ������������
  par_task_name         VARCHAR2,                                                -- ��� ������ ��� dbms_parallel_execute
  par_portions_count    INTEGER,                                                 -- ���������� ������, �� ������� ������ ������
  par_log_level         VARCHAR2 DEFAULT 'ERROR',                                -- ������� ������������ � ��� ��������� �� �������: WARN, ERROR, FATAL
  par_repair_sql        CLOB     DEFAULT null                                    -- SQL/PLSQL ��� ����������, � ������, ���� ����� (chunk) �������� �������������� ��� ��� ��������� ��������� ������ 
                                                                                 -- ����� ��������� �� �� �� �������, ��� � par_sql
) 
RETURN INTEGER;  -- ���������� ������ (chunk), ������������� � �������� (���� ������ ������������ � ������� ��� std.log_items) 
--
-- �������� �������� (�������) ������� �������� ��� ��������� �������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_partitions
--
FUNCTION part_pos_tab
(
  par_table_owner         dba_tab_partitions.table_owner%TYPE,
  par_table_name          dba_tab_partitions.table_name%TYPE,
  par_partition_name      dba_tab_partitions.partition_name%TYPE DEFAULT null
) 
RETURN mantas.RF_TAB_NUMBER PIPELINED; 
--
-- �������� �������� (������� � ����� ���������) ������� �����������, ������� �������� ��� 
-- ��������� ��������� ������� �, ��������, ��������� ��������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_subpartitions
--
FUNCTION part_subpart_pos_tab
(
  par_table_owner         dba_tab_subpartitions.table_owner%TYPE,
  par_table_name          dba_tab_subpartitions.table_name%TYPE,
  par_partition_name      dba_tab_subpartitions.partition_name%TYPE DEFAULT null
) 
RETURN mantas.RF_TAB_NUMBER_PAIR PIPELINED;
--
-- �������� ������������ �������� �� ������ ��� ��������� �������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_partitions
--
FUNCTION get_part_name
(
  par_table_owner            dba_tab_partitions.table_owner%TYPE,
  par_table_name             dba_tab_partitions.table_name%TYPE,
  par_partition_position     dba_tab_partitions.partition_position%TYPE
) 
RETURN VARCHAR2;
--
-- �������� ������������ ����������� �� ������ ��� ��������� ������� �� �������� ��������� �������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_subpartitions
--
FUNCTION get_subpart_name
(
  par_table_owner            dba_tab_subpartitions.table_owner%TYPE,
  par_table_name             dba_tab_subpartitions.table_name%TYPE,
  par_partition_position     dba_tab_partitions.partition_position%TYPE,
  par_subpartition_position  dba_tab_subpartitions.subpartition_position%TYPE
) 
RETURN VARCHAR2;
end rf_pkg_parallel;
/

CREATE OR REPLACE PACKAGE BODY mantas.rf_pkg_parallel IS
--
-- ��������� ��������� ������ SQL/PLSQL ����������� (����� dbms_parallel_execute), 
-- ��� ���� ��������� �� ����� (chunks) ������ ��������/����������� ��������� �������
-- ������� ��������� ��������� ��� ������� ����������� (AUTHID CURRENT_USER) �� ������� - � ����������� ����� ������ ���� ����� �� dbms_scheduler, ��� �� ����
-- ������� ����������, ����� � ������������ MANTAS ���� ����� �� �� �������� (������ ��������, ��������� ������), ������� ����������� � ����������� SQL/PLSQL
--
FUNCTION exec_in_parallel
(
  par_sql               CLOB,                                                    -- SQL/PLSQL ��� ����������, ������ ��������� �� ��� ��������/����������� ����� ������ 
                                                                                 -- #PART_SUBPART# - ���������� ���� �� PARTITION (<��� ��������>), ���� SUBPARTITION (<��� �����������>)
                                                                                 -- ����� ����� ��������� �� ������ #PARTITION_NAME# �/��� #SUBPARTITION_NAME# - ���������� �� ����� ��������/�����������
  par_degree            INTEGER,                                                 -- ������� ������������
  par_task_name         VARCHAR2,                                                -- ��� ������ ��� dbms_parallel_execute
  par_table_owner       dba_tab_subpartitions.table_owner%TYPE,                  -- ����� �������, �������� ��������� �� chunks 
  par_table_name        dba_tab_subpartitions.table_name%TYPE,                   -- ������������ �������, �������� ��������� �� chunks
  par_partition_name    dba_tab_subpartitions.partition_name%TYPE  DEFAULT null, -- ������������ ��������, �������������� �������� chunks (� ������ �����������)
  par_chunk_type        VARCHAR2 DEFAULT 'AUTO',  -- ���� ������������� ���� chunk: P - ��������, SP - �����������, 'AUTO' - ���������� �������������, � ������������ � ���������� ���������� ��������� ������� 
  par_log_level         VARCHAR2 DEFAULT 'ERROR',                                -- ������� ������������ � ��� ��������� �� �������: WARN, ERROR, FATAL
  par_repair_sql        CLOB     DEFAULT null                                    -- SQL/PLSQL ��� ����������, � ������, ���� ��������/����������� �������� �������������� ��� ��� ��������� ��������� ������ 
                                                                                 -- ����� ��������� �� �� �� �������, ��� � par_sql
) 
RETURN INTEGER IS  -- ���������� ������ (chunk), ������������� � �������� (���� ������ ������������ � ������� ��� std.log_items) 

  var_chunk_cursor_p CLOB := 
q'{SELECT column_value as start_id, column_value as end_id
  FROM TABLE(mantas.rf_pkg_parallel.part_pos_tab('#PAR_TABLE_OWNER#', '#PAR_TABLE_NAME#', '#PAR_PARTITION_NAME#'))
ORDER BY 1}';
       
  var_chunk_cursor_sp CLOB := 
q'{SELECT num_1 as start_id, num_2 as end_id
  FROM TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab('#PAR_TABLE_OWNER#', '#PAR_TABLE_NAME#', '#PAR_PARTITION_NAME#')) 
ORDER BY 1, 2}';

  var_chunk_sql_p   CLOB :=
q'{DECLARE
  var_position      INTEGER;
  var_partname      VARCHAR2(255);
  var_sql           CLOB;  
BEGIN
  var_position := nvl(:start_id, :end_id);  
  var_partname := mantas.rf_pkg_parallel.get_part_name('#PAR_TABLE_OWNER#', '#PAR_TABLE_NAME#', var_position);

  var_sql := 
q'[
 #SQL#
]';

  var_sql := replace(var_sql, '#PART_SUBPART#', 'PARTITION ('||var_partname||')');
  var_sql := replace(var_sql, '#PARTITION_NAME#', var_partname);                                

  EXECUTE IMMEDIATE var_sql;
end;
}';

  var_chunk_sql_sp   CLOB :=
q'{DECLARE
  var_part_position     INTEGER;
  var_subpart_position  INTEGER;
  var_partname          VARCHAR2(255);
  var_subpartname       VARCHAR2(255);

  var_sql               CLOB;  
BEGIN
  var_part_position    := :start_id;
  var_subpart_position := :end_id;  
  var_partname := mantas.rf_pkg_parallel.get_part_name('#PAR_TABLE_OWNER#', '#PAR_TABLE_NAME#', var_part_position);
  var_subpartname := mantas.rf_pkg_parallel.get_subpart_name('#PAR_TABLE_OWNER#', '#PAR_TABLE_NAME#', var_part_position, var_subpart_position);
  
  var_sql := 
q'[
 #SQL#
]';

  var_sql := replace(var_sql, '#PART_SUBPART#', 'SUBPARTITION ('||var_subpartname||')');
  var_sql := replace(var_sql, '#PARTITION_NAME#', var_partname);
  var_sql := replace(var_sql, '#SUBPARTITION_NAME#', var_subpartname);

  EXECUTE IMMEDIATE var_sql;
end;
}';

  var_sp_flag    INTEGER;
  var_err_count  INTEGER;
  
  prcname        VARCHAR2(64 CHAR) := 'mantas.rf_pkg_parallel.exec_in_parallel';

  -- ���������� ���������, ����������� �������������� ��� �������������� ��������/�����������
  PROCEDURE repair IS
    var_sql  CLOB;
  BEGIN
    -- ���� ��� SQL/PLSQL ��� �������������� - ������ �� ������
    if par_repair_sql is null or length(par_repair_sql) = 0 Then
      return;
    end if;  
  
    if var_sp_flag = 1 Then
      -- ���� �� ������������, ���������� ���������������
      FOR r IN(SELECT mantas.rf_pkg_parallel.get_part_name(par_table_owner, par_table_name, sp.num_1) as partname, 
                      mantas.rf_pkg_parallel.get_subpart_name(par_table_owner, par_table_name, sp.num_1, sp.num_2) as subpartname,                       
                      c.status
                 FROM TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab(par_table_owner, par_table_name, par_partition_name)) sp
                      left join dba_parallel_execute_chunks c on c.task_owner = 'MANTAS' and
                                                                 c.task_name = par_task_name and
                                                                 c.start_id = sp.num_1 and
                                                                 c.end_id = sp.num_2
                WHERE nvl(c.status, '?') <> 'PROCESSED') LOOP                
        BEGIN
          std.pkg_log.info('��������� �������������� ��� �����������: '||r.subpartname||
                           ', �������: '||par_table_owner||'.'||par_table_name||', ������: '||par_task_name||
                           ', ������ ��������� ���� �����������: '||nvl(r.status, '<��� ������� - �� ������ chunk>'), prcname);  
        
          var_sql := par_repair_sql;
          var_sql := replace(var_sql, '#PART_SUBPART#', 'PARTITION ('||r.partname||')');
          var_sql := replace(var_sql, '#PARTITION_NAME#', r.partname);
          var_sql := replace(var_sql, '#SUBPARTITION_NAME#', r.subpartname);
          std.pkg_log.trace(to_char(substr(var_sql, 1, 2000)), prcname);
          
          EXECUTE IMMEDIATE var_sql;

          std.pkg_log.info('��������� ��������� �������������� ��� �����������: '||r.subpartname||
                           ', �������: '||par_table_owner||'.'||par_table_name||', ������: '||par_task_name, prcname);  
        EXCEPTION
          WHEN OTHERS THEN
            std.pkg_log.fatal('������ ��� ���������� �������������� ��� �����������: '||r.subpartname||
                              ', �������: '||par_table_owner||'.'||par_table_name||', ������: '||par_task_name||
                              '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);  
        END;  
      END LOOP;              
    else
      -- ���� �� ���������, ���������� ���������������
      FOR r IN(SELECT mantas.rf_pkg_parallel.get_part_name(par_table_owner, par_table_name, p.column_value) as partname, 
                      c.status
                 FROM TABLE(mantas.rf_pkg_parallel.part_pos_tab(par_table_owner, par_table_name, par_partition_name)) p
                      left join dba_parallel_execute_chunks c on c.task_owner = 'MANTAS' and
                                                                 c.task_name = par_task_name and
                                                                 c.start_id = p.column_value
                WHERE nvl(c.status, '?') <> 'PROCESSED') LOOP                
        BEGIN
          std.pkg_log.info('��������� �������������� ��� ��������: '||r.partname||
                           ', �������: '||par_table_owner||'.'||par_table_name||', ������: '||par_task_name||
                           ', ������ ��������� ���� ��������: '||nvl(r.status, '<��� ������� - �� ������ chunk>'), prcname);  
        
          var_sql := par_repair_sql;
          var_sql := replace(var_sql, '#PART_SUBPART#', 'PARTITION ('||r.partname||')');
          var_sql := replace(var_sql, '#PARTITION_NAME#', r.partname);
          std.pkg_log.trace(to_char(substr(var_sql, 1, 2000)), prcname);
          
          EXECUTE IMMEDIATE var_sql;

          std.pkg_log.info('��������� ��������� �������������� ��� ��������: '||r.partname||
                           ', �������: '||par_table_owner||'.'||par_table_name||', ������: '||par_task_name, prcname);  
        EXCEPTION
          WHEN OTHERS THEN
            std.pkg_log.fatal('������ ��� ���������� �������������� ��� ��������: '||r.partname||
                              ', �������: '||par_table_owner||'.'||par_table_name||', ������: '||par_task_name||
                              '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);  
        END;  
      END LOOP;              
    end if;    
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.fatal('������ ��� ���������� ��������������, �������: '||par_table_owner||'.'||par_table_name||
                        ', ������: '||par_task_name||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);  
  END repair; 
BEGIN
  if par_sql is null or length(par_sql) = 0 or
     par_degree is null or par_degree <= 0 or 
     par_task_name is null or par_table_owner is null or par_table_name is null or
     par_chunk_type is null or par_chunk_type not in ('P', 'SP', 'AUTO') or
     par_log_level is null or par_log_level not in ('TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL') Then
    raise_application_error(-20001, '�������� �������� ��������� '||prcname);
  end if;   
  --
  -- ��������� ��� ���������: �������� ��� �����������
  --
  if par_chunk_type = 'AUTO' Then
    SELECT count(*)
      INTO var_sp_flag
      FROM TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab(par_table_owner, par_table_name))
     WHERE rownum <= 1;
  else 
    var_sp_flag := case when par_chunk_type = 'SP' then 1 else 0 end;
  end if;  
             
  -- ������� task, ���� �� ������� � ����������� �������
  begin dbms_parallel_execute.drop_task(par_task_name); exception when others then null; end;

  -- ������� task
  dbms_parallel_execute.create_task(par_task_name);
  -- ������� chunk'�
  
  dbms_parallel_execute.create_chunks_by_sql(par_task_name, 
                                             replace(replace(replace(
                                               case when var_sp_flag = 1 
                                                    then var_chunk_cursor_sp
                                                    else var_chunk_cursor_p
                                               end,
                                               '#PAR_TABLE_OWNER#',    par_table_owner),
                                               '#PAR_TABLE_NAME#',     par_table_name),
                                               '#PAR_PARTITION_NAME#', par_partition_name), false);
  
  -- ��������� task
  dbms_parallel_execute.run_task(par_task_name, replace(replace(replace(replace(
                                                        case when var_sp_flag = 1 
                                                             then var_chunk_sql_sp
                                                             else var_chunk_sql_p
                                                        end, 
                                                        '#PAR_TABLE_OWNER#',    par_table_owner),
                                                        '#PAR_TABLE_NAME#',     par_table_name),
                                                        '#PAR_PARTITION_NAME#', par_partition_name),
                                                        '#SQL#',            par_sql),                                                         
                                 DBMS_SQL.NATIVE, parallel_level => par_degree);

  -- ���� ���� ������ - ���������� ��������� � ���
  var_err_count := std.pkg_log.log_parallel_task_errors('MANTAS', par_task_name, par_log_level, prcname);
  
  -- ���� ���� �������������� chunk'� - ��������� ��� ��� ��������������
  repair;
  
  -- ������� task
  dbms_parallel_execute.drop_task(par_task_name);
  
  return var_err_count;
EXCEPTION
  WHEN OTHERS THEN
    repair;
    RAISE;
END exec_in_parallel;  
--
-- ��������� ��������� ������ SQL/PLSQL ����������� (����� dbms_parallel_execute), 
-- ��� ���� ��������� �� ����� (chunks) ������ ��� ������, ���������� � ���� ���������� ����� ����� #PORTION_NUMBER#
-- (�������������� ��������� ���� ������� � ��������, ����� ����������������� ��� ��� ��� �� �������� � �������, 
--  �� ��������� ������ ��������� �� ����� ����� ������� ���� ora_hash(id, 63) + 1 = #PORTION_NUMBER#)
--
FUNCTION exec_in_parallel2
(
  par_sql               CLOB,                                        -- SQL/PLSQL ��� ����������, ������ ��������� �� ����� ����� #PORTION_NUMBER# - ���������� �� ����� �� 1 �� par_portions_count ������������
  par_degree            INTEGER,                                     -- ������� ������������
  par_task_name         VARCHAR2,                                    -- ��� ������ ��� dbms_parallel_execute
  par_portions_count    INTEGER,                                     -- ���������� ������, �� ������� ������ ������
  par_log_level         VARCHAR2 DEFAULT 'ERROR',                    -- ������� ������������ � ��� ��������� �� �������: WARN, ERROR, FATAL
  par_repair_sql        CLOB     DEFAULT null                        -- SQL/PLSQL ��� ����������, � ������, ���� ����� (chunk) �������� �������������� ��� ��� ��������� ��������� ������ 
) 
RETURN INTEGER IS  -- ���������� ������ (chunk), ������������� � �������� (���� ������ ������������ � ������� ��� std.log_items) 

  var_chunk_cursor CLOB := 
q'{SELECT level as start_id, level as end_id
  FROM dual
CONNECT BY LEVEL <= #PAR_PORTIONS_COUNT#  
ORDER BY 1}';
       
  var_chunk_sql    CLOB :=
q'{DECLARE
  var_portion_number  INTEGER;
  var_sql             CLOB;  
BEGIN
  var_portion_number := nvl(:start_id, :end_id);  

  var_sql := 
q'[
 #SQL#
]';

  var_sql := replace(var_sql, '#PORTION_NUMBER#', to_char(var_portion_number));

  EXECUTE IMMEDIATE var_sql;
end;
}';

  var_err_count  INTEGER;
  
  prcname        VARCHAR2(64 CHAR) := 'mantas.rf_pkg_parallel.exec_in_parallel';

  -- ���������� ���������, ����������� �������������� ��� �������������� ��������/�����������
  PROCEDURE repair IS
    var_sql  CLOB;
  BEGIN
    -- ���� ��� SQL/PLSQL ��� �������������� - ������ �� ������
    if par_repair_sql is null or length(par_repair_sql) = 0 Then
      return;
    end if;  
  
    -- ���� �� ������, ���������� ���������������      
    FOR r IN(SELECT p.portion_number, 
                    c.status
               FROM (select level as portion_number
                       from dual
                     connect by level <= par_portions_count) p
                    left join dba_parallel_execute_chunks c on c.task_owner = 'MANTAS' and
                                                               c.task_name = par_task_name and
                                                               c.start_id = p.portion_number
              WHERE nvl(c.status, '?') <> 'PROCESSED') LOOP                
      BEGIN
        std.pkg_log.info('��������� �������������� ��� ����� �����: '||to_char(r.portion_number)||', ������: '||par_task_name||
                         ', ������ ��������� ���� �����: '||nvl(r.status, '<��� ������� - �� ������ chunk>'), prcname);  
        
        var_sql := par_repair_sql;
        var_sql := replace(var_sql, '#PORTION_NUMBER#', to_char(r.portion_number));
        std.pkg_log.trace(to_char(substr(var_sql, 1, 2000)), prcname);
          
        EXECUTE IMMEDIATE var_sql;

        std.pkg_log.info('��������� ��������� �������������� ��� ����� �����: '||to_char(r.portion_number)||', ������: '||par_task_name, prcname);  
      EXCEPTION
        WHEN OTHERS THEN
          std.pkg_log.fatal('������ ��� ���������� �������������� ��� ����� �����: '||to_char(r.portion_number)||
                            ', ������: '||par_task_name||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);  
      END;  
    END LOOP;              
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.fatal('������ ��� ���������� ��������������, ������: '||par_task_name||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);  
  END repair; 
BEGIN
  if par_sql is null or length(par_sql) = 0 or
     par_degree is null or par_degree <= 0 or 
     par_task_name is null or
     par_portions_count is null or par_portions_count <= 0 or 
     par_log_level is null or par_log_level not in ('TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL') Then
    raise_application_error(-20001, '�������� �������� ��������� '||prcname);
  end if;   

  -- ������� task, ���� �� ������� � ����������� �������
  begin dbms_parallel_execute.drop_task(par_task_name); exception when others then null; end;

  -- ������� task
  dbms_parallel_execute.create_task(par_task_name);
  -- ������� chunk'�
  
  dbms_parallel_execute.create_chunks_by_sql(par_task_name, 
                                             replace(var_chunk_cursor, '#PAR_PORTIONS_COUNT#', to_char(par_portions_count)), false);
  
  -- ��������� task
  dbms_parallel_execute.run_task(par_task_name, replace(var_chunk_sql, '#SQL#', par_sql),                                                         
                                 DBMS_SQL.NATIVE, parallel_level => par_degree);

  -- ���� ���� ������ - ���������� ��������� � ���
  var_err_count := std.pkg_log.log_parallel_task_errors('MANTAS', par_task_name, par_log_level, prcname);
  
  -- ���� ���� �������������� chunk'� - ��������� ��� ��� ��������������
  repair;

  -- ������� task
  dbms_parallel_execute.drop_task(par_task_name);
  
  return var_err_count;
EXCEPTION
  WHEN OTHERS THEN
    repair;
    RAISE;
END exec_in_parallel2;  
--
-- �������� �������� (�������) ������� �������� ��� ��������� �������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_partitions
--
FUNCTION part_pos_tab
(
  par_table_owner         dba_tab_partitions.table_owner%TYPE,
  par_table_name          dba_tab_partitions.table_name%TYPE,
  par_partition_name      dba_tab_partitions.partition_name%TYPE DEFAULT null
) 
RETURN mantas.RF_TAB_NUMBER PIPELINED IS 
BEGIN
  FOR r in (SELECT partition_position
              FROM dba_tab_partitions 
             WHERE table_owner = par_table_owner and 
                   table_name = par_table_name and
                   partition_name = nvl(par_partition_name, partition_name)) LOOP
    PIPE ROW(r.partition_position);
  END LOOP;
END part_pos_tab;           
--
-- �������� �������� (������� � ����� ���������) ������� �����������, ������� �������� ��� 
-- ��������� ��������� ������� �, ��������, ��������� ��������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_subpartitions
--
FUNCTION part_subpart_pos_tab
(
  par_table_owner         dba_tab_subpartitions.table_owner%TYPE,
  par_table_name          dba_tab_subpartitions.table_name%TYPE,
  par_partition_name      dba_tab_subpartitions.partition_name%TYPE DEFAULT null
) 
RETURN mantas.RF_TAB_NUMBER_PAIR PIPELINED IS 
  var_res    mantas.RF_TYPE_NUMBER_PAIR := mantas.RF_TYPE_NUMBER_PAIR(null, null);
BEGIN
  FOR r in (SELECT partition_position, subpartition_position
              FROM dba_tab_partitions p
                   join dba_tab_subpartitions sp on sp.table_owner    = p.table_owner and 
                                                    sp.table_name     = p.table_name and
                                                    sp.partition_name = p.partition_name
             WHERE p.table_owner = par_table_owner and 
                   p.table_name = par_table_name and
                   p.partition_name = nvl(par_partition_name, p.partition_name)) LOOP

    var_res.num_1 := r.partition_position;
    var_res.num_2 := r.subpartition_position;
    PIPE ROW(var_res);
  END LOOP;
END part_subpart_pos_tab;           
--
-- �������� ������������ �������� �� ������ ��� ��������� �������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_partitions
--
FUNCTION get_part_name
(
  par_table_owner            dba_tab_partitions.table_owner%TYPE,
  par_table_name             dba_tab_partitions.table_name%TYPE,
  par_partition_position     dba_tab_partitions.partition_position%TYPE
) 
RETURN VARCHAR2 IS 
  var_res   VARCHAR2(64 CHAR);
BEGIN
  FOR r in (SELECT partition_name
              FROM dba_tab_partitions 
             WHERE table_owner = par_table_owner and 
                   table_name = par_table_name and
                   partition_position = par_partition_position) LOOP
    var_res := r.partition_name;
    EXIT;
  END LOOP;
  
  return var_res;
END get_part_name;           
--
-- �������� ������������ ����������� �� ������ ��� ��������� ������� �� �������� ��������� �������
-- ������� �����, ����� �� ��������� ���� ����� �� dba_tab_subpartitions
--
FUNCTION get_subpart_name
(
  par_table_owner            dba_tab_subpartitions.table_owner%TYPE,
  par_table_name             dba_tab_subpartitions.table_name%TYPE,
  par_partition_position     dba_tab_partitions.partition_position%TYPE,
  par_subpartition_position  dba_tab_subpartitions.subpartition_position%TYPE
) 
RETURN VARCHAR2 IS 
  var_res   VARCHAR2(64 CHAR);
BEGIN
  FOR r in (SELECT sp.subpartition_name
              FROM dba_tab_partitions p
                   join dba_tab_subpartitions sp on sp.table_owner    = p.table_owner and 
                                                    sp.table_name     = p.table_name and
                                                    sp.partition_name = p.partition_name
             WHERE p.table_owner = par_table_owner and 
                   p.table_name = par_table_name and
                   p.partition_position = par_partition_position and
                   sp.subpartition_position = par_subpartition_position) LOOP
    var_res := r.subpartition_name;
    EXIT;
  END LOOP;
  
  return var_res;
END get_subpart_name;           
end rf_pkg_parallel;
/

GRANT EXECUTE ON mantas.rf_pkg_parallel TO BUSINESS;
GRANT EXECUTE ON mantas.rf_pkg_parallel TO STD;
GRANT EXECUTE ON mantas.rf_pkg_parallel TO REKS000IBS;
