--set define off
--drop package mantas.RF_PKG_SCNRO;
--drop TYPE mantas.RF_TAB_VARCHAR;

--grant select any table to mantas;
--grant create any view to mantas;
--grant select any table to kdd_mnr;
--grant execute any procedure to kdd_mnr;
--grant select any table to business;
--grant execute any procedure to business;
--grant select any table to kdd_alg;
--grant execute any procedure to kdd_alg;
--grant select any table to kdd_algorithm;
--grant execute any procedure to kdd_algorithm;
--grant execute on business.rf_pkg_util to ofsconf, mantas;

create or replace TYPE mantas.RF_TAB_VARCHAR AS TABLE OF VARCHAR2(2000 CHAR)
/

drop TYPE mantas.RF_TAB_OPOK
/
create or replace TYPE mantas.RF_TYPE_OPOK AS OBJECT
(
  -- ������� �������� �������
  op_cat_cd           VARCHAR2(3 CHAR),    -- ��������� ��������: WT, CT, BOT (�����������, ��������, ����������)
  partition_date      DATE,                -- ���� ����������������� ������� ��������
  subpartition_key    INTEGER,             -- ���� �������������������� ������� ��������
  opok_status         INTEGER,             -- ������ ��������� ���� ��� ��������
  trxn_seq_id         NUMBER(22),          -- ������������� ��������
  trxn_scrty_seq_id   NUMBER(22),          -- ������������� ������ � ������ ������ � ��������
  opok_list           VARCHAR2(2000 CHAR), -- ������ � ������ ���� � ���������������� ������, ��� ������� ������� �������� (� �������: <��� ���� 1>~<ID �������>~<ID ������ ������� ����>~||<��������� � ��������� (�����)>|<��� ���� 2>~<ID �������>~<ID ������ ������� ����>~||<��������� � ��������� (�����)>|� �. �.)
  trxn_dt             DATE,                -- ���� ���������� ��������
  data_dump_dt        DATE,                -- ����� ������� �������� ������ - ���� �������� �������� (������� DATA_DUMP_DT � ��������������� ������� ��������)
  branch_id           INTEGER,             -- ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
  -- ���. ������� ������� ��������� ���������
  trxn_crncy_am       NUMBER(28,8),        -- ����� �������� � ������ ���������� 
  trxn_base_am        NUMBER(28,8),        -- ����� �������� � ������� ������ (�������� ����������)
  trxn_crncy_cd       VARCHAR2(3),         -- ��� ������ ��������
  trxn_desc           VARCHAR2(2000 CHAR), -- ���������� �������
  nsi_op_id           VARCHAR2(20 CHAR),   -- ��� ���� �������� ��� ���   
  scrty_base_am       NUMBER(28,8),        -- ����� ������ ������ � ������� ������ (�������� ����������), ��� ������������ ����� ��������
  scrty_type_cd       VARCHAR2(64 CHAR),   -- ��� ���� ������ ������
  scrty_id            VARCHAR2(64 CHAR),   -- ����� ������ ������        
  orig_nm             VARCHAR2(2000 CHAR), -- ������������ �����������
  orig_inn_nb         VARCHAR2(64 CHAR),   -- ��� �����������
  orig_acct_nb        VARCHAR2(64 CHAR),   -- ����� ����� �����������
  orig_own_fl         VARCHAR2(1 CHAR),    -- ���� Y/N - �������� �� ���������� ����������
  debit_cd            VARCHAR2(64 CHAR),   -- ���� ������ ��������������� �������� 
  send_instn_nm       VARCHAR2(2000 CHAR),   -- ������������ ����� �����������
  send_instn_id       VARCHAR2(64 CHAR),   -- ���/SWIFT ����� �����������
  send_instn_acct_id  VARCHAR2(64 CHAR),   -- ����. ���� ����� �����������
  benef_nm            VARCHAR2(2000 CHAR), -- ������������ ����������
  benef_inn_nb        VARCHAR2(64 CHAR),   -- ��� ����������
  benef_acct_nb       VARCHAR2(64 CHAR),   -- ����� ����� ����������
  benef_own_fl        VARCHAR2(1 CHAR),    -- ���� Y/N - �������� �� ���������� ����������
  credit_cd           VARCHAR2(64 CHAR),   -- ���� ������� ��������������� �������� 
  rcv_instn_nm        VARCHAR2(2000 CHAR),   -- ������������ ����� ����������
  rcv_instn_id        VARCHAR2(64 CHAR),   -- ���/SWIFT ����� ����������
  rcv_instn_acct_id   VARCHAR2(64 CHAR),   -- ����. ���� ����� ����������
  tb_id               INTEGER,             -- ����� ��
  osb_id              INTEGER,             -- ����� ���
  src_sys_cd          VARCHAR2(3 CHAR),    -- ��� �������-��������� ��������
  src_change_dt       DATE,                -- ���� ���������� ��������� �������� � ���������
  src_user_nm         VARCHAR2(255 CHAR),  -- ��� ������������, ��������� ����������� �������� � ���������
  src_cd              VARCHAR2(64 CHAR)    -- ��� �������� � �������-���������
)
/

create or replace TYPE mantas.RF_TAB_OPOK AS TABLE OF RF_TYPE_OPOK
/

drop TYPE mantas.RF_TAB_OPOK_ALERT
/
create or replace TYPE mantas.RF_TYPE_OPOK_ALERT AS OBJECT
(
  object_tp_cd        VARCHAR2(64 CHAR),   -- TRXN - ����� �� ��������; TRXN_SCRTY - ����� �� ������ ������ � ��������; TRXN_SCRTY_JOINT - ������������ ����� �� �������� � �� (������������) ������ ������ � ���
  op_cat_cd           VARCHAR2(3 CHAR),    -- ��������� ��������: WT, CT, BOT (�����������, ��������, ����������)
  trxn_seq_id         NUMBER(22),          -- ������������� ��������
  trxn_scrty_seq_id   NUMBER(22),          -- ������������� ������ � ������ ������ � ��������
  opok_list           VARCHAR2(2000 CHAR), -- ������ � ������ ���� � ���������������� ������, ��� ������� ������� �������� (� �������: <��� ���� 1>~<ID �������>~<ID ������ ������� ����>~<��������� � ��������� (�����)>~ID ������ ������, ��������� ��� ��� ���� 1|<��� ���� 2>~<ID �������>~<ID ������ ������� ����>~<��������� � ��������� (�����)>~ID ������ ������, ��������� ��� ��� ���� 2|� �. �.)
  trxn_dt             DATE,                -- ���� ���������� ��������
  data_dump_dt        DATE,                -- ����� ������� �������� ������ - ���� �������� �������� (������� DATA_DUMP_DT � ��������������� ������� ��������)
  branch_id           INTEGER              -- ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
)
/
create or replace TYPE mantas.RF_TAB_OPOK_ALERT AS TABLE OF RF_TYPE_OPOK_ALERT
/

-- �������������, ���������� ������� (��������� SQL-���������) � ������������ � ����������� � �������� ��������� ����
CREATE OR REPLACE FORCE VIEW mantas.rfv_opok_rule_sql as
SELECT rl.rule_seq_id,
       opd.record_seq_id,
       rl.rule_cd, 
       rl.vers_number,
       rl.crit_status_cd,
       rl.vers_status_cd,
       rl.act_dup_number,
       rl.test_dup_number,
       rl.err_fl,
       rl.test_err_fl,
       rl.opok_nb,
       rl.group_nb,
       rl.query_cd,
       rl.priority_nb,
       rl.op_cat_cd,
       opd.limit_am,
       nullif(greatest(nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')), nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy'))), to_date('01.01.0001', 'dd.mm.yyyy')) as start_dt, 
       nullif(least(nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')), nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))), to_date('31.12.9999', 'dd.mm.yyyy')) as end_dt,
       rl.trxn_am_col,
       rl.tb_sql_tx,
       /* ������� �� ���� ���������� �������� */
       to_clob('')||
       case when nullif(greatest(nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')), nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy'))), to_date('01.01.0001', 'dd.mm.yyyy')) is not null and 
                 nullif(least(nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')), nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))), to_date('31.12.9999', 'dd.mm.yyyy')) is not null 
            then 'trunc('||rl.trxn_dt_col||')'||' between to_date('''||
                 to_char(nullif(greatest(nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')), nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy'))), to_date('01.01.0001', 'dd.mm.yyyy')), 'dd.mm.yyyy')||
                 ''', ''dd.mm.yyyy'') and to_date('''||
                 to_char(nullif(least(nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')), nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))), to_date('31.12.9999', 'dd.mm.yyyy')), 'dd.mm.yyyy')||
                 ''', ''dd.mm.yyyy'')'
            when nullif(greatest(nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')), nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy'))), to_date('01.01.0001', 'dd.mm.yyyy')) is not null and 
                 nullif(least(nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')), nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))), to_date('31.12.9999', 'dd.mm.yyyy')) is null 
            then 'trunc('||rl.trxn_dt_col||')'||' >= to_date('''||
                 to_char(nullif(greatest(nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')), nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy'))), to_date('01.01.0001', 'dd.mm.yyyy')), 'dd.mm.yyyy')||
                 ''', ''dd.mm.yyyy'')'              
            when nullif(greatest(nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')), nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy'))), to_date('01.01.0001', 'dd.mm.yyyy')) is null and 
                 nullif(least(nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')), nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))), to_date('31.12.9999', 'dd.mm.yyyy')) is not null 
            then 'trunc('||rl.trxn_dt_col||')'||' <= to_date('''||
                 to_char(nullif(least(nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')), nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))), to_date('31.12.9999', 'dd.mm.yyyy')), 'dd.mm.yyyy')||
                 ''', ''dd.mm.yyyy'')'              
       end as trxn_dt_sql_tx, 
       rl.optp_sql_tx,
       rl.src_sql_tx,
       rl.dbt_cdt_sql_tx,
       rl.acct_sql_tx,
       rl.wlist_sql_tx,
       rl.lex_sql_tx,
       rl.lex2_sql_tx,
       rl.custom_sql_tx,
       rl.explanation_sql_tx,
       rl.note_tx
  FROM (WITH op_cat_v AS
         (select op_cat_cd, 
                 lower(op_cat_cd)||'.' as alias,
                 decode(op_cat_cd, 'WT', 'wt.trxn_exctn_dt',
                                   'CT', 'ct.trxn_exctn_dt',
                                   'BOT', 'bot.exctn_dt') as trxn_exctn_dt,
                 decode(op_cat_cd, 'WT', 'wt.rf_trxn_opok_am',
                                   'CT', 'ct.rf_trxn_opok_am',
                                   'BOT', 'bot.trxn_base_am') as trxn_am_col,
                 decode(op_cat_cd, 'WT', 'wt.rf_orig_acct_nb',
                                   'CT', 'ct.rf_acct_nb',
                                   'BOT', 'acct.alt_acct_id') as acct_nb,
                 decode(op_cat_cd, 'WT', 'wt.rf_benef_acct_nb') as acct2_nb,
                 decode(op_cat_cd, 'WT', 'wt.orig_to_benef_instr_tx',
                                   'CT', 'ct.desc_tx',
                                   'BOT', 'bot.dscr_tx') as trxn_desc_tx,
                 decode(op_cat_cd, 'WT', 'coalesce(wt.orig_nm, orig.full_nm)||nvl2(wt.rf_orig_add_info_tx, '' ''||wt.rf_orig_add_info_tx, null)', 
                                   'CT', 'coalesce(ct.rf_cust_nm, cust.full_nm)', 
                                   'BOT', 'cust.full_nm') as cust_nm, 
                 decode(op_cat_cd, 'WT', 'coalesce(wt.benef_nm, benef.full_nm)||nvl2(wt.rf_benef_add_info_tx, '' ''||wt.rf_benef_add_info_tx, null)') as cust2_nm,
                 decode(op_cat_cd, 'WT', 'coalesce(wt.rf_orig_inn_nb, orig.tax_id)', 
                                   'CT', 'coalesce(ct.rf_cust_inn_nb, cust.tax_id)', 
                                   'BOT', 'cust.tax_id') as cust_inn, 
                 decode(op_cat_cd, 'WT', 'coalesce(wt.rf_benef_inn_nb, benef.tax_id)') as cust2_inn 
            from (select case level when 1 then 'CT' when 2 then 'BOT' when 3 then 'WT' end as op_cat_cd
                    from dual
                  connect by level <= 3)),
        rf_opok_criteria_v AS
         (select op_crit.crit_cd,
                 st.status,
                 decode(st.status, 'ACT', op_crit.crit_tx, nvl(op_crit.test_crit_tx, op_crit.crit_tx)) as crit_tx,
                 decode(st.status, 'ACT', op_crit.subtle_crit_tx, nvl(op_crit.test_subtle_crit_tx, op_crit.subtle_crit_tx)) as subtle_crit_tx,
                 decode(st.status, 'ACT', op_crit.null_fl, nvl(op_crit.test_null_fl, op_crit.null_fl)) as null_fl
            from mantas.rf_opok_criteria op_crit,
                 (select case level when 1 then 'ACT' when 2 then 'TEST' end as status
                    from dual
                  connect by level <= 2) st)          
        SELECT rl.rule_seq_id,
               rl.rule_cd, 
               rl.vers_number,
               st.status as crit_status_cd,
               rl.vers_status_cd,
               case when rl.vers_status_cd = 'ACT' then row_number() over(partition by rl.rule_cd, rl.vers_status_cd, st.status order by rl.vers_number desc) end as act_dup_number,
               case when rl.vers_status_cd in ('ACT','TEST') then row_number() over(partition by rl.rule_cd, case when rl.vers_status_cd in ('ACT','TEST') then 'TEST' else rl.vers_status_cd end, st.status order by case when rl.vers_status_cd = 'TEST' then 1 else 2 end, rl.vers_number desc) end as test_dup_number,
                 rl.err_fl,
               rl.test_err_fl,
               rl.opok_nb,
               opk.group_nb,
               rl.query_cd,
               rl.priority_nb,
               rl.op_cat_cd,
               rl.start_dt, 
               rl.end_dt,
               opcat.trxn_exctn_dt as trxn_dt_col,
               opcat.trxn_am_col as trxn_am_col,
               to_clob('')||
               case when rl.tb_id <> 0 
                    then opcat.alias||'rf_branch_id between '||to_char(rl.tb_id*100000)||' and '||to_char(rl.tb_id*100000 + 99999) 
               end as tb_sql_tx,
               /* ������� �� ��� �������� ��� ��� */
               to_clob('')||
               case when trim(',' from trim(crit_optp.crit_tx)||','||trim(rl.optp_add_crit_tx)) is not null
                    --then ''','||trim(',' from trim(crit_optp.crit_tx)||','||trim(rl.optp_add_crit_tx))||','' like ''%,''||'||opcat.alias||'trxn_type1_cd||'',%''' 
                    then 'mantas.rf_pkg_scnro.check_in_list('||opcat.alias||'trxn_type1_cd, '''||trim(',' from trim(crit_optp.crit_tx)||','||trim(rl.optp_add_crit_tx))||''') = 1'
               end as optp_sql_tx, 
               /* ������� �� �������-�������� �������� */
               to_clob('')||
               case when replace(rl.src_crit_tx, ' ') is not null
                    --then ''','||replace(rl.src_crit_tx, ' ')||','' like ''%,''||'||opcat.alias||'src_sys_cd||'',%''' 
                    then 'mantas.rf_pkg_scnro.check_in_list('||opcat.alias||'src_sys_cd, '''||replace(rl.src_crit_tx, ' ')||''') = 1'
               end as src_sql_tx,
               /* ������� �� �������: ����������/�������� */
               to_clob('')||
               case when rl.op_cat_cd in ('CT', 'BOT') and rl.dbt_cdt_cd is not null
                    then opcat.alias||'dbt_cdt_cd = '''||rl.dbt_cdt_cd||'''' 
               end as dbt_cdt_sql_tx,     
               /* ������� �� ������ ������ �����������/���������� */
               /* ���������� ������� ����: 
                   ((���� ����������� ���� ��� ����� ���� ��� ���� ����������� ���������� ��� ���������/����������� ������� �1 ������������� ���� ����������� ��� ����� ��� ���� �� ������������ ����������� ��� �� ���������� �������) 
                    �
                    (���� ���������� ���� ��� ������ ���� ��� ���� ���������� ���������� ��� ���������/����������� ������� �2 ������������� ���� ���������� ��� ������ ��� ���� �� ������������ ���������� ��� �� ���������� �������)) 
                   ��� 
                   (������: �������� 1 ������������� ����������/������ � �������� 2 ������������� ����������/�����) */
               to_clob('')||
               case when (trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx)) is not null and
                          (','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                           ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                           ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                           ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'))
                         or
                         (rl.op_cat_cd = 'WT' and trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx)) is not null and
                          (','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                           ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                           ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                           ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'))                                      
                    then replace(replace(
                           '((1 = 1'||
                           -- � ����������
                           case when trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx)) is not null and
                                     (','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                                      ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                                      ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                                      ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%')
                                then ' and (1 <> 1'||
                                     -- ��� ���� ����������� ����
                                     case when nvl(rl.acct_null_fl, crit_ac1.null_fl) = 'Y' and
                                               ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' 
                                          then ' or '||opcat.acct_nb||' is null '
                                     end||       
                                     -- ��� ����� ���� (��� ��������/����������: ����� ��� ������ - � ����������� ��: ����������/��������)
                                     case when nvl(rl.acct_null_fl, crit_ac1.null_fl) = 'Y' and
                                               ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'
                                          then case when rl.op_cat_cd = 'WT' or (rl.op_cat_cd in ('CT', 'BOT') and nvl(rl.dbt_cdt_cd, 'D') = 'D')
                                                    then ' or '||opcat.alias||'rf_debit_cd is null '
                                               end||
                                               case when rl.op_cat_cd in ('CT', 'BOT') and nvl(rl.dbt_cdt_cd, 'C') = 'C'
                                                    then ' or '||opcat.alias||'rf_credit_cd is null '
                                               end       
                                     end||                                     
                                     -- ��� ���� ����������� ����������
                                     case when rl.op_cat_cd = 'WT' and
                                               ','||upper(replace(trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx)), ' '))||',' 
                                                    like '%,����������,%' and
                                               ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' 
                                          then ' or mantas.rf_pkg_scnro.is_foreign_acct(wt.send_instn_id, '||opcat.acct_nb||') = 1 '
                                     end||                                                   
                                     -- ��� ���� ����������� 
                                     case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.acct_nb||', 1, 5), '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''') = 1'||
                                               case when trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx)) is not null 
                                                    then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.acct_nb||', '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1'
                                               end||')'
                                     end||
                                     -- ��� ����� (��� ��������/����������: ����� ��� ������ - � ����������� ��: ����������/��������)
                                     case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then case when rl.op_cat_cd = 'WT' or (rl.op_cat_cd in ('CT', 'BOT') and nvl(rl.dbt_cdt_cd, 'D') = 'D')
                                                    then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.alias||'rf_debit_cd, 1, 5), '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''') = 1'||
                                                         case when trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx)) is not null 
                                                              then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.alias||'rf_debit_cd, '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1'
                                                         end||')'
                                               end||          
                                               case when rl.op_cat_cd in ('CT', 'BOT') and nvl(rl.dbt_cdt_cd, 'C') = 'C'
                                                    then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.alias||'rf_credit_cd, 1, 5), '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''') = 1'||
                                                         case when trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx)) is not null 
                                                              then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.alias||'rf_credit_cd, '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1'
                                                         end||')'
                                              end           
                                     end||       
                                     -- ��� ������������ �����������
                                     case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then ' or mantas.rf_pkg_scnro.check_acct_in_text('||opcat.cust_nm||', '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''', '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1'
                                     end||       
                                     -- ��� ���������� �������
                                     case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then ' or ('||opcat.trxn_desc_tx||' is not null and mantas.rf_pkg_scnro.check_acct_in_text('||opcat.trxn_desc_tx||', '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''', '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1)'
                                     end||')'      
                           end||
                           -- � ����������
                           case when rl.op_cat_cd = 'WT' and trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx)) is not null and
                                     (','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                                      ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                                      ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                                      ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%')
                                then ' and (1 <> 1'||
                                     -- ��� ���� ���������� ����
                                     case when nvl(rl.acct2_null_fl, crit_ac2.null_fl) = 'Y' and
                                               ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' 
                                          then ' or '||opcat.acct2_nb||' is null '
                                     end||       
                                     -- ��� ������ ����
                                     case when nvl(rl.acct2_null_fl, crit_ac2.null_fl) = 'Y' and
                                               ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'
                                          then ' or '||opcat.alias||'rf_credit_cd is null '
                                     end||       
                                     -- ��� ���� ���������� ����������
                                     case when rl.op_cat_cd = 'WT' and
                                               ','||upper(replace(trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx)), ' '))||',' 
                                                    like '%,����������,%' and
                                               ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' 
                                          then ' or mantas.rf_pkg_scnro.is_foreign_acct(wt.rcv_instn_id, '||opcat.acct2_nb||') = 1 '
                                     end||       
                                     -- ��� ���� ����������
                                     case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.acct2_nb||', 1, 5), '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''') = 1'||
                                               case when trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx)) is not null 
                                                    then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.acct2_nb||', '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1'
                                               end||')'
                                     end||
                                     -- ��� ������
                                     case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.alias||'rf_credit_cd, 1, 5), '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''') = 1'||
                                               case when trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx)) is not null 
                                                    then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.alias||'rf_credit_cd, '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1'
                                               end||')'
                                     end||       
                                     -- ��� ������������ ����������
                                     case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then ' or mantas.rf_pkg_scnro.check_acct_in_text('||opcat.cust2_nm||', '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''', '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1'
                                     end||       
                                     -- ��� ���������� �������
                                     case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                          then ' or ('||opcat.trxn_desc_tx||' is not null and mantas.rf_pkg_scnro.check_acct_in_text('||opcat.trxn_desc_tx||', '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''', '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1)'
                                     end||')'      
                           end||')'||  
                           -- �������� ��������
                           case when rl.acct_reverse_fl = 'Y' and rl.op_cat_cd = 'WT'
                                then ' or (1 = 1'||
                                     -- � ����������
                                     case when trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx)) is not null and
                                               (','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                                                ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                                                ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' or
                                                ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%')
                                          then ' and (1 <> 1'||
                                               -- ��� ���� ���������� ����
                                               case when nvl(rl.acct_null_fl, crit_ac1.null_fl) = 'Y' and
                                                         ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' 
                                                    then ' or '||opcat.acct2_nb||' is null '
                                               end||       
                                               -- ��� ������ ����
                                               case when nvl(rl.acct_null_fl, crit_ac1.null_fl) = 'Y' and
                                                         ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'
                                                    then ' or '||opcat.alias||'rf_credit_cd is null '
                                               end||       
                                               -- ��� ���� ���������� ����������
                                               case when rl.op_cat_cd = 'WT' and
                                                         ','||upper(replace(trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx)), ' '))||',' 
                                                              like '%,����������,%' and
                                                         ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%' 
                                                    then ' or mantas.rf_pkg_scnro.is_foreign_acct(wt.rcv_instn_id, '||opcat.acct2_nb||') = 1 '
                                               end||                                                   
                                               -- ��� ���� ����������
                                               case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.acct2_nb||', 1, 5), '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''') = 1'||
                                                         case when trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx)) is not null 
                                                              then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.acct2_nb||', '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1'
                                                         end||')'
                                               end||
                                               -- ��� ������
                                               case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.alias||'rf_credit_cd, 1, 5), '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''') = 1'||
                                                         case when trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx)) is not null 
                                                              then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.alias||'rf_credit_cd, '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1'
                                                         end||')'
                                               end||       
                                               -- ��� ������������ ����������
                                               case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or mantas.rf_pkg_scnro.check_acct_in_text('||opcat.cust2_nm||', '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''', '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1'
                                               end||       
                                               -- ��� ���������� �������
                                               case when ','||replace(nvl(rl.acct_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or ('||opcat.trxn_desc_tx||' is not null and mantas.rf_pkg_scnro.check_acct_in_text('||opcat.trxn_desc_tx||', '''||trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx))||''', '''||trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx))||''') = 1)'
                                               end||')'      
                                     end||
                                     -- � ����������
                                     case when rl.op_cat_cd = 'WT' and trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx)) is not null and
                                               (','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                                                ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                                                ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' or
                                                ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%')
                                          then ' and (1 <> 1'||
                                               -- ��� ���� ����������� ����
                                               case when nvl(rl.acct2_null_fl, crit_ac2.null_fl) = 'Y' and
                                                         ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' 
                                                    then ' or '||opcat.acct_nb||' is null '
                                               end||       
                                               -- ��� ����� ����
                                               case when nvl(rl.acct2_null_fl, crit_ac2.null_fl) = 'Y' and
                                                         ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'
                                                    then ' or '||opcat.alias||'rf_debit_cd is null '
                                               end||       
                                               -- ��� ���� ����������� ����������
                                               case when rl.op_cat_cd = 'WT' and
                                                         ','||upper(replace(trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx)), ' '))||',' 
                                                              like '%,����������,%' and
                                                         ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%' 
                                                    then ' or mantas.rf_pkg_scnro.is_foreign_acct(wt.send_instn_id, '||opcat.acct_nb||') = 1 '
                                               end||       
                                               -- ��� ���� �����������
                                               case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.acct_nb||', 1, 5), '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''') = 1'||
                                                         case when trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx)) is not null 
                                                              then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.acct_nb||', '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1'
                                                         end||')'
                                               end||
                                               -- ��� �����
                                               case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or (mantas.rf_pkg_scnro.check_acct_5(substr('||opcat.alias||'rf_debit_cd, 1, 5), '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''') = 1'||
                                                         case when trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx)) is not null 
                                                              then ' and mantas.rf_pkg_scnro.check_acct_additional('||opcat.alias||'rf_debit_cd, '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1'
                                                         end||')'
                                               end||       
                                               -- ��� ������������ �����������
                                               case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or mantas.rf_pkg_scnro.check_acct_in_text('||opcat.cust_nm||', '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''', '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1'
                                               end||       
                                               -- ��� ���������� �������
                                               case when ','||replace(nvl(rl.acct2_area_tx, '��'), ' ')||',' like '%,��,%'  
                                                    then ' or ('||opcat.trxn_desc_tx||' is not null and mantas.rf_pkg_scnro.check_acct_in_text('||opcat.trxn_desc_tx||', '''||trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx))||''', '''||trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx))||''') = 1)'
                                               end||')'      
                                     end||')'  
                           end||')', '1 = 1 and '), '1 <> 1 or ')  
               end as acct_sql_tx,
               /* ������� �� ��������������/�� �������������� �����������/���������� ������� �� */
               /* ���������� ������� ����: 
                   (��� ����������� �����������/�� ����������� ������� ��� �����������
                    �
                    ��� ���������� �����������/�� ����������� ������� ��� ����������) 
                   ��� 
                   (������: �������� ����������� ��� ���������� � �������� ���������� ��� �����������) */
               to_clob('')||
               case when rl.cust_watch_list_cd is not null or 
                         (rl.op_cat_cd = 'WT' and rl.cust2_watch_list_cd is not null)
                    then replace(
                           '((1 = 1'||
                           -- � ����������
                           case when rl.cust_watch_list_cd is not null and nvl(rl.cust_not_watch_list_fl, 'N') <> 'Y'
                                then ' and '||opcat.cust_inn||' is not null and mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust_watch_list_cd||''', '||opcat.cust_inn||', '||opcat.trxn_exctn_dt||') = 1'  
                                when rl.cust_watch_list_cd is not null and nvl(rl.cust_not_watch_list_fl, 'N') = 'Y'
                                then ' and ('||opcat.cust_inn||' is null or nvl(mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust_watch_list_cd||''', '||opcat.cust_inn||', '||opcat.trxn_exctn_dt||'), 0) <> 1)'                                    
                           end||
                           -- � ����������
                           case when rl.op_cat_cd = 'WT' and rl.cust2_watch_list_cd is not null and nvl(rl.cust2_not_watch_list_fl, 'N') <> 'Y'
                                then ' and '||opcat.cust2_inn||' is not null and mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust2_watch_list_cd||''', '||opcat.cust2_inn||', '||opcat.trxn_exctn_dt||') = 1'
                                when rl.op_cat_cd = 'WT' and rl.cust2_watch_list_cd is not null and nvl(rl.cust2_not_watch_list_fl, 'N') = 'Y'
                                then ' and ('||opcat.cust2_inn||' is null or nvl(mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust2_watch_list_cd||''', '||opcat.cust2_inn||', '||opcat.trxn_exctn_dt||'), 0) <> 1)'
                           end||')'||  
                           -- �������� ��������
                           case when rl.acct_reverse_fl = 'Y' and rl.op_cat_cd = 'WT'
                                then ' or (1 = 1'||
                                     -- � ����������
                                     case when rl.cust_watch_list_cd is not null and nvl(rl.cust_not_watch_list_fl, 'N') <> 'Y'
                                          then ' and '||opcat.cust2_inn||' is not null and mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust_watch_list_cd||''', '||opcat.cust2_inn||', '||opcat.trxn_exctn_dt||') = 1'  
                                          when rl.cust_watch_list_cd is not null and nvl(rl.cust_not_watch_list_fl, 'N') = 'Y'
                                          then ' and ('||opcat.cust2_inn||' is null or nvl(mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust_watch_list_cd||''', '||opcat.cust2_inn||', '||opcat.trxn_exctn_dt||'), 0) <> 1)'                                    
                                     end||
                                     -- � ����������
                                     case when rl.op_cat_cd = 'WT' and rl.cust2_watch_list_cd is not null and nvl(rl.cust2_not_watch_list_fl, 'N') <> 'Y'
                                          then ' and '||opcat.cust_inn||' is not null and mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust2_watch_list_cd||''', '||opcat.cust_inn||', '||opcat.trxn_exctn_dt||') = 1'
                                          when rl.op_cat_cd = 'WT' and rl.cust2_watch_list_cd is not null and nvl(rl.cust2_not_watch_list_fl, 'N') = 'Y'
                                          then ' and ('||opcat.cust_inn||' is null or nvl(mantas.rf_pkg_scnro.check_org_in_wlist('''||rl.cust2_watch_list_cd||''', '||opcat.cust_inn||', '||opcat.trxn_exctn_dt||'), 0) <> 1)'
                                     end||')' 
                           end||')', '1 = 1 and ')  
               end as wlist_sql_tx,  
               /* ������� �� ������� � ���������� �������/������������ �����������/������������ ���������� */      
               to_clob('')||
               case when (trim(crit_lex.crit_tx) is not null and 
                           (','||replace(rl.lexeme_area_tx, ' ')||',' like '%,��,%' or
                            ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�,%' or
                            (rl.op_cat_cd = 'WT' and ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�2,%'))) or
                         trim(rl.lexeme_add_purpose_tx) is not null or
                         trim(rl.lexeme_add_cust_tx) is not null or
                         (rl.op_cat_cd = 'WT' and trim(rl.lexeme_add_cust2_tx) is not null)
                    then replace(
                           '(1 <> 1'||
                           case when (','||replace(rl.lexeme_area_tx, ' ')||',' like '%,��,%' and trim(crit_lex.crit_tx) is not null) or
                                     trim(rl.lexeme_add_purpose_tx) is not null 
                                then ' or ('||opcat.trxn_desc_tx||' is not null and mantas.rf_pkg_scnro.check_lexeme('||opcat.trxn_desc_tx||', '''||replace(trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,��,%' then trim(crit_lex.crit_tx) end||','||trim(rl.lexeme_add_purpose_tx)), '''', '''''')||''') = 1)'     
                           end ||  
                           case when (','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�,%' and trim(crit_lex.crit_tx) is not null) or
                                     trim(rl.lexeme_add_cust_tx) is not null 
                                then ' or ('||opcat.cust_nm||' is not null and mantas.rf_pkg_scnro.check_lexeme('||opcat.cust_nm||', '''||replace(trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�,%' then trim(crit_lex.crit_tx) end||','||trim(rl.lexeme_add_cust_tx)), '''', '''''')||''') = 1)'
                           end ||  
                           case when rl.op_cat_cd = 'WT' and
                                     ((','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�2,%' and trim(crit_lex.crit_tx) is not null) or trim(rl.lexeme_add_cust2_tx) is not null) 
                                then ' or ('||opcat.cust2_nm||' is not null and mantas.rf_pkg_scnro.check_lexeme('||opcat.cust2_nm||', '''||replace(trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�2,%' then trim(crit_lex.crit_tx) end||','||trim(rl.lexeme_add_cust2_tx)), '''', '''''')||''') = 1)'
                           end 
                           ||')', '1 <> 1 or ')
               end as lex_sql_tx,
               /* ������� �� ������� 2 � ���������� �������/������������ �����������/������������ ���������� */      
               to_clob('')||
               case when (trim(crit_lex2.crit_tx) is not null and 
                           (','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,��,%' or
                            ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�,%' or
                            (rl.op_cat_cd = 'WT' and ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�2,%'))) or
                         trim(rl.lexeme2_add_purpose_tx) is not null or
                         trim(rl.lexeme2_add_cust_tx) is not null or
                         (rl.op_cat_cd = 'WT' and trim(rl.lexeme2_add_cust2_tx) is not null)
                    then replace(
                           '(1 <> 1'||
                           case when (','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,��,%' and trim(crit_lex2.crit_tx) is not null) or
                                     trim(rl.lexeme2_add_purpose_tx) is not null 
                                then ' or ('||opcat.trxn_desc_tx||' is not null and mantas.rf_pkg_scnro.check_lexeme('||opcat.trxn_desc_tx||', '''||replace(trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,��,%' then trim(crit_lex2.crit_tx) end||','||trim(rl.lexeme2_add_purpose_tx)), '''', '''''')||''') = 1)'     
                           end ||  
                           case when (','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�,%' and trim(crit_lex2.crit_tx) is not null) or
                                     trim(rl.lexeme2_add_cust_tx) is not null 
                                then ' or ('||opcat.cust_nm||' is not null and mantas.rf_pkg_scnro.check_lexeme('||opcat.cust_nm||', '''||replace(trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�,%' then trim(crit_lex2.crit_tx) end||','||trim(rl.lexeme2_add_cust_tx)), '''', '''''')||''') = 1)'
                           end ||  
                           case when rl.op_cat_cd = 'WT' and
                                     ((','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�2,%' and trim(crit_lex2.crit_tx) is not null) or trim(rl.lexeme2_add_cust2_tx) is not null) 
                                then ' or ('||opcat.cust2_nm||' is not null and mantas.rf_pkg_scnro.check_lexeme('||opcat.cust2_nm||', '''||replace(trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�2,%' then trim(crit_lex2.crit_tx) end||','||trim(rl.lexeme2_add_cust2_tx)), '''', '''''')||''') = 1)'
                           end 
                           ||')', '1 <> 1 or ')
               end as lex2_sql_tx,
               /* ������������� ������� (SQL) */ 
               to_clob('')||
               case when nvl(trim(rl.custom_crit_sql_tx), trim(crit_cust.crit_tx)) is not null
                    then '('||
                            replace(replace(replace(
                                    nvl(trim(rl.custom_crit_sql_tx), trim(crit_cust.crit_tx)),
                                    '#PARAM#', rl.custom_crit_param_tx),
                                    '#PARAM_CHAR#', ''''||rl.custom_crit_param_tx||''''),
                                    '#PARAM_CHARLIST#', (select listagg(''''||trim(column_value)||'''', ',') within group(order by column_value)
                                                           from table(mantas.rf_pkg_scnro.list_to_tab(rl.custom_crit_param_tx))))
                         ||')'
               end as custom_sql_tx, 
               to_clob('')||
               replace(replace(replace(
                       coalesce(trim(rl.explanation_sql_tx), trim(crit_expl.crit_tx), ''''''),
                       '#PARAM#', rl.custom_crit_param_tx),
                       '#PARAM_CHAR#', ''''||rl.custom_crit_param_tx||''''),
                       '#PARAM_CHARLIST#', (select listagg(''''||trim(column_value)||'''', ',') within group(order by column_value)
                                              from table(mantas.rf_pkg_scnro.list_to_tab(rl.custom_crit_param_tx)))) as explanation_sql_tx, 
               rl.note_tx
          FROM mantas.rf_opok_rule rl
               JOIN mantas.rf_opok opk ON opk.opok_nb = rl.opok_nb
               JOIN op_cat_v opcat ON opcat.op_cat_cd = rl.op_cat_cd
               CROSS JOIN (select case level when 1 then 'ACT' when 2 then 'TEST' end as status
                             from dual
                           connect by level <= 2) st 
               LEFT JOIN rf_opok_criteria_v crit_ac1  ON crit_ac1.crit_cd  = rl.acct_crit_cd and crit_ac1.status = st.status
               LEFT JOIN rf_opok_criteria_v crit_ac2  ON crit_ac2.crit_cd  = rl.acct2_crit_cd and crit_ac2.status = st.status
               LEFT JOIN rf_opok_criteria_v crit_lex  ON crit_lex.crit_cd  = rl.lexeme_crit_cd and crit_lex.status = st.status
               LEFT JOIN rf_opok_criteria_v crit_lex2 ON crit_lex2.crit_cd = rl.lexeme2_crit_cd and crit_lex2.status = st.status
               LEFT JOIN rf_opok_criteria_v crit_optp ON crit_optp.crit_cd = rl.optp_crit_cd and crit_optp.status = st.status
               LEFT JOIN rf_opok_criteria_v crit_cust ON crit_cust.crit_cd = rl.custom_crit_cd and crit_cust.status = st.status
               LEFT JOIN rf_opok_criteria_v crit_expl ON crit_expl.crit_cd = rl.explanation_crit_cd and crit_expl.status = st.status) rl
       LEFT JOIN mantas.rf_opok_details opd ON opd.opok_nb = rl.opok_nb and 
                                               opd.active_fl = 'Y' and
                                               nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')) <= nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')) and
                                               nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')) <= nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')) 
/

create or replace package mantas.rf_pkg_scnro IS
--
-- �������� ���������� � ������� ������� � ���
--
pck_start_date   DATE; -- ���� ������ �������
FUNCTION get_start_date RETURN DATE DETERMINISTIC PARALLEL_ENABLE;

pck_end_date     DATE; -- ���� ������ �������
FUNCTION get_end_date RETURN DATE DETERMINISTIC PARALLEL_ENABLE;

pck_tb_id        INTEGER; -- ��� �� (���� null, �� ���)
FUNCTION get_tb_id RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;

pck_op_cat_cd    VARCHAR2(3 CHAR); -- ��������� ��������: WT/CT/BOT
pck_trxn_seq_id  NUMBER(22);       -- ID ��������
FUNCTION get_op_cat_cd RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
FUNCTION get_trxn_seq_id RETURN NUMBER DETERMINISTIC PARALLEL_ENABLE;

pck_last_days_qty   INTEGER; -- ���������� ������������� ���� �� ������� ���� � �������, �� ��������� - 32 (����� � ���� ����)
FUNCTION get_last_days_qty RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
PROCEDURE set_last_days_qty(par_last_days_qty  INTEGER);
--
-- ������������� ������ (������ � �������� �������� ����� �����������) � �������
--
FUNCTION list_to_tab(par_string  VARCHAR2, par_delimiter VARCHAR2 DEFAULT ',')
RETURN RF_TAB_VARCHAR DETERMINISTIC PARALLEL_ENABLE;

-- ������ ��� CLOB
FUNCTION list_to_tab(par_string  CLOB, par_delimiter VARCHAR2 DEFAULT ',')
RETURN RF_TAB_VARCHAR DETERMINISTIC PARALLEL_ENABLE;
--
-- �������� � ������ ������� ������ ��������� ������
--
FUNCTION add_to_list_elements(par_string  VARCHAR2, par_add_string VARCHAR2, par_delimiter VARCHAR2 DEFAULT ',')
RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;

-- ������ ��� CLOB
FUNCTION add_to_list_elements(par_string  CLOB, par_add_string VARCHAR2, par_delimiter VARCHAR2 DEFAULT ',')
RETURN CLOB DETERMINISTIC PARALLEL_ENABLE;
--
-- ����������: �������� �� ��������� ���� ������ � ����������� �����
--  par_instn_id - ���/SWIFT �����
--  par_acct     - ����� �����
-- ������������ ��������:
--  1 - ���� � ����������� �����
--  0 - ���� � ���������� �����
--  NULL - �������� �������� ���������
--
FUNCTION is_foreign_acct(par_instn_id  VARCHAR2,  par_acct  VARCHAR2)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
--
-- ��������� ����� ����������� ����� (5 ����) �� �������������� � ������, ���������� ������� �������
-- ������� ������� �� ��������� �������, ������������� ����� �������, ��������: 401-408,!40817,!40820,474.
-- �������� ��������� �������:
--  1) �� ��������� - ����������� ����� ����� (�� 1 �� 5 ����), ��������, 408
--     ����������� ���� ��������� ��� �������, ���� ��� ����� ���������� � ��������� ����
--     (���� ������ ����� �� ����� ��� 5 ����, �� ���, ���������� ��������, ��� ������� ������� ���������� �� 5 ���� ������, � ������� - ���������)
--  2) �� ��������� � �������� - ����������� ��� ������ ����� ����� �����, ��������, 401-408, 40810-40813,
--     ���������� ���� � ������� ����� ���� �� 1 �� 5, �� ������ ���� ���������� � ������� ������ ���������
-- ����������� ���� ������ ������������� ���� �� ������ �� ������� ������� (��� ���������).
-- ���� ����������� ���� ������������� ���� �� ������ ������� � ���������� (����� �������� ������ ��������������� ���� - !),
-- �� �� �� ��������� ��� �������.
-- ������������ ��������:
--  1 - ����� ����� ������������� �������
--  0 - ����� ����� �� ������������� �������
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_5(par_acct  VARCHAR2, par_rule VARCHAR2)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
--
-- ������������� ��������� ����� ����� �� �������������� � ������, ���������� ��������� �������
-- ������� ������� ����������� ����� �������, ����� ������ ������� ������ ���� �� ����� 5 �������� (���� ��� - ����� ��������� ���������� %),
-- � ������� '%' - ����� ������������������ ��������, '_' - ����� ��������� ������,
-- ��������: 20309________01%,20309________02%,20310________01%,20310________02%
-- ������ ������� ������ �������������� ��� �������������� �������� ������� ������ ����� ������� check_acct_5,
-- � �������� �������� ������� ��������� ������ ��, ������� �������� �������, ����������� check_acct_5.
-- ���� ������� �� check_acct_5 �� ������� ���������, �� �������� ���������������� ������� � ��� ������� �������� �� �����.
-- ������:
--  - � check_acct_5 ����������� ������� '20309-20311,408'
--  - ���������� ��������� ������� - ����� ������ 20309 ����� ������ ��, ������� ������������� ��������:
---   20309________01%,20309________02%
--  � ������� ��������� ��� �������:
--   WHERE RF_PKG_SCNRO.check_acct_5(acctno, '20309-20311,408') = 1 and
--         RF_PKG_SCNRO.check_acct_additional(acctno, '20309________01%,20309________02%') = 1
--  ������� check_acct_additional ������ 1 ��� ������ 20310%, 20311%, 408%,
--  ������ ��� ��� ���� ������ �� ������� �� ������ ����������� �������,
--  � ��� ������ 20309% ������ 1 ������ ��� ������, ��������������� ���� �� ������ �� ��������� ���������� ��������
-- ������������ ��������:
--  1 - ����� ����� ������������� ���� �� ������ ������� ��� ��� ��������, ���������� ��� 5-������� ����
--  0 - ����� ����� �� ������������� �� ������ ������� (��� ���� ���� �������, ���������� ��� 5-������� ����)
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_additional(par_acct  VARCHAR2, par_templates VARCHAR2)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
--
-- ����� ����� ����� (������ ������) � ���������� ������ � ��������� �� �� �������������� ������, ���������� ������� �������
-- (��. check_acct_5) � ����������� ������� ������� (��. check_acct_additional)
-- ������������ ��������:
--  1 - � ������ ������ ���� �� ���� ����� �����, ������� ������������� ���������� ������� � ������� (��� �������� check_acct_5 = 1 � check_acct_additional = 1)
--  0 - ����� ������� ������ �� �������
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_in_text(par_text  VARCHAR2, par_rule VARCHAR2, par_templates VARCHAR2  DEFAULT null)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
--
-- ����� ����� ����� (������ ������) � ���������� ������ � ��������� �� �� ������������ ���������� �������
-- ������������ ��������:
--  1 - � ������ ������ ���� �� ���� ����� �����, ������� ������������� ���������� ������� (����� like ������)
--  0 - ����� ������� ������ �� �������
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_template_in_text(par_text  VARCHAR2, par_template  VARCHAR2)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
--
-- ��������� ����� �� ������������ ������� ������
-- ������� ������ �������� � ����: '�������&����,���,^�����,!�����,!�����&�����' - ����� ������������� ����� �������, ����:
--  - � ��� �� ����������� ������� "�����"
--  - � ��� �� ����������� ����� ��� ������� "�����" � "�����"
--  - ����������� ����� ��� ������� "�������" � "����" ��� ����������� ������� "���", ��� ���� ��� �� �������� ������ "������" ������� "�����"
-- � ����� ������ ������� ������� �� ������� ������/������� ������ � ��������������. � ������ ������� ������������� ����� ���� &.
-- ����������� ! �������� ��������� - ���� � ������ ����������� ������� � ����� ������������� ��� ����������� ��� ������� �� ������ ������ � ����� �������������, �� ����� �� ������������� �������,
-- ������ �������� ������� ������ �� �����������.
-- ����������� ^ �������� "������" ������� - �. �. �������, ������ ������� �������� "��������" �������, � �� ������� �� ������� �����������
-- "������" ������� ����������� "��-��������", �� ������ ���� ������� "������" ������
-- �������/������ ������ ��� ������������ - ���� � ������ ����������� ����� ������� ��� ����������� ��� ������� �� ������ ������ � ��� �� "������" �������, �� ����� ������������� �������
-- ������� ����� ���������� �/��� ������������� ����. �������� | - � ���� ������ � ������ ������ ����� ������������ �/��� ��������������� �� ��������� �������
-- �������: |����, ����|, |����| - ����� ����������, �������������, ��������� � "����"
--
-- ������������ ��������: 1 - ����� ������������� ������� ������, 0 - �� �������������, null - �������� �������� ���������
--
FUNCTION check_lexeme(par_text  VARCHAR2, par_rule  VARCHAR2)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
--
-- �������� ����� ������������ ������� �� ��� ���� (������� ���������� ��� ��� �����������!)
--
FUNCTION get_crit_tx
(  
  par_crit_cd        rf_opok_criteria.crit_cd%TYPE,                -- ��� ������������ �������
  par_crit_tp_cd     rf_opok_criteria.crit_tp_cd%TYPE DEFAULT null -- �������������� ��� ������������ ������� (���� �������� ��� ����� ���������� �� ���������� ������� ������ null)
)
RETURN rf_opok_criteria.crit_tx%TYPE PARALLEL_ENABLE;
--
-- ��������� ��������� ������� �� ������� ������ � ��������� �����
--
FUNCTION check_lexeme
(
  par_texts          RF_TAB_VARCHAR,               -- ��������� ������� (��������, �������) ��� ��������
  par_crit_cd        rf_opok_criteria.crit_cd%TYPE -- ��� ������� ������
)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE; -- 1 - ���� �� ���� ������� (�����) ������������� ���������� ������� ������
                                              -- 0 - �� ���� �� ��������� �� ������������� ���������� ������� ������
                                              -- null - ������
--
-- ��������� ������� (�����) �� ������������ �������, ����������� ������� ���������, 
-- ��� ���� ����� ��������� � ������ ����� ������ ��������������� ���� (!), ��� �������� ���������
-- �������: 1) ������� RUR ������������� �������: RUR, USD, EUR
--          2) ������� RUR �� ������������� �������: USD, EUR             
--          3) ������� RUR �� ������������� �������: !RUR             
--          4) ������� RUR ������������� �������: !USD             
-- ������������ ��������: 1 - ������� ������������� �������, 0 - �� �������������, null - �������� �������� ���������
--
FUNCTION check_in_list(par_element  VARCHAR2, par_rule  VARCHAR2, par_case_sensitive INTEGER DEFAULT 0)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
--
-- ��������� �� �� ��������� ��������� (���� ������ ���) �� �������������� ���������� �������
-- ������������ ��������: 
--   1 - �� ����������� ���������� �������, 0 - �� �����������, null - �������� �������� ���������
--
FUNCTION check_org_in_wlist(par_watch_list_cd  VARCHAR2, par_inn_nb  VARCHAR2, par_date DATE DEFAULT null)
RETURN INTEGER PARALLEL_ENABLE;
--
-- �������� �������� ���� (��������, ���������� ������������� ��������), � ������������ � ������������ ��������� ���������
--
FUNCTION find_opok (
  par_sdate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� �) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_edate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� ��) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_tb_id            INTEGER                       DEFAULT null,  -- ������ �� ����. ���� �������� (38 - ����������)
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ���������, ���� ������, �� ������ ��� ������� � �����������
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ����, ���� ������, �� ����������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ���� ������, �� ����������� ������ �������, ��������� � ���
  par_vers_status_cd   VARCHAR2                      DEFAULT null,  -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������
  par_crit_status_cd   VARCHAR2                      DEFAULT null   -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN mantas.RF_TAB_OPOK PIPELINED;
--
-- ������� ���������� ��������, ������� ��������� ��������� ��� ����, ����� ������� ���� � ������������ � ������������ ��������� ���������
-- ������������ ��� ������: ��������� ����� ����� ����������� ������� find_opok
--
FUNCTION estimate_find_opok (
  par_sdate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� �) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_edate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� ��) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_tb_id            INTEGER                       DEFAULT null,  -- ������ �� ����. ���� �������� (38 - ����������)
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ���������, ���� ������, �� ������ ��� ������� � �����������
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ����, ���� ������, �� ����������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ���� ������, �� ����������� ������ �������, ��������� � ���
  par_vers_status_cd   VARCHAR2                      DEFAULT null,  -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������
  par_crit_status_cd   VARCHAR2                      DEFAULT null   -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN INTEGER;
--
-- �������� �������� ������� � ������������ � ��������� ��������� ����
--
FUNCTION opok_alerts (
  par_mode             VARCHAR2                      DEFAULT 'NEW', -- 'NEW' - ��������� ��������� ��� �������� � �������� ��������, �� ������� ��� ��� �������; 'TRXN' - ��������� ��������� ��� ��������� ��������
  par_op_cat_cd        VARCHAR2                      DEFAULT null,  -- ������������ � ������ 'TRXN' - ��������� ��������: WT - �����������, CT - ��������, BOT - ����������
  par_trxn_seq_id      NUMBER                        DEFAULT null   -- ������������ � ������ 'TRXN' - ID �������� (PK ������ WIRE_TRXN/CASH_TRXN/BACK_OFFICE_TRXN)   
  )
RETURN mantas.RF_TAB_OPOK_ALERT PIPELINED;
--
-- ����������� �������������, ���������� ������ ��������� ����, ��� ���������� ������� ���������
--
FUNCTION rebuild_opok_view (
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ��� �������� ���������� ����������� view, ���� null - �� ���
  par_changed_only_fl  VARCHAR2                      DEFAULT 'Y'    -- ���� Y/N - ������� �� ����������� ������ �� view, ������� ������� ������������ (��� ������� ��������� ���������� ���� REBUILD_FL) ��� ���
  )
RETURN INTEGER; -- ���������� ������������ ��� ������������ ������ ��������� � �������� (���� �������� ������ "���� ������?")
--
-- ��������� ������� ��������� (������ ����� �� �������������� ������������), ����������/����� ����� ������� ������, ��������/�������� ��������� �� �������
-- ��������! ������� ��������� COMMIT!
--
FUNCTION check_and_mark_opok_rule (
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ���� ������, �� ����������� ������ �������, ��������� � ���
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ���������, ���� ������, �� ������ ��� ������� � �����������
  par_crit_cd          rf_opok_criteria.crit_cd%TYPE DEFAULT null,  -- ��� ������������ ������� ���������, ���� ������, �� ����������� ������ �������, ������������ ��� ������� (����������� �� ����)
  par_vers_status_cd   rf_opok_rule.vers_status_cd%TYPE DEFAULT 'ALL', -- ��� ������� ������ �������, ����������� � ��������: 'ACT' - ��������� ����������� ������ ������, � �. �. (��. ������� RF_OPOK_RULE.VERS_STATUS_CD), 'ALL' - ��������� ���
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ALL'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������, 'ALL' - �������� ������ � � ������������, � � ��������� ���������� ���������
  )
RETURN INTEGER; -- ���������� ������������ ������ ��������� � �������� (���� �������� ������ "���� ������?" ��� "���� ������ - ����?")
--
-- ��������� ������� ��������� (������ ����� �� �������������� ������������), ���� ������� �� ������ �������� - ������� �������� ��������� �� ������
--
FUNCTION check_opok_rule (
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE,  -- ������������� ������� ���������, ������� ���������� ���������
  par_crit_status_cd   VARCHAR2                        -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN VARCHAR2; -- ��������� �� ������, ���� ������� ������� ������ �������� - null
--
-- ������������ SELECT ���������� ���� (��� view ���������, ��� �������� ����������, ��� ��������� ���������) � ������������ � ��������� ��������� � ������� RF_OPOK_RULE
--
FUNCTION get_opok_select(
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'VIEW' - ��� view ���������, 'RUN' - ��� ���������� ���������, ��������� ������� (����� ��������), 'CHECK' - ��� �������� ���������� ������ ���������, 'FIND' - ��� ��������� ��������� ����
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ��������� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� ������ ��� ������� ���������� � SELECT (�������� ���������� ��� ��������� ���������)
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ���� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� � SELECT ���������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ��������� - ������������ ��� ������� 'VIEW'/'RUN', ���� ������ ��� ������� 'CHECK' � 'FIND', �� � SELECT ���������� ������ �������, ��������� � ���
  par_vers_status_cd   VARCHAR2                      DEFAULT 'ACT', -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ACT'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN CLOB;
--
-- ��������������� ������ ������� ��������� (� ������� RF_OPOK_RULE_SNAPSHOT)
-- �������, ��������������� ����������� ������� ������ ���������, ����������� � RF_OPOK_RULE_SNAPSHOT (������ ���� ��� ���������� �� ����������� ������, ��� ������������ ���)
-- ��������! ������� ��������� COMMIT!
--
FUNCTION shapshot_opok_rule (
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null   -- ��� ������� ���������, ���� ������, �� ��������� ������ ������ ������, ��������� � ���
  )
RETURN INTEGER; -- ���������� ��������� ������� (���� ��� ������ ��� ���� �����������, �� 0)
--
-- ������� ������ ����� ���� ��� ������ (���� ����, ���� ����� ���������, ���������� �� ���������)
-- ���������� �������, �� ������������
--
FUNCTION get_opok_list(
  par_review_id      mantas.kdd_review.review_id%TYPE,
  par_pimary_opok    VARCHAR2  DEFAULT NULL
  )
RETURN VARCHAR2;
--
-- ������� ������ ������ ��� ������, � ������������ � �������� ����������� ��������� ��������
-- ���� ����� ����� ��������������� ���������� �������� ��������� ���� �, ��������������, ���������� ������� ������, ������� ������ ������ ������������ � ��������� ����:
-- <������� ��������� ���� ���� ����� �������>|<������� ���. ���� 1 ���� ����� �������>|<������� ���. ���� 2 ���� ����� �������>|� �. �.
-- ���� � ����� ������� ������������ ����� ��� ������ ������ (��������� ����� ���������� �), �� ��� ����� ����������� |
--
FUNCTION get_lexeme_list(
  par_review_id      mantas.kdd_review.review_id%TYPE,
  par_area           VARCHAR2 -- �� - ������� ��� ���������� �������, � - ������� ������� ��������� (�����������), �2 - ������� ������� ���������
                              -- ����� ��������� ����� ����������� ����� �������, ��������: �, �2
  )
RETURN VARCHAR2;
--
-- ������� ������ ������ ��� ���������� ������� ���������
-- ���� � ����� ������� ������������ ����� ��� ������ ������ (��������� ����� ���������� �), �� ��� ����������� |
--
FUNCTION get_rule_lexeme_list(
  par_rule_seq_id    mantas.rf_opok_rule.rule_seq_id%TYPE,
  par_area           VARCHAR2, -- �� - ������� ��� ���������� �������, � - ������� ������� ��������� (�����������), �2 - ������� ������� ���������
                               -- ����� ��������� ����� ����������� ����� �������, ��������: �, �2
  par_crit_status_cd VARCHAR2  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN VARCHAR2;
--
-- �������� � ������ ������ ����. ������� ���, ����� ��������� ������� ������ ���� ��� ��������� �� ���������� ������������:
-- \% => % | \_ => _ | \\ => \ | % => , | _ => ,
--
FUNCTION refine_lexeme_list(par_string  VARCHAR2)
RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
--
-- ������� ����� ��������� �� ��������� ��� ���������� ������ ������� ���������
--
FUNCTION get_default_expl(
  par_snapshot_id      mantas.rf_opok_rule_snapshot.snapshot_id%TYPE
  )
RETURN VARCHAR2;
--
-- ������� ����� ��������� � ��������� ��� ������ 
--
FUNCTION get_explanation(
  par_review_id      mantas.kdd_review.review_id%TYPE
  )
RETURN VARCHAR2;
--
-- �������� ���
--
PROCEDURE clear_cache;
end RF_PKG_SCNRO;
/

CREATE OR REPLACE package body mantas.RF_PKG_SCNRO IS
--
-- ��� ��� ������� list_to_tab (�����-�� ������� ������ ������������� ������-�� ������������� �������� �������)
--
TYPE TAB_VARCHAR_CACHE IS TABLE OF mantas.RF_TAB_VARCHAR INDEX BY VARCHAR2(5000 BYTE);
pck_list_to_tab_cache  TAB_VARCHAR_CACHE;
--
-- ��� ����������� ������� ��������� (��� ������� check_lexeme � ������)
--
TYPE VARCHAR_CACHE IS TABLE OF VARCHAR2(5000 BYTE) INDEX BY VARCHAR2(64 CHAR);
pck_crit_cache       VARCHAR_CACHE;
pck_crit_cache_date  DATE := null; -- SYSDATE �� ������ ������ ������ ������ ������������ ������� � ��� (����� 1 ��� ��� "���������")
--
-- ��� ��� ������� opok_alerts � ������ TRXN
--
pck_opok_alerts_trxn_sql   CLOB; -- ����� ������������ �������
pck_opok_alerts_trxn_date  DATE; -- SYSDATE �� ������ ������ ������ ������� � ��� (����� 1 ��� ��� "���������")
--
-- ������� ������� � �������� ����������
--
FUNCTION get_start_date RETURN DATE DETERMINISTIC PARALLEL_ENABLE IS 
BEGIN 
  if pck_start_date is null Then
    pck_start_date := to_date(sys_context('CLIENTCONTEXT', 'AMLOPOK_START_DATE'), 'dd.mm.yyyy');
  end if;
  
  return pck_start_date; 
END;
  
FUNCTION get_end_date RETURN DATE DETERMINISTIC PARALLEL_ENABLE IS
BEGIN 
  if pck_end_date is null Then
    pck_end_date := to_date(sys_context('CLIENTCONTEXT', 'AMLOPOK_END_DATE'), 'dd.mm.yyyy');
  end if;
  
  return pck_end_date; 
END;

FUNCTION get_tb_id RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS
BEGIN 
  if pck_tb_id is null Then
    pck_tb_id := to_number(sys_context('CLIENTCONTEXT', 'AMLOPOK_TB_ID'));
  end if;
  
  return pck_tb_id; 
END;

FUNCTION get_op_cat_cd RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE IS
BEGIN 
  if pck_op_cat_cd is null Then
    pck_op_cat_cd := sys_context('CLIENTCONTEXT', 'AMLOPOK_OP_CAT_CD');
  end if;
  
  return pck_op_cat_cd; 
END;

FUNCTION get_trxn_seq_id RETURN NUMBER DETERMINISTIC PARALLEL_ENABLE IS
BEGIN 
  if pck_trxn_seq_id is null Then
    pck_trxn_seq_id := to_number(sys_context('CLIENTCONTEXT', 'AMLOPOK_TRXN_SEQ_ID'));
  end if;
  
  return pck_trxn_seq_id; 
END;


FUNCTION get_last_days_qty RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS
BEGIN 
  if pck_last_days_qty is null Then
    pck_last_days_qty := to_number(sys_context('CLIENTCONTEXT', 'AMLOPOK_LAST_DAYS_QTY'));
  end if;
  
  return pck_last_days_qty; 
END;

PROCEDURE set_last_days_qty(par_last_days_qty  INTEGER) IS 
BEGIN 
  pck_last_days_qty := par_last_days_qty; 
  dbms_session.set_context('CLIENTCONTEXT', 'AMLOPOK_LAST_DAYS_QTY', to_char(par_last_days_qty));  
END; 
--
-- ������������ SELECT ���������� ���� (��� view ���������, ��� �������� ����������, ��� ��������� ���������) ��� ��������� �������� ���������:
--  - ����������� ������ ��������� (��� ������� = '�����������')
--  - ������ ��������� ������ ����� (��� ������� = '������_������')
--
FUNCTION get_opok_select_standard(
  par_query_cd         rf_opok_query.query_cd%TYPE,                  -- ��� ������� ��������� - '�����������' ��� '������_������'
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'VIEW' - ��� view ���������, 'RUN' - ��� ���������� ���������, ��������� ������� (����� ��������), 'CHECK' - ��� �������� ���������� ������ ���������, 'FIND' - ��� ��������� ��������� ����
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ��������� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� ������ ��� ������� ���������� � SELECT (�������� ���������� ��� ��������� ���������)
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ���� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� � SELECT ���������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_vers_status_cd   VARCHAR2                      DEFAULT 'ACT', -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ACT'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN CLOB;
--
-- ������������ SELECT ���������� ���� (��� view ���������, ��� �������� ����������, ��� ��������� ���������) 
-- ��� ������� ��������� ������ �������� �� ����� (��� ������� = '������_��������')
--
FUNCTION get_opok_select_firstrxn(
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'VIEW' - ��� view ���������, 'RUN' - ��� ���������� ���������, ��������� ������� (����� ��������), 'CHECK' - ��� �������� ���������� ������ ���������, 'FIND' - ��� ��������� ��������� ����
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ��������� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� ������ ��� ������� ���������� � SELECT (�������� ���������� ��� ��������� ���������)
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ���� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� � SELECT ���������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_vers_status_cd   VARCHAR2                      DEFAULT 'ACT', -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������; �������� �� ����������� ���� ������ par_rule_seq_id
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ACT'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN CLOB; -- �������������� SELECT ���������� ����, ���� ��� ��������� ������� �� ��������� ������ ���������, �� null
--
-- ������������� ������ (������ � �������� �������� ����� �����������) � �������
--
FUNCTION list_to_tab(
  par_string  VARCHAR2,
  par_delimiter VARCHAR2 DEFAULT ','
  )
RETURN RF_TAB_VARCHAR DETERMINISTIC PARALLEL_ENABLE IS

  var_tab    RF_TAB_VARCHAR := RF_TAB_VARCHAR();
  var_spos   INTEGER := 1;
  var_epos   INTEGER;
  var_i      INTEGER := 1;
BEGIN
  if par_string is null or par_delimiter is null Then
    return null;
  end if;
  --
  -- ���� ����� ������ ��� ����������� � ����� ������������ (���� � ����) - ������� �������� �� ����
  --
  if pck_list_to_tab_cache.EXISTS(par_string||par_delimiter) Then
    return pck_list_to_tab_cache(par_string||par_delimiter);
  end if;

  LOOP
    var_epos := instr(par_string, par_delimiter, var_spos);
    var_tab.extend;

    if var_epos > 0 Then
      var_tab(var_i) := substr(par_string, var_spos, var_epos - var_spos);
      var_spos := var_epos + length(par_delimiter);
    else
      var_tab(var_i) := substr(par_string, var_spos);
    end if;

    EXIT WHEN var_epos <= 0;
    var_i := var_i + 1;
  END LOOP;
  --
  -- ��������� ��� � ���������� ���������
  --
  pck_list_to_tab_cache(par_string||par_delimiter) := var_tab;

  return  var_tab;
END list_to_tab;

-- ������� ��� CLOB - ��� �� ���, �� ��� ����������� (CLOB �� ����� ���� ������)
FUNCTION list_to_tab(par_string  CLOB, par_delimiter VARCHAR2 DEFAULT ',')
RETURN RF_TAB_VARCHAR DETERMINISTIC PARALLEL_ENABLE is

  var_tab    RF_TAB_VARCHAR := RF_TAB_VARCHAR();
  var_spos   INTEGER := 1;
  var_epos   INTEGER;
  var_i      INTEGER := 1;
BEGIN
  if par_string is null or par_delimiter is null Then
    return null;
  end if;

  LOOP
    var_epos := instr(par_string, par_delimiter, var_spos);
    var_tab.extend;

    if var_epos > 0 Then
      var_tab(var_i) := substr(par_string, var_spos, var_epos - var_spos);
      var_spos := var_epos + length(par_delimiter);
    else
      var_tab(var_i) := substr(par_string, var_spos);
    end if;

    EXIT WHEN var_epos <= 0;
    var_i := var_i + 1;
  END LOOP;

  return  var_tab;
END list_to_tab;  
--
-- �������� � ������ ������� ������ ��������� ������
--
FUNCTION add_to_list_elements(
  par_string     VARCHAR2, 
  par_add_string VARCHAR2, 
  par_delimiter  VARCHAR2 DEFAULT ','
  )
RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE IS
  var_result     VARCHAR2(2000 CHAR);
BEGIN
  if par_string is null or par_delimiter is null Then
    return null;
  end if;

  SELECT listagg(column_value||par_add_string, par_delimiter) within group(order by rownum)
    INTO var_result
    FROM table(rf_pkg_scnro.list_to_tab(par_string, par_delimiter));
  
  return var_result;
END add_to_list_elements;  

-- ������ ��� CLOB 
FUNCTION add_to_list_elements(
  par_string     CLOB, 
  par_add_string VARCHAR2, 
  par_delimiter  VARCHAR2 DEFAULT ','
  )
RETURN CLOB DETERMINISTIC PARALLEL_ENABLE IS
  var_result     CLOB;
BEGIN
  if par_string is null or par_delimiter is null Then
    return null;
  end if;

  SELECT replace(replace(replace(replace(replace(
           xmlagg(xmlelement("X", column_value||par_add_string)).getclobval(), 
         '</X><X>', par_delimiter), '<X>'), '</X>'), '&'||'lt;', '<'), '&'||'gt;', '>')
    INTO var_result
    FROM table(rf_pkg_scnro.list_to_tab(par_string, par_delimiter));
  
  return var_result;
END add_to_list_elements;  
--
-- ����������: �������� �� ��������� ���� ������ � ����������� �����
--  par_instn_id - ���/SWIFT �����
--  par_acct     - ����� �����
-- ������������ ��������:
--  1 - ���� � ����������� �����
--  0 - ���� � ���������� �����
--  NULL - �������� �������� ��������� ��� ���������� ����������
--
FUNCTION is_foreign_acct(par_instn_id  VARCHAR2,  par_acct  VARCHAR2)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS
  var_cntry_cd   VARCHAR2(2 CHAR);
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_instn_id) is null and trim(par_acct) is null Then
    return null;
  end if;
  --
  -- ��������� ���������� ������ �� ���/SWIFT �����
  --
  if trim(par_instn_id) is not null Then
    var_cntry_cd := case when regexp_like(trim(par_instn_id),'^04\d{7}$')  -- ��� ��
                         then 'RU'
                         when length(trim(par_instn_id)) in (8, 11) -- SWIFT
                         then substr(trim(par_instn_id), 5, 2)
                         when length(trim(par_instn_id)) = 9 and
                              translate(trim(par_instn_id), 'a1234567890', 'a') is null -- ��� ������ ������
                         then 'XX'     
                    end;
  end if;
  --
  -- ���� ���� �����������, �� ���� - ����������� (���� ���� ���������� ������ ������� 20-�������)
  --
  if var_cntry_cd = 'RU' Then
    return 0;
  elsif var_cntry_cd is not null Then
    return 1;
  end if;                        
  --
  -- ���� ����� ����� ������� �� 20 ��� ������ ����, �������, ��� ��� ���� � ���������� �����,
  -- ���� � ������ ����� ���� ���������� ������� (����� 6-�� ������� � 20-�������� - ���� 20309�...) - �������, 
  -- ��� ��� ���� � ����������� �����
  --
  if length(trim(par_acct)) >= 20 and
     translate(substr(trim(par_acct), 1, 5)||substr(trim(par_acct), 7), 'a1234567890', 'a') is null Then
    return 0;
  elsif trim(par_acct) is not null and
        translate(trim(par_acct), 'a1234567890', 'a') is not null Then
    return 1;  
  end if;
  --
  -- �������� ��������: ����� ����� ������� �� < 20 ���� � ���/SWIFT ��� ��� �� ����������
  --
  return null;  
EXCEPTION
  WHEN OTHERS THEN
    return null;
END is_foreign_acct;
--
-- ��������� ����� ����������� ����� (5 ����) �� �������������� � ������, ���������� ������� �������
-- ������� ������� �� ��������� �������, ������������� ����� �������, ��������: 401-408,!40817,!40820,474.
-- �������� ��������� �������:
--  1) �� ��������� - ����������� ����� ����� (�� 1 �� 5 ����), ��������, 408
--     ����������� ���� ��������� ��� �������, ���� ��� ����� ���������� � ��������� ����
--     (���� ������ ����� �� ����� ��� 5 ����, �� ���, ���������� ��������, ��� ������� ������� ���������� �� 5 ���� ������, � ������� - ���������)
--  2) �� ��������� � �������� - ����������� ��� ������ ����� ����� �����, ��������, 401-408, 40810-40813,
--     ���������� ���� � ������� ����� ���� �� 1 �� 5, �� ������ ���� ���������� � ������� ������ ���������
-- ����������� ���� ������ ������������� ���� �� ������ �� ������� ������� (��� ���������).
-- ���� ����������� ���� ������������� ���� �� ������ ������� � ���������� (����� �������� ������ ��������������� ���� - !),
-- �� �� �� ��������� ��� �������.
-- ������������ ��������:
--  1 - ����� ����� ������������� �������
--  0 - ����� ����� �� ������������� �������
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_5(
  par_acct  VARCHAR2,
  par_rule  VARCHAR2
  )
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS

  var_acct     INTEGER;
  var_result   INTEGER;
BEGIN
  --
  -- � ������� �� ������ ���� ����������� ��������, ������ �����, �����, ��������������� ����, �������, ������,
  -- ����� ������� �����: ����������
  --
  if translate(replace(upper(par_rule), '����������'), 'a1234567890-!, ', 'a') is not null Then
    return null;
  end if;
  --
  -- ����������� ����� ����� ������ ���� ������������� ������ �� ����� ��� �� 5 ����
  --
  if translate(trim(par_acct), 'a1234567890', 'a') is not null or length(trim(par_acct)) > 5 Then
    return null;
  end if;

  var_acct := to_number(rpad(trim(par_acct), 5, 0));

  SELECT /*+ NO_PARALLEL*/ 
         case when count(*) = 0 or sum(not_flag) > 0
              then 0
              else 1
         end
    INTO var_result
    FROM (SELECT case when substr(column_value, 1, 1) = '!' then 1 else 0 end as not_flag,
                 case when substr(column_value, 1, 1) = '!'
                      then case when instr(column_value, '-') > 0
                                then to_number(rpad(substr(column_value, 2, instr(column_value, '-') - 2), 5, 0))
                                else to_number(rpad(substr(column_value, 2), 5, 0))
                           end
                      else case when instr(column_value, '-') > 0
                                then to_number(rpad(substr(column_value, 1, instr(column_value, '-') - 1), 5, 0))
                                else to_number(rpad(substr(column_value, 1), 5, 0))
                           end
                 end as min_acct,
                 case when substr(column_value, 1, 1) = '!'
                      then case when instr(column_value, '-') > 0
                                then to_number(rpad(substr(column_value, instr(column_value, '-') + 1), 5, 9))
                                else to_number(rpad(substr(column_value, 2), 5, 9))
                           end
                      else case when instr(column_value, '-') > 0
                                then to_number(rpad(substr(column_value, instr(column_value, '-') + 1), 5, 9))
                                else to_number(rpad(substr(column_value, 1), 5, 9))
                           end
                 end as max_acct
            FROM TABLE(list_to_tab(replace(replace(upper(par_rule), ' '), '����������'))))
   WHERE var_acct between min_acct and max_acct;

  return var_result;
EXCEPTION
  WHEN OTHERS THEN
    return null;
END check_acct_5;
--
-- ������������� ��������� ����� ����� �� �������������� � ������, ���������� ��������� �������
-- ������� ������� ����������� ����� �������, ����� ������ ������� ������ ���� �� ����� 5 �������� (���� ��� - ����� ��������� ���������� %),
-- � ������� '%' - ����� ������������������ ��������, '_' - ����� ��������� ������,
-- ��������: 20309________01%,20309________02%,20310________01%,20310________02%
-- ������ ������� ������ �������������� ��� �������������� �������� ������� ������ ����� ������� check_acct_5,
-- � �������� �������� ������� ��������� ������ ��, ������� �������� �������, ����������� check_acct_5.
-- ���� ������� �� check_acct_5 �� ������� ���������, �� �������� ���������������� ������� � ��� ������� �������� �� �����.
-- ������:
--  - � check_acct_5 ����������� ������� '20309-20311,408'
--  - ���������� ��������� ������� - ����� ������ 20309 ����� ������ ��, ������� ������������� ��������:
---   20309________01%,20309________02%
--  � ������� ��������� ��� �������:
--   WHERE RF_PKG_SCNRO.check_acct_5(acctno, '20309-20311,408') = 1 and
--         RF_PKG_SCNRO.check_acct_additional(acctno, '20309________01%,20309________02%') = 1
--  ������� check_acct_additional ������ 1 ��� ������ 20310%, 20311%, 408%,
--  ������ ��� ��� ���� ������ �� ������� �� ������ ����������� �������,
--  � ��� ������ 20309% ������ 1 ������ ��� ������, ��������������� ���� �� ������ �� ��������� ���������� ��������
-- ������������ ��������:
--  1 - ����� ����� ������������� ���� �� ������ ������� ��� ��� ��������, ���������� ��� 5-������� ����
--  0 - ����� ����� �� ������������� �� ������ ������� (��� ���� ���� �������, ���������� ��� 5-������� ����)
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_additional(
  par_acct      VARCHAR2,
  par_templates VARCHAR2
  )
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS

  var_count   INTEGER;
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_acct) is null or trim(par_templates) is null Then
    return null;
  end if;
  --
  -- ���� ��� ���������������� 5-�������� ����� �� ������� �� ������ ������� - ����� ���������� 1
  --
  SELECT /*+ NO_PARALLEL*/ count(*)
    INTO var_count
    FROM (select distinct substr(trim(column_value), 1, 5) as acct_5
            from TABLE(list_to_tab(par_templates)))
   WHERE par_acct like acct_5||'%' and
         rownum <= 1;

  if var_count = 0 Then
    return 1;
  end if;
  --
  -- ���� ����� ����� ������������� ���� �� ������ ������� - ���������� 1, ����� - 0
  --
  SELECT /*+ NO_PARALLEL*/ count(*)
    INTO var_count
    FROM (select trim(column_value) as templ
            from TABLE(list_to_tab(par_templates)))
   WHERE par_acct like templ and
         rownum <= 1;

  return var_count;
EXCEPTION
  WHEN OTHERS THEN
    return null;
END check_acct_additional;
--
-- ����� ����� ����� (������ ������) � ���������� ������ � ��������� �� �� �������������� ������, ���������� ������� �������
-- (��. check_acct_5) � ����������� ������� ������� (��. check_acct_additional)
-- ������������ ��������:
--  1 - � ������ ������ ���� �� ���� ����� �����, ������� ������������� ���������� ������� � ������� (��� �������� check_acct_5 = 1 � check_acct_additional = 1)
--  0 - ����� ������� ������ �� �������
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_in_text(
  par_text      VARCHAR2,               -- �����, � ������� ���������� ����� � ��������� ������ ������
  par_rule      VARCHAR2,               -- ������ �������, ���������� ������� �� ������ ������ - ��. ������� check_acct_5
  par_templates VARCHAR2   DEFAULT null -- ���������� ������ ��� ������� ������ - ��. ������� check_acct_additional
  )
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS -- 1 ������ ����, ��������������� ��������, 0 - �� ������, null - �������� �������� ���������

  var_text     VARCHAR2(2000 CHAR);
  var_occur    INTEGER := 1;
  var_acct     VARCHAR2(2000 CHAR);
  var_result   INTEGER := 0;
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_text) is null or trim(par_rule) is null Then
    return null;
  end if;
  --
  -- � ������� �� ������ ���� ����������� ��������, ������ �����, �����, ��������������� ����, �������, ������,
  -- ����� ������� �����: ����������
  --
  if translate(replace(upper(par_rule), '����������'), 'a1234567890-!, ', 'a') is not null Then
    return null;
  end if;
  --
  -- ���� � ��������� ������ ������ � ������, �� �������� ������� ��� ������� � �������� �����.
  -- ����� �� ��������: �� ����� 20 ���� ������.
  -- ���� �� ������ ���������
  --
  var_text := translate(par_text, 'a '||CHR(13)||CHR(10)||CHR(9)||CHR(0), 'a');

  WHILE true LOOP
    var_acct := regexp_substr(var_text, '\d{20,}', 1, var_occur);
    if var_acct is null Then
      EXIT;
    end if;

    if check_acct_5(substr(var_acct, 1, 5), par_rule) = 1 and
       (trim(par_templates) is null or check_acct_additional(var_acct, par_templates) = 1) Then
      var_result := 1;
      EXIT;
    end if;

    var_occur := var_occur + 1;
  END LOOP;

  return var_result;
END check_acct_in_text;
--
-- ����� ����� ����� (������ ������) � ���������� ������ � ��������� �� �� ������������ ���������� �������
-- ������������ ��������:
--  1 - � ������ ������ ���� �� ���� ����� �����, ������� ������������� ���������� ������� (����� like ������)
--  0 - ����� ������� ������ �� �������
--  NULL - �������� �������� ���������
--
FUNCTION check_acct_template_in_text(
  par_text      VARCHAR2,               -- �����, � ������� ���������� ����� � ��������� ������ ������
  par_template  VARCHAR2                -- ������ ������ �����, � ������� ����� ����������� % - ����� ������������������ �������� � _ - ����� ��������� ������
  )
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS -- 1 ������ ����, ��������������� ��������, 0 - �� ������, null - �������� �������� ���������

  var_text     VARCHAR2(2000 CHAR);
  var_occur    INTEGER := 1;
  var_acct     VARCHAR2(2000 CHAR);
  var_result   INTEGER := 0;
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_text) is null or trim(par_template) is null Then
    return null;
  end if;
  --
  -- ���� � ��������� ������ ������ � ������, �� �������� ������� ��� ������� � �������� �����.
  --
  var_text := translate(par_text, 'a '||CHR(13)||CHR(10)||CHR(9)||CHR(0), 'a');
  --
  -- ���� ��������� ������ ��������, ����� ����. �������� % � _, ������ ����� - ���� 20+ - ������� ����� �����,
  -- ����� - ������ ��������� �� like
  --
  if translate(par_template, 'a1234567890%_ ', 'a') is null Then
    --
    -- ����� �� ��������: �� ����� 20 ���� ������.
    -- ���� �� ������ ���������
    --
    WHILE true LOOP
      var_acct := regexp_substr(var_text, '\d{20,}', 1, var_occur);
      if var_acct is null Then
        EXIT;
      end if;

      if var_acct like replace(par_template, ' ') Then
        var_result := 1;
        EXIT;
      end if;

      var_occur := var_occur + 1;
    END LOOP;
  else
    if lower(var_text) like '%'||lower(replace(par_template, ' '))||'%' Then
      var_result := 1;
    end if;    
  end if;
  
  return var_result;
END check_acct_template_in_text;
--
-- ��������� ����� �� ������������ ������� ������
-- ������� ������ �������� � ����: '�������&����,���,^�����,!�����,!�����&�����' - ����� ������������� ����� �������, ����:
--  - � ��� �� ����������� ������� "�����"
--  - � ��� �� ����������� ����� ��� ������� "�����" � "�����"
--  - ����������� ����� ��� ������� "�������" � "����" ��� ����������� ������� "���", ��� ���� ��� �� �������� ������ "������" ������� "�����"
-- � ����� ������ ������� ������� �� ������� ������/������� ������ � ��������������. � ������ ������� ������������� ����� ���� &.
-- ����������� ! �������� ��������� - ���� � ������ ����������� ������� � ����� ������������� ��� ����������� ��� ������� �� ������ ������ � ����� �������������, �� ����� �� ������������� �������,
-- ������ �������� ������� ������ �� �����������.
-- ����������� ^ �������� "������" ������� - �. �. �������, ������ ������� �������� "��������" �������, � �� ������� �� ������� �����������
-- "������" ������� ����������� "��-��������", �� ������ ���� ������� "������" ������
-- �������/������ ������ ��� ������������ - ���� � ������ ����������� ����� ������� ��� ����������� ��� ������� �� ������ ������ � ��� �� "������" �������, �� ����� ������������� �������
-- ������� ����� ���������� �/��� ������������� ����. �������� | - � ���� ������ � ������ ������ ����� ������������ �/��� ��������������� �� ��������� �������
-- �������: |����, ����|, |����| - ����� ����������, �������������, ��������� � "����"
--
-- ������������ ��������: 1 - ����� ������������� ������� ������, 0 - �� �������������, null - �������� �������� ���������
--
FUNCTION check_lexeme(
  par_text      VARCHAR2, -- �����, ������� ���������� ��������� �� ������������ ������� ������
  par_rule      VARCHAR2  -- ������� ������ - �������� ������/������� ������ ����� ������� � ��������������
  )
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS -- 1 - ����� ������������� ������� ������, 0 - �� �������������, null - �������� �������� ���������

  var_result          INTEGER := 0;          -- �� ��������� ����� �� ������������� �������

  var_text            VARCHAR2(2000 CHAR);                              -- ����������� ����� � ������ ��������
  var_false_lex       mantas.RF_TAB_VARCHAR := mantas.RF_TAB_VARCHAR(); -- ������ "�����������" "������" ������
  var_text_replace    VARCHAR2(2000 CHAR);                              -- ����������� ����� � ���������� "�������" ���������
  var_flag            INTEGER;
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_text) is null or trim(par_rule) is null Then
    return null;
  end if;
  --
  -- ���� �� ��������/������� ������ � �������, ������� "�����������", ����� - ���������, � ������� �������������:
  -- 1) ! - ������� ���������, ���� ����� ���� - ����� �� ������������� �������
  -- 2) ^ - "������" �������, ���� ����� ���� - ���������� ��� ������������ ������� "�����������" "�������������" ������
  -- 3) ��� ������������ ("�������������" �������) - ���������, ��� ������� �� "������"
  --
  var_text := lower(par_text);
  FOR r IN (SELECT /*+ NO_PARALLEL*/ 
                   modificator, 
                   -- ������ �� ������ � | (����� ����) �������, �� ����������� � ��������-��������
                   (select listagg(case when substr(column_value, 1, 1) = '|' or 
                                             substr(column_value, length(column_value)) = '|'
                                        then regexp_replace(column_value, '[^[:alnum:]]')
                                        else column_value
                                   end, '&') within group (order by rownum)
                      from table(mantas.rf_pkg_scnro.list_to_tab(lexem, '&'))) as lexem, 
                   lex_exists_flag, 
                   only_neg_rule_flag
              FROM (SELECT modificator, lexem,
                           (select decode(sum(case when -- |������� - ����� ���������� � �������
                                                        substr(column_value, 1, 1) = '|' and
                                                        substr(column_value, length(column_value)) <> '|' 
                                                   then case when regexp_like(var_text, '(\W|^)'||regexp_replace(substr(column_value, 2), '[^[:alnum:]]'))
                                                             then 1
                                                        end
                                                   when -- �������| - ����� ������������� ��������
                                                        substr(column_value, 1, 1) <> '|' and
                                                        substr(column_value, length(column_value)) = '|' 
                                                   then case when regexp_like(var_text, regexp_replace(substr(column_value, 1, length(column_value)-1), '[^[:alnum:]]')||'(\W|$)')
                                                             then 1
                                                        end
                                                   when -- |�������| - ����� ��������� � ��������
                                                        substr(column_value, 1, 1) = '|' and
                                                        substr(column_value, length(column_value)) = '|' 
                                                   then case when regexp_like(var_text, '(\W|^)'||regexp_replace(substr(column_value, 2, length(column_value)-2), '[^[:alnum:]]')||'(\W|$)')
                                                             then 1
                                                        end                                                        
                                                   when -- ������� �������, ��������� ����� like
                                                        var_text like '%'||column_value||'%' escape '\'
                                                   then 1
                                              end),
                                              count(*), 1, 0)
                              from table(mantas.rf_pkg_scnro.list_to_tab(lexem, '&'))) as lex_exists_flag, -- ���� ����������� ������, ��� ���� � ������ ������ ����������� ��� ������� �� ������
                           case when sum(decode(modificator, null, 1, 0)) over() = 0 and -- "�������������" ������ ���
                                     sum(decode(modificator, '!', 1, 0)) over() > 0      -- "�������������" ������� ����
                                then 1
                                else 0
                           end as only_neg_rule_flag   -- ����, ��� ������� �������� ������ "�������������" �������
                      FROM (select case when substr(trim(column_value), 1, 1) in ('!', '^')
                                        then substr(trim(column_value), 1, 1)
                                    end as modificator,    -- ����������� �������/������ ������
                                   case when substr(trim(column_value), 1, 1) in ('!', '^')
                                        then substr(trim(column_value), 2)
                                        else trim(column_value)
                                   end as lexem            -- �������/����� ������ ��� ������������
                              from table(mantas.rf_pkg_scnro.list_to_tab(lower(trim(par_rule))))))
             WHERE NOT(nvl(modificator, ' ') = '^' and lex_exists_flag = 0) -- �� ����� "������" �������, ������������� � ����������� ������
            ORDER BY lex_exists_flag desc, /*"�������������" ������� � �����, �� ����� � ����� �������������*/
                     case modificator when '!' then 1 /*����� ���������� 0*/ when '^' then 2 /*�����������*/ else 3 /*��������� �� ������������*/ end
                     ) LOOP
    --
    -- ������� ����������� �� "������������" ���������
    --
    if r.lex_exists_flag = 1 Then
        --
      -- ���� 1 - ���������: ���� "���������" ��������� - ������� 0
      --
      if r.modificator = '!' Then
        var_result := 0;
        EXIT;
      --
      -- ���� 2 - "������" �������: ���� "���������" ������ ������� - ���������� �� ��� ������������ ������� "�����������" "�������������" ������
      --
      elsif r.modificator = '^' Then
        var_false_lex.extend();
        var_false_lex(var_false_lex.COUNT) := r.lexem;
      --
      -- ���� 3 - "�������������" ������� - ���������, ��� "�����������" ������� �� �������� "������"
      --
      elsif r.modificator is null Then
        --
        -- ���� "������" ������ ������ �� "���������", �� ������� 1
        --
        if var_false_lex.COUNT = 0 Then
            var_result := 1;
          EXIT;
        end if;
        --
        -- �������� �� ������ ��� "������" �������, ����������� � "�����������"
        --
        var_text_replace := var_text;
        var_flag := 0;
        FOR r2 IN (SELECT /*+ NO_PARALLEL*/ distinct false_lex.column_value as false_lex
                     FROM TABLE(var_false_lex) false_lex,
                          TABLE(mantas.rf_pkg_scnro.list_to_tab(r.lexem, '&')) lex
                    WHERE false_lex.column_value like '%'||lex.column_value||'%' escape '\') LOOP
          var_text_replace := replace(var_text_replace, r2.false_lex);
          var_flag := 1;
        END LOOP;
        --
        -- ���� ��� "������" ������, ��������������� �����������, �� ������� 1
        --
        if var_flag = 0 Then
            var_result := 1;
          EXIT;
        end if;
        --
        -- ��������� - ����������� �� ��� � ������������ ������ ��� �������/����� ������, ���� �� - ������� 1
        --
        SELECT /*+ NO_PARALLEL*/ 
               decode(sum(case when var_text_replace like '%'||column_value||'%' escape '\'
                               then 1
                          end),
                      count(*), 1, 0)
          INTO var_flag
          FROM TABLE(mantas.rf_pkg_scnro.list_to_tab(r.lexem, '&'));
        --
        -- ���� ������� ��-�������� �����������, �� ������� 1
        --
        if var_flag = 1 Then
            var_result := 1;
          EXIT;
        end if;
      end if;
    --
    -- ����� �� "�������������" ������, ������ ��� "�����������" ������ (������������� ��� �� "������" "�������������")
    --
    else
      --
      -- ���� ������� �������� ������ ������������� (�� "�����������") ������� - ������� 1, ����� - ������� 0
      --
      if r.only_neg_rule_flag = 1 Then
        var_result := 1;
      else
        var_result := 0;
      end if;
      EXIT;
    end if;
  END LOOP;
  --
  -- �������
  --
  return var_result;
END check_lexeme;
--
-- �������� ����� ������������ ������� �� ��� ���� (������� ���������� ��� ��� �����������!)
--
FUNCTION get_crit_tx
(  
  par_crit_cd        rf_opok_criteria.crit_cd%TYPE,                -- ��� ������������ �������
  par_crit_tp_cd     rf_opok_criteria.crit_tp_cd%TYPE DEFAULT null -- �������������� ��� ������������ ������� (���� �������� ��� ����� ���������� �� ���������� ������� ������ null)
)
RETURN rf_opok_criteria.crit_tx%TYPE PARALLEL_ENABLE IS

  var_crit_cd    rf_opok_criteria.crit_cd%TYPE;
  var_crit_tx    rf_opok_criteria.crit_tx%TYPE;
BEGIN
  var_crit_cd := trim(par_crit_cd);
  --
  -- �������� ����������
  --
  if var_crit_cd is null Then
    return null;
  end if;
  --
  -- ���� ��� ������� (��� ������ ����), ������� ���
  --
  if SYSDATE - pck_crit_cache_date > 1/24 Then     
    pck_crit_cache.DELETE;
    pck_crit_cache_date := null;
  end if;
  --
  -- ���� � ���� ���� ������ ������� - ������� ��� �� ����,
  -- ����� - ������� �� ������� � ������� � ���
  --    
  if pck_crit_cache.EXISTS(var_crit_cd) Then
    var_crit_tx := pck_crit_cache(var_crit_cd);
  else
    SELECT /*+ NO_PARALLEL*/ 
           max(trim(crit_tx))
      INTO var_crit_tx
      FROM rf_opok_criteria
     WHERE crit_cd = var_crit_cd and
           crit_tp_cd = nvl(par_crit_tp_cd, crit_tp_cd); 

    if var_crit_tx is not null Then
      pck_crit_cache(var_crit_cd) := var_crit_tx;
      
      -- ���� ��� ������ ������ � ��� - ���������� �����
      if pck_crit_cache_date is null Then
        pck_crit_cache_date := SYSDATE;
      end if;    
    end if;  
  end if;

  return var_crit_tx;
END get_crit_tx;  
--
-- ��������� ��������� ������� �� ������� ������ � ��������� �����
--
FUNCTION check_lexeme
(
  par_texts          RF_TAB_VARCHAR,               -- ��������� ������� (��������, �������) ��� ��������
  par_crit_cd        rf_opok_criteria.crit_cd%TYPE -- ��� ������� ������
)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS -- 1 - ���� �� ���� ������� (�����) ������������� ���������� ������� ������
                                                -- 0 - �� ���� �� ��������� �� ������������� ���������� ������� ������
                                                -- null - ������
  var_crit_tx    rf_opok_criteria.crit_tx%TYPE;
  var_result     INTEGER;
BEGIN
  --
  -- �������� ����������
  --
  if par_texts is null or trim(par_crit_cd) is null or par_texts.COUNT() = 0 Then
    return null;
  end if;
  --
  -- ������� ����� ������� �� �������
  --
  var_crit_tx := get_crit_tx(par_crit_cd, 2);
  --
  -- ���� ����� ������� �������� �� ������� - ������
  --  
  if var_crit_tx is null Then
    return null;
  end if;
  --
  -- ��������� ������ �������� ������� ���� �� ������ ����������� ��� �������
  --
  var_result := 0;
  FOR i IN 1..par_texts.COUNT() LOOP
    
    if trim(par_texts(i)) is not null and 
       mantas.rf_pkg_scnro.check_lexeme(par_texts(i), var_crit_tx) = 1 Then
       
      var_result := 1;
      EXIT;
    end if;        
  END LOOP;
  
  return var_result;  
END check_lexeme;
--
-- ��������� ������� (�����) �� ������������ �������, ����������� ������� ���������, 
-- ��� ���� ����� ��������� � ������ ����� ������ ��������������� ���� (!), ��� �������� ���������
-- �������: 1) ������� RUR ������������� �������: RUR, USD, EUR
--          2) ������� RUR �� ������������� �������: USD, EUR             
--          3) ������� RUR �� ������������� �������: !RUR             
--          4) ������� RUR ������������� �������: !USD             
--
-- ������������ ��������: 1 - ������� ������������� �������, 0 - �� �������������, null - �������� �������� ���������
--
FUNCTION check_in_list(par_element  VARCHAR2, par_rule  VARCHAR2, par_case_sensitive INTEGER DEFAULT 0)
RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS
  var_element  VARCHAR2(32000);
  var_rule     VARCHAR2(32000);
  var_result   INTEGER;
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_element) is null or trim(par_rule) is null Then
    return null;
  end if;
  --
  -- ���� ��������� � ������ �������� �� ����� - ��������� ��� � ������� �������
  --
  if par_case_sensitive = 0 Then
    var_element := upper(trim(par_element));
    var_rule := upper(trim(par_rule));
  else   
    var_element := trim(par_element);
    var_rule := trim(par_rule);
  end if;  
  --
  -- �������� ������������ �������� �������
  --
  SELECT /*+ NO_PARALLEL*/
         case when sum(case when var_element = trim(leading '!' from trim(column_value)) and
                                 substr(trim(column_value), 1, 1) = '!'
                            then 1
                       end) > 0
              then 0 /*������� ��������� ��������*/
              when sum(case when var_element = trim(leading '!' from trim(column_value)) and
                                 substr(trim(column_value), 1, 1) <> '!'
                            then 1
                       end) > 0
              then 1 /*������� ������� ��� ���������*/  
              else /*������� �� ����������� � �������*/
                   case when sum(case when substr(trim(column_value), 1, 1) <> '!'
                                      then 1
                                 end) > 0
                        then 0 /*������� ������� �� ������ �� ���������*/
                        else 1 /*� ������� ������ ���������*/
                   end         
         end
    INTO var_result                      
    FROM table(mantas.rf_pkg_scnro.list_to_tab(trim(var_rule)));
    
  return var_result;      
END check_in_list;
--
-- ��������� �� �� ��������� ��������� (���� ������ ���) �� �������������� ���������� �������
-- ������������ ��������: 
--   1 - �� ����������� ���������� �������, 0 - �� �����������, null - �������� �������� ���������
--
FUNCTION check_org_in_wlist(par_watch_list_cd  VARCHAR2, par_inn_nb  VARCHAR2, par_date DATE DEFAULT null)
RETURN INTEGER PARALLEL_ENABLE IS
  var_result    INTEGER;
BEGIN
  --
  -- �������� ���������� ���������
  --
  if par_watch_list_cd is null or trim(par_inn_nb) is null Then
    return null;
  end if;
  --
  -- �������� �������������� � ������� ��
  --
  SELECT /*+ NO_PARALLEL*/ count(*)
    INTO var_result
    FROM mantas.rf_watch_list_org
   WHERE watch_list_cd = par_watch_list_cd and
         inn_nb = trim(par_inn_nb) and
         active_fl = 'Y' and
         (par_date is null or par_date between nvl(start_dt, par_date) and nvl(end_dt, par_date)) and 
         rownum <= 1;
         
  return var_result;       
END check_org_in_wlist;
--
-- �������� �������� ���� (��������, ���������� ������������� ��������), � ������������ � ������������ ��������� ���������
--
FUNCTION find_opok (
  par_sdate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� �) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_edate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� ��) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_tb_id            INTEGER                       DEFAULT null,  -- ������ �� ����. ���� �������� (38 - ����������)
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ���������, ���� ������, �� ������ ��� ������� � �����������
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ����, ���� ������, �� ����������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ���� ������, �� ����������� ������ �������, ��������� � ���
  par_vers_status_cd   VARCHAR2                      DEFAULT null,  -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������
  par_crit_status_cd   VARCHAR2                      DEFAULT null   -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN mantas.RF_TAB_OPOK PIPELINED IS

  var_vers_status_cd   VARCHAR2(4 CHAR);
  var_crit_status_cd   VARCHAR2(4 CHAR);

  var_opok    mantas.RF_TYPE_OPOK := mantas.RF_TYPE_OPOK(null, null, null, null, null, null, null, null, null, null,
                                                         null, null, null, null, null, null, null, null, null, null,
                                                         null, null, null, null, null, null, null, null, null, null,
                                                         null, null, null, null, null, null, null, null, null, null);
  var_sql     CLOB;

  TYPE c_opok_type IS REF CURSOR;
  c_opok      c_opok_type;
BEGIN
  --
  -- ���� �� ������� ��������: ��������/����������� �������/�������� ��������� - 
  -- ������� �� � ����������� �� ������� ���������� ������� ��������� (���� ��� �������)
  --
  SELECT case when max(vers_status_cd) in ('TEST','DEV')
              then nvl(par_vers_status_cd, 'TEST')
              else nvl(par_vers_status_cd, 'ACT')
         end,
         case when max(vers_status_cd) in ('TEST','DEV')
              then nvl(par_crit_status_cd, 'TEST')
              else nvl(par_crit_status_cd, 'ACT')
         end       
    INTO var_vers_status_cd, var_crit_status_cd
    FROM mantas.rf_opok_rule 
   WHERE rule_seq_id = par_rule_seq_id;    
  --
  -- ���������� ����� �������, ������������ ���� ��������� ����
  --
  var_sql := get_opok_select('FIND', par_rule_seq_id => par_rule_seq_id,
                                     par_opok_nb => par_opok_nb,
                                     par_query_cd => par_query_cd,
                                     par_vers_status_cd => var_vers_status_cd,
                                     par_crit_status_cd => var_crit_status_cd);
  --
  -- ��������� �������� ����������, ������� ������, ������ � ���������� ������
  --
  pck_start_date := trunc(par_sdate);
  pck_end_date := trunc(par_edate);
  pck_tb_id := par_tb_id;
  dbms_session.set_context('CLIENTCONTEXT', 'AMLOPOK_START_DATE', to_char(par_sdate, 'dd.mm.yyyy'));
  dbms_session.set_context('CLIENTCONTEXT', 'AMLOPOK_END_DATE', to_char(par_edate, 'dd.mm.yyyy'));
  dbms_session.set_context('CLIENTCONTEXT', 'AMLOPOK_TB_ID', to_char(par_tb_id));

  EXECUTE IMMEDIATE 'alter session force parallel query parallel 64';

  OPEN c_opok FOR var_sql;
  LOOP
    FETCH c_opok INTO var_opok.op_cat_cd, var_opok.partition_date, var_opok.subpartition_key, var_opok.opok_status, 
                      var_opok.trxn_seq_id, var_opok.trxn_scrty_seq_id, var_opok.opok_list, var_opok.trxn_dt, var_opok.data_dump_dt, var_opok.branch_id,
                      var_opok.trxn_crncy_am, var_opok.trxn_base_am, var_opok.trxn_crncy_cd, var_opok.trxn_desc, var_opok.nsi_op_id,
                      var_opok.scrty_base_am, var_opok.scrty_type_cd, var_opok.scrty_id,  
                      var_opok.orig_nm, var_opok.orig_inn_nb, var_opok.orig_acct_nb, var_opok.orig_own_fl, var_opok.debit_cd,
                      var_opok.send_instn_nm, var_opok.send_instn_id, var_opok.send_instn_acct_id,
                      var_opok.benef_nm, var_opok.benef_inn_nb, var_opok.benef_acct_nb, var_opok.benef_own_fl, var_opok.credit_cd,
                      var_opok.rcv_instn_nm, var_opok.rcv_instn_id, var_opok.rcv_instn_acct_id,
                      var_opok.tb_id, var_opok.osb_id, var_opok.src_sys_cd, var_opok.src_change_dt, var_opok.src_user_nm, var_opok.src_cd;

    EXIT WHEN c_opok%NOTFOUND;
    PIPE ROW(var_opok);
  END LOOP;
  CLOSE c_opok;
  
  EXECUTE IMMEDIATE 'alter session enable parallel query';
END find_opok;
--
-- ������� ���������� ��������, ������� ��������� ��������� ��� ����, ����� ������� ���� � ������������ � ������������ ��������� ���������
-- ������������ ��� ������: ��������� ����� ����� ����������� ������� find_opok
--
FUNCTION estimate_find_opok (
  par_sdate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� �) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_edate            DATE,                                        -- ������������ - ������ �� ���� ���������� �������� (���� ��) - ������������� ������ �������� � ����� ���������� ������ ��� ����� ���������
  par_tb_id            INTEGER                       DEFAULT null,  -- ������ �� ����. ���� �������� (38 - ����������)
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ���������, ���� ������, �� ������ ��� ������� � �����������
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ����, ���� ������, �� ����������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ���� ������, �� ����������� ������ �������, ��������� � ���
  par_vers_status_cd   VARCHAR2                      DEFAULT null,  -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������
  par_crit_status_cd   VARCHAR2                      DEFAULT null   -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN INTEGER IS
  var_vers_status_cd   VARCHAR2(4 CHAR);
  var_crit_status_cd   VARCHAR2(4 CHAR);

  var_sql           CLOB;
  var_statement_id  VARCHAR2(30);
  var_result        INTEGER;
BEGIN
  SAVEPOINT estimate_find_opok;
  --
  -- ���� �� ������� ��������: ��������/����������� �������/�������� ��������� - 
  -- ������� �� � ����������� �� ������� ���������� ������� ��������� (���� ��� �������)
  --
  SELECT case when max(vers_status_cd) in ('TEST','DEV')
              then nvl(par_vers_status_cd, 'TEST')
              else nvl(par_vers_status_cd, 'ACT')
         end,
         case when max(vers_status_cd) in ('TEST','DEV')
              then nvl(par_crit_status_cd, 'TEST')
              else nvl(par_crit_status_cd, 'ACT')
         end       
    INTO var_vers_status_cd, var_crit_status_cd
    FROM mantas.rf_opok_rule 
   WHERE rule_seq_id = par_rule_seq_id;    
  --
  -- ���� �� �������� ��������/������ ����� � ��������, �� ������� ����� ���������� ����������
  -- ��������� ������, ������������ �� ����� ������ �� ������ ����������� ��������/������ ������ � ��������
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/
                   to_clob('')||
                   'SELECT '||trxn_am_col||' FROM '||table_name||' WHERE '||
                     trxn_dt_col||' between to_date('''||to_char(par_sdate, 'dd.mm.yyyy')||''', ''dd.mm.yyyy'') and '||
                                           'to_date('''||to_char(par_edate, 'dd.mm.yyyy')||''', ''dd.mm.yyyy'') + 1 - 1/24/3600'||
                     case when limit_am > 0
                          then ' and '||trxn_am_col||' >= '||to_char(limit_am)
                     end||       
                     case when par_tb_id is not null and subpart_key_col is not null
                          then ' and '||subpart_key_col||' between '||to_char(par_tb_id*1000)||' and '||to_char(par_tb_id*1000 + 999)
                     end||       
                     case when par_tb_id is not null and tb_id_col is not null
                          then ' and '||tb_id_col||' = '||to_char(par_tb_id)
                     end as sql_tx                           
              FROM (SELECT case when query_cd = '�����������' and op_cat_cd = 'WT' then 'BUSINESS.WIRE_TRXN wt'
                                when query_cd = '�����������' and op_cat_cd = 'CT' then 'BUSINESS.CASH_TRXN ct'
                                when query_cd = '�����������' and op_cat_cd = 'BOT' then 'BUSINESS.BACK_OFFICE_TRXN bot'
                                when query_cd = '������_������' and op_cat_cd = 'WT' then 'BUSINESS.RF_WIRE_TRXN_SCRTY wts'
                                when query_cd = '������_������' and op_cat_cd = 'CT' then 'BUSINESS.RF_CASH_TRXN_SCRTY cts'
                           end as table_name,                                                      
                           case when query_cd = '�����������' and op_cat_cd = 'WT' then 'wt.trxn_exctn_dt'
                                when query_cd = '�����������' and op_cat_cd = 'CT' then 'ct.trxn_exctn_dt'
                                when query_cd = '�����������' and op_cat_cd = 'BOT' then 'bot.exctn_dt'
                                when query_cd = '������_������' and op_cat_cd = 'WT' then 'wts.trxn_exctn_dt'
                                when query_cd = '������_������' and op_cat_cd = 'CT' then 'cts.trxn_exctn_dt'
                           end as trxn_dt_col,
                           case when query_cd = '�����������' and op_cat_cd = 'WT' then 'wt.rf_trxn_opok_am'
                                when query_cd = '�����������' and op_cat_cd = 'CT' then 'ct.rf_trxn_opok_am'
                                when query_cd = '�����������' and op_cat_cd = 'BOT' then 'bot.trxn_base_am'
                                when query_cd = '������_������' and op_cat_cd = 'WT' then 'wts.scrty_base_am'
                                when query_cd = '������_������' and op_cat_cd = 'CT' then 'cts.scrty_base_am'
                           end as trxn_am_col,
                           min(rl.limit_am) as limit_am,
                           case when query_cd = '�����������' 
                                then lower(rl.op_cat_cd)||'.rf_subpartition_key'
                           end as subpart_key_col,
                           case when query_cd = '������_������' 
                                then lower(rl.op_cat_cd)||'s.tb_id'
                           end as tb_id_col
                      FROM mantas.rfv_opok_rule_sql rl
                     WHERE /* ���������� �������� ��������� ������ ��� "�����������" ��������*/
                           rl.query_cd in ('�����������', '������_������') and
                           NOT (rl.query_cd = '������_������' and rl.op_cat_cd = 'BOT') and
                           rl.query_cd = nvl(par_query_cd, rl.query_cd) and
                           rl.rule_seq_id = nvl(par_rule_seq_id, rl.rule_seq_id) and
                           (par_opok_nb is null or rl.opok_nb = par_opok_nb or rl.group_nb = par_opok_nb) and
                           rl.crit_status_cd = var_crit_status_cd and
                           (par_rule_seq_id is not null or (var_vers_status_cd = 'ACT' and rl.act_dup_number = 1) or (var_vers_status_cd = 'TEST' and rl.test_dup_number = 1)) and
                           ((var_crit_status_cd = 'ACT' and err_fl = 'N') or (var_crit_status_cd = 'TEST' and test_err_fl = 'N'))
                    GROUP BY query_cd,
                             op_cat_cd)) LOOP

    var_sql := var_sql||' UNION ALL '||r.sql_tx;
  END LOOP;             
  
  var_sql := substr(var_sql, length(' UNION ALL ') + 1);
  
  if var_sql is null then 
    return null;
  end if;  
  --
  -- ��������� ���������� �����, ������������ �������� ����� ������������ EXPLAIN PLAN �������� CARDINALITY
  --
  var_statement_id := to_char(sysdate, 'ddmmyyyyhh24miss');

  --dbms_output.put_line('EXPLAIN PLAN SET statement_id = '''||var_statement_id||''' FOR '||var_sql);
  EXECUTE IMMEDIATE 'EXPLAIN PLAN SET statement_id = '''||var_statement_id||''' FOR '||var_sql;

  SELECT /*+ NO_PARALLEL*/ max(cardinality) 
    INTO var_result
    FROM plan_table 
   WHERE statement_id = var_statement_id and 
         depth = 0;
         
  ROLLBACK TO SAVEPOINT estimate_find_opok;       

  return var_result;
END estimate_find_opok;
--
-- �������� �������� ������� � ������������ � ��������� ��������� ����
--
FUNCTION opok_alerts (
  par_mode             VARCHAR2                      DEFAULT 'NEW', -- 'NEW' - ��������� ��������� ��� �������� � �������� ��������, �� ������� ��� ��� �������; 'TRXN' - ��������� ��������� ��� ��������� ��������
  par_op_cat_cd        VARCHAR2                      DEFAULT null,  -- ������������ � ������ 'TRXN' - ��������� ��������: WT - �����������, CT - ��������, BOT - ����������
  par_trxn_seq_id      NUMBER                        DEFAULT null   -- ������������ � ������ 'TRXN' - ID �������� (PK ������ WIRE_TRXN/CASH_TRXN/BACK_OFFICE_TRXN)   
  )
RETURN mantas.RF_TAB_OPOK_ALERT PIPELINED IS
  var_alert   mantas.RF_TYPE_OPOK_ALERT := mantas.RF_TYPE_OPOK_ALERT(null, null, null, null, null, null, null, null);
  var_sql     CLOB;

  TYPE c_alert_type IS REF CURSOR;
  c_alert     c_alert_type;

  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_scnro.opok_alerts)';
BEGIN
  --
  -- �������� ����������
  --
  if par_mode not in ('NEW', 'TRXN') or 
     par_op_cat_cd not in ('WT', 'CT', 'BOT') or
     (par_mode = 'TRXN' and (par_op_cat_cd is null or par_trxn_seq_id is null)) Then
     
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);
  end if;
  --
  -- �������� ����� �������, ������������� �� ����� ������ �� ������ ����� (����� ������������� �������� ��� ��������� ������ ������ � ��������)
  --
  -- � ������ ��������� ��������� �������� �������� ������ �� ����, ���� � ������� ��� ������ ������ �� ����� ���� - ����������� ���������� ��������� ��������
  if par_mode = 'TRXN' and
     pck_opok_alerts_trxn_sql is not null and
     SYSDATE - pck_opok_alerts_trxn_date <= 1/24 Then
     
     var_sql := pck_opok_alerts_trxn_sql;
  else   
    var_sql := '';
    FOR r IN (SELECT /*+ NO_PARALLEL*/
                     to_clob('')||' UNION ALL '||
                     'SELECT op_cat_cd, trxn_seq_id, trxn_scrty_seq_id, opok_list, trxn_dt, data_dump_dt, branch_id, '||
                            'op_cat_cd||''|''||to_char(trxn_seq_id)||''|'''||case when q.query_cd = '������_������' 
                                                                                  then '||case when mult_scrty_flag = 1 then to_char(trxn_scrty_seq_id) end'
                                                                             end||' as alert_group_code '||
             case when par_mode = 'TRXN'
                  then to_clob('')||
                       'FROM mantas.'||q.view_nm||' ' -- ��� ��������� �������� ���������� view - �������� �� ��������
                  when par_mode = 'NEW'               
                  then to_clob('')||
                       'FROM ('||get_opok_select('RUN', par_query_cd => q.query_cd, par_crit_status_cd => 'ACT')||') ' -- ��� ��������� ��������� ����������� ������ � ���������������� �������
             end||       
                      'WHERE '||case when par_mode = 'NEW'
                                     then 'rf_partition_date = to_date(''01.01.9999'', ''dd.mm.yyyy'') and '||
                                          --'rf_partition_date between to_date(''01.07.2014'', ''dd.mm.yyyy'') and to_date(''31.07.2014'', ''dd.mm.yyyy'') and '||
                                          'NOT EXISTS(select null '||
                                                       'from mantas.kdd_review rv '||
                                                      'where rv.rf_op_cat_cd = op_cat_cd and '||
                                                            'rv.rf_trxn_seq_id = trxn_seq_id and '||
                                                            'rv.rf_alert_type_cd = ''O'') and '
                                     when par_mode = 'TRXN'  
                                     then 'op_cat_cd = mantas.rf_pkg_scnro.get_op_cat_cd and '||
                                          'trxn_seq_id = mantas.rf_pkg_scnro.get_trxn_seq_id and '
                                end||
                             'opok_list is not null' as sql_tx           
                FROM mantas.rf_opok_query q
               WHERE /*query_cd in ('�����������', '������_��������', '������_������') and */
                     EXISTS(select null
                              from mantas.rf_opok_rule rl
                             where rl.query_cd = q.query_cd and
                                   rl.vers_status_cd = 'ACT' and
                                   nvl(rl.err_fl, 'N') <> 'Y')) LOOP
      var_sql := var_sql||r.sql_tx;                             
    END LOOP; 

    var_sql := substr(var_sql, length(' UNION ALL ') + 1);      
    
    var_sql := 'SELECT '||case when par_mode = 'TRXN' then '/*+ NO_PARALLEL*/ ' else '/*+ PARALLEL(64)*/ ' end||
                      'case when sum(decode(trxn_scrty_seq_id, null, 0, 1)) = 0 '|| -- � ������ ��� ������ ������
                           'then ''TRXN'' '||
                           'when sum(decode(trxn_scrty_seq_id, null, 1, 0)) = 0 '|| -- � ������ ������ ������ ������
                           'then ''TRXN_SCRTY'' '||
                           'else ''TRXN_SCRTY_JOINT'' '||
                      'end as object_tp_cd, '||     
                      'op_cat_cd, trxn_seq_id, max(trxn_scrty_seq_id) as trxn_scrty_seq_id, '||
                      'listagg(rf_pkg_scnro.add_to_list_elements(opok_list, ''~''||to_char(trxn_scrty_seq_id), ''|''), ''|'') within group(order by rownum) as opok_list, '||
                      --'replace(replace(replace(xmlagg(xmlelement("X", rf_pkg_scnro.add_to_list_elements(opok_list, ''~''||to_char(trxn_scrty_seq_id), ''|''))).getclobval(), ''</X><X>'', ''|''), ''<X>''), ''</X>'') as opok_list, '|| -- ������� ��� CLOB - ���� ��������
                      'max(trxn_dt) as trxn_dt, max(data_dump_dt) as data_dump_dt, max(branch_id) as branch_id '||
                 'FROM ('||var_sql||') '||
                'GROUP BY op_cat_cd, trxn_seq_id, alert_group_code';                            
    --
    -- ���������� �������������� ������ � ���
    --
    if par_mode = 'TRXN' Then
      pck_opok_alerts_trxn_sql := var_sql;
      pck_opok_alerts_trxn_date := SYSDATE;
    end if;  
  end if;  
  --
  -- ������������� ��������� �������� � ��������
  --
  if par_mode = 'TRXN' Then
    pck_op_cat_cd := par_op_cat_cd;
    pck_trxn_seq_id := par_trxn_seq_id;
    dbms_session.set_context('CLIENTCONTEXT', 'AMLOPOK_OP_CAT_CD', par_op_cat_cd);
    dbms_session.set_context('CLIENTCONTEXT', 'AMLOPOK_TRXN_SEQ_ID', to_char(par_trxn_seq_id));
  end if;
  --
  -- ��������� ������, ���������� ����������
  --
  OPEN c_alert FOR var_sql;
  LOOP
    FETCH c_alert INTO var_alert.object_tp_cd, var_alert.op_cat_cd, var_alert.trxn_seq_id, var_alert.trxn_scrty_seq_id, 
                       var_alert.opok_list, var_alert.trxn_dt, var_alert.data_dump_dt, var_alert.branch_id;

    EXIT WHEN c_alert%NOTFOUND;
    PIPE ROW(var_alert);
  END LOOP;
  CLOSE c_alert;  
END opok_alerts;
--
-- ����������� �������������, ���������� ������ ��������� ����, ��� ���������� ������� ���������/��� ���� ��������
--
FUNCTION rebuild_opok_view (
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ��� �������� ���������� ����������� view, ���� null - �� ���
  par_changed_only_fl  VARCHAR2                      DEFAULT 'Y'    -- ���� Y/N - ������� �� ����������� ������ �� view, ������� ������� ������������ (��� ������� ��������� ���������� ���� REBUILD_FL) ��� ���
  )
RETURN INTEGER IS -- ���������� ������������ ��� ������������ ������ ��������� � �������� (���� �������� ������ "���� ������?")
  var_sql     CLOB;
  var_count   INTEGER := 0;
  var_dummy   INTEGER;
  
  var_grant_to_list    VARCHAR2(255) := 'KDD_ALG, KDD_MNR with grant option';
BEGIN
  --
  -- ���� �� �������� ���������, view ������� ����� �������������
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/ query_cd, view_nm
              FROM mantas.rf_opok_query
             WHERE query_cd = nvl(par_query_cd, query_cd) and
                   rebuild_fl = decode(par_changed_only_fl, 'Y', 'Y', rebuild_fl)) LOOP
    --
    -- �������� ������� ���������, �������� �� ��������������� view, ������� ���������
    --
    var_count := var_count + check_and_mark_opok_rule(par_query_cd => r.query_cd, par_vers_status_cd => 'ACT', par_crit_status_cd => 'ACT');
    --
    -- ������� ������ ��� view
    --
    var_sql := get_opok_select('VIEW', par_query_cd => r.query_cd, par_crit_status_cd => 'ACT');

    if var_sql is not null Then
      --
      -- ���������� view
      --
      EXECUTE IMMEDIATE 'CREATE OR REPLACE VIEW MANTAS.'||r.view_nm||' as '||var_sql;
      --
      -- �������� ������ ������� � �������� ���������, � ������������ � �������� ������� view
      --
      var_dummy := shapshot_opok_rule(r.query_cd);
      --
      -- ������� ����� �� view
      --
      EXECUTE IMMEDIATE 'grant SELECT on MANTAS.'||r.view_nm||' to '||var_grant_to_list;
    end if;
  END LOOP;
  --
  -- �������
  --
  return var_count;
END rebuild_opok_view;
--
-- ��������� ������� ��������� (������ ����� �� �������������� ������������), ����������/����� ����� ������� ������, ��������/�������� ��������� �� �������
-- ��������! ������� ��������� COMMIT!
--
FUNCTION check_and_mark_opok_rule (
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ���������, ���� ������, �� ����������� ������ �������, ��������� � ���
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ���������, ���� ������, �� ������ ��� ������� � �����������
  par_crit_cd          rf_opok_criteria.crit_cd%TYPE DEFAULT null,  -- ��� ������������ ������� ���������, ���� ������, �� ����������� ������ �������, ������������ ��� ������� (����������� �� ����)
  par_vers_status_cd   rf_opok_rule.vers_status_cd%TYPE DEFAULT 'ALL', -- ��� ������� ������ �������, ����������� � ��������: 'ACT' - ��������� ����������� ������ ������, � �. �. (��. ������� RF_OPOK_RULE.VERS_STATUS_CD), 'ALL' - ��������� ��� (����� ��������)
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ALL'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������, 'ALL' - �������� ������ � � ������������, � � ��������� ���������� ���������
  )
RETURN INTEGER IS -- ���������� ������������ ������ ��������� � �������� (���� �������� ������ "���� ������?" ��� "���� ������ - ����?")

  var_count   INTEGER := 0;
BEGIN
  --
  -- ���������/������ ��� ������ ��������� ����� ������� ������ ��� ����������� �/��� �������� ������ ����������� �������
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/ 
                   rule_seq_id,
                   case when par_crit_status_cd in ('ACT', 'ALL')
                        then check_opok_rule(rule_seq_id, 'ACT')
                   end as err_mess_tx,
                   case when par_crit_status_cd in ('TEST', 'ALL')
                        then check_opok_rule(rule_seq_id, 'TEST')
                   end as test_err_mess_tx
              FROM rf_opok_rule
             WHERE query_cd = nvl(par_query_cd, query_cd) and
                   rule_seq_id = nvl(par_rule_seq_id, rule_seq_id) and
                   (nvl(acct_crit_cd, '?#$')        = coalesce(par_crit_cd, acct_crit_cd, '?#$') or
                    nvl(acct2_crit_cd, '?#$')       = coalesce(par_crit_cd, acct2_crit_cd, '?#$') or
                    nvl(lexeme_crit_cd, '?#$')      = coalesce(par_crit_cd, lexeme_crit_cd, '?#$') or
                    nvl(optp_crit_cd, '?#$')        = coalesce(par_crit_cd, optp_crit_cd, '?#$') or
                    nvl(custom_crit_cd, '?#$')      = coalesce(par_crit_cd, custom_crit_cd, '?#$') or
                    nvl(explanation_crit_cd, '?#$') = coalesce(par_crit_cd, explanation_crit_cd, '?#$')) and
                   vers_status_cd = case when par_vers_status_cd = 'ALL' and vers_status_cd <> 'ARC'
                                         then vers_status_cd
                                         when par_vers_status_cd <> 'ALL'
                                         then par_vers_status_cd
                                    end) LOOP

    UPDATE rf_opok_rule t
       SET t.err_fl = case when par_crit_status_cd in ('ACT', 'ALL')
                           then decode(r.err_mess_tx, null, 'N', 'Y')
                           else t.err_fl
                      end,
           t.err_mess_tx = case when par_crit_status_cd in ('ACT', 'ALL')
                                then r.err_mess_tx
                                 else t.err_mess_tx
                           end,
           t.test_err_fl = case when par_crit_status_cd in ('TEST', 'ALL')
                                then decode(r.test_err_mess_tx, null, 'N', 'Y')
                                else t.test_err_fl
                           end,
           t.test_err_mess_tx = case when par_crit_status_cd in ('TEST', 'ALL')
                                     then r.test_err_mess_tx
                                       else t.test_err_mess_tx
                                end
     WHERE t.rule_seq_id = r.rule_seq_id;

     if r.err_mess_tx is not null or r.test_err_mess_tx is not null Then
       var_count := var_count + 1;
     end if;
  END LOOP;
  --
  -- Commit � �������
  --
  COMMIT;

  RETURN var_count;
END check_and_mark_opok_rule;
--
-- ��������� ������� ��������� (������ ����� �� �������������� ������������), ���� ������� �� ������ �������� - ������� �������� ��������� �� ������
--
FUNCTION check_opok_rule (
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE,  -- ������������� ������� ���������, ������� ���������� ���������
  par_crit_status_cd   VARCHAR2                        -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN VARCHAR2 IS -- ��������� �� ������, ���� ������� ������� ������ �������� - null

  var_count      INTEGER;
  var_err_msg    VARCHAR2(2000 CHAR) := null;
  var_sql        CLOB;
BEGIN
  --
  -- ���� � ������� ��������� ������ ��� ������� - ������� ��������� ���������
  --
  SELECT /*+ NO_PARALLEL*/
         count(*),
         max(case when replace(acct_crit_cd||acct_add_crit_tx||acct_subtle_crit_tx||acct_area_tx||
                               acct2_crit_cd||acct2_add_crit_tx||acct2_subtle_crit_tx||acct2_area_tx||acct_reverse_fl||
                               cust_watch_list_cd||cust2_watch_list_cd||
                               lexeme_crit_cd||lexeme_area_tx||lexeme_add_purpose_tx||lexeme_add_cust_tx||lexeme_add_cust2_tx||
                               lexeme2_crit_cd||lexeme2_area_tx||lexeme2_add_purpose_tx||lexeme2_add_cust_tx||lexeme2_add_cust2_tx||
                               optp_crit_cd||optp_add_crit_tx||src_crit_tx||custom_crit_cd||custom_crit_sql_tx, ' ') is null
                  then '������� �� �������� ������� ���������'
             end)
    INTO var_count, var_err_msg
    FROM rf_opok_rule
   WHERE rule_seq_id = par_rule_seq_id;
  --
  -- ���� ������ �������� id ������� ��� �������� ������� ��������� ��������� - ������
  --
  if var_count = 0 or par_crit_status_cd not in ('ACT', 'TEST') Then
    raise_application_error(-20001, '�������� �������� ��������� (rf_pkg_scnro.check_opok_rule)');
  end if;

  if var_err_msg is not null Then
    return var_err_msg;
  end if;
  --
  -- �������� ��������� ������� �� �����:
  --  - ������� �� ���� �������� ������ ��������� �������: �����, �������, �����, ��������������� ����, ����� ������� �����: ����������
  --  - ������� �� ���� ������ ��������� ���� �� ���� ������������� �������
  --
  SELECT /*+ NO_PARALLEL*/
         trim(trim(';' from
           case when par_crit_status_cd = 'ACT'
                then case when crit1.crit_cd is not null and translate(replace(upper(crit1.crit_tx), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ����������� ������� �� ���� (������ ��������) �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������), ��� ������������ �������: '||crit1.crit_cd
                     end||
                     case when crit2.crit_cd is not null and translate(replace(upper(crit2.crit_tx), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ����������� ������� �� ���� (������ ��������) �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������), ��� ������������ �������: '||crit2.crit_cd
                     end||
                     case when trim(rl.acct_add_crit_tx) is not null and translate(replace(upper(trim(rl.acct_add_crit_tx)), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ������� �� ���� ������� ��������� �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������)'
                     end||
                     case when trim(rl.acct2_add_crit_tx) is not null and translate(replace(upper(trim(rl.acct2_add_crit_tx)), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ������� �� ���� ������� ��������� �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������)'
                     end||
                     case when replace(trim(',' from trim(crit1.crit_tx)||','||trim(rl.acct_add_crit_tx)), ' ') is not null and
                               0 = (select count(*)
                                      from TABLE(mantas.rf_pkg_scnro.list_to_tab(replace(trim(',' from trim(crit1.crit_tx)||','||trim(rl.acct_add_crit_tx)), ' ')))
                                     where column_value not like '!%')
                          then '; ������� �� ���� ������� ��������� �������� ������ ������� ���������, ������ ���� ���� �� ���� ������������� ������� (��������, �� ���� �������� ������: 1-9)'
                     end||
                     case when replace(trim(',' from trim(crit2.crit_tx)||','||trim(rl.acct2_add_crit_tx)), ' ') is not null and
                               0 = (select count(*)
                                      from TABLE(mantas.rf_pkg_scnro.list_to_tab(replace(trim(',' from trim(crit2.crit_tx)||','||trim(rl.acct2_add_crit_tx)), ' ')))
                                     where column_value not like '!%')
                          then '; ������� �� ���� ������� ��������� �������� ������ ������� ���������, ������ ���� ���� �� ���� ������������� ������� (��������, �� ���� �������� ������: 1-9)'
                     end  
                when par_crit_status_cd = 'TEST'
                then case when crit1.crit_cd is not null and translate(replace(upper(crit1.test_crit_tx), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ����������� ������� �� ���� (������ ��������) �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������). ��� ������������ �������: '||crit1.crit_cd
                     end||
                     case when crit2.crit_cd is not null and translate(replace(upper(crit2.test_crit_tx), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ����������� ������� �� ���� (������ ��������) �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������). ��� ������������ �������: '||crit2.crit_cd
                     end||
                     case when trim(rl.acct_add_crit_tx) is not null and translate(replace(upper(trim(rl.acct_add_crit_tx)), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ������� �� ���� ������� ��������� �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������)'
                     end||
                     case when trim(rl.acct2_add_crit_tx) is not null and translate(replace(upper(trim(rl.acct2_add_crit_tx)), '����������'), 'a1234567890-!, ', 'a') is not null
                          then '; ������� �� ���� ������� ��������� �������� ����������� ������� (���������: �����, �������, �����, ��������������� ����, ������)'
                     end||
                     case when replace(trim(',' from nvl(trim(crit1.test_crit_tx), trim(crit1.crit_tx))||','||trim(rl.acct_add_crit_tx)), ' ') is not null and
                               0 = (select count(*)
                                      from TABLE(mantas.rf_pkg_scnro.list_to_tab(replace(trim(',' from nvl(trim(crit1.test_crit_tx), trim(crit1.crit_tx))||','||trim(rl.acct_add_crit_tx)), ' ')))
                                     where column_value not like '!%')
                          then '; ������� �� ���� ������� ��������� �������� ������ ������� ���������, ������ ���� ���� �� ���� ������������� ������� (��������, �� ���� �������� ������: 1-9)'
                     end||
                     case when replace(trim(',' from nvl(trim(crit2.test_crit_tx), trim(crit2.test_crit_tx))||','||trim(rl.acct2_add_crit_tx)), ' ') is not null and
                               0 = (select count(*)
                                      from TABLE(mantas.rf_pkg_scnro.list_to_tab(replace(trim(',' from nvl(trim(crit2.test_crit_tx), trim(crit2.test_crit_tx))||','||trim(rl.acct2_add_crit_tx)), ' ')))
                                     where column_value not like '!%')
                          then '; ������� �� ���� ������� ��������� �������� ������ ������� ���������, ������ ���� ���� �� ���� ������������� ������� (��������, �� ���� �������� ������: 1-9)'
                     end                                     
           end))                         
    INTO var_err_msg
    FROM rf_opok_rule rl
         left join rf_opok_criteria crit1 on crit1.crit_cd = rl.acct_crit_cd
         left join rf_opok_criteria crit2 on crit2.crit_cd = rl.acct2_crit_cd
   WHERE rule_seq_id = par_rule_seq_id;
  
  if var_err_msg is not null Then
    -- ������� ������ ����� �������
    return upper(substr(var_err_msg, 1, 1))||substr(var_err_msg, 2);
  end if;
  --
  -- ������� ������ �������� ������� �� �������������� ������������
  --
  var_sql := get_opok_select('CHECK', par_rule_seq_id => par_rule_seq_id, par_crit_status_cd => par_crit_status_cd);
  --
  -- �������� ������, ���� ��������� ������ - ������� ��������� ���������
  --
  BEGIN
    EXECUTE IMMEDIATE var_sql;
  EXCEPTION
    WHEN OTHERS THEN
      var_err_msg := SQLERRM;
  END;

  return var_err_msg;
END check_opok_rule;
--
-- ������������ SELECT ���������� ���� (��� view ���������, ��� �������� ����������, ��� ��������� ���������) � ������������ � ��������� ��������� � ������� RF_OPOK_RULE
--
FUNCTION get_opok_select(
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'VIEW' - ��� view ���������, 'RUN' - ��� ���������� ���������, ��������� ������� (����� ��������), 'CHECK' - ��� �������� ���������� ������ ���������, 'FIND' - ��� ��������� ��������� ����
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ��������� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� ������ ��� ������� ���������� � SELECT (�������� ���������� ��� ��������� ���������)
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ���� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� � SELECT ���������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null,  -- ��� ������� ��������� - ������������ ��� ������� 'VIEW'/'RUN', ���� ������ ��� ������� 'CHECK' � 'FIND', �� � SELECT ���������� ������ �������, ��������� � ���
  par_vers_status_cd   VARCHAR2                      DEFAULT 'ACT', -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ACT'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN CLOB IS
  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_scnro.get_opok_select)';

  var_sql        CLOB := null;
  var_sql_tmp    CLOB := null;
  var_flag       INTEGER := 0;
BEGIN
  --
  -- �������� ����������
  --
  if par_query_type not in ('VIEW', 'RUN', 'CHECK', 'FIND') or (par_query_type in ('VIEW', 'RUN') and trim(par_query_cd) is null) Then
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);
  end if;
  --
  -- � ����������� �� ���� ������� � ���� ������� ��������� (���� ������) �������� SELECT
  --
  if par_query_type in ('VIEW', 'RUN') Then
    if par_query_cd in ('�����������', '������_������') Then
      var_sql := get_opok_select_standard(par_query_cd, par_query_type, par_rule_seq_id, par_opok_nb, par_vers_status_cd, par_crit_status_cd);

    elsif par_query_cd = '������_��������' Then
      var_sql := get_opok_select_firstrxn(par_query_type, par_rule_seq_id, par_opok_nb, par_vers_status_cd, par_crit_status_cd);

    /* ���������� ��� ��������� ���������� ���� ������� ��������� �� ����� �������:
    elsif par_query_cd = '???' Then
      var_sql := get_opok_select_&&&(par_query_type, par_rule_seq_id, par_opok_nb, par_vers_status_cd, par_crit_status_cd);
    */
    else
      raise_application_error(-20001, '��������� ��� ������� ��������� �� ��������������: '||par_query_cd||prc_name);
    end if;

  elsif par_query_type in ('CHECK', 'FIND') Then

    if par_query_cd = '�����������' or par_query_cd is null Then
      var_sql_tmp := get_opok_select_standard('�����������', par_query_type, par_rule_seq_id, par_opok_nb, par_vers_status_cd, par_crit_status_cd);
      
      if var_sql_tmp is not null Then
        var_sql := case when var_sql is null then to_clob('') else var_sql||' UNION ALL ' end||var_sql_tmp ;
      end if;
        
      var_flag := 1;
    end if;

    if par_query_cd = '������_������' or par_query_cd is null Then
      var_sql_tmp := get_opok_select_standard('������_������', par_query_type, par_rule_seq_id, par_opok_nb, par_vers_status_cd, par_crit_status_cd);
      
      if var_sql_tmp is not null Then
        var_sql := case when var_sql is null then to_clob('') else var_sql||' UNION ALL ' end||var_sql_tmp ;
      end if;
        
      var_flag := 1;
    end if;

    if par_query_cd = '������_��������' or par_query_cd is null Then
      var_sql_tmp := get_opok_select_firstrxn(par_query_type, par_rule_seq_id, par_opok_nb, par_vers_status_cd, par_crit_status_cd);
      
      if var_sql_tmp is not null Then
        var_sql := case when var_sql is null then to_clob('') else var_sql||' UNION ALL ' end||var_sql_tmp ;
      end if;
        
      var_flag := 1;
    end if;

    /* ���������� ��� ��������� ���������� ���� ������� ��������� �� ����� �������:
    if par_query_cd = '???' or par_query_cd is null Then
      var_sql_tmp := get_opok_select_&&&(par_query_type, par_rule_seq_id, par_opok_nb, par_vers_status_cd, par_crit_status_cd);
      
      if var_sql_tmp is not null Then
        var_sql := case when var_sql is null then to_clob('') else var_sql||' UNION ALL ' end||var_sql_tmp ;
      end if;
        
      var_flag := 1;
    end if;
    */

    if var_flag = 0 Then
      raise_application_error(-20001, '��������� ��� ������� ��������� �� ��������������: '||par_query_cd||prc_name);
    end if;

    if par_query_type = 'CHECK' and var_sql is not null Then
      var_sql := 'SELECT count(*) FROM ('||var_sql||')';
    end if;
  end if;

  return var_sql;
END get_opok_select;
--
-- ������������ SELECT ���������� ���� (��� view ���������, ��� �������� ����������, ��� ��������� ���������) ��� ��������� �������� ���������:
--  - ����������� ������ ��������� (��� ������� = '�����������')
--  - ������ ��������� ������ ����� (��� ������� = '������_������')
--
FUNCTION get_opok_select_standard(
  par_query_cd         rf_opok_query.query_cd%TYPE,                  -- ��� ������� ��������� - '�����������' ��� '������_������'
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'VIEW' - ��� view ���������, 'RUN' - ��� ���������� ���������, ��������� ������� (����� ��������), 'CHECK' - ��� �������� ���������� ������ ���������, 'FIND' - ��� ��������� ��������� ����
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ��������� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� ������ ��� ������� ���������� � SELECT (�������� ���������� ��� ��������� ���������)
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ���� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� � SELECT ���������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_vers_status_cd   VARCHAR2                      DEFAULT 'ACT', -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������; �������� �� ����������� ���� ������ par_rule_seq_id
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ACT'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN CLOB IS -- �������������� SELECT ���������� ����, ���� ��� ��������� ������� �� ��������� ������ ���������, �� null

  var_sql              CLOB;

  var_expr             CLOB;
  var_wt_expr          CLOB;
  var_wt_min_amount    NUMBER;
  var_ct_expr          CLOB;
  var_ct_min_amount    NUMBER;
  var_bot_expr         CLOB;
  var_bot_min_amount   NUMBER;

  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_scnro.get_opok_select_standard)';
BEGIN
  --
  -- �������� ���������� ���������
  --
  if par_query_cd is null or par_query_cd not in ('�����������', '������_������') or
     par_query_type is null or par_query_type not in ('VIEW', 'RUN', 'CHECK', 'FIND') Then
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);    
  end if;
  --
  -- ����������� ������ ��� view ��������� (par_query_type = 'VIEW') �������� ������� ��� ���� ��������� ����� � ����� ���:
  -- SELECT 'WT',
  --         <ID ��������>,
  --         CASE WHEN <������� �� ������� ��������� ���� 1 � ����������� 1>
  --              THEN <��� ���� 1>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --              WHEN <������� �� ������� ��������� ���� 1 � ����������� 2>
  --              THEN <��� ���� 1>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --                ...
  --         END||'|'
  --         CASE WHEN <������� �� ������� ��������� ���� 2 � ����������� 1>
  --              THEN <��� ���� 2>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --              WHEN <������� �� ������� ��������� ���� 2 � ����������� 2>
  --              THEN <��� ���� 2>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --                ...
  --         END||'|'
  --         ...
  --        /* �� ���������� ����� ��� ������ ������ ����������� - trim | � replace || �� | */,
  --        <���� ���������� �������� � ��. �������> - ��. ��� RF_TYPE_OPOK
  --   FROM WIRE_TRXN wt
  --        LEFT JOIN CUST benef
  --        LEFT JOIN CUST orig
  --  WHERE TRXN_BASE_AM >= <��������� �����>
  --  UNION ALL
  --  <����������� ������ ��� �������� �������� �� CASH_TRXN>
  --  UNION ALL
  --  <����������� ������ ��� ���������� �������� �� BACK_OFFICE_TRXN>
  --
  -- ������ ��� �������� ���������� (par_query_type = 'CHECK') �������� � ������ WHERE ������� 1 <> 1 � ������� "�������" ��������
  -- SELECT count(*) FROM (<������� ������>)
  --
  -- ������ ��� ��������� ��������� (par_query_type = 'FIND') �������� � ������ WHERE �������������� �������:
  --  - �� ���� �������� (��������� � �������� get_start_date, get_end_date)
  --  - �� ��� ���������������� ����� (��������� � ������� get_tb_id), ���� null, �� ��� ��
  -- ����� ������ ��� ��������� ��������� �������� �� ��������� � ������� �������� �������������� �������, ����������� �������� (��. ��� RF_TYPE_OPOK)
  --
  -- ������ ��������� ������ ����� �������� �������������� ������� (RF_WIRE_TRXN_SCRTY/RF_CASH_TRXN_SCRTY), �� ������� ������������� ����������� ��
  -- ����� � �� ���� ������ ��������������� ������� �������� WIRE_TRXN/CASH_TRXN, 
  -- � ����� �������������� ������� - �������, ��� � �������� ����� ����� ������ ������
  --
  -- ���� �� �������� ��������� � ������������ � �����������, � ������� ����������� � ����������� - ����������� ��������� ��� select-list, ��� ������ ��������� �������� - ��������
  -- (���� �� listagg ���� �������� � CLOB - ����� ���� �� ��� ����������� � ������� ������ �������, �� �������� ������������ VARCHAR2;
  --  �������������� � xmlagg �� �������)
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/
                   to_clob('')||
                   ' WHEN '||trim(substr(expr, 1, length(expr) - length(' and ')))||
                            ' THEN '''||to_char(opok_nb)||'~'||to_char(rule_seq_id)||'~'||to_char(record_seq_id)||'~''||'||explanation_sql_tx as expr,
                   op_cat_cd,
                   /* ���� ������ ���������� ����� CASE WHEN */
                   case when row_number() over(partition by op_cat_cd, group_nb order by priority_nb, rule_seq_id, start_dt nulls first) = 1
                        then 1
                        else 0
                   end as first_expr_fl,
                   /* ���� ��������� ���������� ����� CASE WHEN */
                   case when op_cat_cd <> nvl(lead(op_cat_cd) over(order by op_cat_cd, group_nb, priority_nb, rule_seq_id, start_dt nulls first), '?') or
                             group_nb  <> nvl(lead(group_nb)  over(order by op_cat_cd, group_nb, priority_nb, rule_seq_id, start_dt nulls first), -1)
                        then 1
                        else 0
                   end as last_expr_fl,
                   min_amount
              FROM (SELECT to_clob('')||
                           /* ������� �� ����� �������� */
                           case when rl.limit_am > min(rl.limit_am) over(partition by rl.op_cat_cd)
                                then case when par_query_cd = '�����������'
                                          then rl.trxn_am_col||' >= '||to_char(rl.limit_am)||' and '
                                          when par_query_cd = '������_������' and rl.op_cat_cd in ('WT', 'CT')
                                          then decode(rl.op_cat_cd, 'WT', 'wts.scrty_base_am', 'CT', 'cts.scrty_base_am')||' >= '||to_char(rl.limit_am)||' and '
                                     end          
                           end ||
                           rl.trxn_dt_sql_tx||nvl2(rl.trxn_dt_sql_tx, ' and ', '')||
                           rl.optp_sql_tx||nvl2(rl.optp_sql_tx, ' and ', '')||
                           rl.src_sql_tx||nvl2(rl.src_sql_tx, ' and ', '')||
                           rl.dbt_cdt_sql_tx||nvl2(rl.dbt_cdt_sql_tx, ' and ', '')||
                           rl.acct_sql_tx||nvl2(rl.acct_sql_tx, ' and ', '')||
                           rl.lex_sql_tx||nvl2(rl.lex_sql_tx, ' and ', '')||
                           rl.lex2_sql_tx||nvl2(rl.lex2_sql_tx, ' and ', '')||
                           rl.wlist_sql_tx||nvl2(rl.wlist_sql_tx, ' and ', '')||
                           rl.custom_sql_tx||nvl2(rl.custom_sql_tx, ' and ', '') as expr,
                           rl.op_cat_cd,
                           nvl(rl.group_nb, rl.opok_nb) as group_nb,
                           rl.priority_nb,
                           rl.rule_seq_id,
                           rl.record_seq_id,
                           rl.start_dt,
                           rl.opok_nb,
                           rl.explanation_sql_tx,
                           min(rl.limit_am) over(partition by rl.op_cat_cd) as min_amount
                      FROM mantas.rfv_opok_rule_sql rl
                     WHERE rl.query_cd = par_query_cd and
                           rl.rule_seq_id = nvl(par_rule_seq_id, rl.rule_seq_id) and
                           (par_opok_nb is null or rl.opok_nb = par_opok_nb or rl.group_nb = par_opok_nb) and
                           rl.crit_status_cd = par_crit_status_cd and
                           (par_rule_seq_id is not null or (par_vers_status_cd = 'ACT' and rl.act_dup_number = 1) or (par_vers_status_cd = 'TEST' and rl.test_dup_number = 1)) and
                           ((par_crit_status_cd = 'ACT' and err_fl = 'N') or (par_crit_status_cd = 'TEST' and test_err_fl = 'N') or par_query_type = 'CHECK'))
             WHERE trim(expr) is not null
            ORDER BY op_cat_cd, group_nb, priority_nb, rule_seq_id, start_dt nulls first) LOOP

    var_expr := case when r.first_expr_fl = 1 then 'CASE' end||r.expr||case when r.last_expr_fl = 1 then ' END' end;
    --
    -- ������� ��������� ������� WHEN ... THEN ... � ��������������� ������������� ����������
    --
    if r.op_cat_cd = 'WT' Then
      var_wt_expr := var_wt_expr||case when r.first_expr_fl = 1 and var_wt_expr is not null then '||''|''||' end||var_expr;
      var_wt_min_amount := r.min_amount;
    elsif r.op_cat_cd = 'CT' Then
      var_ct_expr := var_ct_expr||case when r.first_expr_fl = 1 and var_ct_expr is not null then '||''|''||' end||var_expr;
      var_ct_min_amount := r.min_amount;
    elsif r.op_cat_cd = 'BOT' Then
      var_bot_expr := var_bot_expr||case when r.first_expr_fl = 1 and var_bot_expr is not null then '||''|''||' end||var_expr;
      var_bot_min_amount := r.min_amount;
    end if;
  END LOOP;

  if var_wt_expr is null and var_ct_expr is null and var_bot_expr is null Then
    return null;
  end if;
  --
  -- ���������� �������� SELECT
  --
  var_sql := substr(
             to_clob('')||
             case when var_wt_expr is not null
                  then to_clob('')||' UNION ALL '||
                       'SELECT '||case when par_query_type = 'CHECK' 
                                       then '/*+ NO_PARALLEL*/ '
                                       when par_query_type = 'FIND' and par_query_cd <> '������_������'  
                                       then '/*+ PARALLEL (wt 64) INDEX(wt)*/ '
                                       when par_query_type = 'VIEW' and par_query_cd <> '������_������'
                                       then '/*+ PARALLEL (wt 64)*/ '
                                       when par_query_type = 'VIEW' and par_query_cd = '������_������'
                                       then '/*+ LEADING(wts)*/ ' -- ������������ ��� ��������� ��������� ��������
                                       when par_query_type = 'RUN' and par_query_cd <> '������_������'  
                                       then '/*+ PARALLEL (wt 64) LEADING(wt) USE_NL(orig) INDEX(orig RF_CUST_SEQ_AK1) USE_NL(benef) INDEX(benef RF_CUST_SEQ_AK1)*/ '
                                       when par_query_type = 'RUN' and par_query_cd = '������_������'  
                                       then '/*+ USE_NL(orig) INDEX(orig RF_CUST_SEQ_AK1) USE_NL(benef) INDEX(benef RF_CUST_SEQ_AK1)*/ '
                                  end||
                              '''WT'' as op_cat_cd, wt.rf_partition_date, wt.rf_subpartition_key, wt.rf_opok_status, '||
                               case when par_query_cd = '������_������' then 'wts.fo_trxn_seq_id' else 'wt.fo_trxn_seq_id' end||' as trxn_seq_id, '||
                               case when par_query_cd = '������_������' then 'wts.trxn_scrty_seq_id' else 'to_number(null)' end||' as trxn_scrty_seq_id, '||
                               'regexp_replace(trim(''|'' from '||var_wt_expr||'), ''\|{2,}'', ''|'') as opok_list, wt.trxn_exctn_dt as trxn_dt, '||
                               'wt.data_dump_dt as data_dump_dt, wt.rf_branch_id as branch_id '||
                               case when par_query_cd = '������_������' and par_query_type in ('VIEW', 'RUN', 'CHECK')
                                    then ', case when count(wts.trxn_scrty_seq_id) over(partition by wts.fo_trxn_seq_id) > 1 or '||
                                                     'wts.scrty_base_am <> wt.trxn_base_am '||
                                                'then 1 '||
                                                'else 0 '||
                                           'end as mult_scrty_flag '
                               end||     
                               case par_query_type
                                 when 'FIND'
                                 then ', wt.rf_trxn_crncy_am as trxn_crncy_am, wt.trxn_base_am as trxn_base_am, wt.rf_trxn_crncy_cd as trxn_crncy_cd, wt.orig_to_benef_instr_tx as trxn_desc, wt.trxn_type1_cd as nsi_op_id, '||
                                        case when par_query_cd = '������_������' 
                                             then 'wts.scrty_base_am as scrty_base_am, nvl(wts.scrty_type_cd, s.prod_type_cd) as scrty_type_cd, coalesce(wts.scrty_id, s.alt_scrty_intrl_id, wts.scrty_nb, s.cstm_6_tx) as scrty_id, '
                                             else 'to_number(null) as scrty_base_am, to_char(null) as scrty_type_cd, to_char(null) as scrty_id, '
                                        end||     
                                        'nvl(wt.orig_nm, orig.full_nm)||nvl2(wt.rf_orig_add_info_tx, '' (''||wt.rf_orig_add_info_tx||'')'', null) as orig_nm, nvl(wt.rf_orig_inn_nb, orig.tax_id) as orig_inn_nb, wt.rf_orig_acct_nb as orig_acct_nb, case when wt.rf_orig_acct_seq_id <> -1 then ''Y'' end as orig_own_fl, wt.rf_debit_cd as debit_cd, '||
                                        'wt.send_instn_nm as send_instn_nm, wt.send_instn_id as send_instn_id, wt.send_instn_acct_id as send_instn_acct_id, '||
                                        'nvl(wt.benef_nm, benef.full_nm)||nvl2(wt.rf_benef_add_info_tx, '' (''||wt.rf_benef_add_info_tx||'')'', null) as benef_nm, nvl(wt.rf_benef_inn_nb, benef.tax_id) as benef_inn_nb, wt.rf_benef_acct_nb as benef_acct_nb, case when wt.rf_benef_acct_seq_id <> -1 then ''Y'' end as benef_own_fl, wt.rf_credit_cd as credit_cd, '||
                                        'wt.rcv_instn_nm as rcv_instn_nm, wt.rcv_instn_id as rcv_instn_id, wt.rcv_instn_acct_id as rcv_instn_acct_id, '||
                                        'trunc(wt.rf_branch_id/100000) as tb_id, wt.rf_branch_id - round(wt.rf_branch_id, -5) as osb_id, '||
                                        'wt.src_sys_cd, wt.rf_src_change_dt as src_change_dt, wt.rf_src_user_nm as src_user_nm, wt.rf_src_cd as src_cd '
                               end||
                         'FROM '||case when par_query_cd = '������_������' then 'BUSINESS.RF_WIRE_TRXN_SCRTY wts JOIN ' end||
                              'BUSINESS.WIRE_TRXN wt '||case when par_query_cd = '������_������' then 'ON wt.fo_trxn_seq_id = wts.fo_trxn_seq_id ' end||
                              'LEFT JOIN BUSINESS.CUST orig ON orig.cust_seq_id = wt.rf_orig_cust_seq_id '||
                              'LEFT JOIN BUSINESS.CUST benef ON benef.cust_seq_id = wt.rf_benef_cust_seq_id '||
                              case when par_query_cd = '������_������' and par_query_type = 'FIND'
                                   then 'LEFT JOIN BUSINESS.SCRTY s ON s.scrty_seq_id = wts.scrty_seq_id ' 
                              end||
                        'WHERE '||case when par_query_cd = '������_������' then 'wts.scrty_base_am' else 'wt.rf_trxn_opok_am' end||' >= '||to_char(var_wt_min_amount)||
                         ' and nvl(wt.rf_canceled_fl, ''N'') <> ''Y'''||
                         case when par_query_cd = '������_������' then ' and nvl(wts.canceled_fl, ''N'') <> ''Y''' end||
                         case par_query_type
                           when 'CHECK' then ' and 1 <> 1'
                           when 'FIND'  then ' and '||case when par_query_cd = '������_������' then 'wts.trxn_exctn_dt' else 'wt.trxn_exctn_dt' end||' between mantas.rf_pkg_scnro.get_start_date and mantas.rf_pkg_scnro.get_end_date + 1 - 1/24/3600'||
                                             case when par_query_cd = '������_������' then '' else ' and wt.rf_partition_date between mantas.rf_pkg_scnro.get_start_date and to_date(''01.01.9999'', ''dd.mm.yyyy'')' end||
                                             case when par_query_cd = '������_������' 
                                                  then ' and wts.tb_id between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99, mantas.rf_pkg_scnro.get_tb_id)'
                                                  else ' and wt.rf_subpartition_key between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id*1000) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99999, mantas.rf_pkg_scnro.get_tb_id*1000 + 999)'
                                             end
                           -- ���������� �� view ����������� �� ���������� �������� - view ������������ ������ ������ ��� (����������) ��������� ��������� ��������
                           when 'VIEW' then ' and ''WT'' = mantas.rf_pkg_scnro.get_op_cat_cd'||
                                            ' and '||case when par_query_cd = '������_������' then 'wts.' else 'wt.' end||'fo_trxn_seq_id = mantas.rf_pkg_scnro.get_trxn_seq_id'
                         end
             end||
             case when var_ct_expr is not null
                  then to_clob('')||' UNION ALL '||
                       'SELECT '||case when par_query_type = 'CHECK' 
                                       then '/*+ NO_PARALLEL*/ '
                                       when par_query_type = 'FIND' and par_query_cd <> '������_������' 
                                       then '/*+ PARALLEL (ct 64) INDEX(ct)*/ '
                                       when par_query_type = 'VIEW' and par_query_cd <> '������_������'
                                       then '/*+ PARALLEL (ct 64)*/ '
                                       when par_query_type = 'VIEW' and par_query_cd = '������_������'
                                       then '/*+ LEADING(cts)*/ ' -- ������������ ��� ��������� ��������� ��������
                                       when par_query_type = 'RUN' and par_query_cd <> '������_������'  
                                       then '/*+ PARALLEL (ct 64) LEADING(ct) USE_NL(cust) INDEX(cust RF_CUST_SEQ_AK1)*/ '
                                       when par_query_type = 'RUN' and par_query_cd = '������_������'  
                                       then '/*+ USE_NL(cust) INDEX(cust RF_CUST_SEQ_AK1)*/ '
                                  end||
                              '''CT'' as op_cat_cd, ct.rf_partition_date, ct.rf_subpartition_key, ct.rf_opok_status, '||
                               case when par_query_cd = '������_������' then 'cts.fo_trxn_seq_id' else 'ct.fo_trxn_seq_id' end||' as trxn_seq_id, '||
                               case when par_query_cd = '������_������' then 'cts.trxn_scrty_seq_id' else 'to_number(null)' end||' as trxn_scrty_seq_id, '||
                               'regexp_replace(trim(''|'' from '||var_ct_expr||'), ''\|{2,}'', ''|'') as opok_list, ct.trxn_exctn_dt as trxn_dt, '||
                               'ct.data_dump_dt as data_dump_dt, ct.rf_branch_id as branch_id '||
                               case when par_query_cd = '������_������' and par_query_type in ('VIEW', 'RUN', 'CHECK')
                                    then ', case when count(cts.trxn_scrty_seq_id) over(partition by cts.fo_trxn_seq_id) > 1 or '||
                                                     'cts.scrty_base_am <> ct.trxn_base_am '||
                                                'then 1 '||
                                                'else 0 '||
                                           'end as mult_scrty_flag '
                               end||     
                               case par_query_type
                                 when 'FIND'
                                 then ', ct.rf_trxn_acct_am as trxn_crncy_am, ct.trxn_base_am as trxn_base_am, ct.rf_acct_crncy_cd as trxn_crncy_cd, ct.desc_tx as trxn_desc, ct.trxn_type1_cd as nsi_op_id, '||
                                        case when par_query_cd = '������_������' 
                                             then 'cts.scrty_base_am as scrty_base_am, nvl(cts.scrty_type_cd, s.prod_type_cd) as scrty_type_cd, coalesce(cts.scrty_id, s.alt_scrty_intrl_id, cts.scrty_nb, s.cstm_6_tx) as scrty_id, '
                                             else 'to_number(null) as scrty_base_am, to_char(null) as scrty_type_cd, to_char(null) as scrty_id, '
                                        end||     
                                        'case when ct.dbt_cdt_cd = ''D'' then nvl(ct.rf_cust_nm, cust.full_nm) end as orig_nm, '||
                                        'case when ct.dbt_cdt_cd = ''D'' then nvl(ct.rf_cust_inn_nb, cust.tax_id) end as orig_inn_nb, '||
                                        'case when ct.dbt_cdt_cd = ''D'' then ct.rf_acct_nb end as orig_acct_nb, '||
                                        'case when ct.dbt_cdt_cd = ''D'' then case when ct.rf_acct_seq_id <> -1 then ''Y'' end end as orig_own_fl, '||
                                        'ct.rf_debit_cd as debit_cd, to_char(null) as send_instn_nm, to_char(null) as send_instn_id, to_char(null) as send_instn_acct_id, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then nvl(ct.rf_cust_nm, cust.full_nm) end as benef_nm, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then nvl(ct.rf_cust_inn_nb, cust.tax_id) end as benef_inn_nb, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then ct.rf_acct_nb end as benef_acct_nb, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then case when ct.rf_acct_seq_id <> -1 then ''Y'' end end as benef_own_fl, '||
                                        'ct.rf_credit_cd as credit_cd, to_char(null) as rcv_instn_nm, to_char(null) as rcv_instn_id, to_char(null) as rcv_instn_acct_id, '||
                                        'trunc(ct.rf_branch_id/100000) as tb_id, ct.rf_branch_id - round(ct.rf_branch_id, -5) as osb_id, '||
                                        'ct.src_sys_cd, ct.rf_src_change_dt as src_change_dt, ct.rf_src_user_nm as src_user_nm, ct.rf_src_cd as src_cd '
                               end||
                         'FROM '||case when par_query_cd = '������_������' then 'BUSINESS.RF_CASH_TRXN_SCRTY cts JOIN ' end||
                              'BUSINESS.CASH_TRXN ct '||case when par_query_cd = '������_������' then 'ON ct.fo_trxn_seq_id = cts.fo_trxn_seq_id ' end||
                              'LEFT JOIN BUSINESS.CUST cust ON cust.cust_seq_id = ct.rf_cust_seq_id '||
                              case when par_query_cd = '������_������' and par_query_type = 'FIND'
                                   then 'LEFT JOIN BUSINESS.SCRTY s ON s.scrty_seq_id = cts.scrty_seq_id ' 
                              end||
                        'WHERE '||case when par_query_cd = '������_������' then 'cts.scrty_base_am' else 'ct.rf_trxn_opok_am' end||' >= '||to_char(var_ct_min_amount)||
                         ' and nvl(ct.rf_canceled_fl, ''N'') <> ''Y'''||
                         case when par_query_cd = '������_������' then ' and nvl(cts.canceled_fl, ''N'') <> ''Y''' end||
                         case par_query_type
                           when 'CHECK' then ' and 1 <> 1'
                           when 'FIND'  then ' and '||case when par_query_cd = '������_������' then 'cts.trxn_exctn_dt' else 'ct.trxn_exctn_dt' end||' between mantas.rf_pkg_scnro.get_start_date and mantas.rf_pkg_scnro.get_end_date + 1 - 1/24/3600'||
                                             case when par_query_cd = '������_������' then '' else ' and ct.rf_partition_date between mantas.rf_pkg_scnro.get_start_date and to_date(''01.01.9999'', ''dd.mm.yyyy'')' end||
                                             case when par_query_cd = '������_������' 
                                                  then ' and cts.tb_id between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99, mantas.rf_pkg_scnro.get_tb_id)'
                                                  else ' and ct.rf_subpartition_key between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id*1000) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99999, mantas.rf_pkg_scnro.get_tb_id*1000 + 999)'
                                             end
                           -- ���������� �� view ����������� �� ���������� �������� - view ������������ ������ ������ ��� (����������) ��������� ��������� ��������
                           when 'VIEW' then ' and ''CT'' = mantas.rf_pkg_scnro.get_op_cat_cd'||
                                            ' and '||case when par_query_cd = '������_������' then 'cts.' else 'ct.' end||'fo_trxn_seq_id = mantas.rf_pkg_scnro.get_trxn_seq_id'
                         end
             end||
             case when var_bot_expr is not null
                  then to_clob('')||' UNION ALL '||
                       'SELECT '||case when par_query_type = 'CHECK' 
                                       then '/*+ NO_PARALLEL*/ '
                                       when par_query_type = 'FIND'  
                                       then '/*+ PARALLEL (bot 64) INDEX(bot)*/ '
                                       when par_query_type = 'VIEW' 
                                       then '/*+ PARALLEL (bot 64)*/ '
                                       when par_query_type = 'RUN'   
                                       then '/*+ PARALLEL (bot 64) LEADING(bot) USE_NL(acct) INDEX(acct RF_ACCT_SEQ_AK1) USE_NL(cust) INDEX(cust RF_CUST_SEQ_AK1)*/ '
                                  end||
                              '''BOT'' as op_cat_cd, bot.rf_partition_date, bot.rf_subpartition_key, bot.rf_opok_status, bot.bo_trxn_seq_id as trxn_seq_id, to_number(null) as trxn_scrty_seq_id, '||
                               'regexp_replace(trim(''|'' from '||var_bot_expr||'), ''\|{2,}'', ''|'') as opok_list, bot.exctn_dt as trxn_dt, '||
                               'bot.data_dump_dt as data_dump_dt, bot.rf_branch_id as branch_id '||
                               case when par_query_cd = '������_������' and par_query_type in ('VIEW', 'RUN', 'CHECK')
                                    then ', 0 as mult_scrty_flag '
                               end||     
                               case par_query_type
                                 when 'FIND'
                                 then ', bot.trxn_rptng_am as trxn_crncy_am, bot.trxn_base_am as trxn_base_am, bot.rptng_crncy_cd as trxn_crncy_cd, bot.dscr_tx as trxn_desc, bot.trxn_type1_cd as nsi_op_id, '||
                                        'to_number(null) as scrty_base_am, to_char(null) as scrty_type_cd, to_char(null) as scrty_id, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then cust.full_nm end as orig_nm, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then cust.tax_id end as orig_inn_nb, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then acct.alt_acct_id end as orig_acct_nb, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then ''Y'' end as orig_own_fl, '||
                                        'bot.rf_debit_cd as debit_cd, to_char(null) as send_instn_nm, to_char(null) as send_instn_id, to_char(null) as send_instn_acct_id, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then cust.full_nm end as benef_nm, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then cust.tax_id end as benef_inn_nb, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then acct.alt_acct_id end as benef_acct_nb, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then ''Y'' end as benef_own_fl, '||
                                        'bot.rf_credit_cd as credit_cd, to_char(null) as rcv_instn_nm, to_char(null) as rcv_instn_id, to_char(null) as rcv_instn_acct_id, '||
                                        'trunc(bot.rf_branch_id/100000) as tb_id, bot.rf_branch_id - round(bot.rf_branch_id, -5) as osb_id, '||
                                        'bot.src_sys_cd, bot.rf_src_change_dt as src_change_dt, bot.rf_src_user_nm as src_user_nm, bot.rf_src_cd as src_cd '
                               end||
                         'FROM BUSINESS.BACK_OFFICE_TRXN bot '||
                              'LEFT JOIN BUSINESS.ACCT acct ON acct.acct_seq_id = to_number(bot.acct_intrl_id) '||
                              'LEFT JOIN BUSINESS.CUST cust ON cust.cust_seq_id = to_number(acct.prmry_cust_intrl_id) '||
                        'WHERE bot.trxn_base_am >= '||to_char(var_bot_min_amount)||
                         ' and nvl(bot.rf_canceled_fl, ''N'') <> ''Y'''||
                         case par_query_type
                           when 'CHECK' then ' and 1 <> 1'
                           when 'FIND'  then ' and bot.exctn_dt between mantas.rf_pkg_scnro.get_start_date and mantas.rf_pkg_scnro.get_end_date + 1 - 1/24/3600'||
                                             ' and bot.rf_partition_date between mantas.rf_pkg_scnro.get_start_date and to_date(''01.01.9999'', ''dd.mm.yyyy'')'||
                                             ' and bot.rf_subpartition_key between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id*1000) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99999, mantas.rf_pkg_scnro.get_tb_id*1000 + 999)'
                           -- ���������� �� view ����������� �� ���������� �������� - view ������������ ������ ������ ��� (����������) ��������� ��������� ��������
                           when 'VIEW' then ' and ''BOT'' = mantas.rf_pkg_scnro.get_op_cat_cd'||
                                            ' and bot.bo_trxn_seq_id = mantas.rf_pkg_scnro.get_trxn_seq_id'
                         end
             end,
             length(' UNION ALL ') + 1);

  if par_query_type = 'CHECK' and var_sql is not null Then
    var_sql := 'SELECT count(*) FROM ('||var_sql||')';
  elsif par_query_type = 'FIND' and var_sql is not null Then
    var_sql := 'SELECT * FROM ('||var_sql||') WHERE opok_list is not null';
  end if;

  return var_sql;
END get_opok_select_standard;
--
-- ������������ SELECT ���������� ���� (��� view ���������, ��� �������� ����������, ��� ��������� ���������) 
-- ��� ������� ��������� ������ �������� �� ����� (��� ������� = '������_��������')
--
FUNCTION get_opok_select_firstrxn(
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'VIEW' - ��� view ���������, 'RUN' - ��� ���������� ���������, ��������� ������� (����� ��������), 'CHECK' - ��� �������� ���������� ������ ���������, 'FIND' - ��� ��������� ��������� ����
  par_rule_seq_id      rf_opok_rule.rule_seq_id%TYPE DEFAULT null,  -- ������������� ���������� ������� ��������� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� ������ ��� ������� ���������� � SELECT (�������� ���������� ��� ��������� ���������)
  par_opok_nb          rf_opok.opok_nb%TYPE          DEFAULT null,  -- ��� ����/��������� ��� ���� - ��� ������� 'CHECK' � 'FIND', ���� ������, �� � SELECT ���������� ������ �������, ����������� � ��������� ����� ����/���� ���� ������
  par_vers_status_cd   VARCHAR2                      DEFAULT 'ACT', -- �������: 'ACT' - ��������� ����������� ������ ������, 'TEST' - ��������� �������� ������ ������ (������� ��������� �� ������ ������) � ������ ��� �� ���������� - �����������; �������� �� ����������� ���� ������ par_rule_seq_id
  par_crit_status_cd   VARCHAR2                      DEFAULT 'ACT'  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN CLOB IS -- �������������� SELECT ���������� ����, ���� ��� ��������� ������� �� ��������� ������ ���������, �� null

  var_sql              CLOB;

  var_expr             CLOB;
  var_wt_expr          CLOB;
  var_wt_min_amount    NUMBER;
  var_ct_expr          CLOB;
  var_ct_min_amount    NUMBER;
  var_bot_expr         CLOB;
  var_bot_min_amount   NUMBER;

  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_scnro.get_opok_select_firstrxn)';
BEGIN
  --
  -- �������� ���������� ���������
  --
  if par_query_type is null or par_query_type not in ('VIEW', 'RUN', 'CHECK', 'FIND') Then
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);    
  end if;
  --
  -- ������ ��� view ��������� (par_query_type = 'VIEW') �������� ������� ��� ���� ��������� ����� � ����� ���:
  -- SELECT 'WT',
  --         <ID ��������>,
  --         CASE WHEN <������� �� ������� ��������� ���� 1 � ����������� 1>
  --              THEN <��� ���� 1>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --              WHEN <������� �� ������� ��������� ���� 1 � ����������� 2>
  --              THEN <��� ���� 1>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --                ...
  --         END||'|'
  --         CASE WHEN <������� �� ������� ��������� ���� 2 � ����������� 1>
  --              THEN <��� ���� 2>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --              WHEN <������� �� ������� ��������� ���� 2 � ����������� 2>
  --              THEN <��� ���� 2>||'~'||<ID �������>||'~'||<ID ������ ������� ����>||'~'||<��������� � ��������� (�����)>
  --                ...
  --         END||'|'
  --         ...
  --        /* �� ���������� ����� ��� ������ ������ ����������� - trim | � replace || �� | */,
  --        <���� ���������� �������� � ��. �������> - ��. ��� RF_TYPE_OPOK
  --   FROM ACCT ac
  --        JOIN WIRE_TRXN wt ON (���� �������� = ���� ������ �������� �� �����, ���� ����������� ��� ���� ���������� = ����)
  --        LEFT JOIN CUST benef
  --        LEFT JOIN CUST orig
  --  WHERE ac.rf_first_actvy_dt >= <���� � �������, �� ������� �� �������� ������ ������ ��������>
--          TRXN_BASE_AM >= <��������� �����>
  --  UNION ALL
  --  <����������� ������ ��� �������� �������� �� CASH_TRXN>
  --  UNION ALL
  --  <����������� ������ ��� ���������� �������� �� BACK_OFFICE_TRXN>
  --
  -- ������ ��� �������� ���������� (par_query_type = 'CHECK') �������� � ������ WHERE ������� 1 <> 1 � ������� "�������" ��������
  -- SELECT count(*) FROM (<������� ������>)
  --
  -- ������ ��� ��������� ��������� (par_query_type = 'FIND') �������� � ������ WHERE �������������� �������:
  --  - �� ���� �������� (��������� � �������� get_start_date, get_end_date)
  --  - �� ��� ���������������� ����� (��������� � ������� get_tb_id), ���� null, �� ��� ��
  -- ����� ������ ��� ��������� ��������� �������� �� ��������� � ������� �������� �������������� �������, ����������� �������� (��. ��� RF_TYPE_OPOK)
  --
  -- ���� �� �������� ��������� � ������������ � �����������, � ������� ����������� � ����������� - ����������� ��������� ��� select-list, ��� ������ ��������� �������� - ��������
  -- (���� �� listagg ���� �������� � CLOB - ����� ���� �� ��� ����������� � ������� ������ �������, �� �������� ������������ VARCHAR2;
  --  �������������� � xmlagg �� �������)
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/
                   to_clob('')||
                   ' WHEN '||trim(substr(expr, 1, length(expr) - length(' and ')))||
                            ' THEN '''||to_char(opok_nb)||'~'||to_char(rule_seq_id)||'~'||to_char(record_seq_id)||'~''||'||explanation_sql_tx as expr,
                   op_cat_cd,
                   /* ���� ������ ���������� ����� CASE WHEN */
                   case when row_number() over(partition by op_cat_cd, group_nb order by priority_nb, rule_seq_id, start_dt nulls first) = 1
                        then 1
                        else 0
                   end as first_expr_fl,
                   /* ���� ��������� ���������� ����� CASE WHEN */
                   case when op_cat_cd <> nvl(lead(op_cat_cd) over(order by op_cat_cd, group_nb, priority_nb, rule_seq_id, start_dt nulls first), '?') or
                             group_nb  <> nvl(lead(group_nb)  over(order by op_cat_cd, group_nb, priority_nb, rule_seq_id, start_dt nulls first), -1)
                        then 1
                        else 0
                   end as last_expr_fl,
                   min_amount
              FROM (SELECT to_clob('')||
                           /* ������� �� ����� �������� */
                           case when rl.limit_am > min(rl.limit_am) over(partition by rl.op_cat_cd)
                                then rl.trxn_am_col||' >= '||to_char(rl.limit_am)||' and '
                           end ||
                           rl.trxn_dt_sql_tx||nvl2(rl.trxn_dt_sql_tx, ' and ', '')||
                           rl.optp_sql_tx||nvl2(rl.optp_sql_tx, ' and ', '')||
                           rl.src_sql_tx||nvl2(rl.src_sql_tx, ' and ', '')||
                           rl.dbt_cdt_sql_tx||nvl2(rl.dbt_cdt_sql_tx, ' and ', '')||
                           rl.acct_sql_tx||nvl2(rl.acct_sql_tx, ' and ', '')||
                           rl.lex_sql_tx||nvl2(rl.lex_sql_tx, ' and ', '')||
                           rl.lex2_sql_tx||nvl2(rl.lex2_sql_tx, ' and ', '')||
                           rl.wlist_sql_tx||nvl2(rl.wlist_sql_tx, ' and ', '')||
                           rl.custom_sql_tx||nvl2(rl.custom_sql_tx, ' and ', '') as expr,
                           rl.op_cat_cd,
                           nvl(rl.group_nb, rl.opok_nb) as group_nb,
                           rl.priority_nb,
                           rl.rule_seq_id,
                           rl.record_seq_id,
                           rl.start_dt,
                           rl.opok_nb,
                           rl.explanation_sql_tx,
                           min(rl.limit_am) over(partition by rl.op_cat_cd) as min_amount
                      FROM mantas.rfv_opok_rule_sql rl
                     WHERE rl.query_cd = '������_��������' and
                           rl.rule_seq_id = nvl(par_rule_seq_id, rl.rule_seq_id) and
                           (par_opok_nb is null or rl.opok_nb = par_opok_nb or rl.group_nb = par_opok_nb) and
                           rl.crit_status_cd = par_crit_status_cd and
                           (par_rule_seq_id is not null or (par_vers_status_cd = 'ACT' and rl.act_dup_number = 1) or (par_vers_status_cd = 'TEST' and rl.test_dup_number = 1)) and
                           ((par_crit_status_cd = 'ACT' and err_fl = 'N') or (par_crit_status_cd = 'TEST' and test_err_fl = 'N') or par_query_type = 'CHECK'))
             WHERE trim(expr) is not null
            ORDER BY op_cat_cd, group_nb, priority_nb, rule_seq_id, start_dt nulls first) LOOP

    var_expr := case when r.first_expr_fl = 1 then 'CASE' end||r.expr||case when r.last_expr_fl = 1 then ' END' end;
    --
    -- ������� ��������� ������� WHEN ... THEN ... � ��������������� ������������� ����������
    --
    if r.op_cat_cd = 'WT' Then
      var_wt_expr := var_wt_expr||case when r.first_expr_fl = 1 and var_wt_expr is not null then '||''|''||' end||var_expr;
      var_wt_min_amount := r.min_amount;
    elsif r.op_cat_cd = 'CT' Then
      var_ct_expr := var_ct_expr||case when r.first_expr_fl = 1 and var_ct_expr is not null then '||''|''||' end||var_expr;
      var_ct_min_amount := r.min_amount;
    elsif r.op_cat_cd = 'BOT' Then
      var_bot_expr := var_bot_expr||case when r.first_expr_fl = 1 and var_bot_expr is not null then '||''|''||' end||var_expr;
      var_bot_min_amount := r.min_amount;
    end if;
  END LOOP;

  if var_wt_expr is null and var_ct_expr is null and var_bot_expr is null Then
    return null;
  end if;
  --
  -- ���������� �������� SELECT
  --
  var_sql := substr(
             to_clob('')||
             case when var_wt_expr is not null
                  then to_clob('')||' UNION ALL '||
                       'SELECT '||case when par_query_type = 'CHECK' 
                                       then '/*+ NO_PARALLEL*/ '
                                       when par_query_type in ('FIND', 'RUN')
                                       then '/*+ PARALLEL(acct 64) ORDERED INDEX(acct) PUSH_PRED(wt) INDEX(wt.wt1 RF_WIRE_TRXN_ORIG_ACCT_FK_I) INDEX(wt.wt2 RF_WIRE_TRXN_BENEF_ACCT_FK_I)*/ '
                                       when par_query_type = 'VIEW'  
                                       then '/*+ PUSH_PRED(wt) LEADING(wt)*/ ' -- ������������ ��� ��������� ��������� ��������
                                  end||
                             '''WT'' as op_cat_cd, wt.rf_partition_date, wt.rf_subpartition_key, wt.rf_opok_status, wt.fo_trxn_seq_id as trxn_seq_id, to_number(null) as trxn_scrty_seq_id, '||
                               'regexp_replace(trim(''|'' from '||var_wt_expr||'), ''\|{2,}'', ''|'') as opok_list, wt.trxn_exctn_dt as trxn_dt, '||
                               'wt.data_dump_dt as data_dump_dt, wt.rf_branch_id as branch_id '||
                               case when par_query_type in ('VIEW', 'RUN', 'CHECK')
                                    then ', acct.acct_seq_id as acct_seq_id '
                               end||     
                               case par_query_type
                                 when 'FIND'
                                 then ', wt.rf_trxn_crncy_am as trxn_crncy_am, wt.trxn_base_am as trxn_base_am, wt.rf_trxn_crncy_cd as trxn_crncy_cd, wt.orig_to_benef_instr_tx as trxn_desc, wt.trxn_type1_cd as nsi_op_id, '||
                                        'to_number(null) as scrty_base_am, to_char(null) as scrty_type_cd, to_char(null) as scrty_id, '||     
                                        'nvl(wt.orig_nm, orig.full_nm) as orig_nm, nvl(wt.rf_orig_inn_nb, orig.tax_id) as orig_inn_nb, wt.rf_orig_acct_nb as orig_acct_nb, case when wt.rf_orig_acct_seq_id <> -1 then ''Y'' end as orig_own_fl, wt.rf_debit_cd as debit_cd, '||
                                        'wt.send_instn_nm as send_instn_nm, wt.send_instn_id as send_instn_id, wt.send_instn_acct_id as send_instn_acct_id, '||
                                        'nvl(wt.benef_nm, benef.full_nm) as benef_nm, nvl(wt.rf_benef_inn_nb, benef.tax_id) as benef_inn_nb, wt.rf_benef_acct_nb as benef_acct_nb, case when wt.rf_benef_acct_seq_id <> -1 then ''Y'' end as benef_own_fl, wt.rf_credit_cd as credit_cd, '||
                                        'wt.rcv_instn_nm as rcv_instn_nm, wt.rcv_instn_id as rcv_instn_id, wt.rcv_instn_acct_id as rcv_instn_acct_id, '||
                                        'trunc(wt.rf_branch_id/100000) as tb_id, wt.rf_branch_id - round(wt.rf_branch_id, -5) as osb_id, '||
                                        'wt.src_sys_cd, wt.rf_src_change_dt as src_change_dt, wt.rf_src_user_nm as src_user_nm, wt.rf_src_cd as src_cd '
                               end||
                         'FROM BUSINESS.ACCT acct '||
                              'JOIN (select wt1.rf_orig_acct_seq_id as acct_seq_id, wt1.* '||
                                      'from business.wire_trxn wt1 '|| 
                                    'union all '||
                                    'select wt2.rf_benef_acct_seq_id as acct_seq_id, wt2.* '||
                                      'from business.wire_trxn wt2) wt '||
                                'ON wt.acct_seq_id = acct.acct_seq_id and wt.trxn_exctn_dt + 0 = acct.rf_first_actvy_dt '||
                              'LEFT JOIN BUSINESS.CUST orig ON orig.cust_seq_id = wt.rf_orig_cust_seq_id '||
                              'LEFT JOIN BUSINESS.CUST benef ON benef.cust_seq_id = wt.rf_benef_cust_seq_id '||
                        'WHERE case when nvl(acct.src_sys_cd, ''?'') <> ''BCK''
                                    then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date(''31.12.9999'', ''dd.mm.yyyy''))
                               end'||case par_query_type
                                       when 'FIND'  
                                       then ' between mantas.rf_pkg_scnro.get_start_date and mantas.rf_pkg_scnro.get_end_date + 1 - 1/24/3600'
                                       else ' >= trunc(sysdate) - mantas.rf_pkg_scnro.get_last_days_qty'
                                     end||
                         ' and wt.rf_trxn_opok_am >= '||to_char(var_wt_min_amount)||
                         ' and nvl(wt.rf_canceled_fl, ''N'') <> ''Y'''||
                         case par_query_type
                           when 'CHECK' then ' and 1 <> 1'
                           when 'FIND'  then ' and wt.rf_subpartition_key between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id*1000) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99999, mantas.rf_pkg_scnro.get_tb_id*1000 + 999)'
                           -- ���������� �� view ����������� �� ���������� �������� - view ������������ ������ ������ ��� (����������) ��������� ��������� ��������
                           when 'VIEW' then ' and ''WT'' = mantas.rf_pkg_scnro.get_op_cat_cd'||
                                            ' and wt.fo_trxn_seq_id = mantas.rf_pkg_scnro.get_trxn_seq_id'
                         end
             end||
             case when var_ct_expr is not null
                  then to_clob('')||' UNION ALL '||
                       'SELECT '||case when par_query_type = 'CHECK' 
                                       then '/*+ NO_PARALLEL*/ '
                                       when par_query_type in ('FIND', 'RUN') 
                                       then '/*+ PARALLEL(acct 64) ORDERED INDEX(acct) USE_NL(ct) INDEX(ct RF_CASH_TRXN_ACCT_FK_I)*/ '
                                       when par_query_type = 'VIEW'  
                                       then '/*+ LEADING(ct)*/ ' -- ������������ ��� ��������� ��������� ��������
                                  end||
                             '''CT'' as op_cat_cd, ct.rf_partition_date, ct.rf_subpartition_key, ct.rf_opok_status, ct.fo_trxn_seq_id as trxn_seq_id, to_number(null) as trxn_scrty_seq_id, '||
                               'regexp_replace(trim(''|'' from '||var_ct_expr||'), ''\|{2,}'', ''|'') as opok_list, ct.trxn_exctn_dt as trxn_dt, '||
                               'ct.data_dump_dt as data_dump_dt, ct.rf_branch_id as branch_id '||
                               case when par_query_type in ('VIEW', 'RUN', 'CHECK')
                                    then ', acct.acct_seq_id as acct_seq_id '
                               end||     
                               case par_query_type
                                 when 'FIND'
                                 then ', ct.rf_trxn_acct_am as trxn_crncy_am, ct.trxn_base_am as trxn_base_am, ct.rf_acct_crncy_cd as trxn_crncy_cd, ct.desc_tx as trxn_desc, ct.trxn_type1_cd as nsi_op_id, '||
                                        'to_number(null) as scrty_base_am, to_char(null) as scrty_type_cd, to_char(null) as scrty_id, '||
                                        'case when ct.dbt_cdt_cd = ''D'' then nvl(ct.rf_cust_nm, cust.full_nm) end as orig_nm, '||
                                        'case when ct.dbt_cdt_cd = ''D'' then nvl(ct.rf_cust_inn_nb, cust.tax_id) end as orig_inn_nb, '||
                                        'case when ct.dbt_cdt_cd = ''D'' then ct.rf_acct_nb end as orig_acct_nb, '||
                                        'case when ct.dbt_cdt_cd = ''D'' then case when ct.rf_acct_seq_id <> -1 then ''Y'' end end as orig_own_fl, '||
                                        'ct.rf_debit_cd as debit_cd, to_char(null) as send_instn_nm, to_char(null) as send_instn_id, to_char(null) as send_instn_acct_id, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then nvl(ct.rf_cust_nm, cust.full_nm) end as benef_nm, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then nvl(ct.rf_cust_inn_nb, cust.tax_id) end as benef_inn_nb, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then ct.rf_acct_nb end as benef_acct_nb, '||
                                        'case when ct.dbt_cdt_cd = ''C'' then case when ct.rf_acct_seq_id <> -1 then ''Y'' end end as benef_own_fl, '||
                                        'ct.rf_credit_cd as credit_cd, to_char(null) as rcv_instn_nm, to_char(null) as rcv_instn_id, to_char(null) as rcv_instn_acct_id, '||
                                        'trunc(ct.rf_branch_id/100000) as tb_id, ct.rf_branch_id - round(ct.rf_branch_id, -5) as osb_id, '||
                                        'ct.src_sys_cd, ct.rf_src_change_dt as src_change_dt, ct.rf_src_user_nm as src_user_nm, ct.rf_src_cd as src_cd '
                               end||
                         'FROM BUSINESS.ACCT acct '||
                              'JOIN BUSINESS.CASH_TRXN ct ON ct.rf_acct_seq_id = acct.acct_seq_id and ct.trxn_exctn_dt + 0 = acct.rf_first_actvy_dt '||
                              'LEFT JOIN BUSINESS.CUST cust ON cust.cust_seq_id = ct.rf_cust_seq_id '||
                        'WHERE case when nvl(acct.src_sys_cd, ''?'') <> ''BCK''
                                    then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date(''31.12.9999'', ''dd.mm.yyyy''))
                               end'||case par_query_type
                                       when 'FIND'  
                                       then ' between mantas.rf_pkg_scnro.get_start_date and mantas.rf_pkg_scnro.get_end_date + 1 - 1/24/3600'
                                       else ' >= trunc(sysdate) - mantas.rf_pkg_scnro.get_last_days_qty'
                                     end||
                         ' and ct.rf_trxn_opok_am >= '||to_char(var_ct_min_amount)||
                         ' and nvl(ct.rf_canceled_fl, ''N'') <> ''Y'''||
                         case par_query_type
                           when 'CHECK' then ' and 1 <> 1'
                           when 'FIND'  then ' and ct.rf_subpartition_key between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id*1000) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99999, mantas.rf_pkg_scnro.get_tb_id*1000 + 999)'
                           -- ���������� �� view ����������� �� ���������� �������� - view ������������ ������ ������ ��� (����������) ��������� ��������� ��������
                           when 'VIEW' then ' and ''CT'' = mantas.rf_pkg_scnro.get_op_cat_cd'||
                                            ' and ct.fo_trxn_seq_id = mantas.rf_pkg_scnro.get_trxn_seq_id'
                         end
             end||
             case when var_bot_expr is not null
                  then to_clob('')||' UNION ALL '||
                       'SELECT '||case when par_query_type = 'CHECK' 
                                       then '/*+ NO_PARALLEL*/ '
                                       when par_query_type in ('FIND', 'RUN') 
                                       then '/*+ PARALLEL(acct 64) ORDERED INDEX(acct) USE_NL(bot) INDEX(bot RF_BO_TRXN_ACCT_FK_I)*/ '
                                       when par_query_type = 'VIEW'  
                                       then '/*+ LEADING(bot)*/ ' -- ������������ ��� ��������� ��������� ��������
                                  end||
                            '''BOT'' as op_cat_cd, bot.rf_partition_date, bot.rf_subpartition_key, bot.rf_opok_status, bot.bo_trxn_seq_id as trxn_seq_id, to_number(null) as trxn_scrty_seq_id, '||
                               'regexp_replace(trim(''|'' from '||var_bot_expr||'), ''\|{2,}'', ''|'') as opok_list, bot.exctn_dt as trxn_dt, '||
                               'bot.data_dump_dt as data_dump_dt, bot.rf_branch_id as branch_id '||
                               case when par_query_type in ('VIEW', 'RUN', 'CHECK')
                                    then ', acct.acct_seq_id as acct_seq_id '
                               end||     
                               case par_query_type
                                 when 'FIND'
                                 then ', bot.trxn_rptng_am as trxn_crncy_am, bot.trxn_base_am as trxn_base_am, bot.rptng_crncy_cd as trxn_crncy_cd, bot.dscr_tx as trxn_desc, bot.trxn_type1_cd as nsi_op_id, '||
                                        'to_number(null) as scrty_base_am, to_char(null) as scrty_type_cd, to_char(null) as scrty_id, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then cust.full_nm end as orig_nm, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then cust.tax_id end as orig_inn_nb, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then acct.alt_acct_id end as orig_acct_nb, '||
                                        'case when bot.dbt_cdt_cd = ''D'' then ''Y'' end as orig_own_fl, '||
                                        'bot.rf_debit_cd as debit_cd, to_char(null) as send_instn_nm, to_char(null) as send_instn_id, to_char(null) as send_instn_acct_id, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then cust.full_nm end as benef_nm, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then cust.tax_id end as benef_inn_nb, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then acct.alt_acct_id end as benef_acct_nb, '||
                                        'case when bot.dbt_cdt_cd = ''C'' then ''Y'' end as benef_own_fl, '||
                                        'bot.rf_credit_cd as credit_cd, to_char(null) as rcv_instn_nm, to_char(null) as rcv_instn_id, to_char(null) as rcv_instn_acct_id, '||
                                        'trunc(bot.rf_branch_id/100000) as tb_id, bot.rf_branch_id - round(bot.rf_branch_id, -5) as osb_id, '||
                                        'bot.src_sys_cd, bot.rf_src_change_dt as src_change_dt, bot.rf_src_user_nm as src_user_nm, bot.rf_src_cd as src_cd '
                               end||
                         'FROM BUSINESS.ACCT acct '||
                              'JOIN BUSINESS.BACK_OFFICE_TRXN bot ON bot.rf_acct_seq_id = acct.acct_seq_id and bot.exctn_dt + 0 = acct.rf_first_actvy_dt '||
                              'LEFT JOIN BUSINESS.CUST cust ON cust.cust_seq_id = to_number(acct.prmry_cust_intrl_id) '||
                        'WHERE case when nvl(acct.src_sys_cd, ''?'') <> ''BCK''
                                    then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date(''31.12.9999'', ''dd.mm.yyyy''))
                               end'||case par_query_type
                                       when 'FIND'  
                                       then ' between mantas.rf_pkg_scnro.get_start_date and mantas.rf_pkg_scnro.get_end_date + 1 - 1/24/3600'
                                       else ' >= trunc(sysdate) - mantas.rf_pkg_scnro.get_last_days_qty'
                                     end||
                         ' and bot.trxn_base_am >= '||to_char(var_bot_min_amount)||
                         ' and nvl(bot.rf_canceled_fl, ''N'') <> ''Y'''||
                         case par_query_type
                           when 'CHECK' then ' and 1 <> 1'
                           when 'FIND'  then ' and bot.rf_subpartition_key between decode(mantas.rf_pkg_scnro.get_tb_id, null, 0, mantas.rf_pkg_scnro.get_tb_id*1000) and decode(mantas.rf_pkg_scnro.get_tb_id, null, 99999, mantas.rf_pkg_scnro.get_tb_id*1000 + 999)'
                           -- ���������� �� view ����������� �� ���������� �������� - view ������������ ������ ������ ��� (����������) ��������� ��������� ��������
                           when 'VIEW' then ' and ''BOT'' = mantas.rf_pkg_scnro.get_op_cat_cd'||
                                            ' and bot.bo_trxn_seq_id = mantas.rf_pkg_scnro.get_trxn_seq_id'
                         end
             end,
             length(' UNION ALL ') + 1);


  if par_query_type = 'CHECK' and var_sql is not null Then
    var_sql := 'SELECT count(*) FROM ('||var_sql||')';
  elsif par_query_type = 'FIND' and var_sql is not null Then
    var_sql := 'SELECT * FROM ('||var_sql||') WHERE opok_list is not null';
  end if;

  return var_sql;
END get_opok_select_firstrxn;
--
-- ��������������� ������ ������� ��������� (� ������� RF_OPOK_RULE_SNAPSHOT)
-- �������, ��������������� ����������� ������� ������ ���������, ����������� � RF_OPOK_RULE_SNAPSHOT (������ ���� ��� ���������� �� ����������� ������, ��� ������������ ���)
-- ��������! ������� ��������� COMMIT!
--
FUNCTION shapshot_opok_rule (
  par_query_cd         rf_opok_query.query_cd%TYPE   DEFAULT null   -- ��� ������� ���������, ���� ������, �� ��������� ������ ������ ������, ��������� � ���
  )
RETURN INTEGER IS -- ���������� ��������� ������� (���� ��� ������ ��� ���� �����������, �� 0)
  var_count  INTEGER := 0;
BEGIN
  --
  -- ���� �� �������� ���������, � ������� ���������� ������ �������
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/
                   rl.rule_seq_id,
                   opd.record_seq_id,
                   rl.opok_nb,
                   rl.query_cd,
                   rl.priority_nb,
                   opd.limit_am,
                   rl.tb_id,
                   rl.op_cat_cd,
                   rl.dbt_cdt_cd,
                   rl.kgrko_party_cd,
                   trim(',' from trim(crit_ac1.crit_tx)||','||trim(rl.acct_add_crit_tx)) as acct_crit_tx,
                   trim(',' from trim(crit_ac1.subtle_crit_tx)||','||trim(rl.acct_subtle_crit_tx)) as acct_subtle_crit_tx,
                   rl.acct_area_tx,
                   nvl(rl.acct_null_fl, crit_ac1.null_fl) as acct_null_fl,
                   trim(',' from trim(crit_ac2.crit_tx)||','||trim(rl.acct2_add_crit_tx)) as acct2_crit_tx,
                   trim(',' from trim(crit_ac2.subtle_crit_tx)||','||trim(rl.acct2_subtle_crit_tx)) as acct2_subtle_crit_tx,
                   rl.acct2_area_tx,
                   nvl(rl.acct2_null_fl, crit_ac2.null_fl) as acct2_null_fl,
                   rl.acct_reverse_fl,
                   trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,��,%' then trim(crit_lex.crit_tx) end||','||trim(rl.lexeme_add_purpose_tx)) as lexeme_purpose_tx,
                   trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�,%' then trim(crit_lex.crit_tx) end||','||trim(rl.lexeme_add_cust_tx)) as lexeme_cust_tx,
                   trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�2,%' then trim(crit_lex.crit_tx) end||','||trim(rl.lexeme_add_cust2_tx)) as lexeme_cust2_tx,
                   trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,��,%' then trim(crit_lex2.crit_tx) end||','||trim(rl.lexeme2_add_purpose_tx)) as lexeme2_purpose_tx,
                   trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�,%' then trim(crit_lex2.crit_tx) end||','||trim(rl.lexeme2_add_cust_tx)) as lexeme2_cust_tx,
                   trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�2,%' then trim(crit_lex2.crit_tx) end||','||trim(rl.lexeme2_add_cust2_tx)) as lexeme2_cust2_tx,
                   rl.cust_watch_list_cd, 
                   rl.cust_not_watch_list_fl, 
                   rl.cust2_watch_list_cd, 
                   rl.cust2_not_watch_list_fl, 
                   trim(',' from trim(crit_optp.crit_tx)||','||trim(rl.optp_add_crit_tx)) as optp_crit_tx,
                   rl.src_crit_tx,
                   nvl(trim(rl.custom_crit_sql_tx), trim(crit_cust.crit_tx)) as custom_crit_sql_tx,
                   rl.custom_crit_param_tx,
                   nvl(trim(rl.explanation_sql_tx), trim(crit_expl.crit_tx)) as explanation_sql_tx,
                   nullif(greatest(nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')), nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy'))), to_date('01.01.0001', 'dd.mm.yyyy')) as start_dt,
                   nullif(least(nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')), nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))), to_date('31.12.9999', 'dd.mm.yyyy')) as end_dt,
                   rl.note_tx
              FROM mantas.rf_opok_rule rl
                   JOIN mantas.rf_opok opk ON opk.opok_nb = rl.opok_nb
                   LEFT JOIN mantas.rf_opok_criteria crit_ac1  ON crit_ac1.crit_cd  = rl.acct_crit_cd
                   LEFT JOIN mantas.rf_opok_criteria crit_ac2  ON crit_ac2.crit_cd  = rl.acct2_crit_cd
                   LEFT JOIN mantas.rf_opok_criteria crit_lex  ON crit_lex.crit_cd  = rl.lexeme_crit_cd
                   LEFT JOIN mantas.rf_opok_criteria crit_lex2 ON crit_lex2.crit_cd = rl.lexeme2_crit_cd
                   LEFT JOIN mantas.rf_opok_criteria crit_optp ON crit_optp.crit_cd = rl.optp_crit_cd
                   LEFT JOIN mantas.rf_opok_criteria crit_cust ON crit_cust.crit_cd = rl.custom_crit_cd
                   LEFT JOIN mantas.rf_opok_criteria crit_expl ON crit_expl.crit_cd = rl.explanation_crit_cd
                   LEFT JOIN mantas.rf_opok_details opd ON opd.opok_nb = rl.opok_nb and
                                                           opd.active_fl = 'Y' and
                                                           nvl(opd.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')) <= nvl(rl.end_dt, to_date('31.12.9999', 'dd.mm.yyyy')) and
                                                           nvl(rl.start_dt, to_date('01.01.0001', 'dd.mm.yyyy')) <= nvl(opd.end_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
             WHERE query_cd = nvl(par_query_cd, query_cd) and
                   rl.vers_status_cd = 'ACT' and
                   rl.err_fl = 'N'
           MINUS
           SELECT /*+ NO_PARALLEL*/ rule_seq_id, record_seq_id, opok_nb, query_cd, priority_nb, limit_am, tb_id, op_cat_cd, dbt_cdt_cd, kgrko_party_cd, acct_crit_tx, acct_subtle_crit_tx, acct_area_tx, acct_null_fl, acct2_crit_tx, acct2_subtle_crit_tx, acct2_area_tx, acct2_null_fl, acct_reverse_fl, lexeme_purpose_tx, lexeme_cust_tx, lexeme_cust2_tx, lexeme2_purpose_tx, lexeme2_cust_tx, lexeme2_cust2_tx, cust_watch_list_cd, cust_not_watch_list_fl, cust2_watch_list_cd, cust2_not_watch_list_fl, optp_crit_tx, src_crit_tx, custom_crit_sql_tx, custom_crit_param_tx, explanation_sql_tx, start_dt, end_dt, note_tx
             FROM mantas.rf_opok_rule_snapshot
            WHERE query_cd = nvl(par_query_cd, query_cd) and
                  actual_fl = 'Y') LOOP
    --
    -- ������� ���� ������������ � �������� ������
    --
    UPDATE mantas.rf_opok_rule_snapshot
       SET actual_fl = 'N'
     WHERE rule_seq_id = r.rule_seq_id and
           record_seq_id = r.record_seq_id and
           actual_fl = 'Y';
    --
    -- ������� ����� ������
    --
    INSERT INTO mantas.rf_opok_rule_snapshot(snapshot_id, rule_seq_id, record_seq_id, actual_fl, opok_nb, query_cd, priority_nb, limit_am,
                                             tb_id, op_cat_cd, dbt_cdt_cd, kgrko_party_cd, acct_crit_tx, acct_subtle_crit_tx, acct_area_tx, acct_null_fl,
                                             acct2_crit_tx, acct2_subtle_crit_tx, acct2_area_tx, acct2_null_fl, acct_reverse_fl,
                                             lexeme_purpose_tx, lexeme_cust_tx, lexeme_cust2_tx, lexeme2_purpose_tx, lexeme2_cust_tx, lexeme2_cust2_tx,
                                             cust_watch_list_cd, cust_not_watch_list_fl, cust2_watch_list_cd, cust2_not_watch_list_fl,
                                             optp_crit_tx, src_crit_tx, custom_crit_sql_tx, custom_crit_param_tx, explanation_sql_tx, 
                                             start_dt, end_dt, note_tx)
           VALUES(mantas.rf_opok_rule_snapshot_seq.NextVal, r.rule_seq_id, r.record_seq_id, 'Y', r.opok_nb, r.query_cd, r.priority_nb, r.limit_am,
                  r.tb_id, r.op_cat_cd, r.dbt_cdt_cd, r.kgrko_party_cd, r.acct_crit_tx, r.acct_subtle_crit_tx, r.acct_area_tx, r.acct_null_fl,
                  r.acct2_crit_tx, r.acct2_subtle_crit_tx, r.acct2_area_tx, r.acct2_null_fl, r.acct_reverse_fl,
                  r.lexeme_purpose_tx, r.lexeme_cust_tx, r.lexeme_cust2_tx, r.lexeme2_purpose_tx, r.lexeme2_cust_tx, r.lexeme2_cust2_tx,
                  r.cust_watch_list_cd, r.cust_not_watch_list_fl, r.cust2_watch_list_cd, r.cust2_not_watch_list_fl,
                  r.optp_crit_tx, r.src_crit_tx, r.custom_crit_sql_tx, r.custom_crit_param_tx, r.explanation_sql_tx, 
                  r.start_dt, r.end_dt, r.note_tx);

    var_count := var_count + 1;
  END LOOP;
  --
  -- COMMIT � �������
  --
  COMMIT;

  return var_count;
END shapshot_opok_rule;
--
-- ������� ������ ����� ���� ��� ������ (���� ����, ���� ����� ���������, ���������� �� ���������)
--
FUNCTION get_opok_list(
  par_review_id      mantas.kdd_review.review_id%TYPE,
  par_pimary_opok    VARCHAR2  DEFAULT NULL
  )
RETURN VARCHAR2 IS
  var_result  VARCHAR2(2000);
BEGIN
  SELECT listagg(trim(value_tx), ', ') within group(order by case value_tx when '7001' then 1 when '6001' then 99 else 98 end,  value_tx)
    INTO var_result
    FROM (select distinct bb.value_tx
            from mantas.kdd_break b,
                 mantas.kdd_break_binding bb
           where b.prnt_break_id = par_review_id and
                 bb.break_id = b.break_id and
                 bb.bindg_nm = ('OPOK') and
                 bb.value_tx is not null and
                 bb.value_tx <> nvl(par_pimary_opok, '!@&'));

  return var_result;
END get_opok_list;
--
-- ������� ������ ������ ��� ������, � ������������ � �������� ����������� ��������� ��������
-- ���� ����� ����� ��������������� ���������� �������� ��������� ���� �, ��������������, ���������� ������� ������, ������� ������ ������ ������������ � ��������� ����:
-- <������� ��������� ���� ���� ����� �������>|<������� ���. ���� 1 ���� ����� �������>|<������� ���. ���� 2 ���� ����� �������>|� �. �.
-- ���� � ����� ������� ������������ ����� ��� ������ ������ (��������� ����� ���������� �), �� ��� ����� ����������� |
--
FUNCTION get_lexeme_list(
  par_review_id      mantas.kdd_review.review_id%TYPE,
  par_area           VARCHAR2 -- �� - ������� ��� ���������� �������, � - ������� ������� ��������� (�����������), �2 - ������� ������� ���������
                              -- ����� ��������� ����� ����������� ����� �������, ��������: �, �2
  )
RETURN VARCHAR2 IS
  var_result  VARCHAR2(2000);
BEGIN
  SELECT /*+ NO_PARALLEL*/ listagg(trim(lexeme)||nvl2(trim(lexeme2), '|'||trim(lexeme2), ''), '|') within group(order by priority_nb,  snapshot_id)
    INTO var_result
    FROM (select rvo.opok_nb,
                 opk.priority_nb,
                 mantas.rf_pkg_scnro.refine_lexeme_list(
                                 case when ','||replace(par_area, ' ')||',' like '%,��,%'
                                      then ','||sn.lexeme_purpose_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�,%'
                                      then ','||sn.lexeme_cust_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�2,%'
                                      then ','||sn.lexeme_cust2_tx
                                 end
                   ) as lexeme,
                 mantas.rf_pkg_scnro.refine_lexeme_list(
                                 case when ','||replace(par_area, ' ')||',' like '%,��,%'
                                      then ','||sn.lexeme2_purpose_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�,%'
                                      then ','||sn.lexeme2_cust_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�2,%'
                                      then ','||sn.lexeme2_cust2_tx
                                 end
                   ) as lexeme2,
                 sn.snapshot_id
            from mantas.kdd_review rv,
                 mantas.rf_kdd_review_opok rvo,
                 mantas.rf_opok_rule_snapshot sn,
                 mantas.rf_opok opk  
           where rv.review_id = par_review_id and
                 rvo.review_id = rv.review_id and
                 rvo.repeat_nb = rv.rf_repeat_nb and
                 sn.snapshot_id = rvo.snapshot_id and
                 opk.opok_nb = rvo.opok_nb);
  
/*  SELECT listagg(trim(lexeme), '|') within group(order by case value_tx when '7001' then 1 when '6001' then 99 else 98 end,  value_tx)
    INTO var_result
    FROM (select distinct bb.value_tx,
                 case when par_mode = 1 or
                           (par_mode = 2 and extractValue(xmltype(b.tshld_tx), 'ThresholdSet/Threshold[@name="CHECK_CUST_NAME"][1]/Value[1]') = '1')
                      then replace(extractValue(xmltype(b.tshld_tx), 'ThresholdSet/Threshold[@name="LEXEME"][1]/Value[1]'), '''')
                 end as lexeme
            from mantas.kdd_break b,
                 mantas.kdd_break_binding bb
           where b.prnt_break_id = par_review_id and
                 bb.break_id = b.break_id and
                 bb.bindg_nm = ('OPOK') and
                 bb.value_tx is not null);*/

  return var_result;
EXCEPTION
  WHEN OTHERS THEN
    return null;  
END get_lexeme_list;
--
-- ������� ������ ������ ��� ���������� ������� ���������
-- ���� � ����� ������� ������������ ����� ��� ������ ������ (��������� ����� ���������� �), �� ��� ����������� |
--
FUNCTION get_rule_lexeme_list(
  par_rule_seq_id    mantas.rf_opok_rule.rule_seq_id%TYPE,
  par_area           VARCHAR2, -- �� - ������� ��� ���������� �������, � - ������� ������� ��������� (�����������), �2 - ������� ������� ���������
                               -- ����� ��������� ����� ����������� ����� �������, ��������: �, �2
  par_crit_status_cd VARCHAR2  -- �������: 'ACT' - ��������� ����������� �������� ��������� (��. RF_OPOK_CRITERIA), 'TEST' - ��������� �������� �������� ��������� � ������ ��� �� ���������� - �����������
  )
RETURN VARCHAR2 IS
  var_result  VARCHAR2(2000);
BEGIN
  SELECT /*+ NO_PARALLEL*/ trim('|' from trim(lexeme)||nvl2(trim(lexeme2), '|'||trim(lexeme2), ''))
    INTO var_result
    FROM (select mantas.rf_pkg_scnro.refine_lexeme_list(
                                 case when ','||replace(par_area, ' ')||',' like '%,��,%'
                                      then ','||lexeme_purpose_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�,%'
                                      then ','||lexeme_cust_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�2,%'
                                      then ','||lexeme_cust2_tx
                                 end
                   ) as lexeme,
                 mantas.rf_pkg_scnro.refine_lexeme_list(
                                 case when ','||replace(par_area, ' ')||',' like '%,��,%'
                                      then ','||lexeme2_purpose_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�,%'
                                      then ','||lexeme2_cust_tx
                                 end||
                                 case when ','||replace(par_area, ' ')||',' like '%,�2,%'
                                      then ','||lexeme2_cust2_tx
                                 end
                   ) as lexeme2
            from (select trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,��,%' then case when rl.crit_status_cd = 'TEST' then nvl(trim(crit_lex.test_crit_tx), trim(crit_lex.crit_tx)) else trim(crit_lex.crit_tx) end end||','||trim(rl.lexeme_add_purpose_tx)) as lexeme_purpose_tx,
                         trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�,%' then case when rl.crit_status_cd = 'TEST' then nvl(trim(crit_lex.test_crit_tx), trim(crit_lex.crit_tx)) else trim(crit_lex.crit_tx) end end||','||trim(rl.lexeme_add_cust_tx)) as lexeme_cust_tx,
                         trim(',' from case when ','||replace(rl.lexeme_area_tx, ' ')||',' like '%,�2,%' then case when rl.crit_status_cd = 'TEST' then nvl(trim(crit_lex.test_crit_tx), trim(crit_lex.crit_tx)) else trim(crit_lex.crit_tx) end end||','||trim(rl.lexeme_add_cust2_tx)) as lexeme_cust2_tx,
                         trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,��,%' then case when rl.crit_status_cd = 'TEST' then nvl(trim(crit_lex2.test_crit_tx), trim(crit_lex2.crit_tx)) else trim(crit_lex2.crit_tx) end end||','||trim(rl.lexeme2_add_purpose_tx)) as lexeme2_purpose_tx,
                         trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�,%' then case when rl.crit_status_cd = 'TEST' then nvl(trim(crit_lex2.test_crit_tx), trim(crit_lex2.crit_tx)) else trim(crit_lex2.crit_tx) end end||','||trim(rl.lexeme2_add_cust_tx)) as lexeme2_cust_tx,
                         trim(',' from case when ','||replace(rl.lexeme2_area_tx, ' ')||',' like '%,�2,%' then case when rl.crit_status_cd = 'TEST' then nvl(trim(crit_lex2.test_crit_tx), trim(crit_lex2.crit_tx)) else trim(crit_lex2.crit_tx) end end||','||trim(rl.lexeme2_add_cust2_tx)) as lexeme2_cust2_tx
                    from (select case when vers_status_cd in ('TEST','DEV')
                                      then nvl(par_crit_status_cd, 'TEST')
                                      else nvl(par_crit_status_cd, 'ACT')
                                 end as crit_status_cd,
                                 t.* 
                            from mantas.rf_opok_rule t 
                           where rule_seq_id = par_rule_seq_id) rl
                         LEFT JOIN mantas.rf_opok_criteria crit_lex  ON crit_lex.crit_cd  = rl.lexeme_crit_cd
                         LEFT JOIN mantas.rf_opok_criteria crit_lex2 ON crit_lex2.crit_cd = rl.lexeme2_crit_cd
                   ));
  
  return var_result;
EXCEPTION
  WHEN OTHERS THEN
    return null;  
END get_rule_lexeme_list;
--
-- �������� � ������ ������ ����. ������� ���, ����� ��������� ������� ������ ���� ��� ��������� �� ���������� ������������:
-- \% => % | \_ => _ | \\ => \ | % => , | _ => , | & => ,
--
FUNCTION refine_lexeme_list(par_string  VARCHAR2)
RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE IS
BEGIN
  return regexp_replace(trim(',' from -- ����� ������� ����� ���������/�������� ~
           ----- ��������� ������� - ��������� ������� �� ������ ������� - ��� ���������, �������� �� ������� ��_� => ��,�
           regexp_replace(','||replace(
           ----------------------------
           replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
             par_string, '|',  ''), -- ������� | (������� ������ ����) �� ������
                         '\\', '#[BACKSLASH]#'), -- ����� \\ �������� ������, ��������, ��� �������: \\%
                         '\%', '#[PERCENT]#'),
                         '\_', '#[UNDERSCORE]#'),
                         '%',  ','), -- ����� �������� �� ~[#%#]~
                         '_',  ','), -- ����� �������� �� ~[#_#]~
                         '&',  ','),
                         '#[PERCENT]#', '%'),
                         '#[UNDERSCORE]#', '_'),
                         '#[BACKSLASH]#', '\')
           ----------------------------
           , ',', ',,')||',', '\,[^,]{1}\,')
           ----------------------------                         
               ), '\,{2,}', ',');                                              
END refine_lexeme_list;
--
-- ������� ����� ��������� �� ��������� ��� ���������� ������ ������� ���������
--
FUNCTION get_default_expl(
  par_snapshot_id      mantas.rf_opok_rule_snapshot.snapshot_id%TYPE
  )
RETURN VARCHAR2 IS
  var_result    VARCHAR2(32000);
BEGIN
  SELECT /*+ NO_PARALLEL*/ trim(trim(',' from trim(
           case when query_cd = '������_��������'
                then '������ �������� �� �����, '
           end||
           -- ����� ��� �������
           case when op_cat_cd = 'WT' and 
                     trim(lexeme_purpose_tx||lexeme_cust_tx||lexeme_cust2_tx||lexeme2_purpose_tx||lexeme2_cust_tx||lexeme2_cust2_tx) is not null
                then '������� � '||case when trim(lexeme_purpose_tx||lexeme2_purpose_tx) is not null then '���������� �������, ' end||
                                   case when trim(lexeme_cust_tx||lexeme_cust2_tx||lexeme2_cust_tx||lexeme2_cust2_tx) is not null 
                                        then '������������ '||case when trim(lexeme_cust_tx||lexeme2_cust_tx) is not null then '�����������, ' end||
                                                              case when trim(lexeme_cust2_tx||lexeme2_cust2_tx) is not null then '����������, ' end
                                   end
                when op_cat_cd in ('CT', 'BOT') and trim(lexeme_purpose_tx||lexeme_cust_tx||lexeme2_purpose_tx||lexeme2_cust_tx) is not null
                then '������� � '||case when trim(lexeme_purpose_tx||lexeme2_purpose_tx) is not null then '���������� �������, ' end||
                                   case when trim(lexeme_cust_tx||lexeme2_cust_tx) is not null 
                                        then '������������ '||case when dbt_cdt_cd = 'D'   then '�����������, ' end||
                                                              case when dbt_cdt_cd = 'C'   then '����������, ' end||
                                                              case when dbt_cdt_cd is null then '�����������/����������, ' end  
                                   end                                     
           end||
           -- ����� ��� �����
           case when op_cat_cd = 'WT' and trim(acct_crit_tx||acct_subtle_crit_tx||acct2_crit_tx||acct2_subtle_crit_tx) is not null
                then '������� �� ���� '||case when trim(acct_crit_tx||acct_subtle_crit_tx) is not null or
                                                   (acct_reverse_fl = 'Y' and trim(acct2_crit_tx||acct2_subtle_crit_tx) is not null)
                                              then '�����������, ' 
                                         end||
                                         case when trim(acct2_crit_tx||acct2_subtle_crit_tx) is not null or
                                                   (acct_reverse_fl = 'Y' and trim(acct_crit_tx||acct_subtle_crit_tx) is not null)
                                              then '����������, ' 
                                         end                                          
                when op_cat_cd in ('CT', 'BOT') and trim(acct_crit_tx||acct_subtle_crit_tx) is not null
                then '������� �� ���� '||case when dbt_cdt_cd = 'D' then '�����������, ' end||
                                         case when dbt_cdt_cd = 'C'   then '����������, ' end||
                                         case when dbt_cdt_cd is null then '�����������/����������, ' end
                
           end||
           -- ����� ��� ���� �������� ��� ���
           case when trim(optp_crit_tx) is not null
                then '��� �������� ��� ��� ���� ��: '||trim(optp_crit_tx)||', '
           end||     
           -- ����� ��� ������� �����������
           case when op_cat_cd = 'WT' and 
                     trim(cust_watch_list_cd) is not null and
                     nvl(cust_not_watch_list_fl, '?') <> 'Y'
                then '���������� �� ������� ����������� '||cust_watch_list_cd||', '
           end||
           case when op_cat_cd = 'WT' and 
                     trim(cust2_watch_list_cd) is not null and
                     nvl(cust2_not_watch_list_fl, '?') <> 'Y'
                then '���������� �� ������� ����������� '||cust2_watch_list_cd||', '
           end||
           case when op_cat_cd in ('CT', 'BOT') and 
                     trim(cust_watch_list_cd) is not null and
                     nvl(cust_not_watch_list_fl, '?') <> 'Y'
                then case when dbt_cdt_cd = 'D'   
                          then '����������'
                          when dbt_cdt_cd = 'C'   
                          then '����������'
                          when dbt_cdt_cd is null 
                          then '����������/����������'
                     end||' �� ������� ����������� '||cust_watch_list_cd||', '
           end
           )))
    INTO var_result
    FROM mantas.rf_opok_rule_snapshot
   WHERE snapshot_id = par_snapshot_id;

  return var_result; 
EXCEPTION
  WHEN OTHERS THEN   
    return null;
END get_default_expl;  
--
-- ������� ����� ��������� � ��������� ��� ������ 
--
FUNCTION get_explanation(
  par_review_id      mantas.kdd_review.review_id%TYPE
  )
RETURN VARCHAR2 IS
  var_result    VARCHAR2(32000);
BEGIN
  SELECT /*+ NO_PARALLEL*/ 
         case when max(rv.rf_object_tp_cd) like 'OES%'
              then '��������� (���) ������� ������� ��� �������������'||
                   case when max(trim(oes.manual_cmnt)) is null 
                        then to_char(null)
                        else ': '||max(trim(oes.manual_cmnt))
                   end         
              when max(rvr.non_opok_fl) = 'Y'
              then '�� �������� �������� ����� ��������� ��������'
              when  max(rvr.non_opok_fl) = 'J'
              then '������ ����� ��������� �������� � ����� � ���������� ������ ������ � ��������� �� �������� (� ��������: '||
                   to_char(max(rv_joint.review_id))||')'
              else case when count(distinct rvo.opok_nb) > 1
                        then listagg(to_char(rvo.opok_nb)||': '||coalesce(rvo.explanation_tx, mantas.rf_pkg_scnro.get_default_expl(rvo.snapshot_id)), ', ')
                                     within group(order by opk.priority_nb, rvo.record_id)
                        else listagg(coalesce(rvo.explanation_tx, mantas.rf_pkg_scnro.get_default_expl(rvo.snapshot_id)), ', ')
                                     within group(order by opk.priority_nb, rvo.record_id)     
                   end                  
         end                  
    INTO var_result
    FROM mantas.kdd_review rv
         left join mantas.rf_kdd_review_run rvr on rvr.review_id = rv.review_id and
                                                   rvr.repeat_nb = rv.rf_repeat_nb
         left join mantas.rf_oes_321 oes on oes.review_id = rv.review_id and 
                                            oes.current_fl = 'Y' and
                                            rv.rf_object_tp_cd like 'OES%'                                    
         left join mantas.rf_kdd_review_opok rvo on rvo.review_id = rvr.review_id and
                                                    rvo.repeat_nb = rvr.repeat_nb
         left join mantas.rf_opok opk on opk.opok_nb = rvo.opok_nb
         left join mantas.kdd_review rv_joint on rvr.non_opok_fl = 'J' and
                                                 rv_joint.rf_op_cat_cd = rv.rf_op_cat_cd and
                                                 rv_joint.rf_trxn_seq_id = rv.rf_trxn_seq_id and
                                                 rv_joint.rf_object_tp_cd in ('TRXN_SCRTY_JOINT', 'TRXN')
   WHERE rv.review_id = par_review_id;
  --
  -- ������� ������ ����� �������
  --  
  var_result := upper(substr(var_result, 1, 1))||substr(var_result, 2);
  
  return var_result;
EXCEPTION
  WHEN OTHERS THEN   
    return null;
END get_explanation;  
--
-- �������� ���
--
PROCEDURE clear_cache IS  
BEGIN 
  pck_opok_alerts_trxn_sql := null;
  pck_opok_alerts_trxn_date := null;
  
  pck_crit_cache.DELETE;  
  pck_crit_cache_date := null; 
END clear_cache;

-- ��������� ���� (�������������)
BEGIN
  if pck_last_days_qty is null Then
    pck_last_days_qty := to_number(sys_context('CLIENTCONTEXT', 'AMLOPOK_LAST_DAYS_QTY'));
  end if;

  if pck_last_days_qty is null Then
    pck_last_days_qty := 32; -- ���������� ������������� ���� �� ������� ���� � �������, �� ��������� - 32 (����� � ���� ����)
  end if;  
END RF_PKG_SCNRO;
/

GRANT EXECUTE on mantas.rf_pkg_scnro to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_scnro to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_scnro to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_scnro to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_scnro to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_scnro to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_scnro to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_scnro to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_scnro to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_scnro to KYC;
GRANT EXECUTE on mantas.rf_pkg_scnro to STD;
GRANT EXECUTE on mantas.rf_pkg_scnro to KDD_ALG;


GRANT CREATE ANY VIEW to mantas;

