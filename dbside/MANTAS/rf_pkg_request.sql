create or replace package mantas.rf_pkg_request is
  --
  -- ������������ �������� ������ � �������� id �������
  --
  procedure log_request_error(p_errmsg clob, p_id number);
  --
  -- ������� ���������� ����� update'� �� ��������� �������, ���������� ��������� ����,
  -- ������ ������ � ������� json.
  --
  function update_generate(
                                p_schema varchar2,
                                p_table_name varchar2,
                                p_key_name   varchar2,
                                p_data       rf_json,
                                p_where      varchar2 default null ) return clob;
  --
  -- ������� ���������� ����� insert'a � �������� ��������� ����
  -- �� ����� ������������������
  --
  function insert_generate(
                                p_schema varchar2,
                                p_table_name  varchar2,
                                p_column_name varchar2,
                                p_seq_name    varchar2,
                                p_key_name    varchar2 default null,
                                p_data        rf_json) return clob;
  --
  -- ������� ��������� �� ���� ��������� varchar2(4000) � ���������� ������ � ������� json
  -- � ����� ����������.
  --
  function exec_exception(p_errmsg varchar2) return clob;
  --
  -- ������� ���������� ������� where �� ������ �������� ������ � p_data
  -- ���� ������� p_prefix - ����� ��������� ����� ������ ��������� ������� ������ p_prefix
  --
  --function where_generate(p_data   rf_json,
  --                             p_prefix varchar2 default null) return clob;
  --
  -- ������� ���������� ������� order by �� ������ �������� ������ � p_data
  -- ���� ������� p_prefix - ����� ��������� ����� ������ ��������� ������� ������ p_prefix
  --
  --function orderby_generate(p_data   rf_json,
  --                               p_prefix varchar2 default null) return clob;
  --
  -- ������� ��������� ������� ������
  --
  function exec_sql_insert(p_sql      clob,
                           p_id_req   number,
                           p_id       varchar2,
                           p_clientid varchar2,
                           p_schema   varchar2 default null,
                           p_table    VARCHAR2 DEFAULT NULL,
                           p_id_name  varchar2 default null) return clob;
  --
  -- ������� ��������� ������ �� ��������� ������� � ���������� json
  --
  function exec_sql_update(p_sql        varchar2,
                           p_bind_param varchar2,
                           p_id_req     number,
                           p_schema     varchar2 default null, --��� �����
                           p_table      varchar2 default null, --��� �������
                           p_id_name    varchar2 default null, --��� �������� PK
                           p_id_type    varchar2 default null  --��� PK �������� 
                          ) return clob;
  --
  -- ������� ������� ������ �� ��������� ������� � ���������� json
  --
  function exec_sql_delete(p_sql varchar2, p_id_req number) return clob;
  --
  -- ������� ��������� ������� �� ������, ������������ ��� ������ �����.
  -- ������� ���������� ��������� ������� json.
  --
  function exec_sql_read(p_sql    varchar2,
                             p_id_req number) return clob;
                             
  /* ����� ��������� ���������� exec_sql_read, �� ���������� json */
  function exec_sql_read_json( p_sql in varchar2, p_id_request in number, p_binds rf_json default null ) return rf_json;

  --
  -- ������� ��������� ������� �� ������, ������������ ����� ����� ������.
  -- ������� ���������� ��������� ������� json.
  --
  --function exec_sql_pageread(p_sql    varchar2,
  --                           p_start  number default 0,
  --                           p_end    number default 50,
  --                           p_id_req number) return clob;
  --
  -- ������� ��������� ������� �� ������, ������������ ������������ ������.
  -- ������� ���������� ��������� ������� json.
  --
  function exec_sql_singleread(p_sql    varchar2,
                               p_id     varchar2,
                               p_id_req number) return clob;
  --
  -- ��������� json ��� ������� rf_rule_snapshot � treegrid �� �������
  --
  function exec_ruleshapshot(v_review number,
                             v_repeat number,
                             p_id_req number) return clob;
  -- 
  -- �������� ������������� �������� ������� ������
  --
  FUNCTION get_current_audit_event_id
    RETURN mantas.rf_audit_event.audit_seq_id%TYPE; -- ID ������ � ������� ������


  --
  -- ���������� ������
  --
  function process_request(par_request in clob, -- �������� ������ � ������� json
                           par_user    in varchar2, -- ��� �������� ������������
                           par_ip_addr in varchar2 DEFAULT '0.0.0.0' --   IP ������ �������� ������������
                            ) return clob; -- ����� �� ������ � ������� json
end rf_pkg_request;
/
create or replace package body mantas.rf_pkg_request is

  -- ������� ������� ������
  g_audit_id        rf_audit_event.audit_seq_id%TYPE; -- ������� ������� ������
  g_err_type_cd     rf_audit_event.err_type_cd%TYPE;  -- ���� ������� ��������� �� ������, ����������� � ������� ������
  g_message_tx      rf_audit_event.message_tx%TYPE;   -- ��������� �� ����������� ��������� �������, ����������� � ������� ������
  --
  -- ������� ����� �� mantas.kdd_queue_master ����� �������
  --
  function get_queue_query(p_id number) return varchar2 is
    --���� ��� �������� mantas.kdd_queue_master.queue_query%type
    --�� ����� BUG 3340820 CREATE PL/SQL FUNCTION FAILS WITH PLS-801: INTERNAL ERROR [1401]
    v_queue_query varchar2(4000);
  begin
    select max(k.queue_query)
      into v_queue_query
      from mantas.kdd_queue_master k
     where k.queue_seq_id = p_id;
    return v_queue_query;
  end;
  --
  -- ������� ��������� ���� �� ��� �� ���� ��
  --
  function is_date(p_string varchar2) return boolean is
  begin
    return case when to_date(p_string, 'YYYY-MM-DD HH24:MI:SS') = to_date(p_string,
                                                                          'YYYY-MM-DD HH24:MI:SS') then true end;
  exception
    when others then
      return false;
  end;
  --
  -- ������� ���������� ���� ������, ���� ������ to_date() ��� ���
  --
  function get_string_or_date(p_string varchar2) return varchar2 is
    v_result varchar2(200) default null;
  begin
    begin
      if trim(p_string) is null then
        return null;
      else
        v_result := 'to_date(''' ||
                    to_char(to_date(p_string, 'YYYY-MM-DD HH24:MI:SS'),
                            'YYYY-MM-DD HH24:MI:SS') ||
                    ''', ''YYYY-MM-DD HH24:MI:SS'')';
      end if;
    exception
      when others then
        return '''' || p_string || '''';
    end;
    return v_result;
  end;
  --
  -- ������� ���������� ����� update'� �� ��������� �������, ���������� ��������� ����,
  -- ������ ������ � ������� json.
  --
  function update_generate(p_schema     varchar2,
                           p_table_name varchar2,
                           p_key_name   varchar2,
                           p_data       rf_json,
                           p_where      varchar2 default null ) return clob is
    v_sql   clob default null;
    v_index integer default 0;
  begin
    for r in (select lower(ut.column_name) as column_name,
                     case
                       when ut.data_type like '%NUMBER%' then
                        'number'
                       when ut.data_type like '%CHAR%' then
                        'string'
                       when ut.data_type like '%DATE%' then
                        'date'
                     end as data_type
                from all_tab_columns ut
               where table_name = upper(p_table_name)
                 and owner = upper(p_schema)
               order by ut.column_id asc) loop
      if v_index > 0 then
        v_sql := v_sql || ',';
      end if;
      if p_data.exist(upper(r.column_name)) then
        if r.data_type = 'string' then
          if rf_json_ext.get_string(p_data, upper(r.column_name)) is null and
             p_data.exist(upper(r.column_name)) then
            v_sql := v_sql || r.column_name || ' = null ' || chr(13);
          else
            if p_data.exist(upper(r.column_name)) then
              v_sql := v_sql || r.column_name || ' = ''' ||
                       replace(rf_json_ext.get_string(p_data,
                                                      upper(r.column_name)),
                               '''',
                               '''''') || '''' || chr(13);
            end if;
          end if;
        else
          if r.data_type = 'number' then
            if rf_json_ext.get_number(p_data, upper(r.column_name)) is null and
               p_data.exist(upper(r.column_name)) then
              v_sql := v_sql || r.column_name || ' = null ' || chr(13);
            else
              if p_data.exist(upper(r.column_name)) then
                v_sql := v_sql || r.column_name || ' = ' ||
                         rf_json_ext.get_number(p_data,
                                                upper(r.column_name)) ||
                         chr(13);
              end if;
            end if;
          else
            if r.data_type = 'date' then
              if rf_json_ext.get_date(p_data, upper(r.column_name)) is null and
                 p_data.exist(upper(r.column_name)) then
                v_sql := v_sql || r.column_name || ' = null ' || chr(13);
              else
                if p_data.exist(upper(r.column_name)) then
                  v_sql := v_sql || r.column_name || ' = to_date(' || '''' ||
                           to_char(rf_json_ext.get_date(p_data,
                                                        upper(r.column_name)),
                                   'DD-MM-YYYY HH24:MI:SS') || '''' || ',' || '''' ||
                           'DD-MM-YYYY HH24:MI:SS' || '''' || ')' ||
                           chr(13);
                end if;
              end if;
            end if;
          end if;
        end if;
      end if;
      v_index := v_index + 1;
      v_sql   := rtrim(v_sql, ',');
    end loop;
  
    v_sql := 'update ' || lower(p_table_name) || ' set ' || chr(13) ||
             ltrim(rtrim(v_sql, ','), ',');
    
    /* ���� ������������ p_where ������, �� ����� ������� �� ��� */
    if p_where is not null then
      v_sql := v_sql||p_where;
    else 
      v_sql := v_sql||' where '||p_key_name||' = :bind_variable';
    end if;
    
    return v_sql;
  end;
  --
  -- ������� ���������� ����� insert'a � �������� ��������� ����
  -- �� ����� ������������������
  --
  function insert_generate(p_schema      varchar2,
                           p_table_name  varchar2,
                           p_column_name varchar2, -- ��� ������� � ��������� 
                           p_seq_name    varchar2, -- ��� ��������
                           p_key_name    varchar2 default null, -- �������� ���� � �������� �������� ��� ���� �������� ��������
                           -- ������ ���� ���������� � �������� id �� ������
                           -- ���� ���-�� ����������! ����!
                           p_data rf_json) return clob is
    v_sql      clob default null;
    v_data_sql clob default null;
  begin
    v_sql      := 'declare' || chr(13) || '  v_id_tmp varchar2(250);' ||
                  chr(13) || 'begin ' || chr(13) || '  insert into ' ||
                  lower(p_table_name) || '(';
    v_data_sql := 'values(';
    -- get_string � get_number ���� �� ������ ������, ���� ������� ������
    -- ���� �� ��������� �� � char � number ��������������.
    -- ��� ����, ����� ����� �� ��������� � ������� ������� �� ��� �������.
    for r in (select lower(ut.column_name) as column_name,
                     case
                       when ut.data_type like '%NUMBER%' then
                        'number'
                       when ut.data_type like '%CHAR%' then
                        'string'
                       when ut.data_type like '%DATE%' then
                        'date'
                     end as data_type
                from all_tab_columns ut
               where table_name = upper(p_table_name)
                 and owner = upper(p_schema)
               order by ut.column_id asc) loop
      
      if upper(p_column_name) = upper(r.column_name) then
        v_sql := v_sql || lower(r.column_name) || ',';
        v_data_sql := v_data_sql || p_seq_name || '.nextval,';
      elsif p_data.exist(upper(r.column_name)) then
        v_sql := v_sql || lower(r.column_name) || ',';
        if r.data_type = 'string' then
          if rf_json_ext.get_string(p_data, upper(r.column_name)) is null and
             p_data.exist(upper(r.column_name)) then
            v_data_sql := v_data_sql || 'null,';
          else
            if p_data.exist(upper(r.column_name)) then
              v_data_sql := v_data_sql || '''' ||
                            replace(rf_json_ext.get_string(p_data,
                                                           upper(r.column_name)),
                                    '''',
                                    '''''') || ''',';
            end if;
          end if;
        else
          if r.data_type = 'number' then
            if rf_json_ext.get_number(p_data, upper(r.column_name)) is null and
               p_data.exist(upper(r.column_name)) then
              v_data_sql := v_data_sql || 'null,';
            else
              if p_data.exist(upper(r.column_name)) then
                v_data_sql := v_data_sql ||
                              rf_json_ext.get_number(p_data,
                                                     upper(r.column_name)) || ',';
              end if;
            end if;
          else
            if r.data_type = 'date' then
              if rf_json_ext.get_date(p_data, upper(r.column_name)) is null and
                 p_data.exist(upper(r.column_name)) then
                v_data_sql := v_data_sql || 'null,';
              else
                if p_data.exist(upper(r.column_name)) then
                  v_data_sql := v_data_sql || 'to_date('''||
                                to_char(rf_json_ext.get_date(p_data, upper(r.column_name)), 'DD-MM-YYYY HH24:MI:SS') ||
                                ''',''DD-MM-YYYY HH24:MI:SS''),';
                end if;
              end if;
            end if;
          end if;
        end if;
      end if;
    end loop;
  
    if p_column_name is not null then
      v_sql := rtrim(v_sql, ',') || ') ' || rtrim(v_data_sql, ',') ||
               ') returning ' || p_column_name || ' into v_id_tmp;';
    else
      v_sql := rtrim(v_sql, ',') || ') ' || rtrim(v_data_sql, ',') ||
              -- ���������� ������� ���-�� ����, � ���� ������ �� ������, �� ����� ����� "�� �����" -1
              -- ���� �������, ��� ��������� ��� ��������, �� � ���� �� ������?!
               ') returning ' || nvl(p_key_name, to_char(-1)) ||
               ' into v_id_tmp;';
    end if;
  
    v_sql := v_sql || chr(13) || '  :v_id_ := v_id_tmp;' || chr(13) ||
             'end;' || chr(13);
    --commit; -- ���������������� �������� �������� 28.09.2016
  
    return v_sql;
  
  end;
  --
  -- ������������ �������� ������ � �������� id �������
  --
  procedure log_request_error(p_errmsg clob, p_id number) is
    pragma autonomous_transaction;
  begin
    insert into mantas.rf_request_exception_log
      (id, exception_msg)
    values
      (p_id, p_errmsg);
    commit;
  end;
  --
  -- ������� ��������� �� ���� ��������� varchar2(4000) � ���������� ������ � ������� json
  -- � ����� ����������.
  --
  function exec_exception(p_errmsg varchar2) return clob is
    v_j    rf_json default null;
    v_clob clob default null;
  begin
    v_j := rf_json();
    v_j.put('success', false);
    v_j.put('message', p_errmsg);
    dbms_lob.createtemporary(v_clob, true);
    v_j.to_clob(v_clob, true);
    
    g_err_type_cd:='T';
    g_message_tx:=p_errmsg;

    return v_clob;
  end;
  --
  -- ������� ���������� ������� where �� ������ �������� ������ � p_data
  -- ���� ������� p_prefix - ����� ��������� ����� ������ ��������� ������� ������ p_prefix
  --
  function where_generate(p_data rf_json, p_prefix varchar2 default null)
    return clob is
    v_sql clob default null;
    v_tmp varchar2(100) default null;
  begin
    v_sql := ' where ';
    if not p_data.exist('filter') then
      return null;
    else
      for r in 1 .. to_number(rf_json_ext.get_json_list(p_data, 'filter')
                              .count) loop
        v_tmp := rf_json_ext.get_json_value(rf_json_ext.get_json(p_data,'filter[' || r ||']'),'value')
                 .get_type;
        v_sql := v_sql || case
                   when r > 1 then
                    ' and '
                 end || case
                   when p_prefix is not null then
                    p_prefix || '.'
                 end || rf_json_ext.get_string(rf_json_ext.get_json(p_data,
                                                                    'filter[' || r || ']'),
                                               'property') || ' = ';
        if v_tmp = 'number' then
          v_sql := v_sql || rf_json_ext.get_number(rf_json_ext.get_json(p_data,
                                                                        'filter[' || r || ']'),
                                                   'value');
        else
          v_sql := v_sql ||
                   get_string_or_date(rf_json_ext.get_string(rf_json_ext.get_json(p_data,
                                                                                  'filter[' || r || ']'),
                                                             'value'));
        end if;
      end loop;
    end if;
    return v_sql;
  end;
  --
  -- ������� ���������� ������� order by �� ������ �������� ������ � p_data
  -- ���� ������� p_prefix - ����� ��������� ����� ������ ��������� ������� ������ p_prefix
  --
  function orderby_generate(p_data rf_json, p_prefix varchar2 default null)
    return clob is
    v_sql clob default null;
  begin
    if not p_data.exist('sort') then
      return null;
    else
      v_sql := ' order by ';
      for r in 1 .. to_number(rf_json_ext.get_json_list(p_data, 'sort')
                              .count) loop
        v_sql := v_sql || case
                   when r > 1 then
                    ', '
                 end || case
                   when p_prefix is not null then
                    p_prefix || '.'
                 end || rf_json_ext.get_string(rf_json_ext.get_json(p_data,
                                                                    'sort[' || r || ']'),
                                               'property') || ' ' ||
                 rf_json_ext.get_string(rf_json_ext.get_json(p_data,
                                                             'sort[' || r || ']'),
                                        'direction');
      end loop;
    end if;
    return v_sql;
  end;
  --
  -- ������� ��������� ������� ������
  --
  function exec_sql_insert(p_sql      clob,
                           p_id_req   number,
                           p_id       varchar2,
                           p_clientid varchar2,
                           p_schema   varchar2 default null,
                           p_table    varchar2 default null,
                           p_id_name  varchar2 default null) return clob is
    v_j     rf_json default null;
    v_clob  clob default null;
    v_id_   varchar2(250) default null;
    v_data_type  varchar2(250);
    v_sql        varchar2(32000);
  begin
    dbms_lob.createtemporary(v_clob, true);
    execute immediate p_sql
      using out v_id_;
    v_j     := rf_json();
    if p_schema is not null and p_table is not null and
       p_id_name is not null and v_id_ is not null Then
      
      -- TO DO: ���������� �� ����� �������, ��� ������ �������������� �������� �� ���������
      select max(data_type)
        into v_data_type
        from all_tab_columns
       where owner = upper(trim(p_schema)) and
             table_name = upper(trim(p_table)) and
             column_name = upper(trim(p_id_name));
      
      if v_data_type in ('NUMBER', 'INTEGER') Then
        v_sql := 'select t.*, '''||p_clientid||''' as "#clientId#", '||
                         coalesce(v_id_, p_id, 'to_number(null)')||' as "_id_" '||
                   'from '||p_schema||'.'||p_table||' t '||
                  'where '||p_id_name||' = '||v_id_;
      else  
        v_sql := 'select t.*, '''||p_clientid||''' as "#clientId#", '||
                         ''''||coalesce(v_id_, p_id)||''' as "_id_" '||
                   'from '||p_schema||'.'||p_table||' t '||
                  'where '||p_id_name||' = '''||v_id_||'''';
      end if;
      
      v_j.put('data', rf_json_dyn.executeList(v_sql));
    end if;
    
    v_j.put('success', true);

    v_j.to_clob(v_clob, true);    
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '�� ������� �������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;

  --
  -- ������� ��������� ������ �� ��������� ������� � ���������� json
  --
  function exec_sql_update(p_sql        varchar2,
                           p_bind_param varchar2,
                           p_id_req     number,
                           p_schema     varchar2 default null, --��� �����
                           p_table      varchar2 default null, --��� �������
                           p_id_name    varchar2 default null, --��� �������� PK
                           p_id_type    varchar2 default null  --��� PK �������� 
                          ) return clob is
    v_j    rf_json default null;
    v_clob clob default null;
    v_sql  varchar2(32000);
    v_data_type varchar2(250);
  begin
    if p_bind_param is not null then
      execute immediate p_sql
        using p_bind_param;
    else
      execute immediate p_sql;
    end if;
    v_j := rf_json();
    
    if p_schema is not null and p_table is not null and p_id_name is not null and p_bind_param is not null then

      -- ���� ��� PK �������� �� �����, �������� ���������� ��� � all_tab_columns
      if p_id_type is null then
        select max(data_type) into v_data_type
          from all_tab_columns
         where owner = upper(trim(p_schema)) 
           and table_name = upper(trim(p_table)) 
           and column_name = upper(trim(p_id_name));

        else v_data_type := upper(p_id_type);       
      end if;
      
      -- ��������� sql ��� ����� data
      if v_data_type in ('NUMBER', 'INTEGER') then
        v_sql := 'select t.*, '||p_bind_param||' as "_id_" from '||p_schema||'.'||p_table||' t '||
                  'where '||p_id_name||' = '||p_bind_param;
      else  
        v_sql := 'select t.*, '''||p_bind_param||''' as "_id_" from '||p_schema||'.'||p_table||' t '||
                  'where '||p_id_name||' = '''||p_bind_param||'''';
      end if;
                
      v_j.put('data', rf_json_dyn.executeList(v_sql));
    end if;
    
    v_j.put('success', true);
    dbms_lob.createtemporary(v_clob, true);
    v_j.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '�� ������� �������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  --
  -- ������� ������� ������ �� ��������� ������� � ���������� json
  --
  function exec_sql_delete(p_sql varchar2, p_id_req number) return clob is
    v_j    rf_json default null;
    v_clob clob default null;
  begin
    execute immediate p_sql;
    v_j := rf_json();
    v_j.put('success', true);
    dbms_lob.createtemporary(v_clob, true);
    v_j.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '�� ������� ������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  --
  -- ������� ��������� ������� �� ������, ������������ ��� ������ �����.
  -- ������� ���������� ��������� ������� json.
  --
  function exec_sql_read(p_sql varchar2, p_id_req number) return clob is
    v_json      rf_json := null;
    v_json_list rf_json_list := null;
    v_clob      clob := null;
  begin
    v_json_list := rf_json_dyn.executelist( p_sql );
    v_json := rf_json();
    v_json.put( 'success', true );
    v_json.put( 'totalCount', v_json_list.count );
    v_json.put( 'data', v_json_list );
    dbms_lob.createtemporary(v_clob, true);
    v_json.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error( sqlerrm, p_id_req );
        return exec_exception('�� ������� ��������� ������!'||chr(13)||'��������� ���������� ������.'||chr(13)||sqlerrm);
      end;
  end;
  
  /* ����� ��������� ���������� exec_sql_read, �� ���������� json + ������ � ����������� ����������� */
  function exec_sql_read_json( p_sql in varchar2, p_id_request in number, p_binds rf_json default null ) return rf_json
  is
    v_json rf_json;
    v_json_list rf_json_list;
  begin
    /* ������ ������� ���������� */
    if p_binds is not null then
      /* ���� json c ����������� ���������������, ������� ����� ��� ���� ���� ���� �������� */
      if p_binds.count > 0 then
        v_json_list := rf_json_dyn.executelist( p_sql, p_binds );
      /* � �������� ������ ��������� ���������� ��� ���������� */  
      else
        v_json_list := rf_json_dyn.executelist( p_sql );
      end if;
    /* ���� json c ����������� �� ���������������, ��������� ���������� ��� ���������� */    
    else
      v_json_list := rf_json_dyn.executelist( p_sql );
    end if;  
    v_json := rf_json();
    v_json.put( 'success', true );
    v_json.put( 'totalCount', v_json_list.count );
    v_json.put( 'data', v_json_list );
    return v_json;
    
    exception
      when others then 
        log_request_error( sqlerrm, p_id_request );
        v_json := rf_json();
        v_json.put( 'success', false );
        v_json.put( 'message', '�� ������� ��������� ������! ��������� ���������� ������.'||sqlerrm() );
        return v_json;
  end;

  --
  -- ������� ��������� ������� �� ������, ������������ ��� ������ �����, �� � ��������� ������������ ������.
  -- ������� ���������� ��������� ������� json.
  -- � ������� ���������� ������� count(*) over() _cnt_ , �������� count �� ��������������
  --
  function exec_sql_limitread(p_sql        varchar2,
                              p_id_req     number,
                              p_param_list rf_json_list,
                              p_limit      number default 1000) return clob is
    v_jobj       rf_json_list default null;
    v_j          rf_json default null;
    v_tmp_j      rf_json default null;
    v_clob       clob default null;
    v_jparam     rf_json default null;
    v_tmp        varchar2(200) default null;
    v_tmp_name   varchar2(200) default null;
    v_tmp_value  varchar2(4000) default null;
    v_tmp_nvalue number default null;
    v_tmp_date   date;
  begin
    dbms_lob.createtemporary(v_clob, true);
    v_jparam := rf_json();
    v_jparam.put('lim', p_limit);
    p_param_list.to_clob(v_clob, true);
    v_tmp_j := rf_json('{"filter": ' || v_clob || '}');
    for r in 1 .. to_number(rf_json_ext.get_json_list(v_tmp_j, 'filter')
                            .count) loop
      v_tmp      := rf_json_ext.get_json_value(rf_json_ext.get_json(v_tmp_j,'filter[' || r ||']'),'value')
                    .get_type;
      v_tmp_name := rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                'filter[' || r || ']'),
                                           'property');
      if v_tmp <> 'null' then
        if v_tmp = 'number' then
          v_tmp_nvalue := rf_json_ext.get_number(rf_json_ext.get_json(v_tmp_j,
                                                                      'filter[' || r || ']'),
                                                 'value');
          if v_tmp_name = 'par_tb_id' and v_tmp_nvalue = 0 then
            v_jparam.put(v_tmp_name, rf_json_value.makenull);
          else
            v_jparam.put(v_tmp_name, v_tmp_nvalue);
          end if;
        else
          if is_date(rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                 'filter[' || r || ']'),
                                            'value')) then
            v_tmp_date := to_date(rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                              'filter[' || r || ']'),
                                                         'value'),
                                  'YYYY-MM-DD HH24:MI:SS');
            v_jparam.put(v_tmp_name,
                         rf_json_ext.to_date2(rf_json_ext.to_json_value(v_tmp_date)));
          else
            v_tmp_value := rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                       'filter[' || r || ']'),
                                                  'value');
            v_jparam.put(v_tmp_name, v_tmp_value);
          end if;
        end if;
      else
        -- ������� �������������� ����������� ���������� par_vers_status_cd/par_crit_status_cd � ACT
        -- ������ ���� ��������� find_opok/estimate_find_opok �������� ��, ���� ��� null 
        --if lower(v_tmp_name) in
        --   ('par_vers_status_cd', 'par_crit_status_cd') then
        --  v_jparam.put(v_tmp_name, 'ACT');
        --else
          v_jparam.put(v_tmp_name, rf_json_value.makenull);
        --end if;
      end if;
    
    end loop;
    v_jobj := rf_json_dyn.executeList(p_sql, v_jparam);
    v_j    := rf_json();
    v_j.put('success', true);
    v_j.put('data', v_jobj);
    v_j.put('totalCount',
            nvl(rf_json_ext.get_number(v_j, 'data[1]._cnt_'), 0));
    v_j.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '�� ������� ��������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  --
  -- ������� ��������� ������� �� ������, ������������ ����� ����� ������.
  -- ������� ���������� ��������� ������� json.
  --
  function exec_sql_pageread(p_sql    varchar2,
                             p_start  number default 0,
                             p_end    number default 50,
                             p_id_req number) return clob is
    v_jobj  rf_json_list default null;
    v_j     rf_json default null;
    v_clob  clob default null;
    v_count number default null;
  begin
    v_jobj := rf_json_dyn.executeList('select * from (' || p_sql ||
                                      ') where "v_$0" between :b1 and :b2',
                                      rf_json('{"b1":' || p_start ||
                                              ', "b2":' || p_end || '}'));
    execute immediate 'select count(*) cnt from (' || p_sql || ')'
      into v_count;
    v_j := rf_json();
    v_j.put('success', true);
    v_j.put('totalCount', nvl(v_count, 0));
    v_j.put('data', v_jobj);
    dbms_lob.createtemporary(v_clob, true);
    v_j.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '�� ������� ��������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  --
  -- ������� ��������� ������� �� ������, ������������ ������������ ������.
  -- ������� ���������� ��������� ������� json.
  --
  function exec_sql_singleread(p_sql    varchar2,
                               p_id     varchar2,
                               p_id_req number) return clob is
    v_jobj rf_json_list default null;
    v_j    rf_json default null;
    v_clob clob default null;
  begin
    -- � ������ ������������� �������, ������������� �������, �������� ID ������� ���� ���������� :id
    v_jobj := rf_json_dyn.executeList(p_sql,
                                      rf_json('{"id":' || p_id || '}'));
    v_j := rf_json();
    v_j.put('success', true);
    v_j.put('data', v_jobj);
    dbms_lob.createtemporary(v_clob, true);
    v_j.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '�� ������� ��������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  --
  -- ������� ��������� ���������� ������
  --
  function exec_estimate_opok(p_id_req number, p_param_list rf_json_list)
    return clob is
    v_tmp_j      rf_json default null;
    v_clob       clob default null;
    v_jparam     rf_json default null;
    v_tmp        varchar2(200) default null;
    v_tmp_name   varchar2(200) default null;
    v_tmp_value  varchar2(4000) default null;
    v_tmp_nvalue number default null;
    v_tmp_date   date;
    v_cnt        number default 0;
  begin
    dbms_lob.createtemporary(v_clob, true);
    v_jparam := rf_json();
    p_param_list.to_clob(v_clob, true);
    v_tmp_j := rf_json('{"filter": ' || v_clob || '}');
    for r in 1 .. to_number(rf_json_ext.get_json_list(v_tmp_j, 'filter')
                            .count) loop
      v_tmp      := rf_json_ext.get_json_value(rf_json_ext.get_json(v_tmp_j,'filter[' || r ||']'),'value')
                    .get_type;
      v_tmp_name := rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                'filter[' || r || ']'),
                                           'property');
      if v_tmp <> 'null' then
        if v_tmp = 'number' then
          v_tmp_nvalue := rf_json_ext.get_number(rf_json_ext.get_json(v_tmp_j,
                                                                      'filter[' || r || ']'),
                                                 'value');
          if v_tmp_name = 'par_tb_id' and v_tmp_nvalue = 0 then
            v_jparam.put(v_tmp_name, rf_json_value.makenull);
          else
            v_jparam.put(v_tmp_name, v_tmp_nvalue);
          end if;
        else
          if is_date(rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                 'filter[' || r || ']'),
                                            'value')) then
            v_tmp_date := to_date(rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                              'filter[' || r || ']'),
                                                         'value'),
                                  'YYYY-MM-DD HH24:MI:SS');
            v_jparam.put(v_tmp_name,
                         rf_json_ext.to_date2(rf_json_ext.to_json_value(v_tmp_date)));
          else
            v_tmp_value := rf_json_ext.get_string(rf_json_ext.get_json(v_tmp_j,
                                                                       'filter[' || r || ']'),
                                                  'value');
            v_jparam.put(v_tmp_name, v_tmp_value);
          end if;
        end if;
      else
        -- ������� �������������� ����������� ���������� par_vers_status_cd/par_crit_status_cd � ACT
        -- ������ ���� ��������� find_opok/estimate_find_opok �������� ��, ���� ��� null 
        --if lower(v_tmp_name) in
        --   ('par_vers_status_cd', 'par_crit_status_cd') then
        --  v_jparam.put(v_tmp_name, 'ACT');
        --else
          v_jparam.put(v_tmp_name, rf_json_value.makenull);
        --end if;
      end if;
    
    end loop;
    v_cnt := mantas.rf_pkg_scnro.estimate_find_opok(par_sdate          => rf_json_ext.get_string(v_jparam,
                                                                                                 'par_sdate'),
                                                    par_edate          => rf_json_ext.get_string(v_jparam,
                                                                                                 'par_edate'),
                                                    par_tb_id          => rf_json_ext.get_number(v_jparam,
                                                                                                 'par_tb_id'),
                                                    par_rule_seq_id    => rf_json_ext.get_number(v_jparam,
                                                                                                 'par_rule_seq_id'),
                                                    par_opok_nb        => rf_json_ext.get_number(v_jparam,
                                                                                                 'par_opok_nb'),
                                                    par_query_cd       => rf_json_ext.get_number(v_jparam,
                                                                                                 'par_query_cd'),
                                                    par_vers_status_cd => rf_json_ext.get_string(v_jparam,
                                                                                                 'par_vers_status_cd'),
                                                    par_crit_status_cd => rf_json_ext.get_string(v_jparam,
                                                                                                 'par_crit_status_cd'));
  
    v_jparam := rf_json();
    v_jparam.put('success', true);
    v_jparam.put('count', nvl(v_cnt, 0));
    v_jparam.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '�� ������� ��������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  --
  -- ��������� json ��� ������� rf_rule_snapshot � treegrid �� �������
  --
  function exec_ruleshapshot(v_review number,
                             v_repeat number,
                             p_id_req number) return clob is
    v_clob     clob default null;
    v_tmp_clob clob default null;
    v_json     rf_json default null;
    v_mini     clob default null;
    v_mini2    clob default null;
    v_mini3    clob default null;
    v_data     rf_json default null;
    v_tmpjson  rf_json default null;
    v_id       number;
    v_ruletxt  varchar2(2000);
    v_nbtxt    varchar2(2000);
  
    function take_record(v_mini    clob,
                         v_mini2   clob,
                         v_mini3   clob,
                         v_ruletxt varchar2,
                         v_nbtxt   varchar2) return clob is
      v_data     rf_json default null;
      v_clob     clob default null;
      v_tmp_clob clob default null;
      v_tmini    clob default null;
      v_tmini2   clob default null;
      v_tmini3   clob default null;
    
    begin
      dbms_lob.createtemporary(v_clob, true);
      if v_mini is not null then
        v_data  := rf_json();
        v_tmini := '[' || v_mini || ']';
        v_data.put('COLUMN_NAME', '�������� ����������:');
        v_data.put('COLUMN_VALUE', '');
        v_data.put('expanded', true);
        v_data.put('children', rf_json_list(v_tmini));
        v_data.to_clob(v_clob, true);
        v_tmp_clob := v_clob;
      end if;
      if v_mini2 is not null then
        v_data   := rf_json();
        v_tmini2 := '[' || v_mini2 || ']';
        v_data.put('COLUMN_NAME', '������ ����');
        v_data.put('COLUMN_VALUE', '');
        v_data.put('expanded', true);
        v_data.put('children', rf_json_list(v_tmini2));
        v_data.to_clob(v_clob, true);
        v_tmp_clob := case
                        when v_tmp_clob is not null then
                         v_tmp_clob || ','
                        else
                         null
                      end || v_clob;
      end if;
      if v_mini3 is not null then
        v_data   := rf_json();
        v_tmini3 := '[' || v_mini3 || ']';
        v_data.put('COLUMN_NAME',
                   '���������� ������� �� �������');
        v_data.put('COLUMN_VALUE', '');
        v_data.put('expanded', true);
        v_data.put('children', rf_json_list(v_tmini3));
        v_data.to_clob(v_clob, true);
        v_tmp_clob := case
                        when v_tmp_clob is not null then
                         v_tmp_clob || ','
                        else
                         null
                      end || v_clob;
      end if;
      if v_tmp_clob is not null then
        v_tmp_clob := '[' || v_tmp_clob || ']';
      
        v_tmp_clob := '{ ' || chr(13) || '"COLUMN_NAME": "' || v_ruletxt ||
                      '", ' || chr(13) || '"COLUMN_VALUE": "' || v_nbtxt ||
                      '", ' || chr(13) || '"expanded": true,' || chr(13) ||
                      '"children": ' || v_tmp_clob || chr(13) || '}';
      end if;
      return v_tmp_clob;
    end;
  begin
    v_json    := rf_json();
    v_data    := rf_json();
    v_tmpjson := rf_json();
    v_json.put('success', true);
    dbms_lob.createtemporary(v_clob, true);
    for r in (select rn, z."_id_" as id, z.column_name, z.column_value
                from (select snapshot_id as "_id_",
                             case rn
                               when 1 then
                                'SNAPSHOT_ID'
                               when 2 then
                                'RULE_SEQ_ID'
                               when 3 then
                                'RECORD_SEQ_ID'
                               when 4 then
                                'ACTUAL_FL'
                               when 5 then
                                'SNAPSHOT_TIME'
                               when 6 then
                                'OPOK_NB'
                               when 7 then
                                'QUERY_CD'
                               when 8 then
                                'PRIORITY_NB'
                               when 9 then
                                'LIMIT_AM'
                               when 10 then
                                'TB_ID'
                               when 11 then
                                'OP_CAT_CD'
                               when 12 then
                                'DBT_CDT_CD'
                               when 13 then
                                'ACCT_CRIT_TX'
                               when 14 then
                                'ACCT_SUBTLE_CRIT_TX'
                               when 15 then
                                'ACCT_AREA_TX'
                               when 16 then
                                'ACCT_NULL_FL'
                               when 17 then
                                'ACCT2_CRIT_TX'
                               when 18 then
                                'ACCT2_SUBTLE_CRIT_TX'
                               when 19 then
                                'ACCT2_AREA_TX'
                               when 20 then
                                'ACCT2_NULL_FL'
                               when 21 then
                                'ACCT_REVERSE_FL'
                               when 22 then
                                'LEXEME_PURPOSE_TX'
                               when 23 then
                                'LEXEME_CUST_TX'
                               when 24 then
                                'LEXEME_CUST2_TX'
                               when 25 then
                                'LEXEME2_PURPOSE_TX'
                               when 26 then
                                'LEXEME2_CUST_TX'
                               when 27 then
                                'LEXEME2_CUST2_TX'
                               when 28 then
                                'OPTP_CRIT_TX'
                               when 29 then
                                'SRC_CRIT_TX'
                               when 30 then
                                'CUSTOM_CRIT_SQL_TX'
                               when 31 then
                                'CUSTOM_CRIT_PARAM_TX'
                               when 32 then
                                'EXPLANATION_SQL_TX'
                               when 33 then
                                'START_DT'
                               when 34 then
                                'END_DT'
                               when 35 then
                                'NOTE_TX'
                             end column_name,
                             case rn
                               when 1 then
                                to_char(snapshot_id)
                               when 2 then
                                to_char(rule_seq_id)
                               when 3 then
                                to_char(record_seq_id)
                               when 4 then
                                to_char(actual_fl)
                               when 5 then
                                to_char(snapshot_time, 'DD-MM-YYYY HH24:MI:SS')
                               when 6 then
                                to_char(opok_nb)
                               when 7 then
                                to_char(query_cd)
                               when 8 then
                                to_char(priority_nb)
                               when 9 then
                                trim(to_char(limit_am,
                                             '999,999,999,999,999,999,999.99'))
                               when 10 then
                                to_char(nullif(tb_id, 0))
                               when 11 then
                                to_char(op_cat_cd)
                               when 12 then
                                to_char(dbt_cdt_cd)
                               when 13 then
                                to_char(acct_crit_tx)
                               when 14 then
                                to_char(acct_subtle_crit_tx)
                               when 15 then
                                to_char(acct_area_tx)
                               when 16 then
                                to_char(acct_null_fl)
                               when 17 then
                                to_char(acct2_crit_tx)
                               when 18 then
                                to_char(acct2_subtle_crit_tx)
                               when 19 then
                                to_char(acct2_area_tx)
                               when 20 then
                                to_char(acct2_null_fl)
                               when 21 then
                                to_char(acct_reverse_fl)
                               when 22 then
                                to_char(lexeme_purpose_tx)
                               when 23 then
                                to_char(lexeme_cust_tx)
                               when 24 then
                                to_char(lexeme_cust2_tx)
                               when 25 then
                                to_char(lexeme2_purpose_tx)
                               when 26 then
                                to_char(lexeme2_cust_tx)
                               when 27 then
                                to_char(lexeme2_cust2_tx)
                               when 28 then
                                to_char(optp_crit_tx)
                               when 29 then
                                to_char(src_crit_tx)
                               when 30 then
                                to_char(custom_crit_sql_tx)
                               when 31 then
                                to_char(custom_crit_param_tx)
                               when 32 then
                                to_char(explanation_sql_tx)
                               when 33 then
                                to_char(start_dt, 'DD-MM-YYYY HH24:MI:SS')
                               when 34 then
                                to_char(end_dt, 'DD-MM-YYYY HH24:MI:SS')
                               when 35 then
                                to_char(note_tx)
                             end as column_value,
                             rn
                        from mantas.rf_opok_rule_snapshot
                       cross join (select level as rn
                                    from dual
                                  connect by level <= 35)
                       where snapshot_id in
                             (select snapshot_id
                                from mantas.rf_kdd_review_opok
                               where review_id = v_review
                                 and repeat_nb = v_repeat)) z
               where z.column_value is not null
               order by "_id_", rn) loop
      if r.rn = 2 then
        select '����: ' || od.opok_nb || ', �������: ' || od.rule_cd ||
               ', ������: ' || od.vers_number,
               od.note_tx
          into v_ruletxt, v_nbtxt
          from mantas.rf_opok_rule od
         where od.rule_seq_id = r.column_value;
      end if;
    
      if (v_id <> r.id) and (v_id is not null) then
        v_id       := r.id;
        v_tmp_clob := case
                        when v_tmp_clob is null then
                         v_tmp_clob
                        else
                         v_tmp_clob || ','
                      end ||
                      take_record(v_mini, v_mini2, v_mini3, v_ruletxt, v_nbtxt);
        v_mini     := '';
        v_mini2    := '';
        v_mini3    := '';
        v_ruletxt  := '';
        v_nbtxt    := '';
      else
        if v_id is null then
          v_id := r.id;
        end if;
      end if;
    
      if ((r.rn > 15) and (r.rn < 21)) then
        v_tmpjson.put('COLUMN_NAME', r.column_name);
        v_tmpjson.put('COLUMN_VALUE', r.column_value);
        v_tmpjson.put('leaf', true);
        v_mini2 := case
                     when v_mini2 is not null then
                      v_mini2 || ','
                     else
                      null
                   end || v_tmpjson.to_char;
      else
        if ((r.rn > 21) and (r.rn < 29)) then
          v_tmpjson.put('COLUMN_NAME', r.column_name);
          v_tmpjson.put('COLUMN_VALUE', r.column_value);
          v_tmpjson.put('leaf', true);
          v_mini3 := case
                       when v_mini3 is not null then
                        v_mini3 || ','
                       else
                        null
                     end || v_tmpjson.to_char;
        else
          v_tmpjson.put('COLUMN_NAME', r.column_name);
          v_tmpjson.put('COLUMN_VALUE', r.column_value);
          v_tmpjson.put('leaf', true);
          v_mini := case
                      when v_mini is not null then
                       v_mini || ','
                      else
                       null
                    end || v_tmpjson.to_char;
        end if;
      end if;
    end loop;
    v_tmp_clob := case
                    when v_tmp_clob is null then
                     v_tmp_clob
                    else
                     v_tmp_clob || ','
                  end ||
                  take_record(v_mini, v_mini2, v_mini3, v_ruletxt, v_nbtxt);
    if v_tmp_clob is not null then
      v_tmp_clob := '[' || v_tmp_clob || ']';
    end if;
    v_json.put('data', rf_json_list(v_tmp_clob));
    v_json.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '��� ������������ ������ � �������� ���������� ��������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������.' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  --
  -- ���������� ������� � �������������� ���� ����
  --
  function exec_set_opok(p_id      number,
                         p_opoknb  number,
                         p_dopoknb varchar2,
                         p_user    varchar2,
                         p_id_req  number) return clob is
    v_clob           clob default null;
    v_json           rf_json default null;
    v_dataj          rf_json default null;
    v_list           rf_json_list default null;
    v_review_ids     mantas.rf_tab_number;
    v_act_review_ids mantas.rf_tab_number;
    v_action         mantas.rf_tab_varchar;
  begin
    v_json  := rf_json();
    v_dataj := rf_json();
    v_list  := rf_json_list();
    v_json.put('success', true);
    v_action     := mantas.rf_tab_varchar('RF_SETOPOK');
    v_review_ids := mantas.rf_tab_number(p_id);
    v_clob       := mantas.rf_pkg_review.do_actions(par_review_ids          => v_review_ids,
                                                    par_actvy_type_cds      => v_action,
                                                    par_new_review_owner_id => null,
                                                    par_due_dt              => null,
                                                    par_note_tx             => null,
                                                    par_owner_id            => p_user,
                                                    par_cmmnt_id            => null,
                                                    par_opok_nb             => p_opoknb,
                                                    par_add_opoks_tx        => p_dopoknb,
                                                    par_act_review_ids      => v_act_review_ids);
  
    for r in 0 .. (v_act_review_ids.count - 1) loop
      v_dataj.put('id', v_act_review_ids(r + 1));
      v_list.append(v_dataj.to_json_value);
    end loop;
    v_json.put('data', v_list);
    v_json.put('message', v_clob);
    v_clob := '';
    dbms_lob.createtemporary(v_clob, true);
    v_json.to_clob(v_clob, true);
    return v_clob;
  exception
    when others then
      begin
        log_request_error(sqlerrm, p_id_req);
        return exec_exception(p_errmsg => '��� ��������� ���� ���� ��������� ������!' ||
                                          chr(13) ||
                                          '��������� ���������� ������ ��� ��������� ������!' ||
                                          chr(13) || sqlerrm);
      end;
  end;
  
  -- 
  -- �������� ������������� �������� ������� ������
  --
  FUNCTION get_current_audit_event_id
    RETURN mantas.rf_audit_event.audit_seq_id%TYPE IS -- ID ������ � ������� ������
    BEGIN
      RETURN g_audit_id;
    END;
  
  --
  -- ���������� ������
  --
  function process_request(par_request in clob, -- �������� ������ � ������� json
                           par_user    in varchar2, -- ��� �������� ������������
                           par_ip_addr in varchar2 DEFAULT '0.0.0.0' --   IP ������ �������� ������������
                           ) return clob is
    -- ����� �� ������ � ������� json
    var_clob       clob default null;
    v_jobj         rf_json default null;
    v_json         rf_json default null;
    v_id_request   number default null;
    v_formname     varchar2(100) default null;
    v_sql          varchar2(32000) default null;
    v_success      BOOLEAN;
    v_message      rf_audit_event.message_tx%TYPE;
  begin
    -- ������� �������� ��������� form � ���� json �� �������� ��� ����� ������� ��� �� ��������
    -- ���� ��������, �� �������� ������ � ���������� � ���������� ���������
    begin
      -- ���������� ���������
      dbms_session.set_context('CLIENTCONTEXT', 'AML_USER', par_user);
      dbms_session.set_context('CLIENTCONTEXT', 'AML_IP_ADDRESS', par_ip_addr);
      dbms_session.set_context('CLIENTCONTEXT', 'AML_BATCH_DATE', NULL);
      dbms_session.set_context('CLIENTCONTEXT', 'AML_ACTVY_TYPE_CD', NULL);
      g_err_type_cd:=NULL;
      g_message_tx:=NULL;

      begin
        v_jobj     := rf_json(par_request);
        v_formname := rf_json_ext.get_string(v_jobj, 'form');
      exception
        when others then
          var_clob := exec_exception(p_errmsg => '�� ������� ��������� ������!' ||
                                                 chr(13) ||
                                                 '��������� ���������� ������.');
          -- ����������� �������
          insert into mantas.rf_request_log
            (id, request, username, formname)
          values
            (mantas.rf_request_log_seq.nextval,
             par_request,
             par_user,
             v_formname)
          returning id into v_id_request;
          commit;
          raise;
      end;
      
      -- ������ � ������� � �����
      g_audit_id:=rf_pkg_audit.record_gui_event(par_user, par_ip_addr, v_jobj);
      
      -- ����������� �������
      insert into mantas.rf_request_log
        (id, request, username, formname)
      values
        (mantas.rf_request_log_seq.nextval,
         par_request,
         par_user,
         v_formname)
      returning id into v_id_request;
      commit;
    end;
    --
    --
    -- ������ ������� �������� ������� (MANTAS.RF_FUNCTION_OBJECT_MAP � ��.)
    --
    --
    if rf_json_ext.get_string(v_jobj, 'form') = 'ObjectsAccess' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_sql_read('SELECT ' ||
                                  'fo.object_code as "_id_", ' ||
                                  'fo.object_code as OBJECT_CODE, ' ||
                                  'SUBSTR(min(DECODE(fo.access_mode,''F'',''1F'',''T'',''2T'',''R'',''3R'')),2) as ACCESS_MODE ' ||
                                  'FROM ofsconf.cssms_usr_group_map ug ' ||
                                  'join ofsconf.cssms_group_role_map gr on gr.v_group_code = ug.v_group_code ' ||
                                  'join ofsconf.cssms_role_function_map rf on rf.v_role_code = gr.v_role_code ' ||
                                  'join mantas.rf_function_object_map fo on fo.v_function_code = rf.v_function_code ' ||
                                  'WHERE ug.v_usr_id = ''' || par_user || ''' '||
                                  'GROUP BY fo.object_code',
                                  v_id_request);
      end if;
    end if;  

    --
    --
    -- ��������� BUSINESS.ORG
    --
    --
    if ((rf_json_ext.get_string(v_jobj, 'form') = 'OPOKRuleTB') or
       (rf_json_ext.get_string(v_jobj, 'form') = 'TB')) then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_sql_read('select rownum as "v_$0", x.tb_id as "_id_", x.* from (select to_number(0) as tb_id, ''��� ��������'' as tb_name from dual ' ||
                                  chr(13) || ' union all ' || chr(13) ||
                                  ' select to_number(org_intrl_id) as tb_id, org_nm as tb_name from business.org where org_type_cd = ''��'') x order by x.tb_id asc',
                                  v_id_request);
      end if;
    end if;
  
    --
    --
    -- ��������� MANTAS.RF_PKG_SCNRO.FIND_OPOK
    --
    --
    if rf_json_ext.get_string(v_jobj, 'form') = 'OPOKTest' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        if not v_jobj.exist('filter') then
          rf_pkg_audit.set_event_result(g_audit_id,'T','��������� ���������� ������! ��������� ������� ���������!');                  
          -- ������� ����������
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_USER');
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_IP_ADDRESS');
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_BATCH_DATE');
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_ACTVY_TYPE_CD');
          g_audit_id:=NULL;

          return exec_exception('��������� ���������� ������!' || chr(13) ||
                                '��������� ������� ���������!');
        end if;
        var_clob := exec_sql_limitread('select * from (select ' ||
                                       'count(*) over () "_cnt_", ' ||
                                       chr(13) || 'TRXN_SEQ_ID, ' ||
                                       chr(13) || -- ID ��������
                                       'case OP_CAT_CD ' || chr(13) ||
                                       '  when ''WT'' then ''�/���'' ' ||
                                       chr(13) ||
                                       '  when ''CT'' then ''���'' ' ||
                                       chr(13) ||
                                       '  when ''BOT'' then ''�����'' ' ||
                                       chr(13) || 'end as OP_CAT_CD, ' ||
                                       chr(13) || -- ���/����
                                       '(select listagg(regexp_substr(column_value, ''[^\~]+'', 1, 1), '', '') within group(order by regexp_substr(column_value, ''[^\~]+'', 1, 1))  ' ||
                                       chr(13) ||
                                       '   from table(mantas.rf_pkg_scnro.list_to_tab(OPOK_LIST, ''|''))) as OPOK_LIST,    ' ||
                                       chr(13) || -- ���� ��.
                                       'TRXN_DT, ' || chr(13) || -- ���� ��������
                                       'TRXN_BASE_AM, ' || chr(13) || -- ����� � ������
                                       'TRXN_CRNCY_AM, ' || chr(13) || -- ����� � ������
                                       'TRXN_CRNCY_CD, ' || chr(13) || -- ������
                                       'TRXN_DESC, ' || chr(13) || -- ���������� �������
                                       'ORIG_NM,  ' || chr(13) || -- ������������ �����������
                                       'ORIG_INN_NB, ' || chr(13) || -- ��� �����������
                                       'business.rf_pkg_util.format_account(nullif(ORIG_ACCT_NB, ''-1'')) as ORIG_ACCT_NB,  ' ||
                                       chr(13) || -- ���� �����������
                                       'ORIG_OWN_FL,  ' || chr(13) || -- ���������� - ������ �� ��
                                       'business.rf_pkg_util.format_account(DEBIT_CD) as DEBIT_CD,  ' ||
                                       chr(13) || -- ���� ������
                                       'SEND_INSTN_NM,  ' || chr(13) || -- ���� �����������
                                       'SEND_INSTN_ID,  ' || chr(13) || -- ��� ����� �����������
                                       'business.rf_pkg_util.format_account(SEND_INSTN_ACCT_ID) as SEND_INSTN_ACCT_ID,   ' ||
                                       chr(13) || -- ����. ���� ����� �����������
                                       'BENEF_NM,  ' || chr(13) || -- ����������
                                       'BENEF_INN_NB,  ' || chr(13) || -- ��� ����������
                                       'business.rf_pkg_util.format_account(nullif(BENEF_ACCT_NB, ''-1'')) as BENEF_ACCT_NB,  ' ||
                                       chr(13) || -- ���� ����������
                                       'BENEF_OWN_FL,  ' || chr(13) || -- ���������� - ������ �� ��
                                       'business.rf_pkg_util.format_account(CREDIT_CD) as CREDIT_CD,  ' ||
                                       chr(13) || -- ���� �������
                                       'RCV_INSTN_NM,  ' || chr(13) || -- ���� ����������
                                       'RCV_INSTN_ID,   ' || chr(13) || -- ��� ����� ����������
                                       'business.rf_pkg_util.format_account(RCV_INSTN_ACCT_ID) as RCV_INSTN_ACCT_ID,   ' ||
                                       chr(13) || -- ����. ���� ����� ����������
                                       '(select listagg(regexp_substr(column_value, ''[^\~]+'', 1, 4), ''; '') within group(order by regexp_substr(column_value, ''[^\~]+'', 1, 1))  ' ||
                                       chr(13) ||
                                       '   from table(mantas.rf_pkg_scnro.list_to_tab(OPOK_LIST, ''|''))) as explanation_tx,  ' ||
                                       chr(13) || -- ������� ���������
                                       'trim(to_char(trxn.NSI_OP_ID)||'' ''||optp.op_type_nm) as NSI_OP,   ' ||
                                       chr(13) || -- ��� �������� (��� ���)
                                       'SCRTY_BASE_AM,  ' || chr(13) || -- ������� ����. ������
                                       'SCRTY_ID,  ' || chr(13) || -- ����� ������ ������
                                       'SCRTY_TYPE_CD,  ' || chr(13) || -- ��� ����. ������
                                       'TRXN_SCRTY_SEQ_ID,  ' || chr(13) || -- ID ����. ������
                                       'TB_ID,  ' || chr(13) || -- ����� ��
                                       'OSB_ID,  ' || chr(13) || -- ����� ���
                                       'DATA_DUMP_DT,  ' || chr(13) || -- ���� ��������
                                       'SRC_SYS_CD,  ' || chr(13) || -- �������-��������
                                       'SRC_CHANGE_DT,  ' || chr(13) || -- ���� ��������� � ���������
                                       'SRC_USER_NM,  ' || chr(13) || -- ������������
                                       'SRC_CD,  ' || chr(13) || -- ��� � ������� ���������
                                       '(select listagg(mantas.rf_pkg_scnro.get_rule_lexeme_list(regexp_substr(column_value, ''[^\~]+'', 1, 2), ''��'', :par_crit_status_cd), ''|'' ' ||
                                       ') within group(order by regexp_substr(column_value, ''[^\~]+'', 1, 1), regexp_substr(column_value, ''[^\~]+'', 1, 2))   ' ||
                                       chr(13) ||
                                       '   from table(mantas.rf_pkg_scnro.list_to_tab(OPOK_LIST, ''|'' ' ||
                                       '))) as TRXN_DESC_LEX,   ' ||
                                       chr(13) || -- ������� ���������� �������
                                       '(select listagg(mantas.rf_pkg_scnro.get_rule_lexeme_list(regexp_substr(column_value, ''[^\~]+'', 1, 2), ''�'', :par_crit_status_cd), ''|'' ' ||
                                       ') within group(order by regexp_substr(column_value, ''[^\~]+'', 1, 1), regexp_substr(column_value, ''[^\~]+'', 1, 2))   ' ||
                                       chr(13) ||
                                       '   from table(mantas.rf_pkg_scnro.list_to_tab(OPOK_LIST, ''|'' ' ||
                                       '))) as ORIG_LEX,   ' || chr(13) || -- ������� �����������
                                       '(select listagg(mantas.rf_pkg_scnro.get_rule_lexeme_list(regexp_substr(column_value, ''[^\~]+'', 1, 2), ''�2'', :par_crit_status_cd), ''|'' ' ||
                                       ') within group(order by regexp_substr(column_value, ''[^\~]+'', 1, 1), regexp_substr(column_value, ''[^\~]+'', 1, 2))   ' ||
                                       chr(13) ||
                                       '    from table(mantas.rf_pkg_scnro.list_to_tab(OPOK_LIST, ''|'' ' ||
                                       '))) as BENEF_LEX    ' || chr(13) || -- ������� ����������
                                       'from table(mantas.rf_pkg_scnro.find_opok(par_sdate => :par_sdate, ' ||
                                       chr(13) ||
                                       'par_edate => :par_edate, par_tb_id => :par_tb_id, par_rule_seq_id => :par_rule_seq_id ' ||
                                       ',par_opok_nb => :par_opok_nb, par_query_cd => :par_query_cd, par_vers_status_cd => :par_vers_status_cd, par_crit_status_cd => :par_crit_status_cd ' ||
                                       ')) trxn  ' || chr(13) ||
                                       '    left join business.rf_nsi_op_types optp on optp.nsi_op_id = trxn.nsi_op_id  ' ||
                                       chr(13) || ' ) where rownum <= :lim',
                                       v_id_request,
                                       rf_json_ext.get_json_list(v_jobj,
                                                                 'filter'),
                                       1000);
      end if;
      if rf_json_ext.get_string(v_jobj, 'action') = 'estimate' then
        if not v_jobj.exist('filter') then
          rf_pkg_audit.set_event_result(g_audit_id,'T','��������� ���������� ������! ��������� ������� ���������!');                  
          -- ������� ����������
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_USER');
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_IP_ADDRESS');
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_BATCH_DATE');
          dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_ACTVY_TYPE_CD');
          g_audit_id:=NULL;
          g_err_type_cd:=NULL;
          g_message_tx:=NULL;
          return exec_exception('��������� ���������� ������!' || chr(13) ||
                                '��������� ������� ���������!');
        end if;
        var_clob := exec_estimate_opok(v_id_request,
                                       rf_json_ext.get_json_list(v_jobj,
                                                                 'filter'));
      
      end if;
    end if;
  
    --
    --
    -- ��������� MANTAS.KDD_QUEUE_MASTER
    --
    --
    if rf_json_ext.get_string(v_jobj, 'form') = 'AlertsFiltersCollection' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
      
        var_clob := exec_sql_read(' select rownum             as "v_$0", ' ||
                                  chr(13) ||
                                  ' z.queue_seq_id     as "_id_", ' ||
                                  chr(13) || ' z.queue_display_nm, ' ||
                                  chr(13) ||
                                  ' z.is_default       as is_default ' ||
                                  chr(13) || ' from ' || chr(13) ||
                                  ' (select distinct k.*, ' || chr(13) ||
                                  '                  case ' || chr(13) ||
                                  '                    when k.queue_seq_id = 9001 then ' ||
                                  chr(13) || '                     1 ' ||
                                  chr(13) || '                    else ' ||
                                  chr(13) || '                     0 ' ||
                                  chr(13) ||
                                  '                  end as is_default ' ||
                                  chr(13) ||
                                  '    from mantas.kdd_queue_master k ' ||
                                  chr(13) ||
                                  '    join mantas.kdd_queue_role_map l ' ||
                                  chr(13) ||
                                  '      on l.queue_seq_id = k.queue_seq_id ' ||
                                  chr(13) ||
                                  '   where k.queue_type = ''AL'' ' ||
                                  chr(13) ||
                                  '     and mantas.rf_pkg_adm.has_role(par_owner_id  => ''' ||
                                  par_user || ''',  ' || chr(13) ||
                                  '                                    par_role_code => l.role_cd) = 1 ' ||
                                  chr(13) ||
                                  '   order by k.queue_display_nm asc) z ',
                                  v_id_request);
      end if;
      -- action 'getDefault' ������������ ��� ��������� �������
      -- �� ����� ����� "������" ������ � ��� ��������� � else
      -- � ���� ����� ������� ������� � ���, ��� ��� � ��������� ��������� ������ �� isDefault
      -- �� � � ��������� ���� ����� ������ ��� ������,
      -- �� ���� ����� �������������� ���� is_default �� action read, �� ��� ����� ���� ������ ������������
      if rf_json_ext.get_string(v_jobj, 'action') = 'getDefault' then
        var_clob := exec_sql_read(' select rownum             as "v_$0", ' ||
                                  chr(13) ||
                                  ' z.queue_seq_id     as "_id_", ' ||
                                  chr(13) || ' z.queue_display_nm, ' ||
                                  chr(13) ||
                                  ' z.is_default       as is_default ' ||
                                  chr(13) || ' from ' || chr(13) ||
                                  ' (select distinct k.*, ' || chr(13) ||
                                  '                  case ' || chr(13) ||
                                  '                    when k.queue_seq_id = 9001 then ' ||
                                  chr(13) || '                     1 ' ||
                                  chr(13) || '                    else ' ||
                                  chr(13) || '                     0 ' ||
                                  chr(13) ||
                                  '                  end as is_default ' ||
                                  chr(13) ||
                                  '    from mantas.kdd_queue_master k ' ||
                                  chr(13) ||
                                  '    join mantas.kdd_queue_role_map l ' ||
                                  chr(13) ||
                                  '      on l.queue_seq_id = k.queue_seq_id ' ||
                                  chr(13) ||
                                  '   where k.queue_type = ''AL'' ' ||
                                  chr(13) ||
                                  '     and mantas.rf_pkg_adm.has_role(par_owner_id  => ''' ||
                                  par_user || ''',  ' || chr(13) ||
                                  '                                    par_role_code => l.role_cd) = 1 ' ||
                                  chr(13) ||
                                  '   order by k.queue_display_nm asc) z where z.is_default = 1 ',
                                  v_id_request);
        v_json   := rf_json(var_clob);
        rf_json_ext.put(v_json,
                        '_id_',
                        rf_json_ext.get_number(v_json, 'data[1]._id_'));
        rf_json_ext.remove(v_json, 'data');
        rf_json_ext.remove(v_json, 'totalCount');
        rf_json_ext.put(v_json, 'sheep', '(sheep)');
        rf_json_ext.put(v_json, 'dog', '(dog)');
        rf_json_ext.put(v_json, 'cat', '(cat)');
        v_json.to_clob(var_clob, true);
      end if;
    end if;
  

    /* TODO ����������� Elsif. �������������� json to clob ������� � ����� ����� � ����� ������ */
    /* ���������� Alert list, Alert actions */
    if v_formname = 'AlertList' or 
       v_formname = 'AlertAction' or
       v_formname = 'AlertRuleSnapshot' or
       v_formname = 'AlertsStatistics' or
       v_formname = 'AlertHistory' OR
       v_formname = 'AlertAttachments' or
       v_formname = 'AlertNarrative' or
       v_formname = 'GOZChanges' then
      v_json := rf_pkg_request_alerts.process_request( par_request, par_user, v_id_request, var_clob );
      if var_clob is null then
        dbms_lob.createtemporary( var_clob, true );
        v_json.to_clob( var_clob, true );
      end if;
    end if;
    
    /* ���������� trxnList, trxnAction */
    if v_formname = 'TrxnList' or 
       v_formname = 'TrxnAction' then
      v_json := rf_pkg_request_trxn_search.process_request( par_request, par_user, v_id_request, var_clob );
      if var_clob is null then
        dbms_lob.createtemporary( var_clob, true );
        v_json.to_clob( var_clob, true );
      end if;
    end if;
    
    /* ���������� ������� �������� */
    if v_formname = 'TrxnDetails' or
       v_formname = 'TrxnParty' or 
       v_formname = 'CustAddrList' or 
       v_formname = 'CustIdDocList' or
       v_formname = 'CustPhonList' or
       v_formname = 'CustEmailList' or
       v_formname = 'CustCustList' or 
       v_formname = 'CustDetails' or
       v_formname = 'CustAcctList' then
      v_json := rf_pkg_request_details.process_request( par_request, par_user, v_id_request );
      dbms_lob.createtemporary( var_clob, true );
      v_json.to_clob( var_clob, true );
    end if;
  
    /* ��������� �������� */
    if v_formname = 'OES321TB_REF' or
       v_formname = 'CUR_REF' or
       v_formname = 'DOCTYPE_REF' or 
       v_formname = 'REGION_REF' or
       v_formname = 'CNTRY_NUM_REF' 
       or v_formname = 'AssignRule' 
       or v_formname = 'AssignReplacement'
       or v_formname = 'AssignLimit'
       or v_formname = 'OPOK'
       or v_formname = 'OPOKDetails'
       or v_formname = 'OPOKCriteria'
       or v_formname = 'OPOKRule'
       or v_formname = 'WatchList'
       or v_formname = 'WatchListOrg'
       or v_formname = 'ReportsList'
    then
        v_json := rf_pkg_request_dicts.process_request( par_request, par_user, v_id_request );
        dbms_lob.createtemporary( var_clob, true );
        v_json.to_clob( var_clob, true );
    end if;
    
    /* ��������� ���� */
    if v_formname = 'OES321Details' or
       v_formname = 'OES321Party' or
       v_formname = 'OESCheck' or
       v_formname = 'OES321Org' or 
       v_formname = 'OES321PartyOrg' or
       v_formname = 'OESCheckRule' or
       v_formname = 'OES321Create' 
       then
         v_json := rf_pkg_request_forms.process_request( par_request, par_user, v_id_request );
         dbms_lob.createtemporary( var_clob, true );
         v_json.to_clob( var_clob, true );
    end if;
    
    /* ��������� ������� ��������� ��� */
    if v_formname = 'OES321Changes' or
       v_formname = 'OES321ChangeDetails'
       then
         v_json := rf_pkg_request_oes_changes.process_request( par_request, par_user, v_id_request );
         dbms_lob.createtemporary( var_clob, true );
         v_json.to_clob( var_clob, true );
    end if;
    
    /* ��������� ������ ������� */
    if v_formname = 'OESCustSearch' then
      v_json := rf_pkg_request_searches.process_request( par_request, par_user, v_id_request );
      dbms_lob.createtemporary( var_clob, true );
      v_json.to_clob( var_clob, true );
    end if; 

     /* ��������� ������� */
    if v_formname in ('ImpType', 'ImpSession', 'ImpFiles') then
      v_json := rf_pkg_request_imports.process_request( par_request, par_user, v_id_request );
      dbms_lob.createtemporary( var_clob, true );
      v_json.to_clob( var_clob, true );
    end if;
    
    /* ��������� �������� �� ������ � ���������� (trxn �������) */
    if v_formname = 'TrxnExtra' then
      v_json := rf_pkg_request_trxn.process_request( par_request, par_user, v_id_request );
      dbms_lob.createtemporary( var_clob, true );
      v_json.to_clob( var_clob, true );
    end if;  
    /*------------------------------------------------------------------------*/
    
    --
    --
    -- ��������� BUSINESS.ORG
    --
    --
    if rf_json_ext.get_string(v_jobj, 'form') = 'TBOSB' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_sql_read('select rownum as "v_$0", z.* from ( ' ||
                                  chr(13) ||
                                  ' select o.org_nm, regexp_substr(org_intrl_id, ''[^-]+'', 1, 1) tb_nb, ' ||
                                  chr(13) ||
                                  ' regexp_substr(org_intrl_id, ''[^-]+'', 1, 2) osb_nb, ' ||
                                  chr(13) ||
                                  'org_intrl_id as "_id_"  from business.org o where o.org_type_cd = ''���'' ) z',
                                  v_id_request);
      end if;
    end if;
    --
    --
    -- ��������� MANTAS.KDD_REVIEW_STATUS
    --
    -- ������ ������������ �� ����� ������� ��� ������ ��������
    if rf_json_ext.get_string(v_jobj, 'form') = 'Statuses' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_sql_read('select rownum as "v_$0", t1.status_cd as "_id_", ' ||
                                  chr(13) ||
                                  '    t1.status_displ_order_nb, t1.status_cd, t2.code_disp_tx ' ||
                                  chr(13) ||
                                  ' from mantas.kdd_review_status t1 ' ||
                                  chr(13) ||
                                  ' join mantas.kdd_code_set_trnln t2 ' ||
                                  chr(13) ||
                                  '   on t2.code_set = ''AlertStatus'' ' ||
                                  chr(13) ||
                                  ' and t2.code_val = t1.status_cd ' ||
                                  chr(13) ||
                                  ' where upper(t2.code_disp_tx) not like ''%�� ������������%'' ' ||
                                  chr(13) ||
                                  ' order by t1.status_displ_order_nb',
                                  v_id_request);
      end if;
    end if;
    --
    --
    -- ��������� MANTAS.KDD_REVIEW_OWNER
    --
    -- ������ ������������ �� ����� ������ �������������
    if rf_json_ext.get_string(v_jobj, 'form') = 'Owners' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_sql_read('select rownum as "v_$0", owner_seq_id as "_id_", owner_seq_id, owner_dsply_nm, owner_id,
                                          decode(owner_id, '''||par_user||''', ''Y'', ''N'') is_current,
                                          decode(mantas.rf_pkg_adm.is_supervisor(owner_id), 1, ''Y'', ''N'') is_supervisor,
                                          decode(mantas.rf_pkg_adm.has_function(owner_id, ''RFALERTS_W''), 1, ''Y'', ''N'') is_allow_alert_editing
                                     from mantas.kdd_review_owner 
                                    where own_alert_fl = ''Y''
                                      and mantas.rf_pkg_adm.is_enabled(owner_id) = 1
                                    order by owner_dsply_nm',
                                  v_id_request);
      end if;
    end if;
    --
    --
    -- ��������� mantas.rf_rule_snapshot
    --
    -- ������ ������������ ��� ������ ����� ������� ���������
    -- json ��� ������ ������� �� ����������, ���������� ������, �� ���� ������ ��� ���� �������,
    -- � ���������� ��� ����� ������ ��������� ��� � ��������� ���������� ����
    -- ������ ��� ������ ���������� �� ���� ������������, ����� no_data_found � ��� ������ ��������
    if rf_json_ext.get_string(v_jobj, 'form') = 'RULESnapshot' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_ruleshapshot(rf_json_ext.get_number(v_jobj,
                                                             'reviewId'),
                                      rf_json_ext.get_number(v_jobj,
                                                             'repeatId'),
                                      v_id_request);
      end if;
    end if;

    --
    --��������� mantas.kdd_cmmnt
    --
    if rf_json_ext.get_string(v_jobj, 'form') = 'AlertComment' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_sql_read(q'{
          select c.cmmnt_id as "_id_", 
                 c.cmmnt_id, 
                 c.cmmnt_tx, 
                 c.displ_order_nb, 
                 cl.scnro_class_cd, 
                 c.cmmnt_cat_cd 
            from mantas.kdd_cmmnt c 
            join mantas.kdd_scnro_class_cmmnt cl on cl.cmmnt_id = c.cmmnt_id
           where cl.scnro_class_cd = 'ML'
           order by c.displ_order_nb 
          }',
          v_id_request
        );  
      end if;
    end if;

    --
    --��������� ofsconf.cssms_role_mast 
    --
    if rf_json_ext.get_string(v_jobj, 'form') = 'Roles' then
      if rf_json_ext.get_string(v_jobj, 'action') = 'read' then
        var_clob := exec_sql_read(q'{
          select rm.v_role_code as "_id_",
              rm.v_role_code as role_cd,
              rm.v_role_name as role_nm
            from ofsconf.cssms_role_mast rm
              join ofsconf.cssms_role_function_map rfm ON rfm.V_ROLE_CODE=rm.V_ROLE_CODE
           where rm.v_role_code in ('AMANALYST1', 'AMSUPVISR','AMRESPNSBL', 'AMANALYTIC', 'AMMAINANLT', 'AMMETHODLG', 'AMMAINMETH')
             and rfm.v_function_code='RFALERTS_W'
          }',
          v_id_request
        );  
      end if;
    end if;


    if trim(var_clob) is null then
      log_request_error('��������� ���������� ������!' || chr(13) ||
                        '���� �� ����� ��� ������� ������ � ���������� �������, �� �� ������� � ���.' ||
                        chr(13) ||
                        '�� ������� ����������, ��� ������ � ���� ��������, � ������ ������.',
                        v_id_request);
      rf_pkg_audit.set_event_result(g_audit_id,'T','��������� ���������� ������! ���� �� ����� ��� ������� ������ � ���������� �������, �� �� ������� � ���. �� ������� ����������, ��� ������ � ���� ��������, � ������ ������.');                  
      -- ������� ����������
      dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_USER');
      dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_IP_ADDRESS');
      dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_BATCH_DATE');
      dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_ACTVY_TYPE_CD');
      g_err_type_cd:=NULL;
      g_message_tx:=NULL;
      g_audit_id:=NULL;
      return exec_exception('��������� ���������� ������!' || chr(13) ||
                            '���� �� ����� ��� ������� ������ � ���������� �������, �� �� ������� � ���.' ||
                            chr(13) ||
                            '�� ������� ����������, ��� ������ � ���� ��������, � ������ ������.');
    end if;
    -- ����������� �������
    -- ���� ������ ��� ��������� ��������� ������ �� ��� �� ����  
    -- � ��� ��� ����� ���� �������� ����, ��� ��� �� ������ ������ ���� ������ ����
    -- ��������� ����������, ���� �� ����� �� ����������� ������� rf_request_response_loq
    /*
    insert into mantas.rf_request_response_loq
      (id, id_request, response)
    values
      (mantas.rf_request_response_seq.nextval, v_id_request, var_clob);
    */  
    commit;
    
    -- ��������� ������ ������, � ���� ������ ��� ���� ��������� ��������� � �����
    IF v_json IS NOT NULL
      THEN -- ���� JSON ��������, �� ������� � ����
        -- �������� �������� �� JSON ������ � ��������� �� ������
        v_success:=rf_json_ext.get_bool(v_json,'success');
        v_message:=rf_json_ext.get_string(v_json,'message');
        IF NOT v_success OR v_message IS NOT NULL THEN
          rf_pkg_audit.set_event_result(g_audit_id,CASE WHEN NOT v_success THEN 'T' ELSE NULL END,v_message);
        END IF;
      ELSIF g_err_type_cd IS NOT NULL OR g_message_tx IS NOT NULL  THEN -- ������ ������������� ����� exec_exception
        rf_pkg_audit.set_event_result(g_audit_id,g_err_type_cd,g_message_tx);
    END IF;  
    
    -- ������� ����������
    dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_USER');
    dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_IP_ADDRESS');
    dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_BATCH_DATE');
    dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_ACTVY_TYPE_CD');
    g_audit_id:=NULL;
    g_err_type_cd:=NULL;
    g_message_tx:=NULL;

    return var_clob;
  
  exception
    when others then
      begin   
        log_request_error(sqlerrm, v_id_request);
        rf_pkg_audit.set_event_result(g_audit_id,'T',dbms_utility.format_error_stack);
        -- ������� ����������
        dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_USER');
        dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_IP_ADDRESS');
        dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_BATCH_DATE');
        dbms_session.clear_context('CLIENTCONTEXT', NULL, 'AML_ACTVY_TYPE_CD');
        g_audit_id:=NULL;
        g_err_type_cd:=NULL;
        g_message_tx:=NULL;
        return exec_exception('��� ������� ������� ��������� ������!' ||
                              chr(13) || sqlerrm);
      end;
  end process_request;
end rf_pkg_request;
/
