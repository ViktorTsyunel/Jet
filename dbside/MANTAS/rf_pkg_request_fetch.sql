create or replace package mantas.rf_pkg_request_fetch
is
  /* ����� ��������� ������������ ������������ �������� ���������� �� �������.
   * ���������� ������� ����������� � ��������� ������ ����� dbms_scheduler
   *
   * �������������� ������:
   *  - grant create job to mantas;
   *  - grant manage scheduler to mantas;
   *  - grant create any context to mantas;
   *
   * ������ ���������� ���� ��� �� ��������� � ����������� ����������� �������
   */


  /* ����� ��������� ������ � ������� rf_request_fetch �� ����������, 
   * ������� ������������ ���� ��� is not null
   *
   * p_fetch_id - id �������
   * p_status_cd - ������ ����������
   * p_err_msg_tx - ����� ������
   * p_total_ct - ����� ���������� �����
   * p_fetch_ct - ���������� �����, ��� �������� �������
   * p_page_ct - ����� ���������� �������
   * p_next_page_nb - ����� ��������, ������� ������ ���������� �������
   * p_last_fetch_time - ���� � ����� ���������� �������
   */
  procedure update_page_info( 
    p_fetch_id        in mantas.rf_request_fetch.fetch_id%type,
    p_status_cd       in mantas.rf_request_fetch.status_cd%type default null,
    p_err_msg_tx      in mantas.rf_request_fetch.err_msg_tx%type default null,
    p_total_ct        in mantas.rf_request_fetch.total_ct%type default null,
    p_fetch_ct        in mantas.rf_request_fetch.fetch_ct%type default null,
    p_page_ct         in mantas.rf_request_fetch.page_ct%type default null,
    p_next_page_nb    in mantas.rf_request_fetch.next_page_nb%type default null,
    p_last_fetch_time in mantas.rf_request_fetch.last_fetch_time%type default null
  );


  /* ����� ��������� ������ ���������� ������� ��� �������
   *
   * ��������� ������
   *   p_sql - ����� �������
   *   p_count_field - ��� ��������, � ������� �������� ����� ���������� �������
   *   p_user - ��� ������������
   * ��������� ������
   *   rf_json - json � ������� ������ ��������
   */
  function run( p_sql in varchar2, p_count_field in varchar2, p_user in varchar2 ) return clob;

  /* ����� ��� ���������� � �������� � ������� ���� ������� (����������� ����� dbms_scheduler)
   *
   * ��������� ������
   *   p_fetch_id - id �������
   *   p_count_field - ��� ��������, � ������� �������� ����� ���������� �������
   */
  procedure fetch_pages( p_fetch_id in number, p_count_field in varchar2 );

  /* ����� ��� ��������� ���������� ��������� ��������
   *
   * ��������� ������
   *   p_fetch_id - id �������
   *   p_user - ��� ������������
   * ��������� ������
   *   rf_json - json � ������� ��������
   */
  function get_next_page( p_fetch_id in number, p_user in varchar2 ) return clob;

  /* ����� ��� ��������� ���������� �� ���� ���������� ������� (��� �������, �������������� �� ������ ������) 
   *
   * ���������� ������
   *   p_fetch_id - id �������
   *   p_user - ��� ������������
   * ��������� ������
   *   rf_json - ���������� ������� � ������� json
   */
  function get_all_page( p_fetch_id in number, p_user in varchar2 ) return clob;
  
  /* ����� ������� ������ �������� � ���������� � ���. 
   * � ������, ����� �������� is null ��������� ������ ������, 
   * � ��������� ������ ��������� ������ �� ����� ���������.
   *
   * ��������� ������
   *   p_fetch_id - id �������
   */
  procedure delete_pages( p_fetch_id in number default null );
end;
/


create or replace package body mantas.rf_pkg_request_fetch
is
  /* TODO ������� ����� �������� � �������. ����������� ����� ������������� � �������� �������� ����������. */ 

  /* ������� ����� ������ */
  c_job_prefix constant varchar2(4) := 'JRP_';
  /* ����� �������� ���������� ������� �� ���������� ������ */
  c_timeout_sec constant number := 60 * 60; --60 �����
  /* ����������� ��������� ���������� ����� � ������� */
  c_max_row_size constant number := 100000;
  /* ����� ����� �������, �� ��������� �������� ��� ������ ���� ������� */
  c_lifetime_page_min constant number := 120;
  /* ��� ��������� oracle */
  c_ctx_name constant varchar2(30) := 'pages_context';
  /* ��� ��������� ��������� */
  c_ctx_deleted_name constant varchar2(30) := 'date_deleted';
  /* ������ ����� ������� ��� ������� �������� */
  c_page_bucket_size constant number := 5;
  /* ���������� �� ������������� �������, �� ����������� ������� ����� �������� �������� ��������� ������� */
  c_page_remain_size constant number := 2;
  /* ����� ����� ��� ��������� �������� ������� */
  c_pause_sec constant number := 5;

  /* ����� ������ ��� �������� ���� ���������� �������� ������ �� rf_request_fetch
   * ���� ������������ �� ��������� oracle �� ����� �� ��������� c_ctx_name
   * ��� ������� ����������� ��������� ���� �������� ��� sysdate - c_lifetime_page_min
   *
   * ��������� ������
   *   date - ���� ��������� ������� �������
   */
  function get_deleted_date return date
  is
    v_ctx_value varchar2(128);
  begin
    select sys_context( c_ctx_name, c_ctx_deleted_name ) 
      into v_ctx_value
      from dual;
    if v_ctx_value is null then
      v_ctx_value := to_char( sysdate - numtodsinterval( c_lifetime_page_min, 'MINUTE' ), 'DD.MM.YYYY HH24:MI:SS' );
      dbms_session.set_context( c_ctx_name, c_ctx_deleted_name, v_ctx_value );
    end if;
    
    return to_date( v_ctx_value, 'DD.MM.YYYY HH24:MI:SS' );
  end;


  /* ����� ������� ������ �������� � ���������� � ���. 
   * � ������, ����� �������� is null ��������� ������ ������, 
   * � ��������� ������ ��������� ������ �� ����� ���������.
   *
   * ��������� ������
   *   p_fetch_id - id �������
   */
  procedure delete_pages( p_fetch_id in number default null )
  is
  begin
    if p_fetch_id is not null then
      delete from mantas.rf_request_fetch
       where fetch_id = p_fetch_id;
    else
      if get_deleted_date <= sysdate - numtodsinterval( c_lifetime_page_min, 'MINUTE' ) then
        dbms_session.set_context( c_ctx_name, c_ctx_deleted_name, to_char( sysdate, 'DD.MM.YYYY HH24:MI:SS' ) );
        delete from mantas.rf_request_fetch
         where last_fetch_time <= sysdate - numtodsinterval( c_lifetime_page_min, 'MINUTE' );
      end if;
    end if;

    commit; 
  end;


  /* ����� ��������� ������ � ������� rf_request_fetch �� ����������, 
   * ������� ������������ ���� ��� is not null
   *
   * p_fetch_id - id �������
   * p_status_cd - ������ ����������
   * p_err_msg_tx - ����� ������
   * p_total_ct - ����� ���������� �����
   * p_fetch_ct - ���������� �����, ��� �������� �������
   * p_page_ct - ����� ���������� �������
   * p_next_page_nb - ����� ��������, ������� ������ ���������� �������
   * p_last_fetch_time - ���� � ����� ���������� �������
   */
  procedure update_page_info( 
    p_fetch_id        in mantas.rf_request_fetch.fetch_id%type,
    p_status_cd       in mantas.rf_request_fetch.status_cd%type default null,
    p_err_msg_tx      in mantas.rf_request_fetch.err_msg_tx%type default null,
    p_total_ct        in mantas.rf_request_fetch.total_ct%type default null,
    p_fetch_ct        in mantas.rf_request_fetch.fetch_ct%type default null,
    p_page_ct         in mantas.rf_request_fetch.page_ct%type default null,
    p_next_page_nb    in mantas.rf_request_fetch.next_page_nb%type default null,
    p_last_fetch_time in mantas.rf_request_fetch.last_fetch_time%type default null
  ) is
  begin
    update mantas.rf_request_fetch 
       set status_cd = nvl2( p_status_cd, p_status_cd, status_cd ),
           err_msg_tx = nvl2( p_err_msg_tx, p_err_msg_tx, err_msg_tx ),
           total_ct = nvl2( p_total_ct, p_total_ct, total_ct ),
           fetch_ct = nvl2( p_fetch_ct, p_fetch_ct, fetch_ct ),
           page_ct = nvl2( p_page_ct, p_page_ct, page_ct ),
           next_page_nb = nvl2( p_next_page_nb, p_next_page_nb, next_page_nb ),
           last_fetch_time = nvl2( p_last_fetch_time, p_last_fetch_time, last_fetch_time )
     where fetch_id = p_fetch_id;
    commit;
  end;


  /* ����� ��� ���������� � �������� � ������� ���� ������� (����������� ����� dbms_scheduler)
   *
   * ��������� ������
   *   p_fetch_id - id �������
   *   p_count_field - ��� ��������, � ������� �������� ����� ���������� �������
   */
  procedure fetch_pages( p_fetch_id in number, p_count_field in varchar2 )
  is
    v_page_size number := 1;
    v_sql varchar2(32000);
    v_binds varchar2(32000);
    v_bind_json rf_json;
    v_cursor_id number;
    v_count_rows number;
    v_end_flag number := 0; 
    v_json_list rf_json_list;
    v_page_number number := 0;
    v_clob clob;
    v_error_message varchar2(1024);
    v_status number;
    v_max_row number := 0;
    
    v_status_cd number := 1;
    v_remain_page_count number;
    v_last_fetch_time date;
  begin
    /* ���������� ������ �������� */
    v_page_size := mantas.rf_pkg_adm.get_install_param(9003);
    
    /* �������� ����� ������� */
    select trim(substr(query_tx, 1, instr(query_tx, '###BIND_JSON###') - 1)),
           trim(substr(query_tx, instr(query_tx, '###BIND_JSON###') + 15 ))
      into v_sql, v_binds
      from mantas.rf_request_fetch 
     where fetch_id = p_fetch_id;

    /* �������� ����� � rf_request_fetch */
    update_page_info( 
      p_fetch_id => p_fetch_id, 
      p_status_cd => 1 
    );
    
    /* ��������� ������ �� �������  */
    v_cursor_id := dbms_sql.open_cursor;
    dbms_sql.parse(v_cursor_id, v_sql, dbms_sql.native);
    /* ���������� ����������� */
    v_bind_json := rf_json(v_binds);
    if v_bind_json.count > 0 then
      rf_json_dyn.bind_json(v_cursor_id, v_bind_json);
    end if;
    v_status := dbms_sql.execute(v_cursor_id); 
    
    /* ���� ���������� �������, ���� executeListPage ���������� ������ */
    loop
      v_json_list := rf_json_list();
      v_json_list := rf_json_dyn.executelistpage( v_cursor_id, v_page_size, p_count_field, v_count_rows, v_end_flag );
      v_max_row := v_max_row + v_json_list.count;
      v_count_rows := nvl( v_count_rows, -1 );
      dbms_lob.createtemporary(v_clob, true);
      v_json_list.to_clob( v_clob, true );
      v_page_number := v_page_number + 1;

      /* �������� �����������, ���� ���� ������ �� ������� */
      insert into mantas.rf_request_fetch_pages(
        fetch_id, page_nb, page_ct, page_data
      ) values (
        p_fetch_id, v_page_number, v_json_list.count, v_clob
      );
      commit;
      
      if v_page_number = 1 then
        /* �������� ������ � rf_request_fetch */
        update_page_info(
          p_fetch_id => p_fetch_id,
          p_total_ct => v_count_rows,
          p_page_ct => 99999
        );
      end if;
      
      /* ����� �� ����� ���� ��������� ��� ������ */
      exit when v_end_flag = 1
             or v_max_row >= c_max_row_size;
      
      /* ��������� �������� �������. ���� ��������� ������ N ������� � ������ �� ����� FORCE FETCH ��� CANCEL FETCH */
      if mod(v_page_number, c_page_bucket_size) = 0 and v_status_cd not in (11) then
        loop 
          /* ������ ������ � ���������� ��������������� ������� */
          select status_cd, v_page_number - next_page_nb + 1, last_fetch_time
            into v_status_cd, v_remain_page_count, v_last_fetch_time
            from mantas.rf_request_fetch
           where fetch_id = p_fetch_id;
          /* ���� ����� �������� ��������� ����� ����������� � ��������� c_lifetime_page_min, �� status_cd = CANCEL FETCH */
          if v_last_fetch_time < sysdate - numtodsinterval(c_lifetime_page_min, 'MINUTE') then
            v_status_cd := 12;
          end if;  
          /* ������� �� ����� �������� ����: 
           *   - ������ ����� FORCE FETCH ��� CANCEL FETCH 
           *   - ���������� ��������������� ������� ������ ��� ���������� � ��������� c_page_remain_size 
           */
          exit when v_status_cd in (11, 12)
                 or v_remain_page_count < c_page_remain_size;
          /* ������������� ���������� �� N ������ */
          dbms_lock.sleep(c_pause_sec); 
        end loop;
      end if;
      
      /* ����� �� ����� ���� ������ = CANCEL FETCH */
      exit when v_status_cd = 12;
      
    end loop;

    dbms_sql.close_cursor(v_cursor_id);
    commit;
    
    /* �������� ������ � rf_request_fetch */
    update_page_info(
      p_fetch_id => p_fetch_id,
      p_total_ct => v_max_row,
      p_page_ct => v_page_number,
      p_status_cd => 2 
    );
    
    /* ������� ������ �������� */
    begin
      delete_pages;
      exception 
        when others then null;
    end;  
    
    exception
      when others then
        v_error_message := sqlerrm();
        /* ���������� ���������, �������� ������� ������ */
        rollback;
        begin
          dbms_sql.close_cursor(v_cursor_id);
          exception
            when others then null;
        end;  
        /* �������� ������ � rf_request_fetch */
        update_page_info( 
          p_fetch_id => p_fetch_id,
          p_status_cd => -1,
          p_err_msg_tx => v_error_message
        );
  end;


  /* ����� ������� � ��������� ������ �������� �������
   *
   * ���������� ������
   *   p_fetch_id - id �������
   *   p_count_field - - ��� ��������, � ������� �������� ����� ���������� �������
   */
  procedure create_scheduler( p_fetch_id in number, p_count_field in varchar2 )
  is
  begin
    dbms_scheduler.create_job (
      job_name        => c_job_prefix||p_fetch_id,
      job_type        => 'PLSQL_BLOCK',
      job_action      => 'begin mantas.rf_pkg_request_fetch.fetch_pages( '||p_fetch_id||', '''||p_count_field||''' ); end;',
      start_date      => null,
      repeat_interval => null,
      end_date        => null,
      auto_drop       => TRUE,
      enabled         => TRUE
    );
    /* TODO ��������� ��������� �������� ������ */
  end;


  /* ����� ������������� ������ �� �������� � ��������� ������ 
   * 
   * ���������� ������
   *   p_fetch_id - id �������
   */
  procedure stop_scheduler( p_fetch_id in number )
  is
  begin
    /* ��������� ������ */  
    update_page_info( 
      p_fetch_id => p_fetch_id, 
      p_status_cd => -1, 
      p_err_msg_tx => 'Job stoped by timeout' 
    );
    /* ������������� ������ */
    begin
      dbms_scheduler.stop_job( 
        job_name => c_job_prefix||p_fetch_id,
        force    => true
      );
      exception
        when others then null;
    end;  
  end;


  /* ����� �������� ������ ��������. 
   * ���� �������� p_next_page = null, �� ������� �������� �� rf_request_fetch.next_page_nb
   *
   * ���������� ������
   *   p_fetch_id - id �������
   *   p_user - ��� ������������
   *   p_grand_total - ����� ���������� �����
   *   p_total_count - ���������� �����, ���������� ������������
   *   p_next_page - ��������� �������� ��� ��������
   * ��������� ������
   *   clob - ���������� �������� � ������� json_list
   */
  function get_page_data( 
    p_fetch_id in out number, 
    p_user in varchar2, 
    p_grand_total out number,
    p_fetch_count out number,
    p_page_count out number,
    p_next_page in out number
  ) return clob
  is
    v_user varchar2(64);
    v_page_ct number;
    v_next_page number;
    v_data clob;
    v_status number := 0;
  begin
    /* ��������� ���������� �������� 
     * ��� �������� ���������� ��������� �������� ����� ������� �� ������� rf_request_fetch.next_page_nb
     * ��� ���������� ������ ����� ��������, ����� �������� ���������� �� ������� ��������� p_next_page
     */
    select f.total_ct, f.fetch_ct, nvl(fp.page_ct, 0), f.page_ct, nvl2( fp.page_ct, nvl( p_next_page, f.next_page_nb ), f.next_page_nb - 1 ), f.user_nm, nvl(fp.page_data, '[]')
      into p_grand_total, p_fetch_count, p_page_count, v_page_ct, p_next_page, v_user, v_data
      from mantas.rf_request_fetch f
      left join mantas.rf_request_fetch_pages fp on f.fetch_id = fp.fetch_id and fp.page_nb = nvl( p_next_page, f.next_page_nb )
     where f.fetch_id = p_fetch_id;
    
    /* ��������� ����� ������������ �� ������ � ������ */
    if nvl( v_user, '?' ) <> nvl( p_user, '?' ) then
      raise_application_error( -20000, '������������ ����������, ������ � ���������� ���������.' );
    end if;

    /* ��� ��������� �������� ������� p_fetch_id */
    if p_next_page >= v_page_ct then
      p_fetch_id := null;
    end if;  

    return v_data;
    
    exception
      when no_data_found then
        raise_application_error( -20001, '�������� �� ����������.' );
      when others then 
        raise;
  end;


  /* ����� ��� ��������� ���������� ��������� ��������
   *
   * ��������� ������
   *   p_fetch_id - id �������
   *   p_user - ��� ������������
   * ��������� ������
   *   rf_json - json � ������� ��������
   */
  function get_next_page( p_fetch_id in number, p_user in varchar2 ) return clob
  is
    v_fetch_id number;
    v_grand_total number;
    v_fetch_count number;
    v_page_count number;
    v_next_page number := null;
    v_data clob;
    v_page clob;
  begin
    /* ���������� ������ � rf_request_fetch 
     * ������ ���������� ���� � ������ update_page_info -> commit
     * � ������ ������ ���������� ��������� � ����� exception - rollback
     */
    select fetch_id into v_fetch_id
      from mantas.rf_request_fetch
     where fetch_id = p_fetch_id
    for update;

    /* ��� ������ � ������������ ��������� ���������� clob, �.�. json �� clob ����� ����� �������� */
    v_page := get_page_data( v_fetch_id, p_user, v_grand_total, v_fetch_count, v_page_count, v_next_page );
    v_data := '{'||
              '"success":"true",'||
              '"fetchId":'||nvl(to_char(v_fetch_id), 'null')||','||
              '"totalCount":'||(v_fetch_count + v_page_count)||','||
              '"grandTotal":'||(case 
                                  when v_grand_total = -1 then 'null'
                                  else to_char(v_grand_total)
                                end)||', '||
              '"data":'||v_page||               
              '}';

    /* ���� ��������� ��������� ��������, �� ������� ������ */    
    if v_fetch_id is null then
      delete_pages( p_fetch_id );
    else  
      /* �������� ������ � rf_request_fetch */
      update_page_info(
        p_fetch_id => v_fetch_id,
        p_fetch_ct => v_fetch_count + v_page_count,
        p_next_page_nb => v_next_page + 1,
        p_last_fetch_time => sysdate
      );
    end if;    
    
    return v_data;
    
    exception
      when others then 
        /* ������ ���������� */
        rollback;
        raise;
  end;


  /* ����� ��� ��������� ���������� �� ���� ���������� ������� (��� �������, �������������� �� ������ ������) 
   *
   * ���������� ������
   *   p_fetch_id - id �������
   *   p_user - ��� ������������
   * ��������� ������
   *   rf_json - ���������� ������� � ������� json
   */
  function get_all_page( p_fetch_id in number, p_user in varchar2 ) return clob
  is
    v_fetch_id number;
    v_grand_total number := 0;
    v_total_count number := 0;
    v_fetch_count number;
    v_page_count number;
    v_next_page number;
    v_data clob;
    v_page clob := null;
    v_ready_page_count number;
  begin
    /* ���������� ������ � rf_request_fetch 
     * ������ ���������� ���� � ������ update_page_info -> commit
     * � ������ ������ ���������� ��������� � ����� exception - rollback
     */
    select fetch_id into v_fetch_id
      from mantas.rf_request_fetch
     where fetch_id = p_fetch_id
    for update;

    /* ���������� ����� ��������� �������� */
    select min(next_page_nb) into v_next_page
      from mantas.rf_request_fetch 
     where fetch_id = p_fetch_id;
    /* ���������� ���������� �������������� ������� */
    select count(*) into v_ready_page_count
      from mantas.rf_request_fetch_pages
     where fetch_id = p_fetch_id;

    /* ��� ������ � ������������ ��������� ���������� clob, �.�. json �� clob ����� ����� �������� */
    v_data := '{'||
              '"success":"true",'||
              '"data":[';
    /* ��������� ������ �� ������� */ 
    for i in v_next_page..v_ready_page_count loop
      v_next_page := i; 
      v_page := get_page_data( v_fetch_id, p_user, v_grand_total, v_fetch_count, v_page_count, v_next_page );
      if i < v_ready_page_count then 
        v_data := v_data||chr(13)||substr( v_page, 2, dbms_lob.getlength( v_page ) - 2 )||','; 
       else
         v_data := v_data||chr(13)||substr( v_page, 2, dbms_lob.getlength( v_page ) - 2 );
       end if;
      /* ���������� ����������� ������ �� ��������� */
      v_total_count := v_total_count + v_page_count;
    end loop;
    /* ���������� ����������� ������ �� ��������� ���� ����� ����������� �������� */
    v_total_count := v_total_count + v_fetch_count;
    
    /* to_clob + to_char ����� ��� concat(clob, clob) */
    v_data := v_data||to_clob(
              '],'||
              '"fetchId":'||nvl(to_char(v_fetch_id), 'null')||','||
              '"totalCount":'||to_char(v_total_count)||','|| 
              '"grandTotal":'||case 
                                 when v_grand_total = -1 then 'null'
                                 else to_char(v_grand_total)
                               end||
              '}');

    /* ���� ��������� ��������� ��������, �� ������� ������ */    
    if v_fetch_id is null then
      delete_pages( p_fetch_id );
    else  
      /* �������� ������ � rf_request_fetch */
      update_page_info(
        p_fetch_id => v_fetch_id,
        p_fetch_ct => v_total_count,
        p_next_page_nb => v_ready_page_count + 1,
        p_last_fetch_time => sysdate
      );
    end if;   
    
    return v_data;
    
    exception
      when others then
        /* ������ ���������� */
        rollback;
        raise;
  end;


  /* ����� ��������� ������ ���������� ������� ��� �������
   *
   * ��������� ������
   *   p_sql - ����� �������
   *   p_count_field - ��� ��������, � ������� �������� ����� ���������� �������
   *   p_user - ��� ������������
   * ��������� ������
   *   rf_json - json � ������� ������ ��������
   */
  function run( p_sql in varchar2, p_count_field in varchar2, p_user in varchar2 ) return clob
  is
    v_fetch_id number;
    v_attempt_count number;
    v_page_count number;
    v_page clob;
    v_error_message varchar2(1024);
    v_status number;
  begin
    /* ������������ ������ */
    insert into mantas.rf_request_fetch (
      fetch_id, query_tx, status_cd, total_ct, fetch_ct, page_ct, next_page_nb, 
      err_msg_tx, user_nm,  request_time, last_fetch_time
    ) values (
      mantas.rf_request_log_seq.nextval, p_sql, 0, 0, 0, 0, 1,
      null, p_user, sysdate, sysdate 
    ) returning fetch_id into v_fetch_id;
    commit;
    
    /* � ��������� ������ ��������� �������� ������� */
    create_scheduler( v_fetch_id, p_count_field );
    
    /* ���� N ������ ��������� ������ ������� */
    v_attempt_count := 1;
    loop
      select page_ct, status_cd, err_msg_tx 
        into v_page_count, v_status, v_error_message
        from mantas.rf_request_fetch
       where fetch_id = v_fetch_id;
      exit when v_page_count > 0 
                or v_attempt_count > c_timeout_sec
                or v_status = -1;

      v_attempt_count := v_attempt_count + 1;
      dbms_lock.sleep(1);
    end loop;

    /* ���� ��������� ������ - �������� ����� ������ */
    if v_status = -1 then
      raise_application_error( -20002, v_error_message );
    /* ���� ������ �� ����������� �� N ������ - ������������� ������, �������� ����� ������ */
    elsif v_page_count = 0 then
      stop_scheduler( v_fetch_id );
      raise_application_error( -20003, '������ ��������� �� ��������.' );
    /* ������ ����������� ��� ������. ��������� ��������� */
    else
      v_page := get_next_page( v_fetch_id, p_user );
    end if;

    return v_page;
    
    exception
      when others then 
        v_error_message := sqlerrm();
        rollback;
        /* �������� ������ � rf_request_fetch */
        update_page_info( 
          p_fetch_id => v_fetch_id, 
          p_status_cd => -1, 
          p_err_msg_tx => v_error_message 
        );
        raise;
  end;

end;
/