grant EXECUTE on BUSINESS.rfv_trxn to MANTAS with grant option;
grant EXECUTE on BUSINESS.rf_pkg_util to MANTAS with grant option;

CREATE OR REPLACE FORCE VIEW mantas.rfv_opok_review_list as
SELECT /*+ ORDERED USE_NL(st rvo tb osb trxn optp goz_orig goz_benef oes oes_trxn trxn_scrty oes_org) */
       -- �������� ������
       rv.review_id             as review_id, 
       rv.status_cd             as rv_status_cd,
       rv.status_dt             as rv_status_dt,
       rv.creat_ts              as rv_creat_ts,
       rv.creat_id              as rv_creat_id,
       rv.due_dt                as rv_due_dt,
       rv.owner_seq_id          as rv_owner_seq_id, 
       rv.owner_org             as rv_owner_org,
       rv.orig_owner_seq_id     as rv_orig_owner_seq_id, 
       rv.rf_object_tp_cd       as rv_object_tp_cd, 
       rv.rf_op_cat_cd          as rv_op_cat_cd,
       rv.rf_trxn_seq_id        as rv_trxn_seq_id,
       rv.rf_trxn_scrty_seq_id  as rv_trxn_scrty_seq_id,
       rv.rf_repeat_nb          as rv_repeat_nb,
       rv.rf_data_dump_dt       as rv_data_dump_dt,
       rv.rf_branch_id          as rv_branch_id,
       trunc(rv.rf_branch_id / 100000)              as rv_tb_id, 
       rv.rf_branch_id - round(rv.rf_branch_id, -5) as rv_osb_id, 
       oes_org.oes_321_org_id   as rv_oes_org_id,
       oes_org.tb_name          as rv_kgrko_nm,
       case when oes_org.oes_321_org_id is not null  
            then nvl(decode(oes_org.branch_fl, '0', nullif(oes_org.numbf_s, '0'), 
                                               '1', nullif(oes_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
       end as rv_kgrko_id,       
       rv.rf_kgrko_party_cd     as rv_kgrko_party_cd,
       rv.last_actvy_type_cd    as rv_last_actvy_type_cd,
       rv.prcsng_batch_cmplt_fl as rv_prcsng_batch_cmplt_fl,
       rv.rf_cls_dt             as rv_cls_dt,
       rv.cls_id                as rv_cls_id,       
       rv.cls_actvy_type_cd     as rv_cls_actvy_type_cd,
       rv.cls_class_cd          as rv_cls_class_cd,
       rv.review_type_cd        as rv_review_type_cd, 
       rv.rf_wrk_flag           as rv_wrk_flag,
       rv.rf_trxn_dt            as rv_trxn_dt,
       rv.rf_trxn_base_am       as rv_trxn_base_am,
       rv.rf_partition_key      as rv_partition_key,
       rv.rf_subpartition_date  as rv_subpartition_date,
       rv.rf_opok_nb            as opok_nb, 
       rv.rf_add_opoks_tx       as add_opoks_tx,
       -- �������� ��������/�������� �� ���
       nvl(trxn.op_cat_cd, oes_trxn.op_cat_cd) as op_cat_cd,   
       nvl(trxn.dbt_cdt_cd, oes_trxn.dbt_cdt_cd) as dbt_cdt_cd,   
       nvl(trxn.trxn_seq_id, oes_trxn.trxn_seq_id) as trxn_seq_id,   
       nvl(trxn.trxn_dt, oes_trxn.trxn_dt) as trxn_dt,  
       nvl(trxn.branch_id, oes_trxn.branch_id) as branch_id,  
       nvl(trxn.org_intrl_id, oes_trxn.org_intrl_id) as org_intrl_id,
       case when nvl(trxn.org_intrl_id, oes_trxn.org_intrl_id) is not null
            then business.rf_pkg_util.get_trxn_org_desc(nvl(trxn.org_intrl_id, oes_trxn.org_intrl_id), 
                                                        nvl(trxn.branch_id, oes_trxn.branch_id))
       end as org_desc,                                                 
       nvl(trxn.trxn_crncy_am, oes_trxn.trxn_crncy_am) as trxn_crncy_am,  
       nvl(trxn.trxn_base_am, oes_trxn.trxn_base_am) as trxn_base_am,  
       nvl(trxn.trxn_crncy_cd, oes_trxn.trxn_crncy_cd) as trxn_crncy_cd,  
       nvl(trxn.trxn_crncy_rate_am, oes_trxn.trxn_crncy_rate_am) as trxn_crncy_rate_am,  
       nvl(trxn.trxn_crncy_old_rate_fl, oes_trxn.trxn_crncy_old_rate_fl) as trxn_crncy_old_rate_fl,  
       nvl(trxn.trxn_desc, oes_trxn.trxn_desc) as trxn_desc,  
       nvl(trxn.nsi_op_id, oes_trxn.nsi_op_id) as nsi_op_id,  
       nvl(trxn.orig_nm, oes_trxn.orig_nm) as orig_nm,  
       nvl(trxn.orig_inn_nb, oes_trxn.orig_inn_nb) as orig_inn_nb,  
       nvl(trxn.orig_kpp_nb, oes_trxn.orig_kpp_nb) as orig_kpp_nb,  
       nvl(trxn.orig_acct_nb, oes_trxn.orig_acct_nb) as orig_acct_nb,  
       nvl(trxn.orig_own_fl, oes_trxn.orig_own_fl) as orig_own_fl,  
       nvl(trxn.debit_cd, oes_trxn.debit_cd) as debit_cd,  
       nvl(trxn.orig_addr_tx, oes_trxn.orig_addr_tx) as orig_addr_tx,  
       nvl(trxn.orig_iddoc_tx, oes_trxn.orig_iddoc_tx) as orig_iddoc_tx,  
       nvl(trxn.orig_reg_dt, oes_trxn.orig_reg_dt) as orig_reg_dt,  
       nvl(trxn.orig_goz_op_cd, oes_trxn.orig_goz_op_cd) as orig_goz_op_cd, 
       nvl(goz_orig.code_val2_nm, goz_orig.code_desc_tx) as orig_goz_op_nm, 
       nvl(trxn.send_instn_nm, oes_trxn.send_instn_nm) as send_instn_nm,  
       nvl(trxn.send_instn_id, oes_trxn.send_instn_id) as send_instn_id,  
       nvl(trxn.send_instn_cntry_cd, oes_trxn.send_instn_cntry_cd) as send_instn_cntry_cd,  
       nvl(trxn.send_instn_acct_id, oes_trxn.send_instn_acct_id) as send_instn_acct_id, 
       nvl(trxn.send_branch_id, oes_trxn.send_branch_id) as send_branch_id, 
       nvl(trxn.send_oes_org_id, oes_trxn.send_oes_org_id) as send_oes_org_id, 
       nvl(trxn.send_kgrko_nm, oes_trxn.send_kgrko_nm) as send_kgrko_nm, 
       nvl(trxn.send_kgrko_id, oes_trxn.send_kgrko_id) as send_kgrko_id,         
       nvl(trxn.benef_nm, oes_trxn.benef_nm) as benef_nm,  
       nvl(trxn.benef_inn_nb, oes_trxn.benef_inn_nb) as benef_inn_nb,  
       nvl(trxn.benef_kpp_nb, oes_trxn.benef_kpp_nb) as benef_kpp_nb,  
       nvl(trxn.benef_acct_nb, oes_trxn.benef_acct_nb) as benef_acct_nb,  
       nvl(trxn.benef_own_fl, oes_trxn.benef_own_fl) as benef_own_fl,  
       nvl(trxn.credit_cd, oes_trxn.credit_cd) as credit_cd,  
       nvl(trxn.benef_addr_tx, oes_trxn.benef_addr_tx) as benef_addr_tx,  
       nvl(trxn.benef_iddoc_tx, oes_trxn.benef_iddoc_tx) as benef_iddoc_tx,  
       nvl(trxn.benef_reg_dt, oes_trxn.benef_reg_dt) as benef_reg_dt,  
       nvl(trxn.benef_goz_op_cd, oes_trxn.benef_goz_op_cd) as benef_goz_op_cd,  
       nvl(goz_benef.code_val2_nm, goz_benef.code_desc_tx) as benef_goz_op_nm, 
       nvl(trxn.rcv_instn_nm, oes_trxn.rcv_instn_nm) as rcv_instn_nm,  
       nvl(trxn.rcv_instn_id, oes_trxn.rcv_instn_id) as rcv_instn_id,  
       nvl(trxn.rcv_instn_cntry_cd, oes_trxn.rcv_instn_cntry_cd) as rcv_instn_cntry_cd,  
       nvl(trxn.rcv_instn_acct_id, oes_trxn.rcv_instn_acct_id) as rcv_instn_acct_id,  
       nvl(trxn.rcv_branch_id, oes_trxn.rcv_branch_id) as rcv_branch_id, 
       nvl(trxn.rcv_oes_org_id, oes_trxn.rcv_oes_org_id) as rcv_oes_org_id, 
       nvl(trxn.rcv_kgrko_nm, oes_trxn.rcv_kgrko_nm) as rcv_kgrko_nm, 
       nvl(trxn.rcv_kgrko_id, oes_trxn.rcv_kgrko_id) as rcv_kgrko_id, 
       nvl(trxn.trxn_doc_id, oes_trxn.trxn_doc_id) as trxn_doc_id,  
       nvl(trxn.trxn_doc_dt, oes_trxn.trxn_doc_dt) as trxn_doc_dt,  
       nvl(trxn.trxn_conv_crncy_cd, oes_trxn.trxn_conv_crncy_cd) as trxn_conv_crncy_cd,  
       nvl(trxn.trxn_conv_am, oes_trxn.trxn_conv_am) as trxn_conv_am,  
       nvl(trxn.trxn_conv_base_am, oes_trxn.trxn_conv_base_am) as trxn_conv_base_am,  
       nvl(trxn.trxn_cash_symbols_tx, oes_trxn.trxn_cash_symbols_tx) as trxn_cash_symbols_tx,  
       nvl(trxn.bank_card_id_nb, oes_trxn.bank_card_id_nb) as bank_card_id_nb,  
       nvl(trxn.bank_card_trxn_dt, oes_trxn.bank_card_trxn_dt) as bank_card_trxn_dt,  
       nvl(trxn.merchant_no, oes_trxn.merchant_no) as merchant_no,  
       nvl(trxn.atm_id, oes_trxn.atm_id) as atm_id,  
       nvl(trxn.tb_id, oes_trxn.tb_id) as tb_id,  
       nvl(trxn.osb_id, oes_trxn.osb_id) as osb_id,  
       nvl(trxn.src_sys_cd, oes_trxn.src_sys_cd) as src_sys_cd,  
       nvl(trxn.src_change_dt, oes_trxn.src_change_dt) as src_change_dt,  
       nvl(trxn.src_user_nm, oes_trxn.src_user_nm) as src_user_nm,  
       nvl(trxn.src_cd, oes_trxn.src_cd) as src_cd,  
       nvl(trxn.canceled_fl, oes_trxn.canceled_fl) as canceled_fl,
       nvl(trxn.data_dump_dt, oes_trxn.data_dump_dt) as data_dump_dt,
       -- �������� ������ ������
       trxn_scrty.scrty_type_cd    as scrty_type_cd,
       trxn_scrty.scrty_id         as scrty_id,
       trxn_scrty.scrty_nb         as scrty_nb,
       trxn_scrty.scrty_base_am    as scrty_base_am,
       trxn_scrty.scrty_nominal    as scrty_nominal,
       trxn_scrty.nominal_crncy_cd as scrty_nominal_crncy_cd,
       trxn_scrty.scrty_price      as scrty_price,
       trxn_scrty.price_crncy_cd   as scrty_price_crncy_cd,
       trxn_scrty.scrty_seq_id     as scrty_seq_id,
       trxn_scrty.canceled_fl      as scrty_canceled_fl,
       -- ���. �������� ��� �����������/������������� � ������/������� ������
       st.code_disp_tx             as rv_status_nm,
       rvo.owner_id                as rv_owner_id,
       optp.op_type_nm             as nsi_op_type_nm,
       tb.org_nm                   as tb_nm,
       osb.org_nm                  as osb_nm,
       oes.oes_321_id              as oes_id,
       oes.send_fl                 as oes_send_fl,
       oes.check_status            as oes_check_status
  FROM mantas.kdd_review rv
       LEFT JOIN mantas.rf_oes_321 oes ON oes.review_id = rv.review_id and oes.current_fl = 'Y'
       LEFT JOIN mantas.kdd_code_set_trnln st on st.code_set = 'AlertStatus' and st.code_val = rv.status_cd
       LEFT JOIN mantas.kdd_review_owner rvo on rvo.owner_seq_id = rv.owner_seq_id
       LEFT JOIN business.org tb on tb.org_intrl_id = to_char(trunc(rv.rf_branch_id / 100000))
       LEFT JOIN business.org osb on osb.org_intrl_id = to_char(trunc(rv.rf_branch_id / 100000)) || '-' || 
                                                        to_char(rv.rf_branch_id - trunc(rv.rf_branch_id, -5))
       LEFT JOIN mantas.rf_oes_321 oes on oes.review_id = rv.review_id and oes.current_fl = 'Y'
       LEFT JOIN table(business.rfv_trxn(rv.rf_op_cat_cd, rv.rf_trxn_seq_id, rv.rf_data_dump_dt)) trxn ON rv.rf_object_tp_cd like 'TRXN%'
       LEFT JOIN business.rf_nsi_op_types optp on optp.nsi_op_id = trxn.nsi_op_id
       LEFT JOIN business.ref_table_detail goz_orig ON goz_orig.code_set_id = 'RF_GOZ_OP_TYPE_DBT' and goz_orig.code_val1_nm = trxn.orig_goz_op_cd
       LEFT JOIN business.ref_table_detail goz_benef ON goz_benef.code_set_id = 'RF_GOZ_OP_TYPE_CDT' and goz_benef.code_val1_nm = trxn.benef_goz_op_cd
       LEFT JOIN table(mantas.rfv_oes_trxn(oes.oes_321_id)) oes_trxn ON rv.rf_object_tp_cd like 'OES%'
       LEFT JOIN business.rfv_trxn_scrty trxn_scrty ON trxn_scrty.op_cat_cd = rv.rf_op_cat_cd and 
                                                       trxn_scrty.trxn_scrty_seq_id = rv.rf_trxn_scrty_seq_id and
                                                       rv.rf_object_tp_cd in ('TRXN_SCRTY', 'TRXN_SCRTY_JOINT')
       LEFT JOIN mantas.rf_oes_321_org oes_org ON oes_org.oes_321_org_id = mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(rv.rf_branch_id/100000), rv.rf_branch_id - round(rv.rf_branch_id, -5), rv.rf_trxn_dt)
 WHERE rv.rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/ and 
       NOT(rv.rf_object_tp_cd like 'OES%' and oes.oes_321_id is null);

GRANT SELECT on mantas.rfv_opok_review_list to KDD_ALGORITHM;
GRANT SELECT on mantas.rfv_opok_review_list to KDD_MINER;
GRANT SELECT on mantas.rfv_opok_review_list to MANTAS_LOADER;
GRANT SELECT on mantas.rfv_opok_review_list to MANTAS_READER;
GRANT SELECT on mantas.rfv_opok_review_list to RF_RSCHEMA_ROLE;
GRANT SELECT on mantas.rfv_opok_review_list to BUSINESS;
GRANT SELECT on mantas.rfv_opok_review_list to CMREVMAN with grant option;
GRANT SELECT on mantas.rfv_opok_review_list to KDD_MNR with grant option;
GRANT SELECT on mantas.rfv_opok_review_list to KDD_REPORT with grant option;
GRANT SELECT on mantas.rfv_opok_review_list to KYC;
GRANT SELECT on mantas.rfv_opok_review_list to STD;
GRANT SELECT on mantas.rfv_opok_review_list to KDD_ALG;

