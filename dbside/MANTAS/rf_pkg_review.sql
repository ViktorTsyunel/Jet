create or replace package mantas.rf_pkg_review is
--
-- ������ ��������������� � ��������-���������������
--
type tab_number is table of number index by binary_integer;
--
-- ��������� �������� (��������) ��� ������� (��������) - ����� ���������
--
function do_actions
(
  par_review_ids          in rf_tab_number,                  -- ������ ��������������� �������, ��� ��������� �������� ����� ���� �� ������
  par_actvy_type_cds      in rf_tab_varchar,                 -- ������ ����� ����� ��������, ������� ���� ��������� � ��������� �������
  par_new_review_owner_id in kdd_review_owner.owner_seq_id%type, -- ID ������������ - ������ �������������� ��� ��������� �������
  par_due_dt              in kdd_review.due_dt%type,         -- ����� ���� ��������� �������
  par_note_tx             in kdd_note_hist.note_tx%type,     -- ����������� � ��������� ���� ��� ������������ ��������
  par_owner_id            in kdd_review_owner.owner_id%type, -- ����� �������� ������������, ������������ ������ ��������
  par_cmmnt_id            in kdd_cmmnt.cmmnt_id%type,        -- ������������� ������������ ����������� ��� ������������ ��������
  par_opok_nb             in kdd_review.rf_opok_nb%type,     -- �������� ��� ���� ��� ��������� � ������
  par_add_opoks_tx        in kdd_review.rf_add_opoks_tx%type,-- �������������� ���� ���� ��� ��������� � ������
  par_act_review_ids     out rf_tab_number                   -- ������ ��������������� �������, ��� �������� ���� ��������� ���� �� ���� ��������
)
return varchar2; -- ���������, ������� ������� ���������� �� ���������� ��� ������������ (���� NULL, �� ���������� ������ �� ����)
--
-- ��������� �������� (��������) ��� ������� (��������) - ������ ���������
--
procedure do_actions
(
  p_review_id           in varchar2, -- �������� ��������������� ������� ����� �������
  p_actvy_type_cd       in varchar2, -- �������� ����� ����� �������� ����� �������
  p_new_review_owner_id in varchar2, -- ����� ������������ - ������ �������������� ��� ��������� �������, ������ ������� ������ ����������
  p_due_dt              in varchar2, -- ����� ���� ��������� �������
  p_note_tx             in varchar2, -- ����������� � ��������� ���� ��� ������������ ��������
  p_owner_id            in varchar2, -- ����� �������� ������������, ������������ ������ ��������
  p_cmmnt_id            in varchar2, -- ������������� ������������ ����������� ��� ������������ ��������
  p_opok_nb             in varchar2, -- �������� ��� ���� ��� ��������� � ������
  p_add_opoks_tx        in varchar2  -- �������������� ���� ���� ��� ��������� � ������
);
--
-- �������� �������� ���������������� �������, �������� ��������������� �����������
-- ��������! ��������� ���������� �������� ���������� ���������, ������� ������ ���� ����������� �� ������ ����������� ��������
--
procedure set_reviews_exported
(
  p_review_ids          mantas.rf_tab_number,   -- ��������� ��������������� ������� 
  p_filename            varchar2                -- ��� �����, � ������� ���� ��������� ������
);
--
-- ���������� ����� ������ ������ � ������������ � ����������� ��� ��� ��������� � ��� ������� ��������
-- (���� ������ �� ������ ����������, �� ������������ NULL)
--
function get_new_status
(
  par_actvy_type_cd     mantas.kdd_activity_type_cd.actvy_type_cd%type, -- ��� ��������
  par_status_cd         kdd_review.status_cd%type                       -- ������� ������ ������
)
return kdd_review.status_cd%type; -- ����� ������ ������  
--
-- �������� ��������� ����������� ��� ������ (� ��������� ��� � ����� ��� ������)
--
function get_last_note
(
  par_review_id         kdd_review.review_id%type                       -- ID ������
)
return clob; -- ����� ���������� �� ���� ����������� + (��� � ����� ��� ������)  
--
-- ������������ �������� ������� � ������ �����, ��������� � ��������� ���������
-- ������� ����� ������������� � ��������, ����� � ����� ��������� ������� ��������� ������� (��-�� ���������� ������ �����)
--
function get_review_scrty_desc
(
  par_op_cat_cd             varchar2,                                     -- ��������� ��������: WT - �����������, CT - ��������
  par_trxn_seq_id           number                                        -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN)
)
return varchar2; -- �������� ������������ ������� � ������ �����, ��������� � ��������� ���������
--
-- ������������ �����, ����������� ��������� ��������� � �������� ��� ���������� ������ (� ��� ����� ���� ������������� �������� � ���������)
--
function get_review_trxn_change
(
  par_review_id         kdd_review.review_id%type                        -- ID ������
) 
return varchar2; -- �����, ������������ �� ���������� ������������ - �������� ������������ ��������� ��� ����� ��� ������������� ��������
--
-- �������� ��� ������ ��������/������ ������ (���������� �������� ���������� ��������� � ��������������� ������) � ������������ �������� ������������ ���������
--
function get_trxn_change
(
  par_review_id         kdd_review.review_id%type,                       -- ID ������
  par_new_repeat_nb     rf_kdd_review_run.repeat_nb%type,                -- ���������� ����� "������" ��������� ������, ���� null - ���������, ������������ ��� ������
  par_old_repeat_nb     rf_kdd_review_run.repeat_nb%type                 -- ���������� ����� "�����������" ��������� ������, ���� null - �� 1 ������ "������"
) 
return varchar2; -- �������� ������������ ������� ����: ���������� �������, �����, ������������ �����������; null, ���� ��������� ���
--
-- �������� ��� ������ ��������/������ ������ (���������� ������ ���������) � ������������ �������� ������������ ���������
--
function get_trxn_change
(
  par_op_cat_cd             varchar2,                                     -- ��������� ��������: WT - �����������, CT - ��������, BOT - ����������
  par_trxn_seq_id           number,                                       -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN/BACK_OFFICE_TRXN)
  par_trxn_scrty_seq_id     number,                                       -- ID ������ ������ � �������� (PK ������ RF_WIRE_TRXN_SCRTY/RF_CASH_TRXN_SCRTY)
  par_new_arh_dump_dt       date,                                         -- ���� "�����" ������ ��������, ���� null - ������� ������
  par_old_arh_dump_dt       date                                          -- ���� "�������" ������ ��������, ���� null - ������� ������
) 
return varchar2; -- �������� ������������ ������� ����: ���������� �������, �����, ������������ �����������; null, ���� ��������� ���
--
-- ������������ �������� ��������� � ���������� ����� ���� ��� ������
--
function get_opok_change
(
  par_review_id         kdd_review.review_id%type,                       -- ID ������
  par_new_repeat_nb     rf_kdd_review_run.repeat_nb%type default null,   -- ���������� ����� "������" ��������� ������, ���� null - ���������, ������������ ��� ������
  par_old_repeat_nb     rf_kdd_review_run.repeat_nb%type default null    -- ���������� ����� "�����������" ��������� ������, ���� null - �� 1 ������ "������"
) 
return varchar2; -- �������� ��������� � ����� ���� ����: =5002, +4005, -3001; null, ���� ��������� ��������� ���
--
-- �������� �������� ��������� ������� ��������� �������, 
-- ��� ���� ���������� � ���������� ����� ������ ������� (� ������������ � ��������� ��������� � ������� ��������)
-- ��������������� ���������, ������������ ���������� do_actions � ������������ ���������� ���������. 
--
procedure update_reviews
(
  p_review_ids          mantas.rf_tab_number,                           -- ��������� ��������������� ������� 
  p_actvy_type_cd       mantas.kdd_activity_type_cd.actvy_type_cd%type, -- ��� ��������
  p_new_owner_seq_id    kdd_review.owner_seq_id%type,                   -- ����� ������������� (���� null, �� �� ����������)
  p_new_owner_org       kdd_review.owner_org%type,                      -- ������������� ������ �������������� (���� ����� ������������� null, �� �� ����������)
  p_due_dt              kdd_review.due_dt%type,                         -- ����� ���� ��������� (���� null, �� �� ����������)
  p_opok_nb             kdd_review.rf_opok_nb%type,                     -- ����� �������� ��� ���� (���� null, �� �� ����������)
  p_add_opoks_tx        kdd_review.rf_add_opoks_tx%type,                -- ����� �������������� ���� ���� (���� �� ������ �������� ��� ����, �� �� ����������)
  p_owner_seq_id        kdd_review_owner.owner_seq_id%type              -- ������� ������������, ����������� ��������
);
--
-- ������� ����� ����� � ������������ � ������������ ��������� (������������ ��������) ��� "�������"
-- ������� �� ��������� COMMIT!
--
function create_review
(
  par_object_tp_cd          kdd_review.rf_object_tp_cd%type,      -- TRXN - ����� �� ��������; TRXN_SCRTY - ����� �� ������ ������ � ��������; TRXN_SCRTY_JOINT - ������������ ����� �� �������� � �� (������������) ������ ������ � ���
  par_op_cat_cd             kdd_review.rf_op_cat_cd%type,         -- ��������� ��������: WT, CT, BOT (�����������, ��������, ����������)
  par_trxn_seq_id           kdd_review.rf_trxn_seq_id%type,       -- ������������� ��������
  par_trxn_scrty_seq_id     kdd_review.rf_trxn_scrty_seq_id%type, -- ������������� ������ � ������ ������ � ��������
  par_opok_list             varchar2,                             -- ������ � ������ ���� � ���������������� ������, ��� ������� ������� �������� (� �������: <��� ���� 1>~<ID �������>~<ID ������ ������� ����>~||<��������� � ��������� (�����)>|<��� ���� 2>~<ID �������>~<ID ������ ������� ����>~||<��������� � ��������� (�����)>|� �. �.)
  par_data_dump_dt          kdd_review.rf_data_dump_dt%type,      -- ����� ������� �������� ������ - ���� �������� �������� (������� DATA_DUMP_DT � ��������������� ������� ��������, ���� �� ������� - ������������ ��������)
  par_branch_id             kdd_review.rf_branch_id%type,         -- ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
  par_owner_seq_id          kdd_review.owner_seq_id%type,         -- �������������, ��������������� � ����� (���� null, �� ���������������)
  par_owner_org             kdd_review.owner_org%type,            -- ������������� ��������������, ��������������� � ����� (���� ������������� null, �� �� ���������������)
  par_trxn_dt               date,                                 -- ���� ���������� ��������, ������������ ��� ���������� ����� ��������� ������������ ������ (���� �� ������� - ������������ ��������)
  par_creat_id              kdd_review.creat_id%type,             -- ������������, ��������� ����� (���� ����� ������� ��������� �������, �� id ������������ MANTAS)
  par_prcsng_batch_cmplt_fl kdd_review.prcsng_batch_cmplt_fl%type, -- ���� Y/N - ������ �� ������� �������� (���� ���, ����� �� ����� �� ����������)
  par_actvy_type_cd         kdd_activity_type_cd.actvy_type_cd%type default null,  -- ��� ��������, ��������������� ��������� ��������, ��������������� �������� ������ (����������� ��� �������, ����������� ��� ��������� ���������)
  par_note_tx               kdd_note_hist.note_tx%type default null, -- ����������� � ��������� ���� � ������������ ������
  par_cmmnt_id              kdd_cmmnt.cmmnt_id%type default null, -- ������������� ������������ ����������� � ������������ ������
  par_kgrko_party_cd        kdd_review.rf_kgrko_party_cd%type default null, -- �������, � ������� ������ ����� (1 - ����������, 2 - ����������) - ����������� ��� ������������� ��������
  par_opok_nb               kdd_review.rf_opok_nb%type default null, -- �������� ��� ���� - ����������� ��� ������ �������� ������
  par_add_opoks_tx          kdd_review.rf_add_opoks_tx%type default null, -- �������������� ���� ���� - ����� ����������� ��� ������ �������� ������ 
  par_status_cd             kdd_review.status_cd%type default null, -- ������, � ������� ������� ������� ����� - ����������� ��� ������ �������� ������
  par_non_opok_fl           rf_kdd_review_run.non_opok_fl%type default null -- ������� ���������� ����� ���� ��� �������������� ��������� - ����������� ��� ������ �������� ������, ������ ���� ����� M (Manual)
)
return kdd_review.review_id%type; -- ID ���������� ������
--
-- ���������������� ��������� �������� ��� ��������� ������� � ������� ��������������� �����������
-- ������� ������ ��������������� ��������� ��������, ��� ���� � �������� ������� ������� ������������ id ������
-- ��������������� ���������, ������������ ���������� do_actions � ������������ ���������� ���������. 
--
function create_activities
(
  p_review_ids          mantas.rf_tab_number,              -- ��������� ��������������� ������� 
  p_actvy_type_cd       kdd_activity.actvy_type_cd%type,   -- ��� ������������ ��������
  p_cmmnt_id            kdd_cmmnt.cmmnt_id%type,           -- ������������� ������������ �����������
  p_note_tx             varchar2,                          -- ����� �����������
  p_owner_seq_id        kdd_review_owner.owner_seq_id%type -- ������� ������������, ����������� ��������
)
return tab_number;
-- 
-- �� ��, ��� � ����������, �� ��� ������������� ��������
--
procedure create_activities
(
  p_review_ids          mantas.rf_tab_number,              -- ��������� ��������������� ������� 
  p_actvy_type_cd       kdd_activity.actvy_type_cd%type,   -- ��� ������������ ��������
  p_cmmnt_id            kdd_cmmnt.cmmnt_id%type,           -- ������������� ������������ �����������
  p_note_tx             varchar2,                          -- ����� �����������
  p_owner_seq_id        kdd_review_owner.owner_seq_id%type -- ������� ������������, ����������� ��������
);
--
-- ������� ���������� ������
-- � ������ ��������� ���������� ������ �� ���������� = null
-- � ������ ������ ���������� ���������� ����� ������
--
function add_attachment 
(
  p_alert_id in number,       -- ID ��������
  p_user in varchar2,         -- ��� ������������
  p_attach_name in varchar2,  -- ������������, �������� �����
  p_cmmt_id in number,        -- ID �����������
  p_note_tx in varchar2,      -- ������������ �����������
  p_file_name in varchar2,    -- ��� �����
  p_file_type in varchar2,    -- ��� �����
  p_file_charset in varchar2, -- ��������� �����
  p_file_blob in blob,         -- ���������� �����
  p_ip_address in varchar2 default '0.0.0.0' -- IP ����� ������������

) 
return varchar2;
--
-- ������� ������ ������
--
function get_attachment 
(
  p_attch_id in number,        -- ID �����
  p_file_name out varchar2,    -- ��� �����
  p_content_type out varchar2, -- ��� �����
  p_content_data out blob,     -- ���� �����
  p_charset out varchar2,      -- ��������� �����
  p_user in varchar2 default 'UNKNOWN',      -- ��� ������������
  p_ip_address in varchar2 default '0.0.0.0' -- IP ����� ������������
) 
return varchar2;
--
-- ����� �������� �����
--
procedure delete_attachment
(
  p_attch_id in mantas.rf_attachment.attch_seq_id%type, -- ID �����
  p_user in varchar2                                    -- ��� ������������
);
--
--������� ��� ���������� �������
--
function create_narrative(
  p_alert_id in number,
  p_narrative in clob,
  p_user in varchar2
) return number;
--
-- ����������� � �������� �������� ����� �������� ������
--
PROCEDURE archive_old_alerts;
--
-- ������� ����������, ���� �� �������� � ������.
--   1 - ����
--   0 - ���
--
function has_attachments(
  p_alert_id in number
) return number parallel_enable;
--
-- ������� ��������� ��������� ��������: 
--   - ������������� �������� p_send_oes_org_id � / ��� p_rcv_oes_org_id
--   - ������� ��� ���������� �������� (p_alert_party_fl)
--      1 - � ������� ����� �����������;
--      2 - � ������� ����� ����������;
--      3 - ���������� �������������. ��� �������� �������� ��������� �� ���������.
--
function set_trxn_kgrko(
  p_op_cat_cd       in varchar, -- ��� ��������
  p_trxn_seq_id     in number,  -- ����� ��������
  p_send_oes_org_id in number,  -- ����� ������� �����������
  p_rcv_oes_org_id  in number,  -- ����� ������� ����������
  p_alert_party_fl  in number,  -- ���� ������������� ������������ �������
  p_user            in varchar2 -- ��� ������������
) return mantas.rf_tab_number;

end rf_pkg_review;
/

create or replace package body mantas.rf_pkg_review is
--
-- �������� ���������� ���������, ����������� ������� ��������. ������������ � ��������� set_reviews_exported.
--
pck_actvy_type_cd       mantas.kdd_activity_type_cd.actvy_type_cd%type; -- ��� ��������
pck_new_owner_seq_id    kdd_review.owner_seq_id%type;                   -- ����� ������������� (���� null, �� �� ����������)
pck_new_owner_org       kdd_review.owner_org%type;                      -- ������������� ������ �������������� (���� ����� ������������� null, �� �� ����������)
pck_due_dt              kdd_review.due_dt%type;                         -- ����� ���� ��������� (���� null, �� �� ����������)
pck_opok_nb             kdd_review.rf_opok_nb%type;                     -- ����� �������� ��� ���� (���� null, �� �� ����������)
pck_add_opoks_tx        kdd_review.rf_add_opoks_tx%type;                -- ����� �������������� ���� ���� (���� �� ������ �������� ��� ����, �� �� ����������)
pck_cmmnt_id            kdd_cmmnt.cmmnt_id%type;
pck_note_tx             kdd_note_hist.note_tx%type;
pck_owner_seq_id        kdd_review_owner.owner_seq_id%type;             -- ������� ������������, ����������� ��������
pck_owner_id            kdd_review_owner.owner_id%type;                 -- ����� �������� ������������
pck_review_ids          mantas.rf_tab_number;                           -- �������������� �������, ����������� � �����
pck_file_count          integer;                                        -- ���������� ��������� ������ ��� �������� �������
--
-- ��������������� ��������� � �������
--
--���������� ID ������������
function get_owner_id (
  p_user in mantas.kdd_review_owner.owner_id%type,   -- ��� ������������
  p_prcname in varchar2                              -- ��� ������
) return number
is
  v_owner_id mantas.kdd_review_owner.owner_seq_id%type;
begin
  select max(owner_seq_id)
    into v_owner_id
    from mantas.kdd_review_owner
   where owner_id = p_user;
  
  if v_owner_id is null then
    raise_application_error( -20001, '������ �������� ������� ������������: '||p_user||' ('||p_prcname||')' );
  end if; 
  
  return v_owner_id;
end;
--
-- �������� �������� ��������� ������� ��������� �������, 
-- ��� ���� ���������� � ���������� ����� ������ ������� (� ������������ � ��������� ��������� � ������� ��������)
--
procedure update_reviews
(
  p_review_ids          mantas.rf_tab_number,                           -- ��������� ��������������� ������� 
  p_actvy_type_cd       mantas.kdd_activity_type_cd.actvy_type_cd%type, -- ��� ��������
  p_new_owner_seq_id    kdd_review.owner_seq_id%type,                   -- ����� ������������� (���� null, �� �� ����������)
  p_new_owner_org       kdd_review.owner_org%type,                      -- ������������� ������ �������������� (���� ����� ������������� null, �� �� ����������)
  p_due_dt              kdd_review.due_dt%type,                         -- ����� ���� ��������� (���� null, �� �� ����������)
  p_opok_nb             kdd_review.rf_opok_nb%type,                     -- ����� �������� ��� ���� (���� null, �� �� ����������)
  p_add_opoks_tx        kdd_review.rf_add_opoks_tx%type,                -- ����� �������������� ���� ���� (���� �� ������ �������� ��� ����, �� �� ����������)
  p_owner_seq_id        kdd_review_owner.owner_seq_id%type              -- ������� ������������, ����������� ��������
) is
begin
  merge /*+ NO_PARALLEL*/ into mantas.kdd_review t using
    (select /*+ NO_PARALLEL*/
            review_id,
            nvl(st_new.status_cd, rv.status_cd)      as status_cd,  -- ����� ������ ������
            case when nvl(st_old.closed_status_fl, 'N') = 'N' and
                      nvl(st_new.closed_status_fl, 'N') = 'Y' 
                 then 'CLOSED' -- ����� ������� � ��������� ������ ��� ������ ��������
                 when nvl(st_old.closed_status_fl, 'N') = 'Y' and
                      nvl(st_new.closed_status_fl, 'N') = 'N' 
                 then 'OPENED' -- ����� ��������� �� ���������� ������� ��� ������ ��������
                 when nvl(st_old.closed_status_fl, 'N') = nvl(st_new.closed_status_fl, 'N')
                 then 'SAME'   -- ����� ��� ���, ��� � ������� ���� � ���������, ���� � ����������� �������
            end as closed_status, 
            act.cls_class_cd,  -- ����� �������� ������  
            nvl(p_new_owner_seq_id, rv.owner_seq_id) as owner_seq_id,
            case when p_new_owner_seq_id is not null
                 then p_new_owner_org
                 else rv.owner_org
            end as owner_org,
            nvl(p_due_dt, rv.due_dt)                 as due_dt,
            nvl(p_opok_nb, rv.rf_opok_nb)            as rf_opok_nb,
            case when p_opok_nb is not null
                 then p_add_opoks_tx
                 else rv.rf_add_opoks_tx
            end                                      as rf_add_opoks_tx                    
       from table(p_review_ids) ids,
            mantas.kdd_review rv,
            mantas.kdd_review_status st_new,
            mantas.kdd_review_status st_old,
            mantas.kdd_activity_type_cd act
      where rv.review_id = ids.column_value and
            st_old.status_cd = rv.status_cd and
            st_new.status_cd(+) = mantas.rf_pkg_review.get_new_status(p_actvy_type_cd, rv.status_cd) and
            act.actvy_type_cd = p_actvy_type_cd) v
  on (t.review_id = v.review_id)
  when matched then
    update 
       set t.status_cd = v.status_cd,
           t.owner_seq_id = v.owner_seq_id,
           t.owner_org = v.owner_org,
           t.due_dt = v.due_dt,
           t.rf_opok_nb = v.rf_opok_nb,
           t.rf_add_opoks_tx = v.rf_add_opoks_tx,
           t.status_dt = case when t.status_cd <> v.status_cd then sysdate else t.status_dt end,
           t.last_actvy_type_cd = p_actvy_type_cd,
           t.cls_id             = case v.closed_status when 'CLOSED' then p_owner_seq_id  when 'OPENED' then to_number(null) else t.cls_id end,
           t.cls_actvy_type_cd  = case v.closed_status when 'CLOSED' then p_actvy_type_cd when 'OPENED' then to_char(null)   else t.cls_actvy_type_cd end,
           t.cls_class_cd       = case v.closed_status when 'CLOSED' then v.cls_class_cd  when 'OPENED' then to_char(null)   else t.cls_class_cd end,
           t.rf_cls_dt          = case v.closed_status when 'CLOSED' then sysdate         when 'OPENED' then to_date(null)   else t.rf_cls_dt end,
           t.rf_partition_key   = case when nvl(v.closed_status, '?') <> 'CLOSED' then 'ACT' else t.rf_partition_key end, -- ����������� � �������� ��������, ���� ����� �� ������
           t.lock_id = null, -- ������� ���������� � ������, ���� ��� ����,
           t.lock_ts = null; 
end update_reviews;  
--
-- ������� ����� ����� � ������������ � ������������ ��������� (������������ ��������) ��� "�������"
-- ������� �� ��������� COMMIT!
--
function create_review
(
  par_object_tp_cd          kdd_review.rf_object_tp_cd%type,      -- TRXN - ����� �� ��������; TRXN_SCRTY - ����� �� ������ ������ � ��������; TRXN_SCRTY_JOINT - ������������ ����� �� �������� � �� (������������) ������ ������ � ���
  par_op_cat_cd             kdd_review.rf_op_cat_cd%type,         -- ��������� ��������: WT, CT, BOT (�����������, ��������, ����������)
  par_trxn_seq_id           kdd_review.rf_trxn_seq_id%type,       -- ������������� ��������
  par_trxn_scrty_seq_id     kdd_review.rf_trxn_scrty_seq_id%type, -- ������������� ������ � ������ ������ � ��������
  par_opok_list             varchar2,                             -- ������ � ������ ���� � ���������������� ������, ��� ������� ������� �������� (� �������: <��� ���� 1>~<ID �������>~<ID ������ ������� ����>~||<��������� � ��������� (�����)>|<��� ���� 2>~<ID �������>~<ID ������ ������� ����>~||<��������� � ��������� (�����)>|� �. �.)
  par_data_dump_dt          kdd_review.rf_data_dump_dt%type,      -- ����� ������� �������� ������ - ���� �������� �������� (������� DATA_DUMP_DT � ��������������� ������� ��������, ���� �� ������� - ������������ ��������)
  par_branch_id             kdd_review.rf_branch_id%type,         -- ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
  par_owner_seq_id          kdd_review.owner_seq_id%type,         -- �������������, ��������������� � ����� (���� null, �� ���������������)
  par_owner_org             kdd_review.owner_org%type,            -- ������������� ��������������, ��������������� � ����� (���� ������������� null, �� �� ���������������)
  par_trxn_dt               date,                                 -- ���� ���������� ��������, ������������ ��� ���������� ����� ��������� ������������ ������ (���� �� ������� - ������������ ��������)
  par_creat_id              kdd_review.creat_id%type,             -- ������������, ��������� ����� (���� ����� ������� ��������� �������, �� id ������������ MANTAS)
  par_prcsng_batch_cmplt_fl kdd_review.prcsng_batch_cmplt_fl%type, -- ���� Y/N - ������ �� ������� �������� (���� ���, ����� �� ����� �� ����������)
  par_actvy_type_cd         kdd_activity_type_cd.actvy_type_cd%type default null,  -- ��� ��������, ��������������� ��������� ��������, ��������������� �������� ������ (����������� ��� �������, ����������� ��� ��������� ���������)
  par_note_tx               kdd_note_hist.note_tx%type default null, -- ����������� � ��������� ���� � ������������ ������
  par_cmmnt_id              kdd_cmmnt.cmmnt_id%type default null, -- ������������� ������������ ����������� � ������������ ������
  par_kgrko_party_cd        kdd_review.rf_kgrko_party_cd%type default null, -- �������, � ������� ������ ����� (1 - ����������, 2 - ����������) - ����������� ��� ������������� ��������
  par_opok_nb               kdd_review.rf_opok_nb%type default null, -- �������� ��� ���� - ����������� ��� ������ �������� ������
  par_add_opoks_tx          kdd_review.rf_add_opoks_tx%type default null, -- �������������� ���� ���� - ����� ����������� ��� ������ �������� ������ 
  par_status_cd             kdd_review.status_cd%type default null, -- ������, � ������� ������� ������� ����� - ����������� ��� ������ �������� ������
  par_non_opok_fl           rf_kdd_review_run.non_opok_fl%type default null -- ������� ���������� ����� ���� ��� �������������� ��������� - ����������� ��� ������ �������� ������, ������ ���� ����� M (Manual)
)
return kdd_review.review_id%type is -- ID ���������� ������

  var_review_id         kdd_review.review_id%type;
  var_review_ids        mantas.rf_tab_number := rf_tab_number();
  var_actvy_ids         rf_pkg_review.tab_number;
  
  var_err_code          number;
  var_err_msg           varchar2(255);
begin
  --
  -- �������� ����������
  --
  if par_op_cat_cd not in ('WT', 'CT', 'BOT') or par_op_cat_cd is null or par_trxn_seq_id is null or 
     (par_opok_list is null and par_opok_nb is null) or par_kgrko_party_cd not in (1, 2) or 
     par_non_opok_fl not in ('Y', 'J', 'M') then
    raise_application_error(-20001, '�������� �������� ��������� (mantas.rf_pkg_review.create_review)');
  end if;  
  --
  -- ������� ������ � �������� ������� ������� � ������� �������� ���������
  --
  var_review_id := KDD_REVIEW_SEQ.NextVal;
  /*mantas.p_sequence_nextval('REVIEW_ID_SEQ', var_review_id, var_err_code, var_err_msg); 
  if var_review_id is null then
    raise_application_error(-20001, '������ ��� ��������� ���������� �������������� (REVIEW_ID_SEQ): '||var_err_code||' '||var_err_msg||' (mantas.rf_pkg_review.create_review)');
  end if;*/    

  insert /*+ NO_PARALLEL*/ all
         into kdd_review
           (review_id, creat_id, creat_ts, lock_id, lock_ts, status_cd, status_dt, cntry_id, cls_id, cntry_key_id, 
            scnro_id, scnro_class_cd, pttrn_id, dmn_cd, alert_ct, reopn_fl, reasn_fl, owner_seq_id, scnro_ct, score_ct, score_fl, 
            due_dt, auto_cls_ct, cstm_1_tx, cstm_2_tx, cstm_3_tx, cstm_4_tx, cstm_5_tx, cstm_1_dt, cstm_2_dt, cstm_3_dt, 
            cstm_1_rl, cstm_2_rl, cstm_3_rl, auto_reasn_fl, last_actvy_type_cd, prev_match_ct_sm_scnro, prev_match_ct_all, 
            owner_org, age, focal_ntity_dsply_nm, focal_ntity_dsply_id, prev_match_ct_sm_scnro_class, send_ntfctn_cd, 
            bus_dmn_st, bus_dmn_dsply_nm_st, jrsdcn_cd, prcsng_batch_nm, prcsng_batch_cmplt_fl, hilgt_tx, orig_owner_seq_id, 
            cls_actvy_type_cd, cls_class_cd, scnro_displ_nm, extrl_ref_id, extrl_ref_link, extrl_ref_src_id, rqst_audit_seq_id, 
            prcsng_dt, review_type_cd, alert_case_ct, last_link_fl, link_updt_ts, link_updt_id, reg_status_list, 
            rf_object_tp_cd, rf_op_cat_cd, rf_trxn_seq_id, rf_trxn_scrty_seq_id, rf_repeat_nb, rf_opok_nb, rf_add_opoks_tx,  
            rf_branch_id, rf_data_dump_dt, rf_cls_dt, rf_trxn_dt, rf_trxn_base_am, rf_partition_key, rf_alert_type_cd, rf_kgrko_party_cd)
         values
           (review_id, creat_id, creat_ts, lock_id, lock_ts, status_cd, status_dt, cntry_id, cls_id, cntry_key_id, 
            scnro_id, scnro_class_cd, pttrn_id, dmn_cd, alert_ct, reopn_fl, reasn_fl, owner_seq_id, scnro_ct, score_ct, score_fl, 
            due_dt, auto_cls_ct, cstm_1_tx, cstm_2_tx, cstm_3_tx, cstm_4_tx, cstm_5_tx, cstm_1_dt, cstm_2_dt, cstm_3_dt, 
            cstm_1_rl, cstm_2_rl, cstm_3_rl, auto_reasn_fl, last_actvy_type_cd, prev_match_ct_sm_scnro, prev_match_ct_all, 
            owner_org, age, focal_ntity_dsply_nm, focal_ntity_dsply_id, prev_match_ct_sm_scnro_class, send_ntfctn_cd, 
            bus_dmn_st, bus_dmn_dsply_nm_st, jrsdcn_cd, prcsng_batch_nm, prcsng_batch_cmplt_fl, hilgt_tx, orig_owner_seq_id, 
            cls_actvy_type_cd, cls_class_cd, scnro_displ_nm, extrl_ref_id, extrl_ref_link, extrl_ref_src_id, rqst_audit_seq_id, 
            prcsng_dt, review_type_cd, alert_case_ct, last_link_fl, link_updt_ts, link_updt_id, reg_status_list, 
            rf_object_tp_cd, rf_op_cat_cd, rf_trxn_seq_id, rf_trxn_scrty_seq_id, rf_repeat_nb, rf_opok_nb, rf_add_opoks_tx,  
            rf_branch_id, rf_data_dump_dt, rf_cls_dt, rf_trxn_dt, rf_trxn_base_am, rf_partition_key, 'O', rf_kgrko_party_cd)
         into rf_kdd_review_run
           (review_id, repeat_nb, data_dump_dt, non_opok_fl, actvy_id)
         values
           (review_id, 0, rf_data_dump_dt, par_non_opok_fl, to_number(null))                                        
         into kdd_review_scnro
           (review_id, scnro_id, match_ct)
         values
           (review_id, scnro_id, 0)                                        
        (select /*+ NO_PARALLEL*/
                var_review_id    as review_id, 
                par_creat_id     as creat_id, 
                sysdate          as creat_ts, 
                to_number(null)  as lock_id, 
                to_date(null)    as lock_ts, 
                nvl(par_status_cd, 'NW') as status_cd, 
                sysdate          as status_dt, 
                113000004        as cntry_id,     -- ��� ��������� �������: ������ (�� ������ ��������� ������� "��������")
                to_number(null)  as cls_id, 
                -1               as cntry_key_id, -- ��������� ������
                101              as scnro_id,     -- �������� ��������� ���� (� 2 ��������� ����), ����������� �� ������ ������, ����� ������������� � NULL
                'ML'             as scnro_class_cd, 
                1010             as pttrn_id, 
                'MTS'            as dmn_cd, 
                1                as alert_ct, 
                'N'              as reopn_fl, 
                'N'              as reasn_fl, 
                par_owner_seq_id as owner_seq_id, 
                1                as scnro_ct, 
                to_number(null)  as score_ct, 
                'Y'              as score_fl,
                case when opok.opok_nb = 6001 and nvl(par_non_opok_fl, '?') <> 'M' -- �������������� ��������� 6001 ���� - ��� ����� 
                     then to_date(null)
                     else mantas.rf_add_work_days(coalesce(par_trxn_dt, wt.trxn_exctn_dt, ct.trxn_exctn_dt, bot.exctn_dt), 
                                                  nvl(to_number(rf_pkg_adm.get_install_param(9001)), 3)) -- ���� ������������� �������� (���. ����)                                                   
                end              as due_dt, 
                to_number(null)  as auto_cls_ct, 
                to_char(null)    as cstm_1_tx, 
                to_char(null)    as cstm_2_tx, 
                to_char(null)    as cstm_3_tx, 
                to_char(null)    as cstm_4_tx, 
                to_char(null)    as cstm_5_tx, 
                to_date(null)    as cstm_1_dt, 
                to_date(null)    as cstm_2_dt, 
                to_date(null)    as cstm_3_dt, 
                to_number(null)  as cstm_1_rl, 
                to_number(null)  as cstm_2_rl, 
                to_number(null)  as cstm_3_rl, 
                'N'              as auto_reasn_fl, 
                'MTS000'         as last_actvy_type_cd, -- �������� ������
                0                as prev_match_ct_sm_scnro, 
                0                as prev_match_ct_all,  
                par_owner_org    as owner_org, 
                0                as age, 
                '������ �� �� �����������' as focal_ntity_dsply_nm, 
                -1               as focal_ntity_dsply_id, -- ��������� ������
                0                as prev_match_ct_sm_scnro_class, 
                'D'              as send_ntfctn_cd,
                'a'              as bus_dmn_st, 
                'GEN'            as bus_dmn_dsply_nm_st, 
                'RF'             as jrsdcn_cd, 
                'DLY'            as prcsng_batch_nm, 
                par_prcsng_batch_cmplt_fl as prcsng_batch_cmplt_fl, 
                to_char(null)    as hilgt_tx, 
                par_owner_seq_id as orig_owner_seq_id,
                to_char(null)    as cls_actvy_type_cd, 
                to_char(null)    as cls_class_cd, 
                to_char(null)    as scnro_displ_nm, 
                to_char(null)    as extrl_ref_id, 
                to_char(null)    as extrl_ref_link, 
                to_char(null)    as extrl_ref_src_id, 
                to_number(null)  as rqst_audit_seq_id,
                trunc(sysdate)   as prcsng_dt, 
                'AL'             as review_type_cd, 
                0                as alert_case_ct, 
                to_char(null)    as last_link_fl, 
                to_date(null)    as link_updt_ts, 
                to_number(null)  as link_updt_id, 
                to_char(null)    as reg_status_list,
                par_object_tp_cd as rf_object_tp_cd, 
                par_op_cat_cd    as rf_op_cat_cd, 
                par_trxn_seq_id  as rf_trxn_seq_id, 
                par_trxn_scrty_seq_id as rf_trxn_scrty_seq_id, 
                0                as rf_repeat_nb, 
                nvl2(par_opok_nb, par_opok_nb,      opok.opok_nb)      as rf_opok_nb, 
                nvl2(par_opok_nb, par_add_opoks_tx, opok.add_opoks_tx) as rf_add_opoks_tx, 
                par_branch_id    as rf_branch_id,
                coalesce(par_data_dump_dt, wt.data_dump_dt, ct.data_dump_dt, bot.data_dump_dt) as rf_data_dump_dt, 
                to_date(null)    as rf_cls_dt,
                coalesce(wt.trxn_exctn_dt, ct.trxn_exctn_dt, bot.exctn_dt) as rf_trxn_dt, 
                coalesce(wt.trxn_base_am, ct.trxn_base_am, bot.trxn_base_am) as rf_trxn_base_am, 
                'ACT' as rf_partition_key,
                par_kgrko_party_cd as rf_kgrko_party_cd                
           from dual d
                left join (select max(first_opok_nb) as opok_nb,
                                  listagg(nullif(to_number(opok), first_opok_nb), ',') within group (order by priority_nb) as add_opoks_tx
                             from (select distinct 
                                          trim(regexp_substr(column_value, '[^\~]+', 1, 1)) as opok,
                                          nvl(opk.priority_nb, opk.opok_nb) as priority_nb,
                                          first_value(opk.opok_nb) over(order by nvl(opk.priority_nb, opk.opok_nb)) as first_opok_nb  
                                     from table(rf_pkg_scnro.list_to_tab(replace(par_opok_list, '~', ' ~ '), '|')) t
                                          join rf_opok opk on opk.opok_nb = to_number(trim(regexp_substr(t.column_value, '[^\~]+', 1, 1))))) opok on 1 = 1
                left join business.wire_trxn wt on par_op_cat_cd = 'WT' and
                                                   wt.fo_trxn_seq_id = par_trxn_seq_id
                left join business.cash_trxn ct on par_op_cat_cd = 'CT' and
                                                   ct.fo_trxn_seq_id = par_trxn_seq_id
                left join business.back_office_trxn bot on par_op_cat_cd = 'BOT' and
                                                           bot.bo_trxn_seq_id = par_trxn_seq_id);
  --
  -- ���������� �������� ���������� ����
  --          
  insert into rf_kdd_review_opok(record_id, review_id, repeat_nb, trxn_scrty_seq_id, opok_nb, explanation_tx, snapshot_id)
    (select /*+ NO_PARALLEL*/ 
            rf_kdd_review_opok_seq.nextval as record_id,         
            var_review_id, 
            0 as repeat_nb,
            to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 5))) as trxn_scrty_seq_id,
            to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 1))) as opok_nb,
            trim(regexp_substr(opok.column_value, '[^\~]+', 1, 4)) as explanation_tx,
            rls.snapshot_id
       from table(mantas.rf_pkg_scnro.list_to_tab(replace(par_opok_list, '~', ' ~ '), '|')) opok
            join rf_opok_rule_snapshot rls on rls.rule_seq_id = to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 2))) and 
                                              rls.record_seq_id = to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 3))) and
                                              rls.actual_fl = 'Y');  
  --
  -- ������������ �������� � ��������������� �����������, ������������� ������ �� �������� � ������� �������� ���������
  --
  var_review_ids.extend;
  var_review_ids(1) := var_review_id; 
  var_actvy_ids := rf_pkg_review.create_activities(p_review_ids    => var_review_ids, 
                                                   p_actvy_type_cd => nvl(par_actvy_type_cd, 'MTS000'),
                                                   p_cmmnt_id      => par_cmmnt_id,
                                                   p_note_tx       => par_note_tx,
                                                   p_owner_seq_id  => par_creat_id);
                                                   
  if var_actvy_ids is not null and var_actvy_ids.count() > 0 then                                
    update /*+ NO_PARALLEL*/ rf_kdd_review_run
       set actvy_id = var_actvy_ids(var_review_id)
     where review_id = var_review_id;     
  end if;   
  --
  -- �������
  --  
  return var_review_id;
exception
  when others then
    if sqlcode < -20000 then
      raise;
    else  
      raise_application_error(-20001, '������ ��� �������� ������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||sqlerrm||' (mantas.rf_pkg_review.create_review)');
    end if;      
end create_review;  
--
-- ���������������� ��������� �������� ��� ��������� ������� � ������� ��������������� �����������
-- ������� ������ ��������������� ��������� ��������, ��� ���� � �������� ������� ������� ������������ id ������
-- ��������������� ���������, ������������ ���������� do_actions � ������������ ���������� ���������. 
--
FUNCTION create_activities
(
  p_review_ids          mantas.rf_tab_number,              -- ��������� ��������������� ������� 
  p_actvy_type_cd       kdd_activity.actvy_type_cd%type,   -- ��� ������������ ��������
  p_cmmnt_id            kdd_cmmnt.cmmnt_id%type,           -- ������������� ������������ �����������
  p_note_tx             varchar2,                          -- ����� �����������
  p_owner_seq_id        kdd_review_owner.owner_seq_id%type -- ������� ������������, ����������� ��������
)
RETURN TAB_NUMBER IS

  var_result            tab_number;

  var_actvy_id          kdd_activity.actvy_id%type;
  var_note_id           kdd_note_hist.note_id%type;

  var_err_code          number;
  var_err_msg           varchar2(255);
BEGIN
  --
  -- �������� ����������
  --
  if p_review_ids is null or p_review_ids.count() = 0 or trim(p_actvy_type_cd) is null or p_owner_seq_id is null then
    return var_result;
  end if;
  --
  -- ���� �� �������
  --
  for r in (select /*+ NO_PARALLEL*/ rv.review_id, rv.status_cd, rv.owner_seq_id, oes.oes_321_id as rf_oes_id
              from table(p_review_ids) ids
                   join mantas.kdd_review rv on rv.review_id = ids.column_value
                   left join mantas.rf_oes_321 oes on oes.review_id = rv.review_id and
                                                      oes.current_fl = 'Y') loop
    --
    -- ������������ ��������
    --
    var_actvy_id := KDD_ACTIVITY_SEQ.NextVal;
    /*mantas.p_sequence_nextval('ACTIVITY_ID_SEQ', var_actvy_id, var_err_code, var_err_msg); 
    if var_actvy_id is null then
      raise_application_error(-20001, '������ ��� ��������� ���������� �������������� (ACTIVITY_ID_SEQ): '||var_err_code||' '||var_err_msg||' (mantas.rf_pkg_review.create_activities)');
    end if;*/    

    insert into mantas.kdd_activity(actvy_id, review_id, start_dt, due_dt, end_dt, new_review_status_cd, new_review_owner_id, 
                                    actvy_type_cd, creat_id, extrl_ref_id, extrl_ref_link, extrl_ref_src_id, rqst_audit_seq_id,
                                    rf_oes_id)
           values(var_actvy_id, r.review_id, sysdate, null, null, r.status_cd, r.owner_seq_id,
                  p_actvy_type_cd, p_owner_seq_id, null, null, null, null,
                  r.rf_oes_id);                                    

    var_result(r.review_id) := var_actvy_id; 
    --
    -- ���������� ����������� �����������
    --
    if p_cmmnt_id is not null then
      insert into mantas.kdd_activity_cmmnt(actvy_id, cmmnt_id)
            values(var_actvy_id, p_cmmnt_id);
    end if;  
    --
    -- ���������� ��������� �����������
    --
    if trim(p_note_tx) is not null then
      var_note_id := KDD_NOTE_HIST_SEQ.NextVal;
      /*mantas.p_sequence_nextval('NOTE_ID_SEQ', var_note_id, var_err_code, var_err_msg); 
      if var_note_id is null then
        raise_application_error(-20001, '������ ��� ��������� ���������� �������������� (NOTE_ID_SEQ): '||var_err_code||' '||var_err_msg||' (mantas.rf_pkg_review.create_activities)');
      end if;*/    

      insert into mantas.kdd_note_hist(note_id, note_tx, creat_id, creat_ts, modify_fl)
             values(var_note_id, trim(p_note_tx), p_owner_seq_id, sysdate, 'N');         
             
      insert into mantas.kdd_activity_note(actvy_id, note_id)
            values(var_actvy_id, var_note_id);
     end if;       
  end loop;   
  
  return var_result;              
END create_activities;
--
-- ���������������� ��������� �������� ��� ��������� ������� � ������� ��������������� �����������
-- �� ��, ��� � ����������, �� ��� ������������� ��������
--
PROCEDURE create_activities
(
  p_review_ids          mantas.rf_tab_number,              -- ��������� ��������������� ������� 
  p_actvy_type_cd       kdd_activity.actvy_type_cd%type,   -- ��� ������������ ��������
  p_cmmnt_id            kdd_cmmnt.cmmnt_id%type,           -- ������������� ������������ �����������
  p_note_tx             varchar2,                          -- ����� �����������
  p_owner_seq_id        kdd_review_owner.owner_seq_id%type -- ������� ������������, ����������� ��������
) is
  var_dummy      tab_number;
BEGIN 
  var_dummy := create_activities(p_review_ids, p_actvy_type_cd, p_cmmnt_id, p_note_tx, p_owner_seq_id);  
END create_activities;
--
-- ��������� �������� (��������) ��� ������� (��������) - ����� ���������
--
FUNCTION do_actions
(
  par_review_ids          in rf_tab_number,                  -- ������ ��������������� �������, ��� ��������� �������� ����� ���� �� ������
  par_actvy_type_cds      in rf_tab_varchar,                 -- ������ ����� ����� ��������, ������� ���� ��������� � ��������� �������
  par_new_review_owner_id in kdd_review_owner.owner_seq_id%type, -- ID ������������ - ������ �������������� ��� ��������� �������
  par_due_dt              in kdd_review.due_dt%type,         -- ����� ���� ��������� �������
  par_note_tx             in kdd_note_hist.note_tx%type,     -- ����������� � ��������� ���� ��� ������������ ��������
  par_owner_id            in kdd_review_owner.owner_id%type, -- ����� �������� ������������, ������������ ������ ��������
  par_cmmnt_id            in kdd_cmmnt.cmmnt_id%type,        -- ������������� ������������ ����������� ��� ������������ ��������
  par_opok_nb             in kdd_review.rf_opok_nb%type,     -- �������� ��� ���� ��� ��������� � ������
  par_add_opoks_tx        in kdd_review.rf_add_opoks_tx%type,-- �������������� ���� ���� ��� ��������� � ������
  par_act_review_ids     out rf_tab_number                   -- ������ ��������������� �������, ��� �������� ���� ��������� ���� �� ���� ��������
)
RETURN VARCHAR2 IS -- ���������, ������� ������� ���������� �� ���������� ��� ������������ (���� NULL, �� ���������� ������ �� ����)

  var_owner_seq_id             kdd_review_owner.owner_seq_id%type;
  var_act_review_ids           mantas.rf_tab_number := rf_tab_number();   
  var_tmp_review_ids           mantas.rf_tab_number := rf_tab_number();   
  var_tmp_review_ids2          mantas.rf_tab_number := rf_tab_number();   
  var_res_review_ids           mantas.rf_tab_number := rf_tab_number();   
  var_res_review_ids_commit    mantas.rf_tab_number := rf_tab_number();   
  
  var_new_owner_seq_id  kdd_review_owner.owner_seq_id%type; 
  var_new_owner_org     kdd_review.owner_org%type;
  var_new_due_dt        kdd_review.due_dt%type;
  var_new_opok_nb       kdd_review.rf_opok_nb%type;    
  var_new_add_opoks_tx  kdd_review.rf_add_opoks_tx%type;  
  var_cmmnt_id          kdd_cmmnt.cmmnt_id%type;
  
  var_act_msg           varchar2(2000 char);
  var_act_warn_msg      varchar2(2000 char);
  var_err_msg           varchar2(2000 char);
  var_result_msg        varchar2(2000 char);
  var_result_msg_commit varchar2(2000 char);
  var_commit_flag       integer;
  var_msg_required_flag integer;
  
  var_delete_count      integer;
  var_create_count      integer;  
  var_repeat_count      integer;
  var_trxn_sum          integer;
  var_delete_sum        integer;
  var_create_sum        integer;
  var_repeat_sum        integer;
  var_trxn_absent_count integer;
  
  var_review_id         kdd_review.review_id%TYPE; 

  var_act_oes_ids       mantas.rf_tab_number := rf_tab_number();   
  var_oes_321_id        rf_oes_321.oes_321_id%TYPE;
  var_oes_check_status  rf_oes_321.check_status%TYPE;
  var_oes_count         integer;
  var_oes_err_count     integer;
  var_oes_warn_count    integer;
  var_oes_absent_count  integer;

  var_count             integer;
  prcname               varchar2(255) := ' (mantas.rf_pkg_review.do_actions)';  
BEGIN
  --
  -- ���� �������� �� ������� - ������
  --
  if (par_actvy_type_cds is null or par_actvy_type_cds.count = 0) then
    raise_application_error(-20001, '�� ������� �� ������ ��������'||prcname);
  end if;  
  --
  -- ���� ������� �� ������� � ��� �� ������� � ������ - ������
  --
  if (par_review_ids is null or par_review_ids.count = 0) then
    if par_actvy_type_cds is not null and par_actvy_type_cds.count = 1 and par_actvy_type_cds(1) = 'RF_EXPORT' then
      null;
    else  
      raise_application_error(-20001, '�� ������� �� ������ ������'||prcname);
    end if;  
  end if;  
  --
  -- ��������� ID �������� ������������
  --
  select max(owner_seq_id)
    into var_owner_seq_id
    from mantas.kdd_review_owner
   where owner_id = par_owner_id;    
   
  if var_owner_seq_id is null then
    raise_application_error(-20001, '������ �������� ������� ������������: '||par_owner_id||prcname);
  end if; 
  --
  -- ������� � �������� ������������� � ������: ��������������, ���� ���������, ���� ����
  --
  -- �������������
  if par_new_review_owner_id is not null then
    select max(owner_seq_id), max(rptg_group_cd)
      into var_new_owner_seq_id, var_new_owner_org
      from mantas.kdd_review_owner
     where owner_seq_id = par_new_review_owner_id;    
   
    if var_new_owner_seq_id is null then
      raise_application_error(-20001, '������ �������� ����� �������������: '||to_char(par_new_review_owner_id)||prcname);
    end if; 
  end if;  
  
  -- ���� ���������
  var_new_due_dt := par_due_dt;
  
  -- �������� ��� ����
  begin
    if par_opok_nb is not null then
      select opok_nb
        into var_new_opok_nb
        from rf_opok
       where opok_nb = par_opok_nb;
    end if;  
  exception
    when others then
      raise_application_error(-20001, '������ �������� ��� �������������� �������� ��� ���� ��������: '||to_char(par_opok_nb)||prcname);
  end;

  -- �������������� ���� ����
  begin
    if trim(par_add_opoks_tx) is not null then
      select listagg(to_char(ref_opk.opok_nb), ',') within group(order by opk.rn),
             sum(decode(ref_opk.opok_nb, null, 1, 0)) -- ���������� �������������� ����� ����
        into var_new_add_opoks_tx, var_count
        from (select rownum as rn, 
                     to_number(column_value) as opok_nb
                from table(mantas.rf_pkg_scnro.list_to_tab(par_add_opoks_tx))) opk,
             mantas.rf_opok ref_opk
       where ref_opk.opok_nb(+) = opk.opok_nb;
    end if;  
  exception
    when others then
      raise_application_error(-20001, '������� �������� ��� �������������� �������������� ���� ���� ��������: '||par_add_opoks_tx||prcname);
  end;

  if var_count > 0 then
    raise_application_error(-20001, '������� �������� ��� �������������� �������������� ���� ���� ��������: '||par_add_opoks_tx||prcname);
  end if;  
  
  -- id ������������ �����������
  begin
    if par_cmmnt_id is not null then
      select cmmnt_id
        into var_cmmnt_id
        from kdd_cmmnt
       where cmmnt_id = par_cmmnt_id;
    end if;  
  exception
    when others then
      raise_application_error(-20001, '������ �������� ��� �������������� ������������� ������������ �����������: '||to_char(par_cmmnt_id)||prcname);
  end;          
  --
  -- ���� �� ����������� ���������
  --  
  begin 
    var_msg_required_flag := null;
    
    for r in (SELECT distinct act.actvy_type_cd, 
                     case when act.req_reasn_fl = 'Y'
                          then 'Y' 
                          else 'N'
                     end as reasn_fl,                                  -- ������� ��������� ��������������
                     nvl(act.req_due_date_fl, 'N') as due_date_fl,     -- ������� ��������� ����� ��������� (���� �������)
                     case when act.actvy_type_cd = 'RF_SETOPOK' 
                          then 'Y' 
                          else 'N' 
                     end as opok_fl,                                   -- ������� ��������� ����� ����,
                     case when act.next_review_status_cd is not null or 
                              (select count(*)
                                 from mantas.rf_kdd_actvy_type_new_status a_st
                                where a_st.actvy_type_cd = act.actvy_type_cd and
                                      rownum <= 1) > 0
                         then 'Y'             
                         else 'N'
                     end as new_status_flag, -- ������� ��������� ����� ������� ��� ������ ��������
                     case when st.closed_status_fl = 'Y' or 
                              (select count(*)
                                 from mantas.rf_kdd_actvy_type_new_status a_st,
                                      mantas.kdd_review_status st
                                where a_st.actvy_type_cd = act.actvy_type_cd and
                                      st.status_cd = a_st.new_status_cd and
                                      st.closed_status_fl = 'Y' and
                                      rownum <= 1) > 0
                         then 'Y'             
                         else 'N'
                     end as closed_status_flag, -- ������� ���������� �������� � ��������� ������ ��� ������ ��������
                     case when act.actvy_type_cd = 'RF_SETOPOK'
                          then '������� ���������� ���(�) ��������: '||to_char(var_new_opok_nb)||
                               case when var_new_add_opoks_tx is null then null else ', �������������� ����: '||var_new_add_opoks_tx end
                     end as note_tx, 
                     act.displ_order_nb,
                     act.actvy_type_nm,
                     act.rf_oes_actvy_type_cd,
                     case when act.rf_oes_actvy_type_cd = 'CREATE' 
                          then nvl(act.rf_oes_action, '1')
                     end as rf_oes_action,
                     rf_pkg_adm.has_activity(par_owner_id, act.actvy_type_cd) as has_rights_flag
                FROM table(par_actvy_type_cds) cds,
                     --table(mantas.rf_pkg_scnro.list_to_tab('MTS000, MTS001, MTS002, MTS003, MTS008, MTS018, MTS110, MTS112, RF_OES, RF_CANCEL, RF_SETOPOK, RF_EXPORT, RF_REPEAT, RF_OES#, RF_CANCEL#, RF_IGNORE#, RF_DLTD-')) cds,
                     mantas.kdd_activity_type_cd act,
                     mantas.kdd_review_status st
               WHERE act.actvy_type_cd = trim(cds.column_value) and
                     st.status_cd(+) = act.next_review_status_cd
              ORDER BY new_status_flag,             -- ������� ��������, �� ���������� � ����� �������
                       closed_status_flag,          -- ������� �������� �� � ��������� �������
                       act.displ_order_nb,          -- �� ������� ����������� �� ����������
                       act.actvy_type_cd) LOOP      -- �� �������������� �������� - ��� ������������� ����������

      var_act_msg := null;
      var_act_warn_msg := null;
      var_err_msg := null;
      var_commit_flag := null;
      var_act_review_ids := null;
      --
      -- ������������� ��� �������� � �������� ������ (�������� ������������, ��������, ��� ������ ������� ��������� ���)
      --
      dbms_session.set_context('CLIENTCONTEXT', 'AML_ACTVY_TYPE_CD', r.actvy_type_cd);
      --
      -- ���� � ������������ ��� ���� �� �������� - ��������� ��������� �� ������, ���������� ��������
      --
      if nvl(r.has_rights_flag, 0) <> 1 then
        var_err_msg := '��� ���� �� ���������� ��������: "'||r.actvy_type_nm||'". �������� �� ���������!';
        var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||var_err_msg, 1, 2000);

        var_msg_required_flag := 1;
        GOTO ACTIVITY_DONE;
      end if;  
      --------------------------------------------------------------------------------------------------------------
      -- ���� �����������: ��� ������ ��������/����������� (���) ����� ��������� ��������
      --------------------------------------------------------------------------------------------------------------
      --
      -- ��������� ����� �������, ��� �������� � ������������ � �� ������� �������� ����� ���� ��������� ��� �������� 
      --     
      select rv.review_id
        bulk collect into var_act_review_ids
        from table(par_review_ids) ids,
             mantas.kdd_review rv,
             mantas.kdd_actvy_type_review_status act_st
       where rv.review_id = ids.column_value and
             act_st.actvy_type_cd = r.actvy_type_cd and
             act_st.status_cd = rv.status_cd and
             rv.rf_alert_type_cd = 'O'; -- ������ ������ �� ������������� ��������
        
      -- ��������� ��� ������������: ����� ��� �������������� �������� ��� ��������� �������
      if par_review_ids is not null and par_review_ids.count > 0 and        
         var_act_review_ids is not null and var_act_review_ids.count < par_review_ids.count then

        var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                              '��������: "'||r.actvy_type_nm||'" �� ��������� (� �� ���������) ��� '||
                              case when par_review_ids.count = 1
                                   then '�������� �'||to_char(par_review_ids(1))
                                   else to_char(par_review_ids.count - var_act_review_ids.count)||' '||
                                        case par_review_ids.count - var_act_review_ids.count - trunc(par_review_ids.count - var_act_review_ids.count, -1)
                                          when 1 
                                          then '��������'
                                          else '��������'
                                        end
                              end, 1, 2000);
        var_msg_required_flag := 1;                                           
      end if; 
      --
      -- ���������� ����� �������, ��� ��������, � ������������ � �� ������� ��������,
      -- ������� ������������ ����� ����� ��������� ������ ��������
      --
      if var_act_review_ids is not null and var_act_review_ids.count > 0 Then
        select rv.review_id
          bulk collect into var_tmp_review_ids
          from table(var_act_review_ids) ids,
               mantas.kdd_review rv
         where rv.review_id = ids.column_value and
               mantas.rf_pkg_adm.allow_alert_editing(par_owner_id, rv.status_cd) = 1;

        -- ��������� ��� ������������: ����� ��� ���������� ���� ��� ��������� �������
        if var_tmp_review_ids is not null and var_tmp_review_ids.count < var_act_review_ids.count then

          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                '������������ ����, ����� ��������� ��������: "'||r.actvy_type_nm||'" ��� '||
                                case when par_review_ids.count = 1
                                     then '�������� �'||to_char(par_review_ids(1))
                                     else to_char(var_act_review_ids.count - var_tmp_review_ids.count)||' '||
                                          case var_act_review_ids.count - var_tmp_review_ids.count - trunc(var_act_review_ids.count - var_tmp_review_ids.count, -1)
                                            when 1 
                                            then '��������'
                                            else '��������'
                                          end
                                end, 1, 2000);
          var_msg_required_flag := 1;                                           
        end if; 

        var_act_review_ids := var_tmp_review_ids;
      end if;  
      /*
      --
      -- ���� ��� ������� � ������ � ���������� �� ������� ������� ������� - 
      -- ��������� ��� ������ �������� ������������ (� ��� ����������� - ��� ������) � ��������:
      --  "������� ��������" (RF_READY)
      --  "������� �������� (������ ������)" (RF_READY+)
      --  "������� �������� (������ ��������)" (RF_CREADY+)
      --  "������� �������� (��������)" (RF_READY=)
      --
      if r.rf_oes_actvy_type_cd = 'SEND' and (par_review_ids is null or par_review_ids.count = 0) Then
        select review_id         
          bulk collect into var_act_review_ids
          from mantas.kdd_review
         where status_cd in ('RF_READY', 'RF_READY+', 'RF_CREADY+', 'RF_READY=') and
               (owner_seq_id = var_owner_seq_id or rf_pkg_adm.is_supervisor(par_owner_id) = 1);
      end if;
      */
      --
      -- ����� ��������� (���) �� ��������� ����
      --
      var_act_oes_ids := rf_tab_number();
      --
      -- ���� �������� ������� �������� ��������� (���) �, ��������������, ����������� ������ ��� �������/���������, ��������� ��������,
      -- �� ��������� �������� ������� �� ���������, ��������� ������� ������� � ���������, ��������� ��������
      --
      if r.rf_oes_actvy_type_cd in ('CHECK', 'SEND') Then
        if var_act_review_ids is not null and var_act_review_ids.count > 0 Then
          mantas.rf_pkg_oes_check.check_oes_by_alerts(par_form_cd           => '321',
                                                      par_review_ids        => var_act_review_ids,
                                                      par_nonerr_oes_ids    => var_act_oes_ids,
                                                      par_nonerr_review_ids => var_tmp_review_ids,
                                                      par_oes_warn_count    => var_oes_warn_count,
                                                      par_oes_err_count     => var_oes_err_count,
                                                      par_oes_absent_count  => var_oes_absent_count,
                                                      par_commit_flag       => 0);
        else                                              
          var_act_oes_ids    := rf_tab_number();
          var_tmp_review_ids := rf_tab_number();   
          var_oes_warn_count := 0;
          var_oes_err_count := 0;
          var_oes_absent_count := 0;
        end if;  
        
        var_act_review_ids := var_tmp_review_ids;
        
        -- ��������� ��� ������������: ����� ��� ������������� ��������
        if var_oes_err_count <> 0 Then
          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                '��������: "'||r.actvy_type_nm||'" �� ��������� ��� '||
                                case when par_review_ids.count = 1
                                     then '�������� �'||to_char(par_review_ids(1))||', �. �. ��������� (���) �� ������ ��������!'
                                     else to_char(var_oes_err_count)||' '||
                                          case var_oes_err_count - trunc(var_oes_err_count, -1)
                                            when 1 
                                            then '��������'
                                            else '��������'
                                          end||', �. �. ��������� (���) �� ������ ��������!'
                                end, 1, 2000);
          var_msg_required_flag := 1;                                           
        end if;
        
        -- ��������� ��� ������������: ����� ��� ���������� ��������� (���)
        if var_oes_absent_count <> 0 Then
          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                '��������: "'||r.actvy_type_nm||'" �� ��������� ��� '||
                                case when par_review_ids.count = 1
                                     then '�������� �'||to_char(par_review_ids(1))||', �. �. ��� �������� �� ������������ ��������� (���)!'
                                     else to_char(var_oes_absent_count)||' '||
                                          case var_oes_absent_count - trunc(var_oes_absent_count, -1)
                                            when 1 
                                            then '��������'
                                            else '��������'
                                          end||', �. �. ��� �������� �� ������������ ��������� (���)!'
                                end, 1, 2000);
          var_msg_required_flag := 1;                                           
        end if;
        
        -- ��������� ��� ������������: ����� ��� ��������� (���), ��������� �������� � ����������������
        if var_oes_warn_count <> 0 Then
          var_act_warn_msg := case when par_review_ids.count = 1
                                   then ', ��� ���� ��������� (���) ������ �������� � ����������������!'
                                   else ', ��� ���� ��������� (���), ��������� �������� � ����������������: '||to_char(var_oes_warn_count)
                              end;     
          var_msg_required_flag := 1;                                           
        end if;       
      end if;             
      --------------------------------------------------------------------------------------------------------------
      -- ���� ���������� ��������
      --------------------------------------------------------------------------------------------------------------
      --
      -- ���� �������� - ������� � ������, �� ��������� �������� 
      --
      if r.rf_oes_actvy_type_cd = 'SEND' then
        --
        -- ������������ ��������� (���)
        -- � ������ ������ ��� ��������� (���������� �������, ������ ������������) 
        -- ��������� ���������: rf_pkg_review.set_reviews_exported, ���������� ������������ ��������; ��� ������� ��������� � ��������� ���������
        --
        if var_act_oes_ids is not null and var_act_oes_ids.count > 0 then
        
          -- 1) ������������� �������� ��� ������� set_reviews_exported
          pck_actvy_type_cd    := r.actvy_type_cd;
          pck_new_owner_seq_id := case when r.reasn_fl = 'Y' then var_new_owner_seq_id end;
          pck_new_owner_org    := case when r.reasn_fl = 'Y' then var_new_owner_org end;
          pck_due_dt           := case when r.due_date_fl = 'Y' then var_new_due_dt end;
          pck_opok_nb          := case when r.opok_fl = 'Y' then var_new_opok_nb end;
          pck_add_opoks_tx     := case when r.opok_fl = 'Y' and var_new_opok_nb is not null then var_new_add_opoks_tx end;
          pck_cmmnt_id         := var_cmmnt_id;
          pck_note_tx          := trim(trim(';' from r.note_tx||'; '||trim(par_note_tx)));
          pck_owner_seq_id     := var_owner_seq_id;
          pck_owner_id         := par_owner_id;
          pck_review_ids       := rf_tab_number();
          pck_file_count       := 0;
      
          -- 2) ���������� ��������� �������
          begin
            mantas.rf_pkg_oes.export_xml_321p(var_act_oes_ids);
          exception  
            when others then
              if sqlcode < -20000 then
                var_err_msg := sqlerrm;
              else
                var_err_msg := '������ ��� �������� �������� � �����. ����� ������: '||dbms_utility.format_error_backtrace||' '||sqlerrm||prcname;
              end if;
              rollback;
          end;
        
          -- ��������� ��� ������������: ����� ��� ������� ����������� ��������        
          if pck_review_ids is not null and pck_review_ids.count > 0 then
            var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                  '��������: "'||r.actvy_type_nm||'" ������� ��������� ��� '||
                                  case when par_review_ids.count = 1
                                       then '�������� �'||to_char(par_review_ids(1))
                                       else to_char(pck_review_ids.count)||' '||
                                            case pck_review_ids.count - trunc(pck_review_ids.count, -1)
                                              when 1 
                                              then '��������'
                                              else '��������'
                                            end
                                  end||
                                  ', ������� ������: '||to_char(pck_file_count)||var_act_warn_msg, 1, 2000);
          end if;       
        
          -- ��������� ��� ������������: ��������� �� ������
          if var_err_msg is not null then          
            var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||var_err_msg, 1, 2000);
          end if;

          -- ��������� ��� ������������: ��������������, ��� �� ��� �������� ���� ���������
          if  pck_review_ids is null or pck_review_ids.count < var_act_oes_ids.count then
            var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                  '��������! �� ���� ��������� ��������: '||to_char(var_act_oes_ids.count - case when pck_review_ids is null then 0 else pck_review_ids.count end), 
                                  1, 2000);
          end if;          
          
          -- ��������� �������� ������������� ������������ �������
          var_act_review_ids := pck_review_ids;
              
          -- 3) ������� ��������
          pck_actvy_type_cd    := null;
          pck_new_owner_seq_id := null;
          pck_new_owner_org    := null;
          pck_due_dt           := null;
          pck_opok_nb          := null;
          pck_add_opoks_tx     := null;
          pck_cmmnt_id         := null;
          pck_note_tx          := null;
          pck_owner_seq_id     := null;    
          pck_owner_id         := null;
          pck_review_ids       := null;
          pck_file_count       := null;
          --
          -- ������������� �������, ��� �������� ��������� COMMIT, ������������� ���� �������������� ��������������� ���������
          --     
          var_commit_flag := 1;
          var_msg_required_flag := 1;
        elsif (par_review_ids is null or par_review_ids.count = 0) and var_oes_err_count = 0 and var_oes_absent_count = 0 then
          -- ��������� ��� ������������: �����, ��� ������ ���������
          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                '��� ��������, ������� ����� ���� ��������� � ����� (�������� � ��������: ������� ��������, ������� �������� (������ ������), ������� �������� (������ ��������), ������� �������� (��������))', 
                                1, 2000);
          var_msg_required_flag := 1;
        end if;         
      --  
      -- ���� �������� - �������� �������� (���������), �� ��������� ��������� �������� ��������, ��������� � ���������� ��������
      --
      elsif r.actvy_type_cd = 'RF_RUN' then
        --
        -- ���� �� ���������
        --
        var_tmp_review_ids := rf_tab_number();   
        var_trxn_sum := 0;
        var_delete_sum := 0;
        var_create_sum := 0;
        var_repeat_sum := 0;
        var_trxn_absent_count := 0;
            
        for r2 in (select distinct 
                          rv.rf_op_cat_cd, 
                          rv.rf_trxn_seq_id, 
                          case when rv.rf_object_tp_cd like 'TRXN%'
                               then 'TRXN'
                               when rv.rf_object_tp_cd like 'OES%'
                               then 'OES'
                          end as obj_type,
                          case when rv.rf_object_tp_cd like 'OES%'
                               then rv.review_id
                          end as review_id                  -- ��� ����������� �������� ���������� �������������� �������
                     from table(var_act_review_ids) ids,
                          mantas.kdd_review rv
                    where rv.review_id = ids.column_value) loop

          -- ��������� ������ ������, ��������� � ���������� (��� ������ ��� ��� ����������)
          if r2.rf_trxn_seq_id is not null and r2.obj_type = 'TRXN' Then
            begin
              -- ��������� ��������� ��������
              rf_pkg_scnro_run.run_opok_repeat(par_op_cat_cd     => r2.rf_op_cat_cd,
                                               par_trxn_seq_id   => r2.rf_trxn_seq_id,   
                                               par_owner_seq_id  => var_owner_seq_id,
                                               par_actvy_type_cd => r.actvy_type_cd,
                                               par_note_tx       => trim(trim(';' from r.note_tx||'; '||trim(par_note_tx))),
                                               par_cmmnt_id      => var_cmmnt_id,
                                               par_arh_mode      => 'E', -- ������ ������� ������������� ��� ����� ��������
                                               par_delete_count  => var_delete_count,
                                               par_create_count  => var_create_count,  
                                               par_repeat_count  => var_repeat_count);                 

              -- �������� � ����������� �������������� ������� �� ����������� ��������� (� ��� ����� ������, ��������� � ���������� ��������)
              select review_id
                bulk collect into var_tmp_review_ids2
                from (select column_value as review_id from table(var_tmp_review_ids)
                      union
                      select review_id from mantas.kdd_review where rf_op_cat_cd = r2.rf_op_cat_cd and rf_trxn_seq_id = r2.rf_trxn_seq_id);

              var_tmp_review_ids := var_tmp_review_ids2;
                    
              -- ����������� ��������        
              var_trxn_sum := var_trxn_sum + 1;
              var_delete_sum := var_delete_sum + var_delete_count;
              var_create_sum := var_create_sum + var_create_count;
              var_repeat_sum := var_repeat_sum + var_repeat_count;
            exception  
              when others then
                if sqlcode < -20000 then
                  var_err_msg := sqlerrm;
                else
                  var_err_msg := '������ ��� ��������� �������� (���������) ��������. ��������� ��������: '||r2.rf_op_cat_cd||'; ID ��������: '||to_char(r2.rf_trxn_seq_id)||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||sqlerrm||prcname;
                end if;
                rollback;
            end;
            --
            -- ��� ������ - ���������� ��������� �������� ��������
            --
            if var_err_msg is not null then
              exit;
            end if;
          else 
            var_trxn_absent_count := var_trxn_absent_count + 1;
          end if;    
        end loop;           
        
        -- ��������� ��� ������������: ����� �� �������� ���������
        if var_trxn_sum > 0 and var_tmp_review_ids.count > 0 then       
          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                '��������: "'||r.actvy_type_nm||'" ������� ��������� ��� '||
                                case when var_tmp_review_ids.count = 1
                                     then '�������� �'||to_char(var_tmp_review_ids(1))
                                     else to_char(var_tmp_review_ids.count)||' '||
                                          case var_tmp_review_ids.count - trunc(var_tmp_review_ids.count, -1)
                                            when 1 
                                            then '��������'
                                            else '��������'
                                          end
                                end, 1, 2000);

          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                '��������� (����������) ��������: '||to_char(var_trxn_sum)||'; '||
                                '������������ �������� ��������: '||to_char(var_delete_sum)||'; '||
                                '������������ ��������� ������������ ��������: '||to_char(var_repeat_sum)||'; '||
                                '������� ����� ��������: '||to_char(var_create_sum),
                                1, 2000);            
        end if;                        

        -- ��������� ��� ������������: ��������� �� ������
        if var_err_msg is not null then          
          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||var_err_msg, 1, 2000);
        end if;
        
        -- ��������� ��� ������������: ����� ��� ������������� �������� ��� ������ ���/������� ��� ��������
        if var_trxn_absent_count <> 0 Then
          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                '��������: "'||r.actvy_type_nm||'" �� ��������� ��� '||
                                case when par_review_ids.count = 1
                                     then '�������� �'||to_char(par_review_ids(1))||', �. �. ��� ��������� (���) ������� ������� (��� �������������), �������� �� ���������� ����� ��������!'
                                     else to_char(var_trxn_absent_count)||' '||
                                          case var_trxn_absent_count - trunc(var_trxn_absent_count, -1)
                                            when 1 
                                            then '��������'
                                            else '��������'
                                          end||', �. �. ��� ��������� (���) ������� ������� (��� �������������), �������� �� ���������� ����� ��������!'
                                end, 1, 2000);
        end if;                
        --
        -- ������������� �������, ��� �������� ��������� COMMIT, ������������� ���� �������������� ��������������� ���������,
        -- ��������� ������ ������������� ������������ �������
        --     
        var_commit_flag := 1;
        var_msg_required_flag := 1;
        var_act_review_ids := var_tmp_review_ids;
      --  
      -- ���� �������� - ����������� ���, �� ������� ����� ������ � ���
      --
      elsif r.actvy_type_cd = 'RF_OESCOPY' then
        --
        -- ���� �� �������-�������� (���� �������������� ����������� �� ������, �� ����� �� ������ ������ �����)
        --
        var_oes_count := 0;
        var_oes_warn_count := 0;
        var_oes_err_count := 0;
        var_oes_absent_count := 0;
        var_tmp_review_ids.delete;
        
        for r2 in (select distinct 
                          rv.review_id,
                          oes.oes_321_id                 
                     from table(var_act_review_ids) ids
                          join mantas.kdd_review rv on rv.review_id = ids.column_value
                          left join mantas.rf_oes_321 oes on oes.review_id = rv.review_id and
                                                             oes.current_fl = 'Y' and
                                                             nvl(oes.deleted_fl, 'N') <> 'Y') loop
          --
          -- �������� ���, ���� � ������ �� ����, ��������� ����� ���
          --
          if r2.oes_321_id is not null Then
            mantas.rf_pkg_oes.copy_oes_321(r2.oes_321_id, var_owner_seq_id, var_review_id, var_oes_321_id);

            if var_review_id is not null and var_oes_321_id is not null Then
              var_oes_count := var_oes_count + 1;
              var_oes_check_status := rf_pkg_oes_check.check_oes(par_form_cd => '321', par_oes_id => var_oes_321_id, par_commit_flag => 0); 
               
              if var_oes_check_status = 'W' Then
                var_oes_warn_count := var_oes_warn_count + 1;
              elsif var_oes_check_status = 'E' Then 
                var_oes_err_count := var_oes_err_count + 1;
              end if;
              
              var_tmp_review_ids.extend;
              var_tmp_review_ids(var_tmp_review_ids.count) := var_review_id;
            else
              raise_application_error(-20001, '����������� ������: ��������� rf_pkg_oes_check.check_oes �� ������� ������������� ����������� ��� ����������� ���');
            end if;    
          else
            var_oes_absent_count := var_oes_absent_count + 1;
          end if;
        end loop;
        
        -- ��������� ��� ������������: ����� �� �������� ����������� � ����������� �������� ���
        if var_act_review_ids.count > 0 Then
          -- ����������� ������: ������������ ��������� ���� ��� � ������� ��� ����������
          if par_review_ids.count = 1 and var_oes_count > 0 Then
            var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                  '��� �� �������� �'||to_char(par_review_ids(1))||' ������� �����������. '||
                                  '������� ����� ��� - ����� ��������: '||to_char(var_review_id)||
                                  ', ����� ���: '||to_char(var_oes_321_id), 1, 2000);
            
            if var_oes_warn_count <> 0 or var_oes_err_count <> 0 Then
              var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                    '. ����� ���'||
                                    case when var_oes_err_count <> 0
                                         then ' �� ������ �������� � ������� ���������'
                                         when var_oes_warn_count <> 0
                                         then ' ������ �������� � ����������������'
                                    end, 1, 2000);
            end if;                        
          -- ����������� ������: ������������ ��������� ���� ��� �� ����������� �� �������
          elsif par_review_ids.count = 1 and var_oes_count = 0 Then
            var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                  '��������: "'||r.actvy_type_nm||'"�� ���������!'||
                                  case when var_oes_absent_count > 0 
                                       then '� �������� �'||to_char(par_review_ids(1))||' ��� ���.'
                                  end, 1, 2000);       
          else
            -- ����� ������ (���� �������������� ����������� �� ������, �� ����� �� ������ ������ �����):
            var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                  '��������: "'||r.actvy_type_nm||'" ��������� ��� '||
                                  to_char(var_act_review_ids.count)||' '||
                                  case var_act_review_ids.count - trunc(var_act_review_ids.count, -1)
                                    when 1 
                                    then '��������'
                                    else '��������'
                                  end||
                                  '. ������� ����� ��������� (���): '||to_char(var_oes_count)||'; '||
                                  '���, �� ��������� ��������: '||to_char(var_oes_err_count)||'; '||
                                  '���, ��������� �������� � ����������������: '||to_char(var_oes_warn_count)||'; '||
                                  '�� ������� ���, �. �. ��� ���-�������: '||to_char(var_oes_absent_count),
                                  1, 2000);
          end if;
        end if;                            
        --
        -- ������������� ���� �������������� ��������������� ���������,
        -- ��������� ������ ����� ��������� �������
        --     
        var_msg_required_flag := 1;
        var_act_review_ids := var_tmp_review_ids;
      else  
        --
        -- ������� �������� - �������� � ��������� ������� �/��� ������ ��������� ������ + ��������/��������� ��������� � ��������� (���)
        -- �������� ������� ����������, ����������� ��� ��������� ��������� ������ (��� ����, ����� �������������, ����� ���� ���������)
        --
        if r.reasn_fl = 'Y' and var_new_owner_seq_id is null then
          var_err_msg := '�� ������ ����� �������������. ��������: "'||r.actvy_type_nm||'" �� ���������!';
        elsif r.due_date_fl = 'Y' and var_new_due_dt is null then
          var_err_msg := '�� ������ ����� ���� ���������. ��������: "'||r.actvy_type_nm||'" �� ���������!';
        elsif r.opok_fl = 'Y' and var_new_opok_nb is null then
          var_err_msg := '�� ������ ����� �������� ��� ���� ��������. ��������: "'||r.actvy_type_nm||'" �� ���������!';
        end if;  
        --
        -- ���� ����������� ��������� ���� - ����������� ����� �������� � ������ � ������������ � ������� ��������,
        -- ��������� �������������� ��������: ��������/�������� ��������� � ��.
        --
        if var_err_msg is null Then
          -- �������� �������� �������
          rf_pkg_review.update_reviews(p_review_ids       => var_act_review_ids,
                                       p_actvy_type_cd    => r.actvy_type_cd, 
                                       p_new_owner_seq_id => case when r.reasn_fl = 'Y' then var_new_owner_seq_id end, 
                                       p_new_owner_org    => case when r.reasn_fl = 'Y' then var_new_owner_org end,
                                       p_due_dt           => case when r.due_date_fl = 'Y' then var_new_due_dt end, 
                                       p_opok_nb          => case when r.opok_fl = 'Y' then var_new_opok_nb end, 
                                       p_add_opoks_tx     => case when r.opok_fl = 'Y' and var_new_opok_nb is not null then var_new_add_opoks_tx end, 
                                       p_owner_seq_id     => var_owner_seq_id);
          --
          -- ���� ��� �������� ��������� ���������/�������������� ����� ����, �� ��������� �� �� �������� ����� � ������� ���������
          --
          if r.opok_fl = 'Y' Then
            MERGE /*+ NO_PARALLEL*/ INTO mantas.rf_oes_321 t USING 
              (SELECT /*+ NO_PARALLEL*/ oes_321_id                    
                 FROM table(var_act_review_ids) ids
                      join mantas.rf_oes_321 oes on oes.review_id = ids.column_value and
                                                    oes.current_fl = 'Y'and
                                                    nvl(oes.send_fl, 'N') <> 'Y' and
                                                    nvl(oes.deleted_fl, 'N') <> 'Y') v
            ON (t.oes_321_id = v.oes_321_id)
            WHEN MATCHED THEN
              UPDATE 
                 SET t.vo = var_new_opok_nb,
                     t.dop_v = nvl(var_new_add_opoks_tx, '0'),
                     t.modified_by = par_owner_id,
                     t.modified_date = SYSDATE; 
          end if;            
          --
          -- ���� �������� ������������ �������� ��������� (���) - �������� ��������� � �������� ��
          --
          var_oes_count := 0;
          var_oes_warn_count := 0;
          var_oes_err_count := 0;

          if r.rf_oes_actvy_type_cd = 'CREATE' Then
            --
            -- ���� �� �������
            --
            FOR r2 in (SELECT /*+ NO_PARALLEL*/ ids.column_value as review_id, oes.oes_321_id, oes.action
                         FROM table(var_act_review_ids) ids
                              left join mantas.rf_oes_321 oes on oes.review_id = ids.column_value and
                                                                 oes.current_fl = 'Y' and
                                                                 nvl(oes.send_fl, 'N') <> 'Y' and
                                                                 nvl(oes.deleted_fl, 'N') <> 'Y') LOOP                                                                 
              --
              -- ���� ��������� ��� ��� - ������� ���, ����� - ������� ���� ACTION, ���� �����
              --
              if r2.oes_321_id is null Then
                
                var_oes_321_id := mantas.rf_pkg_oes.create_oes_321p(r2.review_id, r.rf_oes_action, par_owner_id);
                var_oes_count := var_oes_count + 1;
                              
              elsif nvl(r2.action, '0') <> r.rf_oes_action Then
              
                UPDATE mantas.rf_oes_321
                   SET action = r.rf_oes_action,                       
                       modified_by = par_owner_id,
                       modified_date = SYSDATE
                 WHERE oes_321_id = r2.oes_321_id;  
               
                var_oes_321_id := r2.oes_321_id; 
              else
                var_oes_321_id := r2.oes_321_id; 
              end if; 
              --
              -- �������� ��������� (���)
              --    
              var_oes_check_status := mantas.rf_pkg_oes_check.check_oes(par_form_cd => '321', par_oes_id => var_oes_321_id, par_commit_flag => 0); 
               
              if var_oes_check_status = 'W' Then
                var_oes_warn_count := var_oes_warn_count + 1;
              elsif var_oes_check_status = 'E' Then 
                var_oes_err_count := var_oes_err_count + 1;
              end if;    
            END LOOP;                                                    
          --  
          -- .. ���� �������� ������������ �������� ��������� (���) - ������� ��������� ���������� (����� ������)
          --  
          elsif r.rf_oes_actvy_type_cd = 'DELETE' Then
          
            MERGE /*+ NO_PARALLEL*/ INTO mantas.rf_oes_321 t USING 
              (SELECT /*+ NO_PARALLEL*/ oes_321_id                    
                 FROM table(var_act_review_ids) ids
                      join mantas.kdd_review rv on rv.review_id = ids.column_value
                      join mantas.rf_oes_321 oes on oes.review_id = ids.column_value and
                                                    oes.current_fl = 'Y' and
                                                    nvl(oes.send_fl, 'N') <> 'Y' and
                                                    nvl(oes.deleted_fl, 'N') <> 'Y'
                WHERE rv.rf_object_tp_cd not like 'OES%') v
            ON (t.oes_321_id = v.oes_321_id)
            WHEN MATCHED THEN
              UPDATE 
                 SET t.deleted_fl = 'Y',
                     t.current_fl = null,
                     t.modified_by = par_owner_id,
                     t.modified_date = SYSDATE; 
          end if;
          --
          -- ������������ ������ �������� ��� ������� � ���������� ����������� �� ����
          --        
          rf_pkg_review.create_activities(p_review_ids    => var_act_review_ids, 
                                          p_actvy_type_cd => r.actvy_type_cd,
                                          p_cmmnt_id      => var_cmmnt_id,
                                          p_note_tx       => trim(trim(';' from r.note_tx||'; '||trim(par_note_tx))),
                                          p_owner_seq_id  => var_owner_seq_id);

          -- ��������� ��� ������������: ����� �� �������� ���������� �������� �/��� ���������/����������� ���������� (���)
          if var_act_review_ids.count > 0 Then
            -- ������� ������: ������������ ������� ���������� �������� ���� ��������, �������������� ��������� ������� ���������
            if par_review_ids.count = 1 and (var_oes_warn_count <> 0 or var_oes_err_count <> 0) Then
              var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                    '��������� (���) ��� �������� �'||to_char(par_review_ids(1))||
                                    case when var_oes_err_count <> 0
                                         then ' �� ������ �������� � ������� ���������'
                                         when var_oes_warn_count <> 0
                                         then ' ������ �������� � ����������������'
                                    end, 1, 2000);
            else  
              -- ����� ������:
              var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                    '��������: "'||r.actvy_type_nm||'" ������� ��������� ��� '||
                                    case when par_review_ids.count = 1
                                         then '�������� �'||to_char(par_review_ids(1))
                                         else to_char(var_act_review_ids.count)||' '||
                                              case var_act_review_ids.count - trunc(var_act_review_ids.count, -1)
                                                when 1 
                                                then '��������'
                                                else '��������'
                                              end
                                    end||var_act_warn_msg, 1, 2000);
              
              if var_oes_count <> 0 or var_oes_warn_count <> 0 or var_oes_err_count <> 0 Then
                var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||
                                      '������� ����� ��������� (���): '||to_char(var_oes_count)||'; '||
                                      '���, �� ��������� ��������: '||to_char(var_oes_err_count)||'; '||
                                      '���, ��������� �������� � ����������������: '||to_char(var_oes_warn_count),
                                      1, 2000);            
              end if;       
            end if;
            --
            -- ���� ���� ������/�������������� ��� �������� ���, �� ��������� ������������ - �����������
            --       
            if var_oes_warn_count <> 0 or var_oes_err_count <> 0 Then
              var_msg_required_flag := 1;
            end if;  
          end if;       
        else
          -- ��������� ��� ������������: ��������� �� ������
          var_act_msg := substr(case when var_act_msg is not null then var_act_msg||chr(13)||chr(10) end||var_err_msg, 1, 2000);
          -- ������ ������ ������������ �������
          var_act_review_ids := rf_tab_number();   
        end if;  
      end if;    
      <<ACTIVITY_DONE>>   
      --
      -- ���������� ��� �������� � ��������� ������
      --
      dbms_session.set_context('CLIENTCONTEXT', 'AML_ACTVY_TYPE_CD', null);
      --------------------------------------------------------------------------------------------------------------
      -- ���� ���������� ����������� ���������� ��������
      --------------------------------------------------------------------------------------------------------------
      --
      -- ����������� �������� ��������� � �������� ������ ������������ �������
      --
      if var_act_msg is not null then
        var_result_msg := substr(case when var_result_msg is not null then var_result_msg||chr(13)||chr(10) end||var_act_msg, 1, 2000);
      end if;  
      
      if var_act_review_ids is not null and var_act_review_ids.count > 0 then
        select review_id
          bulk collect into var_tmp_review_ids
          from (select column_value as review_id from table(var_res_review_ids)
                union
                select column_value as review_id from table(var_act_review_ids));

        var_res_review_ids := var_tmp_review_ids;
      end if;  
      --
      -- ���� �������� ��������� COMMIT - ���������� ����������� ��������� ��� ������������ � ������ ������������ �������
      -- � ����������� ���������� - ��� �������� ��� �� ����� ���� ��������, � ��� ����� �������� ���� � ������ ������
      --
      if var_commit_flag = 1 then
        var_result_msg_commit := var_result_msg;
        var_res_review_ids_commit := var_res_review_ids;
      end if;  
      --
      -- ���� ���� ������ - ���������� ��������� �������� (������� �� �����)
      --
      if var_err_msg is not null then
        var_msg_required_flag := 1;
        exit;
      end if;  
    end loop;                                              
  exception
    when others then
      if sqlcode < -20000 then
        var_result_msg := substr(case when var_result_msg_commit is not null then var_result_msg_commit||chr(13)||chr(10) end||sqlerrm, 1, 2000);
      else
        var_result_msg := substr(case when var_result_msg_commit is not null then var_result_msg_commit||chr(13)||chr(10) end||'������ ��� ���������� �������� ��� ����������. ����� ������: '||dbms_utility.format_error_backtrace||' '||sqlerrm||prcname, 1, 2000);
      end if;
      
      rollback;
      dbms_session.set_context('CLIENTCONTEXT', 'AML_ACTVY_TYPE_CD', null);
      
      par_act_review_ids := var_res_review_ids_commit;
      return var_result_msg;
  end;    
  
  commit;  
  --
  -- �������
  --  
  par_act_review_ids := var_res_review_ids;
  --
  -- ��������� ������� ���������, ���� ��������� ������������ �������� ��� ����� �������, ��������� ��� ������� 
  -- � ��� ������ ������� �������� �� ���� ������������
  --  
  if var_msg_required_flag is null and 
     par_review_ids is not null and par_review_ids.count = 1 and 
     par_actvy_type_cds is not null and par_actvy_type_cds.count = 1 then 
     
    return null;
  else
    return var_result_msg;
  end if;  
END do_actions;
--
-- ��������� �������� (��������) ��� ������� (��������) - ������ ���������
--
procedure do_actions
(
  p_review_id           in varchar2, -- �������� ��������������� ������� ����� �������
  p_actvy_type_cd       in varchar2, -- �������� ����� ����� �������� ����� �������
  p_new_review_owner_id in varchar2, -- ����� ������������ - ������ �������������� ��� ��������� ������� (�������� ������?)
  p_due_dt              in varchar2, -- ����� ���� ��������� �������
  p_note_tx             in varchar2, -- ����������� � ��������� ���� ��� ������������ ��������
  p_owner_id            in varchar2, -- ����� �������� ������������, ������������ ������ ��������
  p_cmmnt_id            in varchar2, -- ������������� ������������ ����������� ��� ������������ ��������
  p_opok_nb             in varchar2, -- �������� ��� ���� ��� ��������� � ������
  p_add_opoks_tx        in varchar2  -- �������������� ���� ���� ��� ��������� � ������
) is
  
  var_owner_seq_id      kdd_review_owner.owner_seq_id%type;
  var_review_ids        mantas.rf_tab_number := rf_tab_number();   
  var_actvy_type_cds    mantas.rf_tab_varchar := rf_tab_varchar();   
  
  var_new_owner_seq_id  kdd_review_owner.owner_seq_id%type; 
  var_new_owner_org     kdd_review.owner_org%type;
  var_new_due_dt        kdd_review.due_dt%type;
  var_new_opok_nb       kdd_review.rf_opok_nb%type;    
  var_new_add_opoks_tx  kdd_review.rf_add_opoks_tx%type;  
  var_cmmnt_id          kdd_cmmnt.cmmnt_id%type;
  
  var_act_review_ids    mantas.rf_tab_number := rf_tab_number();   
  var_msg               varchar2(2000 char);

  var_count             integer;
  prcname               varchar2(255) := ' (mantas.rf_pkg_review.do_actions)';  
begin
  --
  -- ��������� ID �������� ������������
  --
  select max(owner_seq_id)
    into var_owner_seq_id
    from mantas.kdd_review_owner
   where owner_id = p_owner_id;    
   
  if var_owner_seq_id is null then
    raise_application_error(-20001, '������ �������� ������� ������������: '||p_owner_id||prcname);
  end if; 
  --
  -- ������� �� ������ �������������� �������������� �������
  --   
  begin
    if trim(p_review_id) is not null then
      select rv.review_id
        bulk collect into var_review_ids
        from table(rf_pkg_scnro.list_to_tab(p_review_id)) ids,
             mantas.kdd_review rv
       where rv.review_id = to_number(trim(ids.column_value));
    end if;   
  exception
    when others then
      raise_application_error(-20001, '������ ��� ������� ������ � ���������������� �������: '||p_review_id||'; ����� ������: '||dbms_utility.format_error_backtrace||' '||sqlerrm||prcname);
  end;
  --
  -- ������� �� ������ ���� ����������� ��������
  --
  begin
    if trim(p_actvy_type_cd) is not null then
      select act.actvy_type_cd
        bulk collect into var_actvy_type_cds
        from table(rf_pkg_scnro.list_to_tab(p_actvy_type_cd)) cds,
             mantas.kdd_activity_type_cd act
       where act.actvy_type_cd = trim(cds.column_value);
    end if;   
  exception
    when others then
      raise_application_error(-20001, '������ ��� ������� ������ � ������ ����������� ��������: '||p_actvy_type_cd||'; ����� ������: '||dbms_utility.format_error_backtrace||' '||sqlerrm||prcname);
  end;
  --
  -- ������� � �������� ������������� � ������: ���� ���������, ��������������, ���� ����
  --
  -- ���� ���������
  begin
    if trim(p_due_dt) is not null then
      var_new_due_dt := to_date(trim(p_due_dt), 'mm/dd/yyyy');
    end if;  
  exception
    when others then
      raise_application_error(-20001, '������ ��� ����������� ����: '||p_due_dt||', ��������� ���� � ������� mm/dd/yyyy; ����� ������: '||dbms_utility.format_error_backtrace||' '||sqlerrm||prcname);
  end;
  
  -- �������������
  if trim(p_new_review_owner_id) is not null then
    select max(owner_seq_id), max(rptg_group_cd)
      into var_new_owner_seq_id, var_new_owner_org
      from mantas.kdd_review_owner
     where owner_id = trim(p_new_review_owner_id);    
   
    if var_new_owner_seq_id is null then
      raise_application_error(-20001, '������ �������� ����� �������������: '||p_new_review_owner_id||prcname);
    end if; 
  end if;  

  -- �������� ��� ����
  begin
    if trim(p_opok_nb) is not null then
      select opok_nb
        into var_new_opok_nb
        from mantas.rf_opok
       where opok_nb = trim(p_opok_nb);
    end if;  
  exception
    when others then
      raise_application_error(-20001, '������ �������� ��� �������������� �������� ��� ���� ��������: '||p_opok_nb||prcname);
  end;

  -- �������������� ���� ����
  begin
    if trim(p_add_opoks_tx) is not null then
      select listagg(to_char(ref_opk.opok_nb), ',') within group(order by opk.rn),
             sum(decode(ref_opk.opok_nb, null, 1, 0)) -- ���������� �������������� ����� ����
        into var_new_add_opoks_tx, var_count
        from (select rownum as rn, 
                     to_number(column_value) as opok_nb
                from table(mantas.rf_pkg_scnro.list_to_tab(p_add_opoks_tx))) opk,
             mantas.rf_opok ref_opk
       where ref_opk.opok_nb(+) = opk.opok_nb;
    end if;  
  exception
    when others then
      raise_application_error(-20001, '������� �������� ��� �������������� �������������� ���� ���� ��������: '||p_add_opoks_tx||prcname);
  end;

  if var_count > 0 then
    raise_application_error(-20001, '������� �������� ��� �������������� �������������� ���� ���� ��������: '||p_add_opoks_tx||prcname);
  end if;  
  
  -- id ������������ �����������
  begin
    if trim(p_cmmnt_id) is not null then
      select cmmnt_id
        into var_cmmnt_id
        from mantas.kdd_cmmnt
       where cmmnt_id = to_number(trim(p_cmmnt_id));
    end if;  
  exception
    when others then
      raise_application_error(-20001, '������ �������� ��� �������������� ������������� ������������ �����������: '||p_cmmnt_id||prcname);
  end;  
  --
  -- ������ ������� API ������ ����������
  --
  var_msg := do_actions(par_review_ids          => var_review_ids,
                        par_actvy_type_cds      => var_actvy_type_cds,
                        par_new_review_owner_id => var_new_owner_seq_id,
                        par_due_dt              => var_new_due_dt,
                        par_note_tx             => p_note_tx,
                        par_owner_id            => p_owner_id,
                        par_cmmnt_id            => var_cmmnt_id,
                        par_opok_nb             => var_new_opok_nb,
                        par_add_opoks_tx        => var_new_add_opoks_tx,
                        par_act_review_ids      => var_act_review_ids);         
end do_actions;
--
-- �������� �������� ���������������� �������, �������� ��������������� �����������
-- ��������! ��������� ���������� �������� ���������� ���������, ������� ������ ���� ����������� �� ������ ����������� ��������
--
procedure set_reviews_exported
(
  p_review_ids          mantas.rf_tab_number,   -- ��������� ��������������� ������� 
  p_filename            varchar2                -- ��� �����, � ������� ���� ��������� ������
) is

  var_tmp_review_ids    mantas.rf_tab_number := rf_tab_number();
begin
  --
  -- ���� �� ������� ������� ��� �� ���������� �������� - ������ �� ������
  --
  if p_review_ids is null or p_review_ids.count = 0 or pck_actvy_type_cd is null then
    return;
  end if;  
  --
  -- �������� �������� ������� � ������������ � ����������� ��������� (� �. �. ��������� ������ � ����� ������)
  --
  rf_pkg_review.update_reviews(p_review_ids       => p_review_ids,
                               p_actvy_type_cd    => pck_actvy_type_cd, 
                               p_new_owner_seq_id => pck_new_owner_seq_id, 
                               p_new_owner_org    => pck_new_owner_org, 
                               p_due_dt           => pck_due_dt, 
                               p_opok_nb          => pck_opok_nb, 
                               p_add_opoks_tx     => pck_add_opoks_tx, 
                               p_owner_seq_id     => pck_owner_seq_id);
  --
  -- �������� ������� ��������� (���) ������� �������������
  --
  MERGE /*+ NO_PARALLEL*/ INTO mantas.rf_oes_321 t USING 
    (SELECT /*+ NO_PARALLEL*/ oes_321_id                    
       FROM table(p_review_ids) ids
            join mantas.rf_oes_321 oes on oes.review_id = ids.column_value and
                                          oes.current_fl = 'Y'and
                                          nvl(oes.send_fl, 'N') <> 'Y' and
                                          nvl(oes.deleted_fl, 'N') <> 'Y') v
  ON (t.oes_321_id = v.oes_321_id)
  WHEN MATCHED THEN
    UPDATE 
       SET t.send_fl = 'Y',
           t.modified_by = pck_owner_id,
           t.modified_date = SYSDATE,
           t.date_p = trunc(sysdate);
  --
  -- ������������ ������ �������� ��� ������� � ���������� ����������� �� ����
  --        
  rf_pkg_review.create_activities(p_review_ids    => p_review_ids, 
                                  p_actvy_type_cd => pck_actvy_type_cd,
                                  p_cmmnt_id      => pck_cmmnt_id,
                                  p_note_tx       => trim(';' from trim('��������� � ����: '||p_filename||'; '||pck_note_tx)),
                                  p_owner_seq_id  => pck_owner_seq_id); 
  --
  -- ��������� ������ ��������������� ����������� �������, ����������� ������� ������
  --
  select review_id
    bulk collect into var_tmp_review_ids
    from (select column_value as review_id from table(pck_review_ids)
          union
          select column_value as review_id from table(p_review_ids));
  
  pck_review_ids := var_tmp_review_ids;

  pck_file_count := pck_file_count + 1;
  --
  -- ���������, ��� ��� ������ ���������
  --
  commit; 
end set_reviews_exported;
--
-- ���������� ����� ������ ������ � ������������ � ����������� ��� ��� ��������� � ��� ������� ��������
-- (���� ������ �� ������ ����������, �� ������������ NULL)
--
function get_new_status
(
  par_actvy_type_cd     mantas.kdd_activity_type_cd.actvy_type_cd%type, -- ��� ��������
  par_status_cd         kdd_review.status_cd%type                       -- ������� ������ ������
)
return kdd_review.status_cd%type is -- ����� ������ ������  

  var_result    mantas.kdd_review_status.status_cd%type;
begin
  for r in (select new_status_cd, 1 as priority
              from mantas.rf_kdd_actvy_type_new_status
             where actvy_type_cd = par_actvy_type_cd and
                   status_cd = par_status_cd
            union all
            select next_review_status_cd, 2 as priority
              from mantas.kdd_activity_type_cd
             where actvy_type_cd = par_actvy_type_cd and
                   next_review_status_cd is not null
            order by priority) loop
    var_result := r.new_status_cd; 
    exit;           
  end loop;
  
  return var_result;
end;    
--
-- �������� ��������� ����������� ��� ������ (� ��������� ��� � ����� ��� ������)
--
function get_last_note
(
  par_review_id         kdd_review.review_id%type                       -- ID ������
)
return clob is -- ����� ���������� �� ���� ����������� + (��� � ����� ��� ������)  

  var_result      clob;
begin
  for r in (select to_clob('')||c.cmmnt_tx as note_tx, a.start_dt as creat_ts, rvo.owner_id, a.actvy_id
              from kdd_activity a
                   join kdd_activity_cmmnt ac on ac.actvy_id = a.actvy_id
                   join kdd_cmmnt c on c.cmmnt_id = ac.cmmnt_id
                   left join kdd_review_owner rvo on rvo.owner_seq_id = a.creat_id
             where a.review_id = par_review_id
            union all
            select nh.note_tx, nh.creat_ts, rvo.owner_id, a.actvy_id
              from kdd_activity a
                   join kdd_activity_note an on an.actvy_id = a.actvy_id
                   join kdd_note_hist nh on nh.note_id = an.note_id
                   left join kdd_review_owner rvo on rvo.owner_seq_id = nh.creat_id
             where a.review_id = par_review_id
            union all
            select to_clob('�������������� ������� � ��������'), nh.creat_ts, rvo.owner_id, rn.actvy_id
              from kdd_review_narrative rn
                   join kdd_note_hist nh on nh.note_id = rn.note_id
                   left join kdd_review_owner rvo on rvo.owner_seq_id = nh.creat_id
             where rn.review_id = par_review_id
            order by creat_ts desc nulls last, actvy_id desc nulls last) loop

    var_result := r.note_tx||case when r.creat_ts is not null or r.owner_id is not null
                                  then ' ('||trim(to_char(r.creat_ts, 'dd.mm.yyyy hh24:mi:ss')||' '||r.owner_id)||')'
                             end;
    exit;                              
  end loop;
  
  return var_result;
end get_last_note;
--
-- ������������ �������� ������� � ������ �����, ��������� � ��������� ���������
-- ������� ����� ������������� � ��������, ����� � ����� ��������� ������� ��������� ������� (��-�� ���������� ������ �����)
--
function get_review_scrty_desc
(
  par_op_cat_cd             varchar2,                                     -- ��������� ��������: WT - �����������, CT - ��������
  par_trxn_seq_id           number                                        -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN)
)
return varchar2 is -- �������� ������������ ������� � ������ �����, ��������� � ��������� ���������
  var_result    varchar2(2000 char);
begin
  -- ��� �������� - ��� ��������
  if par_op_cat_cd is null or par_trxn_seq_id is null Then
    return null;
  end if;
    
  select case when sum(decode(scrty.trxn_scrty_seq_id, null, 0, 1)) = 0 and -- ���� � �������� ��� ������ ����� � 
                   (sum(decode(rv.rf_kgrko_party_cd, null, 0, 1)) = 0 or    -- ���������� "�������������" ������� - ���������� null
                    count(*) = 1)
              then to_char(null)
              else listagg(case when rv.review_id is not null 
                                then '�'||rv.review_id||', ��� ��. '||to_char(rv.rf_opok_nb)||nvl2(rv.rf_add_opoks_tx, '/'||rv.rf_add_opoks_tx, null)||', '||st.code_disp_tx||', '
                                else '�� �������� ��������: '
                           end||
                           case when scrty.trxn_scrty_seq_id is not null
                                then scrty_tp.code_desc_tx||' '||nvl(scrty.scrty_id, scrty.scrty_nb)||', �����: '||to_char(scrty.scrty_base_am)
                                else '�����: '||to_char(trxn.trxn_base_am)
                           end||
                           case rv.rf_kgrko_party_cd
                             when 1 then ' (������ �����������)'
                             when 2 then ' (������ ����������)'
                           end, ';'||chr(13)||chr(10)) within group (order by rv.rf_opok_nb, rv.review_id)
         end
    into var_result             
    from (select * 
            from kdd_review 
           where rf_op_cat_cd = par_op_cat_cd and
                 rf_trxn_seq_id = par_trxn_seq_id and
                 rf_alert_type_cd = 'O') rv
         full join (select scrty_type_cd, scrty_id, scrty_nb, scrty_base_am, 
                           'WT' as op_cat_cd, fo_trxn_seq_id as trxn_seq_id, trxn_scrty_seq_id
                      from business.rf_wire_trxn_scrty
                     where par_op_cat_cd = 'WT' and
                           fo_trxn_seq_id = par_trxn_seq_id and
                           nvl(canceled_fl, 'N') <> 'Y'
                    union all
                    select scrty_type_cd, scrty_id, scrty_nb, scrty_base_am,
                           'CT' as op_cat_cd, fo_trxn_seq_id as trxn_seq_id, trxn_scrty_seq_id
                      from business.rf_cash_trxn_scrty
                     where par_op_cat_cd = 'CT' and
                           fo_trxn_seq_id = par_trxn_seq_id and
                           nvl(canceled_fl, 'N') <> 'Y') scrty 
                on scrty.op_cat_cd = rv.rf_op_cat_cd and 
                   scrty.trxn_seq_id = rv.rf_trxn_seq_id and
                   scrty.trxn_scrty_seq_id = rv.rf_trxn_scrty_seq_id
         left join (select trxn_base_am, 'WT' as op_cat_cd, fo_trxn_seq_id as trxn_seq_id
                      from business.wire_trxn
                     where par_op_cat_cd = 'WT' and
                           fo_trxn_seq_id = par_trxn_seq_id
                    union all
                    select trxn_base_am, 'CT' as op_cat_cd, fo_trxn_seq_id as trxn_seq_id
                      from business.cash_trxn
                     where par_op_cat_cd = 'CT' and
                           fo_trxn_seq_id = par_trxn_seq_id) trxn
                on trxn.op_cat_cd = rv.rf_op_cat_cd and 
                   trxn.trxn_seq_id = rv.rf_trxn_seq_id
         left join kdd_code_set_trnln st on st.code_set = 'AlertStatus' and st.code_val = rv.status_cd
         left join business.ref_table_detail scrty_tp on scrty_tp.code_set_id = 'RF_SCRTY_PROD_TYPE' and scrty_tp.code_val1_nm = scrty.scrty_type_cd
   where rownum <= 20; /*������ �� ������������ listagg 2000 CHAR*/

  return var_result;
exception
  when others then
    return null;
end get_review_scrty_desc;
--
-- ������������ �����, ����������� ��������� ��������� � �������� ��� ���������� ������ (� ��� ����� ���� ������������� �������� � ���������)
--
function get_review_trxn_change
(
  par_review_id         kdd_review.review_id%type                        -- ID ������
) 
return varchar2 is -- �����, ������������ �� ���������� ������������ - �������� ������������ ��������� ��� ����� ��� ������������� ��������
  var_result            varchar2(2000 char);
begin
  if par_review_id is null then
    return null;
  end if;
  --
  -- ���� �������� ������� - ���������� ��������� �� ����, ����� - ���������� �������� ������������ ��������� (���� ���� ��������� ���������)
  --
  select case when max(coalesce(wt.rf_canceled_fl, ct.rf_canceled_fl, bot.rf_canceled_fl)) = 'Y'
              then '�������� �������/������������ � ���������'
              when max(coalesce(wts.canceled_fl, cts.canceled_fl)) = 'Y'
              then '������ ������ � �������� �������/������������ � ���������'
              when max(rv.rf_repeat_nb) = 0 -- �� ���� ��������� ���������, ������ ����������
              then to_char(null)    
              else max(rf_pkg_review.get_trxn_change(rv.review_id, rv.rf_repeat_nb, rv.rf_repeat_nb - 1))
         end       
    into var_result
    from kdd_review rv
         left join business.wire_trxn wt on wt.fo_trxn_seq_id = rv.rf_trxn_seq_id and
                                            rv.rf_op_cat_cd = 'WT'
         left join business.cash_trxn ct on ct.fo_trxn_seq_id = rv.rf_trxn_seq_id and
                                            rv.rf_op_cat_cd = 'CT'
         left join business.back_office_trxn bot on bot.bo_trxn_seq_id = rv.rf_trxn_seq_id and
                                                    rv.rf_op_cat_cd = 'BOT'
         left join business.rf_wire_trxn_scrty wts on wts.trxn_scrty_seq_id = rv.rf_trxn_scrty_seq_id and
                                                      rv.rf_op_cat_cd = 'WT'
         left join business.rf_cash_trxn_scrty cts on cts.trxn_scrty_seq_id = rv.rf_trxn_scrty_seq_id and
                                                      rv.rf_op_cat_cd = 'CT'
    where rv.review_id = par_review_id;

  return var_result;
end get_review_trxn_change; 
--
-- �������� ��� ������ ��������/������ ������ (���������� �������� ���������� ��������� � ��������������� ������) � ������������ �������� ������������ ���������
--
function get_trxn_change
(
  par_review_id         kdd_review.review_id%type,                       -- ID ������
  par_new_repeat_nb     rf_kdd_review_run.repeat_nb%type,                -- ���������� ����� "������" ��������� ������, ���� null - ���������, ������������ ��� ������
  par_old_repeat_nb     rf_kdd_review_run.repeat_nb%type                 -- ���������� ����� "�����������" ��������� ������, ���� null - �� 1 ������ "������"
) 
return varchar2 is -- �������� ������������ ������� ����: ���������� �������, �����, ������������ �����������; null, ���� ��������� ���
  var_result            varchar2(2000 char);
begin
  if par_review_id is null or par_new_repeat_nb = par_old_repeat_nb then
    return null;
  end if;
  --
  -- ��������� ���� ��������������� ������ � ������� ��
  --  
  select max(rf_pkg_review.get_trxn_change(rv.rf_op_cat_cd, rv.rf_trxn_seq_id, rv.rf_trxn_scrty_seq_id,  newr.data_dump_dt,  oldr.data_dump_dt))
    into var_result
    from kdd_review rv
         join rf_kdd_review_run newr on newr.review_id = rv.review_id and
                                        newr.repeat_nb = nvl(par_new_repeat_nb, rv.rf_repeat_nb)
         join rf_kdd_review_run oldr on oldr.review_id = rv.review_id and
                                        oldr.repeat_nb = nvl(par_old_repeat_nb, newr.repeat_nb - 1)
   where rv.review_id = par_review_id;

  return var_result;
end get_trxn_change;
--
-- �������� ��� ������ ��������/������ ������ (���������� ������ ���������) � ������������ �������� ������������ ���������
--
function get_trxn_change
(
  par_op_cat_cd             varchar2,                                     -- ��������� ��������: WT - �����������, CT - ��������, BOT - ����������
  par_trxn_seq_id           number,                                       -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN/BACK_OFFICE_TRXN)
  par_trxn_scrty_seq_id     number,                                       -- ID ������ ������ � �������� (PK ������ RF_WIRE_TRXN_SCRTY/RF_CASH_TRXN_SCRTY)
  par_new_arh_dump_dt       date,                                         -- ���� "�����" ������ ��������, ���� null - ������� ������
  par_old_arh_dump_dt       date                                          -- ���� "�������" ������ ��������, ���� null - ������� ������
) 
return varchar2 is -- �������� ������������ ������� ����: ���������� �������, �����, ������������ �����������; null, ���� ��������� ���
  var_result            varchar2(2000 char);
begin
  --
  -- �������� ����������
  --
  if par_op_cat_cd not in ('WT', 'CT', 'BOT') or par_op_cat_cd is null or par_trxn_seq_id is null or 
     par_new_arh_dump_dt = par_old_arh_dump_dt or (par_new_arh_dump_dt is null and par_old_arh_dump_dt is null) then
    return null;
  end if;
  --
  -- ������� ��������� ������ �������� 
  --
  with trxn as
       (select to_date('01.01.9000', 'dd.mm.yyyy') as arh_dump_dt, 
               fo_trxn_seq_id as trxn_seq_id,
               trxn_exctn_dt, rf_branch_id, rf_trxn_crncy_am, trxn_base_am, rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, 
               orig_to_benef_instr_tx as trxn_desc, trxn_type1_cd,
               to_char(null) as trxn_conv_crncy_cd,
               to_number(null) as trxn_conv_am,
               to_number(null) as trxn_conv_base_am,
               rf_orig_cust_seq_id, orig_nm, rf_orig_first_nm, rf_orig_midl_nm, rf_orig_inn_nb, rf_orig_kpp_nb,
               rf_orig_acct_nb, rf_orig_acct_seq_id, rf_debit_cd,
               rf_orig_reg_addr_tx, rf_orig_fact_addr_tx,
               rf_orig_doc_type_cd, rf_orig_doc_series_id, rf_orig_doc_no_id, rf_orig_doc_issued_by_tx, rf_orig_doc_dpt_cd, rf_orig_doc_issue_dt,
               rf_orig_resident_fl, rf_orig_gndr_cd, rf_orig_ogrn_nb, rf_orig_birth_dt, rf_orig_birthplace_tx, rf_orig_reg_org_nm, rf_orig_okpo_nb, rf_orig_reg_dt,
               rf_orig_goz_op_cd,
               send_instn_nm, send_instn_id, rf_send_instn_cntry_cd, send_instn_acct_id,
               rf_benef_cust_seq_id, benef_nm, rf_benef_first_nm, rf_benef_midl_nm, rf_benef_inn_nb, rf_benef_kpp_nb,
               rf_benef_acct_nb, rf_benef_acct_seq_id, rf_credit_cd,
               rf_benef_reg_addr_tx, rf_benef_fact_addr_tx,
               rf_benef_doc_type_cd, rf_benef_doc_series_id, rf_benef_doc_no_id, rf_benef_doc_issued_by_tx, rf_benef_doc_dpt_cd, rf_benef_doc_issue_dt,
               rf_benef_resident_fl, rf_benef_gndr_cd, rf_benef_ogrn_nb, rf_benef_birth_dt, rf_benef_birthplace_tx, rf_benef_reg_org_nm, rf_benef_okpo_nb, rf_benef_reg_dt,
               rf_benef_goz_op_cd,
               rcv_instn_nm, rcv_instn_id, rf_rcv_instn_cntry_cd, rcv_instn_acct_id,
               rf_scnd_orig_cust_seq_id, scnd_orig_nm, rf_scnd_orig_first_nm, rf_scnd_orig_midl_nm,  
               rf_scnd_orig_reg_addr_tx, rf_scnd_orig_fact_addr_tx,
               rf_scnd_orig_doc_type_cd, rf_scnd_orig_doc_series_id, rf_scnd_orig_doc_no_id, rf_scnd_orig_doc_issued_by_tx, rf_scnd_orig_doc_dpt_cd, rf_scnd_orig_doc_issue_dt,               
               rf_scnd_orig_resident_fl, rf_scnd_orig_gndr_cd, rf_scnd_orig_birth_dt, rf_scnd_orig_birthplace_tx,
               rf_scnd_benef_cust_seq_id, scnd_benef_nm, rf_scnd_benef_first_nm, rf_scnd_benef_midl_nm,  
               rf_scnd_benef_reg_addr_tx, rf_scnd_benef_fact_addr_tx,
               rf_scnd_benef_doc_type_cd, rf_scnd_benef_doc_series_id, rf_scnd_benef_doc_no_id, rf_scnd_benef_doc_issued_by_tx, rf_scnd_benef_doc_dpt_cd, rf_scnd_benef_doc_issue_dt,               
               rf_scnd_benef_resident_fl, rf_scnd_benef_gndr_cd, rf_scnd_benef_birth_dt, rf_scnd_benef_birthplace_tx, 
               rf_trxn_doc_id, rf_trxn_doc_dt, bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx,
               rf_src_change_dt, rf_canceled_fl 
          from business.wire_trxn
         where par_op_cat_cd = 'WT' and
               fo_trxn_seq_id = par_trxn_seq_id
        union all
        select arh_dump_dt,
               fo_trxn_seq_id as trxn_seq_id,
               trxn_exctn_dt, rf_branch_id, rf_trxn_crncy_am, trxn_base_am, rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, 
               orig_to_benef_instr_tx as trxn_desc, trxn_type1_cd,
               to_char(null) as trxn_conv_crncy_cd,
               to_number(null) as trxn_conv_am,
               to_number(null) as trxn_conv_base_am,
               rf_orig_cust_seq_id, orig_nm, rf_orig_first_nm, rf_orig_midl_nm, rf_orig_inn_nb, rf_orig_kpp_nb,
               rf_orig_acct_nb, rf_orig_acct_seq_id, rf_debit_cd,
               rf_orig_reg_addr_tx, rf_orig_fact_addr_tx,
               rf_orig_doc_type_cd, rf_orig_doc_series_id, rf_orig_doc_no_id, rf_orig_doc_issued_by_tx, rf_orig_doc_dpt_cd, rf_orig_doc_issue_dt,
               rf_orig_resident_fl, rf_orig_gndr_cd, rf_orig_ogrn_nb, rf_orig_birth_dt, rf_orig_birthplace_tx, rf_orig_reg_org_nm, rf_orig_okpo_nb, rf_orig_reg_dt,
               rf_orig_goz_op_cd,
               send_instn_nm, send_instn_id, rf_send_instn_cntry_cd, send_instn_acct_id,
               rf_benef_cust_seq_id, benef_nm, rf_benef_first_nm, rf_benef_midl_nm, rf_benef_inn_nb, rf_benef_kpp_nb,
               rf_benef_acct_nb, rf_benef_acct_seq_id, rf_credit_cd,
               rf_benef_reg_addr_tx, rf_benef_fact_addr_tx,
               rf_benef_doc_type_cd, rf_benef_doc_series_id, rf_benef_doc_no_id, rf_benef_doc_issued_by_tx, rf_benef_doc_dpt_cd, rf_benef_doc_issue_dt,
               rf_benef_resident_fl, rf_benef_gndr_cd, rf_benef_ogrn_nb, rf_benef_birth_dt, rf_benef_birthplace_tx, rf_benef_reg_org_nm, rf_benef_okpo_nb, rf_benef_reg_dt,
               rf_benef_goz_op_cd,
               rcv_instn_nm, rcv_instn_id, rf_rcv_instn_cntry_cd, rcv_instn_acct_id,
               rf_scnd_orig_cust_seq_id, scnd_orig_nm, rf_scnd_orig_first_nm, rf_scnd_orig_midl_nm,  
               rf_scnd_orig_reg_addr_tx, rf_scnd_orig_fact_addr_tx,
               rf_scnd_orig_doc_type_cd, rf_scnd_orig_doc_series_id, rf_scnd_orig_doc_no_id, rf_scnd_orig_doc_issued_by_tx, rf_scnd_orig_doc_dpt_cd, rf_scnd_orig_doc_issue_dt,               
               rf_scnd_orig_resident_fl, rf_scnd_orig_gndr_cd, rf_scnd_orig_birth_dt, rf_scnd_orig_birthplace_tx,
               rf_scnd_benef_cust_seq_id, scnd_benef_nm, rf_scnd_benef_first_nm, rf_scnd_benef_midl_nm,  
               rf_scnd_benef_reg_addr_tx, rf_scnd_benef_fact_addr_tx,
               rf_scnd_benef_doc_type_cd, rf_scnd_benef_doc_series_id, rf_scnd_benef_doc_no_id, rf_scnd_benef_doc_issued_by_tx, rf_scnd_benef_doc_dpt_cd, rf_scnd_benef_doc_issue_dt,               
               rf_scnd_benef_resident_fl, rf_scnd_benef_gndr_cd, rf_scnd_benef_birth_dt, rf_scnd_benef_birthplace_tx, 
               rf_trxn_doc_id, rf_trxn_doc_dt, bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx,
               rf_src_change_dt, rf_canceled_fl 
          from business.wire_trxn_arh
         where par_op_cat_cd = 'WT' and
               fo_trxn_seq_id = par_trxn_seq_id and
               arh_dump_dt in (par_new_arh_dump_dt, par_old_arh_dump_dt)
        union all
        select to_date('01.01.9000', 'dd.mm.yyyy') as arh_dump_dt,
               fo_trxn_seq_id as trxn_seq_id,
               trxn_exctn_dt, rf_branch_id, rf_trxn_acct_am as rf_trxn_crncy_am, trxn_base_am, rf_acct_crncy_cd as rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, 
               desc_tx as trxn_desc, trxn_type1_cd,
               rf_conv_crncy_cd as trxn_conv_crncy_cd,
               rf_trxn_conv_am as trxn_conv_am,
               rf_trxn_conv_base_am as trxn_conv_base_am,
               case when dbt_cdt_cd = 'D' then rf_cust_seq_id end as rf_orig_cust_seq_id, 
               case when dbt_cdt_cd = 'D' then rf_cust_nm end as orig_nm,                 
               case when dbt_cdt_cd = 'D' then rf_cust_first_nm end as rf_orig_first_nm,                 
               case when dbt_cdt_cd = 'D' then rf_cust_midl_nm end as rf_orig_midl_nm,                     
               case when dbt_cdt_cd = 'D' then rf_cust_inn_nb end as rf_orig_inn_nb, 
               case when dbt_cdt_cd = 'D' then rf_cust_kpp_nb end as rf_orig_kpp_nb,               
               case when dbt_cdt_cd = 'D' then rf_acct_nb end as rf_orig_acct_nb, 
               case when dbt_cdt_cd = 'D' then rf_acct_seq_id end as rf_orig_acct_seq_id, 
               rf_debit_cd,
               case when dbt_cdt_cd = 'D' then rf_cust_reg_addr_tx end as rf_orig_reg_addr_tx, 
               case when dbt_cdt_cd = 'D' then rf_cust_fact_addr_tx end as rf_orig_fact_addr_tx,
               case when dbt_cdt_cd = 'D' then rf_cust_doc_type_cd end as rf_orig_doc_type_cd, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_series_id end as rf_orig_doc_series_id, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_no_id end as rf_orig_doc_no_id, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_issued_by_tx end as rf_orig_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_dpt_cd end as rf_orig_doc_dpt_cd, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_issue_dt end as rf_orig_doc_issue_dt,
               case when dbt_cdt_cd = 'D' then rf_cust_resident_fl end as rf_orig_resident_fl, 
               case when dbt_cdt_cd = 'D' then rf_cust_gndr_cd end as rf_orig_gndr_cd, 
               case when dbt_cdt_cd = 'D' then rf_cust_ogrn_nb end as rf_orig_ogrn_nb, 
               case when dbt_cdt_cd = 'D' then rf_cust_birth_dt end as rf_orig_birth_dt, 
               case when dbt_cdt_cd = 'D' then rf_cust_birthplace_tx end as rf_orig_birthplace_tx, 
               case when dbt_cdt_cd = 'D' then rf_cust_reg_org_nm end as rf_orig_reg_org_nm, 
               case when dbt_cdt_cd = 'D' then rf_cust_okpo_nb end as rf_orig_okpo_nb, 
               case when dbt_cdt_cd = 'D' then rf_cust_reg_dt end as rf_orig_reg_dt,
               case when dbt_cdt_cd = 'D' then rf_acct_goz_op_cd end as rf_orig_goz_op_cd,
               to_char(null) as send_instn_nm,
               to_char(null) as send_instn_id,
               to_char(null) as rf_send_instn_cntry_cd,
               to_char(null) as send_instn_acct_id,               
               case when dbt_cdt_cd = 'C' then rf_cust_seq_id end as rf_benef_cust_seq_id, 
               case when dbt_cdt_cd = 'C' then rf_cust_nm end as benef_nm,                 
               case when dbt_cdt_cd = 'C' then rf_cust_first_nm end as rf_benef_first_nm,                 
               case when dbt_cdt_cd = 'C' then rf_cust_midl_nm end as rf_benef_midl_nm,                     
               case when dbt_cdt_cd = 'C' then rf_cust_inn_nb end as rf_benef_inn_nb, 
               case when dbt_cdt_cd = 'C' then rf_cust_kpp_nb end as rf_benef_kpp_nb,               
               case when dbt_cdt_cd = 'C' then rf_acct_nb end as rf_benef_acct_nb, 
               case when dbt_cdt_cd = 'C' then rf_acct_seq_id end as rf_benef_acct_seq_id,                
               rf_credit_cd,
               case when dbt_cdt_cd = 'C' then rf_cust_reg_addr_tx end as rf_benef_reg_addr_tx, 
               case when dbt_cdt_cd = 'C' then rf_cust_fact_addr_tx end as rf_benef_fact_addr_tx,
               case when dbt_cdt_cd = 'C' then rf_cust_doc_type_cd end as rf_benef_doc_type_cd, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_series_id end as rf_benef_doc_series_id, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_no_id end as rf_benef_doc_no_id, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_issued_by_tx end as rf_benef_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_dpt_cd end as rf_benef_doc_dpt_cd, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_issue_dt end as rf_benef_doc_issue_dt,
               case when dbt_cdt_cd = 'C' then rf_cust_resident_fl end as rf_benef_resident_fl, 
               case when dbt_cdt_cd = 'C' then rf_cust_gndr_cd end as rf_benef_gndr_cd, 
               case when dbt_cdt_cd = 'C' then rf_cust_ogrn_nb end as rf_benef_ogrn_nb, 
               case when dbt_cdt_cd = 'C' then rf_cust_birth_dt end as rf_benef_birth_dt, 
               case when dbt_cdt_cd = 'C' then rf_cust_birthplace_tx end as rf_benef_birthplace_tx, 
               case when dbt_cdt_cd = 'C' then rf_cust_reg_org_nm end as rf_benef_reg_org_nm, 
               case when dbt_cdt_cd = 'C' then rf_cust_okpo_nb end as rf_benef_okpo_nb, 
               case when dbt_cdt_cd = 'C' then rf_cust_reg_dt end as rf_benef_reg_dt,
               case when dbt_cdt_cd = 'C' then rf_acct_goz_op_cd end as rf_benef_goz_op_cd,
               to_char(null) as rcv_instn_nm,
               to_char(null) as rcv_instn_id,
               to_char(null) as rf_rcv_instn_cntry_cd,
               to_char(null) as rcv_instn_acct_id,                              
               case when dbt_cdt_cd = 'D' then rf_cndtr_cust_seq_id end as rf_scnd_orig_cust_seq_id, 
               case when dbt_cdt_cd = 'D' then cndtr_nm end as scnd_orig_nm, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_first_nm end as rf_scnd_orig_first_nm, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_midl_nm end as rf_scnd_orig_midl_nm,  
               case when dbt_cdt_cd = 'D' then rf_cndtr_reg_addr_tx end as rf_scnd_orig_reg_addr_tx, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_fact_addr_tx end as rf_scnd_orig_fact_addr_tx,
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_type_cd end as rf_scnd_orig_doc_type_cd, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_series_id end as rf_scnd_orig_doc_series_id, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_no_id end as rf_scnd_orig_doc_no_id, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_issued_by_tx end as rf_scnd_orig_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_dpt_cd end as rf_scnd_orig_doc_dpt_cd, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_issue_dt end as rf_scnd_orig_doc_issue_dt,                              
               case when dbt_cdt_cd = 'D' then rf_cndtr_resident_fl end as rf_scnd_orig_resident_fl, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_gndr_cd end as rf_scnd_orig_gndr_cd, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_birth_dt end as rf_scnd_orig_birth_dt, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_birthplace_tx end as rf_scnd_orig_birthplace_tx,  
               case when dbt_cdt_cd = 'C' then rf_cndtr_cust_seq_id end as rf_scnd_benef_cust_seq_id, 
               case when dbt_cdt_cd = 'C' then cndtr_nm end as scnd_benef_nm, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_first_nm end as rf_scnd_benef_first_nm, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_midl_nm end as rf_scnd_benef_midl_nm,  
               case when dbt_cdt_cd = 'C' then rf_cndtr_reg_addr_tx end as rf_scnd_benef_reg_addr_tx, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_fact_addr_tx end as rf_scnd_benef_fact_addr_tx,
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_type_cd end as rf_scnd_benef_doc_type_cd, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_series_id end as rf_scnd_benef_doc_series_id, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_no_id end as rf_scnd_benef_doc_no_id, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_issued_by_tx end as rf_scnd_benef_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_dpt_cd end as rf_scnd_benef_doc_dpt_cd, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_issue_dt end as rf_scnd_benef_doc_issue_dt,               
               case when dbt_cdt_cd = 'C' then rf_cndtr_resident_fl end as rf_scnd_benef_resident_fl, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_gndr_cd end as rf_scnd_benef_gndr_cd, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_birth_dt end as rf_scnd_benef_birth_dt, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_birthplace_tx end as rf_scnd_benef_birthplace_tx,  
               rf_trxn_doc_id, rf_trxn_doc_dt, bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx,
               rf_src_change_dt, rf_canceled_fl 
          from business.cash_trxn
         where par_op_cat_cd = 'CT' and
               fo_trxn_seq_id = par_trxn_seq_id
        union all
        select arh_dump_dt,
               fo_trxn_seq_id as trxn_seq_id,
               trxn_exctn_dt, rf_branch_id, rf_trxn_acct_am as rf_trxn_crncy_am, trxn_base_am, rf_acct_crncy_cd as rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, 
               desc_tx as trxn_desc, trxn_type1_cd,
               rf_conv_crncy_cd as trxn_conv_crncy_cd,
               rf_trxn_conv_am as trxn_conv_am,
               rf_trxn_conv_base_am as trxn_conv_base_am,
               case when dbt_cdt_cd = 'D' then rf_cust_seq_id end as rf_orig_cust_seq_id, 
               case when dbt_cdt_cd = 'D' then rf_cust_nm end as orig_nm,                 
               case when dbt_cdt_cd = 'D' then rf_cust_first_nm end as rf_orig_first_nm,                 
               case when dbt_cdt_cd = 'D' then rf_cust_midl_nm end as rf_orig_midl_nm,                     
               case when dbt_cdt_cd = 'D' then rf_cust_inn_nb end as rf_orig_inn_nb, 
               case when dbt_cdt_cd = 'D' then rf_cust_kpp_nb end as rf_orig_kpp_nb,               
               case when dbt_cdt_cd = 'D' then rf_acct_nb end as rf_orig_acct_nb, 
               case when dbt_cdt_cd = 'D' then rf_acct_seq_id end as rf_orig_acct_seq_id, 
               rf_debit_cd,
               case when dbt_cdt_cd = 'D' then rf_cust_reg_addr_tx end as rf_orig_reg_addr_tx, 
               case when dbt_cdt_cd = 'D' then rf_cust_fact_addr_tx end as rf_orig_fact_addr_tx,
               case when dbt_cdt_cd = 'D' then rf_cust_doc_type_cd end as rf_orig_doc_type_cd, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_series_id end as rf_orig_doc_series_id, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_no_id end as rf_orig_doc_no_id, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_issued_by_tx end as rf_orig_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_dpt_cd end as rf_orig_doc_dpt_cd, 
               case when dbt_cdt_cd = 'D' then rf_cust_doc_issue_dt end as rf_orig_doc_issue_dt,
               case when dbt_cdt_cd = 'D' then rf_cust_resident_fl end as rf_orig_resident_fl, 
               case when dbt_cdt_cd = 'D' then rf_cust_gndr_cd end as rf_orig_gndr_cd, 
               case when dbt_cdt_cd = 'D' then rf_cust_ogrn_nb end as rf_orig_ogrn_nb, 
               case when dbt_cdt_cd = 'D' then rf_cust_birth_dt end as rf_orig_birth_dt, 
               case when dbt_cdt_cd = 'D' then rf_cust_birthplace_tx end as rf_orig_birthplace_tx, 
               case when dbt_cdt_cd = 'D' then rf_cust_reg_org_nm end as rf_orig_reg_org_nm, 
               case when dbt_cdt_cd = 'D' then rf_cust_okpo_nb end as rf_orig_okpo_nb, 
               case when dbt_cdt_cd = 'D' then rf_cust_reg_dt end as rf_orig_reg_dt,
               case when dbt_cdt_cd = 'D' then rf_acct_goz_op_cd end as rf_orig_goz_op_cd,
               to_char(null) as send_instn_nm,
               to_char(null) as send_instn_id,
               to_char(null) as rf_send_instn_cntry_cd,
               to_char(null) as send_instn_acct_id,               
               case when dbt_cdt_cd = 'C' then rf_cust_seq_id end as rf_benef_cust_seq_id, 
               case when dbt_cdt_cd = 'C' then rf_cust_nm end as benef_nm,                 
               case when dbt_cdt_cd = 'C' then rf_cust_first_nm end as rf_benef_first_nm,                 
               case when dbt_cdt_cd = 'C' then rf_cust_midl_nm end as rf_benef_midl_nm,                     
               case when dbt_cdt_cd = 'C' then rf_cust_inn_nb end as rf_benef_inn_nb, 
               case when dbt_cdt_cd = 'C' then rf_cust_kpp_nb end as rf_benef_kpp_nb,               
               case when dbt_cdt_cd = 'C' then rf_acct_nb end as rf_benef_acct_nb, 
               case when dbt_cdt_cd = 'C' then rf_acct_seq_id end as rf_benef_acct_seq_id,                
               rf_credit_cd,
               case when dbt_cdt_cd = 'C' then rf_cust_reg_addr_tx end as rf_benef_reg_addr_tx, 
               case when dbt_cdt_cd = 'C' then rf_cust_fact_addr_tx end as rf_benef_fact_addr_tx,
               case when dbt_cdt_cd = 'C' then rf_cust_doc_type_cd end as rf_benef_doc_type_cd, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_series_id end as rf_benef_doc_series_id, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_no_id end as rf_benef_doc_no_id, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_issued_by_tx end as rf_benef_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_dpt_cd end as rf_benef_doc_dpt_cd, 
               case when dbt_cdt_cd = 'C' then rf_cust_doc_issue_dt end as rf_benef_doc_issue_dt,
               case when dbt_cdt_cd = 'C' then rf_cust_resident_fl end as rf_benef_resident_fl, 
               case when dbt_cdt_cd = 'C' then rf_cust_gndr_cd end as rf_benef_gndr_cd, 
               case when dbt_cdt_cd = 'C' then rf_cust_ogrn_nb end as rf_benef_ogrn_nb, 
               case when dbt_cdt_cd = 'C' then rf_cust_birth_dt end as rf_benef_birth_dt, 
               case when dbt_cdt_cd = 'C' then rf_cust_birthplace_tx end as rf_benef_birthplace_tx, 
               case when dbt_cdt_cd = 'C' then rf_cust_reg_org_nm end as rf_benef_reg_org_nm, 
               case when dbt_cdt_cd = 'C' then rf_cust_okpo_nb end as rf_benef_okpo_nb, 
               case when dbt_cdt_cd = 'C' then rf_cust_reg_dt end as rf_benef_reg_dt,
               case when dbt_cdt_cd = 'C' then rf_acct_goz_op_cd end as rf_benef_goz_op_cd,
               to_char(null) as rcv_instn_nm,
               to_char(null) as rcv_instn_id,
               to_char(null) as rf_rcv_instn_cntry_cd,
               to_char(null) as rcv_instn_acct_id,                              
               case when dbt_cdt_cd = 'D' then rf_cndtr_cust_seq_id end as rf_scnd_orig_cust_seq_id, 
               case when dbt_cdt_cd = 'D' then cndtr_nm end as scnd_orig_nm, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_first_nm end as rf_scnd_orig_first_nm, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_midl_nm end as rf_scnd_orig_midl_nm,  
               case when dbt_cdt_cd = 'D' then rf_cndtr_reg_addr_tx end as rf_scnd_orig_reg_addr_tx, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_fact_addr_tx end as rf_scnd_orig_fact_addr_tx,
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_type_cd end as rf_scnd_orig_doc_type_cd, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_series_id end as rf_scnd_orig_doc_series_id, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_no_id end as rf_scnd_orig_doc_no_id, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_issued_by_tx end as rf_scnd_orig_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_dpt_cd end as rf_scnd_orig_doc_dpt_cd, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_doc_issue_dt end as rf_scnd_orig_doc_issue_dt,                              
               case when dbt_cdt_cd = 'D' then rf_cndtr_resident_fl end as rf_scnd_orig_resident_fl, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_gndr_cd end as rf_scnd_orig_gndr_cd, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_birth_dt end as rf_scnd_orig_birth_dt, 
               case when dbt_cdt_cd = 'D' then rf_cndtr_birthplace_tx end as rf_scnd_orig_birthplace_tx,  
               case when dbt_cdt_cd = 'C' then rf_cndtr_cust_seq_id end as rf_scnd_benef_cust_seq_id, 
               case when dbt_cdt_cd = 'C' then cndtr_nm end as scnd_benef_nm, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_first_nm end as rf_scnd_benef_first_nm, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_midl_nm end as rf_scnd_benef_midl_nm,  
               case when dbt_cdt_cd = 'C' then rf_cndtr_reg_addr_tx end as rf_scnd_benef_reg_addr_tx, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_fact_addr_tx end as rf_scnd_benef_fact_addr_tx,
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_type_cd end as rf_scnd_benef_doc_type_cd, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_series_id end as rf_scnd_benef_doc_series_id, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_no_id end as rf_scnd_benef_doc_no_id, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_issued_by_tx end as rf_scnd_benef_doc_issued_by_tx, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_dpt_cd end as rf_scnd_benef_doc_dpt_cd, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_doc_issue_dt end as rf_scnd_benef_doc_issue_dt,               
               case when dbt_cdt_cd = 'C' then rf_cndtr_resident_fl end as rf_scnd_benef_resident_fl, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_gndr_cd end as rf_scnd_benef_gndr_cd, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_birth_dt end as rf_scnd_benef_birth_dt, 
               case when dbt_cdt_cd = 'C' then rf_cndtr_birthplace_tx end as rf_scnd_benef_birthplace_tx,  
               rf_trxn_doc_id, rf_trxn_doc_dt, bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx,
               rf_src_change_dt, rf_canceled_fl 
          from business.cash_trxn_arh
         where par_op_cat_cd = 'CT' and
               fo_trxn_seq_id = par_trxn_seq_id and
               arh_dump_dt in (par_new_arh_dump_dt, par_old_arh_dump_dt)
        union all
        select to_date('01.01.9000', 'dd.mm.yyyy') as arh_dump_dt,
               bo_trxn_seq_id as trxn_seq_id,
               exctn_dt as trxn_exctn_dt, rf_branch_id, trxn_rptng_am as rf_trxn_crncy_am, trxn_base_am, rptng_crncy_cd as rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, 
               dscr_tx as trxn_desc, trxn_type1_cd,
               to_char(null) as trxn_conv_crncy_cd,
               to_number(null) as trxn_conv_am,
               to_number(null) as trxn_conv_base_am,
               case when dbt_cdt_cd = 'D' then rf_cust_seq_id end as rf_orig_cust_seq_id, 
               to_char(null) as orig_nm,                 
               to_char(null) as rf_orig_first_nm,                 
               to_char(null) as rf_orig_midl_nm,                     
               to_char(null) as rf_orig_inn_nb, 
               to_char(null) as rf_orig_kpp_nb,               
               to_char(null) as rf_orig_acct_nb, 
               case when dbt_cdt_cd = 'D' then rf_acct_seq_id end as rf_orig_acct_seq_id, 
               rf_debit_cd,
               to_char(null) as rf_orig_reg_addr_tx, 
               to_char(null) as rf_orig_fact_addr_tx,
               to_number(null) as rf_orig_doc_type_cd, 
               to_char(null) as rf_orig_doc_series_id, 
               to_char(null) as rf_orig_doc_no_id, 
               to_char(null) as rf_orig_doc_issued_by_tx, 
               to_char(null) as rf_orig_doc_dpt_cd, 
               to_date(null) as rf_orig_doc_issue_dt,
               to_char(null) as rf_orig_resident_fl, 
               to_char(null) as rf_orig_gndr_cd, 
               to_char(null) as rf_orig_ogrn_nb, 
               to_date(null) as rf_orig_birth_dt, 
               to_char(null) as rf_orig_birthplace_tx, 
               to_char(null) as rf_orig_reg_org_nm, 
               to_char(null) as rf_orig_okpo_nb, 
               to_date(null) as rf_orig_reg_dt,
               case when dbt_cdt_cd = 'D' then rf_acct_goz_op_cd end as rf_orig_goz_op_cd,
               to_char(null) as send_instn_nm,
               to_char(null) as send_instn_id,
               to_char(null) as rf_send_instn_cntry_cd,
               to_char(null) as send_instn_acct_id,               
               case when dbt_cdt_cd = 'C' then rf_cust_seq_id end as rf_benef_cust_seq_id, 
               to_char(null) as benef_nm,                 
               to_char(null) as rf_benef_first_nm,                 
               to_char(null) as rf_benef_midl_nm,                     
               to_char(null) as rf_benef_inn_nb, 
               to_char(null) as rf_benef_kpp_nb,               
               to_char(null) as rf_benef_acct_nb, 
               case when dbt_cdt_cd = 'C' then rf_acct_seq_id end as rf_benef_acct_seq_id,                
               rf_credit_cd,
               to_char(null) as rf_benef_reg_addr_tx, 
               to_char(null) as rf_benef_fact_addr_tx,
               to_number(null) as rf_benef_doc_type_cd, 
               to_char(null) as rf_benef_doc_series_id, 
               to_char(null) as rf_benef_doc_no_id, 
               to_char(null) as rf_benef_doc_issued_by_tx, 
               to_char(null) as rf_benef_doc_dpt_cd, 
               to_date(null) as rf_benef_doc_issue_dt,
               to_char(null) as rf_benef_resident_fl, 
               to_char(null) as rf_benef_gndr_cd, 
               to_char(null) as rf_benef_ogrn_nb, 
               to_date(null) as rf_benef_birth_dt, 
               to_char(null) as rf_benef_birthplace_tx, 
               to_char(null) as rf_benef_reg_org_nm, 
               to_char(null) as rf_benef_okpo_nb, 
               to_date(null) as rf_benef_reg_dt,
               case when dbt_cdt_cd = 'C' then rf_acct_goz_op_cd end as rf_benef_goz_op_cd,
               to_char(null) as rcv_instn_nm,
               to_char(null) as rcv_instn_id,
               to_char(null) as rf_rcv_instn_cntry_cd,
               to_char(null) as rcv_instn_acct_id,                              
               to_number(null) as rf_scnd_orig_cust_seq_id, 
               to_char(null) as scnd_orig_nm, 
               to_char(null) as rf_scnd_orig_first_nm, 
               to_char(null) as rf_scnd_orig_midl_nm,  
               to_char(null) as rf_scnd_orig_reg_addr_tx, 
               to_char(null) as rf_scnd_orig_fact_addr_tx,
               to_number(null) as rf_scnd_orig_doc_type_cd, 
               to_char(null) as rf_scnd_orig_doc_series_id, 
               to_char(null) as rf_scnd_orig_doc_no_id, 
               to_char(null) as rf_scnd_orig_doc_issued_by_tx, 
               to_char(null) as rf_scnd_orig_doc_dpt_cd, 
               to_date(null) as rf_scnd_orig_doc_issue_dt,                              
               to_char(null) as rf_scnd_orig_resident_fl, 
               to_char(null) as rf_scnd_orig_gndr_cd, 
               to_date(null) as rf_scnd_orig_birth_dt, 
               to_char(null) as rf_scnd_orig_birthplace_tx,  
               to_number(null) as rf_scnd_benef_cust_seq_id, 
               to_char(null) as scnd_benef_nm, 
               to_char(null) as rf_scnd_benef_first_nm, 
               to_char(null) as rf_scnd_benef_midl_nm,  
               to_char(null) as rf_scnd_benef_reg_addr_tx, 
               to_char(null) as rf_scnd_benef_fact_addr_tx,
               to_number(null) as rf_scnd_benef_doc_type_cd, 
               to_char(null) as rf_scnd_benef_doc_series_id, 
               to_char(null) as rf_scnd_benef_doc_no_id, 
               to_char(null) as rf_scnd_benef_doc_issued_by_tx, 
               to_char(null) as rf_scnd_benef_doc_dpt_cd, 
               to_date(null) as rf_scnd_benef_doc_issue_dt,               
               to_char(null) as rf_scnd_benef_resident_fl, 
               to_char(null) as rf_scnd_benef_gndr_cd, 
               to_date(null) as rf_scnd_benef_birth_dt, 
               to_char(null) as rf_scnd_benef_birthplace_tx,  
               to_char(null) as rf_trxn_doc_id, 
               to_date(null) as rf_trxn_doc_dt, 
               bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx,
               rf_src_change_dt, rf_canceled_fl 
          from business.back_office_trxn
         where par_op_cat_cd = 'BOT' and
               bo_trxn_seq_id = par_trxn_seq_id
        union all
        select arh_dump_dt,
               bo_trxn_seq_id as trxn_seq_id,
               exctn_dt as trxn_exctn_dt, rf_branch_id, trxn_rptng_am as rf_trxn_crncy_am, trxn_base_am, rptng_crncy_cd as rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, 
               dscr_tx as trxn_desc, trxn_type1_cd,
               to_char(null) as trxn_conv_crncy_cd,
               to_number(null) as trxn_conv_am,
               to_number(null) as trxn_conv_base_am,
               case when dbt_cdt_cd = 'D' then rf_cust_seq_id end as rf_orig_cust_seq_id, 
               to_char(null) as orig_nm,                 
               to_char(null) as rf_orig_first_nm,                 
               to_char(null) as rf_orig_midl_nm,                     
               to_char(null) as rf_orig_inn_nb, 
               to_char(null) as rf_orig_kpp_nb,               
               to_char(null) as rf_orig_acct_nb, 
               case when dbt_cdt_cd = 'D' then rf_acct_seq_id end as rf_orig_acct_seq_id, 
               rf_debit_cd,
               to_char(null) as rf_orig_reg_addr_tx, 
               to_char(null) as rf_orig_fact_addr_tx,
               to_number(null) as rf_orig_doc_type_cd, 
               to_char(null) as rf_orig_doc_series_id, 
               to_char(null) as rf_orig_doc_no_id, 
               to_char(null) as rf_orig_doc_issued_by_tx, 
               to_char(null) as rf_orig_doc_dpt_cd, 
               to_date(null) as rf_orig_doc_issue_dt,
               to_char(null) as rf_orig_resident_fl, 
               to_char(null) as rf_orig_gndr_cd, 
               to_char(null) as rf_orig_ogrn_nb, 
               to_date(null) as rf_orig_birth_dt, 
               to_char(null) as rf_orig_birthplace_tx, 
               to_char(null) as rf_orig_reg_org_nm, 
               to_char(null) as rf_orig_okpo_nb, 
               to_date(null) as rf_orig_reg_dt,
               case when dbt_cdt_cd = 'D' then rf_acct_goz_op_cd end as rf_orig_goz_op_cd,
               to_char(null) as send_instn_nm,
               to_char(null) as send_instn_id,
               to_char(null) as rf_send_instn_cntry_cd,
               to_char(null) as send_instn_acct_id,               
               case when dbt_cdt_cd = 'C' then rf_cust_seq_id end as rf_benef_cust_seq_id, 
               to_char(null) as benef_nm,                 
               to_char(null) as rf_benef_first_nm,                 
               to_char(null) as rf_benef_midl_nm,                     
               to_char(null) as rf_benef_inn_nb, 
               to_char(null) as rf_benef_kpp_nb,               
               to_char(null) as rf_benef_acct_nb, 
               case when dbt_cdt_cd = 'C' then rf_acct_seq_id end as rf_benef_acct_seq_id,                
               rf_credit_cd,
               to_char(null) as rf_benef_reg_addr_tx, 
               to_char(null) as rf_benef_fact_addr_tx,
               to_number(null) as rf_benef_doc_type_cd, 
               to_char(null) as rf_benef_doc_series_id, 
               to_char(null) as rf_benef_doc_no_id, 
               to_char(null) as rf_benef_doc_issued_by_tx, 
               to_char(null) as rf_benef_doc_dpt_cd, 
               to_date(null) as rf_benef_doc_issue_dt,
               to_char(null) as rf_benef_resident_fl, 
               to_char(null) as rf_benef_gndr_cd, 
               to_char(null) as rf_benef_ogrn_nb, 
               to_date(null) as rf_benef_birth_dt, 
               to_char(null) as rf_benef_birthplace_tx, 
               to_char(null) as rf_benef_reg_org_nm, 
               to_char(null) as rf_benef_okpo_nb, 
               to_date(null) as rf_benef_reg_dt,
               case when dbt_cdt_cd = 'C' then rf_acct_goz_op_cd end as rf_benef_goz_op_cd,
               to_char(null) as rcv_instn_nm,
               to_char(null) as rcv_instn_id,
               to_char(null) as rf_rcv_instn_cntry_cd,
               to_char(null) as rcv_instn_acct_id,                              
               to_number(null) as rf_scnd_orig_cust_seq_id, 
               to_char(null) as scnd_orig_nm, 
               to_char(null) as rf_scnd_orig_first_nm, 
               to_char(null) as rf_scnd_orig_midl_nm,  
               to_char(null) as rf_scnd_orig_reg_addr_tx, 
               to_char(null) as rf_scnd_orig_fact_addr_tx,
               to_number(null) as rf_scnd_orig_doc_type_cd, 
               to_char(null) as rf_scnd_orig_doc_series_id, 
               to_char(null) as rf_scnd_orig_doc_no_id, 
               to_char(null) as rf_scnd_orig_doc_issued_by_tx, 
               to_char(null) as rf_scnd_orig_doc_dpt_cd, 
               to_date(null) as rf_scnd_orig_doc_issue_dt,                              
               to_char(null) as rf_scnd_orig_resident_fl, 
               to_char(null) as rf_scnd_orig_gndr_cd, 
               to_date(null) as rf_scnd_orig_birth_dt, 
               to_char(null) as rf_scnd_orig_birthplace_tx,  
               to_number(null) as rf_scnd_benef_cust_seq_id, 
               to_char(null) as scnd_benef_nm, 
               to_char(null) as rf_scnd_benef_first_nm, 
               to_char(null) as rf_scnd_benef_midl_nm,  
               to_char(null) as rf_scnd_benef_reg_addr_tx, 
               to_char(null) as rf_scnd_benef_fact_addr_tx,
               to_number(null) as rf_scnd_benef_doc_type_cd, 
               to_char(null) as rf_scnd_benef_doc_series_id, 
               to_char(null) as rf_scnd_benef_doc_no_id, 
               to_char(null) as rf_scnd_benef_doc_issued_by_tx, 
               to_char(null) as rf_scnd_benef_doc_dpt_cd, 
               to_date(null) as rf_scnd_benef_doc_issue_dt,               
               to_char(null) as rf_scnd_benef_resident_fl, 
               to_char(null) as rf_scnd_benef_gndr_cd, 
               to_date(null) as rf_scnd_benef_birth_dt, 
               to_char(null) as rf_scnd_benef_birthplace_tx,  
               to_char(null) as rf_trxn_doc_id, 
               to_date(null) as rf_trxn_doc_dt, 
               bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx,
               rf_src_change_dt, rf_canceled_fl 
          from business.back_office_trxn_arh
         where par_op_cat_cd = 'BOT' and
               bo_trxn_seq_id = par_trxn_seq_id and
               arh_dump_dt in (par_new_arh_dump_dt, par_old_arh_dump_dt))
  select case when max(n.trxn_seq_id) is null or max(o.trxn_seq_id) is null 
              then to_char(null)
              else 
                max(to_char(substr(
                      to_clob('')||                 
                      case when n.trxn_exctn_dt <> o.trxn_exctn_dt                                   then ', ���� ��������' end||
                      case when nvl(n.rf_branch_id, -1) <> nvl(o.rf_branch_id, -1)                   then ', ��/���' end||
                      case when nvl(n.rf_trxn_crncy_am, 0) <> nvl(o.rf_trxn_crncy_am, 0)             then ', ����� � ������' end||
                      case when n.trxn_base_am <> o.trxn_base_am                                     then ', ����� � ������' end||
                      case when nvl(n.rf_trxn_crncy_cd, '$&!@') <> nvl(o.rf_trxn_crncy_cd, '$&!@')   then ', ������' end||
                      case when nvl(n.rf_trxn_crncy_rate_am, 0) <> nvl(o.rf_trxn_crncy_rate_am, 0)   then ', ���� ������' end||
                      case when nvl(n.trxn_desc, '$&!@') <> nvl(o.trxn_desc, '$&!@')                 then ', ���������� �������' end||
                      case when nvl(n.trxn_type1_cd, '$&!@') <> nvl(o.trxn_type1_cd, '$&!@')         then ', ��� �������� ��� ���' end||
                      case when nvl(n.trxn_conv_crncy_cd, '$&!@') <> nvl(o.trxn_conv_crncy_cd, '$&!@') then ', ������ ���������' end||
                      case when nvl(n.trxn_conv_am, 0) <> nvl(o.trxn_conv_am, 0)                     then ', ����� ���������' end||
                      case when nvl(n.trxn_conv_base_am, 0) <> nvl(o.trxn_conv_base_am, 0)           then ', ����� ��������� � ������' end||
                      case when nvl(n.rf_orig_cust_seq_id, -1) <> nvl(o.rf_orig_cust_seq_id, -1)     then ', ����������' end||
                      case when nvl(n.orig_nm, '$&!@') <> nvl(o.orig_nm, '$&!@')                     then ', ������������ �����������' end||
                      case when nvl(n.rf_orig_first_nm, '$&!@') <> nvl(o.rf_orig_first_nm, '$&!@')   then ', ��� �����������' end||
                      case when nvl(n.rf_orig_midl_nm, '$&!@') <> nvl(o.rf_orig_midl_nm, '$&!@')     then ', �������� �����������' end||
                      case when nvl(n.rf_orig_inn_nb, '$&!@') <> nvl(o.rf_orig_inn_nb, '$&!@')       then ', ��� �����������' end||
                      case when nvl(n.rf_orig_kpp_nb, '$&!@') <> nvl(o.rf_orig_kpp_nb, '$&!@')       then ', ��� �����������' end||
                      case when nvl(n.rf_orig_acct_nb, '$&!@') <> nvl(o.rf_orig_acct_nb, '$&!@')     then ', ����� ����� �����������' end||
                      case when nvl(n.rf_orig_acct_seq_id, -1) <> nvl(o.rf_orig_acct_seq_id, -1)     then ', ���� �����������' end||
                      case when nvl(n.rf_debit_cd, '$&!@') <> nvl(o.rf_debit_cd, '$&!@')             then ', �����' end||
                      case when nvl(n.rf_orig_reg_addr_tx, '$&!@') <> nvl(o.rf_orig_reg_addr_tx, '$&!@') then ', ����� ����������� �����������' end||
                      case when nvl(n.rf_orig_fact_addr_tx, '$&!@') <> nvl(o.rf_orig_fact_addr_tx, '$&!@') then ', ����� ����� ���������� �����������' end||
                      case when nvl(n.rf_orig_doc_type_cd, -1) <> nvl(o.rf_orig_doc_type_cd, -1)     then ', ��� ��. ��������� �����������' end||
                      case when nvl(n.rf_orig_doc_series_id, '$&!@') <> nvl(o.rf_orig_doc_series_id, '$&!@') then ', ����� ��. ���. �����������' end||
                      case when nvl(n.rf_orig_doc_no_id, '$&!@') <> nvl(o.rf_orig_doc_no_id, '$&!@') then ', ����� ��. ���. �����������' end||
                      case when nvl(n.rf_orig_doc_issued_by_tx, '$&!@') <> nvl(o.rf_orig_doc_issued_by_tx, '$&!@') then ', ��� ����� ��. ���. �����������' end||
                      case when nvl(n.rf_orig_doc_dpt_cd, '$&!@') <> nvl(o.rf_orig_doc_dpt_cd, '$&!@') then ', ��� ������������� ��. ���. �����������' end||
                      case when nvl(n.rf_orig_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_orig_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ������ ��. ���. �����������' end||
                      case when nvl(n.rf_orig_resident_fl, '$&!@') <> nvl(o.rf_orig_resident_fl, '$&!@') then ', ������� ������������� �����������' end||
                      case when nvl(n.rf_orig_gndr_cd, '$&!@') <> nvl(o.rf_orig_gndr_cd, '$&!@')     then ', ��� �����������' end||
                      case when nvl(n.rf_orig_ogrn_nb, '$&!@') <> nvl(o.rf_orig_ogrn_nb, '$&!@')     then ', ���� �����������' end||
                      case when nvl(n.rf_orig_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_orig_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� �������� �����������' end||
                      case when nvl(n.rf_orig_birthplace_tx, '$&!@') <> nvl(o.rf_orig_birthplace_tx, '$&!@') then ', ����� �������� �����������' end||
                      case when nvl(n.rf_orig_reg_org_nm, '$&!@') <> nvl(o.rf_orig_reg_org_nm, '$&!@') then ', ���. ����� �����������' end||
                      case when nvl(n.rf_orig_okpo_nb, '$&!@') <> nvl(o.rf_orig_okpo_nb, '$&!@')     then ', ���� �����������' end||
                      case when nvl(n.rf_orig_reg_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_orig_reg_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ����������� �����������' end||
                      case when nvl(n.rf_orig_goz_op_cd, '$&!@') <> nvl(o.rf_orig_goz_op_cd, '$&!@') then ', ��� �������� ��� (��������)' end||
                      case when nvl(n.send_instn_nm, '$&!@') <> nvl(o.send_instn_nm, '$&!@') then ', ������������ ����� �����������' end||
                      case when nvl(n.send_instn_id, '$&!@') <> nvl(o.send_instn_id, '$&!@') then ', ��� ����� �����������' end||
                      case when nvl(n.rf_send_instn_cntry_cd, '$&!@') <> nvl(o.rf_send_instn_cntry_cd, '$&!@') then ', ������ ����� �����������' end||
                      case when nvl(n.send_instn_acct_id, '$&!@') <> nvl(o.send_instn_acct_id, '$&!@') then ', ����. ���� ����� �����������' end||
                      case when nvl(n.rf_benef_cust_seq_id, -1) <> nvl(o.rf_benef_cust_seq_id, -1)   then ', ����������' end||
                      case when nvl(n.benef_nm, '$&!@') <> nvl(o.benef_nm, '$&!@')                   then ', ������������ ����������' end||
                      case when nvl(n.rf_benef_first_nm, '$&!@') <> nvl(o.rf_benef_first_nm, '$&!@') then ', ��� ����������' end||
                      case when nvl(n.rf_benef_midl_nm, '$&!@') <> nvl(o.rf_benef_midl_nm, '$&!@')   then ', �������� ����������' end||
                      case when nvl(n.rf_benef_inn_nb, '$&!@') <> nvl(o.rf_benef_inn_nb, '$&!@')     then ', ��� ����������' end||
                      case when nvl(n.rf_benef_kpp_nb, '$&!@') <> nvl(o.rf_benef_kpp_nb, '$&!@')     then ', ��� ����������' end||
                      case when nvl(n.rf_benef_acct_nb, '$&!@') <> nvl(o.rf_benef_acct_nb, '$&!@')   then ', ����� ����� ����������' end||
                      case when nvl(n.rf_benef_acct_seq_id, -1) <> nvl(o.rf_benef_acct_seq_id, -1)   then ', ���� ����������' end||
                      case when nvl(n.rf_credit_cd, '$&!@') <> nvl(o.rf_credit_cd, '$&!@')           then ', ������' end||
                      case when nvl(n.rf_benef_reg_addr_tx, '$&!@') <> nvl(o.rf_benef_reg_addr_tx, '$&!@') then ', ����� ����������� ����������' end||
                      case when nvl(n.rf_benef_fact_addr_tx, '$&!@') <> nvl(o.rf_benef_fact_addr_tx, '$&!@') then ', ����� ����� ���������� ����������' end||
                      case when nvl(n.rf_benef_doc_type_cd, -1) <> nvl(o.rf_benef_doc_type_cd, -1)   then ', ��� ��. ��������� ����������' end||
                      case when nvl(n.rf_benef_doc_series_id, '$&!@') <> nvl(o.rf_benef_doc_series_id, '$&!@') then ', ����� ��. ���. ����������' end||
                      case when nvl(n.rf_benef_doc_no_id, '$&!@') <> nvl(o.rf_benef_doc_no_id, '$&!@') then ', ����� ��. ���. ����������' end||
                      case when nvl(n.rf_benef_doc_issued_by_tx, '$&!@') <> nvl(o.rf_benef_doc_issued_by_tx, '$&!@') then ', ��� ����� ��. ���. ����������' end||
                      case when nvl(n.rf_benef_doc_dpt_cd, '$&!@') <> nvl(o.rf_benef_doc_dpt_cd, '$&!@') then ', ��� ������������� ��. ���. ����������' end||
                      case when nvl(n.rf_benef_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_benef_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ������ ��. ���. ����������' end||
                      case when nvl(n.rf_benef_resident_fl, '$&!@') <> nvl(o.rf_benef_resident_fl, '$&!@') then ', ������� ������������� ����������' end||
                      case when nvl(n.rf_benef_gndr_cd, '$&!@') <> nvl(o.rf_benef_gndr_cd, '$&!@') then ', ��� ����������' end||
                      case when nvl(n.rf_benef_ogrn_nb, '$&!@') <> nvl(o.rf_benef_ogrn_nb, '$&!@') then ', ���� ����������' end||
                      case when nvl(n.rf_benef_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_benef_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� �������� ����������' end||
                      case when nvl(n.rf_benef_birthplace_tx, '$&!@') <> nvl(o.rf_benef_birthplace_tx, '$&!@') then ', ����� �������� ����������' end||
                      case when nvl(n.rf_benef_reg_org_nm, '$&!@') <> nvl(o.rf_benef_reg_org_nm, '$&!@') then ', ���. ����� ����������' end||
                      case when nvl(n.rf_benef_okpo_nb, '$&!@') <> nvl(o.rf_benef_okpo_nb, '$&!@') then ', ���� ����������' end||
                      case when nvl(n.rf_benef_reg_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_benef_reg_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ����������� ����������' end||
                      case when nvl(n.rf_benef_goz_op_cd, '$&!@') <> nvl(o.rf_benef_goz_op_cd, '$&!@') then ', ��� �������� ��� (����������)' end||
                      case when nvl(n.rcv_instn_nm, '$&!@') <> nvl(o.rcv_instn_nm, '$&!@')           then ', ������������ ����� ����������' end||
                      case when nvl(n.rcv_instn_id, '$&!@') <> nvl(o.rcv_instn_id, '$&!@')           then ', ��� ����� ����������' end||
                      case when nvl(n.rf_rcv_instn_cntry_cd, '$&!@') <> nvl(o.rf_rcv_instn_cntry_cd, '$&!@') then ', ������ ����� ����������' end||
                      case when nvl(n.rcv_instn_acct_id, '$&!@') <> nvl(o.rcv_instn_acct_id, '$&!@') then ', ����. ���� ����� ����������' end||
                      case when nvl(n.rf_scnd_orig_cust_seq_id, -1) <> nvl(o.rf_scnd_orig_cust_seq_id, -1) then ', ������������� �����������' end||
                      case when nvl(n.scnd_orig_nm, '$&!@') <> nvl(o.scnd_orig_nm, '$&!@')           then ', ������������ ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_first_nm, '$&!@') <> nvl(o.rf_scnd_orig_first_nm, '$&!@') then ', ��� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_midl_nm, '$&!@') <> nvl(o.rf_scnd_orig_midl_nm, '$&!@') then ', �������� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_reg_addr_tx, '$&!@') <> nvl(o.rf_scnd_orig_reg_addr_tx, '$&!@') then ', ����� ����������� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_fact_addr_tx, '$&!@') <> nvl(o.rf_scnd_orig_fact_addr_tx, '$&!@') then ', ����� ����� ���������� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_doc_type_cd, -1) <> nvl(o.rf_scnd_orig_doc_type_cd, -1) then ', ��� ��. ��������� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_doc_series_id, '$&!@') <> nvl(o.rf_scnd_orig_doc_series_id, '$&!@') then ', ����� ��. ���. ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_doc_no_id, '$&!@') <> nvl(o.rf_scnd_orig_doc_no_id, '$&!@') then ', ����� ��. ���. ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_doc_issued_by_tx, '$&!@') <> nvl(o.rf_scnd_orig_doc_issued_by_tx, '$&!@') then ', ��� ����� ��. ���. ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_doc_dpt_cd, '$&!@') <> nvl(o.rf_scnd_orig_doc_dpt_cd, '$&!@') then ', ��� ������������� ��. ���. ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_scnd_orig_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ������ ��. ���. ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_resident_fl, '$&!@') <> nvl(o.rf_scnd_orig_resident_fl, '$&!@') then ', ������� ������������� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_gndr_cd, '$&!@') <> nvl(o.rf_scnd_orig_gndr_cd, '$&!@') then ', ��� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_scnd_orig_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� �������� ������������� �����������' end||
                      case when nvl(n.rf_scnd_orig_birthplace_tx, '$&!@') <> nvl(o.rf_scnd_orig_birthplace_tx, '$&!@') then ', ����� �������� ������������� �����������' end||
                      case when nvl(n.rf_scnd_benef_cust_seq_id, -1) <> nvl(o.rf_scnd_benef_cust_seq_id, -1) then ', ������������� ����������' end||
                      case when nvl(n.scnd_benef_nm, '$&!@') <> nvl(o.scnd_benef_nm, '$&!@') then ', ������������ ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_first_nm, '$&!@') <> nvl(o.rf_scnd_benef_first_nm, '$&!@') then ', ��� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_midl_nm, '$&!@') <> nvl(o.rf_scnd_benef_midl_nm, '$&!@') then ', �������� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_reg_addr_tx, '$&!@') <> nvl(o.rf_scnd_benef_reg_addr_tx, '$&!@') then ', ����� ����������� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_fact_addr_tx, '$&!@') <> nvl(o.rf_scnd_benef_fact_addr_tx, '$&!@') then ', ����� ����� ���������� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_doc_type_cd, -1) <> nvl(o.rf_scnd_benef_doc_type_cd, -1) then ', ��� ��. ��������� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_doc_series_id, '$&!@') <> nvl(o.rf_scnd_benef_doc_series_id, '$&!@') then ', ����� ��. ���. ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_doc_no_id, '$&!@') <> nvl(o.rf_scnd_benef_doc_no_id, '$&!@') then ', ����� ��. ���. ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_doc_issued_by_tx, '$&!@') <> nvl(o.rf_scnd_benef_doc_issued_by_tx, '$&!@') then ', ��� ����� ��. ���. ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_doc_dpt_cd, '$&!@') <> nvl(o.rf_scnd_benef_doc_dpt_cd, '$&!@') then ', ��� ������������� ��. ���. ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_scnd_benef_doc_issue_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ������ ��. ���. ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_resident_fl, '$&!@') <> nvl(o.rf_scnd_benef_resident_fl, '$&!@') then ', ������� ������������� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_gndr_cd, '$&!@') <> nvl(o.rf_scnd_benef_gndr_cd, '$&!@') then ', ��� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_scnd_benef_birth_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� �������� ������������� ����������' end||
                      case when nvl(n.rf_scnd_benef_birthplace_tx, '$&!@') <> nvl(o.rf_scnd_benef_birthplace_tx, '$&!@') then ', ����� �������� ������������� ����������' end||                        
                      case when nvl(n.rf_trxn_doc_id, '$&!@') <> nvl(o.rf_trxn_doc_id, '$&!@')       then ', ����� ���������� ���������' end||
                      case when nvl(n.rf_trxn_doc_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_trxn_doc_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ���������� ���������' end||
                      case when nvl(n.bank_card_id_nb, -1) <> nvl(o.bank_card_id_nb, -1)             then ', ����� ���������� �����' end||
                      case when nvl(n.cstm_1_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.cstm_1_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� � ����� �������� (��)' end||
                      case when nvl(n.mantas_trxn_chanl_dtl_1_tx, '$&!@') <> nvl(o.mantas_trxn_chanl_dtl_1_tx, '$&!@') then ', ����� ������������ (��)' end||
                      case when nvl(n.mantas_trxn_chanl_dtl_2_tx, '$&!@') <> nvl(o.mantas_trxn_chanl_dtl_2_tx, '$&!@') then ', �������� (��)' end||
                      case when nvl(n.rf_src_change_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> nvl(o.rf_src_change_dt, to_date('01.01.9000', 'dd.mm.yyyy')) then ', ���� ���������� ��������� � ���������' end
                    , 3, 2000)))
         end   
    into var_result             
    from -- "�����" ������ �������� - �� ����� 1 ������
         (select * from trxn where arh_dump_dt = nvl(par_new_arh_dump_dt, to_date('01.01.9000', 'dd.mm.yyyy'))) n 
         full join
         -- "�������" ������ �������� - �� ����� 1 ������
         (select * from trxn where arh_dump_dt = nvl(par_old_arh_dump_dt, to_date('01.01.9000', 'dd.mm.yyyy'))) o on 1 = 1;
  --
  -- ������� ������ ��������� ������ ������ � ������� � ���������
  --
  if par_trxn_scrty_seq_id is not null then
    with trxn_scrty as
         (select to_date('01.01.9000', 'dd.mm.yyyy') as arh_dump_dt,
                 trxn_scrty_seq_id,
                 scrty_type_cd, scrty_id, scrty_nb, scrty_nominal, nominal_crncy_cd, scrty_price, price_crncy_cd, scrty_base_am, scrty_seq_id
            from business.rf_wire_trxn_scrty
           where par_op_cat_cd = 'WT' and
                 trxn_scrty_seq_id = par_trxn_scrty_seq_id
          union all
          select arh_dump_dt,
                 trxn_scrty_seq_id,
                 scrty_type_cd, scrty_id, scrty_nb, scrty_nominal, nominal_crncy_cd, scrty_price, price_crncy_cd, scrty_base_am, scrty_seq_id
            from business.rf_wire_trxn_scrty_arh
           where par_op_cat_cd = 'WT' and
                 trxn_scrty_seq_id = par_trxn_scrty_seq_id and
                 arh_dump_dt in (par_new_arh_dump_dt, par_old_arh_dump_dt)
          union all
          select to_date('01.01.9000', 'dd.mm.yyyy') as arh_dump_dt,
                 trxn_scrty_seq_id,
                 scrty_type_cd, scrty_id, scrty_nb, scrty_nominal, nominal_crncy_cd, scrty_price, price_crncy_cd, scrty_base_am, scrty_seq_id
            from business.rf_cash_trxn_scrty
           where par_op_cat_cd = 'CT' and
                 trxn_scrty_seq_id = par_trxn_scrty_seq_id
          union all
          select arh_dump_dt,
                 trxn_scrty_seq_id,
                 scrty_type_cd, scrty_id, scrty_nb, scrty_nominal, nominal_crncy_cd, scrty_price, price_crncy_cd, scrty_base_am, scrty_seq_id
            from business.rf_cash_trxn_scrty_arh
           where par_op_cat_cd = 'CT' and
                 trxn_scrty_seq_id = par_trxn_scrty_seq_id and
                 arh_dump_dt in (par_new_arh_dump_dt, par_old_arh_dump_dt))
    select case when max(n.trxn_scrty_seq_id) is null or max(o.trxn_scrty_seq_id) is null 
                then to_char(null)
                else               
                  max(to_char(substr(
                        to_clob('')||
                        nvl2(var_result, ', '||var_result, to_char(null))|| 
                        case when nvl(n.scrty_type_cd, '$&!@') <> nvl(o.scrty_type_cd, '$&!@')       then ', ��� ������ ������' end||
                        case when nvl(n.scrty_id, '$&!@') <> nvl(o.scrty_id, '$&!@')                 then ', ������ ����� ������ ������' end||
                        case when nvl(n.scrty_nb, '$&!@') <> nvl(o.scrty_nb, '$&!@')                 then ', ����� ������ ������ ��� �����' end||
                        case when nvl(n.scrty_nominal, 0) <> nvl(o.scrty_nominal, 0)                 then ', ������� ������ ������' end||
                        case when nvl(n.nominal_crncy_cd, '$&!@') <> nvl(o.nominal_crncy_cd, '$&!@') then ', ������ �������� ������ ������' end||
                        case when nvl(n.scrty_price, 0) <> nvl(o.scrty_price, 0)                     then ', ���� ���������� ������ ������' end||
                        case when nvl(n.price_crncy_cd, '$&!@') <> nvl(o.price_crncy_cd, '$&!@')     then ', ������ ���������� ������ ������' end||
                        case when nvl(n.scrty_base_am, 0) <> nvl(o.scrty_base_am, 0)                 then ', �������� ���������� ������ ������' end||
                        case when nvl(n.scrty_seq_id, -1) <> nvl(o.scrty_seq_id, -1)                 then ', ������ ������' end
                        , 3, 2000)))
           end   
      into var_result             
    from -- "�����" ������ ������ ������ - �� ����� 1 ������
         (select * from trxn_scrty where arh_dump_dt = nvl(par_new_arh_dump_dt, to_date('01.01.9000', 'dd.mm.yyyy'))) n 
         full join
         -- "�������" ������ ������ ������ - �� ����� 1 ������
         (select * from trxn_scrty where arh_dump_dt = nvl(par_old_arh_dump_dt, to_date('01.01.9000', 'dd.mm.yyyy'))) o on 1 = 1;
  end if;
  
  return var_result;
end get_trxn_change;
--
-- ������������ �������� ��������� � ���������� ����� ���� ��� ������
--
function get_opok_change
(
  par_review_id         kdd_review.review_id%type,                       -- ID ������
  par_new_repeat_nb     rf_kdd_review_run.repeat_nb%type default null,   -- ���������� ����� "������" ��������� ������, ���� null - ���������, ������������ ��� ������
  par_old_repeat_nb     rf_kdd_review_run.repeat_nb%type default null    -- ���������� ����� "�����������" ��������� ������, ���� null - �� 1 ������ "������"
) 
return varchar2 is -- �������� ��������� � ����� ���� ����: =5002, +4005, -3001; null, ���� ��������� ��������� ���
  var_result            varchar2(2000 char);
begin
  --
  -- ���������� ���������� ���� ���������, ��������� ������ � ���������
  --
  select case when count(n.rf_trxn_seq_id) = 0 or count(o.rf_trxn_seq_id) = 0 -- �� ���� ��������� ��������� ��� ��� �� �������
              then to_char(null)
              when max(n.non_opok_fl) = 'Y' and nvl(max(o.non_opok_fl), 'N') <> 'Y'
              then '�� �������� ��������'
              when max(n.non_opok_fl) = 'J' and nvl(max(o.non_opok_fl), 'N') <> 'J'
              then '������ � ����� � ���������� ������ ������ � ��������� �� �������� (� ��������: '||max(to_char(n.joint_review_id))||')'
              else case when sum(case when n.opok_nb is null or o.opok_nb is null then 1 else 0 end) = 0
                        then '����� ���� �������� �� ��������: '
                   end||
                   listagg(case when n.opok_nb is not null and o.opok_nb is not null
                                then '='||to_char(n.opok_nb)
                                when n.opok_nb is not null and o.opok_nb is null
                                then '+'||to_char(n.opok_nb)
                                when n.opok_nb is null and o.opok_nb is not null
                                then '-'||to_char(o.opok_nb)
                           end, ', ') within group(order by nvl(n.opok_nb, o.opok_nb))
         end
    into var_result
    from -- ���� ���� "������" ��������� - �� ����� ������ ��� ���� (1 ������, ���� ����� ���� �� ��������)
         (select distinct 
                 rvr.non_opok_fl, rvo.opok_nb, rv.rf_op_cat_cd, rv.rf_trxn_seq_id,
                 case when rvr.non_opok_fl = 'J'
                      then (select max(review_id)
                              from kdd_review
                             where rf_op_cat_cd = rv.rf_op_cat_cd and
                                   rf_trxn_seq_id = rv.rf_trxn_seq_id and
                                   rf_object_tp_cd in ('TRXN_SCRTY_JOINT', 'TRXN'))
                 end as joint_review_id                   
            from kdd_review rv
                 join rf_kdd_review_run rvr on rvr.review_id = rv.review_id and
                                               rvr.repeat_nb = nvl(par_new_repeat_nb, rv.rf_repeat_nb)
                 left join rf_kdd_review_opok rvo on rvo.review_id = rvr.review_id and
                                                     rvo.repeat_nb = rvr.repeat_nb
           where rv.review_id = par_review_id and
                 rv.rf_repeat_nb <> 0 /*���� ��������� ��������� �� ���� - ���������� ������*/) n
         full join
         -- ���� ���� "�����������" ��������� - �� ����� ������ ��� ���� (1 ������, ���� ����� ���� �� ��������)         
         (select distinct rvr.non_opok_fl, rvo.opok_nb, rv.rf_op_cat_cd, rv.rf_trxn_seq_id
            from kdd_review rv
                 join rf_kdd_review_run rvr on rvr.review_id = rv.review_id and
                                               rvr.repeat_nb = nvl(par_old_repeat_nb, nvl(par_new_repeat_nb, rv.rf_repeat_nb) - 1)
                 left join rf_kdd_review_opok rvo on rvo.review_id = rvr.review_id and
                                                     rvo.repeat_nb = rvr.repeat_nb
           where rv.review_id = par_review_id and
                 rv.rf_repeat_nb <> 0 /*���� ��������� ��������� �� ���� - ���������� ������*/) o on n.opok_nb = o.opok_nb;

  return var_result;
end get_opok_change;
--
-- ������� ���������� ������
-- � ������ ��������� ���������� ������ �� ���������� = null
-- � ������ ������ ���������� ���������� ����� ������
--
function add_attachment (
  p_alert_id in number,       -- ID ��������
  p_user in varchar2,         -- ��� ������������
  p_attach_name in varchar2,  -- ������������, �������� �����
  p_cmmt_id in number,        -- ID �����������
  p_note_tx in varchar2,      -- ������������ �����������
  p_file_name in varchar2,    -- ��� �����
  p_file_type in varchar2,    -- ��� �����
  p_file_charset in varchar2, -- ��������� �����
  p_file_blob in blob,         -- ���������� �����
  p_ip_address in varchar2 default '0.0.0.0' -- IP ����� ������������
) return varchar2
is
  v_ids          mantas.rf_tab_number := mantas.rf_tab_number();
  v_owner_seq_id number;
  v_activity_ids rf_pkg_review.tab_number;
  v_activity_id  number;
  v_attach_id    number;
  v_allow        NUMBER;
  v_audit_id     rf_audit_event.audit_seq_id%TYPE; -- ������� ������� ������
begin
    -- ������� ������ ������
    v_audit_id:=rf_pkg_audit.record_gui_event(p_user, p_ip_address
      ,RF_JSON('{"form":"ALERTATTACHMENTS",action:"ADD","alert_id":'||TO_CHAR(p_alert_id)||',"attach_name":"'||p_attach_name||'","cmmt_id":"'||p_cmmt_id||'","note_tx":"'||p_note_tx||'","file_name":"'||p_file_name||'","file_type":"'||p_file_type||'","file_charset":"'||p_file_charset||'"}'));     

    -- �������� �� �����
    SELECT NVL(MAX(mantas.rf_pkg_adm.allow_alert_editing(p_user,kr.status_cd)),0)
      INTO v_allow
      FROM mantas.kdd_review kr 
      WHERE kr.review_id=p_alert_id;
    IF  v_allow=0 THEN
     rf_pkg_audit.set_event_result(v_audit_id,'A','� ��� ��� ���� �� ��������� ���������� ��������!');                  
     RETURN '� ��� ��� ���� �� ��������� ���������� ��������!';
    END IF;

  /* ���������� ID ������������ */
  select max(owner_seq_id) into v_owner_seq_id
    from mantas.kdd_review_owner
   where owner_id = p_user;
  if v_owner_seq_id is null then
    rf_pkg_audit.set_event_result(v_audit_id,'T','��������� ���������� ������! ������ �������� ������� ������������: '||p_user);                  
    raise_application_error(-20001, '������ �������� ������� ������������: '||p_user);
  end if; 

  v_ids.extend(1);
  v_ids(1) := p_alert_id;
  /* ������������ �������� */
  v_activity_ids := create_activities(
    p_review_ids    => v_ids, 
    p_actvy_type_cd => 'MTS008', 
    p_cmmnt_id      => p_cmmt_id,
    p_note_tx       => p_note_tx, 
    p_owner_seq_id  => v_owner_seq_id
  );
  
  if v_activity_ids.count > 1 or v_activity_ids.count = 0 then
    rf_pkg_audit.set_event_result(v_audit_id,'T','��������� ���������� ������! ������ ����������� ��������');                  
    raise_application_error(-20001, '������ ����������� ��������');
  end if;
  
  /* ��������� ���� */
  insert into mantas.rf_attachment (
    attch_seq_id, attch_nm, file_nm, content_type_tx, charset_tx, 
    content_data, deleted_fl, created_by, created_date
  ) values (
    rf_attachment_seq.nextval, p_attach_name, p_file_name, p_file_type,  p_file_charset,
    p_file_blob, 'N', p_user, sysdate
  ) returning attch_seq_id into v_attach_id;

  -- ��������� ������ ������ ����� ��������� ID

  /* ����� �������� - �������� */
  v_activity_id := v_activity_ids(v_activity_ids.first);
  insert into mantas.kdd_activity_doc( actvy_id, doc_id ) 
  values ( v_activity_id, v_attach_id );
  
  /* Commit, ����� */
  commit;
  return null;
  
  exception
    when others then
      rollback;
      rf_pkg_audit.set_event_result(v_audit_id,'T','��������� ���������� ������! ������ �������� ����� '||p_file_name||'. '||sqlerrm());                  
      return '������ �������� ����� '||p_file_name||'. '||sqlerrm();
end add_attachment;
--
-- ������� ������ ������
--
function get_attachment (
  p_attch_id in number,        -- ID �����
  p_file_name out varchar2,    -- ��� �����
  p_content_type out varchar2, -- ��� �����
  p_content_data out blob,     -- ���� �����
  p_charset out varchar2,       -- ��������� �����
  p_user in varchar2 default 'UNKNOWN',      -- ��� ������������
  p_ip_address in varchar2 default '0.0.0.0' -- IP ����� ������������

) return varchar2
is
  v_audit_id     rf_audit_event.audit_seq_id%TYPE; -- ������� ������� ������
begin
  -- ������� ������ ������
  v_audit_id:=rf_pkg_audit.record_gui_event(p_user, p_ip_address
    ,RF_JSON('{"form":"ALERTATTACHMENTS",action:"GET","_id_":'||TO_CHAR(p_attch_id)||'}'));     
  
  select file_nm, content_type_tx, content_data, nvl(charset_tx, 'UTF-8') 
    into p_file_name, p_content_type, p_content_data, p_charset
    from mantas.rf_attachment
   where attch_seq_id = p_attch_id;
  
  return null;
  
  exception
    when others then 
      rf_pkg_audit.set_event_result(v_audit_id,'T','��������� ���������� ������! ������ ������ �����. '||sqlerrm());                  
      return '������ ������ �����. '||sqlerrm();
end get_attachment;
--
-- ����� �������� �����
--
procedure delete_attachment(
  p_attch_id in mantas.rf_attachment.attch_seq_id%type, -- ID �����
  p_user in varchar2                                    -- ��� ������������
)
is
  v_owner_seq_id mantas.kdd_review_owner.owner_seq_id%type;
  v_ids          mantas.rf_tab_number := mantas.rf_tab_number();
  v_activity_ids rf_pkg_review.tab_number;
  v_activity_id  mantas.kdd_activity_doc.actvy_id%type;
begin
  /* ���������� ID ������������ */
  v_owner_seq_id := get_owner_id( p_user, 'mantas.delete_attachment' );
  v_ids.extend(1);
  
  select ka.review_id into v_ids(1)
    from mantas.rf_attachment ra
    join mantas.kdd_activity_doc kad on kad.doc_id = ra.attch_seq_id
    join mantas.kdd_activity ka on ka.actvy_id = kad.actvy_id
   where attch_seq_id = p_attch_id;

  /* ������������ �������� */
  v_activity_ids := create_activities(
    p_review_ids    => v_ids, 
    p_actvy_type_cd => 'MTS888', 
    p_cmmnt_id      => null,
    p_note_tx       => null, 
    p_owner_seq_id  => v_owner_seq_id
  );

  /* "�������" ���� */
  update mantas.rf_attachment
     set deleted_fl = 'Y',
         modified_by = p_user,
         modified_date = sysdate
   where attch_seq_id = p_attch_id;

  /* ����� �������� - �������� */
  v_activity_id := v_activity_ids(v_activity_ids.first);
  insert into mantas.kdd_activity_doc( actvy_id, doc_id ) 
  values ( v_activity_id, p_attch_id );
  
  commit; 
  
  exception
    when others then
      rollback;
      raise;
end;
--
--������� ��� ���������� �������
--
function create_narrative(
  p_alert_id in number,
  p_narrative in clob,
  p_user in varchar2
) return number
is
  v_ids          mantas.rf_tab_number := mantas.rf_tab_number();
  v_activity_ids rf_pkg_review.tab_number;
  v_owner_seq_id number;
  v_note_id      number;
  v_narrative_id number;
  v_err_code     number;
  v_err_msg      varchar2(255);
  v_activity_id  number;
begin
  /* ���������� ID ������������ */
  select max(owner_seq_id) into v_owner_seq_id
    from mantas.kdd_review_owner
   where owner_id = p_user;
  if v_owner_seq_id is null then
    raise_application_error(-20001, '������ �������� ������� ������������: '||p_user);
  end if;
  
  v_ids.extend(1);
  v_ids(1) := p_alert_id;
  /* ������������ �������� */
  v_activity_ids := create_activities(
    p_review_ids    => v_ids, 
    p_actvy_type_cd => 'MTS554', --�������� �����������
    p_cmmnt_id      => null,
    p_note_tx       => null, 
    p_owner_seq_id  => v_owner_seq_id
  );
  
  /* ��������� ������� */
  v_note_id := KDD_NOTE_HIST_SEQ.NextVal;
  /*mantas.p_sequence_nextval('NOTE_ID_SEQ', v_note_id, v_err_code, v_err_msg); 
  if v_note_id is null then
    raise_application_error(-20001, '������ ��� ��������� ���������� �������������� (NOTE_ID_SEQ): '||v_err_code||' '||v_err_msg||' (mantas.rf_pkg_review.create_review)');
  end if;*/
  insert into mantas.kdd_note_hist (
    note_id, note_tx, creat_id, creat_ts
  ) values (
    v_note_id, p_narrative, v_owner_seq_id,  sysdate
  );

  /* ������ ���� ������ �������� � Y �� N */
  update mantas.kdd_review_narrative
     set actv_fl = 'N'
   where review_id = p_alert_id
     and actv_fl = 'Y';

  /* ����� ������� - �������� */
  v_activity_id := v_activity_ids(v_activity_ids.first);
  mantas.p_sequence_nextval('REVIEW_NRTV_SEQ_ID_SEQ', v_narrative_id, v_err_code, v_err_msg);
  if v_narrative_id is null then
    raise_application_error(-20001, '������ ��� ��������� ���������� �������������� (REVIEW_NRTV_SEQ_ID_SEQ): '||v_err_code||' '||v_err_msg||' (mantas.rf_pkg_review.create_review)');
  end if;
  insert into mantas.kdd_review_narrative( 
    review_nrtv_id, review_id, note_id, actv_fl, actvy_id 
  ) values (
    v_narrative_id, p_alert_id,  v_note_id, 'Y', v_activity_id 
  );
  
  /* Commit, ����� */
  commit;
  return v_narrative_id;
  
  exception
    when others then
      rollback;
      raise;
end create_narrative;
--
-- ����������� � �������� �������� ����� �������� ������
--
PROCEDURE archive_old_alerts IS
BEGIN
  --
  -- ���������� � ����� ������, �������� (�������� � ��������� ������) ����� ��� N ���� ����� (�� ��������� 30 ����)
  --
  UPDATE mantas.kdd_review rv
     SET rf_partition_key = case when status_cd in ('RF_DONE') -- ��������� (���) ���� ����������
                                 then 'ARHS'      
                                 else (select decode(count(*), 0, 'ARHC', 'ARHS')
                                         from mantas.kdd_activity a
                                        where a.review_id = rv.review_id and
                                              a.new_review_status_cd in ('RF_DONE'))
                            end                    
   WHERE rf_partition_key = 'ACT' and
         rf_alert_type_cd = 'O' and
         coalesce(rf_cls_dt, to_date('31.12.9999', 'dd.mm.yyyy')) < trunc(SYSDATE) - nvl(to_number(mantas.rf_pkg_adm.get_install_param(9002)), 30);    
  
  COMMIT; 
END archive_old_alerts;
--
-- ������� ����������, ���� �� �������� � ������.
--   1 - ����
--   0 - ���
--
function has_attachments(
  p_alert_id in number
) return number parallel_enable
is
  v_count number := 0;
begin
  select count(*)
    into v_count
    from mantas.rf_attachment a
    join mantas.kdd_activity_doc ad on ad.doc_id = a.attch_seq_id
    join mantas.kdd_activity a on a.actvy_id = ad.actvy_id
   where a.review_id = p_alert_id
     and a.actvy_type_cd = 'MTS008'
     and a.deleted_fl = 'N'
     and rownum = 1;
  return v_count;   
end has_attachments;
--
-- ������� ��������� ��������� ��������: 
--   - ������������� �������� p_send_oes_org_id � / ��� p_rcv_oes_org_id
--   - ������� ��� ���������� �������� (p_alert_party_fl)
--      1 - � ������� ����� �����������;
--      2 - � ������� ����� ����������;
--      3 - ���������� �������������. ��� �������� �������� ��������� �� ���������.
--
function set_trxn_kgrko(
  p_op_cat_cd       in varchar, -- ��� ��������
  p_trxn_seq_id     in number,  -- ����� ��������
  p_send_oes_org_id in number,  -- ����� ������� �����������
  p_rcv_oes_org_id  in number,  -- ����� ������� ����������
  p_alert_party_fl  in number,  -- ���� ������������� ������������ �������
  p_user            in varchar2 -- ��� ������������
) return mantas.rf_tab_number
is
  v_owner_seq_id mantas.kdd_review_owner.owner_seq_id%type;
  v_activity_ids rf_pkg_review.tab_number;
  v_ids          mantas.rf_tab_number;
  v_kgrko_cnt    NUMBER;
  v_new_kgrko_party_cd mantas.kdd_review.rf_kgrko_party_cd%TYPE;
  v_new_branch_id      mantas.kdd_review.rf_branch_id%TYPE; 
begin
  savepoint set_trxn_kgrko;
  v_ids:=mantas.rf_tab_number();
  /* ���������� ID ������������ */
  select max(owner_seq_id) into v_owner_seq_id
    from mantas.kdd_review_owner
   where owner_id = p_user;
  if v_owner_seq_id is null then
    raise_application_error(-20001, '������ �������� ������� ������������: '||p_user);
  end if;
  
  /* ��������� ������ � business.rf_trxn_extra */
  merge into business.rf_trxn_extra t using (
    select p_op_cat_cd op_cat_cd, p_trxn_seq_id trxn_seq_id from dual) p
  on (p.op_cat_cd = t.op_cat_cd and p.trxn_seq_id = t.fo_trxn_seq_id)
  when matched then
    update set t.send_oes_org_id = p_send_oes_org_id,
               t.rcv_oes_org_id = p_rcv_oes_org_id,
               t.modified_by = p_user,
               t.modified_date = sysdate
  when not matched then
    insert (op_cat_cd, fo_trxn_seq_id, send_oes_org_id, rcv_oes_org_id, created_by, created_date)
    values (p_op_cat_cd, p_trxn_seq_id, p_send_oes_org_id, p_rcv_oes_org_id, p_user, sysdate);

  -- ��������� ��� ��������
  IF rf_pkg_kgrko.is_inter_kgrko(null, null, null, 1, p_send_oes_org_id, p_rcv_oes_org_id)=0
    THEN -- ������� ��������
      SELECT COUNT(DISTINCT r.rf_kgrko_party_cd)
        INTO v_kgrko_cnt
        FROM mantas.kdd_review r
        WHERE r.rf_op_cat_cd = p_op_cat_cd
        AND r.rf_trxn_seq_id = p_trxn_seq_id
        AND rf_alert_type_cd = 'O';
      IF v_kgrko_cnt<=1 THEN -- ���� ����� ��������� �� ����� ���������� �����
        -- ���������� ���� ����������������
        UPDATE mantas.kdd_review r
          SET r.rf_kgrko_party_cd=NULL
          WHERE r.rf_op_cat_cd = p_op_cat_cd
            AND r.rf_trxn_seq_id = p_trxn_seq_id
            AND rf_alert_type_cd = 'O'
            AND rf_branch_id=(SELECT 
                                  CASE
                                    WHEN p_send_oes_org_id IS NOT NULL 
                                      THEN nvl(mantas.rf_pkg_kgrko.override_branch_id(t.send_branch_id, p_send_oes_org_id, trunc(t.trxn_dt)),t.branch_id)
                                   WHEN p_rcv_oes_org_id IS NOT NULL 
                                     THEN nvl(mantas.rf_pkg_kgrko.override_branch_id(t.rcv_branch_id, p_rcv_oes_org_id, trunc(t.trxn_dt)), t.branch_id)
                                   ELSE branch_id 
                                  END
                                FROM TABLE(business.rfv_trxn(r.rf_op_cat_cd, r.rf_trxn_seq_id, r.rf_data_dump_dt)) t);
      END IF;
      -- ������������ �������� ��� ���� ���������� �������� �� p_op_cat_cd � p_trxn_seq_id     
      SELECT r.review_id bulk collect into v_ids
        FROM mantas.kdd_review r
        WHERE r.rf_op_cat_cd = p_op_cat_cd
        AND r.rf_trxn_seq_id = p_trxn_seq_id
        AND rf_alert_type_cd = 'O';
    ELSE -- ��������������� ��������
      FOR c_alerts IN (SELECT rowid, r.review_id, rf_pkg_kgrko.get_oes_321_org_id(TRUNC(r.rf_branch_id/100000)
                             ,r.rf_branch_id-TRUNC(r.rf_branch_id/100000)*100000,TRUNC(r.rf_trxn_dt)) oes_org_id
                             ,r.rf_op_cat_cd, r.rf_trxn_seq_id, r.rf_data_dump_dt
          FROM mantas.kdd_review r
          WHERE r.rf_op_cat_cd = p_op_cat_cd
            AND r.rf_trxn_seq_id = p_trxn_seq_id
            AND rf_alert_type_cd = 'O' 
            AND rf_kgrko_party_cd IS NULL)
        LOOP
          IF p_alert_party_fl=3 
            THEN -- ���������� ������ ������������
              IF c_alerts.oes_org_id = p_rcv_oes_org_id 
                THEN -- ���� �������� ����������
                  v_new_kgrko_party_cd:=2;
                ELSE -- �� ���� ��������� ������� ����������
                  v_new_kgrko_party_cd:=1;
            END IF;
            ELSE -- ������ �����
              v_new_kgrko_party_cd:=p_alert_party_fl;
          END IF;    
          -- ���������� ����� rf_branch_id
          BEGIN
            IF v_new_kgrko_party_cd = 1
              THEN  -- ����������
                SELECT 
                  CASE 
                    WHEN p_send_oes_org_id IS NOT NULL 
                      THEN NVL(mantas.rf_pkg_kgrko.override_branch_id(t.send_branch_id, p_send_oes_org_id, trunc(t.trxn_dt)),t.branch_id)
                    ELSE t.branch_id
                  END
                INTO v_new_branch_id  
                FROM TABLE(business.rfv_trxn(c_alerts.rf_op_cat_cd, c_alerts.rf_trxn_seq_id, c_alerts.rf_data_dump_dt)) t;
              ELSE  -- ����������
                SELECT 
                  CASE WHEN p_rcv_oes_org_id IS NOT NULL 
                      THEN NVL(mantas.rf_pkg_kgrko.override_branch_id(t.rcv_branch_id, p_rcv_oes_org_id, trunc(t.trxn_dt)),t.branch_id)
                      ELSE t.branch_id
                  END
                INTO v_new_branch_id  
                FROM TABLE(business.rfv_trxn(c_alerts.rf_op_cat_cd, c_alerts.rf_trxn_seq_id, c_alerts.rf_data_dump_dt)) t;
            END IF;
          EXCEPTION 
            WHEN OTHERS THEN
              raise_application_error(-20000,'���������� ������: �� ����� ���������� �������� ��� ����, ������ Oracle: '||sqlerrm);
          END;
          -- ����������� ������
          UPDATE mantas.kdd_review r
            SET r.rf_kgrko_party_cd=v_new_kgrko_party_cd, r.rf_branch_id=v_new_branch_id
            WHERE r.rowid=c_alerts.rowid;
          -- ��������� ������ � ������ ��� ����������
          v_ids.EXTEND;
          v_ids(v_ids.LAST):=c_alerts.review_id;
        END LOOP;
  END IF;
  
  /* ������������ �������� */
  create_activities(
    p_review_ids    => v_ids, 
    p_actvy_type_cd => 'RF_SETKGR',
    p_cmmnt_id      => null,
    p_note_tx       => null, 
    p_owner_seq_id  => v_owner_seq_id
  );
  
  commit;

  return v_ids;
  
  exception
    when others then 
      rollback to savepoint set_trxn_kgrko;
      raise;
end set_trxn_kgrko;

end rf_pkg_review;
/

GRANT EXECUTE on mantas.rf_pkg_review to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_review to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_review to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_review to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_review to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_review to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_review to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_review to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_review to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_review to KYC;
GRANT EXECUTE on mantas.rf_pkg_review to STD;
GRANT EXECUTE on mantas.rf_pkg_review to KDD_ALG;
