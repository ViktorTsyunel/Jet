create or replace package mantas.rf_pkg_request_trxn is

  /* ������� ��������� ������� ��� ������� ����������� � ��������� (trxn �������)
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json;
end;
/

create or replace package body mantas.rf_pkg_request_trxn is

  /* ��������� ������� TrxnExtra read.
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_trxn_extra( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000);
    v_trxn_id number;
    v_op_cat_cd varchar2(32);
    v_json rf_json;
    v_bind_json rf_json := rf_json;
  begin
    v_op_cat_cd := rf_json_ext.get_string(p_json, 'op_cat_cd');
    v_trxn_id := rf_json_ext.get_number(p_json, 'trxn_seq_id');
    
    /* ���� ��� � ����� �������� �� �����, ������� ��� ��������� �������� ������� - ���������� ������ */
    if v_op_cat_cd is null or v_trxn_id is null then
      v_json := rf_json();
      v_json.put('success', false);
      v_json.put('message', '�������� �� ��������� ��� ���, ��������� �������.');
    else  
      v_sql := q'{
        select coalesce(e.op_cat_cd, t.op_cat_cd) as op_cat_cd
             , coalesce(e.fo_trxn_seq_id, t.trxn_seq_id) as trxn_seq_id
             , coalesce(e.send_oes_org_id, t.send_oes_org_id) as send_oes_org_id
             , coalesce(e.rcv_oes_org_id, t.rcv_oes_org_id) as rcv_oes_org_id
             , e.created_by
             , e.created_date
             , e.modified_by
             , e.modified_date
          from table(business.rfv_trxn(:OP_CAT_CD, :TRXN_SEQ_ID)) t
          left join business.rf_trxn_extra e on e.op_cat_cd = t.op_cat_cd and e.fo_trxn_seq_id = t.trxn_seq_id
      }';
      v_bind_json.put('OP_CAT_CD', v_op_cat_cd);
      v_bind_json.put('TRXN_SEQ_ID', v_trxn_id);
      v_json := rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
    end if;
    
    return v_json;
  end;


  /* ������� ��������� ������� ��� ������� ����������� � ��������� (trxn �������)
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json
  is
    v_request    clob := null;
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
  begin
    /* ������ ���� � ������ ��� "T" */
    v_request := regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2');
    
    v_json_in := rf_json(v_request);
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');
    
    case
      when v_form_name = 'TrxnExtra' and v_action = 'read' then v_json_out := get_trxn_extra( v_json_in, p_id_request );
    end case;

    return v_json_out;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        return rf_json( rf_pkg_request.exec_exception( '��������� ���������� ������.'||chr(13)||sqlerrm() ) );
  end;

end;
/