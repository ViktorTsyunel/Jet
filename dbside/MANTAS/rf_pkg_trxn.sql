begin execute immediate 'drop TYPE mantas.RF_TAB_VARCHAR_PAIR'; exception when others then null; end;
/

create or replace TYPE mantas.RF_TYPE_VARCHAR_PAIR AS OBJECT
(
  col1  VARCHAR2(2000 CHAR),          
  col2  VARCHAR2(2000 CHAR)          
)
/

create or replace TYPE mantas.RF_TAB_VARCHAR_PAIR AS TABLE OF RF_TYPE_VARCHAR_PAIR
/

CREATE OR REPLACE 
PACKAGE MANTAS.RF_PKG_TRXN IS
--
-- ��������� �������� "�������� �������� �� ����" ��� ���������� ����������
-- � ���������� ����� �������� ��� ��������:
--  - ��������� ����� � ��� � ��������� ����� ����, ���� �������� ����� �� ����������
--  - ���������� ������������ ����� - ����������� ��������� ��� ���� (�������� ��� ��������������), 
--    ��������� ��� ���������� ���, ���������� ������ ������ �� "�������� ��������" ��� "������ ������"
-- � ������ ������������� �������� ����� ����������� �������������� ������, � �. �. ��� ������ �� ����� ��������
--
FUNCTION make_trxn_opok
(
  par_mode                in VARCHAR2,                       -- ����� ������: TRXN - �������� ��� ����������, SCRTY - ��� ������� ��������
  par_trxn_ids            in rf_tab_varchar,                 -- � ����������� �� ������ - ������ ��������������� �������� ��� ������ �����/������� - � ������ TRXN: <op_cat_cd>|<trxn_seq_id>|<review_id>, � ������ SCRTY: <op_cat_cd>|<trxn_scrty_seq_id>|<review_id>
  par_opok_nb             in kdd_review.rf_opok_nb%TYPE,     -- ��� ����, �� �������� ��������� ��������/������ ������ �������� ��������
  par_oes_date_s          in rf_oes_321.date_s%TYPE,         -- ���� ��������� �������� - ������������� � ��� � ���� DATE_S
  par_owner_seq_id        in kdd_review_owner.owner_seq_id%TYPE, -- ID ������������ - �������������� �� ��������� �����������/���������� �������
  par_override_owner_fl   in VARCHAR2 DEFAULT 'N',           -- ���� "�������� �������������� ��� ��������� ��������� ��� ���������� ��������" - Y/N
  par_skip_kgrko_fl       in VARCHAR2 DEFAULT 'N',           -- ���� "���������� (�� ������������) ������������� ��������" - Y/N
  par_kgrko_party_cd      in INTEGER,                        -- ������� - ������������� �������� �������� ��������:
                                                             --  0 - � ����� �������� �����, 1 - ������ � ������� ����� ����������� (������), 2 - ������ � ������� ����� ���������� (�������)
  par_note_tx             in kdd_note_hist.note_tx%TYPE,     -- ����������� � ��������� ���� ��� ������� ��������
  par_cmmnt_id            in kdd_cmmnt.cmmnt_id%TYPE,        -- ������������� ������������ ����������� ��� ������� ��������
  par_user_id             in kdd_review_owner.owner_id%TYPE, -- ����� �������� ������������, ������������ ������ ��������
  par_updated_trxn_ids    out rf_tab_varchar_pair            -- ������ ���: ������/����� ������������� �������� ��� ������ ������/������ (�������������� ���� <op_cat_cd>|<trxn_seq_id>|<review_id> ��� <op_cat_cd>|<trxn_scrty_seq_id>|<review_id>)
                                                             -- �������� �������������� ��� �������� ��� ������ �����/�������, ������� ���������� ��� ���������� ������� �������� � ������ ���� ��������� �� ����������
                                                             -- ������ ������������� � ���� ����� ���� ������, ���� ��������� ��������� �����, �������� �� ���� � par_trxn_ids (�� ��������������� �������� - ����)
                                                             -- ����� ������������� � ���� ����� ���������� �� ������� ����������� <review_id>
)
RETURN VARCHAR2;  -- ���������, ������� ������� ���������� �� ���������� ��� ������������ (���� NULL, �� ���������� ������ �� ����)
END RF_PKG_TRXN;
/

CREATE OR REPLACE 
PACKAGE BODY MANTAS.RF_PKG_TRXN IS
--
-- ��������� �������� "�������� �������� �� ����" ��� ���������� ����������
-- � ���������� ����� �������� ��� ��������:
--  - ��������� ����� � ��� � ��������� ����� ����, ���� �������� ����� �� ����������
--  - ���������� ������������ ����� - ����������� ��������� ��� ���� (�������� ��� ��������������), 
--    ��������� ��� ���������� ���, ���������� ������ ������ �� "�������� ��������" ��� "������ ������"
-- � ������ ������������� �������� ����� ����������� �������������� ������, � �. �. ��� ������ �� ����� ��������
--
FUNCTION make_trxn_opok
(
  par_mode                in VARCHAR2,                       -- ����� ������: TRXN - �������� ��� ����������, SCRTY - ��� ������� ��������
  par_trxn_ids            in rf_tab_varchar,                 -- � ����������� �� ������ - ������ ��������������� �������� ��� ������ �����/������� - � ������ TRXN: <op_cat_cd>|<trxn_seq_id>|<review_id>, � ������ SCRTY: <op_cat_cd>|<trxn_scrty_seq_id>|<review_id>
  par_opok_nb             in kdd_review.rf_opok_nb%TYPE,     -- ��� ����, �� �������� ��������� ��������/������ ������ �������� ��������
  par_oes_date_s          in rf_oes_321.date_s%TYPE,         -- ���� ��������� �������� - ������������� � ��� � ���� DATE_S
  par_owner_seq_id        in kdd_review_owner.owner_seq_id%TYPE, -- ID ������������ - �������������� �� ��������� �����������/���������� �������
  par_override_owner_fl   in VARCHAR2 DEFAULT 'N',           -- ���� "�������� �������������� ��� ��������� ��������� ��� ���������� ��������" - Y/N
  par_skip_kgrko_fl       in VARCHAR2 DEFAULT 'N',           -- ���� "���������� (�� ������������) ������������� ��������" - Y/N
  par_kgrko_party_cd      in INTEGER,                        -- ������� - ������������� �������� �������� ��������:
                                                             --  0 - � ����� �������� �����, 1 - ������ � ������� ����� ����������� (������), 2 - ������ � ������� ����� ���������� (�������)
  par_note_tx             in kdd_note_hist.note_tx%TYPE,     -- ����������� � ��������� ���� ��� ������� ��������
  par_cmmnt_id            in kdd_cmmnt.cmmnt_id%TYPE,        -- ������������� ������������ ����������� ��� ������� ��������
  par_user_id             in kdd_review_owner.owner_id%TYPE, -- ����� �������� ������������, ������������ ������ ��������
  par_updated_trxn_ids    out rf_tab_varchar_pair            -- ������ ���: ������/����� ������������� �������� ��� ������ ������/������ (�������������� ���� <op_cat_cd>|<trxn_seq_id>|<review_id> ��� <op_cat_cd>|<trxn_scrty_seq_id>|<review_id>)
                                                             -- �������� �������������� ��� �������� ��� ������ �����/�������, ������� ���������� ��� ���������� ������� �������� � ������ ���� ��������� �� ����������
                                                             -- ������ ������������� � ���� ����� ���� ������, ���� ��������� ��������� �����, �������� �� ���� � par_trxn_ids (�� ��������������� �������� - ����)
                                                             -- ����� ������������� � ���� ����� ���������� �� ������� ����������� <review_id>
)
RETURN VARCHAR2 IS  -- ���������, ������� ������� ���������� �� ���������� ��� ������������ (���� NULL, �� ���������� ������ �� ����)
  var_count             INTEGER;

  var_owner_org         kdd_review.owner_org%type;
  var_cmmnt_id          kdd_cmmnt.cmmnt_id%type;
  var_user_seq_id       kdd_review_owner.owner_seq_id%TYPE;
  
  var_inter_kgrko_cd    INTEGER;
  var_opok_nb           kdd_review.rf_opok_nb%TYPE;
  var_add_opoks_tx      kdd_review.rf_add_opoks_tx%TYPE;
  var_review1_id        kdd_review.review_id%TYPE;
  var_review2_id        kdd_review.review_id%TYPE;
  var_review_id         kdd_review.review_id%TYPE;
  var_oes_321_id        rf_oes_321.oes_321_id%TYPE;
  var_oes_check_status  VARCHAR2(1 CHAR);
  
  var_arh_flag          INTEGER;
  var_alert_done_flag   INTEGER;
  var_create_oes_flag   INTEGER;
  var_tmp_ids           mantas.rf_tab_number := mantas.rf_tab_number();

  var_allow_alert_edit_flag  INTEGER;
  var_wrong_status_flag      INTEGER;
  var_status_nm              VARCHAR2(255);

  var_trxn_count              INTEGER := 0; -- ���-�� ���������� (����������) ��������
  var_trxn_inter_count        INTEGER := 0; -- � �. �. �������������
  var_trxn_not_found_count    INTEGER := 0; -- ���-�� �� ��������� ��������
  var_trxn_canceled_count     INTEGER := 0; -- ���-�� ���������� ��������
  var_trxn_skip_inter_count   INTEGER := 0; -- ���-�� ����������� ������������� ��������
  var_trxn_done_count         INTEGER := 0; -- ���-�� ������������ ��������
 
  var_alert_create_count      INTEGER := 0; -- ���-�� ��������� �������
  var_alert_update_count      INTEGER := 0; -- ���-�� �������� �������
  var_alert_skip_opok_count   INTEGER := 0; -- ���-�� ����������� �������, ��� ���������� �� ���������� ����
  var_alert_skip_status_count INTEGER := 0; -- ���-�� ����������� �������, ������� ������ ������� �� ��������� ��������� ��������
  var_alert_skip_access_count INTEGER := 0; -- ���-�� ����������� ������� ��-�� ���������� ���� �������� �� � �������� ������������

  var_oes_create_count  INTEGER := 0; -- ���-�� ��������� ���
  var_oes_update_count  INTEGER := 0; -- ���-�� �������� ���
  var_oes_warn_count    INTEGER := 0; -- ���-�� ���, ��������� �������� � ����������������
  var_oes_err_count     INTEGER := 0; -- ���-�� ���, �� ��������� ��������
  
  var_msg               VARCHAR2(2000 CHAR);
  var_trxn_msg          VARCHAR2(2000 CHAR);
  var_err_msg           VARCHAR2(2000 CHAR);
  var_changed_rv_ids    mantas.rf_tab_number := rf_tab_number();
  var_res               rf_tab_varchar_pair := rf_tab_varchar_pair();

  prcname               varchar2(255) := ' (mantas.rf_pkg_trxn.make_trxn_opok)';

  -- ������������ ��������� ����� (� ���������� ����������)
  procedure restore_alert(par_review_id INTEGER) is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    var_tmp_ids.DELETE;
    var_tmp_ids.extend;
    var_tmp_ids(1) := par_review_id;
  
    rf_pkg_review.update_reviews(p_review_ids       => var_tmp_ids,
                                 p_actvy_type_cd    => 'RF_RESTORE',
                                 p_new_owner_seq_id => null,
                                 p_new_owner_org    => null,
                                 p_due_dt           => null,
                                 p_opok_nb          => null,
                                 p_add_opoks_tx     => null,
                                 p_owner_seq_id     => var_user_seq_id);
  
    rf_pkg_review.create_activities(p_review_ids    => var_tmp_ids,
                                    p_actvy_type_cd => 'RF_RESTORE',
                                    p_cmmnt_id      => null,
                                    p_note_tx       => '�������������� � ����� � ������� ������������� � ��������� (��� ���������� �������� "�������� �������� �� ����", ���: '||to_char(par_opok_nb)||')',
                                    p_owner_seq_id  => var_user_seq_id);
    COMMIT;
  end restore_alert;   
BEGIN
  --
  -- �������� ���������� ���������
  --
  -- ����� ������
  if par_mode is null or par_mode not in ('TRXN', 'SCRTY') Then
    raise_application_error(-20001, '�� ������ ��� ������ �������� ����� ������: '||to_char(par_mode)||'. ��������� ��������: TRXN, SCRTY'||prcname);
  end if;
  
  -- ������ �������� 
  if (par_trxn_ids is null or par_trxn_ids.count = 0) Then
    raise_application_error(-20001, '�� ������� �� ����� ��������'||prcname);
  end if;

  -- ��� ���� 
  SELECT count(*)
    INTO var_count
    FROM mantas.rf_opok
   WHERE opok_nb = par_opok_nb;

  if var_count = 0 then
    raise_application_error(-20001, '�� ������ ��� ������ �������������� ��� ���� ��������: '||to_char(par_opok_nb)||prcname);
  end if;

  -- ������� �������� �����, � ������� �������� �������� ������������� ��������
  if nvl(par_skip_kgrko_fl, 'N') <> 'Y' and par_kgrko_party_cd is null Then
    raise_application_error(-20001, '�� ������ ������� �������� ����� (�����������, ����������, ���), � ������� �������� �������� ������������� ��������. ��������� ��������: 0, 1, 2'||prcname);
  elsif par_kgrko_party_cd not in (0, 1, 2) Then
    raise_application_error(-20001, '������ �������� ������� �������� �����, � ������� �������� �������� ������������� ��������: '||to_char(par_kgrko_party_cd)||'. ��������� ��������: 0, 1, 2'||prcname);
  end if;
  
  -- �������������
  if par_owner_seq_id is null Then
    raise_application_error(-20001, '�� ������ �������������'||prcname);
  end if;
 
  SELECT count(*), max(rptg_group_cd)
    INTO var_count, var_owner_org
    FROM mantas.kdd_review_owner
   WHERE owner_seq_id = par_owner_seq_id;

  if var_count = 0 then
    raise_application_error(-20001, '������ �������������� �������������: '||to_char(par_owner_seq_id)||prcname);
  end if;

  -- ���� "�������� �������������� ��� ��������� ��������� ��� ���������� ��������"
  if par_override_owner_fl is null or par_override_owner_fl not in ('Y', 'N') Then
    raise_application_error(-20001, '�� ������ ��� ������ �������� ���� "�������� �������������� ��� ��������� ��������� ��� ���������� ��������": '||par_override_owner_fl||'. ��������� ��������: Y, N'||prcname);
  end if;
  
  -- ���� "���������� ������������� ��������"
  if par_skip_kgrko_fl not in ('Y', 'N') Then
    raise_application_error(-20001, '������ �������� ���� "���������� ������������� ��������": '||par_skip_kgrko_fl||'. ��������� ��������: Y, N'||prcname);
  end if;

  -- id ������������ �����������
  if par_cmmnt_id is not null then
    SELECT count(*)
      INTO var_count
      FROM mantas.kdd_cmmnt
     WHERE cmmnt_id = par_cmmnt_id;
  
    if var_count = 0 then
      raise_application_error(-20001, '������ �������������� ������������� ������������ �����������: '||to_char(par_cmmnt_id)||prcname);
    end if;    
  end if;

  -- ID �������� ������������
  select max(owner_seq_id)
    into var_user_seq_id
    from mantas.kdd_review_owner
   where owner_id = par_user_id;

  if var_user_seq_id is null then
    raise_application_error(-20001, '�� ������ ��� ������ �������������� ������� ������������: '||par_user_id||prcname);
  end if;
  --
  -- ��������, ��� � �������� ������������ ���� ����� �� ���������� �������� "�������� �������� �� ����"
  --
  if nvl(rf_pkg_adm.has_activity(par_user_id, 'RF_OES$'), 0) <> 1 Then
    raise_application_error(-20001, '��� ���� �� ���������� ��������: "�������� �������� �� ����". �������� �� ���������!'||prcname);
  end if;
  --
  -- ���� �� ��������� ��������� ��� ������ ������� (�� �������), ��� ������ �������� �������� ������ ��������� �������
  -- (�� ������������� �������� ����� ���� ����� ��� ������)
  --
  FOR r IN (SELECT t.*,
                   case when t.kgrko_override_flag = 1
                        then mantas.rf_pkg_kgrko.override_branch_id(t.trxn_debit_branch_id, t.send_oes_org_id, trunc(t.trxn_exctn_dt)) 
                        else t.trxn_debit_branch_id
                   end as debit_branch_id,         
                   case when t.kgrko_override_flag = 1
                        then mantas.rf_pkg_kgrko.override_branch_id(t.trxn_credit_branch_id, t.rcv_oes_org_id, trunc(t.trxn_exctn_dt)) 
                        else t.trxn_credit_branch_id
                   end as credit_branch_id,         
                   mantas.rf_pkg_kgrko.is_inter_kgrko(t.trxn_debit_branch_id, t.trxn_credit_branch_id, trunc(t.trxn_exctn_dt),
                                                      t.kgrko_override_flag, t.send_oes_org_id, t.rcv_oes_org_id) as is_inter_kgrko,
                   'TRXN' as object_tp_cd,
                   count(*) over() as trxn_count
              FROM (select /*+ LEADING(t) USE_NL(wt ct bot trxe) INDEX(wt PK_WIRE_TRXN) INDEX(ct PK_CASH_TRXN) INDEX(bot PK_BO_TRXN) INDEX(trxe RF_TRXN_EXTRA_PK)*/ 
                           substr(t.column_value, 1, instr(t.column_value, '|', -1) - 1)      as trxn_input_id,
                           regexp_substr(t.column_value, '[^|]+', 1, 1)                       as op_cat_cd,
                           coalesce(wt.fo_trxn_seq_id, ct.fo_trxn_seq_id, bot.bo_trxn_seq_id) as trxn_seq_id, 
                           to_number(null)                                                    as trxn_scrty_seq_id,                   
                           coalesce(wt.trxn_exctn_dt,  ct.trxn_exctn_dt,  bot.exctn_dt)       as trxn_exctn_dt,                   
                           coalesce(wt.trxn_base_am, ct.trxn_base_am,  bot.trxn_base_am)      as trxn_base_am,                   
                           to_number(null)                                                    as scrty_base_am,   
                           coalesce(wt.data_dump_dt, ct.data_dump_dt, bot.data_dump_dt)       as data_dump_dt,                
                           coalesce(wt.rf_canceled_fl, ct.rf_canceled_fl, bot.rf_canceled_fl) as trxn_canceled_fl,
                           coalesce(wt.rf_branch_id,   ct.rf_branch_id,   bot.rf_branch_id)   as branch_id,
                           case when wt.fo_trxn_seq_id is not null
                                then business.rf_pkg_util.get_acct_branch_id(wt.rf_orig_acct_seq_id, wt.rf_orig_acct_nb, wt.send_instn_id,
                                                                             wt.rf_orig_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd) 
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                             ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd)
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����?
                                when bot.bo_trxn_seq_id is not null and bot.dbt_cdt_cd = 'D'
                                then business.rf_pkg_util.get_acct_branch_id(bot.rf_acct_seq_id, null, 'SBRF',
                                                                             null, bot.exctn_dt, bot.src_sys_cd)
                           end as trxn_debit_branch_id,                          
                           case when wt.fo_trxn_seq_id is not null
                                then business.rf_pkg_util.get_acct_branch_id(wt.rf_benef_acct_seq_id, wt.rf_benef_acct_nb, wt.rcv_instn_id,
                                                                             wt.rf_benef_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����? 
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                             ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd) 
                                when bot.bo_trxn_seq_id is not null and bot.dbt_cdt_cd = 'C'
                                then business.rf_pkg_util.get_acct_branch_id(bot.rf_acct_seq_id, null, 'SBRF',
                                                                             null, bot.exctn_dt, bot.src_sys_cd)
                           end as trxn_credit_branch_id,
                           case when trxe.rowid is not null
                                then 1
                                else 0
                           end as kgrko_override_flag,
                           trxe.send_oes_org_id,
                           trxe.rcv_oes_org_id,  
                           listagg(nvl(regexp_substr(t.column_value, '[^|]+', 1, 3), '<null>'), ',') within group(order by t.column_value) as review_ids,                                                          
                           --cast(collect(nvl(regexp_substr(t.column_value, '[^|]+', 1, 3), '<null>')) as mantas.rf_tab_varchar) as review_ids,                   
                           case when sum(decode(regexp_substr(t.column_value, '[^|]+', 1, 3), null, 1)) > 0
                                then 1
                                else 0
                           end null_review_id_flag                                                      
                      from TABLE(par_trxn_ids) t
                           /*(select 'CT|10750467|14103' as column_value from dual
                            union all
                            select 'CT|10750467|14103' as column_value from dual
                            union all
                            select 'CT|10750463|14102' as column_value from dual
                            union all
                            select 'WT|8979263|12632' from dual
                            union all
                            select 'WT|8432599|11566' from dual
                            union all
                            select 'BOT|12007565|' from dual
                            ) t*/
                           left join business.wire_trxn wt on regexp_substr(t.column_value, '[^|]+', 1, 1) = 'WT' and
                                                              wt.fo_trxn_seq_id = to_number(regexp_substr(t.column_value, '[^|]+', 1, 2))
                           left join business.cash_trxn ct on regexp_substr(t.column_value, '[^|]+', 1, 1) = 'CT' and
                                                              ct.fo_trxn_seq_id = to_number(regexp_substr(t.column_value, '[^|]+', 1, 2))
                           left join business.back_office_trxn bot on regexp_substr(t.column_value, '[^|]+', 1, 1) = 'BOT' and
                                                                      bot.bo_trxn_seq_id = to_number(regexp_substr(t.column_value, '[^|]+', 1, 2))
                           left join business.rf_trxn_extra trxe ON trxe.op_cat_cd = regexp_substr(t.column_value, '[^|]+', 1, 1) and 
                                                                    trxe.fo_trxn_seq_id = to_number(regexp_substr(t.column_value, '[^|]+', 1, 2)) 
                    group by substr(t.column_value, 1, instr(t.column_value, '|', -1) - 1),
                             regexp_substr(t.column_value, '[^|]+', 1, 1),
                             coalesce(wt.fo_trxn_seq_id, ct.fo_trxn_seq_id, bot.bo_trxn_seq_id),                   
                             coalesce(wt.trxn_exctn_dt,  ct.trxn_exctn_dt,  bot.exctn_dt),
                             coalesce(wt.trxn_base_am, ct.trxn_base_am,  bot.trxn_base_am),
                             coalesce(wt.data_dump_dt, ct.data_dump_dt, bot.data_dump_dt),                   
                             coalesce(wt.rf_canceled_fl, ct.rf_canceled_fl, bot.rf_canceled_fl),
                             coalesce(wt.rf_branch_id,   ct.rf_branch_id,   bot.rf_branch_id),
                             case when wt.fo_trxn_seq_id is not null
                                then business.rf_pkg_util.get_acct_branch_id(wt.rf_orig_acct_seq_id, wt.rf_orig_acct_nb, wt.send_instn_id,
                                                                             wt.rf_orig_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd) 
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                             ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd)
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����?
                                when bot.bo_trxn_seq_id is not null and bot.dbt_cdt_cd = 'D'
                                then business.rf_pkg_util.get_acct_branch_id(bot.rf_acct_seq_id, null, 'SBRF',
                                                                             null, bot.exctn_dt, bot.src_sys_cd)
                           end,                          
                           case when wt.fo_trxn_seq_id is not null
                                then business.rf_pkg_util.get_acct_branch_id(wt.rf_benef_acct_seq_id, wt.rf_benef_acct_nb, wt.rcv_instn_id,
                                                                             wt.rf_benef_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����? 
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                             ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd) 
                                when bot.bo_trxn_seq_id is not null and bot.dbt_cdt_cd = 'C'
                                then business.rf_pkg_util.get_acct_branch_id(bot.rf_acct_seq_id, null, 'SBRF',
                                                                             null, bot.exctn_dt, bot.src_sys_cd)
                           end,
                           case when trxe.rowid is not null
                                then 1
                                else 0
                           end,
                           trxe.send_oes_org_id,
                           trxe.rcv_oes_org_id) t                          
             WHERE par_mode = 'TRXN'
            UNION ALL
            SELECT t.*,
                   case when t.kgrko_override_flag = 1
                        then mantas.rf_pkg_kgrko.override_branch_id(t.trxn_debit_branch_id, t.send_oes_org_id, trunc(t.trxn_exctn_dt)) 
                        else t.trxn_debit_branch_id
                   end as debit_branch_id,         
                   case when t.kgrko_override_flag = 1
                        then mantas.rf_pkg_kgrko.override_branch_id(t.trxn_credit_branch_id, t.rcv_oes_org_id, trunc(t.trxn_exctn_dt)) 
                        else t.trxn_credit_branch_id
                   end as credit_branch_id,         
                   mantas.rf_pkg_kgrko.is_inter_kgrko(t.trxn_debit_branch_id, t.trxn_credit_branch_id, trunc(t.trxn_exctn_dt),
                                                      t.kgrko_override_flag, t.send_oes_org_id, t.rcv_oes_org_id) as is_inter_kgrko,
                   case when t.op_cat_cd = 'WT' and t.trxn_seq_id is not null and t.trxn_base_am = t.scrty_base_am and 
                             1 = (select count(*) 
                                    from business.rf_wire_trxn_scrty wts 
                                   where wts.fo_trxn_seq_id = t.trxn_seq_id and
                                         nvl(wts.canceled_fl, 'N') <> 'Y') -- �������� "�������" ����� �� ����� ������ ������
                        then 'TRXN_SCRTY_JOINT' 
                        when t.op_cat_cd = 'CT' and t.trxn_seq_id is not null and t.trxn_base_am = t.scrty_base_am and 
                             1 = (select count(*) 
                                    from business.rf_cash_trxn_scrty cts 
                                   where cts.fo_trxn_seq_id = t.trxn_seq_id and
                                         nvl(cts.canceled_fl, 'N') <> 'Y') 
                        then 'TRXN_SCRTY_JOINT' 
                        else 'TRXN_SCRTY'
                   end as object_tp_cd,                                                 
                   count(*) over() as trxn_count
              FROM (select /*+ LEADING(t) USE_NL(wts wt cts ct trxe) INDEX(wts RF_WIRE_TRXN_SCRTY_PK) INDEX(wt PK_WIRE_TRXN) INDEX(cts RF_CASH_TRXN_SCRTY_PK) INDEX(ct PK_CASH_TRXN) INDEX(trxe RF_TRXN_EXTRA_PK)*/ 
                           substr(t.column_value, 1, instr(t.column_value, '|', -1) - 1)      as trxn_input_id,
                           regexp_substr(t.column_value, '[^|]+', 1, 1)                       as op_cat_cd,
                           coalesce(wts.fo_trxn_seq_id, cts.fo_trxn_seq_id)                   as trxn_seq_id, 
                           coalesce(wts.trxn_scrty_seq_id, cts.trxn_scrty_seq_id)             as trxn_scrty_seq_id,                   
                           coalesce(wt.trxn_exctn_dt,  ct.trxn_exctn_dt)                      as trxn_exctn_dt,                   
                           coalesce(wt.trxn_base_am, ct.trxn_base_am)                         as trxn_base_am,                   
                           coalesce(wts.scrty_base_am, cts.scrty_base_am)                     as scrty_base_am, 
                           coalesce(wt.data_dump_dt, ct.data_dump_dt)                         as data_dump_dt,                  
                           coalesce(wts.canceled_fl, cts.canceled_fl, wt.rf_canceled_fl,  ct.rf_canceled_fl) as trxn_canceled_fl,
                           coalesce(wt.rf_branch_id,   ct.rf_branch_id)                       as branch_id,
                           case when wt.fo_trxn_seq_id is not null
                                then business.rf_pkg_util.get_acct_branch_id(wt.rf_orig_acct_seq_id, wt.rf_orig_acct_nb, wt.send_instn_id,
                                                                             wt.rf_orig_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd) 
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                             ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd)
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����?
                           end as trxn_debit_branch_id,                          
                           case when wt.fo_trxn_seq_id is not null
                                then business.rf_pkg_util.get_acct_branch_id(wt.rf_benef_acct_seq_id, wt.rf_benef_acct_nb, wt.rcv_instn_id,
                                                                             wt.rf_benef_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����? 
                                when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                             ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd) 
                           end as trxn_credit_branch_id,
                           case when trxe.rowid is not null
                                then 1
                                else 0
                           end as kgrko_override_flag,
                           trxe.send_oes_org_id,
                           trxe.rcv_oes_org_id,                                                                               
                           listagg(nvl(regexp_substr(t.column_value, '[^|]+', 1, 3), '<null>'), ',') within group(order by t.column_value) as review_ids,                                                          
                           --cast(collect(nvl(regexp_substr(t.column_value, '[^|]+', 1, 3), '<null>')) as mantas.rf_tab_varchar) as review_ids,
                           case when sum(decode(regexp_substr(t.column_value, '[^|]+', 1, 3), null, 1)) > 0
                                then 1
                                else 0
                           end null_review_id_flag                                                      
                      from TABLE(par_trxn_ids) t
                           /*(select 'CT|14|14097' as column_value from dual
                            union all
                            select 'CT|16|14098' as column_value from dual
                            union all
                            select 'CT|10750463|14102' as column_value from dual
                            union all
                            select 'WT|8979263|12632' from dual
                            ) t*/
                           left join business.rf_wire_trxn_scrty wts on regexp_substr(t.column_value, '[^|]+', 1, 1) = 'WT' and
                                                                        wts.trxn_scrty_seq_id = to_number(regexp_substr(t.column_value, '[^|]+', 1, 2))
                           left join business.wire_trxn wt on wt.fo_trxn_seq_id = wts.fo_trxn_seq_id
                           left join business.rf_cash_trxn_scrty cts on regexp_substr(t.column_value, '[^|]+', 1, 1) = 'CT' and
                                                                        cts.trxn_scrty_seq_id = to_number(regexp_substr(t.column_value, '[^|]+', 1, 2))
                           left join business.cash_trxn ct on ct.fo_trxn_seq_id = cts.fo_trxn_seq_id
                           left join business.rf_trxn_extra trxe ON trxe.op_cat_cd = regexp_substr(t.column_value, '[^|]+', 1, 1) and 
                                                                    trxe.fo_trxn_seq_id = coalesce(wt.fo_trxn_seq_id, ct.fo_trxn_seq_id) 
                    group by substr(t.column_value, 1, instr(t.column_value, '|', -1) - 1),
                             regexp_substr(t.column_value, '[^|]+', 1, 1),
                             coalesce(wts.fo_trxn_seq_id, cts.fo_trxn_seq_id), 
                             coalesce(wts.trxn_scrty_seq_id, cts.trxn_scrty_seq_id),                   
                             coalesce(wt.trxn_exctn_dt,  ct.trxn_exctn_dt),                   
                             coalesce(wt.trxn_base_am, ct.trxn_base_am),                   
                             coalesce(wts.scrty_base_am, cts.scrty_base_am),
                             coalesce(wt.data_dump_dt, ct.data_dump_dt),                   
                             coalesce(wts.canceled_fl, cts.canceled_fl, wt.rf_canceled_fl,  ct.rf_canceled_fl),
                             coalesce(wt.rf_branch_id,   ct.rf_branch_id),
                             case when wt.fo_trxn_seq_id is not null
                                  then business.rf_pkg_util.get_acct_branch_id(wt.rf_orig_acct_seq_id, wt.rf_orig_acct_nb, wt.send_instn_id,
                                                                               wt.rf_orig_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd) 
                                  when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                  then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                               ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd)
                                  when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                  then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����?
                             end,                          
                             case when wt.fo_trxn_seq_id is not null
                                  then business.rf_pkg_util.get_acct_branch_id(wt.rf_benef_acct_seq_id, wt.rf_benef_acct_nb, wt.rcv_instn_id,
                                                                               wt.rf_benef_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)
                                  when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'D'
                                  then to_number(null) -- ����� �� ����� ���� ������ �����, ������������ �� ������� �����? 
                                  when ct.fo_trxn_seq_id is not null and ct.dbt_cdt_cd = 'C'
                                  then business.rf_pkg_util.get_acct_branch_id(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF',
                                                                               ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd) 
                             end,
                             case when trxe.rowid is not null
                                  then 1
                                  else 0
                             end,
                             trxe.send_oes_org_id,
                             trxe.rcv_oes_org_id                                                                               
                    ) t
             WHERE par_mode = 'SCRTY'    
           ) LOOP                          

    SAVEPOINT make_trxn_opok; -- ����� ������ ��������� ��������� �������� - �� ���, ���� ���, ����� ������������
    BEGIN
      if var_trxn_count = 0 Then
        var_trxn_count := r.trxn_count;
      end if;  

      if r.is_inter_kgrko = 1 Then
        var_trxn_inter_count := var_trxn_inter_count + 1;
      end if;  
      --
      -- ���� ��������� ��������/������ ������ �� ������� - ��������� � ���������
      --
      if r.trxn_seq_id is null Then
        var_trxn_not_found_count := var_trxn_not_found_count + 1;
        GOTO NEXT_TRXN;
      end if;
      --
      -- ���� ��������� ��������/������ ������ �������� - ��������� � ���������
      --
      if r.trxn_canceled_fl = 'Y' Then
        var_trxn_canceled_count := var_trxn_canceled_count + 1;
        GOTO NEXT_TRXN;
      end if;
      --
      -- ���� ��������� �������� ������������� � ���������� ���� �������� ������������� �������� - 
      -- ��������� � ���������
      --    
      if r.is_inter_kgrko = 1 and par_skip_kgrko_fl = 'Y' Then
        var_trxn_skip_inter_count := var_trxn_skip_inter_count + 1;
        GOTO NEXT_TRXN;
      end if;  
      --
      -- ���������: ����� ������/��� �� �������� ������ ���� � ���������� ������� �������� � ����� �� ��� ��� ����
      -- �������� ��������� �������� ������ �� ������ "����� ������ ������ ����":
      --  1) ��� ������ �� ������������� ��������, ���������� �������� � ����� �������� ����� - ������ � rf_inter_kgrko_cd = 1, 2
      --  2) ���� �� ������� �� ������������� �������� - ����� � rf_inter_kgrko_cd = 1 ��� 2
      --  3) ���� ����� �� ������� �������� (���������������) - ����� � rf_inter_kgrko_cd is null
      --
      -- ����� ��� "�������������" ������
      if r.is_inter_kgrko = 1 and par_kgrko_party_cd = 0 Then -- �������� ������������� � ������������ ������, ��� ������������� �������� �������� �������� � ���� ��������
        var_inter_kgrko_cd := 3;    
      -- ����� ���� "�������������" ����� �� �����������
      elsif r.is_inter_kgrko = 1 and par_kgrko_party_cd = 1 Then 
        var_inter_kgrko_cd := 1;    
      -- ����� ���� "�������������" ����� �� ����������
      elsif r.is_inter_kgrko = 1 and par_kgrko_party_cd = 2 Then 
        var_inter_kgrko_cd := 2;    
      else  
        var_inter_kgrko_cd := 0;    
      end if;    
      --
      -- ����������: ���� �� ��� ������ �� ��������/������ ������, 
      -- ��� ������������� �������� ����������: ����� ����� �� ������� �����������, ����� - �� ������� ����������
      --
      mantas.rf_pkg_kgrko.get_existing_alerts(var_inter_kgrko_cd, r.op_cat_cd, r.trxn_seq_id, r.trxn_scrty_seq_id, var_review1_id, var_review2_id);
      --
      -- ���� �� �������/���, ������� ���� �������/�������� - ���� ��� ��� ��������
      --
      var_arh_flag := 0;        -- ���� ������������� ��������� ��������
      var_alert_done_flag := 0; -- ����, ��� �����/������ �� �������� �������/�������� (� �� ���������)
      var_trxn_msg := null;
      var_changed_rv_ids.DELETE;
      FOR r2 IN (SELECT rv.review_id,
                        t.kgrko_party_cd,
                        rv.rf_data_dump_dt, 
                        -- ����, ��� ������ ����� ���� ���������������
                        case when rv.status_cd like '%-' or rv.status_cd like '%=' -- ����� ������/� �������� �������� (� ���������� ������ ��������)
                             then 1
                             else 0
                        end as restore_flag, 
                        -- ����, ��� ������� ������ ������ �� ��������� ��������� ��������� ��������
                        case when rv.rowid is not null and act_st.rowid is null
                             then 1
                             else 0
                        end as wrong_status_flag, 
                        -- ���� ������� � �������� ������������ ���� �������� ������ �����
                        case when rv.rowid is not null
                             then mantas.rf_pkg_adm.allow_alert_editing(par_user_id, rv.status_cd) 
                        end as allow_alert_edit_flag,                                   
                        -- ����, ��� ��������� ��� ���� ���� �� ��������� � ������������, � �������� �� ������������ � ������/��� ���� ����       
                        case when rv.status_cd in ('RF_CANCEL', 'RF_CANCEL+', 'RF_CREADY+') -- ����� �������/����� ������� ����� �������� ���������
                             then 1
                             else 0
                        end as override_opok_flag, 
                        -- ����, ��� ��������� ��� ���� ��� ���� � ���/������      
                        case when oes.oes_321_id is not null and
                                  replace(','||to_char(oes.vo)||','||replace(oes.dop_v, ' ')||',', ' ') like '%,'||to_char(par_opok_nb)||',%'
                             then 1
                             when rv.review_id is not null and
                                  replace(','||to_char(rv.rf_opok_nb)||','||replace(rv.rf_add_opoks_tx, ' ')||',', ' ') like '%,'||to_char(par_opok_nb)||',%'
                             then 1
                             else 0
                        end as opok_exists_flag,         
                        -- ACTION, ������� ������ ���� � ���
                        case when rv.status_cd = 'RF_DONE' or rv.status_cd like '%+' or rv.status_cd like '%=' -- ����� � ������� ����� �������� ���������
                             then '3' -- ������ ������
                             else '1' -- ��������� ���������
                        end as action, 
                        -- ����� ������ ����� ���� (���� ��� ��������: ��������, ��������������)
                        case when oes.oes_321_id is not null 
                             then trim(',' from to_char(oes.vo)||
                                                case when nullif(replace(oes.dop_v, ' '), '0') is not null
                                                     then ','||nullif(replace(oes.dop_v, ' '), '0')
                                                end||','||to_char(par_opok_nb))
                             when rv.review_id is not null
                             then trim(',' from to_char(rv.rf_opok_nb)||
                                                case when replace(rv.rf_add_opoks_tx, ' ') is not null
                                                     then ','||replace(rv.rf_add_opoks_tx, ' ')
                                                end||','||to_char(par_opok_nb))
                        end as new_opoks,
                        -- ������������� �� ���������� �����
                        case when rv.review_id is not null and rv.rf_wrk_flag = 1 and nvl(par_override_owner_fl, 'N') = 'N'  
                             then to_number(null) -- �� �������� �������������� � ��� ������������� ���������� ��������� ������
                             else par_owner_seq_id
                        end as owner_seq_id,                               
                        -- ��/��� ��� ������������ ������ - � ������������ �� �������� (����������/����������)
                        case when t.kgrko_party_cd = 1
                             then coalesce(r.debit_branch_id, r.branch_id)
                             when t.kgrko_party_cd = 2
                             then coalesce(r.credit_branch_id, r.branch_id)
                             else r.branch_id
                        end as branch_id,        
                        rv.rf_kgrko_party_cd,
                        oes.oes_321_id,
                        st.code_disp_tx as status_nm
                   FROM -- 1 ��� 2 �������, ��������������� ������ ��� ���� ����������� �������
                        (select case when var_inter_kgrko_cd = 3
                                     then level 
                                     when var_inter_kgrko_cd = 0
                                     then to_number(null)
                                     else var_inter_kgrko_cd
                                end as kgrko_party_cd,
                                case when var_inter_kgrko_cd = 3 and level = 1
                                     then var_review1_id 
                                     when var_inter_kgrko_cd = 3 and level = 2
                                     then var_review2_id 
                                     when var_inter_kgrko_cd in (1, 0)
                                     then var_review1_id
                                     when var_inter_kgrko_cd = 2
                                     then var_review2_id 
                                end as review_id                                                            
                           from dual 
                         connect by level <= case when var_inter_kgrko_cd = 3 
                                                  then 2 
                                                  else 1 
                                             end) t
                        /*(select 1 as kgrko_party_cd,
                                11566 as review_id
                           from dual
                         union all
                         select 2 as kgrko_party_cd,
                                to_number(null) as review_id
                           from dual) t*/                                                                            
                        left join mantas.kdd_review rv on rv.review_id = t.review_id
                        left join mantas.rf_oes_321 oes on oes.review_id = rv.review_id and
                                                           oes.current_fl = 'Y' and
                                                           nvl(oes.send_fl, 'N') <> 'Y' and
                                                           nvl(oes.deleted_fl, 'N') <> 'Y'
                        left join mantas.kdd_actvy_type_review_status act_st on act_st.actvy_type_cd = 'RF_OES$' and -- ��������: �������� �������� �� ����
                                                                                act_st.status_cd = rv.status_cd 
                        left join mantas.kdd_code_set_trnln st on st.code_set = 'AlertStatus' and 
                                                                  st.code_val = rv.status_cd                                                      
                ) LOOP

        var_create_oes_flag := 0; -- ���� ������������� �������� ���
        var_review_id := null;    -- �����, �� �������� ���������� ������� ���
        var_status_nm := r2.status_nm;
        --
        -- ���� ����� ��� ������ (��� � �������� ��������), �� ���������� ��� ��������������
        -- (��� �������� ��������, �. �. �������� �� ������� - ���������� ��)
        -- ������ ��� � ���������� ����������
        --
        if r2.restore_flag = 1 Then
          restore_alert(r2.review_id);

          -- ������� ����� ������ ������
          SELECT mantas.rf_pkg_adm.allow_alert_editing(par_user_id, rv.status_cd) as allow_alert_edit_flag, 
                 case when rv.rowid is not null and act_st.rowid is null
                      then 1
                      else 0
                 end as wrong_status_flag,
                 st.code_disp_tx as status_nm
            INTO var_allow_alert_edit_flag, 
                 var_wrong_status_flag, 
                 var_status_nm
            FROM mantas.kdd_review rv
                 left join mantas.kdd_actvy_type_review_status act_st on act_st.actvy_type_cd = 'RF_OES$' and
                                                                         act_st.status_cd = rv.status_cd
                 left join mantas.kdd_code_set_trnln st on st.code_set = 'AlertStatus' and 
                                                           st.code_val = rv.status_cd                                                        
           WHERE review_id = r2.review_id;
        end if;
        --
        -- ������������� ���� ������������� ��������� ��������, ���� ����� ������� � ������ �� ��������� � ������ ������� � ��������
        --
        if r2.rf_data_dump_dt <> r.data_dump_dt Then
          var_arh_flag := 1;
        end if;
        --
        -- ���� ��������� ��� ���� � ������ ��� ���� (� �� ������ ���� �������������) - ���������� �����
        --
        if r2.opok_exists_flag = 1 and r2.override_opok_flag <> 1 Then
          var_alert_skip_opok_count := var_alert_skip_opok_count + 1;
          
          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' ��������� �������� �'||to_char(r2.review_id)||
                                          ' - �������� ��� �������� �� ���� '||to_char(par_opok_nb)||'.';
          end if;  
          
          GOTO NEXT_ALERT;
        end if;  
        --
        -- ���� ������� ������ ������ �� ��������� ��������� �������� - ���������� �����
        --
        if (r2.restore_flag = 0 and r2.wrong_status_flag = 1) or
           (r2.restore_flag = 1 and var_wrong_status_flag = 1) Then
          var_alert_skip_status_count := var_alert_skip_status_count + 1;

          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' ��������� �������� �'||to_char(r2.review_id)||
                                          ' - �������� "�������� �������� �� ����" �� ��������� ��� �������� � �������: '||var_status_nm||'.';
          end if;  

          GOTO NEXT_ALERT;
        end if;
        --
        -- ���� � �������� ������������ ��� ���� �������� ����� � ��� ������� ������� - ���������� �����
        --  
        if (r2.restore_flag = 0 and r2.allow_alert_edit_flag <> 1) or
           (r2.restore_flag = 1 and var_allow_alert_edit_flag <> 1) Then
          var_alert_skip_access_count := var_alert_skip_access_count + 1;

          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' ��������� �������� �'||to_char(r2.review_id)||
                                          ' - ������������ ����, ����� ��������� �������� ��� ��������� � �������: '||var_status_nm||'.';
          end if;  

          GOTO NEXT_ALERT;
        end if;
        --
        -- ��������� ����� �������� ��� ��������� � �������������� ����� ����
        --  
        if r2.override_opok_flag = 1 or r2.review_id is null Then
          var_opok_nb := par_opok_nb;
          var_add_opoks_tx := null;
        else
          SELECT opok_nb, add_opoks_tx
            INTO var_opok_nb, var_add_opoks_tx
            FROM (select max(first_opok_nb) as opok_nb,
                         listagg(nullif(to_number(opok), first_opok_nb), ',') within group (order by priority_nb) as add_opoks_tx
                    from (select distinct
                                 t.column_value as opok,
                                 nvl(opk.priority_nb, opk.opok_nb) as priority_nb,
                                 first_value(opk.opok_nb) over(order by nvl(opk.priority_nb, opk.opok_nb)) as first_opok_nb
                            from table(mantas.rf_pkg_scnro.list_to_tab(r2.new_opoks)) t
                                 join mantas.rf_opok opk on opk.opok_nb = to_number(t.column_value)));
        end if;
        --
        -- ���� ������ ��� ��� - �������� ���, ������ ����� � ������� "�������� ��������" (RF_OES)
        --
        if r2.review_id is null Then
          var_review_id := rf_pkg_review.create_review(par_object_tp_cd => r.object_tp_cd,
                                                       par_op_cat_cd => r.op_cat_cd,
                                                       par_trxn_seq_id =>  r.trxn_seq_id,
                                                       par_trxn_scrty_seq_id => r.trxn_scrty_seq_id,
                                                       par_opok_list => null,
                                                       par_data_dump_dt => r.data_dump_dt,
                                                       par_branch_id => r2.branch_id,
                                                       par_owner_seq_id => par_owner_seq_id,
                                                       par_owner_org => var_owner_org,
                                                       -- ����, �� ������� ������������� ���� ��������� ������ (3 ���. ���)                                                     
                                                       par_trxn_dt => case when var_opok_nb in (6001, 8001, 5003, 5007) -- ����, �� ������� ���� ������������� �� ���� ���������
                                                                           then nvl(par_oes_date_s, trunc(sysdate))
                                                                           else r.trxn_exctn_dt
                                                                      end,
                                                       par_creat_id => var_user_seq_id,
                                                       par_prcsng_batch_cmplt_fl => 'Y',
                                                       par_actvy_type_cd => 'RF_OES$',
                                                       par_note_tx => par_note_tx||case when par_note_tx is not null then '; ' end||'��������: �������� �������� �� ���� '||to_char(par_opok_nb),
                                                       par_cmmnt_id => par_cmmnt_id,
                                                       par_kgrko_party_cd => r2.kgrko_party_cd,
                                                       par_opok_nb        => var_opok_nb,
                                                       par_add_opoks_tx   => var_add_opoks_tx,
                                                       par_status_cd      => 'RF_OES',
                                                       par_non_opok_fl    => 'M');
          var_alert_create_count := var_alert_create_count + 1;
          -- 
          -- ������������� �����: ������������� �������� ������ ���, ������������� ��������� ��������
          --
          var_create_oes_flag := 1;        
          var_arh_flag := 1;
          
          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' ������� �������� �'||to_char(var_review_id)||'.';
          end if;  
        else
          --
          -- ������� �������� ������ - ��������, ���. ��� ����, �������������, ������
          --
          var_tmp_ids.DELETE;
          var_tmp_ids.extend;
          var_tmp_ids(1) := r2.review_id;
  
          rf_pkg_review.update_reviews(p_review_ids       => var_tmp_ids,
                                       p_actvy_type_cd    => 'RF_OES$',
                                       p_new_owner_seq_id => r2.owner_seq_id,
                                       p_new_owner_org    => case when r2.owner_seq_id is not null then var_owner_org end,
                                       p_due_dt           => null,
                                       p_opok_nb          => var_opok_nb, 
                                       p_add_opoks_tx     => var_add_opoks_tx,
                                       p_owner_seq_id     => var_user_seq_id);
 
          rf_pkg_review.create_activities(p_review_ids    => var_tmp_ids,
                                          p_actvy_type_cd => 'RF_OES$',
                                          p_cmmnt_id      => par_cmmnt_id,
                                          p_note_tx       => par_note_tx||case when par_note_tx is not null then '; ' end||'��������: �������� �������� �� ���� '||to_char(par_opok_nb),
                                          p_owner_seq_id  => var_user_seq_id);        
          var_alert_update_count := var_alert_update_count + 1;

          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' �������� �������� �'||to_char(r2.review_id)||'.';
          end if;  
          -- 
          -- ������� �������� ��� (���� �� ���� � �� ���������)
          --
          if r2.oes_321_id is not null Then
            UPDATE mantas.rf_oes_321 
               SET action = r2.action,
                   vo = var_opok_nb,
                   dop_v = nvl(var_add_opoks_tx, '0'),
                   date_s = case when par_oes_date_s is not null and 
                                      par_oes_date_s <> to_date('01.01.2099','dd.mm.yyyy') and
                                      var_opok_nb in (6001, 8001, 5003, 5007) -- ����, ��� ������� ����������� ���� ���������
                                 then par_oes_date_s
                                 else date_s
                            end,
                   modified_by = par_user_id,
                   modified_date = SYSDATE
             WHERE oes_321_id = r2.oes_321_id;
            
            var_oes_321_id := r2.oes_321_id; 
            var_oes_update_count := var_oes_update_count + 1;

            -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
            if r.trxn_count = 1 Then
              var_trxn_msg := var_trxn_msg||' �������� ��� �'||to_char(r2.oes_321_id)||'.';
            end if;  
          else
            --
            -- ������������� ���� ������������� �������� ������ ��� �� ������������� ������
            --
            var_create_oes_flag := 1;
            var_review_id := r2.review_id; 
          end if;
        end if;
        --
        -- ���� ���� - �������� ��� �� ������/������������� ������
        --
        if var_create_oes_flag = 1 Then
          var_oes_321_id := mantas.rf_pkg_oes.create_oes_321p(var_review_id, r2.action, par_user_id);
          -- ���� ����, ��������� � ��� ���� ���������
          if var_opok_nb in (6001, 8001, 5003, 5007) Then -- ����, ��� ������� ����������� ���� ���������
            UPDATE mantas.rf_oes_321 
               SET date_s = nvl(par_oes_date_s, trunc(sysdate))
             WHERE oes_321_id = var_oes_321_id;
          end if;
          
          var_oes_create_count := var_oes_create_count + 1;

          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' ������� ��� �'||to_char(var_oes_321_id)||'.';
          end if;  
        end if;  
        --
        -- �������� ��������� (���)
        --
        var_oes_check_status := mantas.rf_pkg_oes_check.check_oes(par_form_cd => '321', par_oes_id => var_oes_321_id, par_commit_flag => 0);
  
        if var_oes_check_status = 'W' Then
          var_oes_warn_count := var_oes_warn_count + 1;

          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' ��� ������ �������� � ����������������.';
          end if;  
        elsif var_oes_check_status = 'E' Then
          var_oes_err_count := var_oes_err_count + 1;

          -- ��������� ��������� ���������, ���� �������� ������� ��� ����� ��������
          if r.trxn_count = 1 Then
            var_trxn_msg := var_trxn_msg||' ��� �� ������ �������� � ������� ���������.';
          end if;  
        end if;
        
        var_alert_done_flag := 1;

        if nvl(r2.review_id, var_review_id) is not null Then
          var_changed_rv_ids.extend;
          var_changed_rv_ids(var_changed_rv_ids.count) := nvl(r2.review_id, var_review_id);
        end if;
        
        <<NEXT_ALERT>>
        null;
      END LOOP; -- ���� �� �����������/����������� �������
      --
      -- ���� ���� - ��������� ��������� ��������
      --
      if var_arh_flag = 1 Then
        mantas.rf_pkg_arh.archive_one(p_id => r.trxn_seq_id, p_entity => r.op_cat_cd, p_arh_dump_dt => r.data_dump_dt);
      end if;

      -- ���� ���� �� ���� ����� �� �������� ��� ������/�������:
      --  - ����������� ������� ������������� ������������ ��������
      --  - ���������� ���� ������/����� ������������� � �������������� ������ �� ��������� ��������:
      --     1) �����/������������ ������ ������ �������� ����� ���������������
      --     2) ���� ���� ����� �� ������ ������������� - �� ��������� � ������� ������
      --     3) ���� ������ �� �������������� ���, �� ���� ������ ������������� ��� ������ - ��������� ��, 
      --        �� ������ � ������ �� ������� �� ������ ��������
      --     4) ������ ������������� � ������ ������� �� ���������������� � ������� ������
      if var_alert_done_flag = 1 Then
        var_trxn_done_count := var_trxn_done_count + 1;
        
        FOR r2 IN (SELECT case when r.null_review_id_flag > 0 and   -- ��� �������� ���� �������� ������������� ��� ������
                                    old_rv.review_id is null and    -- ����� ������������� �� ������ ����� ������ (��������)
                                    row_number() over(order by old_rv.review_id nulls first, new_rv.review_id) = 1 -- ������ ���� ����� ������������� ������ �� ������ ������������� ��� ������
                               then 1
                          end as null_old_flag,            
                          old_rv.review_id as old_review_id,
                          to_char(new_rv.review_id) as new_review_id
                     FROM (select distinct column_value as review_id
                             from TABLE(var_changed_rv_ids)) new_rv
                          left join 
                          (select distinct column_value as review_id
                             from TABLE(mantas.rf_pkg_scnro.list_to_tab(r.review_ids))) old_rv on old_rv.review_id = to_char(new_rv.review_id)
                   ORDER BY new_rv.review_id) LOOP
          var_res.extend;
          var_res(var_res.count) := rf_type_varchar_pair(case when r2.old_review_id is not null 
                                                              then r.trxn_input_id||'|'||r2.old_review_id
                                                              when r2.null_old_flag > 0 
                                                              then r.trxn_input_id||'|'
                                                         end,
                                                         r.trxn_input_id||'|'||r2.new_review_id);
        END LOOP;
      end if;  
      --
      -- ��������� ��������� ������������ ��������
      --    
      <<NEXT_TRXN>>
      COMMIT;
      --
      -- ���� �������� ������ ���� - �������� ��������� ��������� ��� ������������
      --      
      if r.trxn_count = 1 Then
        if var_trxn_done_count > 0 Then
          var_msg := '�������� "�������� �������� �� ����" ��������� ��� ��������� '||
                     case when r.is_inter_kgrko = 1 then '������������� ' end||'�������� (ID = '||r.trxn_input_id||').';                
        else
          var_msg := '�������� "�������� �������� �� ����" �� ��������� ��� ��������� '||
                     case when r.is_inter_kgrko = 1 then '������������� ' end||'�������� (ID = '||r.trxn_input_id||').';
        end if; 
        
        if var_trxn_not_found_count > 0 Then
          var_msg := var_msg||' �������� �� �������.';
        end if;
          
        if var_trxn_canceled_count > 0 Then
          var_msg := var_msg||' �������� �������� � �������-���������.';
        end if;

        if var_trxn_skip_inter_count > 0 Then
          var_msg := var_msg||' ������ ���� �������� ������������� ��������.';
        end if;
        
        var_msg := trim(var_msg||var_trxn_msg);
      end if; 
    EXCEPTION
      WHEN OTHERS THEN
        -- ���������� ���������� ��������
        begin ROLLBACK TO make_trxn_opok; exception when others then null; end;
        -- ���������� ������, ���������� ���������
        var_err_msg := '������ ��� ���������� �������� "�������� �������� �� ����" ��� �������� � ID: '||r.trxn_input_id||'. ��������� �������� ��������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM;
        EXIT;
    END;      
  END LOOP; -- ���� �� ���������
  --
  -- ���������� �������������� ��������� ��� ������������
  --
  if var_trxn_count > 1 Then
    if var_trxn_done_count > 0 and var_err_msg is null Then
      var_msg := '�������� "�������� �������� �� ����" ���������.';                
    elsif var_err_msg is not null Then
      var_msg := var_err_msg;                
    else
      var_msg := '�������� "�������� �������� �� ����" �� ���������.';
    end if; 
    
    var_msg := var_msg||' �������������� (����������) ��������: '||to_char(var_trxn_count);
    if var_trxn_inter_count > 0 Then    
      var_msg := var_msg||', � �. �. �������������: '||to_char(var_trxn_inter_count)||'.';
    else  
      var_msg := var_msg||'.';
    end if;
    
    if var_trxn_not_found_count > 0 Then    
      var_msg := var_msg||' �� ������� ��������: '||to_char(var_trxn_not_found_count)||'.';
    end if;
    
    if var_trxn_canceled_count > 0 Then    
      var_msg := var_msg||' ��������� ���������� ��������: '||to_char(var_trxn_canceled_count)||'.';
    end if;

    if var_trxn_skip_inter_count > 0 Then    
      var_msg := var_msg||' ��������� ������������� ��������: '||to_char(var_trxn_skip_inter_count)||'.';
    end if;

    if var_alert_skip_opok_count > 0 Then    
      var_msg := var_msg||' ��������� ��������, ��� ���������� �� ���� '||to_char(par_opok_nb)||': '||to_char(var_alert_skip_opok_count)||'.';
    end if;

    if var_alert_skip_status_count > 0 Then    
      var_msg := var_msg||' ��������� ��������, ������� ������ ������� �� ��������� ��������� ��������: '||to_char(var_alert_skip_status_count)||'.';
    end if;

    if var_alert_skip_access_count > 0 Then    
      var_msg := var_msg||' ��������� �������� ��-�� ���������� ���� �� ���������: '||to_char(var_alert_skip_access_count)||'.';
    end if;

    if var_alert_create_count > 0 Then    
      var_msg := var_msg||' ������� ����� ��������: '||to_char(var_alert_create_count)||'.';
    end if;

    if var_alert_update_count > 0 Then    
      var_msg := var_msg||' �������� ��������: '||to_char(var_alert_update_count)||'.';
    end if;

    if var_oes_create_count > 0 Then    
      var_msg := var_msg||' ������� ����� ��������� (���): '||to_char(var_oes_create_count)||'.';
    end if;

    if var_oes_update_count > 0 Then    
      var_msg := var_msg||' �������� ���: '||to_char(var_oes_update_count)||'.';
    end if;

    if var_oes_err_count > 0 Then    
      var_msg := var_msg||' ���, �� ��������� ��������: '||to_char(var_oes_err_count)||'.';
    end if;

    if var_oes_warn_count > 0 Then    
      var_msg := var_msg||' ���, ��������� �������� � ����������������: '||to_char(var_oes_warn_count)||'.';
    end if;
  elsif var_trxn_count = 1 and var_err_msg is not null Then
    var_msg := var_err_msg||'. '||var_msg;
  end if;

  par_updated_trxn_ids := var_res;
  return var_msg;
END make_trxn_opok;
END RF_PKG_TRXN;
/

GRANT EXECUTE on mantas.rf_pkg_trxn to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_trxn to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_trxn to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_trxn to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_trxn to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_trxn to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_trxn to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_trxn to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_trxn to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_trxn to KYC;
GRANT EXECUTE on mantas.rf_pkg_trxn to STD;
GRANT EXECUTE on mantas.rf_pkg_trxn to KDD_ALG;
