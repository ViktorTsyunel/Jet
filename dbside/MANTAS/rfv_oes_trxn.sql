GRANT EXECUTE ON BUSINESS.RF_TAB_TRXN TO MANTAS;
GRANT EXECUTE ON BUSINESS.RF_TYPE_TRXN TO MANTAS;

CREATE OR REPLACE 
FUNCTION mantas.rfv_oes_trxn
(
  par_oes_id           mantas.rf_oes_321.oes_321_id%TYPE       -- ID ��� 321-�
)
return BUSINESS.RF_TAB_TRXN pipelined is

  var_row       BUSINESS.RF_TYPE_TRXN := BUSINESS.RF_TYPE_TRXN(null,null,null,null,null,null,null,null,null,null,
                                                               null,null,null,null,null,null,null,null,null,null,
                                                               null,null,null,null,null,null,null,null,null,null,
                                                               null,null,null,null,null,null,null,null,null,null,
                                                               null,null,null,null,null,null,null,null,null,null,
                                                               null,null,null,null,null,null,null,null,null,null,
                                                               null,null,null,null,null,null,null);
BEGIN
  if par_oes_id is null Then
    return;
  end if;
    
  FOR r IN (SELECT /*+ ORDERED INDEX(oes RF_OES_321_PK) USE_NL(rv p0 p3 cur cur_con cntr0 cntr3 rv2 send_org rcv_org) INDEX(rv2 RF_KDD_REVIEW_INTERKGRKO_I)*/
                   -- �������� �������� ��������
                   to_char(null)                                                           as op_cat_cd,
                   to_char(null)                                                           as dbt_cdt_cd,
                   to_number(null)                                                         as trxn_seq_id,
                   nullif(oes.data, to_date('01.01.2099', 'DD.MM.YYYY'))                   as trxn_dt,
                   to_number(null)                                                         as branch_id,
                   to_char(null)                                                           as org_intrl_id,
                   nullif(oes.sum, 0)                                                      as trxn_crncy_am,
                   nullif(oes.sume, 0)                                                     as trxn_base_am,
                   cur.iso_alfa                                                            as trxn_crncy_cd,
                   to_number(null)                                                         as trxn_crncy_rate_am,
                   to_char(null)                                                           as trxn_crncy_old_rate_fl,
                   decode(oes.prim_1, '0', null, oes.prim_1)||
                     decode(oes.prim_2, '0', null, oes.prim_2)                             as trxn_desc,
                   to_char(null)                                                           as nsi_op_id,
                   -- ����������
                   nullif(p0.nameu, '0')                                                   as orig_nm,
                   nullif(p0.nd, '0')                                                      as orig_inn_nb,
                   to_char(null)                                                           as orig_kpp_nb,
                   nullif(p0.acc_b, '0')                                                   as orig_acct_nb,
                   decode(oes.b_payer, 1, 'Y', 'N')                                        as orig_own_fl,
                   to_char(null)                                                           as debit_cd,
                   business.rf_pkg_util.get_oes_addr_tx(p0.oes_321_id, p0.block_nb)        as orig_addr_tx,
                   business.rf_pkg_util.format_iddoc_text(
                     nullif(p0.kd, '0'),
                     nullif(p0.sd, '0'),
                     nullif(p0.vd1, '0'),
                     nullif(p0.vd2, '0'),
                     null,
                     nullif(trunc(p0.vd3), to_date('01.01.2099', 'DD.MM.YYYY')),
                     null)                                                                 as orig_iddoc_tx,
                   decode(p0.tu, 1, nullif(trunc(p0.gr), 
                                           to_date('01.01.2099', 'DD.MM.YYYY')),
                                 to_date(null))                                            as orig_reg_dt,
                   to_char(null)                                                           as orig_goz_op_cd,              
                   case when oes.b_payer in (1, 2) 
                        then to_char(null) 
                        else nullif(p0.name_b, '0')
                   end                                                                     as send_instn_nm,
                   case when oes.b_recip in (1, 2) 
                        then nvl(nullif(oes.bik_ss, '0'), nullif(oes.bik_s, '0'))
                        else nullif(p0.bik_b, '0')
                   end                                                                     as send_instn_id,
                   cntr0.cntry_cd                                                          as send_instn_cntry_cd,
                   nullif(p0.acc_cor_b, '0')                                               as send_instn_acct_id,
                   send_org.tb_code * 100000 + nvl(send_org.osb_code, 0)                   as send_branch_id,
                   send_org.oes_321_org_id                                                 as send_oes_org_id,
                   send_org.tb_name                                                        as send_kgrko_nm,
                   case when send_org.oes_321_org_id is not null
                         then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), '1', nullif(send_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                   end                                                                     as send_kgrko_id,
                   -- ����������
                   nullif(p3.nameu, '0')                                                   as benef_nm,
                   nullif(p3.nd, '0')                                                      as benef_inn_nb,
                   to_char(null)                                                           as benef_kpp_nb,
                   nullif(p3.acc_b, '0')                                                   as benef_acct_nb,
                   decode(oes.b_recip, 1, 'Y', 'N')                                        as benef_own_fl,
                   to_char(null)                                                           as credit_cd,
                   business.rf_pkg_util.get_oes_addr_tx(p3.oes_321_id, p3.block_nb)        as benef_addr_tx,
                   business.rf_pkg_util.format_iddoc_text(
                     nullif(p3.kd, '0'),
                     nullif(p3.sd, '0'),
                     nullif(p3.vd1, '0'),
                     nullif(p3.vd2, '0'),
                     null,
                     nullif(trunc(p3.vd3), to_date('01.01.2099', 'DD.MM.YYYY')),
                     null)                                                                 as benef_iddoc_tx,
                   decode(p3.tu, 1, nullif(trunc(p3.gr), 
                                           to_date('01.01.2099', 'DD.MM.YYYY')),
                                 to_date(null))                                            as benef_reg_dt,
                   to_char(null)                                                           as benef_goz_op_cd,              
                   case when oes.b_payer in (1, 2) 
                        then to_char(null)
                        else nullif(p3.name_b, '0')
                   end                                                                     as rcv_instn_nm,
                   case when oes.b_recip in (1, 2) 
                        then nvl(nullif(oes.bik_ss, '0'), nullif(oes.bik_s, '0'))
                        else nullif(p3.bik_b, '0')
                   end                                                                     as rcv_instn_id,
                   cntr3.cntry_cd                                                          as rcv_instn_cntry_cd,
                   nullif(p3.acc_cor_b, '0')                                               as rcv_instn_acct_id,
                   rcv_org.tb_code * 100000 + nvl(rcv_org.osb_code, 0)                     as rcv_branch_id,
                   rcv_org.oes_321_org_id                                                  as rcv_oes_org_id,
                   rcv_org.tb_name                                                         as rcv_kgrko_nm,
                   case when rcv_org.oes_321_org_id is not null
                         then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), '1', nullif(rcv_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                   end                                                                     as rcv_kgrko_id,
                   -- ���. �������� ��������
                   to_char(nullif(oes.num_pay_d, 0))                                       as trxn_doc_id,
                   nullif(trunc(oes.date_pay_d), to_date('01.01.2099', 'DD.MM.YYYY'))      as trxn_doc_dt,
                   cur_con.iso_alfa                                                        as trxn_conv_crncy_cd,
                   nullif(oes.sum_con, 0)                                                  as trxn_conv_am,
                   to_number(null)                                                         as trxn_conv_base_am,
                   to_char(null)                                                           as trxn_cash_symbols_tx,
                   to_number(null)                                                         as bank_card_id_nb,
                   to_date(null)                                                           as bank_card_trxn_dt,
                   to_char(null)                                                           as merchant_no,
                   to_char(null)                                                           as atm_id,
                   to_number(null)                                                         as tb_id,
                   to_number(null)                                                         as osb_id,
                   to_char(null)                                                           as src_sys_cd,
                   to_date(null)                                                           as src_change_dt,
                   to_char(null)                                                           as src_user_nm,
                   to_char(null)                                                           as src_cd,
                   oes.deleted_fl                                                          as canceled_fl,
                   to_date(null)                                                           as data_dump_dt
              FROM mantas.rf_oes_321 oes
                   join mantas.kdd_review rv on rv.review_id = oes.review_id
                   left join mantas.rf_oes_321_party p0 on p0.oes_321_id = oes.oes_321_id and
                                                           p0.block_nb = 0
                   left join mantas.rf_oes_321_party p3 on p3.oes_321_id = oes.oes_321_id and
                                                           p3.block_nb = 3
                   left join business.rf_currency cur on cur.iso_num = to_number(oes.curren)
                   left join business.rf_currency cur_con on cur_con.iso_num = to_number(oes.curren_con)
                   left join business.rf_country cntr0 on cntr0.iso_numeric_cd = substr(p0.kodcn_b, 1, 3)
                   left join business.rf_country cntr3 on cntr3.iso_numeric_cd = substr(p3.kodcn_b, 1, 3)
                   -- ��� ������������� ������ ��� (rf_inter_kgrko_id is not null) �������� ������ ���
                   left join mantas.kdd_review rv2 on rv2.rf_inter_kgrko_id = rv.rf_inter_kgrko_id and
                                                      rv2.review_id <> rv.review_id
                   left join mantas.rf_oes_321 oes2 on oes2.review_id = rv2.review_id and
                                                       oes2.current_fl = 'Y' and
                                                       nvl(oes2.deleted_fl, 'N') <> 'Y'                                  
                   left join mantas.rf_oes_321_org send_org ON send_org.oes_321_org_id = case when rv.rf_inter_kgrko_id is not null and -- ��� �������������
                                                                                                   rv.rf_kgrko_party_cd = 1             -- �� �����������
                                                                                              then oes.oes_321_org_id
                                                                                              when rv.rf_inter_kgrko_id is not null and -- ��� ������������� 
                                                                                                   rv.rf_kgrko_party_cd = 2 and         -- �� ����������
                                                                                                   oes2.oes_321_id is not null          -- ���� ����������� ������ ��� - �� �����������
                                                                                              then oes2.oes_321_org_id                                                                                                
                                                                                              when oes.b_payer in ('1', '2')            -- ���������� - ������ ��� ��� ����
                                                                                              then oes.oes_321_org_id
                                                                                         end
                   left join mantas.rf_oes_321_org rcv_org ON rcv_org.oes_321_org_id = case when rv.rf_inter_kgrko_id is not null and -- ��� ������������� 
                                                                                                 rv.rf_kgrko_party_cd = 2             -- �� ����������
                                                                                            then oes.oes_321_org_id                                                                                                
                                                                                            when rv.rf_inter_kgrko_id is not null and -- ��� �������������
                                                                                                 rv.rf_kgrko_party_cd = 1 and         -- �� �����������
                                                                                                 oes2.oes_321_id is not null          -- ���� ����������� ������ ��� - �� ����������
                                                                                            then oes2.oes_321_org_id
                                                                                            when oes.b_recip in ('1', '2')            -- ���������� - ������ ��� ��� ����
                                                                                            then oes.oes_321_org_id
                                                                                       end
             WHERE oes.oes_321_id = par_oes_id
            ORDER BY oes2.oes_321_id) LOOP       
    var_row.op_cat_cd := r.op_cat_cd;
    var_row.dbt_cdt_cd := r.dbt_cdt_cd;
    var_row.trxn_seq_id := r.trxn_seq_id;
    var_row.trxn_dt := r.trxn_dt;
    var_row.branch_id := r.branch_id;
    var_row.org_intrl_id := r.org_intrl_id;
    var_row.trxn_crncy_am := r.trxn_crncy_am;
    var_row.trxn_base_am := r.trxn_base_am;
    var_row.trxn_crncy_cd := r.trxn_crncy_cd;
    var_row.trxn_crncy_rate_am := r.trxn_crncy_rate_am;
    var_row.trxn_crncy_old_rate_fl := r.trxn_crncy_old_rate_fl;
    var_row.trxn_desc := r.trxn_desc;
    var_row.nsi_op_id := r.nsi_op_id;
    var_row.orig_nm := r.orig_nm;
    var_row.orig_inn_nb := r.orig_inn_nb;
    var_row.orig_kpp_nb := r.orig_kpp_nb;
    var_row.orig_acct_nb := r.orig_acct_nb;
    var_row.orig_own_fl := r.orig_own_fl;
    var_row.debit_cd := r.debit_cd;
    var_row.orig_addr_tx := r.orig_addr_tx;
    var_row.orig_iddoc_tx := r.orig_iddoc_tx;
    var_row.orig_reg_dt := r.orig_reg_dt;
    var_row.orig_goz_op_cd := r.orig_goz_op_cd;
    var_row.send_instn_nm := r.send_instn_nm;
    var_row.send_instn_id := r.send_instn_id;
    var_row.send_instn_cntry_cd := r.send_instn_cntry_cd;
    var_row.send_instn_acct_id := r.send_instn_acct_id;
    var_row.send_branch_id := r.send_branch_id;
    var_row.send_oes_org_id := r.send_oes_org_id;
    var_row.send_kgrko_nm := r.send_kgrko_nm;
    var_row.send_kgrko_id := r.send_kgrko_id;
    var_row.benef_nm := r.benef_nm;
    var_row.benef_inn_nb := r.benef_inn_nb;
    var_row.benef_kpp_nb := r.benef_kpp_nb;
    var_row.benef_acct_nb := r.benef_acct_nb;
    var_row.benef_own_fl := r.benef_own_fl;
    var_row.credit_cd := r.credit_cd;
    var_row.benef_addr_tx := r.benef_addr_tx;
    var_row.benef_iddoc_tx := r.benef_iddoc_tx;
    var_row.benef_reg_dt := r.benef_reg_dt;
    var_row.benef_goz_op_cd := r.benef_goz_op_cd;
    var_row.rcv_instn_nm := r.rcv_instn_nm;
    var_row.rcv_instn_id := r.rcv_instn_id;
    var_row.rcv_instn_cntry_cd := r.rcv_instn_cntry_cd;
    var_row.rcv_instn_acct_id := r.rcv_instn_acct_id;
    var_row.rcv_branch_id := r.rcv_branch_id;
    var_row.rcv_oes_org_id := r.rcv_oes_org_id;
    var_row.rcv_kgrko_nm := r.rcv_kgrko_nm;
    var_row.rcv_kgrko_id := r.rcv_kgrko_id;
    var_row.trxn_doc_id := r.trxn_doc_id;
    var_row.trxn_doc_dt := r.trxn_doc_dt;
    var_row.trxn_conv_crncy_cd := r.trxn_conv_crncy_cd;
    var_row.trxn_conv_am := r.trxn_conv_am;
    var_row.trxn_conv_base_am := r.trxn_conv_base_am;
    var_row.trxn_cash_symbols_tx := r.trxn_cash_symbols_tx;      
    var_row.bank_card_id_nb := r.bank_card_id_nb;
    var_row.bank_card_trxn_dt := r.bank_card_trxn_dt;
    var_row.merchant_no := r.merchant_no;
    var_row.atm_id := r.atm_id;
    var_row.tb_id := r.tb_id;
    var_row.osb_id := r.osb_id;
    var_row.src_sys_cd := r.src_sys_cd;
    var_row.src_change_dt := r.src_change_dt;
    var_row.src_user_nm := r.src_user_nm;
    var_row.src_cd := r.src_cd;
    var_row.canceled_fl := r.canceled_fl;
    var_row.data_dump_dt := r.data_dump_dt;
      
    PIPE ROW(var_row);
  END LOOP;
END rfv_oes_trxn; 
/

grant EXECUTE on MANTAS.rfv_oes_trxn to BUSINESS;

grant EXECUTE on MANTAS.rfv_oes_trxn to DATA_READER;

grant EXECUTE on MANTAS.rfv_oes_trxn to KDD_ALGORITHM;

grant EXECUTE on MANTAS.rfv_oes_trxn to KDD_ANALYST;

grant EXECUTE on MANTAS.rfv_oes_trxn to KDD_MINER;

grant EXECUTE on MANTAS.rfv_oes_trxn to RF_RSCHEMA_ROLE;

grant EXECUTE on MANTAS.rfv_oes_trxn to CMREVMAN;

grant EXECUTE on MANTAS.rfv_oes_trxn to KDD_MNR with grant option;

grant EXECUTE on MANTAS.rfv_oes_trxn to KDD_REPORT with grant option;

grant EXECUTE on MANTAS.rfv_oes_trxn to KYC;

grant EXECUTE on MANTAS.rfv_oes_trxn to STD;


