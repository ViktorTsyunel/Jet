-- ���: �������� � XML �� ������� 321�
create or replace TYPE mantas.RF_TYPE_321P_OPER AS OBJECT
(
  -- ������� � ���������������� ��������/������, ����������� � XML
  op_cat_cd           VARCHAR2(3 CHAR),    -- ��������� ��������: WT, CT, BOT (�����������, ��������, ����������)
  trxn_seq_id         NUMBER(22),          -- ������������� ��������
  trxn_scrty_seq_id   NUMBER(22),          -- ������������� ������ � ������ ������ � ��������
  review_id           NUMBER(22),          -- ������������� ������
  -- �������� ����� OPER_INFO
  TERROR              VARCHAR2(1 CHAR),
  VO                  VARCHAR2(4 CHAR),
  DOP_V               VARCHAR2(2000 CHAR),
  DATA                VARCHAR2(10 CHAR), -- yyyy-mm-dd
  SUME                VARCHAR2(17 CHAR), -- 14.2
  SUM                 VARCHAR2(17 CHAR), -- 14.2
  CURREN              VARCHAR2(3 CHAR), 
  PRIM                VARCHAR2(2000 CHAR), 
  NUM_PAY_D           VARCHAR2(12 CHAR), 
  DATE_PAY_D          VARCHAR2(10 CHAR), -- yyyy-mm-dd
  METAL               VARCHAR2(3 CHAR), 
  PRIZ6001            VARCHAR2(2000 CHAR), 
  B_PAYER             VARCHAR2(1 CHAR), 
  B_RECIP             VARCHAR2(1 CHAR), 
  PART                VARCHAR2(1 CHAR), 
  DESCR               VARCHAR2(2000 CHAR), 
  CURREN_CON          VARCHAR2(3 CHAR), 
  SUM_CON             VARCHAR2(17 CHAR), -- 14.2
  PRIZ_SD             VARCHAR2(1 CHAR), 
  DATE_S              VARCHAR2(10 CHAR), -- yyyy-mm-dd
  -- �������� ����� DOP_OPER_INFO
  PRMANUAL            VARCHAR2(2000 CHAR), 
  COMMENT_            VARCHAR2(2000 CHAR), 
  OPERATOR_M          VARCHAR2(2000 CHAR), 
  -- �������� ����� SOURCE_INFO
  FILIAL              VARCHAR2(2000 CHAR), 
  ASYSTEM             VARCHAR2(2000 CHAR), 
  idOperSystem        VARCHAR2(2000 CHAR), 
  OPERATOR_S          VARCHAR2(2000 CHAR),
  -- �������� ����� TYPE_INFO
  VERSION_P             VARCHAR2(2000 CHAR),
  ACTION              VARCHAR2(2000 CHAR),
  NUMB_P              VARCHAR2(2000 CHAR),
  DATE_P              VARCHAR2(2000 CHAR),
  TEL                 VARCHAR2(2000 CHAR),

  -- �������� ����� KO_INFO
  REGN                 VARCHAR2(2000 CHAR),
  ND_KO                 VARCHAR2(2000 CHAR),
  KTU_S                 VARCHAR2(2000 CHAR),
  BIK_S                 VARCHAR2(2000 CHAR),
  NUMBF_S                 VARCHAR2(2000 CHAR),
  BRANCH                 VARCHAR2(2000 CHAR),
  KTU_SS                 VARCHAR2(2000 CHAR),
  BIK_SS                 VARCHAR2(2000 CHAR),
  NUMBF_SS                 VARCHAR2(2000 CHAR),
  oes_321_org_id           number(22), --���� �� �� �����������  
  refer_r2            varchar2(2000 char)
)
/

drop TYPE mantas.RF_TAB_321P_PARTY
/

-- ���: �������� �������� � XML �� ������� 321�
create or replace TYPE mantas.RF_TYPE_321P_PARTY AS OBJECT
(
  -- �������� ����� MEMBER
  TUREAL              VARCHAR2(1 CHAR),  
  PRU                 VARCHAR2(6 CHAR),  
  NAMEU               VARCHAR2(2000 CHAR),  
  KODCR               VARCHAR2(5 CHAR),  
  KODCN               VARCHAR2(5 CHAR),  
  -- �������� ����� MEMBER/AMR (����� �����������)
  AMR_ADRESS_S        VARCHAR2(2 CHAR),  
  AMR_ADRESS_R        VARCHAR2(2000 CHAR),  
  AMR_ADRESS_G        VARCHAR2(2000 CHAR),  
  AMR_ADRESS_U        VARCHAR2(2000 CHAR),  
  AMR_ADRESS_D        VARCHAR2(2000 CHAR),  
  AMR_ADRESS_K        VARCHAR2(2000 CHAR),  
  AMR_ADRESS_O        VARCHAR2(2000 CHAR),  
  -- �������� ����� MEMBER/ADRESS (����� ���������������)
  ADR_ADRESS_S        VARCHAR2(2 CHAR),  
  ADR_ADRESS_R        VARCHAR2(2000 CHAR),  
  ADR_ADRESS_G        VARCHAR2(2000 CHAR),  
  ADR_ADRESS_U        VARCHAR2(2000 CHAR),  
  ADR_ADRESS_D        VARCHAR2(2000 CHAR),  
  ADR_ADRESS_K        VARCHAR2(2000 CHAR),  
  ADR_ADRESS_O        VARCHAR2(2000 CHAR),  
  -- ����� ����� MEMBER/ADRESS
  KD                  VARCHAR2(2 CHAR),  
  SD                  VARCHAR2(2000 CHAR),  
  RG                  VARCHAR2(2000 CHAR),  
  ND                  VARCHAR2(2000 CHAR),  
  -- �������� ����� MEMBER/VD (�������������� ��������)
  VD_1                VARCHAR2(2000 CHAR),  
  VD_2                VARCHAR2(2000 CHAR),  
  VD_3                VARCHAR2(10 CHAR),  
  VD_4                VARCHAR2(2 CHAR),  
  VD_5                VARCHAR2(2000 CHAR),  
  VD_6                VARCHAR2(10 CHAR),  
  VD_7                VARCHAR2(10 CHAR),  
  MC_1                VARCHAR2(2000 CHAR),  
  MC_2                VARCHAR2(10 CHAR),  
  MC_3                VARCHAR2(10 CHAR),  
  -- ����� ����� MEMBER/VD
  GR                  VARCHAR2(10 CHAR),  
  BP                  VARCHAR2(2000 CHAR),  
  VP                  VARCHAR2(1 CHAR),  
  -- �������� ����� CO
  ACC_B               VARCHAR2(2000 CHAR),  
  ACC_COR_B           VARCHAR2(2000 CHAR),  
  NAME_IS_B           VARCHAR2(2000 CHAR),  
  BIK_IS_B            VARCHAR2(2000 CHAR),  
  CARD_B              VARCHAR2(1 CHAR),  
  NAME_B              VARCHAR2(2000 CHAR),  
  KODCN_B             VARCHAR2(5 CHAR),  
  BIK_B               VARCHAR2(2000 CHAR),  
  NAME_R              VARCHAR2(2000 CHAR),  
  KODCN_R             VARCHAR2(5 CHAR),  
  BIK_R               VARCHAR2(2000 CHAR),
  ADD_INFO            VARCHAR2(2000 CHAR),
  -- ��������������, �� �������� � XML/DBF
  oes_321_p_org_id    NUMBER(22),
  cust_seq_id         NUMBER(22)
)
/

create or replace TYPE mantas.RF_TAB_321P_PARTY as TABLE OF mantas.RF_TYPE_321P_PARTY
/
create or replace TYPE mantas.RF_TAB_NUMBER AS TABLE OF NUMBER
/

--drop package mantas.rf_pkg_oes
CREATE OR REPLACE PACKAGE mantas.rf_pkg_oes IS
--
-- ��������� ���� ��������� � ������������ � 321-� ��� ���������� ��������� (���)
--
PROCEDURE get_oes_321p (
  par_oes_321_id   IN  mantas.rf_oes_321.oes_321_id%TYPE,
  par_oper        OUT  mantas.RF_TYPE_321P_OPER,
  par_parties     OUT  mantas.RF_TAB_321P_PARTY
  );
--
-- ��������� XML � ������������ � 321-� ��� ��������� �������� ����� ���������
--
FUNCTION get_xml_321p (
  par_oper             mantas.RF_TYPE_321P_OPER,
  par_parties          mantas.RF_TAB_321P_PARTY
  )
RETURN XMLType;
--
-- ��������� XML � ������������ � 321-� ��� ���������� ��������� (���)
--
FUNCTION get_xml_321p (
  par_oes_321_id    mantas.rf_oes_321.oes_321_id%TYPE
  )
RETURN XMLType;
--
-- ��������� � xml-����� ��������� ��������� (���) �� ������� ��������� 321-�
--
PROCEDURE export_xml_321p (
  par_oes_321_ids      mantas.RF_TAB_NUMBER -- ������ ��������������� ��������� (���) ��� �������� � ����
  ); 
--
-- ��������� ���� ��������� 321-� � ������������ � ��������� �������
--
PROCEDURE get_oes_321p_by_alert (
  par_review_id    IN  mantas.kdd_review.review_id%TYPE,
  par_oper        OUT  mantas.RF_TYPE_321P_OPER,
  par_parties     OUT  mantas.RF_TAB_321P_PARTY
  );
--
-- ��������� ���� ��������� ��������� 321-� ���������� ���������� ��/���
-- (� ������������ �� ������������ �� - ��������� ��� �� ��������� ���� ��������)
--
PROCEDURE set_oes_321_party_tb (
  par_tb_id    IN      mantas.rf_oes_321_party_org.tb_code%TYPE,
  par_osb_id   IN      mantas.rf_oes_321_party_org.osb_code%TYPE,
  par_trxn_dt  IN      DATE,
  par_mode     IN      VARCHAR2,  -- 'FULL' - ��� ��������, 'CUST' - ��������, ����������� � ���������� ��������� (� �� � ����� ���������)
  par_party    IN OUT  mantas.RF_TYPE_321P_PARTY
  );
--
-- ������� ��������� 321-� �� ���������� ������
--
FUNCTION create_oes_321p(
  par_review_id        mantas.kdd_review.review_id%TYPE,
  par_action           mantas.rf_oes_321.action%TYPE,
  par_user             mantas.kdd_review_owner.owner_id%TYPE
  )
RETURN mantas.rf_oes_321.oes_321_id%TYPE;

--
-- ����� ������� ����� � ��������� ��� � �����������
--
procedure create_oes_321_manual (
  par_opok_nb         in  rf_opok.opok_nb%type,         -- ��� ��������
  par_add_opoks_tx    in  varchar2,                     -- �������������� ���� ��������
  par_oes_321_org_id  in  rf_oes_321_org.oes_321_org_id%type, -- ID ������� �����, � �������� ��������� ���
  par_branch_id       in  kdd_review.rf_branch_id%type, -- �������� ����������� ������ ���� ���������� is null - ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
  par_owner_seq_id    in  kdd_review.owner_seq_id%type, -- �������������, ��������������� � ����� (���� null, �� ���������������)
  par_creat_id        in  kdd_review.creat_id%type,     -- ������������, ��������� ���
  par_note_tx         in  kdd_note_hist.note_tx%type,   -- ����������� � ��������� ���� � ������������ ��� - �� oes_manual_cmnt
  par_due_dt          in  kdd_review.due_dt%type,       -- ���� ��������� 
  par_inter_kgrko_id  in  kdd_review.rf_inter_kgrko_id%type, --ID, ������������ ��� ������ ��������� (���) � ������ ������������� ��������
  par_kgrko_party_cd  in  kdd_review.rf_kgrko_party_cd%type, --������� ���� ��������� ������������� �������� (1/2 - ���������� / ����������)
  par_review_id       out kdd_review.review_id%type,    -- ����� ������
  par_oes_321_id      out rf_oes_321.oes_321_id%type    -- ����� ��� ��������
);
--
-- ����� ������� ����� � ��������� ��� � ����������� �� ���������� ������
--
procedure create_oes_321_manual (
  p_branch_id       in  kdd_review.rf_branch_id%type, -- �������� ����������� ������ ���� ���������� is null - ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
  p_owner_seq_id    in  kdd_review.owner_seq_id%type, -- �������������, ��������������� � ����� (���� null, �� ���������������)
  p_creat_id        in  kdd_review.creat_id%type,     -- ������������, ��������� ���
  p_note_tx         in  kdd_note_hist.note_tx%type,   -- ����������� � ��������� ���� � ������������ ��� - �� oes_manual_cmnt
  p_oper            in  rf_type_321p_oper,            -- ��������
  p_parties         in  rf_tab_321p_party,            -- ���������
  p_review_id       out kdd_review.review_id%type,    -- ����� ������
  p_oes_321_id      out rf_oes_321.oes_321_id%type    -- ����� ��� ��������
);
--
-- ����� ������� ����� ��� �� ����� oes_321_id. �������� ������ �� ����������.
--
procedure copy_oes_321 (
  p_oes_321_id     in rf_oes_321.oes_321_id%type, -- ���� ���������, ������� ����� �����������
  p_owner_seq_id   in kdd_review_owner.owner_seq_id%type, -- id ������������ 
  p_new_review_id  out kdd_review.review_id%type, -- ����� ���� ���������� ��������
  p_new_oes_321_id out rf_oes_321.oes_321_id%type -- ����� ���� ���������
);

END rf_pkg_oes;
/

CREATE OR REPLACE PACKAGE BODY mantas.rf_pkg_oes IS
--
-- ��������� ���� ��������� � ������������ � 321-� ��� ���������� ��������� (���)
--
PROCEDURE get_oes_321p (
  par_oes_321_id   IN  mantas.rf_oes_321.oes_321_id%TYPE,
  par_oper        OUT  mantas.RF_TYPE_321P_OPER,
  par_parties     OUT  mantas.RF_TAB_321P_PARTY
  ) IS
  v_party   mantas.rf_type_321p_party;
  v_max_block number;
BEGIN
  par_oper := mantas.rf_type_321p_oper(
    null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null
  );

  select version, regn, nd_ko, ktu_s, bik_s, 
         numbf_s, branch, ktu_ss, bik_ss, numbf_ss, 
         tel, oes_321_org_id, to_char(data, 'YYYY-MM-DD') , to_char(date_s, 'YYYY-MM-DD'), terror, 
         vo, dop_v, LTRIM(TO_CHAR(sume,'99999999999990.00')) as sume, LTRIM(TO_CHAR(sum,'99999999999990.00')) as sum, curren, 
         nvl(prim_1||nullif(prim_2, '0'), '0'), num_pay_d, to_char(date_pay_d, 'YYYY-MM-DD'), metal, priz6001,
         b_payer, b_recip, part, nvl(descr_1||nullif(descr_2, '0'), '0'), curren_con,
         DECODE(NVL(sum_con,0),0,'0',LTRIM(TO_CHAR(sum_con,'99999999999990.00'))) as sum_con, priz_sd, '0' as prmanual, '0' as comment_, '0' as operator_m,         
         (select to_char(trunc(rf_branch_id/100000))||decode(rf_branch_id - round(rf_branch_id, -5), 0, to_char(null), to_char(rf_branch_id - round(rf_branch_id, -5))) 
            from mantas.kdd_review 
           where review_id = o.review_id) as filial,
         '600' as asystem, to_char(o.review_id) as idopersystem, '0' as operator_s, action,
         '0' numb_p, to_char(sysdate, 'YYYY-MM-DD') as date_p, refer_r2
    into par_oper.version_p, par_oper.regn, par_oper.nd_ko, par_oper.ktu_s, par_oper.bik_s,
         par_oper.numbf_s,  par_oper.branch, par_oper.ktu_ss, par_oper.bik_ss, par_oper.numbf_ss,
         par_oper.tel, par_oper.oes_321_org_id, par_oper.data, par_oper.date_s, par_oper.terror,
         par_oper.vo, par_oper.dop_v, par_oper.sume, par_oper.sum, par_oper.curren,
         par_oper.prim, par_oper.num_pay_d, par_oper.date_pay_d, par_oper.metal, par_oper.priz6001,
         par_oper.b_payer, par_oper.b_recip, par_oper.part, par_oper.descr, par_oper.curren_con,
         par_oper.sum_con, par_oper.priz_sd, par_oper.prmanual, par_oper.comment_, par_oper.operator_m,
         par_oper.filial, par_oper.asystem, par_oper.idopersystem, par_oper.operator_s, par_oper.action, 
         par_oper.numb_p, par_oper.date_p, par_oper.refer_r2
    from mantas.rf_oes_321 o
   where oes_321_id = par_oes_321_id;
   
  select max(block_nb) into v_max_block
    from mantas.rf_oes_321_party 
   where oes_321_id = par_oes_321_id;

  par_parties := mantas.rf_tab_321p_party();
  par_parties.extend(v_max_block + 1);
  
  for cur in ( select tu, pr, nameu, kodcr, kodcn, kd, sd, rg, nd, vd1, vd2, vd3,
                      vd4, vd5, vd6, vd7, mc1, mc2, mc3, gr, bp, vp, acc_b, acc_cor_b,
                      amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, adress_s, adress_r,
                      adress_g, adress_u, adress_d, adress_k, adress_o, name_b, kodcn_b,
                      bik_b, card_b, name_is, bik_is, name_r, kodcn_r, bik_r, block_nb
                 from mantas.rf_oes_321_party
                where oes_321_id = par_oes_321_id ) loop

    v_party := mantas.rf_type_321p_party(
      null, null, null, null, null, null, null, null, null, null,
      null, null, null, null, null, null, null, null, null, null,
      null, null, null, null, null, null, null, null, null, null,
      null, null, null, null, null, null, null, null, null, null,
      null, null, null, null, null, null, null, null, null, null
    );

    v_party.tureal := cur.tu;
    v_party.pru := cur.pr;
    v_party.nameu := cur.nameu;
    v_party.kodcr := cur.kodcr;
    v_party.kodcn := cur.kodcn;
    v_party.kd := cur.kd;
    v_party.sd := cur.sd;
    v_party.rg := cur.rg;
    v_party.nd := cur.nd;
    v_party.vd_1 := cur.vd1;
    v_party.vd_2 := cur.vd2;
    v_party.vd_3 := to_char(cur.vd3, 'YYYY-MM-DD');
    v_party.vd_4 := cur.vd4;
    v_party.vd_5 := cur.vd5;
    v_party.vd_6 := to_char(cur.vd6, 'YYYY-MM-DD');
    v_party.vd_7 := to_char(cur.vd7, 'YYYY-MM-DD');
    v_party.mc_1 := cur.mc1;
    v_party.mc_2 := to_char(cur.mc2, 'YYYY-MM-DD');
    v_party.mc_3 := to_char(cur.mc3, 'YYYY-MM-DD');
    v_party.gr := to_char(cur.gr, 'YYYY-MM-DD');
    v_party.bp := cur.bp;
    v_party.vp := cur.vp;
    v_party.acc_b := cur.acc_b;
    v_party.acc_cor_b := cur.acc_cor_b;
    v_party.amr_adress_s := cur.amr_s;
    v_party.amr_adress_r := cur.amr_r;
    v_party.amr_adress_g := cur.amr_g;
    v_party.amr_adress_u := cur.amr_u;
    v_party.amr_adress_d := cur.amr_d;
    v_party.amr_adress_k := cur.amr_k;
    v_party.amr_adress_o := cur.amr_o;
    v_party.adr_adress_s := cur.adress_s;
    v_party.adr_adress_r := cur.adress_r;
    v_party.adr_adress_g := cur.adress_g;
    v_party.adr_adress_u := cur.adress_u;
    v_party.adr_adress_d := cur.adress_d;
    v_party.adr_adress_k := cur.adress_k;
    v_party.adr_adress_o := cur.adress_o;
    v_party.card_b := cur.card_b;
    v_party.name_b := cur.name_b;
    v_party.kodcn_b := cur.kodcn_b;
    v_party.bik_b := cur.bik_b;
    v_party.name_is_b := cur.name_is;
    v_party.bik_is_b := cur.bik_is;
    v_party.name_r := cur.name_r;
    v_party.kodcn_r := cur.kodcn_r;
    v_party.bik_r := cur.bik_r;

    
    par_parties(cur.block_nb + 1) := v_party;

  end loop;
END get_oes_321p;  
--
-- ��������� XML � ������������ � 321-� ��� ��������� �������� ����� ���������
--
FUNCTION get_xml_321p (
  par_oper         mantas.RF_TYPE_321P_OPER,
  par_parties      mantas.RF_TAB_321P_PARTY
  )
RETURN XMLType IS

  var_i              INTEGER;

  var_oper_info      XMLType;
  var_dop_oper_info  XMLType;
  var_source_info    XMLType;
  var_type_info      XMLType; 
  var_ko_info        XMLType;

  var_member_info    XMLType;
  var_member_info_0  XMLType;
  var_member_info_1  XMLType;
  var_member_info_2  XMLType;
  var_member_info_3  XMLType;
  var_member_info_4  XMLType;
  var_result         XMLType;
BEGIN
  --
  -- �������� ����� xml, ��������������� �������� (�� ����������)
  --
  if par_oper is not null Then
    SELECT 
           xmlelement("OPER_INFO", xmlelement("TERROR",     par_oper.TERROR),
                                   xmlelement("VO",         par_oper.VO),
                                   xmlelement("DOP_V",      par_oper.DOP_V),
                                   xmlelement("DATA",       par_oper.DATA),
                                   xmlelement("SUME",       par_oper.SUME),
                                   xmlelement("SUM",        par_oper.SUM),
                                   xmlelement("CURREN",     par_oper.CURREN),
                                   xmlelement("PRIM",       par_oper.PRIM),
                                   xmlelement("NUM_PAY_D",  par_oper.NUM_PAY_D),
                                   xmlelement("DATE_PAY_D", par_oper.DATE_PAY_D),
                                   xmlelement("METAL",      par_oper.METAL),
                                   xmlelement("PRIZ6001",   par_oper.PRIZ6001),
                                   xmlelement("B_PAYER",    par_oper.B_PAYER),
                                   xmlelement("B_RECIP",    par_oper.B_RECIP),
                                   xmlelement("PART",       par_oper.PART),
                                   xmlelement("DESCR",      par_oper.DESCR),
                                   xmlelement("CURREN_CON", par_oper.CURREN_CON),
                                   xmlelement("SUM_CON",    par_oper.SUM_CON),
                                   xmlelement("PRIZ_SD",    par_oper.PRIZ_SD),
                                   xmlelement("DATE_S",     par_oper.DATE_S)),
           xmlelement("DOP_OPER_INFO", xmlelement("PRMANUAL",   par_oper.PRMANUAL),
                                       xmlelement("COMMENT",    par_oper.COMMENT_),
                                       xmlelement("OPERATOR_M", par_oper.OPERATOR_M)),
           xmlelement("SOURCE_INFO", xmlelement("FILIAL",       par_oper.FILIAL),
                                     xmlelement("ASYSTEM",      par_oper.ASYSTEM),
                                     xmlelement("idOperSystem", par_oper.idOperSystem),
                                     xmlelement("OPERATOR_S",   par_oper.OPERATOR_S)),
           xmlelement("TYPE_INFO",   xmlelement("VERSION",      par_oper.VERSION_P),
                                     xmlelement("ACTION",       par_oper.ACTION),
                                     xmlelement("NUMB_P",       par_oper.NUMB_P),
                                     xmlelement("DATE_P",       par_oper.DATE_P),
                                     xmlelement("TEL",          par_oper.TEL),
                                     xmlelement("REFER_R2",     par_oper.REFER_R2)),
           xmlelement("KO_INFO",     xmlelement("REGN",         par_oper.REGN),
                                     xmlelement("ND_KO",        par_oper.ND_KO),
                                     xmlelement("KTU_S",        par_oper.KTU_S),
                                     xmlelement("BIK_S",        par_oper.BIK_S),
                                     xmlelement("NUMBF_S",      par_oper.NUMBF_S),
                                     xmlelement("BRANCH",       par_oper.BRANCH),
                                     xmlelement("KTU_SS",       par_oper.KTU_SS),
                                     xmlelement("BIK_SS",       par_oper.BIK_SS),
                                     xmlelement("NUMBF_SS",     par_oper.NUMBF_SS))
                                     
      INTO var_oper_info, var_dop_oper_info, var_source_info, var_type_info, var_ko_info
      FROM dual;
  end if;
  --
  -- �������� ����� xml, ��������������� ����������
  --
  if par_parties is not null and par_parties.COUNT > 0 Then

    FOR var_i IN 1..par_parties.COUNT LOOP

      if par_parties(var_i) is not null Then
        SELECT xmlelement("MEMBER_INFO", xmlelement("BLOCK", to_char(var_i - 1)),
                                         xmlelement("MEMBER", xmlelement("TUREAL", par_parties(var_i).TUREAL),
                                                              xmlelement("PRU",    par_parties(var_i).PRU),
                                                              xmlelement("NAMEU",  par_parties(var_i).NAMEU),
                                                              xmlelement("KODCR",  par_parties(var_i).KODCR),
                                                              xmlelement("KODCN",  par_parties(var_i).KODCN),
                                                              xmlelement("AMR", xmlelement("ADRESS_S", par_parties(var_i).AMR_ADRESS_S),
                                                                                xmlelement("ADRESS_R", par_parties(var_i).AMR_ADRESS_R),
                                                                                xmlelement("ADRESS_G", par_parties(var_i).AMR_ADRESS_G),
                                                                                xmlelement("ADRESS_U", par_parties(var_i).AMR_ADRESS_U),
                                                                                xmlelement("ADRESS_D", par_parties(var_i).AMR_ADRESS_D),
                                                                                xmlelement("ADRESS_K", par_parties(var_i).AMR_ADRESS_K),
                                                                                xmlelement("ADRESS_O", par_parties(var_i).AMR_ADRESS_O)),
                                                              xmlelement("ADRESS", xmlelement("ADRESS_S", par_parties(var_i).ADR_ADRESS_S),
                                                                                   xmlelement("ADRESS_R", par_parties(var_i).ADR_ADRESS_R),
                                                                                   xmlelement("ADRESS_G", par_parties(var_i).ADR_ADRESS_G),
                                                                                   xmlelement("ADRESS_U", par_parties(var_i).ADR_ADRESS_U),
                                                                                   xmlelement("ADRESS_D", par_parties(var_i).ADR_ADRESS_D),
                                                                                   xmlelement("ADRESS_K", par_parties(var_i).ADR_ADRESS_K),
                                                                                   xmlelement("ADRESS_O", par_parties(var_i).ADR_ADRESS_O)),
                                                              xmlelement("KD", par_parties(var_i).KD),
                                                              xmlelement("SD", par_parties(var_i).SD),
                                                              xmlelement("RG", par_parties(var_i).RG),
                                                              xmlelement("ND", par_parties(var_i).ND),
                                                              xmlelement("VD", xmlelement("VD_1", par_parties(var_i).VD_1),
                                                                               xmlelement("VD_2", par_parties(var_i).VD_2),
                                                                               xmlelement("VD_3", par_parties(var_i).VD_3),
                                                                               xmlelement("VD_4", par_parties(var_i).VD_4),
                                                                               xmlelement("VD_5", par_parties(var_i).VD_5),
                                                                               xmlelement("VD_6", par_parties(var_i).VD_6),
                                                                               xmlelement("VD_7", par_parties(var_i).VD_7),
                                                                               xmlelement("MC_1", par_parties(var_i).MC_1),
                                                                               xmlelement("MC_2", par_parties(var_i).MC_2),
                                                                               xmlelement("MC_3", par_parties(var_i).MC_3)),
                                                              xmlelement("GR", par_parties(var_i).GR),
                                                              xmlelement("BP", par_parties(var_i).BP),
                                                              xmlelement("VP", par_parties(var_i).VP)),
                                         xmlelement("CO", xmlelement("ACC_B", par_parties(var_i).ACC_B),
                                                          xmlelement("ACC_COR_B", par_parties(var_i).ACC_COR_B),
                                                          xmlelement("NAME_IS_B", par_parties(var_i).NAME_IS_B),
                                                          xmlelement("BIK_IS_B",  par_parties(var_i).BIK_IS_B),
                                                          xmlelement("CARD_B",    par_parties(var_i).CARD_B),
                                                          xmlelement("NAME_B",    par_parties(var_i).NAME_B),
                                                          xmlelement("KODCN_B",   par_parties(var_i).KODCN_B),
                                                          xmlelement("BIK_B",     par_parties(var_i).BIK_B),
                                                          xmlelement("NAME_R",    par_parties(var_i).NAME_R),
                                                          xmlelement("KODCN_R",   par_parties(var_i).KODCN_R),
                                                          xmlelement("BIK_R",     par_parties(var_i).BIK_R)))
          INTO var_member_info
          FROM dual;

        if    var_i = 1 Then var_member_info_0 := var_member_info;
        elsif var_i = 2 Then var_member_info_1 := var_member_info;
        elsif var_i = 3 Then var_member_info_2 := var_member_info;
        elsif var_i = 4 Then var_member_info_3 := var_member_info;
        elsif var_i = 5 Then var_member_info_4 := var_member_info;
        end if;
      end if;
    END LOOP;
  end if;
  --
  -- �������� �������� XML, �������
  --
  select xmlelement("INFO_PART", var_type_info, 
                                 var_ko_info,
                                 var_oper_info,
                                 var_dop_oper_info,
                                 var_member_info_0,
                                 var_member_info_1,
                                 var_member_info_2,
                                 var_member_info_3,
                                 var_member_info_4,
                                 var_source_info)
    into var_result
    from dual;

  return var_result;
END get_xml_321p;
--
-- ��������� XML � ������������ � 321-� ��� ���������� ��������� (���)
--
FUNCTION get_xml_321p (
  par_oes_321_id    mantas.rf_oes_321.oes_321_id%TYPE
  )
RETURN XMLType IS

  var_oper           mantas.RF_TYPE_321P_OPER;
  var_parties        mantas.RF_TAB_321P_PARTY;
BEGIN
  --
  -- �������� ��� �� ����� 321-�
  --
  get_oes_321p(par_oes_321_id, var_oper, var_parties);
  --
  -- ��������� � ���������� XML
  --
  return get_xml_321p(var_oper, var_parties);
END get_xml_321p;
--
-- ��������� � xml-����� ��������� ��������� (���) �� ������� ��������� 321-�
--
PROCEDURE export_xml_321p
(
  par_oes_321_ids      mantas.RF_TAB_NUMBER -- ������ ��������������� ��������� (���) ��� �������� � ����
) IS

  var_review_ids               mantas.RF_TAB_NUMBER;
  var_clob                     CLOB;
  var_file_name                VARCHAR2(255 CHAR);
  var_exp_date                 DATE;
  var_operation_type           varchar(10 CHAR):='28000321';
  var_ASystem                  VARCHAR2(10 CHAR):='600';--���� ��������� �� ����������� ��� ��� ASYSTEM (�� ������ ������ 600- BackOffice)
BEGIN
  var_exp_date:=SYSDATE;--���� �������� ���� ���������� ��������� ����

  FOR r in (select --rf_opok_nb,
                   xmlelement("MAIN",xmlattributes('fns.esb.sbrf.ru' as "xmlns",  'http://www.w3.org/2001/XMLSchema-instance' as "xmlns:xsi", 'fns.esb.sbrf.rutest.xsd' as "xsi:schemaLocation")
                                    ,xmlelement("WORK",xmlelement("REPORT", 'FIN_MONITORING')
                                                      ,xmlelement("DESCRIPTION",'�������� �� �� ������-��������� (������ 321-�)')
                                                      ,xmlelement("DATE",TO_CHAR(var_exp_date,'YYYY-MM-DD'))
                                                      ,xmlelement("MESSAGEID",'1')
                                                      ,xmlelement("ASYSTEM",var_ASystem)
                                                      ,xmlelement("DEPARTMENT",nvl(rvo.owner_id, 'OFM')) -- ����� �������������� ����������
                                                )
                                    ,xmlagg(get_xml_321p(oes.oes_321_id))
                                    ,xmlelement("SENDER",xmlelement("TELEPHON", '')
                                                        ,xmlelement("FIO", '')
                                                        ,xmlelement("POST", '')
                                               )
                             ).getclobval() as xml_data,
                   nvl(rvo.owner_id, 'OFM') as department
              from table(par_oes_321_ids) ids,
                   mantas.rf_oes_321 oes,
                   mantas.kdd_review rv,
                   mantas.kdd_review_owner rvo
             where oes.oes_321_id = ids.column_value and
                   rv.review_id = oes.review_id and
                   rvo.owner_seq_id (+)= rv.owner_seq_id
            group by --rf_opok_nb, 
                     nvl(rvo.owner_id, 'OFM')) loop

    var_clob := '<?xml version = "1.0" encoding = "windows-1251"?>'||r.xml_data;
    --
    --�������� XML  � ����
    --
    --������������ ����� �����
    var_file_name:=var_operation_type||--��� �������� 28000321
                   var_ASystem||SUBSTR('__________',1,10-length(var_ASystem))||--���� ��������� �� ����������� ��� ��� ASYSTEM, ����������� ������ ������ �_� �� 10-�� ��������
                   r.department||SUBSTR('____________',1,12-length(r.department))||--����� �������������� ����������, ����������� ������ ������ �_� �� 12-�� ��������
                   TO_CHAR(var_exp_date,'YYYY-MM-DD_HHMISS')||--������� ������������ ���� � ������� YYYY-MM-DD
                   --to_char(r.rf_opok_nb)||
                   '.xml';-- ��� ����
                   
    dbms_xslprocessor.clob2file(var_clob, 'AML_EXP_321P_DIR', var_file_name, 171 /*CL8MSWIN1251*/);
    --
    -- ������� �������������� ����������� � ���� �������
    --
    select rv.review_id
      bulk collect into var_review_ids
      from table(par_oes_321_ids) ids,
           mantas.rf_oes_321 oes,
           mantas.kdd_review rv,
           mantas.kdd_review_owner rvo
     where oes.oes_321_id = ids.column_value and
           rv.review_id = oes.review_id and
           rvo.owner_seq_id (+)= rv.owner_seq_id and
           --rv.rf_opok_nb = r.rf_opok_nb and
           nvl(rvo.owner_id, 'OFM') = r.department;
    --
    -- ������� ������, ������� ����������� � �������� ��� ����������� �������    
    --
    mantas.rf_pkg_review.set_reviews_exported(var_review_ids, var_file_name);

  END LOOP;
END export_xml_321p;
--
-- ��������� ���� ��������� � ������������ � 321-� ��� ���������� ������
--
PROCEDURE get_oes_321p_by_alert (
  par_review_id    IN  mantas.kdd_review.review_id%TYPE,
  par_oper        OUT  mantas.RF_TYPE_321P_OPER,
  par_parties     OUT  mantas.RF_TAB_321P_PARTY
  ) IS

  var_oes_parties_tx     mantas.rf_opok.oes_parties_tx%TYPE;
  var_oes_parties_def_tx mantas.rf_opok.oes_parties_tx%TYPE;

  var_dbt_cdt_cd         VARCHAR2(1 CHAR);
  var_tb_id              mantas.rf_oes_321_party_org.tb_code%TYPE;
  var_osb_id             mantas.rf_oes_321_party_org.osb_code%TYPE;
  var_trxn_dt            DATE;
  var_interkgrko_flag    INTEGER;
  var_rv_kgrko_id        mantas.rf_oes_321_org.numbf_s%TYPE;
  
  var_party              mantas.RF_TYPE_321P_PARTY;
  var_exists_fl          INTEGER;
  var_b_flag             VARCHAR2(1 CHAR);
BEGIN
  --
  -- �������������
  --
  par_oper := mantas.RF_TYPE_321P_OPER(null, null, null, null, null, null, null, null, null, null,
                                       null, null, null, null, null, null, null, null, null, null,
                                       null, null, null, null, null, null, null, null, null, null,
                                       null, null, null, null, null, null, null, null, null, null, 
                                       null, null, null, null, null, null, null);
  par_parties := mantas.RF_TAB_321P_PARTY();
  -- �������� 5 ���������� (! ������� 1-5 ������ ������� ������ 0-4 !)
  par_parties.extend();
  par_parties.extend();
  par_parties.extend();
  par_parties.extend();
  par_parties.extend();
  --
  -- ������� �������� ����� ���������, ����������� � ��������
  --
  SELECT rv.op_cat_cd, rv.trxn_seq_id, rv.rv_trxn_scrty_seq_id, rv.review_id,
         case when rv.opok_nb = 7001 then '2' else '0' end as TERROR,
         to_char(rv.opok_nb) as VO,
         nvl(replace(rv.add_opoks_tx, ' '), '0') as DOP_V,
         to_char(trunc(rv.trxn_dt), 'yyyy-mm-dd') as DATA,
         case when rv.rv_object_tp_cd = 'TRXN_SCRTY' -- ���� ��� ��������� ��������� �� ������ ������, �� ����� - ��� ����� ������ ������
              then nvl(trim(to_char(rv.scrty_base_am, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,''')), '0')
              else case when rv.opok_nb = 1003 and rv.trxn_conv_base_am is not null and -- ���� ��� ��������� ����� � �������� ���������� ���������� ������ ���� ������ - ������ ���������� ����������� ������
                             rv.trxn_base_am < (select nvl(min(limit_am), 600000) 
                                                  from mantas.rf_opok_details 
                                                 where opok_nb = rv.opok_nb and
                                                       trunc(rv.trxn_dt) between nvl(start_dt, to_date('01.01.0001', 'dd.mm.yyyy')) and
                                                                                 nvl(end_dt, to_date('01.01.9999', 'dd.mm.yyyy')))  
                        then nvl(trim(to_char(rv.trxn_conv_base_am, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,''')), '0')
                        else nvl(trim(to_char(rv.trxn_base_am, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,''')), '0')
                   end                                  
         end as SUME,
         case when rv.rv_object_tp_cd = 'TRXN_SCRTY' -- ���� ��� ��������� ��������� �� ������ ������, �� ����� - ��� ����� ������ ������
              then case when nvl(trim(rv.scrty_price_crncy_cd), 'RUR') = 'RUR'
                        then nvl(trim(to_char(rv.scrty_base_am, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,''')), '0') -- �������� ����� = �������� 
                        else nvl(trim(to_char(rv.scrty_price, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,''')), '0')   -- �������� ����� = ���� ������� ������ ������
                   end
              else nvl(case when crn.metal_flag = 1 -- ���� ����. ������, �� �������� �����
                            then trim(to_char(rv.trxn_base_am,  '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''))
                            else trim(to_char(rv.trxn_crncy_am, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''))
                       end, '0') 
         end as SUM,
         case when crn.metal_flag = 1 or rv.trxn_crncy_cd = 'RUR' -- ���� ����. ������, �� �����
              then '643'
              else coalesce(to_char(crn.iso_num), rv.trxn_crncy_cd, '0')
         end as CURREN,
         case when rv.src_sys_cd = 'GAT'
              then substr(nvl(trim(nvl(optp.op_prim_tx, optp.op_type_nm)||' '||rv.trxn_desc||' '||
                                   business.rf_pkg_util.get_trxn_scrty_desc(rv.rv_op_cat_cd, rv.rv_trxn_seq_id, rv.rv_trxn_scrty_seq_id)), '0'), 1, 2000)
              else substr(nvl(trim(coalesce(rv.trxn_desc, optp.op_prim_tx, optp.op_type_nm)||' '||
                                   business.rf_pkg_util.get_trxn_scrty_desc(rv.rv_op_cat_cd, rv.rv_trxn_seq_id, rv.rv_trxn_scrty_seq_id)), '0'), 1, 2000) 
         end as PRIM,         
         case when trim(rv.trxn_doc_id) is null or -- ���������� ����� ���������� �������� � 0, ���� �� ������� ������� ��� ����������. ����� ��������� ��������� � REFER_R2.
                   length(trim(rv.trxn_doc_id)) > 12 or
                   translate(trim(rv.trxn_doc_id), 'a1234567890', 'a') is not null
              then '0'
              else trim(rv.trxn_doc_id)
         end as NUM_PAY_D,                        
         case when nvl(rv.trxn_doc_id, '0') = '0' -- ���� ��� ������ ����. ��������� - �� ����������� ����
              then '2099-01-01'
              else nvl(to_char(trunc(rv.trxn_doc_dt), 'yyyy-mm-dd'), '2099-01-01') 
         end as DATE_PAY_D,
         decode(crn.metal_flag, 1, NVL(crn.metal_code, substr('A'||to_char(crn.iso_num), 1, 3)), '0') as METAL,
         '0' as PRIZ6001,
         '0' as B_PAYER, -- ���� ������ 0, ����� ��������� � ������������ � ����������� (������� �����������: 1 - ������, 2 - ��� �� ��, 0 - ����)
         '0' as B_RECIP, -- ���� ������ 0, ����� ��������� � ������������ � ����������� (������� ����������:  1 - ������, 2 - ��� �� ��, 0 - ����)
         '0' as PART,
         substr(nvl(trim(case when rv.bank_card_trxn_dt is not null
                              then '���� �������� �� ���������� �����: '||to_char(rv.bank_card_trxn_dt, 'dd.mm.yyyy hh24:mi:ss')
                         end||' '||
                         business.rf_pkg_util.get_trxn_scrty_desc(rv.rv_op_cat_cd, rv.rv_trxn_seq_id, rv.rv_trxn_scrty_seq_id)), 
                    '0'), 1, 2000) as DESCR, 
         substr(decode(rv.trxn_conv_crncy_cd, 'RUR', '643', coalesce(to_char(conv_crn.iso_num), rv.trxn_conv_crncy_cd, '0')), 1, 3) as CURREN_CON,
         nvl(trim(to_char(rv.trxn_conv_am, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,''')), '0') as SUM_CON,
         case when rv.opok_nb = 5005 then '1' else '0' end as PRIZ_SD, -- 0 - ������, 1 - ���� ��������� (��� �����, ��� ��� ???)         
         case when rv.opok_nb in (6001, 8001, 5003, 5007)
              then to_char(trunc(SYSDATE), 'yyyy-mm-dd')
              else '2099-01-01' 
         end as DATE_S, -- ���� ��������� (6001), ����, ����� �� �� ����� �������� � ... (5003,5007,8001)         
         '0' as PRMANUAL, -- ������� ������� ���������� ��������
         '0' as COMMENT_, -- ����������� � ������� ���������� ��������
         '0' as OPERATOR_M, -- ��� � ��������� ���������, ���������� �������� �������
         rv.TB_ID||DECODE(rv.OSB_ID,'0',NULL,rv.OSB_ID) as FILIAL, -- ��� �������������, � ������� ��������� �������� - ������������ ������� (��� ��������): �� ��� ���
         '600' as ASYSTEM, -- ��� �������-��������� ���������
         to_char(rv.review_id) as idOperSystem, -- ���������� ID �������� � ������� (BackOffice ����������� ���������� ����� ��������� � �������)
         '0' as OPERATOR_S, -- ��� � ��������� ��������� (������ ����������� �������� ������������, Back - 0)\
         '2' as VERSION_P,
         null as ACTION, --������ ��� ��� ������ ������� ���������� create_oes_321p
         '0' as NUMB_P,
         null as DATE_P,
         nvl(org.tel, '0') as TEL,
         nvl(org.regn, '0') as REGN,
         nvl(org.nd_ko, '0') as ND_KO,
         nvl(org.ktu_s, '0') as KTU_S,
         nvl(org.bik_s, '0') as BIK_S,
         nvl(org.numbf_s, '0') as NUMBF_S,
         nvl(org.branch_fl, '0') as BRANCH,
         nvl(org.ktu_ss, '0') as KTU_SS,
         nvl(org.bik_ss, '0') as BIK_SS,
         nvl(org.numbf_ss, '0') as NUMBF_SS,
         org.oes_321_org_id as oes_321_org_id, 
         --����� ���������� ���������, ���� �� ������� ������� ��� ����������.
         case 
           when length(trim(rv.trxn_doc_id)) > 12 or translate(trim(rv.trxn_doc_id), 'a1234567890', 'a') is not null then substr(trim(rv.trxn_doc_id), 1, 2000)
           else '0'
         end as refer_r2,
         case when rv.opok_nb = 5005
              then case when rv.op_cat_cd in ('CT', 'BOT') and rv.dbt_cdt_cd = 'D' -- �������� �� ����� ��� ��� ���� ������� �������� ����. �������
                        then '0-ORIG,1-SCND_ORIG,3-SBRF'
                        when rv.op_cat_cd in ('CT', 'BOT') and rv.dbt_cdt_cd = 'C' -- ���������� �� ���� ��� ��� ���� ������� �������� ����. �������
                        then '0-SBRF,2-SCND_BENEF,3-BENEF'
                        when rv.op_cat_cd = 'WT' and rv.orig_acct_nb like '203%' and nvl(rv.benef_acct_nb, '?') not like '203%' -- �������� �� ����� ���
                        then '0-ORIG,1-SCND_ORIG,3-SBRF'
                        when rv.op_cat_cd = 'WT' and nvl(rv.orig_acct_nb, '?') not like '203%' and rv.benef_acct_nb like '203%' -- ���������� �� ���� ���
                        then '0-SBRF,2-SCND_BENEF,3-BENEF'
                        else '0-ORIG,1-SCND_ORIG,2-SCND_BENEF,3-BENEF' -- �������� ��� ������� ���-������ ��� � �������� ���� ���-������
                   end
         end as oes_parties_tx, -- ������������� ������ ������������ ������ ��������� � ����� ���������� ��������
         case when rv.op_cat_cd = 'CT' and rv.dbt_cdt_cd = 'D' and orig_own_fl = 'Y' -- ��������� �������� �� �����
              then '0-ORIG,1-SCND_ORIG,2-SCND_ORIG,3-ORIG' -- ������ � � �����������, � � ���������� ��������� �����
              when rv.op_cat_cd = 'CT' and rv.dbt_cdt_cd = 'C' and benef_own_fl = 'Y' -- �������� �������� �� ����
              then '0-BENEF,1-SCND_BENEF,2-SCND_BENEF,3-BENEF' -- ������ � � �����������, � � ���������� ��������� �����
              when rv.op_cat_cd = 'BOT' and rv.dbt_cdt_cd = 'D' and orig_own_fl = 'Y' -- ���������� �������� �� ����� (������)
              then '0-ORIG,1-SCND_ORIG,3-SBRF' -- ������ � ���������� ��������
              when rv.op_cat_cd = 'BOT' and rv.dbt_cdt_cd = 'C' and benef_own_fl = 'Y' -- ���������� ���������� �� ���� (������)
              then '0-SBRF,2-SCND_BENEF,3-BENEF' -- ������ � ����������� ��������
              else '0-ORIG,1-SCND_ORIG,2-SCND_BENEF,3-BENEF'  
         end as oes_parties_def_tx, -- ������ ������������ ������ ��������� � ����� ���������� �������� �� ���������
         rv.dbt_cdt_cd as dbt_cdt_cd,
         rv.tb_id   as tb_id,
         rv.osb_id  as osb_id,
         rv.trxn_dt as trxn_dt,
         case when rv.send_kgrko_id <> rv.rcv_kgrko_id -- ������ �������� ����� - ������ � ��������
              then 1
              else 0       
         end as interkgrko_flag,
         rv.rv_kgrko_id as rv_kgrko_id
    INTO par_oper.op_cat_cd, par_oper.trxn_seq_id, par_oper.trxn_scrty_seq_id, par_oper.review_id,
         par_oper.TERROR, par_oper.VO, par_oper.DOP_V, par_oper.DATA, par_oper.SUME,  par_oper.SUM, par_oper.CURREN, par_oper.PRIM,
         par_oper.NUM_PAY_D, par_oper.DATE_PAY_D, par_oper.METAL, par_oper.PRIZ6001, par_oper.B_PAYER, par_oper.B_RECIP,
         par_oper.PART, par_oper.DESCR, par_oper.CURREN_CON, par_oper.SUM_CON, par_oper.PRIZ_SD, par_oper.DATE_S,
         par_oper.PRMANUAL, par_oper.COMMENT_, par_oper.OPERATOR_M,
         par_oper.FILIAL, par_oper.ASYSTEM, par_oper.idOperSystem, par_oper.OPERATOR_S,
         par_oper.VERSION_P,par_oper.ACTION,par_oper.NUMB_P,par_oper.DATE_P,par_oper.TEL,
         par_oper.REGN,par_oper.ND_KO,par_oper.KTU_S,par_oper.BIK_S,par_oper.NUMBF_S,par_oper.BRANCH,par_oper.KTU_SS,par_oper.BIK_SS,par_oper.NUMBF_SS,par_oper.oes_321_org_id,
         par_oper.refer_r2,
         var_oes_parties_tx, var_oes_parties_def_tx, var_dbt_cdt_cd, var_tb_id, var_osb_id, var_trxn_dt, var_interkgrko_flag, var_rv_kgrko_id
    FROM mantas.rfv_opok_review rv
         left join business.rf_currency crn on crn.iso_alfa = rv.trxn_crncy_cd
         left join business.rf_currency conv_crn on conv_crn.iso_alfa = rv.trxn_conv_crncy_cd
         left join business.rf_nsi_op_types optp on optp.nsi_op_id = rv.nsi_op_id
         left join mantas.rf_oes_321_org org on org.oes_321_org_id = rv.rv_oes_org_id
   WHERE review_id = par_review_id;
  --
  -- ������� �������� ����� ���������, ����������� � ���������� ��������
  --
  FOR r in (SELECT opk.opok_nb,
                   substr(trim(blk.column_value), 1, 1) as block_nb,
                   substr(trim(blk.column_value), 3) as party_cd,
                   case when substr(trim(blk.column_value), 3) = lag(substr(trim(blk.column_value), 3)) over(order by substr(trim(blk.column_value), 3), substr(trim(blk.column_value), 1, 1))
                        then 1
                        else 0
                   end same_party_fl, -- ���� 0/1 - �������� �������� ��� ��, ��� � � ���������� ������
                   opk.oes_parties_tx
              FROM mantas.rf_opok opk,
                   table(mantas.rf_pkg_scnro.list_to_tab(coalesce(var_oes_parties_tx, opk.oes_parties_tx, var_oes_parties_def_tx))) blk
             WHERE opk.opok_nb = par_oper.VO
            ORDER BY 3, 2  /*������� ���������� ����� - ����������� - ���������� ����� ���� ������, ������ ���������� � �������*/) LOOP
    --
    -- ������ �� �������� ���������
    --
    if r.party_cd is null or r.party_cd not in ('ORIG', 'SCND_ORIG', 'SCND_BENEF', 'BENEF', 'SBRF') or
       r.block_nb is null or r.block_nb not in ('0', '1', '2', '3', '4') Then
       raise_application_error(-20001, '�������� ��������� �������� ���������� �������� � ������ ��������� 321-� ��� ���� ����: '||to_char(r.opok_nb)||
                                       ', ������ ���������: '||r.oes_parties_tx||'. ��������� ������ ����: 0-ORIG,1-SCND_ORIG,2-SCND_BENEF,3-BENEF,4-SBRF (rf_pkg_oes.get_oes_321p_by_alert)');
    end if;

    if r.same_party_fl <> 1 Then -- ���� ��� �� ��� �� ��������, ��� � �� ���������� �������� ����� - ��������� ���� ������

      var_party := mantas.RF_TYPE_321P_PARTY(null, null, null, null, null, null, null, null, null, null,
                                             null, null, null, null, null, null, null, null, null, null,
                                             null, null, null, null, null, null, null, null, null, null,
                                             null, null, null, null, null, null, null, null, null, null,
                                             null, null, null, null, null, null, null, null, null, null);

      if r.party_cd = 'SBRF' Then 
        --
        -- ����������� �������� ���������������� ��
        --
        set_oes_321_party_tb(var_tb_id, var_osb_id, var_trxn_dt, 'FULL', var_party);

        var_b_flag := '2';
        var_exists_fl := 1;
        
      else
        SELECT /*case when -- ������ �������� ��������� ������������ � ���'�� ��, ��������������� ��������
                         cust.rf_bic_nb = par_oper.BIK_S or cust.rf_bic_nb = par_oper.BIK_SS or
                         -- ��� ��� ������� ��������� � ��� ��, ��������������� �������� � 
                         --     ��� ����� ��������� ��������� � ���'�� ��, ��������������� ��������
                         (rvp.inn_nb = par_oper.ND_KO and 
                          (rvp.instn_id = par_oper.BIK_S or rvp.INSTN_ID = par_oper.BIK_SS))
                    then '2' -- ��� ����
                    else nvl(rvp.IS_CLIENT, '0')
               end as b_flag,*/
               case when -- ��� ������� ��������� � ��� �� ��
                         rvp.inn_nb = par_oper.ND_KO or 
                         -- ������ �������� ��������� ������������ � ���'�� �� ��                 
                         (cust.rf_bic_nb is not null and mantas.rf_pkg_rule.is_sbrf(cust.rf_bic_nb) = 1)                         
                    then '2' -- ��� ����
                    else nvl(rvp.IS_CLIENT, '0')
               end as b_flag,                         
               rvp.TUREAL, --��� ��������� �������� 1-��, 2-��, 3-��, 4-���������� (������ ��� "�������")
               case
                 when SUBSTR(rvp.opok_nb,1,2) = '30' --���� ��������� � ���������� �� ����������� �������������� ��������������...
                 then case
                        --�������� ������ �����������
                        when nvl(cntry_cr.iso_numeric_cd, '643') = '364' --����� ���������� ���� ������
                          or (mantas.rf_pkg_scnro.check_lexeme(NVL(rvp.ADR_SETTLM_NM,rvp.ADR_CITY_NM)||rvp.adr_state_nm||rvp.ADR_STREET_NAME_TX,'����,���,iri,irn,iran,tehran,�������,������,������,������,�����,�������,�����') = 1)
                        then '1'||'364'||'00' -- � ������ ���� ������ ����������� ����
                        when nvl(cntry_cr.iso_numeric_cd, '643') = '408' --����� ���������� ���� ������
                          or (mantas.rf_pkg_scnro.check_lexeme(NVL(rvp.ADR_SETTLM_NM,rvp.ADR_CITY_NM)||rvp.adr_state_nm||rvp.ADR_STREET_NAME_TX,'����,����,prk,pyongyang,dprk,korea,�������,������,������,�����,������,��������') = 1)
                        then '1'||'408'||'00' -- � ������ ���� ������ ����������� �������� �����
                        --�������� ������ ����������
                        when nvl(cntry_cn.iso_numeric_cd, '643') = '364' --����� ���������� ���� ������
                          or (mantas.rf_pkg_scnro.check_lexeme(NVL(rvp.ADH_SETTLM_NM,rvp.ADH_CITY_NM)||rvp.adh_state_nm||rvp.ADH_STREET_NAME_TX,'����,���,iri,irn,iran,tehran,�������,������,������,������,�����,�������,�����') = 1)
                        then '2'||'364'||'00' -- � ������ ���� ������ ����������� ����
                        when nvl(cntry_cn.iso_numeric_cd, '643') = '408' --����� ���������� ���� ������
                          or (mantas.rf_pkg_scnro.check_lexeme(NVL(rvp.ADH_SETTLM_NM,rvp.ADH_CITY_NM)||rvp.adh_state_nm||rvp.ADH_STREET_NAME_TX,'����,����,prk,pyongyang,dprk,korea,�������,������,������,�����,������,��������') = 1)
                        then '2'||'408'||'00' -- � ������ ���� ������ ����������� �������� �����
                        --�������� ��������������� �����
                        when nvl(bnk.iso_numeric_cd, '643') = '364' --����� ���������� ���� ������
                          or (mantas.rf_pkg_scnro.check_lexeme(NVL(rvp.ADR_SETTLM_NM,rvp.ADR_CITY_NM)||rvp.adr_state_nm||rvp.ADR_STREET_NAME_TX,'����,���,iri,irn,iran,tehran,�������,������,������,������,�����,�������,�����') = 1)
                        then '3'||'364'||'00'
                        when nvl(bnk.iso_numeric_cd, '643') = '364' --����� ���������� ���� ������
                          or (mantas.rf_pkg_scnro.check_lexeme(NVL(rvp.ADR_SETTLM_NM,rvp.ADR_CITY_NM)||rvp.adr_state_nm||rvp.ADR_STREET_NAME_TX,'����,���,iri,irn,iran,tehran,�������,������,������,������,�����,�������,�����') = 1)
                        then '3'||'364'||'00'
                        else '0'
                      end
                 when SUBSTR(rvp.opok_nb,1,2) = '70' --���� ��������� �� ������������
                 then '0'
                 else '0'
               end as PRU, --������� ��������� ��������
               SUBSTR(nvl(case when rvp.TUREAL = '3' /*��*/
                               then coalesce(rvp.cust_fl_nm, -- ��� ��, ���������������� �� 
                                             trim(regexp_replace(replace(rvp.cust_nm, '�� '), 
                                                                 '��������������\s+���������������|���.\s+���������������', 
                                                                 '', 1, 0, 'i')), -- ������������ �� � ��������� ������� ����: "�������������� ���������������"
                                             rvp.cust_nm) -- ������������ �� ��� ���������
                               else rvp.cust_nm
                          end, '0'),
                      1, 2000) as NAMEU, --������������ ��������� ��������
               nvl(nullif(case when rvp.TUREAL in ('2', '3') /*��,��*/
                               then nvl(fl_cntry_cr.iso_numeric_cd, cntry_cr.iso_numeric_cd)    -- ������ ���������� (����� �����������) ��
                               when rvp.TUREAL in ('1') /*��*/
                               then nvl(cntry_ctz.iso_numeric_cd, cntry_cr.iso_numeric_cd)      -- ������ ����������� ��
                               else cntry_cr.iso_numeric_cd  
                          end||'00', 
                          '00'), '0') as KODCR, -- ��� ������ ����������� ���������
               nvl(nullif(case when rvp.TUREAL in ('2', '3') /*��,��*/
                               then cntry_ctz.iso_numeric_cd                                    -- �����������
                               else cntry_cn.iso_numeric_cd                                     -- ������ ���������� �� (����� �����������)
                          end||'00',
                          '00'), '0') as KODCN, -- ��� ������ ���������� ���������/����������� (��� ��/��)
               nvl(SUBSTR(state_cr.okato_cd,1,2), '0') as AMR_ADRESS_S, -- ��� �������� ���������� ��������� �� ����� ??? ���� �������� ������������ ������� �/��� ������ �����, ���� ��� ������ �� ������ ???
               SUBSTR(nvl(rvp.ADR_DISTRICT_NM,'0'),1,2000) as AMR_ADRESS_R, --����� (������) ���������������� � ���������� ��������               
               SUBSTR(coalesce(case when rvp.ADR_SETTLM_NM is not null and rvp.ADR_SETTLM_TYPE_CD is not null
                                    then rvp.ADR_SETTLM_TYPE_CD||'. '||rvp.ADR_SETTLM_NM
                                    else rvp.ADR_SETTLM_NM
                               end,
                               case when rvp.ADR_CITY_NM is not null and rvp.ADR_CITY_TYPE_CD is not null
                                    then rvp.ADR_CITY_TYPE_CD||'. '||rvp.ADR_CITY_NM
                                    else rvp.ADR_CITY_NM
                               end,
                               '0'),
                      1, 2000) as AMR_ADRESS_G, --���������� �����
               SUBSTR(nvl(case when rvp.ADR_STREET_NAME_TX is not null and rvp.ADR_STREET_TYPE_TX is not null
                               then rvp.ADR_STREET_TYPE_TX||'. '||rvp.ADR_STREET_NAME_TX
                               else rvp.ADR_STREET_NAME_TX
                          end, '0'),
                      1, 2000) as AMR_ADRESS_U, --������������ �����
               SUBSTR(nvl(rvp.ADR_HOUSE_NUMBER_TX,'0'),1,2000) as AMR_ADRESS_D, --����� ���� (����� ��������)
               SUBSTR(nvl(rvp.ADR_BUILDING_NUMBER_TX,'0'),1,2000) as AMR_ADRESS_K, --����� ������� (����� ��������)
               SUBSTR(nvl(rvp.ADR_FLAT_NUMBER_TX,'0'),1,2000) as AMR_ADRESS_O, --����� ����� (����� ��������)
               nvl(SUBSTR(state_cn.okato_cd,1,2), '0') as ADR_ADRESS_S, --��� �������� ���������� ��������� �� ����� ??? ���� �������� ������������ ������� �/��� ������ �����, ���� ��� ������ �� ������ ???
               SUBSTR(nvl(rvp.ADH_DISTRICT_NM,'0'),1,2000) as ADR_ADRESS_R,             --����� (������) ���������������� � ���������� ��������               
               SUBSTR(coalesce(case when rvp.ADH_SETTLM_NM is not null and rvp.ADH_SETTLM_TYPE_CD is not null
                                    then rvp.ADH_SETTLM_TYPE_CD||'. '||rvp.ADH_SETTLM_NM
                                    else rvp.ADH_SETTLM_NM
                               end,
                               case when rvp.ADH_CITY_NM is not null and rvp.ADH_CITY_TYPE_CD is not null
                                    then rvp.ADH_CITY_TYPE_CD||'. '||rvp.ADH_CITY_NM
                                    else rvp.ADH_CITY_NM
                               end,
                               '0'),
                      1, 2000) as ADR_ADRESS_G, --���������� �����
               SUBSTR(nvl(case when rvp.ADH_STREET_NAME_TX is not null and rvp.ADH_STREET_TYPE_TX is not null
                               then rvp.ADH_STREET_TYPE_TX||'. '||rvp.ADH_STREET_NAME_TX
                               else rvp.ADH_STREET_NAME_TX
                          end, '0'),
                      1, 2000) as ADR_ADRESS_U, --������������ �����
               SUBSTR(nvl(rvp.ADH_HOUSE_NUMBER_TX,'0'),1,2000) as ADR_ADRESS_D,--����� ���� (����� ��������)
               SUBSTR(nvl(rvp.ADH_BUILDING_NUMBER_TX,'0'),1,2000) as ADR_ADRESS_K,--����� ������� (����� ��������)
               SUBSTR(nvl(rvp.ADH_FLAT_NUMBER_TX,'0'),1,2000) as ADR_ADRESS_O,--����� ����� (����� ��������)
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/  
                               then '0'
                               when replace(trim(rvp.DOCUMENT_SERIES), '0')||rvp.DOCUMENT_NO||
                                    rvp.DOCUMENT_ISSUED_DPT_TX||rvp.DOCUMENT_ISSUED_DPT_CD is not null or
                                    rvp.DOCUMENT_ISSUED_DT is not null 
                               then nvl(iddoctp.cbrf_cd, '21')  -- ����������� ������� �� �� ���������, ���� ���� ���
                               else nvl(iddoctp.cbrf_cd, '0') 
                          end ,'0'), 1, 2) as KD, --��� ���������, ��������������� ��������
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/  then case when regexp_like(trim(rvp.OKPO_NB), '^0+$') 
                                                                       then '0' /*���� ���� ����, �� 0*/ 
                                                                       else rvp.OKPO_NB 
                                                                  end 
                                                             else case when regexp_like(trim(rvp.DOCUMENT_SERIES), '^0+$') 
                                                                       then '0' /*���� ���� ����, �� 0*/ 
                                                                       else rvp.DOCUMENT_SERIES 
                                                                  end end ,'0'),1,2000) as SD, --���� ��� ����� ���������(��� ��/��)
               SUBSTR(nvl(case when rvp.TUREAL <> '2' /*��*/ then case when regexp_like(trim(rvp.OGRN_NB), '^0+$') 
                                                                       then '0' /*���� ���� ����, �� 0*/ 
                                                                       else rvp.OGRN_NB 
                                                                  end 
                                                             else '0' end ,'0'),1,2000) as RG, --���� ��� ������
               SUBSTR(case when regexp_like(trim(rvp.INN_NB), '^0+$') 
                           then '0' /*���� ���� ����, �� 0*/ 
                           else nvl(trim(rvp.INN_NB),'0') end, 1, 2000) as ND, --��� ��� ��� ����������� ����������� (���)
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ then '0' else rvp.DOCUMENT_NO end ,'0'),1,2000) as VD_1, --����� ��������� (��,��)
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ then '0' else rvp.document_issued_dpt_tx||nvl2(rvp.document_issued_dpt_cd, ', '||rvp.document_issued_dpt_cd, null) end ,'0'),1,2000) as VD_2, --������������ ������, ��������� ��������(��,��) + ��� �������������
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ then '2099-01-01' else to_char(rvp.DOCUMENT_ISSUED_DT, 'yyyy-mm-dd') end,'2099-01-01'),1,10) as VD_3, --���� ������ ���������(��,��)
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ or NVL(rvp.RESIDENT_FL,'Y') ='Y' then '0' else iddoctp_stay.cbrf_cd end ,'0'),1,2) as VD_4, -- ??? ��� ���������, ��������������� ����� ����������, ���� - ��� �������� ???
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ or NVL(rvp.RESIDENT_FL,'Y') ='Y' then '0' else iddoc_stay.series_id||iddoc_stay.no_id end ,'0'),1,2000) as VD_5, -- �� iddoc_stay --����� � ����� ���������, ��������������� ����� �� ���������� (����������) � ���������� ���������.
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ or NVL(rvp.RESIDENT_FL,'Y') ='Y' then '2099-01-01' else to_char(iddoc_stay.issue_dt, 'yyyy-mm-dd') end,'2099-01-01'),1,10) as VD_6, --���� ������ ����� �������� ����� �� ���������� (����������) � ���������� ���������.
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ or NVL(rvp.RESIDENT_FL,'Y') ='Y' then '2099-01-01' else to_char(iddoc_stay.end_dt, 'yyyy-mm-dd') end,'2099-01-01'),1,10) as VD_7, --���� ��������� ����� �������� ����� �� ���������� (����������) � ���������� ���������.
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ then '0' else iddoc_mc.series_id||iddoc_mc.no_id end, '0'),1,2000) as MC_1, -- ����� ������������ �����
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ then '2099-01-01' else to_char(iddoc_mc.issue_dt, 'yyyy-mm-dd') end, '2099-01-01'),1,10) as MC_2, -- ���� ������ �������� ��
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ then '2099-01-01' else to_char(iddoc_mc.end_dt, 'yyyy-mm-dd') end, '2099-01-01'),1,10) as MC_3,  --���� �������� �������� ��
               SUBSTR(nvl(case when rvp.TUREAL in ('2', '3') /*��,��*/ 
                               then to_char(rvp.birth_dt,'yyyy-mm-dd') 
                               else to_char(rvp.reg_dt,'yyyy-mm-dd')
                          end, '2099-01-01'),1,10) as GR, -- ���� ��������/����������� ??? ��������� ��� ������� ��, ��/�� ???
               SUBSTR(nvl(case when rvp.TUREAL = '1' /*��*/ then '0' else rvp.birthplace_tx end ,'0'),1,2000) as BP,   -- ����� ��������
               '0' as VP, -- 1 - ���� �������������������, ���������������, 2 - ����, ������������� �� ���������, 0 - ����
               SUBSTR(nvl(rvp.ACCT_NB,'0'),1,2000) as ACC_B, --��������� ���� ��������� � ������������� ��� ��������� �����������
               SUBSTR(nvl(case when rvp.OP_CAT_CD = 'WT' then rvp.INSTN_ACCT_ID end,'0'),1,2000) as ACC_COR_B, --����� ������������������ �����               
               nvl(case when rvp.BANK_CARD_ID_NB is not null or rvp.BANK_CARD_TRXN_DT is not null
                        then SUBSTR(rvp.INSTN_NM, 1, 2000)
                   end, '0') as NAME_IS_B, --������������ ��������� ����������� - �������� ���������� �����
               nvl(case when rvp.BANK_CARD_ID_NB is not null or rvp.BANK_CARD_TRXN_DT is not null
                        then SUBSTR(rvp.INSTN_ID, 1, 2000)
                   end, '0') as BIK_IS_B,  --��� S.W.I.F.T. ��������� ����������� - �������� ���������� �����               
               case when rvp.BANK_CARD_ID_NB is null and rvp.BANK_CARD_TRXN_DT is null
                    then '0'
                    when rvp.IS_CLIENT = '1'
                    then '1'
                    else '2'  
               end as CARD_B, --������� ��������� ���������� �����
               SUBSTR(nvl(rvp.INSTN_NM, '0'), 1, 2000) as NAME_B, --������������ �����
               nvl2(bnk.iso_numeric_cd, bnk.iso_numeric_cd||'00', '0') as KODCN_B, --��� ���������� ��������������� �����
               SUBSTR(nvl(rvp.INSTN_ID, '0'), 1, 2000) as BIK_B, -- ��� / S.W.I.F.T.�����
               '0' as NAME_R,  --������������ ����� �� �������� ���������� ������
               '0' as KODCN_R, --��� ���������� ����� �� �������� ���������� ������
               '0' as BIK_R,   -- ��� / S.W.I.F.T.����� �� �������� ���������� ������
               nullif(rvp.cust_seq_id, -1) as cust_seq_id,
               case when rvp.cust_nm is null and rvp.acct_nb is null and rvp.document_no is null and rvp.inn_nb is null
                    then 0 -- ���� ��� ��������� �� ��������� ������� �������� ���� - ������ ��� ��� � ��������
                    else 1
               end as exists_fl,
               SUBSTR(
                 -- ���������� ������������ �������, ���� ��� ���� � ��� �� ������� ����������
                 case when rvp.adr_state_seq_id is null and rvp.adr_state_nm is not null
                      then CHR(13)||CHR(10)||'������ (����� �����������): '||trim(rvp.adr_state_nm||' '||rvp.adr_state_type_cd)
                 end||
                 case when rvp.adh_state_seq_id is null and rvp.adh_state_nm is not null
                      then CHR(13)||CHR(10)||'������ (����� ����� ����������): '||trim(rvp.adh_state_nm||' '||rvp.adh_state_type_cd)
                 end||
                 -- ���������� ������������������� ����� �����������, ���� �� ��� ������������ �������� ������ ��������
                 case when rvp.adr_unstruct_fl = 1 and rvp.adr_unstruct_tx is not null and
                           ((rvp.adr_city_nm is null and rvp.adr_settlm_nm is null) or
                            rvp.adr_street_name_tx is null)
                      then CHR(13)||CHR(10)||'����� �����������: '||rvp.adr_unstruct_tx
                 end||
                 -- ���������� ������������������� ����� ����� ����������, ���� �� ��� ������������ �������� ������ ��������
                 case when rvp.adh_unstruct_fl = 1 and rvp.adh_unstruct_tx is not null and
                           ((rvp.adh_city_nm is null and rvp.adh_settlm_nm is null) or
                            rvp.adh_street_name_tx is null)
                      then CHR(13)||CHR(10)||'����� ����� ����������: '||rvp.adh_unstruct_tx
                 end||
                 -- ���������� ������ ������ �������, ���� ��� ��� ������ � ��� ���� ���������� ������ �����������/����� ����������
                 case when rvp.cust_seq_id <> -1 and 
                           rvp.adr_city_nm is null and rvp.adr_settlm_nm is null and rvp.adr_street_name_tx is null and 
                           rvp.adh_city_nm is null and rvp.adh_settlm_nm is null and rvp.adh_street_name_tx is null                      
                      then CHR(13)||CHR(10)||business.rf_pkg_util.get_last_addresses_text(rvp.cust_seq_id, 3) -- 3 ��������� ������
                 end       
                 , 3, 2000) as add_info
          INTO var_b_flag, 
               var_party.TUREAL, var_party.PRU, var_party.NAMEU, var_party.KODCR, var_party.KODCN,
               var_party.AMR_ADRESS_S, var_party.AMR_ADRESS_R, var_party.AMR_ADRESS_G, var_party.AMR_ADRESS_U,
               var_party.AMR_ADRESS_D, var_party.AMR_ADRESS_K, var_party.AMR_ADRESS_O,
               var_party.ADR_ADRESS_S, var_party.ADR_ADRESS_R, var_party.ADR_ADRESS_G, var_party.ADR_ADRESS_U,
               var_party.ADR_ADRESS_D, var_party.ADR_ADRESS_K, var_party.ADR_ADRESS_O,
               var_party.KD, var_party.SD, var_party.RG, var_party.ND,
               var_party.VD_1, var_party.VD_2, var_party.VD_3, var_party.VD_4, var_party.VD_5, var_party.VD_6, var_party.VD_7,
               var_party.MC_1, var_party.MC_2, var_party.MC_3,
               var_party.GR, var_party.BP, var_party.VP,
               var_party.ACC_B, var_party.ACC_COR_B, var_party.NAME_IS_B, var_party.BIK_IS_B, var_party.CARD_B, var_party.NAME_B,
               var_party.KODCN_B, var_party.BIK_B, var_party.NAME_R, var_party.KODCN_R, var_party.BIK_R,
               var_party.cust_seq_id,
               var_exists_fl,
               var_party.add_info
          FROM (select rvp.*,
                       case when rvp.cust_type_cd in ('ORG', 'FIN')
                            then '1' -- ������ ��
                            when rvp.cust_type_cd = 'IND'
                            then '2' -- ������ ��
                            when rvp.cust_type_cd = 'IND_ORG'
                            then '3' -- ������ ��
                            else
                              case when mantas.rf_pkg_scnro.check_acct_5(substr(rvp.acct_nb,1,5),'401-407, 40807, 421, 420, 301-329, 303, 40804-40809, 40812, 40814, 40815, 40821, 410-422, 425, 427, 428,429, 430-440') = 1 --����������� �� �� ����������� ������ �����
                                   then '1' -- ������ ��
                                   when mantas.rf_pkg_scnro.check_acct_5(substr(rvp.acct_nb,1,5),'40817, 40820, 426, 423,40810, 40812, 40813') = 1 --����������� �� �� ����������� ������ �����
                                   then '2' -- ������ ��
                                   when mantas.rf_pkg_scnro.check_acct_5(substr(rvp.acct_nb,1,5),'40802, 40803') = 1 --����������� �� �� ����������� ������ �����
                                   then '3' -- ������ ��
                                   when length(rvp.inn_nb) = 10 --����������� �� �� ����� ���
                                   then '1' -- ������ ��
                                   when (length(rvp.inn_nb) = 12 or rvp.inn_nb is null) and --����������� �� �� ����� ��� (���� ����) � ������������
                                        (upper(rvp.cust_nm) like '% �� %' or upper(rvp.cust_nm) like '�� %' or upper(rvp.cust_nm) like '%���% %������%')
                                   then '3' -- ������ ��                                     
                                   when length(rvp.inn_nb) = 12 --����������� �� �� ����� ���
                                   then '2' -- ������ ��                                     
                                   when rvp.party_cd in ('SCND_ORIG','SCND_BENEF') -- ���� ��� ������������� �����������/���������� � �� ����� ������� �� ����������� �� ��� ��
                                   then '2' -- ������ ��  
                                   when rvp.opok_nb in (1003, 1004, 1005, 1006, 1008, 4001) -- ���� �������� ��� ������������ ��������� - ���. ����
                                   then '2' -- ������ ��
                                   else '4' -- �� ������ ����������
                              end
                       end as TUREAL, --��� ��������� �������� 1-��, 2-��, 3-��, 4-���������� (������ ��� "�������")                       
                       /*case when -- ��� ����� ��������� ��������� � ��� ��, ��������������� ��������
                                 rvp.INSTN_ID = par_oper.BIK_S or rvp.INSTN_ID = par_oper.BIK_SS or
                                 -- ��� ����� ��������� ����������� �� �� � ��� �� � ������ ����� ��������� � ����� �� � ��������
                                 (rvp.INSTN_ID is not null and rvp.ACCT_NB is not null and
                                  mantas.rf_pkg_rule.is_sbrf(rvp.INSTN_ID) = 1 and substr(rvp.ACCT_NB, 10, 2) = trim(to_char(var_tb_id, '00')))
                            then '1'
                            else '0'  
                       end as IS_CLIENT -- ���� ����, ��� ���������� �������� �������� ������ ����� */
                       case -- ��������/���������� �������� - ����������/����������, �������� �� ������ ����� (�� �� �������������!), ��������� ��������
                            -- TO DO: ����������� �������� ������������� �������� ��������, ��������, ����� �������� �� ���� � ����� ������� ����� ����� �������
                            when rvp.op_cat_cd in ('CT', 'BOT') and r.party_cd in ('ORIG', 'BENEF')
                            then '1' 
                            -- ����������� ������������� ��������   
                            when var_interkgrko_flag = 1 
                            then case when var_rv_kgrko_id <> rvp.kgrko_id
                                      then '0' -- ������ �������� ����� � ������ � � ����� ��������� ������ => �� ������
                                      else '1' -- ����� - ������
                                 end                                                       
                            -- ������� ����������� ��������
                            else case -- ���� ��������, ��� �������� �������� - ������ �� ��: ���� ������ �� ���� ��� ���/SWIFT �� ��
                                      when rvp.acct_seq_id <> -1 or
                                           (rvp.instn_id is not null and mantas.rf_pkg_rule.is_sbrf(rvp.instn_id) = 1) 
                                      then '1'
                                      else '0'       
                                 end                                  
                       end as IS_CLIENT -- ���� ����, ��� ���������� �������� �������� ������ �����      
                  from mantas.rfv_opok_review_party rvp
                 where rvp.review_id = par_review_id and
                       rvp.party_cd = r.party_cd) rvp
               left join business.rf_country cntry_cr on cntry_cr.cntry_cd = rvp.adr_country_cd
               left join business.rf_country fl_cntry_cr on fl_cntry_cr.cntry_cd = rvp.fl_adr_country_cd
               left join business.rf_country cntry_cn on cntry_cn.cntry_cd = rvp.adh_country_cd
               left join business.rf_country cntry_ctz on cntry_ctz.cntry_cd = rvp.citizenship_cd
               left join business.rf_addr_object state_cr on state_cr.addr_seq_id = rvp.adr_state_seq_id
               left join business.rf_addr_object state_cn on state_cn.addr_seq_id = rvp.adh_state_seq_id
               left join business.rf_cust_id_doc_type iddoctp on iddoctp.doc_type_cd = rvp.doc_type_cd
               left join business.rf_cust_id_doc iddoc_stay on iddoc_stay.cust_doc_seq_id = rvp.stay_cust_doc_seq_id
               left join business.rf_cust_id_doc_type iddoctp_stay on iddoctp_stay.doc_type_cd = iddoc_stay.doc_type_cd
               left join business.rf_cust_id_doc iddoc_mc on iddoc_mc.cust_doc_seq_id = rvp.mc_cust_doc_seq_id
               left join business.rf_cust_id_doc_type iddoctp_mc on iddoctp_mc.doc_type_cd = iddoc_mc.doc_type_cd
               left join business.rf_country bnk on bnk.cntry_cd = rvp.instn_cntry_cd 
               left join business.cust cust on cust.cust_seq_id = rvp.cust_seq_id;
        --
        -- ���� ��� 1008 ��� (���� �������� � �������� �������), �� ������������� ������������� ������� ������ �������
        --
        if r.opok_nb = 1008 and var_b_flag = '0' Then
          var_b_flag := '1';
        end if;  
        --
        -- ���� �������� ����������� ��� ��� ����, �� ����������� �������� ������� ��� ��
        --
        if var_b_flag = '2' Then   
          set_oes_321_party_tb(var_tb_id, var_osb_id, var_trxn_dt, 'CUST', var_party);
          var_exists_fl := 1;
        end if;  
        --
        -- ���� �������� ����������� ��� ��� ���� ��� ������ ������ �����, �� ���������� � 0 ���� NAME_B, KODCN_B, BIK_B
        --
        if var_b_flag in ('1', '2') Then   
          var_party.NAME_B := '0';
          var_party.KODCN_B := '0';
          var_party.BIK_B  := '0';
          var_exists_fl := 1;
        end if;  
        --
        -- �������������� ���������:
        --  - ������������� ������� ����� �����, ���� �������� �������� ��
        --  - ��� ��, �� ��������� ������������� ����� ��������������� � ����� �����������
        --
        if var_exists_fl = 1 Then
          if var_party.TUREAL = '1' Then --�������������� ��������� ����� ��� ��
             var_party.KD := '0';
             var_party.VD_1 := '0';
          elsif var_party.TUREAL = '2' then --�������������� ��������� ����� ��� ��
             NULL;
          elsif var_party.TUREAL = '3' then --�������������� ��������� ����� ��� ��
             NULL;
          end if;
          --
          -- ��������� � ����� ������ ������, ���� ����� ����, � ������ � ��� - ���
          --
          if var_party.KODCR = '0' and
             var_party.AMR_ADRESS_S||var_party.AMR_ADRESS_R|| var_party.AMR_ADRESS_G||var_party.AMR_ADRESS_U||
             var_party.AMR_ADRESS_D||var_party.AMR_ADRESS_K||var_party.AMR_ADRESS_O <> '0000000' Then
            var_party.KODCR := '64300';
          end if;  
           
          if var_party.KODCN = '0' and
             var_party.TUREAL not in ('0', '2', '3') and -- ������ ��� ��/����������� ��� - ��� ��/�� ���� KODCN �������� ����������� � �� ������� � �������
             var_party.ADR_ADRESS_S||var_party.ADR_ADRESS_R|| var_party.ADR_ADRESS_G||var_party.ADR_ADRESS_U||
             var_party.ADR_ADRESS_D||var_party.ADR_ADRESS_K||var_party.ADR_ADRESS_O <> '0000000' Then
            var_party.KODCN := '64300';
          end if;  
          --
          -- ���� ����� �����������, �� � ���� ������� ������ ���� 00
          --
          if var_party.KODCR not in ('0', '64300') Then 
            var_party.AMR_ADRESS_S := '00';
          end if;
          
          if var_party.KODCN not in ('0', '64300') and
            var_party.TUREAL not in ('0', '2', '3') Then -- ������ ��� ��/����������� ��� - ��� ��/�� ���� KODCN �������� ����������� � �� ������� � �������
            var_party.ADR_ADRESS_S := '00';
          end if;
          --
          -- ��������� ��� ������� � ������� ������������ ������ ������� ��� �������, ������� ������ �������
          -- ������ ����� ������� ���� ������ ���������� ������, ������� ������������ ���. ������ �� ��������
          --
          if var_party.AMR_ADRESS_S = '45' or 
             (upper(var_party.AMR_ADRESS_G) like '%������%' and upper(var_party.AMR_ADRESS_G) not like '%���������%') Then
            var_party.AMR_ADRESS_S := '45';
            var_party.AMR_ADRESS_R := '0';  
          elsif var_party.AMR_ADRESS_S = '40' or 
                (upper(var_party.AMR_ADRESS_G) like '%�����%���������%' and upper(var_party.AMR_ADRESS_G) not like '%������%') Then            
            var_party.AMR_ADRESS_S := '40';
            var_party.AMR_ADRESS_R := '0';  
          elsif var_party.AMR_ADRESS_S = '67' or 
                (upper(var_party.AMR_ADRESS_G) like '%�����������%' and var_party.AMR_ADRESS_S = '0') Then -- ���� ��������� ������������ � ������ ��������          
            var_party.AMR_ADRESS_S := '67';
            var_party.AMR_ADRESS_R := '0';  
          end if;  

          if var_party.ADR_ADRESS_S = '45' or 
             (upper(var_party.ADR_ADRESS_G) like '%������%' and upper(var_party.ADR_ADRESS_G) not like '%���������%') Then
            var_party.ADR_ADRESS_S := '45';
            var_party.ADR_ADRESS_R := '0';  
          elsif var_party.ADR_ADRESS_S = '40' or 
                (upper(var_party.ADR_ADRESS_G) like '%�����%���������%' and upper(var_party.ADR_ADRESS_G) not like '%������%') Then            
            var_party.ADR_ADRESS_S := '40';
            var_party.ADR_ADRESS_R := '0';  
          elsif var_party.ADR_ADRESS_S = '67' or 
                (upper(var_party.ADR_ADRESS_G) like '%�����������%' and var_party.ADR_ADRESS_S = '0') Then -- ���� ��������� ������������ � ������ ��������          
            var_party.ADR_ADRESS_S := '67';
            var_party.ADR_ADRESS_R := '0';  
          end if;  
        end if;
      end if; -- if r.party_cd = 'SBRF' ... else ...
    end if; -- if r.same_party_fl <> 1 ... 

    if var_exists_fl = 1  Then
      par_parties(to_number(r.block_nb) + 1) := var_party;
       
      if r.block_nb = '0' Then
        par_oper.B_PAYER := var_b_flag;
      elsif r.block_nb = '3' Then
        par_oper.B_RECIP := var_b_flag;
      end if;            
      --
      -- ���������� � 0 ����� ����� ��� �������� �������� �� �������, ��������������� �����
      -- �. �. � ������ �������� �������� �� ���� ���������� ����� ����� ��� �����������,
      -- � ������ ������ �������� �� ����� - ��� ����������
      --
      if par_oper.op_cat_cd = 'CT' and var_dbt_cdt_cd = 'D' and r.block_nb = '3' Then
        par_parties(to_number(r.block_nb) + 1).ACC_B := '0';
      elsif par_oper.op_cat_cd = 'CT' and var_dbt_cdt_cd = 'C' and r.block_nb = '0' Then
        par_parties(to_number(r.block_nb) + 1).ACC_B := '0';
      end if;        
      /*if r.opok_nb = 1001 and r.block_nb = '3' Then
        par_parties(to_number(r.block_nb) + 1).ACC_B := '0';
      elsif (r.opok_nb = 1002 and r.block_nb = '0') or 
            (r.opok_nb = 1008 and r.block_nb = '0') Then
        par_parties(to_number(r.block_nb) + 1).ACC_B := '0';
      end if;*/  
    end if;
  END LOOP;
  --
  -- ���� ����������/���������� ����������� ��� ��� ����, �� ������� � null ��� �������������
  --
  if par_oper.B_PAYER = '2' and par_parties(2) is not null Then
    par_parties(2) := mantas.RF_TYPE_321P_PARTY(null, null, null, null, null, null, null, null, null, null,
                                                null, null, null, null, null, null, null, null, null, null,
                                                null, null, null, null, null, null, null, null, null, null,
                                                null, null, null, null, null, null, null, null, null, null, 
                                                null, null, null, null, null, null, null, null, null, null);
  end if;
  
  if par_oper.B_RECIP = '2' and par_parties(3) is not null Then
    par_parties(3) := mantas.RF_TYPE_321P_PARTY(null, null, null, null, null, null, null, null, null, null,
                                                null, null, null, null, null, null, null, null, null, null,
                                                null, null, null, null, null, null, null, null, null, null,
                                                null, null, null, null, null, null, null, null, null, null, 
                                                null, null, null, null, null, null, null, null, null, null);
  end if;    
END get_oes_321p_by_alert;
--
-- ��������� ���� ��������� ��������� 321-� ���������� ���������� ��/���
-- (� ������������ �� ������������ �� - ��������� ��� �� ��������� ���� ��������)
--
PROCEDURE set_oes_321_party_tb (
  par_tb_id    IN      mantas.rf_oes_321_party_org.tb_code%TYPE,
  par_osb_id   IN      mantas.rf_oes_321_party_org.osb_code%TYPE,
  par_trxn_dt  IN      DATE,
  par_mode     IN      VARCHAR2,  -- 'FULL' - ��� ��������, 'CUST' - ��������, ����������� � ���������� ��������� (� �� � ����� ���������)
  par_party    IN OUT  mantas.RF_TYPE_321P_PARTY
  ) IS
BEGIN 
  SELECT oes_321_p_org_id, tu, pr, nameu, kodcr, kodcn, 
         amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, 
         adress_s, adress_r, adress_g, adress_u, adress_d, adress_k, adress_o, 
         kd, sd, rg, nd, 
         vd1, vd2, to_char(vd3, 'YYYY-MM-DD'), vd4, vd5, to_char(vd6, 'YYYY-MM-DD'), to_char(vd7, 'YYYY-MM-DD'), mc1, to_char(mc2, 'YYYY-MM-DD'), to_char(mc3, 'YYYY-MM-DD'), 
         to_char(gr, 'YYYY-MM-DD'), bp, vp, 
         decode(par_mode, 'CUST', par_party.acc_b, acc_b), 
         decode(par_mode, 'CUST', par_party.acc_cor_b , acc_cor_b), 
         decode(par_mode, 'CUST', '0', name_b), 
         decode(par_mode, 'CUST', '0', kodcn_b), 
         decode(par_mode, 'CUST', '0', bik_b), 
         decode(par_mode, 'CUST', par_party.card_b, card_b), 
         decode(par_mode, 'CUST', par_party.name_is_b, name_is), 
         decode(par_mode, 'CUST', par_party.bik_is_b, bik_is), 
         decode(par_mode, 'CUST', par_party.name_r, name_r), 
         decode(par_mode, 'CUST', par_party.kodcn_r, kodcn_r), 
         decode(par_mode, 'CUST', par_party.bik_r, bik_r)          
    INTO par_party.oes_321_p_org_id, par_party.tureal, par_party.pru, par_party.nameu, par_party.kodcr, par_party.kodcn, 
         par_party.amr_adress_s, par_party.amr_adress_r, par_party.amr_adress_g, par_party.amr_adress_u, par_party.amr_adress_d, par_party.amr_adress_k, par_party.amr_adress_o, 
         par_party.adr_adress_s, par_party.adr_adress_r, par_party.adr_adress_g, par_party.adr_adress_u, par_party.adr_adress_d, par_party.adr_adress_k, par_party.adr_adress_o, 
         par_party.kd, par_party.sd, par_party.rg, par_party.nd, 
         par_party.vd_1, par_party.vd_2, par_party.vd_3, par_party.vd_4, par_party.vd_5, par_party.vd_6, par_party.vd_7, par_party.mc_1, par_party.mc_2, par_party.mc_3, 
         par_party.gr, par_party.bp, par_party.vp, 
         par_party.acc_b, par_party.acc_cor_b, par_party.name_b, par_party.kodcn_b, par_party.bik_b, par_party.card_b, par_party.name_is_b, par_party.bik_is_b, par_party.name_r, par_party.kodcn_r, par_party.bik_r 
    FROM mantas.rf_oes_321_party_org 
   WHERE oes_321_p_org_id = mantas.rf_pkg_kgrko.get_oes_321_party_org_id(par_tb_id, par_osb_id, par_trxn_dt);
  /*FROM (select row_number() over(order by oes_321_p_org_id desc) as dup_num,
                 t.*
            from mantas.rf_oes_321_party_org t
           where tb_code = par_tb_id and
                 active_fl = 'Y' and
                 sysdate between nvl(start_dt, sysdate-1) and nvl(end_dt, sysdate+1))
   WHERE dup_num = 1;*/
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    null;  
END set_oes_321_party_tb;
--
-- ��������������� �������: ������� ������ ��� 321-�
--
FUNCTION create_empty_oes_321 
(
  par_vo              in  rf_oes_321.vo%TYPE,           -- ��� ��������
  par_dop_v           in  rf_oes_321.dop_v%TYPE,        -- �������������� ���� ��������
  par_date_s          in  rf_oes_321.date_s%TYPE,       -- ���� ��������� 
  par_oes_321_org_id  in  rf_oes_321_org.oes_321_org_id%type, -- ID ������� �����, � �������� ��������� ���
  par_branch_id       in  integer,                      -- �������� ����������� ������ ���� ���������� is null - ��� ��, � �������� ��������� ���, � ������������ � ��� ����������� ���������� � ��, ��������������� ��������
  par_manual_cmnt     in  rf_oes_321.manual_cmnt%TYPE,  -- ����������� � ��������� ���� � ������������ ��� 
  par_action          in  rf_oes_321.action%TYPE,       -- �������� ��� ���� ACTION
  par_created_by      in  rf_oes_321.created_by%TYPE,   -- ������������, ��������� ���
  par_review_id       in  rf_oes_321.review_id%TYPE     -- ID ������, � ������� ������ ���
)
RETURN rf_oes_321.oes_321_id%TYPE IS -- ID ���������� ��� 
  prc_name     constant varchar2(64) := ' (mantas.rf_pkg_oes.create_empty_oes_321)';
  
  var_oes_321_id     rf_oes_321.oes_321_id%type;  
BEGIN
  --
  -- �������� ���������
  --  
  if par_vo is null or par_review_id is null Then
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);
  end if;  
  --
  -- �������� ��������� ���
  --
  var_oes_321_id := mantas.rf_oes_321_seq.nextval;

  INSERT INTO mantas.rf_oes_321(oes_321_id, review_id, version, action, 
                                numb_p, date_p, date_s, tel, refer_r2, 
                                oes_321_org_id, regn, nd_ko, ktu_s, bik_s, numbf_s, branch, ktu_ss, bik_ss, numbf_ss, 
                                terror, vo, dop_v, data, sume, sum, 
                                curren, prim_1, prim_2, num_pay_d, date_pay_d, 
                                metal, priz6001, b_payer, b_recip, part, descr_1, descr_2, 
                                curren_con, sum_con, priz_sd, manual_cmnt, 
                                current_fl, check_status, check_dt, 
                                send_fl, send_dt, deleted_fl, 
                                created_by, created_date, modified_by, modified_date)
  (SELECT var_oes_321_id as oes_321_id, par_review_id as review_id, '2' as version, nvl(par_action, '1') as action, 
          to_number(null) as numb_p, to_date(null) as date_p, 
          case when par_vo in (6001, 8001, 5003, 5007)
               then nvl(par_date_s, to_date('01.01.2099', 'dd.mm.yyyy'))
               else to_date('01.01.2099', 'dd.mm.yyyy')
          end as date_s, -- ���� ��������� (6001), ����, ����� �� �� ����� �������� � ... (5003,5007,8001)         
          nvl(org.tel, '0') as tel, 
          '0' as refer_r2, 
          org.oes_321_org_id as oes_321_org_id, 
          nvl(org.regn, '0') as regn, 
          nvl(org.nd_ko, '0') as nd_ko, 
          nvl(org.ktu_s, '0') as ktu_s, 
          nvl(org.bik_s, '0') as bik_s, 
          nvl(org.numbf_s, '0') as numbf_s, 
          nvl(org.branch_fl, '0') as branch, 
          nvl(org.ktu_ss, '0') as ktu_ss, 
          nvl(org.bik_ss, '0') as bik_ss, 
          nvl(org.numbf_ss, '0') as numbf_ss, 
          case when par_vo = 7001 then '2' else '0' end as terror, 
          par_vo as vo, nvl(par_dop_v, '0') as dop_v, to_date('01.01.2099', 'dd.mm.yyyy') as data, 0 as sume, 0 as sum, 
          '643' as curren, '0' as prim_1, '0' as prim_2, 0 as num_pay_d, to_date('01.01.2099', 'dd.mm.yyyy') as date_pay_d, 
          '0' as metal, '0' as priz6001, '0' as b_payer, '0' as b_recip, '0' as part, '0' as descr_1, '0' as descr_2, 
          '0' as curren_con, '0' as sum_con, 
          case when par_vo = 5005 then '1' else '0' end as priz_sd, 
          par_manual_cmnt as manual_cmnt, 
          'Y' as current_fl, to_char(null) as check_status, to_date(null) as check_dt, 
          'N' as send_fl, to_date(null) as send_dt, to_char(null) as deleted_fl, 
          par_created_by as created_by, sysdate as created_date, to_char(null) as modified_by, to_date(null) as modified_date        
     FROM dual
     left join mantas.rf_oes_321_org org on org.oes_321_org_id = coalesce(par_oes_321_org_id, rf_pkg_kgrko.get_oes_321_org_id(trunc(par_branch_id/100000), (par_branch_id-round(par_branch_id, -5)), null))
  );
  --
  -- �������� ���������� ���
  --
  INSERT INTO mantas.rf_oes_321_party(oes_321_id, block_nb, oes_321_p_org_id, cust_seq_id, 
                                      tu, pr, nameu, kodcr, kodcn,
                                      amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, 
                                      adress_s, adress_r, adress_g, adress_u, adress_d, adress_k, adress_o,
                                      kd, sd, rg, nd, vd1, vd2, vd3, 
                                      vd4, vd5, vd6, vd7, 
                                      mc1, mc2, mc3, 
                                      gr, bp, vp, reserv02,
                                      kodcn_b, name_b, bik_b, acc_b, acc_cor_b, 
                                      card_b, name_is, bik_is, kodcn_r, name_r, bik_r,
                                      resrv_b2, reserv612, note)
  (SELECT var_oes_321_id as oes_321_id, level - 1 as block_nb, to_number(null) as oes_321_p_org_id, to_number(null) as cust_seq_id, 
          0 as tu, '0' as pr, '0' as nameu, '0' as kodcr, '0' as kodcn,
          '0' as amr_s, '0' as amr_r, '0' as amr_g, '0' as amr_u, '0' as amr_d, '0' as amr_k, '0' as amr_o, 
          '0' as adress_s, '0' as adress_r, '0' as adress_g, '0' as adress_u, '0' as adress_d, '0' as adress_k, '0' as adress_o,
          '0' as kd, '0' as sd, '0' as rg, '0' as nd, '0' as vd1, '0' as vd2, to_date('01.01.2099', 'dd.mm.yyyy') as vd3, 
          '0' as vd4, '0' as vd5, to_date('01.01.2099', 'dd.mm.yyyy') as vd6, to_date('01.01.2099', 'dd.mm.yyyy') as vd7, 
          '0' as mc1, to_date('01.01.2099', 'dd.mm.yyyy') as mc2, to_date('01.01.2099', 'dd.mm.yyyy') as mc3, 
          to_date('01.01.2099', 'dd.mm.yyyy') as gr, '0' as bp, '0' as vp, '0' as reserv02,
          '0' as kodcn_b, '0' as name_b, '0' as bik_b, '0' as acc_b, '0' as acc_cor_b, 
          '0' as card_b, '0' as name_is, '0' as bik_is, '0' as kodcn_r, '0' as name_r, '0' as bik_r,
          '0' as resrv_b2, '0' as reserv612, to_char(null) as note
     FROM dual
   CONNECT BY level <= 5);  
  
  return var_oes_321_id;                                    
end create_empty_oes_321;
-- 
-- ������� ��������� (���) 321-� �� ���������� ������, � ��� ����� �� ������, ���������������� ������� ���
--
-- Author  : ������� � ��
FUNCTION create_oes_321p
(
  par_review_id        mantas.kdd_review.review_id%TYPE,
  par_action           mantas.rf_oes_321.action%TYPE,
  par_user             mantas.kdd_review_owner.owner_id%TYPE
)
RETURN mantas.rf_oes_321.oes_321_id%TYPE IS
  
  var_object_tp_cd     mantas.kdd_review.rf_object_tp_cd%TYPE;
  var_source_oes_id    mantas.rf_oes_321.oes_321_id%TYPE;
  var_count            INTEGER;

  var_branch_id        mantas.kdd_review.rf_branch_id%TYPE;
  var_opok_nb          mantas.kdd_review.rf_opok_nb%TYPE; 
  var_add_opoks_tx     mantas.kdd_review.rf_add_opoks_tx%TYPE; 
  
  var_oes_321_id       mantas.rf_oes_321.oes_321_id%TYPE;
  var_oper             mantas.RF_TYPE_321P_OPER;
  var_parties          mantas.RF_TAB_321P_PARTY;
BEGIN
  --
  -- ���������: ����� �� �������� (TRXN) ��� ��� ������ ��� (OES), 
  -- ���� ������ ��� - ������ ���������� ��� ���� �� ������ - � �������� ������� ��� �����������
  --
  SELECT max(rf_object_tp_cd), max(oes_321_id), count(*), 
         max(rf_branch_id), max(rf_opok_nb), max(rf_add_opoks_tx)
    INTO var_object_tp_cd, var_source_oes_id, var_count, 
         var_branch_id, var_opok_nb, var_add_opoks_tx
    FROM (select rv.rf_object_tp_cd,
                 oes.oes_321_id,
                 rv.rf_branch_id, 
                 rv.rf_opok_nb,
                 rv.rf_add_opoks_tx,
                 row_number() over(order by case when oes.current_fl = 'Y' then 1 else 2 end, oes.oes_321_id desc nulls last) as rn
            from mantas.kdd_review rv
                 left join mantas.rf_oes_321 oes on oes.review_id = rv.review_id
           where rv.review_id = par_review_id)
   WHERE rn = 1;
  
  if var_count = 0 Then
    raise_application_error(-20001, '�������� �������� ��������� - par_review_id: '||to_char(par_review_id)||' (mantas.rf_pkg_oes.create_oes_321p)');
  end if;
  --
  -- ���������� ���� � �������� ��� ������� ������, ���� ����� ��� ����
  --
  UPDATE mantas.rf_oes_321
     SET current_fl = null
   WHERE review_id = par_review_id and 
         current_fl is not null;
  --
  -- ���� ��� ������ ��� - �������� � ������� (��� ������� ������), ����� - ������� �� ������
  --
  if var_object_tp_cd = 'OES321' Then 

    if var_source_oes_id is not null Then
      var_oes_321_id := mantas.rf_oes_321_seq.nextval;
            
      -- ������� ��������� ��� - ����� � �������, �� ����������� �������� � �� � ��������� �����
      INSERT INTO mantas.rf_oes_321(oes_321_id, review_id, version, action, 
                                    numb_p, date_p, date_s, tel, refer_r2, 
                                    oes_321_org_id, regn, nd_ko, ktu_s, bik_s, numbf_s, branch, ktu_ss, bik_ss, numbf_ss, 
                                    terror, vo, dop_v, data, sume, sum, curren, prim_1, prim_2, num_pay_d, date_pay_d, 
                                    metal, priz6001, b_payer, b_recip, part, descr_1, descr_2, curren_con, sum_con,
                                    priz_sd, manual_cmnt, 
                                    current_fl, check_status, check_dt, 
                                    send_fl, send_dt, deleted_fl, 
                                    created_by, created_date, modified_by, modified_date)
      (SELECT var_oes_321_id as oes_321_id, par_review_id as review_id, oes.version, par_action as action, 
              oes.numb_p, oes.date_p, oes.date_s, 
              nvl(org.tel, oes.tel) as tel, 
              oes.refer_r2, 
              nvl(org.oes_321_org_id, oes.oes_321_org_id) as oes_321_org_id, 
              nvl(org.regn, oes.regn) as regn, 
              nvl(org.nd_ko, oes.nd_ko) as nd_ko, 
              nvl(org.ktu_s, oes.ktu_s) as ktu_s, 
              nvl(org.bik_s, oes.bik_s) as bik_s, 
              nvl(org.numbf_s, oes.numbf_s) as numbf_s, 
              nvl(org.branch_fl, oes.branch) as branch, 
              nvl(org.ktu_ss, oes.ktu_ss) as ktu_ss, 
              nvl(org.bik_ss, oes.bik_ss) as bik_ss, 
              nvl(org.numbf_ss, oes.numbf_ss) as numbf_ss, 
              oes.terror, oes.vo, oes.dop_v, oes.data, oes.sume, oes.sum, oes.curren, oes.prim_1, oes.prim_2, oes.num_pay_d, oes.date_pay_d, 
              oes.metal, oes.priz6001, oes.b_payer, oes.b_recip, oes.part, oes.descr_1, oes.descr_2, oes.curren_con, oes.sum_con,
              oes.priz_sd, oes.manual_cmnt, 
              'Y' as current_fl, to_char(null) as check_status, to_date(null) as check_dt, 
              'N' as send_fl, to_date(null) as send_dt, to_char(null) as deleted_fl, 
              par_user as created_by, sysdate as created_date, to_char(null) as modified_by, to_date(null) as modified_date
         FROM mantas.rf_oes_321 oes
         left join mantas.rf_oes_321_org org on org.oes_321_org_id = rf_pkg_kgrko.get_oes_321_org_id(trunc(var_branch_id/100000), (var_branch_id-round(var_branch_id, -5)), trunc(oes.data))
        WHERE oes.oes_321_id = var_source_oes_id);         
      
      -- ������� ���������� ��� - ����� � �������
      INSERT INTO mantas.rf_oes_321_party(oes_321_id, block_nb, oes_321_p_org_id, cust_seq_id, tu, pr, nameu, kodcr, kodcn,
                                          amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, 
                                          adress_s, adress_r, adress_g, adress_u, adress_d, adress_k, adress_o,
                                          kd, sd, rg, nd, vd1, vd2, vd3, vd4, vd5, vd6, vd7, mc1, mc2, mc3, gr, bp, vp, reserv02,
                                          kodcn_b, name_b, bik_b, acc_b, acc_cor_b, card_b, name_is, bik_is, kodcn_r, name_r, bik_r,
                                          resrv_b2, reserv612, note)
      (SELECT var_oes_321_id as oes_321_id, block_nb, oes_321_p_org_id, cust_seq_id, tu, pr, nameu, kodcr, kodcn,
              amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, 
              adress_s, adress_r, adress_g, adress_u, adress_d, adress_k, adress_o,
              kd, sd, rg, nd, vd1, vd2, vd3, vd4, vd5, vd6, vd7, mc1, mc2, mc3, gr, bp, vp, reserv02,
              kodcn_b, name_b, bik_b, acc_b, acc_cor_b, card_b, name_is, bik_is, kodcn_r, name_r, bik_r,
              resrv_b2, reserv612, note
         FROM mantas.rf_oes_321_party
        WHERE oes_321_id = var_source_oes_id);                                     
    else
      -- ������� ������ ���
      var_oes_321_id := create_empty_oes_321(par_vo          => var_opok_nb, 
                                             par_dop_v       => var_add_opoks_tx,
                                             par_date_s      => to_date(null),
                                             par_oes_321_org_id => to_number(null),
                                             par_branch_id   => var_branch_id,
                                             par_manual_cmnt => '��������� ������� ������, �. �. ��� ������ �� ������� ����� ���������� ���',
                                             par_action      => par_action,
                                             par_created_by  => par_user,
                                             par_review_id   => par_review_id);
    end if;  
  else
    -- �������� ���������� ���������
    get_oes_321p_by_alert(par_review_id, var_oper, var_parties);

    --������� ����� ���������
    INSERT INTO mantas.rf_oes_321
      (--��������� ����
       oes_321_id, review_id, deleted_fl, check_dt, send_dt, created_by, created_date, modified_by, modified_date,
       manual_cmnt, current_fl, send_fl, check_status, version, numb_p, date_p, action,
       --��
       regn, nd_ko, ktu_s, bik_s, numbf_s, 
       branch, ktu_ss, bik_ss, numbf_ss, tel, oes_321_org_id,
       --������ ��������
       data, date_s, 
       terror, vo, dop_v, sume, sum, curren, 
       prim_1, prim_2, 
       num_pay_d, date_pay_d, 
       metal, priz6001, b_payer, b_recip, part, 
       descr_1, descr_2, 
       curren_con, sum_con, priz_sd,
       refer_r2)
    VALUES
      (--��������� ����
       mantas.rf_oes_321_seq.nextval, par_review_id, null, null, null, par_user, sysdate, null, null, 
       null, 'Y', 'N', null, var_oper.version_p, null, null, par_action,
       --��
       var_oper.regn, var_oper.nd_ko, var_oper.ktu_s, var_oper.bik_s, var_oper.numbf_s, 
       var_oper.branch, var_oper.ktu_ss, var_oper.bik_ss, var_oper.numbf_ss, var_oper.tel, var_oper.oes_321_org_id,
       --������ ��������
       to_date(var_oper.data, 'YYYY-MM-DD'), nvl(to_date(var_oper.date_s, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
       var_oper.terror, var_oper.vo, var_oper.dop_v, 
       to_number(var_oper.sume, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''), 
       to_number(var_oper.sum, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''), var_oper.curren, 
       nvl(substr(var_oper.prim, 1, 254), '0'), nvl(substr(var_oper.prim, 255), '0'),
       var_oper.num_pay_d, nvl(to_date(var_oper.date_pay_d, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
       var_oper.metal, var_oper.priz6001, var_oper.b_payer, var_oper.b_recip, var_oper.part,
       nvl(substr(var_oper.descr, 1, 254), '0'), nvl(substr(var_oper.descr, 255), '0'),
       var_oper.curren_con, 
       to_number(var_oper.sum_con, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''), var_oper.priz_sd,
       var_oper.refer_r2
       )
    returning oes_321_id into var_oes_321_id;

    FOR i IN 1 .. 5 LOOP

      INSERT INTO mantas.rf_oes_321_party oep
        (oes_321_id, block_nb, tu, pr, nameu, kodcr, kodcn, kd, sd, rg, nd, vd1, vd2, vd3, vd4, vd5, vd6, vd7,
         mc1, mc2, mc3, gr, bp, vp, acc_b, acc_cor_b, amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o,
         adress_s, adress_r, adress_g, adress_u, adress_d, adress_k, adress_o, card_b, name_b, kodcn_b, bik_b,                   
         name_is, bik_is, name_r, kodcn_r, bik_r, note, oes_321_p_org_id, cust_seq_id)
      VALUES
        (var_oes_321_id,
         i - 1,
         case when i - 1 in (0, 3) then nvl(var_parties(i).tureal, '4') else nvl(var_parties(i).tureal, '0') end,
         nvl(var_parties(i).pru, '0'),
         nvl(var_parties(i).nameu, '0'),
         nvl(var_parties(i).kodcr, '0'),
         nvl(var_parties(i).kodcn, '0'),
         nvl(var_parties(i).kd, '0'),
         nvl(var_parties(i).sd, '0'),
         nvl(var_parties(i).rg, '0'),
         nvl(var_parties(i).nd, '0'),
         nvl(var_parties(i).vd_1, '0'),
         nvl(var_parties(i).vd_2, '0'),
         nvl(to_date(var_parties(i).vd_3, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
         nvl(var_parties(i).vd_4, '0'),
         nvl(var_parties(i).vd_5, '0'),
         nvl(to_date(var_parties(i).vd_6, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
         nvl(to_date(var_parties(i).vd_7, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
         nvl(var_parties(i).mc_1, '0'),
         nvl(to_date(var_parties(i).mc_2, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
         nvl(to_date(var_parties(i).mc_3, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
         nvl(to_date(var_parties(i).gr, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
         nvl(var_parties(i).bp, '0'),
         nvl(var_parties(i).vp, '0'),
         nvl(var_parties(i).acc_b, '0'),
         nvl(var_parties(i).acc_cor_b, '0'),
         nvl(var_parties(i).amr_adress_s, '0'),
         nvl(var_parties(i).amr_adress_r, '0'),
         nvl(var_parties(i).amr_adress_g, '0'),
         nvl(var_parties(i).amr_adress_u, '0'),
         nvl(var_parties(i).amr_adress_d, '0'),
         nvl(var_parties(i).amr_adress_k, '0'),
         nvl(var_parties(i).amr_adress_o, '0'),
         nvl(var_parties(i).adr_adress_s, '0'),
         nvl(var_parties(i).adr_adress_r, '0'),
         nvl(var_parties(i).adr_adress_g, '0'),
         nvl(var_parties(i).adr_adress_u, '0'),
         nvl(var_parties(i).adr_adress_d, '0'),
         nvl(var_parties(i).adr_adress_k, '0'),
         nvl(var_parties(i).adr_adress_o, '0'),
         nvl(var_parties(i).card_b, '0'),
         nvl(var_parties(i).name_b, '0'),
         nvl(var_parties(i).kodcn_b, '0'),
         nvl(var_parties(i).bik_b, '0'),
         nvl(var_parties(i).name_is_b, '0'),
         nvl(var_parties(i).bik_is_b, '0'),
         nvl(var_parties(i).name_r, '0'),
         nvl(var_parties(i).kodcn_r, '0'),
         nvl(var_parties(i).bik_r, '0'),
         var_parties(i).add_info,
         var_parties(i).oes_321_p_org_id,
         var_parties(i).cust_seq_id
       );
    END LOOP;
  end if;
  
  return(var_oes_321_id);
END create_oes_321p;

--
-- ����� ������� ����� � ��������� ��� � �����������
--
procedure create_oes_321_manual (
  par_opok_nb         in  rf_opok.opok_nb%type,         -- ��� ��������
  par_add_opoks_tx    in  varchar2,                     -- �������������� ���� ��������
  par_oes_321_org_id  in  rf_oes_321_org.oes_321_org_id%type, -- ID ������� �����, � �������� ��������� ���
  par_branch_id       in  kdd_review.rf_branch_id%type, -- �������� ����������� ������ ���� ���������� is null - ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
  par_owner_seq_id    in  kdd_review.owner_seq_id%type, -- �������������, ��������������� � ����� (���� null, �� ���������������)
  par_creat_id        in  kdd_review.creat_id%type,     -- ������������, ��������� ���
  par_note_tx         in  kdd_note_hist.note_tx%type,   -- ����������� � ��������� ���� � ������������ ��� - �� oes_manual_cmnt
  par_due_dt          in  kdd_review.due_dt%type,       -- ���� ��������� 
  par_inter_kgrko_id  in  kdd_review.rf_inter_kgrko_id%type, --ID, ������������ ��� ������ ��������� (���) � ������ ������������� ��������
  par_kgrko_party_cd  in  kdd_review.rf_kgrko_party_cd%type, --������� ���� ��������� ������������� �������� (1/2 - ���������� / ����������)
  par_review_id       out kdd_review.review_id%type,    -- ����� ������
  par_oes_321_id      out rf_oes_321.oes_321_id%type   -- ����� ��� ���������
)
is
  c_method_name constant varchar2(64) := 'mantas.rf_pkg_oes.create_oes_321_manual';
  
  v_oes_id     rf_oes_321.oes_321_id%type;  
  v_err_code   number;
  v_err_msg    varchar2(255);
  v_owner_id   mantas.kdd_review_owner.owner_id%type;
  v_org        mantas.kdd_review_owner.rptg_group_cd%type;
  
  v_review_ids mantas.rf_tab_number := rf_tab_number();
  v_actvy_ids  rf_pkg_review.tab_number;
begin
  par_review_id := KDD_REVIEW_SEQ.NextVal;
  /*mantas.p_sequence_nextval('REVIEW_ID_SEQ', par_review_id, v_err_code, v_err_msg);
  if par_review_id is null then
    raise_application_error(-20001, '������ ��� ��������� ���������� �������������� (REVIEW_ID_SEQ): '||v_err_code||' '||v_err_msg||' ('||c_method_name||')');
  end if;*/

  select max(owner_id), max(rptg_group_cd)
    into v_owner_id, v_org
    from mantas.kdd_review_owner
   where owner_seq_id = par_owner_seq_id;

  if v_owner_id is null then
    raise_application_error(-20001, '������ �������� ������������� ������������ (��������� ������): '||par_owner_seq_id||' ('||c_method_name||')');
  end if;

  /* ��������� ������ � kdd_review */
  insert into kdd_review (
    review_id, 
    creat_id, 
    creat_ts, 
    status_cd, 
    status_dt, 
    cntry_id, 
    cntry_key_id,
    scnro_id, 
    scnro_class_cd, 
    pttrn_id, 
    dmn_cd, 
    alert_ct, 
    reopn_fl, 
    reasn_fl, 
    owner_seq_id, 
    scnro_ct, 
    score_fl,
    due_dt, 
    auto_reasn_fl, 
    last_actvy_type_cd, 
    prev_match_ct_sm_scnro, 
    prev_match_ct_all, 
    owner_org, 
    age, 
    focal_ntity_dsply_nm, 
    focal_ntity_dsply_id, 
    prev_match_ct_sm_scnro_class, 
    send_ntfctn_cd, 
    bus_dmn_st, 
    bus_dmn_dsply_nm_st, 
    jrsdcn_cd, 
    prcsng_batch_nm, 
    prcsng_batch_cmplt_fl, 
    orig_owner_seq_id, 
    prcsng_dt, 
    review_type_cd, 
    alert_case_ct, 
    rf_object_tp_cd, 
    rf_op_cat_cd,
    rf_trxn_seq_id,
    rf_trxn_scrty_seq_id,
    rf_repeat_nb, 
    rf_opok_nb, 
    rf_add_opoks_tx, 
    rf_branch_id,
    rf_data_dump_dt,
    rf_cls_dt,
    rf_trxn_dt,
    rf_trxn_base_am,
    rf_partition_key,
    rf_alert_type_cd,
    rf_kgrko_party_cd,
    rf_inter_kgrko_id
  ) 
  (SELECT par_review_id, --review_id
          par_creat_id, --creat_id
          sysdate, --creat_ts
          'RF_OES', --status_cd
          sysdate, --status_dt
          113000004, --cntry_id
          -1, --cntry_key_id
          101, --scnro_id
          'ML', --scnro_class_cd
          1010, --pttrn_id
          'MTS', --dmn_cd
          1, --alert_ct
          'N', --reopn_fl
          'N', --reasn_fl
          par_owner_seq_id, --owner_seq_id
          1, --scnro_ct
          'Y', --score_f
          mantas.rf_add_work_days(
            par_due_dt,
            nvl(to_number(rf_pkg_adm.get_install_param(9001)), 3)
          ), --due_dt
          'N', --auto_reasn_fl
          'MTS000', --last_actvy_type_cd
          0, --prev_match_ct_sm_scnro
          0, --prev_match_ct_all
          v_org, --owner_org
          0, --age
          '������ �� �� �����������', --focal_ntity_dsply_nm
          -1, --focal_ntity_dsply_id
          0, --prev_match_ct_sm_scnro_class
          'D', --send_ntfctn_cd
          'a', --bus_dmn_st
          'GEN', --bus_dmn_dsply_nm_st
          'RF', --jrsdcn_cd
          'DLY', --prcsng_batch_nm
          'Y', --prcsng_batch_cmplt_fl
          par_owner_seq_id, --orig_owner_seq_id
          trunc(sysdate), --prcsng_dt
          'AL', --review_type_cd
          0, --alert_case_ct
          'OES321', --rf_object_tp_cd
          to_char(null), -- rf_op_cat_cd
          to_number(null), -- rf_trxn_seq_id
          to_number(null), -- rf_trxn_scrty_seq_id
          0, --rf_repeat_nb
          par_opok_nb, --rf_opok_nb
          par_add_opoks_tx, --rf_add_opoks_tx
          case when org.oes_321_org_id is not null
               then org.tb_code * 100000 + nvl(org.osb_code, 0)
               else par_branch_id
          end, --rf_branch_id
          to_date(null), -- rf_data_dump_dt
          to_date(null), -- rf_cls_dt
          to_date(null), -- rf_trxn_dt
          to_number(null), -- rf_trxn_base_am
          'ACT', -- rf_partition_key 
          'O', -- rf_alert_type_cd - ����� �� ������������� ��������
          par_kgrko_party_cd,
          par_inter_kgrko_id
     FROM dual
          left join mantas.rf_oes_321_org org on org.oes_321_org_id = par_oes_321_org_id
    );

  /* ������� ������ ��� 321-� */
  par_oes_321_id := create_empty_oes_321(par_vo          => par_opok_nb, 
                                         par_dop_v       => par_add_opoks_tx,
                                         par_date_s      => par_due_dt,
                                         par_oes_321_org_id => par_oes_321_org_id,
                                         par_branch_id   => par_branch_id,
                                         par_manual_cmnt => par_note_tx,
                                         par_action      => '1',
                                         par_created_by  => v_owner_id,
                                         par_review_id   => par_review_id);
  
  /* ������������ �������� */
  v_review_ids.extend;
  v_review_ids(1) := par_review_id;
  v_actvy_ids := rf_pkg_review.create_activities(
                   p_review_ids    => v_review_ids,
                   p_actvy_type_cd => 'MTS000',
                   p_cmmnt_id      => null,
                   p_note_tx       => par_note_tx,
                   p_owner_seq_id  => par_creat_id
                 );
  
  /* !!!��������!!! �� ���������� ���������� ��� ������������� �������� */
  if par_inter_kgrko_id is null then
    commit;
  end if;  
  
  exception
    when others then 
      rollback;
      par_review_id := null;
      par_oes_321_id := null;
      raise_application_error(-20001, '������ ��� �������� ������ ��� ��������: '||sqlerrm||' ('||c_method_name||').');
end create_oes_321_manual;

--
-- ����� ������� ����� � ��������� ��� � ����������� �� ���������� ������
--
procedure create_oes_321_manual (
  p_branch_id       in  kdd_review.rf_branch_id%TYPE, -- ��� ��/���, � �������� ��������� ��������: ����� �� * 100 000 + ����� ���������
  p_owner_seq_id    in  kdd_review.owner_seq_id%TYPE, -- �������������, ��������������� � ����� (���� null, �� ���������������)
  p_creat_id        in  kdd_review.creat_id%TYPE,     -- ������������, ��������� ���
  p_note_tx         in  kdd_note_hist.note_tx%TYPE,   -- ����������� � ��������� ���� � ������������ ��� - �� oes_manual_cmnt
  p_oper            in  RF_TYPE_321P_OPER,            -- ��������
  p_parties         in  RF_TAB_321P_PARTY,            -- ���������
  p_review_id       out kdd_review.review_id%TYPE,    -- ����� ������
  p_oes_321_id      out rf_oes_321.oes_321_id%TYPE    -- ����� ��� ��������
) IS
  l_created_by        kdd_review_owner.owner_id%TYPE;
  l_owner_id          kdd_review_owner.owner_id%TYPE;
  l_org               kdd_review_owner.rptg_group_cd%TYPE;
  l_review_ids        rf_tab_number := rf_tab_number();
  l_actvy_ids         rf_pkg_review.tab_number;
  l_oes_check_status  VARCHAR2(1 CHAR);  
BEGIN
  SAVEPOINT create_oes_321_manual;
  
  -- �������� ������ �� ������������
  SELECT MAX(owner_id), MAX(rptg_group_cd)
    INTO l_owner_id, l_org
    FROM mantas.kdd_review_owner
   WHERE owner_seq_id = p_owner_seq_id;
  IF l_owner_id IS NULL THEN
    raise_application_error(-20001, '������ �������� ������������� ������������ (������������� �� �����): '||p_owner_seq_id );
  END IF;

  -- �������� ������ �� ���������
  SELECT MAX(owner_id)
    INTO l_created_by
    FROM mantas.kdd_review_owner
   WHERE owner_seq_id = p_creat_id;
  IF l_owner_id IS NULL THEN
    raise_application_error(-20001, '������ �������� ������������� ������������ (���������): '||p_creat_id );
  END IF;
  
  -- ������� �����
  INSERT INTO kdd_review (
    review_id, creat_id, creat_ts, status_cd, status_dt, cntry_id, cntry_key_id, scnro_id, scnro_class_cd, 
    pttrn_id, dmn_cd, alert_ct, reopn_fl, reasn_fl, owner_seq_id, scnro_ct, score_fl, due_dt, auto_reasn_fl, 
    last_actvy_type_cd, prev_match_ct_sm_scnro, prev_match_ct_all, owner_org, age, focal_ntity_dsply_nm, 
    focal_ntity_dsply_id, prev_match_ct_sm_scnro_class, send_ntfctn_cd, bus_dmn_st, bus_dmn_dsply_nm_st, 
    jrsdcn_cd, prcsng_batch_nm, prcsng_batch_cmplt_fl, orig_owner_seq_id, prcsng_dt, review_type_cd, 
    alert_case_ct, rf_object_tp_cd, rf_op_cat_cd, rf_trxn_seq_id, rf_trxn_scrty_seq_id, rf_repeat_nb, 
    rf_opok_nb, rf_add_opoks_tx, rf_branch_id, rf_data_dump_dt, rf_cls_dt, rf_trxn_dt, rf_trxn_base_am,
    rf_partition_key, rf_alert_type_cd, rf_kgrko_party_cd, rf_inter_kgrko_id ) 
  VALUES( kdd_review_seq.NEXTVAL, --review_id
          p_creat_id, --creat_id
          SYSDATE, --creat_ts
          'RF_OES', --status_cd
          SYSDATE, --status_dt
          113000004, --cntry_id
          -1, --cntry_key_id
          101, --scnro_id
          'ML', --scnro_class_cd
          1010, --pttrn_id
          'MTS', --dmn_cd
          1, --alert_ct
          'N', --reopn_fl
          'N', --reasn_fl
          p_owner_seq_id, --owner_seq_id
          1, --scnro_ct
          'Y', --score_f
          rf_add_work_days(CASE WHEN p_oper.vo in (6001, 8001, 5003, 5007) -- ����, �� ������� ���� ������������� �� ���� ���������
                              THEN nvl(nullif(TO_DATE(p_oper.DATE_S,'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')), trunc(SYSDATE))
                              ELSE TO_DATE(p_oper.DATA,'YYYY-MM-DD')
                            END
                           ,nvl(to_number(rf_pkg_adm.get_install_param(9001)),3)), --due_dt
          'N', --auto_reasn_fl
          'MTS000', --last_actvy_type_cd
          0, --prev_match_ct_sm_scnro
          0, --prev_match_ct_all
          l_org, --owner_org
          0, --age
          '������ �� �� �����������', --focal_ntity_dsply_nm
          -1, --focal_ntity_dsply_id
          0, --prev_match_ct_sm_scnro_class
          'D', --send_ntfctn_cd
          'a', --bus_dmn_st
          'GEN', --bus_dmn_dsply_nm_st
          'RF', --jrsdcn_cd
          'DLY', --prcsng_batch_nm
          'Y', --prcsng_batch_cmplt_fl
          p_owner_seq_id, --orig_owner_seq_id
          trunc(sysdate), --prcsng_dt
          'AL', --review_type_cd
          0, --alert_case_ct
          'OES321', --rf_object_tp_cd
          to_char(null), -- rf_op_cat_cd
          to_number(null), -- rf_trxn_seq_id
          to_number(null), -- rf_trxn_scrty_seq_id
          0, --rf_repeat_nb
          p_oper.vo, --rf_opok_nb
          DECODE(p_oper.dop_v,'0',NULL,p_oper.dop_v), --rf_add_opoks_tx
          p_branch_id, --rf_branch_id
          to_date(null), -- rf_data_dump_dt
          to_date(null), -- rf_cls_dt
          TO_DATE(p_oper.DATA,'YYYY-MM-DD'), -- rf_trxn_dt
          TO_NUMBER(p_oper.sume, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''), -- rf_trxn_base_am
          'ACT', -- rf_partition_key 
          'O', -- rf_alert_type_cd - ����� �� ������������� ��������
          NULL, --par_kgrko_party_cd,
          NULL --par_inter_kgrko_id
        ) RETURNING review_id INTO p_review_id; 

    --������� ����� ���������
    INSERT INTO mantas.rf_oes_321
      (--��������� ����
       oes_321_id, review_id, deleted_fl, check_dt, send_dt, created_by, created_date, modified_by, modified_date,
       manual_cmnt, current_fl, send_fl, check_status, version, numb_p, date_p, action,
       --��
       regn, nd_ko, ktu_s, bik_s, numbf_s, 
       branch, ktu_ss, bik_ss, numbf_ss, tel, oes_321_org_id,
       --������ ��������
       data, date_s, 
       terror, vo, dop_v, sume, sum, curren, 
       prim_1, prim_2, 
       num_pay_d, date_pay_d, 
       metal, priz6001, b_payer, b_recip, part, 
       descr_1, descr_2, 
       curren_con, sum_con, priz_sd,
       refer_r2)
      VALUES
        (--��������� ����
         rf_oes_321_seq.nextval, p_review_id, null, null, null, l_created_by, sysdate, null, null, 
         null, 'Y', 'N', null, NVL(p_oper.version_p,'2'), p_oper.numb_p, TO_DATE(p_oper.date_p, 'YYYY-MM-DD'),  NVL(p_oper.action,'1'),
         --��
         NVL(p_oper.regn,'0'), NVL(p_oper.nd_ko,'0'), NVL(p_oper.ktu_s,'0'), NVL(p_oper.bik_s,'0'), NVL(p_oper.numbf_s,'0'), 
         NVL(p_oper.branch,'0'), NVL(p_oper.ktu_ss,'0'), NVL(p_oper.bik_ss,'0'), NVL(p_oper.numbf_ss,'0'), NVL(p_oper.tel,'0'), p_oper.oes_321_org_id,
         --������ ��������
         TO_DATE(p_oper.data, 'YYYY-MM-DD'), NVL(to_date(p_oper.date_s, 'YYYY-MM-DD'), TO_DATE('01.01.2099', 'dd.mm.yyyy')),
         NVL(p_oper.terror,'0'), p_oper.vo, NVL(p_oper.dop_v,'0'), 
         NVL(TO_NUMBER(p_oper.sume, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''),0), 
         NVL(TO_NUMBER(p_oper.sum, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''),0), NVL(p_oper.curren,'643'), 
         NVL(SUBSTR(p_oper.prim, 1, 254), '0'), NVL(SUBSTR(p_oper.prim, 255), '0'),
         NVL(p_oper.num_pay_d,0), NVL(to_date(p_oper.date_pay_d, 'YYYY-MM-DD'), TO_DATE('01.01.2099', 'dd.mm.yyyy')),
         NVL(p_oper.metal,'0'), NVL(p_oper.priz6001,'0'), p_oper.b_payer, NVL(p_oper.b_recip,'0'), NVL(p_oper.part,'0'),
         NVL(SUBSTR(p_oper.descr, 1, 254), '0'), NVL(SUBSTR(p_oper.descr, 255), '0'),
         NVL(p_oper.curren_con,'0'), 
         NVl(TO_NUMBER(p_oper.sum_con, '999999999999999999999999999D99', 'NLS_NUMERIC_CHARACTERS=''.,'''),0), NVL(p_oper.priz_sd,'0'),
         NVL(p_oper.refer_r2,'0')
         )
      RETURNING oes_321_id INTO p_oes_321_id;

    -- ���������� ����������
    FOR i IN 1 .. 5 LOOP
      INSERT INTO mantas.rf_oes_321_party oep
        (oes_321_id, block_nb, tu, pr, nameu, kodcr, kodcn, kd, sd, rg, nd, vd1, vd2, vd3, vd4, vd5, vd6, vd7,
         mc1, mc2, mc3, gr, bp, vp, acc_b, acc_cor_b, amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o,
         adress_s, adress_r, adress_g, adress_u, adress_d, adress_k, adress_o, card_b, name_b, kodcn_b, bik_b,                   
         name_is, bik_is, name_r, kodcn_r, bik_r, note, oes_321_p_org_id, cust_seq_id)
        VALUES
          (p_oes_321_id,
           i - 1,
           case when i - 1 in (0, 3) then nvl(p_parties(i).tureal, '4') else nvl(p_parties(i).tureal, '0') end,
           nvl(p_parties(i).pru, '0'),
           nvl(p_parties(i).nameu, '0'),
           nvl(p_parties(i).kodcr, '0'),
           nvl(p_parties(i).kodcn, '0'),
           nvl(p_parties(i).kd, '0'),
           nvl(p_parties(i).sd, '0'),
           nvl(p_parties(i).rg, '0'),
           nvl(p_parties(i).nd, '0'),
           nvl(p_parties(i).vd_1, '0'),
           nvl(p_parties(i).vd_2, '0'),
           nvl(to_date(p_parties(i).vd_3, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
           nvl(p_parties(i).vd_4, '0'),
           nvl(p_parties(i).vd_5, '0'),
           nvl(to_date(p_parties(i).vd_6, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
           nvl(to_date(p_parties(i).vd_7, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
           nvl(p_parties(i).mc_1, '0'),
           nvl(to_date(p_parties(i).mc_2, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
           nvl(to_date(p_parties(i).mc_3, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
           nvl(to_date(p_parties(i).gr, 'YYYY-MM-DD'), to_date('01.01.2099', 'dd.mm.yyyy')),
           nvl(p_parties(i).bp, '0'),
           nvl(p_parties(i).vp, '0'),
           nvl(p_parties(i).acc_b, '0'),
           nvl(p_parties(i).acc_cor_b, '0'),
           nvl(p_parties(i).amr_adress_s, '0'),
           nvl(p_parties(i).amr_adress_r, '0'),
           nvl(p_parties(i).amr_adress_g, '0'),
           nvl(p_parties(i).amr_adress_u, '0'),
           nvl(p_parties(i).amr_adress_d, '0'),
           nvl(p_parties(i).amr_adress_k, '0'),
           nvl(p_parties(i).amr_adress_o, '0'),
           nvl(p_parties(i).adr_adress_s, '0'),
           nvl(p_parties(i).adr_adress_r, '0'),
           nvl(p_parties(i).adr_adress_g, '0'),
           nvl(p_parties(i).adr_adress_u, '0'),
           nvl(p_parties(i).adr_adress_d, '0'),
           nvl(p_parties(i).adr_adress_k, '0'),
           nvl(p_parties(i).adr_adress_o, '0'),
           nvl(p_parties(i).card_b, '0'),
           nvl(p_parties(i).name_b, '0'),
           nvl(p_parties(i).kodcn_b, '0'),
           nvl(p_parties(i).bik_b, '0'),
           nvl(p_parties(i).name_is_b, '0'),
           nvl(p_parties(i).bik_is_b, '0'),
           nvl(p_parties(i).name_r, '0'),
           nvl(p_parties(i).kodcn_r, '0'),
           nvl(p_parties(i).bik_r, '0'),
           p_parties(i).add_info,
           p_parties(i).oes_321_p_org_id,
           p_parties(i).cust_seq_id
         );
      END LOOP;

  -- ������������ �������� 
  l_review_ids.extend;
  l_review_ids(1) := p_review_id;
  l_actvy_ids := rf_pkg_review.create_activities(
                   p_review_ids    => l_review_ids,
                   p_actvy_type_cd => 'MTS000',
                   p_cmmnt_id      => null,
                   p_note_tx       => p_note_tx,
                   p_owner_seq_id  => p_creat_id
                 );

  -- ��������� ���������
  l_oes_check_status := rf_pkg_oes_check.check_oes(par_form_cd => '321', par_oes_id => p_oes_321_id, par_commit_flag => 0);
  
EXCEPTION
  WHEN OTHERS THEN 
    ROLLBACK TO SAVEPOINT create_oes_321_manual;
    p_review_id := null;
    p_oes_321_id := null;
    raise_application_error(-20001, '������ ��� �������� ��� ��� ��������: '||sqlerrm);
END create_oes_321_manual;

--
-- ����� ������� ����� ��� �� ����� oes_321_id. �������� ������ �� ����������.
--
procedure copy_oes_321 (
  p_oes_321_id     in rf_oes_321.oes_321_id%type, -- ���� ���������, ������� ����� �����������
  p_owner_seq_id   in kdd_review_owner.owner_seq_id%type, -- id ������������ 
  p_new_review_id  out kdd_review.review_id%type, -- ����� ���� ���������� ��������
  p_new_oes_321_id out rf_oes_321.oes_321_id%type -- ����� ���� ���������
) is
  c_method_name  constant varchar2(64) := 'mantas.rf_pkg_oes.copy_oes_321';
  v_err_code     number;
  v_err_msg      varchar2(255);
  v_owner_seq_id kdd_review_owner.owner_seq_id%type := null;
  v_owner_id     kdd_review_owner.owner_id%type := null;
  v_review_id    kdd_review.review_id%type;
  v_review_ids   rf_tab_number := rf_tab_number();
  v_actvy_ids    rf_pkg_review.tab_number;
begin
  /* �������� ����� �������� ����� REVIEW_ID */
  p_new_review_id := KDD_REVIEW_SEQ.NextVal;
  /*mantas.p_sequence_nextval('REVIEW_ID_SEQ', p_new_review_id, v_err_code, v_err_msg);
  if p_new_review_id is null then
    raise_application_error(-20001, '������ ��� ��������� ���������� �������������� (REVIEW_ID_SEQ): '||v_err_code||' '||v_err_msg||' ('||c_method_name||')');
  end if;*/
  
  /* ���������� ��� ������������ */
  select owner_id into v_owner_id 
    from kdd_review_owner
   where owner_seq_id = p_owner_seq_id;
  
  /* ���������� ������������� ������������ */
  select max(owner_seq_id) into v_owner_seq_id 
    from kdd_review_owner 
   where own_alert_fl = 'Y' 
     and actv_fl = 'Y' 
     and owner_seq_id = p_owner_seq_id;
  /* ���� ������������ �� ���������, �� ��������� �������������� ������������ �� ��������� */
  if v_owner_seq_id is null then
    v_owner_seq_id := rf_pkg_assign.def_owner;
  end if;  
  /* ���� ����������� �� ��������� - ������ */
  if v_owner_seq_id is null then
    raise_application_error(-20001, '�� ������� ���������� �������������� ������������.');
  end if;
  
  /* ���������� ���� �������� */
  select review_id 
    into v_review_id
    from rf_oes_321 
   where oes_321_id = p_oes_321_id;

  /* ������� ����� ������ � ���������� �������� */
  insert into kdd_review (
    /* ����� �������� */ 
    review_id, creat_id, creat_ts, status_cd, status_dt, owner_seq_id, orig_owner_seq_id, rf_alert_type_cd, rf_kgrko_party_cd, rf_inter_kgrko_id,
    /* �������� ��� ��������� */
    lock_id, lock_ts, cntry_id, cls_id, cntry_key_id, scnro_id, scnro_class_cd, pttrn_id, dmn_cd, alert_ct, reopn_fl, reasn_fl, 
    scnro_ct, score_ct, score_fl, due_dt, auto_cls_ct, cstm_1_tx, cstm_2_tx, cstm_3_tx, cstm_4_tx, cstm_5_tx, cstm_1_dt, cstm_2_dt, 
    cstm_3_dt, cstm_1_rl, cstm_2_rl, cstm_3_rl, auto_reasn_fl, last_actvy_type_cd, prev_match_ct_sm_scnro, prev_match_ct_all, 
    owner_org, age, focal_ntity_dsply_nm, focal_ntity_dsply_id, prev_match_ct_sm_scnro_class, send_ntfctn_cd, bus_dmn_st, 
    bus_dmn_dsply_nm_st, jrsdcn_cd, prcsng_batch_nm, prcsng_batch_cmplt_fl, hilgt_tx, 
    cls_actvy_type_cd, cls_class_cd, scnro_displ_nm, extrl_ref_id, extrl_ref_link, extrl_ref_src_id, rqst_audit_seq_id, prcsng_dt, 
    review_type_cd, alert_case_ct, last_link_fl, link_updt_ts, link_updt_id, reg_status_list, rf_object_tp_cd, rf_op_cat_cd, 
    rf_trxn_seq_id, rf_trxn_scrty_seq_id, rf_repeat_nb, rf_opok_nb, rf_add_opoks_tx, rf_branch_id, rf_data_dump_dt, rf_cls_dt, 
    rf_trxn_dt, rf_trxn_base_am/*, rf_wrk_flag*/, rf_partition_key/*, rf_subpartition_date*/ )
  select /* ����� �������� */ 
         p_new_review_id, p_owner_seq_id, sysdate, 'RF_OES', sysdate, v_owner_seq_id, v_owner_seq_id, 'O', null, null,
         /* �������� ��� ��������� */
         lock_id, lock_ts, cntry_id, cls_id, cntry_key_id, scnro_id, scnro_class_cd, pttrn_id, dmn_cd, alert_ct, reopn_fl, reasn_fl, 
         scnro_ct, score_ct, score_fl, due_dt, auto_cls_ct, cstm_1_tx, cstm_2_tx, cstm_3_tx, cstm_4_tx, cstm_5_tx, cstm_1_dt, cstm_2_dt, 
         cstm_3_dt, cstm_1_rl, cstm_2_rl, cstm_3_rl, auto_reasn_fl, last_actvy_type_cd, prev_match_ct_sm_scnro, prev_match_ct_all, 
         owner_org, age, focal_ntity_dsply_nm, focal_ntity_dsply_id, prev_match_ct_sm_scnro_class, send_ntfctn_cd, bus_dmn_st, 
         bus_dmn_dsply_nm_st, jrsdcn_cd, prcsng_batch_nm, prcsng_batch_cmplt_fl, hilgt_tx, 
         cls_actvy_type_cd, cls_class_cd, scnro_displ_nm, extrl_ref_id, extrl_ref_link, extrl_ref_src_id, rqst_audit_seq_id, prcsng_dt, 
         review_type_cd, alert_case_ct, last_link_fl, link_updt_ts, link_updt_id, reg_status_list, rf_object_tp_cd, rf_op_cat_cd, 
         rf_trxn_seq_id, rf_trxn_scrty_seq_id, rf_repeat_nb, rf_opok_nb, rf_add_opoks_tx, rf_branch_id, rf_data_dump_dt, rf_cls_dt, 
         rf_trxn_dt, rf_trxn_base_am/*, rf_wrk_flag*/, rf_partition_key/*, rf_subpartition_date*/
    from kdd_review
   where review_id = v_review_id;

  /* ������������ �������� �� �������� ����� �������� */
  v_review_ids.extend;
  v_review_ids(1) := p_new_review_id;
  v_actvy_ids := rf_pkg_review.create_activities(
                   p_review_ids    => v_review_ids,
                   p_actvy_type_cd => 'RF_OESCOPY',
                   p_cmmnt_id      => null,
                   p_note_tx       => '�������� ����� �������� � '||v_review_id||' ��� � '||p_oes_321_id||'.',
                   p_owner_seq_id  => p_owner_seq_id
                 );

  /* ������� ����� ��������� ���. ������. */
  p_new_oes_321_id := rf_oes_321_seq.nextval;
  insert into rf_oes_321 (
    /* ����� �������� */ 
    oes_321_id, review_id, check_dt, created_by, created_date, modified_by, modified_date, manual_cmnt, current_fl, send_fl, check_status,
    /* �������� ��� ��������� */
    deleted_fl, send_dt, numb_p, date_p, action, regn, nd_ko, ktu_s, bik_s, numbf_s, branch, ktu_ss, bik_ss, numbf_ss, 
    tel, oes_321_org_id, data, date_s, terror, vo, dop_v, sume, sum, curren, prim_1, prim_2, num_pay_d, date_pay_d, metal, 
    priz6001, b_payer, b_recip, part, descr_1, descr_2, curren_con, sum_con, priz_sd, refer_r2, version )
  select /* ����� �������� */ 
         p_new_oes_321_id, p_new_review_id, null, v_owner_id, sysdate, null, null,
         /* �����������. ���� ��� ���� ��������� ���������� � ����� ��������, ������� �� � ���������� ����� � ����� */
         trim(regexp_replace(manual_cmnt, 'K���� �������� � [0-9]+, ��� � [0-9]+.'))||' K���� �������� � '||v_review_id||', ��� � '||p_oes_321_id||'. ',
         'Y', 'N', null, 
         /* �������� ��� ��������� */ 
         deleted_fl, send_dt, numb_p, date_p, action, regn, nd_ko, ktu_s, bik_s, numbf_s, branch, ktu_ss, bik_ss, numbf_ss, 
         tel, oes_321_org_id, data, date_s, terror, vo, dop_v, sume, sum, curren, prim_1, prim_2, num_pay_d, date_pay_d, metal, 
         priz6001, b_payer, b_recip, part, descr_1, descr_2, curren_con, sum_con, priz_sd, refer_r2, version
    from rf_oes_321
   where oes_321_id = p_oes_321_id;

  /* ������� ����� ��������� ���. ���������. */
  insert into rf_oes_321_party (
    /* ����� �������� */ 
    oes_321_id, 
    /* �������� ��� ��������� */ 
    block_nb, tu, pr, nameu, kodcr, kodcn, kd, sd, rg, nd, vd1, vd2, vd3, vd4, vd5, vd6, vd7, mc1, mc2, mc3, gr, bp, vp, 
    acc_b, acc_cor_b, amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, adress_s, adress_r, adress_g, adress_u, adress_d, 
    adress_k, adress_o, name_b, kodcn_b, bik_b, card_b, name_is, bik_is, name_r, kodcn_r, bik_r, reserv02, note, resrv_b2, 
    reserv612, oes_321_p_org_id, cust_seq_id/*, nameu_fi, nameu_f*/ )
  select /* ����� �������� */ 
         p_new_oes_321_id, 
         /* �������� ��� ��������� */ 
         block_nb, tu, pr, nameu, kodcr, kodcn, kd, sd, rg, nd, vd1, vd2, vd3, vd4, vd5, vd6, vd7, mc1, mc2, mc3, gr, bp, vp, 
         acc_b, acc_cor_b, amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, adress_s, adress_r, adress_g, adress_u, adress_d, 
         adress_k, adress_o, name_b, kodcn_b, bik_b, card_b, name_is, bik_is, name_r, kodcn_r, bik_r, reserv02, note, resrv_b2, 
         reserv612, oes_321_p_org_id, cust_seq_id/*, nameu_fi, nameu_f*/
    from rf_oes_321_party
   where oes_321_id = p_oes_321_id;

  exception 
    when others then
      p_new_review_id := null;
      p_new_oes_321_id := null;
      raise_application_error(-20001, '������ ��� ����������� ���������: '||sqlerrm);
end;

end rf_pkg_oes;
/

GRANT EXECUTE on mantas.rf_pkg_oes to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_oes to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_oes to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_oes to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_oes to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_oes to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_oes to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_oes to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_oes to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_oes to KYC;
GRANT EXECUTE on mantas.rf_pkg_oes to STD;
GRANT EXECUTE on mantas.rf_pkg_oes to KDD_ALG;
  
