create or replace package mantas.rf_pkg_request_searches is

  /* ����� ��������� ������� ��� ������� 
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json;

end;
/

create or replace package body mantas.rf_pkg_request_searches is
  
  /* ����������� �� ��������� ������� */
  c_row_count constant number := 50;
  
  /* ����� ��������� ������� ������ ������.
   * ������������� ���� ���������� ����������� �����������.
   *
   * ��������� ������
   *   p_key - �������� ��������� � ���� sys.anydata
   *   p_field_name - ��� ���� � ������� ������
   *   p_bind_name - ��� ���������� �����������
   *   p_bind - json ��� ���������� �����������
   *   p_condition - ������ AND ��� OR ��� ������� ������
   * ��������� ������
   *   varchar2 - ������ � �������� ������
   */
  function get_filter_value(
    p_key        in sys.anydata,
    p_field_name in varchar2,
    p_bind_name  in varchar2,
    p_bind       in out rf_json,
    p_condition  in varchar2 default 'and'
  ) return varchar2
  is
    v_string varchar2(500) := null;
    v_date   date := null;
    v_number number := null;
    v_filter varchar2(512);
  begin
    if sys.anydata.getTypeName(p_key) = 'SYS.VARCHAR2' then 
      v_string := sys.anydata.accessVarchar2(p_key);
      if v_string is not null then
        v_filter := ' '||p_condition||' '||p_field_name||' = :'||p_bind_name;
        p_bind.put(p_bind_name, v_string);
      else
        v_filter := null;
      end if;
      
    elsif sys.anydata.getTypeName(p_key) = 'SYS.NUMBER' then 
      v_number := sys.anydata.accessNumber(p_key);
      if v_number is not null then
        v_filter := ' '||p_condition||' '||p_field_name||' = :'||p_bind_name;
        p_bind.put(p_bind_name, v_number);
      else
        v_filter := null;
      end if;  
      
    elsif sys.anydata.getTypeName(p_key) = 'SYS.DATE' then 
      v_date := sys.anydata.accessDate(p_key);
      if v_date is not null then
        v_filter := ' '||p_condition||' '||p_field_name||' = :'||p_bind_name;
        p_bind.put(p_bind_name, v_date);
      else
        v_filter := null;
      end if;  
    
    else
      v_filter := null;
    end if;
    
    return v_filter;
  end;


  /* ����� ��� ��������� ������� SearchParty search
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_customers( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql         varchar2(16000) := null;
    v_temp_sql    varchar2(4000)  := null;
    v_bind_json   rf_json         := rf_json;
    v_search_type varchar2(8)     := null;
    v_tu          number          := null;
    v_current_oes321id number    := null;
    v_inn         varchar2(32)    := null;
    v_ogrn        varchar2(32)    := null;
    v_okpo        varchar2(32)    := null;
    v_doc_type    varchar2(32)    := null;
    v_doc_ser     varchar2(32)    := null;
    v_doc_num     varchar2(32)    := null;
    v_doc_sn      varchar2(255)   := null; 
    v_first_name  varchar2(255)   := null;
    v_last_name   varchar2(255)   := null;
    v_middle_name varchar2(255)   := null;
    v_fio         varchar2(800)   := null;
    v_birth_date  varchar2(32)    := null;
    v_account     varchar2(32)    := null;
    v_cust_seq_id number          := null;
    v_filter      varchar2(4000)  := null;
    v_filter_cust varchar2(1024)  := null;
    v_filter_doc  varchar2(1024)  := null;
    v_filter_acc  varchar2(1024)  := null;
    v_filter_fio  varchar2(1024)  := null;
    v_search_mode binary_integer;

  begin
    /* ��������� ��������� ������� */
    v_search_type := rf_json_ext.get_string(p_json, 'searchType');
    v_tu          := rf_json_ext.get_number(p_json, 'tu');
    v_current_oes321id:= rf_json_ext.get_number(p_json, 'oes_321_id');
    v_inn         := rf_json_ext.get_string(p_json, 'nd');
    v_ogrn        := rf_json_ext.get_string(p_json, 'rg');
    v_okpo        := rf_json_ext.get_string(p_json, 'okpo');
    v_doc_type    := rf_json_ext.get_string(p_json, 'kd');
    v_doc_ser     := rf_json_ext.get_string(p_json, 'series');
    v_doc_num     := rf_json_ext.get_string(p_json, 'vd1');
    v_doc_sn      := business.rf_normalize_docno(v_doc_ser, v_doc_num, null, v_doc_type);
    v_first_name  := rf_json_ext.get_string(p_json, 'first_nm');
    v_last_name   := rf_json_ext.get_string(p_json, 'last_nm');
    v_middle_name := rf_json_ext.get_string(p_json, 'midl_nm');
    v_fio         := business.rf_normalize_name(v_last_name||v_first_name||v_middle_name );
    v_birth_date  := rf_json_ext.get_string(p_json, 'gr');
    v_account     := rf_json_ext.get_string(p_json, 'acc_b');
    v_cust_seq_id := rf_json_ext.get_number(p_json, 'cust_seq_id');
    
    /* ���� �� ������� ����� ������� ��� ������ - ������ */
    if trim(v_inn) is null and trim(v_ogrn) is null and trim(v_okpo) is null and trim(v_account) is null
    and trim(v_first_name) is null and trim(v_last_name) is null and trim(v_middle_name) is null and trim(v_birth_date) is null
    and trim(v_doc_type) is null and trim(v_doc_ser) is null and trim(v_doc_num) is null and trim(v_cust_seq_id) is null then 
      raise_application_error(-20100, '�� ��������� �������� ��� ������.');
    end if;
    
    -- ���������� � �����
    rf_pkg_audit.add_search_params(
        par_audit_seq_id    => rf_pkg_request.get_current_audit_event_id
       ,par_last_nm         => v_last_name
       ,par_first_nm        => v_first_name
       ,par_midl_nm         => v_middle_name
       ,par_birth_dt        => to_date(v_birth_date, rf_json_ext.format_string)
       ,par_doc_type_cd     => v_doc_type
       ,par_doc_series_id   => v_doc_ser
       ,par_doc_no_id       => v_doc_num
       ,par_acct_nb         => v_account
       ,par_inn_nb          => v_inn
       ,par_ogrn_nb         => v_ogrn
       ,par_okpo_nb         => v_okpo
       ,par_cust_seq_id     => v_cust_seq_id);

    /* ���������� ������ �� �� �� ��������� - �����:
     *   - ��������� ���
     *   - �������� ��������
     *   - �������� ��� > 10 ��������
     *   - �������� ����� �����
     */
    /*if trim(v_first_name)||trim(v_last_name)||trim(v_middle_name) is not null
    or trim(v_doc_type)||trim(v_doc_ser)||trim(v_doc_num) is not null
    or length(v_inn) > 10 
    or trim(v_account) is not null then
      raise_application_error(-20101, '����� �� � ����� �� ������ ����� ���������.');
    end if;*/

    /* ���� ��������� ����� ���� ������� ���������, �� �� ��������� ������������ �������� - ������ */
    if  trim(v_doc_type)||trim(v_doc_ser)||trim(v_doc_num) is not null
    and v_doc_num is null then
      raise_application_error(-20100, '��������� �� ��� ������������ �������� � ����� ���������.');
    end if;

    /* ���� ��������� ����� ���� ������� ���, �� �� ��������� ������������ �������� - ������ */
    if  trim(v_first_name)||trim(v_last_name)||trim(v_middle_name)||trim(v_birth_date) is not null
    and (v_first_name is null or v_last_name is null) then
      raise_application_error(-20100, '��������� �� ��� ������������ �������� � ����� ���.');
    end if;

    /* ���� ��� ������: ��� ��� ����� */
    if v_search_type in ('ALL', 'OES') then
      v_sql := q'{
        select t.tu, t.nameu, t.nd, t.rg, t.gr, t.bp,
               t.kodcr, t.amr_s, t.amr_r, t.amr_g, t.amr_u, t.amr_d, t.amr_k, t.amr_o,
               t.kodcn, t.adress_s, t.adress_r, t.adress_g, t.adress_u, t.adress_d, t.adress_k, t.adress_o,
               t.kd, t.sd, t.vd1, t.vd2, t.vd3,
               t.vd4, t.vd5, t.vd6, t.vd7,
               t.mc1, t.mc2, t.mc3,
               nvl(t.amr_tx, t.adress_tx) addr_text,
               t.kd_tx,
               t.data trxn_dt, 
               null src_cd, 
               t.cust_seq_id, 
               t.code_disp_tx rv_status_nm, 
               'OES' row_type_cd, 
               '���' row_type_nm,
               owner_id
          from (select  p.tu as tu,
                       nullif(p.nameu, '0') as nameu, 
                       nullif(p.nd, '0') as nd, 
                       nullif(p.rg, '0') as rg, 
                       nullif(p.gr, to_date('01.01.2099', 'DD.MM.YYYY')) as gr, 
                       nullif(p.bp, '0') as bp,
                       
                       nullif(p.kodcr, '0') as kodcr, 
                       nullif(p.amr_s, '0') as amr_s, 
                       nullif(p.amr_r, '0') as amr_r, 
                       nullif(p.amr_g, '0') as amr_g, 
                       nullif(p.amr_u, '0') as amr_u, 
                       nullif(p.amr_d, '0') as amr_d, 
                       nullif(p.amr_k, '0') as amr_k, 
                       nullif(p.amr_o, '0') as amr_o,
                       business.rf_pkg_util.get_oes_addr_tx(p.oes_321_id, p.block_nb) amr_tx,
                       
                       nullif(p.kodcn, '0') as kodcn, 
                       nullif(p.adress_s, '0') as adress_s, 
                       nullif(p.adress_r, '0') as adress_r, 
                       nullif(p.adress_g, '0') as adress_g, 
                       nullif(p.adress_u, '0') as adress_u, 
                       nullif(p.adress_d, '0') as adress_d, 
                       nullif(p.adress_k, '0') as adress_k, 
                       nullif(p.adress_o, '0') as adress_o,
                       business.rf_pkg_util.get_oes_addr_tx(p.oes_321_id, p.block_nb, 'ADDR') adress_tx,
                       
                       nullif(p.kd, '0')  as kd,
                       dt.code_disp_tx    as kd_tx, 
                       nullif(p.sd, '0')  as sd,  
                       nullif(p.vd1, '0') as vd1, 
                       nullif(p.vd2, '0') as vd2,
                       nullif(p.vd3, to_date('01.01.2099', 'DD.MM.YYYY')) as vd3,
          
                       nullif(p.vd4, '0') as vd4,
                       nullif(p.vd5, '0') as vd5,
                       nullif(p.vd6, to_date('01.01.2099', 'DD.MM.YYYY')) as vd6, 
                       nullif(p.vd7, to_date('01.01.2099', 'DD.MM.YYYY')) as vd7, 
                       
                       nullif(p.mc1, '0') as mc1,
                       nullif(p.mc2, to_date('01.01.2099', 'DD.MM.YYYY')) as mc2, 
                       nullif(p.mc3, to_date('01.01.2099', 'DD.MM.YYYY')) as mc3, 
          
                       -- �������������� ��������
                       o.data,
                       p.cust_seq_id,
                       st.code_disp_tx,
                       row_number() over (order by o.data desc nulls last) rn,
                       ro.owner_id
                       
                  from mantas.rf_oes_321_party p
                  join mantas.rf_oes_321 o on o.oes_321_id = p.oes_321_id
                  join mantas.kdd_review r on r.review_id = o.review_id and r.rf_alert_type_cd = 'O'
                  left join mantas.kdd_code_set_trnln dt on dt.code_val = p.kd and dt.code_set = 'RF_OES_321_IDDOCTYPE'
                  left join mantas.kdd_code_set_trnln st on st.code_val = r.status_cd and st.code_set = 'AlertStatus'
                  left join mantas.kdd_review_owner ro on ro.owner_seq_id = r.owner_seq_id
                 where (r.status_cd in ('RF_READY', 'RF_DONE') )
                  and p.tu in (:TU,4)
                  and o.oes_321_id!=NVL(:current_oes321id,-1)
                  ##FILTER_OES##
               ) t
         where t.rn <= :ROW_COUNT
      }';
      
      v_sql := replace(v_sql, '##TU##', nvl(to_char(v_tu), 'null'));

      /* ������ �� ���� ��������� */
      v_bind_json.put('TU', v_tu);
      -- �� ����� �������� ���
      v_bind_json.put('current_oes321id', v_current_oes321id);
      
      v_filter := v_filter||
        /* ������ �� ��� */
        get_filter_value(sys.anydata.convertVarchar2(v_inn), 'p.nd', 'ND', v_bind_json)||
        /* ������ �� ���� */
        get_filter_value(sys.anydata.convertVarchar2(v_ogrn), 'p.rg', 'OGRN', v_bind_json)||
        /* ������ �� ���� */
        get_filter_value(sys.anydata.convertVarchar2(v_okpo), 'p.sd', 'OKPO', v_bind_json)||
        /* ������ �� ��������� */
        get_filter_value(sys.anydata.convertVarchar2(v_doc_type), 'nullif(p.kd, ''0'')', 'DOC_TP', v_bind_json)||
        /* ������ �� ����� ������ ��������� */
        get_filter_value(sys.anydata.convertVarchar2(v_doc_sn), 'business.rf_normalize_docno(p.sd, p.vd1, null, p.kd, p.tu)', 'DOC_SN', v_bind_json)||
        /* ������ �� ��� */
        /* �������� !!! ������������������ �������� �� ��� ������ ������ ��������� ��-�� ������� �������� AND ��� OR */
        get_filter_value(sys.anydata.convertVarchar2(v_fio), 'business.rf_normalize_name(p.nameu)', 'NAMEU_1', v_bind_json, 'and (')||
        get_filter_value(sys.anydata.convertVarchar2(v_fio), 'business.rf_normalize_name(p.nameu_fi)', 'NAMEU_2', v_bind_json, 'or')||
        get_filter_value(sys.anydata.convertVarchar2(v_fio), 'business.rf_normalize_name(p.nameu_f)', 'NAMEU_3', v_bind_json, 'or')||
        case 
          when v_fio is not null then ')'
          else null
        end||
        /* ������ �� ���� �������� */
        get_filter_value(sys.anydata.convertDate(to_date(v_birth_date, rf_json_ext.format_string)), 'p.gr', 'BIRTH_DT', v_bind_json)||
        /* ������ �� ������ ����� */
        get_filter_value(sys.anydata.convertVarchar2(v_account), 'p.acc_b', 'ACCOUNT', v_bind_json)||
        /* ������ �� ������ ������� */
        get_filter_value(sys.anydata.convertNumber(v_cust_seq_id), 'p.cust_seq_id', 'CUST_SEQ_ID', v_bind_json);
      
      v_sql := replace(v_sql, '##FILTER_OES##', v_filter);
    end if;

    /* ���� ��� ������: ���������� �������� ��� ����� */
    if v_search_type in ('ALL', 'CUST') then

      /* ���� ��� ������ ������ �� ���, �� ���������� ������� */
      if v_sql is not null then
        v_sql := v_sql||chr(10)||'union all'||chr(10);
      end if; 

      v_filter_cust := v_filter_cust||
        /* ������ �� ��� */
        get_filter_value(sys.anydata.convertVarchar2(v_inn), 't.tax_id', 'ND_C', v_bind_json)||
        /* ������ �� ���� */
        get_filter_value(sys.anydata.convertVarchar2(v_ogrn), 't.rf_ogrn_nb', 'OGRN_C', v_bind_json)||
        /* ������ �� ���� */
        get_filter_value(sys.anydata.convertVarchar2(v_okpo), 't.rf_okpo_nb', 'OKPO_C', v_bind_json)||
        /* ������ �� ������ ������� */
        get_filter_value(sys.anydata.convertNumber(v_cust_seq_id), 't.cust_seq_id', 'CUST_SEQ_ID_C', v_bind_json);
        

      v_filter_fio := v_filter_fio||  
        /* ������ �� ��� */
        /* �������� !!! ������������������ �������� �� ��� ������ ������ ��������� ��-�� ������� �������� AND ��� OR */
        get_filter_value(sys.anydata.convertVarchar2(v_fio), 'business.rf_normalize_name(t.last_nm||t.first_nm||t.midl_nm)', 'FIO_1', v_bind_json, 'and (')||
        get_filter_value(sys.anydata.convertVarchar2(v_fio), 'business.rf_normalize_name(t.last_nm||t.first_nm)', 'FIO_2', v_bind_json, 'or')||
        get_filter_value(sys.anydata.convertVarchar2(v_fio), 'business.rf_normalize_name(t.last_nm)', 'FIO_3', v_bind_json, 'or')||
        get_filter_value(sys.anydata.convertVarchar2(v_fio), 'business.rf_normalize_name(t.full_nm)', 'FIO_4', v_bind_json, 'or')||
        case 
          when v_fio is not null then ')'
          else null
        end||
        /* ������ �� ���� �������� */
        get_filter_value(sys.anydata.convertDate(to_date(v_birth_date, rf_json_ext.format_string)), 't.birth_dt', 'BIRTH_DT_C', v_bind_json);
        
      v_filter_doc := v_filter_doc||
        /* ������ �� ��������� */
        get_filter_value(sys.anydata.convertVarchar2(v_doc_type), 'dt.cbrf_cd', 'DOC_TP_C', v_bind_json)||
        /* ������ �� ����� � ������ ��������� */
        get_filter_value(sys.anydata.convertVarchar2(v_doc_sn), 'business.rf_normalize_docno(d.series_id, d.no_id, d.doc_type_cd)', 'DOC_SR_C', v_bind_json);
        
      v_filter_acc := v_filter_acc||
        /* ������ �� ������ ����� */
        get_filter_value(sys.anydata.convertVarchar2(v_account), 'a.alt_acct_id', 'ACCOUNT_C', v_bind_json);

      -- ��������� ��������� ������
      CASE 
        WHEN  v_filter_acc IS NOT NULL THEN  -- ����� �� ����� � ���������� �������
          v_search_mode:=1;
          v_temp_sql := q'{
            select /*+FIRST_ROWS*/ a.rf_prmry_cust_seq_id cust_seq_id, null rf_src_change_dt, row_number() over (order by a.rf_prmry_cust_seq_id desc nulls last) rn
              from business.acct a 
              join business.cust t on a.rf_prmry_cust_seq_id = t.cust_seq_id
             where 1 = 1}'||v_filter_acc||v_filter_cust;    

        WHEN  v_filter_cust IS NOT NULL OR (v_tu!=3 AND v_filter_fio IS NOT NULL) THEN -- ���c� �� ���������� �������
          v_search_mode:=2;
          v_temp_sql := q'{
            select /*+FIRST_ROWS*/ t.cust_seq_id, t.rf_src_change_dt, row_number() over (order by t.rf_src_change_dt desc nulls last) rn 
              from business.cust t
             where 1 = 1}'||v_filter_cust;

        WHEN  v_filter_doc IS NOT NULL AND v_tu!=3 THEN -- ����� �� ��������� � �� ��
          v_search_mode:=3;
          v_temp_sql := q'{
            select /*+ FIRST_ROWS INDEX(t.d, RF_CUSTIDDOC_NO_I) */ t.cust_seq_id, null rf_src_change_dt, row_number() over (order by t.cust_seq_id desc nulls last) rn
              from (select distinct d.cust_seq_id
                      from business.rf_cust_id_doc d
                      join business.rf_cust_id_doc_type dt on dt.doc_type_cd = d.doc_type_cd
                     where 1 = 1}'||v_filter_doc||') t';

        WHEN  v_filter_fio IS NOT NULL AND v_tu=3 THEN -- ����� �� ��� ��
          v_search_mode:=4;
          v_temp_sql := q'{
            select /*+FIRST_ROWS*/ t2.cust_seq_id, t2.rf_src_change_dt, row_number() over (order by t2.rf_src_change_dt desc nulls last) rn 
              from business.cust t2
                join business.cust t on t2.rf_ind_cust_seq_id=t.cust_seq_id
             where 1 = 1}'||v_filter_fio;

        WHEN  v_filter_doc IS NOT NULL AND v_tu=3 THEN --����� �� ��������� �� 
          v_search_mode:=5;
          v_temp_sql := q'{
            select /*+ FIRST_ROWS INDEX(t2.d, RF_CUSTIDDOC_NO_I) */ t.cust_seq_id, t.rf_src_change_dt, row_number() over (order by t.rf_src_change_dt desc nulls last) rn 
              from (select distinct d.cust_seq_id
                      from business.rf_cust_id_doc d
                      join business.rf_cust_id_doc_type dt on dt.doc_type_cd = d.doc_type_cd
                     where 1 = 1}'||v_filter_doc||') t2
                join business.cust t on t.rf_ind_cust_seq_id=t2.cust_seq_id';

        ELSE 
          RAISE_APPLICATION_ERROR(-20000,'�� ������ ��������� ������');
      END CASE;

      -- ��������� �������
      IF v_tu!=3 
        THEN -- �� ��
          -- ��������  ��� �� ��
          IF v_search_mode<=2 AND v_filter_fio IS NOT NULL THEN
            v_temp_sql:=v_temp_sql||' '||v_filter_fio;
          END IF;
          -- �������� �������� �� ��
          IF v_search_mode<=2 AND v_filter_doc IS NOT NULL THEN
             v_temp_sql := v_temp_sql||q'{
               and t.cust_seq_id in (select d.cust_seq_id
                                       from business.rf_cust_id_doc d
                                         join business.rf_cust_id_doc_type dt on dt.doc_type_cd = d.doc_type_cd
                                       where 1 = 1}'||v_filter_doc||')';

          END IF;
        ELSE -- ��
          -- �������� ��� ��
          IF v_search_mode<=2 AND v_filter_fio IS NOT NULL THEN
             v_temp_sql := v_temp_sql||q'{
               and t.rf_ind_cust_seq_id in (select t.cust_seq_id
                                       from business.cust t
                                       where 1 = 1}'||v_filter_fio||')';

          END IF;
          -- �������� ��������� ��
          IF v_search_mode<=2 AND v_filter_doc IS NOT NULL THEN
             v_temp_sql := v_temp_sql||q'{
               and t.rf_ind_cust_seq_id in (select d.cust_seq_id
                                       from business.rf_cust_id_doc d
                                         join business.rf_cust_id_doc_type dt on dt.doc_type_cd = d.doc_type_cd
                                       where 1 = 1}'||v_filter_doc||')';
          END IF;
          -- �������� ��������� �� � ��� ��
          IF v_search_mode=4 AND v_filter_doc IS NOT NULL THEN
             v_temp_sql := v_temp_sql||q'{
               and t.cust_seq_id in (select d.cust_seq_id
                                       from business.rf_cust_id_doc d
                                         join business.rf_cust_id_doc_type dt on dt.doc_type_cd = d.doc_type_cd
                                       where 1 = 1}'||v_filter_doc||')';
          END IF;
      END IF;

      v_sql := v_sql||q'{
          select oc.tu, oc.nameu, oc.nd, oc.rg, oc.gr, oc.bp,
                 oc.kodcr, oc.amr_s, oc.amr_r, oc.amr_g, oc.amr_u, oc.amr_d, oc.amr_k, oc.amr_o,
                 oc.kodcn, oc.adress_s, oc.adress_r, oc.adress_g, oc.adress_u, oc.adress_d, oc.adress_k, oc.adress_o,
                 oc.kd, oc.sd, oc.vd1, oc.vd2, oc.vd3,
                 oc.vd4, oc.vd5, oc.vd6, oc.vd7,
                 oc.mc1, oc.mc2, oc.mc3,
                 nvl(oc.amr_tx, oc.adress_tx) address_text,
                 dt.code_disp_tx kd_tx,
                 c.rf_src_change_dt trxn_dt, 
                 oc.src_cd, 
                 oc.cust_seq_id, 
                 null rv_status_nm, 
                 'CUST' row_type_cd, 
                 '�������' row_type_nm,
                 null as owner_id
            from ( ##TEMP_SQL## ) c
            join table(mantas.rfv_oes_cust(c.cust_seq_id, null)) oc on 1=1
            left join mantas.kdd_code_set_trnln dt on dt.code_val = oc.kd and dt.code_set = 'RF_OES_321_IDDOCTYPE'
           where c.rn <= :ROW_COUNT 
        }';
      v_sql := replace(v_sql, '##TEMP_SQL##', v_temp_sql);
    end if;

    /* ����������: �� ��������� - ���, �������; �� ���� �������� */
    v_sql := 'select * from ('||v_sql||') order by decode(row_type_cd, ''OES'', 0, ''CUST'', 1, 2), trxn_dt desc nulls last';
    /* ����������� ������� */
    v_bind_json.put('ROW_COUNT', c_row_count);
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;


  /* ����� ��������� ������� ��� ������� 
   *   
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json
  is
    v_request    clob := null;
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
  begin
    /* ������ ���� � ������ ��� "T" */
    v_request := regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2');
    
    v_json_in := rf_json(v_request);
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');

    if v_form_name = 'OESCustSearch' and v_action = 'search' 
      then v_json_out := get_customers( v_json_in, p_id_request );
    end if;

    return v_json_out;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        v_json_out := rf_json();
        v_json_out.put( 'success', false );
        if sqlcode in (-20100, -20101) then
          v_json_out.put( 'message', substr(sqlerrm(), 12));
        else
          v_json_out.put( 'message', '�� ������� ��������� ������! ��������� ���������� ������.'||chr(13)||sqlerrm() );
        end if;  
        return v_json_out;
  end;

end;
/
