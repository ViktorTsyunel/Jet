CREATE OR REPLACE
PROCEDURE mantas.rf_sleep(seconds NUMBER) IS
BEGIN
  dbms_lock.sleep(seconds);
END rf_sleep;
/
  
GRANT EXECUTE on mantas.rf_sleep to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_sleep to KDD_MINER;
GRANT EXECUTE on mantas.rf_sleep to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_sleep to MANTAS_READER;
GRANT EXECUTE on mantas.rf_sleep to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_sleep to BUSINESS;
GRANT EXECUTE on mantas.rf_sleep to CMREVMAN;
GRANT EXECUTE on mantas.rf_sleep to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_sleep to KDD_REPORT;
GRANT EXECUTE on mantas.rf_sleep to KYC;
GRANT EXECUTE on mantas.rf_sleep to STD;
GRANT EXECUTE on mantas.rf_sleep to KDD_ALG;
