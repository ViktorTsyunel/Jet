--grant execute on dbms_lock to mantas;

create or replace package mantas.rf_pkg_arh is
  --
  -- ������� �������� � p_source ��� p_pattern �� p_replace
  -- 
  function replace_clob(p_source clob, p_pattern varchar2, p_replace clob)
    return clob;
--
-- ��������� ������������� �������������� ����������, ��������������� �������������� �������� ���������, � ������� ������ dbms_lock, 
-- ����� ���� ������� rf_arh_object ���������
-- � �� ������ ������ ���������� ����� ������� ��������� ������.
-- ��������! ��� ��������� ���������� ����������� COMMIT! (��� ��������� dbms_lock.allocate_unique � truncate rf_arh_object)
-- �������� �������� �������� ���������:
--  1) lock_arh_object
--  2) ���������� ������� rf_arh_object ���������� (�/��� ������� ����������), ������������ � ��������� 
--  3) enrich_data - ���������� ������ ������������ ��������� 
--  4) archive_all - ����������, �������������
--  5) release_arh_object
--  
PROCEDURE lock_arh_object;
--
-- ��������� ������� �������������� ����������, ��������������� �������������� �������� ���������
-- ��������! ��� ������ ���������� ����������� COMMIT! (��� ��������� dbms_lock.allocate_unique)
--  
PROCEDURE release_arh_object;
  --
  -- ������� ������� ��� �������, ������������� ������.
  -- ��� ��������� ������������ ����������� ������� mantas.rf_arh_tab_list � mantas.rf_arh_template  
  --  
  function generate_archive_code(p_owner      varchar2, -- �������� ����� �������
                                 p_table_name varchar2, -- ��� �������
                                 p_query_nb   mantas.rf_arh_tab_list.query_nb%type default 1, -- ����� �������� �� mantas.rf_arh_tab_list.query_nb
                                 p_single_mode  boolean default false -- ���� ������ ��������� ����� �������� - ����� GTT
                                 ) return clob;
--
-- ��������� GTT-������� ���������������� ������������ �������� ��� ��������� ��������
--
PROCEDURE fill_arh_object_gtt
(
  par_op_cat_cd            VARCHAR2, -- ��������� ��������: 'WT', 'CT', 'BOT'
  par_trxn_seq_id          NUMBER,   -- ID ��������
  par_arh_dump_dt          DATE      -- ���� - ����� �������, � ������� ������� ��������� ������ � �������� �������
);
-- 
-- ��������� ��������� ��������� �������� � ��������� � ��� ��������/������/������ �����
-- � �������� ��������� ������������ Global Temporary Table � �� ����������� COMMIT!
--  
PROCEDURE archive_one
(
  p_id          number,   -- ID ��������
  p_entity      varchar2, -- ��������� ��������: 'WT', 'CT', 'BOT'
  p_arh_dump_dt date default trunc(sysdate, 'HH') -- ���� - ����� �������, � ������� ������� ��������� ������ � �������� �������
);
--
-- "���������" ������� ��������������� ������������ �������� ��� ��������� ����������� - �������� � ��� �������������� ��������/������/������ �����,
-- ��������� � ��� ����������� � ������ ����������� ����������
-- ��������� ���������� �� ������������ ������ dbms_parallel_execute
--
PROCEDURE enrich_sp
(
  par_partition_position   INTEGER, -- ���������� ����� �������������� �������� (dba_tab_partitions.partition_position)
  par_subpart_position     INTEGER  -- ���������� ����� �������������� ����������� (dba_tab_subpartitions.subpartition_position)
);
--
-- "���������" ������� ��������������� ������������ �������� ��� ��������� ����������� - �������� � ��� �������������� ������ ����� ���������,
-- ��� ��������, ��� ���������� � ������ �����������
-- ��������� ���������� �� ������������ ������ dbms_parallel_execute
--
PROCEDURE enrich_cust_cust_sp
(
  par_partition_position   INTEGER, -- ���������� ����� �������������� �������� CUST (dba_tab_partitions.partition_position)
  par_subpart_position     INTEGER  -- ���������� ����� �������������� ����������� (dba_tab_subpartitions.subpartition_position)
);
--
-- "���������" ������� ��������������� ������������ �������� - �������� � ��� �������������� ��������/������/������ �����,
-- ��������� � ��� ����������� � ������ ������� ����������
-- ��������� ���������� � �������� ��������� ������������� ��������, ����������� ������������� �������� dbms_parallel_execute
--
FUNCTION enrich_arh_object RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
--
-- ��������� ��������� ������ ������������� ��� ��������, �������������� ������� ��������� � ��������� ����������� ������� RF_ARH_OBJECT
-- ��������� ���������� �� ������������ ������ dbms_parallel_execute
--
PROCEDURE archive_sp
(
  par_query_id             mantas.rf_arh_queries.query_id%TYPE, -- ID ������� �������������, ������� ���� ���������
  par_subpart_position     INTEGER                              -- ���������� ����� �������������� ����������� (dba_tab_subpartitions.subpartition_position)
);
--
-- ��������� ��������� ��������, �������������� ������� �������� � ������� RF_ARH_OBJECT/RF_ARH_OBJECT_GTT
-- ��������! ��������������, ��� "����������" (��. ���������� ���������) ��� ���������!
--
FUNCTION archive_all RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
end rf_pkg_arh;
/
create or replace package body mantas.rf_pkg_arh is
--
-- ��� ��� ������� generate_archive_code (�����-�� �������� ������� ���������� ������������� ������-�� ������������� �������� �������)
--
TYPE TAB_CLOB_CACHE IS TABLE OF CLOB INDEX BY VARCHAR2(2000);
pck_gen_arh_code_cache  TAB_CLOB_CACHE;
pck_arh_tab_scn         NUMBER;
  --
  -- ������� �������� � p_source ��� p_pattern �� p_replace
  -- 
  function replace_clob(p_source clob, p_pattern varchar2, p_replace clob)
    return clob is
    v_source         clob default null;
    v_pos            number;
    v_length_replace number;
    v_length_pattern number;
  begin
    v_source         := p_source;
    v_pos            := instr(v_source, p_pattern, 1);
    v_length_replace := nvl(length(p_replace), 0);
    v_length_pattern := nvl(length(p_pattern), 0);
    while v_pos > 0 loop
      v_source := substr(v_source, 1, v_pos - 1) || p_replace ||
                  substr(v_source, v_pos + v_length_pattern);
      v_pos    := instr(v_source, p_pattern, v_pos + v_length_replace);
    end loop;
    return v_source;
  end;
--
-- ��������� ������������� �������������� ����������, ��������������� �������������� �������� ���������, � ������� ������ dbms_lock, 
-- ����� ���� ������� rf_arh_object ���������
-- � �� ������ ������ ���������� ����� ������� ��������� ������.
-- ��������! ��� ��������� ���������� ����������� COMMIT! (��� ��������� dbms_lock.allocate_unique � truncate rf_arh_object)
-- �������� �������� �������� ���������:
--  1) lock_arh_object
--  2) ���������� ������� rf_arh_object ���������� (�/��� ������� ����������), ������������ � ��������� 
--  3) enrich_data - ���������� ������ ������������ ��������� 
--  4) archive_all - ����������, �������������
--  5) release_arh_object
--  
PROCEDURE lock_arh_object IS
  var_lockhandle     VARCHAR2(2000 CHAR);
  var_lockres        INTEGER;
  var_count          INTEGER;
BEGIN 
  --
  -- ������������� ���������� � ������ AML_ARH
  --
  dbms_lock.allocate_unique('AML_ARH', var_lockhandle);

  var_lockres := dbms_lock.request(var_lockhandle, dbms_lock.x_mode);

  if var_lockres = 0 /*Success*/ or var_lockres = 4 /*Already own lock*/ Then
    null;
  else
    raise_application_error(-20001, '������ ��� ������� ���������� ���������� ������ �������� ������������� (AML_ARH). ��� �������� dbms_lock.request: '||
                                    to_char(var_lockres));
  end if;  
  --
  -- ������� ������� � �������� ������������ ��������, ���� ��� ����� �� ����� 
  -- (������ ��� ��������� ����� ������� �������� ������)
  -- ��������� DELETE, �. �. TRUNCATE ��� ��������� ����� ����� �����
  --
  SELECT count(*)
    INTO var_count
    FROM mantas.rf_arh_object
   WHERE rownum <= 1;
   
  if var_count > 0 Then
    DELETE FROM mantas.rf_arh_object;
    COMMIT;
  end if;   
END lock_arh_object;
--
-- ��������� ������� �������������� ����������, ��������������� �������������� �������� ���������
-- ��������! ��� ������ ���������� ����������� COMMIT! (��� ��������� dbms_lock.allocate_unique)
--  
PROCEDURE release_arh_object IS
  var_lockhandle     VARCHAR2(2000 CHAR);
  var_lockres        INTEGER;
BEGIN 
  --
  -- ������� ���������� � ������ AML_ARH
  --
  dbms_lock.allocate_unique('AML_ARH', var_lockhandle);

  var_lockres := dbms_lock.release(var_lockhandle);

  if var_lockres = 0 /*Success*/ Then
    null;
  else
    raise_application_error(-20001, '������ ��� ������� ���������� ���������� ������ �������� ������������� (AML_ARH). ��� �������� dbms_lock.release: '||
                                    to_char(var_lockres));
  end if;  
END release_arh_object;
  --
  -- ������� ������� ��� �������, ������������� ������.
  -- ��� ��������� ������������ ����������� ������� mantas.rf_arh_tab_list � mantas.rf_arh_template  
  --  
  function generate_archive_code(p_owner      varchar2, -- �������� ����� �������
                                 p_table_name varchar2, -- ��� �������
                                 p_query_nb   mantas.rf_arh_tab_list.query_nb%type default 1, -- ����� �������� �� mantas.rf_arh_tab_list.query_nb
                                 p_single_mode  boolean default false -- ���� ������ ��������� ����� �������� - ����� GTT
                                 ) return clob is
    v_tmp_collist     clob default null;
    v_tmp_collists    clob default null;
    v_entity_cd       clob default null;
    v_entity_id       clob default null;
    v_arh_table       clob default null;
    v_id_table        clob default null;
    v_src_table       clob default null;
    v_uk_colvt        clob default null;
    v_col_list_update clob default null;
    v_tmp_sql         clob default null;
    v_arh_templ_cd    varchar2(100) default null;
  begin
    --
    -- ������� ��� ��������, ���� ���������� ������ � �������� rf_arh_tab_list, rf_arh_template
    --
    FOR r IN (SELECT GREATEST(tbl.arh_tab_scn, templ.arh_tab_scn) as arh_tab_scn
                FROM (select nvl(max(ora_rowscn), -1) as arh_tab_scn
                        from mantas.rf_arh_tab_list) tbl
                     cross join        
                     (select nvl(max(ora_rowscn), -1) arh_tab_scn
                        from mantas.rf_arh_template) templ
               WHERE /*����� ���������� ��������� �� ��������� � �����*/
                     GREATEST(tbl.arh_tab_scn, templ.arh_tab_scn) <> nvl(pck_arh_tab_scn, -2)) LOOP
      pck_gen_arh_code_cache.DELETE();
      
      if r.arh_tab_scn <> -1 Then
        pck_arh_tab_scn := r.arh_tab_scn;
      end if;  
    END LOOP;           
    --
    -- ���� ��� ���� �������/������� ������ ��������� ��� ����������� - ������� �������� �� ����
    --
    if pck_gen_arh_code_cache.EXISTS(p_owner||'|'||p_table_name||'|'||to_char(p_query_nb)||case when p_single_mode then '|GTT' end) Then
      return pck_gen_arh_code_cache(p_owner||'|'||p_table_name||'|'||to_char(p_query_nb)||case when p_single_mode then '|GTT' end);
    end if;                                            
    --
    -- ��������� ���� - ������ ���� ��������. ������� ��������� ���������� ������� �������������.
    -- 
    for r in (select tl.*, tmpl.templ_sql_tx
                from mantas.rf_arh_tab_list tl
                join mantas.rf_arh_template tmpl
                  on tmpl.arh_templ_cd = tl.arh_templ_cd
               where tl.owner_nm = p_owner
                 and tl.table_nm = p_table_name
                 and tl.query_nb = p_query_nb) loop
      v_entity_cd       := '';
      v_entity_id       := '';
      v_arh_table       := '';
      v_id_table        := '';
      v_src_table       := '';
      v_uk_colvt        := '';
      v_tmp_collist     := '';
      v_tmp_collists    := '';
      v_col_list_update := '';
      v_tmp_sql         := '';
      v_arh_templ_cd    := '';
      v_entity_cd       := r.entity_cd;
      v_entity_id       := r.entity_id_col_nm;
      v_arh_table       := upper(r.owner_nm || '.' || r.table_nm || '_arh');
      v_id_table        := case when p_single_mode 
                                then 'MANTAS.RF_ARH_OBJECT_GTT' 
                                else 'MANTAS.RF_ARH_OBJECT #PART_SUBPART#' 
                           end;
      v_src_table       := upper(r.owner_nm || '.' || r.table_nm);
      v_arh_templ_cd    := r.arh_templ_cd;
      --
      -- ��������� ������ ��� ������ ������� #UK_COL(v,t)# 
      -- (������ ������� ����������� ����� - ������� �� ��������� ���� <1-�� ��������� �� ������>.<��� �������> = <2-�� ��������� �� ������>.<��� �������>)
      --                  
      for k in (select rownum as rn, ac.column_name, tmp.alione, tmp.alitwo
                  from all_cons_columns ac
                  join (select rtrim(regexp_substr(z.lex, '\w+,', 1, 1), ',') alione,
                              rtrim(regexp_substr(z.lex, '\w+\)', 1, 1), ')') alitwo
                         from (select regexp_substr(r.templ_sql_tx,
                                                    '#UK_COL\([^#]+\)#',
                                                    1,
                                                    1) lex
                                 from dual) z) tmp
                    on 1 = 1
                 where ac.owner = r.owner_nm
                   and ac.table_name = r.table_nm
                   and ac.constraint_name = r.table_uk_nm) loop
        v_uk_colvt := v_uk_colvt || case
                        when k.rn = 1 then
                         null
                        else
                         ' and '
                      end || k.alione || '.' || k.column_name || ' = ' ||
                      k.alitwo || '.' || k.column_name;
      end loop;
      --
      -- ��������� ������ �� ������� ������� ��� ������ �������� #COL_LIST(v)#, #COL_LIST#
      --                  
      for n in (select distinct tc.column_name
                  from mantas.rf_arh_tab_list atl
                  join all_tab_cols tc
                    on tc.owner = atl.owner_nm
                   and tc.table_name = atl.table_nm
                   and tc.column_name not in
                       (select upper(trim(column_value)) as column_name
                          from table(mantas.rf_pkg_scnro.list_to_tab(par_string    => atl.exclude_columns_tx,
                                                                     par_delimiter => ',')))
                 where atl.owner_nm = r.owner_nm
                   and atl.table_nm = r.table_nm
                   and atl.query_nb = p_query_nb
                   and tc.hidden_column = 'NO'    -- �� ����������: �������
                   and tc.virtual_column <> 'YES' --                � ����������� �������,
                   and EXISTS(select null         --                � ����� �������, ������������� � �������� �������
                                from all_tab_cols tca
                               where tca.owner = tc.owner and
                                     tca.table_name = tc.table_name||'_ARH' and
                                     tca.column_name = tc.column_name)) loop
        v_tmp_collist := v_tmp_collist || case
                           when v_tmp_collist is not null then
                            ','
                           else
                            null
                         end || n.column_name;
        v_tmp_collists := v_tmp_collists || case
                            when v_tmp_collists is not null then
                             ',#alias#.'
                            else
                             '#alias#.'
                          end || n.column_name;
      end loop;    
      --
      -- ��������� ������ �� ������� ������� ��� ������ ������� #COL_LIST_UPDATE(v)#
      --                  
      for m in (select distinct tc.owner, tc.table_name, tc.column_name
                  from mantas.rf_arh_tab_list atl
                  join all_tab_cols tc
                    on tc.owner = atl.owner_nm
                   and tc.table_name = atl.table_nm
                   and tc.column_name not in
                       (select trim(column_value) as column_name
                          from table(mantas.rf_pkg_scnro.list_to_tab(par_string    => atl.exclude_columns_tx,
                                                                     par_delimiter => ',')))
                   and tc.column_name not in
                       (select acc.column_name
                          from all_cons_columns acc
                         where atl.owner_nm = acc.owner
                           and atl.table_nm = acc.table_name
                           and atl.table_uk_nm = acc.constraint_name)
                 where atl.owner_nm = r.owner_nm
                   and atl.table_nm = r.table_nm
                   and atl.query_nb = p_query_nb
                   and tc.hidden_column = 'NO'    -- �� ����������: �������
                   and tc.virtual_column <> 'YES' --                � ����������� �������,
                   and EXISTS(select null         --                � ����� �������, ������������� � �������� �������
                                from all_tab_cols tca
                               where tca.owner = tc.owner and
                                     tca.table_name = tc.table_name||'_ARH' and
                                     tca.column_name = tc.column_name)) loop
        v_col_list_update := v_col_list_update || case
                               when v_col_list_update is not null then
                                ','
                               else
                                null
                             end || m.column_name || '=' || '#alias#.' ||
                             m.column_name;
      end loop;
      --
      -- �������� �������
      --                      
      v_tmp_sql := r.templ_sql_tx;
      v_tmp_sql := regexp_replace(v_tmp_sql, '#ARH_TABLE#', v_arh_table);
      v_tmp_sql := regexp_replace(v_tmp_sql, '#ID_TABLE#', v_id_table);
      v_tmp_sql := regexp_replace(v_tmp_sql, '#SRC_TABLE#', v_src_table);
      v_tmp_sql := regexp_replace(v_tmp_sql, '#ENTITY_ID#', v_entity_id);
      v_tmp_sql := regexp_replace(v_tmp_sql, '#ENTITY_CD#', v_entity_cd);
      v_tmp_sql := regexp_replace(v_tmp_sql, '#UK_COL\([^#]+\)#', v_uk_colvt);
      v_tmp_sql := regexp_replace(v_tmp_sql, '#COL_LIST#', v_tmp_collist);
    
      for z in (select distinct to_char(regexp_substr(templ_sql_tx,
                                                      '#COL_LIST\([^#]+\)#',
                                                      1,
                                                      level)) as string_to_replace,
                                to_char(regexp_replace(regexp_substr(templ_sql_tx,
                                                                     '#COL_LIST\([^#]+\)#',
                                                                     1,
                                                                     level),
                                                       '#COL_LIST\((.+)\)#',
                                                       '\1')) as ali
                  from mantas.rf_arh_template
                 where arh_templ_cd = v_arh_templ_cd
                connect by level <=
                           regexp_count(templ_sql_tx, '#COL_LIST\([^#]+\)#')) loop
        v_tmp_sql := replace_clob(v_tmp_sql,
                                  z.string_to_replace,
                                  replace_clob(v_tmp_collists,
                                               '#alias#',
                                               z.ali));
      end loop;
    
      for z in (select distinct to_char(regexp_substr(templ_sql_tx,
                                                      '#COL_LIST_UPDATE\([^#]+\)#',
                                                      1,
                                                      level)) as string_to_replace,
                                to_char(regexp_replace(regexp_substr(templ_sql_tx,
                                                                     '#COL_LIST_UPDATE\([^#]+\)#',
                                                                     1,
                                                                     level),
                                                       '#COL_LIST_UPDATE\((.+)\)#',
                                                       '\1')) as ali
                  from mantas.rf_arh_template
                 where arh_templ_cd = v_arh_templ_cd
                connect by level <=
                           regexp_count(templ_sql_tx,
                                        '#COL_LIST_UPDATE\([^#]+\)#')) loop
        v_tmp_sql := replace_clob(v_tmp_sql,
                                  z.string_to_replace,
                                  replace_clob(v_col_list_update,
                                               '#alias#',
                                               z.ali));
      end loop;
      EXIT;                  
    end loop;

    --
    -- � ������ ������������� ��������� �������� ��������� ��� � ���������� ���������
    -- (��� �������� ������������� ������� ����������� �� ��������������� ������� - ��� �� �����)
    --
    if p_single_mode Then
      pck_gen_arh_code_cache(p_owner||'|'||p_table_name||'|'||to_char(p_query_nb)||case when p_single_mode then '|GTT' end) := v_tmp_sql;
    end if;      
    
    return v_tmp_sql;
  end generate_archive_code;
--
-- ��������� GTT-������� ���������������� ������������ �������� ��� ��������� ��������
--
PROCEDURE fill_arh_object_gtt
(
  par_op_cat_cd            VARCHAR2, -- ��������� ��������: 'WT', 'CT', 'BOT'
  par_trxn_seq_id          NUMBER,   -- ID ��������
  par_arh_dump_dt          DATE      -- ���� - ����� �������, � ������� ������� ��������� ������ � �������� �������
) IS
  prcname      VARCHAR2(80 CHAR) := ' rf_pkg_arh.fill_arh_object_gtt';
BEGIN
  --
  -- �������� ���������
  --
  if par_op_cat_cd is null or par_op_cat_cd not in ('WT', 'CT', 'BOT') or par_trxn_seq_id is null or
     par_arh_dump_dt is null Then
    raise_application_error(-20001, '�������� �������� ���������'||prcname);
  end if;
  --
  -- ������� ��������� ������������� �������� � �������������� ��������� � ��� ��������/������/������ ����� �
  -- GTT-������� ������������ ��������
  --
  DELETE FROM mantas.rf_arh_object_gtt;
  
  if par_op_cat_cd = 'WT' Then
    
    INSERT INTO mantas.rf_arh_object_gtt(entity_id, entity_cd, arh_dump_dt) 
    (SELECT /*+ LEADING(wt) USE_NL(wts ac1 ac2)*/ 
            distinct 
            decode(lev, 1, nullif(wt.rf_orig_cust_seq_id, -1),
                        2, nullif(wt.rf_benef_cust_seq_id, -1),
                        3, nullif(wt.rf_scnd_orig_cust_seq_id, -1),
                        4, nullif(wt.rf_scnd_benef_cust_seq_id, -1),
                        5, nullif(wt.rf_send_cust_seq_id, -1),
                        6, nullif(wt.rf_rcv_cust_seq_id, -1),
                        7, nullif(wt.rf_orig_acct_seq_id, -1),
                        8, nullif(wt.rf_benef_acct_seq_id, -1),
                        9, wts.scrty_seq_id,
                        10, nullif(ac1.rf_prmry_cust_seq_id, -1),
                        11, nullif(ac2.rf_prmry_cust_seq_id, -1),
                        12, wt.fo_trxn_seq_id) as entity_id,
            decode(lev, 1, 'CUST',
                        2, 'CUST',
                        3, 'CUST',
                        4, 'CUST',
                        5, 'CUST',
                        6, 'CUST',
                        7, 'ACCT',
                        8, 'ACCT',
                        9, 'SCRTY',
                        10, 'CUST',
                        11, 'CUST',
                        12, 'WT') as entity_cd,
            par_arh_dump_dt as arh_dump_dt
       FROM business.wire_trxn wt
            left join business.rf_wire_trxn_scrty wts on wts.fo_trxn_seq_id = wt.fo_trxn_seq_id and
                                                         wts.scrty_seq_id is not null
            left join business.acct ac1 on ac1.acct_seq_id = wt.rf_orig_acct_seq_id and
                                           ac1.rf_prmry_cust_seq_id is not null and
                                           wt.rf_orig_acct_seq_id <> -1
            left join business.acct ac2 on ac2.acct_seq_id = wt.rf_benef_acct_seq_id and
                                           ac2.rf_prmry_cust_seq_id is not null and
                                           wt.rf_benef_acct_seq_id <> -1
            join (select level as lev
                    from dual
                  connect by level <= 12) on decode(lev, 1, nullif(wt.rf_orig_cust_seq_id, -1),
                                                         2, nullif(wt.rf_benef_cust_seq_id, -1),
                                                         3, nullif(wt.rf_scnd_orig_cust_seq_id, -1),
                                                         4, nullif(wt.rf_scnd_benef_cust_seq_id, -1),
                                                         5, nullif(wt.rf_send_cust_seq_id, -1),
                                                         6, nullif(wt.rf_rcv_cust_seq_id, -1),
                                                         7, nullif(wt.rf_orig_acct_seq_id, -1),
                                                         8, nullif(wt.rf_benef_acct_seq_id, -1),
                                                         9, wts.scrty_seq_id,
                                                         10, nullif(ac1.rf_prmry_cust_seq_id, -1),
                                                         11, nullif(ac2.rf_prmry_cust_seq_id, -1),
                                                         12, wt.fo_trxn_seq_id) is not null
      WHERE wt.fo_trxn_seq_id = par_trxn_seq_id); 
         
  elsif par_op_cat_cd = 'CT' Then
    
    INSERT INTO mantas.rf_arh_object_gtt(entity_id, entity_cd, arh_dump_dt) 
    (SELECT /*+ LEADING(ct) USE_NL(cts ac)*/ 
            distinct 
            decode(lev, 1, nullif(ct.rf_cust_seq_id, -1),
                        2, nullif(ct.rf_cndtr_cust_seq_id, -1),
                        3, nullif(ct.rf_acct_seq_id, -1),
                        4, cts.scrty_seq_id,
                        5, nullif(ac.rf_prmry_cust_seq_id, -1),
                        6, ct.fo_trxn_seq_id) as entity_id,
            decode(lev, 1, 'CUST',
                        2, 'CUST',
                        3, 'ACCT',
                        4, 'SCRTY',
                        5, 'CUST',
                        6, 'CT') as entity_cd,
            par_arh_dump_dt as arh_dump_dt
       FROM business.cash_trxn ct
            left join business.rf_cash_trxn_scrty cts on cts.fo_trxn_seq_id = ct.fo_trxn_seq_id and
                                                         cts.scrty_seq_id is not null
            left join business.acct ac on ac.acct_seq_id = ct.rf_acct_seq_id and
                                          ac.rf_prmry_cust_seq_id is not null and
                                          ct.rf_acct_seq_id <> -1
            join (select level as lev
                    from dual
                  connect by level <= 6) on decode(lev, 1, nullif(ct.rf_cust_seq_id, -1),
                                                        2, nullif(ct.rf_cndtr_cust_seq_id, -1),
                                                        3, nullif(ct.rf_acct_seq_id, -1),
                                                        4, cts.scrty_seq_id,
                                                        5, nullif(ac.rf_prmry_cust_seq_id, -1),
                                                        6, ct.fo_trxn_seq_id) is not null
      WHERE ct.fo_trxn_seq_id = par_trxn_seq_id);
      
  elsif par_op_cat_cd = 'BOT' Then
  
    INSERT INTO mantas.rf_arh_object_gtt(entity_id, entity_cd, arh_dump_dt) 
    (SELECT /*+ LEADING(bot) USE_NL(ac)*/ 
            distinct 
            decode(lev, 1, nullif(bot.rf_cust_seq_id, -1),
                        2, nullif(bot.rf_acct_seq_id, -1),
                        3, nullif(ac.rf_prmry_cust_seq_id, -1),
                        4, bot.bo_trxn_seq_id) as entity_id,
            decode(lev, 1, 'CUST', 
                        2, 'ACCT',
                        3, 'CUST',
                        4, 'BOT') as entity_cd,
            par_arh_dump_dt as arh_dump_dt
       FROM business.back_office_trxn bot
            left join business.acct ac on ac.acct_seq_id = bot.rf_acct_seq_id and
                                          ac.rf_prmry_cust_seq_id is not null and
                                          bot.rf_acct_seq_id <> -1
            join (select level as lev
                    from dual
                  connect by level <= 4) on decode(lev, 1, nullif(bot.rf_cust_seq_id, -1),
                                                        2, nullif(bot.rf_acct_seq_id, -1),
                                                        3, nullif(ac.rf_prmry_cust_seq_id, -1),
                                                        4, bot.bo_trxn_seq_id) is not null
      WHERE bot.bo_trxn_seq_id = par_trxn_seq_id);

  end if;
  --
  -- ������� � GTT-������� �������������� ������������ ������ ����� ���������
  --
  INSERT INTO mantas.rf_arh_object_gtt(entity_id, entity_cd, arh_dump_dt) 
  (SELECT /*+ ORDERED USE_NL(cc) INDEX(cc) */ 
          cc.cust_cust_seq_id as entity_id,
          'CUST_CUST' as entity_cd,
          par_arh_dump_dt as arh_dump_dt
     FROM (select distinct entity_id from mantas.rf_arh_object_gtt where entity_cd = 'CUST') t
          join business.cust_cust cc on cc.rf_cust_seq_id = t.entity_id
   UNION
   SELECT /*+ ORDERED USE_NL(cc) INDEX(cc) */ 
          cc.cust_cust_seq_id as entity_id,
          'CUST_CUST' as entity_cd,
          par_arh_dump_dt as arh_dump_dt
     FROM (select distinct entity_id from mantas.rf_arh_object_gtt where entity_cd = 'CUST') t
          join business.cust_cust cc on cc.rf_rltd_cust_seq_id = t.entity_id);
          
END fill_arh_object_gtt;
-- 
-- ��������� ��������� ��������� �������� � ��������� � ��� ��������/������/������ �����
-- � �������� ��������� ������������ Global Temporary Table � �� ����������� COMMIT!
--  
PROCEDURE archive_one
(
  p_id          number,   -- ID ��������
  p_entity      varchar2, -- ��������� ��������: 'WT', 'CT', 'BOT'
  p_arh_dump_dt date default trunc(sysdate, 'HH') -- ���� - ����� �������, � ������� ������� ��������� ������ � �������� �������
) IS
  prcname      VARCHAR2(80 CHAR) := ' rf_pkg_arh.archive_one';
BEGIN
  --
  -- �������� ���������
  --
  if p_entity is null or p_entity not in ('WT', 'CT', 'BOT') or p_id is null or p_arh_dump_dt is null Then
    raise_application_error(-20001, '�������� �������� ���������'||prcname);
  end if;
  --
  -- ������������ ��������� �������� � ��������� � ��� ������� � GTT-�������
  --
  fill_arh_object_gtt(par_op_cat_cd   => p_entity,
                      par_trxn_seq_id => p_id,
                      par_arh_dump_dt => p_arh_dump_dt);
  --  
  -- ���������� - ��������������� ��������� �������
  -- (������ ��� ��� ���������, ������� ����������� � RF_ARH_OBJECT_GTT) 
  --
  FOR r in (SELECT l.*
              FROM mantas.rf_arh_tab_list l
                   join (select distinct entity_cd
                           from mantas.rf_arh_object_gtt) t on t.entity_cd = l.entity_cd) LOOP
    EXECUTE IMMEDIATE generate_archive_code(p_owner       => r.owner_nm,
                                            p_table_name  => r.table_nm,
                                            p_query_nb    => r.query_nb,
                                            p_single_mode => true);
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      raise_application_error(-20001, '������ ��� ������������� ��������� ��������. ��������� ��������: '||p_entity||'; ID ��������: '||to_char(p_id)||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||prcname);
    end if;          
END archive_one;
--
-- "���������" ������� ��������������� ������������ �������� ��� ��������� ����������� - �������� � ��� �������������� ��������/������/������ �����,
-- ��������� � ��� ����������� � ������ ����������� ����������
-- ��������� ���������� �� ������������ ������ dbms_parallel_execute
--
PROCEDURE enrich_sp
(
  par_partition_position   INTEGER, -- ���������� ����� �������������� �������� (dba_tab_partitions.partition_position)
  par_subpart_position     INTEGER  -- ���������� ����� �������������� ����������� (dba_tab_subpartitions.subpartition_position)
) IS
  var_partname          VARCHAR2(255);
  var_subpartname       VARCHAR2(255);
  var_sql               CLOB;

  var_start_time        DATE;
  var_end_time          DATE;
  
  prcname      VARCHAR2(80 CHAR) := ' rf_pkg_arh.enrich_sp';
BEGIN
  --
  -- ��������� ������������ �������� � �����������
  --
  var_partname := mantas.rf_pkg_parallel.get_part_name('MANTAS', 'RF_ARH_OBJECT', par_partition_position);
  var_subpartname := mantas.rf_pkg_parallel.get_subpart_name('MANTAS', 'RF_ARH_OBJECT', par_partition_position, par_subpart_position);

  if var_partname is null or var_subpartname is null Then
    raise_application_error(-20001, '����������� ������: �������� �������� ���������. ����� ��������: '||to_char(par_partition_position)||', ����� �����������: '||to_char(par_subpart_position)||prcname);
  end if;
  --
  -- � ����������� �� ��������� �������� (������������ �� ����� ��������), ���������� ������ ����������
  --
  if var_partname = 'P_WT' Then    

    var_sql := 
q'{INSERT INTO mantas.rf_arh_object(entity_id, entity_cd, arh_dump_dt) 
(SELECT /*+ LEADING(arh) USE_NL(wt wts ac1 ac2)*/ 
        distinct 
        decode(lev, 1, nullif(wt.rf_orig_cust_seq_id, -1),
                    2, nullif(wt.rf_benef_cust_seq_id, -1),
                    3, nullif(wt.rf_scnd_orig_cust_seq_id, -1),
                    4, nullif(wt.rf_scnd_benef_cust_seq_id, -1),
                    5, nullif(wt.rf_send_cust_seq_id, -1),
                    6, nullif(wt.rf_rcv_cust_seq_id, -1),
                    7, nullif(wt.rf_orig_acct_seq_id, -1),
                    8, nullif(wt.rf_benef_acct_seq_id, -1),
                    9, wts.scrty_seq_id,
                    10, nullif(ac1.rf_prmry_cust_seq_id, -1),
                    11, nullif(ac2.rf_prmry_cust_seq_id, -1)) as entity_id,
       decode(lev, 1, 'CUST',
                   2, 'CUST',
                   3, 'CUST',
                   4, 'CUST',
                   5, 'CUST',
                   6, 'CUST',
                   7, 'ACCT',
                   8, 'ACCT',
                   9, 'SCRTY',
                   10, 'CUST',
                   11, 'CUST') as entity_cd,
       arh.arh_dump_dt as arh_dump_dt
  FROM mantas.rf_arh_object #PART_SUBPART# arh
       join business.wire_trxn wt on wt.fo_trxn_seq_id = arh.entity_id
       left join business.rf_wire_trxn_scrty wts on wts.fo_trxn_seq_id = wt.fo_trxn_seq_id and
                                                    wts.scrty_seq_id is not null
       left join business.acct ac1 on ac1.acct_seq_id = wt.rf_orig_acct_seq_id and
                                      ac1.rf_prmry_cust_seq_id is not null and
                                      wt.rf_orig_acct_seq_id <> -1
       left join business.acct ac2 on ac2.acct_seq_id = wt.rf_benef_acct_seq_id and
                                      ac2.rf_prmry_cust_seq_id is not null and
                                      wt.rf_benef_acct_seq_id <> -1
       join (select level as lev
               from dual
             connect by level <= 11) on decode(lev, 1, nullif(wt.rf_orig_cust_seq_id, -1),
                                                    2, nullif(wt.rf_benef_cust_seq_id, -1),
                                                    3, nullif(wt.rf_scnd_orig_cust_seq_id, -1),
                                                    4, nullif(wt.rf_scnd_benef_cust_seq_id, -1),
                                                    5, nullif(wt.rf_send_cust_seq_id, -1),
                                                    6, nullif(wt.rf_rcv_cust_seq_id, -1),
                                                    7, nullif(wt.rf_orig_acct_seq_id, -1),
                                                    8, nullif(wt.rf_benef_acct_seq_id, -1),
                                                    9, wts.scrty_seq_id,
                                                    10, nullif(ac1.rf_prmry_cust_seq_id, -1),
                                                    11, nullif(ac2.rf_prmry_cust_seq_id, -1)) is not null)}';           
  elsif var_partname = 'P_CT' Then
    
    var_sql := 
q'{INSERT INTO mantas.rf_arh_object(entity_id, entity_cd, arh_dump_dt) 
(SELECT /*+ LEADING(arh) USE_NL(ct cts ac)*/ 
        distinct 
        decode(lev, 1, nullif(ct.rf_cust_seq_id, -1),
                    2, nullif(ct.rf_cndtr_cust_seq_id, -1),
                    3, nullif(ct.rf_acct_seq_id, -1),
                    4, cts.scrty_seq_id,
                    5, nullif(ac.rf_prmry_cust_seq_id, -1)) as entity_id,
        decode(lev, 1, 'CUST',
                    2, 'CUST',
                    3, 'ACCT',
                    4, 'SCRTY',
                    5, 'CUST') as entity_cd,
        arh.arh_dump_dt as arh_dump_dt
   FROM mantas.rf_arh_object #PART_SUBPART# arh
        join business.cash_trxn ct on ct.fo_trxn_seq_id = arh.entity_id
        left join business.rf_cash_trxn_scrty cts on cts.fo_trxn_seq_id = ct.fo_trxn_seq_id and
                                                     cts.scrty_seq_id is not null
        left join business.acct ac on ac.acct_seq_id = ct.rf_acct_seq_id and
                                      ac.rf_prmry_cust_seq_id is not null and
                                      ct.rf_acct_seq_id <> -1
        join (select level as lev
                from dual
              connect by level <= 5) on decode(lev, 1, nullif(ct.rf_cust_seq_id, -1),
                                                    2, nullif(ct.rf_cndtr_cust_seq_id, -1),
                                                    3, nullif(ct.rf_acct_seq_id, -1),
                                                    4, cts.scrty_seq_id,
                                                    5, nullif(ac.rf_prmry_cust_seq_id, -1)) is not null)}';
      
  elsif var_partname = 'P_BOT' Then
  
    var_sql := 
q'{INSERT INTO mantas.rf_arh_object(entity_id, entity_cd, arh_dump_dt) 
(SELECT /*+ LEADING(arh) USE_NL(bot ac)*/ 
        distinct 
        decode(lev, 1, nullif(bot.rf_cust_seq_id, -1),
                    2, nullif(bot.rf_acct_seq_id, -1),
                    3, nullif(ac.rf_prmry_cust_seq_id, -1)) as entity_id,
        decode(lev, 1, 'CUST', 
                    2, 'ACCT',
                    3, 'CUST') as entity_cd,
        arh.arh_dump_dt as arh_dump_dt
   FROM mantas.rf_arh_object #PART_SUBPART# arh
        join business.back_office_trxn bot on bot.bo_trxn_seq_id = arh.entity_id
        left join business.acct ac on ac.acct_seq_id = bot.rf_acct_seq_id and
                                      ac.rf_prmry_cust_seq_id is not null and
                                      bot.rf_acct_seq_id <> -1
        join (select level as lev
                from dual
              connect by level <= 3) on decode(lev, 1, nullif(bot.rf_cust_seq_id, -1),
                                                    2, nullif(bot.rf_acct_seq_id, -1),
                                                    3, nullif(ac.rf_prmry_cust_seq_id, -1)) is not null)}';

  else
    raise_application_error(-20001, '����������� ������. ����������� ������������ ��������: '||var_partname||'. ����� ��������: '||to_char(par_partition_position)||', ����� �����������: '||to_char(par_subpart_position)||prcname);
  end if;
  --
  -- ��������� ������, ������������ �������������� ��������/������/������ ����� � ������� RF_ARH_OBJECT
  --  
  var_sql := replace(var_sql, '#PART_SUBPART#', 'SUBPARTITION ('||var_subpartname||')');

  BEGIN
    var_start_time := SYSDATE;
    
    EXECUTE IMMEDIATE var_sql;
    COMMIT;

    var_end_time := SYSDATE;
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.error('������ ��� ���������� ������� ������ ��������������� �������� � ������� RF_ARH_OBJECT ��� ��������: '||var_partname||', �����������: '||var_subpartname||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. ����� ������� ��. � ��������� ������� ���������', prcname);
      -- ������� � ��� ������������� ������
      for i in 1..trunc(length(var_sql)/2000) + 1 loop
        std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);
      end loop;
  END;  

  -- ���� ���������� ������� ������ ����� ������ - ������� ��������� �� ���� � ���
  if (var_end_time - var_start_time)*24*60 > 1 Then
    std.pkg_log.info('���������� ������� ������ ��������������� �������� � ������� RF_ARH_OBJECT ������ ������������ �����: '||to_char((var_end_time - var_start_time)*24*3600)||' ���. ��� ��������: '||var_partname||', �����������: '||var_subpartname||
                     '. ����� ������� ��. � ��������� ������� ���������', prcname);
    -- ������� � ��� ������������� ������
    for i in 1..trunc(length(var_sql)/2000) + 1 loop
      std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);
    end loop;
  end if;
END enrich_sp;
--
-- "���������" ������� ��������������� ������������ �������� ��� ��������� ����������� - �������� � ��� �������������� ������ ����� ���������,
-- ��� ��������, ��� ���������� � ������ �����������
-- ��������� ���������� �� ������������ ������ dbms_parallel_execute
--
PROCEDURE enrich_cust_cust_sp
(
  par_partition_position   INTEGER, -- ���������� ����� �������������� �������� CUST (dba_tab_partitions.partition_position)
  par_subpart_position     INTEGER  -- ���������� ����� �������������� ����������� (dba_tab_subpartitions.subpartition_position)
) IS
  var_partname          VARCHAR2(255);
  var_subpartname       VARCHAR2(255);
  var_sql               CLOB;

  var_start_time        DATE;
  var_end_time          DATE;
  
  prcname      VARCHAR2(80 CHAR) := ' rf_pkg_arh.enrich_cust_cust_sp';
BEGIN
  --
  -- ��������� ������������ �������� � �����������
  --
  var_partname := mantas.rf_pkg_parallel.get_part_name('MANTAS', 'RF_ARH_OBJECT', par_partition_position);
  var_subpartname := mantas.rf_pkg_parallel.get_subpart_name('MANTAS', 'RF_ARH_OBJECT', par_partition_position, par_subpart_position);

  if var_partname is null or var_subpartname is null Then
    raise_application_error(-20001, '����������� ������: �������� �������� ���������. ����� ��������: '||to_char(par_partition_position)||', ����� �����������: '||to_char(par_subpart_position)||prcname);
  end if;
  
  if var_partname <> 'P_CUST' Then
    raise_application_error(-20001, '����������� ������: ��������� �������� �� ��� �������� P_CUST. ����� ��������: '||to_char(par_partition_position)||', ����� �����������: '||to_char(par_subpart_position)||prcname);
  end if;
  --
  -- ���������� ������ ����������
  --
  var_sql := 
q'{INSERT INTO mantas.rf_arh_object(entity_id, entity_cd, arh_dump_dt) 
(SELECT /*+ ORDERED USE_NL(cc) INDEX(cc)*/ 
        cc.cust_cust_seq_id as entity_id,
        'CUST_CUST' as entity_cd,
        t.arh_dump_dt as arh_dump_dt
   FROM (select distinct entity_id, arh_dump_dt from mantas.rf_arh_object #PART_SUBPART#) t
        join business.cust_cust cc on cc.rf_cust_seq_id = t.entity_id
 UNION
 SELECT /*+ ORDERED USE_NL(cc) INDEX(cc)*/ 
        cc.cust_cust_seq_id as entity_id,
        'CUST_CUST' as entity_cd,
        t.arh_dump_dt as arh_dump_dt
   FROM (select distinct entity_id, arh_dump_dt from mantas.rf_arh_object #PART_SUBPART#) t
        join business.cust_cust cc on cc.rf_rltd_cust_seq_id = t.entity_id)}';
  --
  -- ��������� ������, ������������ �������������� ������ ����� ��������� � ������� RF_ARH_OBJECT
  --  
  var_sql := replace(var_sql, '#PART_SUBPART#', 'SUBPARTITION ('||var_subpartname||')');

  BEGIN
    var_start_time := SYSDATE;
    
    EXECUTE IMMEDIATE var_sql;
    COMMIT;

    var_end_time := SYSDATE;
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.error('������ ��� ���������� ������� ������ ��������������� ������ ����� ��������� � ������� RF_ARH_OBJECT ��� ��������: '||var_partname||', �����������: '||var_subpartname||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. ����� ������� ��. � ��������� ������� ���������', prcname);
      -- ������� � ��� ������������� ������
      for i in 1..trunc(length(var_sql)/2000) + 1 loop
        std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);
      end loop;
  END;  

  -- ���� ���������� ������� ������ ����� ������ - ������� ��������� �� ���� � ���
  if (var_end_time - var_start_time)*24*60 > 1 Then
    std.pkg_log.info('���������� ������� ������ ��������������� ������ ����� ��������� � ������� RF_ARH_OBJECT ������ ������������ �����: '||to_char((var_end_time - var_start_time)*24*3600)||' ���. ��� ��������: '||var_partname||', �����������: '||var_subpartname||
                     '. ����� ������� ��. � ��������� ������� ���������', prcname);
    -- ������� � ��� ������������� ������
    for i in 1..trunc(length(var_sql)/2000) + 1 loop
      std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);
    end loop;
  end if;
END enrich_cust_cust_sp;
--
-- "���������" ������� ��������������� ������������ �������� - �������� � ��� �������������� ��������/������/������ �����,
-- ��������� � ��� ����������� � ������ ������� ����������
-- ��������� ���������� � �������� ��������� ������������� ��������, ����������� ������������� �������� dbms_parallel_execute
--
FUNCTION enrich_arh_object RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
  var_task_name VARCHAR2(64) := 'aml_arh_enrich';
  var_degree    INTEGER := 128;

  var_chunk_cursor CLOB :=
q'{SELECT t.num_1 as start_id, t.num_2 as end_id
  FROM TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab('MANTAS', 'RF_ARH_OBJECT', 'P_WT')) t
UNION ALL
SELECT t.num_1 as start_id, t.num_2 as end_id
  FROM TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab('MANTAS', 'RF_ARH_OBJECT', 'P_CT')) t
UNION ALL
SELECT t.num_1 as start_id, t.num_2 as end_id
  FROM TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab('MANTAS', 'RF_ARH_OBJECT', 'P_BOT')) t  
ORDER BY 1, 2}'; 
  var_chunk_sql CLOB := 'begin mantas.rf_pkg_arh.enrich_sp(:start_id, :end_id); end;'; 
  var_err_count INTEGER;  

  var_task_name2 VARCHAR2(64) := 'aml_arh_enrich_cust_cust';
  var_chunk_cursor2 CLOB :=
q'{SELECT t.num_1 as start_id, t.num_2 as end_id
  FROM TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab('MANTAS', 'RF_ARH_OBJECT', 'P_CUST')) t
ORDER BY 1, 2}'; 
  var_chunk_sql2 CLOB := 'begin mantas.rf_pkg_arh.enrich_cust_cust_sp(:start_id, :end_id); end;'; 

  var_err_count2 INTEGER;  
  
  prcname      VARCHAR2(80 CHAR) := ' rf_pkg_arh.enrich_arh_object';
BEGIN
  std.pkg_log.info('���������� � ������� RF_ARH_OBJECT �������������� ��������/������/������ �����/������ ����� ���������, ��������� � ����������� ���������� (������������� �������� dbms_parallel_execute)', prcname);
  
  -- ������� task, ���� �� ������� � ����������� �������
  begin dbms_parallel_execute.drop_task(var_task_name); exception when others then null; end;

  -- ������� task
  dbms_parallel_execute.create_task(var_task_name);

  -- ������� chunk'�
  dbms_parallel_execute.create_chunks_by_sql(var_task_name, var_chunk_cursor, false);

  -- ��������� task
  dbms_parallel_execute.run_task(var_task_name, var_chunk_sql, DBMS_SQL.NATIVE, parallel_level => var_degree);

  -- ���� ���� ������ - ���������� ��������� � ���
  var_err_count := std.pkg_log.log_parallel_task_errors('MANTAS', var_task_name, 'ERROR', prcname);

  -- ������� task
  dbms_parallel_execute.drop_task(var_task_name);

  std.pkg_log.info('��������� ���������� �������������� ��������/������/������ �����, �������� ���������� �������������� ������ ����� ���������', prcname);
  
  -- ������� task
  dbms_parallel_execute.create_task(var_task_name2);

  -- ������� chunk'�
  dbms_parallel_execute.create_chunks_by_sql(var_task_name2, var_chunk_cursor2, false);

  -- ��������� task
  dbms_parallel_execute.run_task(var_task_name2, var_chunk_sql2, DBMS_SQL.NATIVE, parallel_level => var_degree);

  -- ���� ���� ������ - ���������� ��������� � ���
  var_err_count2 := std.pkg_log.log_parallel_task_errors('MANTAS', var_task_name2, 'ERROR', prcname);

  -- ������� task
  dbms_parallel_execute.drop_task(var_task_name2);

  std.pkg_log.info('��������� ���������� � ������� RF_ARH_OBJECT �������������� ��������/������/������ �����/������ ����� ���������, ��������� � ����������� ���������� (������������� �������� dbms_parallel_execute)', prcname);
  
  -- �������
  if var_err_count > 0 or var_err_count2 > 0 Then
    return 1;
  else 
    return 0;
  end if;    
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ������ � ������� RF_ARH_OBJECT ��������������� ��������/������/������ �����/������ ����� ���������, ��������� � ����������� ����������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    return 1;
END enrich_arh_object;
--
-- ��������� ��������� ������ ������������� ��� ��������, �������������� ������� ��������� � ��������� ����������� ������� RF_ARH_OBJECT
-- ��������� ���������� �� ������������ ������ dbms_parallel_execute
--
PROCEDURE archive_sp
(
  par_query_id             mantas.rf_arh_queries.query_id%TYPE, -- ID ������� �������������, ������� ���� ���������
  par_subpart_position     INTEGER                              -- ���������� ����� �������������� ����������� (dba_tab_subpartitions.subpartition_position)
) IS

  var_partname          VARCHAR2(255);
  var_subpartname       VARCHAR2(255);
  var_sql               CLOB;
  
  var_start_time        DATE;
  var_end_time          DATE;
  
  prcname      VARCHAR2(80 CHAR) := ' rf_pkg_arh.archive_sp';
BEGIN
  --
  -- ������� ����� ������� � ������������ �������� � �����������
  --
  BEGIN
    SELECT q.arh_sql_tx, 
           q.partition_name,
           sp.subpartition_name
      INTO var_sql, var_partname, var_subpartname     
      FROM mantas.rf_arh_queries q
           left join dba_tab_subpartitions sp on sp.table_owner = 'MANTAS' and
                                                 sp.table_name = 'RF_ARH_OBJECT' and
                                                 sp.partition_name = q.partition_name and
                                                 sp.subpartition_position = par_subpart_position
     WHERE q.query_id = par_query_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN                                                 
      raise_application_error(-20001, '����������� ������: ������ �������� ID �������: '||to_char(par_query_id)||', ����� �����������: '||to_char(par_subpart_position)||prcname);
  END;
  
  if var_subpartname is null Then
    raise_application_error(-20001, '����������� ������: �� ������� ����� ��������� �����������. ID �������: '||to_char(par_query_id)||', ��������: '||var_partname||', ����� �����������: '||to_char(par_subpart_position)||prcname);
  end if;                                           

  var_sql := replace(var_sql, '#PART_SUBPART#', 'SUBPARTITION ('||var_subpartname||')');
  var_sql := replace(var_sql, '#PARTITION_NAME#', var_partname);
  var_sql := replace(var_sql, '#SUBPARTITION_NAME#', var_subpartname);
  --
  -- �������� ���������� ������ 
  --
  BEGIN
    var_start_time := SYSDATE;
    
    EXECUTE IMMEDIATE var_sql;
    COMMIT;

    var_end_time := SYSDATE;
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.error('������ ��� ���������� ������� ������������� �������� �� ������� RF_ARH_OBJECT ��� ��������: '||var_partname||', �����������: '||var_subpartname||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. ����� ������� ��. � ��������� ������� ���������', prcname);
      -- ������� � ��� ������������� ������
      for i in 1..trunc(length(var_sql)/2000) + 1 loop
        std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);
      end loop;
  END;  

  -- ���� ���������� ������� ������ ����� ������ - ������� ��������� �� ���� � ���
  if (var_end_time - var_start_time)*24*60 > 1 Then
    std.pkg_log.info('���������� ������� �'||to_char(par_query_id)||' ������ ��������������� �������� � ������� RF_ARH_OBJECT ������ ������������ �����: '||to_char((var_end_time - var_start_time)*24*3600)||' ���. ��� ��������: '||var_partname||', �����������: '||var_subpartname||
                     '. ����� ������� ��. � ��������� ������� ���������', prcname);
    -- ������� � ��� ������������� ������
    for i in 1..trunc(length(var_sql)/2000) + 1 loop
      std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);
    end loop;
  end if;
END archive_sp;
--
-- ��������� ��������� ��������, �������������� ������� �������� � ������� RF_ARH_OBJECT
-- ��������! ��������������, ��� "����������" (��. ���������� ���������) ��� ���������!
--
FUNCTION archive_all RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
  var_task_name VARCHAR2(64) := 'aml_arh';
  var_degree    INTEGER := 128;

  var_chunk_cursor CLOB :=
q'{SELECT q.query_id as start_id, t.num_2 as end_id
  FROM mantas.rf_arh_queries q
       join TABLE(mantas.rf_pkg_parallel.part_subpart_pos_tab('MANTAS', 'RF_ARH_OBJECT', q.partition_name)) t on 1=1
ORDER BY 1, 2}'; 

  var_chunk_sql CLOB := 'begin mantas.rf_pkg_arh.archive_sp(:start_id, :end_id); end;'; 
  var_err_count INTEGER;  

  prcname      VARCHAR2(80 CHAR) := ' rf_pkg_arh.archive_all';
BEGIN
  --
  -- ���������� � ������� RF_ARH_OBJECT �������������� ��������/������/������ �����, ��������� � ����������� ���������� (������������� �������� dbms_parallel_execute)
  --
  var_err_count := rf_pkg_arh.enrich_arh_object;
  
  -- ����� �� ������, ���� ������ ���������
  if var_err_count > 0 Then
    return 1;
  end if;
  --
  -- ��������� ������� ������������� ������������� �������� - ��������� ������ �� ������ ������ ������������� �
  -- ������ ��������������� ����������� ������� RF_ARH_OBJECT
  --
  -- ������� � ���������� ������ ������� ������������� ��� ���� ������������ ������  
  std.pkg_log.info('���������� ������� ������������� �������� � ��������� � ���� ��������/������/������ �����', prcname);
  
  DELETE FROM mantas.rf_arh_queries;
  INSERT INTO mantas.rf_arh_queries(query_id, partition_name, arh_sql_tx)
  SELECT row_number() over(order by l.owner_nm, l.table_nm, l.query_nb) as query_id,
         'P_'||l.entity_cd as partition_name,
         mantas.rf_pkg_arh.generate_archive_code(l.owner_nm, l.table_nm, l.query_nb)  
    FROM mantas.rf_arh_tab_list l;
  COMMIT;
    
  std.pkg_log.info('���������� ���������� �������� � ��������� � ���� ��������/������/������ ����� (������������� �������� dbms_parallel_execute)', prcname);

  -- ������� task, ���� �� ������� � ����������� �������
  begin dbms_parallel_execute.drop_task(var_task_name); exception when others then null; end;

  -- ������� task
  dbms_parallel_execute.create_task(var_task_name);

  -- ������� chunk'�
  dbms_parallel_execute.create_chunks_by_sql(var_task_name, var_chunk_cursor, false);

  -- ��������� task
  dbms_parallel_execute.run_task(var_task_name, var_chunk_sql, DBMS_SQL.NATIVE, parallel_level => var_degree);

  -- ���� ���� ������ - ���������� ��������� � ���
  var_err_count := std.pkg_log.log_parallel_task_errors('MANTAS', var_task_name, 'ERROR', prcname);

  -- ������� task
  dbms_parallel_execute.drop_task(var_task_name);
  
  std.pkg_log.info('��������� ������������ ���������� �������� � ��������� � ���� ��������/������/������ ����� (������������� �������� dbms_parallel_execute)', prcname);
  
  -- �������
  if var_err_count > 0 Then
    return 1;
  else 
    return 0;
  end if;    
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ������������� ���������� ��������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    return 1;
END archive_all;
end rf_pkg_arh;
/
