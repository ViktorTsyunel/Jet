CREATE OR REPLACE 
PACKAGE MANTAS.RF_PKG_KGRKO IS
--
-- ����� �� ���������������� ����������� � ��������� ��� ��������� �������� ����� � ������������� ��������
-- 
-------------------------------------------------------------------------------------------
--
-- �������� ID ������� ����� (RF_OES_321_ORG.OES_321_ORG_ID) �� ��������� ��, ���,  �� ��������� ����
--
FUNCTION get_oes_321_org_id 
(
  p_tb_id   in mantas.rf_oes_321_org.tb_code%type,
  p_osb_id  in mantas.rf_oes_321_org.osb_code%type default null,
  p_trxn_dt in mantas.rf_oes_321_org.end_dt%type default null
) 
RETURN mantas.rf_oes_321_org.oes_321_org_id%type PARALLEL_ENABLE RESULT_CACHE;
--
-- �������� ID ������� ����� - ��������� �������� (RF_OES_321_PARTY_ORG.OES_321_P_ORG_ID) 
-- �� ��������� ��, ���,  �� ��������� ����
--
FUNCTION get_oes_321_party_org_id 
(
  p_tb_id          in mantas.rf_oes_321_party_org.tb_code%type,
  p_osb_id         in mantas.rf_oes_321_party_org.osb_code%type default null,
  p_trxn_dt        in mantas.rf_oes_321_party_org.end_dt%type default null
) 
RETURN mantas.rf_oes_321_party_org.oes_321_p_org_id%type PARALLEL_ENABLE RESULT_CACHE;
--
-- ����������: ��������� �� ��������� ��-���/ID �������� � ������ �������� ����� (��������������, �������� �������� �������������)
--
FUNCTION is_inter_kgrko
(
  par_branch1_id     INTEGER,  -- ������ ��� ��-��� ����: ��� �� * 100000 + ��� ���
  par_branch2_id     INTEGER,  -- ������ ��� ��-���
  par_date           DATE,     -- ����, �� ������� ������� ���������� �������������� ��-��� � �������� ����� (���� ��������)
  par_override_flag  INTEGER DEFAULT 0, -- ���� 1/0, ��� ��������� ���� ��/��� �������������� � ��������� ���������� �������
  par_oes_org1_id    mantas.rf_oes_321_org.oes_321_org_id%TYPE DEFAULT null, -- ID ������� ������� �����
  par_oes_org2_id    mantas.rf_oes_321_org.oes_321_org_id%TYPE DEFAULT null  -- ID ������� ������� �����
) 
RETURN INTEGER RESULT_CACHE PARALLEL_ENABLE;   -- 1 - ��������� ��-���/ID �������� ��������� � ������ �������� �����, 
                                               -- 0 - � ������ ������� (� �. �. ���� ��-���/ID �������� �� �������)
--
-- ���������� �� ��������� ��������: �������� �� ��� �������������
--
FUNCTION is_inter_kgrko
(
  par_op_cat_cd              VARCHAR2, -- WT/CT/BOT - ��������� �������� - �����������/��������/����������
  par_trxn_seq_id            business.wire_trxn.fo_trxn_seq_id%TYPE,       -- ID ��������
  par_orig_acct_seq_id       business.wire_trxn.rf_orig_acct_seq_id%TYPE,  -- ID ����� ����������� (�� ����������� ������)
  par_orig_acct_nb           business.wire_trxn.rf_orig_acct_nb%TYPE,      -- ����� ����� ����������� (������, 20 ����)
  par_send_instn_id          business.wire_trxn.send_instn_id%TYPE,        -- ���/SWIFT ����� �����������, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
  par_orig_cust_seq_id       business.wire_trxn.rf_orig_cust_seq_id%TYPE,  -- ID ������� ����������� - ��������� �����
  par_benef_acct_seq_id      business.wire_trxn.rf_benef_acct_seq_id%TYPE, -- ID ����� ����������� (�� ����������� ������)
  par_benef_acct_nb          business.wire_trxn.rf_benef_acct_nb%TYPE,     -- ����� ����� ����������� (������, 20 ����)
  par_rcv_instn_id           business.wire_trxn.rcv_instn_id%TYPE,         -- ���/SWIFT ����� �����������, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
  par_benef_cust_seq_id      business.wire_trxn.rf_benef_cust_seq_id%TYPE, -- ID ������� ����������� - ��������� �����
  par_trxn_dt                business.wire_trxn.trxn_exctn_dt%TYPE,        -- ���� ��������
  par_src_sys_cd             business.wire_trxn.src_sys_cd%TYPE            -- ��� �������-��������� �������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
)
RETURN INTEGER PARALLEL_ENABLE;   -- 1 - �������� �������������, 0 - ���, null - �������� �������� ���������
--
-- ���������� �� ���������� ���: �������� �� ��������� �������� ������������� 
-- ��� ������� ����� ����� �������� ������ ��� ��� �� ����������� ��������� 
-- (� �������� ������ ������� ���������� ������ ��� � � ��� ��� ���������� � ������� �����)
--
FUNCTION is_inter_kgrko
(
  par_oes_321_id    mantas.rf_oes_321.oes_321_id%TYPE  -- ������������� ���
)  
RETURN INTEGER PARALLEL_ENABLE;   -- 1 - ��������� �������� �������� �������������, 
                                  -- 0 - �������, 
                                  -- null - �� ������� ����������
--
-- �������������� ��� ��-��� (������������ �� �����) � ������������ � ID ������� ����� (���������� �������������)
--
FUNCTION override_branch_id
(
  par_branch_id          INTEGER,                                   -- ��������� ��� ��-���, ����: ��� �� * 100000 + ��� ���
  par_oes_org_id         mantas.rf_oes_321_org.oes_321_org_id%TYPE, -- ID ������� �����, ���������������� � ��������, � �. �. null ��� ������� �����������
  par_date               DATE                                       -- ���� �������� (����, �� ������� ������� ���������� ����� ������� ����� �� ���� ��-���)
) 
RETURN INTEGER RESULT_CACHE PARALLEL_ENABLE;   -- ���������������� ��� ��-��� ����: ��� �� * 100000 + ��� ���
--
-- �������� ��� ��������� ��������/������ ������ ��� ������������ ������ �� ��������������� �������/�������� (����������/����������)
-- ���� ���������� ������ ���, �� � ��������������� ��������� ��������� ��������� null
--
PROCEDURE get_existing_alerts
(
  par_inter_kgrko_cd     INTEGER,  -- ������� �� ����� ������� "�������������" �������� ����� �����:
                                   -- 1 - ����������, 2 - ����������, 3 - ���, 0 - ����� ����� ��� ������� ��������
  par_op_cat_cd          VARCHAR2, -- ������������� ��������/������ ������: ��������� (WT, CT, BOT)
  par_trxn_seq_id        INTEGER,  -- ������������� ��������: ID ��������
  par_trxn_scrty_seq_id  INTEGER,  -- ������������� ������ ������: ID ������ ������
  par_review1_id     OUT mantas.kdd_review.review_id%TYPE, -- ��������� - ID ������ "�� �����������", � ������ par_inter_kgrko_cd = 0 ��� ID ������ ��� ������� ��������                                 
  par_review2_id     OUT mantas.kdd_review.review_id%TYPE  -- ��������� - ID ������ "�� ����������"
);

--
-- ������� �������� ������� ������+��� �� ��������������� �������� (COMMIT �� ��������)
--
FUNCTION create_second_oes
(
  p_first_review_id   IN   mantas.kdd_review.review_id%TYPE       -- ������ ����� �� ��������
 ,p_opok_nb           IN   mantas.kdd_review.rf_opok_nb%TYPE      -- ��� ���� ������������ ������
 ,p_add_opoks_tx      IN   mantas.kdd_review.rf_add_opoks_tx%TYPE -- �������������� ���� ���� ������������ ������
 ,p_due_dt            IN   mantas.kdd_review.due_dt%TYPE          -- ���� ������������ ������      
 ,p_owner_id          IN   mantas.kdd_review.owner_seq_id%TYPE    -- ������������ ������������ ������      
 ,p_user_id           IN   mantas.kdd_review.owner_seq_id%TYPE    -- ��� ������ �����      
 ,p_cmmnt_id          IN   mantas.kdd_cmmnt.cmmnt_id%TYPE         -- ���������� �����������
 ,p_note_tx           IN   mantas.kdd_note_hist.note_tx%TYPE      -- ������������ �����������
 ,p_message           OUT  VARCHAR2                               -- �������� ��������: ��������� (������, �����, ���������� ��������)
 ,p_second_review_id  OUT  mantas.kdd_review.review_id%TYPE       -- �������� ��������: id ���������� ������
 ,p_second_oes_321_id OUT  mantas.rf_oes_321.oes_321_id%TYPE       -- �������� ��������: id ���������� ���
)
RETURN BOOLEAN; -- ������ ��������� ������� ��� ���

END RF_PKG_KGRKO;
/

CREATE OR REPLACE 
PACKAGE BODY MANTAS.RF_PKG_KGRKO IS
---------------------------------------------------------------------------------------------------------
--
-- ��������������� �������
--
-----------------------------------------------------------------------------------------------------------
--
-- ����������: �������� �� ��������� �������� ������������� (����������� ������ � ��� �� ��������)
-- ���������� �������, ����������, ���������� ������ �����������
--
FUNCTION is_inter_kgrko_internal
(
   par_op_cat_cd        VARCHAR2, -- ������������� ��������: ��������� (WT, CT, BOT)
   par_trxn_seq_id      INTEGER,  -- ������������� ��������: ID ��������
   par_kgrko_flag       BOOLEAN,  -- ����, ��� � ��������� ���������� ������� ���������� �� ������ �������� ������ �������� �����
   par_send_kgrko_id    mantas.rf_oes_321_org.numbf_s%TYPE, -- ����� ������� ����� ����������� (�������, ���� �������� ��)
   par_rcv_kgrko_id     mantas.rf_oes_321_org.numbf_s%TYPE  -- ����� ������� ����� ���������� (�������, ���� �������� ��)
)
RETURN INTEGER PARALLEL_ENABLE IS   -- 1 - ��������� �������� �������� �������������, 
                                    -- 0 - �������
                                    -- null - �������� �������� ���������
  var_result    INTEGER;
BEGIN
  --
  -- ���� ������� �������� ��������� - ������� null
  --
  if par_op_cat_cd is null or par_op_cat_cd not in ('WT', 'CT', 'BOT') or par_trxn_seq_id is null Then
    return null;
  end if;
  --
  -- ���������� ���������� ��������������� �������� �� ������������ ��� ��������� �������� ���:
  --  - ���� ���� ����� ��� �����, ��� �� ������ ������, �� �������� �������������
  --  - ��� ����������� �������� - � ������������ � ���������� ���������� ���
  --
  FOR r IN (SELECT case when count(distinct rv.review_id) over() > 1
                        then 1
                        when par_op_cat_cd = 'WT'
                        then rf_pkg_kgrko.is_inter_kgrko(oes.oes_321_id) 
                   end as is_inter_kgrko
              FROM mantas.kdd_review rv
                   join mantas.rf_oes_321 oes on oes.review_id = rv.review_id and
                                                 nvl(oes.deleted_fl, 'N') <> 'Y'
             WHERE rv.rf_op_cat_cd = par_op_cat_cd and
                   rv.rf_trxn_seq_id = par_trxn_seq_id and
                   rv.rf_alert_type_cd = 'O' and
                   nvl(rv.rf_object_tp_cd, '?') <> 'TRXN_SCRTY' and       -- �� ��������� ������ �� ��������� ������ �������
--!!! � ���� ���� ������ ������ �� ��������� ������ ������� - ������, 4001 ???!!!                   
                   (rv.status_cd in ('RF_READY', 'RF_READY+') or          -- ��� ������� �������� (�������������� �������� ��� ������ ������)
                    (oes.send_fl = 'Y' and rv.status_cd <> 'RF_CANCEL'))  -- ��� ���������� � ��� �� ������ �� ��������
            ORDER BY decode(oes.current_fl, 'Y', 1, 2), oes.oes_321_id desc -- ������� ������� ���, ����� ��� ���������
           ) LOOP
    var_result := r.is_inter_kgrko;
    EXIT;
  END LOOP;         

  -- ���� ���� ��������������� ��������� - �������
  if var_result is not null Then
    return var_result;
  end if;       
  --
  -- ���������: ���� �� �� �������� ������, � ����� "�������������" ������ (� ���������� rf_inter_kgrko_cd) � ���������� "�������������" ������
  -- �������� ��������� �������������, ���� ���� "�������������" ������ � ����� ��� ��� ����������
  -- �������� ��������� �������, ���� ���� ���������� "�������������" ������ ��� ���� ������� ������ (�� �����: ���������� ��� ���)
  -- ���� ������� ��� - ���������� ������
  --
/*  SELECT case when count(*) > 0 
              then case when sum(decode(rf_inter_kgrko_cd, null, 0, 1)) > 0
                        then case when sum(case when rf_inter_kgrko_cd is not null and
                                                     status_cd in ('RF_CANCEL', 'RF_CANCEL+', 'RF_CREADY+')    -- �������� ��������/����� �������� ����� �������� "������� �� ��������"
                                                then 1
                                                else 0
                                           end) > 0            
                                  then 0 -- ���� "�������������" ������, ����� ��� ���� ���������� - ������� �������� �������
                                  else 1 -- ���� "�������������" ������, ����� ��� ��� ���������� - ������� �������� �������������
                             end
                        else 0 -- ���� ������, �� ��� "�������������" - ������� �������� ������� 
                   end                   
         end              
    INTO var_result
    FROM mantas.kdd_review
   WHERE rf_op_cat_cd = par_op_cat_cd and
         rf_trxn_seq_id = par_trxn_seq_id and
         rf_alert_type_cd = 'O' and
         nvl(rf_object_tp_cd, '?') <> 'TRXN_SCRTY';  -- ��������� ������ ������ �� "�����" ��������� (�� ��������� ������ �� ��������� ������ �������)
  
  -- ���� ���� ��������������� ��������� - �������
  if var_result is not null Then
    return var_result;
  end if; */      
  
  -- ���������� ��������������� �������� �� ��������� ������� �������� ����� 
  if par_kgrko_flag Then     
    return case when par_send_kgrko_id <> par_rcv_kgrko_id then 1 else 0 end;                                                 
  end if;
  
  -- ���� ���� ��-���, ���� �� ������� - ���������� �� ��������
  /*if par_op_cat_cd = 'WT' Then
    
    SELECT max(rf_pkg_kgrko.is_inter_kgrko(rf_orig_branch_id,  rf_benef_branch_id, trxn_exctn_dt))
      INTO var_result
      FROM business.wire_trxn
     WHERE fo_trxn_seq_id = par_trxn_seq_id; 

  elsif par_op_cat_cd = 'CT' Then

    SELECT max(rf_pkg_kgrko.is_inter_kgrko(rf_acct_branch_id,  rf_cash_branch_id, trxn_exctn_dt))
      INTO var_result
      FROM business.cash_trxn
     WHERE fo_trxn_seq_id = par_trxn_seq_id; 

  elsif par_op_cat_cd = 'BOT' Then    

    var_result := 0; -- ���������� �������� �� ������ ��������������

  end if;*/
  
  return var_result;            
END is_inter_kgrko_internal;                                    
---------------------------------------------------------------------------------------------------------
--
-- �������� ID ������� ����� (RF_OES_321_ORG.OES_321_ORG_ID) �� ��������� ��, ���,  �� ��������� ����
--
FUNCTION get_oes_321_org_id 
(
  p_tb_id   in mantas.rf_oes_321_org.tb_code%type,
  p_osb_id  in mantas.rf_oes_321_org.osb_code%type default null,
  p_trxn_dt in mantas.rf_oes_321_org.end_dt%type default null
) 
RETURN mantas.rf_oes_321_org.oes_321_org_id%type PARALLEL_ENABLE RESULT_CACHE IS
  v_id mantas.rf_oes_321_org.oes_321_org_id%type := null;
begin
  /* ���� ������ ��������� �� � ��� */
  if p_osb_id <> 0 then
    /* ������� MAX ����� ����� �������� ������ NO_DATA_FOUND */
    select max(oes_321_org_id) into v_id
      from (select oes_321_org_id,
                   row_number() over (partition by tb_code order by start_dt desc, oes_321_org_id desc) rn
              from mantas.rf_oes_321_org
             where tb_code = p_tb_id
               and osb_code = p_osb_id
               and trunc(nvl(p_trxn_dt, sysdate)) >= trunc(nvl(start_dt, to_date('01.01.1900', 'DD.MM.YYYY')))
               and trunc(nvl(p_trxn_dt, sysdate)) < trunc(nvl(end_dt, to_date('01.01.2099', 'DD.MM.YYYY'))) + 1
               and active_fl = 'Y'
           )
     where rn = 1;
  end if;
  
  /* ���� �� ������� ���������� oes_321_org_id �� ����� �� � ��� ��� ��� �� ��� �����, �� ���������� ������ �� ���� �� */
  if v_id is null then
    select max(oes_321_org_id) into v_id
      from (select oes_321_org_id,
                   row_number() over (partition by tb_code order by start_dt desc, oes_321_org_id desc) rn
              from mantas.rf_oes_321_org
             where tb_code = p_tb_id
               and osb_code is null
               and trunc(nvl(p_trxn_dt, sysdate)) >= trunc(nvl(start_dt, to_date('01.01.1900', 'DD.MM.YYYY')))
               and trunc(nvl(p_trxn_dt, sysdate)) < trunc(nvl(end_dt, to_date('01.01.2099', 'DD.MM.YYYY'))) + 1
               and active_fl = 'Y'
           )
     where rn = 1;
  end if;

  return v_id;
end get_oes_321_org_id;
--
-- �������� ID ������� ����� - ��������� �������� (RF_OES_321_PARTY_ORG.OES_321_P_ORG_ID) 
-- �� ��������� ��, ���,  �� ��������� ����
--
FUNCTION get_oes_321_party_org_id 
(
  p_tb_id          in mantas.rf_oes_321_party_org.tb_code%type,
  p_osb_id         in mantas.rf_oes_321_party_org.osb_code%type default null,
  p_trxn_dt        in mantas.rf_oes_321_party_org.end_dt%type default null
) 
RETURN mantas.rf_oes_321_party_org.oes_321_p_org_id%type PARALLEL_ENABLE RESULT_CACHE IS
  v_id        mantas.rf_oes_321_party_org.oes_321_p_org_id%type := null;
BEGIN
  /* ���� ������ ��������� �� � ��� */
  if p_osb_id <> 0 then
    /* ������� MAX ����� ����� �������� ������ NO_DATA_FOUND */
    select max(oes_321_p_org_id) 
      into v_id
      from (select oes_321_p_org_id,
                   row_number() over (order by start_dt desc, oes_321_p_org_id desc) rn
              from mantas.rf_oes_321_party_org
             where tb_code = p_tb_id
               and osb_code = p_osb_id
               and trunc(nvl(p_trxn_dt, sysdate)) >= trunc(nvl(start_dt, to_date('01.01.0001', 'DD.MM.YYYY')))
               and trunc(nvl(p_trxn_dt, sysdate)) <  trunc(nvl(end_dt,   to_date('01.01.9999', 'DD.MM.YYYY'))) + 1
               and active_fl = 'Y')
     where rn = 1;
  end if;
  
  /* ���� �� ������� ���������� oes_321_org_id �� ����� �� � ��� ��� ��� �� ��� �����, �� ���������� ������ �� ���� �� */
  if v_id is null then
    select max(oes_321_p_org_id) into v_id
      from (select oes_321_p_org_id,
                   row_number() over (order by start_dt desc, oes_321_p_org_id desc) rn
              from mantas.rf_oes_321_party_org
             where tb_code = p_tb_id
               and osb_code is null
               and trunc(nvl(p_trxn_dt, sysdate)) >= trunc(nvl(start_dt, to_date('01.01.0001', 'DD.MM.YYYY')))
               and trunc(nvl(p_trxn_dt, sysdate)) <  trunc(nvl(end_dt,   to_date('01.01.9999', 'DD.MM.YYYY'))) + 1
               and active_fl = 'Y'
           )
     where rn = 1;
  end if;

  return v_id;
END get_oes_321_party_org_id;
--
-- ����������: ��������� �� ��������� ��-���/ID �������� � ������ �������� ����� (��������������, �������� �������� �������������)
--
FUNCTION is_inter_kgrko
(
  par_branch1_id     INTEGER,  -- ������ ��� ��-��� ����: ��� �� * 100000 + ��� ���
  par_branch2_id     INTEGER,  -- ������ ��� ��-���
  par_date           DATE,     -- ����, �� ������� ������� ���������� �������������� ��-��� � �������� ����� (���� ��������)
  par_override_flag  INTEGER DEFAULT 0, -- ���� 1/0, ��� ��������� ���� ��/��� �������������� � ��������� ���������� �������
  par_oes_org1_id    mantas.rf_oes_321_org.oes_321_org_id%TYPE DEFAULT null, -- ID ������� ������� �����
  par_oes_org2_id    mantas.rf_oes_321_org.oes_321_org_id%TYPE DEFAULT null  -- ID ������� ������� �����
) 
RETURN INTEGER RESULT_CACHE PARALLEL_ENABLE IS -- 1 - ��������� ��-���/ID �������� ��������� � ������ �������� �����, 
                                               -- 0 - � ������ ������� (� �. �. ���� ��-���/ID �������� �� �������)
  var_result    INTEGER;
BEGIN
  --
  -- � ������� ������: ���� �� ������ �����-���� ��-��� ��� ������� ���������� - �������� �� �������������
  --
  if par_override_flag <> 1 and (par_branch1_id is null or par_branch2_id is null or par_branch1_id = par_branch2_id) Then
    return 0;
  end if;
  --
  -- � ������ ���������������: ���� �� ������ �����-���� ORG_ID ��� ������� ���������� - �������� �� �������������
  --
  if par_override_flag = 1 and (par_oes_org1_id is null or par_oes_org2_id is null or par_oes_org1_id = par_oes_org2_id) Then
    return 0;
  end if;
  --
  -- ��������� ����� ������� �� ����� ��� ������� �� ��������� ��-��� �� ����������� "��, ��������������� ��������"
  --
  SELECT case when decode(org1.branch_fl, '0', nullif(org1.numbf_s, '0'), '1', nullif(org1.numbf_ss, '0')) <> 
                   decode(org2.branch_fl, '0', nullif(org2.numbf_s, '0'), '1', nullif(org2.numbf_ss, '0'))
              then 1
              else 0
         end              
    INTO var_result
    FROM dual
         left join rf_oes_321_org org1 on org1.oes_321_org_id = 
                                          case when par_override_flag = 1 
                                               then par_oes_org1_id
                                               else rf_pkg_kgrko.get_oes_321_org_id(trunc(par_branch1_id/100000), par_branch1_id - round(par_branch1_id, -5), par_date) 
                                          end
         left join rf_oes_321_org org2 on org2.oes_321_org_id = 
                                          case when par_override_flag = 1 
                                               then par_oes_org2_id
                                               else rf_pkg_kgrko.get_oes_321_org_id(trunc(par_branch2_id/100000), par_branch2_id - round(par_branch2_id, -5), par_date)
                                          end;
                                                 
  return var_result;                                                 
END is_inter_kgrko;
--
-- ���������� �� ��������� ��������: �������� �� ��� �������������
--
FUNCTION is_inter_kgrko
(
  par_op_cat_cd              VARCHAR2, -- WT/CT/BOT - ��������� �������� - �����������/��������/����������
  par_trxn_seq_id            business.wire_trxn.fo_trxn_seq_id%TYPE,       -- ID ��������
  par_orig_acct_seq_id       business.wire_trxn.rf_orig_acct_seq_id%TYPE,  -- ID ����� ����������� (�� ����������� ������)
  par_orig_acct_nb           business.wire_trxn.rf_orig_acct_nb%TYPE,      -- ����� ����� ����������� (������, 20 ����)
  par_send_instn_id          business.wire_trxn.send_instn_id%TYPE,        -- ���/SWIFT ����� �����������, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
  par_orig_cust_seq_id       business.wire_trxn.rf_orig_cust_seq_id%TYPE,  -- ID ������� ����������� - ��������� �����
  par_benef_acct_seq_id      business.wire_trxn.rf_benef_acct_seq_id%TYPE, -- ID ����� ����������� (�� ����������� ������)
  par_benef_acct_nb          business.wire_trxn.rf_benef_acct_nb%TYPE,     -- ����� ����� ����������� (������, 20 ����)
  par_rcv_instn_id           business.wire_trxn.rcv_instn_id%TYPE,         -- ���/SWIFT ����� �����������, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
  par_benef_cust_seq_id      business.wire_trxn.rf_benef_cust_seq_id%TYPE, -- ID ������� ����������� - ��������� �����
  par_trxn_dt                business.wire_trxn.trxn_exctn_dt%TYPE,        -- ���� ��������
  par_src_sys_cd             business.wire_trxn.src_sys_cd%TYPE            -- ��� �������-��������� �������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
)
RETURN INTEGER PARALLEL_ENABLE IS   -- 1 - �������� �������������, 0 - ���, null - �������� �������� ���������
  var_result    INTEGER;
BEGIN
  --
  -- ��������� ���������
  --
  if par_op_cat_cd is null or par_trxn_seq_id is null or par_op_cat_cd not in ('WT', 'CT', 'BOT') Then 
    return null;
  end if;
  --
  -- ���������� ������� ��������������� �� ��������� ��������� ��������, � ������ ���������� ���������������
  -- �������� �����������/���������� �������������
  --
  SELECT case -- ���� ������������ ���� ������ ������� ��� �������� - ���������� ��������������� �� ���
              when trxe.rowid is not null 
              then rf_pkg_kgrko.is_inter_kgrko(null, null, null, 1, trxe.send_oes_org_id, trxe.rcv_oes_org_id) 
              -- ��������/���������� �������� �� ������ �������������� (�� ������� ����, ����)
              when par_op_cat_cd in ('CT', 'BOT')
              then 0  
              -- �����������: ���� ���� �� ������ (����������/����������) �� ������� - �������� �������
              when par_orig_acct_seq_id is null and par_orig_acct_nb is null 
              then 0    
              when par_benef_acct_seq_id is null and par_benef_acct_nb is null 
              then 0    
              else rf_pkg_kgrko.is_inter_kgrko(par_branch1_id => business.rf_pkg_util.get_acct_branch_id(par_orig_acct_seq_id, par_orig_acct_nb, par_send_instn_id,
                                                                                                         par_orig_cust_seq_id, trunc(par_trxn_dt), par_src_sys_cd), 
                                               par_branch2_id => business.rf_pkg_util.get_acct_branch_id(par_benef_acct_seq_id, par_benef_acct_nb, par_rcv_instn_id,
                                                                                                         par_benef_cust_seq_id, trunc(par_trxn_dt), par_src_sys_cd), 
                                               par_date => trunc(par_trxn_dt))
         end       
    INTO var_result
    FROM dual
         left join business.rf_trxn_extra trxe on trxe.op_cat_cd = par_op_cat_cd and
                                                  trxe.fo_trxn_seq_id = par_trxn_seq_id;
                                                  
  return var_result;                                                 
END is_inter_kgrko;
--
-- ���������� �� ���������� ���: �������� �� ��������� �������� ������������� 
-- ��� ������� ����� ����� �������� ������ ��� ��� �� ����������� ��������� 
-- (� �������� ������ ������� ���������� ������ ��� � � ��� ��� ���������� � ������� �����)
--
FUNCTION is_inter_kgrko
(
  par_oes_321_id    mantas.rf_oes_321.oes_321_id%TYPE  -- ������������� ���
)  
RETURN INTEGER PARALLEL_ENABLE IS -- 1 - ��������� �������� �������� �������������, 
                                  -- 0 - �������, 
                                  -- null - �� ������� ����������
  var_result    INTEGER;
BEGIN
  --
  -- ��� ��� �� ����������� ��������� (� �������� ������ ������� ���������� ������ ��� � � ��� ��� ���������� � ������� �����):
  -- - ���� � ���� ������ ������ �������: B_PAYER = 1 � B_RECEIP = 1, �� �������� ������� = 0
  -- - ���� ����� ������� ������ ������� (B_PAYER/B_RECEIP = 1), 
  --   � � ������ - ������ ������� ������� (B_RECEIP/B_PAYER = 0, BIK_B = �� ��), �� �������� ������������� = 1
  -- - ���� ����� ������� ������ ������� (B_PAYER/B_RECEIP = 1), 
  --   � � ������ - ������� (B_RECEIP/B_PAYER = 0, BIK_B <> 0 � BIK_B <> �� ��), �� ������� = 0
  -- - ����� - ���������� = null
  --
  SELECT max(case when oes.b_payer = '1' and oes.b_recip = '1'
                  then 0
                  when oes.b_payer = '1' and oes.b_recip = '0' and p3.bik_b <> '0' 
                  then mantas.rf_pkg_rule.is_sbrf(p3.bik_b)
                  when oes.b_payer = '0' and oes.b_recip = '1' and p0.bik_b <> '0' 
                  then mantas.rf_pkg_rule.is_sbrf(p0.bik_b)
             end)       
    INTO var_result
    FROM mantas.rf_oes_321 oes
         left join mantas.rf_oes_321_party p0 on p0.oes_321_id = oes.oes_321_id and p0.block_nb = 0
         left join mantas.rf_oes_321_party p3 on p3.oes_321_id = oes.oes_321_id and p3.block_nb = 3
   WHERE oes.oes_321_id = par_oes_321_id; 
  
  return var_result;
END is_inter_kgrko;
--
-- �������������� ��� ��-��� (������������ �� �����) � ������������ � ID ������� ����� (���������� �������������)
--
FUNCTION override_branch_id
(
  par_branch_id          INTEGER,                                   -- ��������� ��� ��-���, ����: ��� �� * 100000 + ��� ���
  par_oes_org_id         mantas.rf_oes_321_org.oes_321_org_id%TYPE, -- ID ������� �����, ���������������� � ��������, � �. �. null ��� ������� �����������
  par_date               DATE                                       -- ���� �������� (����, �� ������� ������� ���������� ����� ������� ����� �� ���� ��-���)
) 
RETURN INTEGER RESULT_CACHE PARALLEL_ENABLE IS   -- ���������������� ��� ��-��� ����: ��� �� * 100000 + ��� ���
  var_branch_id   INTEGER;
BEGIN
  --
  -- ���� ������ ������ ID ������� (������������ ������, ��� ������� �������� �� ������� � ����� ������) - ���������� null
  --
  if par_oes_org_id is null Then
    return null;
  end if;
  --
  -- ���� �� ������ ���������������� ��� ��-��� - �������� ���� ��� �� ID �������
  --  
  if par_branch_id is null Then
    SELECT max(tb_code*100000 + nvl(osb_code, 0))
      INTO var_branch_id
      FROM mantas.rf_oes_321_org
     WHERE oes_321_org_id = par_oes_org_id; 
  
    return var_branch_id;
  end if;
  --
  -- ���������� ��� �� ��� ��-���, ���� �� ������������� ������ ������� �����, ������������� �� ���������� ID �������,
  -- ����� - ���������� ��� ��-���, ������������ �� ID �������
  --  
  SELECT case -- ���� ������ �������� ���������� (��� ��-��� ������������� ����������������� ID �������)
              when org1.oes_321_org_id is not null and
                   org2.oes_321_org_id is not null and                   
                   nvl(decode(org1.branch_fl, '0', nullif(org1.numbf_s, '0'), '1', nullif(org1.numbf_ss, '0')), '-') = 
                   nvl(decode(org2.branch_fl, '0', nullif(org2.numbf_s, '0'), '1', nullif(org2.numbf_ss, '0')), '-')
              then par_branch_id
              -- ���� ������ �������������� ID �������
              when org2.oes_321_org_id is null  
              then par_branch_id
              else org2.tb_code*100000 + nvl(org2.osb_code, 0)
         end
    INTO var_branch_id
    FROM dual
         left join rf_oes_321_org org1 on org1.oes_321_org_id = rf_pkg_kgrko.get_oes_321_org_id(trunc(par_branch_id/100000), par_branch_id - round(par_branch_id, -5), par_date) 
         left join rf_oes_321_org org2 on org2.oes_321_org_id = par_oes_org_id;

  return var_branch_id;      
END override_branch_id;
--
-- �������� ��� ��������� ��������/������ ������ ��� ������������ ������ �� ��������������� �������/�������� (����������/����������)
-- ���� ���������� ������ ���, �� � ��������������� ��������� ��������� ��������� null
--
PROCEDURE get_existing_alerts
(
  par_inter_kgrko_cd     INTEGER,  -- ������� �� ����� ������� "�������������" �������� ����� �����:
                                   -- 1 - ����������, 2 - ����������, 3 - ���, 0 - ����� ����� ��� ������� ��������
  par_op_cat_cd          VARCHAR2, -- ������������� ��������/������ ������: ��������� (WT, CT, BOT)
  par_trxn_seq_id        INTEGER,  -- ������������� ��������: ID ��������
  par_trxn_scrty_seq_id  INTEGER,  -- ������������� ������ ������: ID ������ ������
  par_review1_id     OUT mantas.kdd_review.review_id%TYPE, -- ��������� - ID ������ "�� �����������", � ������ par_inter_kgrko_cd = 0 ��� ID ������ ��� ������� ��������                                 
  par_review2_id     OUT mantas.kdd_review.review_id%TYPE  -- ��������� - ID ������ "�� ����������"
) IS
BEGIN
  --
  -- ���� ��������� �����/������
  --
  SELECT max(case when (par_inter_kgrko_cd in (1, 3) and rf_kgrko_party_cd = 1) or -- ���� � ������� ����� �� �����������
                       (par_inter_kgrko_cd = 0 and rf_kgrko_party_cd is null)      -- ���� � ������� ������� �����
                  then review_id
             end),
         max(case when (par_inter_kgrko_cd in (2, 3) and rf_kgrko_party_cd = 2)    -- ���� � ������� ����� �� ����������
                  then review_id
             end)   
    INTO par_review1_id, par_review2_id                            
    FROM mantas.kdd_review 
   WHERE rf_op_cat_cd = par_op_cat_cd and
         rf_trxn_seq_id = par_trxn_seq_id and
         rf_alert_type_cd = 'O' and
         ((par_trxn_scrty_seq_id is null and nvl(rf_object_tp_cd, '?') <> 'TRXN_SCRTY') or  -- �� ��������� ������ �� ��������� ������ �������, ���� ���� ������ �� ��������
          rf_trxn_seq_id = par_trxn_scrty_seq_id);                                          -- ����� ������ ������ �� ��������� ������ ������, ���� ���� ������ �� ������ ������
END get_existing_alerts;  

FUNCTION create_second_oes
(
  p_first_review_id   IN   mantas.kdd_review.review_id%TYPE       -- ������ ����� �� ��������
 ,p_opok_nb           IN   mantas.kdd_review.rf_opok_nb%TYPE      -- ��� ���� ������������ ������
 ,p_add_opoks_tx      IN   mantas.kdd_review.rf_add_opoks_tx%TYPE -- �������������� ���� ���� ������������ ������
 ,p_due_dt            IN   mantas.kdd_review.due_dt%TYPE          -- ���� ������������ ������      
 ,p_owner_id          IN   mantas.kdd_review.owner_seq_id%TYPE    -- ������������ ������������ ������      
 ,p_user_id           IN   mantas.kdd_review.owner_seq_id%TYPE    -- ��� ������ �����         
 ,p_cmmnt_id          IN   mantas.kdd_cmmnt.cmmnt_id%TYPE         -- ���������� �����������
 ,p_note_tx           IN   mantas.kdd_note_hist.note_tx%TYPE      -- ������������ �����������
 ,p_message           OUT  VARCHAR2                               -- �������� ��������: ��������� (������, �����, ���������� ��������)
 ,p_second_review_id  OUT  mantas.kdd_review.review_id%TYPE       -- �������� ��������: id ���������� ������
 ,p_second_oes_321_id OUT  mantas.rf_oes_321.oes_321_id%TYPE      -- �������� ��������: id ���������� ���
)
RETURN BOOLEAN IS -- ������ ��������� ������� ��� ���
  v_msg                   VARCHAR2(2000);
  v_second_review_id      mantas.kdd_review.review_id%TYPE; 
  v_second_oes_321_id     mantas.rf_oes_321.oes_321_id%TYPE;
  v_owner_org             mantas.kdd_review_owner.rptg_group_cd%TYPE;
  v_object_tp_cd          mantas.kdd_review.rf_object_tp_cd%TYPE;
  v_alert_type_cd         mantas.kdd_review.rf_alert_type_cd%TYPE;
  v_op_cat_cd             mantas.kdd_review.rf_op_cat_cd%TYPE;
  v_trxn_seq_id           mantas.kdd_review.rf_trxn_seq_id%TYPE;
  v_trxn_scrty_seq_id     mantas.kdd_review.rf_trxn_scrty_seq_id%TYPE;
  v_first_kgrko_party_cd  mantas.kdd_review.rf_kgrko_party_cd%TYPE;
  v_second_kgrko_party_cd mantas.kdd_review.rf_kgrko_party_cd%TYPE;
  v_send_oes_org_id       business.rf_trxn_extra.send_oes_org_id%TYPE;
  v_rcv_oes_org_id        business.rf_trxn_extra.rcv_oes_org_id%TYPE;
  v_kgrko_party_cd_cnt    NUMBER;
  v_second_branch_id      mantas.kdd_review.rf_branch_id%TYPE;
  v_tmp_ids               mantas.rf_tab_number;
  v_oes_check_status      VARCHAR2(1 CHAR);  
  v_opok_cnt              NUMBER;
  v_add_opoks_tx          mantas.kdd_review.rf_add_opoks_tx%TYPE;
  v_trxn_exctn_dt         DATE;
  v_creator_id            mantas.kdd_review_owner.owner_id%TYPE;
BEGIN
  SAVEPOINT create_second_oes;
 
  -- �������� ����������� ���������� �� �������� � ������
  -- �������������
  BEGIN
    SELECT rptg_group_cd INTO v_owner_org FROM mantas.kdd_review_owner WHERE owner_seq_id = p_owner_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_message:='������: ������ �������������� �������������!';
      RETURN FALSE;  
  END;

  -- ���������
  BEGIN
    SELECT owner_id INTO v_creator_id FROM mantas.kdd_review_owner WHERE owner_seq_id = p_user_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_message:='������: ������ �������������� ���������!';
      RETURN FALSE;  
  END;
  
  -- ��������� ���� opok_nb
  SELECT count(*) INTO v_opok_cnt FROM  mantas.rf_opok opk WHERE opk.opok_nb=p_opok_nb;
  IF v_opok_cnt=0 THEN
    p_message:='������: ������ �������� ��� �������������� ��� ���� �������� ='||TO_CHAR(p_opok_nb)||'!';
    RETURN FALSE;  
  END IF;

  -- 0 ������ ������
  IF p_add_opoks_tx='0' 
    THEN v_add_opoks_tx:= NULL;
    ELSE v_add_opoks_tx:= p_add_opoks_tx;
  END IF;
  
  -- ��������� ���� add_opoks_tx
  SELECT COUNT(*) 
    INTO v_opok_cnt 
    FROM TABLE(mantas.rf_pkg_scnro.list_to_tab(v_add_opoks_tx)) t
      LEFT OUTER JOIN mantas.rf_opok opk on to_char(opk.opok_nb) = t.column_value
    WHERE opk.opok_nb is null;
  IF v_opok_cnt>0 THEN
    p_message:='������: ������� �������� ��� �������������� �������������� ���� ���� �������� ='||TO_CHAR(v_add_opoks_tx)||'!';
    RETURN FALSE;  
  END IF;
  
  -- �������� ���������� �� ������� ������ � ��������� ���������
  BEGIN
    SELECT r.rf_object_tp_cd,rf_alert_type_cd, r.rf_op_cat_cd,r.rf_trxn_seq_id,r.rf_trxn_scrty_seq_id,r.rf_kgrko_party_cd
          ,COALESCE(e.send_oes_org_id,t.send_oes_org_id) as send_oes_org_id
          ,COALESCE(e.rcv_oes_org_id,t.rcv_oes_org_id) as rcv_oes_org_id
          ,t.trxn_dt as trxn_exctn_dt
          ,(SELECT COUNT(DISTINCT r2.rf_kgrko_party_cd)
             FROM mantas.kdd_review r2
             WHERE r2.rf_op_cat_cd = r.rf_op_cat_cd
             AND r2.rf_trxn_seq_id = r.rf_trxn_seq_id
             AND r2.rf_object_tp_cd = r.rf_object_tp_cd
             AND (r2.rf_trxn_scrty_seq_id=r.rf_trxn_scrty_seq_id OR (r2.rf_trxn_scrty_seq_id IS NULL AND r.rf_trxn_scrty_seq_id IS NULL))
             AND r2.rf_alert_type_cd = 'O') AS kgrko_party_cd_cnt
      INTO v_object_tp_cd, v_alert_type_cd, v_op_cat_cd, v_trxn_seq_id, v_trxn_scrty_seq_id, v_first_kgrko_party_cd, v_send_oes_org_id, v_rcv_oes_org_id, v_trxn_exctn_dt, v_kgrko_party_cd_cnt      
      FROM mantas.kdd_review r
        LEFT OUTER JOIN table(business.rfv_trxn(r.rf_op_cat_cd, r.rf_trxn_seq_id, NULL)) t  ON 1=1
        LEFT OUTER JOIN business.rf_trxn_extra e ON e.op_cat_cd=r.rf_op_cat_cd AND  e.fo_trxn_seq_id=r.rf_trxn_seq_id
      WHERE r.review_id=p_first_review_id; 
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_message:='������: �������� � '||TO_CHAR(p_first_review_id)||' ������������!';
      RETURN FALSE;  
  END;  
  -- ��������� ������������ ������ � ��������
  IF v_alert_type_cd!='O' THEN  
    p_message:='������: �������� � '||TO_CHAR(p_first_review_id)||' �� ��������� � ������������� ��������!';
    RETURN FALSE;  
  ELSIF v_object_tp_cd='OES321' THEN  
    p_message:='������: �������� � '||TO_CHAR(p_first_review_id)||' ������� �������!';
    RETURN FALSE;  
  ELSIF v_send_oes_org_id IS NULL OR  v_rcv_oes_org_id IS NULL OR   v_send_oes_org_id=v_rcv_oes_org_id THEN  
    p_message:='������: �������� � '||TO_CHAR(p_first_review_id)||' �� ���������������!';
    RETURN FALSE;  
  ELSIF v_first_kgrko_party_cd IS NULL THEN  
    p_message:='������: �� �������� � '||TO_CHAR(p_first_review_id)||' �� ������ �������������� � ����������� ��� ����������!';
    RETURN FALSE;  
  ELSIF v_kgrko_party_cd_cnt>1 THEN  
    p_message:='������: ��� �������� � '||TO_CHAR(p_first_review_id)||' ��� ���� ������ ��������!';
    RETURN FALSE;  
  END IF;
  
  -- ���������� branch_id
  IF v_first_kgrko_party_cd=1
    THEN
      v_second_kgrko_party_cd:=2;
      SELECT o.tb_code*100000+nvl(o.osb_code,0)
        INTO v_second_branch_id
        FROM mantas.rf_oes_321_org o 
        WHERE o.oes_321_org_id=v_rcv_oes_org_id;
    ELSE
      v_second_kgrko_party_cd:=1;
      SELECT o.tb_code*100000+nvl(o.osb_code,0)
        INTO v_second_branch_id
        FROM mantas.rf_oes_321_org o 
        WHERE o.oes_321_org_id=v_send_oes_org_id;
  END IF;
  
  -- ������� �����
  v_second_review_id := rf_pkg_review.create_review(
    par_object_tp_cd          => v_object_tp_cd,
    par_op_cat_cd             => v_op_cat_cd,
    par_trxn_seq_id           => v_trxn_seq_id,
    par_trxn_scrty_seq_id     => v_trxn_scrty_seq_id,
    par_opok_list             => NULL,
    par_data_dump_dt          => NULL,
    par_branch_id             => v_second_branch_id,
    par_owner_seq_id          => p_owner_id,
    par_owner_org             => v_owner_org,
    par_trxn_dt               => CASE WHEN p_opok_nb in (6001, 8001, 5003, 5007) THEN NVL(p_due_dt, TRUNC(SYSDATE)) ELSE v_trxn_exctn_dt END,
    par_creat_id              => p_user_id,
    par_prcsng_batch_cmplt_fl => 'Y',
    par_actvy_type_cd         => 'RF_DBLOES',
    par_note_tx               => p_note_tx,
    par_cmmnt_id              => p_cmmnt_id,
    par_kgrko_party_cd        => v_second_kgrko_party_cd,
    par_opok_nb               => p_opok_nb,
    par_add_opoks_tx          => v_add_opoks_tx,
    par_status_cd             => 'RF_OES',
    par_non_opok_fl           => 'M'
    );
  v_msg := '������� �������� �'||to_char(v_second_review_id)||'.';  
  
   -- ������� ���
  v_second_oes_321_id := mantas.rf_pkg_oes.create_oes_321p(v_second_review_id, '1', v_creator_id);
  
  -- ����������� date_s � ��� ������ ��� ������������ ����� ����
  IF p_opok_nb IN (6001, 8001, 5003, 5007) THEN
    UPDATE rf_oes_321 o SET o.date_s=p_due_dt WHERE o.oes_321_id=v_second_oes_321_id;
  END IF;
  
  v_msg := v_msg||' ������� ��� �'||to_char(v_second_oes_321_id)||'.';
  
  
  -- ��������� ���
  v_oes_check_status := mantas.rf_pkg_oes_check.check_oes(par_form_cd => '321', par_oes_id => v_second_oes_321_id, par_commit_flag => 0);
  IF v_oes_check_status = 'W' Then
     v_msg := v_msg||' ��� ������ �������� � ����������������.';
  ELSIF v_oes_check_status = 'E' Then
     v_msg := v_msg||' ��� �� ������ �������� � ������� ���������.';
  END IF;
  
  -- ���������� ���������
  p_message:=v_msg;
  p_second_review_id:=v_second_review_id;
  p_second_oes_321_id:=v_second_oes_321_id;
  RETURN TRUE;
  
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK TO SAVEPOINT create_second_oes;
    p_message:='��������� ���������� ������:'||dbms_utility.format_error_stack;
    RETURN FALSE;  
END create_second_oes;

END RF_PKG_KGRKO;
/
SHOW ERRORS
GRANT EXECUTE on mantas.rf_pkg_kgrko to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_kgrko to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_kgrko to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_kgrko to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_kgrko to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_kgrko to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_kgrko to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_kgrko to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_kgrko to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_kgrko to KYC;
GRANT EXECUTE on mantas.rf_pkg_kgrko to STD;
GRANT EXECUTE on mantas.rf_pkg_kgrko to KDD_ALG;
