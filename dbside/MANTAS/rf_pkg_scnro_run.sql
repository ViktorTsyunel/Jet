grant execute on std.pkg_log to mantas;

begin 
  execute immediate 'drop table mantas.rf_run_opok_repeat#wrk';
exception when others then null; end;
/

begin 
  execute immediate 
'create table mantas.rf_run_opok_repeat#wrk
 (
   full_subpart_name   VARCHAR2(255 CHAR), -- ��� ���������������� ����������� � �������: <owner>.<table_name> subpartition (<subpartition_name>)
   trxn_count          INTEGER,            -- ���������� ������������ ��������
   rv_delete_count     INTEGER,            -- ���������� �������, ��� ������� ������������ ��������
   rv_create_count     INTEGER,            -- ���������� ��������� ������� 
   rv_repeat_count     INTEGER,            -- ���������� �������, ��� ������� ������������ ��������� ������������
   err_exists_flag     INTEGER             -- ���� 0/1 - ���� �� ������ ��� ��������� ���������
 )';
exception when others then null; end;
/

create or replace package mantas.rf_pkg_scnro_run IS
--
-- ��������� ��� �� �������, ����� ����� ������ �������� ������������ � ������� ����� BUSINESS, �� ������� �������
--
PROCEDURE run_all 
(
  par_mark_trxn_processed   INTEGER DEFAULT 1  -- ���� 0/1 - ������� �� �������� ����� ����������� �������� ������������� (����������� ������ �� �������� �������� � ��������)  
);
--
-- ��������� ��������� ����:
--  1) ��������� ��������� ��� ������������ �������� (����������� � �������� ��������), �� ������� ��� ���� ������
--  2) ��������� ����� �������� (����������� � �������� �������� � �� ������� �������)
--
FUNCTION run_opok RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
--
-- ��������� ��������� ��������� ��� ��������� ��������
--
PROCEDURE run_opok_repeat
(
  par_op_cat_cd             VARCHAR2,                                 -- ��������� ��������: WT - �����������, CT - ��������, BOT - ����������
  par_trxn_seq_id           NUMBER,                                   -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN/BACK_OFFICE_TRXN)   
  par_owner_seq_id          kdd_review_owner.owner_seq_id%TYPE,       -- ID ������������, ������������ ��������� ��������
  par_actvy_type_cd         kdd_activity_type_cd.actvy_type_cd%TYPE,  -- ��� ��������, ��������������� ��������� ��������
  par_note_tx               kdd_note_hist.note_tx%TYPE DEFAULT null,  -- ����������� � ��������� ���� � �������� ��������� ��������
  par_cmmnt_id              kdd_cmmnt.cmmnt_id%TYPE DEFAULT null,     -- ������������� ������������ ����������� � �������� ��������� ��������
  par_arh_mode              VARCHAR2 DEFAULT 'E',                     -- ����� ���������� ���������: 'E' (ENTIRELY) - ��������� ��������� ��������� �������� � ��������� ���������, 
                                                                      --                             'R' (REGISTER) - ������ ���������������� �������� � ������ ������������   
  par_delete_count     OUT  INTEGER,      -- ���������� �������, ��� ������� ������������ �������� - ������������ ��������
  par_create_count     OUT  INTEGER,      -- ���������� ��������� ������� - ������������ ��������  
  par_repeat_count     OUT  INTEGER       -- ���������� �������, ��� ������� ������������ ��������� ������������ - ������������ ��������
);
--
-- ���������� ��������� (������ ���� � ���������: �������/��������)
-- ��������� �������������� �� ��� ����, �� ������� ���� ������ ����������, 
-- ��� ���� � ����� ������ �������������� ��������� �� �������, ���������� � ������� ����
--
PROCEDURE calc_calendar;
--
-- �������� ��������������� �������, ������������ � �������� ����-��������/���������/����-��������� 
-- ��������� ������������� ��� ������ ����� ������� �������� ������. � ������ ������ ��������� ����������� exception.
-- ��������! ��������� ��������� TRUNCATE, ��������������, ���������� COMMIT.
--
PROCEDURE truncate_wrk_tables;
end rf_pkg_scnro_run;
/
create or replace package body mantas.rf_pkg_scnro_run IS
--
-- ��������� ��� �� �������, ����� ����� ������ �������� ������������ � ������� ����� BUSINESS, �� ������� �������
--
PROCEDURE run_all 
(
  par_mark_trxn_processed   INTEGER DEFAULT 1  -- ���� 0/1 - ������� �� �������� ����� ����������� �������� ������������� (����������� ������ �� �������� �������� � ��������)  
) IS

  var_err_exists     INTEGER := 0;
  var_tmp            INTEGER := 0;

  prcname            VARCHAR2(64) := ' mantas.rf_pkg_scnro_run.run_all';
BEGIN
  --
  -- �������� ������������� (��������� ����� ����������) ����� ����������� ��������
  --
  var_tmp := business.rf_pkg_postload.scnro_preprocess;
  if var_tmp <> 0 Then var_err_exists := 1; end if;
  --
  -- ���������� �������������, �������������� ��� ���������, � ������������ � ������� ������� ������ ��������� 
  --
  std.pkg_log.info('������������� �������������, �������������� ��� ��������� �������� (������������ ��������)...', prcname);
  
  BEGIN
    var_tmp := mantas.rf_pkg_scnro.rebuild_opok_view(par_query_cd => null, par_changed_only_fl => 'N');
    if var_tmp > 0 Then
      var_err_exists := 1;
      std.pkg_log.error('���������� ����������� ������� ��������� � ��������. ���������� ������: '||to_char(var_tmp)||'. ��� ������� �� ����� ������ ��� ��������� ��������!', prcname);
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      var_err_exists := 1;
      std.pkg_log.error('������ ��� ����������� �������������, �������������� ��� ��������� �������� (������������ ��������). ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
  END;      
  --
  -- ������� ���������, ���� �����
  --
  std.pkg_log.info('������������� ��������� ������� � �������� ����...', prcname);
  BEGIN
    calc_calendar;
  EXCEPTION
    WHEN OTHERS THEN
      var_err_exists := 1;
      std.pkg_log.error('������ ��� ������� ��������� ������� � �������� ���� (mantas.rf_pkg_scnro_run.calc_calendar). ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
  END;                   
  --
  -- �������� ���������(������������ ��������) 
  --  - �������� ������������ ��������, �� ������� ��� ���� ������
  --  - �������� �����/������������ ��������, �� ������� ��� �������
  --
  var_tmp := run_opok;
  if var_tmp <> 0 Then var_err_exists := 1; end if;
  --
  -- �������� ������������� �� ����� ��������� ������
  --
  var_tmp := rf_pkg_assign.assign_alerts;
  if var_tmp <> 0 Then var_err_exists := 1; end if;
  --
  -- �������� ������ �������������, ������ ��� �������� �������������
  --
  std.pkg_log.info('�������� ����� ��������� ������ ������������� ...', prcname);
  BEGIN
    EXECUTE IMMEDIATE 'alter session force parallel dml parallel 64';
    
    UPDATE /*+ PARALLEL(64) INDEX(kdd_review)*/ kdd_review 
       SET prcsng_batch_cmplt_fl = 'Y'
     WHERE case when prcsng_batch_cmplt_fl = 'N'
                then 1
           end = 1 and
           rf_alert_type_cd = 'O'; -- ������ ������ �� ������������� ��������
           
    COMMIT;   
    
    EXECUTE IMMEDIATE 'alter session enable parallel dml';    
  EXCEPTION
    WHEN OTHERS THEN
      var_err_exists := 1;
      std.pkg_log.fatal('������ ��� ��������� ������� �������������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
      begin EXECUTE IMMEDIATE 'alter session enable parallel dml'; exception when others then null; end;
  END;                   
  std.pkg_log.info('��������� �������� ����� ��������� ������ �������������', prcname);
  --
  -- ������� ���������� �� ������� �������
  --
  std.pkg_log.info('�������� ���������� �� ������� ���������� �������� ...', prcname);
  BEGIN
    dbms_stats.gather_table_stats(ownname => 'MANTAS', tabname => 'KDD_REVIEW', granularity => 'ALL', cascade => true, no_invalidate => false, degree => 32);
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.warn('������ ��� ������ ���������� �� ������� ���������� ��������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
  END;                   
  std.pkg_log.info('��������� �������� ���������� �� ������� ���������� ��������', prcname);
  --
  -- �������� � �������� �� ����� �������������� ������ � �������� ���������� "����� ���������"
  -- ��������� ������ �� ������� ���������� ��� ����������� ���������
  --
  var_tmp := rf_pkg_report.auto_report('AFTER_ALERTS');
  --
  -- ���� �� ���� ������ - �������� ����� �����������/���������� �������� ������������� (���������� ������ �� �������� �������� � ��������)
  --
  if par_mark_trxn_processed = 1 and var_err_exists = 0 Then
    mantas.rf_pkg_postscnro.mark_trxn_processed;
  end if;  
END run_all;  
--
-- ��������� ��������� ����:
--  1) ��������� ��������� ��� ������������ �������� (����������� � �������� ��������), �� ������� ��� ���� ������
--  2) ��������� ����� �������� (����������� � �������� �������� � �� ������� �������)
--
FUNCTION run_opok RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������

  var_err_exists            INTEGER := 0;
  var_err_count             INTEGER;
  var_count                 INTEGER;
  
  var_owner_seq_id          kdd_review_owner.owner_seq_id%TYPE;
  var_review_id             kdd_review.review_id%TYPE; 

  var_trxn_sum              INTEGER;
  var_delete_sum            INTEGER;
  var_create_sum            INTEGER;  
  var_repeat_sum            INTEGER;

  var_arh_locked            INTEGER := 0;

  var_sql_template          CLOB :=
q'{DECLARE
  var_err_exists            INTEGER := 0;

  var_trxn_sum              INTEGER;
  var_delete_count          INTEGER;
  var_create_count          INTEGER;  
  var_repeat_count          INTEGER;
  var_delete_sum            INTEGER;
  var_create_sum            INTEGER;  
  var_repeat_sum            INTEGER;

  prcname            VARCHAR2(64) := ' mantas.rf_pkg_scnro_run.run_opok';
BEGIN
  var_trxn_sum := 0;
  var_delete_sum := 0;
  var_create_sum := 0;  
  var_repeat_sum := 0;
  --
  -- ���� �� ��������� � ����������� #PART_SUBPART# ������� business.#PAR_TRXN_TABLE#
  --
  FOR r IN (SELECT /*+ FIRST_ROWS*/
                   '#PAR_OP_CAT_CD#' as op_cat_cd,
                   #PAR_TRXN_SEQ_ID# as trxn_seq_id,                   
                   (select count(*) 
                      from kdd_review 
                     where rf_op_cat_cd = '#PAR_OP_CAT_CD#' and 
                           rf_trxn_seq_id = t.#PAR_TRXN_SEQ_ID# and
                           rf_alert_type_cd = 'O' and 
                           nvl(rf_data_dump_dt, to_date('01.01.9000', 'dd.mm.yyyy')) <> t.data_dump_dt and
                           rownum <= 1) as trxn_change_flag
              FROM BUSINESS.#PAR_TRXN_TABLE# #PART_SUBPART# t
             WHERE rf_active_partition_flag = 1 and
                   EXISTS(select null
                            from kdd_review
                           where rf_op_cat_cd = '#PAR_OP_CAT_CD#' and
                                 rf_trxn_seq_id = t.#PAR_TRXN_SEQ_ID# and
                                 rf_alert_type_cd = 'O')) LOOP
    BEGIN
      SAVEPOINT run_opok;
      --
      -- ���� ����� ������� � ������ (�������) � � �������� ���������� - ��������� ��������� ���������
      --      
      if r.trxn_change_flag <> 0 Then
        mantas.rf_pkg_scnro_run.run_opok_repeat(par_op_cat_cd     => r.op_cat_cd,
                                                par_trxn_seq_id   => r.trxn_seq_id,   
                                                par_owner_seq_id  => #PAR_OWNER_SEQ_ID#,
                                                par_actvy_type_cd => 'RF_RUN',
                                                par_note_tx       => '�������������� ��������� �������� �������� (���������) � ����� � ���������� �� � ���������',
                                                par_arh_mode      => 'R', -- ������ ����������� �������� � ������� �������� � �������������
                                                par_delete_count  => var_delete_count,
                                                par_create_count  => var_create_count,  
                                                par_repeat_count  => var_repeat_count);
                        
        var_trxn_sum := var_trxn_sum + 1;                
        var_delete_sum := var_delete_sum + var_delete_count;
        var_create_sum := var_create_sum + var_create_count;
        var_repeat_sum := var_repeat_sum + var_repeat_count;
        
        COMMIT;                
      end if;                
    EXCEPTION
      WHEN OTHERS THEN
        var_err_exists := 1;
        std.pkg_log.error(SQLERRM, prcname);        
        ROLLBACK TO run_opok;
    END;      
  END LOOP;
  --
  -- ��������� ���������� �� ����������� ���������� ��������� �� ��������������� ������� 
  --
  INSERT INTO mantas.rf_run_opok_repeat#wrk(full_subpart_name, trxn_count, rv_delete_count, rv_create_count, 
                                            rv_repeat_count, err_exists_flag)
         VALUES('BUSINESS.#PAR_TRXN_TABLE# #PART_SUBPART#', var_trxn_sum, var_delete_sum, var_create_sum,
                var_repeat_sum, var_err_exists);  
  COMMIT;                                               
END;
}';

  var_sql_template2         CLOB :=
q'{DECLARE
  var_review_id             mantas.kdd_review.review_id%TYPE; 
  var_err_exists            INTEGER := 0;
  var_create_sum            INTEGER;  

  prcname            VARCHAR2(64) := ' mantas.rf_pkg_scnro_run.run_opok';
BEGIN
  var_create_sum := 0;  
  --
  -- ���� �� ������� � �������� #PART_SUBPART# ������� MANTAS.RF_KDD_REVIEW#WRK
  --
  FOR r IN (SELECT *
              FROM MANTAS.RF_KDD_REVIEW#WRK #PART_SUBPART#) LOOP
    BEGIN
      SAVEPOINT run_opok;
      --
      -- ������� �����
      --          
      var_review_id := rf_pkg_review.create_review(par_object_tp_cd => r.object_tp_cd, 
                                                   par_op_cat_cd => r.op_cat_cd, 
                                                   par_trxn_seq_id =>  r.trxn_seq_id, 
                                                   par_trxn_scrty_seq_id => r.trxn_scrty_seq_id, 
                                                   par_opok_list => r.opok_list, 
                                                   par_data_dump_dt => r.data_dump_dt, 
                                                   par_branch_id => r.branch_id, 
                                                   par_owner_seq_id => to_number(null),
                                                   par_owner_org => to_char(null),
                                                   par_trxn_dt => r.trxn_dt, 
                                                   par_creat_id => #PAR_OWNER_SEQ_ID#, 
                                                   par_prcsng_batch_cmplt_fl => 'N');       
      --
      -- ������� �������� � ������ ������������ ��������
      --
      INSERT INTO rf_arh_object(entity_cd, arh_dump_dt, entity_id)
             VALUES(r.op_cat_cd, r.data_dump_dt, r.trxn_seq_id);
      
      COMMIT;

      var_create_sum := var_create_sum + 1;                                             
    EXCEPTION
      WHEN OTHERS THEN
        var_err_exists := 1;
        std.pkg_log.fatal(SQLERRM, prcname);        
        ROLLBACK TO run_opok;
    END;      
  END LOOP;
  --
  -- ��������� ���������� �� ����������� �������� ������� �� ��������������� ������� 
  --
  INSERT INTO mantas.rf_run_opok_repeat#wrk(full_subpart_name, rv_create_count, err_exists_flag)
         VALUES('MANTAS.RF_KDD_REVIEW#WRK #PART_SUBPART#', var_create_sum, var_err_exists);  
  COMMIT;                                               
END;
}';
  var_sql                   CLOB;

  prcname            VARCHAR2(64) := ' mantas.rf_pkg_scnro_run.run_opok';  
BEGIN
  --
  -- ��������� ������������� ���������� ������������
  --
  SELECT/*+ NO_PARALLEL*/
         max(owner_seq_id)
    INTO var_owner_seq_id
    FROM kdd_review_owner
   WHERE owner_id = 'MANTAS';
  --
  -- ������������� ���������� �������� �������������, ������� ������� rf_arh_object
  -- 
  std.pkg_log.info('������������� ���������� �������� �������������, ������� ������� � ���������������� ������������ �������� (rf_arh_object)...', prcname);
  rf_pkg_arh.lock_arh_object;
  var_arh_locked := 1;
  --
  -- ������� ��������������� ������� mantas.rf_run_opok_repeat#wrk
  -- (truncate, ����� ������ ��� ��������� ����, ������� delete)
  --
  DELETE FROM mantas.rf_run_opok_repeat#wrk;
  COMMIT;
  --
  -- ��������� ��������� �������� �������� ������������� �������� �� ������������ �������� �������� ������ WIRE_TRXN, CASH_TRXN, BACK_OFFICE_TRXN
  --
  std.pkg_log.info('��������� ��������� �������� (���������) ������������ ��������, �� ������� ��� ���� ������ ...', prcname);

  FOR r IN (SELECT case level
                     when 1 then 'WT'
                     when 2 then 'CT'
                     when 3 then 'BOT'
                   end as op_cat_cd,
                   case level
                     when 1 then 'WIRE_TRXN'
                     when 2 then 'CASH_TRXN'
                     when 3 then 'BACK_OFFICE_TRXN'
                   end as trxn_table,
                   case level
                     when 1 then 'fo_trxn_seq_id'
                     when 2 then 'fo_trxn_seq_id'
                     when 3 then 'bo_trxn_seq_id'
                   end as trxn_seq_id                     
              FROM dual
            CONNECT BY level <= 3) LOOP

    var_sql := var_sql_template;
    var_sql := replace(var_sql, '#PAR_OP_CAT_CD#',   r.op_cat_cd);    
    var_sql := replace(var_sql, '#PAR_TRXN_TABLE#',  r.trxn_table);    
    var_sql := replace(var_sql, '#PAR_TRXN_SEQ_ID#', r.trxn_seq_id);    
    var_sql := replace(var_sql, '#PAR_OWNER_SEQ_ID#', nvl(to_char(var_owner_seq_id), 'null'));    

    var_err_count := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_sql,  
                                                             par_degree         => 64, 
                                                             par_task_name      => 'aml_run_opok_repeat_'||lower(r.op_cat_cd),
                                                             par_table_owner    => 'BUSINESS', 
                                                             par_table_name     => r.trxn_table, 
                                                             par_partition_name => 'P_ACTIVE', 
                                                             par_chunk_type     => 'SP',
                                                             par_log_level      => 'ERROR');
    
    if var_err_count > 0 Then
      var_err_exists := 1;
    end if;  
  END LOOP;
  --
  -- ������� �� ��������������� ������� ���������� �� ���������� ���������
  --
  SELECT nvl(sum(trxn_count), 0), 
         nvl(sum(rv_delete_count), 0), 
         nvl(sum(rv_create_count), 0), 
         nvl(sum(rv_repeat_count), 0), 
         case when var_err_exists > 0 or sum(err_exists_flag) > 0 
              then 1
              else 0
         end     
    INTO var_trxn_sum, var_delete_sum, var_create_sum, var_repeat_sum, var_err_exists
    FROM mantas.rf_run_opok_repeat#wrk;

  std.pkg_log.info('��������� ��������� ��������� �������� (���������) ������������ ��������. '||
                   '��������� ��������: '||to_char(var_trxn_sum)||'; '||
                   '������������ �������� �������: '||to_char(var_delete_sum)||'; '||
                   '������������ ��������� ������������ �������: '||to_char(var_repeat_sum)||'; '||
                   '������� ����� �������: '||to_char(var_create_sum), prcname);
  --
  -- ������� ������� � �������� ����������� �������, ���� ��� ����� �� �����
  -- (������ ��� ��������� ����� ������� �������� ������)
  -- ��������� DELETE, �. �. TRUNCATE ��� ��������� ����� ����� �����
  --
  SELECT count(*)
    INTO var_count
    FROM mantas.rf_kdd_review#wrk
   WHERE rownum <= 1;

  if var_count > 0 Then
    DELETE FROM mantas.rf_kdd_review#wrk;
    COMMIT;
  end if;
  --
  -- �������� ��������� ��� ����� �������� (�������� � �������� �������� ��� �������)
  -- ���� �� ���������� �������
  --
  var_create_sum := 0;  
  std.pkg_log.info('��������� ��������� (������������ ��������) ��� ����� �������� ...', prcname);
  FOR r IN (SELECT *
              FROM table(rf_pkg_scnro.opok_alerts(par_mode => 'NEW'))) LOOP
    BEGIN
      if var_create_sum = 0 Then
        std.pkg_log.info('���������� ���������� ��������� (������������ ��������) � ������������� ������� RF_KDD_REVIEW#WRK ...', prcname);
      end if;  
    
      INSERT INTO mantas.rf_kdd_review#wrk(object_tp_cd, op_cat_cd, trxn_seq_id, trxn_scrty_seq_id, 
                                           opok_list, branch_id, data_dump_dt, trxn_dt)
             VALUES(r.object_tp_cd, r.op_cat_cd, r.trxn_seq_id, r.trxn_scrty_seq_id, 
                    r.opok_list, r.branch_id, r.data_dump_dt, r.trxn_dt);                              
      COMMIT;

      var_create_sum := var_create_sum + 1;                                             
    EXCEPTION
      WHEN OTHERS THEN
        var_err_exists := 1;
        std.pkg_log.fatal('������ ��� ������ ����������� ��������� (������������ ��������). ��������� ��������: '||r.op_cat_cd||', ID ��������: '||to_char(r.trxn_seq_id)||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    END;            
  END LOOP;            
  std.pkg_log.info('��������� ���������� ���������� ��������� (������������ ��������) � ������������� ������� RF_KDD_REVIEW#WRK. ������� ������� (����� ������� ����� �������): '||to_char(var_create_sum), prcname);
  --
  -- ������� ��������������� ������� mantas.rf_run_opok_repeat#wrk
  -- (truncate, ����� ������ ��� ��������� ����, ������� delete)
  --
  DELETE FROM mantas.rf_run_opok_repeat#wrk;
  COMMIT;
  --
  -- ������� ������ (������������� ��������)
  --
  std.pkg_log.info('������� ������ (������������� �������� dbms_parallel_execute) � ������������ � ������������ ��������� (������������ ��������). ������ ���� ������� ����� �������: '||to_char(var_create_sum), prcname);
  var_sql := var_sql_template2;
  var_sql := replace(var_sql, '#PAR_OWNER_SEQ_ID#', nvl(to_char(var_owner_seq_id), 'null'));    

  var_err_count := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_sql,  
                                                           par_degree         => 64, 
                                                           par_task_name      => 'aml_create_review',
                                                           par_table_owner    => 'MANTAS', 
                                                           par_table_name     => 'RF_KDD_REVIEW#WRK', 
                                                           par_partition_name => null, 
                                                           par_chunk_type     => 'P',
                                                           par_log_level      => 'FATAL');
    
  if var_err_count > 0 Then
    var_err_exists := 1;
  end if;  
  --
  -- ������� �� ��������������� ������� ���������� �� �������� �������
  --
  SELECT nvl(sum(rv_create_count), 0), 
         case when var_err_exists > 0 or sum(err_exists_flag) > 0 
              then 1
              else 0
         end     
    INTO var_create_sum, var_err_exists
    FROM mantas.rf_run_opok_repeat#wrk;

  std.pkg_log.info('��������� ��������� ��������� (������������ ��������) ��� ����� ��������. ������� ����� �������: '||to_char(var_create_sum), prcname);
  --
  -- ���������� ������� ��������� ��������, ��������, ������, ������ �����, ��������� � ������ ��������
  --
  std.pkg_log.info('���������� ������� ��������� ��������, ��������, ������, ������ �����, ��������� � ������������� � ������ ��������...', prcname);

  var_err_count := rf_pkg_arh.archive_all;
  if var_err_count > 0 Then
    var_err_exists := 1;
  end if;  

  -- ������� ���������� �������������
  rf_pkg_arh.release_arh_object;
  var_arh_locked := 0;
  COMMIT;

  std.pkg_log.info('��������� ������������ ������� ��������� ��������, ��������, ������, ������ �����, ��������� � ������������� � ������ ��������', prcname);
  
  return var_err_exists;
EXCEPTION
  WHEN OTHERS THEN
    BEGIN
      std.pkg_log.fatal('������ ��� ��������� �������� (������������ ��������). ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
      if var_arh_locked = 1 Then
        rf_pkg_arh.release_arh_object;
      end if;
    EXCEPTION
      WHEN OTHERS THEN
        null;
    END;
    
    return 1;    
END run_opok;  
--
-- ��������� ��������� �������� (���������, ��������) ��� ��������� ��������
--
PROCEDURE run_opok_repeat
(
  par_op_cat_cd             VARCHAR2,                                 -- ��������� ��������: WT - �����������, CT - ��������, BOT - ����������
  par_trxn_seq_id           NUMBER,                                   -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN/BACK_OFFICE_TRXN)   
  par_owner_seq_id          kdd_review_owner.owner_seq_id%TYPE,       -- ID ������������, ������������ ��������� ��������
  par_actvy_type_cd         kdd_activity_type_cd.actvy_type_cd%TYPE,  -- ��� ��������, ��������������� ��������� ��������
  par_note_tx               kdd_note_hist.note_tx%TYPE DEFAULT null,  -- ����������� � ��������� ���� � �������� ��������� ��������
  par_cmmnt_id              kdd_cmmnt.cmmnt_id%TYPE DEFAULT null,     -- ������������� ������������ ����������� � �������� ��������� ��������
  par_arh_mode              VARCHAR2 DEFAULT 'E',                     -- ����� ���������� ���������: 'E' (ENTIRELY) - ��������� ��������� ��������� �������� � ��������� ���������, 
                                                                      --                             'R' (REGISTER) - ������ ���������������� �������� � ������ ������������ 
  par_delete_count     OUT  INTEGER,      -- ���������� �������, ��� ������� ������������ �������� - ������������ ��������
  par_create_count     OUT  INTEGER,      -- ���������� ��������� ������� - ������������ ��������  
  par_repeat_count     OUT  INTEGER       -- ���������� �������, ��� ������� ������������ ��������� ������������ - ������������ ��������
) IS

  var_canceled_fl          VARCHAR2(1 CHAR);
  var_data_dump_dt         DATE;
  var_trxn_dt              DATE;
  var_trxn_base_am         NUMBER;

  var_review_id            kdd_review.review_id%TYPE;
  var_review_ids           mantas.RF_TAB_NUMBER := RF_TAB_NUMBER();
  var_review2_ids          mantas.RF_TAB_NUMBER := RF_TAB_NUMBER();
  var_actvy_ids            rf_pkg_review.TAB_NUMBER;
  
  var_set_opok_flag        INTEGER;
  var_opok_repeat_flag     INTEGER;
  var_opok_nb              kdd_review.rf_opok_nb%TYPE;
  var_add_opoks_tx         kdd_review.rf_add_opoks_tx%TYPE;

  prcname                  VARCHAR2(255) := ' (mantas.rf_pkg_scnro_run.run_opok_repeat)';
BEGIN
  std.pkg_log.debug('������ ��������� �������� ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id), prcname);
  --
  -- �������� ���������� ���������
  --
  if par_op_cat_cd is null or par_op_cat_cd not in ('WT', 'CT', 'BOT') or par_trxn_seq_id is null or 
     par_arh_mode not in ('E', 'R') or par_arh_mode is null Then
    raise_application_error(-20001, '�������� �������� ���������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||prcname);
  end if;  

  par_delete_count := 0;
  par_create_count := 0;
  par_repeat_count := 0;
  --
  -- ������� ������ ��� �������� ��������� ��������
  --
  std.pkg_log.debug('������� ������ ��� �������� ��������� ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
  SELECT /*+ NO_PARALLEL*/ 
         max(canceled_fl), max(data_dump_dt), max(trxn_dt), max(trxn_base_am)
    INTO var_canceled_fl, var_data_dump_dt, var_trxn_dt, var_trxn_base_am
    FROM (select nvl(rf_canceled_fl, 'N') as canceled_fl, data_dump_dt, trxn_exctn_dt as trxn_dt, trxn_base_am
            from business.wire_trxn 
           where par_op_cat_cd = 'WT' and
                 fo_trxn_seq_id = par_trxn_seq_id
          union all
          select nvl(rf_canceled_fl, 'N'), data_dump_dt, trxn_exctn_dt as trxn_dt, trxn_base_am
            from business.cash_trxn
           where par_op_cat_cd = 'CT' and
                 fo_trxn_seq_id = par_trxn_seq_id
          union all
          select nvl(rf_canceled_fl, 'N'), data_dump_dt, exctn_dt as trxn_dt, trxn_base_am
            from business.back_office_trxn
           where par_op_cat_cd = 'BOT' and
                 bo_trxn_seq_id = par_trxn_seq_id);

  if var_canceled_fl is null Then
    raise_application_error(-20001, '������ �������������� ������������� ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||prcname);
  end if;
  --
  -- ������������ �������� "�������� ��������" ��� ���� ��� ������������ �������, ���������� �������������� ��������
  --
  if par_actvy_type_cd is not null Then
    std.pkg_log.debug('������������ �������� "�������� ��������" ��� ���� ��� ������������ �������, ���������� �������������� ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
    SELECT /*+ NO_PARALLEL*/ review_id
      BULK COLLECT INTO var_review_ids
      FROM kdd_review
     WHERE rf_op_cat_cd = par_op_cat_cd and 
           rf_trxn_seq_id = par_trxn_seq_id and
           rf_alert_type_cd = 'O';
    
    if var_review_ids is not null and var_review_ids.COUNT > 0 Then
      var_actvy_ids := rf_pkg_review.create_activities(p_review_ids    => var_review_ids,
                                                       p_actvy_type_cd => par_actvy_type_cd,
                                                       p_cmmnt_id      => par_cmmnt_id,
                                                       p_note_tx       => par_note_tx,
                                                       p_owner_seq_id  => par_owner_seq_id);
    end if; 
  end if; 
  --
  -- ���� ��������� �������� ������� - ���������� �������� ��� ���� ��������� � ��� �������:
  --  - ������� �������� ��������������� ��������� � ��� �������, ��� �� ��������� � �� ����������� � �������� ��������
  --  - ���������� �������� ��� ���� �������
  --
  if var_canceled_fl = 'Y' Then
    std.pkg_log.debug('�������� �������/������������ � ���������. ���������� �������� ��� ���� ��������� � ��������� �������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);

    SELECT /*+ NO_PARALLEL*/ review_id
      BULK COLLECT INTO var_review_ids
      FROM kdd_review
     WHERE rf_op_cat_cd = par_op_cat_cd and 
           rf_trxn_seq_id = par_trxn_seq_id and
           rf_alert_type_cd = 'O' and 
           status_cd not like '%-' and 
           status_cd not like '%='; -- �������, �������������� �� - ��� = ��������� � �������� �������� �������� (�� � ����� �������� ��������� ��������������)
            
    if var_review_ids is not null and var_review_ids.COUNT > 0 Then
      -- ��������� ������ � ����� �������, ��������������� �������� ��������� ��������
      rf_pkg_review.update_reviews(p_review_ids       => var_review_ids,
                                   p_actvy_type_cd    => 'RF_DELETE-',
                                   p_new_owner_seq_id => null,
                                   p_new_owner_org    => null,
                                   p_due_dt           => null,
                                   p_opok_nb          => null,
                                   p_add_opoks_tx     => null,
                                   p_owner_seq_id     => par_owner_seq_id);

      -- ������������ ������ �������� ��� ������� � ���������� ����������� �� ����
      rf_pkg_review.create_activities(p_review_ids    => var_review_ids,
                                      p_actvy_type_cd => 'RF_DELETE-',
                                      p_cmmnt_id      => null,
                                      p_note_tx       => '�������� � ����� � �������/�������������� �������� � ���������',
                                      p_owner_seq_id  => par_owner_seq_id);

      par_delete_count := par_delete_count + var_review_ids.COUNT;                                
    end if; 
    --
    -- ����� ��������� � ������������� �������� �������� �������� - �������� ������ �� ����
    --
    GOTO arh_and_return;
    /*--
    -- �������
    --
    COMMIT;
    
    return;   */          
  end if;
  --
  -- �� ������ ������: ���������� �������� ���� ������� ��� ��������� �������� �� ��� �� ������������ � �������� �� ������ ������� 
  --
  SELECT /*+ NO_PARALLEL*/ review_id
    BULK COLLECT INTO var_review_ids
    FROM kdd_review rv
   WHERE rf_op_cat_cd = par_op_cat_cd and 
         rf_trxn_seq_id = par_trxn_seq_id and
         rf_alert_type_cd = 'O' and 
         rf_trxn_scrty_seq_id is not null and
         rf_object_tp_cd = 'TRXN_SCRTY' and
         NOT EXISTS(select null
                      from business.rf_wire_trxn_scrty
                     where par_op_cat_cd = 'WT' and
                           trxn_scrty_seq_id = rv.rf_trxn_scrty_seq_id                           
                    union all
                    select null
                      from business.rf_cash_trxn_scrty
                     where par_op_cat_cd = 'CT' and
                           trxn_scrty_seq_id = rv.rf_trxn_scrty_seq_id) and
         status_cd not like '%-' and 
         status_cd not like '%='; -- �������, �������������� �� - ��� = ��������� � �������� �������� �������� (�� � ����� �������� ��������� ��������������);

  if var_review_ids is not null and var_review_ids.COUNT > 0 Then
    std.pkg_log.debug('���������� �������� ���� ������� ��� ��������� �������� �� ��� �� ������������ � �������� �� ������ �������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
    -- ��������� ������ � ����� �������, ��������������� �������� ��������� ��������
    rf_pkg_review.update_reviews(p_review_ids       => var_review_ids,
                                 p_actvy_type_cd    => 'RF_DELETE-',
                                 p_new_owner_seq_id => null,
                                 p_new_owner_org    => null,
                                 p_due_dt           => null,
                                 p_opok_nb          => null,
                                 p_add_opoks_tx     => null,
                                 p_owner_seq_id     => par_owner_seq_id);

    -- ������������ ������ �������� ��� ������� � ���������� ����������� �� ����
    rf_pkg_review.create_activities(p_review_ids    => var_review_ids,
                                    p_actvy_type_cd => 'RF_DELETE-',
                                    p_cmmnt_id      => null,
                                    p_note_tx       => '�������� � ����� � ��������� ������ ������ �� ��',
                                    p_owner_seq_id  => par_owner_seq_id);

    par_delete_count := par_delete_count + var_review_ids.COUNT;                                
  end if;     
  --
  -- �������� ��������� ��� ��������� ��������:
  --  - ������� ������� ������������ � ����� ���������� �������
  --  - ���������� �������� ������, �������� �����������, ���������� ������������ �������
  -- ����: ��������� �������� �� �������� � �� ������ ������ ������ � ���
  --
  std.pkg_log.debug('�������� ��������� ��� ��������� ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
  var_review_ids.DELETE;
  FOR r IN (WITH new_alerts as
                 (select /*+ MATERIALIZE*/ 
                         t.*, 
                         decode(object_tp_cd, 'TRXN_SCRTY_JOINT', 'TRXN', object_tp_cd) as obj_tp_join,
                         decode(object_tp_cd, 'TRXN_SCRTY_JOINT', to_number(null), trxn_scrty_seq_id) as trxn_scrty_join
                    from table(mantas.rf_pkg_scnro.opok_alerts(par_mode => 'TRXN', 
                                                               par_op_cat_cd => par_op_cat_cd, 
                                                               par_trxn_seq_id => par_trxn_seq_id)) t),
                 old_alerts as
                 (select /*+ MATERIALIZE*/
                         rv.*,
                         decode(rf_object_tp_cd, 'TRXN_SCRTY_JOINT', 'TRXN', rf_object_tp_cd) as obj_tp_join,
                         decode(rf_object_tp_cd, 'TRXN_SCRTY_JOINT', to_number(null), rf_trxn_scrty_seq_id) as trxn_scrty_join
                    from mantas.kdd_review rv
                   where rf_op_cat_cd = par_op_cat_cd and
                         rf_trxn_seq_id = par_trxn_seq_id and
                         rf_alert_type_cd = 'O')                     
            SELECT /*+ NO_PARALLEL*/
                   oa.review_id, 
                   oa.status_cd       as rv_status_cd, 
                   oa.rf_object_tp_cd as rv_object_tp_cd,
                   oa.rf_repeat_nb    as rv_repeat_nb,
                   oa.rf_data_dump_dt as rv_data_dump_dt,
                   oa.rf_trxn_scrty_seq_id as rv_trxn_scrty_seq_id, 
                   -- ���������� �������������� � ��� ������������ ������� 
                   first_value(oa.owner_seq_id) over(order by case when oa.owner_seq_id is not null then 1 else 2 end asc,  -- ������� - � ��������� �������������
                                                              case when oa.rf_object_tp_cd in ('TRXN', 'TRXN_SCRTY_JOINT') 
                                                                   then 1 else 2 
                                                              end asc,                                                      -- ������� - ������� ������, ����� - �� ������ �������
                                                              review_id desc) as rv_owner_seq_id,                           -- ������� - ������ ������
                   first_value(oa.owner_org) over(order by case when oa.owner_seq_id is not null then 1 else 2 end asc,     
                                                           case when oa.rf_object_tp_cd in ('TRXN', 'TRXN_SCRTY_JOINT') 
                                                                then 1 else 2 
                                                           end asc,                        
                                                           review_id desc) as rv_owner_org,                                
                   (select count(*) from old_alerts where rf_trxn_scrty_seq_id = t.trxn_scrty_join) as old_scrty_count,
                   t.canceled_fl, 
                   na.*,
                   (select count(*) from new_alerts where trxn_scrty_seq_id = t.trxn_scrty_join) as new_scrty_count
              FROM (select 'TRXN' as obj_tp_join,
                           to_number(null) as trxn_scrty_join,
                           to_char(null) as canceled_fl
                      from dual     
                    union all
                    select 'TRXN_SCRTY' as obj_tp_join,
                           trxn_scrty_seq_id as trxn_scrty_join,
                           canceled_fl
                      from business.rf_wire_trxn_scrty
                     where par_op_cat_cd = 'WT' and
                           fo_trxn_seq_id = par_trxn_seq_id
                    union all
                    select 'TRXN_SCRTY' as obj_tp_join,
                           trxn_scrty_seq_id as trxn_scrty_join,
                           canceled_fl
                      from business.rf_cash_trxn_scrty
                     where par_op_cat_cd = 'CT' and
                           fo_trxn_seq_id = par_trxn_seq_id) t
                   left join new_alerts na on na.obj_tp_join = t.obj_tp_join and
                                              nvl(na.trxn_scrty_join, -1) = nvl(t.trxn_scrty_join, -1)
                   left join old_alerts oa on oa.obj_tp_join = t.obj_tp_join and
                                              nvl(oa.trxn_scrty_join, -1) = nvl(t.trxn_scrty_join, -1)) LOOP

    -- ���� ������ ��� ���, � ��������/������ ������ �������� �������� - ������� ����� (� ��� �� �������������, ��� � � ������������ ������� �� ��������)
    if r.review_id is null Then
      if trim(r.opok_list) is not null Then
        std.pkg_log.debug('������� ����� (� ��� �� �������������, ��� � � ������������ ������� �� ��������). ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);

        var_review_id := rf_pkg_review.create_review(par_object_tp_cd => r.object_tp_cd, 
                                                     par_op_cat_cd => r.op_cat_cd, 
                                                     par_trxn_seq_id =>  r.trxn_seq_id, 
                                                     par_trxn_scrty_seq_id => r.trxn_scrty_seq_id, 
                                                     par_opok_list => r.opok_list, 
                                                     par_data_dump_dt => var_data_dump_dt, 
                                                     par_branch_id => r.branch_id, 
                                                     -- ���� �� ������� ���������� �������������� - "������" ����� �� ��������� �� ���������
                                                     par_owner_seq_id => nvl(r.rv_owner_seq_id, rf_pkg_assign.def_owner),
                                                     par_owner_org => case when r.rv_owner_seq_id is not null
                                                                           then r.rv_owner_org
                                                                           when rf_pkg_assign.def_owner is not null  
                                                                           then rf_pkg_assign.def_owner_org
                                                                      end,
                                                     par_trxn_dt => r.trxn_dt, 
                                                     par_creat_id => par_owner_seq_id, 
                                                     par_prcsng_batch_cmplt_fl => 'Y',
                                                     par_actvy_type_cd => par_actvy_type_cd,
                                                     par_note_tx => case when r.object_tp_cd in ('TRXN', 'TRXN_SCRTY_JOINT')
                                                                         then '�������� ��� ��������� �������� �������� �� �������� ���������'
                                                                         when r.object_tp_cd = 'TRXN_SCRTY' and r.old_scrty_count = 0
                                                                         then '�������� ��� ��������� �������� ������ ������ �� �������� ���������'
                                                                         when r.object_tp_cd = 'TRXN_SCRTY' and r.old_scrty_count <> 0
                                                                         then '�������� � ����� � ���������� ������ ������ � ��������� ���������'  
                                                                    end); 

        par_create_count := par_create_count + 1;                                
      end if;      
    else -- ���� ����� ��� ����
      --
      -- ���� ������ ������ ������� - �������� ����� � ������ �� ��������� ��������
      --
      if r.canceled_fl = 'Y' Then
        if r.rv_status_cd not like '%-' and r.rv_status_cd not like '%=' Then
          var_review_ids.extend;
          var_review_ids(var_review_ids.COUNT) := r.review_id; 
        end if;
      else 
        --
        -- ���� ����� ��� ������ (��� � �������� ��������), �� ���������� ��� ��������������
        --
        if r.rv_status_cd like '%-' or r.rv_status_cd like '%=' Then
          std.pkg_log.debug('���������� �������������� ������ #'||to_char(r.review_id)||'. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
          var_review2_ids.DELETE;
          var_review2_ids.extend;
          var_review2_ids(1) := r.review_id; 

          rf_pkg_review.update_reviews(p_review_ids       => var_review2_ids,
                                       p_actvy_type_cd    => 'RF_RESTORE',
                                       p_new_owner_seq_id => null,
                                       p_new_owner_org    => null,
                                       p_due_dt           => null,
                                       p_opok_nb          => null,
                                       p_add_opoks_tx     => null,
                                       p_owner_seq_id     => par_owner_seq_id);

          rf_pkg_review.create_activities(p_review_ids    => var_review2_ids,
                                          p_actvy_type_cd => 'RF_RESTORE',
                                          p_cmmnt_id      => null,
                                          p_note_tx       => '�������������� � ����� � ������� ������������� � ���������',
                                          p_owner_seq_id  => par_owner_seq_id);        
        end if;
        --
        -- ���������� ���������� ���������� ���������, � ��� ����� ������������� ������� "�� ����?", ���� ����� ���� � ������ ���
        -- (���������� ����� ���� � ������ �� ������ ������ ����� ���� �� ���� ��������: �� �������� �������� ��� ���� ���� ���������� � ������������ �����)
        --
        std.pkg_log.debug('���������� ���������� ���������� ��������� ��� ������ #'||to_char(r.review_id)||'. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
        INSERT INTO rf_kdd_review_run(review_id, repeat_nb, data_dump_dt, non_opok_fl, actvy_id)
               VALUES(r.review_id, r.rv_repeat_nb + 1, var_data_dump_dt, 
                      case when r.rv_object_tp_cd = 'TRXN_SCRTY' and -- �����, ��������������� ��������� ������ ������
                                trim(r.opok_list) is null and        -- �� ����� ����� ����
                                r.new_scrty_count <> 0               -- �� ������ ������ �������� �������� (� ������ ������������ ������ � ������������ ��������� � ������������ ������)
                           then 'J'                                  -- �������� ������ ������ ���������� ����� ���� ��� ��������� - ��� ���������� � ������������ ����� (JOINT)
                           when trim(r.opok_list) is null 
                           then 'Y' 
                      end, 
                      var_actvy_ids(r.review_id)); 
          
        if trim(r.opok_list) is not null Then       
          INSERT INTO rf_kdd_review_opok(record_id, review_id, repeat_nb, trxn_scrty_seq_id, opok_nb, explanation_tx, snapshot_id)
            (SELECT /*+ NO_PARALLEL*/
                    rf_kdd_review_opok_seq.NextVal as record_id,         
                    r.review_id, 
                    r.rv_repeat_nb + 1 as repeat_nb,
                    to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 5))) as trxn_scrty_seq_id,
                    to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 1))) as opok_nb,
                    trim(regexp_substr(opok.column_value, '[^\~]+', 1, 4)) as explanation_tx,
                    rls.snapshot_id
               FROM table(mantas.rf_pkg_scnro.list_to_tab(replace(r.opok_list, '~', ' ~ '), '|')) opok
                    join rf_opok_rule_snapshot rls on rls.rule_seq_id = to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 2))) and 
                                                      rls.record_seq_id = to_number(trim(regexp_substr(opok.column_value, '[^\~]+', 1, 3))) and
                                                      rls.actual_fl = 'Y');
        end if;     
        --
        -- ���������: ���������� �� ���������� ���� ����, ������� �� (� ��������� ��) �������� ���� ���� � ������, 
        --            ���������� �� ��������� ������������ ������ � ����� � ���������� ���������� ����� ����
        --
        SELECT /*+ NO_PARALLEL*/ 
               -- ���� ������������� � ������������ ���������� ����� ���� � ������
               case when new_opok.opok_list is not null and 
                         nvl(new_opok.opok_list, '0') <> nvl(prev_opok.opok_list, '0') and -- ���������� ���� ���� ����������
                         nvl(rv_opok.opok_list, '0') = nvl(prev_opok.opok_list, '0')       -- ������ ��������� ����� ���� �� ����
                    then 1
               end,               

               new_opok.opok_nb,        -- �������� ��� ���� ��� ����������� � �����
               new_opok.add_opoks_tx,   -- �������������� ���� ���� ��� ����������� � �����

               -- ���� ������������� ���������� ������������ ������ ��-�� ��������� ���������� ����� ����    
               case when nvl(new_opok.opok_list, '0') <> nvl(prev_opok.opok_list, '0') and -- ���������� ���� ���� ����������
                         nvl(rv_opok.opok_list, '0') <> nvl(new_opok.opok_list, '0')       -- ������������� � ������ ���� ���� �� ��������� � �����������
                    then 1
               end               
          INTO var_set_opok_flag, var_opok_nb, var_add_opoks_tx, var_opok_repeat_flag
          FROM /*������ ����� ���������� ����� ���� + �������� �� ��� � �������������� */
               (select listagg(opok, ',') within group(order by opok) as opok_list,
                       max(first_opok_nb) as opok_nb,
                       listagg(nullif(to_number(opok), first_opok_nb), ', ') within group (order by priority_nb) as add_opoks_tx
                  from (select distinct 
                               trim(regexp_substr(column_value, '[^\~]+', 1, 1)) as opok,
                               nvl(opk.priority_nb, opk.opok_nb) as priority_nb,
                               first_value(opk.opok_nb) over(order by nvl(opk.priority_nb, opk.opok_nb)) as first_opok_nb  
                          from table(rf_pkg_scnro.list_to_tab(replace(r.opok_list, '~', ' ~ '), '|')) t
                               join rf_opok opk on opk.opok_nb = to_number(trim(regexp_substr(t.column_value, '[^\~]+', 1, 1))))) new_opok
               cross join
               /*������ ����� ����, ���������� �������� � ���������� ���*/
               (select listagg(opok, ',') within group(order by opok) as opok_list
                  from (select distinct to_char(opok_nb) as opok
                          from rf_kdd_review_opok
                         where review_id = r.review_id and
                               repeat_nb = r.rv_repeat_nb)) prev_opok
               cross join
               /*������ ����� ����, ������������� � ������ (��������, ��� ������ ������������ �������)*/
               (select listagg(opok, ',') within group(order by opok) as opok_list
                  from (select distinct trim(t.column_value) as opok
                          from kdd_review rv
                               join table(rf_pkg_scnro.list_to_tab(to_char(rv.rf_opok_nb)||nvl2(trim(rv.rf_add_opoks_tx), ','||rv.rf_add_opoks_tx, null), ',')) t on 1 = 1
                         where review_id = r.review_id)) rv_opok;
        --
        -- ��������� ���� � ��������� ������
        --
        std.pkg_log.debug('��������� ���� � ��������� ������ #'||to_char(r.review_id)||'. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
        UPDATE /*+ NO_PARALLEL*/ kdd_review 
           SET rf_object_tp_cd =      case when trim(r.opok_list) is not null then r.object_tp_cd      else rf_object_tp_cd end,
               rf_trxn_scrty_seq_id = case when trim(r.opok_list) is not null then r.trxn_scrty_seq_id else rf_trxn_scrty_seq_id end,
               rf_repeat_nb = r.rv_repeat_nb + 1,
               rf_opok_nb =      case when var_set_opok_flag = 1 then var_opok_nb      else rf_opok_nb      end,
               rf_add_opoks_tx = case when var_set_opok_flag = 1 then var_add_opoks_tx else rf_add_opoks_tx end
         WHERE review_id = r.review_id;           
        --
        -- ���������� ��������� ������������ ������, ����:
        --  - ���������� �������� ��������/������ ������ 
        --  ���
        --  - ���������� ���������� ���� ���� � ����� ���������� ���� ���� ���������� �� ������������� � ������
        --  ���
        --  - ��������� ��� ������� � ������ (��������/��������� ������ ������)
        --  ���
        --  - ����� ��� ������������
        --
        if (r.rv_data_dump_dt <> var_data_dump_dt and -- ���������� �������� ��������/������ ������
            rf_pkg_review.get_trxn_change(par_op_cat_cd, par_trxn_seq_id, r.rv_trxn_scrty_seq_id, 
                                          to_date(null), /*������� ������ ��������*/
                                          r.rv_data_dump_dt /*������, ��������������� ����������� ���������*/) is not null) or
           r.rv_data_dump_dt is null or                                           
           var_opok_repeat_flag = 1 or                                               -- ���������� ���������� ���� ����
           nvl(r.object_tp_cd, r.rv_object_tp_cd) <> r.rv_object_tp_cd or            -- ��������� ��� ������� � ������
           r.rv_status_cd like '%-' or r.rv_status_cd like '%=' Then                 -- ����� ��� ������������

          std.pkg_log.debug('���������� ��������� ������������ ������ #'||to_char(r.review_id)||'. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
          var_review2_ids.DELETE;
          var_review2_ids.extend;
          var_review2_ids(1) := r.review_id; 

          rf_pkg_review.update_reviews(p_review_ids       => var_review2_ids,
                                       p_actvy_type_cd    => 'RF_REPEAT',
                                       p_new_owner_seq_id => null,
                                       p_new_owner_org    => null,
                                       p_due_dt           => null,
                                       p_opok_nb          => null,
                                       p_add_opoks_tx     => null,
                                       p_owner_seq_id     => par_owner_seq_id);

          rf_pkg_review.create_activities(p_review_ids    => var_review2_ids,
                                          p_actvy_type_cd => 'RF_REPEAT',
                                          p_cmmnt_id      => null,
                                          p_note_tx       => case when nvl(r.object_tp_cd, r.rv_object_tp_cd) in ('TRXN', 'TRXN_SCRTY_JOINT') and trim(r.opok_list) is not null
                                                                  then '��������� ������������ � ����� � ���������� �������� � ��������� �/��� ���������� ����� ����� ��������'
                                                                  when nvl(r.object_tp_cd, r.rv_object_tp_cd) in ('TRXN', 'TRXN_SCRTY_JOINT') and trim(r.opok_list) is null
                                                                  then '�� �������� �������� � ������������ � ��������� ��������� �������� �� �������� ���������'
                                                                  when nvl(r.object_tp_cd, r.rv_object_tp_cd) = 'TRXN_SCRTY' and trim(r.opok_list) is not null
                                                                  then '��������� ������������ � ����� � ���������� ������ ������ � ��������� �/��� ���������� ����� ����� ��������'
                                                                  when nvl(r.object_tp_cd, r.rv_object_tp_cd) = 'TRXN_SCRTY' and trim(r.opok_list) is null and r.new_scrty_count = 0
                                                                  then '�� �������� �������� � ������������ � ��������� ��������� ������ ������ �� �������� ���������'
                                                                  when nvl(r.object_tp_cd, r.rv_object_tp_cd) = 'TRXN_SCRTY' and trim(r.opok_list) is null and r.new_scrty_count <> 0
                                                                  then '������ � ����� � ���������� ������ ������ � ��������� �� �������� (��� ��������� �������� �� �������� ���������)'  
                                                             end,
                                          p_owner_seq_id  => par_owner_seq_id);        

          par_repeat_count := par_repeat_count + 1;                                
        end if;
      end if; -- if r.canceled_fl = 'Y' Then ... else ... 
    end if; -- if r.review_id is null Then ... else ... 
  END LOOP;
  --
  -- ���������� �������� ��� ��������� ������ �����
  --
  if var_review_ids is not null and var_review_ids.COUNT > 0 Then
    std.pkg_log.debug('���������� �������� ��� ��������� ������ �����. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);
    
    -- ��������� ������ � ����� �������, ��������������� �������� ��������� ��������
    rf_pkg_review.update_reviews(p_review_ids       => var_review_ids,
                                 p_actvy_type_cd    => 'RF_DELETE-',
                                 p_new_owner_seq_id => null,
                                 p_new_owner_org    => null,
                                 p_due_dt           => null,
                                 p_opok_nb          => null,
                                 p_add_opoks_tx     => null,
                                 p_owner_seq_id     => par_owner_seq_id);

    -- ������������ ������ �������� ��� ������� � ���������� ����������� �� ����
    rf_pkg_review.create_activities(p_review_ids    => var_review_ids,
                                    p_actvy_type_cd => 'RF_DELETE-',
                                    p_cmmnt_id      => null,
                                    p_note_tx       => '�������� � ����� � ��������� ������ ������ � �������� � ���������',
                                    p_owner_seq_id  => par_owner_seq_id);

    par_delete_count := par_delete_count + var_review_ids.COUNT;                                
  end if; 
  --
  -- ��������� ������� ������ �������� � ��������� � ��� ��������� � �������� �������.
  -- � ������ R(egister): ��������� ��������� �������� � ������� � �������� ������������ ��������
  -- � ������ E(ntirely): �������� ������������� ��� ��������� ��������
  --
  <<arh_and_return>>
  -- ��������� ��������� ���� �������� � ������������������� �� �������� ���� � ��������� ���� ������� �� ������ ��������  
  UPDATE /*+ NO_PARALLEL*/ mantas.kdd_review
     SET rf_data_dump_dt = var_data_dump_dt, 
         rf_trxn_dt = var_trxn_dt,
         rf_trxn_base_am = var_trxn_base_am
   WHERE rf_op_cat_cd = par_op_cat_cd and
         rf_trxn_seq_id = par_trxn_seq_id and
         rf_alert_type_cd = 'O';    

  if par_arh_mode = 'R' Then
    --
    -- ������� �������� � ������ ������������ ��������
    --
    std.pkg_log.debug('������������ �������� � ������� � �������� ������������ ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);

    INSERT INTO rf_arh_object(entity_cd, arh_dump_dt, entity_id)
           VALUES(par_op_cat_cd, var_data_dump_dt, par_trxn_seq_id);

  elsif par_arh_mode = 'E' Then
    std.pkg_log.debug('��������� ������� ������ �������� � ��������� � ��� ��������� � �������� �������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||' ...', prcname);

    rf_pkg_arh.archive_one(p_id => par_trxn_seq_id, 
                           p_entity => par_op_cat_cd, 
                           p_arh_dump_dt => var_data_dump_dt);         

    std.pkg_log.debug('��������� ��������� ������� ������ �������� � ��������� � ��� ��������� � �������� �������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id), prcname);
  end if; 

  std.pkg_log.debug('��������� ��������� �������� ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id), prcname);
  --
  -- ���� �� ���� ��������
  --
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else  
      raise_application_error(-20001, '������ ��� ��������� �������� (���������) ��������. ��������� ��������: '||par_op_cat_cd||'; ID ��������: '||to_char(par_trxn_seq_id)||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||prcname);
    end if;      
END run_opok_repeat;  
--
-- ���������� ��������� (������ ���� � ���������: ������� ��������)
-- ��������� �������������� �� ��� ����, �� ������� ���� ������ ����������, 
-- ��� ���� � ����� ������ �������������� ��������� �� �������, ���������� � ������� ����
--
PROCEDURE calc_calendar IS
  var_start_year      INTEGER;
  var_end_year        INTEGER;
BEGIN
  --
  -- ��������� �������� �����, �� ������� ������������ ���������
  --
  SELECT /*+ NO_PARALLEL*/
         LEAST(to_number(to_char(nvl(min(clndr_dt), sysdate), 'yyyy')), 
               to_number(to_char(sysdate, 'yyyy')) - 1),
         GREATEST(to_number(to_char(nvl(max(clndr_dt), sysdate), 'yyyy')), 
                  to_number(to_char(sysdate, 'yyyy')) + 1)  
    INTO var_start_year, var_end_year                            
    FROM kdd_cal_holiday
   WHERE clndr_nm = 'SYSCAL'; 
  --
  -- ������������ � ���������� ���������
  --
  DELETE /*+ NO_PARALLEL*/ FROM kdd_cal WHERE clndr_nm = 'SYSCAL';
  INSERT /*+ NO_PARALLEL*/ INTO kdd_cal(clndr_nm, clndr_dt, clndr_day_age, bus_day_fl, bus_day_age, day_of_wk, bus_day_type_cd)
    (SELECT /*+ NO_PARALLEL*/
            'SYSCAL' as clndr_nm,
            days.clndr_dt, 
            days.clndr_dt - trunc(sysdate) as clndr_day_age,
            case when hld.hldy_type_cd = 'C' -- ��������
                 then 'N' -- ���������
                 when hld.hldy_type_cd = 'W' -- ������� ���� (� ����� � ��������� ���������)
                 then 'Y' -- �������
                 when trim(upper(to_char(days.clndr_dt, 'Day', 'NLS_DATE_LANGUAGE=AMERICAN'))) in ('SATURDAY', 'SUNDAY') -- �������, �����������
                 then 'N' -- ���������  
                 else 'Y' -- ������� 
            end as bus_day_fl,
            days.clndr_dt - trunc(sysdate) as bus_day_age,
            case trim(upper(to_char(days.clndr_dt, 'Day', 'NLS_DATE_LANGUAGE=AMERICAN')))
              when 'MONDAY' then 1
              when 'TUESDAY' then 2
              when 'WEDNESDAY' then 3
              when 'THURSDAY' then 4
              when 'FRIDAY' then 5
              when 'SATURDAY' then 6
              when 'SUNDAY' then 7
            end as day_of_wk,    
            case when hld.hldy_type_cd = 'C' -- ��������
                 then 'C' -- Closed
                 when hld.hldy_type_cd = 'W' -- ������� ���� (� ����� � ��������� ���������)
                 then 'N' -- Normal
                 when trim(upper(to_char(days.clndr_dt, 'Day', 'NLS_DATE_LANGUAGE=AMERICAN'))) in ('SATURDAY', 'SUNDAY') -- �������, �����������
                 then 'C' -- Closed 
                 else 'N' -- Normal 
            end as bus_day_type_cd
       FROM (select to_date('01.01.'||to_char(var_start_year), 'dd.mm.yyyy') + level - 1 as clndr_dt 
               from dual
             connect by level <= to_date('31.12.'||to_char(var_end_year), 'dd.mm.yyyy') - to_date('01.01.'||to_char(var_start_year), 'dd.mm.yyyy') + 1) days
            left join kdd_cal_holiday hld on hld.clndr_nm = 'SYSCAL' and hld.clndr_dt = days.clndr_dt);
            
  COMMIT;          
END calc_calendar;            
--
-- �������� ��������������� �������, ������������ � �������� ����-��������/���������/����-��������� 
-- ��������� ������������� ��� ������ ����� ������� �������� ������. � ������ ������ ��������� ����������� exception.
-- ��������! ��������� ��������� TRUNCATE, ��������������, ���������� COMMIT.
--
PROCEDURE truncate_wrk_tables IS
  prcname                  VARCHAR2(255) := ' (mantas.rf_pkg_scnro_run.truncate_wrk_tables)';
BEGIN
  std.pkg_log.info('������� ��������������� �������: mantas.rf_kdd_review#wrk, mantas.rf_arh_object ...', prcname);
  -- ������ ����������� �������
  EXECUTE IMMEDIATE 'TRUNCATE TABLE mantas.rf_kdd_review#wrk';
  -- ������ ������������ ��������
  EXECUTE IMMEDIATE 'TRUNCATE TABLE mantas.rf_arh_object';
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.fatal('������ ��� ������� ��������������� ������: mantas.rf_kdd_review#wrk, mantas.rf_arh_object. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    RAISE;
END truncate_wrk_tables;
end rf_pkg_scnro_run;
/

GRANT EXECUTE on mantas.rf_pkg_scnro_run to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to KYC;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to STD;
GRANT EXECUTE on mantas.rf_pkg_scnro_run to KDD_ALG;

