begin 
  execute immediate
  'create global temporary table mantas.rf_id(column_value INTEGER) on commit delete rows';
exception  
  when others then
    null;
end;
/
    
create or replace TYPE mantas.RF_TAB_CLOB AS TABLE OF CLOB
/

drop TYPE mantas.RF_TAB_OES_CHECK
/
create or replace TYPE mantas.RF_TYPE_OES_CHECK AS OBJECT
(
  form_cd             VARCHAR2(64 CHAR),   -- ��� (�����) ����� ���, �������� 321
  oes_id              NUMBER(22),          -- ID ���, ������ � ����� ����� �������������� ���������� ���.
  msg_tx              VARCHAR2(2000 CHAR), -- ����� ��������� �� ������/��������������
  critical_fl         VARCHAR2(1),         -- ���� (Y/N) ����������� ������ ��������: Y - ������, N - ��������������
  field_nm            VARCHAR2(255 CHAR),  -- ������������ ���� ���, � ������� ������� ������ ��������
  field_cd            VARCHAR2(64 CHAR),   -- ��� ���� ���, � ������� ������� ������ ��������
  field_order_nb      INTEGER,             -- ���������� ����� ���� ���, � ������� ������� ������ ��������
  table_nm            VARCHAR2(64 CHAR),   -- ��� ������� � ��, ������ � �������� � ������� ����� �������������� ���� ��� ��������� �� ����������
  column_nm           VARCHAR2(64 CHAR),   -- ��� ������� � ������� ��, ������ � �������� � ������� ����� �������������� ���� ��� ��������� �� ����������
  block_cd            VARCHAR2(64 CHAR),   -- ��� �����, ������ � �������� � �������� �������������� ���� ��� ��������� �� ����������
  rule_seq_id         NUMBER(22)           -- ID ������� ��������, ��� ���������� �������� ���� ������������ ������ ��������� ��������
)
/

create or replace TYPE mantas.RF_TAB_OES_CHECK AS TABLE OF RF_TYPE_OES_CHECK
/

create or replace package mantas.RF_PKG_OES_CHECK IS
--
-- ��������� �������� ��� ���������� ���
-- ������� ������� � ���������� ������ ������ ��������� �������� ��� ���������� ��� � ������� RF_OES_CHECK
-- ������������ ��������:
--  W  - Warning (��������������)
--  E  - Error (������)
--  O  - OK (�� ������)
--
FUNCTION check_oes 
(
  par_form_cd          VARCHAR2,  -- ��� ����� ���, ��������: 321
  par_oes_id           NUMBER,    -- ID ���, ������ � ����� ����� �������������� ���������� ���
  par_commit_flag      INTEGER DEFAULT 1 -- ���� 0/1 - ������� �� ������ COMMIT
) 
RETURN VARCHAR2;  
--
-- ��������� �������� ������� �������������� ��� ��� ��������� �������
--
PROCEDURE check_oes_by_alerts 
(
  par_form_cd             IN   VARCHAR2,             -- ��� ����� ���, ��������: 321
  par_review_ids          IN   mantas.RF_TAB_NUMBER, -- ������ ��������������� �������, ��� ������� ���������� ��������� ���
  par_nonerr_oes_ids     OUT   mantas.RF_TAB_NUMBER, -- ���������: ������ ��������������� ���, ��������� �������� ��� ������ (��, ��������, � ����������������)
  par_nonerr_review_ids  OUT   mantas.RF_TAB_NUMBER, -- ���������: ������ ��������������� �������, ��������� ������� ������ �������� ��� ������ (��, ��������, � ����������������)
  par_oes_warn_count     OUT   INTEGER,              -- ���������: ���������� ���, ��������� �������� � ���������������� (�� ��� ������)
  par_oes_err_count      OUT   INTEGER,              -- ���������: ���������� ���, �� ��������� �������� (���� ������)
  par_oes_absent_count   OUT   INTEGER,              -- ���������: ���������� ������������� ��� (���������� �������, ����� �� ������ �� ������� ����� ������� �������������� ���)
  par_commit_flag         IN   INTEGER DEFAULT 1     -- ���� 0/1 - ������� �� ������ COMMIT
);
--
-- �������� �������� ��������� �������� ��� ���������� ��� � ������������ � ���������� ��������� ��������
--
FUNCTION check_oes_tab 
(
  par_form_cd           VARCHAR2,                       -- ��� ����� ���, ��������: 321
  par_oes_id            NUMBER,                         -- ID ���, ������ � ����� ����� �������������� ���������� ���
  par_tab_check_select  mantas.RF_TAB_CLOB DEFAULT null -- ������ �������� �������� ���
)
RETURN mantas.RF_TAB_OES_CHECK PIPELINED;
--
-- �������� ������ ��������� �������� ��� ���������� ��� � ������������ � ���������� ��������� ��������
--
FUNCTION get_check_list
(
  par_form_cd           VARCHAR2,                       -- ��� ����� ���, ��������: 321
  par_oes_id            NUMBER,                         -- ID ���, ������ � ����� ����� �������������� ���������� ���
  par_tab_check_select  mantas.RF_TAB_CLOB DEFAULT null -- ������ �������� �������� ���
)
RETURN CLOB PARALLEL_ENABLE;
--
-- ������������ ������ SELECT'�� �������� ��� ��� ��������� �����, �� ������ �� ������ ������� ��������.
-- ��� ������������� - ��������� ������� �� ������������ � ����������/����� � ��� ���� ������
-- ��������! ������� ����� ��������� COMMIT! (� ������ �������� ������)
--
FUNCTION get_tab_check_select
(
  par_form_cd          VARCHAR2 -- ��� ����� ���, ��� ������� ������������ ������� ��������
)
RETURN mantas.RF_TAB_CLOB PARALLEL_ENABLE;
--
-- ��������� ������� �������� (������ ����� �� �������������� ������������), ����������/����� ���� ������� ������, ��������/�������� ��������� �� ������
-- ��������! ������� ��������� COMMIT!
--
FUNCTION check_and_mark_rule 
(
  par_form_cd          VARCHAR2 DEFAULT null,                           -- ��� ����� ���, ��� ������� ��������� �������, ��������: 321
  par_rule_seq_id      rf_oes_check_rule.rule_seq_id%TYPE DEFAULT null  -- ������������� ���������� ������� ��������, ���� ������, �� ������ ��� ������� � �����������
)
RETURN INTEGER; -- ���������� ������������ ������ �������� � �������� (�������� ������ "���� ������?")
--
-- ��������� ������� �������� (������ ����� �� �������������� ������������), ���� ������� �� ������ �������� - ������� �������� ��������� �� ������
--
FUNCTION check_rule 
(
  par_rule_seq_id      rf_oes_check_rule.rule_seq_id%TYPE -- ������������� ���������� ������� ��������, ���� ������, �� ������ ��� ������� � �����������
)
RETURN VARCHAR2; -- ��������� �� ������, ���� ������� ������� ������ �������� - null
--
-- ������������ SELECT �������� ��� ���������� ���� (��� ���������� ��������, ��� �������� ����������) � ������������ � ��������� �������� � ������� RF_OES_CHECK_RULE
--
FUNCTION get_check_select
(
  par_form_cd          VARCHAR2, -- ��� ����� ���, ��� ������� ������������ ������ ��������
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'RUN' - ��� ���������� �������� ���, 'CHECK' - ��� �������� ���������� ������ ��������
  par_rule_seq_id      rf_oes_check_rule.rule_seq_id%TYPE -- ������������� ������� ��������, ��� �������� ������������ ������ ��������
)
RETURN CLOB PARALLEL_ENABLE;
--
-- �������� ���
--
PROCEDURE clear_cache;
end RF_PKG_OES_CHECK;
/

create or replace package body mantas.RF_PKG_OES_CHECK IS
--
-- ��� �������� ��������
--
TYPE TAB_TAB_CLOB_CACHE  IS TABLE OF mantas.RF_TAB_CLOB INDEX BY VARCHAR2(64 CHAR);
TYPE TAB_NUMBER_CACHE IS TABLE OF NUMBER INDEX BY VARCHAR2(64 CHAR);
pck_tab_check_select_cache  TAB_TAB_CLOB_CACHE; -- ������ �������� �������� ��������, ������� ���� - ��� �����
pck_tab_check_scn_cache     TAB_NUMBER_CACHE;   -- ������ ������������ scn �� ������ ����������� ������� �������� ��������, ���� - ��� �����
--
-- ��������� �������� ��� ���������� ���
-- ������� ������� � ���������� ������ ������ ��������� �������� ��� ���������� ��� � ������� RF_OES_CHECK
-- ������������ ��������:
--  W  - Warning (��������������)
--  E  - Error (������)
--  O  - OK (�� ������)
--  NULL - ��� ������ ���
--
FUNCTION check_oes 
(
  par_form_cd          VARCHAR2,  -- ��� ����� ���, ��������: 321
  par_oes_id           NUMBER,    -- ID ���, ������ � ����� ����� �������������� ���������� ���
  par_commit_flag      INTEGER DEFAULT 1 -- ���� 0/1 - ������� �� ������ COMMIT
) 
RETURN VARCHAR2 IS  
  var_tab_check_select  mantas.RF_TAB_CLOB;
  var_result            VARCHAR2(1 CHAR);
  
  var_dummy             INTEGER;
  prc_name              VARCHAR2(64) := ' (mantas.rf_pkg_oes_check.check_oes)';
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_form_cd) is null or par_oes_id is null Then     
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);
  end if;
  --
  -- ������� ���������� �� ������� ������ ������� ��������
  --
  var_tab_check_select := get_tab_check_select(par_form_cd);  
  --
  -- ��������� ������ ���, ����� ������������ ������ �� ����� �������� ���������� ��������
  -- (������ ��������� ��������� � ���������� ��� ���������� ������������ ���������)
  --
  if par_form_cd = '321' Then
    BEGIN
      SELECT oes_321_id
        INTO var_dummy
        FROM mantas.rf_oes_321
       WHERE oes_321_id = par_oes_id
      FOR UPDATE;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        raise_application_error(-20001, '������ �������������� ������������� ��� (321): '||to_char(par_oes_id)||prc_name);
    END;      
  end if;  
  --
  -- ������ ��� ������������ ��������� ��� ���������� ���
  --
  DELETE FROM mantas.rf_oes_check WHERE form_cd = par_form_cd and oes_id = par_oes_id;
  --
  -- ���� �������� ��� - ��������� �������� = �����
  --
  if var_tab_check_select.count = 0 Then
    var_result := 'O';
  else    
    --
    -- ������� � ������� ����� ��������� ��� ���������� ���
    --
    INSERT INTO mantas.rf_oes_check(check_seq_id, form_cd, oes_id, msg_tx, critical_fl, field_nm, field_cd, field_order_nb, 
                                    table_nm, column_nm, block_cd, rule_seq_id)
    SELECT mantas.rf_oes_check_seq.NextVal as check_seq_id,
           par_form_cd, par_oes_id, nvl(msg_tx, '������� �������� ��� �'||to_char(rule_seq_id)||' �� ������������ ����� ���������. ���������� � ���������� ��������������'), nvl(critical_fl, 'Y'), field_nm, field_cd, field_order_nb, 
           table_nm, column_nm, block_cd, rule_seq_id                                  
      FROM TABLE(mantas.rf_pkg_oes_check.check_oes_tab(par_form_cd, par_oes_id, var_tab_check_select));
    --
    -- ��������� �������� ��������� ��������
    --
    if SQL%ROWCOUNT = 0 Then  
      var_result := 'O';
    else
      SELECT decode(max(critical_fl), 'Y', 'E', 'W')
        INTO var_result
        FROM mantas.rf_oes_check 
       WHERE form_cd = par_form_cd and
             oes_id = par_oes_id;
    end if;         
  end if;         
  --
  -- ��������� ��������� � ���� �������� � ���
  --
  if par_form_cd = '321' Then
    UPDATE mantas.rf_oes_321
       SET check_dt     = sysdate,
           check_status = var_result
     WHERE oes_321_id = par_oes_id;
     
    -- ���� ��� ������ ��� - ���������� NULL
    if SQL%ROWCOUNT = 0 Then  
      var_result := null;
    end if;  
  end if;
  --
  -- COMMIT � �������
  --
  if par_commit_flag = 1 Then
    COMMIT;
  end if;  
  
  return var_result;
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      raise_application_error(-20001, '������ ��� �������� ��������� (���). ��� �����: '||par_form_cd||'; ID ���: '||to_char(par_oes_id)||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||prc_name);
    end if;
END check_oes;  
--
-- ��������� �������� ������� �������������� ��� ��� ��������� �������
--
PROCEDURE check_oes_by_alerts 
(
  par_form_cd             IN   VARCHAR2,             -- ��� ����� ���, ��������: 321
  par_review_ids          IN   mantas.RF_TAB_NUMBER, -- ������ ��������������� �������, ��� ������� ���������� ��������� ���
  par_nonerr_oes_ids     OUT   mantas.RF_TAB_NUMBER, -- ���������: ������ ��������������� ���, ��������� �������� ��� ������ (��, ��������, � ����������������)
  par_nonerr_review_ids  OUT   mantas.RF_TAB_NUMBER, -- ���������: ������ ��������������� �������, ��������� ������� ������ �������� ��� ������ (��, ��������, � ����������������)
  par_oes_warn_count     OUT   INTEGER,              -- ���������: ���������� ���, ��������� �������� � ���������������� (�� ��� ������)
  par_oes_err_count      OUT   INTEGER,              -- ���������: ���������� ���, �� ��������� �������� (���� ������)
  par_oes_absent_count   OUT   INTEGER,              -- ���������: ���������� ������������� ��� (���������� �������, ����� �� ������ �� ������� ����� ������� �������������� ���)
  par_commit_flag         IN   INTEGER DEFAULT 1     -- ���� 0/1 - ������� �� ������ COMMIT
) IS
  
  const_parallel_count   INTEGER := 1000; -- ���������� �������, ������� � �������� �� ��������� ����������� � ������������ ������

  var_oes_check      mantas.RF_TYPE_OES_CHECK := mantas.RF_TYPE_OES_CHECK(null, null, null, null, null, null, null, null, null, null,
                                                                          null);
  var_tab_check_select   mantas.RF_TAB_CLOB;
  var_sql                CLOB;

  TYPE c_oes_by_alerts_type IS REF CURSOR;
  c_oes_by_alerts    c_oes_by_alerts_type;
  
  var_review_id          mantas.kdd_review.review_id%TYPE;
  var_oes_id             NUMBER(22);
  var_check_status_old   VARCHAR2(1 CHAR);
  var_check_status_new   VARCHAR2(1 CHAR);
  var_check_list_new     CLOB;

  var_nonerr_oes_ids     mantas.RF_TAB_NUMBER := mantas.RF_TAB_NUMBER();
  var_nonerr_review_ids  mantas.RF_TAB_NUMBER := mantas.RF_TAB_NUMBER();
  var_oes_warn_count     INTEGER := 0;
  var_oes_err_count      INTEGER := 0;
  var_oes_absent_count   INTEGER := 0;

  prc_name           VARCHAR2(64) := ' (mantas.rf_pkg_oes_check.check_oes_by_alerts)';
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_form_cd) is null or par_form_cd not in ('321') or 
     par_review_ids is null or par_review_ids.COUNT = 0 Then     
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);
  end if;
  --
  -- ������� ���������� �� ������� ������ ������� ��������
  --
  var_tab_check_select := get_tab_check_select(par_form_cd);  
  --
  -- �������� ����� �������, ������������� �� ����� ������ �� ������ �����, 
  -- ��� ���� ��� ������ ������������ ����������� ���, ������� � ����� ���������� ��� ��������;
  --
  -- ���� �������� �������� ���, ������ ��� �� ������ ������������ ������� ��������, ��������� ������ 
  -- ������� ���������� ������, ������������ ��� ������� ��� ������� �������� ��������
  --     
  if par_form_cd = '321' Then
    --
    -- ���� ������� �� ������� ����� - ��������� � ���� �����
    --
    if var_tab_check_select.count > 0 and par_review_ids.count < const_parallel_count Then 

      var_sql := 'SELECT /*+ ORDERED USE_NL(oes) INDEX(oes RF_OES321_REVIEW_FK_I)*/ '||
                        'ids.column_value as review_id, oes.oes_321_id as oes_id, oes.check_status, '||
                        'case when oes.oes_321_id is null '||
                             'then to_clob(null) '||
                             'else mantas.rf_pkg_oes_check.get_check_list(''321'', oes.oes_321_id, :var_tab_check_select) '||
                        'end as check_list '||
                   'FROM (select distinct column_value from table(:par_review_ids)) ids '||      
                        'left join mantas.rf_oes_321 oes on oes.review_id = ids.column_value and '||
                                                           'oes.current_fl = ''Y'' and '||
                                                           'nvl(oes.send_fl, ''N'') <> ''Y'' and '||
                                                           'nvl(oes.deleted_fl, ''N'') <> ''Y''';
      
    elsif var_tab_check_select.count > 0 and par_review_ids.count >= const_parallel_count Then
      --
      -- ��������� �������������� ������� � ������� - ��� ����� ��� ����������������� �������
      --
      DELETE FROM mantas.rf_id;
      INSERT INTO mantas.rf_id SELECT distinct column_value FROM table(par_review_ids);
      --
      -- ��������� ������ � ������������ ������
      --
      var_sql := 'SELECT /*+ PARALLEL(16) ORDERED USE_NL(oes) INDEX(oes RF_OES321_REVIEW_FK_I)*/ '||
                        'ids.column_value as review_id, oes.oes_321_id as oes_id, oes.check_status, '||
                        'case when oes.oes_321_id is null '||
                             'then to_clob(null) '||
                             'else mantas.rf_pkg_oes_check.get_check_list(''321'', oes.oes_321_id, :var_tab_check_select) '||
                        'end as check_list '||
                   'FROM mantas.rf_id ids '||
                        'left join mantas.rf_oes_321 oes on oes.review_id = ids.column_value and '||
                                                           'oes.current_fl = ''Y'' and '||
                                                           'nvl(oes.send_fl, ''N'') <> ''Y'' and '||
                                                           'nvl(oes.deleted_fl, ''N'') <> ''Y''';
    else
      var_sql := 'SELECT /*+ ORDERED USE_NL(oes) INDEX(oes RF_OES321_REVIEW_FK_I)*/ '||
                        'ids.column_value as review_id, oes.oes_321_id as oes_id, oes.check_status, '||
                        'to_clob(null) as check_list '||
                   'FROM (select distinct column_value from table(:par_review_ids)) ids '||
                        'left join mantas.rf_oes_321 oes on oes.review_id = ids.column_value and '||
                                                           'oes.current_fl = ''Y'' and '||
                                                           'nvl(oes.send_fl, ''N'') <> ''Y'' and '||
                                                           'nvl(oes.deleted_fl, ''N'') <> ''Y''';
    end if;                        
  end if;                        
  --
  -- ��������� ������, ���� �� �������
  --  
  if var_tab_check_select.count > 0 and par_review_ids.count < const_parallel_count Then 
    OPEN c_oes_by_alerts FOR var_sql USING var_tab_check_select, par_review_ids;
  elsif var_tab_check_select.count > 0 and par_review_ids.count >= const_parallel_count Then
    OPEN c_oes_by_alerts FOR var_sql USING var_tab_check_select;
  else
    OPEN c_oes_by_alerts FOR var_sql USING par_review_ids;
  end if;
    
  LOOP
    FETCH c_oes_by_alerts INTO var_review_id, var_oes_id, var_check_status_old, var_check_list_new;
    EXIT WHEN c_oes_by_alerts%NOTFOUND;
    --
    -- ���� � ������ ��� ��� - ����������� ������� ������������� ���
    --
    if var_oes_id is null Then
      var_oes_absent_count := var_oes_absent_count + 1;
    else 
      -- 
      -- ���� ���� �������� ��� ������ ���� �������� - ���������� ���������� (���� ������� �� ���� � ��� - ������ �� ������)
      --
      if nvl(var_check_status_old, '?') <> 'O' or to_char(substr(var_check_list_new, 1, 1)) is not null Then
        --
        -- ������ ��� ������������ ��������� ��� ������� ���
        --
        DELETE FROM mantas.rf_oes_check WHERE form_cd = par_form_cd and oes_id = var_oes_id;
        --
        -- ������� ����� ��������� ��� ���������� ���
        --        
        if to_char(substr(var_check_list_new, 1, 1)) is not null Then
          INSERT INTO mantas.rf_oes_check(check_seq_id, form_cd, oes_id, msg_tx, critical_fl, field_nm, field_cd, field_order_nb, 
                                          table_nm, column_nm, block_cd, rule_seq_id)
          SELECT mantas.rf_oes_check_seq.NextVal as check_seq_id,
                 par_form_cd as form_cd, 
                 var_oes_id as oes_id, 
                 trim(regexp_substr(chk.column_value, '[^\~]+', 1, 5)) as msg_tx,
                 trim(regexp_substr(chk.column_value, '[^\~]+', 1, 2)) as critical_fl,
                 f.field_nm,
                 f.field_cd,
                 f.field_order_nb,
                 f.table_nm,
                 f.column_nm,
                 f.res_block_cd as block_cd, 
                 to_number(trim(regexp_substr(chk.column_value, '[^\~]+', 1, 1))) as rule_seq_id
            FROM TABLE(mantas.rf_pkg_scnro.list_to_tab(replace(var_check_list_new, '~', ' ~ '), '|')) chk
                 left join mantas.rf_oes_field f on f.form_cd = par_form_cd and
                                                    f.column_cd = trim(regexp_substr(chk.column_value, '[^\~]+', 1, 3)) and
                                                    nvl(f.block_cd, '#!NULL!#') = nvl(trim(regexp_substr(chk.column_value, '[^\~]+', 1, 4)), '#!NULL!#');

          SELECT decode(max(critical_fl), 'Y', 'E', 'W')
            INTO var_check_status_new
            FROM mantas.rf_oes_check 
           WHERE form_cd = par_form_cd and
                 oes_id = var_oes_id;        
        else
          var_check_status_new := 'O';
        end if;         
        --
        -- ��������� ��������� � ���� �������� � ���
        --
        if par_form_cd = '321' Then
          UPDATE mantas.rf_oes_321
             SET check_dt     = sysdate,
                 check_status = var_check_status_new
           WHERE oes_321_id = var_oes_id;
        end if;
      else
        var_check_status_new := 'O';
      end if; -- if nvl(var_check_status_old, '?') <> 'O' or to_char(substr(var_check_list_new, 1, 1)) is not null ...
      --
      -- ���� ��������� ������ ��������, ��������� ��� � ����� � �������
      --
      if var_check_status_new <> 'E' Then
        var_nonerr_review_ids.extend;
        var_nonerr_review_ids(var_nonerr_review_ids.count) := var_review_id;

        var_nonerr_oes_ids.extend;
        var_nonerr_oes_ids(var_nonerr_oes_ids.count) := var_oes_id;
      else
        var_oes_err_count := var_oes_err_count + 1;
      end if;

      if var_check_status_new = 'W' Then
        var_oes_warn_count := var_oes_warn_count + 1;
      end if;
    end if; -- if var_oes_id is null ... else ...
  END LOOP;
  CLOSE c_oes_by_alerts; 
  --
  -- COMMIT, �������
  -- 
  par_nonerr_oes_ids    := var_nonerr_oes_ids;
  par_nonerr_review_ids := var_nonerr_review_ids;
  par_oes_warn_count    := var_oes_warn_count;
  par_oes_err_count     := var_oes_err_count;
  par_oes_absent_count  := var_oes_absent_count; 
  
  if par_commit_flag = 1 Then
    COMMIT; 
  end if;  
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      raise_application_error(-20001, '������ ��� �������� ��������� (���), ��������� � ��������. '||
                                      case when par_review_ids.COUNT = 1
                                           then '����� ������: '||to_char(par_review_ids(1))
                                           else '���������� �������: '||to_char(par_review_ids.COUNT)
                                      end||'. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||prc_name);
    end if;
END check_oes_by_alerts;
--
-- �������� �������� ��������� �������� ��� ���������� ��� � ������������ � ���������� ��������� ��������
--
FUNCTION check_oes_tab 
(
  par_form_cd           VARCHAR2,                       -- ��� ����� ���, ��������: 321
  par_oes_id            NUMBER,                         -- ID ���, ������ � ����� ����� �������������� ���������� ���
  par_tab_check_select  mantas.RF_TAB_CLOB DEFAULT null -- ������ �������� �������� ���
)
RETURN mantas.RF_TAB_OES_CHECK PIPELINED IS

  var_oes_check  mantas.RF_TYPE_OES_CHECK := mantas.RF_TYPE_OES_CHECK(null, null, null, null, null, null, null, null, null, null,
                                                                      null);
  var_tab_check_select  mantas.RF_TAB_CLOB;
  var_clob              CLOB;
  var_sql               CLOB;

  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_oes_check.check_oes_tab)';
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_form_cd) is null or par_oes_id is null Then     
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);
  end if;
  --
  -- ������� ������ �������� ��������, ���� �� �� ������ �� ���������
  --
  if par_tab_check_select is null Then
    var_tab_check_select := get_tab_check_select(par_form_cd);  
  else   
    var_tab_check_select := par_tab_check_select;
  end if;  
  --
  -- ���� �������� �������� ��� - �����
  --
  if var_tab_check_select.count = 0 Then
    return;
  end if;  
  --
  -- ������� CLOB �� ������� ��������� �������� ��� ���������� ���
  --
  var_clob := get_check_list(par_form_cd, par_oes_id, var_tab_check_select);
  --
  -- ���� ������ �� ���� - ��������� ������, ���������� � �������
  --
  if var_clob is not null and to_char(substr(var_clob, 1, 1)) is not null Then
    --
    -- ��������� ������, ���������� �� ����� ������ �� ������ ��������� ��������, ������������ � ������� RF_OES_CHECK
    --
    var_oes_check.form_cd := par_form_cd;
    var_oes_check.oes_id  := par_oes_id;

    FOR r IN (SELECT /*+ NO_PARALLEL */ 
                     trim(regexp_substr(chk.column_value, '[^\~]+', 1, 5)) as msg_tx,  
                     trim(regexp_substr(chk.column_value, '[^\~]+', 1, 2)) as critical_fl, 
                     f.field_nm, 
                     f.field_cd, 
                     f.field_order_nb, 
                     f.table_nm, 
                     f.column_nm, 
                     f.res_block_cd as block_cd,  
                     to_number(trim(regexp_substr(chk.column_value, '[^\~]+', 1, 1))) as rule_seq_id 
                FROM table(mantas.rf_pkg_scnro.list_to_tab(replace(var_clob, '~', ' ~ '), '|')) chk  
                     left join mantas.rf_oes_field f on f.form_cd = trim(par_form_cd) and 
                                                        f.column_cd = trim(regexp_substr(chk.column_value, '[^\~]+', 1, 3)) and 
                                                        nvl(f.block_cd, '#!NULL!#') = nvl(trim(regexp_substr(chk.column_value, '[^\~]+', 1, 4)), '#!NULL!#')) LOOP

      var_oes_check.msg_tx         := r.msg_tx;
      var_oes_check.critical_fl    := r.critical_fl;
      var_oes_check.field_nm       := r.field_nm; 
      var_oes_check.field_cd       := r.field_cd;
      var_oes_check.field_order_nb := r.field_order_nb;
      var_oes_check.table_nm       := r.table_nm;
      var_oes_check.column_nm      := r.column_nm;
      var_oes_check.block_cd       := r.block_cd;
      var_oes_check.rule_seq_id    := r.rule_seq_id;

      PIPE ROW(var_oes_check);
    END LOOP;
  end if;
END check_oes_tab;
--
-- �������� ������ ��������� �������� ��� ���������� ��� � ������������ � ���������� ��������� ��������
--
FUNCTION get_check_list
(
  par_form_cd           VARCHAR2,                       -- ��� ����� ���, ��������: 321
  par_oes_id            NUMBER,                         -- ID ���, ������ � ����� ����� �������������� ���������� ���
  par_tab_check_select  mantas.RF_TAB_CLOB DEFAULT null -- ������ �������� �������� ���
)
RETURN CLOB PARALLEL_ENABLE IS
  var_tab_check_select  mantas.RF_TAB_CLOB;

  TYPE c_oes_check_type IS REF CURSOR;
  c_oes_check           c_oes_check_type;

  var_oes_id            NUMBER;
  var_clob              CLOB;
  var_result            CLOB;

  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_oes_check.get_check_list)';
BEGIN
  --
  -- �������� ����������
  --
  if trim(par_form_cd) is null or par_oes_id is null Then     
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);
  end if;
  --
  -- ������� ������ �������� ��������, ���� �� �� ������ �� ���������
  --
  if par_tab_check_select is null Then
    var_tab_check_select := get_tab_check_select(par_form_cd); 
  else   
    var_tab_check_select := par_tab_check_select;
  end if;  
  --
  -- ���� �������� �������� ��� - ���������� null
  --
  if var_tab_check_select.count = 0 Then
    return null;
  end if;  
  --
  -- ��������� ������� ��������, ���������� �������� ������ ���������
  -- ���� �� �������� �������� ���
  --
  FOR i IN 1..var_tab_check_select.count LOOP    
    --
    -- ���� ����� ������� �� ���� - ��������� ���
    --
    if var_tab_check_select(i) is not null and to_char(substr(var_tab_check_select(i), 1, 1)) is not null Then

      OPEN c_oes_check FOR var_tab_check_select(i) USING par_oes_id;
      LOOP
        FETCH c_oes_check INTO var_oes_id, var_clob;        
        EXIT; -- ������ ������ ���������� �� ����� ����� ������ - ����� �� ������ ��������
      END LOOP;
      CLOSE c_oes_check; 
      --
      -- ��������� ��������� ��������� �������� � ������
      --
      if to_char(substr(var_clob, 1, 1)) is not null Then
        var_result := var_result||'|'||var_clob; 
      end if;  
    end if;
  END LOOP;  
    
  var_result := substr(var_result, 2);
    
  return var_result;
END get_check_list;  
--
-- ������������ ������ SELECT'�� �������� ��� ��� ��������� �����, �� ������ �� ������ ������� ��������.
-- ��� ������������� - ��������� ������� �� ������������ � ����������/����� � ��� ���� ������
-- ��������! ������� ����� ��������� COMMIT! (� ������ �������� ������)
--
FUNCTION get_tab_check_select
(
  par_form_cd          VARCHAR2 -- ��� ����� ���, ��� ������� ������������ ������� ��������
)
RETURN mantas.RF_TAB_CLOB PARALLEL_ENABLE IS

  var_scn               NUMBER;
  var_tab_check_select  mantas.RF_TAB_CLOB := mantas.RF_TAB_CLOB();
  var_count             INTEGER;

  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_oes_check.get_tab_check_select)';
BEGIN
  --
  -- �������� ���������� ���������
  --
  if trim(par_form_cd) is null Then
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);    
  end if;   
  --
  -- ���������: �� ���������� �� ������� �������� ��� ������ ����� ����� ���������� ����������� �������� ��������, 
  -- ���� �� ���������� - �������� ������� �� ����
  --
  SELECT nvl(max(ora_rowscn), -1)
    INTO var_scn
    FROM mantas.rf_oes_check_rule
   WHERE form_cd = par_form_cd; 
   
  if pck_tab_check_scn_cache.EXISTS(par_form_cd) and
     pck_tab_check_scn_cache(par_form_cd) = var_scn and
     pck_tab_check_select_cache.EXISTS(par_form_cd) Then

    var_tab_check_select := pck_tab_check_select_cache(par_form_cd);
  else
    --
    -- �������� ������� �������� ���, ������� ���������
    --
    var_count := check_and_mark_rule(par_form_cd);
    --
    -- �������� ������� �������� ��� - �� ������ �� ������ �������, �������� � ���
    --  
    var_tab_check_select.delete;
    FOR r IN (SELECT mantas.rf_pkg_oes_check.get_check_select(par_form_cd, 'RUN', rule_seq_id) as sql_tx
                FROM mantas.rf_oes_check_rule                 
               WHERE form_cd = par_form_cd and
                     active_fl = 'Y' and
                     err_fl = 'N'
              ORDER BY rule_seq_id) LOOP
       var_tab_check_select.extend;       
       var_tab_check_select(var_tab_check_select.count) := r.sql_tx;              
    END LOOP;
                     
    pck_tab_check_scn_cache(par_form_cd) := var_scn;
    pck_tab_check_select_cache(par_form_cd) := var_tab_check_select;
  end if;

  return var_tab_check_select;
END get_tab_check_select;  
--
-- ��������� ������� �������� (������ ����� �� �������������� ������������), ����������/����� ���� ������� ������, ��������/�������� ��������� �� ������
-- ��������! ������� ��������� COMMIT!
--
FUNCTION check_and_mark_rule 
(
  par_form_cd          VARCHAR2 DEFAULT null,                           -- ��� ����� ���, ��� ������� ��������� �������, ��������: 321
  par_rule_seq_id      rf_oes_check_rule.rule_seq_id%TYPE DEFAULT null  -- ������������� ���������� ������� ��������, ���� ������, �� ������ ��� ������� � �����������
)
RETURN INTEGER IS -- ���������� ������������ ������ �������� � �������� (�������� ������ "���� ������?")
  PRAGMA AUTONOMOUS_TRANSACTION;
  
  var_count         INTEGER := 0;
  var_change_flag   INTEGER := 0;
BEGIN
  --
  -- ���������/������ ��� ������ ��������� ����� ������� ������ ��� ����������� �/��� �������� ������ ����������� �������
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/ 
                   rule_seq_id,
                   err_mess_tx,
                   err_fl,
                   mantas.rf_pkg_oes_check.check_rule(rule_seq_id) as new_err_mess_tx
              FROM mantas.rf_oes_check_rule
             WHERE form_cd = nvl(par_form_cd, form_cd) and
                   rule_seq_id = nvl(par_rule_seq_id, rule_seq_id)) LOOP
    
    if nvl(r.err_mess_tx, '$#!') = nvl(r.new_err_mess_tx, '$#!') and
       r.err_fl = case when r.new_err_mess_tx is null then 'N' else 'Y' end Then
      null;
    else   
      UPDATE rf_oes_check_rule t
         SET t.err_fl = case when r.new_err_mess_tx is null then 'N' else 'Y' end,
             t.err_mess_tx = r.new_err_mess_tx
       WHERE t.rule_seq_id = r.rule_seq_id;
       
      var_change_flag := 1; 
    end if;
     
     if r.new_err_mess_tx is not null Then
       var_count := var_count + 1;
     end if;
  END LOOP;
  --
  -- Commit � �������
  --
  if var_change_flag = 1 Then
    COMMIT;
  end if;
  
  RETURN var_count;
END check_and_mark_rule;
--
-- ��������� ������� �������� (������ ����� �� �������������� ������������), ���� ������� �� ������ �������� - ������� �������� ��������� �� ������
--
FUNCTION check_rule 
(
  par_rule_seq_id      rf_oes_check_rule.rule_seq_id%TYPE -- ������������� ���������� ������� ��������, ���� ������, �� ������ ��� ������� � �����������
)
RETURN VARCHAR2 IS -- ��������� �� ������, ���� ������� ������� ������ �������� - null
  var_form_cd    mantas.rf_oes_check_rule.form_cd%TYPE;
  var_err_msg    VARCHAR2(2000 CHAR) := null;
  var_sql        CLOB;
BEGIN
  --
  -- �������� ������������� �������, ������� ��� �����
  --
  SELECT /*+ NO_PARALLEL*/ 
         max(form_cd)
    INTO var_form_cd
    FROM mantas.rf_oes_check_rule
   WHERE rule_seq_id = par_rule_seq_id;
   
  if var_form_cd is null Then
    raise_application_error(-20001, '������ �������������� ������������� ������� ��������: '||to_char(par_rule_seq_id)||' (mantas.rf_pkg_oes_check.check_rule)');
  end if;  
  --
  -- ��������, ��� � ������� ������� ������������ �����
  --
  SELECT /*+ NO_PARALLEL*/
         listagg('����: '||trim(b1.column_value)||' ����������� � ����� ���', '; ') within group(order by trim(b1.column_value)) 
    INTO var_err_msg
    FROM mantas.rf_oes_check_rule rl
         join table(mantas.rf_pkg_scnro.list_to_tab(rl.block_list_tx)) b1 on 1 = 1
         left join (select distinct block_cd
                      from mantas.rf_oes_field 
                     where form_cd = var_form_cd and 
                           block_cd is not null) b2 on b2.block_cd = trim(b1.column_value)
   WHERE rl.rule_seq_id = par_rule_seq_id and
         upper(trim(rl.block_list_tx)) <> 'ALL' and
         b2.block_cd is null;

  if var_err_msg is not null Then
    return var_err_msg;
  end if;  
  --
  -- ��������, ��� � ������� ������� ������������ �������
  --
  SELECT /*+ NO_PARALLEL*/
         listagg(case when coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                         then to_char(null)
                                         else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                    end,
                                    trim(b1.column_value), b2.block_cd) is null
                      then '�������: '||substr(trim(col.column_value), instr(trim(col.column_value),'.') + 1)||' ����������� � ����� ���'
                      else '�������: '||substr(trim(col.column_value), instr(trim(col.column_value),'.') + 1)||' ����������� � �����: '||
                           coalesce(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1), trim(b1.column_value), b2.block_cd)
                 end, '; ') 
                 within group(order by coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                                     then to_char(null)
                                                     else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                                end,
                                                trim(b1.column_value), b2.block_cd) nulls first, 
                                       substr(trim(col.column_value), instr(trim(col.column_value),'.') + 1)) 
    INTO var_err_msg
    FROM mantas.rf_oes_check_rule rl
         join table(mantas.rf_pkg_scnro.list_to_tab(rl.col_list_tx)) col on 1 = 1
         left join table(mantas.rf_pkg_scnro.list_to_tab(rl.block_list_tx)) b1 on upper(trim(rl.block_list_tx)) <> 'ALL'and 
                                                                                  instr(col.column_value, '.') = 0
         left join (select distinct block_cd
                      from mantas.rf_oes_field 
                     where form_cd = var_form_cd and 
                           block_cd is not null) b2 on upper(trim(rl.block_list_tx)) = 'ALL' and 
                                                       instr(col.column_value, '.') = 0
         left join mantas.rf_oes_field f on f.form_cd = rl.form_cd and 
                                            f.column_cd = substr(trim(col.column_value), instr(trim(col.column_value),'.') + 1) and
                                            nvl(f.block_cd, '#!NULL!#') = coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                                                                        then '#!NULL!#'
                                                                                        else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                                                                   end,
                                                                                   trim(b1.column_value), b2.block_cd, '#!NULL!#') 
   WHERE rl.rule_seq_id = par_rule_seq_id and
         f.form_cd is null;

  if var_err_msg is not null Then
    return var_err_msg;
  end if;  
  --
  -- ������� ������ �������� ������� �� �������������� ������������
  --
  var_sql := get_check_select(par_form_cd => var_form_cd, par_query_type => 'CHECK', par_rule_seq_id => par_rule_seq_id);
  --
  -- �������� ������, ���� ��������� ������ - ������� ��������� ���������
  --
  BEGIN
    EXECUTE IMMEDIATE var_sql;
  EXCEPTION
    WHEN OTHERS THEN
      var_err_msg := SQLERRM;
  END;

  return var_err_msg;
END check_rule;
--
-- ������������ SELECT �������� ��� ���������� ���� (��� ���������� ��������, ��� �������� ����������) � ������������ � ��������� �������� � ������� RF_OES_CHECK_RULE
--
FUNCTION get_check_select
(
  par_form_cd          VARCHAR2, -- ��� ����� ���, ��� ������� ������������ ������ ��������
  par_query_type       VARCHAR2, -- ����� ������������ SELECT: 'RUN' - ��� ���������� �������� ���, 'CHECK' - ��� �������� ���������� ������ ��������
  par_rule_seq_id      rf_oes_check_rule.rule_seq_id%TYPE -- ������������� ������� ��������, ��� �������� ������������ ������ ��������
)
RETURN CLOB PARALLEL_ENABLE IS 
  var_sql              CLOB;
  var_expr             CLOB;

  prc_name       VARCHAR2(64) := ' (mantas.rf_pkg_oes_check.get_check_select)';
BEGIN
  --
  -- �������� ���������� ���������
  --
  if par_form_cd is null or par_form_cd not in ('321') or 
     par_query_type is null or par_query_type not in ('RUN', 'CHECK') or
     par_rule_seq_id is null Then
    raise_application_error(-20001, '�������� �������� ���������'||prc_name);    
  end if;
  --
  -- ����������� ������ �������� ����:
  -- SELECT <ID OES>,
  --        CASE WHEN <SQL �������� �� ������� �������� 1 ��� ������� 1, ����� 1>
  --             THEN '<ID ������� 1>~<���� �����������>~<��� ������� 1>~<��� ����� 1>~'||<SQL ��������� �� ������� �������� 1 ��� ������� 1>
  --        END||'|'
  --        CASE WHEN <SQL �������� �� ������� �������� 1 ��� ������� 2, ����� 1>
  --             THEN '<ID ������� 1>~<���� �����������>~<��� ������� 2>~<��� ����� 1>~'||<SQL ��������� �� ������� �������� 1 ��� ������� 2>
  --        END||'|'
  --        CASE WHEN <SQL �������� �� ������� �������� 1 ��� ������� 1, ����� 2>
  --             THEN '<ID ������� 1>~<���� �����������>~<��� ������� 1>~<��� ����� 2>~'||<SQL ��������� �� ������� �������� 1 ��� ������� 1>
  --        END||'|'
  --        ...
  --        CASE WHEN <SQL �������� �� ������� �������� 2 ��� ������� 1, ����� 1>
  --             THEN '<ID ������� 2>~<���� �����������>~<��� ������� 1>~<��� ����� 1>~'||<SQL ��������� �� ������� �������� 2 ��� ������� 1>
  --        END||'|'
  --         ...
  --        /* �� ���������� ����� ��� ������ ������ ����������� - trim | � replace || �� | */
  --   FROM <������� ��� � ����������� �� par_form_cd>
  --  WHERE oes_id = :par_oes_id
  --
  -- ������ ��� �������� ���������� (par_query_type = 'CHECK') �������� ���� WHERE c �������� 1 <> 1 � ������� "�������" ��������
  -- SELECT count(*) FROM (<������ �������� � WHERE 1 <> 1>)
  --
  -- ���� �� �������� �������� �������� � ������������ � ����������� - ����������� ��������� ��� select-list'�
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/
                   to_clob('')||'to_clob(CASE WHEN '||
                     replace(replace(replace(replace(replace(replace(replace(rl.check_sql_tx,
                       '#FIELD_SQL#',        f.field_sql_tx),
                       '##.',                nullif(coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                                                  then to_char(null)
                                                                  else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                                             end,
                                                             trim(b1.column_value), b2.block_cd)||'.', '.')),
                       '#BLOCK#',            ''''||coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                                                 then to_char(null)
                                                                 else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                                            end,
                                                            trim(b1.column_value), b2.block_cd)||''''),
                       '#COLUMN_NM#',        ''''||f.column_nm||''''),
                       '#FIELD_CODE#',       f.field_cd),
                       '#FIELD_NAME#',       f.field_nm),
                       '#FIELD_MAX_LENGTH#', to_char(f.field_max_length))||
                     ' THEN '||
                     ''''||to_char(rl.rule_seq_id)||'~'||rl.critical_fl||'~'||
                     substr(trim(col.column_value), instr(trim(col.column_value),'.') + 1)||'~'||
                     coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                   then to_char(null)
                                   else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                              end,
                              trim(b1.column_value), b2.block_cd)||'~''||'||
                     replace(replace(replace(replace(replace(replace(replace(rl.msg_sql_tx,
                       '#FIELD_SQL#',        f.field_sql_tx),
                       '##.',                nullif(coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                                                  then to_char(null)
                                                                  else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                                             end,
                                                             trim(b1.column_value), b2.block_cd)||'.', '.')),
                       '#BLOCK#',            ''''||coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                                                 then to_char(null)
                                                                 else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                                            end,
                                                            trim(b1.column_value), b2.block_cd)||''''),
                       '#COLUMN_NM#',        ''''||f.column_nm||''''),
                       '#FIELD_CODE#',       f.field_cd),
                       '#FIELD_NAME#',       f.field_nm),
                       '#FIELD_MAX_LENGTH#', to_char(f.field_max_length))||' END)' as expr
              FROM mantas.rf_oes_check_rule rl
                   left join table(mantas.rf_pkg_scnro.list_to_tab(rl.col_list_tx)) col on 1 = 1
                   left join table(mantas.rf_pkg_scnro.list_to_tab(rl.block_list_tx)) b1 on upper(trim(rl.block_list_tx)) <> 'ALL' and 
                                                                                            instr(col.column_value, '.') = 0
                   left join (select distinct block_cd
                                from mantas.rf_oes_field
                               where form_cd = par_form_cd and
                                     block_cd is not null) b2 on upper(trim(rl.block_list_tx)) = 'ALL' and 
                                                                 instr(col.column_value, '.') = 0
                   left join mantas.rf_oes_field f on f.form_cd = par_form_cd and 
                                                      f.column_cd = substr(trim(col.column_value), instr(trim(col.column_value),'.') + 1) and
                                                      nvl(f.block_cd, '#!NULL!#') = coalesce(case when upper(substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)) = 'OES'
                                                                                                  then '#!NULL!#'
                                                                                                  else substr(trim(col.column_value), 1, instr(trim(col.column_value),'.') - 1)
                                                                                             end,
                                                                                             trim(b1.column_value), b2.block_cd, '#!NULL!#')                 
             WHERE rl.form_cd = par_form_cd and
                   /* ��� ���������� �������� ��� (RUN) ������� ������ ������� ������� 
                    * ��� ���������� �������� ������������ ������� (CHECK) ������� ����� ������� */ 
                   rl.active_fl = decode(par_query_type, 'RUN', 'Y', rl.active_fl) and
                   rl.rule_seq_id = nvl(par_rule_seq_id, rl.rule_seq_id) and
                   (rl.err_fl = 'N' or par_query_type = 'CHECK')    
            ORDER BY f.field_order_nb) LOOP

    var_expr := var_expr||case when var_expr is not null then '||''|''||' end||r.expr;
  END LOOP;

  if var_expr is null Then
    return null;
  end if;
  --
  -- ���������� �������� SELECT
  --
  if par_form_cd = '321' Then
    var_sql := to_clob('')||
                 'SELECT '||case when par_query_type = 'CHECK' 
                                 then '/*+ NO_PARALLEL*/ '
                            end||
                         'oes.oes_321_id as oes_id, '||
                         'regexp_replace(trim(''|'' from to_clob('''')||'||var_expr||'), ''\|{2,}'', ''|'') as check_list '||
                   'FROM mantas.rf_oes_321 oes '||
                        'left join mantas.rf_oes_321_party p0 on p0.oes_321_id = oes.oes_321_id and p0.block_nb = 0 '||
                        'left join mantas.rf_oes_321_party p1 on p1.oes_321_id = oes.oes_321_id and p1.block_nb = 1 '||
                        'left join mantas.rf_oes_321_party p2 on p2.oes_321_id = oes.oes_321_id and p2.block_nb = 2 '||
                        'left join mantas.rf_oes_321_party p3 on p3.oes_321_id = oes.oes_321_id and p3.block_nb = 3 '||
                        'left join mantas.rf_oes_321_party p4 on p4.oes_321_id = oes.oes_321_id and p4.block_nb = 4'||
                 case when par_query_type = 'RUN'
                      then ' WHERE oes.oes_321_id = :par_oes_id'
                      when par_query_type = 'CHECK'
                      then ' WHERE 1 <> 1'
                 end;
  end if;
  
  if par_query_type = 'CHECK' and var_sql is not null Then
    var_sql := 'SELECT count(to_char(substr(check_list, 1, 1))) FROM ('||var_sql||')';
  end if;

  return var_sql;
END get_check_select;
--
-- �������� ���
--
PROCEDURE clear_cache IS 
BEGIN
  pck_tab_check_select_cache.DELETE;
  pck_tab_check_scn_cache.DELETE;
END clear_cache;
end RF_PKG_OES_CHECK;
/
