DECLARE
  v_apex_user VARCHAR2(30);
BEGIN
  BEGIN
    SELECT table_owner INTO v_apex_user FROM all_synonyms WHERE synonym_name='APEX_RELEASE' AND owner='PUBLIC';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      raise_application_error(-20000,'Oracle APEX �� ������!');
  END;
  EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM MANTAS.RF_WWV_FLOW_SESSION FOR '||v_apex_user||'.WWV_FLOW_SESSION';
  EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM MANTAS.RF_WWV_FLOW_SESSIONS$ FOR '||v_apex_user||'.WWV_FLOW_SESSIONS$';
  EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM MANTAS.RF_WWV_FLOW_CGI FOR '||v_apex_user||'.WWV_FLOW_CGI';
  --EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM MANTAS.WWV_FLOW_AUTHENTICATION FOR '||v_apex_user||'.WWV_FLOW_AUTHENTICATION';
END;
/

GRANT EXECUTE ON MANTAS.RF_WWV_FLOW_SESSION TO MANTAS;
GRANT SELECT ON MANTAS.RF_WWV_FLOW_SESSIONS$ TO MANTAS;
--GRANT EXECUTE ON MANTAS.WWV_FLOW_AUTHENTICATION TO MANTAS;
GRANT EXECUTE ON MANTAS.RF_WWV_FLOW_CGI TO MANTAS;

begin EXECUTE IMMEDIATE 'CREATE ROLE APEX_ADMINISTRATOR_ROLE'; exception when others then null; end;
/
GRANT APEX_ADMINISTRATOR_ROLE to mantas;


CREATE OR REPLACE
PACKAGE mantas.rf_pkg_apex_adapter IS
--
-- Sentry-������� - ���������� ������ ���, ����� � APEX �������� HTTP-������
--
FUNCTION apex_sentry RETURN BOOLEAN; -- true - ���������� ������, false - ������� �� �������� ������ "��� �������"
--
-- ����������: ���� �� �������������� ���������� �� ���� � ��������� ���������� APEX � ���������� IP-������
-- ���� ���� - ������� ��� (���������� ������������), ������� ����� ������������, �������� ������ ����������.
-- ����� ��������� ������������ ����������
-- ��������! ������� ��������� COMMIT!
--
FUNCTION expend_entry_ticket
(
  par_ip_address    mantas.rf_apex_entry_ticket.ip_address%TYPE,
  par_app_id        mantas.rf_apex_entry_ticket.app_id%TYPE
)  
RETURN mantas.rf_apex_entry_ticket.owner_id%TYPE; -- ����� ������������, ���� ���������� ����, null - �����
--
-- ������� ���������� �� ���� � ��������� ���������� APEX � ���������� IP-������
-- ��������! ��������� ��������� COMMIT!
--
PROCEDURE create_entry_ticket
(
  par_ip_address    mantas.rf_apex_entry_ticket.ip_address%TYPE,
  par_app_id        mantas.rf_apex_entry_ticket.app_id%TYPE,
  par_owner_id      mantas.rf_apex_entry_ticket.owner_id%TYPE
);
--
-- ������� APEX-������ ��� ���������� ip-������ - ���������� ��� ������ (logout) ������������ �� AML
--
PROCEDURE delete_apex_sessions
(
  par_ip_address    mantas.rf_apex_entry_ticket.ip_address%TYPE
);
-- 
-- ��������������� ����� ������� �� �������� APEX ������
-- 
FUNCTION add_report_job
(
  p_app_id      apex_applications.application_id%TYPE                 -- ������������� ������������ ����������    
 ,p_app_page_id apex_application_pages.page_id%TYPE DEFAULT 1     -- ����� ��������� �������� ���������� 
 ,p_params      rf_tab_key_value                    DEFAULT NULL  -- ������ ��������� ������������ ����������
 ,p_v_function_code rf_report_jobs.v_function_code%TYPE   DEFAULT NULL
 ,p_rep_no          rf_report_jobs.rep_no%TYPE            DEFAULT NULL
) RETURN rf_report_jobs.rep_job_id%TYPE;
-- 
-- ��������� ������� �� �������� APEX ������
-- 
PROCEDURE run_report_job
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
);

--
-- �������� ������ ��� APEX ������ (������������� ��� ������ �� JOB)
--
PROCEDURE create_report_session
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
);
--
-- ���������� ��������� APEX ������ (������������� ��� ������ �� JOB ����� ������ create_report_session)
--
PROCEDURE process_report
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
);
--
-- ������� ������� ������ �� ����������� � ������ �����������
--  
FUNCTION refine_report 
(
  p_report IN CLOB  -- ����� ������
)
  RETURN CLOB;
--
-- ������� ����� �� ���� �� �������������� �������
-- ��������! �������� COMMIT  (��� ��������)
--  
PROCEDURE delete_report
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
);
--
-- �������� ����� �� ���� �� �������������� ������� (� �������� ������� ���
-- ��������! �������� COMMIT  (��� ��������)
--  
FUNCTION get_report_by_job_id
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
 ,p_delete BOOLEAN DEFAULT TRUE            -- ������� ����� ����� ������
 ,p_refine BOOLEAN DEFAULT TRUE            -- ��������� ����� �� ������ � ��������
)
  RETURN rf_report_jobs.rep_content%TYPE;
--
-- ������������ � �������� �����  (� �������� ������� ���)
-- ��������! �������� COMMIT  
--  
FUNCTION get_report
(
  p_app_id      apex_applications.application_id%TYPE               -- ������������� ������������ ����������    
 ,p_app_page_id apex_application_pages.page_id%TYPE   DEFAULT 1     -- ����� ��������� �������� ���������� 
 ,p_params      rf_tab_key_value                      DEFAULT NULL  -- ������ ��������� ������������ ����������
 ,p_v_function_code rf_report_jobs.v_function_code%TYPE   DEFAULT NULL
 ,p_rep_no          rf_report_jobs.rep_no%TYPE            DEFAULT NULL
 ,p_delete BOOLEAN DEFAULT TRUE            -- ������� ����� 
 ,p_refine BOOLEAN DEFAULT TRUE            -- ��������� ����� �� ������ � ��������
)
  RETURN rf_report_jobs.rep_content%TYPE;
--
-- �������� ��������� ������������ ������
--
FUNCTION get_report_name
(
  p_app_id      apex_applications.application_id%TYPE               -- ������������� ������������ ����������    
 ,p_app_page_id apex_application_pages.page_id%TYPE   DEFAULT 1     -- ����� ��������� �������� ���������� 
)
  RETURN VARCHAR2;
  
END rf_pkg_apex_adapter;
/

CREATE OR REPLACE
PACKAGE BODY mantas.rf_pkg_apex_adapter IS
--
-- ������� ����������������� � JSON ��������� APEX (����������, ��������, ���������)
--
FUNCTION format_apex_env RETURN rf_json IS  
    l_json rf_json;
  BEGIN
    l_json:=rf_json();
    -- ���������
    FOR c_param IN (SELECT api.item_name name, v(api.item_name) value
                      FROM apex_application_page_items api
                       WHERE api.application_id=v('APP_ID')
                    UNION ALL
                    SELECT ai.item_name name, v(ai.item_name) value
                      FROM apex_application_items ai
                       WHERE ai.application_id=v('APP_ID'))
      LOOP
        IF c_param.value IS NOT NULL THEN
          l_json.put(c_param.name,c_param.value);
        END IF;
      END LOOP;
    RETURN l_json;
  END format_apex_env;
  

--
-- Sentry-������� - ���������� ������ ���, ����� � APEX �������� HTTP-������
--
FUNCTION apex_sentry RETURN BOOLEAN IS -- true - ���������� ������, false - ������� �� �������� ������ "��� �������"
  var_url_pars      VARCHAR2(32000);
  var_ip_address    mantas.rf_apex_entry_ticket.ip_address%TYPE;
  var_app_id        mantas.rf_apex_entry_ticket.app_id%TYPE;
  var_page_id       apex_application_pages.page_id%TYPE;
  var_session_id    NUMBER;

  var_cookie_name   VARCHAR2(4000);
  var_cookie_path   VARCHAR2(4000);
  var_cookie_domain VARCHAR2(4000);
  var_secure        BOOLEAN;  
  var_session       rf_wwv_flow_session.T_SESSION_RECORD; 
  var_audit_seq_id  mantas.rf_audit_event.audit_seq_id%TYPE;
  var_owner_id      mantas.rf_apex_entry_ticket.owner_id%TYPE;
  l_env             rf_json;
BEGIN
  -- ��������� �������� IP-�����, ID ���������� � ID ������ �� URL
  -- ���� IP-����� ��� ID ���������� �������� �� ������� - �� ������������ HTTP-������ (������� FALSE)
  --
  if lower(trim(owa_util.get_cgi_env('PATH_INFO'))) = '/f' Then
    var_url_pars := trim(owa_util.get_cgi_env('QUERY_STRING')); -- ������ ����: p=101:1:5326863055590:::::
    
    if regexp_like(var_url_pars, '^p=\d+.*') Then
      var_ip_address := trim(owa_util.get_cgi_env('REMOTE_ADDR'));
      var_app_id := to_number(trim(regexp_substr(var_url_pars, '\d+', 1, 1)));
      var_page_id := NVL(to_number(trim(regexp_substr(var_url_pars, '\d+', 1, 2))),1);
      var_session_id := to_number(trim(regexp_substr(replace(var_url_pars, ':', ' : '), '[^\:]+', 1, 3)));

      if var_ip_address is null or var_app_id is null Then
        var_audit_seq_id:=rf_pkg_audit.record_event(
          par_user_login      => NULL
         ,par_ip_address      => var_ip_address
         ,par_event_type_cd   => 'APEX_UNKNOWN'
         ,par_event_params    => format_apex_env  
         ,par_event_descr_tx  => var_url_pars
         ,par_err_type_cd     => 'T'
         ,par_message_tx      =>  '�� �������� ip-����� ��� ������������� APEX ����������'
         ); 
        return false; -- �� �������� ip ��� �pp_id
      end if;
    else
      var_audit_seq_id:=rf_pkg_audit.record_event(
        par_user_login      => NULL
       ,par_ip_address      => var_ip_address
       ,par_event_type_cd   => 'APEX_UNKNOWN'
       ,par_event_params    => format_apex_env  
       ,par_event_descr_tx  => var_url_pars
       ,par_err_type_cd     => 'T'
       ,par_message_tx      =>  '�� ���������� URL'
       ); 
      return false;  -- �� ���������� URL
    end if;    
  else
    -- ������� ������� ���� - ���������� ����������� ��������
    return wwv_flow_custom_auth_std.is_session_valid; -- �� /f ������
  end if;
  
  
  --
  -- ��������� �������� ������ � ������������ � ����
  -- 
  wwv_flow_custom_auth_std.get_cookie_props(p_flow          => var_app_id,
                                            p_cookie_name   => var_cookie_name,
                                            p_cookie_path   => var_cookie_path,
                                            p_cookie_domain => var_cookie_domain,
                                            p_secure        => var_secure);
  var_session := rf_wwv_flow_session.get_by_cookie_name(var_cookie_name);  
  --
  -- ���� ��� ������:
  --  1) ������ ���� ������������ � ���������� - ��� ������ � ������������ � ���� - ��������� ����, ���� ���� �����
  --  2) ��������� ���� ������������ � ���������� - ���� ������ �� ����, ��� ID ������ � URL - ��������� ������������� � ������, ���� ���� �����
  --  3) ������� ������ ����� ��������� ����� � ���������� - ���� ������ �� ���� � ID ������ � URL - �������� ������, ���� ��� ������������� ���� �����
  --  
  if var_session.id is null Then -- ��� ������ � ������������ � ����
    --
    -- �������� ����� �� ����, ������� � ����������� �� ��� �������
    --
    var_owner_id := expend_entry_ticket(var_ip_address, var_app_id);

    if var_owner_id is not null Then          
      apex_authentication.post_login(p_username => var_owner_id);
      -- �� ������� ������ � �����, ��� ��� ����� �������� ������ ��������� �������� �� ������� ������
      return true;  -- ���������� ��������������� (����� ������)
    else
      var_audit_seq_id:=rf_pkg_audit.record_event(
        par_user_login      => NVL(var_owner_id,V('APP_USER'))
       ,par_ip_address      => var_ip_address
       ,par_event_type_cd   => 'APEX_'||var_app_id||'_'||var_page_id
       ,par_event_params    => format_apex_env  
       ,par_event_descr_tx  => var_url_pars
       ,par_err_type_cd     => 'A'
       ,par_message_tx      => '������ �����������. �� ������� ���������� �� ������ ������ ��� ����� ������������/IP-������'
       ); 
      return false; -- �� ������ ��� �� ���������� �����
    end if;  
  else
    --
    -- ��������, ��� ��� ������ ���� ������� ��� ���� �� IP-������ � ���� �� ����������,
    -- ���� ��� - �� ������������ HTTP-������ (������� FALSE)
    --  
    if var_session.remote_addr = var_ip_address and 
       -- var_session.on_new_instance_fired_for �������� ������ ����� ���������� ����: 101i102a103b
       ','||translate(var_session.on_new_instance_fired_for, 'iab', ',,,')||',' like '%,'||var_app_id||',%' Then      
      null; -- ������ ������ ��������
    else
      var_audit_seq_id:=rf_pkg_audit.record_event(
        par_user_login      => NVL(var_owner_id,V('APP_USER'))
       ,par_ip_address      => var_ip_address
       ,par_event_type_cd   => 'APEX_'||var_app_id||'_'||var_page_id
       ,par_event_params    => format_apex_env  
       ,par_event_descr_tx  => var_url_pars
       ,par_err_type_cd     => 'A'
       ,par_message_tx      => '������ �����������. ������������ ������ APEX (���������� ���������� �/��� IP-�����)'
       ); 
      return false;  -- ������ �� ��� ���� �� ���������� ��� ip �������
    end if;    
    --
    -- ��������, ��� ID ������ �� URL ��������� � ID ������, ��������� �� ���� 
    --  
    if var_session.id <> var_session_id Then 
      var_audit_seq_id:=rf_pkg_audit.record_event(
        par_user_login      => NVL(var_owner_id,V('APP_USER'))
       ,par_ip_address      => var_ip_address
       ,par_event_type_cd   => 'APEX_'||var_app_id||'_'||var_page_id
       ,par_event_params    => format_apex_env  
       ,par_event_descr_tx  => var_url_pars
       ,par_err_type_cd     => 'A'
       ,par_message_tx      => '������ �����������. ������������ ������ APEX (���������� ID ������ � cookie � � URL)'
       ); 
      return false;  -- �� ��������� ID ������ � ID ������ �� cookie
    end if;    
    --
    -- ���� � URL �� ������ ID ������ (��������� ���� � ����������), �� ������� ������� ������ �� ����
    --        
    if var_session_id is null Then
      var_owner_id := expend_entry_ticket(var_ip_address, var_app_id);
  
      if var_owner_id is not null Then  
        --
        -- ������������� ������ �� ���� ��� �������
        --
        wwv_flow.g_instance := var_session.id;
        apex_custom_auth.define_user_session(p_user       => var_owner_id,
                                             p_session_id => var_session.id);
        -- �� ������� ������ � �����, ��� ��� ����� �������� ������ ��������� �������� �� ������� ������ 
        return true;  -- ��������� �������������� � ������
      else
        var_audit_seq_id:=rf_pkg_audit.record_event(
          par_user_login      => NVL(var_owner_id,V('APP_USER'))
         ,par_ip_address      => var_ip_address
         ,par_event_type_cd   => 'APEX_'||var_app_id||'_'||var_page_id
         ,par_event_params    => format_apex_env  
         ,par_event_descr_tx  => var_url_pars
         ,par_err_type_cd     => 'A'
         ,par_message_tx      => '������ �����������. �� ������� ���������� �� ������ ������ ��� ����� ������������/IP-������'
          ); 
        return false; -- �� ��������� �������������� � ������
      end if;  
    else  
      -- ������ ������ ��� ��������, ������� ������
      var_audit_seq_id:=rf_pkg_audit.record_event(
        par_user_login      => NVL(var_owner_id,V('APP_USER'))
       ,par_ip_address      => var_ip_address
       ,par_event_type_cd   => 'APEX_'||var_app_id||'_'||var_page_id
       ,par_event_params    => format_apex_env  
       ,par_event_descr_tx  => var_url_pars
       ); 
      return true; -- ������� ������
    end if;  
  end if;
END apex_sentry;
--
-- ����������: ���� �� �������������� ���������� �� ���� � ��������� ���������� APEX � ���������� IP-������
-- ���� ���� - ������� ��� (���������� ������������), ������� ����� ������������, �������� ������ ����������.
-- ����� ��������� ������������ ����������
-- ��������! ������� ��������� COMMIT!
--
FUNCTION expend_entry_ticket
(
  par_ip_address    mantas.rf_apex_entry_ticket.ip_address%TYPE,
  par_app_id        mantas.rf_apex_entry_ticket.app_id%TYPE
)  
RETURN mantas.rf_apex_entry_ticket.owner_id%TYPE IS -- ����� ������������, ���� ���������� ����, null - �����
  var_owner_id    mantas.rf_apex_entry_ticket.owner_id%TYPE;
BEGIN
  SELECT max(owner_id)
    INTO var_owner_id
    FROM mantas.rf_apex_entry_ticket
   WHERE ip_address = par_ip_address and
         app_id = par_app_id and
         sysdate between created_time and created_time + 1/24/60;
  --
  -- ������� ������ ����� � ��� ������������ (� ������ � ������, ��������� "�������" ������, ������ ��� ��������)
  --
  DELETE FROM mantas.rf_apex_entry_ticket
   WHERE (var_owner_id is not null and ip_address = par_ip_address and app_id = par_app_id) or
         created_time + 1/24/60 < sysdate or
         created_time > sysdate;
  
  COMMIT;
  
  return var_owner_id;  
END expend_entry_ticket;  
--
-- ������� ���������� �� ���� � ��������� ���������� APEX � ���������� IP-������
-- ��������! ��������� ��������� COMMIT!
--
PROCEDURE create_entry_ticket
(
  par_ip_address    mantas.rf_apex_entry_ticket.ip_address%TYPE,
  par_app_id        mantas.rf_apex_entry_ticket.app_id%TYPE,
  par_owner_id      mantas.rf_apex_entry_ticket.owner_id%TYPE
)  
IS
  var_audit_seq_id mantas.rf_audit_event.audit_seq_id%TYPE;
BEGIN
  --
  -- ��������, ��� ��� ��������� ������� � ��� � ���������� ������������ ���� ����� �� ��������� ���������� APEX
  -- (���� ��� ���������� APEX - ��� ������, ������� ������ ��������� ������� ������� RFREP_<APP_ID>)
  --  
  IF trim(par_ip_address) is not null and trim(par_app_id) is not null and trim(par_owner_id) is not null  
    THEN
      IF rf_pkg_adm.has_function(par_owner_id, 'RFREP_'||to_char(par_app_id)) = 1 OR  (par_owner_id='AMLREP_INTERNAL' AND par_ip_address='127.0.0.1')
        THEN
          MERGE INTO mantas.rf_apex_entry_ticket t USING
            (SELECT par_ip_address as ip_address, 
                    par_app_id as app_id
               FROM dual) v
          ON (t.ip_address = v.ip_address and
              t.app_id = v.app_id)
          WHEN MATCHED THEN
            UPDATE 
               SET owner_id = par_owner_id,
                   created_time = SYSDATE
          WHEN NOT MATCHED THEN
            INSERT (ip_address, app_id, created_time, owner_id)
            VALUES (v.ip_address, v.app_id, SYSDATE, par_owner_id);                                  
      
          COMMIT;
        ELSE   
          var_audit_seq_id:=rf_pkg_audit.record_event(
            par_user_login      => par_owner_id
           ,par_ip_address      => par_ip_address
           ,par_event_type_cd   => 'APEX_'||par_app_id||'_1'
           ,par_event_params    => NULL
           ,par_err_type_cd     => 'A'
           ,par_message_tx      => '������ �����������. �� ���� ������� � ������'
            ); 
      END IF;
    ELSE  
      var_audit_seq_id:=rf_pkg_audit.record_event(
        par_user_login      => par_owner_id
       ,par_ip_address      => par_ip_address
       ,par_event_type_cd   => 'APEX_UNKNOWN'
       ,par_event_params    => NULL
       ,par_err_type_cd     => 'T'
       ,par_message_tx      => '�������� �������� ���������. ������� rf_pkg_apex_adapter.create_entry_ticket()'
        ); 
 END IF;
END create_entry_ticket;
--
-- ������� APEX-������ ��� ���������� ip-������ - ���������� ��� ������ (logout) ������������ �� AML
--
PROCEDURE delete_apex_sessions
(
  par_ip_address    mantas.rf_apex_entry_ticket.ip_address%TYPE
) IS
BEGIN
  FOR r IN (SELECT id
              FROM mantas.rf_wwv_flow_sessions$
             WHERE remote_addr = par_ip_address) LOOP
    mantas.rf_wwv_flow_session.delete_session(r.id);
  END LOOP;
  COMMIT;               
END delete_apex_sessions;
-- 
-- ��������������� ����� ������� �� �������� APEX ������
-- 
FUNCTION add_report_job
(
  p_app_id          apex_applications.application_id%TYPE               -- ������������� ������������ ����������    
 ,p_app_page_id     apex_application_pages.page_id%TYPE   DEFAULT 1     -- ����� ��������� �������� ���������� 
 ,p_params          rf_tab_key_value                      DEFAULT NULL  -- ������ ��������� ������������ ����������
 ,p_v_function_code rf_report_jobs.v_function_code%TYPE   DEFAULT NULL
 ,p_rep_no          rf_report_jobs.rep_no%TYPE            DEFAULT NULL
)
  RETURN rf_report_jobs.rep_job_id%TYPE IS
    l_job_id rf_report_jobs.rep_job_id%TYPE;
    l_app_check CHAR(1);
  BEGIN 
    -- ��������� ������������ ID ����������
    BEGIN 
      SELECT 'x' 
        INTO l_app_check
        FROM apex_applications a
        WHERE a.application_id=p_app_id;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR(-20000,'���������� APEX c application_id='||TO_CHAR(p_app_id)||' �� ����������!');
     END;  
    -- ��������� ������������ ID �������� ����������
    BEGIN 
      SELECT 'x' 
        INTO l_app_check
        FROM apex_application_pages ap
        WHERE ap.application_id=p_app_id AND ap.page_id=p_app_page_id;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR(-20000,'� ���������� APEX c application_id='||TO_CHAR(p_app_id)||' �� ���������� �������� '||NVL(TO_CHAR(p_app_page_id),'NULL')||'!');
     END;  
    -- ��������� ������������ ����������
    IF p_params IS NOT NULL THEN
      FOR i IN p_params.FIRST .. p_params.LAST
        LOOP
          BEGIN 
            SELECT 'x' 
              INTO l_app_check
              FROM apex_application_page_items ai
              WHERE ai.application_id=p_app_id
                AND ai.item_name=p_params(i).key;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR(-20000,'� ���������� APEX c application_id='||TO_CHAR(p_app_id)||' �� ���������� ��������� '||NVL(p_params(i).key,'NULL')||'!');
          END;  
        END LOOP;
    END IF;
    
    INSERT INTO rf_report_jobs(rep_job_id,app_id,app_page_id,rep_params,status_cd,v_function_code,rep_no)
      VALUES(rf_report_jobs_seq.NEXTVAL,p_app_id,p_app_page_id,p_params,'NEW',p_v_function_code,p_rep_no)
      RETURNING rep_job_id INTO l_job_id;
    RETURN l_job_id;  
  END add_report_job;
-- 
-- ��������� ������� �� �������� APEX ������
-- 
PROCEDURE run_report_job
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
)  
  IS
    l_job_status rf_report_jobs.status_cd%TYPE;
  
    -- ��������������� ���������: ��������� JOB � ��������� ��� ����������
    PROCEDURE start_job_and_wait_complete
      (
        p_job_name VARCHAR2 
       ,p_job_text VARCHAR2 
      ) IS 
        l_job_count NUMBER;
      BEGIN
         -- ��������� ������� � ��������� ���
         dbms_scheduler.create_job(
            job_name        => p_job_name,
            job_type        => 'PLSQL_BLOCK',
            job_action      => p_job_text,
            start_date      => null, 
            repeat_interval => null,
            enabled         => TRUE,
            auto_drop       => TRUE,
            comments        => '������� �� ���������� APEX ������'
          );
          -- ���� ���������� 
          LOOP
            SELECT COUNT(*)
             INTO l_job_count
              FROM all_scheduler_jobs
            WHERE job_name=p_job_name;
            EXIT WHEN  l_job_count=0;
            dbms_lock.sleep(1);
          END LOOP;
      
      END start_job_and_wait_complete;
      
  BEGIN
    -- ��������� �� ������� �������
    BEGIN
      SELECT status_cd 
        INTO l_job_status
        FROM rf_report_jobs j
        WHERE j.rep_job_id=p_job_id
          AND j.status_cd='NEW';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR(-20000,'������� �� ���������� APEX ������ c rep_job_id='||TO_CHAR(p_job_id)||' �� ���������� ��� ������ ��� �� NEW!');
     END;  
   -- ������� APEX �������
   start_job_and_wait_complete('AML_APEX_SES_'||to_char(p_job_id),'BEGIN mantas.rf_pkg_apex_adapter.create_report_session('||TO_CHAR(p_job_id)||'); END;');
   -- ��������� ��� ������ ��������� �������
    BEGIN
      SELECT status_cd 
        INTO l_job_status
        FROM rf_report_jobs j
        WHERE j.rep_job_id=p_job_id
          AND j.status_cd='SESSION_CREATED';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR(-20000,'������ ��� �������� APEX ������ ������ c rep_job_id='||TO_CHAR(p_job_id));
     END;  
   -- ��������� ��������� ������
   start_job_and_wait_complete('AML_APEX_RUN_'||to_char(p_job_id),'BEGIN mantas.rf_pkg_apex_adapter.process_report('||TO_CHAR(p_job_id)||'); END;');
  END run_report_job;
--
-- �������� ������ ��� APEX ������ (������������� ��� ������ �� JOB)
--
PROCEDURE create_report_session
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
)  
  IS
    l_job           rf_report_jobs%ROWTYPE;
    l_report        CLOB;
    l_thePage       htp.htbuf_arr; 
    l_lines         number default 99999999; 
    l_owa_nm        owa.vc_arr; 
    l_owa_vl        owa.vc_arr; 
    l_session_id    rf_report_jobs.apex_session_id%TYPE;
    l_cookie        rf_report_jobs.apex_cookie_value%TYPE;
    l_skip          BOOLEAN;
    
  BEGIN
    -- �������� ������ �������
    BEGIN
      SELECT * 
        INTO l_job
        FROM rf_report_jobs j
        WHERE j.rep_job_id=p_job_id
          AND j.status_cd='NEW'
        FOR UPDATE NOWAIT;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR(-20000,'������� �� ���������� APEX ������ c rep_job_id='||TO_CHAR(p_job_id)||' �� ���������� ��� ������ ��� �� NEW!');
     END; 
    -- �������� ������� ��������  � ������
    UPDATE rf_report_jobs j SET status_cd='RUNNING', start_time=SYSTIMESTAMP WHERE j.rep_job_id=p_job_id;
    COMMIT;
    -- �������� ���������
    BEGIN
      -- ������������� OWA 
      l_owa_nm(1) := 'REMOTE_ADDR'; 
      l_owa_vl(1) := '127.0.0.1'; 
      l_owa_nm(2) := 'REQUEST_PROTOCOL';
      l_owa_vl(2) := 'HTTP';
      l_owa_nm(3) := 'PATH_INFO';
      l_owa_vl(3) := '/f';
      l_owa_nm(4) := 'QUERY_STRING';
      l_owa_vl(4) := 'p='||l_job.app_id||':'||l_job.app_page_id||'::::::';
      
      htp.init;
      owa.init_cgi_env( l_owa_nm.count, l_owa_nm, l_owa_vl ); 
      owa.reset_get_page;
      htp.HTBUF_LEN := 63;   

      -- ������� APEX ������
      rf_pkg_apex_adapter.create_entry_ticket('127.0.0.1',l_job.app_id,'AMLREP_INTERNAL');
      f(p=>TO_CHAR(l_job.app_id)||':'||TO_CHAR(l_job.app_page_id)||'::::::'); 
      
      -- �������� id ������
      l_session_id:=apex_custom_auth.get_session_id;
      
      -- �������� ���� ��������� ����� APEX
      dbms_lob.createtemporary(l_report,true);
      owa.get_page( l_thePage, l_lines ); 
      FOR i IN 1 .. l_lines LOOP 
        l_report:=l_report||l_thePage(i); 
        IF INSTR(l_thePage(i),'Set-Cookie:')>0 THEN
          l_cookie:=REGEXP_SUBSTR(l_thePage(i),'.+;',12);
          EXIT;
        END IF;
      END LOOP;     
      
      --��������� ���������
      IF l_session_id IS NOT NULL AND l_cookie IS NOT NULL 
        THEN
          UPDATE rf_report_jobs j 
            SET status_cd='SESSION_CREATED', apex_session_id=l_session_id, apex_cookie_value=l_cookie,err_mess_tx=NULL, rep_content=l_report 
            WHERE j.rep_job_id=p_job_id;
          COMMIT;
        ELSE  
          UPDATE rf_report_jobs j SET status_cd='ERROR', end_time=SYSTIMESTAMP,err_mess_tx='�� ������ �������� ������ �/��� ����', apex_session_id=l_session_id, apex_cookie_value=l_cookie, rep_content=l_report WHERE j.rep_job_id=p_job_id;
          COMMIT;
      END IF;    
    EXCEPTION 
      WHEN OTHERS THEN
        -- ��������� ��������� � ���������� ������ 
        UPDATE rf_report_jobs j SET status_cd='ERROR', end_time=SYSTIMESTAMP, err_mess_tx=dbms_utility.format_error_stack WHERE j.rep_job_id=p_job_id;
        COMMIT;
        RAISE;
    END;
  END create_report_session;   
--
-- ���������� ��������� APEX ������ (������������� ��� ������ �� JOB)
--
PROCEDURE process_report
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
)  
  IS
    l_job             rf_report_jobs%ROWTYPE;
    l_report          CLOB;
    l_thePage         htp.htbuf_arr; 
    l_lines           number default 99999999; 
    l_owa_nm          owa.vc_arr; 
    l_owa_vl          owa.vc_arr; 
    l_workspace_id    apex_applications.workspace_id%TYPE;
    l_skip            BOOLEAN;
    l_rep_par_names   VARCHAR2(32000);
    l_rep_par_values  VARCHAR2(32000);
    
  BEGIN
    -- �������� ������ �������
    BEGIN
      SELECT * 
        INTO l_job
        FROM rf_report_jobs j
        WHERE j.rep_job_id=p_job_id
          AND j.status_cd='SESSION_CREATED'
        FOR UPDATE NOWAIT;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR(-20000,'������� �� ���������� APEX ������ c rep_job_id='||TO_CHAR(p_job_id)||' �� ���������� ��� ������ ��� �� SESSION_CREATED!');
     END; 
    -- �������� ������� ��������  � ������
    UPDATE rf_report_jobs j SET status_cd='RUNNING', start_time=SYSTIMESTAMP WHERE j.rep_job_id=p_job_id;
    COMMIT;
    -- �������� ���������
    BEGIN
      -- ������������� OWA 
      l_owa_nm(1) := 'REMOTE_ADDR'; 
      l_owa_vl(1) := '127.0.0.1'; 
      l_owa_nm(2) := 'REQUEST_PROTOCOL';
      l_owa_vl(2) := 'HTTP';
      l_owa_nm(3) := 'HTTP_COOKIE'; 
      l_owa_vl(3) := l_job.apex_cookie_value; 
      
      
      htp.init;
      owa.init_cgi_env( l_owa_nm.count, l_owa_nm, l_owa_vl ); 
      owa.reset_get_page;
      htp.HTBUF_LEN := 63;   

      -- ���������� ���������
      IF l_job.rep_params IS NOT NULL THEN
        FOR i IN l_job.rep_params.FIRST .. l_job.rep_params.LAST
          LOOP
            apex_util.set_session_state(l_job.rep_params(i).key,l_job.rep_params(i).value );
            l_rep_par_names:=l_rep_par_names||','||l_job.rep_params(i).key;
            l_rep_par_values:=l_rep_par_values||',\'||l_job.rep_params(i).value||'\';
          END LOOP;
      END IF;
      l_rep_par_names:=LTRIM(l_rep_par_names,',');
      l_rep_par_values:=LTRIM(l_rep_par_values,',');
            
      -- ��������� �����
      f(p=>TO_CHAR(l_job.app_id)||':'||TO_CHAR(l_job.app_page_id)||':'||TO_CHAR(l_job.apex_session_id)||'::::'||l_rep_par_names||':'||l_rep_par_values||':YES'); 
      
      -- ��������� ��������� ����������
      dbms_lob.createtemporary(l_report,true);
      owa.get_page( l_thePage, l_lines ); 
      l_skip:=TRUE;
      FOR i IN 1 .. l_lines LOOP 
        -- ���������� http ���������
        IF l_skip
          THEN 
            IF instr(l_thePage(i),'<!DOCTYPE html>')>0  THEN
              l_skip:=FALSE;
              l_report:=l_report||l_thePage(i); 
            END IF;
          ELSE 
            l_report:=l_report||l_thePage(i); 
        END IF;
      END LOOP;     
    --��������� ���������
    UPDATE rf_report_jobs j 
      SET status_cd='READY', end_time=SYSTIMESTAMP, rep_content=l_report , apex_session_id=NULL, apex_cookie_value=NULL
      WHERE j.rep_job_id=p_job_id;
    COMMIT;
    EXCEPTION 
      WHEN OTHERS THEN
        -- ��������� ��������� � ���������� ������ 
        UPDATE rf_report_jobs j SET status_cd='ERROR', end_time=SYSTIMESTAMP, err_mess_tx=dbms_utility.format_error_stack WHERE j.rep_job_id=p_job_id;
        COMMIT;
        RAISE;
    END;
  END process_report;   
  
--
-- ������� ������� ������ �� ����������� � ������ �����������
--  
FUNCTION refine_report 
(
  p_report IN CLOB
)
  RETURN CLOB IS
    l_report CLOB;
  BEGIN
    l_report:=p_report;
    l_report:=REGEXP_REPLACE(l_report,'<style> html {visibility:hidden;} </style>','');
    l_report:=REGEXP_REPLACE(l_report,'<script .*?</script>','',1,0,'n');
    l_report:=REGEXP_REPLACE(l_report,'<button .*?</button>','',1,0,'n');
    l_report:=REGEXP_REPLACE(l_report,'<a href=.*?</a>','',1,0,'n');
    l_report:=REGEXP_REPLACE(l_report,'<link .*?>','',1,0,'n');
    l_report:=REGEXP_REPLACE(l_report,'<table class="t-Report-pagination.*?</table>','',1,0,'n');
    l_report:=REGEXP_REPLACE(l_report,'release \d+.\d+','');
    RETURN l_report;
  END refine_report;
--
-- ������� ����� �� ���� �� �������������� �������
-- ��������! �������� COMMIT  (��� ��������)
--  
PROCEDURE delete_report
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
)
  IS
  BEGIN
    DELETE FROM rf_report_jobs r WHERE r.rep_job_id=p_job_id;
    COMMIT;
  END delete_report;
--
-- �������� ����� �� ���� �� �������������� ������� (� �������� ������� ���
-- ��������! �������� COMMIT  (��� ��������)
--  
FUNCTION get_report_by_job_id
(
  p_job_id rf_report_jobs.rep_job_id%TYPE  -- ������������� ������� �� ���������� ������
 ,p_delete BOOLEAN DEFAULT TRUE            -- ������� ����� ����� ������
 ,p_refine BOOLEAN DEFAULT TRUE            -- ��������� ����� �� ������ � ��������
)
  RETURN rf_report_jobs.rep_content%TYPE IS
    l_rep_content rf_report_jobs.rep_content%TYPE;
  BEGIN
    -- �������� �����
    BEGIN 
      SELECT r.rep_content INTO l_rep_content
        FROM rf_report_jobs r
        WHERE r.rep_job_id=p_job_id
          AND r.status_cd='READY';
    EXCEPTION    
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20000,'������� �� ���������� APEX ������ c rep_job_id='||TO_CHAR(p_job_id)||' �� ���������� ��� ������ ��� �� READY!');
    END;
    
    -- ������� ���� ����� 
    IF p_refine THEN
      l_rep_content:=refine_report(l_rep_content);
    END IF;
    -- ������� ���� �����
    IF p_delete THEN
      delete_report(p_job_id);
    END IF;
    
    RETURN l_rep_content;
  END get_report_by_job_id;
--
-- ������������ � �������� �����  (� �������� ������� ���)
-- ��������! �������� COMMIT  
--  
FUNCTION get_report
(
  p_app_id      apex_applications.application_id%TYPE               -- ������������� ������������ ����������    
 ,p_app_page_id apex_application_pages.page_id%TYPE   DEFAULT 1     -- ����� ��������� �������� ���������� 
 ,p_params      rf_tab_key_value                      DEFAULT NULL  -- ������ ��������� ������������ ����������
 ,p_v_function_code rf_report_jobs.v_function_code%TYPE   DEFAULT NULL
 ,p_rep_no          rf_report_jobs.rep_no%TYPE            DEFAULT NULL
 ,p_delete BOOLEAN DEFAULT TRUE            -- ������� ����� 
 ,p_refine BOOLEAN DEFAULT TRUE            -- ��������� ����� �� ������ � ��������
)
  RETURN rf_report_jobs.rep_content%TYPE IS
    l_rep rf_report_jobs.rep_job_id%TYPE;
  BEGIN
    -- ��������� �����
    l_rep:=add_report_job(p_app_id=>p_app_id, p_app_page_id=>p_app_page_id, p_params=>p_params,p_v_function_code=>p_v_function_code, p_rep_no=>p_rep_no);
    -- ��������� �����
    run_report_job(l_rep);
    -- �������� �����
    RETURN get_report_by_job_id(p_job_id=>l_rep, p_delete=>p_delete, p_refine=>p_refine);
  END get_report;
--
-- �������� ��������� ������������ ������
--
FUNCTION get_report_name
(
  p_app_id      apex_applications.application_id%TYPE               -- ������������� ������������ ����������    
 ,p_app_page_id apex_application_pages.page_id%TYPE   DEFAULT 1     -- ����� ��������� �������� ���������� 
)
  RETURN VARCHAR2 IS
     l_name VARCHAR2(2000);
  BEGIN
    SELECT CASE WHEN REPLACE(UPPER(TRIM(t.application_name)), ' ') = REPLACE(UPPER(TRIM(t.page_name)), ' ')
             THEN t.application_name
             ELSE t.application_name||', '||t.page_name
           END AS rep_name
      INTO l_name  
      FROM apex_application_pages t 
      WHERE t.application_id = p_app_id 
        AND t.page_id = p_app_page_id;  
    RETURN l_name;
 EXCEPTION 
   WHEN NO_DATA_FOUND THEN
     RAISE_APPLICATION_ERROR(-20000,'APEX ������ c application_id='||TO_CHAR(p_app_id)||' � page_id='||TO_CHAR(p_app_page_id)||' �� ����������!');
 END get_report_name;
END rf_pkg_apex_adapter;
/

CREATE OR REPLACE 
FUNCTION mantas.rf_apex_sentry RETURN BOOLEAN IS
BEGIN
  return rf_pkg_apex_adapter.apex_sentry;
END rf_apex_sentry;
/

GRANT EXECUTE ON mantas.rf_apex_sentry TO AMLREP;    
