create or replace package mantas.rf_pkg_request_oes_changes is

  /* ������� ��������� ������� �� ������� 
   *   
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json;

end;
/


create or replace package body mantas.rf_pkg_request_oes_changes is
  
  /* ��������� ������� ��� ��������� ������� ��������� ��� - OES321Changes readByOesId
   *
   * ��������� �������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� �������
   *   rf_json - json �����
   */
  function get_oes_321_changes ( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(2048);
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select t.oes_321_id, to_char(t.batch_date, 'DD.MM.YYYY HH24:MI:SS') batch_date,
             trim(o.owner_dsply_nm||nvl2(o.owner_dsply_nm, ' ('||t.modified_by||')', o.owner_dsply_nm)) modified_by,
             t.field_list, 
             a.actvy_type_nm, 
             t.ip_address
        from (select c.oes_321_id, batch_date, c.modified_by, 
                     --������ ����� �� ������� rf_oes_field. ������������ ���� ������� �� ������� mantas.rfv_oes_changes
                     listagg(nvl(f.field_cd, c.column_cd||decode(c.block_nb, -1, null, c.block_nb)), ', ') 
                     --����������: block_nb, field_order_nb, ������������ ���� � mantas.rfv_oes_changes � ����� ������ block_nb.
                     within group (order by c.block_nb, f.field_order_nb nulls last) field_list,
                     c.actvy_type_cd, 
                     c.ip_address
                from table(mantas.rfv_oes_changes(:OESID)) c
                left join mantas.rf_oes_field f on nvl(f.res_block_cd, -1) = c.block_nb and f.column_nm = c.column_cd
               where changed_flag = 'Y' 
               group by c.oes_321_id,
                        c.batch_date, 
                        c.modified_by, 
                        c.actvy_type_cd, 
                        c.ip_address
              ) t
        left join mantas.kdd_review_owner o on o.owner_id = t.modified_by
        left join mantas.kdd_activity_type_cd a on a.actvy_type_cd = t.actvy_type_cd
       order by t.batch_date desc
    }';
    v_bind_json.put( 'OESID', rf_json_ext.get_number( p_json, 'oes_321_id' ) );
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;
  
  
  /* ��������� ������� ��� ��������� ������� ������ ��������� ��� - OES321ChangeDetails read
   *
   * ��������� �������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� �������
   *   rf_json - json �����
   */
  function get_oes_321_change_details ( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(2048);
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select row_number() over (order by decode(c.block_nb, -1,0,  0,1,  3,2,  4,4,  1,5,  2,6,  7), f.field_order_nb) order_nb,
             decode(c.block_nb,
               -1, '�������� �� ��������',
               0,  '����������',
               1,  '������������� �����������',
               2,  '������������� ����������',
               3,  '����������',
               4,  '�� ����� � �� ���������',
               ''
             ) block_tx,
             decode(c.column_cd,
               'OES_321_ORG_ID', 'ID ��������� �����������',
               'MANUAL_CMNT', '����������� � ���������',
               'CUST_SEQ_ID', 'ID �������',
               'OES_321_P_ORG_ID', 'ID ��������� ��������',
               'NOTE', '����������',
               nvl(f.field_nm,'')
             ) field_nm,
             nvl(f.field_cd, c.column_cd||decode(c.block_nb, -1, null, c.block_nb)) field_cd,
             decode(c.old_value, 'NULL', null, c.old_value) old_value,
             decode(c.new_value, 'NULL', null, c.new_value) new_value,
             c.changed_flag
        from table(mantas.rfv_oes_changes(:OESID, to_date(:BATCH_DATE, 'DD.MM.YYYY HH24:MI:SS'))) c
        left join mantas.rf_oes_field f on nvl(f.res_block_cd, -1) = c.block_nb and f.column_nm = c.column_cd
        where f.column_nm is not null 
           or c.column_cd in ('OES_321_ORG_ID', 'MANUAL_CMNT', 'CUST_SEQ_ID', 'OES_321_P_ORG_ID', 'NOTE') 
       order by decode(c.block_nb, -1,0,  0,1,  3,2,  4,4,  1,5,  2,6,  7), f.field_order_nb
    }';
    v_sql := replace(v_sql, '##DATE_FORMAT##', rf_json_ext.format_string);
    v_bind_json.put( 'OESID', rf_json_ext.get_number( p_json, 'oes_321_id' ) );
    v_bind_json.put( 'BATCH_DATE', ''||rf_json_ext.get_string( p_json, 'batch_date' )||'' );
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;
  
  /* ������� ��������� ������� �� ������� 
   *   
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json
  is
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
  begin
      /* ������ ���� � ������ ��� "T" */
    v_json_in := rf_json(regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2'));
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');

    case 
      when v_form_name = 'OES321Changes' and v_action='readByOesId' then 
        v_json_out := get_oes_321_changes( v_json_in, p_id_request );
      when v_form_name = 'OES321ChangeDetails' and v_action='read' then 
        v_json_out := get_oes_321_change_details( v_json_in, p_id_request );
      else null;
    end case;

    return v_json_out;
  end;

end;
/
