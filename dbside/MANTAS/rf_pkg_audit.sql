CREATE OR REPLACE PACKAGE mantas.rf_pkg_audit IS
  --
  -- �������� � ������ ������ ������� �� ���������� ������������ (service.jsp)
  --
  FUNCTION record_gui_event
  (
    par_user_login    rf_audit_event.user_login%TYPE,  -- ��� �������� ������������ (������������ ������)
    par_ip_address    rf_audit_event.ip_address%TYPE,  -- IP ����� �������� ������������
    par_request       rf_json                          -- ������ �� GUI (service.jsp) � ������� JSON
  ) 
  RETURN mantas.rf_audit_event.audit_seq_id%TYPE; -- ID ��������� ������ � ������� ������
  --
  -- �������� � ������ ������ �������
  --
  FUNCTION record_event
  (
    par_user_login       rf_audit_event.user_login%TYPE,     -- ��� �������� ������������ (������������ ������)
    par_ip_address       rf_audit_event.ip_address%TYPE,     -- IP ����� �������� ������������
    par_event_type_cd    rf_audit_event.event_type_cd%TYPE,  -- ��� ���� �������
    par_event_params     rf_json,                            -- ��������� ������� � ������� json, ��� ������� �� GUI (service.jsp) - ������
    par_gui_flag         INTEGER DEFAULT 0,                               -- ���� 1/0, ��� ��� ������� �� GUI (service.jsp), par_event_params �������� ���� ������
    par_event_descr_tx   rf_audit_event.event_descr_tx%TYPE DEFAULT null, -- �������� ������� - �����������, ���� ���������� ��� ��������� �������� �������, ����� ����������� � ����������� � �����������
    par_err_type_cd      rf_audit_event.err_type_cd%TYPE DEFAULT null,    -- ������� ������: null/A/T - �����������, ���� ���������� ���������� ������� � �������
    par_message_tx       rf_audit_event.message_tx%TYPE DEFAULT null      -- ����� ��������� (� ���������, �� ������) - �����������, ���� ���������� ���������� ������� � ����������
  ) 
  RETURN mantas.rf_audit_event.audit_seq_id%TYPE; -- ID ��������� ������ � ������� ������
  --
  -- �������� � ��� ������������ ������ ������� ������ ��������� ������
  --
  PROCEDURE add_search_params
  (
    par_audit_seq_id      rf_audit_event.audit_seq_id%TYPE,                         -- ID ������ ������� ������
    par_search_type_tx    rf_audit_search_params.search_type_tx%TYPE DEFAULT NULL,  -- ����������/����������/���������� ��� ���������� 
    par_last_nm           rf_audit_search_params.last_nm%TYPE DEFAULT NULL,         -- �������, �� ������� ���������� �����
    par_first_nm          rf_audit_search_params.first_nm%TYPE DEFAULT NULL,        -- ���, �� ��������  ���������� �����
    par_midl_nm           rf_audit_search_params.midl_nm%TYPE DEFAULT NULL,         -- ��������, �� ��������  ���������� �����
    par_birth_dt          rf_audit_search_params.birth_dt%TYPE DEFAULT NULL,        -- ���� ��������, �� �������  ���������� �����
    par_doc_type_cd       rf_audit_search_params.doc_type_cd%TYPE DEFAULT NULL,     -- ��� ���, �� ��������  ���������� �����
    par_doc_series_id     rf_audit_search_params.doc_series_id%TYPE DEFAULT NULL,   -- ����� ���, �� �������  ���������� �����
    par_doc_no_id         rf_audit_search_params.doc_no_id%TYPE DEFAULT NULL,       -- ����� ���, �� ��������  ���������� �����
    par_acct_nb           rf_audit_search_params.acct_nb%TYPE DEFAULT NULL,         -- ����� �����, �� ��������  ���������� �����
    par_inn_nb            rf_audit_search_params.inn_nb%TYPE DEFAULT NULL,          -- ���, �� ��������  ���������� �����
    par_ogrn_nb           rf_audit_search_params.ogrn_nb%TYPE DEFAULT NULL,         -- ����, �� ��������  ���������� �����
    par_okpo_nb           rf_audit_search_params.okpo_nb%TYPE DEFAULT NULL,         -- ����, �� ��������  ���������� �����
    par_watch_list_cd     rf_audit_search_params.watch_list_cd%TYPE DEFAULT NULL,   -- ��� ������� �����������, �� ��������  ���������� �����
    par_cust_seq_id       rf_audit_search_params.cust_seq_id%TYPE DEFAULT NULL     -- ID �������, �� ��������  ���������� �����
  );
  --
  -- ���������� � ��� ������������ ������ ������� ������ ��������� ���������� �������
  --
  PROCEDURE set_event_result
  (
    par_audit_seq_id     rf_audit_event.audit_seq_id%TYPE,  -- ID ������ ������� ������
    par_err_type_cd      rf_audit_event.err_type_cd%TYPE,   -- ������� ������: null/A/T 
    par_message_tx       rf_audit_event.message_tx%TYPE     -- ����� ��������� (� ���������, �� ������)
  );
  --
  -- ������� ������ � ������� RF_AUDIT_EVENT
  --
  FUNCTION create_audit_event
  (
    par_event            rf_audit_event%ROWTYPE
  )   
  RETURN mantas.rf_audit_event.audit_seq_id%TYPE; -- ID ��������� ������ � ������� ������
  --
  -- �������� ������������ �������� ��� �������/��� (�� ����)
  --
  FUNCTION get_actvy_name
  (
    par_actvy_type_cd      kdd_activity_type_cd.actvy_type_cd%TYPE -- ��� �������� ��� �������/���
  )
  RETURN kdd_activity_type_cd.actvy_type_nm%TYPE; -- ������������ ��������
  
  --
  -- ��������� ������������� ���� 
  --
  PROCEDURE init;
  
END rf_pkg_audit;
/

CREATE OR REPLACE PACKAGE BODY mantas.rf_pkg_audit IS
  
  -- ��� ����� ������� ������
  TYPE audit_event_type_tt IS TABLE OF rf_audit_event_type%ROWTYPE INDEX BY rf_audit_event.event_type_cd%TYPE;
  g_audit_event_types audit_event_type_tt;
  
  -- ��� kdd_activity_type_cd
  TYPE kdd_activity_type_tt IS TABLE OF kdd_activity_type_cd.actvy_type_nm%TYPE INDEX BY kdd_activity_type_cd.actvy_type_cd%TYPE;
  g_activity_types kdd_activity_type_tt;
  
  -- ��� ��� ������ ID ��������
  TYPE object_ids_tt IS TABLE OF rf_audit_event.object_id%TYPE;
  
  -- ��� ���������� �������
  g_last_audit_seq_id rf_audit_event.audit_seq_id%TYPE;
  g_last_event_time   rf_audit_event.event_time%TYPE;
  g_last_event_type   rf_audit_event.event_type_cd%TYPE;
  g_last_user_login   rf_audit_event.user_login%TYPE;

  --
  -- �������� � ������ ������ ������� �� ���������� ������������ (service.jsp)
  --
  FUNCTION record_gui_event
  (
    par_user_login    rf_audit_event.user_login%TYPE,  -- ��� �������� ������������ (������������ ������)
    par_ip_address    rf_audit_event.ip_address%TYPE,  -- IP ����� �������� ������������
    par_request       rf_json                          -- ������ �� GUI (service.jsp) � ������� JSON
  ) 
  RETURN mantas.rf_audit_event.audit_seq_id%TYPE IS -- ID ��������� ������ � ������� ������
    l_formname      VARCHAR2(255);
    l_action        VARCHAR2(255); 
    l_event_type_cd rf_audit_event.event_type_cd%TYPE;
    l_err_type_cd   rf_audit_event.err_type_cd%TYPE;
    l_message_tx    rf_audit_event.message_tx%TYPE;
  BEGIN
    -- ���������� ��� �������� GUI_<FORM>_<ACTION> � �������� record_event, ��� ���� par_gui_flag = 1
    -- ���� ��� FORM ��� ACTION ���������� ������� � ����� �������� GUI_UNKNOWN ����� create_audit_event
    l_formname := rf_json_ext.get_string(par_request, 'form');
    l_action   := rf_json_ext.get_string(par_request, 'action'); 
    IF l_formname IS NOT NULL AND l_action IS NOT NULL
      THEN
        l_event_type_cd:='GUI_'||UPPER(l_formname)||'_'||UPPER(l_action);
        l_err_type_cd:=NULL;
        l_message_tx:=NULL;
      ELSE
        l_event_type_cd:='GUI_UNKNOW';
        l_err_type_cd:='T';
        l_message_tx:='� ������� ���������� ��� ������ ���� �� �����: form �/��� action';
    END IF;  
    RETURN record_event(
       par_user_login     => par_user_login
      ,par_ip_address     => par_ip_address
      ,par_event_type_cd  => l_event_type_cd
      ,par_event_params   => par_request
      ,par_gui_flag       => 1
      ,par_event_descr_tx => NULL
      ,par_err_type_cd    => l_err_type_cd
      ,par_message_tx     => l_message_tx
      );
  END record_gui_event;
  
  --
  -- ��������� �������� ���� ������� � ����, ���� �� ������� ���������� ��������
  --
  FUNCTION get_audit_event_type(
    par_event_type_cd    rf_audit_event.event_type_cd%TYPE  -- ��� ���� �������
  )  
  RETURN rf_audit_event_type%ROWTYPE IS -- ������ �� ������� ����� ������� ������
    l_event_type rf_audit_event_type%ROWTYPE;
  BEGIN
    -- ��������� ���� �� ������ � ����
    IF g_audit_event_types.EXISTS(par_event_type_cd)
      THEN
        -- ������ ���������� ������
        l_event_type:=g_audit_event_types(par_event_type_cd);
      ELSE 
        -- ����� ������ ��� �������
        l_event_type.event_type_cd:=par_event_type_cd;
    END IF;
    
    RETURN l_event_type;
  END get_audit_event_type;
  
  --
  -- ���������������� ������ ��� ������������
  --  
  PROCEDURE record_internal_error_event(
    par_user_login       rf_audit_event.user_login%TYPE     -- ��� �������� ������������ (������������ ������)
   ,par_ip_address       rf_audit_event.ip_address%TYPE     -- IP ����� �������� ������������
   ,par_event_params     rf_json                            -- ��������� ������� � ������� json, ��� ������� �� GUI (service.jsp) - ������
   ,par_event_descr_tx   rf_audit_event.event_descr_tx%TYPE -- �������� ������� - �����������, ���� ���������� ��� ��������� �������� �������, ����� ����������� � ����������� � �����������
   ,par_message_tx       rf_audit_event.message_tx%TYPE     -- ����� ��������� (� ���������, �� ������) - �����������, ���� ���������� ���������� ������� � ����������
   ) IS
     l_error_event mantas.rf_audit_event.audit_seq_id%TYPE;
   BEGIN
     l_error_event:=record_event(
                            par_user_login     => par_user_login
                           ,par_ip_address     => par_ip_address
                           ,par_event_type_cd  => 'SYS_AUDIT_EVENT_ERROR'
                           ,par_event_params   => par_event_params
                           ,par_gui_flag       => 0
                           ,par_event_descr_tx => par_event_descr_tx
                           ,par_err_type_cd    => 'T'
                           ,par_message_tx     => par_message_tx
                           );
    END record_internal_error_event;
  
  --
  -- ��������� ����������� �������������� ������ � ��������� �������, � ���� ���� ����������...
  --
  FUNCTION is_audit_event_valid(
      par_audit_seq_id     rf_audit_event.audit_seq_id%TYPE  -- ID ������ ������� ������
    ) RETURN BOOLEAN IS
  BEGIN
    -- ��������� ������ ���� ������ �� ���������
    IF par_audit_seq_id!=g_last_audit_seq_id OR g_last_audit_seq_id IS NULL THEN
      BEGIN
        -- ���� �� ��������� ��� ���, ��� �� �� ������ �� ���� ���������
        SELECT e.audit_seq_id, e.event_time, e.event_type_cd, e.user_login
          INTO g_last_audit_seq_id, g_last_event_time, g_last_event_type, g_last_user_login
          FROM rf_audit_event e WHERE e.audit_seq_id=par_audit_seq_id AND e.event_time>=SYSDATE-2;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          -- ���� �� ���� ���������
          BEGIN
            SELECT e.audit_seq_id, e.event_time, e.event_type_cd, e.user_login
              INTO g_last_audit_seq_id, g_last_event_time, g_last_event_type, g_last_user_login
              FROM rf_audit_event e WHERE e.audit_seq_id=par_audit_seq_id;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN FALSE;               
          END;
      END;
    END IF;
    
    RETURN TRUE;
    
  END;

  --
  -- �������� ������� ��������������� �� JSON �� ��������� �����
  --
  FUNCTION get_object_ids(
    par_json rf_json  -- JSON � ������� ����
   ,par_object_id_path rf_audit_event_type.object_id_path%type
   ,par_object_id_path2 rf_audit_event_type.object_id_path2%type
   ) RETURN object_ids_tt IS
     l_jval       rf_json_value;
     l_id_list    rf_json_list;
     l_obj_ids    object_ids_tt;
   BEGIN
      l_obj_ids:=object_ids_tt();
      IF par_object_id_path IS NOT NULL AND par_json IS NOT NULL THEN
        l_jval:=rf_json_ext.get_json_value(par_json, par_object_id_path);
        IF l_jval IS NOT NULL THEN 
           CASE l_jval.get_type 
            WHEN 'string' THEN
              l_obj_ids.extend;
              l_obj_ids(l_obj_ids.last):=l_jval.get_string;
            WHEN 'number' THEN
              l_obj_ids.extend;
              l_obj_ids(l_obj_ids.last):=TO_CHAR(l_jval.get_number);
            WHEN 'array'  THEN
              -- ���� �� �������
              l_id_list:=rf_json_list(l_jval);
              FOR i IN 1..l_id_list.count 
                LOOP
                  -- �������� ������� �������
                  l_jval:=l_id_list.get(i);
                  CASE l_jval.get_type 
                     WHEN 'string' THEN
                       l_obj_ids.extend;
                       l_obj_ids(l_obj_ids.LAST):=l_jval.get_string;
                     WHEN 'number' THEN
                       l_obj_ids.extend;
                       l_obj_ids(l_obj_ids.LAST):=TO_CHAR(l_jval.get_number);
                     WHEN 'object' THEN
                       l_obj_ids:=l_obj_ids MULTISET UNION ALL get_object_ids(rf_json(l_jval),par_object_id_path2,NULL);-- ����������� ����� �� ������� ����������
                     ELSE 
                       NULL;
                  END CASE;     
                END LOOP;
              
            WHEN 'object' THEN
              l_obj_ids:=get_object_ids(rf_json(l_jval),par_object_id_path2,NULL);-- ����������� ����� �� ������� ����������
            ELSE
              NULL;
          END CASE; 
       END IF;   
     END IF;
     RETURN l_obj_ids; 
   END get_object_ids;

  --
  -- �������� � ������ ������ �������
  --
  FUNCTION record_event
  (
    par_user_login       rf_audit_event.user_login%TYPE,     -- ��� �������� ������������ (������������ ������)
    par_ip_address       rf_audit_event.ip_address%TYPE,     -- IP ����� �������� ������������
    par_event_type_cd    rf_audit_event.event_type_cd%TYPE,  -- ��� ���� �������
    par_event_params     rf_json,                            -- ��������� ������� � ������� json, ��� ������� �� GUI (service.jsp) - ������
    par_gui_flag         INTEGER DEFAULT 0,                               -- ���� 1/0, ��� ��� ������� �� GUI (service.jsp), par_event_params �������� ���� ������
    par_event_descr_tx   rf_audit_event.event_descr_tx%TYPE DEFAULT null, -- �������� ������� - �����������, ���� ���������� ��� ��������� �������� �������, ����� ����������� � ����������� � �����������
    par_err_type_cd      rf_audit_event.err_type_cd%TYPE DEFAULT null,    -- ������� ������: null/A/T - �����������, ���� ���������� ���������� ������� � �������
    par_message_tx       rf_audit_event.message_tx%TYPE DEFAULT null      -- ����� ��������� (� ���������, �� ������) - �����������, ���� ���������� ���������� ������� � ����������
  ) 
  RETURN mantas.rf_audit_event.audit_seq_id%TYPE IS -- ID ��������� ������ � ������� ������
    l_event_type      rf_audit_event_type%ROWTYPE; 
    l_event           rf_audit_event%ROWTYPE;  
    l_obj             rf_json_value; 
    l_obj_ids         object_ids_tt;
    l_parsed_old_sql  rf_tab_varchar;
    l_old_sql_name    VARCHAR2(2000);
    l_old_sql         VARCHAR2(16000);
    l_bind_json       rf_json;
    l_bind_str        VARCHAR2(2000);
    l_old_sql_jlist   rf_json_list;
    l_old_sql_json    rf_json;
  BEGIN
    -- �������� �������� ���� ������� �� ����
    l_event_type:=get_audit_event_type(par_event_type_cd);
    
    -- ��������� ����������� ���� 
    l_event.user_login:=par_user_login;
    l_event.ip_address:=par_ip_address;
    l_event.event_type_cd:=par_event_type_cd;
    l_event.err_type_cd:=par_err_type_cd;
    l_event.message_tx:=par_message_tx; 
    
    IF par_event_params IS NOT NULL THEN 
        dbms_lob.createtemporary(l_event.event_params, TRUE);
        par_event_params.to_clob(l_event.event_params,TRUE);
    END IF;    
    
    -- �������� �������
    IF par_event_descr_tx IS NOT NULL THEN 
      -- ���������� �������� ���� �� ���
      l_event.event_descr_tx:=par_event_descr_tx;
    ELSIF l_event_type.event_descr_sql IS NOT NULL THEN
      -- ��������� �������� ���
      BEGIN
        EXECUTE IMMEDIATE l_event_type.event_descr_sql USING IN par_event_params,OUT l_event.event_descr_tx;
      EXCEPTION 
        WHEN OTHERS THEN
          -- ���� ��� ��������� �������� ����, �������� - ������, � ������������ ������
          l_event.event_descr_tx:=NULL;
          record_internal_error_event(par_user_login     => par_user_login
                         ,par_ip_address     => par_ip_address
                         ,par_event_params   => par_event_params
                         ,par_event_descr_tx => '��� ������� ��������� PL/SQL �� ���� EVENT_DESCR_SQL ��� ���� ������� '||par_event_type_cd||', ��������� ������ Oracle'
                         ,par_message_tx     => DBMS_UTILITY.FORMAT_ERROR_STACK);
      END;  
    END IF;  
    -- ���� ��� � �� ��������� �� ��������� ����������� ��������� �� �����������
    IF l_event.event_descr_tx IS NULL  AND l_event_type.event_descr_tx IS NOT NULL THEN
      l_event.event_descr_tx:=l_event_type.event_descr_tx ;
    END IF;  
    
    -- ��������� ��� � id �������
    l_event.object_type_cd:=l_event_type.object_type_cd;
    l_obj_ids:=get_object_ids(par_event_params,l_event_type.object_id_path,l_event_type.object_id_path2);
    -- ���� ������ ���� �������, �� �������� ��� � ������ ������
    IF l_obj_ids.COUNT=1 THEN 
      l_event.object_id:=l_obj_ids(1);
    END IF;
    
    -- ��������� ������ �����������, ���� ���� ����� �������� � ���������
    IF l_event_type.old_values_sql IS NOT NULL  AND l_obj_ids.COUNT>0 THEN
      -- ��������� �� �������
     l_parsed_old_sql:=rf_pkg_scnro.list_to_tab(l_event_type.old_values_sql,'##');
     
     -- �������� ������ ����������
     l_bind_json:=rf_json();
     FOR i IN 1..l_obj_ids.LAST 
       LOOP
         l_bind_str:=l_bind_str||',:p_'||TO_CHAR(i);
         l_bind_json.put('p_'||TO_CHAR(i),l_obj_ids(i));
       END LOOP;
     l_bind_str:=SUBSTR(l_bind_str,2);
     
     l_old_sql_json:=rf_json();
     -- ���� �� ��������
     FOR i IN 0.. (l_parsed_old_sql.COUNT/2-1)
       LOOP
         -- �������� ��� ������� � ����� �������
         l_old_sql_name:=l_parsed_old_sql(1+i*2);
         l_old_sql:=REPLACE(l_parsed_old_sql(2+i*2),'#OBJECT_IDS#',l_bind_str);

         -- ��������� ������
         BEGIN 
           l_old_sql_jlist := rf_json_dyn.executelist( l_old_sql, l_bind_json);
           l_old_sql_json.put(l_old_sql_name,l_old_sql_jlist);
         EXCEPTION 
           WHEN OTHERS THEN
             record_internal_error_event(par_user_login     => par_user_login
                                        ,par_ip_address     => par_ip_address
                                        ,par_event_params   => par_event_params
                                        ,par_event_descr_tx => '��� ������� ��������� SQL('||l_old_sql||') �� ���� OLD_VALUES_SQL ��� ���� ������� '||par_event_type_cd||', ��������� ������ Oracle'
                                        ,par_message_tx     => DBMS_UTILITY.FORMAT_ERROR_STACK);
         END;  
       END LOOP;
       
       -- �������� ��������� � CLOB
       dbms_lob.createtemporary(l_event.old_values, TRUE);
       l_old_sql_json.TO_CLOB(l_event.old_values, TRUE);
       
    END IF;
    
    RETURN create_audit_event(l_event);
  END record_event;
  --
  -- ���������� � ��� ������������ ������ ������� ������ ��������� ���������� �������
  --
  PROCEDURE set_event_result
  (
    par_audit_seq_id     rf_audit_event.audit_seq_id%TYPE,  -- ID ������ ������� ������
    par_err_type_cd      rf_audit_event.err_type_cd%TYPE,   -- ������� ������: null/A/T 
    par_message_tx       rf_audit_event.message_tx%TYPE     -- ����� ��������� (� ���������, �� ������)
  ) IS
  BEGIN
    -- ��������� ������������ ������ ������
    IF is_audit_event_valid(par_audit_seq_id)
      THEN
        -- ��������� ������ � ������� ������
        UPDATE rf_audit_event e
          SET e.err_type_cd=par_err_type_cd
             ,e.message_tx= par_message_tx
          WHERE e.audit_seq_id=par_audit_seq_id AND e.event_time=g_last_event_time;
        COMMIT;  
      ELSE
        -- ������������ ������
        record_internal_error_event(par_user_login     => NULL
                                   ,par_ip_address     => NULL
                                   ,par_event_params   => NULL
                                   ,par_event_descr_tx => '������� �������������� �������������� ������ ������ � audit_seq_id='||TO_CHAR(par_audit_seq_id)
                                   ,par_message_tx     => '������� �������������� �������������� ������ ������ ������ � audit_seq_id='||TO_CHAR(par_audit_seq_id));

    END IF;
  END set_event_result;
  --
  -- ������� ������ � ������� RF_AUDIT_EVENT
  --
  FUNCTION create_audit_event
  (
    par_event            rf_audit_event%ROWTYPE -- ������ � ��������� �������
  )   
  RETURN mantas.rf_audit_event.audit_seq_id%TYPE IS -- ID ��������� ������ � ������� ������
  PRAGMA AUTONOMOUS_TRANSACTION;
    l_event rf_audit_event%ROWTYPE;
  BEGIN
    l_event:=par_event;
    -- ��������� ������ ����
    l_event.audit_seq_id:=rf_audit_event_seq.NEXTVAL;
    l_event.event_time:=sysdate;
    -- ���������� ��������� �������
    INSERT INTO mantas.rf_audit_event VALUES l_event;
    -- �������� ������ ���������� �������
    g_last_audit_seq_id:=l_event.audit_seq_id;   
    g_last_event_time:=l_event.event_time;
    g_last_event_type:=l_event.event_type_cd;
    g_last_user_login:=l_event.user_login;
    COMMIT;
    RETURN l_event.audit_seq_id;
  END create_audit_event;
  
  --
  -- �������� ������������ �������� ��� �������/��� (�� ����)
  --
  FUNCTION get_actvy_name
  (
    par_actvy_type_cd      kdd_activity_type_cd.actvy_type_cd%TYPE -- ��� �������� ��� �������/���
  )
  RETURN kdd_activity_type_cd.actvy_type_nm%TYPE IS -- ������������ ��������
  BEGIN
    -- ��������� ���� �� ������ � ����
    IF g_activity_types.EXISTS(par_actvy_type_cd) THEN
      RETURN g_activity_types(par_actvy_type_cd);
    END IF;
    RETURN NULL;
  END get_actvy_name;

  --
  -- �������� � ��� ������������ ������ ������� ������ ��������� ������
  --
  PROCEDURE add_search_params
  (
    par_audit_seq_id      rf_audit_event.audit_seq_id%TYPE,                         -- ID ������ ������� ������
    par_search_type_tx    rf_audit_search_params.search_type_tx%TYPE DEFAULT NULL,  -- ����������/����������/���������� ��� ���������� 
    par_last_nm           rf_audit_search_params.last_nm%TYPE DEFAULT NULL,         -- �������, �� ������� ���������� �����
    par_first_nm          rf_audit_search_params.first_nm%TYPE DEFAULT NULL,        -- ���, �� ��������  ���������� �����
    par_midl_nm           rf_audit_search_params.midl_nm%TYPE DEFAULT NULL,         -- ��������, �� ��������  ���������� �����
    par_birth_dt          rf_audit_search_params.birth_dt%TYPE DEFAULT NULL,        -- ���� ��������, �� �������  ���������� �����
    par_doc_type_cd       rf_audit_search_params.doc_type_cd%TYPE DEFAULT NULL,     -- ��� ���, �� ��������  ���������� �����
    par_doc_series_id     rf_audit_search_params.doc_series_id%TYPE DEFAULT NULL,   -- ����� ���, �� �������  ���������� �����
    par_doc_no_id         rf_audit_search_params.doc_no_id%TYPE DEFAULT NULL,       -- ����� ���, �� ��������  ���������� �����
    par_acct_nb           rf_audit_search_params.acct_nb%TYPE DEFAULT NULL,         -- ����� �����, �� ��������  ���������� �����
    par_inn_nb            rf_audit_search_params.inn_nb%TYPE DEFAULT NULL,          -- ���, �� ��������  ���������� �����
    par_ogrn_nb           rf_audit_search_params.ogrn_nb%TYPE DEFAULT NULL,         -- ����, �� ��������  ���������� �����
    par_okpo_nb           rf_audit_search_params.okpo_nb%TYPE DEFAULT NULL,         -- ����, �� ��������  ���������� �����
    par_watch_list_cd     rf_audit_search_params.watch_list_cd%TYPE DEFAULT NULL,   -- ��� ������� �����������, �� ��������  ���������� �����
    par_cust_seq_id       rf_audit_search_params.cust_seq_id%TYPE DEFAULT NULL     -- ID �������, �� ��������  ���������� �����
  ) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    -- ��������� ������������ ������ ������
    IF is_audit_event_valid(par_audit_seq_id)
      THEN
        -- ��������� ������ � ������� 
        INSERT INTO rf_audit_search_params (audit_seq_id,event_time,user_login,search_type_tx,last_nm,first_nm,midl_nm,birth_dt,doc_type_cd,doc_series_id,doc_no_id,acct_nb,inn_nb,ogrn_nb,okpo_nb,watch_list_cd,cust_seq_id)
          VALUES(par_audit_seq_id,g_last_event_time,g_last_user_login,par_search_type_tx,par_last_nm,par_first_nm,par_midl_nm,par_birth_dt,par_doc_type_cd,par_doc_series_id,par_doc_no_id,par_acct_nb,par_inn_nb,par_ogrn_nb,par_okpo_nb,par_watch_list_cd,par_cust_seq_id);
        COMMIT;  
      ELSE
        -- ������������ ������
        record_internal_error_event(par_user_login     => NULL
                                   ,par_ip_address     => NULL
                                   ,par_event_params   => NULL
                                   ,par_event_descr_tx => '������� �������� ��������� ������ � �������������� ������ ������ � audit_seq_id='||TO_CHAR(par_audit_seq_id)
                                   ,par_message_tx     => '������� �������� ��������� ������ � �������������� ������ ������ � audit_seq_id='||TO_CHAR(par_audit_seq_id));

    END IF;
  END add_search_params;
  
  --
  -- ��������� ������������� ���� 
  --
  PROCEDURE init IS
    BEGIN
      -- ������� ��������� ������
      g_last_audit_seq_id:=NULL;
      g_last_event_time:=NULL;
      g_audit_event_types.DELETE;
      g_activity_types.DELETE;
      -- �������� � ��������� ������ ������� rf_audit_event_type
      FOR c_et IN (SELECT * FROM rf_audit_event_type)
        LOOP
          g_audit_event_types(c_et.event_type_cd):=c_et;
        END LOOP;
      -- �������� � ��������� ������ ������� kdd_activity_type_cd
      FOR c_at IN (SELECT at.actvy_type_cd, at.actvy_type_nm FROM kdd_activity_type_cd at)
        LOOP
          g_activity_types(c_at.actvy_type_cd):=c_at.actvy_type_nm;
        END LOOP;
    END init;    

BEGIN
  init;
END rf_pkg_audit;
/