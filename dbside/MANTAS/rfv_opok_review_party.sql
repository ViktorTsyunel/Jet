grant EXECUTE on BUSINESS.rfv_trxn_party to MANTAS with grant option;

CREATE OR REPLACE FORCE VIEW mantas.rfv_opok_review_party as
SELECT /*+ ORDERED USE_NL(trxn_party) */
       rv.review_id             as review_id, 
       rv.status_cd             as rv_status_cd,
       rv.status_dt             as rv_status_dt,
       rv.creat_ts              as rv_creat_ts,
       rv.creat_id              as rv_creat_id,
       rv.due_dt                as rv_due_dt,
       rv.owner_seq_id          as rv_owner_seq_id, 
       rv.owner_org             as rv_owner_org,
       rv.orig_owner_seq_id     as rv_orig_owner_seq_id, 
       rv.rf_object_tp_cd       as rv_object_tp_cd, 
       rv.rf_op_cat_cd          as rv_op_cat_cd,
       rv.rf_trxn_seq_id        as rv_trxn_seq_id,
       rv.rf_trxn_scrty_seq_id  as rv_trxn_scrty_seq_id,
       rv.rf_repeat_nb          as rv_repeat_nb,
       rv.rf_data_dump_dt       as rv_data_dump_dt,
       rv.rf_branch_id          as rv_branch_id,
       rv.last_actvy_type_cd    as rv_last_actvy_type_cd,
       rv.prcsng_batch_cmplt_fl as rv_prcsng_batch_cmplt_fl,
       rv.rf_cls_dt             as rv_cls_dt,
       rv.cls_id                as rv_cls_id,       
       rv.cls_actvy_type_cd     as rv_cls_actvy_type_cd,
       rv.cls_class_cd          as rv_cls_class_cd,
       rv.review_type_cd        as rv_review_type_cd, 
       rv.rf_opok_nb            as opok_nb, 
       rv.rf_add_opoks_tx       as add_opoks_tx,
       trxn_party.* 
  FROM mantas.kdd_review rv
       LEFT JOIN TABLE(business.rfv_trxn_party(rv.rf_op_cat_cd, rv.rf_trxn_seq_id, rv.rf_data_dump_dt)) trxn_party ON 1 = 1
 WHERE rv.rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/;

GRANT SELECT on mantas.rfv_opok_review_party to KDD_ALGORITHM;
GRANT SELECT on mantas.rfv_opok_review_party to KDD_MINER;
GRANT SELECT on mantas.rfv_opok_review_party to MANTAS_LOADER;
GRANT SELECT on mantas.rfv_opok_review_party to MANTAS_READER;
GRANT SELECT on mantas.rfv_opok_review_party to RF_RSCHEMA_ROLE;
GRANT SELECT on mantas.rfv_opok_review_party to BUSINESS;
GRANT SELECT on mantas.rfv_opok_review_party to CMREVMAN with grant option;
GRANT SELECT on mantas.rfv_opok_review_party to KDD_MNR with grant option;
GRANT SELECT on mantas.rfv_opok_review_party to KDD_REPORT with grant option;
GRANT SELECT on mantas.rfv_opok_review_party to KYC;
GRANT SELECT on mantas.rfv_opok_review_party to STD;
GRANT SELECT on mantas.rfv_opok_review_party to KDD_ALG;

