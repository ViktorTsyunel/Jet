begin
  execute immediate 'drop type mantas.rf_tab_oes_change';
  exception
    when others then null;
end;
/

create or replace type mantas.rf_type_oes_change as object (
  oes_321_id    number,
  block_nb      number,
  batch_date    date,
  modified_by   varchar2(255 char),
  actvy_type_cd varchar2(10 char),
  ip_address    varchar2(64 char),
  column_cd     varchar2(64 char),
  changed_flag  varchar2(1 char),
  old_value     varchar2(4000 char),
  new_value     varchar2(4000 char)
)
/

create or replace type mantas.rf_tab_oes_change as table of mantas.rf_type_oes_change
/


create or replace function mantas.rfv_oes_changes (
  p_oes_321_id in number,
  p_batch_date in date default null,
  p_output_sql in varchar2 default 'N'
) return mantas.rf_tab_oes_change pipelined
is
  c_oes_field_list constant varchar2(128) := 'oes_321_id, -1 block_nb, batch_date, modified_by, actvy_type_cd, ip_address';
  c_oes_party_field_list constant varchar2(128) := 'oes_321_id, block_nb, batch_date, modified_by, actvy_type_cd, ip_address';

  type v_cursor is ref cursor;
  v_search_cursor v_cursor;
  v_sql varchar2(32000);
  v_sql_oper varchar2(16000);
  v_sql_party varchar2(16000);
  v_change mantas.rf_type_oes_change;
  v_old_field_list varchar2(4000);
  v_new_field_list varchar2(4000);
  v_old_unpivot_list varchar2(4000);
  v_new_unpivot_list varchar2(4000);
  v_count number;
  
  /* ������ OLD � NEW ��������� �������, ��������������� TO_CHAR() � ��������������� � ������ 
   * ��������!!! ������ �������� ������������ � ������ 'NULL', ����� �������� ���� ����� � ���������� unpivot �������.
   */
  procedure get_feild_list(p_table_name in varchar2)
  is
  begin
    select listagg(old_column_name, ', ') within group (order by column_id) old_list,
           listagg(new_column_name, ', ') within group (order by column_id) new_list
      into v_old_field_list, v_new_field_list
      from (select column_id,
                   case 
                     when data_type in ('NUMBER', 'FLOAT') then 'nvl(to_char('||old_column_name||'), ''NULL'') as '||old_column_name 
                     when data_type = 'DATE' then 'nvl(to_char('||old_column_name||', ''DD.MM.YYYY HH24:MI:SS''), ''NULL'') as '||old_column_name
                     when instr(data_type, 'TIMESTAMP') > 0 then  'nvl(to_char('||old_column_name||', ''DD.MM.YYYY HH24:MI:SS.FF''), ''NULL'') as '||old_column_name
                     else 'nvl(to_char('||old_column_name||'), ''NULL'') as '||old_column_name
                   end old_column_name,
                   
                   case 
                     when data_type in ('NUMBER', 'FLOAT') then 'nvl(to_char('||new_column_name||'), ''NULL'') as '||new_column_name
                     when data_type = 'DATE' then 'nvl(to_char('||new_column_name||', ''DD.MM.YYYY HH24:MI:SS''), ''NULL'') as '||new_column_name
                     when instr(data_type, 'TIMESTAMP') > 0 then 'nvl(to_char('||new_column_name||', ''DD.MM.YYYY HH24:MI:SS.FF''), ''NULL'') as '||new_column_name
                     else 'nvl(to_char('||new_column_name||'), ''NULL'') as '||new_column_name
                   end new_column_name
                   
              from (select lower(column_name) old_column_name, 
                           regexp_replace(lower(column_name), '^old_', 'new_') new_column_name,
                           column_id, data_type
                      from all_tab_columns 
                     where table_name = upper(p_table_name)
                       and column_name like 'OLD\_%' escape '\'
                   )
         );
  end;
  

  /* ������ OLD � NEW ��������� �������, ��������������� � ������, ��� UNPIVOT 
   * ��������!!! ������ �������� ������������ � ������ 'NULL', ����� �������� ���� ����� � ���������� unpivot �������.
   */
  procedure get_unpivot_field_list(p_table_name in varchar2)
  is
  begin
    select listagg(old_column_name, ', ') within group (order by column_id) old_list,
           listagg(new_column_name, ', ') within group (order by column_id) new_list
      into v_old_unpivot_list, v_new_unpivot_list
      from (select lower(old_column_name)||' as '''||regexp_replace(old_column_name, '^OLD_')||'''' old_column_name, 
                   lower(new_column_name)||' as '''||regexp_replace(new_column_name, '^NEW_')||'''' new_column_name, 
                   column_id
              from (select column_name old_column_name, 
                           regexp_replace(column_name, '^OLD_', 'NEW_') new_column_name,
                           column_id
                      from all_tab_columns 
                     where table_name = upper(p_table_name) 
                       and column_name like 'OLD\_%' escape '\'
                   )
           );
  end;
  
begin
  v_change := mantas.rf_type_oes_change(null, null, null, null, null, null, null, null, null, null);
  
  select count(*) 
    into v_count
    from mantas.rf_oes_321_changes
   where oes_321_id = p_oes_321_id
     and batch_date = nvl(p_batch_date, batch_date)
     and rownum = 1;
  
  if v_count > 0 then 
    /* ��������� ��������� ��� ������� �� ������� �������� */
    get_feild_list('RF_OES_321_CHANGES');
    get_unpivot_field_list('RF_OES_321_CHANGES');
    /* �������� ������ �� ������� �������� */
    v_sql_oper := q'{
      select t_old.oes_321_id,
             t_old.block_nb,
             t_old.batch_date,
             t_old.modified_by,
             t_old.actvy_type_cd,
             t_old.ip_address,
             t_old.oes_field,
             case
               when t_old.old_value is not null and t_new.new_value is not null and t_old.old_value <> t_new.new_value then 'Y'
               else 'N'
             end changed_flag,  
             t_old.old_value,
             t_new.new_value
        from (select oes_321_id, -1 block_nb, batch_date, modified_by, actvy_type_cd, ip_address, oes_field, old_value
                from (select oes_321_id, -1 block_nb, batch_date, modified_by, actvy_type_cd, ip_address,
                             ##OLD_OES_FIELD_LIST##
                        from mantas.rf_oes_321_changes
                       where oes_321_id = ##P_OES_321_ID##
                         and batch_date = nvl(to_date('##BATCH_DATE##', 'DD.MM.YYYY HH24:MI:SS'), batch_date)
                     )
                unpivot (old_value for oes_field in (##OLD_UNPIVOT_FIELD_LIST##))
             ) t_old
        join (select oes_321_id, -1 block_nb, batch_date, modified_by, actvy_type_cd, ip_address, oes_field, new_value
                from (select oes_321_id, -1 block_nb, batch_date, modified_by, actvy_type_cd, ip_address,
                             ##NEW_OES_FIELD_LIST##
                        from mantas.rf_oes_321_changes
                       where oes_321_id = ##P_OES_321_ID##
                         and batch_date = nvl(to_date('##BATCH_DATE##', 'DD.MM.YYYY HH24:MI:SS'), batch_date)
                     )
                unpivot (new_value for oes_field in (##NEW_UNPIVOT_FIELD_LIST##))
             ) t_new on t_old.oes_field = t_new.oes_field and t_old.batch_date = t_new.batch_date and t_old.block_nb = t_new.block_nb
    }';
    v_sql_oper := replace(v_sql_oper, '##OLD_OES_FIELD_LIST##', v_old_field_list);
    v_sql_oper := replace(v_sql_oper, '##OLD_UNPIVOT_FIELD_LIST##', v_old_unpivot_list);
    v_sql_oper := replace(v_sql_oper, '##NEW_OES_FIELD_LIST##', v_new_field_list);
    v_sql_oper := replace(v_sql_oper, '##NEW_UNPIVOT_FIELD_LIST##', v_new_unpivot_list);
    v_sql_oper := replace(v_sql_oper, '##P_OES_321_ID##', p_oes_321_id);
    v_sql_oper := replace(v_sql_oper, '##BATCH_DATE##', to_char(p_batch_date, 'DD.MM.YYYY HH24:MI:SS'));
  end if;


  select count(*) 
    into v_count
    from mantas.rf_oes_321_party_changes
   where oes_321_id = p_oes_321_id
     and batch_date = nvl(p_batch_date, batch_date)
     and rownum = 1;

   if v_count > 0 then  
    /* ��������� ��������� ��� ������� �� ���������� �������� */
    get_feild_list('RF_OES_321_PARTY_CHANGES');
    get_unpivot_field_list('RF_OES_321_PARTY_CHANGES');
    /* �������� ������ �� ���������� �������� */
    v_sql_party := q'{
      select t_old.oes_321_id,
             t_old.block_nb,
             t_old.batch_date,
             t_old.modified_by,
             t_old.actvy_type_cd,
             t_old.ip_address,
             t_old.oes_field,
             case
               when t_old.old_value is not null and t_new.new_value is not null and t_old.old_value <> t_new.new_value then 'Y'
               else 'N'
             end changed_flag,  
             t_old.old_value,
             t_new.new_value
        from (select oes_321_id, block_nb, batch_date, modified_by, actvy_type_cd, ip_address, oes_field, old_value
                from (select oes_321_id, block_nb, batch_date, modified_by, actvy_type_cd, ip_address,
                             ##OLD_OES_FIELD_LIST##
                        from mantas.rf_oes_321_party_changes
                       where oes_321_id = ##P_OES_321_ID##
                         and batch_date = nvl(to_date('##BATCH_DATE##', 'DD.MM.YYYY HH24:MI:SS'), batch_date)
                     )
                unpivot (old_value for oes_field in (##OLD_UNPIVOT_FIELD_LIST##))
             ) t_old
        join (select oes_321_id, block_nb, batch_date, modified_by, actvy_type_cd, ip_address, oes_field, new_value
                from (select oes_321_id, block_nb, batch_date, modified_by, actvy_type_cd, ip_address,
                             ##NEW_OES_FIELD_LIST##
                        from mantas.rf_oes_321_party_changes
                       where oes_321_id = ##P_OES_321_ID##
                         and batch_date = nvl(to_date('##BATCH_DATE##', 'DD.MM.YYYY HH24:MI:SS'), batch_date)
                     )
                unpivot (new_value for oes_field in (##NEW_UNPIVOT_FIELD_LIST##))
             ) t_new on t_old.oes_field = t_new.oes_field and t_old.batch_date = t_new.batch_date and t_old.block_nb = t_new.block_nb
    }';
    v_sql_party := replace(v_sql_party, '##OLD_OES_FIELD_LIST##', v_old_field_list);
    v_sql_party := replace(v_sql_party, '##OLD_UNPIVOT_FIELD_LIST##', v_old_unpivot_list);
    v_sql_party := replace(v_sql_party, '##NEW_OES_FIELD_LIST##', v_new_field_list);
    v_sql_party := replace(v_sql_party, '##NEW_UNPIVOT_FIELD_LIST##', v_new_unpivot_list);
    v_sql_party := replace(v_sql_party, '##P_OES_321_ID##', p_oes_321_id);
    v_sql_party := replace(v_sql_party, '##BATCH_DATE##', to_char(p_batch_date, 'DD.MM.YYYY HH24:MI:SS'));
  end if;

  /* ���������� ������� */
  if v_sql_oper is not null and v_sql_party is not null then
    v_sql := v_sql_oper||chr(13)||'union all'||chr(13)||v_sql_party;
  else
    v_sql := v_sql_oper||v_sql_party;
  end if;

  /* Debug p_output_sql = 'Y' */
  if p_output_sql = 'Y' then
    dbms_output.put_line(v_sql);
    else
      if v_sql is not null then
        open v_search_cursor for v_sql;
        loop
          fetch v_search_cursor into 
            v_change.oes_321_id,
            v_change.block_nb,
            v_change.batch_date,
            v_change.modified_by,
            v_change.actvy_type_cd,
            v_change.ip_address,
            v_change.column_cd,
            v_change.changed_flag,
            v_change.old_value,
            v_change.new_value;
          exit when v_search_cursor%NOTFOUND;
          pipe row(v_change);
        end loop;
        close v_search_cursor;
    end if;    
  end if;
end;
/