grant execute on std.pkg_log to mantas;

create or replace package mantas.rf_pkg_assign IS
--
-- �������� ID ��������� ������� �� ���������
--
FUNCTION def_owner RETURN kdd_review_owner.owner_seq_id%TYPE RESULT_CACHE;
--
-- �������� ��� ������������� ��������� ������� �� ���������
--
FUNCTION def_owner_org RETURN kdd_review_owner.rptg_group_cd%TYPE RESULT_CACHE;
--
-- �������� ����� (������������ ���������� �������) ��� ���������� ������������
--
FUNCTION get_owner_limit 
(
  par_owner_seq_id       kdd_review_owner.owner_seq_id%TYPE, -- ID ������������, ��� �������� ����� ���������� ����������� �����
  par_limit_kind         VARCHAR2,                           -- ��� ������: 'F' - ������, '1' - ������ �������
  par_date               DATE                                -- ����, �� ������� ���������� ����������� �����
)     
RETURN INTEGER RESULT_CACHE; -- �������� ������ ���������� ����, ��� ���������� ������������, ������������ �� ��������� ����
--
-- ��������� ������������� ��� ����� ��������� �������
--
FUNCTION assign_alerts RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
end rf_pkg_assign;
/
create or replace package body mantas.rf_pkg_assign IS
--
-- ��������� ������������� ������� �� ������������� � ������������ � ����������� ��������� ������ (#wrk).
-- ���������� ������������� �������� � rf_assign_result#wrk
--
PROCEDURE assign_alerts#wrk
(
  par_order_nb          INTEGER    -- ����� ������� ������������� - 1 ��� 2
);
--
-- �������� ID ��������� ������� �� ���������
--
FUNCTION def_owner RETURN kdd_review_owner.owner_seq_id%TYPE RESULT_CACHE IS
  var_owner_seq_id   kdd_review_owner.owner_seq_id%TYPE;
BEGIN
  SELECT /*+ NO_PARALLEL*/ max(owner_seq_id)
    INTO var_owner_seq_id
    FROM kdd_review_owner
   WHERE owner_id = rf_pkg_adm.get_install_param(78, 1); -- Alert Owner

  return var_owner_seq_id;
END def_owner;          
--
-- �������� ��� ������������� ��������� ������� �� ���������
--
FUNCTION def_owner_org RETURN kdd_review_owner.rptg_group_cd%TYPE RESULT_CACHE IS
  var_owner_org   kdd_review_owner.rptg_group_cd%TYPE;
BEGIN
  SELECT /*+ NO_PARALLEL*/ max(rptg_group_cd)
    INTO var_owner_org
    FROM kdd_review_owner
   WHERE owner_id = rf_pkg_adm.get_install_param(78, 1); -- Alert Owner

  return var_owner_org;
END def_owner_org;          
--
-- �������� ����� (������������ ���������� �������) ��� ���������� ������������
--
FUNCTION get_owner_limit 
(
  par_owner_seq_id       kdd_review_owner.owner_seq_id%TYPE, -- ID ������������, ��� �������� ����� ���������� ����������� �����
  par_limit_kind         VARCHAR2,                           -- ��� ������: 'F' - ������, '1' - ������ �������
  par_date               DATE                                -- ����, �� ������� ���������� ����������� �����
)     
RETURN INTEGER RESULT_CACHE IS -- �������� ������ ���������� ����, ��� ���������� ������������, ������������ �� ��������� ����
  var_result   INTEGER;
BEGIN
  --
  -- ����� ����������� ������� ���������� �������� ������������. 
  -- ������ ������������� ������� (� ������� �����������)
  -- 1) ��� ����������� ������������
  -- 2) ��� ���� - ��������� ��� ���� �������������, ������ ��������� ����
  -- 3) �� ���������
  -- ����� ������� ������ ���������� ���������� �����������
  --
  FOR r IN (SELECT /*+ NO_PARALLEL*/
                   case limit_tp_cd
                     when 'U' then 1 
                     when 'R' then 2 
                     when 'D' then 3
                   end as priority,
                   case par_limit_kind
                     when 'F' then full_limit_qt
                     when '1' then least(order1_limit_qt, full_limit_qt)
                   end as limit_qt    
              FROM rf_assign_limit t
             WHERE par_date between nvl(start_dt, par_date) and nvl(end_dt, par_date) and
                   active_fl = 'Y' and
                   ((limit_tp_cd = 'U' and owner_seq_id = par_owner_seq_id) or
                    (limit_tp_cd = 'R' and EXISTS(select null
                                                    from kdd_review_owner rvo
                                                         join ofsconf.cssms_usr_group_map ug on ug.v_usr_id = rvo.owner_id
                                                         join ofsconf.cssms_group_role_map gr on gr.v_group_code = ug.v_group_code
                                                   where rvo.owner_seq_id = par_owner_seq_id and
                                                         gr.v_role_code = t.role_cd)) or 
                    limit_tp_cd = 'D')
            ORDER BY 1, 2 -- �������� ������������, ����������� �� �������� ������
           ) LOOP
           
    var_result := r.limit_qt;
    EXIT;
  END LOOP;
  
  return var_result;
END get_owner_limit;
--
-- ��������� ������������� ��� ����� ��������� �������
--
FUNCTION assign_alerts RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������ 

  var_sysdate        DATE;
  prcname            VARCHAR2(64) := ' mantas.rf_pkg_assign.assign_alerts';
BEGIN
  std.pkg_log.info('��������� ������������� �� ����� ��������� ������ ...', prcname);
  --
  -- ��������� ������ �������, �� ������� ���������� ��������� �������������: ������� ����� + 6 ����� (����� ������� ����� 18:00 ������������ ��� ���������� �������)
  --
  var_sysdate := trunc(SYSDATE + 6/24);
  --
  -- ���������� ������� ������������� - ���������� 0 � null � ���� ��, ��� (���� ����� ��� �� ����������)
  --
  UPDATE rf_assign_rule
     SET tb_id = decode(tb_id, 0, null, tb_id),
         osb_id = case when osb_id = 0 or tb_id = 0 or tb_id is null
                       then to_number(null)
                       else osb_id
                  end       
   WHERE (tb_id = 0 or osb_id = 0);      
  COMMIT;       
  --
  -- ��������� ������� ������� �� ��������� (���� ��� - ���� �� �������������)
  --
  LOCK TABLE kdd_review IN EXCLUSIVE MODE;
  --
  -- �� ������ ������: ������� ��������� �������
  --
  DELETE FROM rf_assign_alertgroup#wrk;
  DELETE FROM rf_assign_user#wrk;
  DELETE FROM rf_assign_useralert#wrk;
  DELETE FROM rf_assign_result#wrk;  
  --
  -- ��������� ��������� ������� ����� ������� � ������������� � ������������ � ������ ����������������� ��������
  --
  INSERT INTO rf_assign_alertgroup#wrk(group_no, opok_nb, tb_id, osb_id, alert_qt, remnant_qt, done_fl)
    SELECT row_number() over(order by opok_nb, tb_id, osb_id) as group_no,
           t.*
      FROM (select nvl(rf_opok_nb, 0) as opok_nb, 
                   nvl(trunc(rf_branch_id/100000), 0) as tb_id,
                   nvl(rf_branch_id - round(rf_branch_id, -5), 0) as osb_id,
                   count(*) as alert_qt,
                   count(*) as remnant_qt,
                   0 as done_fl
              from kdd_review t
             where case when prcsng_batch_cmplt_fl = 'N'
                        then 1
                   end = 1 and
                   owner_seq_id is null and
                   rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/
            group by nvl(rf_opok_nb, 0), 
                     nvl(trunc(rf_branch_id/100000), 0),
                     nvl(rf_branch_id - round(rf_branch_id, -5), 0)) t;
  --
  -- ��������� ��������� ������� ������������� � ������������ � ��������������, �������������� � ����������� �������� �������������/����������
  -- ������������� ������ ������ �������
  --
  INSERT INTO rf_assign_user#wrk(owner_seq_id, alert_qt, limit_qt, remnant_qt)
    SELECT owner_seq_id, 
           alert_qt,
           limit_qt,
           greatest(limit_qt - alert_qt, 0) as remnant_qt -- ��������� ����� �� ���������� ��� ����������� ������������ �������
      FROM (select owner_seq_id,
                   (select count(*)
                      from kdd_review
                     where owner_seq_id = t.owner_seq_id and
                           creat_ts between trunc(var_sysdate) - 6/24 and trunc(var_sysdate) - 6/24 + 1 - 1/24/3600 and
                           rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/) as alert_qt, -- ���������� ��� ����������� ������������ �������, ��� ���� ���� �� �������� ��������� � ���� �� ��� (trunc(creat_ts + 6/24) = trunc(var_sysdate))
                   nvl(rf_pkg_assign.get_owner_limit(owner_seq_id, '1', var_sysdate), 0) as limit_qt        
              from (select distinct nvl(rp.new_owner_seq_id, rl.owner_seq_id) as owner_seq_id
                      from rf_assign_rule rl
                           left join rf_assign_replacement rp on var_sysdate between nvl(rp.start_dt, var_sysdate) and nvl(rp.end_dt, var_sysdate) and
                                                                 rp.active_fl = 'Y' and
                                                                 rp.owner_seq_id = rl.owner_seq_id
                     where var_sysdate between nvl(rl.start_dt, var_sysdate) and nvl(rl.end_dt, var_sysdate) and
                           rl.active_fl = 'Y') t);
  --
  -- ����: ������� ������ ������� ������������� �������, ����� - ������
  --  
  FOR step IN 1..2 LOOP
    --
    -- ��������� ��������� ������� �������� ������������� � ������� �������: ����� ������ ����� ������������ ����� ������������
    -- �� ������ ����� - � ������������ � ��������� ������ �������, �� ������ - ��������� � ������������ � ��������� ������ �������  
    --
    MERGE INTO rf_assign_useralert#wrk t USING
      (SELECT distinct 
              nvl(rp.new_owner_seq_id, rl.owner_seq_id) as owner_seq_id, 
              ag.group_no
         FROM rf_assign_alertgroup#wrk ag
              join rf_assign_rule rl on var_sysdate between nvl(rl.start_dt, var_sysdate) and nvl(rl.end_dt, var_sysdate) and
                                        rl.active_fl = 'Y' and
                                        rl.order2_fl = decode(step, 1, 'N', 2, 'Y') and -- �� ������ ����� - ������ ������� ������ �������, �� ������ - ������ ������ (������� �� ���������, �������������� ������� ������ ������� �����������)
                                        ((nvl(rl.opok_nb, ag.opok_nb) = ag.opok_nb and
                                          nvl(rl.tb_id, ag.tb_id) = ag.tb_id and
                                          nvl(rl.osb_id, ag.osb_id) = ag.osb_id) or
                                         rl.any_fl = 'Y') and
                                        NOT(rl.any_fl <> 'Y' and rl.opok_nb is null and rl.tb_id is null and rl.osb_id is null) -- �� ��������� ������ �������
              left join rf_assign_replacement rp on var_sysdate between nvl(rp.start_dt, var_sysdate) and nvl(rp.end_dt, var_sysdate) and
                                                    rp.active_fl = 'Y' and
                                                    rp.owner_seq_id = rl.owner_seq_id) v
    ON (v.owner_seq_id = t.owner_seq_id and
        v.group_no = t.group_no)
    WHEN NOT MATCHED THEN
      INSERT(owner_seq_id, group_no)
      VALUES(v.owner_seq_id, v.group_no);
    --
    -- �� ������ ����� ��������� ������ ������ ��� �������������
    --
    if step = 2 Then
      UPDATE rf_assign_user#wrk
         SET limit_qt = nvl(rf_pkg_assign.get_owner_limit(owner_seq_id, 'F', var_sysdate), 0),
             remnant_qt = greatest(nvl(rf_pkg_assign.get_owner_limit(owner_seq_id, 'F', var_sysdate), 0) - alert_qt, 0);
    end if;         
    --
    -- ��������� ������������� ��� ���������������� ������� ����� �������������
    --                                                     
    assign_alerts#wrk(step);                                       
  END LOOP;
  --
  -- ����������� � ������ ������������� � ������������ � ������������ �������������
  --
  FOR r IN (SELECT res.owner_seq_id, 
                   rvo.rptg_group_cd as owner_org, 
                   ag.opok_nb, ag.tb_id, ag.osb_id, 
                   sum(res.alert_qt) as alert_qt
              FROM rf_assign_result#wrk res
                   join rf_assign_alertgroup#wrk ag on ag.group_no = res.group_no
                   join kdd_review_owner rvo on rvo.owner_seq_id = res.owner_seq_id
            GROUP BY res.owner_seq_id, rvo.rptg_group_cd, ag.opok_nb, ag.tb_id, ag.osb_id
            HAVING sum(res.alert_qt) > 0) LOOP

    UPDATE kdd_review 
       SET owner_seq_id      = r.owner_seq_id,
           orig_owner_seq_id = r.owner_seq_id,
           owner_org         = r.owner_org 
     WHERE case when prcsng_batch_cmplt_fl = 'N'
                then 1
           end = 1 and
           owner_seq_id is null and
           rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/ and
           nvl(rf_opok_nb, 0) = r.opok_nb and
           nvl(trunc(rf_branch_id/100000), 0) = r.tb_id and
           nvl(rf_branch_id - round(rf_branch_id, -5), 0) = r.osb_id and
           rownum <= r.alert_qt;            
  END LOOP; 
  --
  -- ���������������� ������ ������� �� ������������ �� ���������
  --
  UPDATE kdd_review 
     SET owner_seq_id      = rf_pkg_assign.def_owner,
         orig_owner_seq_id = rf_pkg_assign.def_owner,
         owner_org         = rf_pkg_assign.def_owner_org         
   WHERE case when prcsng_batch_cmplt_fl = 'N'
              then 1
         end = 1 and
         owner_seq_id is null and
         rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/;
         
  COMMIT;       
  std.pkg_log.info('��������� ��������� ������������� �� ����� ��������� ������', prcname);
  
  return 0;
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ������������� ������� �� �������������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    return 1;
END assign_alerts;                   
--
-- ��������� ������������� ������� �� ������������� � ������������ � ����������� ��������� ������ (#wrk).
-- ���������� ������������� �������� � rf_assign_result#wrk
--
PROCEDURE assign_alerts#wrk
(
  par_order_nb          INTEGER    -- ����� ������� ������������� - 1 ��� 2
) IS

  var_cluster_no        INTEGER;
  var_owner_seq_id      rf_assign_user#wrk.owner_seq_id%TYPE;
  var_group_no          INTEGER;
  var_group_remnant_qt  INTEGER;
  var_alert_qt          INTEGER;
  var_transfer_flag     INTEGER;

  var_count             INTEGER;
  var_count2            INTEGER;
  
  prc_name              VARCHAR2(64) := ' mantas.rf_pkg_assign.assign_alerts#wrk';
BEGIN
  --
  -- ������������ ������������� � ������ ������� �� ���������������� ���������:
  --  1. ���������� ������ ��������, ����� ���������, ���������� � ���������� ������� �������������
  --
  if par_order_nb > 1 Then
    UPDATE /*+ NO_PARALLEL*/ rf_assign_alertgroup#wrk SET cluster_no = null, done_fl = 0;
    UPDATE /*+ NO_PARALLEL*/ rf_assign_user#wrk SET cluster_no = null;
  end if;
  --
  -- 2. ��������� ��������� ����� �������� -1 ��� ����� ������� ��� ������������� � ������������� ��� ����� �������
  --
  UPDATE /*+ NO_PARALLEL*/ rf_assign_alertgroup#wrk ag
     SET cluster_no = -1
   WHERE NOT EXISTS(select null
                      from rf_assign_useralert#wrk
                     where group_no = ag.group_no);
                      
  UPDATE /*+ NO_PARALLEL*/ rf_assign_user#wrk u
     SET cluster_no = -1
   WHERE NOT EXISTS(select null
                      from rf_assign_useralert#wrk
                     where owner_seq_id = u.owner_seq_id);
  --
  -- 3. ����������� ������ ��������� �������������/������� �������
  --    ���� - ���� ���� �� ���������� � ���� ��� ����� �������� ������������
  --
  var_cluster_no := 1;
  WHILE true LOOP
    --
    -- ������ ������-������ ������������, ��� �� ������������ � ��������
    --
    SELECT /*+ NO_PARALLEL*/ min(owner_seq_id)
      INTO var_owner_seq_id
      FROM rf_assign_user#wrk
     WHERE cluster_no is null;
    --
    -- ���� ��� ������������ ��������� � ��������� - �����
    -- 
    if var_owner_seq_id is null Then
      var_cluster_no := var_cluster_no - 1; -- ���������� ��������� ����������� ����� ��������
      EXIT;
    end if;
    --
    -- ����������� ���������� ������������ ����� ����� ��������
    --
    UPDATE /*+ NO_PARALLEL*/ rf_assign_user#wrk
       SET cluster_no = var_cluster_no
     WHERE owner_seq_id = var_owner_seq_id;  
    --
    -- ��� ������ �������/������������, ������� ����� ������� ������� � ���������� ������������, �������� ����� �������
    -- ���� - ���� ��������� ������������ ��� ������ �������, �� ������� ����� ���������, ������� � �������������/����� ������� ��������
    --
    var_count := 0;
    WHILE true LOOP
      UPDATE /*+ NO_PARALLEL*/ rf_assign_alertgroup#wrk
         SET cluster_no = var_cluster_no
       WHERE cluster_no is null and
             group_no in (select ua.group_no
                            from rf_assign_user#wrk u
                                 join rf_assign_useralert#wrk ua on ua.owner_seq_id = u.owner_seq_id
                           where u.cluster_no = var_cluster_no);
      
      if SQL%ROWCOUNT > 0 Then
        UPDATE /*+ NO_PARALLEL*/ rf_assign_user#wrk
           SET cluster_no = var_cluster_no
         WHERE cluster_no is null and
               owner_seq_id in (select ua.owner_seq_id
                                  from rf_assign_alertgroup#wrk ag
                                       join rf_assign_useralert#wrk ua on ua.group_no = ag.group_no
                                 where ag.cluster_no = var_cluster_no);
      end if;
      --
      -- ���� ����� �������������/����� ������� � ������� �������� �� ������� - �����
      --                   
      if SQL%ROWCOUNT <= 0 Then
        EXIT;
      end if;  
      --
      -- ������ �� ������������ �����
      --
      var_count := var_count + 1;
      if var_count > 1000000 Then
        raise_application_error(-20001, '����������� ���� ���������� ���������� ��������'||prc_name);
      end if;  
    END LOOP;
    
    var_cluster_no := var_cluster_no + 1;
    --
    -- ������ �� ������������ �����
    --
    if var_cluster_no > 1000000 Then
      raise_application_error(-20001, '����������� ���� ������������� �������������/����� ������� �� ���������'||prc_name);
    end if;  
  END LOOP;
  --
  -- ������������ ������ �������, ������������� ��������, ����� �������������, ������������� ���� �� ��������
  -- ���� �� ���������
  --
  FOR i_cluster IN 1..var_cluster_no LOOP
    --
    -- ���� 1
    -- ������������ ����� ������������� ����������� ��������� ���������� �������:
    --  � �����:
    --   - ���������� ������ �������, ���������� "�������� ����� ������" - ������ � ����������� ��������� �������:  ��������� ������� ������ ��������� ������������� ����� ���������������� ���������� ������� � ������
    --   - ������������ ��� ������ ������� �� �������������, ��� ���� � ������ ������� ��������� �������� "�������������" ������������� (�� ����������� ���������� �������, ������� ������������ ����� ���������� ������������)
    --
    var_count := 0;
    WHILE true LOOP
      --
      -- ��������� ������ �������, ���������� "�������� ����� ������" ����� ��� �������������� (����������������)
      --
      var_group_no := null;
      FOR r IN (SELECT /*+ NO_PARALLEL*/ ag.group_no, sum(greatest(u.remnant_qt, 0)) - ag.remnant_qt, ag.remnant_qt
                  FROM rf_assign_alertgroup#wrk ag
                       join rf_assign_useralert#wrk ua on ua.group_no = ag.group_no
                       join rf_assign_user#wrk u on u.owner_seq_id = ua.owner_seq_id
                 WHERE ag.cluster_no = i_cluster and
                       ag.remnant_qt > 0 and
                       ag.done_fl = 0                        
                GROUP BY ag.group_no, ag.remnant_qt
                ORDER BY 2) LOOP

        var_group_no := r.group_no;
        var_group_remnant_qt := r.remnant_qt;
        EXIT;
      END LOOP;            
      --
      -- ���� ��� ������ ������� ���������� - �����
      --
      if var_group_no is null Then
        EXIT;
      end if;
      --
      -- ������������ ������ ������ ������� �� �������������
      -- ���� - ���� ���� ��������� ������������ (� ������������� �������) ��� ���� ������ ������� �� ������������ �������  
      -- 
      var_count2 := 0;
      WHILE true LOOP
        --
        -- ��������� ������������, ����������� "�������� ����� ������" (����� ��������� ��� ������ ������ ������� ������������� � ������������� �������)
        -- � ������������ ���������� �������, ������� �� ���� ����� ���������
        --
        var_owner_seq_id := null;
        FOR r IN (SELECT /*+ NO_PARALLEL*/ ua.owner_seq_id, sum(ag2.remnant_qt), least(u.remnant_qt, var_group_remnant_qt) as alert_qt
                    FROM rf_assign_useralert#wrk ua
                         join rf_assign_user#wrk u on u.owner_seq_id = ua.owner_seq_id and
                                                      u.remnant_qt > 0
                         join rf_assign_useralert#wrk ua2 on ua2.owner_seq_id = u.owner_seq_id                             
                         join rf_assign_alertgroup#wrk ag2 on ag2.group_no = ua2.group_no
                   WHERE ua.group_no = var_group_no                       
                  GROUP BY ua.owner_seq_id, least(u.remnant_qt, var_group_remnant_qt)
                  ORDER BY 2) LOOP

          var_owner_seq_id := r.owner_seq_id;
          var_alert_qt := r.alert_qt;
          EXIT;
        END LOOP;
        --
        -- ���� ��������� ������������� ������ ��� - �����
        --
        if var_owner_seq_id is null Then
          EXIT;
        end if;  
        --
        -- ��������� ���������� ���������� ������� ������ ������ �� ������� ������������
        --
        INSERT INTO rf_assign_result#wrk(owner_seq_id, group_no, alert_qt, order_nb)
               VALUES(var_owner_seq_id, var_group_no, var_alert_qt, par_order_nb); 

        UPDATE /*+ NO_PARALLEL*/ rf_assign_user#wrk
           SET alert_qt = alert_qt + var_alert_qt,
               remnant_qt = remnant_qt - var_alert_qt
         WHERE owner_seq_id = var_owner_seq_id;
         
        var_group_remnant_qt := var_group_remnant_qt - var_alert_qt;       
        --
        -- ���� ������ ������� ��������� ������������ - �����
        --
        if var_group_remnant_qt <= 0 Then
          EXIT;
        end if;  
        --
        -- ������ �� ������������ �����
        --
        var_count2 := var_count2 + 1;
        if var_count2 > 1000000 Then
          raise_application_error(-20001, '����������� ���� ������������� ��������� ������ ������� �� �������������'||prc_name);
        end if;  
      END LOOP; -- ���� ������������� ��������� ������ ������� �� ������������� 
      --
      -- �������� ������ ������������, ����������� � ��� ���������������� �������, ��������� � ���������
      --  
      UPDATE /*+ NO_PARALLEL*/ rf_assign_alertgroup#wrk
         SET remnant_qt = var_group_remnant_qt,
             done_fl = 1
       WHERE group_no = var_group_no;  
      --
      -- ������ �� ������������ �����
      --
      var_count := var_count + 1;
      if var_count > 1000000 Then
        raise_application_error(-20001, '����������� ���� �� ������� �������'||prc_name);
      end if;  
    END LOOP; -- ���� �� ������� �������
    --
    -- ���� 2
    -- ����������� ������������� ������� �� ������������� - ������������� ������ �� ����� ����������� � ����� ����������� �������������
    -- ���� - ���� ���� ������������, ���������� ������� � ������� ���� �� �� 2 ������� ������, ��� � ������ �������������, ��� ���� 
    -- �������� �������������� ������� �� ������ �� ������
    -- 
    var_count := 0;
    WHILE true LOOP

      var_transfer_flag := 0;
      FOR r IN (SELECT /*+ NO_PARALLEL*/
                       owner_seq_id,
                       group_no,
                       new_owner_seq_id,
                       -- ���������� ������� ������ ������, �������������� �� ������� ������ ��������������                    
                       -- ����������� ��� ������������������ �� ������ ������ ���������� �������, ���������� �� ���������� ����� �������������
                       -- (�� �� ����� ������� ������ � ������ �������������� � 
                       --  � ���������� ������ ���������� �� �������: ���� 5 ������� ������ �� 2 ����� �������������, �� ����� ���������������� �� 3 + 3, � 3 + 2 ������)
                       case -- ����������� ���������� ������� ��� �� ��������� ������������������ ���������� - ��������� ������� �� �������, ����������� �� ������
                            when group_transfer_qt >= sum(least(round(group_transfer_qt/new_owner_count), new_owner_remnant_qt)) over(partition by owner_seq_id, group_no order by new_owner_seq_id)
                            then least(round(group_transfer_qt/new_owner_count), new_owner_remnant_qt)
                            
                            -- ���������� ������� ��������� ������������������ ���������� - ��������� �������
                            else least(round(group_transfer_qt/new_owner_count), new_owner_remnant_qt) +
                                 group_transfer_qt  - sum(least(round(group_transfer_qt/new_owner_count), new_owner_remnant_qt)) over(partition by owner_seq_id, group_no order by new_owner_seq_id) 
                       end as trasfer_qt, 
                       -- ���������������� ������ �������/��������� �������������� � ������������ ����������� � �������������
                       dense_rank() over(order by group_transfer_qt desc, owner_seq_id, group_no) as rnk
                  FROM (select u1_ag.owner_seq_id, 
                               u1_ag.group_no, 
                               u2.owner_seq_id as new_owner_seq_id,
                               -- ���������� ������� ������ ������, ������� ������� ������������ �� ����� ������������� (���� �������� �� ������)
                               -- ����������� ��� ���������� �������, ������� ���� ���������, ����� ��������� ����� ���������� ������� � ��������� � ����� �������������
                               -- (�� �� ����� ���������� ������� ������ ������, �������������� �������� �������������)
                               least(u1.alert_qt -
                                     round((u1.alert_qt + sum(u2.alert_qt) over(partition by u1_ag.owner_seq_id, u1_ag.group_no)) /
                                           (1 + count(*) over(partition by u1_ag.owner_seq_id, u1_ag.group_no))),  -- ������� ���������� ������� � ��������� � ����� �������������
                                     u1_ag.alert_qt) as group_transfer_qt,
                               count(*) over(partition by u1_ag.owner_seq_id, u1_ag.group_no) as new_owner_count,
                               u2.remnant_qt as new_owner_remnant_qt                         
                          from -- ����� ���������� �������������� ������� �� ������� ��������������/������ �������
                               (select owner_seq_id, 
                                       group_no, 
                                       sum(alert_qt) as alert_qt
                                  from rf_assign_result#wrk
                                 where order_nb = par_order_nb -- � ������������ ��������� ������� ������ ������, �������������� � ������ �������      
                                group by owner_seq_id, group_no
                                having sum(alert_qt) > 0) u1_ag
                               -- �������� ������������� 
                               join rf_assign_user#wrk u1 on u1.owner_seq_id = u1_ag.owner_seq_id 
                               -- ����� ������������� 
                               join rf_assign_user#wrk u2 on u2.owner_seq_id <> u1.owner_seq_id and                      -- ����� ������������� ���������� �� ��������
                                                             u2.remnant_qt > 0 and                                       -- ����� ������ �������������� �� ��������
                                                             u1.alert_qt - u2.alert_qt >= 2 and -- ����� ���������� ������� ������ �������������� ���� �� �� 2 ������, ��� � �������� (�. �. � ���������� �������� ���������� ������������)
                                                             EXISTS(select null
                                                                      from rf_assign_useralert#wrk 
                                                                     where owner_seq_id = u2.owner_seq_id and
                                                                           group_no = u1_ag.group_no))                    -- ����� ������������� ����� ������������ ������ ���� ������ (��� ������� ��������������� ����������� ��������)
                ORDER BY rnk, new_owner_seq_id) LOOP    
        --
        -- ��������� ������ ������ ������ ������ (������ � ������������ ����������� � �������������), ����������������� ��������� ����� ����� �������� �� ������� ��������
        --                         
        if r.rnk > 1 Then
          EXIT;
        end if;  
        --
        -- ��������� ������������ ���������� ������� � ��������� �������������� �� ������
        --
        if r.trasfer_qt > 0 Then
          INSERT INTO rf_assign_result#wrk(owner_seq_id, group_no, alert_qt, order_nb)
                 VALUES(r.owner_seq_id, r.group_no, -r.trasfer_qt, par_order_nb); 

          INSERT INTO rf_assign_result#wrk(owner_seq_id, group_no, alert_qt, order_nb)
                 VALUES(r.new_owner_seq_id, r.group_no, r.trasfer_qt, par_order_nb); 

          UPDATE /*+ NO_PARALLEL*/ rf_assign_user#wrk
             SET alert_qt = alert_qt - r.trasfer_qt,
                 remnant_qt = remnant_qt + r.trasfer_qt
           WHERE owner_seq_id = r.owner_seq_id;
          
          UPDATE /*+ NO_PARALLEL*/ rf_assign_user#wrk
             SET alert_qt = alert_qt + r.trasfer_qt,
                 remnant_qt = remnant_qt - r.trasfer_qt
           WHERE owner_seq_id = r.new_owner_seq_id;

          var_transfer_flag := 1;
        end if;          
      END LOOP;
      --
      -- ���� ��������� �� ���� - ������ ��������� �����������, �����
      --
      if var_transfer_flag = 0 Then
        EXIT;
      end if;  
      --
      -- ������ �� ������������ �����
      --
      var_count := var_count + 1;
      if var_count > 1000000 Then
        raise_application_error(-20001, '����������� ���� �� ��������� ������������������ �� ����� ����������� �������� ������������� � ����� �����������'||prc_name);
      end if;  
    END LOOP; -- ���� �� ��������� ������������������ �� ����� ����������� �������� ������������� � ����� ����������� �����
  END LOOP; -- ���� �� ���������          
END assign_alerts#wrk;
end rf_pkg_assign;
/

