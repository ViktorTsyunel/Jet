--
-- ����� �������� ��������������� �������, ������������ ��� ��������� ������ ���������
--
create or replace package mantas.rf_pkg_rule IS
--
-- ��������� ������ ���������� ������� �� �������������� ���������� ������� ����� ���
-- �� ������� ������ �� ������������ ������� � ��������� �����
--
FUNCTION check_cust_addr
(
  par_cust_seq_id    business.cust.cust_seq_id%TYPE,             -- id �������, ��� �������� ������� ��������� ������
  par_cntry_list     VARCHAR2,                                   -- ������ ����� ����� ����� �������
  par_crit_cd        rf_opok_criteria.crit_cd%TYPE DEFAULT null  -- ��� ������� ������
)
RETURN INTEGER PARALLEL_ENABLE; -- 1 - ���� �� ���� ����� ����� ������ �� ���������� ������ ��� ������������� ���������� ������� ������
                                -- 0 - �� ���� �� ������� �� ���������
                                -- null - ������


/* ����� �������� ������������� ������ ��� ����. ������� � ��������� �����
 * ������� ���������:
 *   par_iso_num - ��� ������ ��� ����. �������
 *   par_metal_flag - 1: ����. ������, 0: ������, null: �� ����� - !!!!���������� ��������, ��������� ������������ check_metal()
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_currency (
  par_iso_num    business.rf_currency.iso_num%type,
  par_metal_flag business.rf_currency.metal_flag%type default 0
) return number result_cache parallel_enable;

/* ����� �������� ������������� ����. ������� � ��������� �����
 * ������� ���������:
 *   par_metal_code - ��� ����. �������
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_metal (
  par_metal_code    business.rf_currency.metal_code%type
) return number result_cache parallel_enable;


/* ����� �������� ������� � ��������� �����
 * ������� ���������:
 *   par_iso_num - ������ ��� ����� ���� ������� �� �� �����
 *   par_metal_flag - ������ ��� ����� ���� ������� �� �� ����� 
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_region( 
  par_okato_cd varchar2 default null,
  par_kladr_cd varchar2 default null    
) return number result_cache parallel_enable;


/* ����� �������� ������ � ��������� �����
 * ������� ���������:
 *   par_iso_numeric_cd - �������� ��� ISO ������ (3 �����)
 *   par_cntry_cd - ��� ������ (2 �������)
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_country (
  par_iso_numeric_cd business.rf_country.iso_numeric_cd%type default null,
  par_cntry_cd       business.rf_country.cntry_cd%type default null
) return number result_cache parallel_enable;


/* ����� �������� ���� ��������������� ��������� � ��������� �����
 * ������� ���������:
 *   par_code_val - ��� ���� ��������� � ������������ � 321-�
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_id_doc_type(
  par_code_val mantas.kdd_code_set_trnln.code_val%type
) return number result_cache parallel_enable;


/* ����� �������� ������������ ����� � ���
 * ������� ���������:
 *   par_inn - ���, 10 ��� 12 ��������
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_inn(
  par_inn varchar2
) return number result_cache parallel_enable;


/* ����� ����������, ��� ������ �������� ������������ (�������� ���� �� ���� ����� �������)
 * ������� ���������:
 *   par_list1 - ������ 1 ��������� ����� �������
 *   par_list2 - ������ 2 ��������� ����� �������
 *   par_case_sensitive - ��������� ���������: 0 - ��� ����� ��������, 1 - � ������ ��������
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_lists_intersection(
  par_list1          varchar2,
  par_list2          varchar2,
  par_case_sensitive integer default 0
) return number result_cache parallel_enable;


/* ����� ����������� ���������� �������� � ������. �������� ������� �� ������������� ��������� ������, 
 * ������ ������������� ������� ������������ ������ ���� ���, ���� ������������� ��������� ���, �� ������������ ������ �������.
 * ������� ���������:
 *   par_string - ������ � ����������
 *   par_delimiter - ������ ����������� (1 ������)
 *   par_case_sensitive - ��������� ���������: 0 - ��� ����� ��������, 1 - � ������ ��������
 * ���������:
 *   rf_tab_varchar - ������� varchar � ����������� �������� 
 */
function dup_in_list_to_tab(
  par_string         varchar2,
  par_delimiter      varchar2 default ',',
  par_case_sensitive integer default 0
) return mantas.rf_tab_varchar deterministic parallel_enable;


/* ����� �������� �������������� ��� � ����� ��������������� ������
 * ������� ���������:
 *   par_bank_code - ��� ��� �����
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function is_sbrf(
  par_bank_code varchar2
) return number result_cache parallel_enable;


/* ����� �������� ��� � �������� rf_cbrf_bnkseek � rf_cbrf_bnkdel
 * ������� ���������:
 *   par_newnum - ���
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_bic(
  par_newnum business.rf_cbrf_bnkseek.newnum%type
) return number result_cache parallel_enable;


/* ����� �������� ���. ���������� 1, ���� ��������� ��� �������� ����������� �� ��������� ����, ����� - ���������� 0.
 * ������� ���������:
 *   par_newnum - ���
 *   par_date - ����
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_bic_active(
  par_newnum business.rf_cbrf_bnkseek.newnum%type,
  par_date   date
) return number result_cache parallel_enable;


/* ����� �������� ���. ���������� 1, ���� ��������� ��� �������� �����������.
 * ������� ���������:
 *   par_newnum - ���
 *   par_regn - ������ ���. �����
 *   par_regn_full_fl - ���� 0/1, ��� � par_regn ������ ������ ���. �����
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_bic_regn(
  par_newnum       business.rf_cbrf_bnkseek.newnum%type, 
  par_regn         business.rf_cbrf_bnkseek.regn%type,
  par_regn_full_fl integer default 0,
  par_filn         varchar2 default null
) return number result_cache parallel_enable;


/* ����� �������� ������������ ����� 20-�� �������� �����. 
 * ������� ���������:
 *   par_acct_nb - ����� �����
 *   par_regn - ����� ���
 * ���������:
 *   1 - �������� ��������
 *   0 - �������� �� ��������
 *   null - �������� �������� ���������
 */
function check_acct_nb(
  par_acct_nb  varchar2, 
  par_bic      varchar2
) return number result_cache parallel_enable;


/* ����� �������� ����. 
 * ������� ���������:
 *   par_okpo - ����� ����
 * ���������:
 *   1 - �������� ��������
 *   0 - �������� �� ��������
 *   null - �������� �������� ���������
 */
function check_okpo(
  par_okpo varchar2
) return number result_cache parallel_enable;

-- 
-- ����������: �������� �� ��������� ���� ��������� ������ ��� (��������������) � �� ��
-- ���� �������� ��������� ������ ���, ���� ��� ���������� ����� ����������� ������� ����_���_��������� � �� ������ � �� ��
-- ���������: 1 - ��, 0 - ���, null - �������� �������� ���������
--
FUNCTION is_goz_acct
(
  par_acct5_nb       VARCHAR2, -- ���������� ����� ����� (������ 5 ����)
  par_bic_nb         VARCHAR2  -- ���/SWIFT �����, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
) 
RETURN INTEGER result_cache parallel_enable;
end rf_pkg_rule;
/

CREATE OR REPLACE package body mantas.rf_pkg_rule IS
--
-- ��� ����������� ������� ��������� (��� ������� check_lexeme � ������)
--
TYPE VARCHAR_CACHE IS TABLE OF VARCHAR2(2000) INDEX BY VARCHAR2(64);
pck_crit_cache       VARCHAR_CACHE;
pck_crit_cache_date  DATE := null; -- SYSDATE �� ������ ������ ������ ������ ������������ ������� � ��� (����� 1 ��� ��� "���������")
--
-- ��������� ������ ���������� ������� �� �������������� ���������� ������� ����� ���
-- �� ������� ������ �� ������������ ������� � ��������� �����
--
FUNCTION check_cust_addr
(
  par_cust_seq_id    business.cust.cust_seq_id%TYPE,             -- id �������, ��� �������� ������� ��������� ������
  par_cntry_list     VARCHAR2,                                   -- ������ ����� ����� ����� �������
  par_crit_cd        rf_opok_criteria.crit_cd%TYPE DEFAULT null  -- ��� ������� ������
)
RETURN INTEGER PARALLEL_ENABLE IS -- 1 - ���� �� ���� ����� ����� ������ �� ���������� ������ ��� ������������� ���������� ������� ������
                                  -- 0 - �� ���� �� ������� �� ���������
                                  -- null - ������
  var_crit_tx    rf_opok_criteria.crit_tx%TYPE := null;
  var_result     INTEGER;
BEGIN
  --
  -- �������� ����������
  --
  if par_cust_seq_id is null or (trim(par_cntry_list) is null and trim(par_crit_cd) is null) Then
    return null;
  end if;
  --
  -- ������� ����� ������� �� �������
  --
  if trim(par_crit_cd) is not null Then
    var_crit_tx := rf_pkg_scnro.get_crit_tx(par_crit_cd, 2);
    if var_crit_tx is null Then
      return null;
    end if;
  end if;    
  --
  -- ���� �� ������� ���������� �������, ��������������� ��������� ��������
  --
  var_result := 0;
  FOR r IN (SELECT /*+ NO_PARALLEL INDEX(cust_addr RF_CUST_ADDR_CUST_USG_I)*/ null 
              FROM business.cust_addr
             WHERE rf_cust_seq_id = par_cust_seq_id and
                   (
                      -- ��� ������ � ������ ����������� ������ �����
                      (trim(par_cntry_list) is not null and
                       addr_cntry_cd in (select trim(column_value)
                                           from table(mantas.rf_pkg_scnro.list_to_tab(upper(par_cntry_list))))) 
                    OR
                      (var_crit_tx is not null and
                        (
                          -- �������� ������: �����, ������, �����, ����� �� ����� � �������� �������
                          ((addr_city_nm is not null or rf_state_nm is not null or addr_rgn_nm is not null or 
                            addr_strt_line1_tx is not null) 
                           and
                           mantas.rf_pkg_scnro.check_lexeme(addr_city_nm||'|'||rf_state_nm||'|'||addr_rgn_nm||'|'||
                                                            addr_strt_line1_tx, 
                                                            var_crit_tx) = 1) 
                         or
                         -- ����� ������� �� ���� � �������� �������
                         (rf_addr_tx is not null and
                          mantas.rf_pkg_scnro.check_lexeme(rf_addr_tx, 
                                                           var_crit_tx) = 1)
                        ) 
                      )
                   ) and
                   rownum <= 1) LOOP                     
             
    var_result := 1;
    EXIT;
  END LOOP;

  return var_result;   
END check_cust_addr;


/* ����� �������� ������������� ������ ��� ����. ������� � ��������� �����
 * ������� ���������:
 *   par_iso_num - �������� ��� ISO ������ (3 �����)
 *   par_metal_flag - 1: ����. ������, 0: ������, null: �� ����� !!!!���������� ��������, ��������� ������������ check_metal()
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_currency (
  par_iso_num    business.rf_currency.iso_num%type,
  par_metal_flag business.rf_currency.metal_flag%type default 0
) return number result_cache parallel_enable
is
  v_value  business.rf_currency.iso_num%type;
  v_return number := null;
begin
  if par_iso_num is not null then
    select max(iso_num) into v_value
      from business.rf_currency
     where iso_num = par_iso_num
       and metal_flag = nvl(par_metal_flag, metal_flag);

    if v_value is not null then
      v_return := 1; 
    else 
      v_return := 0;
    end if;
  end if;    
  
  return v_return;
end;

/* ����� �������� �������������  ������� � ��������� �����
 * ������� ���������:
 *   par_metal_code - ��� ������ 
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_metal (
  par_metal_code    business.rf_currency.metal_code%type
) return number result_cache parallel_enable
is
  v_value  business.rf_currency.iso_num%type;
  v_return number := null;
begin
  if par_metal_code is not null then
    select max(iso_num) into v_value
      from business.rf_currency
     where NVL(metal_code,'A'||iso_num)=par_metal_code
       and metal_flag = 1;
    if v_value is not null then
      v_return := 1; 
    else 
      v_return := 0;
    end if;
  end if;    
  
  return v_return;
end check_metal;


/* ����� �������� ������� � ��������� �����
 * ������� ���������:
 *   par_iso_num - ������ ��� ����� ���� ������� �� �� �����
 *   par_metal_flag - ������ ��� ����� ���� ������� �� �� ����� 
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_region( 
  par_okato_cd varchar2 default null,
  par_kladr_cd varchar2 default null    
) return number result_cache parallel_enable
is
  v_value  business.rf_addr_object.addr_seq_id%type;
  v_return number := null;
begin
  if par_okato_cd is not null or par_kladr_cd is not null then
    select max(addr_seq_id) into v_value
      from business.rf_addr_object
     where substr(okato_cd, 1, 2) = nvl(par_okato_cd, substr(okato_cd, 1, 2))
       and substr(kladr_cd, 1, 2) = nvl(par_kladr_cd, substr(kladr_cd, 1, 2));
    
    if v_value is not null then
      v_return := 1;
    else
      v_return := 0;
    end if;
  end if;
  
  return v_return;
end;


/* ����� �������� ������ � ��������� �����
 * ������� ���������:
 *   par_iso_numeric_cd - �������� ��� ISO ������ (3 �����)
 *   par_cntry_cd - ��� ������ (2 �������)
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_country (
  par_iso_numeric_cd business.rf_country.iso_numeric_cd%type default null,
  par_cntry_cd       business.rf_country.cntry_cd%type default null
) return number result_cache parallel_enable
is
  v_value  business.rf_country.iso_numeric_cd%type;
  v_return number := null;
begin
  if par_iso_numeric_cd is not null or par_cntry_cd is not null then
    select max(iso_numeric_cd) into v_value
      from business.rf_country
     where iso_numeric_cd = nvl(par_iso_numeric_cd, iso_numeric_cd)
       and cntry_cd = nvl(par_cntry_cd, cntry_cd)
       and rownum = 1;

    if v_value is not null then
      v_return := 1;
    else
      v_return := 0;
    end if;
  end if;  
  
  return v_return;
end;


/* ����� �������� ���� ��������������� ��������� � ��������� �����
 * ������� ���������:
 *   par_code_val - ��� ���� ��������� � ������������ � 321-�
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_id_doc_type(
  par_code_val mantas.kdd_code_set_trnln.code_val%type
) return number result_cache parallel_enable
is
  v_value  mantas.kdd_code_set_trnln.code_val%type;
  v_return number := null;
begin
  if par_code_val is not null then
    select max(code_val) into v_value
      from mantas.kdd_code_set_trnln
     where code_val = par_code_val
       and code_set = 'RF_OES_321_IDDOCTYPE'
       and rownum = 1;

    if v_value is not null then
      v_return := 1;
    else
      v_return := 0;
    end if;  
  end if;  
  
  return v_return;
end;


/* ����� �������� ������������ ����� � ���
 * ������� ���������:
 *   par_inn - ���, 10 ��� 12 ��������
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_inn(
  par_inn varchar2
) return number result_cache parallel_enable
is
  v_value number;
  v_return number;
begin
  v_return := 1;
  /* �������� 10-�� �������� ��� */
  if length(par_inn) = 10 then
    v_value := substr(par_inn, 1, 1) * 2  + 
               substr(par_inn, 2, 1) * 4  + 
               substr(par_inn, 3, 1) * 10 + 
               substr(par_inn, 4, 1) * 3  + 
               substr(par_inn, 5, 1) * 5  + 
               substr(par_inn, 6, 1) * 9  + 
               substr(par_inn, 7, 1) * 4  + 
               substr(par_inn, 8, 1) * 6  + 
               substr(par_inn, 9, 1) * 8;
    if mod(mod(v_value, 11), 10) != to_number(substr(par_inn, 10, 1)) then
      v_return := 0;
    end if;
    
  /* �������� 12-�� �������� ��� */
  elsif length(par_inn) = 12 then
    v_value := substr(par_inn,  1, 1) * 7  + 
               substr(par_inn,  2, 1) * 2  + 
               substr(par_inn,  3, 1) * 4  + 
               substr(par_inn,  4, 1) * 10 + 
               substr(par_inn,  5, 1) * 3  + 
               substr(par_inn,  6, 1) * 5  + 
               substr(par_inn,  7, 1) * 9  + 
               substr(par_inn,  8, 1) * 4  + 
               substr(par_inn,  9, 1) * 6  + 
               substr(par_inn, 10, 1) * 8;
    if mod(mod(v_value, 11), 10) != to_number(substr(par_inn, 11, 1)) then
      v_return := 0;
    else
      v_value := substr(par_inn,  1, 1) * 3  + 
                 substr(par_inn,  2, 1) * 7  + 
                 substr(par_inn,  3, 1) * 2  + 
                 substr(par_inn,  4, 1) * 4  + 
                 substr(par_inn,  5, 1) * 10 + 
                 substr(par_inn,  6, 1) * 3  + 
                 substr(par_inn,  7, 1) * 5  + 
                 substr(par_inn,  8, 1) * 9  + 
                 substr(par_inn,  9, 1) * 4  + 
                 substr(par_inn, 10, 1) * 6  + 
                 substr(par_inn, 11, 1) * 8;
      if mod(mod(v_value, 11), 10) != to_number(substr(par_inn, 12, 1)) then
        v_return := 0;
      end if;
    end if;  

  /* ���� ������ ��� �� ����� 10 ��� 12 �������� */
  else
    v_return := null;
  end if;
  
  return v_return;
  
  exception
    when others then return null;
end;


/* ����� ����������, ��� ������ �������� ������������ (�������� ���� �� ���� ����� �������)
 * ������� ���������:
 *   par_list1 - ������ 1 ��������� ����� �������
 *   par_list2 - ������ 2 ��������� ����� �������
 *   par_case_sensitive - ��������� ���������: 0 - ��� ����� ��������, 1 - � ������ ��������
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_lists_intersection(
  par_list1          varchar2,
  par_list2          varchar2,
  par_case_sensitive integer default 0
) return number result_cache parallel_enable
is
  v_value  number;
  v_return number := null;
begin
  if par_list1 is not null and par_list2 is not null then
    select count(*) into v_value
      from ( select column_value 
               from table(mantas.rf_pkg_scnro.list_to_tab(decode(par_case_sensitive, 0, upper(par_list1), par_list1)))
             intersect
             select column_value 
               from table(mantas.rf_pkg_scnro.list_to_tab(decode(par_case_sensitive, 0, upper(par_list2), par_list2)))
           );

    if v_value > 0 then
      v_return := 1;
    else
      v_return := 0;
    end if;
  end if;  

  return v_return;
end;


/* ����� ����������� ���������� �������� � ������. �������� ������� �� ������������� ��������� ������, 
 * ������ ������������� ������� ������������ ������ ���� ���, ���� ������������� ��������� ���, �� ������������ ������ �������.
 * ������� ���������:
 *   par_string - ������ � ����������
 *   par_delimiter - ������ ����������� (1 ������)
 *   par_case_sensitive - ��������� ���������: 0 - ��� ����� ��������, 1 - � ������ ��������
 * ���������:
 *   rf_tab_varchar - ������� varchar � ����������� �������� 
 */
function dup_in_list_to_tab(
  par_string         varchar2,
  par_delimiter      varchar2 default ',',
  par_case_sensitive integer default 0
) return mantas.rf_tab_varchar deterministic parallel_enable
is
  v_table  mantas.rf_tab_varchar := mantas.rf_tab_varchar();
begin
  select column_value bulk collect into v_table
    from table(mantas.rf_pkg_scnro.list_to_tab(decode(par_case_sensitive, 0, upper(par_string), par_string), par_delimiter)) 
   group by column_value
  having count(*)> 1;

  return v_table;
end;


/* ����� �������� �������������� ��� � ����� ��������������� ������
 * ������� ���������:
 *   par_bank_code - ��� ��� �����
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function is_sbrf(
  par_bank_code varchar2
) return number result_cache parallel_enable
is
  v_value  business.org.org_intrl_id%type;
  v_return number := null;
begin
  if par_bank_code is not null then
    select max(org_intrl_id) into v_value
      from business.org
     where org_type_cd = '��' 
       and (   upper(cstm_2_tx) = upper(par_bank_code) --���
            or upper(cstm_4_tx) = upper(par_bank_code) --�����
           );
    
    if v_value > 0 then
      v_return := 1;
    else
      v_return := 0;
    end if;
  end if;
  
  return v_return;
end;


/* ����� �������� ��� � �������� rf_cbrf_bnkseek � rf_cbrf_bnkdel
 * ������� ���������:
 *   par_newnum - ���
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_bic(
  par_newnum business.rf_cbrf_bnkseek.newnum%type
) return number result_cache parallel_enable
is
  v_return number := null;
begin
  if par_newnum is not null then
    select count(newnum) into v_return
      from business.rf_cbrf_bnkseek
     where newnum = par_newnum
       and rownum = 1;
       
    if v_return = 0 then
      select count(newnum) into v_return
        from business.rf_cbrf_bnkdel
       where newnum = par_newnum
         and rownum = 1;
    end if;
  end if;    
  
  return v_return;
end;


/* ����� �������� ���. ���������� 1, ���� ��������� ��� �������� ����������� �� ��������� ����, ����� - ���������� 0.
 * ������� ���������:
 *   par_newnum - ���
 *   par_date - ����
 * ���������:
 *    1 - �������� ��������
 *    0 - �������� �� ��������
 *    null - �������� �������� ���������
 */
function check_bic_active(
  par_newnum business.rf_cbrf_bnkseek.newnum%type,
  par_date   date
) return number result_cache parallel_enable
is
  v_return number := null;
begin
  if par_newnum is not null and par_date is not null then
    select count(newnum) into v_return
      from business.rf_cbrf_bnkseek
     where newnum = par_newnum
       and date_in <= par_date -- �� ��������� ���� �� ��� ����
       and nvl(real, 'ok') <> '����' -- �� �� �������������
       and (    nvl(real, 'ok') not in ('����', '����') 
             or date_ch > par_date ) -- � �� �� �������� ��������/�� ������ ����. ���� �� ��������� ����
       and rownum = 1;
  end if;

  return v_return;
end;


/* ����� �������� ���. ���������� 1, ���� ��������� ��� �������� �����������.
 * ������� ���������:
 *   par_newnum - ���
 *   par_regn - ������ ���. �����
 *   par_regn_full_fl - ���� 0/1, ��� � par_regn ������ ������ ���. �����
 * ���������:
 *   1 - �������� ��������
 *   0 - �������� �� ��������
 *   null - �������� �������� ���������
 */
function check_bic_regn(
  par_newnum       business.rf_cbrf_bnkseek.newnum%type, 
  par_regn         business.rf_cbrf_bnkseek.regn%type,
  par_regn_full_fl integer default 0,
  par_filn         varchar2 default null 
) return number result_cache parallel_enable
is
  v_return number := null;
begin
  if par_newnum is not null and par_regn is not null then
    if par_regn_full_fl = 0 then
      select count(*) into v_return
        from business.rf_cbrf_bnkseek
       where newnum = par_newnum
         and regexp_substr(regn, '[[:digit:]]+') = par_regn
         and rownum = 1;
    else
      select count(*) into v_return
        from business.rf_cbrf_bnkseek
       where newnum = par_newnum
         and regexp_substr(regn, '[[:digit:]]+') = par_regn
         and case
               when regexp_instr(regn, '[[:alpha:]/-]') > 0 then reverse(regexp_substr(reverse(regn), '[[:digit:]]+'))
               else '-1'
             end = nvl(par_filn, '-1')
         and rownum = 1;
          end if;
  end if;
  
  return v_return;
end;


/* ����� �������� ������������ ����� 20-�� �������� �����. 
 * ������� ���������:
 *   par_acct_nb - ����� �����
 *   par_regn - ����� ���
 * ���������:
 *   1 - �������� ��������
 *   0 - �������� �� ��������
 *   null - �������� �������� ���������
 */
function check_acct_nb(
  par_acct_nb  varchar2, 
  par_bic      varchar2
) return number result_cache parallel_enable
is
  type t_array_of_number is varray(23) of integer;
  v_keys          t_array_of_number := t_array_of_number(7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1);
  v_acct          varchar2(23 char);
  v_currency_code varchar2(1 char);
  v_rkc_key       varchar2(3 char);
  v_sum           number := 0;
  v_return        number := null;
begin
  if length(par_acct_nb) = 20 and length(par_bic) = 9 and regexp_instr(par_bic, '[[:alpha:]]') = 0 then
    /* ������������ ��� ������: �- 0, � - 1,  � - 2,  � - 3, � - 4, � - 5, � - 6, � - 7, � - 8, � - 9 */
    v_currency_code := upper(substr(par_acct_nb, 6, 1));
    if v_currency_code = 'A' or v_currency_code = '�'
      then v_currency_code := '0';
    elsif v_currency_code = 'B' or v_currency_code = '�'
      then v_currency_code := '1';
    elsif v_currency_code = 'C' or v_currency_code = '�'
      then v_currency_code := '2';
    elsif v_currency_code = 'E' or v_currency_code = '�'
      then v_currency_code := '3';
    elsif v_currency_code = 'H' or v_currency_code = '�'
      then v_currency_code := '4';
    elsif v_currency_code = 'K' or v_currency_code = '�'
      then v_currency_code := '5';
    elsif v_currency_code = 'M' or v_currency_code = '�'
      then v_currency_code := '6';
    elsif v_currency_code = 'P' or v_currency_code = '�'
      then v_currency_code := '7';
    elsif v_currency_code = 'T' or v_currency_code = '�'
      then v_currency_code := '8';
    elsif v_currency_code = 'X' or v_currency_code = '�'
      then v_currency_code := '9';
    elsif regexp_instr(v_currency_code, '[[:alpha:]]') > 0
      then v_currency_code := null;
    end if;
    
    if v_currency_code is not null then
      /* ����������  �������� ����� ��������� ����������� � ����������� �� ������.
       * ������� ����������� ���� 
       * ����� ��� ������������ � ���������� �� ��� */
      if substr(par_bic, 7) in ('000', '001', '002') then
        v_rkc_key := '0'||substr(par_bic, 5, 2);
      else 
        v_rkc_key := substr(par_bic, 7);
      end if;  
      v_acct := v_rkc_key||substr(par_acct_nb, 1, 5)||v_currency_code||substr(par_acct_nb, 7, 2)||'0'||substr(par_acct_nb, 10);
      /* ���������� ������������ �������:
       *   - ������������ ������� ������� ��������� ������ ����� �� ��������������� ������� �����������
       *   - ����� ������� �������� ���������� ������������ 
       *   - ������� ������ ����������� ����� ���������� �� 3
       *   - ������� ������  ����������� ������������ ����������� � �������� �������� ������������ ����� */
       
      for i in 1..23 loop
        v_sum := v_sum + mod(to_number(substr(v_acct, i, 1)) * v_keys(i) ,10);
      end loop;
      
      if mod(mod(v_sum, 10) * 3, 10) = to_number(substr(par_acct_nb, 9, 1)) then
        v_return := 1;
      else
        v_return := 0;
      end if;  
    end if;  
  end if;
  
  return v_return;
  
  exception
    when others then
      return -1;
end;


/* ����� �������� ����. 
 * ������� ���������:
 *   par_okpo - ����� ����
 * ���������:
 *   1 - �������� ��������
 *   0 - �������� �� ��������
 *   null - �������� �������� ���������
 */
function check_okpo(
  par_okpo varchar2
) return number result_cache parallel_enable
is
  v_return number := null;
  v_sum    number := 0; 
  v_key    number;
begin
  if length(par_okpo) = 8 or length(par_okpo) = 10 then
    for i in 1..length(par_okpo) - 1 loop
      v_sum := v_sum + to_number(substr(par_okpo, i, 1)) * i;
    end loop;
    v_key := mod(v_sum, 11);
    /* ���� ���� = 10, �� �������� ������������������ ����� �� 2 � ��������� ������.
     * ���� ����������� ���� ������ 10, �� ����� ����� ����������� � 1. */
    if v_key = 10 then
      v_sum := 0;
      for i in 1..length(par_okpo) - 1 loop
        v_sum := v_sum + to_number(substr(par_okpo, i, 1)) * case 
                                                               when i+2 > 10 then mod((i+2), 11) + 1
                                                               else mod((i+2), 11)
                                                             end;
      end loop;
      v_key := mod(v_sum, 11);
      /* ���� ���� = 10 �� ���� => 0 */
      if v_key = 10 then
        v_key := 0;
      end if;
    end if;
    
    if v_key = to_number(substr(par_okpo, -1)) then
      v_return := 1;
    else
      v_return := 0;
    end if;
  end if;
  
  return v_return;
end;

-- 
-- ����������: �������� �� ��������� ���� ��������� ������ ��� (��������������) � �� ��
-- ���� �������� ��������� ������ ���, ���� ��� ���������� ����� ����������� ������� ����_���_��������� � �� ������ � �� ��
-- ���������: 1 - ��, 0 - ���, null - �������� �������� ���������
--
FUNCTION is_goz_acct
(
  par_acct5_nb       VARCHAR2, -- ���������� ����� ����� (������ 5 ����)
  par_bic_nb         VARCHAR2  -- ���/SWIFT �����, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
) 
RETURN INTEGER result_cache parallel_enable IS
BEGIN
  --
  -- �������� ����������
  --
  if par_acct5_nb is null or par_bic_nb is null Then
    return null;
  end if;
  --
  -- �������� ���������� ����� �����
  --
  if nvl(rf_pkg_scnro.check_acct_5(par_acct5_nb, rf_pkg_scnro.get_crit_tx('����_���_���������', 1)), 0) <> 1 Then
    return 0;
  end if;
  --
  -- �������� ����, � ������� ������ ����
  --  
  if par_bic_nb = 'SBRF' Then
    return 1;
  else
    return nvl(rf_pkg_rule.is_sbrf(par_bic_nb), 0);
  end if;    
END is_goz_acct;
END rf_pkg_rule;
/

GRANT EXECUTE on mantas.rf_pkg_rule to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_rule to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_rule to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_rule to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_rule to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_rule to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_rule to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_rule to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_rule to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_rule to KYC;
GRANT EXECUTE on mantas.rf_pkg_rule to STD;
GRANT EXECUTE on mantas.rf_pkg_rule to KDD_ALG;
