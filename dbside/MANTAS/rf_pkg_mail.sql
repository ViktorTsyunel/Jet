CREATE OR REPLACE 
PACKAGE mantas.rf_pkg_mail IS
  --
  -- ���� ��� �����������
  --
  TYPE t_attachment IS RECORD(
      filename    VARCHAR2(255 CHAR)
     ,mime_type   VARCHAR2(255 CHAR)
     ,attachment  CLOB
    );
  TYPE t_attachments_tab IS TABLE OF t_attachment INDEX BY BINARY_INTEGER;
  
  --
  -- ��������� ��������� �� e-mail, ����� APEX_MAIL ��� ����������
  -- ��������! �������� COMMIT
  --  
  PROCEDURE send
  (
     p_from    VARCHAR2
    ,p_to      mantas.rf_tab_varchar
    ,p_subject VARCHAR2
    ,p_body    CLOB
  );
  --
  -- ��������� ��������� �� e-mail, ����� APEX_MAIL � �����������
  -- ��������! �������� COMMIT
  --  
  PROCEDURE send
  (
     p_from    VARCHAR2
    ,p_to      mantas.rf_tab_varchar
    ,p_subject VARCHAR2
    ,p_body    CLOB
    ,p_attachments mantas.rf_pkg_mail.t_attachments_tab 
  );

END rf_pkg_mail;
/

CREATE OR REPLACE
PACKAGE BODY mantas.rf_pkg_mail IS
  --
  -- ������������ CLOB � BLOB
  --
  FUNCTION c2b( c IN CLOB ) RETURN BLOB
    IS
      pos PLS_INTEGER := 1;
      buffer RAW( 32767 );
      res BLOB;
      lob_len PLS_INTEGER := DBMS_LOB.getLength( c );
    BEGIN
      DBMS_LOB.createTemporary( res, TRUE );
      DBMS_LOB.OPEN( res, DBMS_LOB.LOB_ReadWrite );
      
      LOOP
        buffer := UTL_RAW.cast_to_raw( DBMS_LOB.SUBSTR( c, 16000, pos ) );
        IF UTL_RAW.LENGTH( buffer ) > 0 THEN
          DBMS_LOB.writeAppend( res, UTL_RAW.LENGTH( buffer ), buffer );
        END IF;
        pos := pos + 16000;
        EXIT WHEN pos > lob_len;
      END LOOP;
      RETURN res; -- res is OPEN here
    END c2b;
  
  --
  -- ��������� ��������� �� e-mail, ����� APEX_MAIL � �����������
  -- ��������! �������� COMMIT
  --  
  PROCEDURE send
  (
     p_from    VARCHAR2
    ,p_to      mantas.rf_tab_varchar
    ,p_subject VARCHAR2
    ,p_body    CLOB
    ,p_attachments mantas.rf_pkg_mail.t_attachments_tab 
  ) IS
      l_mail_id       NUMBER;
      l_workspace_id  apex_applications.workspace_id%TYPE;
      l_to            VARCHAR2(32000);
    BEGIN
      -- �������������� ��������� APEX
      BEGIN 
        SELECT workspace_id
            INTO l_workspace_id
            FROM apex_applications
            WHERE rownum=1 AND workspace='AMLREP';
      EXCEPTION 
        WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-20000,'��� �������� ����� ������ ���� ���� ���� ������ � apex_applications' );
      END;      
        
      wwv_flow_api.set_security_group_id(l_workspace_id);
      
      -- �������� ������ �����������
      SELECT LISTAGG(column_value,',') WITHIN GROUP (ORDER BY column_value)
        INTO l_to
        FROM table(p_to);
        
      -- ������� ������
      l_mail_id := apex_mail.send(
                  p_to    => l_to
                 ,p_from  => p_from
                 ,p_body  => p_body
                 ,p_subj  => p_subject);
        IF p_attachments.COUNT>0 THEN
          FOR i IN p_attachments.FIRST .. p_attachments.LAST
            LOOP 
              apex_mail.add_attachment(
                    p_mail_id     => l_mail_id
                   ,p_attachment  => c2b(p_attachments(i).attachment)
                   ,p_filename    => p_attachments(i).filename
                   ,p_mime_type   => p_attachments(i).mime_type);
              
            END LOOP;  
        END IF;    
        COMMIT; 
        apex_mail.push_queue();      
    END send;
  --
  -- ��������� ��������� �� e-mail, ����� APEX_MAIL ��� ����������
  -- ��������! �������� COMMIT
  --      
  PROCEDURE send
  (
     p_from    VARCHAR2
    ,p_to      mantas.rf_tab_varchar
    ,p_subject VARCHAR2
    ,p_body    CLOB
  ) IS
      l_attachments mantas.rf_pkg_mail.t_attachments_tab;
    BEGIN
      send(p_from,p_to,p_subject,p_body,l_attachments);
    END send;
    
END rf_pkg_mail;
/