CREATE OR REPLACE PACKAGE mantas.rf_pkg_import_oes IS
 
  -- �������� ���� ��� �������� � GTT STD.IMP_SBRF_321XML
  FUNCTION add_321xml_file(
    p_file_nm       IN mantas.rf_imp_file.file_nm%TYPE      -- ��� �����
   ,p_archive_nm    IN mantas.rf_imp_file.archive_nm%TYPE   -- ��� ������ (���� ���� ��������� - NULL)
   ,p_file          IN BLOB                                 -- ������ �����
   ,p_message       OUT VARCHAR2                            -- ��������� �� ������
  ) RETURN NUMBER;  -- ������ ��������� (1 �������, ����� 0)  

  -- �������� ���� ��� �������� � GTT STD.IMP_SBRF_321XML
  FUNCTION add_321xml_8001_file(
    p_file_nm       IN mantas.rf_imp_file.file_nm%TYPE      -- ��� �����
   ,p_archive_nm    IN mantas.rf_imp_file.archive_nm%TYPE   -- ��� ������ (���� ���� ��������� - NULL)
   ,p_file          IN BLOB                                 -- ������ �����
   ,p_message       OUT VARCHAR2                            -- ��������� �� ������
  ) RETURN NUMBER;  -- ������ ��������� (1 �������, ����� 0)  

  -- ���������/���������� ��� ����� �� GTT STD.IMP_SBRF_321XML
  FUNCTION process_321xml_files(
    p_message      OUT VARCHAR2 -- ��������� �������� ��������� (������� ����� ���������� ������, �� ��� � �������)
  ) RETURN NUMBER;  -- ������ ��������� (1 �������, ����� 0)  
  
  -- ���������/���������� ��� ����� �� GTT STD.IMP_SBRF_321XML
  FUNCTION process_321xml_8001_files(
    p_message      OUT VARCHAR2 -- ��������� �������� ��������� (������� ����� ���������� ������, �� ��� � �������)
  ) RETURN NUMBER;  -- ������ ��������� (1 �������, ����� 0)  

END rf_pkg_import_oes;
/

CREATE OR REPLACE PACKAGE BODY mantas.rf_pkg_import_oes IS
  
  conversion_error EXCEPTION;
  PRAGMA EXCEPTION_INIT(conversion_error, -20600);  
  
  -- ��� ������� ��������� ��������������� ���������
  TYPE filtered_opok_count_tt IS TABLE OF PLS_INTEGER INDEX BY  VARCHAR2(4 CHAR);

  -- �������� ���� ��� �������� � GTT STD.IMP_SBRF_321XML
  FUNCTION add_321xml_file(
    p_file_nm       IN mantas.rf_imp_file.file_nm%TYPE      -- ��� �����
   ,p_archive_nm    IN mantas.rf_imp_file.archive_nm%TYPE   -- ��� ������ (���� ���� ��������� - NULL)
   ,p_file          IN BLOB                                 -- ������ �����
   ,p_message       OUT VARCHAR2                            -- ��������� �� ������
  ) RETURN NUMBER IS  -- ������ ��������� (1 �������, ����� 0)  
    v_file_id number := -1;
  BEGIN
    -- ���������� ������������ FILE_ID �� ��������� �������
    SELECT nvl(MAX(file_id), 0) + 1 INTO v_file_id
      FROM std.imp_sbrf_321xml;

    -- ��������� ���� �� ��������� �������
    INSERT INTO std.imp_sbrf_321xml (file_id, file_nm, archive_nm, error_loading_fl, file_body) 
      VALUES (v_file_id, p_file_nm, p_archive_nm, 0, XMLTYPE.createxml(p_file, NULL, 'fns.esb.sbrf.ru'));
    RETURN 1;

    -- � ������ ������ ���������� �������� � ����� � �������� ������
    EXCEPTION
      WHEN OTHERS THEN
        p_message := sqlerrm();
        INSERT INTO std.imp_sbrf_321xml (file_id, file_nm, archive_nm, error_loading_fl, err_msg_tx) 
          VALUES (v_file_id, p_file_nm, p_archive_nm, 1, p_message);
        RETURN 0;
  END add_321xml_file;

  -- �������� ���� ��� �������� � GTT STD.IMP_SBRF_321XML
  FUNCTION add_321xml_8001_file(
    p_file_nm       IN mantas.rf_imp_file.file_nm%TYPE      -- ��� �����
   ,p_archive_nm    IN mantas.rf_imp_file.archive_nm%TYPE   -- ��� ������ (���� ���� ��������� - NULL)
   ,p_file          IN BLOB                                 -- ������ �����
   ,p_message       OUT VARCHAR2                            -- ��������� �� ������
  ) RETURN NUMBER IS  -- ������ ��������� (1 �������, ����� 0)  
    v_file_id number := -1;
  BEGIN
    -- ���������� ������������ FILE_ID �� ��������� �������
    SELECT nvl(MAX(file_id), 0) + 1 INTO v_file_id
      FROM std.imp_sbrf_321xml_8001;

    -- ����������, ����� ��������� �������� XML ����� �� DTD.
    execute immediate q'{alter session set events ='31156 trace name context forever, level 2'}';

    -- ��������� ���� �� ��������� �������
    INSERT INTO std.imp_sbrf_321xml_8001 (file_id, file_nm, archive_nm, error_loading_fl, file_body) 
      VALUES (v_file_id, p_file_nm, p_archive_nm, 0, XMLTYPE.createxml(p_file, NULL, NULL));
    RETURN 1;
    
    -- ���������� alter session.
    execute immediate q'{alter session set events ='31156 trace name context off'}';

    -- � ������ ������ ���������� �������� � ����� � �������� ������
    EXCEPTION
      WHEN OTHERS THEN
        p_message := sqlerrm();
        INSERT INTO std.imp_sbrf_321xml_8001 (file_id, file_nm, archive_nm, error_loading_fl, err_msg_tx) 
          VALUES (v_file_id, p_file_nm, p_archive_nm, 1, p_message);
        RETURN 0;
  END add_321xml_8001_file;

  -- ����������� �������� ��� ������������ ������ conversion_error
  PROCEDURE check_and_copy_varchar(
      p_from IN VARCHAR2
     ,p_to IN OUT NOCOPY VARCHAR2
     ,p_name VARCHAR2) IS
    BEGIN
      p_to:=p_from;
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error(-20600,'�� ���������� ������ ���� '||p_name);
    END check_and_copy_varchar;

  -- ����������� �������� ��� ������������ ��� ������ ������� ������ conversion_error
  PROCEDURE check_and_copy_date(
      p_from IN VARCHAR2
     ,p_to IN OUT NOCOPY VARCHAR2
     ,p_name VARCHAR2
     ,p_in_format VARCHAR2 DEFAULT 'YYYY-MM-DD'
     ,p_out_format VARCHAR2 DEFAULT 'YYYY-MM-DD'
     ) IS
    BEGIN
      p_to:=TO_CHAR(TO_DATE(TRIM(p_from),p_in_format),p_out_format);
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error(-20600,'�� ���������� ������ ���� '||p_name);
    END check_and_copy_date;

  -- ����������� �������� ��� ������������ ��� ������ ������� ������ conversion_error
  PROCEDURE check_and_copy_date(
      p_from IN VARCHAR2
     ,p_to IN OUT NOCOPY DATE
     ,p_name VARCHAR2
     ,p_format VARCHAR2 DEFAULT 'YYYY-MM-DD') IS
    BEGIN
      p_to:=TO_DATE(TRIM(p_from),p_format);
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error(-20600,'�� ���������� ������ ���� '||p_name);
    END check_and_copy_date;

  -- ����������� �������� ��� ������������ ��� ������ ������� ������ conversion_error
  PROCEDURE check_and_copy_number(
      p_from   IN VARCHAR2
     ,p_to     IN OUT NOCOPY VARCHAR2
     ,p_name   IN VARCHAR2
     ,p_format IN VARCHAR2 DEFAULT '99999999999990D99'
     ,p_nls    IN VARCHAR2 DEFAULT '.,') IS
    BEGIN
      p_to:=TO_CHAR(TO_NUMBER(TRIM(p_from),p_format,'NLS_NUMERIC_CHARACTERS = '''||p_nls||''''),p_format,'NLS_NUMERIC_CHARACTERS = '''||p_nls||'''');
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error(-20600,'�� ���������� ������ ���� '||p_name||'='||p_from);
    END check_and_copy_number;

  -- ����������� �������� ��� ������������ ��� ������ ������� ������ conversion_error
  PROCEDURE check_and_copy_number(
      p_from IN VARCHAR2
     ,p_to IN OUT NOCOPY NUMBER
     ,p_name VARCHAR2
     ,p_format VARCHAR2 DEFAULT '99999999999990D99') IS
    BEGIN
      p_to:=TO_NUMBER(TRIM(p_from),p_format);
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error(-20600,'�� ���������� ������ ���� '||p_name);
    END check_and_copy_number;

  -- ���������/���������� ��� ����� �� GTT STD.IMP_SBRF_321XML
  FUNCTION process_321xml_files(
    p_message      OUT VARCHAR2 -- ��������� �������� ��������� (������� ����� ���������� ������, �� ��� � �������)
  ) RETURN NUMBER IS  -- ������ ��������� (1 �������, ����� 0)  
    l_file_cnt      NUMBER;
    l_good_file_cnt NUMBER;
    l_param         rf_json;
    l_owner         kdd_review_owner.owner_id%TYPE;
    l_owner_seq_id  kdd_review_owner.owner_seq_id%TYPE;
    l_user_seq_id   kdd_review_owner.owner_seq_id%TYPE;
  BEGIN
    l_file_cnt:=0;
    l_good_file_cnt:=0;
    
    -- ������ � ��������� ������� ���������
    BEGIN
      l_param:=rf_json(rf_pkg_import.get_session_extra_params_tx);
    EXCEPTION 
      WHEN OTHERS THEN
        p_message:='������ ��� ������� ���������� ����������';
        RETURN 0;
    END;  

    l_owner:=rf_json_ext.get_string(l_param,'ownerId');
    IF l_owner IS NULL THEN
      p_message:='������: ������� ������ �������� ownerId';
      RETURN 0;
    END IF;

    SELECT MAX(o.owner_seq_id) INTO l_owner_seq_id FROM kdd_review_owner o WHERE o.owner_id = l_owner;
    IF l_owner_seq_id IS NULL THEN
      p_message:='������ �������� ������������� ������������ (������������� �� �����): '||l_owner;
      RETURN 0;
    END IF;  

    SELECT MAX(o.owner_seq_id) INTO l_user_seq_id FROM kdd_review_owner o WHERE o.owner_id = rf_pkg_import.get_session_user;
    IF l_user_seq_id IS NULL THEN
      p_message:='������ �������� ������������� ������������ (��������� ������): '||rf_pkg_import.get_session_user;
      RETURN 0;
    END IF;  

    -- ���� �� ������
    <<fileloop>>
    FOR c_file IN (SELECT * FROM std.v_imp_sbrf_321XML f ORDER BY f.file_id )
      LOOP
        DECLARE
          l_file_id         rf_imp_file.imp_file_id%TYPE;
          l_data_dt         rf_imp_file.data_dt%TYPE;
          l_dpd_cd          rf_imp_file.dpt_cd%TYPE;
          l_sys_cd          rf_imp_file.sys_cd%TYPE;
          l_order_nb        rf_imp_file.order_nb%TYPE;
          l_load_qty        rf_imp_history.load_qty%TYPE;
          l_branch_id       kdd_review.rf_branch_id%TYPE;
          l_file_body       BLOB;
          l_file_len        NUMBER;
          l_filtered        filtered_opok_count_tt;
          l_filtered_cnt    PLS_INTEGER;
          l_filtered_idx    VARCHAR2(4 CHAR);
          l_filtered_msg    rf_imp_history.err_msg_tx%TYPE;
        BEGIN 
          SAVEPOINT imp_321xml_file;
          l_file_cnt:=l_file_cnt+1;
          l_load_qty:=0;
          
          -- ��������� ������ �������� ����� � �������� �������
          IF c_file.error_loading_fl=1 THEN
            ROLLBACK TO SAVEPOINT imp_321xml_file;
            rf_pkg_import.log_imp_history('M',l_file_id,'E','������ ��� ��������� �����: '||c_file.err_msg_tx, NULL,NULL,c_file.file_nm,NULL,NULL);
            CONTINUE fileloop;
          END IF;
          
          -- ��������� ���� ��������� �����
          BEGIN
            check_and_copy_date   (c_file.data_dt ,l_data_dt ,'DATA_DT');
            check_and_copy_varchar(c_file.dpd_cd  ,l_dpd_cd  ,'DPD_CD');
            check_and_copy_varchar(c_file.sys_cd  ,l_sys_cd  ,'SYS_CD');
            check_and_copy_varchar(c_file.order_nb,l_order_nb,'ORDER_NB');
          EXCEPTION
            WHEN conversion_error THEN
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','������ � ��������� �����: '||SUBSTR(sqlerrm,12), NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END;
          
          -- ��������� �� ������������ ������� ���� �������
          IF NOT rf_pkg_import.check_sys_filter(l_sys_cd) THEN
            rf_pkg_import.log_imp_history('M',l_file_id,'E','��� ���� ������� '||rf_pkg_import.get_session_imp_type_cd||' ��������� �������� ������ �� ��������� '||NVL(l_sys_cd,'�����������'), NULL,NULL,c_file.file_nm,NULL,NULL);
            CONTINUE fileloop;
          END IF;
          
          -- ���������� � ��������� branch_id
          BEGIN 
            SELECT TO_NUMBER(SUBSTR(l_dpd_cd,1,2))*100000+TO_NUMBER(SUBSTR(l_dpd_cd,3,4)) 
              INTO l_branch_id 
              FROM business.org o 
              WHERE o.org_intrl_id=SUBSTR(l_dpd_cd,1,2)||'-'||SUBSTR(l_dpd_cd,3,4);
          EXCEPTION
            WHEN OTHERS THEN 
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','� ��������� ����� ������ �� ���������� ��� �������������: '||l_dpd_cd, NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END;
          
          -- ������� ��� ������� ����
          l_file_id:=rf_pkg_import.find_or_create_imp_file(c_file.file_nm,l_data_dt,l_dpd_cd,l_sys_cd,l_order_nb,c_file.archive_nm,'P');
          
          -- ��������� �� �������� �� ���� ������
          IF mantas.rf_pkg_import.get_last_load_status(l_file_id)='O' THEN
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','���� ��� ��� ������� �������� �����', NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END IF;
          
          -- �������� ������������� ����
          IF NOT rf_pkg_import.lock_imp_file(l_file_id) THEN
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','���� ����������� �������� ������ �������������', NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END IF;

          -- �������� ���������� ����� � ��� ������
          l_file_body:=c_file.file_body.getblobval(871);  -- ��������� UTF8
          l_file_len:=dbms_lob.getlength(l_file_body);

          --
          -- ���� �� ��������� � �����
          --
          <<oploop>>
          FOR c_op IN (SELECT * FROM std.v_sbrf_321xml_operation o WHERE o.file_id=c_file.file_id ORDER BY o.operation_id)
            LOOP
              DECLARE
                l_oes_oper    rf_type_321p_oper;
                l_oes_parties rf_tab_321p_party;
                l_review_id   kdd_review.review_id%TYPE; 
                l_oes_321_id  rf_oes_321.oes_321_id%TYPE;

              BEGIN 
                l_oes_oper:=rf_type_321p_oper(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
                 ,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

                -- ��������� ������� �� ���������
                l_oes_oper.version_p:='2';

                -- �������� �����
                BEGIN
                  -- ��� ��������
                  check_and_copy_number (c_op.vo	        ,l_oes_oper.vo         ,'VO', '9990');
                  
                  -- ������ �� ���� ��������
                  IF NOT rf_pkg_import.check_opok_filter(l_sys_cd,l_oes_oper.vo) THEN
                    IF l_filtered.EXISTS(l_oes_oper.vo)
                      THEN l_filtered(l_oes_oper.vo):=l_filtered(l_oes_oper.vo)+1;
                      ELSE l_filtered(l_oes_oper.vo):=1;
                    END IF;  
                    CONTINUE oploop;
                  END IF;
                  
                  -- ��������� ����
                  check_and_copy_varchar(c_op.action	    ,l_oes_oper.action     ,'ACTION');
                  check_and_copy_number (c_op.numb_p	    ,l_oes_oper.numb_p     ,'NUMB_P','999990');
                  check_and_copy_date   (c_op.date_p	    ,l_oes_oper.date_p     ,'DATE_P');
                  check_and_copy_varchar(c_op.branch	    ,l_oes_oper.branch     ,'BRANCH');
                  check_and_copy_varchar(c_op.numbf_ss    ,l_oes_oper.numbf_ss   ,'NUMBF_SS');
                  check_and_copy_varchar(c_op.terror	    ,l_oes_oper.terror     ,'TERROR');
                  check_and_copy_varchar(c_op.dop_v	      ,l_oes_oper.dop_v      ,'DOP_V');
                  check_and_copy_date   (c_op.data	      ,l_oes_oper.data       ,'DATA');
                  check_and_copy_number (c_op.sume	      ,l_oes_oper.sume       ,'SUME');
                  check_and_copy_number (c_op.sum	        ,l_oes_oper.sum        ,'SUM');
                  check_and_copy_varchar(c_op.curren	    ,l_oes_oper.curren     ,'CURREN');
                  check_and_copy_varchar(c_op.prim	      ,l_oes_oper.prim       ,'PRIM');
                  check_and_copy_date   (c_op.date_pay_d	,l_oes_oper.date_pay_d ,'DATE_PAY_D');
                  check_and_copy_varchar(c_op.metal	      ,l_oes_oper.metal      ,'METAL');
                  check_and_copy_varchar(c_op.priz6001	  ,l_oes_oper.priz6001   ,'PRIZ6001');
                  check_and_copy_varchar(c_op.b_payer	    ,l_oes_oper.b_payer    ,'B_PAYER');
                  check_and_copy_varchar(c_op.b_recip	    ,l_oes_oper.b_recip    ,'B_RECIP');
                  check_and_copy_varchar(c_op.part	      ,l_oes_oper.part       ,'PART');
                  check_and_copy_varchar(c_op.descr	      ,l_oes_oper.descr      ,'DESCR');
                  check_and_copy_varchar(c_op.curren_con	,l_oes_oper.curren_con ,'CURREN_CON');
                  check_and_copy_number (c_op.sum_con	    ,l_oes_oper.sum_con    ,'SUM_CON');
                  check_and_copy_varchar(c_op.priz_sd	    ,l_oes_oper.priz_sd    ,'PRIZ_SD');
                  check_and_copy_date   (c_op.date_s	    ,l_oes_oper.date_s     ,'DATE_S');

                  --��������� ������ ��� NUM_PAY_D � REFER_R2
                  IF LENGTH(c_op.num_pay_d)>12 
                    THEN
                      check_and_copy_varchar(c_op.num_pay_d	  ,l_oes_oper.refer_r2  ,'NUM_PAY_D');
                      l_oes_oper.num_pay_d:='0';
                    ELSE
                      BEGIN
                        check_and_copy_number(c_op.num_pay_d	  ,l_oes_oper.num_pay_d  ,'NUM_PAY_D','999999999990');
                        l_oes_oper.refer_r2:='0';
                      EXCEPTION
                        WHEN conversion_error THEN
                          check_and_copy_varchar(c_op.num_pay_d	  ,l_oes_oper.refer_r2  ,'NUM_PAY_D');
                          l_oes_oper.num_pay_d:='0';
                      END;
                   END IF;
                   
                EXCEPTION
                  WHEN conversion_error THEN
                    ROLLBACK TO SAVEPOINT imp_321xml_file;
                    rf_pkg_import.log_imp_history('R',l_file_id,'E','� ������ ����� '||TO_CHAR(c_op.operation_id)||' ������: '||SUBSTR(sqlerrm,12), NULL,NULL,c_file.file_nm,l_file_len,NULL,l_file_body);
                    CONTINUE fileloop;
                END;

                SELECT                
                    nvl(MAX(o.tel),'0')
                   ,nvl(MAX(o.regn),'0')
                   ,nvl(MAX(o.nd_ko),'0')
                   ,nvl(MAX(o.ktu_s),'0')
                   ,nvl(MAX(o.bik_s),'0')
                   ,nvl(MAX(o.numbf_s),'0')
                   ,nvl(MAX(o.branch_fl),'0')
                   ,nvl(MAX(o.ktu_ss),'0')
                   ,nvl(MAX(o.bik_ss),'0')
                   ,MAX(oes_321_org_id)
                  INTO 
                    l_oes_oper.tel
                   ,l_oes_oper.regn
                   ,l_oes_oper.nd_ko
                   ,l_oes_oper.ktu_s
                   ,l_oes_oper.bik_s
                   ,l_oes_oper.numbf_s
                   ,l_oes_oper.branch
                   ,l_oes_oper.ktu_ss
                   ,l_oes_oper.bik_ss
                   ,l_oes_oper.oes_321_org_id
                  FROM rf_oes_321_org o
                  WHERE o.oes_321_org_id=rf_pkg_kgrko.get_oes_321_org_id(trunc(l_branch_id/100000), l_branch_id - round(l_branch_id, -5), SYSDATE);
                
                -- ������� � �������� ����������
                -- �������� 5 ���������� (! ������� 1-5 ������ ������� ������ 0-4 !)
                l_oes_parties:=rf_tab_321p_party();
                l_oes_parties.extend(5);
                
                <<partyloop>>
                FOR c_party IN (SELECT * FROM std.v_sbrf_321xml_party p WHERE p.file_id=c_op.file_id AND p.operation_id=c_op.operation_id)
                  LOOP 
                    DECLARE
                      l_party_index PLS_INTEGER;  -- ������ � ������� ����������
                    BEGIN

                      -- �������� ��� ��������� �� ������
                      BEGIN
                        check_and_copy_number(c_party.block,l_party_index ,'BLOCK','0');
                      EXCEPTION
                        WHEN conversion_error THEN
                          ROLLBACK TO SAVEPOINT imp_321xml_file;
                          rf_pkg_import.log_imp_history('R',l_file_id,'E','� ������ ����� '||TO_CHAR(c_op.operation_id)||', � ��������� ����� '||TO_CHAR(c_party.party_id)||' ������: '||SUBSTR(sqlerrm,12), NULL,NULL,c_file.file_nm,l_file_len,NULL,l_file_body);
                          CONTINUE fileloop;
                      END;

                      -- ��������� ��� ��������� �� ��������
                      IF l_party_index<0 OR l_party_index>4 THEN
                        ROLLBACK TO SAVEPOINT imp_321xml_file;
                        rf_pkg_import.log_imp_history('R',l_file_id,'E','� ������ ����� '||TO_CHAR(c_op.operation_id)||', � ��������� ����� '||TO_CHAR(c_party.party_id)||' ������: '||SUBSTR(sqlerrm,12), NULL,NULL,c_file.file_nm,l_file_len,NULL,l_file_body);
                        CONTINUE fileloop;
                      END IF;
                      l_party_index:=l_party_index+1;
                      
                      -- ��������� ��� �� ��������� ����������
                      IF l_oes_parties(l_party_index) IS NOT NULL THEN
                        ROLLBACK TO SAVEPOINT imp_321xml_file;
                        rf_pkg_import.log_imp_history('R',l_file_id,'E','� ������ ����� '||TO_CHAR(c_op.operation_id)||', � ��������� ����� '||TO_CHAR(c_party.party_id)||' ������: ������ �������� ��������� � BLOCK='||c_party.block, NULL,NULL,c_file.file_nm,l_file_len,NULL,l_file_body);
                        CONTINUE fileloop;
                      END IF;
                      
                      -- ������� ������ ������
                      l_oes_parties(l_party_index):=rf_type_321p_party(null, null, null, null, null, null, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null
                        ,null, null, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        
                      -- ��������� � ��������� ������
                      BEGIN
                        check_and_copy_varchar(c_party.tureal    ,l_oes_parties(l_party_index).tureal       ,'TUREAL');
                        check_and_copy_varchar(c_party.pru       ,l_oes_parties(l_party_index).pru          ,'PRU');
                        check_and_copy_varchar(c_party.nameu     ,l_oes_parties(l_party_index).nameu        ,'NAMEU');
                        check_and_copy_varchar(c_party.kodcr     ,l_oes_parties(l_party_index).kodcr        ,'KODCR');
                        check_and_copy_varchar(c_party.kodcn     ,l_oes_parties(l_party_index).kodcn        ,'KODCN');
                        check_and_copy_varchar(c_party.kd        ,l_oes_parties(l_party_index).kd           ,'KD');
                        check_and_copy_varchar(c_party.sd        ,l_oes_parties(l_party_index).sd           ,'SD');
                        check_and_copy_varchar(c_party.rg        ,l_oes_parties(l_party_index).rg           ,'RG');
                        check_and_copy_varchar(c_party.nd        ,l_oes_parties(l_party_index).nd           ,'ND');
                        check_and_copy_varchar(c_party.vd_1      ,l_oes_parties(l_party_index).vd_1         ,'VD_1');
                        check_and_copy_varchar(c_party.vd_2      ,l_oes_parties(l_party_index).vd_2         ,'VD_2');
                        check_and_copy_date   (c_party.vd_3      ,l_oes_parties(l_party_index).vd_3         ,'VD_3');
                        check_and_copy_varchar(c_party.vd_4      ,l_oes_parties(l_party_index).vd_4         ,'VD_4');
                        check_and_copy_varchar(c_party.vd_5      ,l_oes_parties(l_party_index).vd_5         ,'VD_5');
                        check_and_copy_date   (c_party.vd_6      ,l_oes_parties(l_party_index).vd_6         ,'VD_6');
                        check_and_copy_date   (c_party.vd_7      ,l_oes_parties(l_party_index).vd_7         ,'VD_7');
                        check_and_copy_varchar(c_party.mc_1      ,l_oes_parties(l_party_index).mc_1         ,'MC_1');
                        check_and_copy_date   (c_party.mc_2      ,l_oes_parties(l_party_index).mc_2         ,'MC_2');
                        check_and_copy_date   (c_party.mc_3      ,l_oes_parties(l_party_index).mc_3         ,'MC_3');
                        check_and_copy_date   (c_party.gr        ,l_oes_parties(l_party_index).gr           ,'GR');
                        check_and_copy_varchar(c_party.bp        ,l_oes_parties(l_party_index).bp           ,'BP');
                        check_and_copy_varchar(c_party.vp        ,l_oes_parties(l_party_index).vp           ,'VP');
                        check_and_copy_varchar(c_party.amr_s     ,l_oes_parties(l_party_index).amr_adress_s ,'AMR_S');
                        check_and_copy_varchar(c_party.amr_r     ,l_oes_parties(l_party_index).amr_adress_r ,'AMR_R');
                        check_and_copy_varchar(c_party.amr_g     ,l_oes_parties(l_party_index).amr_adress_g ,'AMR_G');
                        check_and_copy_varchar(c_party.amr_u     ,l_oes_parties(l_party_index).amr_adress_u ,'AMR_U');
                        check_and_copy_varchar(c_party.amr_d     ,l_oes_parties(l_party_index).amr_adress_d ,'AMR_D');
                        check_and_copy_varchar(c_party.amr_k     ,l_oes_parties(l_party_index).amr_adress_k ,'AMR_K');
                        check_and_copy_varchar(c_party.amr_o     ,l_oes_parties(l_party_index).amr_adress_o ,'AMR_O');
                        check_and_copy_varchar(c_party.address_s ,l_oes_parties(l_party_index).adr_adress_s ,'ADDRESS_S');
                        check_and_copy_varchar(c_party.address_r ,l_oes_parties(l_party_index).adr_adress_r ,'ADDRESS_R');
                        check_and_copy_varchar(c_party.address_g ,l_oes_parties(l_party_index).adr_adress_g ,'ADDRESS_G');
                        check_and_copy_varchar(c_party.address_u ,l_oes_parties(l_party_index).adr_adress_u ,'ADDRESS_U');
                        check_and_copy_varchar(c_party.address_d ,l_oes_parties(l_party_index).adr_adress_d ,'ADDRESS_D');
                        check_and_copy_varchar(c_party.address_k ,l_oes_parties(l_party_index).adr_adress_k ,'ADDRESS_K');
                        check_and_copy_varchar(c_party.address_o ,l_oes_parties(l_party_index).adr_adress_o ,'ADDRESS_O');
                        check_and_copy_varchar(c_party.acc_b     ,l_oes_parties(l_party_index).acc_b        ,'ACC_B');
                        check_and_copy_varchar(c_party.acc_cor_b ,l_oes_parties(l_party_index).acc_cor_b    ,'ACC_COR_B');
                        check_and_copy_varchar(c_party.name_is_b ,l_oes_parties(l_party_index).name_is_b    ,'NAME_IS_B');
                        check_and_copy_varchar(c_party.bik_is_b  ,l_oes_parties(l_party_index).bik_is_b     ,'BIK_IS_B ');
                        check_and_copy_varchar(c_party.card_b    ,l_oes_parties(l_party_index).card_b       ,'CARD_B');
                        check_and_copy_varchar(c_party.name_b    ,l_oes_parties(l_party_index).name_b       ,'NAME_B');
                        check_and_copy_varchar(c_party.kodcn_b   ,l_oes_parties(l_party_index).kodcn_b      ,'KODCN_B');
                        check_and_copy_varchar(c_party.bik_b     ,l_oes_parties(l_party_index).bik_b        ,'BIK_B');
                        check_and_copy_varchar(c_party.name_r    ,l_oes_parties(l_party_index).name_r       ,'NAME_R');
                        check_and_copy_varchar(c_party.kodcn_r   ,l_oes_parties(l_party_index).kodcn_r      ,'KODCN_R');
                        check_and_copy_varchar(c_party.bik_r     ,l_oes_parties(l_party_index).bik_r        ,'BIK_R');
                      EXCEPTION
                        WHEN conversion_error THEN
                          ROLLBACK TO SAVEPOINT imp_321xml_file;
                          rf_pkg_import.log_imp_history('R',l_file_id,'E','� ������ ����� '||TO_CHAR(c_op.operation_id)||', � ��������� ����� '||TO_CHAR(c_party.party_id)||' ������: '||SUBSTR(sqlerrm,12), NULL,NULL, c_file.file_nm, l_file_len, NULL, l_file_body);
                          CONTINUE fileloop;
                      END;
                    END;
                  END LOOP partyloop;
                
                -- ���������������� ������ ����������
                FOR i IN 1..5
                  LOOP
                    IF l_oes_parties(i) IS NULL THEN
                      l_oes_parties(i):=rf_type_321p_party(null, null, null, null, null, null, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null
                        ,null, null, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    END IF;
                  END LOOP;
                  
                -- ������� ����� � ���
                rf_pkg_oes.create_oes_321_manual(
                  p_branch_id     => l_branch_id
                 ,p_owner_seq_id  => l_owner_seq_id
                 ,p_creat_id      => l_user_seq_id
                 ,p_note_tx       => '��������� �� ����� '|| c_file.file_nm
                 ,p_oper          => l_oes_oper 
                 ,p_parties       => l_oes_parties
                 ,p_review_id     => l_review_id
                 ,p_oes_321_id    => l_oes_321_id
                 );
                
                -- ����������� ��� � �����
                INSERT INTO mantas.rf_imp_oes (form_cd,oes_id,imp_file_id,oes_order_nb)
                  VALUES ('321',l_oes_321_id,l_file_id,c_op.operation_id);

                l_load_qty:=l_load_qty+1;
              END;
            END LOOP oploop;
          
          -- ������� ������ � ����������� �������, ������� �� ������������� �������� �������  
          l_filtered_cnt:=0;
          l_filtered_idx:=l_filtered.FIRST;
          WHILE (l_filtered_idx IS NOT NULL)
            LOOP
              l_filtered_msg:=l_filtered_msg||','||l_filtered_idx||' - '||TO_CHAR(l_filtered(l_filtered_idx));
              l_filtered_cnt:=l_filtered_cnt+l_filtered(l_filtered_idx);
              l_filtered_idx:=l_filtered.NEXT(l_filtered_idx);
            END LOOP;
          IF l_filtered_cnt>0 THEN
            l_filtered_msg:='�� ������������� �������� �������: '||TO_CHAR(l_filtered_cnt)||', � �.�. �� �����: '||LTRIM(l_filtered_msg,',');
            rf_pkg_import.log_imp_history('M',l_file_id,'O',l_filtered_msg,NULL ,NULL,c_file.file_nm, l_file_len,NULL,l_file_body);
          END IF;  

          -- ������� ������ �� �������� �������� �����
          rf_pkg_import.log_imp_history('R',l_file_id,'O',NULL,l_load_qty ,NULL,c_file.file_nm, l_file_len,NULL,l_file_body);
          l_good_file_cnt:=l_good_file_cnt+1;  
        EXCEPTION WHEN OTHERS THEN
          ROLLBACK TO SAVEPOINT imp_321xml_file;
          rf_pkg_import.log_imp_history('M',l_file_id,'E','���������� ������: '||dbms_utility.format_error_stack(), NULL,NULL,c_file.file_nm,NULL,NULL);
        END;  

      END LOOP fileloop;
    p_message:='��������� ������:'||TO_CHAR(l_file_cnt)||' �� ��� �������:'||TO_CHAR(l_good_file_cnt)||' � ��������:'||TO_CHAR(l_file_cnt-l_good_file_cnt) ;
    -- ���� �� ���� ���� �� ������ ���������, ����� ���������� ��� ���� ������
    IF l_good_file_cnt = 0 THEN
      RETURN 0;
    END IF;  
    RETURN 1;
  END process_321xml_files;

  -- ���������/���������� ��� ����� �� GTT STD.IMP_SBRF_321XML_8001
  FUNCTION process_321xml_8001_files(
    p_message      OUT VARCHAR2 -- ��������� �������� ��������� (������� ����� ���������� ������, �� ��� � �������)
  ) RETURN NUMBER IS  -- ������ ��������� (1 �������, ����� 0)  
    l_file_cnt      NUMBER;
    l_good_file_cnt NUMBER;
    l_param         rf_json;
    l_owner         kdd_review_owner.owner_id%TYPE;
    l_owner_seq_id  kdd_review_owner.owner_seq_id%TYPE;
    l_user_seq_id   kdd_review_owner.owner_seq_id%TYPE;
  BEGIN
    l_file_cnt:=0;
    l_good_file_cnt:=0;
    
    -- ������ � ��������� ������� ���������
    BEGIN
      l_param:=rf_json(rf_pkg_import.get_session_extra_params_tx);
    EXCEPTION 
      WHEN OTHERS THEN
        p_message:='������ ��� ������� ���������� ����������';
        RETURN 0;
    END;  

    l_owner:=rf_json_ext.get_string(l_param,'ownerId');
    IF l_owner IS NULL THEN
      p_message:='������: ������� ������ �������� ownerId';
      RETURN 0;
    END IF;

    SELECT MAX(o.owner_seq_id) INTO l_owner_seq_id FROM kdd_review_owner o WHERE o.owner_id = l_owner;
    IF l_owner_seq_id IS NULL THEN
      p_message:='������ �������� ������������� ������������ (������������� �� �����): '||l_owner;
      RETURN 0;
    END IF;  

    SELECT MAX(o.owner_seq_id) INTO l_user_seq_id FROM kdd_review_owner o WHERE o.owner_id = rf_pkg_import.get_session_user;
    IF l_user_seq_id IS NULL THEN
      p_message:='������ �������� ������������� ������������ (��������� ������): '||rf_pkg_import.get_session_user;
      RETURN 0;
    END IF;  

    -- ���� �� ������
    <<fileloop>>
    FOR c_file IN (SELECT * FROM std.v_imp_sbrf_321XML_8001 f ORDER BY f.file_id )
      LOOP
        DECLARE
          l_file_id         rf_imp_file.imp_file_id%TYPE;
          l_data_dt         rf_imp_file.data_dt%TYPE;
          l_dpd_cd          rf_imp_file.dpt_cd%TYPE;
          l_sys_cd          rf_imp_file.sys_cd%TYPE;
          l_order_nb        rf_imp_file.order_nb%TYPE;
          l_load_qty        rf_imp_history.load_qty%TYPE;
          l_branch_id       kdd_review.rf_branch_id%TYPE;
          l_file_body       BLOB;
          l_file_len        NUMBER;
          l_filtered        filtered_opok_count_tt;
          l_filtered_cnt    PLS_INTEGER;
          l_filtered_idx    VARCHAR2(4 CHAR);
          l_filtered_msg    rf_imp_history.err_msg_tx%TYPE;
        BEGIN 
          SAVEPOINT imp_321xml_file;
          l_file_cnt:=l_file_cnt+1;
          l_load_qty:=0;
          
          -- ��������� ������ �������� ����� � �������� �������
          IF c_file.error_loading_fl=1 THEN
            ROLLBACK TO SAVEPOINT imp_321xml_file;
            rf_pkg_import.log_imp_history('M',l_file_id,'E','������ ��� ��������� �����: '||c_file.err_msg_tx, NULL,NULL,c_file.file_nm,NULL,NULL);
            CONTINUE fileloop;
          END IF;
          
          -- ��������� ���� ��������� �����
          BEGIN
            check_and_copy_date   (c_file.data_dt ,l_data_dt ,'DATA_DT','DD.MM.YYYY HH24:MI:SS');
            check_and_copy_varchar(c_file.dpd_cd  ,l_dpd_cd  ,'DPD_CD');
            check_and_copy_varchar(c_file.sys_cd  ,l_sys_cd  ,'SYS_CD');
            check_and_copy_varchar(c_file.order_nb,l_order_nb,'ORDER_NB');
          EXCEPTION
            WHEN conversion_error THEN
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','������ � ��������� �����: '||SUBSTR(sqlerrm,12), NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END;
          
          -- ��������� �� ������������ ������� ���� �������
          IF NOT rf_pkg_import.check_sys_filter(l_sys_cd) THEN
            rf_pkg_import.log_imp_history('M',l_file_id,'E','��� ���� ������� '||rf_pkg_import.get_session_imp_type_cd||' ��������� �������� ������ �� ��������� '||NVL(l_sys_cd,'�����������'), NULL,NULL,c_file.file_nm,NULL,NULL);
            CONTINUE fileloop;
          END IF;
          
          -- ���������� � ��������� branch_id
          BEGIN 
            SELECT TO_NUMBER(SUBSTR(l_dpd_cd,1,2))*100000+TO_NUMBER(SUBSTR(l_dpd_cd,3,4)) 
              INTO l_branch_id 
              FROM business.org o 
              WHERE o.org_intrl_id=SUBSTR(l_dpd_cd,1,2)||'-'||SUBSTR(l_dpd_cd,3,4);
          EXCEPTION
            WHEN OTHERS THEN 
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','� ��������� ����� ������ �� ���������� ��� �������������: '||l_dpd_cd, NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END;
          
          -- ������� ��� ������� ����
          l_file_id:=rf_pkg_import.find_or_create_imp_file(c_file.file_nm,l_data_dt,l_dpd_cd,l_sys_cd,l_order_nb,c_file.archive_nm,'P');
          
          -- ��������� �� �������� �� ���� ������
          IF mantas.rf_pkg_import.get_last_load_status(l_file_id)='O' THEN
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','���� ��� ��� ������� �������� �����', NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END IF;
          
          -- �������� ������������� ����
          IF NOT rf_pkg_import.lock_imp_file(l_file_id) THEN
              ROLLBACK TO SAVEPOINT imp_321xml_file;
              rf_pkg_import.log_imp_history('M',l_file_id,'E','���� ����������� �������� ������ �������������', NULL,NULL,c_file.file_nm,NULL,NULL);
              CONTINUE fileloop;
          END IF;

          -- �������� ���������� ����� � ��� ������
          l_file_body:=c_file.file_body.getblobval(871);  -- ��������� UTF8
          l_file_len:=dbms_lob.getlength(l_file_body);

          --
          -- ���� �� ��������� � �����
          --
          <<oploop>>
          FOR c_op IN (SELECT * FROM std.v_sbrf_321xml_8001_operation o WHERE o.file_id=c_file.file_id)
            LOOP
              DECLARE
                l_oes_oper    rf_type_321p_oper;
                l_oes_parties rf_tab_321p_party;
                l_review_id   kdd_review.review_id%TYPE; 
                l_oes_321_id  rf_oes_321.oes_321_id%TYPE;
                l_tmp_char    VARCHAR2(32000);
              BEGIN 
                l_oes_oper:=rf_type_321p_oper(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
                 ,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

                -- �������� �����
                BEGIN
                  -- ��� ��������
                  check_and_copy_number (c_op.vo     	    ,l_oes_oper.vo          ,'VO', '9990');
                  
                  -- ������ �� ���� ��������
                  IF NOT rf_pkg_import.check_opok_filter(l_sys_cd,l_oes_oper.vo) THEN
                    IF l_filtered.EXISTS(l_oes_oper.vo)
                      THEN l_filtered(l_oes_oper.vo):=l_filtered(l_oes_oper.vo)+1;
                      ELSE l_filtered(l_oes_oper.vo):=1;
                    END IF;  
                    CONTINUE oploop;
                  END IF;
                  
                  -- ��������� ����      
                  check_and_copy_varchar(c_op.b_payer	    ,l_oes_oper.b_payer     ,'B_PAYER');
                  check_and_copy_varchar(c_op.b_recip	    ,l_oes_oper.b_recip     ,'B_RECIP');
                  check_and_copy_varchar(c_op.bik_s  	    ,l_oes_oper.bik_s       ,'BIK_S');
                  check_and_copy_varchar(c_op.bik_ss 	    ,l_oes_oper.bik_ss      ,'BIK_SS');                  
                  check_and_copy_varchar(c_op.branch	    ,l_oes_oper.branch      ,'BRANCH');
                  check_and_copy_varchar(c_op.curren	    ,l_oes_oper.curren      ,'CURREN');
                  check_and_copy_varchar(c_op.curren_con	,l_oes_oper.curren_con  ,'CURREN_CON');
                  check_and_copy_date   (c_op.DATA	      ,l_oes_oper.DATA        ,'DATA'       ,'DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.date_pay_d  ,l_oes_oper.date_pay_d  ,'DATE_PAY_D' ,'DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.date_s      ,l_oes_oper.date_s      ,'DATE_S'     ,'DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.date_p      ,l_oes_oper.date_p      ,'DATE_P'     ,'DD.MM.YYYY HH24:MI:SS');
                  l_tmp_char:=c_op.descr_1||CASE WHEN c_op.descr_2='0' THEN NULL ELSE c_op.descr_2 END;
                  check_and_copy_varchar(l_tmp_char       ,l_oes_oper.descr,'DESCR1 ��� DESCR2');
                  check_and_copy_varchar(c_op.dop_v 	    ,l_oes_oper.dop_v       ,'DOP_V');
                  check_and_copy_varchar(c_op.ktu_s 	    ,l_oes_oper.ktu_s       ,'KTU_S');
                  check_and_copy_varchar(c_op.ktu_ss 	    ,l_oes_oper.ktu_ss      ,'KTU_SS');
                  check_and_copy_varchar(c_op.metal 	    ,l_oes_oper.metal       ,'METAL');
                  check_and_copy_varchar(c_op.nd_ko 	    ,l_oes_oper.nd_ko       ,'ND_KO');
                  check_and_copy_number (c_op.num_pay_d   ,l_oes_oper.num_pay_d   ,'NUM_PAY_D','999999999990');
                  check_and_copy_number (c_op.numb_p 	    ,l_oes_oper.numb_p      ,'NUMB_P','999990');
                  check_and_copy_varchar(c_op.numbf_s	    ,l_oes_oper.numbf_s     ,'NUMBF_S');
                  check_and_copy_varchar(c_op.numbf_ss    ,l_oes_oper.numbf_ss    ,'NUMBF_SS');
                  check_and_copy_varchar(c_op.part        ,l_oes_oper.part        ,'PART');
                  l_tmp_char:=c_op.prim_1||CASE WHEN c_op.prim_2='0' THEN NULL ELSE c_op.prim_2 END;
                  check_and_copy_varchar(l_tmp_char	,l_oes_oper.prim,'PRIM_1 ��� PRIM_2');
                  check_and_copy_varchar(c_op.priz_sd	    ,l_oes_oper.priz_sd     ,'PRIZ_SD');
                  check_and_copy_varchar(c_op.priz6001    ,l_oes_oper.priz6001    ,'PRIZ6001');
                  check_and_copy_varchar(c_op.refer_r2    ,l_oes_oper.refer_r2    ,'REFER_R2');
                  check_and_copy_varchar(c_op.regn  	    ,l_oes_oper.regn        ,'REGN');
                  check_and_copy_number (c_op.sum         ,l_oes_oper.sum         ,'SUM');
                  check_and_copy_number (c_op.sum_con     ,l_oes_oper.sum_con     ,'SUM_CON');
                  check_and_copy_number (c_op.sume        ,l_oes_oper.sume        ,'SUME');
                  check_and_copy_varchar(c_op.tel   	    ,l_oes_oper.tel         ,'TEL');
                  check_and_copy_varchar(c_op.terror 	    ,l_oes_oper.terror      ,'TERROR');
                  check_and_copy_varchar(c_op.version	    ,l_oes_oper.version_p   ,'VERSION');
                  -- TODO: ��� � ����, ���� �����������:
                  -- NAME_S
                EXCEPTION
                  WHEN conversion_error THEN
                    ROLLBACK TO SAVEPOINT imp_321xml_file;
                    rf_pkg_import.log_imp_history('R',l_file_id,'E','������: '||SUBSTR(sqlerrm,12), NULL,NULL,c_file.file_nm,l_file_len,NULL,l_file_body);
                    CONTINUE fileloop;
                END;

                SELECT                
                   MAX(oes_321_org_id)
                  INTO 
                   l_oes_oper.oes_321_org_id
                  FROM rf_oes_321_org o
                  WHERE o.oes_321_org_id=rf_pkg_kgrko.get_oes_321_org_id(trunc(l_branch_id/100000), l_branch_id - round(l_branch_id, -5), SYSDATE);
                
                -- ������� � �������� ����������
                -- �������� 5 ���������� (! ������� 1-5 ������ ������� ������ 0-4 !)
                l_oes_parties:=rf_tab_321p_party();
                l_oes_parties.extend(5);
                
                -- ���������������� ������ ����������
                FOR i IN 1..5
                  LOOP
                    IF l_oes_parties(i) IS NULL THEN
                      l_oes_parties(i):=rf_type_321p_party(null, null, null, null, null, null, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null
                        ,null, null, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    END IF;
                  END LOOP;

                -- �������� ����� ����������
                BEGIN
                  check_and_copy_varchar(c_op.acc_b0	    ,l_oes_parties(1).acc_b         ,'ACC_B0');
                  check_and_copy_varchar(c_op.acc_b3	    ,l_oes_parties(4).acc_b         ,'ACC_B3');
                  check_and_copy_varchar(c_op.acc_cor_b0	,l_oes_parties(1).acc_cor_b     ,'ACC_COR_B0');
                  check_and_copy_varchar(c_op.acc_cor_b3  ,l_oes_parties(4).acc_cor_b     ,'ACC_COR_B3');
                  check_and_copy_varchar(c_op.adress_d0	  ,l_oes_parties(1).adr_adress_d  ,'ADRESS_D0');
                  check_and_copy_varchar(c_op.adress_d1   ,l_oes_parties(2).adr_adress_d  ,'ADRESS_D1');
                  check_and_copy_varchar(c_op.adress_d2	  ,l_oes_parties(3).adr_adress_d  ,'ADRESS_D2');
                  check_and_copy_varchar(c_op.adress_d3   ,l_oes_parties(4).adr_adress_d  ,'ADRESS_D3');
                  check_and_copy_varchar(c_op.adress_d4   ,l_oes_parties(5).adr_adress_d  ,'ADRESS_D4');
                  check_and_copy_varchar(c_op.adress_g0	  ,l_oes_parties(1).adr_adress_g  ,'ADRESS_G0');
                  check_and_copy_varchar(c_op.adress_g1   ,l_oes_parties(2).adr_adress_g  ,'ADRESS_G1');
                  check_and_copy_varchar(c_op.adress_g2	  ,l_oes_parties(3).adr_adress_g  ,'ADRESS_G2');
                  check_and_copy_varchar(c_op.adress_g3   ,l_oes_parties(4).adr_adress_g  ,'ADRESS_G3');
                  check_and_copy_varchar(c_op.adress_g4   ,l_oes_parties(5).adr_adress_g  ,'ADRESS_G4');
                  check_and_copy_varchar(c_op.adress_k0	  ,l_oes_parties(1).adr_adress_k  ,'ADRESS_K0');
                  check_and_copy_varchar(c_op.adress_k1   ,l_oes_parties(2).adr_adress_k  ,'ADRESS_K1');
                  check_and_copy_varchar(c_op.adress_k2	  ,l_oes_parties(3).adr_adress_k  ,'ADRESS_K2');
                  check_and_copy_varchar(c_op.adress_k3   ,l_oes_parties(4).adr_adress_k  ,'ADRESS_K3');
                  check_and_copy_varchar(c_op.adress_k4   ,l_oes_parties(5).adr_adress_k  ,'ADRESS_K4');
                  check_and_copy_varchar(c_op.adress_o0	  ,l_oes_parties(1).adr_adress_o  ,'ADRESS_O0');
                  check_and_copy_varchar(c_op.adress_o1   ,l_oes_parties(2).adr_adress_o  ,'ADRESS_O1');
                  check_and_copy_varchar(c_op.adress_o2	  ,l_oes_parties(3).adr_adress_o  ,'ADRESS_O2');
                  check_and_copy_varchar(c_op.adress_o3   ,l_oes_parties(4).adr_adress_o  ,'ADRESS_O3');
                  check_and_copy_varchar(c_op.adress_o4   ,l_oes_parties(5).adr_adress_o  ,'ADRESS_O4');
                  check_and_copy_varchar(c_op.adress_r0	  ,l_oes_parties(1).adr_adress_r  ,'ADRESS_R0');
                  check_and_copy_varchar(c_op.adress_r1   ,l_oes_parties(2).adr_adress_r  ,'ADRESS_R1');
                  check_and_copy_varchar(c_op.adress_r2	  ,l_oes_parties(3).adr_adress_r  ,'ADRESS_R2');
                  check_and_copy_varchar(c_op.adress_r3   ,l_oes_parties(4).adr_adress_r  ,'ADRESS_R3');
                  check_and_copy_varchar(c_op.adress_r4   ,l_oes_parties(5).adr_adress_r  ,'ADRESS_R4');
                  check_and_copy_varchar(c_op.adress_s0	  ,l_oes_parties(1).adr_adress_s  ,'ADRESS_S0');
                  check_and_copy_varchar(c_op.adress_s1   ,l_oes_parties(2).adr_adress_s  ,'ADRESS_S1');
                  check_and_copy_varchar(c_op.adress_s2	  ,l_oes_parties(3).adr_adress_s  ,'ADRESS_S2');
                  check_and_copy_varchar(c_op.adress_s3   ,l_oes_parties(4).adr_adress_s  ,'ADRESS_S3');
                  check_and_copy_varchar(c_op.adress_s4   ,l_oes_parties(5).adr_adress_s  ,'ADRESS_S4');
                  check_and_copy_varchar(c_op.adress_u0	  ,l_oes_parties(1).adr_adress_u  ,'ADRESS_U0');
                  check_and_copy_varchar(c_op.adress_u1   ,l_oes_parties(2).adr_adress_u  ,'ADRESS_U1');
                  check_and_copy_varchar(c_op.adress_u2	  ,l_oes_parties(3).adr_adress_u  ,'ADRESS_U2');
                  check_and_copy_varchar(c_op.adress_u3   ,l_oes_parties(4).adr_adress_u  ,'ADRESS_U3');
                  check_and_copy_varchar(c_op.adress_u4   ,l_oes_parties(5).adr_adress_u  ,'ADRESS_U4');
                  check_and_copy_varchar(c_op.amr_d0	    ,l_oes_parties(1).amr_adress_d  ,'AMR_D0');
                  check_and_copy_varchar(c_op.amr_d1      ,l_oes_parties(2).amr_adress_d  ,'AMR_D1');
                  check_and_copy_varchar(c_op.amr_d2	    ,l_oes_parties(3).amr_adress_d  ,'AMR_D2');
                  check_and_copy_varchar(c_op.amr_d3      ,l_oes_parties(4).amr_adress_d  ,'AMR_D3');
                  check_and_copy_varchar(c_op.amr_d4      ,l_oes_parties(5).amr_adress_d  ,'AMR_D4');
                  check_and_copy_varchar(c_op.amr_g0	    ,l_oes_parties(1).amr_adress_g  ,'AMR_G0');
                  check_and_copy_varchar(c_op.amr_g1      ,l_oes_parties(2).amr_adress_g  ,'AMR_G1');
                  check_and_copy_varchar(c_op.amr_g2	    ,l_oes_parties(3).amr_adress_g  ,'AMR_G2');
                  check_and_copy_varchar(c_op.amr_g3      ,l_oes_parties(4).amr_adress_g  ,'AMR_G3');
                  check_and_copy_varchar(c_op.amr_g4      ,l_oes_parties(5).amr_adress_g  ,'AMR_G4');
                  check_and_copy_varchar(c_op.amr_k0	    ,l_oes_parties(1).amr_adress_k  ,'AMR_K0');
                  check_and_copy_varchar(c_op.amr_k1      ,l_oes_parties(2).amr_adress_k  ,'AMR_K1');
                  check_and_copy_varchar(c_op.amr_k2	    ,l_oes_parties(3).amr_adress_k  ,'AMR_K2');
                  check_and_copy_varchar(c_op.amr_k3      ,l_oes_parties(4).amr_adress_k  ,'AMR_K3');
                  check_and_copy_varchar(c_op.amr_k4      ,l_oes_parties(5).amr_adress_k  ,'AMR_K4');
                  check_and_copy_varchar(c_op.amr_o0	    ,l_oes_parties(1).amr_adress_o  ,'AMR_O0');
                  check_and_copy_varchar(c_op.amr_o1      ,l_oes_parties(2).amr_adress_o  ,'AMR_O1');
                  check_and_copy_varchar(c_op.amr_o2	    ,l_oes_parties(3).amr_adress_o  ,'AMR_O2');
                  check_and_copy_varchar(c_op.amr_o3      ,l_oes_parties(4).amr_adress_o  ,'AMR_O3');
                  check_and_copy_varchar(c_op.amr_o4      ,l_oes_parties(5).amr_adress_o  ,'AMR_O4');
                  check_and_copy_varchar(c_op.amr_r0	    ,l_oes_parties(1).amr_adress_r  ,'AMR_R0');
                  check_and_copy_varchar(c_op.amr_r1      ,l_oes_parties(2).amr_adress_r  ,'AMR_R1');
                  check_and_copy_varchar(c_op.amr_r2	    ,l_oes_parties(3).amr_adress_r  ,'AMR_R2');
                  check_and_copy_varchar(c_op.amr_r3      ,l_oes_parties(4).amr_adress_r  ,'AMR_R3');
                  check_and_copy_varchar(c_op.amr_r4      ,l_oes_parties(5).amr_adress_r  ,'AMR_R4');
                  check_and_copy_varchar(c_op.amr_s0	    ,l_oes_parties(1).amr_adress_s  ,'AMR_S0');
                  check_and_copy_varchar(c_op.amr_s1      ,l_oes_parties(2).amr_adress_s  ,'AMR_S1');
                  check_and_copy_varchar(c_op.amr_s2	    ,l_oes_parties(3).amr_adress_s  ,'AMR_S2');
                  check_and_copy_varchar(c_op.amr_s3      ,l_oes_parties(4).amr_adress_s  ,'AMR_S3');
                  check_and_copy_varchar(c_op.amr_s4      ,l_oes_parties(5).amr_adress_s  ,'AMR_S4');
                  check_and_copy_varchar(c_op.amr_u0	    ,l_oes_parties(1).amr_adress_u  ,'AMR_U0');
                  check_and_copy_varchar(c_op.amr_u1      ,l_oes_parties(2).amr_adress_u  ,'AMR_U1');
                  check_and_copy_varchar(c_op.amr_u2	    ,l_oes_parties(3).amr_adress_u  ,'AMR_U2');
                  check_and_copy_varchar(c_op.amr_u3      ,l_oes_parties(4).amr_adress_u  ,'AMR_U3');
                  check_and_copy_varchar(c_op.amr_u4      ,l_oes_parties(5).amr_adress_u  ,'AMR_U4');
                  check_and_copy_varchar(c_op.bik_b0	    ,l_oes_parties(1).bik_b         ,'BIK_B0');
                  check_and_copy_varchar(c_op.bik_b3	    ,l_oes_parties(4).bik_b         ,'BIK_B3');
                  check_and_copy_varchar(c_op.bik_is_b0	  ,l_oes_parties(1).bik_is_b      ,'BIK_IS_B0');
                  check_and_copy_varchar(c_op.bik_is_b3	  ,l_oes_parties(4).bik_is_b      ,'BIK_IS_B3');
                  check_and_copy_varchar(c_op.bik_r0	    ,l_oes_parties(1).bik_r         ,'BIK_R0');
                  check_and_copy_varchar(c_op.bik_r3	    ,l_oes_parties(4).bik_r         ,'BIK_R3');
                  check_and_copy_varchar(c_op.bp_0	      ,l_oes_parties(1).bp            ,'BP_0');
                  check_and_copy_varchar(c_op.bp_1        ,l_oes_parties(2).bp            ,'BP_1');
                  check_and_copy_varchar(c_op.bp_2	      ,l_oes_parties(3).bp            ,'BP_2');
                  check_and_copy_varchar(c_op.bp_3        ,l_oes_parties(4).bp            ,'BP_3');
                  check_and_copy_varchar(c_op.bp_4        ,l_oes_parties(5).bp            ,'BP_4');
                  check_and_copy_varchar(c_op.card_b0	    ,l_oes_parties(1).card_b        ,'CARD_B0');
                  check_and_copy_varchar(c_op.card_b3	    ,l_oes_parties(4).card_b        ,'CARD_B3');
                  check_and_copy_date   (c_op.gr0	        ,l_oes_parties(1).gr            ,'GR0','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.gr1         ,l_oes_parties(2).gr            ,'GR1','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.gr2	        ,l_oes_parties(3).gr            ,'GR2','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.gr3         ,l_oes_parties(4).gr            ,'GR3','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.gr4         ,l_oes_parties(5).gr            ,'GR4','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_varchar(c_op.kd0 	      ,l_oes_parties(1).kd            ,'KD0');
                  check_and_copy_varchar(c_op.kd1         ,l_oes_parties(2).kd            ,'KD1');
                  check_and_copy_varchar(c_op.kd2 	      ,l_oes_parties(3).kd            ,'KD2');
                  check_and_copy_varchar(c_op.kd3         ,l_oes_parties(4).kd            ,'KD3');
                  check_and_copy_varchar(c_op.kd4         ,l_oes_parties(5).kd            ,'KD4');
                  check_and_copy_varchar(c_op.kodcn_b0    ,l_oes_parties(1).kodcn_b       ,'KODCN_B0');
                  check_and_copy_varchar(c_op.kodcn_b3    ,l_oes_parties(4).kodcn_b       ,'KODCN_B3');
                  check_and_copy_varchar(c_op.kodcn_r0	  ,l_oes_parties(1).kodcn_r       ,'KODCN_R0');
                  check_and_copy_varchar(c_op.kodcn_r3    ,l_oes_parties(4).kodcn_r       ,'KODCN_R3');
                  check_and_copy_varchar(c_op.kodcn0	    ,l_oes_parties(1).kodcn         ,'KODCN0');
                  check_and_copy_varchar(c_op.kodcn1      ,l_oes_parties(2).kodcn         ,'KODCN1');
                  check_and_copy_varchar(c_op.kodcn2      ,l_oes_parties(3).kodcn         ,'KODCN2');
                  check_and_copy_varchar(c_op.kodcn3      ,l_oes_parties(4).kodcn         ,'KODCN3');
                  check_and_copy_varchar(c_op.kodcn4      ,l_oes_parties(5).kodcn         ,'KODCN4');
                  check_and_copy_varchar(c_op.kodcr0	    ,l_oes_parties(1).kodcr         ,'KODCR0');
                  check_and_copy_varchar(c_op.kodcr1      ,l_oes_parties(2).kodcr         ,'KODCR1');
                  check_and_copy_varchar(c_op.kodcr2      ,l_oes_parties(3).kodcr         ,'KODCR2');
                  check_and_copy_varchar(c_op.kodcr3      ,l_oes_parties(4).kodcr         ,'KODCR3');
                  check_and_copy_varchar(c_op.kodcr4      ,l_oes_parties(5).kodcr         ,'KODCR4');
                  check_and_copy_varchar(c_op.mc_01       ,l_oes_parties(1).mc_1          ,'MC_01');
                  check_and_copy_varchar(c_op.mc_11       ,l_oes_parties(2).mc_1          ,'MC_11');
                  check_and_copy_varchar(c_op.mc_21       ,l_oes_parties(3).mc_1          ,'MC_21');
                  check_and_copy_varchar(c_op.mc_31       ,l_oes_parties(4).mc_1          ,'MC_31');
                  check_and_copy_varchar(c_op.mc_41       ,l_oes_parties(5).mc_1          ,'MC_41');
                  check_and_copy_date   (c_op.mc_02       ,l_oes_parties(1).mc_2          ,'MC_02','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_12       ,l_oes_parties(2).mc_2          ,'MC_12','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_22       ,l_oes_parties(3).mc_2          ,'MC_22','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_32       ,l_oes_parties(4).mc_2          ,'MC_32','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_42       ,l_oes_parties(5).mc_2          ,'MC_42','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_03       ,l_oes_parties(1).mc_3          ,'MC_03','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_13       ,l_oes_parties(2).mc_3          ,'MC_13','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_23       ,l_oes_parties(3).mc_3          ,'MC_23','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_33       ,l_oes_parties(4).mc_3          ,'MC_33','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.mc_43       ,l_oes_parties(5).mc_3          ,'MC_43','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_varchar(c_op.name_b0     ,l_oes_parties(1).name_b        ,'NAME_B0');
                  check_and_copy_varchar(c_op.name_b3     ,l_oes_parties(4).name_b        ,'NAME_B3');
                  check_and_copy_varchar(c_op.name_is_b0  ,l_oes_parties(1).name_is_b     ,'NAME_IS_B0');
                  check_and_copy_varchar(c_op.name_is_b3  ,l_oes_parties(4).name_is_b     ,'NAME_IS_B3');
                  check_and_copy_varchar(c_op.name_r0     ,l_oes_parties(1).name_r        ,'NAME_R0');
                  check_and_copy_varchar(c_op.name_r3     ,l_oes_parties(4).name_r        ,'NAME_R3');
                  check_and_copy_varchar(c_op.nameu0	    ,l_oes_parties(1).nameu         ,'NAMEU0');
                  check_and_copy_varchar(c_op.nameu1      ,l_oes_parties(2).nameu         ,'NAMEU1');
                  check_and_copy_varchar(c_op.nameu2      ,l_oes_parties(3).nameu         ,'NAMEU2');
                  check_and_copy_varchar(c_op.nameu3      ,l_oes_parties(4).nameu         ,'NAMEU3');
                  check_and_copy_varchar(c_op.nameu4      ,l_oes_parties(5).nameu         ,'NAMEU4');
                  check_and_copy_varchar(c_op.nd0	        ,l_oes_parties(1).nd            ,'ND0');
                  check_and_copy_varchar(c_op.nd1         ,l_oes_parties(2).nd            ,'ND1');
                  check_and_copy_varchar(c_op.nd2	        ,l_oes_parties(3).nd            ,'ND2');
                  check_and_copy_varchar(c_op.nd3         ,l_oes_parties(4).nd            ,'ND3');
                  check_and_copy_varchar(c_op.nd4         ,l_oes_parties(5).nd            ,'ND4');
                  check_and_copy_varchar(c_op.pru0	      ,l_oes_parties(1).pru           ,'PRU0');
                  check_and_copy_varchar(c_op.pru1        ,l_oes_parties(2).pru           ,'PRU1');
                  check_and_copy_varchar(c_op.pru2	      ,l_oes_parties(3).pru           ,'PRU2');
                  check_and_copy_varchar(c_op.pru3        ,l_oes_parties(4).pru           ,'PRU3');
                  check_and_copy_varchar(c_op.pru4        ,l_oes_parties(5).pru           ,'PRU4');
                  check_and_copy_varchar(c_op.rg0	        ,l_oes_parties(1).rg            ,'RG0');
                  check_and_copy_varchar(c_op.rg1         ,l_oes_parties(2).rg            ,'RG1');
                  check_and_copy_varchar(c_op.rg2	        ,l_oes_parties(3).rg            ,'RG2');
                  check_and_copy_varchar(c_op.rg3         ,l_oes_parties(4).rg            ,'RG3');
                  check_and_copy_varchar(c_op.rg4         ,l_oes_parties(5).rg            ,'RG4');
                  check_and_copy_varchar(c_op.sd0	        ,l_oes_parties(1).sd            ,'SD0');
                  check_and_copy_varchar(c_op.sd1         ,l_oes_parties(2).sd            ,'SD1');
                  check_and_copy_varchar(c_op.sd2	        ,l_oes_parties(3).sd            ,'SD2');
                  check_and_copy_varchar(c_op.sd3         ,l_oes_parties(4).sd            ,'SD3');
                  check_and_copy_varchar(c_op.sd4         ,l_oes_parties(5).sd            ,'SD4');
                  check_and_copy_varchar(c_op.tu0	        ,l_oes_parties(1).tureal        ,'TU0');
                  check_and_copy_varchar(c_op.tu1         ,l_oes_parties(2).tureal        ,'TU1');
                  check_and_copy_varchar(c_op.tu2	        ,l_oes_parties(3).tureal        ,'TU2');
                  check_and_copy_varchar(c_op.tu3         ,l_oes_parties(4).tureal        ,'TU3');
                  check_and_copy_varchar(c_op.tu4         ,l_oes_parties(5).tureal        ,'TU4');
                  check_and_copy_varchar(c_op.vd01        ,l_oes_parties(1).vd_1          ,'VD01');
                  check_and_copy_varchar(c_op.vd11        ,l_oes_parties(2).vd_1          ,'VD11');
                  check_and_copy_varchar(c_op.vd21        ,l_oes_parties(3).vd_1          ,'VD21');
                  check_and_copy_varchar(c_op.vd31        ,l_oes_parties(4).vd_1          ,'VD31');
                  check_and_copy_varchar(c_op.vd41        ,l_oes_parties(5).vd_1          ,'VD41');
                  check_and_copy_varchar(c_op.vd02        ,l_oes_parties(1).vd_2          ,'VD02');
                  check_and_copy_varchar(c_op.vd12        ,l_oes_parties(2).vd_2          ,'VD12');
                  check_and_copy_varchar(c_op.vd22        ,l_oes_parties(3).vd_2          ,'VD22');
                  check_and_copy_varchar(c_op.vd32        ,l_oes_parties(4).vd_2          ,'VD32');
                  check_and_copy_varchar(c_op.vd42        ,l_oes_parties(5).vd_2          ,'VD42');
                  check_and_copy_date   (c_op.vd03        ,l_oes_parties(1).vd_3          ,'VD03','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd13        ,l_oes_parties(2).vd_3          ,'VD13','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd23        ,l_oes_parties(3).vd_3          ,'VD23','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd33        ,l_oes_parties(4).vd_3          ,'VD33','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd43        ,l_oes_parties(5).vd_3          ,'VD43','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_varchar(c_op.vd04        ,l_oes_parties(1).vd_4          ,'VD04');
                  check_and_copy_varchar(c_op.vd14        ,l_oes_parties(2).vd_4          ,'VD14');
                  check_and_copy_varchar(c_op.vd24        ,l_oes_parties(3).vd_4          ,'VD24');
                  check_and_copy_varchar(c_op.vd34        ,l_oes_parties(4).vd_4          ,'VD34');
                  check_and_copy_varchar(c_op.vd44        ,l_oes_parties(5).vd_4          ,'VD44');
                  check_and_copy_varchar(c_op.vd05        ,l_oes_parties(1).vd_5          ,'VD05');
                  check_and_copy_varchar(c_op.vd15        ,l_oes_parties(2).vd_5          ,'VD15');
                  check_and_copy_varchar(c_op.vd25        ,l_oes_parties(3).vd_5          ,'VD25');
                  check_and_copy_varchar(c_op.vd35        ,l_oes_parties(4).vd_5          ,'VD35');
                  check_and_copy_varchar(c_op.vd45        ,l_oes_parties(5).vd_5          ,'VD45');
                  check_and_copy_date   (c_op.vd06        ,l_oes_parties(1).vd_6          ,'VD06','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd16        ,l_oes_parties(2).vd_6          ,'VD16','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd26        ,l_oes_parties(3).vd_6          ,'VD26','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd36        ,l_oes_parties(4).vd_6          ,'VD36','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd46        ,l_oes_parties(5).vd_6          ,'VD46','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd07        ,l_oes_parties(1).vd_7          ,'VD07','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd17        ,l_oes_parties(2).vd_7          ,'VD17','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd27        ,l_oes_parties(3).vd_7          ,'VD27','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd37        ,l_oes_parties(4).vd_7          ,'VD37','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_date   (c_op.vd47        ,l_oes_parties(5).vd_7          ,'VD47','DD.MM.YYYY HH24:MI:SS');
                  check_and_copy_varchar(c_op.vp_0        ,l_oes_parties(1).vp            ,'VP_0');
                  check_and_copy_varchar(c_op.vp_3        ,l_oes_parties(4).vp            ,'VP_3');
                  -- TODO: ��� � ����, ���� �����������:
                  --RESERV612
                  --RESRV_B02
                  --RESRV_B32
                  --RESRV02
                  --RESRV12
                  --RESRV22
                  --RESRV32
                  --RESRV42
                  
                EXCEPTION
                  WHEN conversion_error THEN
                    ROLLBACK TO SAVEPOINT imp_321xml_file;
                    rf_pkg_import.log_imp_history('R',l_file_id,'E','������: '||SUBSTR(sqlerrm,12), NULL,NULL,c_file.file_nm,l_file_len,NULL,l_file_body);
                    CONTINUE fileloop;
                END;

                -- ������� ����� � ���
                rf_pkg_oes.create_oes_321_manual(
                  p_branch_id     => l_branch_id
                 ,p_owner_seq_id  => l_owner_seq_id
                 ,p_creat_id      => l_user_seq_id
                 ,p_note_tx       => '��������� �� ����� '|| c_file.file_nm
                 ,p_oper          => l_oes_oper 
                 ,p_parties       => l_oes_parties
                 ,p_review_id     => l_review_id
                 ,p_oes_321_id    => l_oes_321_id
                 );
                
                -- ����������� ��� � �����
                INSERT INTO mantas.rf_imp_oes (form_cd,oes_id,imp_file_id,oes_order_nb)
                  VALUES ('321',l_oes_321_id,l_file_id,1);

                l_load_qty:=l_load_qty+1;
              END;
            END LOOP oploop;
            
          -- ������� ������ � ����������� �������, ������� �� ������������� �������� �������  
          l_filtered_cnt:=0;
          l_filtered_idx:=l_filtered.FIRST;
          WHILE (l_filtered_idx IS NOT NULL)
            LOOP
              l_filtered_msg:=l_filtered_msg||','||l_filtered_idx||' - '||TO_CHAR(l_filtered(l_filtered_idx));
              l_filtered_cnt:=l_filtered_cnt+l_filtered(l_filtered_idx);
              l_filtered_idx:=l_filtered.NEXT(l_filtered_idx);
            END LOOP;
          IF l_filtered_cnt>0 THEN
            l_filtered_msg:='�� ������������� �������� �������: '||TO_CHAR(l_filtered_cnt)||', � �.�. �� �����: '||LTRIM(l_filtered_msg,',');
            rf_pkg_import.log_imp_history('M',l_file_id,'O',l_filtered_msg,NULL ,NULL,c_file.file_nm, l_file_len,NULL,l_file_body);
          END IF;  

          -- ������� ������ �� �������� �������� �����
          rf_pkg_import.log_imp_history('R',l_file_id,'O',NULL,l_load_qty ,NULL,c_file.file_nm, l_file_len,NULL,l_file_body);
          l_good_file_cnt:=l_good_file_cnt+1;  
        EXCEPTION WHEN OTHERS THEN
          ROLLBACK TO SAVEPOINT imp_321xml_file;
          rf_pkg_import.log_imp_history('M',l_file_id,'E','���������� ������: '||dbms_utility.format_error_stack(), NULL,NULL,c_file.file_nm,NULL,NULL);
        END;  

      END LOOP fileloop;

    p_message:='��������� ������:'||TO_CHAR(l_file_cnt)||' �� ��� �������:'||TO_CHAR(l_good_file_cnt)||' � ��������:'||TO_CHAR(l_file_cnt-l_good_file_cnt) ;
    -- ���� �� ���� ���� �� ������ ���������, ����� ���������� ��� ���� ������
    IF l_good_file_cnt = 0 THEN
      RETURN 0;
    END IF;  
    RETURN 1;
  END process_321xml_8001_files;
  
END;
/
