-- ��������� ������ � set define off
begin
  execute immediate 'drop type mantas.rf_tab_trxn_search';
  exception
    when others then null;
end;
/

create or replace type mantas.rf_type_trxn_search as object (
  op_cat_cd         varchar2(4 char),   --��������� ��������
  trxn_id           number,             --ID ����������
  branch_id         number,             --ID ��������� ����� �� � ���
  exctn_dt          date,               --���� ��������
  base_am           number,             --����� ��������
  desc_tx           varchar2(255 char), --���������� �������
  src_cd            varchar2(3 char),   --������� ��������
  orig_cust_id      number,             --ID �����������
  orig_full_nm      varchar2(900 char), --������������ �����������
  orig_inn_nb       varchar2(64 char),  --��� �����������
  orig_acct_nb      varchar2(64 char),  --����� ����� �����������
  orig_resident_fl  varchar2(1 char),   --������� ��������� �� ��� �����������
  benef_cust_id     number,             --ID ����������
  benef_full_nm     varchar2(900 char), --������������ ����������
  benef_inn_nb      varchar2(64 char),  --��� ����������
  benef_acct_nb     varchar2(64 char),  --����� ����� ����������
  benef_resident_fl varchar2(1 char),   --������� ��������� �� ��� ����������
  debit_acct_nb     varchar2(64 char),  --���� ������
  credit_acct_nb    varchar2(64 char),  --���� �������
  orig_instn_id     varchar2(50 char),  --���/SWIFT ����� �����������
  benef_instn_id    varchar2(50 char),  --���/SWIFT ����� ����������
  orig_acct_id      number,             --ID ����� ����������� 
  benef_acct_id     number,             --ID ����� ����������
  src_sys_cd        varchar2(3 char)    --��� ������� ���������
)
/

create or replace type mantas.rf_tab_trxn_search as table of mantas.rf_type_trxn_search
/

create or replace function mantas.rfv_trxn_search (
  p_op_cat       in varchar2,
  p_party_type   in varchar2,
  p_date_from    in date,
  p_date_to      in date default null,
  p_ammount_from in number default 0,
  p_ammount_to   in number default null,
  p_inn1         in varchar2 default null,
  p_wl_inn1      in varchar2 default null, --�������� ��� �� ������ RF_WATCH_LIST, � �������� ��������� ����������� RF_WATCH_LIST_ORG
  p_acct1        in varchar2 default null,
  p_debit_acct   in varchar2 default null,
  p_inn2         in varchar2 default null,
  p_wl_inn2      in varchar2 default null, --�������� ��� �� ������ RF_WATCH_LIST, � �������� ��������� ����������� RF_WATCH_LIST_ORG
  p_acct2        in varchar2 default null,
  p_credit_acct  in varchar2 default null,
  p_print_sql    in varchar2 default 'N'
) return mantas.rf_tab_trxn_search pipelined 
is
  type v_cursor is ref cursor;
  v_trxn_cursor v_cursor;
  v_trxn        mantas.rf_type_trxn_search;
  v_sql         varchar2(32000);
  v_sql_wt      varchar2(32000);
  v_sql_ct      varchar2(32000);
  v_sql_bt      varchar2(32000);
  v_sql_cust    varchar2(32000);
  v_sql_where   varchar2(32000);
  v_sql_inn     varchar2(32000);
  v_sql_acct    varchar2(32000);
  v_inn_type    varchar2(4);
  v_acct_type1  varchar2(4);
  v_acct_type2  varchar2(4);
  v_field_list  varchar2(4096);
  v_hint        varchar2(256);
  v_op_cat      varchar2(16);
  v_wl_type     varchar2(4);
begin
  v_trxn := mantas.rf_type_trxn_search(
    null, null, null, null, null, null, null, null, null, null, 
    null, null, null, null, null, null, null, null, null, null,
    null, null, null, null
  );
  
  /* ����� �� ����������� � ���������� ���� ������ ��� WT */
  if p_party_type = 'O&B' then 
    if instr(p_op_cat, 'WT') > 0 then 
      v_op_cat := 'WT';
    else 
      v_op_cat := '';
    end if;  
  else
    v_op_cat := p_op_cat;
  end if;
  
  /* ������� �� ��� */
  if p_inn1 is not null and p_inn2 is not null and p_party_type = 'O&B' then 
    v_inn_type := 'O&B';
  elsif p_inn1 is not null and p_inn2 is not null and p_party_type = 'O|B' then
    v_inn_type := 'O|B';
  elsif p_inn1 is not null then 
    v_inn_type := 'O';
  elsif p_inn2 is not null then 
    v_inn_type := 'B';
  else
    v_inn_type := null;
  end if;
  
  /* ������� �� ������� ��� ����������� */
  if p_wl_inn1 is not null and p_wl_inn2 is not null and p_party_type = 'O&B' then 
    v_wl_type := 'O&B';
  elsif p_wl_inn1 is not null and p_wl_inn2 is not null and p_party_type = 'O|B' then
    v_wl_type := 'O|B';
  elsif p_wl_inn1 is not null then 
    v_wl_type := 'O';
  elsif p_wl_inn2 is not null then 
    v_wl_type := 'B';
  else
    v_wl_type := null;
  end if;  
  
  /* ������� �� ����.
   *  - O / B = ���������� / ����������
   *  - A = �������� ����
   *  - D / C = ����� / ������
   */
  if instr(p_party_type, 'O') > 0 then
    if p_acct1 is not null and p_debit_acct is not null then
      v_acct_type1 := 'OAD';
    elsif p_acct1 is not null then
      v_acct_type1 := 'OA';
    elsif p_debit_acct is not null then
      v_acct_type1 := 'OD';
    else
      v_acct_type2 := null;
    end if;
  end if;
  if instr(p_party_type, 'B') > 0 then
    if p_acct2 is not null and p_credit_acct is not null then
      v_acct_type2 := 'BAC';
    elsif p_acct2 is not null then
      v_acct_type2 := 'BA';
    elsif p_credit_acct is not null then
      v_acct_type2 := 'BC';
    else
      v_acct_type2 := null;
    end if;
  end if;
  
  /* WIRE_TRXN */
  if instr(v_op_cat, 'WT') > 0 then
    v_sql_inn := null;
    v_sql_acct := null;
    v_sql_cust := null;
    
    v_field_list := q'{
      'WT' as op_cat_cd,
      t.fo_trxn_seq_id as trxn_id,
      t.rf_branch_id as branch_id,
      t.trxn_exctn_dt as exctn_dt,
      t.rf_trxn_opok_am as base_am,
      t.orig_to_benef_instr_tx as desc_tx,
      t.src_sys_cd as src_cd,
      t.rf_orig_cust_seq_id as orig_cust_id,
      trim(t.orig_nm||' '||t.rf_orig_first_nm||' '||t.rf_orig_midl_nm) as orig_full_nm,
      t.rf_orig_inn_nb as orig_inn_nb,
      t.rf_orig_acct_nb as orig_acct_nb,
      t.rf_orig_resident_fl as orig_resident_fl,
      t.rf_benef_cust_seq_id as benef_cust_id,
      trim(t.benef_nm||' '||t.rf_benef_first_nm||' '||t.rf_benef_midl_nm) as benef_full_nm,
      t.rf_benef_inn_nb as benef_inn_nb,
      t.rf_benef_acct_nb as benef_acct_nb,
      t.rf_benef_resident_fl as benef_resident_fl,
      t.rf_debit_cd as debit_acct_nb,
      t.rf_credit_cd as credit_acct_nb,
      t.send_instn_id as orig_instn_id,
      t.rcv_instn_id as benef_instn_id,
      t.rf_orig_acct_seq_id as orig_acct_id,
      t.rf_benef_acct_seq_id as benef_acct_id,
      t.src_sys_cd as src_sys_cd
    }';
    
    v_hint := '/*+ FIRST_ROWS(1) */ ';
    v_sql_wt := 'select '||v_hint||v_field_list||' from business.wire_trxn t where ';
    
    /* ������� - ���� � � ����� �� */ 
    v_sql_where := 'nvl(t.rf_canceled_fl, ''N'') <> ''Y'' and t.trxn_exctn_dt >= to_date('''||to_char(p_date_from, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') and t.rf_trxn_opok_am >= '||p_ammount_from;
    /* ������� - ���� �� */
    if p_date_to is not null then
      v_sql_where := v_sql_where||' and t.trxn_exctn_dt < to_date('''||to_char(p_date_to, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') + 1';
    end if;
    /* ������� - ����� �� */
    if p_ammount_to is not null then
      v_sql_where := v_sql_where||' and t.rf_trxn_opok_am <= '||p_ammount_to;
    end if;
    /* ������� �� ���� �����������������, ���� �������� ��������� �� ������ */
    if v_inn_type is null and v_acct_type1 is null and v_acct_type2 is null then
      v_sql_where := v_sql_where||' and t.rf_partition_date >= to_date('''||to_char(p_date_from, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'')';
      if p_date_to is not null then
        v_sql_where := v_sql_where||' and ( t.rf_partition_date < to_date('''||to_char(p_date_to, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') + 1 '||
                       'or t.rf_partition_date >= to_date(''01.01.9999'', ''DD.MM.YYYY'') )';
      end if;
    end if;
    
    /* ������� - ��� ����������� �/��� ���������� */
    case 
      when v_inn_type = 'O'   then v_sql_inn := ' and t.rf_orig_inn_nb = '''||p_inn1||'''';
      when v_inn_type = 'B'   then v_sql_inn := ' and t.rf_benef_inn_nb = '''||p_inn2||'''';
      when v_inn_type = 'O&B' then v_sql_inn := ' and t.rf_orig_inn_nb = '''||p_inn1||''' and t.rf_benef_inn_nb = '''||p_inn2||'''';
      when v_inn_type = 'O|B' then v_sql_inn := ' and (t.rf_orig_inn_nb = '''||p_inn1||''' or t.rf_benef_inn_nb = '''||p_inn2||''')';
      else null;
    end case;
    
    /* ������� - ������� ��� ����������� ����������� �/��� ���������� */
    case 
      when v_wl_type = 'O'   then v_sql_inn := v_sql_inn||' and t.rf_orig_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')';
      when v_wl_type = 'B'   then v_sql_inn := v_sql_inn||' and t.rf_benef_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')';
      when v_wl_type = 'O&B' then v_sql_inn := v_sql_inn||' and t.rf_orig_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'||
                                                          ' and t.rf_benef_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')';
      /* ��� ������� ���������� ��� ���������� ����� �� ��� ���������� ������������� � ������� �� ��������. ������� ������� ������� �� ���. */
      when v_wl_type = 'O|B' and v_inn_type = 'O|B' then v_sql_inn := ' and (t.rf_orig_inn_nb = '''||p_inn1||''' and t.rf_orig_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||'''))'||
                                                                      ' or (t.rf_benef_inn_nb = '''||p_inn2||''' and t.rf_benef_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||'''))';
      when v_wl_type = 'O|B' then v_sql_inn := v_sql_inn||' and (t.rf_orig_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'||
                                                          ' or t.rf_benef_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||'''))';
      else null;
    end case;
    
    /* ������� - ����� ����� ����������� �/��� ���������� */
    if v_acct_type1 is not null or v_acct_type2 is not null then
      case
        when v_acct_type1 = 'OAD' then v_sql_acct := '(upper(t.rf_orig_acct_nb) = '''||p_acct1||''' or t.rf_debit_cd = '''||p_debit_acct||''')';
        when v_acct_type1 = 'OA'  then v_sql_acct := 'upper(t.rf_orig_acct_nb) = '''||p_acct1||'''';
        when v_acct_type1 = 'OD'  then v_sql_acct := 't.rf_debit_cd = '''||p_debit_acct||'''';
        else null;
      end case;
      if v_acct_type1 is not null and v_acct_type2 is not null then
        v_sql_acct := v_sql_acct||case
                                    when substr(p_party_type, 2, 1) = '&' then ' and '
                                    when substr(p_party_type, 2, 1) = '|' then ' or '
                                  end;  
      end if;
      case
        when v_acct_type2 = 'BAC' then v_sql_acct := v_sql_acct||'(upper(t.rf_benef_acct_nb) = '''||p_acct2||''' or t.rf_credit_cd = '''||p_credit_acct||''')';
        when v_acct_type2 = 'BA'  then v_sql_acct := v_sql_acct||'upper(t.rf_benef_acct_nb) = '''||p_acct2||'''';
        when v_acct_type2 = 'BC'  then v_sql_acct := v_sql_acct||'t.rf_credit_cd = '''||p_credit_acct||'''';
        else null;
      end case;
      v_sql_acct := chr(13)||'and ('||v_sql_acct||')';
    end if;

    v_sql_wt := v_sql_wt||v_sql_where||v_sql_inn||v_sql_acct;
    
    /* ����� �� business.cust */
    if v_inn_type is not null or v_wl_type is not null then
      case 
        when v_inn_type = 'O' or v_wl_type = 'O' then 
          v_sql_cust := 
            /* ����� �� �������� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            'from business.cust c '||
            'join business.wire_trxn t on t.rf_orig_cust_seq_id = c.cust_seq_id '||
            'where '||v_sql_where||v_sql_acct||' and t.rf_orig_inn_nb is null'||
            /* ����� �� ��� */
            case
              when v_inn_type = 'O' then ' and c.tax_id = '''||p_inn1||''''
            end ||
            /* ����� �� ������� ��� */
            case
              when v_wl_type = 'O' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'
            end;

        when v_inn_type = 'B' or v_wl_type = 'B' then 
          v_sql_cust := 
            /* ����� �� �������� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.wire_trxn t on t.rf_benef_cust_seq_id = c.cust_seq_id '||
            'where '||v_sql_where||v_sql_acct||' and t.rf_benef_inn_nb is null'||
            /* ����� �� ��� */
            case
              when v_inn_type = 'B' then ' and c.tax_id = '''||p_inn2||''''
            end || 
            /* ����� �� ������� ��� */
            case
              when v_wl_type = 'B' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')'
            end;  

        when v_inn_type = 'O&B' or v_wl_type = 'O&B' then 
          v_sql_cust := 
            /* ����� �� ��� �� ��������: ���������� �� ��������� - ���������� �� �������� -> �������� ������ */
            
            /* ����� �� ��� �� ��������: ���������� �� ��������� - ���������� �� ����������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.wire_trxn t on t.rf_orig_cust_seq_id = c.cust_seq_id '||
            /* ����� �������� ������ ��� ����������� � �������� ������ ���� ������, � ��� ���������� �������� */
            'where +'||v_sql_where||v_sql_acct||' and t.rf_orig_inn_nb is null and t.rf_benef_inn_nb is not null'||
            /* ����� �� ��� */
            case
              when v_inn_type = 'O&B' then ' and c.tax_id = '''||p_inn1||''' and t.rf_benef_inn_nb = '''||p_inn2||''''
            end || 
            /* ����� �� ������� ��� */
            case 
              when v_wl_type = 'O&B' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'||
                                          ' and t.rf_benef_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')'
            end
            ||chr(13)||' union all '||chr(13)||
            
            /* ����� �� ��� �� ��������: ���������� �� ����������� - ���������� �� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.wire_trxn t on t.rf_benef_cust_seq_id = c.cust_seq_id '||
            /* ����� �������� ������ ��� ����������� � �������� ������ ���� ��������, � ��� ���������� ������ */
            'where +'||v_sql_where||v_sql_acct||' and t.rf_orig_inn_nb is not null and t.rf_benef_inn_nb is null'||
            /* ����� �� ��� */
            case
              when v_inn_type = 'O&B' then ' and t.rf_orig_inn_nb = '''||p_inn1||''' and c.tax_id = '''||p_inn2||''''
            end || 
            /* ����� �� ������� ��� */
            case 
              when v_wl_type = 'O&B' then ' and t.rf_orig_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'||
                                          ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')'
            end
            ||chr(13)||' union all '||chr(13)||
            
            /* ����� �� ��� �� ��������: ���������� �� ����������� - ���������� �� ����������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.wire_trxn t on t.rf_orig_cust_seq_id = c.cust_seq_id '||
            'join business.cust c2 on c2.cust_seq_id = t.rf_benef_cust_seq_id '||
            /* ����� �������� ������ ��� ����������� � ���������� � �������� ������ ���� ������ */
            'where +'||v_sql_where||v_sql_acct||' and t.rf_orig_inn_nb is null and t.rf_benef_inn_nb is null'||
            /* ����� �� ��� */
            case
              when v_inn_type = 'O&B' then ' and c.tax_id = '''||p_inn1||''' and c2.tax_id = '''||p_inn2||''''
            end || 
            /* ����� �� ������� ��� */
            case 
              when v_wl_type = 'O&B' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'||
                                          ' and c2.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')'
            end;
            
        when v_inn_type = 'O|B' or v_wl_type = 'O|B' then 
          v_sql_cust := 
            /* ����� �� �������� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.wire_trxn t on t.rf_orig_cust_seq_id = c.cust_seq_id '||
            'where '||v_sql_where||v_sql_acct||' and t.rf_orig_inn_nb is null'||
            
            case
              /* ���� ������ �������� ��� � ��������, �� ����������� ������� ������ */
              when v_inn_type = 'O|B' and v_wl_type = 'O|B' then ' and c.tax_id = '''||p_inn1||''' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'||
                                                                  ' and (nvl(t.rf_benef_inn_nb, ''0'') <> '''||p_inn2||''' or not exists (select 1 from mantas.rf_watch_list_org l where l.watch_list_cd = '''||p_wl_inn2||''' and l.inn_nb = t.rf_benef_inn_nb ))'
              /* ���� ������ ������� ������ �� ��� */
              when v_inn_type = 'O|B' then ' and c.tax_id = '''||p_inn1||''' and nvl(t.rf_benef_inn_nb, ''0'') <> '''||p_inn2||''''
              /* ���� ������ ������� ������ �� ������� */
              when v_wl_type = 'O|B' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'||
                                          ' and not exists (select 1 from mantas.rf_watch_list_org l where l.watch_list_cd = '''||p_wl_inn2||''' and l.inn_nb = t.rf_benef_inn_nb)'
              else ''
            end||

            chr(13)||'union all'||chr(13)||
            /* ����� �� �������� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.wire_trxn t on t.rf_benef_cust_seq_id = c.cust_seq_id '||
            'left join business.cust c2 on c2.cust_seq_id = t.rf_orig_cust_seq_id '||
            'where '||v_sql_where||v_sql_acct||' and t.rf_benef_inn_nb is null'||
            
            case
              /* ���� ������ �������� ��� � ��������, �� ����������� ������� ������ */
              when v_inn_type = 'O|B' and v_wl_type = 'O|B' then ' and (c.tax_id = '''||p_inn2||''' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||'''))'||
                                                                 ' and not (nvl(t.rf_orig_inn_nb, ''0'') <> '''||p_inn2||''' or nvl(t.rf_orig_cust_seq_id, ''0'') <> nvl(t.rf_benef_cust_seq_id, ''0'') or nvl(c2.tax_id, ''0'') <> '''||p_inn2||''''||
                                                                 ' or not exists (select 1 from mantas.rf_watch_list_org l where l.watch_list_cd = '''||p_wl_inn2||''' and l.inn_nb = t.rf_orig_inn_nb))'||
                                                                 ' and not exists (select 1 from mantas.rf_watch_list_org l where l.watch_list_cd = '''||p_wl_inn2||''' and l.inn_nb = t.c2.tax_id)'
              /* ���� ������ ������� ������ �� ��� */
              when v_inn_type = 'O|B' then ' and c.tax_id = '''||p_inn2||''' and not (nvl(t.rf_orig_inn_nb, ''0'') <> '''||p_inn1||''' or nvl(t.rf_orig_cust_seq_id, ''0'') <> nvl(t.rf_benef_cust_seq_id, ''0'') or nvl(c2.tax_id, ''0'') <> '''||p_inn1||''')'
              /* ���� ������ ������� ������ �� ������� */
              when v_wl_type = 'O|B' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''') '||
                                          ' and not exists (select 1 from mantas.rf_watch_list_org l where l.watch_list_cd = '''||p_wl_inn1||''' and l.inn_nb = t.rf_orig_inn_nb)'||
                                          ' and not exists (select 1 from mantas.rf_watch_list_org l where l.watch_list_cd = '''||p_wl_inn1||''' and l.inn_nb = t.c2.tax_id)'
              else ''
            end;

        else v_sql_cust := '';
      end case;
      v_sql_wt := v_sql_wt||chr(13)||'union all'||chr(13)||v_sql_cust;
    end if;
    
  end if;

  /* CASH_TRXN */
  if instr(v_op_cat, 'CT') > 0 then
    v_sql_inn := null;
    v_sql_acct := null;
    v_sql_cust := null;
    
    v_field_list := q'{
      'CT' as op_cat_cd, 
      t.fo_trxn_seq_id as trxn_id,
      t.rf_branch_id as branch_id,
      t.trxn_exctn_dt as exctn_dt,
      t.rf_trxn_opok_am as base_am,
      t.desc_tx as desc_tx,
      t.src_sys_cd as src_cd,
      case when t.dbt_cdt_cd = 'D' then t.rf_cust_seq_id end as orig_cust_id,
      case when t.dbt_cdt_cd = 'D' then trim(t.rf_cust_nm||' '||t.rf_cust_first_nm||' '||t.rf_cust_midl_nm) end as orig_full_nm,
      case when t.dbt_cdt_cd = 'D' then t.rf_cust_inn_nb end as orig_inn_nb,
      case when t.dbt_cdt_cd = 'D' then t.rf_acct_nb end as orig_acct_nb,
      case when t.dbt_cdt_cd = 'D' then t.rf_cndtr_resident_fl end as orig_resident_fl,
      case when t.dbt_cdt_cd = 'C' then t.rf_cust_seq_id end as benef_cust_id,
      case when t.dbt_cdt_cd = 'C' then trim(t.rf_cust_nm||' '||t.rf_cust_first_nm||' '||t.rf_cust_midl_nm) end as benef_full_nm,
      case when t.dbt_cdt_cd = 'C' then t.rf_cust_inn_nb end as benef_inn_nb,
      case when t.dbt_cdt_cd = 'C' then t.rf_acct_nb end as benef_acct_nb,
      case when t.dbt_cdt_cd = 'C' then t.rf_cndtr_resident_fl end as benef_resident_fl,
      t.rf_debit_cd as debit_acct_nb,
      t.rf_credit_cd as credit_acct_nb,
      null as orig_instn_id,
      null as benef_instn_id,
      case when t.dbt_cdt_cd = 'D' then t.rf_acct_seq_id end as orig_acct_id, 
      case when t.dbt_cdt_cd = 'C' then t.rf_acct_seq_id end as benef_acct_id,
      t.src_sys_cd as src_sys_cd
    }';
    
    v_hint := '/*+ FIRST_ROWS(1) */ ';
    v_sql_ct := 'select '||v_hint||v_field_list||' from business.cash_trxn t where ';
    
    /* ������� - ���� � � ����� �� */ 
    v_sql_where := 'nvl(t.rf_canceled_fl, ''N'') <> ''Y'' and t.trxn_exctn_dt >= to_date('''||to_char(p_date_from, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') and t.rf_trxn_opok_am >= '||p_ammount_from;
    /* ������� - ���� �� */
    if p_date_to is not null then
      v_sql_where := v_sql_where||' and t.trxn_exctn_dt < to_date('''||to_char(p_date_to, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') + 1';
    end if;
    /* ������� - ����� �� */
    if p_ammount_to is not null then
      v_sql_where := v_sql_where||' and t.rf_trxn_opok_am <= '||p_ammount_to;
    end if;
    /* ������� �� ���� �����������������, ���� �������� ��������� �� ������ */
    if v_inn_type is null and v_acct_type1 is null and v_acct_type2 is null then
      v_sql_where := v_sql_where||' and t.rf_partition_date >= to_date('''||to_char(p_date_from, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'')';
      if p_date_to is not null then
        v_sql_where := v_sql_where||' and ( t.rf_partition_date < to_date('''||to_char(p_date_to, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') + 1 '||
                       'or t.rf_partition_date >= to_date(''01.01.9999'', ''DD.MM.YYYY'') )';
      end if;
    end if;
    
    /* ������� - ��� ����������� ��� ���������� */
    case 
      when v_inn_type = 'O' then v_sql_inn := ' and t.dbt_cdt_cd = ''D'' and t.rf_cust_inn_nb = '''||p_inn1||'''';
      when v_inn_type = 'B' then v_sql_inn := ' and t.dbt_cdt_cd = ''C'' and t.rf_cust_inn_nb = '''||p_inn2||'''';
      when v_inn_type = 'O|B' then v_sql_inn := ' and t.dbt_cdt_cd in (''D'', ''C'') and t.rf_cust_inn_nb = '''||p_inn1||'''';
      else null;
    end case;
    
    /* ������� - ������� ��� ����������� ����������� �/��� ���������� */
    case 
      when v_wl_type = 'O'   then 
        v_sql_inn := v_sql_inn||' and t.dbt_cdt_cd = ''D'''||
                    ' and t.rf_cust_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')';
      when v_wl_type = 'B'   then 
        v_sql_inn := v_sql_inn||' and t.dbt_cdt_cd = ''C'''||
                     ' and t.rf_cust_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')';
      when v_wl_type = 'O|B' then 
        v_sql_inn := v_sql_inn||' and t.dbt_cdt_cd in (''D'', ''C'')'||
                     ' and t.rf_cust_inn_nb in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd in ('''||p_wl_inn1||''', '''||p_wl_inn2||'''))';
      else null;
    end case;

    /* ������� - ����� ����� ����������� ��� ���������� */
    if v_acct_type1 is not null or v_acct_type2 is not null then
      case
        when v_acct_type1 = 'OAD' then v_sql_acct := '(t.dbt_cdt_cd = ''D'' and upper(t.rf_acct_nb) = '''||p_acct1||''' or t.rf_debit_cd = '''||p_debit_acct||''')';
        when v_acct_type1 = 'OA'  then v_sql_acct := '(t.dbt_cdt_cd = ''D'' and upper(t.rf_acct_nb) = '''||p_acct1||''')';
        when v_acct_type1 = 'OD'  then v_sql_acct := '(t.rf_debit_cd = '''||p_debit_acct||''')';
        else null;
      end case;
      if v_acct_type1 is not null and v_acct_type2 is not null then
        v_sql_acct := v_sql_acct||' or ';  
      end if;
      case
        when v_acct_type2 = 'BAC' then v_sql_acct := v_sql_acct||'(t.dbt_cdt_cd = ''C'' and upper(t.rf_acct_nb) = '''||p_acct2||''' or t.rf_credit_cd = '''||p_credit_acct||''')';
        when v_acct_type2 = 'BA'  then v_sql_acct := v_sql_acct||'(t.dbt_cdt_cd = ''C'' and upper(t.rf_acct_nb) = '''||p_acct2||''')';
        when v_acct_type2 = 'BC'  then v_sql_acct := v_sql_acct||'(t.rf_credit_cd = '''||p_credit_acct||''')';
        else null;
      end case;
      v_sql_acct := chr(13)||'and ('||v_sql_acct||')';
    end if;
    
    v_sql_ct := v_sql_ct||v_sql_where||v_sql_inn||v_sql_acct;

    /* ����� �� business.cust */
    if v_inn_type is not null or v_wl_type is not null then
      case 
        when v_inn_type = 'O' or v_wl_type = 'O' then 
          v_sql_cust := 
            /* ����� �� �������� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.cash_trxn t on t.rf_cust_seq_id = c.cust_seq_id and t.dbt_cdt_cd = ''D'' '||
            'where '||v_sql_where||v_sql_acct||' and t.rf_cust_inn_nb is null'||
            /* ����� �� ��� */
            case 
              when v_inn_type = 'O' then ' and c.tax_id = '''||p_inn1||''''
            end ||
            /* ����� �� ������� ��� */
            case
              when v_wl_type = 'O' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')'
            end;  

        when v_inn_type = 'B' or v_wl_type = 'B' then 
          v_sql_cust := 
            /* ����� �� �������� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.cash_trxn t on t.rf_cust_seq_id = c.cust_seq_id and t.dbt_cdt_cd = ''C'' '||
            'where '||v_sql_where||v_sql_acct||
            /* ����� �� ��� */
            case 
              when v_inn_type = 'B' then ' and c.tax_id = '''||p_inn2||''''
            end ||
            /* ����� �� ������� ��� */
            case
              when v_wl_type = 'B' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')'
            end;
            
        when v_inn_type = 'O|B' or v_wl_type = 'O|B' then 
          v_sql_cust := 
            /* ����� �� �������� �������� */
            'select /*+ FIRST_ROWS(1) */ '||v_field_list||
            ' from business.cust c '||
            'join business.cash_trxn t on t.rf_cust_seq_id = c.cust_seq_id and t.dbt_cdt_cd in (''D'', ''C'') '||
            'where '||v_sql_where||v_sql_acct||' and t.rf_cust_inn_nb is null '||
            /* ����� �� ��� */
            case
              when v_inn_type = 'O|B' then ' and c.tax_id = '''||p_inn1||''''
            end ||
            /* ����� �� ������� ��� */
            case
              when v_wl_type = 'O|B' then ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''') '
            end;

        else v_sql_cust := '';
      end case;
      v_sql_ct := v_sql_ct||chr(13)||'union all'||chr(13)||v_sql_cust;
    end if;
    
  end if;


  /* BACK_OFFICE_TRXN */
  if instr(v_op_cat, 'BOT') > 0 then
    v_sql_inn := null;
    v_sql_acct := null;
    
    v_field_list := q'{
      'BOT' as op_cat_cd, 
      t.bo_trxn_seq_id as trxn_id,
      t.rf_branch_id as branch_id,
      t.exctn_dt as exctn_dt,
      t.trxn_base_am as base_am,
      t.dscr_tx as desc_tx,
      t.src_sys_cd as src_cd,
      case when t.dbt_cdt_cd = 'D' then c.cust_seq_id end as orig_cust_id,
      case when t.dbt_cdt_cd = 'D' then c.full_nm end as orig_full_nm,
      case when t.dbt_cdt_cd = 'D' then c.tax_id end as orig_inn_nb,
      case when t.dbt_cdt_cd = 'D' then a.alt_acct_id end as orig_acct_nb,
      case when t.dbt_cdt_cd = 'D' then c.rf_resident_fl end as orig_resident_fl,
      case when t.dbt_cdt_cd = 'C' then c.cust_seq_id end as benef_cust_id,
      case when t.dbt_cdt_cd = 'C' then c.full_nm end as benef_full_nm,
      case when t.dbt_cdt_cd = 'C' then c.tax_id end as benef_inn_nb,
      case when t.dbt_cdt_cd = 'C' then a.alt_acct_id end as benef_acct_nb,
      case when t.dbt_cdt_cd = 'C' then c.rf_resident_fl end as benef_resident_fl,
      t.rf_debit_cd as debit_acct_nb,
      t.rf_credit_cd as credit_acct_nb,
      null as orig_instn_id,
      null as benef_instn_id,
      case when t.dbt_cdt_cd = 'D' then t.rf_acct_seq_id end orig_acct_id, 
      case when t.dbt_cdt_cd = 'C' then t.rf_acct_seq_id end benef_acct_id,
      t.src_sys_cd as src_sys_cd
    }';
    
    v_hint := '/*+ FIRST_ROWS(1) */ ';
    if v_inn_type is not null and v_acct_type1 is null and v_acct_type2 is null then
      v_sql_bt := 'select '||v_hint||v_field_list||
                  'from business.back_office_trxn t '||
                  'join business.cust c on c.cust_seq_id = t.rf_cust_seq_id '||
                  'left join business.acct a on a.acct_seq_id = t.rf_acct_seq_id '||
                  'where ';
    elsif v_inn_type is null and (v_acct_type1 is not null or v_acct_type2 is not null) then
      v_sql_bt := 'select '||v_hint||v_field_list||
                  'from business.back_office_trxn t '||
                  'join business.acct a on a.acct_seq_id = t.rf_acct_seq_id '||
                  'left join business.cust c on c.cust_seq_id = t.rf_cust_seq_id '||
                  'where ';
    elsif v_inn_type is not null and (v_acct_type1 is not null or v_acct_type2 is not null) then
      v_sql_bt := 'select '||v_hint||v_field_list||
                  'from business.back_office_trxn t '||
                  'join business.acct a on a.acct_seq_id = t.rf_acct_seq_id '||
                  'join business.cust c on c.cust_seq_id = t.rf_cust_seq_id '||
                  'where ';
    else
      v_sql_bt := 'select '||v_hint||v_field_list||
                  'from business.back_office_trxn t '||
                  'left join business.acct a on a.acct_seq_id = t.rf_acct_seq_id '||
                  'left join business.cust c on c.cust_seq_id = t.rf_cust_seq_id '||
                  'where ';
    end if;

    /* ������� - ���� � � ����� �� */ 
    v_sql_where := 'nvl(t.rf_canceled_fl, ''N'') <> ''Y'' and t.exctn_dt >= to_date('''||to_char(p_date_from, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') and t.trxn_base_am >= '||p_ammount_from;
    /* ������� - ���� �� */
    if p_date_to is not null then
      v_sql_where := v_sql_where||' and t.exctn_dt < to_date('''||to_char(p_date_to, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') + 1';
    end if;
    /* ������� - ����� �� */
    if p_ammount_to is not null then
      v_sql_where := v_sql_where||' and t.trxn_base_am <= '||p_ammount_to;
    end if;
    /* ������� �� ���� �����������������, ���� �������� ��������� �� ������ */
    if v_inn_type is null and v_acct_type1 is null and v_acct_type2 is null then
      v_sql_where := v_sql_where||' and t.rf_partition_date >= to_date('''||to_char(p_date_from, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'')';
      if p_date_to is not null then
        v_sql_where := v_sql_where||' and ( t.rf_partition_date < to_date('''||to_char(p_date_to, 'DD.MM.YYYY')||''', ''DD.MM.YYYY'') + 1 '||
                       'or t.rf_partition_date >= to_date(''01.01.9999'', ''DD.MM.YYYY'') )';
      end if;
    end if;
    
    /* ������� - ��� ����������� ��� ���������� */
    case 
      when v_inn_type = 'O' then v_sql_inn := ' and t.dbt_cdt_cd = ''D'' and c.tax_id = '''||p_inn1||'''';
      when v_inn_type = 'B' then v_sql_inn := ' and t.dbt_cdt_cd = ''C'' and c.tax_id = '''||p_inn2||'''';
      when v_inn_type = 'O|B' then v_sql_inn := ' and t.dbt_cdt_cd in (''D'', ''C'') and c.tax_id = '''||p_inn1||'''';
      else null;
    end case;
    
    /* ������� - ������� ��� ����������� ����������� �/��� ���������� */
    case 
      when v_wl_type = 'O'   then 
        v_sql_inn := v_sql_inn||' and t.dbt_cdt_cd = ''D'''||
                    ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn1||''')';
      when v_wl_type = 'B'   then 
        v_sql_inn := v_sql_inn||' and t.dbt_cdt_cd = ''C'''||
                     ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd = '''||p_wl_inn2||''')';
      when v_wl_type = 'O|B' then 
        v_sql_inn := v_sql_inn||' and t.dbt_cdt_cd in (''D'', ''C'')'||
                     ' and c.tax_id in (select inn_nb from mantas.rf_watch_list_org where watch_list_cd in ('''||p_wl_inn1||''', '''||p_wl_inn2||'''))';
      else null;
    end case;

    /* ������� - ����� ����� ����������� �/��� ���������� */
    if v_acct_type1 is not null or v_acct_type2 is not null then
      case
        when v_acct_type1 = 'OAD' then v_sql_acct := '(t.dbt_cdt_cd = ''D'' and a.alt_acct_id = '''||p_acct1||''' or t.rf_debit_cd = '''||p_debit_acct||''')';
        when v_acct_type1 = 'OA'  then v_sql_acct := '(t.dbt_cdt_cd = ''D'' and a.alt_acct_id = '''||p_acct1||''')';
        when v_acct_type1 = 'OD'  then v_sql_acct := '(t.rf_debit_cd = '''||p_debit_acct||''')';
        else null;
      end case;
      if v_acct_type1 is not null and v_acct_type2 is not null then
        v_sql_acct := v_sql_acct||' or ';  
      end if;
      case
        when v_acct_type2 = 'BAC' then v_sql_acct := v_sql_acct||'(t.dbt_cdt_cd = ''C'' and a.alt_acct_id = '''||p_acct2||''' or t.rf_credit_cd = '''||p_credit_acct||''')';
        when v_acct_type2 = 'BA'  then v_sql_acct := v_sql_acct||'(t.dbt_cdt_cd = ''C'' and a.alt_acct_id = '''||p_acct2||''')';
        when v_acct_type2 = 'BC'  then v_sql_acct := v_sql_acct||'(t.rf_credit_cd = '''||p_credit_acct||''')';
        else null;
      end case;
      v_sql_acct := chr(13)||'and ('||v_sql_acct||')';
    end if;
    
    v_sql_bt := v_sql_bt||v_sql_where||v_sql_inn||v_sql_acct;
    
  end if;

  v_sql := v_sql_wt;
  if v_sql is not null and v_sql_ct is not null then
    v_sql := v_sql||chr(13)||'union all'||chr(13)||v_sql_ct;
  else
    v_sql := nvl(v_sql, v_sql_ct);
  end if;
  
  if v_sql is not null and v_sql_bt is not null then
    v_sql := v_sql||chr(13)||'union all'||chr(13)||v_sql_bt;
  else
    v_sql := nvl(v_sql, v_sql_bt);
  end if;

  /* Output SQL */
  if p_print_sql = 'Y' then
    dbms_output.put_line(v_sql);
  else
    /* Execute SQL */
    if v_sql is not null then
      open v_trxn_cursor for v_sql;
      loop
        fetch v_trxn_cursor into 
          v_trxn.op_cat_cd,        --��������� ��������
          v_trxn.trxn_id,          --ID ����������
          v_trxn.branch_id,        --ID ��������� ����� �� � ���
          v_trxn.exctn_dt,         --���� ��������
          v_trxn.base_am,          --����� ��������
          v_trxn.desc_tx,          --���������� �������
          v_trxn.src_cd ,          --������� ��������
          v_trxn.orig_cust_id,     --ID �����������
          v_trxn.orig_full_nm,     --������������ �����������
          v_trxn.orig_inn_nb,      --��� �����������
          v_trxn.orig_acct_nb,     --����� ����� �����������
          v_trxn.orig_resident_fl, --������� ��������� �� ��� �����������
          v_trxn.benef_cust_id,    --ID ����������
          v_trxn.benef_full_nm,    --������������ ����������
          v_trxn.benef_inn_nb,     --��� ����������
          v_trxn.benef_acct_nb,    --����� ����� ����������
          v_trxn.benef_resident_fl,--������� ��������� �� ��� ����������
          v_trxn.debit_acct_nb,    --���� ������
          v_trxn.credit_acct_nb,   --���� �������
          v_trxn.orig_instn_id,    --���/SWIFT ����� �����������
          v_trxn.benef_instn_id,   --���/SWIFT ����� ����������
          v_trxn.orig_acct_id,     --ID ����� ����������� 
          v_trxn.benef_acct_id,    --ID ����� ����������
          v_trxn.src_sys_cd;       --��� ������� ���������
        exit when v_trxn_cursor%NOTFOUND;
        pipe row (v_trxn);
      end loop;
      close v_trxn_cursor;
    end if;  
  end if;
end;
/