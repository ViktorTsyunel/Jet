grant execute on std.pkg_log to mantas;
grant create any job to mantas;

create or replace package mantas.rf_pkg_postscnro IS
--
-- ���������� ����� (������ ��� ���������������) �����.
-- ��������! ��������� ��������� COMMIT
--
PROCEDURE update_new_breaks;
--
-- ���������� ����� (������ ��� ���������������) ������, 
-- � ��� ����� ��������� � �������� ������� ������� ��������� ��������������� ��������, ��������, ������
-- ��������! ��������� ��������� COMMIT
--
PROCEDURE update_new_reviews;
--
-- �������� �������� � �������� �������� ������������� (� ����������� �� � �������� ��������)
--
PROCEDURE mark_trxn_processed;
end rf_pkg_postscnro;
/
create or replace package body mantas.rf_pkg_postscnro IS
--
-- ���������� ����� (������ ��� ���������������) �����.
-- ��������! ��������� ��������� COMMIT
--
PROCEDURE update_new_breaks IS
  prcname            VARCHAR2(64) := ' mantas.rf_pkg_postscnro.update_new_breaks';
BEGIN
  EXECUTE IMMEDIATE 'alter session force parallel dml parallel 32';
  EXECUTE IMMEDIATE 'alter session force parallel query parallel 32';
  --
  -- ��������� � ����� ���������� ������� (-1) ������ ����������� ������������� ��������
  --
  std.pkg_log.info('��������� � kdd_break_mtchs ���������� ������� (-1) ������ ����������� ������������� ��������...', prcname);
  UPDATE /*+ PARALLEL(a 32)*/ mantas.kdd_break_mtchs a  
     SET key_id = -1 
   WHERE case when dataset_id = 113001027 and /*CUST_WRP*/ 
                   key_id < 0 and 
                   key_id <> -1
              then 1
         end = 1;
  std.pkg_log.info('��������� ������������� � kdd_break_mtchs ���������� �������', prcname);

  std.pkg_log.info('��������� � kdd_break_binding ���������� ������� (-1) ������ ����������� ������������� ��������...', prcname);
  UPDATE /*+ PARALLEL(a 32)*/ mantas.kdd_break_binding a 
     SET value_tx = '-1' 
   WHERE case when bindg_nm = 'CUSTOMER' and 
                   value_tx <> '-1' and 
                   to_number(value_tx) < 0
              then 1
         end = 1 and            
         EXISTS(select null 
                  from mantas.kdd_break b 
                 where b.break_id = a.break_id and
                       case when b.prnt_break_id is null and
                                 b.dpstn_cd = 'NEW'
                            then 1
                       end = 1);
  std.pkg_log.info('��������� ������������� � kdd_break_binding ���������� �������', prcname);
  COMMIT;
  --
  -- ������� ���. view, ������� ���������� ��������� ������
  --                     
  std.pkg_log.info('��������� view MANTAS.KDD_MATCH_SCORING...', prcname);
  dbms_mview.refresh('MANTAS.KDD_MATCH_SCORING');
  std.pkg_log.info('��������� ��������� view MANTAS.KDD_MATCH_SCORING', prcname);
END update_new_breaks;
--
-- ���������� ����� (������ ��� ���������������) ������, 
-- � ��� ����� ��������� � �������� ������� ������� ��������� ��������������� ��������, ��������, ������
-- ��������! ��������� ��������� COMMIT
--
PROCEDURE update_new_reviews IS
  var_days           INTEGER;
  var_tmp            INTEGER;
  prcname            VARCHAR2(64) := ' mantas.rf_pkg_postscnro.update_new_reviews';
BEGIN
  --
  -- �������� ��� ����� ������� ������� �������� ��������� � ������� ���������� ����� ����
  --
  EXECUTE IMMEDIATE 'alter session force parallel dml parallel 32';
  EXECUTE IMMEDIATE 'alter session force parallel query parallel 32';
  --
  -- ������� � ������ ������� � ���������������� ������������ �������� 
  --
  std.pkg_log.info('������� � ������ ������� � ���������������� ������������ �������� (rf_arh_object)...', prcname);
  execute immediate 'truncate table mantas.rf_arh_object';
  lock table mantas.rf_arh_object in exclusive mode;

  std.pkg_log.info('��������� ��� ����� ������� ������� �������� (rf_kdd_review_run)...', prcname);
  INSERT /*+ PARALLEL(t 32)*/ INTO mantas.rf_kdd_review_run(review_id, repeat_nb, data_dump_dt)
    (SELECT /*+ PARALLEL(rv 32) ORDERED */  
            rv.review_id, 
            0 as repeat_nb,
            max(to_date(bb.value_tx, 'dd.mm.yyyy hh24:mi:ss')) as data_dump_dt
       FROM mantas.kdd_review rv,
            mantas.kdd_break b,
            mantas.kdd_break_binding bb
      WHERE case when rv.rf_object_tp_cd is null then 1 end = 1 and
            b.prnt_break_id = rv.review_id and
            bb.break_id = b.break_id and
            bb.bindg_nm = 'DATA_DUMP_DT' and
            bb.value_tx is not null
     GROUP BY rv.review_id);
  std.pkg_log.info('��������� ��������� ��� ����� ������� ������� �������� (rf_kdd_review_run)', prcname);

  std.pkg_log.info('��������� ��� ����� ������� ������� ���������� ����� ���� (rf_kdd_review_opok)...', prcname);
  INSERT /*+ PARALLEL(t 32)*/ INTO mantas.rf_kdd_review_opok(record_id, review_id, repeat_nb, trxn_scrty_seq_id, opok_nb, explanation_tx, snapshot_id)
    (SELECT /*+ PARALLEL(rv 32) ORDERED */  
            mantas.rf_kdd_review_opok_seq.NextVal as record_id,         
            rv.review_id, 
            0 as repeat_nb,
            to_number(bb2.value_tx) as trxn_scrty_seq_id,
            to_number(regexp_substr(opok.column_value, '[^\~]+', 1, 1)) as opok_nb,
            regexp_substr(opok.column_value, '[^\~]+', 1, 4) as explanation_tx,
            rls.snapshot_id
       FROM mantas.kdd_review rv,
            mantas.kdd_break b,
            mantas.kdd_break_binding bb,
            table(mantas.rf_pkg_scnro.list_to_tab(bb.value_tx, '|')) opok,
            mantas.kdd_break_binding bb2,
            mantas.rf_opok_rule_snapshot rls
      WHERE case when rv.rf_object_tp_cd is null then 1 end = 1 and
            b.prnt_break_id = rv.review_id and
            bb.break_id = b.break_id and 
            bb.bindg_nm = 'OPOK_LIST' and
            bb.value_tx is not null and
            bb2.break_id(+) = b.break_id and
            bb2.bindg_nm(+) = 'TRXN_SCRTY_SEQ_ID' and
            rls.rule_seq_id = to_number(regexp_substr(opok.column_value, '[^\~]+', 1, 2)) and 
            rls.record_seq_id = to_number(regexp_substr(opok.column_value, '[^\~]+', 1, 3)) and
            rls.actual_fl = 'Y');
  std.pkg_log.info('��������� ��������� ��� ����� ������� ������� ���������� ����� ���� (rf_kdd_review_opok)', prcname);
  --
  -- ������� �� ������� ���������� ���������� ������� ���� �� ���� ��������, � ������� ������� ������� ��������� ��������� � ���
  --
  var_days := nvl(to_number(rf_pkg_adm.get_install_param(9001)), 3); -- ���� ������������� �������� (���. ����)
  --
  -- �������� ��� ������� ������ ������ �������������� ���� � ������� ��������
  -- ������� ��� ������� ������ ������ "���" �������� � ������� ��������, ��������� ���������
  --  
  -- ���� �� ����� �������
  --    
  std.pkg_log.info('��������� ��� ������� ������ ������ �������������� ����, ������� �������� (kdd_break_mtchs) � ������� ������������ �������� (rf_arh_object)...', prcname);
  FOR r IN (SELECT /*+ ORDERED*/  
                   rv.review_id, 
                   max(decode(bb.bindg_nm, 'OP_CAT_CD', bb.value_tx)) as op_cat_cd,
                   max(decode(bb.bindg_nm, 'TRXN_SEQ_ID', to_number(bb.value_tx))) as trxn_seq_id,
                   max(decode(bb.bindg_nm, 'TRXN_SCRTY_SEQ_ID', to_number(bb.value_tx))) as trxn_scrty_seq_id,
                   max(decode(bb.bindg_nm, 'BRANCH_ID', to_number(bb.value_tx))) as branch_id,
                   max(decode(bb.bindg_nm, 'DATA_DUMP_DT', to_date(bb.value_tx, 'dd.mm.yyyy hh24:mi:ss'))) as data_dump_dt,
                   rv_opk.rf_object_tp_cd,
                   rv_opk.opok_nb,
                   rv_opk.add_opoks_tx
              FROM mantas.kdd_review rv,
                   mantas.kdd_break b,
                   mantas.kdd_break_binding bb,
                   (select review_id,
                           first_opok_nb as opok_nb,
                           listagg(nullif(opok_nb, first_opok_nb), ', ') within group (order by priority_nb) as add_opoks_tx,
                           case when max(trxn_scrty_flag) = 0
                                then 'TRXN'
                                when max(trxn_only_flag) = 0
                                then 'TRXN_SCRTY'
                                else 'TRXN_SCRTY_JOINT'  
                           end as rf_object_tp_cd       
                      from (select distinct rvo.review_id,
                                   opk.opok_nb,
                                   nvl(opk.priority_nb, opk.opok_nb) as priority_nb,
                                   first_value(opk.opok_nb) over(partition by rvo.review_id order by nvl(opk.priority_nb, opk.opok_nb)) as first_opok_nb,
                                   max(case when rvo.trxn_scrty_seq_id is not null then 1 else 0 end) over(partition by rvo.review_id, rvo.opok_nb) as trxn_scrty_flag,
                                   max(case when rvo.trxn_scrty_seq_id is null then 1 else 0 end) over(partition by rvo.review_id, rvo.opok_nb) as trxn_only_flag
                              from mantas.kdd_review rv,
                                   mantas.rf_kdd_review_opok rvo,
                                   mantas.rf_opok opk
                             where case when rv.rf_object_tp_cd is null then 1 end = 1 and
                                   rvo.review_id = rv.review_id and
                                   rvo.repeat_nb = 0 and
                                   opk.opok_nb = rvo.opok_nb)      
                    group by review_id, first_opok_nb) rv_opk  
             WHERE case when rv.rf_object_tp_cd is null then 1 end = 1 and
                   b.prnt_break_id = rv.review_id and
                   bb.break_id = b.break_id and
                   bb.bindg_nm in ('OP_CAT_CD', 'TRXN_SEQ_ID', 'TRXN_SCRTY_SEQ_ID', 'DATA_DUMP_DT', 'BRANCH_ID') and
                   bb.value_tx is not null and
                   rv_opk.review_id = rv.review_id
            GROUP BY rv.review_id, rv_opk.rf_object_tp_cd, rv_opk.opok_nb, rv_opk.add_opoks_tx) LOOP

    -- �������������� �������� �������
    UPDATE /*+ NO_PARALLEL*/ mantas.kdd_review
       SET rf_object_tp_cd = r.rf_object_tp_cd, 
           rf_op_cat_cd = r.op_cat_cd,
           rf_trxn_seq_id = r.trxn_seq_id,
           rf_trxn_scrty_seq_id = r.trxn_scrty_seq_id,
           rf_repeat_nb = 0,
           rf_opok_nb = r.opok_nb,
           rf_add_opoks_tx = r.add_opoks_tx,
           rf_branch_id = r.branch_id,
           rf_data_dump_dt = r.data_dump_dt,
           due_dt = case when r.opok_nb = 6001 then to_date(null)
                         else case r.op_cat_cd
                                when 'WT'  then (select mantas.rf_add_work_days(trxn_exctn_dt, var_days)  from business.wire_trxn where fo_trxn_seq_id = r.trxn_seq_id)
                                when 'CT'  then (select mantas.rf_add_work_days(trxn_exctn_dt, var_days)  from business.cash_trxn where fo_trxn_seq_id = r.trxn_seq_id)
                                when 'BOT' then (select mantas.rf_add_work_days(exctn_dt, var_days)  from business.back_office_trxn where bo_trxn_seq_id = r.trxn_seq_id)
                              end                            
                    end          
     WHERE review_id = r.review_id;
    
    -- �������� ������� � ��������� 
    MERGE /*+ NO_PARALLEL*/ INTO mantas.kdd_break_mtchs t USING 
     (SELECT br.break_id, 
             case r.op_cat_cd
               when 'WT'  then 113001098       /*WIRE_TRXN_WRP*/
               when 'CT'  then 113001018       /*CASH_TRXN_WRP*/
               when 'BOT' then 113001017       /*BACK_OFFICE_TRXN_WRP*/
             end as dataset_id,       
             r.trxn_seq_id as key_id,
             'MTH' as match_cd
        FROM mantas.kdd_break br
       WHERE br.prnt_break_id = r.review_id) v
    ON (t.break_id = v.break_id and t.dataset_id = v.dataset_id and t.key_id = v.key_id)          
     WHEN NOT MATCHED THEN
       INSERT(break_id, dataset_id, key_id, match_cd)
       VALUES(v.break_id, v.dataset_id, v.key_id, v.match_cd)
       WHERE v.key_id is not null;
    
    -- �������� � ������ ������������ ��������   
    MERGE /*+ NO_PARALLEL*/ INTO mantas.rf_arh_object t USING
     (SELECT r.op_cat_cd    as entity_cd, 
             r.data_dump_dt as arh_dump_dt, 
             r.trxn_seq_id  as entity_id
        FROM dual
       WHERE r.op_cat_cd is not null and r.data_dump_dt is not null and r.trxn_seq_id is not null) v  
    ON (t.entity_cd = v.entity_cd and t.arh_dump_dt = v.arh_dump_dt and t.entity_id = v.entity_id)          
     WHEN NOT MATCHED THEN
       INSERT(entity_cd, arh_dump_dt, entity_id)
       VALUES(v.entity_cd, v.arh_dump_dt, v.entity_id);
  END LOOP;      
  std.pkg_log.info('��������� ��������� ��� ������� ������ ������ �������������� ����, ������� �������� (kdd_break_mtchs) � ������� ������������ �������� (rf_arh_object)', prcname);

  COMMIT;
  --
  -- ���������� ������� ��������� ��������, ��������, ������, ������ �����, ��������� � ������ ��������
  --
  std.pkg_log.info('���������� ������� ��������� ��������, ��������, ������, ������ �����, ��������� � ������ ��������...', prcname);
  var_tmp := rf_pkg_arh.archive_all;
  std.pkg_log.info('��������� ������������ ������� ��������� ��������, ��������, ������, ������ �����, ��������� � ������ ��������', prcname);

  COMMIT;
END update_new_reviews;
--
-- �������� �������� � �������� �������� ������������� (� ����������� �� � �������� ��������)
--
PROCEDURE mark_trxn_processed IS

  var_chunk_cursor   CLOB;
  var_sql_common     CLOB;   
  var_parallel_level INTEGER := 64;
  var_count          INTEGER;
  
  prcname            VARCHAR2(64) := ' mantas.rf_pkg_postscnro.mark_trxn_processed';
BEGIN  
  var_chunk_cursor := 'SELECT to_number(column_value) as start_id, to_number(column_value) as end_id FROM TABLE(mantas.rf_subpart_keys(''BUSINESS'', ''#TABLE#'', ''P_ACTIVE'')) ORDER BY 1';
  var_sql_common   :=    
                         'SET rf_active_partition_flag = 0, '||
                             'rf_opok_status = 2 '||
                       'WHERE rf_partition_date = to_date(''01.01.9999'', ''dd.mm.yyyy'') and rf_subpartition_key between :start_id and :end_id';   
  --
  -- ����������� ��������
  -- ������� ������, ���� ��� �������� �� ����������� ������� 
  --
  BEGIN
    dbms_parallel_execute.drop_task('aml_mark_wire_processed');
  EXCEPTION
    WHEN OTHERS THEN
      null;
  END;

  BEGIN
    -- ��������� ������������ UPDAT'� (������ ��������� ���� �����������)
    std.pkg_log.info('�������� ����������� �������� ������������� ���������� ��������� ����...', prcname);
    dbms_parallel_execute.create_task('aml_mark_wire_processed');
    dbms_parallel_execute.create_chunks_by_sql('aml_mark_wire_processed', replace(var_chunk_cursor, '#TABLE#', 'WIRE_TRXN'), false);  
    dbms_parallel_execute.run_task('aml_mark_wire_processed', 'UPDATE business.wire_trxn '||var_sql_common, DBMS_SQL.NATIVE, parallel_level => var_parallel_level);  
    -- ���������, ��� ��� ����������� ������� ���������, ���� ��� - ���������� ��������� � ���
    var_count := std.pkg_log.log_parallel_task_errors('MANTAS', 'aml_mark_wire_processed', 'ERROR', prcname);
    -- ����������� ��������
    dbms_parallel_execute.drop_task('aml_mark_wire_processed');
    std.pkg_log.info('��������� �������� ����������� �������� �������������', prcname); 
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.error('������ ��� ��������� ����������� �������� �������������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||prcname);
  END;
  --
  -- �������� ��������
  -- ������� ������, ���� ��� �������� �� ����������� ������� 
  --    
  BEGIN
    dbms_parallel_execute.drop_task('aml_mark_cash_processed');
  EXCEPTION
    WHEN OTHERS THEN
      null;
  END;  

  BEGIN
    -- ��������� ������������ UPDAT'� (������ ��������� ���� �����������)
    std.pkg_log.info('�������� �������� �������� ������������� ���������� ��������� ����...', prcname);
    dbms_parallel_execute.create_task('aml_mark_cash_processed');
    dbms_parallel_execute.create_chunks_by_sql('aml_mark_cash_processed', replace(var_chunk_cursor, '#TABLE#', 'CASH_TRXN'), false);  
    dbms_parallel_execute.run_task('aml_mark_cash_processed', 'UPDATE business.cash_trxn '||var_sql_common, DBMS_SQL.NATIVE, parallel_level => var_parallel_level);  
    -- ���������, ��� ��� ����������� ������� ���������, ���� ��� - ���������� ��������� � ���
    var_count := std.pkg_log.log_parallel_task_errors('MANTAS', 'aml_mark_cash_processed', 'ERROR', prcname);
    -- ����������� ��������
    dbms_parallel_execute.drop_task('aml_mark_cash_processed');
    std.pkg_log.info('��������� �������� �������� �������� �������������', prcname);  
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.error('������ ��� ��������� �������� �������� �������������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||prcname);
  END;
  --
  -- ��������� ��������
  -- ������� ������, ���� ��� �������� �� ����������� ������� 
  --  
  BEGIN
    dbms_parallel_execute.drop_task('aml_mark_back_processed');
  EXCEPTION
    WHEN OTHERS THEN
      null;
  END;      

  BEGIN
    -- ��������� ������������ UPDAT'� (������ ��������� ���� �����������)
    std.pkg_log.info('�������� ���������� �������� ������������� ���������� ��������� ����...', prcname);
    dbms_parallel_execute.create_task('aml_mark_back_processed');
    dbms_parallel_execute.create_chunks_by_sql('aml_mark_back_processed', replace(var_chunk_cursor, '#TABLE#', 'BACK_OFFICE_TRXN'), false);  
    dbms_parallel_execute.run_task('aml_mark_back_processed', 'UPDATE business.back_office_trxn '||var_sql_common, DBMS_SQL.NATIVE, parallel_level => var_parallel_level);  
    -- ���������, ��� ��� ����������� ������� ���������, ���� ��� - ���������� ��������� � ���
    var_count := std.pkg_log.log_parallel_task_errors('MANTAS', 'aml_mark_back_processed', 'ERROR', prcname);
    -- ����������� ��������
    dbms_parallel_execute.drop_task('aml_mark_back_processed');
    std.pkg_log.info('��������� �������� ���������� �������� �������������', prcname);  
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.error('������ ��� ��������� ���������� �������� �������������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||prcname);
  END;  
END mark_trxn_processed;
end rf_pkg_postscnro;
/

GRANT EXECUTE on mantas.rf_pkg_postscnro to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_postscnro to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_postscnro to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_postscnro to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_postscnro to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_postscnro to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_postscnro to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_postscnro to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_postscnro to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_postscnro to KYC;
GRANT EXECUTE on mantas.rf_pkg_postscnro to STD;
GRANT EXECUTE on mantas.rf_pkg_postscnro to KDD_ALG;
GRANT EXECUTE on mantas.rf_pkg_postscnro to DATA_LOADER;
grant execute on mantas.rf_pkg_scnro to reks000ibs with grant option;


