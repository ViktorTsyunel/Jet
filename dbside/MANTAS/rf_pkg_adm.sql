grant select on OFSCONF.CSSMS_USR_PROFILE to mantas;
grant select on OFSCONF.CSSMS_USR_GROUP_MAP to mantas;
grant select on OFSCONF.CSSMS_ROLE_FUNCTION_MAP to mantas;

create or replace package mantas.rf_pkg_adm IS
--
-- �������� �������� ��������� �� KDD_INSTALL_PARAM
-- ���� ������������ � ������� dd.mm.yyyy, CLOB ������������ ���������� �� 2000 ��������
--
FUNCTION get_install_param
(
  par_param_id         kdd_install_param.param_id%TYPE, -- id (�����) ���������
  par_attr_num         INTEGER  DEFAULT 0               -- ����� ��������, ���� 0, �� �������� ������ ���������                        
) 
RETURN VARCHAR2 RESULT_CACHE;
--
-- ����������: �������� �� ��������� ������������ ������� ����������� (����� ��������� ������ �� ������ �������������)
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION is_supervisor
(
  par_owner_id         kdd_review_owner.owner_id%TYPE 
) 
RETURN INTEGER RESULT_CACHE;
--
-- ����������: �������� �� ���������� ������������ ��� ������ (�� �����: �� ������ ��� �� ������) ��� ������ ����������� �� ����
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION is_all_alerts
(
  par_owner_id         kdd_review_owner.owner_id%TYPE 
) 
RETURN INTEGER RESULT_CACHE;
--
-- ����������: �������� �� ���������� ������������ ������ � ��������� ������� "�� ������"
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION allow_alert_editing
(
  par_owner_id         kdd_review_owner.owner_id%TYPE,
  par_status_cd        kdd_review.status_cd%TYPE 
) 
RETURN INTEGER RESULT_CACHE;
--
-- ���������� ����� ������� ���������� ������������ � ���������� ���� �������
-- ������������ ��������: 0 - ��� �������, 1 - ������ �� ������, 2 - ������ �� ������ (�. �. � �������� ������)
--
FUNCTION get_imptp_access
(
  par_owner_id         kdd_review_owner.owner_id%TYPE,
  par_imp_type_cd      rf_imp_type.imp_type_cd%TYPE 
) 
RETURN INTEGER RESULT_CACHE;
--
-- ����������: �������� �� ��������� ������������ ��������� �����
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION has_role
(
  par_owner_id         kdd_review_owner.owner_id%TYPE, -- ����� ������������
  par_role_code        VARCHAR2                        -- ��� ����
) 
RETURN INTEGER RESULT_CACHE;
--
-- ����������: �������� �� ��������� ������������ ��������� ��������
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION has_function
(
  par_owner_id         kdd_review_owner.owner_id%TYPE, -- ����� ������������
  par_function_code    VARCHAR2                        -- ��� �������
) 
RETURN INTEGER RESULT_CACHE;
--
-- ����������: �������� �� ��������� ������������ �������� � ���������� ���������� �������� ��� ��������
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION has_activity
(
  par_owner_id         kdd_review_owner.owner_id%TYPE, -- ����� ������������
  par_actvy_type_cd    VARCHAR2                        -- ��� ��������
) 
RETURN INTEGER RESULT_CACHE;
--
-- ����������: ������� �� ��������� ������� ������
-- ������������ ��������: 1 - �������, 0 - ��������� (�������������)
--
FUNCTION is_enabled
(
  par_owner_id         kdd_review_owner.owner_id%TYPE 
) 
RETURN INTEGER RESULT_CACHE;
--
-- �������� ������ email-������� �������� (�����������������) �������������, ���������� ��������� ��������
--
FUNCTION get_emails_by_func
(
  par_function_code    VARCHAR2                        -- ��� �������
) 
RETURN mantas.rf_tab_varchar; -- ������ email-�������
--
-- ���������������� ������������ �� OFSCONF.CSSMS_USR_PROFILE � KDD_REVIEW_OWNER, ��������� ������������
--
PROCEDURE sync_user
(
  par_v_usr_id          ofsconf.cssms_usr_profile.v_usr_id%TYPE,
  par_v_usr_name        ofsconf.cssms_usr_profile.v_usr_name%TYPE
);  
--
-- ���������������� ���� ������������� OFSCONF.CSSMS_USR_PROFILE � KDD_REVIEW_OWNER, ��������� �������������
-- ��������! ��������� �� ��������� COMMIT!
--
PROCEDURE sync_all_users;  
end rf_pkg_adm;
/

create or replace package body mantas.rf_pkg_adm IS
--
-- �������� �������� ��������� �� KDD_INSTALL_PARAM
-- ���� ������������ � ������� dd.mm.yyyy, CLOB ������������ ���������� �� 2000 ��������
--
FUNCTION get_install_param
(
  par_param_id         kdd_install_param.param_id%TYPE, -- id (�����) ���������
  par_attr_num         INTEGER  DEFAULT 0               -- ����� ��������, ���� 0, �� �������� ������ ���������                        
) 
RETURN VARCHAR2 RESULT_CACHE IS  
  var_result   VARCHAR2(2000 CHAR);
BEGIN
  SELECT case par_attr_num
           when 0 then max(param_value_tx)
           when 1 then max(attr_1_value_tx)
           when 2 then max(attr_2_value_tx)
           when 3 then max(attr_3_value_tx)
           when 4 then max(attr_4_value_tx)
           when 5 then max(attr_5_value_tx)
           when 6 then max(attr_6_value_tx)
           when 7 then max(attr_7_value_tx)
           when 8 then max(attr_8_value_tx)
           when 9 then max(attr_9_value_tx)
           when 10 then max(attr_10_value_tx)
           when 11 then max(to_char(substr(attr_11_value_tx, 1, 2000)))
           when 12 then max(to_char(attr_12_value_tx, 'dd.mm.yyyy'))
           when 13 then max(attr_13_value_tx)
           when 14 then max(attr_14_value_tx)
           when 15 then max(attr_15_value_tx)
         end    
    INTO var_result
    FROM kdd_install_param
   WHERE param_id = par_param_id;

  return var_result;  
END get_install_param;
--
-- ����������: �������� �� ��������� ������������ ������� ����������� (����� ��������� ������ �� ������ �������������)
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION is_supervisor
(
  par_owner_id         kdd_review_owner.owner_id%TYPE 
) 
RETURN INTEGER RESULT_CACHE IS
BEGIN
  return has_function(par_owner_id, 'RFALASSIGN'); 
END is_supervisor;  
--
-- ����������: �������� �� ���������� ������������ ��� ������ (�� �����: �� ������ ��� �� ������) ��� ������ ����������� �� ����
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION is_all_alerts
(
  par_owner_id         kdd_review_owner.owner_id%TYPE 
) 
RETURN INTEGER RESULT_CACHE IS
BEGIN
  --
  -- ���� � ������������ � �������� ���� ����� �� �������� ��� �������������� �������,
  -- ��� ���� ���� ����������� ����� �� ������ �� ���� ������� - ������� 1
  --
  if has_function(par_owner_id, 'RFALERTS_W') = 1 or has_function(par_owner_id, 'RFALERTS_R') = 1 Then
    return has_function(par_owner_id, 'RFALERTALL');
  else 
    return 0;  
  end if;        
END is_all_alerts;  
--
-- ����������: �������� �� ���������� ������������ ������ � ��������� ������� "�� ������"
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION allow_alert_editing
(
  par_owner_id         kdd_review_owner.owner_id%TYPE,
  par_status_cd        kdd_review.status_cd%TYPE 
) 
RETURN INTEGER RESULT_CACHE IS
BEGIN
  --
  -- ���� � ������������ � �������� ���� ����� �� �������������� �������,
  -- ��� ���� ��� ������� � ������� "������� ��������" ���� ����������� ����� 
  -- �� �������������� � ���� ������� - ������� 1
  --
  if has_function(par_owner_id, 'RFALERTS_W') = 1 Then
    if par_status_cd like 'RF%READY%' Then 
      return has_function(par_owner_id, 'RFOESREADY');
    else
      return 1;
    end if;
  else
    return 0;
  end if;        
END allow_alert_editing;
--
-- ���������� ����� ������� ���������� ������������ � ���������� ���� �������
-- ������������ ��������: 0 - ��� �������, 1 - ������ �� ������, 2 - ������ �� ������ (�. �. � �������� ������)
--
FUNCTION get_imptp_access
(
  par_owner_id         kdd_review_owner.owner_id%TYPE,
  par_imp_type_cd      rf_imp_type.imp_type_cd%TYPE 
) 
RETURN INTEGER RESULT_CACHE IS
  var_result   INTEGER;
BEGIN
  SELECT max(case -- ���� ���� ����� �� �������������� �� ������ �����������
                  when mantas.rf_pkg_adm.has_function(par_owner_id, 'RFIMPTP_W') = 1
                  then case -- ���� ���� ����� �� �������������� ��� ���������� ���� ������� ���
                            -- ��� ������� ��� ����������� ������� - ���� ����� �� ��������������  
                            when access_nb is null or 
                                 mantas.rf_pkg_adm.has_function(par_owner_id, 'RFIMP'||to_char(access_nb)||'W') = 1
                            then 2 
                            -- ���� ���� ����� �� ������ ��� ���������� ���� ������� - ���� ����� �� ������  
                            when mantas.rf_pkg_adm.has_function(par_owner_id, 'RFIMP'||to_char(access_nb)||'R') = 1
                            then 1 
                            -- ����� - ��� ����
                            else 0  
                       end       
                  -- ���� ���� ����� �� ������ �� ������ �����������
                  when mantas.rf_pkg_adm.has_function(par_owner_id, 'RFIMPTP_R') = 1
                  then case -- ���� ���� ����� �� ������ ��� ������ ��� ���������� ���� ������� ���
                            -- ��� ������� ��� ����������� ������� - ���� ����� �� ������  
                            when access_nb is null or 
                                 mantas.rf_pkg_adm.has_function(par_owner_id, 'RFIMP'||to_char(access_nb)||'R') = 1 or 
                                 mantas.rf_pkg_adm.has_function(par_owner_id, 'RFIMP'||to_char(access_nb)||'W') = 1
                            then 1 
                            -- ����� - ��� ����
                            else 0  
                       end       
                  -- ����� - ��� ����
                  else 0
             end)
    INTO var_result         
    FROM mantas.rf_imp_type
   WHERE imp_type_cd = par_imp_type_cd; 
   
  return var_result; 
END get_imptp_access;
--
-- ����������: �������� �� ��������� ������������ ��������� �����
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION has_role
(
  par_owner_id         kdd_review_owner.owner_id%TYPE, -- ����� ������������
  par_role_code        VARCHAR2                        -- ��� ����
) 
RETURN INTEGER RESULT_CACHE IS
  var_result   INTEGER;
BEGIN
  SELECT count(*)
    INTO var_result
    FROM ofsconf.cssms_usr_group_map ug
         join ofsconf.cssms_group_role_map gr on gr.v_group_code = ug.v_group_code
   WHERE ug.v_usr_id = par_owner_id and
         gr.v_role_code = par_role_code and
         rownum <= 1;
 
  return var_result;  
END has_role;  
--
-- ����������: �������� �� ��������� ������������ ��������� ��������
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION has_function
(
  par_owner_id         kdd_review_owner.owner_id%TYPE, -- ����� ������������
  par_function_code    VARCHAR2                        -- ��� �������
) 
RETURN INTEGER RESULT_CACHE IS
  var_result   INTEGER;
BEGIN
  SELECT count(*)
    INTO var_result
    FROM ofsconf.cssms_usr_group_map ug
         join ofsconf.cssms_group_role_map gr on gr.v_group_code = ug.v_group_code
         join ofsconf.cssms_role_function_map rf on rf.v_role_code = gr.v_role_code 
   WHERE ug.v_usr_id = par_owner_id and
         rf.v_function_code = par_function_code and
         rownum <= 1;
 
  return var_result;  
END has_function;  
--
-- ����������: �������� �� ��������� ������������ �������� � ���������� ���������� �������� ��� ��������
-- ������������ ��������: 1 - ��������, 0 - �� ��������
--
FUNCTION has_activity
(
  par_owner_id         kdd_review_owner.owner_id%TYPE, -- ����� ������������
  par_actvy_type_cd    VARCHAR2                        -- ��� ��������
) 
RETURN INTEGER RESULT_CACHE IS
  var_result   INTEGER;
BEGIN
  SELECT count(*)
    INTO var_result
    FROM ofsconf.cssms_usr_group_map ug
         join ofsconf.cssms_group_role_map gr on gr.v_group_code = ug.v_group_code
         join mantas.kdd_role_activity_type ra on ra.role_cd = gr.v_role_code
   WHERE ug.v_usr_id = par_owner_id and
         ra.actvy_type_cd = par_actvy_type_cd and
         rownum <= 1;
 
  return var_result;  
END has_activity;    
--
-- ����������: ������� �� ��������� ������� ������
-- ������������ ��������: 1 - �������, 0 - ��������� (�������������)
--
FUNCTION is_enabled
(
  par_owner_id         kdd_review_owner.owner_id%TYPE 
) 
RETURN INTEGER RESULT_CACHE IS
  var_result   INTEGER;
BEGIN
  SELECT max(case when f_usr_enabled = 'Y'
                  then 1
                  else 0
             end)         
    INTO var_result
    FROM ofsconf.cssms_usr_profile
   WHERE v_usr_id = par_owner_id;
 
  return var_result;  
END is_enabled;    
--
-- �������� ������ email-������� �������� (�����������������) �������������, ���������� ��������� ��������
--
FUNCTION get_emails_by_func
(
  par_function_code    VARCHAR2                        -- ��� �������
) 
RETURN mantas.rf_tab_varchar IS -- ������ email-�������
  var_result  mantas.rf_tab_varchar := mantas.rf_tab_varchar();
BEGIN
  SELECT distinct trim(u.v_email)
    BULK COLLECT INTO var_result
    FROM ofsconf.cssms_usr_profile u
         join ofsconf.cssms_usr_group_map ug on ug.v_usr_id = u.v_usr_id
         join ofsconf.cssms_group_role_map gr on gr.v_group_code = ug.v_group_code
         join ofsconf.cssms_role_function_map rf on rf.v_role_code = gr.v_role_code 
   WHERE rf.v_function_code = par_function_code and
         trim(u.v_email) is not null and
         u.f_usr_enabled = 'Y' and
         u.f_usr_delete = 'N' and
         u.f_authorize_stat = 'A';
   
  return var_result;   
END get_emails_by_func;
--
-- ���������������� ������������ �� OFSCONF.CSSMS_USR_PROFILE � KDD_REVIEW_OWNER, ��������� ������������
--
PROCEDURE sync_user
(
  par_v_usr_id          ofsconf.cssms_usr_profile.v_usr_id%TYPE,
  par_v_usr_name        ofsconf.cssms_usr_profile.v_usr_name%TYPE
) IS

  var_owner_seq_id      kdd_review_owner.owner_seq_id%TYPE;
  var_owner_type_cd     kdd_review_owner.owner_type_cd%TYPE;
  var_user_prefs_id     kdd_user_prefs.user_prefs_id%TYPE;
  
  var_err_code          NUMBER;
  var_err_msg           VARCHAR2(255);
BEGIN
  --
  -- �������� ���������� ���������
  --
  if par_v_usr_id is null Then
    raise_application_error(-20001, '�������� �������� ��������� (mantas.rf_pkg_adm.sync_user)');    
  end if;  
  --
  -- ���� ��������� ������������ ��� ���������� - ��������� ��� ID � ���
  --
  SELECT max(owner_seq_id), max(owner_type_cd)
    INTO var_owner_seq_id, var_owner_type_cd
    FROM kdd_review_owner
   WHERE owner_id = par_v_usr_id;
  --
  -- �������������� ������ ������������� (owner_type_cd = USER)
  -- 
  if var_owner_seq_id is not null and nvl(var_owner_type_cd, '?') <> 'USER' Then
    raise_application_error(-20001, '���������������� ����� ������ ������������� (mantas.kdd_review_owner.owner_type_cd = USER) (mantas.rf_pkg_adm.sync_user)');    
  end if;  
  --
  -- ���� ������������ ��� ��� - ������������ ���, ����� - ��������� ��������
  --
  if var_owner_seq_id is null Then
    mantas.p_sequence_nextval('OWNER_SEQ_ID_SEQ', var_owner_seq_id, var_err_code, var_err_msg); 
    
    INSERT INTO KDD_REVIEW_OWNER(owner_seq_id, owner_id, owner_type_cd, rptg_group_cd, actv_fl, bus_dmn_st, email_addr_tx, 
                                 owner_dsply_nm, own_alert_fl, last_failed_logon_ts, curr_valid_logon_ts, prev_valid_logon_ts, 
                                 own_case_fl, pwd_chg_dt) 
           VALUES(var_owner_seq_id, par_v_usr_id, 'USER', 'OFM', 'Y', 'a', null, 
                  nvl(par_v_usr_name, par_v_usr_id), 'Y', null, null, null, 
                  'Y', null);     
  else
    UPDATE KDD_REVIEW_OWNER
       SET rptg_group_cd = 'OFM',
           actv_fl = 'Y',
           bus_dmn_st = 'a',
           owner_dsply_nm = nvl(par_v_usr_name, par_v_usr_id),
           own_alert_fl = 'Y',
           own_case_fl = 'Y'
     WHERE owner_seq_id = var_owner_seq_id;         
  end if;
  --
  -- �������� � �����������
  --
  INSERT INTO KDD_REVIEW_OWNER_ORG(owner_seq_id, org_cd) 
    (SELECT var_owner_seq_id, org_cd
       FROM mantas.kdd_org
      WHERE org_cd = 'OFM' 
     MINUS 
     SELECT owner_seq_id, org_cd
       FROM kdd_review_owner_org
      WHERE owner_seq_id = var_owner_seq_id);
  --
  -- �������� � ����������
  --
  INSERT INTO KDD_REVIEW_OWNER_JRSDCN(owner_seq_id, jrsdcn_cd) 
    (SELECT var_owner_seq_id, jrsdcn_cd
       FROM kdd_jrsdcn
      WHERE jrsdcn_cd = 'RF' 
     MINUS 
     SELECT owner_seq_id, jrsdcn_cd
       FROM kdd_review_owner_jrsdcn
      WHERE owner_seq_id = var_owner_seq_id);
  --
  -- �������� � ������� ���������
  --
  INSERT INTO KDD_SCNRO_GRP_ACCESS(owner_seq_id, scnro_grp_id, scnro_grp_nm) 
    (SELECT var_owner_seq_id, scnro_grp_id, scnro_grp_nm
       FROM kdd_scnro_grp
      WHERE scnro_grp_nm = 'ML' 
     MINUS 
     SELECT var_owner_seq_id, g.scnro_grp_id, g.scnro_grp_nm
       FROM kdd_scnro_grp_access a
            join kdd_scnro_grp g on g.scnro_grp_id = a.scnro_grp_id
      WHERE a.owner_seq_id = var_owner_seq_id and 
            g.scnro_grp_nm = 'ML');
  --
  -- �������� � ����� ������
  --
  INSERT INTO KDD_REVIEW_OWNER_CASE_TYPE(owner_seq_id, case_type_cd) 
    (SELECT var_owner_seq_id, case_type_subtype_cd
       FROM kdd_case_type_subtype
      WHERE case_type_cd in ('AML', 'FR') 
     MINUS 
     SELECT owner_seq_id, case_type_cd
       FROM kdd_review_owner_case_type
      WHERE owner_seq_id = var_owner_seq_id);
  --
  -- ��������� ������������
  --
  FOR r IN (SELECT diff.pref_id, diff.value_tx, exist.user_prefs_id, dmn.dmn_cd
              FROM ((SELECT 'AM_DISPL_CONFIG' as pref_id, 'STD' as value_tx FROM dual UNION ALL  
                     SELECT 'AM_SRCH', 'AM_SRCH_1_VW' FROM dual UNION ALL
                     SELECT 'AM_VIEW', queue_display_nm FROM kdd_queue_master WHERE queue_seq_id = 9001) /*��������� ���������*/     
                    MINUS 
                    SELECT pref_id, value_tx
                      FROM kdd_user_prefs
                     WHERE owner_seq_id = var_owner_seq_id and
                           pref_id in ('AM_DISPL_CONFIG', 'AM_SRCH', 'AM_VIEW')) diff
                   join kdd_dmn_cd dmn on dmn.dmn_cd = 'MTS'       
                   left join kdd_user_prefs exist on owner_seq_id = var_owner_seq_id and exist.pref_id = diff.pref_id) LOOP
    --
    -- ���� ��������� ��� ��� - ������������ ���, ���� ���� - ��������� ��������
    --
    if r.user_prefs_id is null Then
      mantas.p_sequence_nextval('USER_PREFS_ID_SEQ', var_user_prefs_id, var_err_code, var_err_msg); 
      
      INSERT INTO KDD_USER_PREFS(user_prefs_id, owner_seq_id, pref_id, dmn_cd, value_tx)
             VALUES (var_user_prefs_id, var_owner_seq_id, r.pref_id, r.dmn_cd, r.value_tx);
    else
      UPDATE KDD_USER_PREFS
         SET value_tx = r.value_tx         
       WHERE user_prefs_id = r.user_prefs_id;    
    end if;
  END LOOP;
END sync_user;    
--
-- ���������������� ���� ������������� OFSCONF.CSSMS_USR_PROFILE � KDD_REVIEW_OWNER, ��������� �������������
-- ��������! ��������� �� ��������� COMMIT!
--
PROCEDURE sync_all_users IS
BEGIN
  FOR r IN (SELECT v_usr_id, v_usr_name
              FROM ofsconf.cssms_usr_profile
             WHERE v_usr_id not in ('SYSADMN', 'SYSAUTH', 'GUEST')) LOOP
    sync_user(r.v_usr_id, r.v_usr_name);         
  END LOOP;
END sync_all_users;  
end rf_pkg_adm;
/

GRANT EXECUTE on mantas.rf_pkg_adm to OFSCONF;
GRANT EXECUTE on mantas.rf_pkg_adm to KDD_ALGORITHM;
GRANT EXECUTE on mantas.rf_pkg_adm to KDD_MINER;
GRANT EXECUTE on mantas.rf_pkg_adm to MANTAS_LOADER;
GRANT EXECUTE on mantas.rf_pkg_adm to MANTAS_READER;
GRANT EXECUTE on mantas.rf_pkg_adm to RF_RSCHEMA_ROLE;
GRANT EXECUTE on mantas.rf_pkg_adm to BUSINESS;
GRANT EXECUTE on mantas.rf_pkg_adm to CMREVMAN;
GRANT EXECUTE on mantas.rf_pkg_adm to KDD_MNR with grant option;
GRANT EXECUTE on mantas.rf_pkg_adm to KDD_REPORT;
GRANT EXECUTE on mantas.rf_pkg_adm to KYC;
GRANT EXECUTE on mantas.rf_pkg_adm to STD;
GRANT EXECUTE on mantas.rf_pkg_adm to KDD_ALG;
