grant read on directory AML_IMP_CBRF_BNK_DIR to mantas;
grant write on directory AML_IMP_CBRF_BNK_DIR to mantas;

grant read on directory AML_SH_DIR to mantas;
grant write on directory AML_SH_DIR to mantas;
grant execute on directory AML_SH_DIR to mantas;

/* Grant on tables */
grant select on business.rf_cbrf_bnkdel_dbf to mantas;
grant select on business.rf_cbrf_bnkseek_dbf to mantas;
grant select on business.rf_cbrf_bnkdel_dbf_2 to mantas;
grant select on business.rf_cbrf_bnkseek_dbf_2 to mantas;

grant select, insert, update, delete on business.rf_cbrf_bnkdel to mantas;
grant select, insert, update, delete on business.rf_cbrf_bnkseek to mantas;

/* Drop obsolete package business.rf_pkg_import. */
begin
  execute immediate 'drop package business.rf_pkg_import';
exception when others then null; end;  
/

create or replace package mantas.rf_pkg_import as
  
  /* ����� ��������� dbf ����. 
   * ����������� ��������� ��� ���������� ����� ������� �� ������� MANTAS.RF_IMP_TYPE �� ���� ������� (p_type).
   *
   * ��������� ������
   *   p_blob - dbf ����
   *   p_type - ��� �������
   *   p_file_name - ��� �����
   *   p_date_act - ���� ������������
   *   p_note - �����������
   *   p_user - ��� ������������
   *   p_status - ��������� �������� O/W/E
   *   p_err_msg - ���������
   *   p_load_qty - ���������� ����������� �������
   */
  procedure save_dbf_to_file (
    p_type      in mantas.rf_imp_history.imp_type_cd%type,
    p_date_act  in mantas.rf_imp_history.actual_dt%type,
    p_file_name in mantas.rf_imp_history.file_nm%type,
    p_note      in mantas.rf_imp_history.note_tx%type,
    p_user      in mantas.rf_imp_history.loaded_by%type,
    p_file      in blob,
    p_status    out mantas.rf_imp_history.load_status%type,
    p_err_msg   out mantas.rf_imp_history.err_msg_tx%type,
    p_load_qty  out mantas.rf_imp_history.load_qty%type );


  /* ����� ��������� ����� �� ������� ������� business.rf_cbrf_bnkseek_dbf � ������� business.rf_cbrf_bnkseek 
   * �������� ���������� � ������ �� ������������.
   *
   * ��������� ������
   *   p_file_type - ��� dbf �����: 
   *     - CB: dbf - ����, ���������� �� �� (www.cbr.ru)
   *     - SB: dbf - ����, ���������� �� ���������
   */
  procedure update_cbrf_bnkseek (
    p_file_type in varchar2 default 'SB' );
  

  /* ����� ��������� ����� �� ������� ������� business.rf_cbrf_bnkdel � ������� business.rf_cbrf_bnkdel 
   * �������� ���������� � ������ �� ������������.
   *
   * ��������� ������
   *   p_file_type - ��� dbf �����: 
   *     - CB: dbf - ����, ���������� �� �� (www.cbr.ru)
   *     - SB: dbf - ����, ���������� �� ���������
   */

  procedure update_cbrf_bnkdel (
    p_file_type in varchar2 default 'SB' );
    
  -- ������ ����� ������ �������� ������ 
  -- ������� ������ � ������� rf_imp_session (� ��������� ��������: ��� ���� ������� � ������������)
  FUNCTION start_imp_session(
    p_imp_type_cd     IN mantas.rf_imp_session.imp_type_cd%TYPE     -- ��� ���� �������
   ,p_user            IN mantas.rf_imp_session.loaded_by%TYPE       -- ��� ������������
   ,p_actual_dt       IN mantas.rf_imp_session.actual_dt%TYPE       -- ���� ������������ (�������� � ����������)
   ,p_extra_params_tx IN mantas.rf_imp_session.extra_params_tx%TYPE -- �������������� ���������
   ,p_note_tx         IN mantas.rf_imp_session.note_tx%TYPE         -- ����������� (�������� � ����������)
   ,p_ip_address      IN mantas.rf_audit_event.ip_address%TYPE DEFAULT '0.0.0.0' -- IP ����� ������������ 
  ) RETURN mantas.rf_imp_session.imp_session_id%TYPE;        -- ���������� id ������

  -- ��������� ������� ������ �������� ������ 
  -- �������� ����� ��������� ������ �������� � ������� ��������
  PROCEDURE end_imp_session;
  
  -- ���������� id ������� ������
  FUNCTION get_imp_session_id
    RETURN mantas.rf_imp_session.imp_session_id%TYPE; -- ID ������

  -- ���������� ������������ ������� ������
  FUNCTION get_session_user
    RETURN mantas.rf_imp_session.loaded_by%TYPE; -- ��� ������������

  -- ���������� ��� ���� ������� ������� ������
  FUNCTION get_session_imp_type_cd
    RETURN mantas.rf_imp_session.loaded_by%TYPE; -- ��� ���� �������

  -- ���������� ���� ������������ ������� ������
  FUNCTION get_session_actual_dt
    RETURN mantas.rf_imp_session.actual_dt%TYPE; -- ���� ������������

  -- ���������� ������������ ��������� ������� ������
  FUNCTION get_session_extra_params_tx
    RETURN mantas.rf_imp_session.extra_params_tx%TYPE; -- �������������� ���������

  -- ���������� ����������� ������� ������
  FUNCTION get_session_note_tx
    RETURN mantas.rf_imp_session.note_tx%TYPE; -- �����������

  -- �����, � ���� ��� ������� ������������� ����� �������� (� ������ � ��������������� �������)
  -- ���������� ������ ���� ��� �����, ���� 4 ��������� ��������� � ����������� �� ���� ��� ����� �����
  -- ��������: ����������� ����������
  FUNCTION find_or_create_imp_file(
    p_file_nm     IN mantas.rf_imp_file.file_nm%TYPE     -- ��� ����� (NULL ���� ���� �� �� �����)
   ,p_data_dt     IN mantas.rf_imp_file.data_dt%TYPE     -- ����, �� ������� ����������� ������.
   ,p_dpt_cd      IN mantas.rf_imp_file.dpt_cd%TYPE      -- ��� �������������, � �������� ��������� ����
   ,p_sys_cd      IN mantas.rf_imp_file.sys_cd%TYPE    -- ��� �������-��������� �����
   ,p_order_nb    IN mantas.rf_imp_file.order_nb%TYPE     -- ���������� ����� ����� ����� ������ �� ��������� ����/�������������/�������-���������
   ,p_archive_nm  IN mantas.rf_imp_file.archive_nm%TYPE  -- ��� ��������� �����, �� �������� ������� ������ ����
   ,p_search_type IN VARCHAR2                            -- ��� ������: 'N'- �� ����� �����, 'P' �� �������������� ����������
  ) RETURN mantas.rf_imp_file.imp_file_id%TYPE;  -- ������������� ��������� ��� ��������� �����

  -- �������� �� ID ����� �������, ������ ��� ��������� ��������
  FUNCTION get_last_load_status(
    p_imp_file_id     IN mantas.rf_imp_file.imp_file_id%TYPE  -- ������������� ��������� �����
  ) RETURN mantas.rf_imp_history.load_status%TYPE; -- ������������ ��������: ������ ��������� �������� �����

  -- ��������� ����������� �� ��� ������� ��������� ������� ��� �������� (�� ������) ���� ������� � ���� ������� ���������
  FUNCTION check_sys_filter(
    p_sys_cd      IN mantas.rf_imp_file.order_nb%TYPE    -- ��� �������-��������� 
  ) RETURN BOOLEAN; -- ������������ ��������: TRUE - ��� ���� ������ �������� (�������� ����� �������), FALSE - ��� �� ������ ��������

  -- ��������� ����������� �� ��� ���� ������� ��� �������� (�� ������) ���� ������� � ���� ������� ���������
  FUNCTION check_opok_filter(
    p_sys_cd      IN mantas.rf_imp_file.order_nb%TYPE    -- ��� �������-��������� 
   ,p_opok        IN VARCHAR2                            -- ��� ���� ��� ��������
  ) RETURN BOOLEAN; -- ������������ ��������: TRUE - ��� ���� ������ �������� (�������� ����� �������), FALSE - ��� �� ������ ��������
  
  -- ������� ������ � ������� �������� ����� (�������� ������ � ��������� ������)
  PROCEDURE log_imp_history(
    p_record_type       IN mantas.rf_imp_history.record_type%TYPE   -- ��� ������ � �������
   ,p_imp_file_id       IN mantas.rf_imp_file.imp_file_id%TYPE      -- ������������� ��������� ��� ��������� �����
   ,p_load_status       IN mantas.rf_imp_history.load_status%TYPE   -- ������ ��������
   ,p_err_msg_tx        IN mantas.rf_imp_history.err_msg_tx%TYPE    -- ����� ��������� �� ������/��������������. ���� �������� ��������, �� ������ ���� ������
   ,p_load_qty          IN mantas.rf_imp_history.load_qty%TYPE      -- ���������� ������� ����������� �������
   ,p_err_qty           IN mantas.rf_imp_history.err_qty%TYPE       -- ���������� ����������� (�� �����������) �������
   ,p_file_nm           IN mantas.rf_imp_history.file_nm%TYPE       -- ��� �������������� �����
   ,p_file_size         IN mantas.rf_imp_history.file_size%TYPE     -- ������ �������������� ����� � ������
   ,p_note_tx           IN mantas.rf_imp_history.note_tx%TYPE       -- ����������� � ��������
   ,p_content_data      IN BLOB DEFAULT NULL                        -- ���������� ������������ �����
  );
  
  -- ���������� ������������� ���� �������� ��� ���� ��� �� ���� ���� �� ������� ��������� ������� ������������ 
  FUNCTION lock_imp_file( 
    p_imp_file_id       IN mantas.rf_imp_file.imp_file_id%TYPE      -- ������������� ��������� ��� ��������� �����
  ) RETURN BOOLEAN;  -- ������ ���������� ����� (TRUE - �������, FALSE - ���� ������������ ������ �������������)
    
  -- �������� ���� ��� �������� (�������� ������������� ���������� ��� ���� ���� �������)
  FUNCTION add_file(
    p_file_nm       IN mantas.rf_imp_file.file_nm%TYPE      -- ��� �����
   ,p_archive_nm    IN mantas.rf_imp_file.archive_nm%TYPE   -- ��� ������ (���� ���� ��������� - NULL)
   ,p_file          IN BLOB                                 -- ������ �����
   ,p_message       OUT VARCHAR2                            -- ��������� �� ������
  ) RETURN NUMBER;  -- ������ ��������� (1 �������, ����� 0)  

  -- ���������/���������� ��� ����� (�������� ������������� ���������� ��� ���� ���� �������)
  FUNCTION process_files(
    p_message      OUT VARCHAR2 -- ��������� �������� ��������� (������� ����� ���������� ������, �� ��� � �������)
  ) RETURN NUMBER;  -- ������ ��������� (1 �������, ����� 0)  
  
end;
/

create or replace package body mantas.rf_pkg_import as

  -- ��� ��� ���������
  TYPE filter_tt IS TABLE OF rf_imp_sys.filter_opok_tx%TYPE INDEX BY rf_imp_sys.sys_cd%TYPE;
  
  -- �������� ������� ������
  g_imp_session_id          rf_imp_session.imp_session_id%TYPE;  -- id ������� ������
  g_session_user            rf_imp_session.loaded_by%TYPE;       -- ��� ������������ ������� ������
  g_session_imp_type_cd     rf_imp_session.loaded_by%TYPE;       -- ��� ���� ������� ������� ������
  g_session_actual_dt       rf_imp_session.actual_dt%TYPE;       -- ���������� ���� ������� ������
  g_session_extra_params_tx rf_imp_session.extra_params_tx%TYPE; -- �������������� ��������� ������� ������
  g_session_note_tx         rf_imp_session.note_tx%TYPE;         -- ����������� ������� ������
  g_session_filters         filter_tt;                           -- ������ �������� ��� ������� ���� ���� ������� 
  g_session_filter_sys      rf_imp_type.filter_sys_tx%TYPE;      -- ������ �� ���� ������� ��� �������� ���� ���� �������
  g_session_ip_address      rf_audit_event.ip_address%TYPE;      -- IP ����� ������ �������� ��������
  g_session_files           rf_json_list;                        -- ������ ����������� ������
     
  c_dbf_header_length constant number := 32; -- ����� ��������� dbf �����
  c_dbf_field_length  constant number := 32; -- ����� ���� � dbf �����
  
  /* �������� ��������� ������� bnkseek � dbf �����, ���������� �� �� (www.cbr.rf) */
  c_bnkseek_fields_struct_cb constant varchar2(4000) := '564B4559000000000000004301000000080000000000000000000000000000005245414C00000000000000430900000004000000000000000000000000000000505A4E0000000000000000430D000000020000000000000000000000000000005545520000000000000000430F0000000100000000000000000000000000000052474E0000000000000000431000000002000000000000000000000000000000494E440000000000000000431200000006000000000000000000000000000000544E5000000000000000004318000000010000000000000000000000000000004E4E500000000000000000431900000019000000000000000000000000000000414452000000000000000043320000001E000000000000000000000000000000524B4300000000000000004350000000090000000000000000000000000000004E414D455000000000000043590000002D0000000000000000000000000000004E414D454E00000000000043860000001E0000000000000000000000000000004E45574E554D000000000043A4000000090000000000000000000000000000004E45574B5300000000000043AD000000090000000000000000000000000000005045524D464F000000000043B60000000600000000000000000000000000000053524F4B0000000000000043BC00000002000000000000000000000000000000415431000000000000000043BE00000007000000000000000000000000000000415432000000000000000043C50000000700000000000000000000000000000054454C454600000000000043CC000000190000000000000000000000000000005245474E0000000000000043E5000000090000000000000000000000000000004F4B504F0000000000000043EE0000000800000000000000000000000000000044545F495A4D000000000044F600000008000000000000000000000000000000434B53000000000000000043FE000000060000000000000000000000000000004B534E5000000000000000430401000014000000000000000000000000000000444154455F494E00000000441801000008000000000000000000000000000000444154455F434800000000442001000008000000000000000000000000000000564B455944454C0000000043280100000800000000000000000000000000000044545F495A4D5200000000443001000008000000000000000000000000000000';
  /* �������� ��������� ������� bnkseek � dbf �����, ���������� �� ��������� */
  c_bnkseek_fields_struct_sb constant varchar2(4000) := '564B4559000000000000004301000000080000000000000000000000000000005245414C00000000000000430900000004000000000000000000000000000000505A4E0000000000000000430D000000020000000000000000000000000000005545520000000000000000430F0000000100000000000000000000000000000052474E0000000000000000431000000002000000000000000000000000000000494E440000000000000000431200000006000000000000000000000000000000544E5000000000000000004318000000010000000000000000000000000000004E4E500000000000000000431900000019000000000000000000000000000000414452000000000000000043320000001E000000000000000000000000000000524B4300000000000000004350000000090000000000000000000000000000004E414D455000000000000043590000002D0000000000000000000000000000004E414D454E00000000000043860000001E0000000000000000000000000000004E45574E554D000000000043A4000000090000000000000000000000000000004E45574B5300000000000043AD000000090000000000000000000000000000005045524D464F000000000043B60000000600000000000000000000000000000053524F4B0000000000000043BC00000002000000000000000000000000000000415431000000000000000043BE00000007000000000000000000000000000000415432000000000000000043C50000000700000000000000000000000000000054454C454600000000000043CC000000190000000000000000000000000000005245474E0000000000000043E5000000090000000000000000000000000000004F4B504F0000000000000043EE0000000800000000000000000000000000000044545F495A4D000000000044F600000008000000000000000000000000000000434B53000000000000000043FE000000060000000000000000000000000000004B534E5000000000000000430401000014000000000000000000000000000000444154455F494E00000000441801000008000000000000000000000000000000564B455944454C00000000432001000008000000000000000000000000000000444154455F434800000000442801000008000000000000000000000000000000';

  /* �������� ��������� ������� bnkdel � dbf �����, ���������� �� �� (www.cbr.rf) */
  c_bnkdel_fields_struct_cb constant varchar2(4000) := '564B455900000000000000430100000008000000000000000000000000000000564B455944454C00000000430900000008000000000000000000000000000000505A4E0000000000000000431100000002000000000000000000000000000000554552000000000000000043130000000100000000000000000000000000000052474E0000000000000000431400000002000000000000000000000000000000494E440000000000000000431600000006000000000000000000000000000000544E500000000000000000431C000000010000000000000000000000000000004E4E500000000000000000431D00000019000000000000000000000000000000414452000000000000000043360000001E000000000000000000000000000000524B4300000000000000004354000000090000000000000000000000000000004E414D4550000000000000435D0000002D0000000000000000000000000000004E414D454E000000000000438A0000001E0000000000000000000000000000004E45574E554D000000000043A8000000090000000000000000000000000000004E45574B5300000000000043B1000000090000000000000000000000000000005045524D464F000000000043BA0000000600000000000000000000000000000053524F4B0000000000000043C000000002000000000000000000000000000000415431000000000000000043C200000007000000000000000000000000000000415432000000000000000043C90000000700000000000000000000000000000054454C454600000000000043D0000000190000000000000000000000000000005245474E0000000000000043E9000000090000000000000000000000000000004F4B504F0000000000000043F2000000080000000000000000000000000000004441544544454C0000000044FA0000000800000000000000000000000000000044545F495A4D0000000000440201000008000000000000000000000000000000434B530000000000000000430A010000060000000000000000000000000000004B534E5000000000000000431001000014000000000000000000000000000000525F434C4F534500000000432401000002000000000000000000000000000000444154455F494E00000000442601000008000000000000000000000000000000';
  /* �������� ��������� ������� bnkseek � dbf �����, ���������� �� ��������� */
  c_bnkdel_fields_struct_sb constant varchar2(4000) := '564B455900000000000000430000000008000000000000000000000000000000564B455944454C00000000430000000008000000000000000000000000000000505A4E0000000000000000430000000002000000000000000000000000000000554552000000000000000043000000000100000000000000000000000000000052474E0000000000000000430000000002000000000000000000000000000000494E440000000000000000430000000006000000000000000000000000000000544E5000000000000000004300000000010000000000000000000000000000004E4E500000000000000000430000000019000000000000000000000000000000414452000000000000000043000000001E000000000000000000000000000000524B4300000000000000004300000000090000000000000000000000000000004E414D455000000000000043000000002D0000000000000000000000000000004E414D454E00000000000043000000001E0000000000000000000000000000004E45574E554D00000000004300000000090000000000000000000000000000004E45574B5300000000000043000000000900000000000000000000000000000053524F4B000000000000004300000000020000000000000000000000000000005045524D464F00000000004300000000060000000000000000000000000000004154310000000000000000430000000007000000000000000000000000000000415432000000000000000043000000000700000000000000000000000000000054454C45460000000000004300000000190000000000000000000000000000005245474E000000000000004300000000090000000000000000000000000000004F4B504F000000000000004300000000080000000000000000000000000000004441544544454C0000000044000000000800000000000000000000000000000044545F495A4D0000000000440000000008000000000000000000000000000000434B5300000000000000004300000000060000000000000000000000000000004B534E5000000000000000430000000014000000000000000000000000000000525F434C4F534500000000430000000002000000000000000000000000000000444154455F494E00000000440000000008000000000000000000000000000000';
  
  /* ��� ��� ������  ���� */
  type t_log is record (
    imp_type_cd mantas.rf_imp_history.imp_type_cd%type, -- ��� �������
    load_status mantas.rf_imp_history.load_status%type, -- ������ �������
    err_msg_tx  mantas.rf_imp_history.err_msg_tx%type,  -- ����� ������
    load_qty    mantas.rf_imp_history.load_qty%type,    -- ���������� ��������������� �������
    err_qty     mantas.rf_imp_history.err_qty%type,     -- ���������� ��������� �������
    actual_dt   mantas.rf_imp_history.actual_dt%type,   -- ���������� ���� ������
    file_nm     mantas.rf_imp_history.file_nm%type,     -- ��� �����
    file_size   mantas.rf_imp_history.file_size%type,   -- ������ �����
    note_tx     mantas.rf_imp_history.note_tx%type,   -- �����������
    loaded_by   mantas.rf_imp_history.loaded_by%type    -- ��� ������������, ��� ���������� ������
  );
  
  g_log t_log;
  
  
  /* ����� ������ ���������� � ���.
   * 
   * ��������� ������
   *   p_imp_type_cd - ��� �������
   *   p_load_status - ������ �������
   *   p_err_msg_tx - ����� ������
   *   p_load_qty - ���������� ��������������� �������
   *   p_err_qty - ���������� ��������� �������
   *   p_actual_dt - ���������� ���� ������
   *   p_file_nm - ��� �����
   *   p_file_size - ������ �����
   *   p_loaded_by - ��� ������������, ��� ���������� ������
   *   p_clear_log - ������� �� ���
   */
  procedure put_in_log (
    p_imp_type_cd in mantas.rf_imp_history.imp_type_cd%type default null,
    p_load_status in mantas.rf_imp_history.load_status%type default null,
    p_err_msg_tx  in mantas.rf_imp_history.err_msg_tx%type default null,
    p_load_qty    in mantas.rf_imp_history.load_qty%type default null,
    p_err_qty     in mantas.rf_imp_history.err_qty%type default null,
    p_actual_dt   in mantas.rf_imp_history.actual_dt%type default null,
    p_file_nm     in mantas.rf_imp_history.file_nm%type default null,
    p_note_tx     in mantas.rf_imp_history.note_tx%type default null,
    p_file_size   in mantas.rf_imp_history.file_size%type default null,
    p_loaded_by   in mantas.rf_imp_history.loaded_by%type default null,
    p_clear_log   in boolean  default false
  ) is
  begin
    if p_clear_log then 
      g_log.imp_type_cd := null;
      g_log.load_status := null;
      g_log.err_msg_tx  := null;
      g_log.load_qty    := null;
      g_log.err_qty     := null;
      g_log.actual_dt   := null;
      g_log.file_nm     := null;
      g_log.file_size   := null;
      g_log.note_tx     := null;
      g_log.loaded_by   := null;
    end if;
    
    g_log.imp_type_cd := nvl(p_imp_type_cd, g_log.imp_type_cd);
    g_log.load_status := nvl(p_load_status, g_log.load_status);
    g_log.err_msg_tx  := case 
                           when p_err_msg_tx is not null and g_log.err_msg_tx is not null then p_err_msg_tx||' '||chr(10)||g_log.err_msg_tx
                           else nvl(p_err_msg_tx, g_log.err_msg_tx)
                         end;  
    g_log.load_qty    := nvl(p_load_qty, g_log.load_qty);
    g_log.err_qty     := nvl(p_err_qty, g_log.err_qty);
    g_log.actual_dt   := nvl(p_actual_dt, g_log.actual_dt);
    g_log.file_nm     := nvl(p_file_nm, g_log.file_nm);
    g_log.file_size   := nvl(p_file_size, g_log.file_size);
    g_log.note_tx     := nvl(p_note_tx, g_log.note_tx);
    g_log.loaded_by   := nvl(p_loaded_by, g_log.loaded_by);
  end;
  
  
  /* ������ ���� � ��
   * 
   * ��������� ������
   *   p_show - ������� ������ � ������� (��� ������)
   */
  procedure write_log (
    p_show boolean default false )
  is
  begin
    if p_show then
      dbms_output.put_line('imp_type_cd = '||g_log.imp_type_cd);
      dbms_output.put_line('load_status = '||g_log.load_status);
      dbms_output.put_line('err_msg_tx  = '||g_log.err_msg_tx);
      dbms_output.put_line('load_qty    = '||g_log.load_qty);
      dbms_output.put_line('err_qty     = '||g_log.err_qty);
      dbms_output.put_line('actual_dt   = '||g_log.actual_dt);
      dbms_output.put_line('file_nm     = '||g_log.file_nm);
      dbms_output.put_line('file_size   = '||g_log.file_size);
      dbms_output.put_line('note_tx     = '||g_log.note_tx);
      dbms_output.put_line('loaded_by   = '||g_log.loaded_by);
    end if;  

    update mantas.rf_imp_history 
       set last_load_flag = null
     where imp_type_cd = g_log.imp_type_cd
       and last_load_flag = 1;

    insert into mantas.rf_imp_history (
      imp_seq_id, imp_type_cd, last_load_flag, load_dt, load_status, err_msg_tx, 
      load_qty, err_qty, actual_dt, file_nm, file_size, note_tx, loaded_by
    ) values (
      rf_imp_history_seq.nextval,
      g_log.imp_type_cd, 
      1, 
      sysdate,
      g_log.load_status,
      g_log.err_msg_tx,
      g_log.load_qty,
      g_log.err_qty,
      g_log.actual_dt,
      g_log.file_nm,
      g_log.file_size,
      g_log.note_tx,
      g_log.loaded_by
    );
  end;

  
  /* ����� ������������� ����������. ������ ��� ������� ������� �� ������� MANTAS.RF_IMP_TYPE
   *
   * ��������� ������
   *   p_type - ��� �������
   */
  procedure init (
    p_type       in  mantas.rf_imp_type.imp_type_cd%type,
    p_dir_name   out mantas.rf_imp_type.directory_nm%type,
    p_file_name  out mantas.rf_imp_type.file_nm%type,
    p_table_name out varchar2 ) 
  is
  begin
    select directory_nm, file_nm,
           case p_type
             when 'CBRF_BNKSEEK' then 'RF_CBRF_BNKSEEK'
             when 'CBRF_BNKDEL'  then 'RF_CBRF_BNKDEL'
           end  
      into p_dir_name, p_file_name, p_table_name
      from rf_imp_type
     where upper(imp_type_cd) = upper(p_type);

    exception
      when no_data_found then 
        raise_application_error(-20001, '�� ��������� ��� ������� "'||upper(p_type)||'" � ������� RF_IMP_TYPE.');
  end;
  
  
  /* ����� �������� ��������� ������� � dbf �����.
   *
   * ��������� ������
   *   p_blob - dbf ����
   *   p_type - ��� �������
   */
  procedure check_dbf_structure (
    p_blob in blob,
    p_type in mantas.rf_imp_type.imp_type_cd%type,
    p_file_type out varchar2 )
  is
  begin
    p_file_type := 'SB';
    
    if p_type = 'CBRF_BNKSEEK' then 
      /* ���� �� - c_dbf_field_length * 28 ��������� */
      if c_bnkseek_fields_struct_cb = dbms_lob.substr(p_blob, c_dbf_field_length * 28, c_dbf_header_length + 1) then p_file_type := 'CB';
        /* ���� ��������� - c_dbf_field_length * 27 ��������� */
        elsif c_bnkseek_fields_struct_sb = dbms_lob.substr(p_blob, c_dbf_field_length * 27, c_dbf_header_length + 1) then p_file_type := 'SB';
        else raise_application_error(-20002, '������ � ��������� DBF �����.');
      end if;

    elsif p_type = 'CBRF_BNKDEL' then 
      /* ���� �� */
      if c_bnkdel_fields_struct_cb = dbms_lob.substr(p_blob, c_dbf_field_length * 27, c_dbf_header_length + 1) then p_file_type := 'CB';
        /* ���� ��������� */
        elsif c_bnkdel_fields_struct_sb = dbms_lob.substr(p_blob, c_dbf_field_length * 27, c_dbf_header_length + 1) then p_file_type := 'SB';
        else raise_application_error(-20002, '������ � ��������� DBF �����.');
      end if;
    end if;  
  end;


  /* ����� ��������� ����� �� ������� ������� business.rf_cbrf_bnkseek_dbf � ������� business.rf_cbrf_bnkseek 
   * �������� ���������� � ������ �� ������������.
   *
   * ��������� ������
   *   p_file_type - ��� dbf �����: 
   *     - CB: dbf - ����, ���������� �� �� (www.cbr.ru)
   *     - SB: dbf - ����, ���������� �� ���������
   */
  procedure update_cbrf_bnkseek (
    p_file_type in varchar2 default 'SB' )
  is
  begin
    delete from business.rf_cbrf_bnkseek;
    
    if upper(p_file_type) = 'CB' then 
      insert into business.rf_cbrf_bnkseek (
        vkey, real, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen, 
        newnum, newks, permfo, srok, at1, at2, telef, regn, okpo, dt_izm, 
        cks, ksnp, date_in, date_ch, vkeydel, dt_izmr )
      select vkey, real, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen, newnum,
             newks, permfo, srok, at1, at2, telef, regn, okpo, to_date(dt_izm, 'YYYYMMDD'),
             cks, ksnp, to_date(date_in, 'YYYYMMDD'), to_date(date_ch, 'YYYYMMDD'), vkeydel,
             to_date(dt_izmr, 'YYYYMMDD')
      from business.rf_cbrf_bnkseek_dbf;
      
    elsif upper(p_file_type) = 'SB' then 
      insert into business.rf_cbrf_bnkseek (
        vkey, real, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen, 
        newnum, newks, permfo, srok, at1, at2, telef, regn, okpo, dt_izm, 
        cks, ksnp, date_in, date_ch, vkeydel )
      select vkey, real, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen, newnum,
             newks, permfo, srok, at1, at2, telef, regn, okpo, to_date(dt_izm, 'YYYYMMDD'),
             cks,ksnp, to_date(date_in, 'YYYYMMDD'), to_date(date_ch, 'YYYYMMDD'), vkeydel
      from business.rf_cbrf_bnkseek_dbf_2;
    
    else
      raise_application_error(-20002, '������ � ����������� DBF �����.');
    end if;  
  end;
  
  
  /* ����� ��������� ����� �� ������� ������� business.rf_cbrf_bnkdel � ������� business.rf_cbrf_bnkdel 
   * �������� ���������� � ������ �� ������������.
   *
   * ��������� ������
   *   p_file_type - ��� dbf �����: 
   *     - CB: dbf - ����, ���������� �� �� (www.cbr.ru)
   *     - SB: dbf - ����, ���������� �� ���������
   */
  procedure update_cbrf_bnkdel (
    p_file_type in varchar2 default 'SB' )
  is
  begin
    delete from business.rf_cbrf_bnkdel;
    
    if upper(p_file_type) = 'CB' then 
      insert into business.rf_cbrf_bnkdel (
        vkey, vkeydel, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, 
        namen, newnum, newks, permfo, srok, at1, at2, telef, regn, 
        okpo, datedel, dt_izm, cks, ksnp, r_close, date_in )
      select vkey, vkeydel, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen,
             newnum, newks, permfo, srok, at1, at2, telef, regn, okpo,
             to_date(datedel, 'YYYYMMDD'), to_date(dt_izm, 'YYYYMMDD'), cks, ksnp,
             r_close, to_date(date_in, 'YYYYMMDD')
        from business.rf_cbrf_bnkdel_dbf;
    
    elsif upper(p_file_type) = 'SB' then 
      insert into business.rf_cbrf_bnkdel (
        vkey, vkeydel, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, 
        namen, newnum, newks, srok, permfo, at1, at2, telef, regn, 
        okpo, datedel, dt_izm, cks, ksnp, r_close, date_in )
      select vkey, vkeydel, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen,
             newnum, newks, srok, permfo, at1, at2, telef, regn, okpo,
             to_date(datedel, 'YYYYMMDD'), to_date(dt_izm, 'YYYYMMDD'), cks, ksnp,
             r_close, to_date(date_in, 'YYYYMMDD')
        from business.rf_cbrf_bnkdel_dbf_2;
        
    else
      raise_application_error(-20002, '������ � ����������� DBF �����.');
    end if; 
  end;
  

  /* ����� ��������� dbf ����. 
   * ����������� ��������� ��� ���������� ����� ������� �� ������� MANTAS.RF_IMP_TYPE �� ���� ������� (p_type).
   *
   * ��������� ������
   *   p_blob - dbf ����
   *   p_type - ��� �������
   *   p_file_name - ��� �����
   *   p_date_act - ���� ������������
   *   p_note - �����������
   *   p_user - ��� ������������
   *   p_status - ��������� �������� O/W/E
   *   p_err_msg - ���������
   *   p_load_qty - ���������� ����������� �������
   */
  procedure save_dbf_to_file (
    p_type      in mantas.rf_imp_history.imp_type_cd%type,
    p_date_act  in mantas.rf_imp_history.actual_dt%type,
    p_file_name in mantas.rf_imp_history.file_nm%type,
    p_note      in mantas.rf_imp_history.note_tx%type,
    p_user      in mantas.rf_imp_history.loaded_by%type,
    p_file      in blob,
    p_status    out mantas.rf_imp_history.load_status%type,
    p_err_msg   out mantas.rf_imp_history.err_msg_tx%type,
    p_load_qty  out mantas.rf_imp_history.load_qty%type )
  is
    v_dir_name     mantas.rf_imp_type.directory_nm%type;
    v_file_name    mantas.rf_imp_type.file_nm%type;
    v_table_name   varchar2(30);

    v_file         utl_file.file_type;
    v_blob_length  number;
    v_buffer       raw(32767);
    v_pos          number := 1;
    v_ammount      binary_integer := 32767;
    v_data         varchar2(100);
    v_count        number;
    v_date         date;
    v_count_in_dbf number := 0;
    v_file_type    varchar2(4);
  begin
    /* ���������� ������ ����� */
    v_blob_length := dbms_lob.getlength(p_file);

    /* ���������� ���������� � ��� */
    put_in_log(
      p_imp_type_cd => p_type,
      p_load_status => 'O',
      p_file_nm     => p_file_name,
      p_file_size   => v_blob_length,
      p_actual_dt   => p_date_act,
      p_note_tx     => p_note,
      p_loaded_by   => p_user,
      p_clear_log   => true
    );

    /* �������� ������������� ����������� ���������� � ����������� �� ���� ������� */
    init(p_type, v_dir_name, v_file_name, v_table_name);

    /* ��������� ��������� dbf ����� ������������� �� ��� ��������� */
    check_dbf_structure(p_file, p_type, v_file_type);

    /* ��� �����, ����������� �� ��������� ������ ��� -> bnkseek_2.dbf */
    if v_file_type = 'SB' then
      v_file_name := replace(v_file_name, '.dbf', '_2.dbf');
    end if;
    
    /* ������ dbf blob � ���� */
    v_file := utl_file.fopen('AML_IMP_CBRF_BNK_DIR', v_file_name, 'wb', 32767);
    while v_pos < v_blob_length loop
      dbms_lob.read(p_file, v_ammount, v_pos, v_buffer);
      utl_file.put_raw(v_file, v_buffer, true);
      v_pos := v_pos + v_ammount;
    end loop;
    utl_file.fclose(v_file);

    /* ��������� ������ �� ������� � ������� ������� */
    if p_type = 'CBRF_BNKSEEK' then 
      update_cbrf_bnkseek(v_file_type);
    elsif p_type = 'CBRF_BNKDEL' then 
      update_cbrf_bnkdel(v_file_type);
    end if;

    /* ��������� ���������� ����������� ������ � ������������ ���� ������������ */
    execute immediate 'select count(*) from business.'||v_table_name into v_count;

    /* �� ��������� dbf �������� ���� ����������� � ���������� ������� � �����  */
    v_data := utl_raw.cast_to_varchar2(dbms_lob.substr(p_file, 32, 1));
    v_date := to_date( (2000 + ascii(substr(v_data, 2, 1))) || lpad(ascii(substr( v_data, 3, 1)), 2, '0') || lpad(ascii(substr( v_data, 4, 1)), 2, '0'), 'YYYYMMDD' );
    v_data := substr( v_data,  5, 4);
    for i in 1 .. length(v_data) loop
      v_count_in_dbf := v_count_in_dbf + ascii(substr(v_data, i, 1)) * power(2, 8*(i-1));
    end loop;

    /* ���������� ���������� � ��� */
    put_in_log(
      p_load_qty    => v_count,
      p_err_qty     => v_count_in_dbf - v_count,
      p_load_status => case 
                         when v_count_in_dbf - v_count <> 0 then 'W'
                       end,
      p_err_msg_tx  => case 
                         when v_count_in_dbf - v_count <> 0 then '�� ��������� '||(v_count_in_dbf - v_count)||' �������.'
                       end
    );
    
    /* ���� ���� dbf ����� ������ ���� ����������� �������� ��� �� ���� ������������, �� ������ �������������� */
    if v_date < mantas.rf_add_work_days(p_date_act, 2, -1) then
      put_in_log( 
        p_load_status => 'W',
        p_err_msg_tx  => '���� ����� ['||to_char(v_date, 'DD.MM.YYYY')||'] ������ ��������� ���� ������������ ['||to_char(p_date_act, 'DD.MM.YYYY')||'].'
      );
    end if;
    
    /* ���������� ��� � �� */
    write_log;
    commit;
    
    /* ���������� ��������� � ��������� �������� */
    p_status   := g_log.load_status;
    p_err_msg  := g_log.err_msg_tx;
    p_load_qty := g_log.load_qty;
    
    exception
      when others then
        rollback;
        put_in_log(
          p_load_status => 'E',
          p_err_msg_tx  => sqlerrm()
        );

        write_log;
        commit;

        p_status   := g_log.load_status;
        p_err_msg  := g_log.err_msg_tx;
        p_load_qty := g_log.load_qty;
  end;

  -- ������ ����� ������ �������� ������ 
  -- ������� ������ � ������� rf_imp_session (� ��������� ��������: ��� ���� ������� � ������������)
  FUNCTION start_imp_session(
    p_imp_type_cd     IN mantas.rf_imp_session.imp_type_cd%TYPE     -- ��� ���� �������
   ,p_user            IN mantas.rf_imp_session.loaded_by%TYPE       -- ��� ������������
   ,p_actual_dt       IN mantas.rf_imp_session.actual_dt%TYPE       -- ���� ������������ (�������� � ����������)
   ,p_extra_params_tx IN mantas.rf_imp_session.extra_params_tx%TYPE -- �������������� ���������
   ,p_note_tx         IN mantas.rf_imp_session.note_tx%TYPE         -- ����������� (�������� � ����������)
   ,p_ip_address      IN mantas.rf_audit_event.ip_address%TYPE DEFAULT '0.0.0.0' -- IP ����� ������������ 
  ) RETURN mantas.rf_imp_session.imp_session_id%TYPE IS        -- ���������� id ������
    BEGIN
      -- ��������� ������� ���������
      IF p_imp_type_cd IS NULL OR p_user IS NULL THEN
        RAISE_APPLICATION_ERROR(-20000,'������ �������� ������ �������: �� ��� ��������� ���������!');
      END IF;
      
      -- ������ ��������� �������
      if (p_imp_type_cd = 'SBRF_321XML') then
        delete from std.imp_sbrf_321xml;
      elsif (p_imp_type_cd = 'SBRF_321XML_8001') then
        delete from std.imp_sbrf_321xml_8001;
      end if;
      
      -- ������� ������
      INSERT INTO rf_imp_session (imp_session_id, imp_type_cd, loaded_by, start_dt, actual_dt, extra_params_tx,note_tx )
        VALUES (rf_imp_session_seq.NEXTVAL,p_imp_type_cd,p_user,SYSDATE,p_actual_dt,p_extra_params_tx,p_note_tx)
        RETURNING imp_session_id INTO g_imp_session_id;
      g_session_imp_type_cd:=p_imp_type_cd;
      g_session_user:=p_user;
      g_session_actual_dt:=p_actual_dt;
      g_session_extra_params_tx:=p_extra_params_tx;
      g_session_note_tx:=p_note_tx;
      g_session_ip_address:=p_ip_address;
      g_session_files:=rf_json_list();
 
      -- ��������� ������ �� ���� �������
      SELECT MAX(t.filter_sys_tx) INTO g_session_filter_sys FROM rf_imp_type t WHERE t.imp_type_cd=p_imp_type_cd;
      -- ��������� ������� ���� ��� ������
      g_session_filters.DELETE;
      FOR c_filters IN (SELECT f.sys_cd,f.filter_opok_tx FROM rf_imp_sys f WHERE f.imp_type_cd=p_imp_type_cd)
        LOOP
          g_session_filters(c_filters.sys_cd):=c_filters.filter_opok_tx;
        END LOOP;

      RETURN g_imp_session_id;
      
    END start_imp_session; 
  
  -- �������� �� ���������� ������
  PROCEDURE validate_session IS
    BEGIN
      IF g_imp_session_id IS NULL THEN
        RAISE_APPLICATION_ERROR(-20000,'������: ������ ������� �� ����������!');
      END IF;
    END validate_session;
    
  -- ��������� ������� ������ �������� ������ 
  -- �������� ����� ��������� ������ �������� � ������� ��������
  PROCEDURE end_imp_session IS
    BEGIN
      validate_session;
      UPDATE rf_imp_session s SET s.end_dt=SYSDATE WHERE s.imp_session_id=g_imp_session_id;
      g_imp_session_id:=NULL;
      g_session_imp_type_cd:=NULL;
      g_session_user:=NULL;
      g_session_actual_dt:=NULL;
      g_session_extra_params_tx:=NULL;
      g_session_note_tx:=NULL;
      g_session_filter_sys:=NULL;
      g_session_ip_address:=NULL;
      g_session_files:=NULL;
      g_session_filters.DELETE;
    END end_imp_session;
  
  -- ���������� id ������� ������
  FUNCTION get_imp_session_id
    RETURN mantas.rf_imp_session.imp_session_id%TYPE IS -- ID ������
    BEGIN
      validate_session;
      RETURN g_imp_session_id;
    END get_imp_session_id;  
    
  -- ���������� ������������ ������� ������
  FUNCTION get_session_user
    RETURN mantas.rf_imp_session.loaded_by%TYPE IS -- ��� ������������
    BEGIN
      validate_session;
      RETURN g_session_user;
    END get_session_user;  

  -- ���������� ��� ���� ������� ������� ������
  FUNCTION get_session_imp_type_cd
    RETURN mantas.rf_imp_session.loaded_by%TYPE IS -- ��� ���� �������
    BEGIN
      validate_session;
      RETURN g_session_imp_type_cd;
    END get_session_imp_type_cd;  

  -- ���������� ���� ������������ ������� ������
  FUNCTION get_session_actual_dt
    RETURN mantas.rf_imp_session.actual_dt%TYPE IS -- ���� ������������
    BEGIN
      validate_session;
      RETURN g_session_actual_dt;
    END get_session_actual_dt;  

  -- ���������� �������������� ��������� ������� ������
  FUNCTION get_session_extra_params_tx
    RETURN mantas.rf_imp_session.extra_params_tx%TYPE IS -- �������������� ���������
    BEGIN
      validate_session;
      RETURN g_session_extra_params_tx;
    END get_session_extra_params_tx;  

  -- ���������� ����������� ������� ������
  FUNCTION get_session_note_tx
    RETURN mantas.rf_imp_session.note_tx%TYPE IS -- �����������
    BEGIN
      validate_session;
      RETURN g_session_note_tx;
    END get_session_note_tx;  

  -- �����, � ���� ��� ������� ������������� ����� �������� (� ������ � ��������������� �������)
  -- ����� ��������� ������ ��������� �������� 
  -- ���������� ������ ���� ��� �����, ���� 4 ��������� ��������� � ����������� �� ���� ��� ����� �����
  -- ��������: ����������� ����������
  FUNCTION find_or_create_imp_file(
    p_file_nm     IN mantas.rf_imp_file.file_nm%TYPE     -- ��� ����� (NULL ���� ���� �� �� �����)
   ,p_data_dt     IN mantas.rf_imp_file.data_dt%TYPE     -- ����, �� ������� ����������� ������.
   ,p_dpt_cd      IN mantas.rf_imp_file.dpt_cd%TYPE      -- ��� �������������, � �������� ��������� ����
   ,p_sys_cd      IN mantas.rf_imp_file.sys_cd%TYPE      -- ��� �������-��������� �����
   ,p_order_nb    IN mantas.rf_imp_file.order_nb%TYPE    -- ���������� ����� ����� ����� ������ �� ��������� ����/�������������/�������-���������
   ,p_archive_nm  IN mantas.rf_imp_file.archive_nm%TYPE  -- ��� ��������� �����, �� �������� ������� ������ ����
   ,p_search_type IN VARCHAR2                            -- ��� ������: 'N'- �� ����� �����, 'P' �� �������������� ����������
  ) RETURN mantas.rf_imp_file.imp_file_id%TYPE IS  -- ������������� ��������� ��� ��������� �����
    PRAGMA AUTONOMOUS_TRANSACTION;
      l_lockhandle  VARCHAR2(128);
      l_file_id     mantas.rf_imp_file.imp_file_id%TYPE;
    BEGIN
      -- ��������� ������� ���������
      validate_session;
      IF p_file_nm IS NULL THEN
         raise_application_error(-20000,'���������� ������: ������ ������ �������� ������ ��� �����');
      END IF;   
      IF p_search_type NOT IN ('N','P')  THEN 
         raise_application_error(-20000,'���������� ������: ������ ������ �������� ������������ ��� ������');
      END IF;     
      -- ������������� ����������
      dbms_lock.allocate_unique('AML_FIND_OR_CREATE_IMP_FILE_LOCK',l_lockhandle);
      IF dbms_lock.request(l_lockhandle, release_on_commit => TRUE)!=0 THEN
        raise_application_error(-20000,'���������� ������: �� ���������� ��������� ���������� AML_FIND_OR_CREATE_IMP_FILE_LOCK');
      END IF;
      -- ���� ��� ��������� ����
      SELECT MIN(s.imp_file_id) 
        INTO l_file_id
        FROM ( 
          SELECT f1.imp_file_id AS imp_file_id  -- ����� �� ����� �����
            FROM rf_imp_file f1
            WHERE p_search_type='N' AND f1.imp_type_cd=g_session_imp_type_cd
              AND f1.file_nm=p_file_nm 
          UNION ALL  
          SELECT f2.imp_file_id -- ����� �� ���������� �����
            FROM rf_imp_file f2
              WHERE p_search_type='P'  AND f2.imp_type_cd=g_session_imp_type_cd 
                AND NVL(f2.data_dt, TO_DATE('01.01.9999', 'dd.mm.yyyy'))=NVL(p_data_dt, TO_DATE('01.01.9999', 'dd.mm.yyyy'))
                AND NVL(f2.dpt_cd,'?%#')=NVL(p_dpt_cd,'?%#') 
                AND NVL(f2.sys_cd,'?%#')=NVL(p_sys_cd,'?%#')
                AND NVL(f2.order_nb,'?%#')=NVL(p_order_nb,'?%#')
            ) s;
      -- ���� �� �����, �� ������� ����� ������
      IF l_file_id IS NULL THEN
        INSERT INTO rf_imp_file (imp_file_id, imp_type_cd, file_nm, data_dt, dpt_cd, sys_cd, order_nb, archive_nm) 
          VALUES (rf_imp_file_seq.NEXTVAL, g_session_imp_type_cd,p_file_nm, p_data_dt, p_dpt_cd, p_sys_cd, p_order_nb, p_archive_nm)
          RETURNING imp_file_id INTO l_file_id; 
      END IF;   
      -- ��������� ��������� ��� ��� ���������� ����������
      COMMIT;
      RETURN l_file_id;
    EXCEPTION
      WHEN OTHERS THEN
        -- ���� ��� �� ���������, �� ���������� ���������, ��� �� ����� ����������
        ROLLBACK;
        RAISE;
    END find_or_create_imp_file;
   
  -- �������� �� ID ����� �������, ������ ��� ��������� ��������
  FUNCTION get_last_load_status(
    p_imp_file_id     IN mantas.rf_imp_file.imp_file_id%TYPE  -- ������������� ��������� �����
  ) RETURN mantas.rf_imp_history.load_status%TYPE IS -- ������������ ��������: ������ ��������� �������� �����
      l_load_status mantas.rf_imp_history.load_status%TYPE;
    BEGIN
      SELECT MAX(h.load_status) INTO l_load_status FROM rf_imp_history h WHERE h.imp_file_id=p_imp_file_id AND h.last_load_flag=1;
      RETURN l_load_status;
    END get_last_load_status;

  -- ��������� ����������� �� ��� ������� ��������� ������� ��� �������� (�� ������) ���� ������� � ���� ������� ���������
  FUNCTION check_sys_filter(
    p_sys_cd      IN mantas.rf_imp_file.order_nb%TYPE    -- ��� �������-��������� 
  ) RETURN BOOLEAN IS -- ������������ ��������: TRUE - ��� ���� ������ �������� (�������� ����� �������), FALSE - ��� �� ������ ��������
    l_filter    VARCHAR2(4000 CHAR);
    l_check_res NUMBER;
  BEGIN 
    validate_session;
    -- �������� ������
    IF g_session_filter_sys IS NOT NULL THEN
      l_check_res:=rf_pkg_scnro.check_in_list(NVL(p_sys_cd,'#UNKNOWN#'), g_session_filter_sys);    
      IF l_check_res IS NULL OR l_check_res=0 THEN
        RETURN FALSE;
      END IF;  
    END IF;
    
    RETURN TRUE;
  END check_sys_filter;

  -- ��������� ����������� �� ��� ���� ������� ��� �������� (�� ������) ���� ������� � ���� ������� ���������
  FUNCTION check_opok_filter(
    p_sys_cd      IN mantas.rf_imp_file.order_nb%TYPE    -- ��� �������-��������� 
   ,p_opok        IN VARCHAR2                            -- ��� ���� ��� ��������
  ) RETURN BOOLEAN IS -- ������������ ��������: TRUE - ��� ���� ������ �������� (�������� ����� �������), FALSE - ��� �� ������ ��������
    l_filter    VARCHAR2(4000 CHAR);
    l_check_res NUMBER;
  BEGIN 
    validate_session;
    -- �������� ������
    IF g_session_filters.EXISTS(NVL(p_sys_cd,'#UNKNOWN#')) THEN
      l_filter:=  g_session_filters(NVL(p_sys_cd,'#UNKNOWN#'));
    END IF;
    IF g_session_filters.EXISTS('*') THEN
      l_filter:= LTRIM(l_filter||','||g_session_filters('*'),',');
    END IF;
    -- �������� ������
    IF l_filter IS NOT NULL THEN
      l_check_res:=rf_pkg_scnro.check_in_list(p_opok, l_filter);    
      IF l_check_res IS NULL OR l_check_res=0 THEN
        RETURN FALSE;
      END IF;  
    END IF;
    
    RETURN TRUE;
  END check_opok_filter;
    
  -- ������� ������ � ������� �������� ����� (�������� ������ � ��������� ������)
  PROCEDURE log_imp_history(
    p_record_type       IN mantas.rf_imp_history.record_type%TYPE   -- ��� ������ � �������
   ,p_imp_file_id       IN mantas.rf_imp_file.imp_file_id%TYPE      -- ������������� ��������� ��� ��������� �����
   ,p_load_status       IN mantas.rf_imp_history.load_status%TYPE   -- ������ ��������
   ,p_err_msg_tx        IN mantas.rf_imp_history.err_msg_tx%TYPE    -- ����� ��������� �� ������/��������������. ���� �������� ��������, �� ������ ���� ������
   ,p_load_qty          IN mantas.rf_imp_history.load_qty%TYPE      -- ���������� ������� ����������� �������
   ,p_err_qty           IN mantas.rf_imp_history.err_qty%TYPE       -- ���������� ����������� (�� �����������) �������
   ,p_file_nm           IN mantas.rf_imp_history.file_nm%TYPE       -- ��� �������������� �����
   ,p_file_size         IN mantas.rf_imp_history.file_size%TYPE     -- ������ �������������� ����� � ������
   ,p_note_tx           IN mantas.rf_imp_history.note_tx%TYPE       -- ����������� � ��������
   ,p_content_data      IN BLOB DEFAULT NULL                        -- ���������� ������������ �����
  ) IS
      l_last_load_flag mantas.rf_imp_history.last_load_flag%TYPE;
      l_imp_seq_id     mantas.rf_imp_history.imp_seq_id%TYPE;
      l_load_dt        mantas.rf_imp_history.load_dt%TYPE; 
    BEGIN
      validate_session;
      -- ��������� ������������ ���������
      IF p_load_status NOT IN ('O','W','E')  THEN 
         raise_application_error(-20000,'���������� ������: ���������� ����������� ������� �������� ����� ������� �� ���������� ������ ');
      END IF;     
      IF p_record_type NOT IN ('M','R')  THEN 
         raise_application_error(-20000,'���������� ������: ���������� ����������� �������� ����� ������� �� ���������� ��� ������ �������');
      END IF;     
      -- ���������� ������ 
      BEGIN
        SAVEPOINT log_imp_history;
        -- ���� ������ ���� R ������� ������ �������� last_load_flag
        IF p_record_type='R' THEN
          l_last_load_flag:=1;
          UPDATE rf_imp_history h SET h.last_load_flag=NULL WHERE h.imp_file_id=p_imp_file_id AND l_last_load_flag=1;
        END IF;

        -- ��������� ������
       INSERT INTO rf_imp_history (imp_seq_id,imp_type_cd,last_load_flag,load_dt,load_status,err_msg_tx,load_qty
                                  ,err_qty,actual_dt,file_nm,file_size,note_tx,loaded_by,imp_file_id,imp_session_id,record_type)
        VALUES (rf_imp_history_seq.NEXTVAL,g_session_imp_type_cd,l_last_load_flag,SYSDATE,p_load_status,p_err_msg_tx,p_load_qty
               ,p_err_qty,g_session_actual_dt,p_file_nm,p_file_size,p_note_tx,g_session_user,p_imp_file_id,g_imp_session_id,p_record_type)
        RETURNING imp_seq_id,load_dt INTO l_imp_seq_id,l_load_dt;

       -- ���� �������� ����������, �� ������� � ���
       IF p_content_data IS NOT NULL THEN 
         INSERT INTO rf_imp_file_image (imp_seq_id,load_dt,content_data)
           VALUES (l_imp_seq_id,l_load_dt,p_content_data);
       END IF; 
       
       EXCEPTION WHEN OTHERS THEN
         ROLLBACK TO SAVEPOINT log_imp_history;
         RAISE;
       END;        
    END log_imp_history;

  -- ���������� ������������� ���� �������� ��� ���� ��� �� ���� ���� �� ������� ��������� ������� ������������ 
  FUNCTION lock_imp_file( 
    p_imp_file_id       IN mantas.rf_imp_file.imp_file_id%TYPE      -- ������������� ��������� ��� ��������� �����
  ) RETURN BOOLEAN IS  -- ������ ���������� ����� (TRUE - �������, FALSE - ���� ������������ ������ �������������)
     l_rowid  ROWID;
     l_dummy  VARCHAR2(1);
     l_return BOOLEAN;
    BEGIN
      validate_session;
      -- ��������� �� ������� �����
      BEGIN 
        SELECT rowid INTO l_rowid FROM rf_imp_file f WHERE f.imp_file_id=p_imp_file_id; 
      EXCEPTION WHEN NO_DATA_FOUND THEN   
        raise_application_error(-20000,'���������� ������: ������� ������������� �������������� ���� �������');
      END;
      --��������� ����
      BEGIN 
        l_return:=TRUE;
        SELECT 'x' INTO l_dummy FROM rf_imp_file f WHERE f.rowid=l_rowid FOR UPDATE SKIP LOCKED;
      EXCEPTION WHEN NO_DATA_FOUND THEN   
        l_return:=FALSE;
      END; 
      RETURN l_return;
    END lock_imp_file;    

  -- �������� ���� ��� �������� (�������� ������������� ���������� ��� ���� ���� �������)
  FUNCTION add_file(
    p_file_nm       IN mantas.rf_imp_file.file_nm%TYPE      -- ��� �����
   ,p_archive_nm    IN mantas.rf_imp_file.archive_nm%TYPE   -- ��� ������ (���� ���� ��������� - NULL)
   ,p_file          IN BLOB                                 -- ������ �����
   ,p_message       OUT VARCHAR2                            -- ��������� �� ������
  ) RETURN NUMBER IS  -- ������ ��������� (1 �������, ����� 0)  
    BEGIN
      validate_session;
      -- ��������� ���� � ������ ����������� ������
      g_session_files.append(p_file_nm);
      CASE g_session_imp_type_cd
        WHEN 'SBRF_321XML' THEN
          RETURN rf_pkg_import_oes.add_321xml_file(p_file_nm,p_archive_nm,p_file,p_message);
        WHEN 'SBRF_321XML_8001' then
          RETURN rf_pkg_import_oes.add_321xml_8001_file(p_file_nm,p_archive_nm,p_file,p_message);
        ELSE 
          p_message:='������! ������������ ��� ���� �������!';
          RETURN 0;
      END CASE;
    END add_file;
    
  -- ���������/���������� ��� ����� (�������� ������������� ���������� ��� ���� ���� �������)
  FUNCTION process_files(
    p_message      OUT VARCHAR2 -- ��������� �������� ��������� (������� ����� ���������� ������, �� ��� � �������)
  ) RETURN NUMBER IS  -- ������ ��������� (1 �������, ����� 0)  
      l_audit_id rf_audit_event.audit_seq_id%TYPE;
      l_aud_json rf_json;
      l_ret_code NUMBER;
    BEGIN
      validate_session;
      -- ������� ������ ������
      l_aud_json:=rf_json();
      l_aud_json.put('imp_session_id',g_imp_session_id);
      l_aud_json.put('actual_dt',TO_CHAR(g_session_actual_dt,'YYYY-MM-DD HH24:MI:SS'));
      l_aud_json.put('extra_params_tx',rf_json(g_session_extra_params_tx));
      l_aud_json.put('note_tx',g_session_note_tx);
      l_aud_json.put('files',g_session_files);
      l_audit_id:=rf_pkg_audit.record_event
          ( par_user_login => g_session_user
           ,par_ip_address => g_session_ip_address
           ,par_event_type_cd => 'GUI_IMP_'||g_session_imp_type_cd
           ,par_event_params  => l_aud_json);
      -- �������� ��� ���������     
      CASE g_session_imp_type_cd
        WHEN 'SBRF_321XML' THEN
          l_ret_code:=rf_pkg_import_oes.process_321xml_files(p_message);
        WHEN 'SBRF_321XML_8001' THEN
          l_ret_code:=rf_pkg_import_oes.process_321xml_8001_files(p_message);
        ELSE 
          p_message:='������! ������������ ��� ���� �������!';
          l_ret_code:=0;
      END CASE;
      -- ���������� c����� ��������� � �����
      rf_pkg_audit.set_event_result(l_audit_id,CASE l_ret_code WHEN 1 THEN NULL ELSE 'T' END,p_message);

      RETURN l_ret_code;
    END process_files;
  
end;
/
