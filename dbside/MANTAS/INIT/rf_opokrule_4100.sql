UPDATE mantas.rf_opok_rule
   SET vers_status_cd = 'ARC'
 WHERE opok_nb between 4100 and 4199 and
       vers_status_cd <> 'ARC' and
       rule_seq_id >= 1000;
        
MERGE INTO mantas.rf_opok_criteria t USING
(select crit_cd, crit_tp_cd, crit_nm, crit_tx, subtle_crit_tx, null_fl, test_crit_tx, test_subtle_crit_tx, test_null_fl, crit_use_cd, active_fl, note_tx from mantas.rf_opok_criteria where 1 <> 1 union all 
 select '����_���_���������', 1, '��������� ����� ��� (��������� �����������, ����������� ���������������� ���������� ������)', '40506,40606,40706,40825', null, 'N', null, null, 'N', '���������', 'Y', 
        '��������! � ������ ���������� ������� ������ � ���� ������ ���������� ����� �������� ���������� ������� BUSINESS.ACCT.RF_GOZ_FIRST_DT_I � ����������� ������ BUSINESS.RF_ACCT_GOZ_FIRST_DT_I. '||
        '��� ����� ���� 4102, 4103 �� ����� ���������� � ������ ������!' 
   from dual union all
 select 'SQL_4101', 4, '�������: ���������� �� ��������� ���� ��� � ����� �����, ����������� ��������', 
q'{nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_orig_acct_nb, 1, 5), 
                                   case when wt.rf_orig_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.send_instn_id
                                   end), 0) <> 1 and
nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_benef_acct_nb, 1, 5), 
                                   case when wt.rf_benef_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.rcv_instn_id
                                   end), 0) = 1}', null, null, null, null, null, '���������', 'Y', null 
   from dual union all
 select 'SQL_4102', 4, '�������: ������ ���������� �� ��������� ���� ��� � ������� ���������� ����� ���, ����������� ��������', 
q'{nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_orig_acct_nb, 1, 5), 
                                   case when wt.rf_orig_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.send_instn_id
                                   end), 0) = 1 and
nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_benef_acct_nb, 1, 5), 
                                   case when wt.rf_benef_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.rcv_instn_id
                                   end), 0) = 1 and
wt.rf_orig_acct_nb <> wt.rf_benef_acct_nb and
wt.trxn_exctn_dt+0 = (select rf_goz_first_dt from business.acct where acct_seq_id = wt.rf_benef_acct_seq_id)}', null, null, null, null, null, '���������', 'Y', null 
   from dual union all
 select 'SQL_4103', 4, '�������: �������� ���������� �� ��������� ���� ��� � ������� ���������� ����� ���, ����������� ��������', 
q'{nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_orig_acct_nb, 1, 5), 
                                   case when wt.rf_orig_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.send_instn_id
                                   end), 0) = 1 and
nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_benef_acct_nb, 1, 5), 
                                   case when wt.rf_benef_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.rcv_instn_id
                                   end), 0) = 1 and
wt.rf_orig_acct_nb <> wt.rf_benef_acct_nb and
wt.trxn_exctn_dt+0 <> nvl((select rf_goz_first_dt from business.acct where acct_seq_id = wt.rf_benef_acct_seq_id), wt.trxn_exctn_dt+1)}', null, null, null, null, null, '���������', 'Y', null 
   from dual union all
 select 'SQL_4104_4113', 4, '�������: �������� � ���������� ����� ��� �� ���� ���� � ��������� ����� (������), ����������� ��������', 
q'{nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_orig_acct_nb, 1, 5), 
                                   case when wt.rf_orig_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.send_instn_id
                                   end), 0) = 1 and
nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_benef_acct_nb, 1, 5), 
                                   case when wt.rf_benef_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.rcv_instn_id
                                   end), 0) <> 1 and 
wt.rf_orig_goz_op_cd in (#PARAM_CHARLIST#)}', null, null, null, null, null, '���������', 'Y', null 
   from dual union all
 select 'SQL_4114', 4, '�������: �������� � ���������� ����� ��� �� ���� ����, ����������� ��������', 
q'{nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_orig_acct_nb, 1, 5), 
                                   case when wt.rf_orig_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.send_instn_id
                                   end), 0) = 1 and
nvl(mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_benef_acct_nb, 1, 5), 
                                   case when wt.rf_benef_acct_seq_id <> -1
                                        then 'SBRF'
                                        else wt.rcv_instn_id
                                   end), 0) <> 1}', null, null, null, null, null, '���������', 'Y', null 
   from dual union all
 select 'SQL_4104_4114_EXPL', 5, '��������� ��� �������� � ���������� ����� ��� �� ���� ����, ����������� ��������', 
q'{'���� ����������� - ��������� ���� ��� � �� ��, ��� �������� ���: '||wt.rf_orig_goz_op_cd||
(select ' '||substr(nvl(code_val2_nm, code_desc_tx), 1, 100) from business.ref_table_detail where code_set_id = 'RF_GOZ_OP_TYPE_DBT' and code_val1_nm = wt.rf_orig_goz_op_cd)}', 
        null, null, null, null, null, '���������', 'Y', null 
   from dual
 ) v
ON (v.crit_cd = t.crit_cd)          
WHEN NOT MATCHED THEN
  insert (crit_cd, crit_tp_cd, crit_nm, crit_tx, subtle_crit_tx, null_fl, test_crit_tx, test_subtle_crit_tx, test_null_fl, crit_use_cd, active_fl, note_tx)
  values (v.crit_cd, v.crit_tp_cd, v.crit_nm, v.crit_tx, v.subtle_crit_tx, v.null_fl, v.test_crit_tx, v.test_subtle_crit_tx, v.test_null_fl, v.crit_use_cd, v.active_fl, v.note_tx)
WHEN MATCHED THEN
  update
     set crit_tp_cd = v.crit_tp_cd, 
         crit_nm = v.crit_nm, 
         crit_tx = v.crit_tx, 
         subtle_crit_tx = v.subtle_crit_tx, 
         null_fl = v.null_fl, 
         test_crit_tx = v.test_crit_tx, 
         test_subtle_crit_tx = v.test_subtle_crit_tx, 
         test_null_fl = v.test_null_fl, 
         crit_use_cd = v.crit_use_cd, 
         active_fl = v.active_fl, 
         note_tx = v.note_tx;

MERGE INTO mantas.rf_opok_rule t USING
(select rule_seq_id, rule_cd, vers_number, opok_nb, query_cd, priority_nb, tb_id, op_cat_cd, dbt_cdt_cd, kgrko_party_cd,
        acct_crit_cd, acct_add_crit_tx, acct_subtle_crit_tx, acct_area_tx, acct_null_fl, 
        acct2_crit_cd, acct2_add_crit_tx, acct2_subtle_crit_tx, acct2_area_tx, acct2_null_fl, acct_reverse_fl, 
        lexeme_crit_cd, lexeme_area_tx, lexeme_add_purpose_tx, lexeme_add_cust_tx, lexeme_add_cust2_tx, 
        lexeme2_crit_cd, lexeme2_area_tx, lexeme2_add_purpose_tx, lexeme2_add_cust_tx, lexeme2_add_cust2_tx, 
        cust_watch_list_cd, cust_not_watch_list_fl, cust2_watch_list_cd, cust2_not_watch_list_fl, 
        optp_crit_cd, optp_add_crit_tx, src_crit_tx, 
        custom_crit_cd, custom_crit_sql_tx, custom_crit_param_tx, explanation_crit_cd, explanation_sql_tx, 
        start_dt, end_dt, vers_status_cd, note_tx
   from mantas.rf_opok_rule where 1 <> 1 union all         
 select 530, 530, 1, 4101, '�����������', 1, 0, 'WT', null, 2 /*� ������� ����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4101', null, null, null, '''���� ���������� - ��������� ���� ��� � �� ��''', 
        null, null, 'ACT', '��������� �� ������ ����� ���������� � �� ��'
   from dual union all
 select 540, 540, 1, 4102, '�����������', 1, 0, 'WT', null, 2 /*� ������� ����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4102', null, null, null, '''����� ����������� � ���������� - ��������� ����� ��� � �� ��, ������ ���������� �� ���� ���������� � ������� ���������� ����� ��� � �� ��''', 
        null, null, 'ACT', '��������� �� ������� ������ � ���� ������� ���������� �� ��������� ���� ��� � �� �� � ������� ���������� ����� ��� � �� ��'
   from dual union all
 select 541, 541, 1, 4103, '�����������', 1, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4102', null, null, null, '''����� ����������� � ���������� - ��������� ����� ��� � �� ��, �������� �� ����� �����������''', 
        null, null, 'ACT', '��������� �� ������� ������ � ���� ������� ���������� �� ��������� ���� ��� � �� �� � ������� ���������� ����� ��� � �� ��, �������� �� ����� �����������'
   from dual union all
 select 550, 550, 1, 4103, '�����������', 1, 0, 'WT', null, 0 /*� ����� ��������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4103', null, null, null, '''����� ����������� � ���������� - ��������� ����� ��� � �� ��, ������ � ����������� ���������� �� ���� ���������� � ������� ���������� ����� ��� � �� �� ��� �������� �� ����� �����������''', 
        null, null, 'ACT', '��������� �� ������� ������, � ������ ���� ������� ���������� �� ��������� ���� ��� � �� �� � ������� ���������� ����� ��� � �� ��'
   from dual union all
 select 560, 560, 1, 4104, '�����������', 20, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '1', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all       
 select 561, 561, 1, 4105, '�����������', 60, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '2', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 562, 562, 1, 4106, '�����������', 90, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '4', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 563, 563, 1, 4107, '�����������', 100, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '11', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 564, 564, 1, 4108, '�����������', 100, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '9', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 565, 565, 1, 4109, '�����������', 100, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '10', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 566, 566, 1, 4110, '�����������', 100, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '8', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 567, 567, 1, 4111, '�����������', 30, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '5', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 568, 568, 1, 4112, '�����������', 70, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '6', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 569, 569, 1, 4113, '�����������', 50, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '7', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���'
   from dual union all
 select 570, 570, 1, 4114, '�����������', 999, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4114', null, null, 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� ��, ������� ����������� ����� ����� 4104-4113 ��� ������ ����� ���� �������� ���'
   from dual union all   
 select 571, 571, 1, 4114, '�����������', 10, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '3', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���, ������� ��������� ��� ����������� - ��������� ��������� � ������������ � �������� ������� ���� �������� ���'
   from dual union all
 select 572, 572, 1, 4114, '�����������', 40, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '99', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���, ������� ��������� ��� ����������� - ��������� ��������� � ������������ � �������� ������� ���� �������� ���'
   from dual union all
 select 573, 573, 1, 4114, '�����������', 80, 0, 'WT', null, 1 /*� ������� �����������*/, 
        null, null, null, null, null, 
        null, null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, null, 
        null, null, null, null, 
        null, null, null, 
        'SQL_4104_4113', null, '14', 'SQL_4104_4114_EXPL', null, 
        null, null, 'ACT', '��������� �� ������ ����� ����������� � �� �� � ���� ���� �������� ���, ������� ��������� ��� ����������� - ��������� ��������� � ������������ � �������� ������� ���� �������� ���'
   from dual   
   ) v
ON (t.rule_seq_id = v.rule_seq_id)   
WHEN NOT MATCHED THEN
  insert (rule_seq_id, rule_cd, vers_number, opok_nb, query_cd, priority_nb, tb_id, op_cat_cd, dbt_cdt_cd, kgrko_party_cd,
          acct_crit_cd, acct_add_crit_tx, acct_subtle_crit_tx, acct_area_tx, acct_null_fl, 
          acct2_crit_cd, acct2_add_crit_tx, acct2_subtle_crit_tx, acct2_area_tx, acct2_null_fl, acct_reverse_fl, 
          lexeme_crit_cd, lexeme_area_tx, lexeme_add_purpose_tx, lexeme_add_cust_tx, lexeme_add_cust2_tx, 
          lexeme2_crit_cd, lexeme2_area_tx, lexeme2_add_purpose_tx, lexeme2_add_cust_tx, lexeme2_add_cust2_tx, 
          cust_watch_list_cd, cust_not_watch_list_fl, cust2_watch_list_cd, cust2_not_watch_list_fl, 
          optp_crit_cd, optp_add_crit_tx, src_crit_tx, 
          custom_crit_cd, custom_crit_sql_tx, custom_crit_param_tx, explanation_crit_cd, explanation_sql_tx, 
          start_dt, end_dt, vers_status_cd, note_tx)
  values (v.rule_seq_id, v.rule_cd, v.vers_number, v.opok_nb, v.query_cd, v.priority_nb, v.tb_id, v.op_cat_cd, v.dbt_cdt_cd, v.kgrko_party_cd,
          v.acct_crit_cd, v.acct_add_crit_tx, v.acct_subtle_crit_tx, v.acct_area_tx, v.acct_null_fl, 
          v.acct2_crit_cd, v.acct2_add_crit_tx, v.acct2_subtle_crit_tx, v.acct2_area_tx, v.acct2_null_fl, v.acct_reverse_fl, 
          v.lexeme_crit_cd, v.lexeme_area_tx, v.lexeme_add_purpose_tx, v.lexeme_add_cust_tx, v.lexeme_add_cust2_tx, 
          v.lexeme2_crit_cd, v.lexeme2_area_tx, v.lexeme2_add_purpose_tx, v.lexeme2_add_cust_tx, v.lexeme2_add_cust2_tx, 
          v.cust_watch_list_cd, v.cust_not_watch_list_fl, v.cust2_watch_list_cd, v.cust2_not_watch_list_fl, 
          v.optp_crit_cd, v.optp_add_crit_tx, v.src_crit_tx, 
          v.custom_crit_cd, v.custom_crit_sql_tx, v.custom_crit_param_tx, v.explanation_crit_cd, v.explanation_sql_tx, 
          v.start_dt, v.end_dt, v.vers_status_cd, v.note_tx)
WHEN MATCHED THEN
  update
     set rule_cd = v.rule_cd, 
         vers_number = v.vers_number, 
         opok_nb = v.opok_nb, 
         query_cd = v.query_cd, 
         priority_nb = v.priority_nb, 
         tb_id = v.tb_id, 
         op_cat_cd = v.op_cat_cd, 
         dbt_cdt_cd = v.dbt_cdt_cd, 
         kgrko_party_cd = v.kgrko_party_cd,
         acct_crit_cd = v.acct_crit_cd, 
         acct_add_crit_tx = v.acct_add_crit_tx, 
         acct_subtle_crit_tx = v.acct_subtle_crit_tx, 
         acct_area_tx = v.acct_area_tx, 
         acct_null_fl = v.acct_null_fl, 
         acct2_crit_cd = v.acct2_crit_cd, 
         acct2_add_crit_tx = v.acct2_add_crit_tx, 
         acct2_subtle_crit_tx = v.acct2_subtle_crit_tx, 
         acct2_area_tx = v.acct2_area_tx, 
         acct2_null_fl = v.acct2_null_fl, 
         acct_reverse_fl = v.acct_reverse_fl, 
         lexeme_crit_cd = v.lexeme_crit_cd, 
         lexeme_area_tx = v.lexeme_area_tx, 
         lexeme_add_purpose_tx = v.lexeme_add_purpose_tx, 
         lexeme_add_cust_tx = v.lexeme_add_cust_tx, 
         lexeme_add_cust2_tx = v.lexeme_add_cust2_tx, 
         lexeme2_crit_cd = v.lexeme2_crit_cd, 
         lexeme2_area_tx = v.lexeme2_area_tx, 
         lexeme2_add_purpose_tx = v.lexeme2_add_purpose_tx, 
         lexeme2_add_cust_tx = v.lexeme2_add_cust_tx, 
         lexeme2_add_cust2_tx = v.lexeme2_add_cust2_tx, 
         cust_watch_list_cd = v.cust_watch_list_cd, 
         cust_not_watch_list_fl = v.cust_not_watch_list_fl, 
         cust2_watch_list_cd = v.cust2_watch_list_cd, 
         cust2_not_watch_list_fl = v.cust2_not_watch_list_fl, 
         optp_crit_cd = v.optp_crit_cd, 
         optp_add_crit_tx = v.optp_add_crit_tx, 
         src_crit_tx = v.src_crit_tx, 
         custom_crit_cd = v.custom_crit_cd, 
         custom_crit_sql_tx = v.custom_crit_sql_tx, 
         custom_crit_param_tx = v.custom_crit_param_tx, 
         explanation_crit_cd = v.explanation_crit_cd, 
         explanation_sql_tx = v.explanation_sql_tx, 
         start_dt = v.start_dt, 
         end_dt = v.end_dt, 
         vers_status_cd = v.vers_status_cd, 
         note_tx = v.note_tx; 

COMMIT;
