MERGE INTO mantas.rf_opok t USING
(select opok_nb, opok_nm, priority_nb, order_nb, group_nb, oes_parties_tx, note from mantas.rf_opok where 1 <> 1 union all
 select 4101, '�������� �� ���������� �������� ������� �� ��������� ���� � ������ ����� �����', 4101, 4101, null, null, null from dual union all
 select 4102, '�������� �� ������� ���������� �������� ������� �� ��������� ���� � ����� ���������� �����', 4102, 4102, null, null, null from dual union all
 select 4103, '�������� �� ������� � ����������� ����������� �������� ������� �� ��������� ���� � ����� ���������� ����� ��� �� �������� �������� ������� � ���������� ����� �� ���� ��������� ����', 4103, 4103, null, null, null from dual union all
 select 4104, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ������ ������� � ������, ���������� ��������, ��������� ������� � ���������� ���� ���������� ���������, ���� ����������� ����������� ���������� ���������, ����������� ���� ������������� ������������ �����������', 4104, 4104, 4114, null, null from dual union all
 select 4105, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ������ �������� �� �������� �������, ���������� �����, �������� ����� �� ����� (�������), ���������� ���������������� �������������', 4105, 4105, 4114, null, null from dual union all
 select 4106, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ������������ ������� � �������, ������������� ��������� ��� ���������� ��������� � ��������������� ��� ���������, ����� ���������� ��������� � ������������� � �������������� ���� ���� ������-�������� ������ (���� ����������� �����, ��������� �����)', 4106, 4106, 4114, null, null from dual union all
 select 4107, '�������� �� �������� �������� ������� � ���������� ����� ��������� ����������� �� ����� ���� ���� � ����� ������������ �������� ������� ��� ��������� ���������� �� ���������������� ���������, ���� ����������� ������ ���������� ���������� �������� �������� ��������������� ���������� �����, � �������, ������������� � ��������������� ���������� � �� ����������� ������� �������, ����������� ���������� ��������������� ���������� � ������� ���� ��������� � �������, ������������� �������������� ���������� ��������� ��� ����������� ��������� (������������) ���� ���������������� ��������� ��� ���� ���������������� ���������, ������������ � ������������ �������� ������������', 4107, 4107, 4114, null, null from dual union all
 select 4108, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ������� � ����������� ������������, ����������� � �������� ��������� �� ���������������� ���������� ������ � �������� � ���������� � ������ �������������� ������', 4108, 4108, 4114, null, null from dual union all
 select 4109, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ������������ �������� �������, ������������ �� ���������� (�����������) ����� ���������� ���������������� ��������� (���������) � �������� ���� ���������������� ��������� (���������) ���������� �������� ������������ (������������) �� ���� ����������� ������� (�� ����������� �������, ����������� �� ��������� ������) �������� �� ������������ ������ ���������, �����, ����������, ��������������, ������������� �������, ������������ ��� ���������� ���������������� ���������� ������, ��� ������� ������������� �������� ������������ (������������) �������������� ����������� ��������, ��������� � ������������� ������ ������, ����� ���������� ���������������� ��������� (���������) � ������������� �������� ������������ (������������) � �������������� ���� ���� ������-�������� ������ (���� ����������� �����, ��������� �����)', 4109, 4109, 4114, null, null from dual union all
 select 4110, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� �������� ����� �������', 4110, 4110, 4114, null, null from dual union all
 select 4111, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ������ �����', 4111, 4111, 4114, null, null from dual union all
 select 4112, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ���������� ��������������� ���������', 4112, 4112, 4114, null, null from dual union all
 select 4113, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ���� � ����� ���������� �������� ������� �� �������', 4113, 4113, 4114, null, null from dual union all
 select 4114, '�������� �� �������� �������� ������� � ���������� ����� �� ����� ���� ����, �� ��������������� �������� �������� �� �������� �������� ������� �� ����� ���� ����, ��������������� ������ ���� �������� 4104 - 4113', 4114, 4114, 4114, null, null from dual) v
 ON (v.opok_nb = t.opok_nb)
 WHEN NOT MATCHED THEN
   INSERT (opok_nb, opok_nm, priority_nb, order_nb, group_nb, oes_parties_tx, note)
   VALUES (v.opok_nb, v.opok_nm, v.priority_nb, v.order_nb, v.group_nb, v.oes_parties_tx, v.note)
 WHEN MATCHED THEN          
   UPDATE
      SET opok_nm = v.opok_nm, 
          priority_nb = v.priority_nb, 
          order_nb = v.order_nb, 
          group_nb = v.group_nb, 
          oes_parties_tx = v.oes_parties_tx, 
          note = v.note;

DELETE FROM mantas.rf_opok_details WHERE opok_nb between 4101 and 4114;

INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4101, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4102, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4103, 50000000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4104, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4105, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4106, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4107, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4108, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4109, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4110, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4111, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4112, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4113, 600000, null, null);
INSERT INTO mantas.rf_opok_details (RECORD_SEQ_ID, OPOK_NB, LIMIT_AM, START_DT, END_DT)
       VALUES (mantas.rf_opok_details_seq.NextVal, 4114, 600000, null, null);
          
COMMIT;
