grant execute on std.pkg_log to mantas;

create or replace package mantas.rf_pkg_report IS
--
-- ������������� ��������� � ��������� ������
--
FUNCTION auto_report
(
  par_rep_build_moment       mantas.rf_report_auto.rep_build_moment%TYPE -- ������, ��� �������� ����� ��������� ������
) 
RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
end rf_pkg_report;
/
create or replace package body mantas.rf_pkg_report IS
--
-- ������������� ��������� � ��������� ������
--
FUNCTION auto_report
(
  par_rep_build_moment       mantas.rf_report_auto.rep_build_moment%TYPE -- ������, ��� �������� ����� ��������� ������
) 
RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
  TYPE c_cursor_type IS REF CURSOR;
  c_cursor              c_cursor_type;
  var_sql               CLOB;

  var_email_subject  VARCHAR2(32000); -- ���� ���������� ������
  var_email_body     CLOB; -- ���� ���������� ������

  TYPE TAB_CLOB IS TABLE OF CLOB;
  
  var_value          VARCHAR2(2000 CHAR);
  var_rep_params     mantas.rf_tab_key_value  := mantas.rf_tab_key_value();

  var_file_name      VARCHAR2(2000 CHAR);
  var_file_names     mantas.rf_tab_varchar := mantas.rf_tab_varchar();

  var_file_body      CLOB;
  var_file_bodies    TAB_CLOB := TAB_CLOB();
  
  var_content_types  mantas.rf_tab_varchar := mantas.rf_tab_varchar();
  
  var_attachs        mantas.rf_pkg_mail.t_attachments_tab;

  var_rep_after_action_sql   TAB_CLOB := TAB_CLOB();
  var_rep_after_names        mantas.rf_tab_varchar := mantas.rf_tab_varchar();

  var_err_flag       INTEGER := 0;
  var_count          INTEGER;
 
  prcname            VARCHAR2(64) := ' mantas.rf_pkg_report.auto_report';
BEGIN
  std.pkg_log.info('�������� � �������� ������ (REP_BUILD_MOMENT = '||par_rep_build_moment||') ...', prcname);
  --
  -- ���� �� ���������
  --
  FOR r IN (SELECT a.*,
                   mantas.rf_pkg_adm.get_emails_by_func(v_function_code) as emails_to
              FROM mantas.rf_report_auto a
             WHERE a.rep_build_moment = par_rep_build_moment) LOOP
    BEGIN
      --
      -- ���� � �������� ��� �� ������ ���������� - ��������� ��������
      --
      if r.emails_to is not null and r.emails_to.count > 0 Then
        null;
      else
        GOTO NEXT_EMAIL;  
      end if;
      --
      -- ��������� ���� ���������� ������
      --
      if r.email_subject_sql is not null Then
        std.pkg_log.info('���������� ���� ������ (V_FUNCTION_CODE = '||r.v_function_code||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
        BEGIN
          var_sql := 'SELECT ('||r.email_subject_sql||') FROM dual';
          OPEN c_cursor FOR var_sql;
          FETCH c_cursor INTO var_email_subject;
          CLOSE c_cursor;
        EXCEPTION
          WHEN OTHERS THEN 
            var_err_flag := 1;
            std.pkg_log.error(substr(to_clob('')||'������ ��� ����������� ���� ������ '||
                                     '(V_FUNCTION_CODE = '||r.v_function_code||', REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||  
                                     '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. '||
                                     '���������� ������: '||var_sql, 1, 2000), prcname);
            begin close c_cursor; exception when others then null; end; 
            GOTO NEXT_EMAIL;
        END;  
      end if;

      var_email_subject := coalesce(var_email_subject, 'AML - �������� '||r.v_function_code);
      --
      -- ��������� ����� ���������� ������
      --
      if r.email_body_sql is not null Then
        std.pkg_log.info('���������� ����� ������ (V_FUNCTION_CODE = '||r.v_function_code||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
        BEGIN
          var_sql := 'SELECT ('||r.email_body_sql||') FROM dual';
          OPEN c_cursor FOR var_sql;
          FETCH c_cursor INTO var_email_body;
          CLOSE c_cursor;
        EXCEPTION
          WHEN OTHERS THEN 
            var_err_flag := 1;
            std.pkg_log.error(substr(to_clob('')||'������ ��� ����������� ������ ������ '||
                                     '(V_FUNCTION_CODE = '||r.v_function_code||', REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||  
                                     '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. '||
                                     '���������� ������: '||var_sql, 1, 2000), prcname);
            begin close c_cursor; exception when others then null; end; 
            GOTO NEXT_EMAIL;
        END;  
      end if;

      var_email_body := coalesce(var_email_body, '��. ����������� � ������ ������');
      --
      -- ���� �� ������� � �������� - �� ������� ������ ������
      --
      var_file_names.delete;
      var_file_bodies.delete;
      var_rep_after_action_sql.delete;
      var_rep_after_names.delete;
      
      FOR r2 IN (SELECT i.*,
                        mantas.rf_pkg_apex_adapter.get_report_name(app_id, app_page_id) as rep_name
                   FROM mantas.rf_report_auto_items i
                  WHERE v_function_code = r.v_function_code
                 ORDER BY rep_no) LOOP
        BEGIN
          --
          -- ���� ���� ������� ���������� - ��������� ���
          --
          if r2.rep_build_condition_sql is not null Then
            std.pkg_log.info('��������� ������� ���������� ��� ������ "'||r2.rep_name||'" '||
                             '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
            BEGIN
              OPEN c_cursor FOR r2.rep_build_condition_sql;
              FETCH c_cursor INTO var_count;
              CLOSE c_cursor;
            EXCEPTION
              WHEN OTHERS THEN 
                var_err_flag := 1;
                std.pkg_log.error(substr(to_clob('')||'������ ��� ���������� ������� ���������� ��� ������ "'||r2.rep_name||'" '||
                                         '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||  
                                         '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. '||
                                         '����� �������: '||r2.rep_build_condition_sql, 1, 2000), prcname);
                begin close c_cursor; exception when others then null; end; 
                GOTO NEXT_REPORT;
            END;  
          
            if var_count > 0 Then
              std.pkg_log.info('������� ���������� ��������� ��� ������ "'||r2.rep_name||'" '||
                               '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
            else
              std.pkg_log.info('������� ���������� �� ��������� ��� ������ "'||r2.rep_name||'" '||
                               '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
              GOTO NEXT_REPORT;
            end if;    
          end if;
          --
          -- ��������� �������� ����������, �� ������� ����� ��������� �����
          --  
          var_rep_params.delete;

          if r2.rep_params is not null and r2.rep_params.count > 0 Then
            FOR i IN 1..r2.rep_params.COUNT LOOP
              std.pkg_log.info('���������� �������� ��������� "'||r2.rep_params(i).key||'" ��� ������ "'||r2.rep_name||'" '||
                               '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
              BEGIN
                var_sql := 'SELECT ('||r2.rep_params(i).value||') FROM dual';
                OPEN c_cursor FOR var_sql;
                FETCH c_cursor INTO var_value;
                CLOSE c_cursor;
              EXCEPTION
                WHEN OTHERS THEN 
                  var_err_flag := 1;
                  std.pkg_log.error(substr(to_clob('')||'������ ��� ����������� ������ ������ '||
                                           '(V_FUNCTION_CODE = '||r.v_function_code||', REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||  
                                           '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. '||
                                           '���������� ������: '||var_sql, 1, 2000), prcname);
                  begin close c_cursor; exception when others then null; end; 
                  GOTO NEXT_REPORT;
              END;              
                
              var_rep_params.extend;
              var_rep_params(var_rep_params.COUNT) := mantas.rf_type_key_value(r2.rep_params(i).key, var_value); 

              std.pkg_log.debug('�������� ��������� "'||r2.rep_params(i).key||'" ��� ������ "'||r2.rep_name||'" '||
                               '�����: '||nvl(var_value, '<null>')||
                               ' (V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
            END LOOP;
          end if;  
          --
          -- ��������� ��� ����� ��� ������
          --          
          if r2.rep_file_nm_sql is not null Then
            std.pkg_log.info('���������� ��� ����� ��� ������ "'||r2.rep_name||'" '||
                             '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
            BEGIN
              var_sql := 'SELECT ('||r2.rep_file_nm_sql||') FROM dual';
              OPEN c_cursor FOR var_sql;
              FETCH c_cursor INTO var_file_name;
              CLOSE c_cursor;
            EXCEPTION
              WHEN OTHERS THEN 
                var_err_flag := 1;
                std.pkg_log.error(substr(to_clob('')||'������ ��� ����������� ������������ ����� ��� ������ "'||r2.rep_name||'" '||
                                         '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||
                                         '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. '||
                                         '���������� ������: '||var_sql, 1, 2000), prcname);
                begin close c_cursor; exception when others then null; end; 
                GOTO NEXT_REPORT;
            END;  
          end if;

          var_file_name := coalesce(var_file_name, r2.rep_name||' ('||r2.v_function_code||'_'||to_char(r2.rep_no)||').html');
          --
          -- ����������, �������� �����
          --
          std.pkg_log.info('������� ����� "'||r2.rep_name||'" '||
                           '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
          var_file_body := mantas.rf_pkg_apex_adapter.get_report(
                             p_app_id          => r2.app_id,
                             p_app_page_id     => r2.app_page_id,
                             p_params          => var_rep_params,
                             p_v_function_code => r2.v_function_code,
                             p_rep_no          => r2.rep_no,
                             p_delete          => TRUE,
                             p_refine          => TRUE
                             );
          std.pkg_log.info('��������� ��������� ����� "'||r2.rep_name||'" '||
                           '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
          
          var_file_names.extend;
          var_file_names(var_file_names.COUNT) := var_file_name;
          
          var_file_bodies.extend;
          var_file_bodies(var_file_bodies.COUNT) := var_file_body;

          var_content_types.extend;
          var_content_types(var_content_types.COUNT) := r2.rep_content_type;
          --
          -- ���������� POST-ACTION ��� ���������� ����� �������� �������� ������
          --
          if r2.rep_after_action_sql is not null Then
            var_rep_after_action_sql.extend;
            var_rep_after_action_sql(var_rep_after_action_sql.COUNT) := r2.rep_after_action_sql;
            
            var_rep_after_names.extend;
            var_rep_after_names(var_rep_after_names.COUNT) := r2.rep_name;
          end if;
                
          <<NEXT_REPORT>>
          null;
        EXCEPTION
          WHEN OTHERS THEN
            var_err_flag := 1;
            if SQLCODE < -20000 Then
              null;
            else  
              std.pkg_log.error('������ ��� ���������� ������ "'||r2.rep_name||'" '||
                                '(V_FUNCTION_CODE = '||r2.v_function_code||', REP_NO = '||r2.rep_no||', REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||
                                '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
            end if;       
        END;    
      END LOOP; -- ���� ���������� ������� � ��������
      --
      -- ���� ���� ���� �� ���� ����� - ���������� ������ ���������
      --  
      if var_file_names is not null and var_file_names.count > 0 Then
 
        var_attachs.delete;
        FOR i IN 1..var_file_names.COUNT LOOP
          var_attachs(i).filename   := var_file_names(i);
          var_attachs(i).mime_type  := var_content_types(i);
          var_attachs(i).attachment := var_file_bodies(i);
        END LOOP;
        
        BEGIN
          std.pkg_log.info('���������� �������� '||r.v_function_code||' ��������� (REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);

          mantas.rf_pkg_mail.send(
            p_from        => r.email_from,
            p_to          => r.emails_to,
            p_subject     => var_email_subject,
            p_body        => var_email_body,
            p_attachments => var_attachs
            );

          std.pkg_log.info('��������� ���������� �������� '||r.v_function_code||' ��������� (REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);
        EXCEPTION
          WHEN OTHERS THEN 
            var_err_flag := 1;
            std.pkg_log.error('������ ��� �������� ������� �������� '||r.v_function_code||' ��������� (REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||
                              '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
            GOTO NEXT_EMAIL;
        END;          
        --
        -- ��������� POST-ACTION'� ��� ������� �� ������������ ��������
        --
        FOR i IN 1..var_rep_after_action_sql.COUNT  LOOP
          std.pkg_log.info('��������� �������� ����� �������� ��� ������ "'||var_rep_after_names(i)||'" '||
                           '(V_FUNCTION_CODE = '||r.v_function_code||', REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);  
          BEGIN
            EXECUTE IMMEDIATE var_rep_after_action_sql(i);          
          EXCEPTION
            WHEN OTHERS THEN 
              var_err_flag := 1;
              std.pkg_log.error(substr(to_clob('')||'������ ��� ���������� �������� ����� �������� ��� ������ "'||var_rep_after_names(i)||'". '||
                                         '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. '||
                                         '����� ��������: '||var_rep_after_action_sql(i), 1, 2000), prcname);
          END;                      
        END LOOP; 
      end if;
       
      <<NEXT_EMAIL>>
      null;
    EXCEPTION
      WHEN OTHERS THEN
        var_err_flag := 1;
        if SQLCODE < -20000 Then
          null;
        else  
          std.pkg_log.error('������ ��� �������� � �������� ������� �������� '||r.v_function_code||' (REP_BUILD_MOMENT = '||par_rep_build_moment||'). '||
                            '����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
        end if;       
    END;    
  END LOOP;

  std.pkg_log.info('��������� �������� � �������� ������� (REP_BUILD_MOMENT = '||par_rep_build_moment||')', prcname);
  COMMIT;
  
  return var_err_flag;
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� �������� � �������� ������� (REP_BUILD_MOMENT = '||par_rep_build_moment||'). ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    return 1;  
END auto_report;
end rf_pkg_report;
/
