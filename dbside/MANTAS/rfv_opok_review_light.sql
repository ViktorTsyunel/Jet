CREATE OR REPLACE FORCE VIEW mantas.rfv_opok_review_light as
SELECT rv.review_id             as review_id, 
       rv.status_cd             as rv_status_cd,
       rv.status_dt             as rv_status_dt,
       rv.creat_ts              as rv_creat_ts,
       rv.creat_id              as rv_creat_id,
       rv.due_dt                as rv_due_dt,
       rv.owner_seq_id          as rv_owner_seq_id, 
       rv.owner_org             as rv_owner_org,
       rv.orig_owner_seq_id     as rv_orig_owner_seq_id, 
       rv.rf_object_tp_cd       as rv_object_tp_cd, 
       rv.rf_op_cat_cd          as rv_op_cat_cd,
       rv.rf_trxn_seq_id        as rv_trxn_seq_id,
       rv.rf_trxn_scrty_seq_id  as rv_trxn_scrty_seq_id,
       rv.rf_repeat_nb          as rv_repeat_nb,
       rv.rf_data_dump_dt       as rv_data_dump_dt,
       rv.rf_branch_id          as rv_branch_id,
       trunc(rv.rf_branch_id / 100000)              as rv_tb_id, 
       rv.rf_branch_id - round(rv.rf_branch_id, -5) as rv_osb_id, 
       rv.rf_kgrko_party_cd     as rv_kgrko_party_cd,
       rv.last_actvy_type_cd    as rv_last_actvy_type_cd,
       rv.prcsng_batch_cmplt_fl as rv_prcsng_batch_cmplt_fl,
       rv.rf_cls_dt             as rv_cls_dt,
       rv.cls_id                as rv_cls_id,       
       rv.cls_actvy_type_cd     as rv_cls_actvy_type_cd,
       rv.cls_class_cd          as rv_cls_class_cd,
       rv.review_type_cd        as rv_review_type_cd, 
       rv.rf_wrk_flag           as rv_wrk_flag,
       rv.rf_trxn_dt            as rv_trxn_dt,
       rv.rf_trxn_base_am       as rv_trxn_base_am,
       rv.rf_partition_key      as rv_partition_key,
       rv.rf_subpartition_date  as rv_subpartition_date,
       rv.rf_opok_nb            as opok_nb, 
       rv.rf_add_opoks_tx       as add_opoks_tx
  FROM mantas.kdd_review rv
 WHERE rv.rf_alert_type_cd = 'O' /*������ ������ �� ������������� ��������*/;

GRANT SELECT on mantas.rfv_opok_review_light to KDD_ALGORITHM;
GRANT SELECT on mantas.rfv_opok_review_light to KDD_MINER;
GRANT SELECT on mantas.rfv_opok_review_light to MANTAS_LOADER;
GRANT SELECT on mantas.rfv_opok_review_light to MANTAS_READER;
GRANT SELECT on mantas.rfv_opok_review_light to RF_RSCHEMA_ROLE;
GRANT SELECT on mantas.rfv_opok_review_light to BUSINESS;
GRANT SELECT on mantas.rfv_opok_review_light to CMREVMAN with grant option;
GRANT SELECT on mantas.rfv_opok_review_light to KDD_MNR with grant option;
GRANT SELECT on mantas.rfv_opok_review_light to KDD_REPORT with grant option;
GRANT SELECT on mantas.rfv_opok_review_light to KYC;
GRANT SELECT on mantas.rfv_opok_review_light to STD;
GRANT SELECT on mantas.rfv_opok_review_light to KDD_ALG;

