grant select on business.trxn_seq to mantas;

create or replace package mantas.rf_pkg_request_forms is

  /* ����� ��������� ������� ��� ������� 
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json;

end;
/


create or replace package body mantas.rf_pkg_request_forms is

  /* ����� ��� ��������� ������� OES321*** update
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   *   p_table_name - ��� �������, � ������� ���������� ����������
   *   p_key_name - ��� �������� ���������� �����
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  function oes_update( 
    p_json in rf_json, 
    p_user in varchar2, 
    p_id_request in number, 
    p_table_name in varchar2, 
    p_key_name in varchar2 ) 
  return rf_json
  is
    v_json      rf_json;
    v_id        number;
  begin
    v_json := rf_json_ext.get_json( p_json, 'data[1]' );
    v_id := rf_json_ext.get_number( v_json, '_id_' );
    v_json.put('MODIFIED_DATE', to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS'));
    v_json.put('MODIFIED_BY', p_user);

    /* ��������� ���������� */
    v_json := rf_json( rf_pkg_request.exec_sql_update(
                         p_sql        => rf_pkg_request.update_generate(
                                           p_schema     => 'MANTAS',
                                           p_table_name => p_table_name,
                                           p_key_name   => p_key_name,
                                           p_data       => v_json
                                         ),
                         p_bind_param => v_id,
                         p_id_req     => p_id_request,
                         p_schema     => 'MANTAS',
                         p_table      => p_table_name,
                         p_id_name    => p_key_name,
                         p_id_type    => 'NUMBER'
                       )
                     );
    return v_json;
  end;
  
  
  /* ����� ��� ��������� ������� OES321*** create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   *   p_table_name - ��� �������, � ������� ���������� ����������
   *   p_key_name - ��� �������� ���������� �����
   *   p_seq_name - ��� ������������������ ��� �������� PK
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  function oes_create( 
    p_json in rf_json, 
    p_user in varchar2, 
    p_id_request in number, 
    p_table_name in varchar2, 
    p_key_name in varchar2, 
    p_seq_name in varchar2 ) 
  return rf_json
  is
    v_clob      clob;
    v_json_data rf_json;
    v_json      rf_json;
    v_sql       varchar2(32000);
  begin
    v_json_data := rf_json_ext.get_json( p_json, 'data[1]' );
    v_json_data.put('CREATED_DATE', to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS'));
    v_json_data.put('CREATED_BY', p_user);
  
    v_json := rf_json( rf_pkg_request.exec_sql_insert(
                         p_sql      => rf_pkg_request.insert_generate(
                                         p_schema      => 'MANTAS',
                                         p_table_name  => p_table_name,
                                         p_column_name => p_key_name,
                                         p_seq_name    => p_seq_name,
                                         p_data        => v_json_data ),
                         p_id_req   => p_id_request,
                         p_id       => rf_json_ext.get_number(v_json_data, '_id_'),
                         p_clientid => rf_json_ext.get_number(v_json_data, '_id_'),
                         p_schema   => 'MANTAS',
                         p_table    => p_table_name,
                         p_id_name  => p_key_name )
                     );

    /* ��������� � ������� json */
    return v_json;
  end;


  /* ����� ��� ��������� ������� OES321*** delete
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   *   p_table_name - ��� �������, � ������� ���������� ��������
   *   p_key_name - ��� �������� ���������� �����
   * ��������� ������
   *   rf_json - json �����
   */
  function oes_delete( 
    p_json in rf_json, 
    p_id_request in number,
    p_table_name in varchar2,
    p_key_name in varchar2) 
  return rf_json
  is
    v_sql varchar2(1024);
    v_json rf_json;
  begin
    FOR i IN 1..rf_json_ext.get_json_list(p_json,'data').count
      LOOP
        v_sql := 'delete from mantas.##TABLE## where ##KEY## = ##ID##';
        v_sql := replace( v_sql, '##TABLE##', p_table_name );     v_sql := replace( v_sql, '##KEY##', p_key_name );
        v_sql := replace( v_sql, '##ID##', rf_json_ext.get_number( rf_json_ext.get_json( p_json, 'data['||to_char(i)||']' ), '_id_' ) );
        v_json := rf_json( rf_pkg_request.exec_sql_delete( v_sql, p_id_request ) );
        IF NOT rf_json_ext.get_bool(rf_json, 'success')   
          THEN RETURN rf_json; -- ���� ������� �� ������ ��������� ��������� � �������
        END IF;
      END LOOP;
    return v_json;
  end;


  /* ����� ��� ��������� ������� OES321Details readByOesId
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_form_321_details( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(2048);
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select oes_321_id as "_id_",
             oes_321_id, review_id, oes_321_org_id, check_dt, send_dt, numb_p,
             date_p, action, regn, nd_ko, ktu_s, bik_s, numbf_s, branch, ktu_ss,
             bik_ss, numbf_ss, tel, data, date_s, terror, vo, dop_v, sume, sum, 
             curren, prim_1, prim_2, num_pay_d, date_pay_d, metal, priz6001, b_payer, 
             b_recip, part, descr_1, descr_2, curren_con, sum_con, priz_sd, manual_cmnt,
             send_fl, check_status, 
             send_fl oes_readonly_fl,
             refer_r2
        from mantas.rf_oes_321
       where oes_321_id = :OESID
    }';
    
    v_bind_json.put( 'OESID', rf_json_ext.get_number( p_json, 'oes321Id' ) );
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;
  
  
  /* ����� ��� ��������� ������� OES321Details readCheckStatus
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_form_321_check_status( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(2048);
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select oes_321_id as "_id_",
             oes_321_id, check_dt, check_status
        from mantas.rf_oes_321
       where oes_321_id = :OESID
    }';

    v_bind_json.put( 'OESID', rf_json_ext.get_number( p_json, 'oes321Id' ) );

    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;

  
  /* ����� ��� ��������� ������� OES321Details update
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function update_form_321_details( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_json            rf_json;
    v_sql             varchar2(32000);
    v_json_value      rf_json_value;
    v_vo              mantas.kdd_review.rf_opok_nb%type := null;
    v_dop_v           mantas.kdd_review.rf_add_opoks_tx%type := null;
    v_sume            mantas.kdd_review.rf_trxn_base_am%type := null;
    v_data            mantas.kdd_review.rf_trxn_dt%type := null;
    v_review_id       mantas.kdd_review.review_id%type;
    v_rf_object_tp_cd mantas.kdd_review.rf_object_tp_cd%type := 'TRXN';
    v_owner_seq_id    mantas.kdd_review_owner.owner_seq_id%type;
    v_ids             mantas.rf_tab_number := mantas.rf_tab_number();
    v_activity_ids    rf_pkg_review.tab_number;
    v_activity_id     number;
    v_check_oes       varchar2(4000);
    v_allow           NUMBER;
  begin
    /* ������ �������� VO ���� �� ���� */
    v_json_value := rf_json_ext.get_json_value( rf_json_ext.get_json( p_json, 'data[1]' ), 'VO' );
    if v_json_value is not null then
      v_vo := rf_json_ext.get_number( rf_json_ext.get_json( p_json, 'data[1]' ), 'VO' );
    end if;
    /* ������ �������� DOP_V ���� �� ���� */
    v_json_value := rf_json_ext.get_json_value( rf_json_ext.get_json( p_json, 'data[1]' ), 'DOP_V' );
    if v_json_value is not null then
      v_dop_v := rf_json_ext.get_string( rf_json_ext.get_json( p_json, 'data[1]' ), 'DOP_V' );
    end if;
    /* ������ �������� SUME ���� �� ���� */
    v_json_value := rf_json_ext.get_json_value( rf_json_ext.get_json( p_json, 'data[1]' ), 'SUME' );
    if v_json_value is not null then
      v_sume := rf_json_ext.get_number( rf_json_ext.get_json( p_json, 'data[1]' ), 'SUME' );
    end if;
    /* ������ �������� DATA ���� �� ���� */
    v_json_value := rf_json_ext.get_json_value( rf_json_ext.get_json( p_json, 'data[1]' ), 'DATA' );
    if v_json_value is not null then
      v_data := rf_json_ext.get_date( rf_json_ext.get_json( p_json, 'data[1]' ), 'DATA' );
    end if;

    /* ���������� review_id � ��� ��� */
    select review_id into v_review_id
       from mantas.rf_oes_321
       where oes_321_id = rf_json_ext.get_number( p_json, 'oes321Id' );

    -- �������� �� �����
    SELECT NVL(MAX(mantas.rf_pkg_adm.allow_alert_editing(p_user,kr.status_cd)),0)
      INTO v_allow
      FROM mantas.kdd_review kr 
      WHERE kr.review_id=v_review_id;
    IF  v_allow=0 THEN
      v_json := rf_json();
      v_json.put('success', false);
      v_json.put('message','��������� ������ ��� � ���� �������!');
      RETURN v_json;
    END IF;

    /* ���� ���� �� ���� �� ���������� �����, ��������� ������ */
    if v_vo is not null or v_dop_v is not null 
    or v_sume is not null or v_data is not null then
      
      /* ��� ���������� ��������� � ��������������� ����� �� ������������ �������� */
      if v_vo is not null or v_dop_v is not null then
        /* ���������� ID ������������ */
        select max(owner_seq_id) into v_owner_seq_id
          from mantas.kdd_review_owner
         where owner_id = p_user;
        if v_owner_seq_id is null then
          raise_application_error(-20001, '������ �������� ������� ������������: '||p_user);
        end if; 
        
        v_ids.extend(1);
        v_ids(1) := v_review_id;
  
        /* ������������ ���������� */
        v_activity_ids := mantas.rf_pkg_review.create_activities(
          p_review_ids    => v_ids, 
          p_actvy_type_cd => 'RF_SETOPOK', 
          p_cmmnt_id      => null,
          p_owner_seq_id  => v_owner_seq_id,
          p_note_tx       => '�� ��������� (���) ������������� ���������� ���(�) ��������: '||to_char(v_vo)||
                             case 
                               when v_dop_v is null then null 
                               else ', �������������� ����: '||v_dop_v 
                             end
        );
      end if;
      
      if v_sume is not null or v_data is not null then
        select rf_object_tp_cd into v_rf_object_tp_cd
          from mantas.kdd_review
         where review_id = v_review_id;
      end if;
      
      update mantas.kdd_review
         set rf_opok_nb = nvl2( v_vo, v_vo, rf_opok_nb ), 
             rf_add_opoks_tx = nullif( nvl2( v_dop_v, v_dop_v, rf_add_opoks_tx ), '0' ),
             /* ����� � ���� �������� ������ ��� ��������� ��� �������� */
             rf_trxn_base_am = case v_rf_object_tp_cd 
                                 /* 0 ��������������� � null */     
                                 when 'OES321' then nvl2( v_sume, nullif( v_sume, 0 ), rf_trxn_base_am )
                                 else rf_trxn_base_am
                               end,
             rf_trxn_dt = case v_rf_object_tp_cd 
                            /* 01.01.2099 ��������������� � null */
                            when 'OES321' then nvl2( v_data, nullif( v_data, to_date( '01.01.2099', 'DD.MM.YYYY' ) ), rf_trxn_dt )
                            else rf_trxn_dt
                          end
      where review_id = v_review_id;
    end if;
    
    /* ��������� ������ */
    v_json := oes_update( p_json, p_user, p_id_request, 'RF_OES_321', 'OES_321_ID' );
    commit;
    /* ��������� �������� */
    v_check_oes := mantas.rf_pkg_oes_check.check_oes( '321', rf_json_ext.get_number( p_json, 'oes321Id' ) );

    return v_json;
    
    exception
      when others then 
        v_json := rf_json();
        v_json.put('success', false);
        v_json.put('message', '�� ������� �������� ������! ��������� ���������� ������. '||sqlerrm());
        return v_json;
  end;
  
  
  /* ����� ��� ��������� ������� OES321Party read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_form_321_party( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(1024);
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select oes_321_id||'|'||block_nb as "_id_",
             oes_321_id, block_nb, tu, pr, nameu, kodcr, kodcn, kd, sd, rg, nd,
             vd1, vd2, vd3, vd4, vd5, vd6, vd7, mc1, mc2, mc3, gr, bp, vp, acc_b,
             acc_cor_b, amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, adress_s,
             adress_r, adress_g, adress_u, adress_d, adress_k, adress_o, name_b,
             kodcn_b, bik_b, card_b, name_is, bik_is, name_r, kodcn_r, bik_r, note,
             cust_seq_id
        from mantas.rf_oes_321_party
       where oes_321_id = :OESID
         and block_nb = :BLOCKNB
    }';

    v_bind_json.put( 'OESID', rf_json_ext.get_number( p_json, 'oes321Id' ) );
    v_bind_json.put( 'BLOCKNB', rf_json_ext.get_number( p_json, 'block_nb' ) );

    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;
  

  /* ����� ��� ��������� ������� OES321Party update
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function update_form_321_party( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_json rf_json;
    v_sql varchar2(32000);
    v_id varchar2(64);
    v_oes_321_id number;
    v_block_nb number;
    v_check_oes    varchar2(4000);
    v_allow           NUMBER;
  begin
    v_id := rf_json_ext.get_string( rf_json_ext.get_json( p_json, 'data[1]' ), '_id_' );
    v_oes_321_id := substr( v_id, 1, instr( v_id, '|' ) -1 ) ;
    v_block_nb := substr( v_id, instr( v_id, '|' )+1 );

    -- �������� �� �����
    SELECT NVL(MAX(mantas.rf_pkg_adm.allow_alert_editing(p_user,kr.status_cd)),0)
      INTO v_allow
      FROM mantas.rf_oes_321 o
        JOIN mantas.kdd_review kr ON kr.review_id=o.review_id 
      WHERE o.oes_321_id=v_oes_321_id;
    IF  v_allow=0 THEN
      v_json := rf_json();
      v_json.put('success', false);
      v_json.put('message','��������� ������ ��� � ���� �������!');
      RETURN v_json;
    END IF;


    /* �������� sql ������ ��� ���������� */
    v_sql := rf_pkg_request.update_generate(
      p_schema     => 'MANTAS',
      p_table_name => 'RF_OES_321_PARTY',
      p_key_name   => NULL,
      p_data       => rf_json_ext.get_json( p_json, 'data[1]' ),
      p_where      => 'where oes_321_id=:oes_id and block_nb=:block_nb'
    );

    /* ��������� ���������� */
    execute immediate v_sql using v_oes_321_id, v_block_nb;
    commit;
    /* ��������� �������� */
    v_check_oes := mantas.rf_pkg_oes_check.check_oes( '321', v_oes_321_id );
    
    /* ��������� � ������� json */
    v_json := rf_json();
    v_json.put('success', true);


    return v_json;
  end;


  /* ����� ��� ��������� ������� OESCheck read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_form_check( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(1024);
    v_bind_json rf_json := rf_json;
  begin
    v_sql := q'{
      select check_seq_id "_id_",
             check_seq_id, form_cd, oes_id, msg_tx, critical_fl, field_nm, field_cd, 
             field_order_nb, table_nm, column_nm, block_cd, rule_seq_id
        from mantas.rf_oes_check
       where form_cd = :FORMCD
         and oes_id = :OESID
       order by critical_fl desc, field_order_nb, check_seq_id
    }';
    
    v_bind_json.put( 'FORMCD', rf_json_ext.get_string( p_json, 'formCd' ) );
    v_bind_json.put( 'OESID', rf_json_ext.get_number( p_json, 'oesId' ) );
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;


  /* ����� ��� ��������� ������� OES321Org read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_form_321_org( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(2048);
  begin
    v_sql := q'{
      select oes_321_org_id "_id_", 
             oes_321_org_id, tb_code, tel, tb_name, regn, nd_ko, ktu_s, bik_s,
             numbf_s, ktu_ss, bik_ss, numbf_ss, active_fl, start_dt, end_dt, osb_code,
             created_by, created_date, modified_by, modified_date, branch_fl
        from mantas.rf_oes_321_org
        }';
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  end;


  /* ����� ��� ��������� ������� OES321PartyOrg read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_form_321_party_org( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(1024);
  begin
    v_sql := q'{
      select oes_321_p_org_id "_id_", 
             oes_321_p_org_id, tb_code, tu, pr, nameu, kodcr, kodcn, kd, sd, rg, nd, 
             vd1, vd2, vd3, vd4, vd5, vd6, vd7, mc1, mc2, mc3, gr, bp, vp, acc_b, acc_cor_b,
             amr_s, amr_r, amr_g, amr_u, amr_d, amr_k, amr_o, adress_s, adress_r, adress_g,
             adress_u, adress_d, adress_k, adress_o, name_b, kodcn_b, bik_b, card_b, name_is,
             bik_is, name_r, kodcn_r, bik_r, reserv02, note, active_fl, start_dt, end_dt,
             created_by, created_date, modified_by, modified_date, osb_code
        from mantas.rf_oes_321_party_org
    }';
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  end;


  /* ����� ��� ��������� ������� OESCheckRule read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_oes_check_rule( p_json in rf_json, p_id_request in number ) return rf_json
  is
    v_sql varchar2(1024);
  begin
    v_sql := q'{
      select rule_seq_id "_id_", 
             rule_seq_id, form_cd, desc_tx, check_sql_tx, msg_sql_tx, col_list_tx, block_list_tx, critical_fl, 
             active_fl, err_fl, err_mess_tx, note_tx, created_by, created_date, modified_by, modified_date
        from mantas.rf_oes_check_rule
    }';
    
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  end;

  /* ����� ��� ��������� ������� OESCheckRule create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function create_oes_check_rule( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_json      rf_json;
    v_json_data rf_json;
    v_count     number;
    v_id        number;
  begin
    v_json_data := rf_json_ext.get_json( p_json, 'data[1]' );
    v_json := oes_create( p_json, p_user, p_id_request, 'RF_OES_CHECK_RULE', 'RULE_SEQ_ID', 'RF_OES_CHECK_RULE_SEQ' );
    v_id := rf_json_ext.get_number( rf_json_ext.get_json( v_json, 'data[1]' ), '_id_' );

    /* ��������� ������� */
    commit;    
    v_count := mantas.rf_pkg_oes_check.check_and_mark_rule(
                 par_rule_seq_id => v_id
               );
    if v_count > 0 then
      v_json.put( 'data', rf_json_dyn.executelist( 'select t.*, '||v_id||' "_id_", '||rf_json_ext.get_number(v_json_data, '_id_')|| '"#clientId#"'||
                                                    ' from mantas.rf_oes_check_rule t where t.rule_seq_id = '||v_id ) 
                );
    end if;

    /* ��������� � ������� json */
    return v_json;
  end;
  

  /* ����� ��� ��������� ������� OESCheckRule update
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function update_oes_check_rule( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_json      rf_json;
    v_id        number;
    v_count     number;
  begin
    v_id := rf_json_ext.get_number( rf_json_ext.get_json( p_json, 'data[1]' ), '_id_' );
    v_json := oes_update( p_json, p_user, p_id_request, 'RF_OES_CHECK_RULE', 'RULE_SEQ_ID' );

    /* ��������� ������� */
    commit;    
    v_count := mantas.rf_pkg_oes_check.check_and_mark_rule(
                 par_rule_seq_id => v_id
               );
    v_json.put( 'data', rf_json_dyn.executeList( 'select t.*, '||v_id||' "_id_" from mantas.rf_oes_check_rule t where t.rule_seq_id = '||v_id ) );

    return v_json;
  end;

  
  /* ����� ��� ��������� ������� OES321Create create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function oes_manual_create( p_json in rf_json, p_user in varchar2 ) return rf_json
  is
    v_json               rf_json;
    v_review_id          number;
    v_oes_id             number;
    v_review_id2         number;
    v_oes_id2            number;
    v_create_user_seq_id number;
    v_double_kgrko_id    number := null;
  begin
    select owner_seq_id into v_create_user_seq_id
      from mantas.kdd_review_owner 
     where upper(owner_id) = upper(p_user);
    
    /* ��� ��������������� �������� �������� ���� double_kgrko_id */ 
    if rf_json_ext.get_number( p_json, 'data[1].double_kgrko_fl' ) = 1 then
      select business.trxn_seq.nextval into v_double_kgrko_id from dual;
    end if;

    /* ������� ������ ���. � ������ ������������� �������� ��� ������� �����������. */
    mantas.rf_pkg_oes.create_oes_321_manual(
      par_opok_nb        => rf_json_ext.get_number( p_json, 'data[1].opok_nb' ),
      /* ���� �������� �����������, �� ����������� '0'. ���� �������� '0', �� �������� null */
      par_add_opoks_tx   => nullif(nvl(rf_json_ext.get_string( p_json, 'data[1].add_opoks_tx' ), '0'), '0'),
      /* �������� �������� �� ���������� ����� */
      par_oes_321_org_id => rf_json_ext.get_number( p_json, 'data[1].oes_321_org_id' ),
      /* ���� �������� osb_id �����������, �� ����������� 0. */
      par_branch_id      => rf_json_ext.get_number( p_json, 'data[1].tb_id' ) * 100000 + nvl( rf_json_ext.get_number( p_json, 'data[1].osb_id' ), 0 ),
      par_owner_seq_id   => rf_json_ext.get_number( p_json, 'data[1].owner_id' ),
      par_creat_id       => v_create_user_seq_id,
      par_note_tx        => rf_json_ext.get_string( p_json, 'data[1].note_tx' ),
      par_due_dt         => rf_json_ext.get_date( p_json, 'data[1].due_dt' ),
      par_inter_kgrko_id => v_double_kgrko_id,
      /* ��� ������������ �������� ����������� ���� ��������� = 1 - ���������� */
      par_kgrko_party_cd => case 
                              when v_double_kgrko_id is not null then 1 
                            end,
      par_review_id      => v_review_id,
      par_oes_321_id     => v_oes_id
    );
    
    /* ������� ������ ���. � ������ ������������� �������� ��� ������� ����������. */
    if v_double_kgrko_id is not null then
      mantas.rf_pkg_oes.create_oes_321_manual(
        par_opok_nb        => rf_json_ext.get_number( p_json, 'data[1].opok_nb2' ),
        /* ���� �������� �����������, �� ����������� '0'. ���� �������� '0', �� �������� null */
        par_add_opoks_tx   => nullif(nvl(rf_json_ext.get_string( p_json, 'data[1].add_opoks_tx2' ), '0'), '0'),
        /* �������� �������� �� ���������� ����� */
        par_oes_321_org_id => rf_json_ext.get_number( p_json, 'data[1].oes_321_org_id2' ),
        /* ���� �������� osb_id �����������, �� ����������� 0. */
        par_branch_id      => rf_json_ext.get_number( p_json, 'data[1].tb_id2' ) * 100000 + nvl( rf_json_ext.get_number( p_json, 'data[1].osb_id2' ), 0 ),
        par_owner_seq_id   => rf_json_ext.get_number( p_json, 'data[1].owner_id2' ),
        par_creat_id       => v_create_user_seq_id,
        par_note_tx        => rf_json_ext.get_string( p_json, 'data[1].note_tx2' ),
        par_due_dt         => rf_json_ext.get_date( p_json, 'data[1].due_dt2' ),
        par_inter_kgrko_id => v_double_kgrko_id,
        /* ��� ������������ �������� ����������� ���� ��������� = 2 - ���������� */
        par_kgrko_party_cd => 2,
        par_review_id      => v_review_id2,
        par_oes_321_id     => v_oes_id2
      );
    end if;
    
    v_json := rf_json();
    v_json.put('success', true);
    v_json.put('review_id', v_review_id);
    v_json.put('review_id2', v_review_id2);
    v_json.put('oes_321_id', v_oes_id);
    v_json.put('oes_321_id2', v_oes_id2);
    
    return v_json;
  end;
    
   /* ����� ��������� ������� ��� �������� 
   *   
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json
  is
    v_request    clob := null;
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
    v_batch_date DATE;
  begin
    /* ������ ���� � ������ ��� "T" */
    v_request := regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2');
    
    v_json_in := rf_json(v_request);
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');
    
    -- ������������� ��������
    if v_form_name in ('OES321Details','OES321Party') and v_action = 'update' then
      BEGIN
          v_batch_date:=TO_DATE(rf_json_ext.get_string(v_json_in, 'BATCH_DATE'),'yyyy-mm-dd hh24:mi:ss');
      EXCEPTION WHEN OTHERS THEN
        v_batch_date:=NULL;
      END;
      dbms_session.set_context('CLIENTCONTEXT', 'AML_BATCH_DATE', TO_CHAR(v_batch_date,'dd.mm.yyyy hh24:mi:ss'));
    end if;
    
    if v_form_name = 'OES321Details' and v_action = 'readByOesId' 
      then v_json_out := get_form_321_details( v_json_in, p_id_request );
    elsif v_form_name = 'OES321Details' and v_action = 'readCheckStatus' 
      then v_json_out := get_form_321_check_status( v_json_in, p_id_request );
    elsif v_form_name = 'OES321Details' and v_action = 'update' 
      then v_json_out := update_form_321_details( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'OES321Party' and v_action = 'read' 
      then v_json_out := get_form_321_party( v_json_in, p_id_request );
    elsif v_form_name = 'OES321Party' and v_action = 'update' 
      then v_json_out := update_form_321_party( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'OESCheck' and v_action = 'read' 
      then v_json_out := get_form_check( v_json_in, p_id_request );
    elsif v_form_name = 'OES321Org' and v_action = 'read'
      then v_json_out := get_form_321_org( v_json_in, p_id_request );
    elsif v_form_name = 'OES321Org' and v_action = 'update'
      then v_json_out := oes_update( v_json_in, p_user, p_id_request, 'RF_OES_321_ORG', 'OES_321_ORG_ID' );
    elsif v_form_name = 'OES321Org' and v_action = 'create'
      then v_json_out := oes_create( v_json_in, p_user, p_id_request, 'RF_OES_321_ORG', 'OES_321_ORG_ID', 'RF_OES_321_ORG_SEQ' );
    elsif v_form_name = 'OES321Org' and v_action = 'delete'
      then v_json_out := oes_delete( v_json_in, p_id_request, 'RF_OES_321_ORG', 'OES_321_ORG_ID' );
    elsif v_form_name = 'OES321PartyOrg' and v_action = 'read'
      then v_json_out := get_form_321_party_org( v_json_in, p_id_request );
    elsif v_form_name = 'OES321PartyOrg' and v_action = 'delete'
      then v_json_out := oes_delete( v_json_in, p_id_request, 'RF_OES_321_PARTY_ORG', 'OES_321_P_ORG_ID' );
    elsif v_form_name = 'OES321PartyOrg' and v_action = 'update'
      then v_json_out := oes_update( v_json_in, p_user, p_id_request, 'RF_OES_321_PARTY_ORG', 'OES_321_P_ORG_ID' );
    elsif v_form_name = 'OES321PartyOrg' and v_action = 'create'
      then v_json_out := oes_create( v_json_in, p_user, p_id_request, 'RF_OES_321_PARTY_ORG', 'OES_321_P_ORG_ID', 'RF_OES_321_PARTY_ORG_SEQ' );
    elsif v_form_name = 'OESCheckRule' and v_action = 'read'
      then v_json_out := get_oes_check_rule( v_json_in, p_id_request );
    elsif v_form_name = 'OESCheckRule' and v_action = 'create'
      then v_json_out := create_oes_check_rule( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'OESCheckRule' and v_action = 'update'
      then v_json_out := update_oes_check_rule( v_json_in, p_user, p_id_request );
    elsif v_form_name = 'OESCheckRule' and v_action = 'delete'
      then v_json_out := oes_delete( v_json_in, p_id_request, 'RF_OES_CHECK_RULE', 'RULE_SEQ_ID' );
    elsif v_form_name = 'OES321Create' and v_action = 'create'
      then v_json_out := oes_manual_create( v_json_in, p_user );      
    end if;
    
    return v_json_out;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        v_json_out := rf_json();
        v_json_out.put( 'success', false );
        v_json_out.put( 'message', '�� ������� ��������� ������! ��������� ���������� ������.'||sqlerrm() );
        return v_json_out;
  end;

end;
/
