-- ��������� ������ � set define off
BEGIN 
  EXECUTE IMMEDIATE
  'DROP TABLE mantas.rf_trxn_id';
EXCEPTION  
  WHEN OTHERS THEN
    NULL;
END;
/
CREATE GLOBAL TEMPORARY TABLE mantas.rf_trxn_id(
        op_cat_cd   VARCHAR2(64)
       ,trxn_seq_id NUMBER
       ,review_id   NUMBER) ON COMMIT DELETE ROWS;
BEGIN 
  EXECUTE IMMEDIATE
  'DROP PUBLIC SYNONYM rf_json_helper';
EXCEPTION  
  WHEN OTHERS THEN
    NULL;
END;
/
CREATE PUBLIC SYNONYM rf_json_helper FOR amladm.rf_json_helper;
GRANT EXECUTE ON rf_json_helper TO mantas;

CREATE OR REPLACE PACKAGE mantas.rf_pkg_request_trxn_search IS

  /* ������� ��������� ������� ��� ���� trxnList trxnAction
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  FUNCTION process_request( p_request IN CLOB, p_user IN VARCHAR2,  p_id_request IN NUMBER, p_response OUT CLOB ) RETURN rf_json;
END;
/
CREATE OR REPLACE PACKAGE BODY mantas.rf_pkg_request_trxn_search IS

  trxnSql CONSTANT VARCHAR2(2048) := q'{SELECT ##SELECTLIST##
      FROM ##GENERATOR## idg 
        LEFT JOIN table(business.rfv_trxn(idg.op_cat_cd, idg.trxn_seq_id, NULL)) trxn  ON 1=1
        LEFT OUTER JOIN  mantas.kdd_review rv ON idg.trxn_seq_id=rv.rf_trxn_seq_id AND  idg.op_cat_cd=rv.rf_op_cat_cd
        LEFT OUTER JOIN  mantas.rf_oes_321_org oes_org ON oes_org.oes_321_org_id = mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(rv.rf_branch_id/100000), rv.rf_branch_id - round(rv.rf_branch_id, -5), rv.rf_trxn_dt)
        LEFT JOIN business.org tb on tb.org_intrl_id = to_char(trunc(trxn.branch_id / 100000))
        LEFT JOIN business.org osb on osb.org_intrl_id = to_char(trunc(trxn.branch_id / 100000)) || '-' ||
                                                         to_char(trxn.branch_id - round(trxn.branch_id, -5))
        LEFT JOIN business.rf_nsi_op_types optp on optp.nsi_op_id = trxn.nsi_op_id
        LEFT JOIN mantas.rf_oes_321 oes ON oes.review_id = rv.review_id and oes.current_fl = 'Y'
        LEFT JOIN mantas.kdd_review_owner rvo on rvo.owner_seq_id = rv.owner_seq_id
        LEFT JOIN mantas.kdd_code_set_trnln st on st.code_set = 'AlertStatus' and st.code_val = rv.status_cd
        LEFT JOIN business.ref_table_detail goz_orig ON goz_orig.code_set_id = 'RF_GOZ_OP_TYPE_DBT' and goz_orig.code_val1_nm = trxn.orig_goz_op_cd
        LEFT JOIN business.ref_table_detail goz_benef ON goz_benef.code_set_id = 'RF_GOZ_OP_TYPE_CDT' and goz_benef.code_val1_nm = trxn.benef_goz_op_cd   
      WHERE rownum<=100000 ##WHERE##
       }';

  trxnSqlcount CONSTANT VARCHAR2(2048) := q'{SELECT ##SELECTLIST##
      FROM ##GENERATOR## idg 
        LEFT OUTER JOIN  mantas.kdd_review rv ON idg.trxn_seq_id=rv.rf_trxn_seq_id AND  idg.op_cat_cd=rv.rf_op_cat_cd
      WHERE rownum<=100000 ##WHERE##
       }';

  -- ���������� ����
  trxnSelectList CONSTANT VARCHAR2(4096) := q'{
            idg.op_cat_cd||'|'||trxn.trxn_seq_id||'|'||rv.review_id as "_id_"
          ,trxn.TRXN_SEQ_ID
          ,case idg.op_cat_cd
                       when 'WT' then  '�����������'
                       when 'CT' then  '��������'
                       when 'BOT' then '����������'
                     end as OP_CAT_NM
          ,idg.op_cat_cd
          ,rv.REVIEW_ID
          ,st.code_disp_tx RV_STATUS_NM
          ,rv.STATUS_CD RV_STATUS_CD
          ,rv.rf_OPOK_NB  OPOK_NB 
          ,rv.RF_ADD_OPOKS_TX  ADD_OPOKS_TX 
          ,decode( oes.send_fl, 'Y', 'S', oes.check_status ) CHECK_STATUS
          ,case 
                       when oes.send_fl = 'Y' then '����������'
                       when upper(oes.check_status) = 'E' then '������� ���������'
                       when upper(oes.check_status) = 'W' then '���� ��������������'
                       when upper(oes.check_status) = 'O' then '�������� �������'
                       when oes.check_status is null and oes.oes_321_id is not null then '�� �����������'
                       else ''
                     end CHECK_STATUS_NM
          ,rvo.OWNER_ID rv_owner_id
          ,mantas.rf_pkg_review.get_last_note( rv.review_id ) as RV_NOTE_TX
          ,trxn.TRXN_DESC
          ,trxn.TRXN_BASE_AM
          ,trxn.TRXN_CRNCY_AM
          ,trxn.TRXN_CRNCY_CD
          ,trxn.TRXN_DT
          ,tb.org_intrl_id TB_ID
          ,osb.org_intrl_id OSB_ID
          ,tb.org_nm TB_NM
          ,osb.org_nm OSB_NM
          ,trxn.SRC_SYS_CD
          ,trxn.TRXN_DOC_ID
          ,trxn.TRXN_DOC_DT
          ,trxn.ORIG_NM
          ,trxn.ORIG_INN_NB
          ,trxn.ORIG_ACCT_NB
          ,trxn.DEBIT_CD
          ,trxn.SEND_INSTN_NM
          ,trxn.SEND_INSTN_ID
          ,trxn.SEND_INSTN_ACCT_ID
          ,trxn.ORIG_ADDR_TX
          ,trxn.ORIG_IDDOC_TX
          ,trxn.ORIG_REG_DT
          ,trxn.BENEF_NM
          ,trxn.BENEF_INN_NB
          ,trxn.BENEF_ACCT_NB
          ,trxn.CREDIT_CD
          ,trxn.RCV_INSTN_NM
          ,trxn.RCV_INSTN_ID
          ,trxn.RCV_INSTN_ACCT_ID
          ,trxn.BENEF_ADDR_TX
          ,trxn.BENEF_IDDOC_TX
          ,trxn.BENEF_REG_DT
          ,trim( to_char( trxn.nsi_op_id ) || ' ' || optp.op_type_nm ) NSI_OP
          ,mantas.rf_pkg_scnro.get_explanation(rv.review_id) EXPLANATION_TX
          ,trxn.data_dump_dt 
          ,trxn.SRC_CHANGE_DT
          ,trxn.SRC_CD
          ,oes.oes_321_id
          ,trxn.send_kgrko_id
          ,trxn.send_kgrko_nm
          ,trxn.rcv_kgrko_id
          ,trxn.rcv_kgrko_nm
          ,oes_org.tb_name AS rv_kgrko_nm
          ,CASE WHEN oes_org.oes_321_org_id IS NOT NULL
              THEN NVL(DECODE(oes_org.branch_fl, '0', NULLIF(oes_org.numbf_s, '0'),'1', NULLIF(oes_org.numbf_ss, '0')), '-')
           END AS rv_kgrko_id
          ,CASE WHEN trxn.send_oes_org_id <> trxn.rcv_oes_org_id THEN 1 ELSE 0 END AS is_inter_kgrko  
          ,TO_NUMBER(NULL) cnt
          ,trim(trxn.benef_goz_op_cd ||' '|| nvl(goz_benef.code_val2_nm, goz_benef.code_desc_tx)) as benef_goz_op_cd
          ,trim(trxn.orig_goz_op_cd ||' '|| nvl(goz_orig.code_val2_nm, goz_orig.code_desc_tx)) as orig_goz_op_cd
          , case 
              when nvl2(trxn.org_intrl_id, business.rf_pkg_util.get_trxn_org_desc(trxn.org_intrl_id,trxn.branch_id), null) is not null
              then business.rf_pkg_util.get_trxn_org_desc(trxn.org_intrl_id,trxn.branch_id)||nvl2(trxn.src_user_nm, ' ('||trxn.src_user_nm||')', '')
              else trxn.src_user_nm
            end as trxn_org_desc     
       }';
  
  trxnGenerator CONSTANT VARCHAR2(2048) := q'{
      (SELECT src.op_cat_cd op_cat_cd,  src.trxn_id trxn_seq_id
         FROM (SELECT tf.* ##ADDFUNC## FROM TABLE(mantas.rfv_trxn_search (
            p_op_cat       => :p_op_cat
           ,p_party_type   => :p_party_type
           ,p_date_from    => TO_DATE(:p_date_from,'YYYY-MM-DD HH24:MI:SS')
           ,p_date_to      => TO_DATE(:p_date_to,'YYYY-MM-DD HH24:MI:SS')
           ,p_ammount_from => :p_ammount_from
           ,p_ammount_to   => :p_ammount_to
           ,p_inn1         => :p_inn1
           ,p_wl_inn1      => :p_wl1          
           ,p_acct1        => :p_acct1
           ,p_debit_acct   => :p_acct_p1
           ,p_inn2         => :p_inn2
           ,p_wl_inn2      => :p_wl2          
           ,p_acct2        => :p_acct2
           ,p_credit_acct  => :p_acct_p2
          )) tf ) src
       LEFT OUTER JOIN business.cust orig ON orig.cust_seq_id = src.orig_cust_id
       LEFT OUTER JOIN business.cust benef ON benef.cust_seq_id = src.benef_cust_id
         WHERE rownum<=100000 ##WHERE##
        )}';

  /* ����������� json (������� ������) � varchar ����������� ��������
   *
   * ��������� �������
   *   p_filters - json ������  {"filters":{ ... }}
   *   p_key     - ���� ������������� ���������
   * ��������� �������
   *   varchar2 - ���������
   */
  FUNCTION json_value2cs_char(p_j rf_json, p_key VARCHAR2) RETURN VARCHAR2 IS
      v_ret VARCHAR2(32000);
      v_jv  rf_json_value;
      v_jl  rf_json_list;
    BEGIN
      IF p_j.exist(p_key) 
        THEN
          v_jv:=p_j.get(p_key);
          IF v_jv.is_String THEN 
              v_ret:=v_jv.get_String;
            ELSIF v_jv.is_Array THEN
                v_jl:=rf_json_list(v_jv);
                FOR i IN 1..v_jl.count 
                  LOOP
                    v_ret:=v_ret||CASE WHEN LENGTH(v_ret)>0 THEN ',' END||v_jl.get(i).get_String;
                  END LOOP;
          END IF;
        ELSE 
          v_ret:=NULL;
      END IF;  
      RETURN v_ret;
    END json_value2cs_char;
  
  /* ���������� �������-���������� ID �� �������� �������� ��� ������
   *
   * ��������� �������
   *   p_filters - json ������  {"filters":{ ... }}
   *   p_binds - json ������ ���������� ����������
   * ��������� �������
   *   varchar2 - SQL ������ 
   */
  FUNCTION get_generator(p_filters IN rf_json, p_binds OUT rf_json) RETURN VARCHAR2 IS
      
      TYPE t_cust IS RECORD (
        nm            business.cust.full_nm%TYPE
       ,inn           business.wire_trxn.rf_orig_acct_nb%TYPE
       ,resident      VARCHAR2(1) -- Y/N
       ,acctCrit      mantas.rf_opok_criteria.crit_cd%TYPE
       ,acctAdd       mantas.rf_opok_criteria.crit_tx%TYPE
       ,acctNb        business.wire_trxn.rf_orig_acct_nb%TYPE
       ,acctArea      rf_json_list
       ,watchList     mantas.rf_watch_list.watch_list_cd%TYPE
       ,notWatchList  mantas.rf_watch_list.watch_list_cd%TYPE
      );
      
      v_opCat           VARCHAR2(9);
      v_partyType       VARCHAR2(3);
      v_lexPurposeCrit  mantas.rf_opok_criteria.crit_cd%TYPE;
      v_lexPurposeAdd   mantas.rf_opok_criteria.crit_tx%TYPE;
      v_purpose         mantas.rf_opok_criteria.crit_tx%TYPE;
      v_notOpok         VARCHAR2(2000);
      v_notOpokTp       NUMBER;
      cust              t_cust;
      cust2             t_cust;
      v_sql             VARCHAR2(32000);
      v_where           VARCHAR2(32000);
      v_addfunc         VARCHAR2(32000);
      v_jl              rf_json_list;
      v_search_type_tx  rf_audit_search_params.search_type_tx%TYPE;
      v_audit_cust2     BOOLEAN;
      
      -- ������������ ������� ���� id in (...) 
      -- ��������: ������������ v_where � p_binds ������������ �������
      PROCEDURE add_list2where(p_key VARCHAR2, p_column VARCHAR2, p_pname VARCHAR2) IS
          v_jl          rf_json_list;
        BEGIN
          v_jl:=rf_json_list(p_filters.get(p_key));
          v_where:=v_where||' AND '||p_column||' IN (';
          FOR i IN 1..v_jl.count 
            LOOP
              v_where:=v_where||':'||p_pname||TO_CHAR(i)||',';
              IF v_jl.get(i).is_number
                THEN p_binds.put(p_pname||TO_CHAR(i),v_jl.get(i).get_Number);
                ELSE p_binds.put(p_pname||TO_CHAR(i),v_jl.get(i).get_String);
              END IF;
            END LOOP;
          v_where:=TRIM(',' FROM v_where)||')';
        END add_list2where;
      
      -- ������� ��������� ���������
      -- ��������: ������������ p_binds ������������ �������
      FUNCTION get_cust_where(p_cust t_cust           -- ������ � �������
                             ,p_param_suffix VARCHAR2 -- 1 ��� 2 ��� �����
                             ,p_type VARCHAR2)        -- ORIG ��� BENEF
                             RETURN VARCHAR2 IS
          v_where           VARCHAR2(32000);
          v_acct_where      VARCHAR2(32000);
          v_acct_fast_bind  rf_json;
          v_crit_tx         mantas.rf_opok_criteria.crit_tx%TYPE;
          v_crit_tx_sub     mantas.rf_opok_criteria.subtle_crit_tx%TYPE;
          isFastSearch      BOOLEAN;
          isSubtle          BOOLEAN;
          isForeign         BOOLEAN;
        BEGIN
          v_where:=''''||p_type||'''='''||p_type||'''';
          
          -- ������� �� ������������
          IF p_cust.Nm IS NOT NULL THEN
            v_where:=v_where||' AND UPPER(NVL(src.'||p_type||'_full_nm,'||p_type||'.full_nm)) LIKE UPPER(:p_cust_nm_'||p_type||')';
            p_binds.put('p_cust_nm_'||p_type,'%'||p_cust.Nm||'%');
          END IF;
          
          -- ������� �� ������
          IF p_cust.resident IS NOT NULL THEN
            v_where:=v_where||' AND NVL(src.'||p_type||'_resident_fl,'||p_type||'.rf_resident_fl) = :p_resident_'||p_type;
            p_binds.put('p_resident_'||p_type,p_cust.resident);
          END IF;         
          
          -- ������� �� ���
          IF p_cust.inn IS NOT NULL  THEN
            IF REGEXP_INSTR(p_cust.inn,'[%|_]')=0 -- ���� �������� �� �����
              THEN -- �������� � pipe_line 
                IF p_param_suffix IS NOT NULL THEN
                  p_binds.put('p_inn'||p_param_suffix,p_cust.inn);
                END IF;
              ELSE -- ������������ ����
                IF p_param_suffix IS NOT NULL THEN
                  p_binds.put('p_inn'||p_param_suffix,TO_CHAR(NULL));
                END IF;
                v_where:=v_where||' AND NVL(src.'||p_type||'_INN_NB,'||p_type||'.tax_id) LIKE :p_inn_'||p_type;
                p_binds.put('p_inn_'||p_type,p_cust.inn);
            END IF;
          END IF;
          
          -- ������� �� ����������� �������
          IF p_cust.watchList IS NOT NULL
              THEN -- �������� � pipe_line 
                IF p_param_suffix IS NOT NULL THEN
                  p_binds.put('p_wl'||p_param_suffix,p_cust.watchList);
                END IF;
          END IF;
          
          -- ������� �� �� ����������� �������
          IF p_cust.notWatchList IS NOT NULL THEN
            v_where:=v_where||' AND NOT EXISTS(SELECT ''X'' FROM mantas.rf_watch_list_org wo WHERE wo.watch_list_cd=:p_nwl_'||p_type||' AND wo.inn_nb=NVL(src.'||p_type||'_INN_NB,'||p_type||'.tax_id))';
            p_binds.put('p_nwl_'||p_type,p_cust.notWatchList);
          END IF;
          
          -- ������� �� �����
          v_acct_fast_bind:=rf_json();
          v_acct_where:='';
          IF REGEXP_INSTR(p_cust.acctNb,'[%|_]')=0 
            THEN isFastSearch:=TRUE;
            ELSE isFastSearch:=FALSE;
          END IF;
          -- ����������� ������� �� �����
          IF cust.acctCrit IS NOT NULL OR cust.acctAdd IS NOT NULL THEN
            SELECT MAX(c.crit_tx),MAX(c.subtle_crit_tx) 
              INTO v_crit_tx, v_crit_tx_sub
              FROM mantas.rf_opok_criteria c
              WHERE c.crit_cd=p_cust.acctCrit;
            IF p_cust.acctAdd IS NOT NULL THEN  
              v_crit_tx:=v_crit_tx||CASE WHEN LENGTH(v_crit_tx)>0 THEN ',' END|| p_cust.acctAdd;
            END IF;
            isSubtle:=v_crit_tx_sub IS NOT NULL;
          END IF;
          -- �������� �� ������� �������� �� ���������� ������
          IF INSTR(UPPER(v_crit_tx),'����������')>0 
            THEN isForeign:=TRUE;          
            ELSE isForeign:=FALSE;
          END IF;
          
          -- ���� �� �������� ������
          FOR i IN 1..p_cust.acctArea.count
            LOOP
              v_acct_where:=v_acct_where||' OR ('''||p_cust.acctArea.get(i).get_string||'''='''||p_cust.acctArea.get(i).get_string||'''';
              
              CASE p_cust.acctArea.get(i).get_string
                WHEN '��' THEN -- ������������ �����������
                  --����������� �������
                  IF v_crit_tx IS NOT NULL THEN
                    v_acct_where:=v_acct_where||' AND mantas.rf_pkg_scnro.check_acct_in_text(src.desc_tx,:p_acct_crit_'
                                              ||p_type||TO_CHAR(i)||',:p_acct_crit_sub_'||p_type||TO_CHAR(i)||')=1';
                    p_binds.put('p_acct_crit_'||p_type||TO_CHAR(i),v_crit_tx);
                    p_binds.put('p_acct_crit_sub_'||p_type||TO_CHAR(i),v_crit_tx_sub);
                  END IF;
                  --����
                  isFastSearch:=FALSE;
                  IF p_cust.acctNb IS NOT NULL THEN 
                    v_acct_where:=v_acct_where||' AND mantas.rf_pkg_scnro.check_acct_template_in_text(src.desc_tx,:p_acct_'
                                              ||p_type||TO_CHAR(i)||') =1';
                    p_binds.put('p_acct_'||p_type||TO_CHAR(i),p_cust.acctNb);
                  END IF;  
                  
                WHEN '��' THEN -- ������������ ���������
                  --����������� �������
                  IF v_crit_tx IS NOT NULL THEN
                    v_acct_where:=v_acct_where||' AND mantas.rf_pkg_scnro.check_acct_in_text(NVL(src.'
                                              ||p_type||'_full_nm,'||p_type||'.full_nm),:p_acct_crit_'
                                              ||p_type||TO_CHAR(i)||',:p_acct_crit_sub_'||p_type||TO_CHAR(i)||')=1';
                    p_binds.put('p_acct_crit_'||p_type||TO_CHAR(i),v_crit_tx);
                    p_binds.put('p_acct_crit_sub_'||p_type||TO_CHAR(i),v_crit_tx_sub);
                  END IF;
                  --����
                  isFastSearch:=FALSE;
                  IF p_cust.acctNb IS NOT NULL THEN  -- �� �����
                    v_acct_where:=v_acct_where||' AND mantas.rf_pkg_scnro.check_acct_template_in_text(NVL(src.'
                                              ||p_type||'_full_nm,'||p_type||'.full_nm),:p_acct_'||p_type||TO_CHAR(i)||') =1';
                    p_binds.put('p_acct_'||p_type||TO_CHAR(i),p_cust.acctNb);
                  END IF;  
                  
                WHEN '��' THEN -- ���� ���������
                  --����������� �������
                  IF v_crit_tx IS NOT NULL THEN
                    v_acct_where:=v_acct_where||' AND ((mantas.rf_pkg_scnro.check_acct_5(SUBSTR(src.'||p_type||'_acct_nb,1,5),:p_acct_crit_'||p_type||TO_CHAR(i)||')=1';
                    p_binds.put('p_acct_crit_'||p_type||TO_CHAR(i),v_crit_tx);
                    IF isForeign
                      THEN 
                        v_acct_where:=v_acct_where||' OR mantas.rf_pkg_scnro.is_foreign_acct(src.'||p_type||'_instn_id, src.'||p_type||'_acct_nb) = 1)';
                      ELSE 
                        v_acct_where:=v_acct_where||')';
                    END IF;
                    IF isSubtle THEN
                      v_acct_where:=v_acct_where||' AND mantas.rf_pkg_scnro.check_acct_additional(src.'
                                                ||p_type||'_acct_nb,:p_acct_crit_sub_'||p_type||TO_CHAR(i)||')=1';
                      p_binds.put('p_acct_crit_sub_'||p_type||TO_CHAR(i),v_crit_tx_sub);                    
                    END IF;
                    v_acct_where:=v_acct_where||')';
                  END IF;
                  --����
                  IF p_cust.acctNb IS NOT NULL THEN 
                    v_acct_where:=v_acct_where||' AND UPPER(src.'||p_type||'_acct_nb) LIKE UPPER(:p_acct_'||p_type||TO_CHAR(i)||')';
                    p_binds.put('p_acct_'||p_type||TO_CHAR(i),p_cust.acctNb);
                    IF p_param_suffix IS NOT NULL THEN 
                      v_acct_fast_bind.put('p_acct'||p_param_suffix,p_cust.acctNb);
                    END IF;
                  END IF;  
                  
                WHEN '��' THEN -- ���� ��������
                  --����������� �������
                  IF v_crit_tx IS NOT NULL THEN
                    v_acct_where:=v_acct_where||' AND ((mantas.rf_pkg_scnro.check_acct_5(SUBSTR(src.'||CASE p_type WHEN 'ORIG' THEN 'debit_acct_nb' ELSE 'credit_acct_nb' END 
                                                  ||',1,5),:p_acct_crit_'||p_type||TO_CHAR(i)||')=1';
                    p_binds.put('p_acct_crit_'||p_type||TO_CHAR(i),v_crit_tx); 
                    v_acct_where:=v_acct_where||')';
                    IF isSubtle THEN
                        v_acct_where:=v_acct_where||' AND mantas.rf_pkg_scnro.check_acct_additional(src.'
                                                  ||CASE p_type WHEN 'ORIG' THEN 'debit_acct_nb' ELSE 'credit_acct_nb' END
                                                  ||',:p_acct_crit_sub_'||p_type||TO_CHAR(i)||')=1';
                        p_binds.put('p_acct_crit_sub_'||p_type||TO_CHAR(i),v_crit_tx_sub);
                    END IF;
                    IF isForeign
                      THEN 
                        v_acct_where:=v_acct_where||' OR mantas.rf_pkg_scnro.is_foreign_acct(src.'||p_type||'_instn_id, src.'
                                                  ||CASE p_type WHEN 'ORIG' THEN 'debit_acct_nb' ELSE 'credit_acct_nb' END||') = 1)';
                      ELSE 
                        v_acct_where:=v_acct_where||')';
                    END IF;

                  END IF;
                  --����
                  IF p_cust.acctNb IS NOT NULL THEN 
                    v_acct_where:=v_acct_where||' AND UPPER(src.'||CASE p_type WHEN 'ORIG' THEN 'debit_acct_nb' ELSE 'credit_acct_nb' END ||') LIKE UPPER(:p_acct_'||p_type||TO_CHAR(i)||')';
                    p_binds.put('p_acct_'||p_type||TO_CHAR(i),p_cust.acctNb);
                    IF p_param_suffix IS NOT NULL THEN
                      v_acct_fast_bind.put('p_acct_p'||p_param_suffix,p_cust.acctNb);
                    END IF;
                  END IF;  
                  
                ELSE 
                  RAISE_APPLICATION_ERROR(-20001,'���������� ������: �������� �� ���������� ������� ���c�� ����� custAcctArea='||cust.acctArea.get(i).get_string);
              END CASE;
              v_acct_where:=v_acct_where||')';
            END LOOP;
          
          IF isFastSearch 
            THEN
              IF p_param_suffix IS NOT NULL THEN
                 p_binds:= rf_json_helper.merge(p_binds,v_acct_fast_bind);
              END IF;
          END IF;  
          
          IF v_acct_where IS NOT NULL THEN
             v_where:= v_where||' AND ('||REGEXP_SUBSTR(v_acct_where,'[^ OR].*')||')';
          END IF;
          
          RETURN v_where;
        END get_cust_where;
    
    -------------------------
    -- ������ ������������ 
    -------------------------
    BEGIN 
            -- ��������� ������������ ���������
      IF rf_json_ext.get_string(p_filters,'dtFrom') IS NULL THEN
        RAISE_APPLICATION_ERROR(-20000,'���� "���� �������� ��" ������ ���� �����������!');
      END IF;
      -- ������ ����������
      v_opCat:=NVL(json_value2cs_char(p_filters,'opCat'),'CT,WT,BOT');
      v_partyType:=rf_json_ext.get_string(p_filters,'partyType');
      v_lexPurposeCrit:=rf_json_ext.get_string(p_filters,'lexPurposeCrit');
      v_lexPurposeAdd:=TRIM(rf_json_ext.get_string(p_filters,'lexPurposeAdd'));
      v_notOpok:=TRIM(TO_CHAR(rf_json_ext.get_number(p_filters,'notOpok')));
      v_notOpokTp:=NVL(rf_json_ext.get_number(p_filters,'notOpokTp'),0);
      
      -- ������ ��������
      cust.Nm:=TRIM(rf_json_ext.get_string(p_filters,'custNm'));
      cust.Inn:=TRIM(rf_json_ext.get_string(p_filters,'custInn'));
      cust.resident:=TRIM(rf_json_ext.get_string(p_filters,'custResident'));
      cust.acctCrit:=rf_json_ext.get_string(p_filters,'acctCrit');
      cust.acctAdd:=TRIM(rf_json_ext.get_string(p_filters,'acctAdd'));
      cust.acctNb:=TRIM(rf_json_ext.get_string(p_filters,'acctNb'));
      -- WatchList
      IF rf_json_ext.get_number(p_filters,'custNotWatchListFl') =1
        THEN -- ��������������� ������
          cust.watchList:=NULL;
          cust.notWatchList:=TRIM(rf_json_ext.get_string(p_filters,'custWatchList'));
        ELSE -- ������� ������
          cust.watchList:=TRIM(rf_json_ext.get_string(p_filters,'custWatchList'));
          cust.notWatchList:=NULL;
      END IF;
      -- ������� ��������   
      IF p_filters.exist('acctArea')
        THEN
          cust.acctArea:=rf_json_list(p_filters.get('acctArea'));
        ELSE -- �� ��������� ���� ���������
          cust.acctArea:=rf_json_list('["��"]');
      END IF;
      
      -- ������ ��������
      cust2.Nm:=TRIM(rf_json_ext.get_string(p_filters,'cust2Nm'));
      cust2.Inn:=TRIM(rf_json_ext.get_string(p_filters,'cust2Inn'));
      cust2.resident:=TRIM(rf_json_ext.get_string(p_filters,'cust2Resident'));
      cust2.acctCrit:=rf_json_ext.get_string(p_filters,'acct2Crit');
      cust2.acctAdd:=TRIM(rf_json_ext.get_string(p_filters,'acct2Add'));
      cust2.acctNb:=TRIM(rf_json_ext.get_string(p_filters,'acct2Nb'));
       -- WatchList
      IF rf_json_ext.get_number(p_filters,'cust2NotWatchListFl') =1
        THEN -- ��������������� ������
          cust2.watchList:=NULL;
          cust2.notWatchList:=TRIM(rf_json_ext.get_string(p_filters,'cust2WatchList'));
        ELSE -- ������� ������
          cust2.watchList:=TRIM(rf_json_ext.get_string(p_filters,'cust2WatchList'));
          cust2.notWatchList:=NULL;
      END IF;
      -- ������� ��������   
      IF p_filters.exist('acct2Area')
        THEN
          cust2.acctArea:=rf_json_list(p_filters.get('acct2Area'));
        ELSE -- �� ��������� ���� ���������
          cust2.acctArea:=rf_json_list('["��"]'); 
      END IF;
      -- ����� ������� ����������
 
      -- ���������� ������ � ������ � �����
      v_audit_cust2:=FALSE;
      CASE v_partyType 
        WHEN 'O' THEN   v_search_type_tx:='����������';
        WHEN 'B' THEN   v_search_type_tx:='����������';
        WHEN 'O|B' THEN v_search_type_tx:='���������� ��� ����������';
        WHEN 'O&B' THEN v_search_type_tx:='����������'; v_audit_cust2:=TRUE;
        ELSE v_search_type_tx:=NULL;
      END CASE;  
      -- ���������� ������ � cust
      IF COALESCE(cust.Nm,cust.AcctNb,cust.Inn,cust.watchList) IS NOT NULL THEN
        rf_pkg_audit.add_search_params(
          par_audit_seq_id    => rf_pkg_request.get_current_audit_event_id
         ,par_search_type_tx  => v_search_type_tx
         ,par_last_nm         => cust.Nm
         ,par_acct_nb         => cust.AcctNb
         ,par_inn_nb          => cust.Inn
         ,par_watch_list_cd   => cust.watchList
        );
      END IF;
      -- ���������� ������ � cust2
      IF v_audit_cust2 AND COALESCE(cust2.Nm,cust2.AcctNb,cust2.Inn,cust2.watchList) IS NOT NULL THEN
        rf_pkg_audit.add_search_params(
          par_audit_seq_id    => rf_pkg_request.get_current_audit_event_id
         ,par_search_type_tx  => '����������'
         ,par_last_nm         => cust2.Nm
         ,par_acct_nb         => cust2.AcctNb
         ,par_inn_nb          => cust2.Inn
         ,par_watch_list_cd   => cust2.watchList
        );
      END IF;
      
      
      -- ������������ ����������� ���������
      p_binds:=rf_json();
     
      -- ��� ��������� ������ �������� pipe_line �������
      p_binds.put('p_op_cat',v_opCat);
      p_binds.put('p_party_type',v_partyType);
      p_binds.put('p_date_from',rf_json_ext.get_string(p_filters,'dtFrom'));
      p_binds.put('p_date_to',rf_json_ext.get_string(p_filters,'dtTo'));
      p_binds.put('p_ammount_from',NVL(rf_json_ext.get_number(p_filters,'amountFrom'),0));
      p_binds.put('p_ammount_to',rf_json_ext.get_number(p_filters,'amountTo'));
 
      -- ������� � ���������
      IF p_filters.exist('osbIds') 
        THEN
          v_jl:=rf_json_list(p_filters.get('osbIds'));
          v_where:=v_where||' AND src.branch_id IN (';
          DECLARE
            v_osbId VARCHAR2(10);
          BEGIN
            FOR i IN 1..v_jl.count 
              LOOP
                v_where:=v_where||':p_osb'||TO_CHAR(i)||',';
                v_osbId:=v_jl.get(i).get_String;
                p_binds.put('p_osb'||TO_CHAR(i),TO_NUMBER(regexp_substr(v_osbId,'\d+',1,1))*100000 +TO_NUMBER(regexp_substr(v_osbId,'\d+',1,2)));
              END LOOP;
          END;
          v_where:=TRIM(',' FROM v_where)||')';
        ELSIF p_filters.exist('tbIds')
          THEN 
            add_list2where('tbIds', 'TRUNC(src.branch_id/100000)', 'p_tb');
      END IF;
          
      -- ���������� �������
      IF v_lexPurposeCrit IS NOT NULL OR v_lexPurposeAdd IS NOT NULL THEN
        SELECT max(c.crit_tx) INTO v_purpose 
          FROM mantas.rf_opok_criteria c
          WHERE c.crit_cd=v_lexPurposeCrit;
        IF  v_lexPurposeAdd IS NOT NULL THEN  
          v_purpose:=v_purpose||CASE WHEN LENGTH(v_purpose)>0 THEN ',' END||v_lexPurposeAdd;
        END IF;
        v_where:=v_where||' AND mantas.rf_pkg_scnro.check_lexeme (src.desc_tx,:p_purpose)=1';
        p_binds.put('p_purpose',v_purpose);
      END IF;
 
      --�������� �� �������� �� ����
      IF v_notOpok IS NOT NULL THEN
        v_addfunc:=v_addfunc||',rf_pkg_kgrko.is_inter_kgrko(tf.op_cat_cd ,tf.trxn_id ,tf.orig_acct_id ,tf.orig_acct_nb ,tf.orig_instn_id ,tf.orig_cust_id ,tf.benef_acct_id ,tf.benef_acct_nb ,tf.benef_instn_id ,tf.benef_cust_id ,tf.exctn_dt ,tf.src_sys_cd) as inter_kgrko_fl';
        IF v_notOpokTp=0 
          THEN
            v_where:=v_where|| q'{ AND ((src.inter_kgrko_fl=1 AND (NOT EXISTS(SELECT 1 FROM mantas.kdd_review r 
                    WHERE r.rf_op_cat_cd=src.op_cat_cd AND r.rf_trxn_seq_id=src.trxn_id 
                       AND mantas.rf_pkg_scnro.check_in_list(:p_not_opok1,TRIM(',' FROM TO_CHAR(r.rf_opok_nb)||','||r.rf_add_opoks_tx))=1
                    HAVING count(distinct r.rf_kgrko_party_cd)=2)))}';
            p_binds.put('p_not_opok1',v_notOpok);
          ELSE
            v_where:=v_where|| q'{ AND ((src.inter_kgrko_fl=1
                  AND (NOT EXISTS(SELECT 1 FROM mantas.kdd_review r 
                    WHERE r.rf_op_cat_cd=src.op_cat_cd AND r.rf_trxn_seq_id=src.trxn_id AND r.rf_kgrko_party_cd=:p_not_opok_tp
                       AND mantas.rf_pkg_scnro.check_in_list(:p_not_opok1,TRIM(',' FROM TO_CHAR(r.rf_opok_nb)||','||r.rf_add_opoks_tx))=1)))}';
            p_binds.put('p_not_opok_tp',v_notOpokTp);
            p_binds.put('p_not_opok1',v_notOpok);
        END IF;  
        v_where:=v_where|| q'{ OR (src.inter_kgrko_fl=0
                    AND (NOT EXISTS(SELECT 1 FROM mantas.kdd_review r 
                      WHERE r.rf_op_cat_cd=src.op_cat_cd AND r.rf_trxn_seq_id=src.trxn_id
                        AND mantas.rf_pkg_scnro.check_in_list(:p_not_opok2,TRIM(',' FROM TO_CHAR(r.rf_opok_nb)||','||r.rf_add_opoks_tx))=1))))}';
        p_binds.put('p_not_opok2',v_notOpok);
      END IF;
   
      -- ������� ��������
      IF p_filters.exist('srcSys') THEN 
        add_list2where('srcSys', 'src.src_cd', 'p_src');       
      END IF;      
   
      -- ����������� �� �������
      CASE v_partyType
        WHEN 'O' THEN
          v_where:=v_where||' AND ('||get_cust_where(cust,'1','ORIG')||')';
        WHEN 'B' THEN
          v_where:=v_where||' AND ('||get_cust_where(cust,'2','BENEF')||')';
        WHEN 'O|B' THEN
          v_where:=v_where||' AND (('||get_cust_where(cust,'1','ORIG')||') OR ('|| get_cust_where(cust,'2','BENEF')||'))';
        WHEN 'O&B' THEN
          v_where:=v_where||' AND (('||get_cust_where(cust,'1','ORIG')||') AND ('|| get_cust_where(cust2,'2','BENEF')||'))';
        ELSE
          RAISE_APPLICATION_ERROR(-20001,'���������� ������: ������� �� ���������� ��� ��������� partyType='||v_partyType);
      END CASE;
      
      -- ��������� ������ ���������� ����������
      IF NOT p_binds.exist('p_inn1')     THEN p_binds.put('p_inn1',TO_CHAR(null)); END IF; 
      IF NOT p_binds.exist('p_wl1')      THEN p_binds.put('p_wl1',TO_CHAR(null)); END IF; 
      IF NOT p_binds.exist('p_acct1')    THEN p_binds.put('p_acct1',TO_CHAR(null)); END IF;
      IF NOT p_binds.exist('p_acct_p1')  THEN p_binds.put('p_acct_p1',TO_CHAR(null)); END IF; 
      IF NOT p_binds.exist('p_inn2')     THEN p_binds.put('p_inn2',TO_CHAR(null)); END IF; 
      IF NOT p_binds.exist('p_wl2')      THEN p_binds.put('p_wl2',TO_CHAR(null)); END IF; 
      IF NOT p_binds.exist('p_acct2')    THEN p_binds.put('p_acct2',TO_CHAR(null)); END IF;
      IF NOT p_binds.exist('p_acct_p2')  THEN p_binds.put('p_acct_p2',TO_CHAR(null)); END IF; 

      -- ������������ ������ 
      v_sql:=trxnGenerator;
      v_sql := REPLACE( v_sql,'##WHERE##', v_where);
      v_sql := REPLACE( v_sql,'##ADDFUNC##', v_addfunc);
      
      RETURN v_sql;
    END get_generator;
  
  /* ���������� �������-���������� ID �� ��������� ������ ID 
   *
   * ��������� �������
   *   p_ids - json_list ID ����: <op_cat_cd>|<trxn_seq_id>|<review_id>
   *   p_binds - json ������ ���������� ����������
   * ��������� �������
   *   varchar2 - SQL ������ 
   */  
  FUNCTION get_generator_by_ids(p_ids IN rf_json_list, p_binds OUT rf_json) RETURN VARCHAR2 IS
      TYPE t_trxn_id IS RECORD (
        op_cat_cd   mantas.kdd_review.rf_op_cat_cd%TYPE
       ,trxn_seq_id mantas.kdd_review.rf_trxn_seq_id%TYPE
       ,review_id   mantas.kdd_review.review_id%TYPE);
      TYPE t_trxn_ids IS TABLE OF t_trxn_id INDEX BY BINARY_INTEGER;
      
      parsed_ids t_trxn_ids;
      v_id       VARCHAR2(255);
      v_sql      VARCHAR2(32000);
      v_where    VARCHAR2(32000);
    BEGIN 
      p_binds:=rf_json();
      -- ��������� ��������
      FOR i IN 1..p_ids.count
        LOOP
          v_id:=p_ids.get(i).get_String;
          parsed_ids(i).op_cat_cd:=regexp_substr(v_id, '[^|]+', 1, 1);
          parsed_ids(i).trxn_seq_id:=TO_NUMBER(regexp_substr(v_id, '[^|]+', 1, 2));
          parsed_ids(i).review_id:=TO_NUMBER(regexp_substr(v_id, '[^|]+', 1, 3));
        END LOOP;
        
      IF parsed_ids.LAST=1 
        THEN -- ���� ������ ���� �� �������
          v_sql:='(SELECT :p_op_cat_cd op_cat_cd,:p_trxn_seq_id trxn_seq_id, :p_review_id review_id FROM DUAL)';
          p_binds.put('p_op_cat_cd',parsed_ids(1).op_cat_cd);
          p_binds.put('p_trxn_seq_id',parsed_ids(1).trxn_seq_id);
          p_binds.put('p_review_id',parsed_ids(1).review_id);
        ELSE -- ���� �� �����
          -- ��������� ��������� �������
          FORALL i IN parsed_ids.FIRST..parsed_ids.LAST
            INSERT INTO mantas.rf_trxn_id (op_cat_cd,trxn_seq_id,review_id) VALUES (parsed_ids(i).op_cat_cd,parsed_ids(i).trxn_seq_id,parsed_ids(i).review_id);
          v_sql:='(SELECT op_cat_cd,trxn_seq_id, review_id FROM mantas.rf_trxn_id)';  
      END IF;
      
      RETURN v_sql;
    END get_generator_by_ids;
    
  /* ��������� ������� trxnList count
   *
   * ��������� �������
   *   p_json - json ������  {"form":"trxnList","action":"count","filters":{ ... }}
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� �������
   *   rf_json - json ����� {"success":"true","count": 123456}
   */
  FUNCTION get_trxn_count(p_json IN rf_json, p_user IN VARCHAR2, p_id_request IN NUMBER, p_response OUT CLOB ) -- �� ����������� ��������� ��� �������������
    RETURN rf_json IS
      v_sql VARCHAR2(32000)    := NULL;
      v_json rf_json           := NULL;
      v_json_filter rf_json    := NULL;
      v_json_list rf_json_list := NULL;
      v_owner_seq_id mantas.kdd_review_owner.owner_seq_id%type;
      v_is_all_alerts NUMBER := 0;
      v_bind_json rf_json;
    BEGIN
   
      IF NOT p_json.exist('filters') THEN 
          RETURN rf_json('{"success": "false", "message" : "������ ��������: ������� �� ���������"}');
      END IF;      
      v_sql := trxnSqlcount;
      v_sql := REPLACE( v_sql,'##SELECTLIST##',' COUNT(*) AS "count", COUNT(rv.REVIEW_ID) as "countAlerts"');
      v_sql := REPLACE( v_sql,'##GENERATOR##',get_generator(rf_json_ext.get_json( p_json, 'filters' ),v_bind_json));
      v_sql := REPLACE( v_sql,'##WHERE##','');
   
     /* ���������� ��������������� ������� */    
      BEGIN
        IF v_bind_json IS NOT NULL AND v_bind_json.count>0
          THEN v_json_list := rf_json_dyn.executelist( v_sql, v_bind_json );
          ELSE v_json_list := rf_json_dyn.executelist( v_sql );
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
          RETURN rf_json('{"success": "false", "message" : "���������� ������: ����������� SQL ��������'||sqlerrm()||'"}');
      END;
     
      v_json := rf_json(v_json_list.get(1));
      v_json.put( 'success', true,1 );
      RETURN v_json;
    END get_trxn_count;

  /* ��������� ������� trxnList Read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  FUNCTION get_trxn_list( p_json IN rf_json, p_user IN VARCHAR2, p_id_request IN NUMBER, p_response OUT CLOB) 
    RETURN rf_json IS
      v_sql             varchar2(32000);
      v_json            rf_json;
      v_cancel_fetch_id mantas.rf_request_fetch.fetch_id%type;
      v_clob CLOB;
      v_bind_json rf_json;
    BEGIN
      v_sql := trxnSql;
      v_sql := REPLACE( v_sql,'##SELECTLIST##',trxnSelectList);

      /* ������������� �������� ������ ��� ����� ��������� ������� */
      v_cancel_fetch_id := nvl(rf_json_ext.get_number(p_json, 'cancelFetchId'), 0);
      IF v_cancel_fetch_id > 0 THEN
        mantas.rf_pkg_request_fetch.update_page_info (
          p_fetch_id  => v_cancel_fetch_id,
          p_status_cd => 12
        );
      END IF;
    
      -- �������� ��� ��������   
      IF p_json.exist('trxnIds') 
        THEN
          -- �������� �� ID
          BEGIN
            v_sql := REPLACE( v_sql,'##GENERATOR##',get_generator_by_ids(rf_json_ext.get_json_list( p_json, 'trxnIds' ),v_bind_json));
            v_sql := REPLACE( v_sql,'##WHERE##',' AND ((idg.review_id IS NULL AND rv.review_id IS NULL) OR idg.review_id=rv.review_id)');
            v_json := rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
            /* ����������� ������ ����������� ������� */
            EXCEPTION
              WHEN OTHERS THEN
                rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
                v_clob := rf_pkg_request.exec_exception( '���������� ������ ��� ������� �������� ������ ��������:'||chr(13)||sqlerrm() );
                v_json := rf_json(v_clob);
          END;           
        ELSE 
          -- ������������ ��������. ������������ ������� � ������ ��������� ������ �������� 
          BEGIN
            IF NOT p_json.exist('filters') THEN 
              RETURN rf_json('{"success": "false", "message" : "������: ������� �� ���������"}');
            END IF;      
            v_sql := REPLACE( v_sql,'##GENERATOR##',get_generator(rf_json_ext.get_json( p_json, 'filters' ),v_bind_json));
            v_sql := REPLACE( v_sql,'##WHERE##','');
-- ����� �������
--dbms_output.put_line(v_sql);
--v_bind_json.print;
            v_sql:=v_sql||' ###BIND_JSON###'||v_bind_json.to_char;
            
            p_response := mantas.rf_pkg_request_fetch.run( v_sql, 'cnt', p_user );
         
            v_json := NULL;
            /* ����������� ������ ����������� ������� */
            EXCEPTION
              WHEN OTHERS THEN
                rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
                v_clob := rf_pkg_request.exec_exception( '���������� ������ ��� ������� �������� ������ ��������:'||chr(13)||sqlerrm() );
                v_json := rf_json(v_clob);
          END;  
      END IF;
      
      RETURN v_json;
    END get_trxn_list;

  /* ����� ��������� ������ ��� ��������� ��� ���� ����������� �������
   *
   * ��������� ������
   *   p_json - json ������
   *   p_action - ��������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  FUNCTION get_trxn_list_page( p_json IN rf_json, p_action IN VARCHAR, p_user IN VARCHAR2, p_id_request IN NUMBER, p_response OUT CLOB ) 
    RETURN rf_json IS
      v_json rf_json := NULL;
      v_fetch_id NUMBER;
      v_clob CLOB;
    BEGIN
      v_fetch_id := rf_json_ext.get_number( p_json, 'fetchId' );
      
      IF v_fetch_id > 0 THEN
        /* ��������� ��������� �������� */
        IF p_action = 'readNext' then 
          p_response := rf_pkg_request_fetch.get_next_page( v_fetch_id, p_user );
        /* ��������� ��� ���������� �������� */  
        ELSIF p_action = 'readLast' then 
          /* ������������� ������ � FORCE FETCH */
          mantas.rf_pkg_request_fetch.update_page_info (
            p_fetch_id  => v_fetch_id,
            p_status_cd => 11
          );
          p_response := rf_pkg_request_fetch.get_all_page( v_fetch_id, p_user );
        END IF;
      END IF;
      
      RETURN v_json;
      EXCEPTION
        WHEN OTHERS THEN
          rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
          v_clob := rf_pkg_request.exec_exception( '���������� ������ ��� ������� �������� �������� ������ ��������:'||chr(13)||sqlerrm() );
          v_json := rf_json(v_clob);
    END get_trxn_list_page;

  /* ��������� ������� trxnAction execById execByFilter
   *
   * ��������� �������
   *   p_json - json ������  {"form":"trxnAction","action":"execById","trxnIds":[ ... ]}
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� �������
   *   rf_json - json ����� {"success":"true/false","message": "...","trxnIds":[ ... ]}
   */
  FUNCTION exec_action(p_json IN rf_json, p_action VARCHAR2, p_user IN VARCHAR2, p_id_request IN NUMBER, p_response OUT CLOB ) -- �� ����������� ��������� ��� �������������
    RETURN rf_json IS
      v_opokNb	      kdd_review.rf_opok_nb%TYPE;
      v_dateS	        rf_oes_321.date_s%TYPE;
      v_ownerId	      kdd_review_owner.owner_seq_id%TYPE;
      v_noteTx	      kdd_note_hist.note_tx%TYPE;
      v_cmmntId	      kdd_cmmnt.cmmnt_id%TYPE;
      v_inter_skip_fl NUMBER;
      v_kgrko_party_cd NUMBER;
      v_change_�wner_fl NUMBER;
      v_trxn_ids      rf_tab_varchar;
      v_ret_upd_trxn_ids rf_tab_varchar_pair;
      v_jl_trxn_ids   rf_json_list;
      v_message       VARCHAR2(2000);
      v_json          rf_json:=rf_json();
      is_by_id        BOOLEAN;
      v_bind_json     rf_json;
      v_sql           VARCHAR2(32000);
      v_ids_string    CLOB;
      v_cursor        BINARY_INTEGER;
      v_rc            SYS_REFCURSOR;
      v_execute       BINARY_INTEGER;
      v_json_in       rf_json:=rf_json();
      v_json_list     rf_json_list:= rf_json_list();
    BEGIN
      -- �������� ��������
      CASE  p_action
      
        WHEN 'execById' THEN
          -- �������� ������� ���������
          IF NOT p_json.exist('trxnIds') THEN 
              RETURN rf_json('{"success": "false", "message" : "������ ����������: �� ������� ������ ��������"}');
          ELSIF NOT p_json.get('trxnIds').is_Array THEN
            RETURN rf_json('{"success": "false", "message" : "������ ����������: �� ���������� ������ ������ ��������"}');
          END IF;     
          is_by_id:=TRUE;
          -- ��������� ������ ID
          v_jl_trxn_ids:=rf_json_list(p_json.get('trxnIds'));
          v_trxn_ids:=rf_tab_varchar();
          FOR i IN 1..v_jl_trxn_ids.count 
            LOOP
              v_trxn_ids.EXTEND;
              v_trxn_ids(i):=v_jl_trxn_ids.get(i).get_String;
            END LOOP;
            
        WHEN 'execByFilter' THEN
          IF NOT p_json.exist('filters') THEN 
              RETURN rf_json('{"success": "false", "message" : "������ ����������: ������� �� ���������"}');
          END IF;   
          is_by_id:=FALSE;
          v_sql := trxnSql;
          v_sql := REPLACE( v_sql,'##SELECTLIST##','idg.op_cat_cd||''|''||idg.trxn_seq_id||''|''||rv.review_id trxn_id');
          v_sql := REPLACE( v_sql,'##GENERATOR##',get_generator(rf_json_ext.get_json( p_json, 'filters' ),v_bind_json));
          v_sql := REPLACE( v_sql,'##WHERE##','');

         /* ���������� ��������������� ������� */    
          BEGIN
            v_cursor := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.NATIVE);
            IF v_bind_json IS NOT NULL AND v_bind_json.count>0
              THEN 
                rf_json_dyn.bind_json(v_cursor, v_bind_json);
            END IF;
            v_execute := DBMS_SQL.EXECUTE(v_cursor);
            v_rc := DBMS_SQL.TO_REFCURSOR(v_cursor);
            FETCH v_rc BULK COLLECT INTO v_trxn_ids;
            CLOSE v_rc;
          EXCEPTION
            WHEN OTHERS THEN
              rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
              RETURN rf_json('{"success": "false", "message" : "���������� ������: ����������� SQL ��������'||sqlerrm()||'"}');
          END;     
        ELSE 
          RETURN rf_json('{"success": "false", "message" : "���������� ������: � ����� rf_pkg_request_trxn ����� TrxnAction ������� ������������ ��������� action='||p_action||'"}');
      END CASE;

      -- ��������� ����������
      v_opokNb:=rf_json_ext.get_number(p_json,'opokNb');
      v_dateS:=TO_DATE(rf_json_ext.get_string(p_json,'dateS'),'YYYY-MM-DD HH24:MI:SS');
      v_ownerId:=rf_json_ext.get_number(p_json,'ownerId');
      v_noteTx:=TRIM(rf_json_ext.get_string(p_json,'noteTx'));
      v_cmmntId:=rf_json_ext.get_number(p_json,'cmmntId');
      v_inter_skip_fl:=rf_json_ext.get_number(p_json,'inter_skip_fl');
      v_kgrko_party_cd:=rf_json_ext.get_number(p_json,'kgrko_party_cd');
      v_change_�wner_fl:=rf_json_ext.get_number(p_json,'changeOwnerFl');

      -- ���������
      v_message:=rf_pkg_trxn.make_trxn_opok
        (
          par_mode              => 'TRXN'
         ,par_trxn_ids          => v_trxn_ids 
         ,par_opok_nb           => v_opokNb
         ,par_oes_date_s        => v_dateS
         ,par_owner_seq_id      => v_ownerId
         ,par_override_owner_fl => CASE v_change_�wner_fl WHEN 1 THEN 'Y' ELSE 'N' END
         ,par_skip_kgrko_fl     => CASE v_inter_skip_fl   WHEN 1 THEN 'Y' ELSE 'N' END
         ,par_kgrko_party_cd    => v_kgrko_party_cd
         ,par_note_tx           => v_noteTx
         ,par_cmmnt_id          => v_cmmntId
         ,par_user_id           => p_user
         ,par_updated_trxn_ids  => v_ret_upd_trxn_ids
        );         
      -- �������� ����� � CLOB ��� ��������
      IF NOT is_by_id THEN
        v_message := v_message||'</br><b>����� ������� ��������� ������������ ��������(-��) ���������� �������� ������ ��������!</b>';
      END IF;
      
      v_json.put('success', true);
      v_json.put('message', v_message);

      IF is_by_id THEN -- ���� ��������� �� Id �� �������� � ����� ������ ���������� ID

        IF v_ret_upd_trxn_ids IS NOT NULL AND v_ret_upd_trxn_ids.COUNT >0 THEN 
          FOR i IN v_ret_upd_trxn_ids.FIRST..v_ret_upd_trxn_ids.LAST
            LOOP       
              v_json_in.put('client_id',v_ret_upd_trxn_ids(i).col1);
              v_json_in.put('_id_',v_ret_upd_trxn_ids(i).col2);
              v_json_list.append( v_json_in.to_json_value() );
            END LOOP;
        END IF; 
      v_json.put('trxnIds', v_json_list);
      END IF;
      dbms_lob.createtemporary(p_response, true);
      v_json.to_clob(p_response, true); 
      
      v_json:=NULL;
      RETURN v_json;
    END exec_action;


  /* ��������� ������� TrxnAction setKGRKO
   * 
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function set_kgrko( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_op_cat_cd varchar2(64) := null;
    v_trxn_seq_id number := null;
    v_sql varchar2(2000);
    v_bind_json rf_json;
    v_review_ids mantas.rf_tab_number;
    v_json rf_json;
    v_json_list rf_json_list;
  begin
    /* ������ �������� �� ������� */
    v_op_cat_cd := rf_json_ext.get_string( p_json, 'op_cat_cd' );
    v_trxn_seq_id := rf_json_ext.get_number( p_json, 'trxn_seq_id' );
    /* ���� �� ������ ��������� ��� � ����� ��������, �� ������ */
    if v_op_cat_cd is null or v_trxn_seq_id is null then
      raise_application_error(-20001, '�� ���������� ������ ��������� ��������.');
    end if;    

    /* ��������� ������� ����� */
    v_review_ids := rf_pkg_review.set_trxn_kgrko (
      p_op_cat_cd       => v_op_cat_cd,
      p_trxn_seq_id     => v_trxn_seq_id,
      p_send_oes_org_id => /* ���� ��������� ����, ��� ������ �������� send_oes_org_id, ����� ���������� �������� � null */
                           case 
                             when rf_json_ext.get_number( p_json, 'send_filial_fl' ) = 1 then rf_json_ext.get_number( p_json, 'send_oes_org_id' )
                             else null
                           end,
      p_rcv_oes_org_id  => /* ���� ��������� ����, ��� ������ �������� rcv_oes_org_id, ����� ���������� �������� � null */
                           case 
                             when rf_json_ext.get_number( p_json, 'rcv_filial_fl' ) = 1 then rf_json_ext.get_number( p_json, 'rcv_oes_org_id' )
                             else null
                           end,
      p_alert_party_fl  => rf_json_ext.get_number( p_json, 'alert_party_fl' ),
      p_user            => p_user
    );

    /* ������ ��� ��������� ����������� ������ �� business.rf_trxn_extra */
    v_sql := q'{
      select op_cat_cd, fo_trxn_seq_id as trxn_seq_id, send_oes_org_id, rcv_oes_org_id, created_by, created_date, modified_by, modified_date
        from business.rf_trxn_extra
       where op_cat_cd = :OP_CAT_CD
         and fo_trxn_seq_id = :TRXN_SEQ_ID
    }';
    v_bind_json := rf_json();
    v_bind_json.put('OP_CAT_CD', v_op_cat_cd);
    v_bind_json.put('TRXN_SEQ_ID', v_trxn_seq_id);
    
    /* ����� json � ������� ���������, ���� �� ���� � ������� trxnIds */
    v_json_list := rf_json_list();
    if v_review_ids.count > 0 then
      /* �������� ���� - WT|123|4 */
      for i in 1..v_review_ids.count loop
        v_json_list.append(v_op_cat_cd||'|'||v_trxn_seq_id||'|'||v_review_ids(i));
      end loop;
    else
      v_json_list.append(v_op_cat_cd||'|'||v_trxn_seq_id);
    end if;
    v_json := rf_json();
    v_json.put('success', true);
    v_json.put('data', rf_json_dyn.executelist( v_sql, v_bind_json ) );
    v_json.put('trxnIds', v_json_list);
    return v_json;

    exception
      when others then
        v_json := rf_json();
        v_json.put('success', false);
        v_json.put('message', '������ ���������� �������� �����. '||trim(regexp_replace(sqlerrm, '^ORA-[0-9]{5}:')));
        return v_json;
  end;


   /* ������� ��������� ������� ��� ���� trxnList trxnAction
   *
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  FUNCTION process_request( p_request IN CLOB, p_user IN VARCHAR2,  p_id_request IN NUMBER, p_response OUT CLOB ) RETURN rf_json IS
    v_request    CLOB;
    v_form_name  VARCHAR2(64);
    v_action     VARCHAR2(64);
    v_json_in    rf_json;
    v_json_out   rf_json;
  BEGIN
 
    /* ������ ���� � ������ ��� "T" */
    v_request := REGEXP_REPLACE(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2');
    
    v_json_in := rf_json(v_request); 
    
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');

    CASE v_form_name
      WHEN 'TrxnList' THEN
        CASE v_action 
          WHEN 'read'         THEN v_json_out := get_trxn_list( v_json_in, p_user, p_id_request, p_response ); 
          WHEN 'readNext'     THEN v_json_out := get_trxn_list_page( v_json_in, v_action, p_user, p_id_request, p_response );
          WHEN 'readLast'     THEN v_json_out := get_trxn_list_page( v_json_in, v_action, p_user, p_id_request, p_response );
          WHEN 'count'        THEN v_json_out := get_trxn_count( v_json_in, p_user, p_id_request, p_response );
          ELSE RETURN rf_json('{"success": "false", "message" : "���������� ������: ������������ action='||v_action||' ��� ����� trxnList"}');
        END CASE;  
      WHEN 'TrxnAction' THEN
        CASE v_action 
          WHEN 'execById'     THEN v_json_out := exec_action(v_json_in, v_action, p_user, p_id_request, p_response);
          WHEN 'execByFilter' THEN v_json_out := exec_action(v_json_in, v_action, p_user, p_id_request, p_response);
          WHEN 'setKGRKO'     THEN v_json_out :=  set_kgrko(v_json_in, p_user, p_id_request);
          ELSE RETURN rf_json('{"success": "false", "message" : "���������� ������: ������������ action='||v_action||' ��� ����� trxnAction"}');
        END CASE;  
      ELSE
        RETURN rf_json('{"success": "false", "message" : "���������� ������: � ����� rf_pkg_request_trxn ������ ������������ ��������� form='||v_form_name||'"}');
    END CASE;
 
    RETURN v_json_out;
  END process_request;

END;
/

