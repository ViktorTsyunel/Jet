create or replace package mantas.rf_pkg_request_dicts is

  /* ������� ��������� ��������� ������
   *   
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json;

end;
/


create or replace package body mantas.rf_pkg_request_dicts is
  
  /* ��������� �������� � ��������� ������ */
  c_sql_tb constant varchar2(4096) := q'{
SELECT o.oes_321_org_id as "_ID_"
    ,o.oes_321_org_id
    ,o.tb_code
    ,o.tb_name
    ,o.tel
    ,o.regn
    ,o.nd_ko
    ,o.ktu_s
    ,o.bik_s
    ,o.numbf_s
    ,o.ktu_ss
    ,o.bik_ss
    ,o.numbf_ss
    ,o.active_fl
    ,o.start_dt
    ,o.end_dt
    ,o.created_by
    ,o.created_date
    ,o.modified_by
    ,o.modified_date
    ,o.branch_fl
    ,DECODE(o.is_osb,1,'','   ')||o.kgrko ||' - '|| o.tb_name
          || CASE WHEN o.start_dt IS NOT NULL AND  o.end_dt IS NOT NULL THEN ' (��������� � '||TO_CHAR(o.start_dt,'dd.mm.yyyy')||' �� '||TO_CHAR(o.end_dt,'dd.mm.yyyy')||')'
                  WHEN o.start_dt IS NOT NULL AND  o.end_dt IS NULL THEN ' (��������� � '||TO_CHAR(o.start_dt,'dd.mm.yyyy')||')'
                  WHEN o.start_dt IS NULL AND  o.end_dt IS NOT NULL THEN ' (��������� �� '||TO_CHAR(o.end_dt,'dd.mm.yyyy')||')'
                  ELSE ''
             END as display_name
    ,tb_sort
    ,is_osb
    ,kgrko
    ,rn
  FROM ( SELECT t.*, ROW_NUMBER() OVER (PARTITION BY t.tb_code order by t.tb_code,t.is_osb,t.kgrko) as rn
             ,FIRST_VALUE(t.kgrko) OVER (PARTITION BY t.tb_code order by t.tb_code,t.is_osb,t.kgrko) as tb_sort
          FROM
            (SELECT org.*, decode(org.osb_code,null,1,2) is_osb
                  ,DECODE(org.branch_fl,1,org.numbf_ss,org.numbf_s) AS kgrko   
                FROM mantas.rf_oes_321_org org
                WHERE org.active_fl = 'Y') t
        ) o,
        business.org tb
  WHERE o.tb_code=tb.org_intrl_id  
    AND tb.org_type_cd = '��'
  ORDER BY TO_NUMBER(REGEXP_REPLACE(o.tb_sort, '[^0-9]+', '')),o.tb_code,o.is_osb, TO_NUMBER(REGEXP_REPLACE(o.kgrko, '[^0-9]+', ''))
  }';
  
  c_sql_currency constant varchar2(1024) := q'{
    select iso_num "_ID_", 
           decode(metal_flag, 1, NVL(metal_code,'A'||iso_num), to_char(iso_num)) cur_code,
           name||' ('||iso_alfa || ')' cur_name, 
           metal_flag
      from business.rf_currency
    union all
    select 0, '0', '�����', 0 from dual 
    union all
    select -1, '0', '�����', 1 from dual
  }';
  
  c_sql_doctype constant varchar2(1024) := q'{
    select code_val "_id_", 
           code_val doc_type_cd, 
           code_disp_tx doc_type_nm
      from mantas.kdd_code_set_trnln t 
     where t.code_set = 'RF_OES_321_IDDOCTYPE'
    union all
    select '0', '0', '�����' from dual
    order by 1
  }';
  
  c_sql_region constant varchar2(1024) := q'{
    select * 
      from (select substr(OKATO_CD,1,2) "_ID_",
                   bo.object_nm||' '||bo.addr_type_cd okato_obj_nm,  
                   substr(OKATO_CD,1,2) okato_cd
              from business.rf_addr_object bo
             where level_NB = 1
               and substr(okato_cd,3,1)='0'
            union all
            select '0', '�����', '0' from dual
            union all
            select '00', '����������� �����������' okato_obj_nm, '00' from dual)
    order by decode(okato_cd, '0', '0', '00', '1', okato_obj_nm)
  }';

  c_sql_country constant varchar2(1024) := q'{
    select * 
      from (select iso_numeric_cd "_ID_", 
                   iso_numeric_cd, cntry_nm
              from business.rf_country
            union all 
            select '0', '0', '�����'
              from dual)
    order by decode(cntry_nm, '�����', null, cntry_nm) nulls first
  }';
  
  c_sql_assign_rule constant varchar2(2048) := q'{
          select ar.rule_seq_id as "_id_",                        
      ar.rule_seq_id,                       
      ar.owner_seq_id,                        
      ro.owner_dsply_nm||' ('||ro.owner_id||')' owner_dsply_nm,                       
      ar.order2_fl,                        
      ar.any_fl,                        
      ar.opok_nb,                        
      ar.tb_id,                       
      tb.org_nm tb_name,                       
      ar.osb_id,                        
      osb.org_nm osb_name,                       
      ar.active_fl,                        
      ar.start_dt,                        
      ar.end_dt,                        
      ar.created_by,                        
      ar.created_date,                        
      ar.modified_by,                        
      ar.modified_date                  
      from mantas.rf_assign_rule ar                  
      join mantas.kdd_review_owner ro on ro.owner_seq_id = ar.owner_seq_id                  
      left join business.org tb on tb.org_intrl_id = to_char(ar.tb_id)                  
      left join business.org osb on osb.org_intrl_id = to_char(ar.tb_id||'-'||ar.osb_id)   
  }';

 c_sql_assign_limit constant varchar2(2048) := q'{  
        select l.limit_seq_id as "_id_",
           l.limit_seq_id,
           l.limit_tp_cd,
           l.role_cd,
           l.owner_seq_id,
           nvl2(l.owner_seq_id,
                          ow.owner_dsply_nm || '(' || ow.owner_id || ') ',
                          null) as owner_dsply_nm,
           l.order1_limit_qt,
           l.full_limit_qt,
           l.active_fl,
           l.start_dt,
           l.end_dt,
           l.created_by,
           l.created_date,
           l.modified_by,
           l.modified_date
      from mantas.rf_assign_limit l
      left join mantas.kdd_review_owner ow
      on ow.owner_seq_id = l.owner_seq_id
  }';
  
 c_sql_opok constant varchar2(2048) := q'{  
    select rownum as "v_$0", o.opok_nb as "_id_", o.*, od.limit_am, od.start_dt
      from mantas.rf_opok o left join mantas.rf_opok_details od 
              on o.opok_nb = od.opok_nb and nvl(od.start_dt,to_date('2000-12-31', 'YYYY-MM-DD')) <= sysdate
                                    and nvl(od.end_dt, to_date('9999-12-31', 'YYYY-MM-DD')) >= sysdate   
  }';         
  
  c_sql_opok_details constant varchar2(2048) := q'{  
      select rownum as "v_$0", od.record_seq_id as "_id_", od.* from mantas.rf_opok_details od
  }';       
   
  c_sql_opok_criteria constant varchar2(2048) := q'{  
      select rownum as "v_$0", od.crit_cd as "_id_", od.* from mantas.rf_opok_criteria od
  }';       
  c_sql_opok_rule constant varchar2(2048) := q'{      
      select rownum as "v_$0", r.rule_seq_id as "_id_", r.* from mantas.rf_opok_rule r  
  }';   
  c_sql_watch_list constant varchar2(2048) := q'{      
      select rownum as "v_$0", w.watch_list_cd as "_id_", w.* from mantas.rf_watch_list w  
  }';    
  c_sql_watch_list_org constant varchar2(2048) := q'{      
      select rownum as "v_$0", w.watch_list_org_id as "_id_", w.* from mantas.rf_watch_list_org w
        where w.watch_list_cd=:p_watch_list_cd
  }';    
  /* ����� ��� ��������� ������� *** create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   *   p_table_name - ��� �������, � ������� ���������� ����������
   *   p_key_name - ��� �������� ���������� �����
   *   p_seq_name - ��� ������������������ ��� �������� PK
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  function dict_create( 
    p_json in rf_json, 
    p_user in varchar2, 
    p_id_request in number, 
    p_table_name in varchar2, 
    p_key_name in varchar2, 
    p_seq_name in varchar2 ) 
  return rf_json
  is
    v_clob      clob;
    v_json_data rf_json;
    v_json      rf_json;
    v_sql       varchar2(32000);
  begin
    v_json_data := rf_json_ext.get_json( p_json, 'data[1]' );
    v_json_data.put('CREATED_DATE', to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS'));
    v_json_data.put('CREATED_BY', p_user);

    v_json := rf_json( rf_pkg_request.exec_sql_insert(
                         p_sql      => rf_pkg_request.insert_generate(
                                         p_schema      => 'MANTAS',
                                         p_table_name  => p_table_name,
                                         p_column_name => p_key_name,
                                         p_seq_name    => p_seq_name,
                                         p_data        => v_json_data ),
                         p_id_req   => p_id_request,
                         p_id       => rf_json_ext.get_number(v_json_data, '_id_'),
                         p_clientid => rf_json_ext.get_number(v_json_data, '_id_'),
                         p_schema   => 'MANTAS',
                         p_table    => p_table_name,
                         p_id_name  => p_key_name )
                     );

    /* ��������� � ������� json */
    return v_json;
  end;

/* ����� ��� ��������� ������� *** update
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   *   p_table_name - ��� �������, � ������� ���������� ����������
   *   p_key_name - ��� �������� ���������� �����
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  function dict_update( 
    p_json in rf_json, 
    p_user in varchar2, 
    p_id_request in number, 
    p_table_name in varchar2, 
    p_key_name in varchar2 ) 
  return rf_json
  is
    v_json      rf_json;
    v_id        VARCHAR(2000);
    v_id_type   VARCHAR2(30);
  begin
    v_json := rf_json_ext.get_json( p_json, 'data[1]' );
    IF rf_json_ext.get_json_value( v_json, '_id_').get_type = 'number'
      THEN 
        v_id := rf_json_ext.get_number( v_json, '_id_' );
        v_id_type := 'NUMBER';
      ELSE
        v_id := rf_json_ext.get_string( v_json, '_id_' );
        v_id_type := 'VARCHAR2';
    END IF;  
    v_json.put('MODIFIED_DATE', to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS'));
    v_json.put('MODIFIED_BY', p_user);
    /* ��������� ���������� */
    v_json := rf_json( rf_pkg_request.exec_sql_update(
                         p_sql        => rf_pkg_request.update_generate(
                                           p_schema     => 'MANTAS',
                                           p_table_name => p_table_name,
                                           p_key_name   => p_key_name,
                                           p_data       => v_json
                                         ),
                         p_bind_param => v_id,
                         p_id_req     => p_id_request,
                         p_schema     => 'MANTAS',
                         p_table      => p_table_name,
                         p_id_name    => p_key_name,
                         p_id_type    => v_id_type
                       )
                     );
    return v_json;
  end;

 /* ������� ��� ��������� ������� update ��� ������������ � ������� PK ����� ��������
  *
  * ��������� ������
  *   p_json - json ������
  *   p_user - ��� �������� ������������
  *   p_id_request - ���������������� ����� �������
  *   p_table_name - ��� �������, � ������� ���������� ����������
  *   p_key_name - ��� �������� ���������� �����
  * ��������� ������
  *   rf_json - json ����� � ������� ����� ���������
  */
  FUNCTION dict_update_by_rowid( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER,
    p_table_name in varchar2, 
    p_key_name in varchar2) 
  RETURN rf_json IS
    v_json        rf_json;
    v_json_in     rf_json;
    v_id_n        NUMBER;
    v_id_c        VARCHAR2(4000);
    is_id_number  BOOLEAN;
    v_rowid       VARCHAR2(30);
    v_clob        CLOB;
    c_rw          SYS_REFCURSOR;
  BEGIN
    v_json_in:= rf_json_ext.get_json( p_json, 'data[1]');
    is_id_number:=(rf_json_ext.get_json_value(v_json_in,'_id_').get_type ='number');
    IF is_id_number
      THEN v_id_n:=rf_json_ext.get_number( v_json_in,'_id_'); 
      ELSE v_id_c:=rf_json_ext.get_string( v_json_in,'_id_'); 
    END IF;
    BEGIN 
      IF is_id_number
        THEN OPEN c_rw FOR 'SELECT rowid FROM '||p_table_name||' WHERE '||p_key_name||'=:p_id' USING v_id_n;
        ELSE OPEN c_rw FOR 'SELECT rowid FROM '||p_table_name||' WHERE '||p_key_name||'=:p_id' USING v_id_c;
      END IF;  
      FETCH c_rw INTO v_rowid;
      CLOSE c_rw;
    EXCEPTION 
      WHEN OTHERS THEN
        IF c_rw%ISOPEN THEN
          CLOSE c_rw;
        END IF;
        RAISE;
    END;
    IF v_rowid IS NULL THEN
      RAISE_APPLICATION_ERROR(-20000,'������� �� ������������ _id_!');
    END IF;
    
    v_json_in.put('MODIFIED_DATE', to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS'));
    v_json_in.put('MODIFIED_BY', p_user);
    /* ��������� ���������� */
    v_json := rf_json( rf_pkg_request.exec_sql_update(
                         p_sql        => rf_pkg_request.update_generate(
                                           p_schema     => 'MANTAS',
                                           p_table_name => p_table_name,
                                           p_key_name   => 'ROWID',
                                           p_data       => v_json_in
                                         ),
                         p_bind_param => v_rowid,
                         p_id_req     => p_id_request,
                         p_schema     => 'MANTAS',
                         p_table      => p_table_name,
                         p_id_name    => 'ROWID',
                         p_id_type    => 'VARCHAR2'
                       )
                     );  
                     
    -- �������� �� ��������� ��������� ���������� � ������������
    IF NOT rf_json_ext.get_bool(v_json, 'success') AND INSTR(rf_json_ext.get_string(v_json, 'message'),'ORA-00001')>0 
      THEN
        v_json:=rf_json();
        v_json.put('success',false);
        v_json.put('message','�������� � ����� ��������������� ��� ����!');
      ELSIF NOT rf_json_ext.get_bool(v_json, 'success') AND INSTR(rf_json_ext.get_string(v_json, 'message'),'ORA-02292')>0 
        THEN
          v_json:=rf_json();
          v_json.put('success',false);
          v_json.put('message','�� ���� ��������, ��� ��� �� ������ ����������� ��� ���������!');
        ELSIF rf_json_ext.get_bool(v_json, 'success')
          THEN-- ��������� ������ 
            IF is_id_number
              THEN
                rf_json_ext.put(v_json, 'data[1]._id_',rf_json_ext.get_number(v_json, 'data[1].'||p_key_name));
                rf_json_ext.put(v_json, 'data[1].client_id', v_id_n);
              ELSE 
                rf_json_ext.put(v_json, 'data[1]._id_',rf_json_ext.get_string(v_json, 'data[1].'||p_key_name));
                rf_json_ext.put(v_json, 'data[1].client_id', v_id_c);
            END IF;
    END IF;

    RETURN v_json;
  END dict_update_by_rowid;
 
  /* ����� ��� ��������� ������� *** delete
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   *   p_table_name - ��� �������, � ������� ���������� ��������
   *   p_key_name - ��� �������� ���������� �����
   * ��������� ������
   *   rf_json - json �����
   */
  function dict_delete( 
    p_json in rf_json, 
    p_id_request in number,
    p_table_name in varchar2,
    p_key_name in varchar2) 
  return rf_json
  is
    v_sql varchar2(1024);
    v_json rf_json;
  begin
    FOR i IN 1..rf_json_ext.get_json_list(p_json,'data').count
      LOOP
        v_sql := 'delete from mantas.##TABLE## where ##KEY## = ##ID##';
        v_sql := replace( v_sql, '##TABLE##', p_table_name );     v_sql := replace( v_sql, '##KEY##', p_key_name );
        -- ��������� ��� ID
        IF rf_json_ext.get_json_value( rf_json_ext.get_json( p_json, 'data['||to_char(i)||']' ), '_id_' ).get_type ='number' 
          THEN v_sql := replace( v_sql, '##ID##', rf_json_ext.get_number( rf_json_ext.get_json( p_json, 'data['||to_char(i)||']' ), '_id_' ) );
          ELSE v_sql := replace( v_sql, '##ID##', ''''||rf_json_ext.get_string( rf_json_ext.get_json( p_json, 'data['||to_char(i)||']' ), '_id_' )||'''' );
        END IF;  
        
        v_json := rf_json( rf_pkg_request.exec_sql_delete( v_sql, p_id_request ) );  
        IF NOT rf_json_ext.get_bool(v_json, 'success')   
          THEN
            IF  INSTR(rf_json_ext.get_string(v_json, 'message'),'ORA-02292')>0 THEN
              v_json:=rf_json();
              v_json.put('success',false);
              v_json.put('message','�� ���� �������, ��� ��� �� ������ ����������� ��� ���������!');
            END IF;
          
            RETURN v_json; -- ���� ������� �� ������ ��������� ��������� � �������
        END IF;
      END LOOP;
    return v_json;
  end;

  /* ������� ��� ��������� ������� AssignReplacement read
  *
  * ��������� �������
  *   p_json - json ������
  *   p_id_request - ���������������� ����� �������
  * ��������� ������
  *   rf_json - json �����
  */
  FUNCTION get_assign_replacement(p_json IN rf_json,p_id_request IN NUMBER) RETURN rf_json IS
    v_sql VARCHAR2(2000);
  BEGIN
    v_sql := q'{
      select r.replacement_seq_id as "_id_",
             r.replacement_seq_id,
             r.owner_seq_id,
             ow.owner_dsply_nm||' ('||ow.owner_id||')' as owner_dsply_nm,  
             r.new_owner_seq_id,
             ow_new.owner_dsply_nm||' ('||ow_new.owner_id||')' as new_owner_dsply_nm,       
             r.active_fl,
             r.start_dt,
             r.end_dt,
             r.created_by,
             r.created_date,
             r.modified_by,
             r.modified_date 
        from mantas.rf_assign_replacement r
        join mantas.kdd_review_owner ow on ow.owner_seq_id = r.owner_seq_id
        join mantas.kdd_review_owner ow_new on ow_new.owner_seq_id = r.new_owner_seq_id
       where r.start_dt <= to_date('##DATE1##', '##DATEFORMAT##') 
         and r.end_dt   >= to_date('##DATE2##', '##DATEFORMAT##')
      }';
    -- r.startDt <= endDt and r.endDt >= startDt   
    v_sql := replace( v_sql, '##DATE1##', replace( rf_json_ext.get_string( p_json, 'endDt' ), 'T', ' ' ) );
    v_sql := replace( v_sql, '##DATE2##', replace( rf_json_ext.get_string( p_json, 'startDt' ), 'T', ' ' ) );
    v_sql := replace( v_sql, '##DATEFORMAT##', rf_json_ext.format_string );
    RETURN rf_pkg_request.exec_sql_read_json( v_sql, p_id_request );
  END get_assign_replacement;

  /* ����� ��� ��������� ������� OPOK create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  FUNCTION opok_create( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_json      rf_json;
    v_clob    CLOB;
    v_clob_details    CLOB;
  BEGIN
    SAVEPOINT sp_opok_create;
    v_clob := rf_pkg_request.exec_sql_insert(rf_pkg_request.insert_generate(p_schema      => 'MANTAS',
                                                p_table_name  => 'RF_OPOK',
                                                p_column_name => null,
                                                p_seq_name    => null,
                                                p_key_name    => 'OPOK_NB',
                                                p_data        => rf_json_ext.get_json(p_json,
                                                                                      'data[1]')),
                                p_id_req => p_id_request,
                                p_id => rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                    'data[1]'),
                                                               'OPOK_NB'),
                                p_clientid => rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                          'data[1]'),
                                                                     '_id_'),
                                p_schema => 'MANTAS',
                                p_table => 'RF_OPOK',
                                p_id_name => 'OPOK_NB');
  
    IF rf_json_ext.get_bool(rf_json(v_clob), 'success') THEN
      v_clob_details := rf_pkg_request.exec_sql_insert(rf_pkg_request.insert_generate(p_schema      => 'MANTAS',
                                                        p_table_name  => 'RF_OPOK_DETAILS',
                                                        p_column_name => 'RECORD_SEQ_ID',
                                                        p_seq_name    => 'RF_OPOK_DETAILS_SEQ',
                                                        p_data        => rf_json_ext.get_json(p_json,
                                                                                              'dataDetails[1]')),
                                        p_id_req => p_id_request,
                                        p_id => rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                            'dataDetails[1]'),
                                                                       '_id_'),
                                        p_clientid => rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                                  'dataDetails[1]'),
                                                                             '_id_'));
      IF NOT (rf_json_ext.get_bool(rf_json(v_clob_details), 'success')) THEN
          ROLLBACK TO SAVEPOINT sp_opok_create;
          v_clob := v_clob_details;--exec_exception('��������� ������ ��� �������� ������� ����!' ||
                       --              chr(13) || sqlerrm);
      END IF;
    END IF;
    RETURN rf_json(v_clob);
  END opok_create;
  
  /* ����� ��� ��������� ������� OPOK delete
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  FUNCTION opok_delete( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_json      rf_json;
    v_clob    CLOB;
    v_clob_details    CLOB;
  BEGIN
    SAVEPOINT opok_delete;
    FOR r in 1 .. to_number(rf_json_ext.get_json_list(p_json, 'data').count) 
      LOOP
        -- �������, ��� �� ������� mantas.rf_opok_rule ������� ��������� ������ �������
        -- �� ���� ���� ��������� ����, � ������� ��� ���� ���������� �� ����������� ������ ��������� ��������
        -- � ����� ���� �� ������ ���� ������
        v_clob_details := rf_pkg_request.exec_sql_delete(p_sql    => 'delete from mantas.rf_opok_details d where d.opok_nb = ' ||
                                                      rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                                  'data[' || r || ']'),
                                                                             '_id_'),
                                          p_id_req => p_id_request);
        IF NOT rf_json_ext.get_bool(rf_json(v_clob_details), 'success') THEN
          ROLLBACK TO SAVEPOINT opok_delete;
          RETURN  rf_json(v_clob_details);
        END IF;
        v_clob := rf_pkg_request.exec_sql_delete(p_sql    => 'delete from mantas.rf_opok o where o.opok_nb = ' ||
                                                rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                            'data[' || r || ']'),
                                                                       '_id_'),
                                    p_id_req => p_id_request);
        IF NOT rf_json_ext.get_bool(rf_json(v_clob), 'success') THEN
          ROLLBACK TO SAVEPOINT opok_delete;       
          IF (upper(rf_json_ext.get_string(rf_json(v_clob_details),'message')) LIKE
             UPPER('%RF_OPOKRULE_OPOK_FK%')) THEN
             v_clob := rf_pkg_request.exec_exception('��������� ������ ��� �������� ����!' ||
                                       chr(13) ||
                                       '��� ������� ���� ���������� �������!' ||
                                       chr(13) ||
                                       '����� ��������� ���� ���������� ������� ������� ��� ����.' ||
                                       chr(13) || sqlerrm);
          END IF;
          RETURN rf_json(v_clob);
        END IF;
      END loop;
    RETURN rf_json(v_clob);          
  END opok_delete;          

   /* ����� ��� �������� ����������� ��������� ��� OPOKDetails 
   *
   * ��������� ������
   *   p_opok_nb - ������������� ����
   * ��������� ������
   *   BOOLEAN - true - ��� ���������, false - ���� �����������
   */
  FUNCTION check_opok_details_dates( 
    p_opok_nb IN NUMBER)
  RETURN BOOLEAN IS
    v_cnt NUMBER;
  BEGIN
    -- ��������� ������ ����������� �������� ����
    -- ���� ����������� ����, �� �������� �� ������
    SELECT count(*)
      INTO v_cnt
      FROM mantas.rf_opok_details o1
         JOIN mantas.rf_opok_details o2
           ON o1.opok_nb = o2.opok_nb
              AND NVL(o1.start_dt, TO_DATE('01-01-0001', 'DD-MM-YYYY')) <= NVL(o2.end_dt, TO_DATE('31-12-9999', 'DD-MM-YYYY'))
              AND NVL(o1.end_dt, TO_DATE('31-12-9999', 'DD-MM-YYYY')) >= NVL(o2.start_dt, TO_DATE('01-01-0001', 'DD-MM-YYYY'))
              AND o1.record_seq_id <> o2.record_seq_id
      WHERE o1.opok_nb = p_opok_nb;
    IF v_cnt > 0 THEN
      RETURN FALSE;
    END IF;
    RETURN TRUE;
  END check_opok_details_dates;
  
  /* ����� ��� ��������� ������� OPOKDetails create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  FUNCTION opok_details_create( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_json      rf_json;
  BEGIN
    SAVEPOINT sp_opok_details_create;
    v_json := dict_create( p_json, p_user, p_id_request,'RF_OPOK_DETAILS', 'RECORD_SEQ_ID','RF_OPOK_DETAILS_SEQ' );
    IF NOT check_opok_details_dates(rf_json_ext.get_number(rf_json_ext.get_json(p_json,'data[1]'),'OPOK_NB')) THEN
      ROLLBACK TO SAVEPOINT sp_opok_details_create;
      RETURN rf_json(rf_pkg_request.exec_exception('������� �������� ������������!' ||chr(13)|| '������ �� �������!'));
    END IF;
    RETURN v_json;
  END opok_details_create;
 
  /* ����� ��� ��������� ������� OPOKDetails update
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  FUNCTION opok_details_update( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_json      rf_json;
    v_opok_nb NUMBER;
  BEGIN
    SAVEPOINT sp_opok_details_update;
    v_json := dict_update( p_json, p_user, p_id_request,'RF_OPOK_DETAILS', 'RECORD_SEQ_ID' );
    -- ��������� ������ ����������� �������� ����
    -- ���� ����������� ����, �� ������� ��������! ������ ������.
    SELECT opok_nb INTO v_opok_nb FROM mantas.rf_opok_details WHERE record_seq_id=rf_json_ext.get_number(rf_json_ext.get_json(p_json,'data[1]'),'_id_');
    IF NOT check_opok_details_dates(v_opok_nb) THEN
      ROLLBACK TO SAVEPOINT sp_opok_details_update;
      RETURN rf_json(rf_pkg_request.exec_exception('������� �������� ������������!' ||chr(13)|| '������ �� ���������!'));
    END IF;
    RETURN v_json;
  END opok_details_update;
  
   /* ����� ��� ��������� ������� OPOKCriteria create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  FUNCTION opok_criteria_create( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_clob    CLOB;
  BEGIN
    v_clob := rf_pkg_request.exec_sql_insert(rf_pkg_request.insert_generate(p_schema      => 'MANTAS',
                                              p_table_name  => 'RF_OPOK_CRITERIA',
                                              p_column_name => null,
                                              p_seq_name    => null,
                                              p_key_name    => 'CRIT_CD',
                                              p_data        => rf_json_ext.get_json(p_json,
                                                                                    'data[1]')),
                              p_id_req => p_id_request,
                              p_id => rf_json_ext.get_string(rf_json_ext.get_json(p_json,
                                                                                  'data[1]'),
                                                             'CRIT_CD'),
                              p_clientid => rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                        'data[1]'),
                                                                   '_id_'),
                              p_schema => 'MANTAS',
                              p_table => 'RF_OPOK_CRITERIA',
                              p_id_name => 'CRIT_CD');
    RETURN rf_json(v_clob);
  END opok_criteria_create;

   /* ����� ��� ��������� ������� OPOKRule create
   *
   * ��������� ������
   *   p_json - json ������
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json ����� � ������� ����� ���������
   */
  FUNCTION opok_rule_create( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_clob        CLOB;
    v_clob_details CLOB;
    v_json        rf_json;
    v_number_one  NUMBER;
    v_cnt         NUMBER;
  BEGIN
    lock table mantas.rf_opok_rule in exclusive mode nowait;
    select nvl(max(rule_cd), 0)
      into v_number_one
      from mantas.rf_opok_rule;
    v_json := rf_json_ext.get_json(p_json, 'data[1]');
    -- put ������������, ���������� ����� update
    -- ������ �������, ��� ���� create, �� ����� � ������ ������� �� ���������
    v_json.put('RULE_CD', v_number_one + 1);
    v_json.put('VERS_NUMBER', 1);
    v_clob := rf_pkg_request.exec_sql_insert(rf_pkg_request.insert_generate(p_schema      => 'MANTAS',
                                                p_table_name  => 'RF_OPOK_RULE',
                                                p_column_name => 'RULE_SEQ_ID',
                                                p_seq_name    => 'RF_OPOK_RULE_SEQ',
                                                p_data        => v_json),
                                p_id_req => p_id_request,
                                p_id => rf_json_ext.get_number(v_json,
                                                               '_id_'),
                                p_clientid => rf_json_ext.get_number(v_json,
                                                                     '_id_'),
                                p_schema => 'MANTAS',
                                p_table => 'RF_OPOK_RULE',
                                p_id_name => 'RULE_SEQ_ID');
    v_cnt := 0;
    -- ����� ����, ��� ������� ���� ��������� ��������� ��������� ��� ����������
    -- ���� ������� �� ��������, �� ��� ����� �������� ��������������� �������
    -- �� ���� ����� ��������� ������ ����� ��������, � ������ ���� �������� ���� data, ������������ �� ������
    -- ��� ������ ����� �������������� ����������
    v_json := rf_json(v_clob);
    v_cnt := rf_pkg_scnro.check_and_mark_opok_rule(par_rule_seq_id => to_char(rf_json_ext.get_number(v_json,'data[1]._id_')));
    if v_cnt > 0 then
      v_clob_details := rf_pkg_request.exec_sql_singleread('select :id as "_id_", r.* from mantas.rf_opok_rule r where r.rule_seq_id = :id'
                                            ,to_char(rf_json_ext.get_number(v_json,'data[1]._id_'))
                                            ,p_id_request);
      if rf_json_ext.get_bool(rf_json(v_clob_details), 'success') then
        v_json.put('data',
                   rf_json_ext.get_json(rf_json(v_clob_details),
                                        'data[1]'));
      else
        v_json.put('success', false);
      end if;
    end if;

    RETURN v_json;
  END opok_rule_create;
  
  /* ����� ��� ��������� ������� OPOKRule update
  *
  * ��������� ������
  *   p_json - json ������
  *   p_user - ��� �������� ������������
  *   p_id_request - ���������������� ����� �������
  * ��������� ������
  *   rf_json - json ����� � ������� ����� ���������
  */
  FUNCTION opok_rule_update( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_clob        CLOB;
    v_json        rf_json;
    v_cnt         NUMBER;
    type t_tab is table of mantas.rf_opok_rule.rule_seq_id%type index by pls_integer;
    v_tab  t_tab;
    v_ids_ clob;
  BEGIN
    lock table mantas.rf_opok_rule in exclusive mode nowait;
  
    if rf_json_ext.get_string(p_json, 'data[1].VERS_STATUS_CD') =
       'ACT' then
      update mantas.rf_opok_rule
         set vers_status_cd = 'ARC'
       where rule_cd in
             (select max(rule_cd)
                from mantas.rf_opok_rule
               where rule_seq_id =
                     rf_json_ext.get_number(p_json, 'data[1]._id_'))
         and vers_status_cd = 'ACT'
         and rule_seq_id <>
             rf_json_ext.get_number(p_json, 'data[1]._id_')
      returning rule_seq_id bulk collect into v_tab;
      if v_tab.count > 0 then
        v_ids_ := '[';
        for i in 1 .. v_tab.count loop
          v_ids_ := v_ids_ || v_tab(i) || ',';
          commit;
        end loop;
        v_ids_ := rtrim(v_ids_, ',');
        v_ids_ := v_ids_ || ']';
      end if;
    end if;
    v_clob := rf_pkg_request.exec_sql_update(
                  p_sql        => rf_pkg_request.update_generate(
                                    p_schema     => 'MANTAS',
                                    p_table_name => 'RF_OPOK_RULE',
                                    p_key_name   => 'RULE_SEQ_ID',
                                    p_data       => rf_json_ext.get_json(p_json,'data[1]')
                                  ),
                  p_bind_param => rf_json_ext.get_number( rf_json_ext.get_json( p_json, 'data[1]' ), '_id_' ),
                  p_id_req     => p_id_request,
                  p_schema     => 'MANTAS',
                  p_table      => 'RF_OPOK_RULE',
                  p_id_name    => 'RULE_SEQ_ID',
                  p_id_type    => 'NUMBER'
                );
    commit;
    v_cnt := rf_pkg_scnro.check_and_mark_opok_rule(par_rule_seq_id => to_char(rf_json_ext.get_number(p_json,'data[1]._id_')));
    if (v_tab.count > 0) and (v_cnt = 0) then
      for i in 1 .. v_tab.count loop
        v_cnt := v_cnt + rf_pkg_scnro.check_and_mark_opok_rule(par_rule_seq_id => to_char(v_tab(i)));
      end loop;
    end if;
  
    if (v_ids_ is not null) or (v_cnt > 0) then
      v_json := rf_json(v_clob);
      if (v_ids_ is null) then
        v_ids_ := '[' ||
                  to_char(rf_json_ext.get_number(p_json,
                                                 'data[1]._id_')) || ']';
      end if;
      v_json.put('_ids_', rf_json_list(v_ids_));
      v_json.to_clob(v_clob, true);
    end if; 
      
    RETURN rf_json(v_clob);
  END opok_rule_update;
  
  /* ����� ��� ��������� ������� OPOKRule create_version
  *
  * ��������� ������
  *   p_json - json ������
  *   p_user - ��� �������� ������������
  *   p_id_request - ���������������� ����� �������
  * ��������� ������
  *   rf_json - json ����� � ������� ����� ���������
  */
  FUNCTION opok_rule_create_version( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_clob        CLOB;
    v_json        rf_json;
  BEGIN
    lock table mantas.rf_opok_rule in exclusive mode nowait;    
    v_json := rf_json(rf_pkg_request.exec_sql_singleread('select :id as "_id_", rule_cd, (select nvl(max(vers_number), 0) +1 from mantas.rf_opok_rule where rule_cd = r.rule_cd) as vers_number, opok_nb, query_cd, priority_nb, tb_id, op_cat_cd, dbt_cdt_cd, acct_crit_cd, acct_add_crit_tx, acct_subtle_crit_tx, acct_area_tx, acct_null_fl, acct2_crit_cd, acct2_add_crit_tx, acct2_subtle_crit_tx, acct2_area_tx, acct2_null_fl, acct_reverse_fl, lexeme_crit_cd, lexeme_area_tx, lexeme_add_purpose_tx, lexeme_add_cust_tx, lexeme_add_cust2_tx, lexeme2_crit_cd, lexeme2_area_tx, lexeme2_add_purpose_tx, lexeme2_add_cust_tx, lexeme2_add_cust2_tx, optp_crit_cd, optp_add_crit_tx, src_crit_tx, custom_crit_cd, custom_crit_sql_tx, custom_crit_param_tx, explanation_crit_cd, explanation_sql_tx, start_dt, end_dt, ''' ||
                                          'TEST' ||
                                          ''' as vers_status_cd, err_fl, err_mess_tx, test_err_fl, test_err_mess_tx, note_tx, kgrko_party_cd, cust_watch_list_cd, cust_not_watch_list_fl, cust2_watch_list_cd, cust2_not_watch_list_fl from mantas.rf_opok_rule r where r.rule_seq_id = :id',
                                          to_char(rf_json_ext.get_number(p_json,
                                                                         '_id_')),
                                          p_id_request));
    v_json := rf_json_ext.get_json(v_json, 'data[1]');
    v_clob := rf_pkg_request.exec_sql_insert(rf_pkg_request.insert_generate(p_schema      => 'MANTAS',
                                                p_table_name  => 'RF_OPOK_RULE',
                                                p_column_name => 'RULE_SEQ_ID',
                                                p_seq_name    => 'RF_OPOK_RULE_SEQ',
                                                p_data        => v_json),
                                p_id_req => p_id_request,
                                p_id => rf_json_ext.get_number(v_json,
                                                               '_id_'),
                                p_clientid => rf_json_ext.get_number(v_json,
                                                                     '_id_'),
                                p_schema => 'MANTAS',
                                p_table => 'RF_OPOK_RULE',
                                p_id_name => 'RULE_SEQ_ID');                       
    RETURN rf_json(v_clob);
  END opok_rule_create_version;
  
  /* ����� ��� ��������� ������� WatchList create
  *
  * ��������� ������
  *   p_json - json ������
  *   p_user - ��� �������� ������������
  *   p_id_request - ���������������� ����� �������
  * ��������� ������
  *   rf_json - json ����� � ������� ����� ���������
  */
  FUNCTION watch_list_create( 
    p_json IN rf_json, 
    p_user IN VARCHAR2, 
    p_id_request IN NUMBER) 
  RETURN rf_json IS
    v_json      rf_json;
    v_clob    CLOB;
  BEGIN
  
    v_clob := rf_pkg_request.exec_sql_insert(rf_pkg_request.insert_generate(p_schema      => 'MANTAS',
                                                p_table_name  => 'RF_WATCH_LIST',
                                                p_column_name => null,
                                                p_seq_name    => null,
                                                p_key_name    => 'WATCH_LIST_CD',
                                                p_data        => rf_json_ext.get_json(p_json,
                                                                                      'data[1]')),
                                p_id_req => p_id_request,
                                p_id => rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                    'data[1]'),
                                                               'WATCH_LIST_CD'),
                                p_clientid => rf_json_ext.get_number(rf_json_ext.get_json(p_json,
                                                                                          'data[1]'),
                                                                     '_id_'),
                                p_schema => 'MANTAS',
                                p_table => 'RF_WATCH_LIST',
                                p_id_name => 'WATCH_LIST_CD');
    v_json:=rf_json(v_clob);    

    IF NOT rf_json_ext.get_bool(v_json, 'success') AND INSTR(rf_json_ext.get_string(v_json, 'message'),'ORA-00001')>0 THEN
      v_json:=rf_json();
      v_json.put('success',false);
      v_json.put('message','�������� � ����� "����� �������" ��� ����!');
    END IF;
    RETURN v_json;
  END watch_list_create;

 /* ������� ��� ��������� ������� ReportsList read
  *
  * ��������� �������
  *   p_json - json ������
  *   p_id_request - ���������������� ����� �������
  *   p_user - ��� ������������
  * ��������� ������
  *   rf_json - json �����
  */
  FUNCTION get_reports_list(p_json IN rf_json,p_id_request IN NUMBER, p_user IN VARCHAR2) RETURN rf_json IS
    v_sql VARCHAR2(2000);
    v_bind_json rf_json;
  BEGIN
    v_sql := q'{
      select distinct 
          regexp_substr(f.v_function_code, '\d+') as rep_id,
          f.v_function_name as rep_nm
        from ofsconf.cssms_usr_group_map ug
          join ofsconf.cssms_group_role_map gr on gr.v_group_code = ug.v_group_code 
          join ofsconf.cssms_role_function_map rf on rf.v_role_code = gr.v_role_code 
          join ofsconf.cssms_function_mast f on f.v_function_code = rf.v_function_code
        where ug.v_usr_id = :p_user and
          regexp_like(f.v_function_code, 'RFREP_\d+')
        order by 2 
      }';
   
    v_bind_json:=rf_json();
    v_bind_json.put('p_user',p_user);
    RETURN rf_pkg_request.exec_sql_read_json(v_sql, p_id_request, v_bind_json);
  END get_reports_list;

  /* ������� ��������� ��������� ������
   *   
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json
  is
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
  begin
      /* ������ ���� � ������ ��� "T" */
    v_json_in := rf_json(regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2'));
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');

    CASE 
      WHEN v_form_name = 'OES321TB_REF' AND v_action='read' THEN
        v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_tb, p_id_request );
        
      WHEN v_form_name = 'CUR_REF' AND v_action='read' THEN
        v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_currency, p_id_request );

      WHEN v_form_name = 'DOCTYPE_REF' AND v_action='read' THEN
          v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_doctype, p_id_request );

      WHEN v_form_name = 'REGION_REF' AND v_action='read' THEN
        v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_region, p_id_request ); 
       
      WHEN v_form_name = 'CNTRY_NUM_REF' AND v_action='read' THEN
        v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_country, p_id_request );
        
      WHEN v_form_name = 'ReportsList' AND v_action='read' THEN
        v_json_out := get_reports_list(  v_json_in, p_id_request, p_user );  

      WHEN v_form_name ='AssignRule' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_assign_rule, p_id_request );
          WHEN 'create' THEN v_json_out := dict_create( v_json_in, p_user, p_id_request,'RF_ASSIGN_RULE', 'RULE_SEQ_ID','RF_ASSIGN_RULE_SEQ' );
          WHEN 'update' THEN v_json_out := dict_update( v_json_in, p_user, p_id_request,'RF_ASSIGN_RULE', 'RULE_SEQ_ID' );
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_ASSIGN_RULE', 'RULE_SEQ_ID' );
          ELSE NULL;
        END CASE;

      WHEN v_form_name ='AssignReplacement' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := get_assign_replacement( v_json_in, p_id_request ); 
          WHEN 'create' THEN v_json_out := dict_create( v_json_in, p_user, p_id_request,'RF_ASSIGN_REPLACEMENT', 'REPLACEMENT_SEQ_ID','RF_ASSIGN_REPLACEMENT_SEQ' );
          WHEN 'update' THEN v_json_out := dict_update( v_json_in, p_user, p_id_request,'RF_ASSIGN_REPLACEMENT', 'REPLACEMENT_SEQ_ID' );
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_ASSIGN_REPLACEMENT', 'REPLACEMENT_SEQ_ID' ); 
          ELSE NULL;
        END CASE;

      WHEN v_form_name ='AssignLimit' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_assign_limit, p_id_request );
          WHEN 'create' THEN v_json_out := dict_create( v_json_in, p_user, p_id_request,'RF_ASSIGN_LIMIT', 'LIMIT_SEQ_ID','RF_ASSIGN_LIMIT_SEQ' );
          WHEN 'update' THEN v_json_out := dict_update( v_json_in, p_user, p_id_request,'RF_ASSIGN_LIMIT', 'LIMIT_SEQ_ID' );
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_ASSIGN_LIMIT', 'LIMIT_SEQ_ID' );  
          ELSE NULL;
        END CASE;
 
      WHEN v_form_name ='OPOK' THEN
         CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_opok, p_id_request );
          WHEN 'create' THEN v_json_out := opok_create( v_json_in, p_user, p_id_request);
          WHEN 'update' THEN v_json_out := dict_update( v_json_in, p_user, p_id_request,'RF_OPOK', 'OPOK_NB' );
          WHEN 'delete' THEN v_json_out := opok_delete( v_json_in, p_user, p_id_request);  
          ELSE NULL;
        END CASE;

       WHEN v_form_name ='OPOKDetails' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_opok_details, p_id_request );
          WHEN 'create' THEN v_json_out := opok_details_create( v_json_in, p_user, p_id_request);
          WHEN 'update' THEN v_json_out := opok_details_update( v_json_in, p_user, p_id_request);
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_OPOK_DETAILS', 'RECORD_SEQ_ID' );  
          ELSE NULL;
        END CASE;

       WHEN v_form_name ='OPOKCriteria' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_opok_criteria, p_id_request );
          WHEN 'create' THEN v_json_out := opok_criteria_create( v_json_in, p_user, p_id_request);
          WHEN 'update' THEN v_json_out := dict_update_by_rowid( v_json_in, p_user, p_id_request,'RF_OPOK_CRITERIA', 'CRIT_CD' );
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_OPOK_CRITERIA', 'CRIT_CD' );  
          ELSE NULL;
        END CASE;      
        
      WHEN v_form_name ='OPOKRule' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_opok_rule, p_id_request );
          WHEN 'create' THEN v_json_out := opok_rule_create( v_json_in, p_user, p_id_request);
          WHEN 'update' THEN v_json_out := opok_rule_update( v_json_in, p_user, p_id_request);
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_OPOK_RULE', 'RULE_SEQ_ID' );  
          WHEN 'create_version' THEN v_json_out := opok_rule_create_version( v_json_in, p_user, p_id_request);
          ELSE NULL;
        END CASE;     

      WHEN v_form_name ='WatchList' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_watch_list, p_id_request );
          WHEN 'create' THEN v_json_out := watch_list_create( v_json_in, p_user, p_id_request);
          WHEN 'update' THEN v_json_out := dict_update_by_rowid( v_json_in, p_user, p_id_request, 'RF_WATCH_LIST', 'WATCH_LIST_CD' );
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_WATCH_LIST', 'WATCH_LIST_CD' );
          ELSE NULL;
        END CASE;     

      WHEN v_form_name ='WatchListOrg' THEN
        CASE v_action
          WHEN 'read'   THEN v_json_out := rf_pkg_request.exec_sql_read_json( c_sql_watch_list_org, p_id_request, rf_json('{"P_WATCH_LIST_CD":"'||rf_json_ext.get_string(rf_json(p_request),'watch_list_cd')||'"}') );
          WHEN 'create' THEN v_json_out := dict_create( v_json_in, p_user, p_id_request,'RF_WATCH_LIST_ORG', 'WATCH_LIST_ORG_ID','RF_WATCH_LIST_ORG_SEQ' );
          WHEN 'update' THEN v_json_out := dict_update( v_json_in, p_user, p_id_request,'RF_WATCH_LIST_ORG', 'WATCH_LIST_ORG_ID' );
          WHEN 'delete' THEN v_json_out := dict_delete( v_json_in, p_id_request, 'RF_WATCH_LIST_ORG', 'WATCH_LIST_ORG_ID' );       
          ELSE NULL;
        END CASE;  

      ELSE NULL;
    END CASE;
  
    return v_json_out;
  end;

end;
/
