DELETE FROM mantas.rf_report_auto_items;
DELETE FROM mantas.rf_report_auto;
--
-- ��������� ����� �������� ��� ��� - ����� ��������� (��, � ������� ��� �� ������������ �����������)
--
INSERT INTO mantas.rf_report_auto(v_function_code, rep_build_moment, email_subject_sql, email_body_sql, email_from)
VALUES('RFMAIL1003', 'AFTER_ALERTS', '''AML - ��������� ����� �������� ��� ���''', 
'''��� �������� ������ �� ���������� � ������� AML ���������� ��������� � ����������� ����� �������� ��� ��� - ��. ����������� � ������ �����''',
'noanswer@aml.sbrf.ru');

INSERT INTO mantas.rf_report_auto_items(v_function_code, rep_no, app_id, app_page_id, rep_params, rep_build_condition_sql, rep_after_action_sql, rep_file_nm_sql, rep_content_type)
VALUES('RFMAIL1003', 1, 1003, 1, 
  mantas.RF_TAB_KEY_VALUE(  
    mantas.RF_TYPE_KEY_VALUE('P_STATUS', '''1'''),
    mantas.RF_TYPE_KEY_VALUE('P_CHANGE_FROM', '''01.01.0001'''),
    mantas.RF_TYPE_KEY_VALUE('P_CHANGE_TO', '''01.01.9999'''),
    mantas.RF_TYPE_KEY_VALUE('P_ALL', 'to_char(null)'),  
    mantas.RF_TYPE_KEY_VALUE('P_IS_USED', 'to_char(null)'),  
    mantas.RF_TYPE_KEY_VALUE('P_FIND_NSI_OP_ID', 'to_char(null)'),  
    mantas.RF_TYPE_KEY_VALUE('P_ROWS', '1000000000'),
    mantas.RF_TYPE_KEY_VALUE('P_HIDE_PARAM', '''Y'''),
    mantas.RF_TYPE_KEY_VALUE('P_STATUS_CD', '''N''')
    ),   
  'SELECT count(*) FROM business.rf_nsi_op_type_changes WHERE case when STATUS_CD = ''N'' then 1 end = 1',
  'BEGIN '||
    'UPDATE business.rf_nsi_op_type_changes '||
       'SET STATUS_CD = ''S'', '||
           'LAST_STATUS_DT = SYSDATE '||
     'WHERE case when STATUS_CD = ''N'' then 1 end = 1; '||
    'COMMIT; '||
  'END;',
  '''����� �� ��������� ����� �������� ��� ���.html''',
  'text/html');
--
-- ��������� ����� �������� ��� ��� - ����� ��������� (��, � ������� ��� �� ������������ �����������)
--
INSERT INTO mantas.rf_report_auto(v_function_code, rep_build_moment, email_subject_sql, email_body_sql, email_from)
VALUES('RFMAIL1004', 'AFTER_ALERTS', '''AML - ��������� ����� �������� ��� ���''', 
'''��� �������� ������ �� ���������� � ������� AML ���������� ��������� � ����������� ����� �������� ��� ��� - ��. ����������� � ������ �����''',
'noanswer@aml.sbrf.ru');

INSERT INTO mantas.rf_report_auto_items(v_function_code, rep_no, app_id, app_page_id, rep_params, rep_build_condition_sql, rep_after_action_sql, rep_file_nm_sql, rep_content_type)
VALUES('RFMAIL1004', 1, 1004, 1, 
  mantas.RF_TAB_KEY_VALUE(
    mantas.RF_TYPE_KEY_VALUE('P_STATUS', '''1'''),
    mantas.RF_TYPE_KEY_VALUE('P_CHANGE_FROM', '''01.01.0001'''),
    mantas.RF_TYPE_KEY_VALUE('P_CHANGE_TO', '''01.01.9999'''),
    mantas.RF_TYPE_KEY_VALUE('P_ALL', 'to_char(null)'),  
    mantas.RF_TYPE_KEY_VALUE('P_IS_USED', 'to_char(null)'),  
    mantas.RF_TYPE_KEY_VALUE('P_FIND_NSI_TR_ID', 'to_char(null)'),  
    mantas.RF_TYPE_KEY_VALUE('P_ROWS', '1000000000'),
    mantas.RF_TYPE_KEY_VALUE('P_HIDE_PARAM', '''Y'''),
    mantas.RF_TYPE_KEY_VALUE('P_STATUS_CD', '''N''')
    ), 
  'SELECT count(*) FROM business.rf_nsi_tr_type_changes WHERE case when STATUS_CD = ''N'' then 1 end = 1',
  'BEGIN '||
    'UPDATE business.rf_nsi_tr_type_changes '||
       'SET STATUS_CD = ''S'', '||
           'LAST_STATUS_DT = SYSDATE '||
     'WHERE case when STATUS_CD = ''N'' then 1 end = 1; '||
    'COMMIT; '||
  'END;',
  '''����� �� ��������� ����� �������� ��� ���.html''',
  'text/html');
--
-- ����� � �������� ������ � ��������� �������� - ��������������
--
/*
INSERT INTO mantas.rf_report_auto(v_function_code, rep_build_moment, email_subject_sql, email_body_sql, email_from)
VALUES('RFMAIL1007', 'AFTER_FULL_LOAD', 
q'{SELECT 'AML - ����� � ���������� �������� � ���������. ��������� ��������: '||
       case when fatal_qty > 0 then 'FATAL'
            when error_qty > 0 then 'ERROR'
            when warn_qty > 0 then 'WARN'
            else 'SUCCESS'
       end  
  FROM (select nvl(sum(case when log_level = 'WARN' then 1 end), 0) as warn_qty,
               nvl(sum(case when log_level = 'ERROR' then 1 end), 0) as error_qty,
               nvl(sum(case when log_level = 'FATAL' then 1 end), 0) as fatal_qty
          from std.log_items
         where log_id = std.pkg_log.get_log and
               log_level in ('WARN', 'ERROR', 'FATAL'))}', 
'''����� �� ��������� ������� ���������� �������� ������ � ��������� ��������, ���������� ������������� ��������''',
'noanswer@aml.sbrf.ru');

INSERT INTO mantas.rf_report_auto_items(v_function_code, rep_no, app_id, app_page_id, rep_params, rep_build_condition_sql, rep_after_action_sql, rep_file_nm_sql, rep_content_type)
VALUES('RFMAIL1005', 1, 1005, 1, 
  mantas.RF_TAB_KEY_VALUE(
    mantas.RF_TYPE_KEY_VALUE('P_LOG_ID', 'to_char(std.pkg_log.get_log)')
    ), 
  'SELECT case when std.pkg_log.get_log is not null then 1 else 0 end FROM dual',
  null,
  '''AML - ����� � �������� ������ � ���������.html''',
  'text/html');
*/

commit;

GRANT SELECT, UPDATE ON business.rf_nsi_op_type_changes TO MANTAS;
GRANT SELECT, UPDATE ON business.rf_nsi_tr_type_changes TO MANTAS;
