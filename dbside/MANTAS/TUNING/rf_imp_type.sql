-- ���� ������� RF_IMP_TYPE
MERGE INTO mantas.rf_imp_type t USING
(SELECT 'CBRF_BNKSEEK' as imp_type_cd,
        '������ ����������� ����������� ��� �� ��' as imp_type_nm,
        'EXT_TABLE_DIR' as directory_nm,
        'bnkseek.dbf' as file_nm,
        'select sys.anydata.convertDate(max(dt_izm)) from business.rf_cbrf_bnkseek' as date_sql_tx,
        'select sys.anydata.convertNumber(count(*)) from business.rf_cbrf_bnkseek' as qty_sql_tx,
        'Y' as active_fl,
        null as note_tx,
        '{dateAct: true}' as params_tx,
        'N' as multiple_files_fl,
        null filter_sys_tx,
		1001 as access_nb
   FROM dual
 UNION ALL
 SELECT 'CBRF_BNKDEL',
        '������ ����������� ��������� (�������������) ��� �� ��',
        'EXT_TABLE_DIR',
        'bnkdel.dbf',
        'select sys.anydata.convertDate(max(dt_izm)) from business.rf_cbrf_bnkdel',
        'select sys.anydata.convertNumber(count(*)) from business.rf_cbrf_bnkdel',
        'Y',
        null,
        '{dateAct: true}',
        'N',
        null,
		1002
   FROM dual
 UNION ALL
 SELECT 'SBRF_321XML',
        '������ ��������� ��� �� XML (����������� ����� ��).',
        null,
        null,
        null,
        null,
        'Y',
        null,
        '{ownerId: true}',
        'Y',
        '!600,!#UNKNOWN#',
		1003
   FROM dual
 UNION ALL
 SELECT 'SBRF_321XML_8001',
        '������ ��������� ��� �� XML (8001 ���).',
        null,
        null,
        null,
        null,
        'Y',
        null,
        '{ownerId: true}',
        'Y',
        '!600,!#UNKNOWN#',
		1004
   FROM dual) v
ON (v.imp_type_cd = t.imp_type_cd)
WHEN NOT MATCHED THEN   
  INSERT (imp_type_cd, imp_type_nm, directory_nm, file_nm, date_sql_tx, qty_sql_tx, active_fl, note_tx, params_tx, multiple_files_fl, filter_sys_tx, access_nb)
  VALUES (v.imp_type_cd, v.imp_type_nm, v.directory_nm, v.file_nm, v.date_sql_tx, v.qty_sql_tx, v.active_fl, v.note_tx, v.params_tx, v.multiple_files_fl, v.filter_sys_tx, v.access_nb)
WHEN MATCHED THEN   
  UPDATE
     SET imp_type_nm = v.imp_type_nm, 
         directory_nm = v.directory_nm, 
         file_nm = v.file_nm, 
         date_sql_tx = v.date_sql_tx, 
         qty_sql_tx = v.qty_sql_tx, 
         active_fl = v.active_fl, 
         note_tx = v.note_tx,
         params_tx = v.params_tx,
         multiple_files_fl = v.multiple_files_fl,
         filter_sys_tx = v.filter_sys_tx,
		 access_nb = v.access_nb;


-- ������ �� ����� ���� ��� ����� ������� - RF_IMP_SYS
MERGE INTO mantas.rf_imp_sys s USING (
SELECT 'SBRF_321XML' as imp_type_cd
     , '*' as sys_cd
     , '5005,8001' as filter_opok_tx
  from dual
UNION ALL
SELECT 'SBRF_321XML_8001'
     , '*'
     , '8001'
  from dual) d
ON (d.imp_type_cd = s.imp_type_cd and d.sys_cd = s.sys_cd)
WHEN NOT MATCHED THEN 
  INSERT (s.imp_type_cd, s.sys_cd, s.filter_opok_tx)
  VALUES (d.imp_type_cd, d.sys_cd, d.filter_opok_tx)
WHEN MATCHED THEN
  UPDATE SET s.filter_opok_tx = d.filter_opok_tx;

commit;