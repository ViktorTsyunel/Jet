MERGE INTO mantas.rf_oes_check_rule t USING 
  (
select rule_seq_id, form_cd, desc_tx, check_sql_tx, msg_sql_tx, col_list_tx, block_list_tx, critical_fl, active_fl, err_fl, err_mess_tx, note_tx, created_by, created_date, modified_by, modified_date from mantas.rf_oes_check_rule where 1 <> 1 union all
select 101, '321', 'VERSION ������ ���� 2', 'oes.VERSION is null or oes.VERSION not in (''2'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 2''', 'VERSION', null, 'Y', 'Y', 'N', null, '110101', null, null, null, null from dual union all
select 102, '321', 'ACTION ������ ���� 1, 2, 3 ��� 4', 'oes.ACTION is null or oes.ACTION not in (''1'',''2'',''3'',''4'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 1, 2, 3 ��� 4.''', 'ACTION', null, 'Y', 'Y', 'N', null, '110201', null, null, null, null from dual union all
select 103, '321', 'DATE_S ����������� ������ ��� ����� 6001, 8001, 5003, 5007', 'oes.DATE_S is null or (oes.VO not in (6001,8001,5003,5007) and DATE_S <> to_date(''01.01.2099'', ''dd.mm.yyyy''))', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ����� �������� (VO), �������� ��: 6001,8001,5003,5007 ������ ���� ��������: 01.01.2099.''', 'DATE_S', null, 'Y', 'Y', 'N', null, '110504', null, null, null, null from dual union all
select 104, '321', 'DATE_S (���� ���������) �� ����� ���� ������ DATA (���� ��������)', 'oes.DATE_S < oes.DATA', '''���� ��������� (DATE_S): ''||to_char(oes.DATE_S, ''dd.mm.yyyy'')||'' �� ����� ���� ������ ���� �������� (DATA): ''||to_char(oes.DATA, ''dd.mm.yyyy'')', 'DATE_S', null, 'Y', 'Y', 'N', null, '110502', null, null, null, null from dual union all
select 105, '321', '����� TEL (���������� �������) �� ������ 6', 'oes.TEL is null or length(oes.TEL) < 6', '''�� ������� ��� ������� ����������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#', 'TEL', null, 'Y', 'Y', 'N', null, '110601', null, null, null, null from dual union all
select 106, '321', '����������������� ���� ������ ���� 0', '#FIELD_SQL# is null or #FIELD_SQL# <> ''0''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 0''', 'p0.RESRV_B2, p3.RESRV_B2, p4.RESERV612', null, 'Y', 'Y', 'N', null, '110701,110702,11703', null, null, null, null from dual union all
select 107, '321', '��� ��������� ����������� ������� �� 10 ��� 5 ���� (���)', '-- ��� ������� �� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null or
-- ��� ���������� ���� ������������
length(#FIELD_SQL#) not in (10, 5)', 'case when translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||'' - ���� �������� �� ������ �����.''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� (���) ��������� ����������� ������ �������� �� 10 (5) ����''
end     ', 'ND_KO', null, 'Y', 'Y', 'N', null, '210201', null, null, null, null from dual union all
select 108, '321', '������ 2 ����� ��� �������� ���������� ��� �������', '-- ��� ������� �� ����������� ���������� ����,
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and length(#FIELD_SQL#) in (10, 12) and
-- ��� �� 97 ������, ��������������� ������
substr(#FIELD_SQL#, 1, 2) not in (''97'') and
-- �� ������ ��� ����� �� �������� ���������� ����� �������
mantas.rf_pkg_rule.check_region(par_kladr_cd => substr(#FIELD_SQL#, 1, 2)) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� �� ������ �������� - ��� ������� � �����: ''||substr(#FIELD_SQL#, 1, 2)', 'OES.ND_KO, ND', 'ALL', 'Y', 'Y', 'N', null, '210202,412002,412004,412006,412008,412010,512002,512004,512006,512008,512010,612002,612004,612006,612008,612010,712002,712004,712006,712008,712010,812002,812004,812006,812008,812010', null, null, null, null from dual union all
select 109, '321', '����������� ����� � ��� - ����������', '-- ��� ������� �� ����������� ���������� ����,
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and length(#FIELD_SQL#) in (10, 12) and
-- ��� �� ��������� � ������ "������������" ���,
 #FIELD_SQL# <> ''3321020983'' and 
-- �� ��� �� �������� �������� �� ������������ ����������� ����
mantas.rf_pkg_rule.check_inn(#FIELD_SQL#) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� �� ������ �������� - �������� ����������� �����''', 'OES.ND_KO, ND', 'ALL', 'Y', 'Y', 'N', null, '210202,412002,412004,412006,412008,412010,512002,512004,512006,512008,512010,612002,612004,612006,612008,612010,712002,712004,712006,712008,712010,812002,812004,812006,812008,812010', null, null, null, null from dual union all
select 110, '321', '��� ����� ������� ���� � �����������', '-- ��� ����� ������
 #FIELD_SQL# not in (''0'', ''00'') and
-- ������ ���� ��� � �����������
mantas.rf_pkg_rule.check_region(par_okato_cd => #FIELD_SQL#) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������� � ����� ����� �����''', 'OES.KTU_S, OES.KTU_SS, AMR_S, ADRESS_S', 'ALL', 'Y', 'Y', 'N', null, '210301,210701,410301,410303,410304,411001,411003,411004,411005,510301,511001,511003,610301,611001,611003,710301,710303,710304,711001,711003,711004,711005,810301,811001,811003', null, null, null, null from dual union all
select 111, '321', '������������ ���� ������� (�� 0, ������)', '#FIELD_SQL# = ''0''', '''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#', 'OES.KTU_S, OES.CURREN, OES.PRIM_1', null, 'Y', 'Y', 'N', null, '210301,210701', null, null, null, null from dual union all
select 112, '321', '���, ����� � ����� ������� �� - ����������� (�� ����������� ��� ��)', 'mantas.rf_pkg_rule.check_bic_regn(oes.BIK_S, oes.REGN, 1, nullif(oes.NUMBF_S, ''0'')) = 0', '''��� ��������� ����������� (BIK_S): ''||oes.BIK_S||'', ����� ����� (REGN): ''||oes.REGN||'' � ����� ������� (NUMBF_S): ''||decode(oes.NUMBF_S, ''0'', ''<��� �������>'', oes.NUMBF_S)||'' �� ������� � ����������� ����������� ��� �� ��'' ', 'BIK_S', null, 'Y', 'Y', 'N', null, '210102,210103', null, null, null, null from dual union all
select 113, '321', '��� ����� � ��� �� (3-4 ����� BIK_S) ��������� � ����� � KTU_S', '-- ����� ��� ����������
length(trim(oes.BIK_S)) = 9 and
-- ����� KTU_S ����������
length(trim(oes.KTU_S)) = 2 and
-- ����� � KTU_S ���� � �����������
mantas.rf_pkg_rule.check_region(par_okato_cd => oes.KTU_S) = 1 and
-- ����� �� ��� �� ��������� � ����� � KTU_S
substr(oes.BIK_S, 3, 2) <> oes.KTU_S', '''��� ����� � ��� ��������� ����������� (BIK_S): ''||oes.BIK_S||'' (3-4 �����: ''||substr(oes.BIK_S, 3, 2)||'') �� ��������� � ����� ����� �� �� (KTU_S): ''||oes.KTU_S ', 'BIK_S', null, 'Y', 'Y', 'N', null, '210302', null, null, null, null from dual union all
select 114, '321', '��� ����� � ��� ������� �� (3-4 ����� BIK_SS) ��������� � ����� � KTU_SS', '-- ����� ��� ����������
length(trim(oes.BIK_SS)) = 9 and
-- ����� KTU_S ����������
length(trim(oes.KTU_SS)) = 2 and
-- ����� � KTU_SS ���� � �����������
mantas.rf_pkg_rule.check_region(par_okato_cd => oes.KTU_SS) = 1 and
-- ����� �� ��� �� ��������� � ����� � KTU_SS
substr(oes.BIK_SS, 3, 2) <> oes.KTU_SS', '''��� ����� � ��� ������� ��������� ����������� (BIK_SS): ''||oes.BIK_SS||'' (3-4 �����: ''||substr(oes.BIK_SS, 3, 2)||'') �� ��������� � ����� ����� �� �� (KTU_SS): ''||oes.KTU_SS ', 'BIK_SS', null, 'Y', 'N' /*������ �������� - ���������*/, 'N', null, '������ �������� - ���������', null, null, null, null from dual union all
select 115, '321', '��� ������� �� (BIK_SS) ���� � ����������� �� (����� ����������� ��� ���������)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ���� ��� � �����������
mantas.rf_pkg_rule.check_bic(#FIELD_SQL#) = 0
', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ��� ��� � ����������� �� �� �� ����� �����������, �� ����� ��������� �����''', 'BIK_SS', null, 'Y', 'Y', 'N', null, '210801', null, null, null, null from dual union all
select 116, '321', '���� KTU_SS, BIK_SS, NUMBF_SS �����������, ���� BRANCH = 1 � ������ ���� 0, ���� BRANCH = 0', ' -- �������� ������������� ��������
(oes.BRANCH = ''1'' and
 -- ������� ������� �� ������
 #FIELD_SQL# = ''0'')
OR
 -- �������� �� ������������� ��������
(oes.BRANCH = ''0'' and
 -- ������� ������� ������
 #FIELD_SQL# <> ''0'')
', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||
case when oes.BRANCH = ''1''
     then ''. ���� ������ ���� ���������, �. �. �������� ������������� ��������, �� ���������� ��� �������������� (BRANCH = 1)''
     when oes.BRANCH = ''0''
     then ''. ���� ������ ����� ��������: 0, �. �. �������� ���������� �������������� (BRANCH = 0)''
end     ', 'KTU_SS, BIK_SS, NUMBF_SS', null, 'Y', 'Y', 'N', null, '210702,210802,210902', null, null, null, null from dual union all
select 117, '321', '���� ������ ���� 0 ��� 1', '#FIELD_SQL# not in (''0'', ''1'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 0 ��� 1.''', 'BRANCH, PRIZ_SD', null, 'Y', 'Y', 'N', null, '210601', null, null, null, null from dual union all
select 118, '321', '���� ������ ���� 0, 1 ��� 2', '#FIELD_SQL# not in (''0'', ''1'', ''2'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 0, 1 ��� 2.''', 'TERROR, B_PAYER, B_RECIP, PART, p0.VP, p3.VP', null, 'Y', 'Y', 'N', null, '310101', null, null, null, null from dual union all
select 119, '321', '������ ���. ����� (DOP_V) �� �������� ��������', '-- ������ ����� ������
 #FIELD_SQL# <> ''0'' and
-- �������� �������
instr(#FIELD_SQL#, '' '') > 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ����� �� ������ ��������� �������, ������ ����������� ����������: "1001,1005,1008" (����� ������� ��� ��������).''', 'DOP_V', null, 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 120, '321', '������ �� ���. ����� (DOP_V) ���������� � ����������� ����� ����', '-- ������ ����� ������
 #FIELD_SQL# <> ''0'' and
-- �� �������� �������
instr(#FIELD_SQL#, '' '') = 0 and
-- ���� ����, ������������� � �����������
(select count(*)
   from table(mantas.rf_pkg_scnro.list_to_tab(#FIELD_SQL#)) t
        left join mantas.rf_opok opk on to_char(opk.opok_nb) = t.column_value
  where opk.opok_nb is null) > 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������� ����, �� ������������ � �����������: ''||
(select listagg(t.column_value, '','') within group(order by t.column_value)
   from table(mantas.rf_pkg_scnro.list_to_tab(#FIELD_SQL#)) t
        left join mantas.rf_opok opk on to_char(opk.opok_nb) = t.column_value
  where opk.opok_nb is null)||
''. ������ ����������� ����������: "1001,1005,1008" (����� ������� ��� ��������).''  ', 'DOP_V', null, 'Y', 'Y', 'N', null, '310301', null, null, null, null from dual union all
select 121, '321', '���. ���� (DOP_V) �� ����������� � �� ��������� �������� ��� (VO)', '-- ������ ����� ������
 #FIELD_SQL# <> ''0'' and
-- �� �������� �������
instr(#FIELD_SQL#, '' '') = 0 and
-- ���� ������������� ����
(select count(*)
   from table(mantas.rf_pkg_rule.dup_in_list_to_tab(oes.VO||'',''||#FIELD_SQL#))) > 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������� ������������� ���� ��� ����, ����������� �������� ���: ''||
(select listagg(t.column_value, '','') within group(order by t.column_value)
   from table(mantas.rf_pkg_rule.dup_in_list_to_tab(oes.VO||'',''||#FIELD_SQL#)) t)  ', 'DOP_V', null, 'Y', 'Y', 'N', null, '310302,310303', null, null, null, null from dual union all
select 122, '321', '���� �������� (DATE) between 01.02.2002 � ������� ����', '#FIELD_SQL# < to_date(''01.02.2002'', ''dd.mm.yyyy'') or
 #FIELD_SQL# > trunc(sysdate)', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ���� ��� ����������� ��������� (��������� � �������� ��� ������� �������� ��������)'' ', 'DATA', null, 'Y', 'Y', 'N', null, '310402', null, null, null, null from dual union all
select 123, '321', '����� ������ ������ 0', '#FIELD_SQL# <= 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#)||''. ����� ������ ���� ������ 0'' ', 'SUM, SUME', null, 'Y', 'Y', 'N', null, '310501,310601', null, null, null, null from dual union all
select 124, '321', '����� �������� � ���������� � ������ ������ ���� �����, ���� ������ - �����', '-- ������ �������� - �����
oes.CURREN = ''643'' and
-- ����� �� �����
oes.SUME <> oes.SUM', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#)||''. ����� ����������� ������ ���� ����� ����� ��������: ''||to_char(oes.SUM)||'', ���� ������ - �����'' ', 'SUME', null, 'Y', 'Y', 'N', null, '310602', null, null, null, null from dual union all
select 125, '321', '����� �������� �� ���� ������ �� ������� �� ����� ���� (��������� � ���.)', '-- ����� ������ ����
oes.SUME > 0 and
-- � ���� ���� ���� � ��������� ��������� ���� ����� ��������
(select count(*)
   from table(mantas.rf_pkg_scnro.list_to_tab(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V))) t
        join mantas.rf_opok_details od on to_char(od.opok_nb) = trim(t.column_value) 
  where oes.DATA between nvl(start_dt, to_date(''01.01.0001'', ''dd.mm.yyyy'')) and 
                         nvl(end_dt, to_date(''31.12.9999'', ''dd.mm.yyyy'')) and
        oes.SUME < od.limit_am) > 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#)||''. ''||
(select listagg(''����� ���� ���������� ��������: ''||to_char(od.limit_am)||
                '' ��� ���� (VO, DOP_V): ''||to_char(od.opok_nb)||
                '' �� ���� ��������: ''||to_char(oes.DATA, ''dd.mm.yyyy''), ''; '') within group(order by od.opok_nb)
   from table(mantas.rf_pkg_scnro.list_to_tab(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V))) t
        join mantas.rf_opok_details od on to_char(od.opok_nb) = trim(t.column_value) 
  where oes.DATA between nvl(start_dt, to_date(''01.01.0001'', ''dd.mm.yyyy'')) and 
        nvl(end_dt, to_date(''31.12.9999'', ''dd.mm.yyyy'')) and
        oes.SUME < od.limit_am)', 'SUME', null, 'Y', 'Y', 'N', null, '310502,310503', null, null, null, null from dual union all
select 126, '321', '��� ������ ���� � �����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ������ ��� � �����������
mantas.rf_pkg_rule.check_currency(#FIELD_SQL#, 0) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ������ ��� � �����������''', 'CURREN, CURREN_CON', null, 'Y', 'Y', 'N', null, '310701,311901', null, null, null, null from dual union all
select 127, '321', '��� �������/������� ������ (1003/1004) ������ �������� �� ����� ���� ������', '-- ������ �������� - �����
 #FIELD_SQL# = ''643'' and
-- ���. ��� ���. ��� ���� - 1003 ��� 1004
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''1003,1004'') = 1', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����� (VO, DOP_V): 1003, 1004 ������ �������� �� ����� ����: �����''', 'CURREN', null, 'Y', 'Y', 'N', null, '310704', null, null, null, null from dual union all
select 128, '321', '������ ��������� �� ����� ���� ������', '#FIELD_SQL# = ''643''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ��������� �� ����� ����: �����''', 'CURREN_CON', null, 'Y', 'Y', 'N', null, '311902', null, null, null, null from dual union all
select 129, '321', '����� ���������� (�� 0) ���� �� ����� 3 ��������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ������ 3
length(trim(#FIELD_SQL#)) < 3
', '''�� ������� ��� ������� �� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL# ', 'OES.PRIM_1, NAMEU, VD2, BP', 'ALL', 'Y', 'Y', 'N', null, '310801,400301,500301,600301,700301,800301', null, null, null, null from dual union all
select 130, '321', 'PRIM_2 ����� ���� ��������� (�� 0), ������ ���� ����� PRIM_1 ������ 200', '-- ������ ����� �������
oes.PRIM_2 <> ''0'' and 
-- ������ ����� �� ��������� ���������
oes.PRIM_1 <> ''0'' and
length(trim(oes.PRIM_1)) <= 200', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)". ''||
''�� ��������� ��������� ���� PRIM_1 - ����� ������: ''||to_char(length(trim(oes.PRIM_1)))||'', ��������: 254. ���� PRIM_2 ������ ���������: 0''', 'PRIM_2', null, 'Y', 'Y', 'N', null, '310901', null, null, null, null from dual union all
select 131, '321', '���� ���������� ��������� (DATE_PAY_D ��� REFER_R2) �����������, ���� ������ ����� ��������� (NUM_PAY_D), ����� - ������ ���� 01.01.2099', ' -- ������ ����� ���������� ���������
((oes.NUM_PAY_D <> 0 or oes.REFER_R2 <> ''0'') and
 -- �� ������� ����
 oes.DATE_PAY_D = to_date(''01.01.2099'', ''dd.mm.yyyy''))
OR
 -- ��� ������ ���������� ���������:
(oes.NUM_PAY_D = 0 and oes.REFER_R2 = ''0'' and
 -- ������� ����
 oes.DATE_PAY_D <> to_date(''01.01.2099'', ''dd.mm.yyyy''))', 'case when -- ������ ����� ���������� ���������
          (oes.NUM_PAY_D <> 0 or oes.REFER_R2 <> ''0'') and
          -- �� ������� ����
          oes.DATE_PAY_D = to_date(''01.01.2099'', ''dd.mm.yyyy'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ���� ������ ���� ������� (�� ������ ���� 01.01.2099), �. �. ������ ����� ���������� ��������� (NUM_PAY_D ��� REFER_R2)''
     when -- ��� ������ ���������� ���������:
          oes.NUM_PAY_D = 0 and oes.REFER_R2 = ''0'' and
          -- ������� ����
          oes.DATE_PAY_D <> to_date(''01.01.2099'', ''dd.mm.yyyy'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ���� ������ ��������� ��������: 01.01.2099, �. �. �� ������ ����� ���������� ��������� (NUM_PAY_D ��� REFER_R2)''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')
end', 'DATE_PAY_D', null, 'Y', 'Y', 'N', null, '311102,311103', null, null, null, null from dual union all
select 132, '321', '���� � ���� �� ������ ������� ����', '-- ������� ����
 #FIELD_SQL# <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ��� ������ �������
 #FIELD_SQL# > trunc(sysdate)', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ���� ��� ����������� ��������� - ��������� � ��������'' ', 'OES.DATE_PAY_D, MC2, VD6', 'ALL', 'Y', 'Y', 'N', null, '311102,412905,512905,612905,712905,812905', null, null, null, null from dual union all
select 133, '321', '��� ����. ������� ���� � �����������', ' -- ���� �������
 #FIELD_SQL# <> ''0'' and
 -- ���� ����. ������� ��� � �����������
 mantas.rf_pkg_rule.check_metal(#FIELD_SQL#) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ����. ������� ��� � �����������. ��� ����. ������� ������ ���� ������ � ������� ��������� ������ A ��� B''', 'METAL', null, 'Y', 'Y', 'N', null, '311201', null, null, null, null from dual union all
select 134, '321', 'PRIZ6001 ����������� ������ ��� ���� 6001', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ��� ���� 6001 � ���. ��� ���.
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''6001'') = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����� �������� (VO, DOP_V), �������� ��: 6001 ������ ���� ��������: 0''', 'PRIZ6001', null, 'Y', 'Y', 'N', null, '311302', null, null, null, null from dual union all
select 135, '321', '���� ���� ���������� (TU4 <> 0), �� PART �� ����� ���� 0', 'p4.TU <> ''0'' and oes.PART = ''0''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. �. �. ���� ����, �� ����� � �� ��������� ��������, ��������� �������� (TU4 <> 0), �� ������ ���� ��������: 1 ��� 2'' ', 'PART', null, 'Y', 'Y', 'N', null, '311602', null, null, null, null from dual union all
select 136, '321', '���� 6001 ��� ��� ACTION ����� 3 ��� 4, �� ���� - ������������', '-- ���� �� �������
 #FIELD_SQL# = ''0'' and
 -- ���. ��� ���. ��� ���� - 6001
(mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''6001'') = 1 
 OR
 oes.ACTION in (''3'', ''4''))', '''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ����������� ��������� ��� ���� (VO, DOP_V): 6001, � ����� � ������ ������� ������ ��� �������� ������ (ACTION = 3 ��� 4)''', 'DESCR_1', null, 'Y', 'Y', 'N', null, '311701', null, null, null, null from dual union all
select 137, '321', '����� ���������� (�� 0) ���� �� ����� 4 ��������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ������ 4
length(trim(#FIELD_SQL#)) < 4
', '''�� ������� ��� ������� �� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL# ', 'DESCR_1', null, 'Y', 'Y', 'N', null, '311701', null, null, null, null from dual union all
select 138, '321', 'DESCR_2 ����� ���� ��������� (�� 0), ������ ���� ����� DESCR_1 ������ 200', '-- ������ ����� �������
oes.DESCR_2 <> ''0'' and 
-- ������ ����� �� ��������� ���������
oes.DESCR_1 <> ''0'' and
length(trim(oes.DESCR_1)) <= 200', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)". ''||
''�� ��������� ��������� ���� DESCR_1 - ����� ������: ''||to_char(length(trim(oes.DESCR_1)))||'', ��������: 254. ���� DESCR_2 ������ ���������: 0''', 'DESCR_2', null, 'Y', 'Y', 'N', null, '311801', null, null, null, null from dual union all
select 139, '321', '����� ��������� (SUM_CON) ������ ���� 0, ���� CURREN_CON = 0, ����� - ������ ���� �������������', ' -- ������� ������ ���������
(oes.CURREN_CON <> 0 and
 -- �� �������/������� ������� ����� ���������
 oes.SUM_CON <= 0)
OR
 -- �� ������� ������ ���������
(oes.CURREN_CON = 0 and
 -- ������� ����� ���������
 oes.SUM_CON <> 0)', 'case when -- ������� ������ ���������
          oes.CURREN_CON <> 0 and
          -- �� �������/������� �������� ����� ���������
          oes.SUM_CON <= 0
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#)||''. ����� ��������� ������ ���� ������������, �. �. ������� ������ ��������� (CURREN_CON)''
     when -- �� ������� ������ ���������
          oes.CURREN_CON = 0 and
          -- ������� ����� ���������
          oes.SUM_CON <> 0
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#)||''. ���� ������ ��������� �������� 0, �. �. �� ������� ������ ��������� (CURREN_CON)''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#)
end', 'SUM_CON', null, 'Y', 'Y', 'N', null, '312001,312002', null, null, null, null from dual union all
select 140, '321', '����� ���������� (�� 0) ���� �� ����� 2 ��������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ������ 2
length(trim(#FIELD_SQL#)) < 2
', '''�� ������� ��� ������� �� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL# ', 'AMR_R, AMR_G, AMR_U, ADRESS_R, ADRESS_G, ADRESS_U', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 141, '321', '���. ���� (DOP_V) �� ����� ��������� 7001', '-- ������ ���. ����� ������
 #FIELD_SQL# <> ''0'' and
-- �������� ��� �� 7001
oes.VO <> 7001 and
-- ���. ���� �������� ��� 7001
mantas.rf_pkg_rule.check_lists_intersection(#FIELD_SQL#, ''7001'') = 1', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� 7001 ������ ���� ������ ��� �������� (VO)'' ', 'DOP_V', null, 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 142, '321', '�������� ��� (VO) �� ����� ���� 6001, ���� ���� ���. ����', '-- �������� ��� 6001
oes.VO = 6001 and
-- ���. ���� ����
oes.DOP_V <> ''0''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� 6001 �� ������ ���� �������� (VO), ���� ���� ������ ���� (DOP_V): ''||oes.DOP_V ', 'VO', null, 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 143, '321', '3000-� ��� 7000-� ���� (VO, DOP_V) ����� ���� ���������, ���� ������ ���� �� ���� ������� ��������� (PRU)', '-- ��� �������� ���������� ����� 0
p0.PR||p1.PR||p2.PR||p3.PR||p4.PR = ''00000'' and  
-- �������� 3000-� ��� 7000-� ���
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''3001,3011,3021,7001'') = 1', '''��� ���������� �������� ������ �� ����� (VO, DOP_V): 3001, 3011, 3021, 7001 ������ ���� ������ ������� ��������� ���� �� ��� ������ ��������� �������� (PRU0, PRU1, PRU2, PRU3 ��� PRU4)''', 'VO', null, 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 144, '321', '����� ���� �� ����� ������������', 'length(#FIELD_SQL#) > #FIELD_MAX_LENGTH#', '''������� ������� �������� � ���� "#FIELD_NAME# (#FIELD_CODE#)". ''||
''������� ����� ������ � ����: ''||to_char(length(#FIELD_SQL#))||'', ��������: #FIELD_MAX_LENGTH#''', 'OES.TEL, OES.REGN, OES.ND_KO, OES.BIK_S, OES.NUMBF_S, OES.BIK_SS, OES.NUMBF_SS, OES.DOP_V, OES.PRIM_1, OES.PRIM_2, OES.PRIZ6001, OES.DESCR_1, OES.DESCR_2, OES.REFER_R2, NAMEU, AMR_R, AMR_G, AMR_U, AMR_D, AMR_K, AMR_O, ADRESS_R, ADRESS_G, ADRESS_U, ADRESS_D, ADRESS_K, ADRESS_O, SD, RG, ND, VD1, VD2, VD5, MC1, BP, p0.ACC_B, p0.ACC_COR_B, p0.NAME_IS, p0.BIK_IS, p0.NAME_B, p0.BIK_B, p0.NAME_R, p0.BIK_R, p3.ACC_B, p3.ACC_COR_B, p3.NAME_IS, p3.BIK_IS, p3.NAME_B, p3.BIK_B, p3.NAME_R, p3.BIK_R', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 145, '321', 
       '������ ���� ���� (NUM_PAY_D ��� REFER_R2) ����� ���� ��������� ������������.',
       '-- ������ ���� ���� ����� ���� ��������� ������������
trim(#FIELD_SQL#) <> ''0'' and oes.NUM_PAY_D <> 0',
       '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ���� (NUM_PAY_D ��� REFER_R2) ����� ���� ��������� ������������.''',
       'REFER_R2', null, 'Y', 'Y', 'N', null, null, null, null, null, null
 from dual union all
select 146, '321', 
       '� ���� REFER_R2 ����� ���� ���� 0, ���� ���������� ��������, ���� �������� ����� 12 ��������.',
       '-- ����� ����� �������� ����� 12 �������� ������������ ���������
trim(#FIELD_SQL#) <> ''0'' and length(trim(#FIELD_SQL#)) <= 12 and translate(trim(#FIELD_SQL#), ''a1234567890'', ''a'') is null',
       '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. � ���� REFER_R2 ����� ���� ���� 0, ���� ���������� ��������, ���� �������� ����� 12 ��������.''',
       'REFER_R2', null, 'Y', 'Y', 'N', null, null, null, null, null, null
 from dual union all 
select 201, '321', '��� ��������� ������� �� 12, 10, 8 ��� 5 ���� � ����������� �� ��������', '-- ��� ������� �� ������ �� ����
translate(##.ND, ''a0123456789'', ''a'') is not null or
-- ��� ���������� ���� ������������
case
  when -- 0 �������� ���������� ���������
       ##.ND = ''0'' or
       -- ������� ����� ��� ��� �� - 10 ����
       (##.TU = 1 and length(##.ND) = 10) or
       -- ��-���������� ����� ����� ��� - 5 ����
       (##.TU = 1 and substr(trim(##.KODCR), 1, 3) <> ''643'' and length(##.ND) = 5) or
       -- �� � ������� ��������������� � �����/����������� ����� ����� ��� - 8 ����
       (##.TU = 1 and substr(trim(##.KODCR), 1, 3) = ''643'' and
        substr(trim(##.ADRESS_S), 1, 2) in (''35'', ''67'') and length(##.ND) = 8) or
       -- ������� ����� ��� ��� ��/�� - 12 ����
       (##.TU in (2, 3) and length(##.ND) = 12) or
       -- ��/�� � ������� ����������� � �����/����������� ����� ����� ��� - 10 ����
       (##.TU in (2, 3) and substr(trim(##.KODCN), 1, 3) = ''643'' and
        substr(trim(##.AMR_S), 1, 2) in (''35'', ''67'') and length(##.ND) = 10)
  then 1
  else 0
end = 0
', 'case when translate(##.ND, ''a0123456789'', ''a'') is not null
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||'' - ���� �������� �� ������ �����.''
     when ##.TU = 1 and substr(trim(##.KODCR), 1, 3) = ''643''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������������ ���� ������ �������� �� 10 ���� (��� ����� � ����������� �������� ��� �� 8 ����).''
     when ##.TU = 1 and substr(trim(##.KODCR), 1, 3) <> ''643''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������������ ���� ����������� ������ �������� �� 10 ����, ��� - �� 5 ����.''
     when ##.TU in (2, 3) and substr(trim(##.KODCN), 1, 3) = ''643''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����������� ����/�� ������ �������� �� 12 ���� (��� ����� � ����������� �������� ��� �� 10 ����).''
     when ##.TU in (2, 3) and substr(trim(##.KODCN), 1, 3) <> ''643''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����������� ����/�� ����������� ������ �������� �� 12 ����.''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. �������� ����� ���.''
end
', 'ND', 'ALL', 'Y', 'Y', 'N', null, '412001,412003,412005,412007,412009,512001,512003,512005,512007,512009,612001,612003,612005,612007,612009,712001,712003,712005,712007,712009,812001,812003,812005,812007,812009,', null, null, null, null from dual union all
select 202, '321', '������, ������ � ���. ����� � ������ ������� (�� 0), ���� ����� ���������� ��� ������� ��� ������ ��������', '-- ��� �� ����������� ��/��
NOT(#COLUMN_NM# = ''KODCN'' and ##.TU in (2, 3)) and
-- ������������ ������� ������ �� ������
 #FIELD_SQL# = ''0'' and
-- ����������� �������� ������������ ������ (�. �. ������������ ��������� ������):
(-- ���� �������� - ��� ������/��� ����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1) or
 -- ��� ���� ��������� �������� ������ (��� ���� ����������� ��������, ����� ������� ������ ������)
 ((#COLUMN_NM# like ''AMR%'' or #COLUMN_NM# = ''KODCR'') and
  ##.AMR_S||##.AMR_R||##.AMR_G||##.AMR_U||##.AMR_D||##.AMR_K||##.AMR_O not in (''0000000'', ''00000000'')) or
 ((#COLUMN_NM# like ''ADRESS%'' or #COLUMN_NM# = ''KODCN'') and
  ##.ADRESS_S||##.ADRESS_R||##.ADRESS_G||##.ADRESS_U||##.ADRESS_D||##.ADRESS_K||##.ADRESS_O not in (''0000000'', ''00000000''))
)', 'case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������� ����� ����������� ��������� ����� ''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������������������� ����, �� ����� � �� ��������� �������� ��������� ��������, ����������� ��������� ����� ''
     when (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1)
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������� ���������������� ����� �� �� ����������� ��������� ����� ''
     when ((#COLUMN_NM# like ''AMR%'' or #COLUMN_NM# = ''KODCR'') and
           ##.AMR_S||##.AMR_R||##.AMR_G||##.AMR_U||##.AMR_D||##.AMR_K||##.AMR_O not in (''0000000'', ''00000000'')) or
          ((#COLUMN_NM# like ''ADRESS%'' or (#COLUMN_NM# = ''KODCN'' and ##.TU not in (2,3))) and
           ##.ADRESS_S||##.ADRESS_R||##.ADRESS_G||##.ADRESS_U||##.ADRESS_D||##.ADRESS_K||##.ADRESS_O not in (''0000000'', ''00000000''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ����������� ��������� � ������ - ���� ��������� ��� ����, ���� ���������� � 0 ��������� ���� � ������ ''
end||
case when (#COLUMN_NM# like ''AMR%'' or #COLUMN_NM# = ''KODCR'') and ##.TU = 2
     then ''����� ���������� (�����������)''
     when #COLUMN_NM# like ''ADRESS%'' and ##.TU = 2
     then ''����� ����������''
     when #COLUMN_NM# like ''AMR%'' or #COLUMN_NM# = ''KODCR''
     then ''�����������''
     when #COLUMN_NM# like ''ADRESS%'' or #COLUMN_NM# = ''KODCN''
     then ''���������������''
end', 'KODCR, AMR_S, AMR_G, KODCN, ADRESS_S, ADRESS_G', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 203, '321', '00 � ���� ������� ������ ���� � ����������� ������� � �� ������ - � ����������', ' -- 00 (������� ������������ ������) �� ����� ���� ������:
(#FIELD_SQL# = ''00'' and
  -- ���� ��� ����� ����������� � ������ ����������� - ��
 ((#COLUMN_NM# like ''AMR%'' and substr(##.KODCR, 1, 3) = ''643'') or
  -- ���� ��� ����� ���������� �� � ������ ���������� �� - �� (� ��/�� � ������ ���������� - �����������)
  (##.TU = 1 and #COLUMN_NM# like ''ADRESS%'' and substr(##.KODCN, 1, 3) = ''643'')))
OR
 -- 00 (������� ������������ ������) ������ ���� ������:
(#FIELD_SQL# <> ''00'' and
  -- ���� ��� ����� ����������� � ������ ����������� - �� ��
 ((#COLUMN_NM# like ''AMR%'' and ##.KODCR <> ''0'' and substr(##.KODCR, 1, 3) <> ''643'') or
  -- ���� ��� ����� ���������� �� � ������ ���������� �� - �� �� (� ��/�� � ������ ���������� - �����������)
  (##.TU not in (2,3,0) and #COLUMN_NM# like ''ADRESS%'' and ##.KODCN <> ''0'' and substr(##.KODCN, 1, 3) <> ''643'')))', 'case when #FIELD_SQL# = ''00''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. �������� 00 ����� ���� ������� ������ ��� ������ ��� ��''||
          case when #COLUMN_NM# like ''AMR%''
               then '' (������ ��� ����� KODCR �� ����� 643)''
               when #COLUMN_NM# like ''ADRESS%''
               then '' (������ ��� ����� KODCN �� ����� 643)''
          end
     when #FIELD_SQL# <> ''00''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������ ��� �� ''||
          case when #COLUMN_NM# like ''AMR%''
               then ''(������ ��� ����� KODCR �� ����� 643)''
               when #COLUMN_NM# like ''ADRESS%''
               then ''(������ ��� ����� KODCN �� ����� 643)''
          end||
          '' ������ ��������� �������� 00''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end', 'AMR_S, ADRESS_S', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 204, '321', '������� ��������� ���������� (�� 0), ���� �������� ��� ��� (TU = 1, 2 ��� 3)', '-- �������� ��� ���������
 ##.TU in (''1'', ''2'', ''3'') and
-- ���� �� �������
 #FIELD_SQL# = ''0''', '''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ����������� ���������, ���� �������� ��� ��������� (TU = 1, 2 ��� 3)''', 'NAMEU', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 205, '321', '��� ������ ���� � �����������', '-- ��� ������ ������
 #FIELD_SQL# <> ''0'' and
-- ��� ����� ���������� �����
length(trim(#FIELD_SQL#)) = 5 and 
-- ��� �� ������ ��������: ��� 000 � ���� ����������� ��/��
NOT(#COLUMN_NM# = ''KODCN'' and ##.TU in (2, 3) and substr(#FIELD_SQL#, 1, 3) = ''000'') and
-- ������ ���� ��� � �����������
mantas.rf_pkg_rule.check_country(par_iso_numeric_cd => substr(#FIELD_SQL#, 1, 3)) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������ � ����� �����: ''||substr(#FIELD_SQL#, 1, 3)||''. ������ ����������� ����������: 64300''', 'KODCR, KODCN, p0.KODCN_B, p0.KODCN_R, p3.KODCN_B, p3.KODCN_R', 'ALL', 'Y', 'Y', 'N', null, '410201,410202,510201,510202,610201,610202,710201,710202,810201,810202,420702,720702,421001,721001', null, null, null, null from dual union all
select 206, '321', '���������� ����� ���������� � ���� ������ ����� 00', '-- ��� ������ ������
 #FIELD_SQL# <> ''0'' and
-- ��� �� ����������� ��/��
NOT(#COLUMN_NM# = ''KODCN'' and ##.TU in (2, 3)) and
 -- ����� �� ����� 5 
(length(trim(#FIELD_SQL#)) <> 5 or
 -- ��� 4-5 ����� �� 00 
 substr(#FIELD_SQL#, 4, 2) <> ''00'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� 5 ����: 1-3 ����� - ��� ������, 4-5 ����� - ����� ���������� - ������ 00. ������ ����������� ����������: 64300''', 'KODCR, KODCN, p0.KODCN_B, p0.KODCN_R, p3.KODCN_B, p3.KODCN_R', 'ALL', 'Y', 'Y', 'N', null, '410203,510203,610203,710203,810203', null, null, null, null from dual union all
select 207, '321', '������� ���� (������. ����. ����. ����) ����� 00, 01 ��� 02', '-- ��� ������ ������
 #FIELD_SQL# <> ''0'' and
-- ��� ����������� ��/��
 #COLUMN_NM# = ''KODCN'' and ##.TU in (2, 3) and
 -- ����� �� ����� 5 
(length(trim(#FIELD_SQL#)) <> 5 or
 -- ��� 4-5 ����� �� 00, 01, 02 
 substr(#FIELD_SQL#, 4, 2) not in (''00'',''01'',''02''))', '''������� ��������� ����������� ''||
decode(#BLOCK#, ''p0'', ''�����������'', ''p1'', ''������������� �����������'', ''p2'', ''������������� ����������'', ''p3'', ''����������'', ''p4'', ''����������'')||
'' ��/�� (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� 5 ����: 1-3 ����� - ��� ������ ��� 000 (��� �����������), 4-5 ����� - ������� ������������ ���������� ������������ ���� - 00, 01 ��� 02. ������ ����������� ����������: 64300''', 'KODCN', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 208, '321', '��� ��������� ���� � ����������� �� (����� ����������� ��� ���������)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
 -- ������ ����� - �� (������ � ���� ������ ���� ���)
((#COLUMN_NM# = ''BIK_B'' and substr(##.KODCN_B, 1, 3) = ''643'') or
 (#COLUMN_NM# = ''BIK_R'' and substr(##.KODCN_R, 1, 3) = ''643'') or
 -- ����� ���� - 9, ������ ��� ��� (� �� SWIFT)
 (#COLUMN_NM# = ''BIK_IS'' and length(trim(#FIELD_SQL#)) = 9)) and
-- ���� ��� � �����������
mantas.rf_pkg_rule.check_bic(#FIELD_SQL#) = 0
', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ��� ��� � ����������� �� �� �� ����� �����������, �� ����� ��������� �����''', 'BIK_B, BIK_R, BIK_IS', 'p0, p3', 'Y', 'Y', 'N', null, '420801,720801,421101,721101', null, null, null, null from dual union all
select 209, '321', '����� ���� SWIFT 8 ��� 11 ��������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ������ ����� - �� �� (������ � ���� ������ ���� SWIFT)
((#COLUMN_NM# = ''BIK_B'' and ##.KODCN_B <> ''0'' and substr(##.KODCN_B, 1, 3) <> ''643'') or
 (#COLUMN_NM# = ''BIK_R'' and ##.KODCN_R <> ''0'' and substr(##.KODCN_R, 1, 3) <> ''643'')) and
-- ����� ������������
length(trim(#FIELD_SQL#)) not in (8, 11)
', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ����� ���� SWIFT ������ ���� 8 ��� 11 ��������''', 'BIK_B, BIK_R', 'p0, p3', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 210, '321', '��� ������ �� ���� SWIFT ���� � �����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ���� SWIFT - ����������
length(trim(#FIELD_SQL#)) in (8, 11) and
-- ������ ����� - �� �� (������ � ���� ������ ���� SWIFT)
((#COLUMN_NM# = ''BIK_B'' and ##.KODCN_B <> ''0'' and substr(##.KODCN_B, 1, 3) <> ''643'') or
 (#COLUMN_NM# = ''BIK_R'' and ##.KODCN_R <> ''0'' and substr(##.KODCN_R, 1, 3) <> ''643'') or
 #COLUMN_NM# = ''BIK_IS'') and
-- ��� ������ � SWIFT ����������
mantas.rf_pkg_rule.check_country(par_cntry_cd => substr(#FIELD_SQL#, 5, 2)) = 0
', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������ � ���� SWIFT: ''||substr(#FIELD_SQL#, 5, 2)||'' ����������� � ����������� �����''', 'BIK_B, BIK_R, BIK_IS', 'p0, p3', 'Y', 'Y', 'N', null, '420804,720804,421102,721102', null, null, null, null from dual union all
select 211, '321', '����� ���/SWIFT 9, 8 ��� 11 ��������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ������ ����������
((#COLUMN_NM# = ''BIK_B'' and ##.KODCN_B = ''0'') or
 (#COLUMN_NM# = ''BIK_R'' and ##.KODCN_R = ''0'') or
 #COLUMN_NM# = ''BIK_IS'') and
-- ����� ������������
length(trim(#FIELD_SQL#)) not in (9, 8, 11)
', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ����� ���� ��� ������ ���� 9 ��������, ����� ���� SWIFT - 8 ��� 11 ��������''', 'BIK_B, BIK_R, BIK_IS', 'p0, p3', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 212, '321', '���� ����� "������" �����, �� ��� � ����� - �����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� "������" �����
 ##.CARD_B = ''1'' and
-- ��� � ���. ����� ������������
mantas.rf_pkg_rule.check_bic_regn(#FIELD_SQL#, oes.REGN) = 0
', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��������� ����� �������� ��, ��������������� ��������''||decode(#BLOCK#, ''p0'', '' (CARD_B0 = 1)'', ''p3'', '' (CARD_B3 = 1)'')||'', ��� ���� ��������� ���: ''||#FIELD_SQL#||'' � ����� �����: ''||oes.REGN||'' �� ������� � ����������� ����������� ��� �� ��'' ', 'BIK_IS', 'p0, p3', 'Y', 'Y', 'N', null, '420402,720402', null, null, null, null from dual union all
select 213, '321', '���� �������� - ������/��� ����, �� ���� (������� �����) ������ ���� 0', '(-- ���� �������� - ��� ������/��� ����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))) and
-- ���� �� ����
 #FIELD_SQL# <> ''0''
 ', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� �������� �������� �������� ��� ��, ��������������� ��������''||decode(#BLOCK#, ''p0'', '' (B_PAYER = 1 ��� 2)'', ''p3'', '' (B_RECIP = 1 ��� 2)'')||'', �� ���� ������ ����� ��������: 0'' ', 'KODCN_B, NAME_B, BIK_B', 'p0, p3', 'Y', 'Y', 'N', null, '420702,720702,420602,720602,420806,720806', null, null, null, null from dual union all
select 214, '321', '���� �������� - �� ������, �� ��� ��� ����� �� ������ ��������� � ��� �� (BIK_S, BIK_SS)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
 -- �������� - �� ������
((#BLOCK# = ''p0'' and oes.B_PAYER = ''0'') or
 (#BLOCK# = ''p3'' and oes.B_RECIP = ''0'')) and
-- ������ ��� ��/�������, ���������������� �������� 
(trim(#FIELD_SQL#) = trim(oes.BIK_S) or
 trim(#FIELD_SQL#) = trim(oes.BIK_SS))
 ', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� �������� �� �������� ��������''||decode(#BLOCK#, ''p0'', '' (B_PAYER = 0)'', ''p3'', '' (B_RECIP = 0)'')||'', �� ��� ��� ����� �� ����� ��������� � ��� ��/������� ��, ��������������� ��������'' ', 'BIK_B', 'p0, p3', 'Y', 'Y', 'N', null, '420805,720805', null, null, null, null from dual union all
select 215, '321', '���� ����� ���, �� ���� (������� �����-��������) ������ ���� 0', '-- ���� ����� ���
 ##.CARD_B = ''0'' and
-- ���� �� ����
 #FIELD_SQL# <> ''0''
 ', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ��� ���������� �������� �� �������������� ���������� �����''||decode(#BLOCK#, ''p0'', '' (CARD_B0 = 0)'', ''p3'', '' (CARD_B3 = 0)'')||'', �� ���� ������ ����� ��������: 0'' ', 'NAME_IS, BIK_IS', 'p0, p3', 'Y', 'Y', 'N', null, '420302,720302,420403,720403', null, null, null, null from dual union all
select 216, '321', 'CARD_B ����� 0, 1, 2 ��� 3', '##.CARD_B not in (''0'', ''1'', ''2'', ''3'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 0, 1, 2 ��� 3'' ', 'CARD_B', 'p0, p3', 'Y', 'Y', 'N', null, '420501,720501', null, null, null, null from dual union all
select 217, '321', '����� ���������� (�� 0) ���� �����������/���������� �� ������ 3 ��������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ������ 3
length(trim(#FIELD_SQL#)) < 3
', '''�� ������� ��� ������� �� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL# ', 'NAME_B, NAME_R, NAME_IS', 'p0, p3', 'Y', 'Y', 'N', null, '420601,720601,420901,720901,420301,720301', null, null, null, null from dual union all
select 218, '321', '������ � ������������ ����� (KODCN_B, NAME_B) �����������, ���� ������� �����-���� �������� ����� ���������', ' -- ���� ���� ��������� ���� - �������� �����
 ##.KODCN_B||##.NAME_B||##.BIK_B||##.ACC_B||##.ACC_COR_B <> ''00000'' and
-- � ������������ ������� ����� �� ������
 #FIELD_SQL# = ''0'' and 
-- ��� �� ��������, ����� ������� ������ ���� 0, �. �. �������� - ������/��� ����
NOT(((#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
     (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))) and
    #COLUMN_NM# in (''KODCN_B'', ''NAME_B'', ''BIK_B''))
 ', '''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ����������� ��������� ��� ������� � ��������� ����� ���������''', 'KODCN_B, NAME_B', 'p0, p3', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 219, '321', '������ � ������������ ����� (KODCN_R, NAME_R) �����������, ���� ������� �����-���� �������� �����, �����������/���������� ���������', ' -- ���� ���� ��������� ���� - �������� �����
 ##.KODCN_R||##.NAME_R||##.BIK_R <> ''000'' and
-- � ������������ ������� ����� �� ������
 #FIELD_SQL# = ''0''', '''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ����������� ��������� ��� ������� � ��������� �����, ����������� (���������� ��) �� ���������''', 'KODCN_R, NAME_R', 'p0, p3', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 220, '321', '������������ ����� (NAME_IS) �����������, ���� ������ ��� �����-��������', ' -- ���� ���� ��������� ���� - �������� �����
 ##.NAME_IS||##.BIK_IS <> ''00'' and
-- � ������������ ������� ����� �� ������
 #FIELD_SQL# = ''0''', '''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ����������� ��������� ��� ������� � ��������� �����-�������� �����''', 'NAME_IS', 'p0, p3', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 221, '321', '����� ����� � ����� �� ������� �� 20 ���� (�����, ��������, ������ ����� � ���� ������)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
 -- ������ ����� - �� 
(substr(##.KODCN_B, 1, 3) = ''643'' or
 -- ��� ���� � ����� �����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (1,2)) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (1,2))) and
 -- ����� ������ ����� ������������
(length(trim(#FIELD_SQL#)) <> 20 or
 -- ��� ����� (����� 6 �������) ������� �� ������ �� ����
 translate(substr(#FIELD_SQL#, 1, 5)||substr(#FIELD_SQL#, 7), ''a0123456789'', ''a'') is not null)', '''�� �������, ������� �� ��������� ��� ����������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� 20-������� ����� ����� - 20 ����, �����, ��������, ����� � 6 ������� (������ ������ ���� ������)'' ', 'ACC_B', 'p0, p3', 'Y', 'Y', 'N', null, '420101,720101,420103,720103', null, null, null, null from dual union all
select 222, '321', '������������ ��/�� ������ ��������� ���� �� ��� ����� (����������� ��������)', '-- �� ��� ��
 ##.TU in (''2'', ''3'') and
-- ������������ �� �������� �� ������ �������
instr(trim(#FIELD_SQL#), '' '') < 1', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. �� ������� ��������� �������, ��� ��� �������� ����������� ����, �� (������ ���� ���� �� ��� �����, ����������� ��������).''', 'NAMEU', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 223, '321', '��� �������� ����������� �� ���� �������� (�� ����������� ��)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ����������
length(trim(#FIELD_SQL#)) = 9 and
 -- ������ ����� - �� (������ � ���� ������ ���� ���)
((#COLUMN_NM# = ''BIK_B'' and substr(##.KODCN_B, 1, 3) = ''643'') or
 (#COLUMN_NM# = ''BIK_R'' and substr(##.KODCN_R, 1, 3) = ''643'') or
 #COLUMN_NM# = ''BIK_IS'') and
-- ���� �� �������� �����������
mantas.rf_pkg_rule.check_bic_active(#FIELD_SQL#, oes.DATA) = 0 and
-- ��� ����������
mantas.rf_pkg_rule.check_bic(#FIELD_SQL#) = 1', '''�������������� ��� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ��� �� �������� ����������� � ����������� �� �� �� ���� ��������: ''||to_char(oes.DATA, ''dd.mm.yyyy'')', 'BIK_B, BIK_R, BIK_IS', 'p0, p3', 'N', 'Y', 'N', null, null, null, null, null, null from dual union all
select 224, '321', '����������� ����� � ������ 20-�������� ����� - ����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
 -- ������ ����� - �� 
(substr(##.KODCN_B, 1, 3) = ''643'' or
 -- ��� ���� � ����� �����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (1,2)) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (1,2))) and
-- ����� ��� ����������
length(trim(##.BIK_B)) = 9 and
-- ����� ������ ����� ����������
length(trim(#FIELD_SQL#)) = 20 and
-- ����� (����� 6 �������) ������� �� ����
translate(substr(#FIELD_SQL#, 1, 5)||substr(#FIELD_SQL#, 7), ''a0123456789'', ''a'') is null and
-- ����������� ������ �� ���������
mantas.rf_pkg_rule.check_acct_nb(#FIELD_SQL#, ##.BIK_B) = 0 ', '''�������������� ��� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ����� 20-�������� ����� �� ������ �������� - �������� ����������� ����''', 'ACC_B', 'p0, p3', 'N', 'Y', 'N', null, null, null, null, null, null from dual union all
select 225, '321', 'TU ������ ���� 0, 1, 2, 3 ��� 4, ��� ���� 0 � 4 �������� �� ��� ���� ����������', '-- � ����������� � ���������� �� ����� ���� 0 (��� ���������)
(#BLOCK# in (''p0'', ''p3'') and #FIELD_SQL# not in (''1'', ''2'', ''3'', ''4''))
or 
-- � �������������� �����������/���������� � ���������� ����� ���� 0, �� ����� ���� 4 (�� ��������)
(#BLOCK# in (''p1'', ''p2'', ''p4'') and #FIELD_SQL# not in (''0'', ''1'', ''2'', ''3''))
', 'case when #BLOCK# in (''p0'', ''p3'') 
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 1, 2, 3 ��� 4.''
     when #BLOCK# in (''p1'', ''p2'', ''p4'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ���� ��������: 0, 1, 2, ��� 3.''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end', 'TU', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 226, '321', '���� �������� - ��� ����, �� TU = 1', '-- ���� �������� ��� ����
((#BLOCK# = ''p0'' and oes.B_PAYER = ''2'') or 
 (#BLOCK# = ''p3'' and oes.B_RECIP = ''2''))
-- � ��� ��������� �� ��
and #FIELD_SQL# <> ''1''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ���������� �������� ��, ��������������� �������� (B_PAYER/B_RECIP = 2), �� ���� ������ ��������� �������� 1''', 'TU', 'p0, p3', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 227, '321', '���� �������� - ������, �� TU <> 4', '-- ��� ��������� �� ���������
 #FIELD_SQL# = ''4'' and
 -- ���� �������� - ��� ������
((#BLOCK# = ''p0'' and oes.B_PAYER = ''1'') or 
 (#BLOCK# = ''p3'' and oes.B_RECIP = ''1'') or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1)) ', 'case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) 
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ���������� �������� �������� ������ (B_PAYER/B_RECIP = 1), �� ���� �� ����� ����� �������� 4''
     when (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1)
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ���������� �������� �������� ������ ���������������� ����� �� ��, �� ���� �� ����� ����� �������� 4''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end     
     ', 'TU', 'p0, p3', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 228, '321', '���� ����������������� ��������������� (VP0 ��� VP3 = 1), �� TU4 <> 0', '-- ���������� ���������������
(#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) and
-- ���������� ���
 #FIELD_SQL# = ''0''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ������������������� ��������������� (VP0 = 1 ��� VP3 = 1), �� ���� �� ����� ����� �������� 0''', 'TU', 'p4', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 229, '321', '������� ��������� (PRU) ������ ���� 0, ���� ���� (VO, DOP_V) ������� �� 3000-� � 7000-�', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �� � ��������, �� � ���. ����� ���� ��� 3000-�, 7000-�
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''3001,3011,3021,7001'') = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ����� ���� ������� �� 0 ������ ��� ����� (VO, DOP_V): 3001, 3011, 3021, 7001''', 'PR', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 230, '321', '������ ����� �������� ��������� (PRU) ����� 1, 2 ��� 3', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ������ ����� ������������
substr(#FIELD_SQL#, 1, 1) not in (''1'', ''2'', ''3'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ����� ������ ����: 1, 2 ��� 3'' ', 'PR', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 231, '321', '��� 3000-� ����� ������� ��������� (PRU) ������� �� 6 ����', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� �� ����� 6 ��� ������� �� ������ �� ����
(length(#FIELD_SQL#) <> 6 or
 translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null) and
-- ���� 3000-� ���� � ��� �� 7001
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''3001,3011,3021'') = 1 and
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''7001'') = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����� (VO, DOP_V): 3001, 3011, 3021 ���� ������ �������� �� 6 ����''', 'PR', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 232, '321', '��� 7000-� ����� ������� ��������� (PRU) ������� �� �����, ��� �� 2 ����', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ����� 2 ��� ������� �� ������ �� ����
(length(#FIELD_SQL#) < 2 or
 translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null) and
-- ��� 7001 ���
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''7001'') = 1 ', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� (VO): 7001 ���� ������ �������� �� ���� � ����� ����''', 'PR', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 233, '321', '��� 3000-� ����� ������� 2-6 ������� ��������� (PRU) �������� ��� ������ �� ������� ���������������� ����� (����, ����)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ����� 6 
length(#FIELD_SQL#) = 6 and
-- ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ���� 3000-� ���� � ��� �� 7001
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''3001,3011,3021'') = 1 and
mantas.rf_pkg_rule.check_lists_intersection(to_char(oes.VO)||decode(oes.DOP_V, ''0'', to_char(null), '',''||oes.DOP_V), ''7001'') = 0 and
-- ������� 2-6 �� ������������� ����� � ����
substr(#FIELD_SQL#, 2) not in (''36400'',''40800'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����� (VO, DOP_V): 3001, 3011, 3021 ������� 2-6: ''||substr(#FIELD_SQL#, 2)||'' ������ ��������� ��� ������ � ����� ���������� �� ������� ���������������� ����������. ��������� ��������: 36400 (����) � 40800 (����)''', 'PR', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 234, '321', '��� �� ��� ��/�� � ������������ �� ���� ������ ���� 0', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� �� ��� ��/�� � ������������ ��
(##.TU = 1 or (##.TU in (2, 3) and substr(##.KODCN, 1, 3) = ''643''))', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ''||
case when ##.TU = 1 
     then ''������������ ����''
     else ''���������� ��''
end||'' ���� ������ ��������� �������� 0''     ', 'MC1, VD4, VD5', 'ALL', 'Y', 'Y', 'N', null, '412801,412802,512801,512802,612801,612802,712801,712802,812801,812802', null, null, null, null from dual union all
select 235, '321', '��� �� ��� ��/�� � ������������ �� ���� ������ ���� 01.01.2099', '-- ���� �������
 #FIELD_SQL# <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- �������� �������� �� ��� ��/�� � ������������ ��
(##.TU = 1 or (##.TU in (2, 3) and substr(##.KODCN, 1, 3) = ''643''))', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ''||
case when ##.TU = 1 
     then ''������������ ����''
     else ''���������� ��''
end||'' ���� ������ ��������� �������� 01.01.2099''     ', 'MC2, MC3, VD6, VD7', 'ALL', 'Y', 'Y', 'N', null, '412902,412903,413002,413003,512902,512903,513002,513003,612902,612903,613002,613003,712902,712903,713002,713003,812902,812903,813002,813003', null, null, null, null from dual union all
select 236, '321', '���� �� ������ ����� ������������ ����� (MC1), �� ���� ����� �������� ������ ���� 01.01.2099', '-- ���� �������
 #FIELD_SQL# <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ����� ������������ ����� �� ������
 ##.MC1 = ''0''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ����������� ������ ������������ ����� (MC_''||substr(#BLOCK#, 2)||''1) ���� ������ ��������� �������� 01.01.2099''     ', 'MC2, MC3', 'ALL', 'Y', 'Y', 'N', null, '412904,413004,512904,513004,612904,613004,712904,713004,812904,813004', null, null, null, null from dual union all
select 237, '321', '������ ����� �������� ������������ ����� �����������, ���� ������� ��������� (MC3)', '-- ����� ������������ ����� ������
 ##.MC1 <> ''0'' and
-- ���� ������ �� �������
 ##.MC2 = to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ���� ��������� �������
 ##.MC3 <> to_date(''01.01.2099'', ''dd.mm.yyyy'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ��������� ����� ��������� �������� ������������ ����� (MC_''||substr(#BLOCK#, 2)||''3): ''||to_char(##.MC3, ''dd.mm.yyyy'')||'' ���� �� ����� ��������� �������� 01.01.2099''     ', 'MC2', 'ALL', 'Y', 'Y', 'N', null, '413005,513005,613005,713005,813005', null, null, null, null from dual union all
select 238, '321', '������ ����� �������� ������������ ����� �� ������ ���������', '-- ����� ������������ ����� ������
 ##.MC1 <> ''0'' and
-- ���� ������ �������
 ##.MC2 <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ���� ��������� �������
 ##.MC3 <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ���� ������ ������ ���� ���������
 ##.MC2 > ##.MC3', '''���� ������ �������� ������������ ����� (MC_''||substr(#BLOCK#, 2)||''2): ''||to_char(##.MC2, ''dd.mm.yyyy'')||'' �� ����� ���� ������ ����� ��������� �������� (MC_''||substr(#BLOCK#, 2)||''3): ''||to_char(##.MC3, ''dd.mm.yyyy'')', 'MC2', 'ALL', 'Y', 'Y', 'N', null, '413005,513005,613005,713005,813005', null, null, null, null from dual union all
select 239, '321', '��� �� ���� ������ ���� 0', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��
 ##.TU = 1', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������������ ���� ���� ������ ��������� �������� 0''     ', 'KD, VD1, VD2, BP', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 240, '321', '��� �� ���� ������ ���� 01.01.2099', '-- ���� �������
 #FIELD_SQL# <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- �������� �������� ��
 ##.TU = 1', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ������������ ���� ���� ������ ��������� �������� 01.01.2099''', 'VD3', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 241, '321', '���� ������/��� ����/������������������ ����������/������������� �������-�� � ��/��, �� ���� ����������� (�� 0)',
'-- ���� ��/��
 ##.TU in (2, 3) and  
-- ���� �� �������
 #FIELD_SQL# = ''0'' and
(-- ���� �������� - ��� ������/��� ����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1) or
 -- ��� ������������� ������ ������� - ��
 (#BLOCK# = ''p1'' and oes.B_PAYER in (''1'', ''2'') and p0.TU = 1) or
 (#BLOCK# = ''p2'' and oes.B_RECIP in (''1'', ''2'') and p3.TU = 1) or 
 -- ��� ������������� ������ ������� - �� �� ������� ��
 (#BLOCK# = ''p1'' and p0.TU = 1 and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or
 (#BLOCK# = ''p2'' and p3.TU = 1 and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1))', 
'''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||
case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) 
     then ''. ��� ������� ����� ��/�� ��� ���� �������� ������������''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''. ��� ������������������� ���� ��/��, �� ����� � �� ��������� �������� ��������� ��������, ��� ���� �������� ������������''
     when (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1)
     then ''. ��� ������� ���������������� ����� �� �� ��� ���� �������� ������������''
     when (#BLOCK# = ''p1'' and oes.B_PAYER in (''1'', ''2'') and p0.TU = 1) or
          (#BLOCK# = ''p2'' and oes.B_RECIP in (''1'', ''2'') and p3.TU = 1)
     then ''. ��� ������������� ������� ����� - �� ��� ���� �������� ������������''
     when (#BLOCK# = ''p1'' and p0.TU = 1 and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or
          (#BLOCK# = ''p2'' and p3.TU = 1 and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1)  
     then ''. ��� ������������� ������� - �� ���������������� ����� �� �� ��� ���� �������� ������������''
end', 'BP', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 242, '321', '���� ������/��� ����/������������������ ����������, �� ���� �����������', '-- ���� �� �������
 #FIELD_SQL# = to_date(''01.01.2099'', ''dd.mm.yyyy'') and
(-- ���� �������� - ��� ������/��� ����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1))  ', 'case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) 
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ������� ����� ��� ���� �������� ������������''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ������������������� ����, �� ����� � �� ��������� �������� ��������� ��������, ��� ���� �������� ������������''
     when (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1)
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ������� ���������������� ����� �� �� ��� ���� �������� ������������''
     else ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')
end', 'GR', 'p0, p3, p4', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 243, '321', '��, �� � ���� ������������� ������ ���� �� ������ 14 ��� �� ���� ��������', '-- ���� �������� �������
 ##.GR <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
 -- �������� �������� ��
(##.TU = ''3'' or
 -- ��� �� � ���� �������������
 (##.TU = ''2'' and #BLOCK# in (''p1'', ''p2''))) and
-- �� ���� �������� ��� 14 ���
months_between(oes.DATA, ##.GR) < 14*12 ', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ''||
case when ##.TU = ''3'' 
     then ''��������������� ��������������� ��� 14 ��� �� ���� �������� (DATA): ''||to_char(oes.DATA, ''dd.mm.yyyy'')
     when ##.TU = ''2'' and #BLOCK# in (''p1'', ''p2'')
     then ''����������� ���� - ������������� ''||decode(#BLOCK#, ''p1'', ''�����������'', ''����������'')||'' ��� 14 ��� �� ���� �������� (DATA): ''||to_char(oes.DATA, ''dd.mm.yyyy'')
end', 'GR', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 244, '321', '��, �� � ���� �����������/����������/���������� ������ ���� ���������������/������ �� ���� ��������', '-- ���� �����������/�������� �������
 ##.GR <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
 -- �������� �������� ��
(##.TU = ''1'' or
 -- ��� �� � ���� �����������/����������/����������
 (##.TU = ''2'' and #BLOCK# in (''p0'', ''p3'', ''p4''))) and
-- ���� �������� ������ ���� �����������/��������
oes.DATA < ##.GR ', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ''||
case when ##.TU = ''1'' 
     then ''����������� ���� �� ���������������� �� ���� �������� (DATA): ''||to_char(oes.DATA, ''dd.mm.yyyy'')
     when ##.TU = ''2'' and #BLOCK# in (''p0'', ''p3'', ''p4'')
     then ''���� �������� ����������� ���� ����� ���� �������� (DATA): ''||to_char(oes.DATA, ''dd.mm.yyyy'')
end', 'GR', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 245, '321', '�������������� ������������������� ����������� (�� ������ 14 ���) ����� ���� ������ ��', ' -- ���������� �������� ��
(p0.TU = ''2'' and
 -- �� ���� �������� ��� ��� 14 ���
 p0.GR <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
 p0.GR < oes.DATA and
 months_between(oes.DATA, p0.GR) < 14*12 and
 -- ������������� ����������� �� �� ��� �� ������
 p1.TU <> 2)', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||'', ���� ������� ������� ���� �������� ''||
case when #BLOCK# = ''p1'' and p1.TU = ''0''
     then ''����������� (GR0). ���������� �������� ������������������ (��� 14 ��� �� ���� ��������), ��� ���� ������ ���� ������ ������������� - ���������� ����. ���� �������� ����������� (GR0): ''||to_char(p0.GR, ''dd.mm.yyyy'')||'', ���� �������� (DATA): ''||to_char(oes.DATA, ''dd.mm.yyyy'')
     when #BLOCK# = ''p1'' and p1.TU <> ''2''
     then ''����������� (GR0). �������������� ������������������� ����������� (��� 14 ��� �� ���� ��������) ����� ���� ������ ���������� ����. ���� �������� ����������� (GR0): ''||to_char(p0.GR, ''dd.mm.yyyy'')||'', ���� �������� (DATA): ''||to_char(oes.DATA, ''dd.mm.yyyy'')
end', 'TU', 'p1', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 246, '321', '���� ������������ ���� ������� �� 8 ����', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��, ������������������ � ��
 ##.TU = ''1'' and 
substr(##.KODCR, 1, 3) = ''643'' and
 -- ���� ������� �� ������ �� ����
(translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null or
 -- ��� ���������� ���� ������������
 length(#FIELD_SQL#) <> 8)', 'case when translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||'' - ���� �������� �� ������ �����.''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ������������ ���� ������ �������� �� 8 ����''
end     ', 'SD', 'ALL', 'Y', 'Y', 'N', null, '411801', null, null, null, null from dual union all
select 247, '321', '����������� ����� � ���� - ����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��, ������������������ � ��
 ##.TU = ''1'' and 
substr(##.KODCR, 1, 3) = ''643'' and
-- ���� ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ���������� ���� ����������
length(#FIELD_SQL#) = 8 and
-- ����������� ����� - ������������
mantas.rf_pkg_rule.check_okpo(#FIELD_SQL#) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� �� ������ �������� - �������� ����������� �����''', 'SD', 'ALL', 'Y', 'Y', 'N', null, '411801', null, null, null, null from dual union all
select 248, '321', '���� ������/��� ����/������������������ ���������� � ��/��, �� ���� ����������� (�� 0)', '-- ���� ��/��
 ##.TU in (1, 3) and  
-- ���� �� �������
 #FIELD_SQL# = ''0'' and 
(-- ���� �������� - ��� ������/��� ����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1))', 'case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) 
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������� ����� ��/�� ��� ���� �������� ������������''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������������������� ���� ��/��, �� ����� � �� ��������� �������� ��������� ��������, ��� ���� �������� ������������''
     when (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1)
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������� ���������������� ����� �� �� ��� ���� �������� ������������''
     else ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end', 'RG', 'p0, p3, p4', 'Y', 'Y', 'N', null, '411901, 511901, 611901, 711901, 411904, 511904, 611904, 711904', null, null, null, null from dual union all
select 249, '321', '��� �� ���� ������ ���� 0', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��
 ##.TU = 2', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����������� ���� ���� ������ ��������� �������� 0''', 'RG', 'ALL', 'Y', 'Y', 'N', null, '411903, 511903, 611903, 711903', null, null, null, null from dual union all
select 250, '321', '���� ������� �� 13 ����', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��, ������������������ � ��
 ##.TU = 1 and 
substr(##.KODCR, 1, 3) = ''643'' and
-- �� ��������������� �� � �����/�����������
substr(trim(##.AMR_S), 1, 2) not in (''35'', ''67'') and
 -- ���� ������� �� ������ �� ����
(translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null or
 -- ��� ���������� ���� ������������
 length(#FIELD_SQL#) <> 13)', 'case when translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||'' - ���� �������� �� ������ �����.''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� ������������ ���� ������ �������� �� 13 ����''
end     ', 'RG', 'ALL', 'Y', 'Y', 'N', null, '411901, 511901, 611901, 711901', null, null, null, null from dual union all
select 251, '321', '������ ����� ���� - 1 ��� 5 (5 ����� ���� ������� � 12.12.2005)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��, ������������������ � ��
 ##.TU = 1 and 
substr(##.KODCR, 1, 3) = ''643'' and
-- ���� ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ���������� ���� ����������
length(#FIELD_SQL#) = 13 and
-- ������ ����� - �� 1 
substr(#FIELD_SQL#, 1, 1) <> ''1'' and
-- ������ ����� �� 5 ��� ���� ����������� �� 12.12.2005
(substr(#FIELD_SQL#, 1, 1) <> ''5'' or
 ##.GR < to_date(''12.12.2005'', ''dd.mm.yyyy''))', 'case when substr(#FIELD_SQL#, 1, 1) not in (''1'', ''5'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ����� ���� ������������ ���� ������ ���� 1 ��� 5''
     when substr(#FIELD_SQL#, 1, 1) = ''5'' and 
          ##.GR < to_date(''12.12.2005'', ''dd.mm.yyyy'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ����� ���� ������������ ���� �� ����� ���� 5 ��� ���, ������������������ �� 12.12.2005''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end ', 'RG', 'ALL', 'Y', 'Y', 'N', null, '411901, 511901, 611901, 711901', null, null, null, null from dual union all
select 252, '321', '����������� ����� ���� - ����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��, ������������������ � ��
 ##.TU = 1 and 
substr(##.KODCR, 1, 3) = ''643'' and
-- ���� ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ���������� ���� ����������
length(#FIELD_SQL#) = 13 and 
-- ���� �� ���� �� ������, ��� ������� �������� �� ��������
 #FIELD_SQL# not in (''1106375000012'', ''1027100980032'', ''1026303277850'', ''1037739677295'') and 
-- ��������� ����� ���� �� ��������� � ������� �������� ������� �� ������� ����� �� ������ 12 ���� �� 11
substr(to_char(mod(to_number(substr(#FIELD_SQL#, 1, 12)), 11)), -1) <> substr(#FIELD_SQL#, -1)', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� �� ������ �������� - �������� ����������� �����''', 'RG', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 253, '321', '������ ������� �� 15 ����', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��
 ##.TU = 3 and 
-- ���� ����������, ���� ��������, �� ��������������� �� � �����/�����������
(substr(##.KODCN, 1, 3) <> ''643'' or
 substr(trim(##.AMR_S), 1, 2) not in (''35'', ''67'')) and
 -- ���� ������� �� ������ �� ����
(translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null or
 -- ��� ���������� ���� ������������
 length(#FIELD_SQL#) <> 15)', 'case when translate(#FIELD_SQL#, ''a0123456789'', ''a'') is not null
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||'' - ���� �������� �� ������ �����.''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ��������������� ��������������� ������ �������� �� 15 ����''
end     ', 'RG', 'ALL', 'Y', 'Y', 'N', null, '411904, 511904, 611904, 711904, 411906, 511906, 611906, 711906', null, null, null, null from dual union all
select 254, '321', '������ ����� ������ - 3', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��
 ##.TU = 3 and 
-- ������ ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ���������� ���� ����������
length(#FIELD_SQL#) = 15 and
-- ������ ����� - �� 3 
substr(#FIELD_SQL#, 1, 1) <> ''3''', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ ����� ������ ��������������� ��������������� ������ ���� 3''', 'RG', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 255, '321', '����������� ����� ������ - ����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��
 ##.TU = 3 and 
-- ������ ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ���������� ���� ����������
length(#FIELD_SQL#) = 15 and 
-- ��������� ����� ������ �� ��������� � ������� �������� ������� �� ������� ����� �� ������ 14 ���� �� 13
substr(to_char(mod(to_number(substr(#FIELD_SQL#, 1, 14)), 13)), -1) <> substr(#FIELD_SQL#, -1)', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ������ �� ������ �������� - �������� ����������� �����''', 'RG', 'ALL', 'Y', 'Y', 'N', null, '411905, 511905, 611905, 711905', null, null, null, null from dual union all
select 256, '321', '���, �����, ��� ����� ��� - ����������� ��� ��/��, ���������� ��������/����� ������/������������������ ����������� ��� �������������� �������/������ ����� ��� ���� ������� �����-�� �������� ���', '-- ���� ��/��
 ##.TU in (2, 3) and  
-- ���� �� �������
 #FIELD_SQL# = ''0'' and 
(-- ���� �������� - ��� ������/��� ���� ��� ��� �������������
 (#BLOCK# in (''p0'', ''p1'') and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# in (''p3'', ''p2'') and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
 (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1) or 
 -- ��� ������� �������� ���
 ##.KD||##.SD||##.VD1||##.VD2 <> ''0000'' or
 ##.VD3 <> to_date(''01.01.2099'', ''dd.mm.yyyy''))', 'case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������� ����� ��/�� ��� ���� �������� ������������''
     when (#BLOCK# = ''p1'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p2'' and oes.B_RECIP in (''1'', ''2''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ��/�� - ������������� ������� ����� ��� ���� �������� ������������''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������������������� ���� ��/��, �� ����� � �� ��������� �������� ��������� ��������, ��� ���� �������� ������������''
     when (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
          (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1) 
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ��/�� - �������/������������� ������� ���������������� ����� �� �� ��� ���� �������� ������������''
     when  ##.KD||##.SD||##.VD1||##.VD2 <> ''0000'' or
           ##.VD3 <> to_date(''01.01.2099'', ''dd.mm.yyyy'')
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ����������� ��������� ��� �������� ���������, ��������������� �������� - ���� ��������� ��� ����, ���� ���������� � 0 ��������� ���� � ��� (���� ������ - � 01.01.2099)''
     else ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end', 'KD, VD1, VD2', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 257, '321', '���� ������ ��� ����������� ��� ��/��, ���������� ��������/����� ������/������������������ ����������� ��� �������������� �������/������ ����� ��� ���� ������� �����-�� �������� ���, ����� ���� ��������� 26 - ��������� ������������� �������� ���������� ��', '-- ���� ��/��
 ##.TU in (2, 3) and  
-- ��� ��� �� �����: 26 - ��������� ������������� �������� ���������� ��
 ##.KD <> ''26'' and 
-- ���� �� �������
 #FIELD_SQL# = to_date(''01.01.2099'', ''dd.mm.yyyy'') and
(-- ���� �������� - ��� ������/��� ���� ��� ��� �������������
 (#BLOCK# in (''p0'', ''p1'') and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# in (''p3'', ''p2'') and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
 (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1) or 
 -- ��� ������� �������� ���
 ##.KD||##.SD||##.VD1||##.VD2 <> ''0000'' or
 ##.VD3 <> to_date(''01.01.2099'', ''dd.mm.yyyy''))', 'case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ������� ����� ��/�� ��� ���� �������� ������������''
     when (#BLOCK# = ''p1'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p2'' and oes.B_RECIP in (''1'', ''2''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ��/�� - ������������� ������� ����� ��� ���� �������� ������������''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ������������������� ���� ��/��, �� ����� � �� ��������� �������� ��������� ��������, ��� ���� �������� ������������''
     when (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
          (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1) 
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ��/�� - �������/������������� ������� ���������������� ����� �� �� ��� ���� �������� ������������''
     when  ##.KD||##.SD||##.VD1||##.VD2 <> ''0000'' or
           ##.VD3 <> to_date(''01.01.2099'', ''dd.mm.yyyy'')
     then ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ���� ����������� ��������� ��� �������� ���������, ��������������� �������� - ���� ��������� ��� ����, ���� ���������� � 0 ��������� ���� � ���''
     else ''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')
end', 'VD3', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 258, '321', '���� ������ ��� ������ ���� 01.01.2099 ��� ���� ��������� 26 - ��������� ������������� �������� ���������� ��', '-- ��� ��� �����: 26 - ��������� ������������� �������� ���������� ��
 ##.KD = ''26'' and 
-- ���� �������
 #FIELD_SQL# <> to_date(''01.01.2099'', ''dd.mm.yyyy'')', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ���� ��������� (KD''||substr(#BLOCK#, 2)||''): ''||##.KD||'' ����������� ���������: 01.01.2099''', 'VD3', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 259, '321', '��� ��������� ���� � �����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ������ ���� ��� � �����������
mantas.rf_pkg_rule.check_id_doc_type(#FIELD_SQL#) = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���� ���������, ��������������� ��������/����� ���������� � ����� �����''', 'KD, VD4', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 260, '321', '������������ ���� ��� � ����������� �� �����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ��� ���� � �����������
mantas.rf_pkg_rule.check_id_doc_type(#FIELD_SQL#) = 1 and
-- �������� - ��/��
 ##.TU in (2, 3) and 
-- ����������� �������
 ##.KODCN <> ''0'' and 
(-- ��� ������� �� ��������� ���� 21-29, ��� ����������� �� ��������� �� ����/�����������
 (substr(##.KODCN, 1, 3) = ''643'' and substr(trim(##.AMR_S), 1, 2) not in (''35'', ''67'') and NOT(to_number(#FIELD_SQL#) between 21 and 29)) or
 -- ��� ��� ��� ����������� ��������� ���� 33-37, 40
 (substr(##.KODCN, 1, 3) = ''000'' and NOT(to_number(#FIELD_SQL#) between 33 and 37 or #FIELD_SQL# = ''40'')) or
 -- ��� ����������� ������� ��������� ���� 31, 32, 36, 37, 40
 (substr(##.KODCN, 1, 3) not in (''643'', ''000'') and #FIELD_SQL# not in (''31'', ''32'', ''36'', ''37'', ''40''))) ', 'case when substr(##.KODCN, 1, 3) = ''643''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ������� �� ����������� ��������� ����� ��� ��������� � �������� ���������, ��������������� ��������. ��� ������� �� ��������� ���� 21-29''
     when substr(##.KODCN, 1, 3) = ''000''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ��� ��� ����������� ����������� ��������� ����� ��� ��������� � �������� ���������, ��������������� ��������. ��� ��� ��� ����������� ��������� ���� 33-37, 40''
     when substr(##.KODCN, 1, 3) not in (''643'', ''000'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����������� ������� ����������� ��������� ����� ��� ��������� � �������� ���������, ��������������� ��������. ��� ����������� ������� ��������� ���� 31, 32, 36, 37, 40''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end', 'KD', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 261, '321', '���� - ������� ��/�� �� �������� ��������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� - ��/��
 ##.TU in (2, 3) and 
-- �������� �������
instr(#FIELD_SQL#, '' '') > 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ���� �� ����� ��������� �������''', 'VD1', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 262, '321', '���� ������ ���� ����� ����� �������� ��/�� � �������', '-- ���� �������
 #FIELD_SQL# <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- �������� - ��/��
 ##.TU in (2, 3) and 
-- ���� �� ���� �������� ��� ����� �������
((##.GR <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and #FIELD_SQL# < ##.GR) or
 #FIELD_SQL# > trunc(sysdate))', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ���� ��� ����������� ��������� (��������� � �������� ��� �� ���� �������� ��/��)'' ', 'VD3', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 263, '321', '����������� ����������� ��� ��/��, ���������� ��������/����� ������/������������������ ����������� ��� �������������� �������/������ �����, � ����� ���� ������ ��� ��������� �� ����� ���������� � ��', '-- ���� ��/��
 ##.TU in (2, 3) and
-- ���� �� �������
 #FIELD_SQL# = ''0'' and
(-- ���� �������� - ��� ������/��� ���� ��� ��� �������������
 (#BLOCK# in (''p0'', ''p1'') and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# in (''p3'', ''p2'') and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
 (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1) or 
 -- ������ ��� ��������� �� ����� ���������� � ��
 ##.VD4 <> ''0'')', 'case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))
     then ''�� ������� ����������� ��/�� (#FIELD_CODE#): ''||#FIELD_SQL#||''. ��� ������� ����� ��� ���� �������� ������������''
     when (#BLOCK# = ''p1'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p2'' and oes.B_RECIP in (''1'', ''2''))
     then ''�� ������� ����������� ��/�� (#FIELD_CODE#): ''||#FIELD_SQL#||''. ��� ������������� ������� ����� ��� ���� �������� ������������''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''�� ������� ����������� ��/�� (#FIELD_CODE#): ''||#FIELD_SQL#||''. ��� ������������������� ����, �� ����� � �� ��������� �������� ��������� ��������, ��� ���� �������� ������������''
     when (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
          (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1) 
     then ''�� ������� ����������� ��/�� (#FIELD_CODE#): ''||#FIELD_SQL#||''. ��� �������/������������� ������� ���������������� ����� �� �� ��� ���� �������� ������������''
     when ##.VD4 <> ''0''
     then ''�� ������� ����������� ��/�� (#FIELD_CODE#): ''||#FIELD_SQL#||'', ��� ���� ������ ��� ��������� �� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''4): ''||##.VD4||''. ���� ������� �����������, ���� �� ���������� �������� �� ����� ����������''
     else ''�� ������� ����������� ��/�� (#FIELD_CODE#): ''||#FIELD_SQL#
end', 'KODCN', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 264, '321', '������������ ���� ��� � ����������� �� �����������', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ����� ��� ���� � �����������
mantas.rf_pkg_rule.check_id_doc_type(#FIELD_SQL#) = 1 and
-- �������� - ��/��
 ##.TU in (2, 3) and
-- ����������� �������
 ##.KODCN <> ''0'' and
-- ����������� �� ��
substr(##.KODCN, 1, 3) <> ''643'' and
(-- ��� ��� ��� ����������� ��������� ���� 33-35, 39, 40, 99
 (substr(##.KODCN, 1, 3) = ''000'' and NOT(to_number(#FIELD_SQL#) between 33 and 35 or #FIELD_SQL# in (''39'', ''40'', ''99''))) or
 -- ��� ����� ����������� ����� (051 - �������, 112 - ��������, 398 - ���������, 417 - ��������) ��������� ���� 31, 39, 40, 99
 (substr(##.KODCN, 1, 3) in (''051'', ''112'', ''398'', ''417'') and #FIELD_SQL# not in (''31'', ''39'', ''40'', ''99'')) or
 -- ��� ��������� ����������� ������� ��������� ���� 39, 40, 99
 (substr(##.KODCN, 1, 3) not in (''000'', ''051'', ''112'', ''398'', ''417'') and #FIELD_SQL# not in (''39'', ''40'', ''99''))) ', 'case when substr(##.KODCN, 1, 3) = ''000''
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ��� ��� ����������� ����������� ��������� ����� ��� ��������� � �������� ���������, ��������������� ����� ���������� � ��. ��� ��� ��� ����������� ��������� ���� 33-35, 39, 40, 99''
     when substr(##.KODCN, 1, 3) in (''051'', ''112'', ''398'', ''417'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����������� ������� �����, �������� � ���������� ���� ����, ����������� ��������� ����� ��� ��������� � �������� ���������, ��������������� ����� ���������� � ��. ��� ������� ����� 051 - �������, 112 - ��������, 398 - ���������, 417 - �������� ��������� ���� 31, 39, 40, 99''
     when substr(##.KODCN, 1, 3) not in (''000'', ''051'', ''112'', ''398'', ''417'')
     then ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ����������� ������� ����������� ��������� ����� ��� ��������� � �������� ���������, ��������������� ����� ���������� � ��. ��� ����������� ������� ��������� ���� 39, 40, 99''
     else ''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#
end', 'VD4', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 265, '321', '��� ��� ���������� ��� ��/�� - �� ������� ��, ������� ����� ����������� ��� ���������� � ��, ���������� ��������/����� ������/������������������ ����������� ��� �������������� �������/������ �����, � ����� ���� ������� �����-�� �������� ���', '-- ���� ��/��
 ##.TU in (2, 3) and
-- ����������� �������
 ##.KODCN <> ''0'' and
-- ����������� �� ��
substr(##.KODCN, 1, 3) <> ''643'' and
-- ���� �� �������
 #FIELD_SQL# = ''0'' and 
(((-- ���� �������� - ��� ������/��� ���� ��� ��� �������������
   (#BLOCK# in (''p0'', ''p1'') and oes.B_PAYER in (''1'', ''2'')) or
   (#BLOCK# in (''p3'', ''p2'') and oes.B_RECIP in (''1'', ''2'')) or
   -- ��� ������������������ ����������
   (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
   -- ��� ������ ������� ��
   (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
   (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1)) and
  (-- � ����� ����������� ��� ����� ���������� ��������� � ��
   ##.AMR_S not in (''0'', ''00'') or
   ##.ADRESS_S not in (''0'', ''00''))) or
 -- ��� ��� ������� �������� ���
 ##.VD4||##.VD5 <> ''00'' or
 ##.VD6 <> to_date(''01.01.2099'', ''dd.mm.yyyy'') or
 ##.VD7 <> to_date(''01.01.2099'', ''dd.mm.yyyy'')
)', '''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ''||
replace(
case when ((#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
           (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))) and
          (##.AMR_S not in (''0'', ''00'') or
           ##.ADRESS_S not in (''0'', ''00''))
     then ''��� ������� ����� ��/��, #PHRASE#''
     when ((#BLOCK# = ''p1'' and oes.B_PAYER in (''1'', ''2'')) or
           (#BLOCK# = ''p2'' and oes.B_RECIP in (''1'', ''2''))) and
          (##.AMR_S not in (''0'', ''00'') or
           ##.ADRESS_S not in (''0'', ''00''))
     then ''��� ��/�� - ������������� ������� �����, #PHRASE#''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) and
          (##.AMR_S not in (''0'', ''00'') or
           ##.ADRESS_S not in (''0'', ''00''))
     then ''��� ������������������� ���� ��/��, �� ����� � �� ��������� �������� ��������� ��������, #PHRASE#''
     when ((#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or 
           (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1)) and
          (##.AMR_S not in (''0'', ''00'') or
           ##.ADRESS_S not in (''0'', ''00'')) 
     then ''��� ��/�� - �������/������������� ������� ���������������� ����� �� ��, #PHRASE#''
     when ##.VD4||##.VD5 <> ''00'' or
          ##.VD6 <> to_date(''01.01.2099'', ''dd.mm.yyyy'') or
          ##.VD7 <> to_date(''01.01.2099'', ''dd.mm.yyyy'')
     then ''��� ���� ����������� ��������� ��� �������� ���������, ��������������� ����� ���������� � �� - ���� ��������� ��� ����, ���� ���������� � 0 ��������� ���� � ��� (����� �������� - � 01.01.2099)''
end,
''#PHRASE#'',
''�� ����������� ����������� �� � �������� ����� ����������� ��� ����� ���������� � ��, ��� ���� �������� ������������'')', 'VD4', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 266, '321', '���� ��� ��� = ��� ���, �� ����� ��� = ����� + ����� ���', '-- ��� ��� � ��� ��� ������� � �����
 ##.KD <> ''0'' and
 ##.VD4 <> ''0'' and
 ##.KD = ##.VD4 and
-- ������ ���������� �� �����
nullif(##.SD, ''0'')||##.VD1 <> ##.VD5 and 
-- ���� ��/��
 ##.TU in (2, 3) and
-- ����������� �������
 ##.KODCN <> ''0'' and
-- ����������� �� ��
substr(##.KODCN, 1, 3) <> ''643''', '''����� ���������, ��������������� ����� ���������� � �� (#FIELD_CODE#): ''||#FIELD_SQL#||'' �� ��������� � ������ � ������� ���������, ��������������� �������� (SD''||substr(#BLOCK#, 2)||'', VD''||substr(#BLOCK#, 2)||''1): ''||nullif(##.SD, ''0'')||##.VD1||'', ���� ���� ���������� ���������''', 'VD5', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 267, '321', '������ ����� �������� ��� �� ������ ���������', '-- ��� ��� ������
 ##.VD4 <> ''0'' and
-- ���� ������ �������
 ##.VD6 <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ���� ��������� �������
 ##.VD7 <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ���� ������ ������ ���� ���������
 ##.VD6 > ##.VD7 and 
-- ���� ��/��
 ##.TU in (2, 3) and
-- ����������� �������
 ##.KODCN <> ''0'' and
-- ����������� �� ��
substr(##.KODCN, 1, 3) <> ''643''', '''���� ������ �������� ��������� �� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''6): ''||to_char(##.VD6, ''dd.mm.yyyy'')||'' �� ����� ���� ������ ����� ��������� �������� (VD''||substr(#BLOCK#, 2)||''7): ''||to_char(##.VD7, ''dd.mm.yyyy'')', 'VD6', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 268, '321', '���� ��� ��� = ������������ �����, �� ����� ��� = ����� ��', '-- ��� ��� - ������������ �����
 ##.VD4 = ''39'' and
-- ������ ���������� �� �����
 ##.VD5 <> ##.MC1 and 
-- ���� ��/��
 ##.TU in (2, 3) and
-- ����������� �������
 ##.KODCN <> ''0'' and
-- ����������� �� ��
substr(##.KODCN, 1, 3) <> ''643''', '''����� ������������ ����� (#FIELD_CODE#): ''||#FIELD_SQL#||'' �� ��������� � ������� ���������, ��������������� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''5): ''||##.VD5||'', ���� ������ ��� ���������, ��������������� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''4) - ������������ �����''', 'MC1', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 269, '321', '���� ��� ��� = ������������ �����, �� ������ ����� �������� ��� = ������ ����� �������� ��', '-- ��� ��� - ������������ �����
 ##.VD4 = ''39'' and
-- ���� ������ ����� �������� �� �����
 ##.VD6 <> ##.MC2 and 
-- ���� ��/��
 ##.TU in (2, 3) and
-- ����������� �������
 ##.KODCN <> ''0'' and
-- ����������� �� ��
substr(##.KODCN, 1, 3) <> ''643''', '''���� ������ ����� �������� ������������ ����� (#FIELD_CODE#): ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||'' �� ��������� � ������� ����� �������� ���������, ��������������� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''6): ''||to_char(##.VD6, ''dd.mm.yyyy'')||'', ���� ������ ��� ���������, ��������������� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''4) - ������������ �����''', 'MC2', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 270, '321', '���� ��� ��� = ������������ �����, �� ������ ����� �������� ��� = ������ ����� �������� ��', '-- ��� ��� - ������������ �����
 ##.VD4 = ''39'' and
-- ���� ��������� ����� �������� �� �����
 ##.VD7 <> ##.MC3 and 
-- ���� ��/��
 ##.TU in (2, 3) and
-- ����������� �������
 ##.KODCN <> ''0'' and
-- ����������� �� ��
substr(##.KODCN, 1, 3) <> ''643''', '''���� ��������� ����� �������� ������������ ����� (#FIELD_CODE#): ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||'' �� ��������� � ���������� ����� �������� ���������, ��������������� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''7): ''||to_char(##.VD7, ''dd.mm.yyyy'')||'', ���� ������ ��� ���������, ��������������� ����� ���������� � �� (VD''||substr(#BLOCK#, 2)||''4) - ������������ �����''', 'MC3', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 271, '321', '���� ������ ���� 0 (������), ���� ��������� ��� (TU = 0)', '-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- ��������� ���
 ##.TU = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� ���������� ��������� ������ ��������� �������� 0''     ', 'PR, NAMEU, KODCR, KODCN, AMR_S, AMR_R, AMR_G, AMR_U, AMR_D, AMR_K, AMR_O, ADRESS_S, ADRESS_R, ADRESS_G, ADRESS_U, ADRESS_D, ADRESS_K, ADRESS_O, KD, SD, RG, ND, VD1, VD2, VD4, VD5, MC1, BP', 'p1, p2, p4', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 272, '321', '���� ������ ���� 01.01.2099, ���� ��������� ��� (TU = 0)', '-- ���� �������
 #FIELD_SQL# <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and
-- ��������� ���
 ##.TU = 0', '''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||to_char(#FIELD_SQL#, ''dd.mm.yyyy'')||''. ��� ���������� ��������� ������ ��������� �������� 01.01.2099''     ', 'VD3, VD6, VD7, MC2, MC3, GR', 'p1, p2, p4', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all 
select 273, '321', '��� � ���� (2-3 �����) ��������� � ����� ����������� �� (������� � 01.01.2004)', 
'-- ���� �������
 #FIELD_SQL# <> ''0'' and
-- �������� �������� ��, ������������������ � ��
 ##.TU = 1 and
substr(##.KODCR, 1, 3) = ''643'' and
-- ���� ������� ������ �� ����
translate(#FIELD_SQL#, ''a0123456789'', ''a'') is null and
-- ���������� ���� ����������
length(#FIELD_SQL#) = 13 and
-- ������� ���� �����������
 ##.GR <> to_date(''01.01.2099'', ''dd.mm.yyyy'') and 
-- ���� ����������� ������� � 01.01.2004 
 ##.GR >= to_date(''01.01.2004'', ''dd.mm.yyyy'') and 
-- ��� � ���� �� ��������� � ����� �����������
substr(#FIELD_SQL#, 2, 2) <> to_char(##.GR, ''yy'')', 
'''������� ��������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||''. ��� � ������ ����: ''||substr(#FIELD_SQL#, 2, 2)||
'' �� ��������� � ����� ����������� ��: ''||to_char(##.GR, ''yy'')', 
'RG', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 274, '321', '���� �����������, ���� �� - ������/��� ����/������������������ ����������', 
'-- ���� ��
 ##.TU = 1 and
-- ���� �� �������
 #FIELD_SQL# = ''0'' and
(-- ���� �������� - ��� ������/��� ����
 (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1))', 
'''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||
case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))
     then ''. ��� ������� ����� �� ��� ���� �������� ������������''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''. ��� ������������������� ��, �� ����� � �� ��������� �������� ��������� ��������, ��� ���� �������� ������������''
     when (#BLOCK# in (''p0'', ''p3'') and ##.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(##.BIK_B) = 1)
     then ''. ��� ������� �� ���������������� ����� �� �� ��� ���� �������� ������������''
end', 
'SD', 'p0, p3, p4', 'N', 'Y', 'N', null, null, null, null, null, null from dual union all
select 275, '321', '������ �����������/����� ���������� ����������� ��� �������������� ��������', '-- ��� �� ����������� ��/��
NOT(#COLUMN_NM# = ''KODCN'' and ##.TU in (2, 3)) and
-- �������� ����
 ##.TU <> 0 and
-- ���� �� �������
 #FIELD_SQL# = ''0'' and
-- ����������� ��������� ������:
(-- ���� �������� - ��� ������/��� ���� ��� ��� �������������
 (#BLOCK# in (''p0'', ''p1'') and oes.B_PAYER in (''1'', ''2'')) or
 (#BLOCK# in (''p3'', ''p2'') and oes.B_RECIP in (''1'', ''2'')) or
 -- ��� ������������������ ����������
 (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1'')) or
 -- ��� ������ ������� ��
 (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or
 (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1)
)', 
'''�� ������� ���� "#FIELD_NAME# (#FIELD_CODE#)": ''||#FIELD_SQL#||
case when (#BLOCK# = ''p0'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p3'' and oes.B_RECIP in (''1'', ''2''))
     then ''. ��� ������� ����� ����������� ��������� ������ ''
     when (#BLOCK# = ''p1'' and oes.B_PAYER in (''1'', ''2'')) or
          (#BLOCK# = ''p2'' and oes.B_RECIP in (''1'', ''2''))
     then ''. ��� ������������� ������� ����� ����������� ��������� ������ ''
     when (#BLOCK# = ''p4'' and (p0.VP = ''1'' or p3.VP = ''1''))
     then ''. ��� ������������������� ����, �� ����� � �� ��������� �������� ��������� ��������, ����������� ��������� ������ ''
     when (#BLOCK# in (''p0'', ''p1'') and p0.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p0.BIK_B) = 1) or
          (#BLOCK# in (''p3'', ''p2'') and p3.BIK_B <> ''0'' and mantas.rf_pkg_rule.is_sbrf(p3.BIK_B) = 1)
     then ''. ��� �������/������������� ������� ���������������� ����� �� �� ����������� ��������� ������ ''
end||
case when #COLUMN_NM# = ''KODCR'' and ##.TU = 2
     then ''����� ���������� (�����������)''
     when #COLUMN_NM# = ''KODCR''
     then ''�����������''
     when #COLUMN_NM# = ''KODCN''
     then ''���������������''
end', 'KODCR, KODCN', 'p1, p2', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all 
select 276, '321', '����� ��� �����������, ���� ��� �������', '-- ���� ��/��
 ##.TU in (2, 3) and
-- ���� �� �������
 #FIELD_SQL# = ''0'' and
-- ��� ��� - �������
 ##.KD = ''21'' 
', 
'''�� ������� ����� �������� ���������� �� (���� "#FIELD_NAME# (#FIELD_CODE#)")''', 
'SD', 'ALL', 'Y', 'Y', 'N', null, null, null, null, null, null from dual union all
select 277, '321', '��� ��������� �� ���� �������� (������ �������� �� ���� ������, ��� �������� �� - �� 20/45 ���/���������)', 
 q'{-- ���� ��/��
 ##.TU in (2, 3) and
 -- ���� �������� �������
 oes.DATA <> to_date('01.01.2099', 'dd.mm.yyyy') and
 -- ���� ������ ������� 
 ##.VD3 <> to_date('01.01.2099', 'dd.mm.yyyy') and
 -- ��� �� ��������� �� ���� ��������
 (-- ���� �������� ������ ���� ������
  oes.DATA < ##.VD3
  or
  -- ���� �������� ����� ����� �������� �������� �� (�������������� �� ��������� ���� ��������)
  ( -- ��� ��� - ������� ��
   ##.KD = '21' and
   -- ���� �������� �������
   ##.GR <> to_date('01.01.2099', 'dd.mm.yyyy') and
   oes.DATA > case -- ���� ������� ����� �� 20 ��� - ��������� �� 20 ��� ������������
                   when ##.VD3 < add_months(##.GR, 240)
                   then add_months(##.GR, 240)
                   -- �����, ���� ������� ����� �� 45 ��� - ��������� �� 45 ��� ������������
                   when ##.VD3 < add_months(##.GR, 540)
                   then add_months(##.GR, 540)
              end))}', 
 q'{'��� �� ��������� �� ���� �������� (������ �������� ��������� �� ���� ������, ��� �������� �� - �� 20 ���/45 ���/���������) - '||
  case when oes.DATA < ##.VD3
       then '���� �������� ������ ���� ������'
       when ##.VD3 < add_months(##.GR, 240)
       then '���� �������� ����� ���������� 20 ���'
       when ##.VD3 < add_months(##.GR, 540)
       then '���� �������� ����� ���������� 45 ���'
  end||
  '. ���� ������: '||to_char(##.VD3, 'dd.mm.yyyy')||', '||
  case when -- ��� ��� - ������� ��
            ##.KD = '21' and
            -- ���� �������� �������
            ##.GR <> to_date('01.01.2099', 'dd.mm.yyyy')
       then '���� ��������: '||to_char(##.GR, 'dd.mm.yyyy')||', '
  end||       
  '���� ��������: '||to_char(oes.DATA, 'dd.mm.yyyy')}',
'VD3', 'ALL', 'N', 'Y', 'N', null, null, null, null, null, null from dual 
   ) v
ON (v.rule_seq_id = t.rule_seq_id)   
WHEN NOT MATCHED THEN
  INSERT (rule_seq_id, form_cd, desc_tx, check_sql_tx, msg_sql_tx, col_list_tx, block_list_tx, critical_fl, active_fl, err_fl, err_mess_tx, note_tx, created_by, created_date, modified_by, modified_date)
  VALUES (v.rule_seq_id, v.form_cd, v.desc_tx, trim(v.check_sql_tx), trim(v.msg_sql_tx), v.col_list_tx, v.block_list_tx, v.critical_fl, v.active_fl, v.err_fl, v.err_mess_tx, v.note_tx, 'SYSTEM', SYSDATE, null, null)
WHEN MATCHED THEN
  UPDATE
     SET form_cd = v.form_cd, 
         desc_tx = v.desc_tx,
         check_sql_tx = trim(v.check_sql_tx),
         msg_sql_tx = trim(v.msg_sql_tx),
         col_list_tx = v.col_list_tx,
         block_list_tx = v.block_list_tx,
         critical_fl = v.critical_fl,
         active_fl = v.active_fl,
         err_fl = v.err_fl,
         err_mess_tx = v.err_mess_tx,
         note_tx = v.note_tx,
         modified_by = 'SYSTEM',
         modified_date = SYSDATE;

commit;
