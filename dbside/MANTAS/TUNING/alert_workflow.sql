-- ��������� workflow ��� ������� - �� ��������� �������� � �������� ��� ����

-- ����������� ������������ ������������ �������� �������
UPDATE mantas.kdd_code_set_trnln
   SET code_disp_tx = decode(code_val, 'NW', '�����', 
                                       'RA', '����� �������������', 
                                       'RO', 'Reopened (�� ������������)', 
                                       'OP', '� ������ (�� ������������)', 
                                       'FL', '���. ������',
                                       'CL', 'Closed (�� ������������)')                                                                                
 WHERE code_set = 'AlertStatus' and code_val not like 'RF%';  
 
-- ����������� ����� ��������
-- ����� (-) � ���� ������� �������� �������������� ��� � �������� �������� �������� �� �������� ���������
-- ���� (+) � ���� ������� �������� �������������� ��� � �������� ���������� ������������ �������� ����� �������� ���������
-- ����� (=) � ���� ������� �������� �������������� ��� � �������� �������� �������� ����� �������� ���������
-- ���������� ����. �������� �������� �������������� ������� � �������� ������������ �������� �� �������� ���������
-- ��������! � �������� ���������� ������������ � �������� ������������ ���� (+), � � ��������� ������� (#)!
-- ��� ������, ��� ���� � �������� ������������ �������, �� �� ������������ ������ ������ � ����� �������, 
--             � ���� � ��������� ������������ ����, �� ����� �������� �� �������� ��� ����� �������� �������� ���������!
MERGE INTO mantas.kdd_review_status t USING
  (SELECT 'RF_OES' as status_cd, 'AL' as review_type_cd, 'Y' as can_nhrit_fl, '' as viewd_by_owner_actvy_type_cd, '' as viewd_result_status_cd, 
          '110' as status_displ_order_nb, '' as prsdnc_order_nb, 'N' as closed_status_fl 
          -- �������� �������� - ������������ ���������, ��� �� ����������
     FROM dual
   UNION ALL
   SELECT 'RF_READY', 'AL', 'Y', '', '', '111', '', 'N' 
          -- ������� �������� - ������������ ���������, ��������� ���������������, ��������� � �������� �� �������� 
     FROM dual       
   UNION ALL
   SELECT 'RF_DONE', 'AL', 'Y', '', '', '112', '', 'Y' 
          -- ���������� 
     FROM dual       
   UNION ALL
   SELECT 'RF_CANCEL', 'AL', 'Y', '', '', '113', '', 'Y'
          -- �� �������� ��������
     FROM dual
   UNION ALL
   SELECT 'RF_REPEAT', 'AL', 'Y', '', '', '10', '', 'N' 
          -- ��������� ������������ - ��������� ��� �� ���� ����������, ��������, ��������� ��� ������������� ��� ������
     FROM dual       
   UNION ALL
   SELECT 'RF_DELETE-', 'AL', 'Y', '', '', '120', '', 'N' 
          -- �������� - ��������� ��� �� ���� ����������, ��������� ��� ������ � ����� �� ��������������/��������� ��������
     FROM dual       
   UNION ALL
   SELECT 'RF_RA-', 'AL', 'Y', '', '', '121', '', 'N' 
          -- �������� (����� �������������) - ������ � �������� �������� (�� �������� ���������) ��� �������� ����� �������������
     FROM dual       
   UNION ALL
   SELECT 'RF_OP-', 'AL', 'Y', '', '', '122', '', 'N' 
          -- �������� (� ������) - ����� � �������� �������� (�� �������� ���������) ��� ������ ������������� �����������
     FROM dual       
   UNION ALL
   SELECT 'RF_FL-', 'AL', 'Y', '', '', '123', '', 'N' 
          -- �������� (���. ������) - ����� � �������� �������� (�� �������� ���������) ������� �������������� ���������� ��� ���������
     FROM dual       
   UNION ALL
   SELECT 'RF_DLTD-', 'AL', 'Y', '', '', '190', '', 'Y' 
          -- ������� - ����� ������� ��������� � ����� �� ��������������/��������� �������� (��������� � ��� �� ��������, ���� �����������, ����������)
     FROM dual       
   UNION ALL
   SELECT 'RF_REPEAT+', 'AL', 'Y', '', '', '130', '', 'N' 
          -- !���������� (��������� ������������) - ��������� ����������, ��������, ��������� ��� ������������� ��� ������ (����� �������� ������ ���������)
     FROM dual       
   UNION ALL
   SELECT 'RF_RA+', 'AL', 'Y', '', '', '131', '', 'N' 
          -- !���������� (��������� ������������, ����� �������������) - ������ � �������� ���������� ������������ (����� �������� ���������) ��� �������� ����� �������������
     FROM dual       
   UNION ALL
   SELECT 'RF_OP+', 'AL', 'Y', '', '', '132', '', 'N' 
          -- !���������� (��������� ������������, � ������) - ����� � �������� ���������� ������������ (����� �������� ���������) ��� ������ ������������� �����������
     FROM dual       
   UNION ALL
   SELECT 'RF_FL+', 'AL', 'Y', '', '', '133', '', 'N' 
          -- !���������� (��������� ������������, ���. ������) - ����� � �������� ���������� ������������ (����� �������� ���������) ������� �������������� ���������� ��� ���������
     FROM dual       
   UNION ALL
   SELECT 'RF_OES+', 'AL', 'Y', '', '', '134', '', 'N' 
          -- ������ ������ - ������������ ��������� � ������ ����� �������������, ��� �� ���������� (����� �������� ����� ��������� � ������ "����������" - RF_DONE)
     FROM dual       
   UNION ALL
   SELECT 'RF_READY+', 'AL', 'Y', '', '', '136', '', 'N' 
          -- ������� �������� (������ ������) - ������������ ��������� (������ ������), ��������� ���������������, ��������� � �������� �� ��������
     FROM dual       
   UNION ALL
   SELECT 'RF_CANCEL+', 'AL', 'Y', '', '', '135', '', 'N' 
          -- ������ �������� (��������� ������������) - ������������ ��������� �� �������� ����� ������������� ���������, ��� �� ���������� (����� �������� ����� ��������� � ������ "�� �������� ��������" - RF_CANCEL)
     FROM dual       
   UNION ALL
   SELECT 'RF_CREADY+', 'AL', 'Y', '', '', '136', '', 'N' 
          -- ������� �������� (������ ��������) - ������������ ��������� (������ ��������), ��������� ���������������, ��������� � �������� �� �������� (����� �������� ����� ��������� � ������ "�� �������� ��������" - RF_CANCEL)
     FROM dual       
   UNION ALL
   SELECT 'RF_DELETE=', 'AL', 'Y', '', '', '140', '', 'N' 
          -- !���������� (��������) - ��������� ����������, ��������� ��� ������ � ����� �� ��������������/��������� �������� (����� �������� ������ ���������)
     FROM dual       
   UNION ALL
   SELECT 'RF_RA=', 'AL', 'Y', '', '', '141', '', 'N' 
          -- !���������� (��������, ����� �������������) - ������ � �������� �������� (����� �������� ���������) ��� �������� ����� �������������
     FROM dual       
   UNION ALL
   SELECT 'RF_OP=', 'AL', 'Y', '', '', '142', '', 'N' 
          -- !���������� (��������, � ������) - ����� � �������� �������� (����� �������� ���������) ��� ������ ������������� �����������
     FROM dual       
   UNION ALL
   SELECT 'RF_FL=', 'AL', 'Y', '', '', '143', '', 'N' 
          -- !���������� (��������, ���. ������) - ����� � �������� �������� (����� �������� ���������) ������� �������������� ���������� ��� ���������
     FROM dual       
   UNION ALL
   SELECT 'RF_CANCEL=', 'AL', 'Y', '', '', '144', '', 'N' 
          -- ������ �������� (��������) - ������������ ��������� �� �������� ����� ������������� ���������, ��� �� ���������� (����� �������� ����� ��������� � ������ "�������" - RF_DLTD-)
     FROM dual       
   UNION ALL
   SELECT 'RF_READY=', 'AL', 'Y', '', '', '145', '', 'N' 
          -- ������� �������� (��������) - ������������ ��������� �� �������� ����� ������������� ���������, ��������� ���������������, ��������� � �������� �� �������� (����� �������� ����� ��������� � ������ "�������" - RF_DLTD-)
     FROM dual       
    ) v
ON (v.status_cd = t.status_cd)
WHEN NOT MATCHED THEN     
  INSERT (status_cd, review_type_cd, can_nhrit_fl, viewd_by_owner_actvy_type_cd, viewd_result_status_cd, status_displ_order_nb, prsdnc_order_nb, closed_status_fl)
  VALUES (v.status_cd, v.review_type_cd, v.can_nhrit_fl, v.viewd_by_owner_actvy_type_cd, v.viewd_result_status_cd, v.status_displ_order_nb, v.prsdnc_order_nb, v.closed_status_fl)
WHEN MATCHED THEN     
  UPDATE
     SET review_type_cd = v.review_type_cd, 
         can_nhrit_fl = v.can_nhrit_fl, 
         viewd_by_owner_actvy_type_cd = v.viewd_by_owner_actvy_type_cd, 
         viewd_result_status_cd = v.viewd_result_status_cd, 
         status_displ_order_nb = v.status_displ_order_nb, 
         prsdnc_order_nb = v.prsdnc_order_nb, 
         closed_status_fl = v.closed_status_fl;

UPDATE mantas.kdd_review_status
   SET viewd_by_owner_actvy_type_cd = 'MTS001',
       viewd_result_status_cd = case when status_cd in ('RF_REPEAT')
                                     then 'OP'
                                     when status_cd in ('RF_DELETE-', 'RF_RA-')
                                     then 'RF_OP-'
                                     when status_cd in ('RF_REPEAT+', 'RF_RA+')
                                     then 'RF_OP+'
                                     when status_cd in ('RF_DELETE=', 'RF_RA=')
                                     then 'RF_OP='
                                end   
 WHERE status_cd in ('RF_REPEAT', 'RF_DELETE-', 'RF_RA-', 'RF_REPEAT+', 'RF_RA+', 'RF_DELETE=', 'RF_RA=');
                                       
         
MERGE INTO mantas.kdd_code_set_trnln t USING
  (SELECT 'AlertStatus' as code_set, 'RF_OES' as code_val, '' as src_sys_cd, '�������� ��������' as code_disp_tx
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_READY', '', '������� ��������'
     FROM dual       
   UNION ALL
   SELECT 'AlertStatus', 'RF_DONE', '', '����������'
     FROM dual       
   UNION ALL
   SELECT 'AlertStatus', 'RF_CANCEL', '', '�� �������� ��������'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_REPEAT', '', '��������� ������������'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_DELETE-', '', '��������'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_RA-', '', '�������� (����� �������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_OP-', '', '�������� (� ������) (�� ������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_FL-', '', '�������� (���. ������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_DLTD-', '', '�������'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_REPEAT+', '', '!���������� (��������� ������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_RA+', '', '!���������� (��������� ������������, ����� �������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_OP+', '', '!���������� (��������� ������������, � ������) (�� ������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_FL+', '', '!���������� (��������� ������������, ���. ������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_OES+', '', '������ ������'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_READY+', '', '������� �������� (������ ������)'
     FROM dual       
   UNION ALL
   SELECT 'AlertStatus', 'RF_CANCEL+', '', '������ �������� (��������� ������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_CREADY+', '', '������� �������� (������ ��������)'
     FROM dual       
   UNION ALL
   SELECT 'AlertStatus', 'RF_DELETE=', '', '!���������� (��������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_RA=', '', '!���������� (��������, ����� �������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_OP=', '', '!���������� (��������, � ������) (�� ������������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_FL=', '', '!���������� (��������, ���. ������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_CANCEL=', '', '������ �������� (��������)'
     FROM dual
   UNION ALL
   SELECT 'AlertStatus', 'RF_READY=', '', '������� �������� (��������)'
     FROM dual       
   ) v
ON (v.code_set = t.code_set and v.code_val = t.code_val)
WHEN NOT MATCHED THEN     
  INSERT (code_set, code_val, src_sys_cd, code_disp_tx)
  VALUES (v.code_set, v.code_val, v.src_sys_cd, v.code_disp_tx)
WHEN MATCHED THEN     
  UPDATE
     SET src_sys_cd = v.src_sys_cd, 
         code_disp_tx = v.code_disp_tx;

-- ����������� � ��������� ������������ �������� ��� ��������
UPDATE mantas.kdd_activity_type_cd
   SET actvy_type_nm = decode(actvy_type_cd, 'MTS000', '��������', 
                                             'MTS001', '�������', 
                                             'MTS002', '�������� �����������', 
                                             'MTS003', '�������� ��������������', 
                                             'MTS008', '���������� ����',
                                             'MTS018', '�������� ���� ���������',
                                             'MTS110', '��������� �����',
                                             'MTS112', '��������� ���. ������',
                                             'MTS554', '������������� �������',
                                             'MTS888', '������� ������������� ����'),
       -- ������� ��������� �������� "�������� ��������������" � REAS �� ACT � �������� "�������� �����������" � EVID �� ACT, ����� ��� ������������ ������ � ���������� ����������
       actvy_cat_cd = case when actvy_type_cd in ('MTS003', 'MTS002') then 'ACT' else actvy_cat_cd end,
       -- ������� ��������� ������ ��� ��������, ������������ ������������ � ������ ��������� (��������� ������ ������ ������������ � ������ �������� ������� ������)
       next_review_status_cd = case when actvy_type_cd in ('MTS001', 'MTS003', 'MTS018', 'MTS110', 'MTS112') then to_char(null) else next_review_status_cd end,
       -- ������ ���� �������������� ����� ������ ����� ��������� ��� �������� '��������� �����', '��������� ���. ������'
       req_due_date_fl = case when actvy_type_cd in ('MTS110', 'MTS112') then 'N' else req_due_date_fl end,
       -- ���������� �������� "�������� �����������" ����� � �����
       displ_order_nb =  case when actvy_type_cd in ('MTS002') then 190 else displ_order_nb end
 WHERE actvy_type_cd in ('MTS000', 'MTS001', 'MTS002', 'MTS003', 'MTS008', 'MTS018', 'MTS110', 'MTS112', 'MTS554', 'MTS888');  

UPDATE mantas.kdd_activity_type_cd
   SET actvy_type_short_nm = actvy_type_nm
 WHERE actvy_type_cd in ('MTS000', 'MTS001', 'MTS002', 'MTS003', 'MTS008', 'MTS018', 'MTS110', 'MTS112', 'MTS554', 'MTS888');  

-- ����������� ����� �������� ��� ��������, �� �������� � �������� �������, ������� ���������, �����   
DELETE FROM mantas.KDD_ROLE_ACTIVITY_TYPE       WHERE ACTVY_TYPE_CD in ('RF_OES', 'RF_CANCEL', 'RF_SETOPOK', 'RF_READY', 'RF_EXPORT', 'RF_REPEAT', 'RF_RUN', 'RF_OES#', 'RF_CANCEL#', 'RF_IGNORE#', 'RF_DLTD-', 'RF_DELETE-', 'RF_RESTORE', 'RF_OESCOPY', 'RF_SETKGR', 'RF_DBLOES', 'RF_OES$');
DELETE FROM mantas.KDD_SCNRO_CLASS_ACTVY_TYPE   WHERE ACTVY_TYPE_CD in ('RF_OES', 'RF_CANCEL', 'RF_SETOPOK', 'RF_READY', 'RF_EXPORT', 'RF_REPEAT', 'RF_RUN', 'RF_OES#', 'RF_CANCEL#', 'RF_IGNORE#', 'RF_DLTD-', 'RF_DELETE-', 'RF_RESTORE', 'RF_OESCOPY', 'RF_SETKGR', 'RF_DBLOES', 'RF_OES$');
DELETE FROM mantas.KDD_ACTVY_TYPE_REVIEW_STATUS WHERE ACTVY_TYPE_CD in ('RF_OES', 'RF_CANCEL', 'RF_SETOPOK', 'RF_READY', 'RF_EXPORT', 'RF_REPEAT', 'RF_RUN', 'RF_OES#', 'RF_CANCEL#', 'RF_IGNORE#', 'RF_DLTD-', 'RF_DELETE-', 'RF_RESTORE', 'RF_OESCOPY', 'RF_SETKGR', 'RF_DBLOES', 'RF_OES$');
DELETE FROM mantas.RF_KDD_ACTVY_TYPE_NEW_STATUS;
DELETE FROM mantas.KDD_ACTVY_TYPE_REVIEW_STATUS WHERE STATUS_CD like 'RF\_%' escape '\';

MERGE INTO mantas.kdd_activity_type_cd t USING
  (SELECT 'RF_OES' as actvy_type_cd, 'None' as actvy_type_desc_tx, 'N' as globl_fl, 'RF_OES' as short_desc_tx, 'RF_OES' as next_review_status_cd, 
          'N' req_cmmnt_fl, '1' as displ_order_nb, '' as dflt_due_dt_lm, 'ACT' as actvy_cat_cd, 'N' as prmpt_fl, '' as prmpt_denied_status_cd, 
          'N' reasn_actvy_fl, '' as alert_suppr_durn, '' as export_dir_ref, '�������� �������� (���)' as actvy_type_nm, '�������� �������� (���)' as actvy_type_short_nm, 
          'Y' as rptg_dsply_fl, 'N' as mantas_actvy_type_fl, '' as email_form_nm, '' as email_format_nm, 'N' as req_reasn_fl, '' as cls_class_cd, 
          'N' as auto_close_fl, '' as doc_tmplt_nm, 'N' as post_action_avail_fl, 'N' as req_due_date_fl, 'N' as promote_to_case_fl, 
          '' as rltd_actvy_type_cd, '' as reg_type_cd,
          'CREATE' as rf_oes_actvy_type_cd, '1' as rf_oes_action
     FROM dual
   UNION ALL
   SELECT 'RF_CANCEL', 'None', 'N', 'RF_CANCEL', 'RF_CANCEL', 
          'N', '2', '', 'ACT', 'N', '', 
          'N', '', '', '�� �������� �������� (������)', '�� �������� �������� (������)', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          'DELETE', ''
     FROM dual
   UNION ALL
   SELECT 'RF_SETOPOK', 'None', 'N', 'RF_SETOPOK', '', 
          'N', '3', '', 'ACT', 'N', '', 
          'N', '', '', '�������� ���(�) ���� ��������', '�������� ���(�) ���� ��������', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_READY', 'None', 'N', 'RF_READY', '', /*����� ������ - ���� RF_READY, ���� RF_READY+, ���� RF_CREADY+, ���� RF_READY= (������� �������� ��������� ������, ���� ������ �� ������/��������, ���� ������ �� �������� � ����� � ��������� ��������)*/
          'N', '4', '', 'ACT', 'N', '', 
          'N', '', '', '�������� �� ��������', '�������� �� ��������', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          'CHECK', ''
     FROM dual
   UNION ALL
   SELECT 'RF_EXPORT', 'None', 'N', 'RF_EXPORT', '', /*����� ������ - ���� RF_DONE, ���� RF_CANCEL (��������� ������ �� ��������, �������� �� �������� ��������), ���� RF_DLTD (��������� ������ �� �������� � ����� � ��������� ��������)*/
          'N', '5', '', 'ACT', 'N', '', 
          'N', '', '', '��������� � ������', '��������� � ������', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          'SEND', ''
     FROM dual
   UNION ALL
   SELECT 'RF_REPEAT', 'None', 'N', 'RF_REPEAT', '', /*����� ������ - ���� RF_REPEAT (���� ������� ������ �� �������� ���������), ���� RF_REPEAT+' (���� �����)*/
          'N', '6', '', 'ACT', 'N', '', 
          'N', '', '', '��������� ������������', '��������� ������������', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_RUN', 'None', 'N', 'RF_RUN', '', 
          'N', '7', '', 'ACT', 'N', '', 
          'N', '', '', '��������� �������� (���������)', '��������� �������� (���������)', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_OES#', 'None', 'N', 'RF_OES+', 'RF_OES+', 
          'N', '8', '', 'ACT', 'N', '', 
          'N', '', '', '��������� ������ ���', '��������� ������ ���', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          'CREATE', '3'
     FROM dual
   UNION ALL
   SELECT 'RF_CANCEL#', 'None', 'N', 'RF_CANCEL+', '', /*����� ������ - ���� RF_CANCEL+', ���� RF_CANCEL= (������ �������� ���� � ����� � ��������� �������������, ���� � ����� � ��������� ��������)*/
          'N', '9', '', 'ACT', 'N', '', 
          'N', '', '', '��������� �������� ���', '��������� �������� ���', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          'CREATE', '4'
     FROM dual
   UNION ALL
   SELECT 'RF_IGNORE#', 'None', 'N', 'RF_IGNORE+', '', /*����� ������ - ���� RF_DONE, ���� RF_DLTD- (������ ������/�������� � ����� � ��������� �������������, ���� ������ �������� � ����� � ��������� �������� ��������������, � ������ ������ �������� � ������� "����������", �� ������ - "�������")*/
          'N', '10', '', 'ACT', 'N', '', 
          'N', '', '', '������/�������� ��� �� ���������', '������/�������� ��� �� ���������', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_DLTD-', 'None', 'N', 'RF_DLTD-', 'RF_DLTD-', 
          'N', '11', '', 'ACT', 'N', '', 
          'N', '', '', '����������� ��������', '����������� ��������', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          'DELETE', ''
     FROM dual
   UNION ALL
   -- ����������� ��������
   SELECT 'RF_DELETE-', 'None', 'N', 'RF_DELETE-', '', /*����� ������ - ���� RF_DELETE- (��������), ���� RF_DELETE= (�������� ����� �������� ���������), ���� RF_DLTD- (������� - ��� ��������, �� ���������� ��������)*/
          'N', '999', '', 'ACT', 'N', '', 
          'N', '', '', '������������ ��������', '������������ ��������', /*����������� ��������, ���������� ������������ ���������� ���������*/
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_RESTORE', 'None', 'N', 'RF_RESTORE', '', /*����� ������ - ���� RF_REPEAT (��������� ������������), ���� RF_REPEAT+ (��������� ������������ ����� �������� ���������)*/
          'N', '999', '', 'ACT', 'N', '', 
          'N', '', '', '������������ ��������������', '������������ ��������������', /*����������� ��������, ���������� ������������ ���������� ���������*/
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   -- ��������, ����������� �� ����������� ����� ��������
   SELECT 'RF_OESCOPY', 'None', 'N', 'RF_OESCOPY', '', 
          'N', '999', '', 'EVID', 'N', '', 
          'N', '', '', '������� ����� ��� (��������� ������ ���)', '������� ����� ��� (��������� ������ ���)', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_SETKGR', 'None', 'N', 'RF_SETKGR', '', 
          'N', '999', '', 'EVID', 'N', '', 
          'N', '', '', '������� ������� �����', '������� ������� �����', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_DBLOES', 'None', 'N', 'RF_DBLOES', '', 
          'N', '999', '', 'EVID', 'N', '', 
          'N', '', '', '�������� �������� � ���� �������� ����� (2 ���)', '�������� �������� � ���� �������� ����� (2 ���)', 
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
   UNION ALL
   SELECT 'RF_OES$', 'None', 'N', 'RF_OES$', '', /*����� ������ - ���� RF_OES/RF_OES+ (�������� ��������/������ ������), ���� RF_REPEAT/RF_REPEAT+ (���� ����� � ������� �� �������� ������� - ��������/�� ��������)*/
          'N', '999', '', 'EVID', 'N', '', 
          'N', '', '', '�������� �������� �� ����', '�������� �������� �� ����', /*������ ��������, ����������� ��� ���������� ��� ��������, ���������� ������������� �� ������ �������� (���������� �� ������ �������)*/
          'Y', 'N', '', '', 'N', '', 
          'N', '', 'N', 'N', 'N', 
          '', '',
          '', ''
     FROM dual
     ) v
ON (v.actvy_type_cd = t.actvy_type_cd)
WHEN NOT MATCHED THEN     
  INSERT (actvy_type_cd, actvy_type_desc_tx, globl_fl, short_desc_tx, next_review_status_cd, 
          req_cmmnt_fl, displ_order_nb, dflt_due_dt_lm, actvy_cat_cd, prmpt_fl, prmpt_denied_status_cd, 
          reasn_actvy_fl, alert_suppr_durn, export_dir_ref, actvy_type_nm, actvy_type_short_nm, 
          rptg_dsply_fl, mantas_actvy_type_fl, email_form_nm, email_format_nm, req_reasn_fl, cls_class_cd, 
          auto_close_fl, doc_tmplt_nm, post_action_avail_fl, req_due_date_fl, promote_to_case_fl, 
          rltd_actvy_type_cd, reg_type_cd, rf_oes_actvy_type_cd, rf_oes_action)
  VALUES (v.actvy_type_cd, v.actvy_type_desc_tx, v.globl_fl, v.short_desc_tx, v.next_review_status_cd, 
          v.req_cmmnt_fl, v.displ_order_nb, v.dflt_due_dt_lm, v.actvy_cat_cd, v.prmpt_fl, v.prmpt_denied_status_cd, 
          v.reasn_actvy_fl, v.alert_suppr_durn, v.export_dir_ref, v.actvy_type_nm, v.actvy_type_short_nm, 
          v.rptg_dsply_fl, v.mantas_actvy_type_fl, v.email_form_nm, v.email_format_nm, v.req_reasn_fl, v.cls_class_cd, 
          v.auto_close_fl, v.doc_tmplt_nm, v.post_action_avail_fl, v.req_due_date_fl, v.promote_to_case_fl, 
          v.rltd_actvy_type_cd, v.reg_type_cd, v.rf_oes_actvy_type_cd, v.rf_oes_action)
WHEN MATCHED THEN  
  UPDATE 
     SET actvy_type_desc_tx     = v.actvy_type_desc_tx, 
         globl_fl               = v.globl_fl, 
         short_desc_tx          = v.short_desc_tx, 
         next_review_status_cd  = v.next_review_status_cd, 
         req_cmmnt_fl           = v.req_cmmnt_fl, 
         displ_order_nb         = v.displ_order_nb, 
         dflt_due_dt_lm         = v.dflt_due_dt_lm, 
         actvy_cat_cd           = v.actvy_cat_cd, 
         prmpt_fl               = v.prmpt_fl, 
         prmpt_denied_status_cd = v.prmpt_denied_status_cd, 
         reasn_actvy_fl         = v.reasn_actvy_fl, 
         alert_suppr_durn       = v.alert_suppr_durn, 
         export_dir_ref         = v.export_dir_ref, 
         actvy_type_nm          = v.actvy_type_nm, 
         actvy_type_short_nm    = v.actvy_type_short_nm, 
         rptg_dsply_fl          = v.rptg_dsply_fl, 
         mantas_actvy_type_fl   = v.mantas_actvy_type_fl, 
         email_form_nm          = v.email_form_nm, 
         email_format_nm        = v.email_format_nm, 
         req_reasn_fl           = v.req_reasn_fl, 
         cls_class_cd           = v.cls_class_cd, 
         auto_close_fl          = v.auto_close_fl, 
         doc_tmplt_nm           = v.doc_tmplt_nm, 
         post_action_avail_fl   = v.post_action_avail_fl, 
         req_due_date_fl        = v.req_due_date_fl, 
         promote_to_case_fl     = v.promote_to_case_fl, 
         rltd_actvy_type_cd     = v.rltd_actvy_type_cd, 
         reg_type_cd            = v.reg_type_cd,
         rf_oes_actvy_type_cd   = v.rf_oes_actvy_type_cd,
         rf_oes_action          = v.rf_oes_action;   

INSERT INTO mantas.KDD_ACTVY_TYPE_REVIEW_STATUS
       (-- �������, � ������� �������� ��������� �������� ���������� ��������
        SELECT 'RF_OES', 'NW' FROM dual UNION ALL
        SELECT 'RF_OES', 'RA' FROM dual UNION ALL
        SELECT 'RF_OES', 'OP' FROM dual UNION ALL
        SELECT 'RF_OES', 'FL' FROM dual UNION ALL
        SELECT 'RF_OES', 'RF_REPEAT' FROM dual UNION ALL
        -- �������, � ������� �������� ������ �������� (�� �������� ��������)
        SELECT 'RF_CANCEL', 'NW' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'RA' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'OP' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'FL' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'RF_REPEAT' FROM dual UNION ALL
        -- �������, � ������� �������� ��������� ����� ����
        SELECT 'RF_SETOPOK', 'NW' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'RA' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'OP' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'FL' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'RF_REPEAT+' FROM dual UNION ALL        
        SELECT 'RF_SETOPOK', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'RF_SETOPOK', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'RF_SETOPOK', 'RF_FL+' FROM dual UNION ALL        
        -- �������, � ������� �������� �������� �� ��������
        SELECT 'RF_READY', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_READY', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_READY', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_READY', 'RF_CANCEL=' FROM dual UNION ALL        
        -- �������, � ������� �������� �������� � ������
        SELECT 'RF_EXPORT', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'RF_CREADY+' FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'RF_READY=' FROM dual UNION ALL        
        -- �������, � ������� �������� ��������� ������������
        SELECT 'RF_REPEAT', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_CANCEL' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_CANCEL+' FROM dual UNION ALL 
        SELECT 'RF_REPEAT', 'RF_CREADY+' FROM dual UNION ALL
        -- �������, � ������� �������� �������� �������� (���������)
        SELECT 'RF_RUN', 'NW' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RA' FROM dual UNION ALL
        SELECT 'RF_RUN', 'OP' FROM dual UNION ALL
        SELECT 'RF_RUN', 'FL' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_CANCEL' FROM dual UNION ALL 
        SELECT 'RF_RUN', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_RA+' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_OP+' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_FL+' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_RUN', 'RF_CANCEL+' FROM dual UNION ALL                               
        SELECT 'RF_RUN', 'RF_CREADY+' FROM dual UNION ALL
        -- �������, � ������� �������� ������ ������ ���
        SELECT 'RF_OES#', 'RF_REPEAT+' FROM dual UNION ALL        
        SELECT 'RF_OES#', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'RF_OES#', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'RF_OES#', 'RF_FL+' FROM dual UNION ALL        
        -- �������, � ������� �������� ������ �������� ���
        SELECT 'RF_CANCEL#', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'RF_CANCEL#', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'RF_CANCEL#', 'RF_FL+' FROM dual UNION ALL        
        SELECT 'RF_CANCEL#', 'RF_DELETE=' FROM dual UNION ALL                
        SELECT 'RF_CANCEL#', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'RF_CANCEL#', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'RF_CANCEL#', 'RF_FL=' FROM dual UNION ALL        
        -- �������, � ������� �������� �������� "������/�������� ��� �� ���������"
        SELECT 'RF_IGNORE#', 'RF_REPEAT+' FROM dual UNION ALL        
        SELECT 'RF_IGNORE#', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'RF_IGNORE#', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'RF_IGNORE#', 'RF_FL+' FROM dual UNION ALL        
        SELECT 'RF_IGNORE#', 'RF_DELETE=' FROM dual UNION ALL                
        SELECT 'RF_IGNORE#', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'RF_IGNORE#', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'RF_IGNORE#', 'RF_FL=' FROM dual UNION ALL        
        -- �������, � ������� �������� ������������� ��������
        SELECT 'RF_DLTD-', 'RF_DELETE-' FROM dual UNION ALL        
        SELECT 'RF_DLTD-', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'RF_DLTD-', 'RF_OP-' FROM dual UNION ALL        
        SELECT 'RF_DLTD-', 'RF_FL-' FROM dual UNION ALL                
        -- �������, � ������� �������� ��������� ��������
        SELECT 'RF_DELETE-', 'NW' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RA' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'OP' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'FL' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_CANCEL' FROM dual UNION ALL 
        SELECT 'RF_DELETE-', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_RA+' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_OP+' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_FL+' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_CREADY+' FROM dual UNION ALL
        -- �������, � ������� �������� ��������������
        SELECT 'RF_RESTORE', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_RESTORE', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_OP-' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_FL-' FROM dual UNION ALL                
        SELECT 'RF_RESTORE', 'RF_DLTD-' FROM dual UNION ALL                
        SELECT 'RF_RESTORE', 'RF_DELETE=' FROM dual UNION ALL                
        SELECT 'RF_RESTORE', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_FL=' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_CANCEL=' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_READY=' FROM dual UNION ALL
        -- �������, � ������� ������� �������� "������� ����� ��� (��������� ������ ���)" - �������� ����� ���
        SELECT 'RF_OESCOPY', 'NW' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RA' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'OP' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'FL' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_CANCEL' FROM dual UNION ALL 
        SELECT 'RF_OESCOPY', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_RA+' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_OP+' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_FL+' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'RF_CREADY+' FROM dual UNION ALL
        -- �������, � ������� ������� �������� "������� ������� �����" - ������������ ��������� ������� ����� �����������/���������� ��� ��������
        SELECT 'RF_SETKGR', 'NW' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RA' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'OP' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'FL' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_CANCEL' FROM dual UNION ALL 
        SELECT 'RF_SETKGR', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_RA+' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_OP+' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_FL+' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'RF_CREADY+' FROM dual UNION ALL
        -- �������, � ������� ������� �������� "�������� �������� � ���� �������� ����� (2 ���)" - �������� ������� ������
        SELECT 'RF_DBLOES', 'NW' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RA' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'OP' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'FL' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_CANCEL' FROM dual UNION ALL 
        SELECT 'RF_DBLOES', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_RA+' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_OP+' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_FL+' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'RF_CREADY+' FROM dual UNION ALL
        -- �������, � ������� �������� �������� ��� ��������� "�������� �������� �� ����"
        SELECT 'RF_OES$', 'NW' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RA' FROM dual UNION ALL
        SELECT 'RF_OES$', 'OP' FROM dual UNION ALL
        SELECT 'RF_OES$', 'FL' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_CANCEL' FROM dual UNION ALL 
        SELECT 'RF_OES$', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_RA+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_OP+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_FL+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_CREADY+' FROM dual UNION ALL
        -- ����� �������, � ������� �������� ���������� �����������
        SELECT 'MTS002', 'RF_OES' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_READY' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_DONE' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_CANCEL' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'MTS002', 'RF_OP-' FROM dual UNION ALL        
        SELECT 'MTS002', 'RF_FL-' FROM dual UNION ALL                
        SELECT 'MTS002', 'RF_DLTD-' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'MTS002', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'MTS002', 'RF_FL+' FROM dual UNION ALL                
        SELECT 'MTS002', 'RF_OES+' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_READY+' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_CREADY+' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS002', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'MTS002', 'RF_FL=' FROM dual UNION ALL                
        SELECT 'MTS002', 'RF_CANCEL=' FROM dual UNION ALL
        SELECT 'MTS002', 'RF_READY=' FROM dual UNION ALL
        -- ����� �������, � ������� �������� ��������� �������������� 
        SELECT 'MTS003', 'RF_OES' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_READY' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_OP-' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_FL-' FROM dual UNION ALL                
        SELECT 'MTS003', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_FL+' FROM dual UNION ALL                
        SELECT 'MTS003', 'RF_OES+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_READY+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_CREADY+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_FL=' FROM dual UNION ALL                
        SELECT 'MTS003', 'RF_CANCEL=' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_READY=' FROM dual UNION ALL
        -- ����� �������, � ������� �������� ������������ �����
        SELECT 'MTS008', 'RF_OES' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_READY' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_DONE' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_CANCEL' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'MTS008', 'RF_OP-' FROM dual UNION ALL        
        SELECT 'MTS008', 'RF_FL-' FROM dual UNION ALL                
        SELECT 'MTS008', 'RF_DLTD-' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'MTS008', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'MTS008', 'RF_FL+' FROM dual UNION ALL                
        SELECT 'MTS008', 'RF_OES+' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_READY+' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_CREADY+' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS008', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'MTS008', 'RF_FL=' FROM dual UNION ALL                
        SELECT 'MTS008', 'RF_CANCEL=' FROM dual UNION ALL
        SELECT 'MTS008', 'RF_READY=' FROM dual UNION ALL
        -- ����� �������, � ������� �������� ��������� ����� ��������� - ���� ���������, � ����� ������� �������� ��������� �����,
        -- ������ �����, ��� ������������ �� ���������, � ����� ���� 6001
        -- ����� �������, � ������� �������� �������� "��������� �����"
        SELECT 'MTS110', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'MTS110', 'RF_OP-' FROM dual UNION ALL        
        SELECT 'MTS110', 'RF_FL-' FROM dual UNION ALL                
        SELECT 'MTS110', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'MTS110', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'MTS110', 'RF_FL+' FROM dual UNION ALL                
        SELECT 'MTS110', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS110', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'MTS110', 'RF_FL=' FROM dual UNION ALL                        
        -- ����� �������, � ������� �������� �������� "��������� ���. ������"
        SELECT 'MTS112', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'MTS112', 'RF_OP-' FROM dual UNION ALL        
        SELECT 'MTS112', 'RF_FL-' FROM dual UNION ALL                
        SELECT 'MTS112', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'MTS112', 'RF_OP+' FROM dual UNION ALL        
        SELECT 'MTS112', 'RF_FL+' FROM dual UNION ALL                
        SELECT 'MTS112', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS112', 'RF_OP=' FROM dual UNION ALL        
        SELECT 'MTS112', 'RF_FL=' FROM dual                                
        /*SELECT 'RF_OES', 'RO' FROM dual UNION ALL*/
        /*SELECT 'RF_CANCEL', 'RO' FROM dual UNION ALL*/
        /*SELECT 'RF_SETOPOK', 'RO' FROM dual UNION ALL*/
        /*SELECT 'MTS005', 'RF_CANCEL' FROM dual UNION ALL*/
        /*SELECT 'MTS005', 'RF_DONE' FROM dual UNION ALL*/
        );

-- �������� � ����� ������ �� ��������, � ����������� �� �������� � �������� �������
INSERT INTO mantas.RF_KDD_ACTVY_TYPE_NEW_STATUS
       (-- ����� ������� ��� �������� "�������� �� ��������"
        SELECT 'RF_READY', 'RF_OES', 'RF_READY' FROM dual UNION ALL
        SELECT 'RF_READY', 'RF_OES+', 'RF_READY+' FROM dual UNION ALL
        SELECT 'RF_READY', 'RF_CANCEL+', 'RF_CREADY+' FROM dual UNION ALL
        SELECT 'RF_READY', 'RF_CANCEL=', 'RF_READY=' FROM dual UNION ALL
        -- ����� ������� ��� �������� "��������� � ������"
        SELECT 'RF_EXPORT', 'RF_READY', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'RF_READY+', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'RF_CREADY+', 'RF_CANCEL' FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'RF_READY=', 'RF_DLTD-' FROM dual UNION ALL
        -- ����� ������� ��� �������� "��������� ������������"
        SELECT 'RF_REPEAT', 'RF_OES', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_READY', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_CANCEL', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_DONE', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_OES+', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_READY+', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_CANCEL+', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'RF_CREADY+', 'RF_REPEAT+' FROM dual UNION ALL
        -- ����� ������� ��� �������� "��������� �������� ���"
        SELECT 'RF_CANCEL#', 'RF_REPEAT+', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_RA+', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_OP+', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_FL+', 'RF_CANCEL+' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_DELETE=', 'RF_CANCEL=' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_RA=', 'RF_CANCEL=' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_OP=', 'RF_CANCEL=' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'RF_FL=', 'RF_CANCEL=' FROM dual UNION ALL
        -- ����� ������� ��� �������� "������/�������� ��� �� ���������"
        SELECT 'RF_IGNORE#', 'RF_REPEAT+', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'RF_RA+', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'RF_OP+', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'RF_FL+', 'RF_DONE' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'RF_DELETE=', 'RF_DLTD-' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'RF_RA=', 'RF_DLTD-' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'RF_OP=', 'RF_DLTD-' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'RF_FL=', 'RF_DLTD-' FROM dual UNION ALL
        -- ����� ������� ��� �������� "�������� ��������������" 
        SELECT 'MTS003', 'NW', 'RA' FROM dual UNION ALL
        SELECT 'MTS003', 'RA', 'RA' FROM dual UNION ALL
        SELECT 'MTS003', 'OP', 'RA' FROM dual UNION ALL
        SELECT 'MTS003', 'FL', 'RA' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_OES', 'RA' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_READY', 'RA' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_REPEAT', 'RA' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_DELETE-', 'RF_RA-' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_RA-', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_OP-', 'RF_RA-' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_FL-', 'RF_RA-' FROM dual UNION ALL                
        SELECT 'MTS003', 'RF_REPEAT+', 'RF_RA+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_RA+', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_OP+', 'RF_RA+' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_FL+', 'RF_RA+' FROM dual UNION ALL                
        SELECT 'MTS003', 'RF_OES+', 'RF_RA+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_READY+', 'RF_RA+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_CANCEL+', 'RF_RA+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_CREADY+', 'RF_RA+' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_DELETE=', 'RF_RA=' FROM dual UNION ALL
        SELECT 'MTS003', 'RF_RA=', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_OP=', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_FL=', 'RF_RA=' FROM dual UNION ALL                
        SELECT 'MTS003', 'RF_CANCEL=', 'RF_RA=' FROM dual UNION ALL        
        SELECT 'MTS003', 'RF_READY=', 'RF_RA=' FROM dual UNION ALL
        -- ����� ������� ��� �������� "��������� �����"
        SELECT 'MTS110', 'NW', 'FL' FROM dual UNION ALL
        SELECT 'MTS110', 'RA', 'FL' FROM dual UNION ALL
        SELECT 'MTS110', 'OP', 'FL' FROM dual UNION ALL
        SELECT 'MTS110', 'FL', 'FL' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_REPEAT', 'FL' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_DELETE-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_RA-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_OP-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_FL-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_REPEAT+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_RA+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_OP+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_FL+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_DELETE=', 'RF_FL=' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_RA=', 'RF_FL=' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_OP=', 'RF_FL=' FROM dual UNION ALL
        SELECT 'MTS110', 'RF_FL=', 'RF_FL=' FROM dual UNION ALL
        -- ����� ������� ��� �������� "��������� ���. ������"
        SELECT 'MTS112', 'NW', 'FL' FROM dual UNION ALL
        SELECT 'MTS112', 'RA', 'FL' FROM dual UNION ALL
        SELECT 'MTS112', 'OP', 'FL' FROM dual UNION ALL
        SELECT 'MTS112', 'FL', 'FL' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_REPEAT', 'FL' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_DELETE-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_RA-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_OP-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_FL-', 'RF_FL-' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_REPEAT+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_RA+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_OP+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_FL+', 'RF_FL+' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_DELETE=', 'RF_FL=' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_RA=', 'RF_FL=' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_OP=', 'RF_FL=' FROM dual UNION ALL
        SELECT 'MTS112', 'RF_FL=', 'RF_FL=' FROM dual UNION ALL       
        -- ����� ������� ��� �������� "������������ ��������"
        SELECT 'RF_DELETE-', 'NW', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RA', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'OP', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'FL', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_OES', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_READY', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_DONE', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_CANCEL', 'RF_DLTD-' FROM dual UNION ALL /* ��������, �� ���������� ��������, ��������� � ��������� ��� ������������� �� ������������*/
        SELECT 'RF_DELETE-', 'RF_REPEAT', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_REPEAT+', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_RA+', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_OP+', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_FL+', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_OES+', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_READY+', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_CANCEL+', 'RF_DELETE=' FROM dual UNION ALL
        SELECT 'RF_DELETE-', 'RF_CREADY+', 'RF_DELETE=' FROM dual UNION ALL
        -- ����� ������� ��� �������� "������������ ��������������"
        SELECT 'RF_RESTORE', 'RF_DELETE-', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_RESTORE', 'RF_RA-', 'RF_REPEAT' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_OP-', 'RF_REPEAT' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_FL-', 'RF_REPEAT' FROM dual UNION ALL                
        SELECT 'RF_RESTORE', 'RF_DLTD-', 'RF_REPEAT' FROM dual UNION ALL                
        SELECT 'RF_RESTORE', 'RF_DELETE=', 'RF_REPEAT+' FROM dual UNION ALL                
        SELECT 'RF_RESTORE', 'RF_RA=', 'RF_REPEAT+' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_OP=', 'RF_REPEAT+' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_FL=', 'RF_REPEAT+' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_CANCEL=', 'RF_REPEAT+' FROM dual UNION ALL        
        SELECT 'RF_RESTORE', 'RF_READY=', 'RF_REPEAT+' FROM dual UNION ALL         
        -- ����� ������� ��� �������� "�������� �������� �� ����" - ������� �� �������� ������� ��������/�� �������� ������������� �� ��������� ������������, ����� - ������������� �� �������� ��������/������ ������
        SELECT 'RF_OES$', 'NW', 'RF_REPEAT'  FROM dual UNION ALL
        SELECT 'RF_OES$', 'RA', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_OES$', 'OP', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_OES$', 'FL', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_OES', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_READY', 'RF_OES' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_DONE', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_CANCEL', 'RF_OES' FROM dual UNION ALL 
        SELECT 'RF_OES$', 'RF_REPEAT', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_REPEAT+', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_RA+', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_OP+', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_FL+', 'RF_REPEAT+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_OES+', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_READY+', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_CANCEL+', 'RF_OES+' FROM dual UNION ALL
        SELECT 'RF_OES$', 'RF_CREADY+', 'RF_OES+' FROM dual 
        );

-- ����������� ����� �������� � ������ ��������
INSERT INTO mantas.KDD_SCNRO_CLASS_ACTVY_TYPE
       (SELECT 'ML', 'RF_OES' FROM dual UNION ALL
        SELECT 'ML', 'RF_READY' FROM dual UNION ALL
        SELECT 'ML', 'RF_CANCEL' FROM dual UNION ALL
        SELECT 'ML', 'RF_SETOPOK' FROM dual UNION ALL
        SELECT 'ML', 'RF_EXPORT' FROM dual UNION ALL
        SELECT 'ML', 'RF_REPEAT' FROM dual UNION ALL
        SELECT 'ML', 'RF_RUN' FROM dual UNION ALL
        SELECT 'ML', 'RF_OES#' FROM dual UNION ALL
        SELECT 'ML', 'RF_CANCEL#' FROM dual UNION ALL
        SELECT 'ML', 'RF_IGNORE#' FROM dual UNION ALL
        SELECT 'ML', 'RF_DLTD-' FROM dual UNION ALL
        SELECT 'ML', 'RF_DELETE-' FROM dual UNION ALL
        SELECT 'ML', 'RF_RESTORE' FROM dual UNION ALL
        SELECT 'ML', 'RF_OESCOPY' FROM dual UNION ALL
        SELECT 'ML', 'RF_SETKGR' FROM dual UNION ALL
        SELECT 'ML', 'RF_DBLOES' FROM dual UNION ALL
        SELECT 'ML', 'RF_OES$' FROM dual 
        );

-- ����������� ����� �������� � ����� (�� ����������� �������� ���� �� ����)
INSERT INTO mantas.KDD_ROLE_ACTIVITY_TYPE
       (SELECT 'RF_OES', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_OES', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_OES', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_OES', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_OES', 'SUPPORTADM'  FROM dual UNION ALL        
        SELECT 'RF_OES', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_READY', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_READY', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_READY', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_READY', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_READY', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_READY', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_CANCEL', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_CANCEL', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_SETOPOK', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'AMSUPVISR' FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_SETOPOK', 'AMRESPNSBL'  FROM dual UNION ALL        
        --SELECT 'RF_EXPORT', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_EXPORT', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_REPEAT', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_REPEAT', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_RUN', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_RUN', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_RUN', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_RUN', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_RUN', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_RUN', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_OES#', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_OES#', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_OES#', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_OES#', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_OES#', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_OES#', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_CANCEL#', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_CANCEL#', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_IGNORE#', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_IGNORE#', 'AMRESPNSBL'  FROM dual UNION ALL        
        SELECT 'RF_DLTD-', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_DLTD-', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_DLTD-', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_DLTD-', 'AMSUPVISR'  FROM dual UNION ALL
        SELECT 'RF_DLTD-', 'SUPPORTADM'  FROM dual UNION ALL 
        SELECT 'RF_DLTD-', 'AMRESPNSBL'  FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'AMSUPVISR' FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_OESCOPY', 'AMRESPNSBL'  FROM dual UNION ALL                
        SELECT 'RF_SETKGR', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'AMSUPVISR' FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_SETKGR', 'AMRESPNSBL'  FROM dual UNION ALL                
        SELECT 'RF_DBLOES', 'AMANALYST1' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'AMANALYST2' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'AMANALYST3' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'AMSUPVISR' FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_DBLOES', 'AMRESPNSBL'  FROM dual UNION ALL                
        SELECT 'RF_OES$', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'RF_OES$', 'AMSUPVISR' FROM dual 
        );
        
-- ������� ����� ����� SUPPORTADM, AMRESPNSBL ����� �� "������" ��������
DELETE FROM mantas.KDD_ROLE_ACTIVITY_TYPE
 WHERE role_cd in ('SUPPORTADM', 'AMRESPNSBL') and
       actvy_type_cd in ('MTS000', 'MTS001', 'MTS002', 'MTS003', 'MTS008', 'MTS018', 'MTS110', 'MTS112');
INSERT INTO mantas.KDD_ROLE_ACTIVITY_TYPE
       (SELECT 'MTS000', 'SUPPORTADM' FROM dual UNION ALL
        SELECT 'MTS001', 'SUPPORTADM' FROM dual UNION ALL
        SELECT 'MTS002', 'SUPPORTADM' FROM dual UNION ALL
        SELECT 'MTS003', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'MTS008', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'MTS018', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'MTS110', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'MTS112', 'SUPPORTADM'  FROM dual UNION ALL
        SELECT 'MTS000', 'AMRESPNSBL' FROM dual UNION ALL
        SELECT 'MTS001', 'AMRESPNSBL' FROM dual UNION ALL
        SELECT 'MTS002', 'AMRESPNSBL' FROM dual UNION ALL
        SELECT 'MTS003', 'AMRESPNSBL'  FROM dual UNION ALL -- ��������� ��������������
        SELECT 'MTS008', 'AMRESPNSBL'  FROM dual UNION ALL
        SELECT 'MTS018', 'AMRESPNSBL'  FROM dual UNION ALL
        SELECT 'MTS110', 'AMRESPNSBL'  FROM dual UNION ALL
        SELECT 'MTS112', 'AMRESPNSBL'  FROM dual);
       
-- ������� �������� � ����� ��� �������������� �������� -- Closed Account, Reopen
DELETE FROM mantas.KDD_ROLE_ACTIVITY_TYPE
 WHERE actvy_type_cd in ('MTS370', 'MTS370A', 'MTS005'); 

-- �������� ������ � ��������� �������������� � ����� ��������� ��� ����� ����������� (AMANALYST<N>)
DELETE FROM mantas.KDD_ROLE_ACTIVITY_TYPE
 WHERE actvy_type_cd in ('MTS003', 'MTS018') and 
       role_cd in ('AMANALYST1', 'AMANALYST2', 'AMANALYST3'); 

-- ����������� �����������
UPDATE mantas.kdd_cmmnt
   SET cmmnt_tx = '��������� �����'
 WHERE cmmnt_id = 7996;
 
MERGE INTO mantas.kdd_cmmnt t USING
 (SELECT 101 as cmmnt_id, 'N' as edit_fl, '�� �������� ��������, ������ ������������ ��-�� �������� �������' as cmmnt_tx, 
         '5' as displ_order_nb, 'SDCT' as cmmnt_cat_cd
    FROM dual) v
ON (v.cmmnt_id = t.cmmnt_id)
WHEN NOT MATCHED THEN         
  INSERT (cmmnt_id, edit_fl, cmmnt_tx, displ_order_nb, cmmnt_cat_cd)
  VALUES (v.cmmnt_id, v.edit_fl, v.cmmnt_tx, v.displ_order_nb, v.cmmnt_cat_cd)
WHEN MATCHED THEN
  UPDATE 
     SET edit_fl = v.edit_fl, 
         cmmnt_tx = v.cmmnt_tx, 
         displ_order_nb = v.displ_order_nb, 
         cmmnt_cat_cd = v.cmmnt_cat_cd;

DELETE FROM mantas.kdd_scnro_class_cmmnt WHERE cmmnt_id = 101;
INSERT INTO mantas.kdd_scnro_class_cmmnt(scnro_class_cd, cmmnt_id) VALUES('ML', 101);

COMMIT;

