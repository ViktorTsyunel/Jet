/* ���������� ������ '+' ������ CHR(43), �. �. + ���������� �� ������*/
delete from mantas.kdd_queue_role_map where queue_seq_id > 9000;
delete from mantas.kdd_queue_master where queue_seq_id > 9000;

insert into mantas.kdd_queue_master (QUEUE_SEQ_ID, QUEUE_CD, QUEUE_DISPLAY_NM, QUEUE_TYPE, QUEUE_QUERY, MODFY_ID, MODFY_DT, COMMENT_TX)
values ('9001', 'RF_WRK', '2. ��������� ���������', 'AL', 'rv.rv_wrk_flag = 1', '', null, '�������������� ������������� ������');

insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMANALYST1', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMANALYST2', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMANALYST3', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMSUPVISR', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'SUPPORTADM', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMRESPNSBL', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMANALYTIC', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMMAINANLT', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMMETHODLG', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMMAINMETH', 'Y');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9001', 'AMAUDITOR', 'Y');

insert into mantas.kdd_queue_master (QUEUE_SEQ_ID, QUEUE_CD, QUEUE_DISPLAY_NM, QUEUE_TYPE, QUEUE_QUERY, MODFY_ID, MODFY_DT, COMMENT_TX)
values ('9002', 'RF_URG', '1. ������� ���������', 'AL', '(rv.rv_wrk_flag = 1 and mantas.rf_add_work_days(trunc(sysdate), 1) >= rv.rv_due_dt)', '', null, '');

insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMANALYST1', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMANALYST2', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMANALYST3', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMSUPVISR', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'SUPPORTADM', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMRESPNSBL', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMANALYTIC', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMMAINANLT', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMMETHODLG', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMMAINMETH', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9002', 'AMAUDITOR', 'N');

insert into mantas.kdd_queue_master (QUEUE_SEQ_ID, QUEUE_CD, QUEUE_DISPLAY_NM, QUEUE_TYPE, QUEUE_QUERY, MODFY_ID, MODFY_DT, COMMENT_TX)
values ('9003', 'RF_EXP', '3. ������� � ������', 'AL', 'rv.rv_status_cd in (''RF_CREADY''||CHR(43), ''RF_READY='', ''RF_READY'', ''RF_READY''||CHR(43))', '', null, '');

insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMANALYST1', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMANALYST2', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMANALYST3', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMSUPVISR', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'SUPPORTADM', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMRESPNSBL', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMANALYTIC', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMMAINANLT', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMMETHODLG', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMMAINMETH', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9003', 'AMAUDITOR', 'N');

insert into mantas.kdd_queue_master (QUEUE_SEQ_ID, QUEUE_CD, QUEUE_DISPLAY_NM, QUEUE_TYPE, QUEUE_QUERY, MODFY_ID, MODFY_DT, COMMENT_TX)
values ('9004', 'RF_CANCEL', '4. �� �������� �������� (�� �������)', 'AL', '(rv.rv_status_cd in (''RF_CANCEL'', ''RF_CANCEL''||CHR(43)) and trunc(rv.rv_status_dt) = trunc(sysdate))', '', null, '');

insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMANALYST1', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMANALYST2', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMANALYST3', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMSUPVISR', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'SUPPORTADM', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMRESPNSBL', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMANALYTIC', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMMAINANLT', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMMETHODLG', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMMAINMETH', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9004', 'AMAUDITOR', 'N');

insert into mantas.kdd_queue_master (QUEUE_SEQ_ID, QUEUE_CD, QUEUE_DISPLAY_NM, QUEUE_TYPE, QUEUE_QUERY, MODFY_ID, MODFY_DT, COMMENT_TX)
values ('9005', 'RF_DONE', '5. ���������� (�� �������)', 'AL', '(rv.rv_last_actvy_type_cd = ''RF_EXPORT'' and trunc(rv.rv_status_dt) = trunc(sysdate))', '', null, '');

insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMANALYST1', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMANALYST2', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMANALYST3', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMSUPVISR', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'SUPPORTADM', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMRESPNSBL', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMANALYTIC', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMMAINANLT', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMMETHODLG', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMMAINMETH', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9005', 'AMAUDITOR', 'N');

insert into mantas.kdd_queue_master (QUEUE_SEQ_ID, QUEUE_CD, QUEUE_DISPLAY_NM, QUEUE_TYPE, QUEUE_QUERY, MODFY_ID, MODFY_DT, COMMENT_TX)
values ('9006', 'RF_OVERDUE', '6. ������������', 'AL', '(rv.rv_status_cd not in (''RF_CANCEL'', ''RF_DLTD-'', ''RF_DONE'') and trunc(sysdate) > rv.rv_due_dt)', '', null, '');

insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMANALYST1', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMANALYST2', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMANALYST3', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMSUPVISR', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'SUPPORTADM', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMRESPNSBL', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMANALYTIC', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMMAINANLT', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMMETHODLG', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMMAINMETH', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9006', 'AMAUDITOR', 'N');

insert into mantas.kdd_queue_master (QUEUE_SEQ_ID, QUEUE_CD, QUEUE_DISPLAY_NM, QUEUE_TYPE, QUEUE_QUERY, MODFY_ID, MODFY_DT, COMMENT_TX)
values ('9007', 'RF_UNASSIGN', '7. �� �������� �������������', 'AL', 'rv.rv_wrk_flag = 1 and (rv.rv_owner_seq_id is null or EXISTS(select null from mantas.kdd_review_owner where owner_seq_id = rv.rv_owner_seq_id and owner_type_cd <> ''USER''))', '', null, '�������������� ������, � ������� ��� �������������� ������������');

insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9007', 'AMSUPVISR', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9007', 'AMRESPNSBL', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9007', 'SUPPORTADM', 'N');
insert into mantas.kdd_queue_role_map(queue_seq_id, role_cd, default_fl)
values ('9007', 'AMAUDITOR', 'N');

--
-- ������� ������ � ������ ��������
--
delete from mantas.kdd_queue_role_map where role_cd in ('AMANALYST1', 'AMANALYST2', 'AMANALYST3', 'AMSUPVISR') and queue_seq_id <= 9000;
-- �� ������ ������: ���� ������ ��������������� ��������� ������
--INSERT INTO mantas.kdd_queue_role_map(role_cd, queue_seq_id, default_fl)
--(
--SELECT 'AMANALYST1', 7, '' /*My New Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 8, 'Y' /*My Open Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 9, '' /*My Reassigned Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 10, '' /*My Overdue Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 11, '' /*My Near Due Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 7, '' /*My New Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 8, 'Y' /*My Open Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 9, '' /*My Reassigned Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 10, '' /*My Overdue Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 11, '' /*My Near Due Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 7, '' /*My New Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 8, 'Y' /*My Open Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 9, '' /*My Reassigned Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 10, '' /*My Overdue Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 11, '' /*My Near Due Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 7, '' /*My New Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 8, 'Y' /*My Open Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 9, '' /*My Reassigned Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 10, '' /*My Overdue Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 11, '' /*My Near Due Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 12, '' /*Management - Overdue Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 13, '' /*Management - Near Due Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 14, '' /*Management - Aged Alerts*/ FROM dual
--);

commit;


