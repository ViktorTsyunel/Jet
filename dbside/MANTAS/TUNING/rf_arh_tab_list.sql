DELETE FROM MANTAS.RF_ARH_TAB_LIST;

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'WIRE_TRXN', 1, 'WT', 'FO_TRXN_SEQ_ID', 'PK_WIRE_TRXN', 'RF_ACTIVE_PARTITION_FLAG, RF_SUBPARTITION_KEY, RF_OPOK_STATUS', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'RF_WIRE_TRXN_SCRTY', 1, 'WT', 'FO_TRXN_SEQ_ID', 'RF_WIRE_TRXN_SCRTY_PK', null, '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'CASH_TRXN', 1, 'CT', 'FO_TRXN_SEQ_ID', 'PK_CASH_TRXN', 'RF_ACTIVE_PARTITION_FLAG, RF_SUBPARTITION_KEY, RF_OPOK_STATUS', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'RF_CASH_TRXN_SCRTY', 1, 'CT', 'FO_TRXN_SEQ_ID', 'RF_CASH_TRXN_SCRTY_PK', null, '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'BACK_OFFICE_TRXN', 1, 'BOT', 'BO_TRXN_SEQ_ID', 'PK_BO_TRXN', 'RF_ACTIVE_PARTITION_FLAG, RF_SUBPARTITION_KEY, RF_OPOK_STATUS', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'ACCT', 1, 'ACCT', 'ACCT_SEQ_ID', 'RF_ACCT_SEQ_AK1', 'RF_PARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'CUST_ACCT', 1, 'ACCT', 'RF_ACCT_SEQ_ID', 'PK_CUST_ACCT', 'RF_PARTITION_KEY, RF_SUBPARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'SCRTY', 1, 'SCRTY', 'SCRTY_SEQ_ID', 'RF_SCRTY_AK2', null, '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'CUST', 1, 'CUST', 'CUST_SEQ_ID', 'RF_CUST_SEQ_AK1', 'RF_PARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'CUST_ADDR', 1, 'CUST', 'RF_CUST_SEQ_ID', 'RF_CUST_ADDR_AK2', 'RF_PARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'CUST_PHON', 1, 'CUST', 'RF_CUST_SEQ_ID', 'RF_CUST_PHON_AK2', 'RF_PARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'CUST_EMAIL_ADDR', 1, 'CUST', 'RF_CUST_SEQ_ID', 'RF_CUST_EMAIL_AK2', 'RF_PARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'CUST_CUST', 1, 'CUST_CUST', 'CUST_CUST_SEQ_ID', 'PK_CUST_CUST', 'RF_PARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'RF_CUST_ID_DOC', 1, 'CUST', 'CUST_SEQ_ID', 'RF_CUSTIDDOC_PK', 'PARTITION_KEY', '�����������');

INSERT INTO MANTAS.RF_ARH_TAB_LIST(OWNER_NM, TABLE_NM, QUERY_NB, ENTITY_CD, ENTITY_ID_COL_NM, TABLE_UK_NM, EXCLUDE_COLUMNS_TX, ARH_TEMPL_CD)
VALUES ('BUSINESS', 'RF_CUST_CLSF_CODE', 1, 'CUST', 'CUST_SEQ_ID', 'RF_CUSTCLSFCD_PK', 'PARTITION_KEY', '�����������');

COMMIT;
