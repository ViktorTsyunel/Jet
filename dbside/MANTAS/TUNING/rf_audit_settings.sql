-- ������� ��������� ������� ������ - � ��������� ������ � �������� ������
-- ������� ����������(��������)/���������(����������) �������� - � ��������� ������ � �������� ������, �������� ID
grant select on ofsconf.cssms_usr_profile to mantas;
grant select on ofsconf.cssms_usr_group_map to mantas;

DELETE FROM mantas.rf_audit_event_type;
DELETE FROM mantas.rf_audit_event_category;
DELETE FROM mantas.rf_audit_object_type;
--
-- ��������� �������
--
INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('SYSTEM', '���������� � ��������������� �������', '������ ��� ��� ���� �������, ����������� �������������, � ������ �������� �������');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('REF', '������ �� �������������', '������/��������/���������/�������� ��������� ������������');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('OPOK', '������ � ����������� ���������', '������/��������/���������/��������/������������ �������� ��������� ��������');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('ALERT', '������ � ����������� ����������/���', '�������� ������ ������������� � ����� ��������� - ��������/�������� � ����������� ����������, ��������/��������������/�������� � ���');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('SEARCH', '����� �� ������ ��������', '����� ������������ ����������, ������, ��������');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('REP_SUMMARY', '������ ��� ������ ��������', '��������� �������, �� ���������� ��������� ������ �������� (������������ ����������, ������, ��������)');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('REP_DETAIL', '������ � ���������� ������� ��������', '��������� �������, ���������� ��������� ������ �������� (������������ ����������, �����, ��������)');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('AUDIT', '������ � �������� ������', '��������/��������/�������/��������� �������� ������ ������� ������');
       
INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('AUTHENTICATION', '�������������� (�����)', '������� �����/������ � ������� � �����������');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('AUTHORIZATION', '����������� (�����)', '������� �������� ������� ������/��������������/���������/��������� ����������');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('IMPORT', '������ (��������) ������', '������� ������� (��������) ������ � �������');

INSERT INTO mantas.rf_audit_event_category(event_cat_cd, event_cat_nm, note_tx)
       VALUES('MISC', '������ �������', '������ �������, ��������, ������������');
--
-- ���������� (SYSTEM)
--
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CNTRY_NUM_REF_READ', '������ ������ ����� ����', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUR_REF_READ', '������ ������ �����', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_DOCTYPE_REF_READ', '������ ������ ����� ���', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OBJECTSACCESS_READ', '������ ������ ��������� ������������ �������� ����������', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321TB_REF_READ', '������ ������ �������� ����� �� ��', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OWNERS_READ', '������ ������ �������������, ������������� �� ��������� ���������� �������� (�������)', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_REGION_REF_READ', '������ ������ �������� ��', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_REPORTSLIST_READ', '������ ������ �������, ��������� ������������', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ROLES_READ', '������ ������ ����� ������� AML', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_STATUSES_READ', '������ ������ �������� ���������� �������� (�������)', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TB_READ', '������ ������ ��������������� ������ �� ��', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TBOSB_READ', '������ ������ ��� �� ��', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTCOMMENT_READ', '������ ������ ����������� ������������ � ��������� ��� ��������', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTSFILTERSCOLLECTION_READ', '������ ������ ����������� �������� ��� �������', 'SYSTEM', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTSSTATISTICS_READ', '������� ����������� ���������� �� �������, ��������� ���������', 'SYSTEM', 1, null, null, null, null, null, null);                                       
--
-- ������ �� ������������� (REF) - ������ ���������� ������������� (AssignLimit)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('ASSIGNLIMIT', '����� ���������� �������������', '������ ������ - ������������� ���������� ��������, ������������ �� ������ ����������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNLIMIT_READ', '������ ������: ������ ���������� �������������', 'REF', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNLIMIT_CREATE', '��������: ������ ���������� �������������', 'REF', 0, 
              'ASSIGNLIMIT', null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNLIMIT_UPDATE', '���������: ������ ���������� �������������', 'REF', 0, 
              'ASSIGNLIMIT', 'data[1]._id_', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNLIMIT_DELETE', '��������: ������ ���������� �������������', 'REF', 0, 
              'ASSIGNLIMIT', 'data', '_id_', null, null, null);                                       
--
-- ������ �� ������������� (REF) - ��������� ������������� (AssignReplacement)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('ASSIGNREPLACEMENT', '��������� �������������', '������ ��������� - ���� ��������� �������� �������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNREPLACEMENT_READ', '������ ������: ��������� �������������', 'REF', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNREPLACEMENT_CREATE', '��������: ��������� �������������', 'REF', 0, 
              'ASSIGNREPLACEMENT', null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNREPLACEMENT_UPDATE', '���������: ��������� �������������', 'REF', 0, 
              'ASSIGNREPLACEMENT', 'data[1]._id_', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNREPLACEMENT_DELETE', '��������: ��������� �������������', 'REF', 0, 
              'ASSIGNREPLACEMENT', 'data', '_id_', null, null, null);                                       
--
-- ������ �� ������������� (REF) - ������� ���������� ������������� (AssignRule)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('ASSIGNRULE', '������� ���������� �������������', '������� ���������� - ����� ���������� �������� ����� ������������ ����� ���������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNRULE_READ', '������: ������� ���������� �������������', 'REF', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNRULE_CREATE', '��������: ������� ���������� �������������', 'REF', 0, 
              'ASSIGNRULE', null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNRULE_UPDATE', '���������: ������� ���������� �������������', 'REF', 0, 
              'ASSIGNRULE', 'data[1]._id_', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ASSIGNRULE_DELETE', '��������: ������� ���������� �������������', 'REF', 0, 
              'ASSIGNRULE', 'data', '_id_', null, null, null);    

--
-- ������ �� ������������� (REF) - ���������� � �� (OES321Org)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('OES321ORG', '���������� � ��', '���������� � ��������� ������������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321ORG_READ', '������: ���������� � ��', 'REF', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321ORG_CREATE', '��������: ���������� � ��', 'REF', 0, 'OES321ORG', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321ORG_UPDATE', '���������: ���������� � ��', 'REF', 0, 'OES321ORG', 'data[1]._id_', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321ORG_DELETE', '��������: ���������� � ��', 'REF', 0, 'OES321ORG', 'data', '_id_', null, null, null);


--
-- ������ �� ������������� (REF) - �� ��������� �������� (OES321PartyOrg) 
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('OES321PARTYORG', '�� ��������� ��������', '���������� � ��������� ������������ ��� �� ���������� ��������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321PARTYORG_READ', '������: �� ��������� ��������', 'REF', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321PARTYORG_CREATE', '��������: �� ��������� ��������', 'REF', 0, 'OES321PARTYORG', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321PARTYORG_UPDATE', '���������: �� ��������� ��������', 'REF', 0, 'OES321PARTYORG', 'data[1]._id_', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321PARTYORG_DELETE', '��������: �� ��������� ��������', 'REF', 0, 'OES321PARTYORG', 'data', '_id_', null, null, null);

--
-- ������ �� ������������� (REF) - ������� �������� ��� (OESCheckRule) 
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('OESCHECKRULE', '������� �������� ���', '���������� � �������� �������� ���');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OESCHECKRULE_READ', '������: ������� �������� ���', 'REF', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OESCHECKRULE_CREATE', '��������: ������� �������� ���', 'REF', 0, 'OESCHECKRULE', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OESCHECKRULE_UPDATE', '���������: ������� �������� ���', 'REF', 0, 'OESCHECKRULE', 'data[1]._id_', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OESCHECKRULE_DELETE', '��������: ������� �������� ���', 'REF', 0, 'OESCHECKRULE', 'data', '_id_', null, null, null);

--
-- ������ �� ������������� (REF) - ���� �������� (WatchList) 
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('WATCHLIST', '���� ��������', '���������� � ����� �������� ��� � �����������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLIST_READ', '������: ���� ��������', 'REF', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLIST_CREATE', '��������: ���� ��������', 'REF', 0, 'WATCHLIST', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLIST_UPDATE', '���������: ���� ��������', 'REF', 0, 'WATCHLIST', 'data[1]._id_', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLIST_DELETE', '��������: ���� ��������', 'REF', 0, 'WATCHLIST', 'data', '_id_', null, null, null);

--
-- ������ �� ������������� (REF) - ������� ����������� (WatchListOrg) 
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('WATCHLISTORG', '������� �����������', '���������� � ������� �����������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLISTORG_READ', '������: �������� �����������', 'REF', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLISTORG_CREATE', '��������: �������� �����������', 'REF', 0, 'WATCHLISTORG', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLISTORG_UPDATE', '���������: �������� �����������', 'REF', 0, 'WATCHLISTORG', 'data[1]._id_', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_WATCHLISTORG_DELETE', '��������: �������� �����������', 'REF', 0, 'WATCHLISTORG', 'data', '_id_', null, null, null);

--
-- ������ � ����������� ��������� (OPOK) - ���� ���� (OPOK)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('OPOK', '��� ����', '��� ���� - ��� �������� ���������� �������� ��������');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOK_READ', '������: ���� ����', 'OPOK', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOK_CREATE', '��������: ���� ����', 'OPOK', 0, 
              'OPOK', null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOK_UPDATE', '���������: ���� ����', 'OPOK', 0, 
              'OPOK', 'data[1]._id_', null, null, null, 'data##SELECT * FROM mantas.rf_opok WHERE opok_nb in (#OBJECT_IDS#)');                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOK_DELETE', '��������: ���� ����', 'OPOK', 0, 
              'OPOK', 'data', '_id_', null, null, 'data##SELECT * FROM mantas.rf_opok WHERE opok_nb in (#OBJECT_IDS#)##dataDetails##SELECT * FROM mantas.rf_opok_details WHERE opok_nb in (#OBJECT_IDS#)');    

--
-- ������ � ����������� ��������� (OPOK) - ������ ���� ���� (OPOKDetails)
--
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKDETAILS_READ', '������: ������ ���� ����', 'OPOK', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKDETAILS_CREATE', '��������: ������ ���� ����', 'OPOK', 0, 'OPOK', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKDETAILS_UPDATE', '���������: ������ ���� ����', 'OPOK', 0, 'OPOK', 'data[1]._id_', null, null, null, 
              'data##SELECT * FROM mantas.rf_opok_details WHERE record_seq_id in (#OBJECT_IDS#)');
--� ���������� ���������� ����� ������� ������ ���� ������ ������� ���� ���� .
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKDETAILS_DELETE', '��������: ������ ���� ����', 'OPOK', 0, 'OPOK', 'data[1]._id_', null, null, null, 
              'data##SELECT * FROM mantas.rf_opok_details WHERE record_seq_id in (#OBJECT_IDS#)'); 

--
-- ������ � ����������� ��������� (OPOK) - ����������� ������� ��������� ���� (OPOKCriteria)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('OPOKCRITERIA', '����������� ������� ��������� ����', '����������� ������� ��������� ����');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKCRITERIA_READ', '������: ����������� ������� ��������� ����', 'OPOK', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKCRITERIA_CREATE', '��������: ����������� ������� ��������� ����', 'OPOK', 0, 'OPOKCRITERIA', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKCRITERIA_UPDATE', '���������: ����������� ������� ��������� ����', 'OPOK', 0, 'OPOKCRITERIA', 'data[1]._id_', null, null, null, 
              'data##SELECT * FROM mantas.rf_opok_criteria WHERE crit_cd in (#OBJECT_IDS#)');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKCRITERIA_DELETE', '��������: ����������� ������� ��������� ����', 'OPOK', 0, 'OPOKCRITERIA', 'data', '_id_', null, null, 
              'data##SELECT * FROM mantas.rf_opok_criteria WHERE crit_cd in (#OBJECT_IDS#)');

--
-- ������ � ����������� ��������� (OPOK) - ������� ��������� ���� (OPOKRule)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('OPOKRULE', '������� ��������� ����', '������� ��������� ����');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKRULE_READ', '������: ������� ��������� ����', 'OPOK', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKRULE_CREATE', '��������: ������� ��������� ����', 'OPOK', 0, 'OPOKRULE', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKRULE_UPDATE', '���������: ������� ��������� ����', 'OPOK', 0, 'OPOKRULE', 'data[1]._id_', null, null, null, 
              'data##SELECT * FROM mantas.rf_opok_rule WHERE rule_seq_id in (#OBJECT_IDS#)');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKRULE_DELETE', '��������: ������� ��������� ����', 'OPOK', 0, 'OPOKRULE', 'data', '_id_', null, null, 
              'data##SELECT * FROM mantas.rf_opok_rule WHERE rule_seq_id in (#OBJECT_IDS#)');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKRULE_CREATE_VERSION', '����� ������: ������� ��������� ����', 'OPOK', 0, 'OPOKRULE', '_id_', null, null, null, null);

--
-- ������ � ����������� ��������� (OPOK) - ������������ ������ ��������� (OPOKTest)
--
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKTEST_READ', '"������������ ������(�) ��������� ����', 'OPOK', 1, 'OPOKRULE' , 'filter[4].value', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OPOKTEST_ESTIMATE', '������ ������� ���������� ��������� ��������� �� ������� ����', 'SYSTEM', 1, null, null, null, null, null, null);

--
-- ������ � ����������� ����������/��� (ALERT)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('ALERT', '���������� �������� (�����)', '����� - �������� ������, � ������� �������� ������� ������������ �������');

INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('ALERTATTACHMENT', '�������� � ������', '�������� - ����, ������������� � ���������� �������� (������)');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTLIST_READ', '������: ������ ���������� ��������', 'ALERT', 1, 
              'ALERT', 'alertIds', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTLIST_READNEXT', '������: ������ ���������� �������� (��������� ��������)', 'ALERT', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTLIST_READLAST', '������: ������ ���������� �������� (���������� ��������)', 'ALERT', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTLIST_COUNTALERT', '������� ���������� ���������� �������� �� ��������', 'ALERT', 1, null, null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTRULESNAPSHOT_READ', '������: ������� ���������, "�����������" ��� ��������', 'ALERT', 1, 
              'ALERT', 'alertId', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTHISTORY_READBYALERTID', '������: ������� �������� ��� ���������� ���������', 'ALERT', 1, 
              'ALERT', 'alertId', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTACTION_READBYFILTER', '������ ������ ��������� �������� ��� �������, ��������������� ��������', 'ALERT', 1, 
              'ALERT', null, null, null, null, null); 
-----
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTACTION_SETKGRKO', '��������� �������� ����� �� ������ ���������� ��������', 'ALERT', 0, 
              'ALERT', 'trxn_seq_id', null, null, null, null); 
              
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTACTION_DOUBLEOES', '�������� �������� � 2-� �������� ����� (2 ���)', 'ALERT', 0, 
              'ALERT', 'first_review_id', null, null, null, null); 

-------
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTACTION_READBYID', '������ ������ ��������� �������� ��� ���������� �������', 'ALERT', 1, 
              'ALERT', 'alertIds', null, null, null, null);               

begin
  INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                         object_type_cd, object_id_path, object_id_path2, 
                                         event_descr_tx, event_descr_sql, old_values_sql)
         VALUES('GUI_ALERTACTION_EXECBYFILTER', '���������� �������� ��� ��������, ���������������� ��������', 'ALERT', 0, 
                'ALERT', null, null, 
                null, 
  q'{declare
    var_json           RF_JSON;
    var_json_list      rf_json_list;
    var_actvy_type_cd  VARCHAR2(255 CHAR);
    var_result         VARCHAR2(2000 CHAR);
  begin
    var_json := :par_json_in;
    
    var_json_list := rf_json_ext.get_json_list(var_json, 'actvyTypeCds');
      
    if var_json_list is not null Then
      if var_json_list.count > 0 Then 
        FOR i IN 1..var_json_list.count LOOP
          var_actvy_type_cd := var_json_list.get(i).get_string; 
          var_result := var_result||', '||mantas.rf_pkg_audit.get_actvy_name(var_actvy_type_cd)||' ('||var_actvy_type_cd||')';         
        END LOOP;
        var_result := substr(var_result, 3);
  
        if var_json_list.count = 1 Then 
          var_result := '��������: '||var_result; 
        else
          var_result := '��������: '||var_result; 
        end if;  
      end if;  
    end if;
    
    :par_res_out := var_result;
  end;}', null);
end;
/

begin
  INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                         object_type_cd, object_id_path, object_id_path2, 
                                         event_descr_tx, event_descr_sql, old_values_sql)
         VALUES('GUI_ALERTACTION_EXECBYID', '���������� �������� ��� ��������', 'ALERT', 0, 
                'ALERT', 'alertIds', null, 
                null, 
  q'{declare
    var_json           RF_JSON;
    var_json_list      rf_json_list;
    var_actvy_type_cd  VARCHAR2(255 CHAR);
    var_result         VARCHAR2(2000 CHAR);
  begin
    var_json := :par_json_in;
    
    var_json_list := rf_json_ext.get_json_list(var_json, 'actvyTypeCds');
      
    if var_json_list is not null Then
      if var_json_list.count > 0 Then 
        FOR i IN 1..var_json_list.count LOOP
          var_actvy_type_cd := var_json_list.get(i).get_string; 
          var_result := var_result||', '||mantas.rf_pkg_audit.get_actvy_name(var_actvy_type_cd)||' ('||var_actvy_type_cd||')';         
        END LOOP;
        var_result := substr(var_result, 3);
  
        if var_json_list.count = 1 Then 
          var_result := '��������: '||var_result; 
        else
          var_result := '��������: '||var_result; 
        end if;  
      end if;  
    end if;
    
    if var_result is not null Then
      var_json_list := rf_json_ext.get_json_list(var_json, 'alertIds');
  
      if var_json_list is not null Then
        if var_json_list.count > 1 Then 
          var_result := var_result||'; ���������� �������: '||to_char(var_json_list.count);
        end if;
      end if;  
    end if;  
    
    :par_res_out := var_result;
  end;}', null);
end;
/


INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTNARRATIVE_READBYALERTID', '������: ������� � ���������� ��������', 'ALERT', 1, 
              'ALERT', 'alertId', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTNARRATIVE_UPDATEBYALERTID', '���������: ������� � ���������� ��������', 'ALERT', 0, 
              'ALERT', 'data', '_id_', null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTATTACHMENTS_READBYALERTID', '������: ������ �������� ��� ������', 'ALERT', 1, 
              'ALERT', 'alertId', null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTATTACHMENTS_UPDATE', '���������: ������������ �������� ��� ������', 'ALERT', 0, 
              'ALERTATTACHMENT', 'data', '_id_', null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTATTACHMENTS_DELETE', '��������: �������� ��� ������', 'ALERT', 0, 
              'ALERTATTACHMENT', 'data', '_id_', null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTATTACHMENTS_ADD', 'C�������: �������� ��� ������', 'ALERT', 0, 
              'ALERTATTACHMENT', null, null, null, null, null);                                       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_ALERTATTACHMENTS_GET', '������: �������� ��� ������', 'ALERT', 1, 
              'ALERTATTACHMENT', '_id_', null, null, null, null);                                       

--TrxnDetails
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNDETAILS_READBYALERTID', '������: ������ ��������', 'ALERT', 1, 'ALERT', 'alertId', null, null, null, null);

--TrxnParty
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNPARTY_READBYALERTID', '������: �������� ��������', 'ALERT', 1, 'ALERT', 'alertId', null, null, null, null);

       
       
--CustAddrList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTADDRLIST_READBYALERTID', '������: ����� ��������� ��������', 'ALERT', 1, 'ALERT', 'alertId', null, null, null, null);

--CustCustList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTCUSTLIST_READBYALERTID', '������: ��������� ���� ��������� ��������', 'ALERT', 1, 'ALERT', 'alertId', null, null, null, null);

--CustEmailList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTEMAILLIST_READBYALERTID', '������: Email ��������� ��������', 'ALERT', 1, 'ALERT', 'alertId', null, null, null, null);

--CustIdDocList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTIDDOCLIST_READBYALERTID', '������: ��������� ��������� ��������', 'ALERT', 1, 'ALERT', 'alertId', null, null, null, null);

--CustPhonList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTPHONLIST_READBYALERTID', '������: ���������� ������ ��������� ��������', 'ALERT', 1, 'ALERT', 'alertId', null, null, null, null);

--
-- ������ � ���������� (TRXN), ���������(CUST), ����������� ��� (OES)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('OES', '��������� ���', '���������� � ���������� ���');

INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('TRXN', '��������', '���������� �� ��������� � ����������');

INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('CUST', '�������', '���������� � �������� �� �����������');

--OES321Details
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321DETAILS_READBYOESID', '������: ��������� ���', 'ALERT', 1, 'OES', 'oes321Id', null, null, null, null);
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321DETAILS_READCHECKSTATUS', '������: ������ ��������� ���', 'ALERT', 1, 'OES', 'oes321Id', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321DETAILS_UPDATE', '���������: ��������� ���', 'ALERT', 0, 'OES', '', null, null, null, null);

--OES321Party
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321PARTY_READ', '������: �������� ��������', 'ALERT', 1, 'OES', 'oes321Id', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321PARTY_UPDATE', '���������: �������� ��������', 'ALERT', 0, 'OES', 'data[1]._id_', null, null, null, null);

--OES321Changes
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321CHANGES_READBYOESID', '������: ������� ��������� ��������� ���', 'ALERT', 1, 'OES', 'oes_321_id', null, null, null, null);

--OES321ChangeDetails
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321CHANGEDETAILS_READ', '������: ������ ������� ��������� ��������� ���', 'ALERT', 1, 'OES', 'oes_321_id', null, null, null, null);

--OES321Create
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OES321CREATE_CREATE', '��������: ��������� ���', 'ALERT', 0, 'OES', null, null, null, null, null);

--OESCheck
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_OESCHECK_READ', '������: ��������� �������� ��������� ���', 'ALERT', 1, 'OES', 'oesId', null, null, null, null);

--TrxnAction
begin
  INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                         object_type_cd, object_id_path, object_id_path2, 
                                         event_descr_tx, event_descr_sql, old_values_sql)
         VALUES('GUI_TRXNACTION_EXECBYID', '���������� �������� ��� ���������', 'ALERT', 0, 'TRXN', 'trxnIds', null, null, 
  q'{
  declare
    v_json rf_json;
    v_cnt number;
  begin
    v_json := :par_json_in;
    v_cnt := rf_json_ext.get_json_list(v_json, 'trxnIds').count;
    :par_res_out := '�������� �������� �� ���� '||rf_json_ext.get_number(v_json, 'opokNb')||' ('||v_cnt||
                    case 
                      when v_cnt = 1 then ' ��������'
                      when v_cnt in (2,3,4) then ' ��������'
                      else ' ��������'
                    end||')';
  end;}', null);
end;
/
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNACTION_EXECBYFILTER', '���������� �������� ��� ����������, ���������������� ��������', 'ALERT', 0, 'TRXN', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNACTION_READ', '������: �������� ��� ����������, ���������������� ��������', 'ALERT', 1, 'TRXN', 'trxnIds', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNACTION_SETKGRKO', '������������ �������� ����� �� ������ ��������', 'ALERT', 0, 'TRXN', 'trxn_seq_id', null, null, null, null);
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNEXTRA_READ', '������: ��������� �������������� ���������� �� ��������', 'ALERT', 1, 'TRXN', 'trxn_seq_id', null, null, null, null);       
       
--
-- ����� �� ������ �������� (SEARCH)
--
--TrxnDetails
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNDETAILS_READBYTRXNID', '������: ������ ��������', 'SEARCH', 1, 'TRXN', 'trxnId', null, null, null, null);

--TrxnParty
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNPARTY_READBYTRXNID', '������: �������� ��������', 'SEARCH', 1, 'TRXN', 'trxnId', null, null, null, null);

--CustAddrList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTADDRLIST_READBYCUSTID', '������: ����� ��������� ��������', 'SEARCH', 1, 'CUST', 'custSeqId', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTADDRLIST_READBYTRXNID', '������: ����� ��������� ��������', 'SEARCH', 1, 'TRXN', 'trxnId', null, null, null, null);

--CustCustList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTCUSTLIST_READBYCUSTID', '������: ��������� ���� ��������� ��������', 'SEARCH', 1, 'CUST', 'custSeqId', null, null, null, null);


INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTCUSTLIST_READBYTRXNID', '������: ��������� ���� ��������� ��������', 'SEARCH', 1, 'TRXN', 'trxnId', null, null, null, null);

--CustEmailList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTEMAILLIST_READBYCUSTID', '������: Email ��������� ��������', 'SEARCH', 1, 'CUST', 'custSeqId', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTEMAILLIST_READBYTRXNID', '������: Email ��������� ��������', 'SEARCH', 1, 'TRXN', 'trxnId', null, null, null, null);

--CustIdDocList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTIDDOCLIST_READBYCUSTID', '������: ��������� ��������� ��������', 'SEARCH', 1, 'CUST', 'custSeqId', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTIDDOCLIST_READBYTRXNID', '������: ��������� ��������� ��������', 'SEARCH', 1, 'TRXN', 'trxnId', null, null, null, null);

--CustPhonList
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTPHONLIST_READBYCUSTID', '������: ���������� ������ ��������� ��������', 'SEARCH', 1, 'CUST', 'custSeqId', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTPHONLIST_READBYTRXNID', '������: ���������� ������ ��������� ��������', 'SEARCH', 1, 'TRXN', 'trxnId', null, null, null, null);

--CustDetails
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_CUSTDETAILS_READBYCUSTID', '������: �������� ��������', 'SEARCH', 1, 'CUST', 'cust_seq_id', null, null, null, null);

--OESCustSearch
begin
  INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                         object_type_cd, object_id_path, object_id_path2, 
                                         event_descr_tx, event_descr_sql, old_values_sql)
         VALUES('GUI_OESCUSTSEARCH_SEARCH', '����� �������', 'SEARCH', 1, null , null, null, null,
  q'{
  declare
    v_json      rf_json;
    v_json_list rf_json_list;
    v_node      varchar2(64);
    --����������� ������������������ ���������
    v_str       varchar2(512) := '����� #TYPE# ��:#INN##OGRN##OKPO##ACCT##FIO##GR##DOC##CUSTID#';
  begin
    v_json := :par_json_in;
    v_str := replace(v_str, '#TYPE#',
                     case
                       when v_json.get('tu').get_number = 1 then '��'
                       when v_json.get('tu').get_number = 2 then '��'
                       when v_json.get('tu').get_number = 3 then '��'
                       else null
                     end);
    v_json_list := v_json.get_keys;
    for i in 1..v_json_list.count loop
      v_node := v_json_list.get(i).value_of;
      
      case
        when v_node = 'nd' then v_str := replace(v_str, '#INN#', ' ���,');
        when v_node = 'rg' then v_str := replace(v_str, '#OGRN#', ' ����,');
        when v_node = 'okpo' then v_str := replace(v_str, '#OKPO#', ' ����,');
        when v_node in ('kd', 'series', 'vd1') then v_str := replace(v_str, '#DOC#', ' ��������,');
        when v_node in ('first_nm', 'last_nm', 'midl_nm') then v_str := replace(v_str, '#FIO#', ' ���,');
        when v_node = 'gr' then v_str := replace(v_str, '#GR#', ' ���� ��������,');
        when v_node = 'acc_b' then v_str := replace(v_str, '#ACCT#', ' ����� �����,');
        when v_node = 'cust_seq_id' then v_str := replace(v_str, '#CUSTID#', ' ID �������,');
        else null;
      end case;             
    end loop;
    --�������: ������������ ����� �� ������, ��������� �������
    :par_res_out := rtrim(regexp_replace(v_str, '#TYPE#|#INN#|#OGRN#|#OKPO#|#ACCT#|#FIO#|#GR#|#DOC#|#CUSTID#'), ',');
  end;}', null);
end;
/

--TrxnList
begin
  INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                         object_type_cd, object_id_path, object_id_path2, 
                                         event_descr_tx, event_descr_sql, old_values_sql)
         VALUES('GUI_TRXNLIST_READ', '����� ��������', 'SEARCH', 1, 'TRXN', null, null, null, 
  q'{
  declare
    v_json      rf_json;
    v_json_list rf_json_list;
    v_node      varchar2(64);
    --����������� ������������������ ���������
    v_str       varchar2(512) := '#ACTION# ��:#DATA##SUM##LEX##DEP##OPER##CUST1##ACCT1##CUST2##ACCT2#';
    v_type      varchar2(8);
    v_action    varchar2(16);
  begin
    v_json := :par_json_in;
    v_action := v_json.get('action').get_string;
    case
      when v_action = 'count' then v_str := replace(v_str, '#ACTION#', '�������');
      else v_str := replace(v_str, '#ACTION#', '�����');
    end case;
    v_type := rf_json_ext.get_json(v_json, 'filters').get('partyType').get_string;
    v_json_list := rf_json(v_json.get('filters')).get_keys;

    for i in 1..v_json_list.count loop
      v_node := v_json_list.get(i).value_of;
      case
        when v_node in ('dtFrom', 'dtTo') then v_str := replace(v_str, '#DATA#', ' ���� ��������,');
        when v_node in ('amountFrom', 'amountTo') then v_str := replace(v_str, '#SUM#', ' ����� ��������,'); 
        when v_node in ('lexPurposeCrit', 'lexPurposeAdd') then v_str := replace(v_str, '#LEX#', ' ���������� �������,');
        when v_node in ('tbIds', 'osbIds') then v_str := replace(v_str, '#DEP#', ' �������������,');
        when v_node in ('notOpok', 'opCat', 'srcSys') then v_str := replace(v_str, '#OPER#', ' �������� �� ��������,');
        when v_node in ('custNm', 'custInn', 'custResident', 'custWatchList') then 
             if v_type in ('O', 'O&'||'B') then
               v_str := replace(v_str, '#CUST1#', ' ����������,');
             elsif v_type in ('B') then 
               v_str := replace(v_str, '#CUST2#', ' ����������,');
             elsif v_type in ('O|B') then 
               v_str := replace(v_str, '#CUST1#', ' ����������,');
               v_str := replace(v_str, '#CUST2#', ' ����������,');
             end if;  
        when v_node in ('acctCrit', 'acctAdd', 'acctNb', 'acctArea') then 
             if v_type in ('O', 'O&'||'B') then
               v_str := replace(v_str, '#ACCT1#', ' ���� �����������,');
             elsif v_type in ('B') then 
               v_str := replace(v_str, '#ACCT2#', ' ���� ����������,');
             elsif v_type in ('O|B') then 
               v_str := replace(v_str, '#ACCT1#', ' ���� �����������,');
               v_str := replace(v_str, '#ACCT2#', ' ���� ����������,');
             end if;
        when v_node in ('cust2Nm', 'cust2Inn', 'cust2Resident', 'cust2WatchList') and v_type in ('O&'||'B') then v_str := replace(v_str, '#CUST2#', ' ����������,');
        when v_node in ('acct2Crit', 'acct2Add', 'acct2Nb', 'acct2Area') and v_type in ('O&'||'B') then v_str := replace(v_str, '#ACCT2#', ' ���� ����������,');
        else null;
      end case;
    end loop;
    
    --�������: ������������ ����� �� ������, ��������� �������
    :par_res_out := rtrim(regexp_replace(v_str, '#DATA#|#SUM#|#LEX#|#DEP#|#OPER#|#CUST1#|#ACCT1#|#CUST2#|#ACCT2#'), ',');
  end;}', null);
end;
/

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                         object_type_cd, object_id_path, object_id_path2, 
                                         event_descr_tx, event_descr_sql, old_values_sql)
         VALUES('GUI_TRXNLIST_COUNT', '������� ���������� �������� ', 'SEARCH', 1, 'TRXN', null, null, null, 
                --������ ��� event_descr_sql ����� �� ������� GUI_TRXNLIST_READ
                (select event_descr_sql from mantas.rf_audit_event_type where event_type_cd = 'GUI_TRXNLIST_READ'), 
                null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNLIST_READNEXT', '�������� ��������� �������� ��������', 'SEARCH', 1, 'TRXN', null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_TRXNLIST_READLAST', '�������� ���������� ������� ��������', 'SEARCH', 1, 'TRXN', null, null, null, null, null);

--
-- ������ ��� ������ �������� (REP_SUMMARY)
--
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1001_1', '�����: �������� ��������� ��������', 'REP_SUMMARY', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1002_1', '�����: �������� �� ����������� �� (���������)', 'REP_SUMMARY', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1002_2', '�����: �������� �� ����������� ��', 'REP_SUMMARY', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1003_1', '�����: ��������� ����� �������� ��� ���', 'REP_SUMMARY', 1, null, null, null, null, null, null);
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1003_2', '�����: ��������� ����� �������� ��� ���(������� ���������)', 'REP_SUMMARY', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1004_1', '�����: ��������� ����� �������� �� ��� ���', 'REP_SUMMARY', 1, null, null, null, null, null, null);
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1004_2', '�����: ��������� ����� �������� �� ��� ��� (������� ���������)', 'REP_SUMMARY', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1005_1', '�����: ���������� ��������� ��������', 'REP_SUMMARY', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1007_1', '�����: �������� ������ ������ � ��������� ���� (��������� ������)', 'REP_SUMMARY', 1, null, null, null, null, null, null);       

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1007_2', '�����: �������� ������ ������ � ��������� ����', 'REP_SUMMARY', 1, null, null, null, null, null, null);     
   
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1007_3', '�����: �������� ������ ������ � ��������� ���� (��������)', 'REP_SUMMARY', 1, null, null, null, null, null, null); 

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1007_4', '�����: �������� ������ ������ � ��������� ���� (������ �����)', 'REP_SUMMARY', 1, null, null, null, null, null, null);   

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1007_5', '�����: �������� ������ ������ � ��������� ���� (������ ����)', 'REP_SUMMARY', 1, null, null, null, null, null, null);  
            

--
-- ������ � �������� ������ (AUDIT)
--
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1006_1', '�����: ������� ������ (��������� ������)', 'AUDIT', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1006_2', '�����: ������� ������', 'AUDIT', 1, null, null, null, null, null, null);
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1006_3', '�����: ������� ������(��������� �������)', 'AUDIT', 1, null, null, null, null, null, null);       
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1006_4', '�����: ������� ������(������� �������� ����������)', 'AUDIT', 1, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_1006_5', '�����: ������� ������(��������� ������)', 'AUDIT', 1, null, null, null, null, null, null);     

--
-- ����������� (�����) (AUTHORIZATION)
--
INSERT INTO mantas.rf_audit_object_type(object_type_cd, object_type_nm, note_tx)
       VALUES('USER', '������������ AML', '���������� � ��������� ��� ������������� AML');

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('SUDIR_CREATE_USER', '�������� ������ ������������.', 'AUTHORIZATION', 0, null, null, null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('SUDIR_UPDATE_USER', '��������� ������������.', 'AUTHORIZATION', 0, 'USER', 'usr_id', null, null, null, 
              q'{data##select u.v_usr_id as usr_id, u.v_usr_name as usr_name, 
                        listagg (r.v_group_code, '|') within group (order by v_group_code) as roles,
                        u.v_email as email, u.v_mobile_nbr as v_mobile_nbr, u.v_usr_address as usr_address 
                   from ofsconf.cssms_usr_profile u
                   left join ofsconf.cssms_usr_group_map r on u.v_usr_id = r.v_usr_id
                  where u.v_usr_id in (#OBJECT_IDS#)
                  group by u.v_usr_id, u.v_usr_name, u.v_email, u.v_mobile_nbr, u.v_usr_address}');
       
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('SUDIR_LOCK_USER', '���������� ������������.', 'AUTHORIZATION', 0, 'USER', 'usr_id', null, null, null, null);

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('SUDIR_UNLOCK_USER', '������������� ������������.', 'AUTHORIZATION', 0, 'USER', 'usr_id', null, null, null, null);

--
-- �����  (IMPORT)
--
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMP_SBRF_321XML', '������ ��������� ��� �� XML (����������� ����� ��)', 'IMPORT', 0, 
              null, null, null, null, null, null);     

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMP_SBRF_321XML_8001', '������ ��������� ��� �� XML (8001 ���)', 'IMPORT', 0, 
              null, null, null, null, null, null);     
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMP_CBRF_BNKSEEK', '������ ����������� ����������� ��� �� ��', 'IMPORT', 0, 
              null, null, null, null, null, null);     
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMP_CBRF_BNKDEL', '������ ����������� ��������� (�������������) ��� �� ��', 'IMPORT', 0, 
              null, null, null, null, null, null);     

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMPTYPE_READ', '������:���� �������', 'IMPORT', 1, 
              null, null, null, null, null, null);  
              
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMPSESSION_READLATEST', '������:����������� ����� ��������� ������', 'IMPORT', 1, 
              null, null, null, null, null, null);                

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMPFILES_READ', '������:����������� ����� � ������ ��������', 'IMPORT', 1, 
              null, null, null, null, null, null);   
--
-- ������ ������� (MISC)
--
INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_UNKNOWN', '����������� ������� ���������� ������������', 'MISC', 0, 
              null, null, null, null, null, null);     

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('APEX_UNKNOWN', '����������� ������� ��� ���������� ������ APEX', 'MISC', 0, 
              null, null, null, null, null, null);     

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('SYS_AUDIT_EVENT_ERROR', '������ ��� ������������ ������ ������', 'MISC', 1, 
              null, null, null, null, null, null);     

INSERT INTO mantas.rf_audit_event_type(event_type_cd, event_type_nm, event_cat_cd, just_read_fl, 
                                       object_type_cd, object_id_path, object_id_path2, 
                                       event_descr_tx, event_descr_sql, old_values_sql)
       VALUES('GUI_IMP_UNKNOWN', '����������� ������� ��� ������� ������', 'MISC', 0, 
              null, null, null, null, null, null);     


COMMIT;

begin
  mantas.rf_pkg_audit.init;
end;
/
