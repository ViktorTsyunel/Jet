CREATE OR REPLACE PROCEDURE MANTAS.p_sequence_nextval
(
    in_sequence_name IN  kdd_counter.sequence_name%TYPE,
    out_next_value   OUT kdd_counter.current_value%TYPE,
    out_error_code   OUT NUMBER,
    out_error_msg    OUT VARCHAR2
)
IS
   --Cursor declarations
   CURSOR seq_cur IS
      SELECT *
      FROM   kdd_counter 
      WHERE  sequence_name = in_sequence_name
      FOR UPDATE;
   --Declare variables
   seq_rec seq_cur%ROWTYPE;
   l_break_flag EXCEPTION;
   --Log error variables
   l_error_code NUMBER := 0; --initialize to 0
   l_err_msg    VARCHAR2(255);
   l_error      VARCHAR2(255) := 'Error in p_sequence_nextval procedure : ';
   --statement for setting autonomous transaction
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
   --
   -- �������� kdd_counter, ������������ �� �������� ��, ������������ ��������
   --  
   if in_sequence_name in ('REVIEW_ID_SEQ', 'ACTIVITY_ID_SEQ', 'NOTE_ID_SEQ') Then 

     if in_sequence_name = 'REVIEW_ID_SEQ' Then     
       out_next_value := KDD_REVIEW_SEQ.NextVal;
     elsif in_sequence_name = 'ACTIVITY_ID_SEQ' Then     
       out_next_value := KDD_ACTIVITY_SEQ.NextVal;
     elsif in_sequence_name = 'NOTE_ID_SEQ' Then     
       out_next_value := KDD_NOTE_HIST_SEQ.NextVal;
     end if;
     
     out_error_code := l_error_code;
     out_error_msg  := l_err_msg;
     return;
   end if;

   -- Open the sequence cursor
   OPEN seq_cur;
   FETCH seq_cur INTO  seq_rec;
   IF seq_cur%NOTFOUND THEN
      --log the error message for no data found and raise flag
      l_error_code := -20102;
      l_err_msg := l_error || 'NO DATA FOUND'||' FOR SEQUENCE_NAME = '||in_sequence_name;
      CLOSE seq_cur;
      RAISE l_break_flag;
   END IF;
   CLOSE seq_cur;
   --check here if max value has been reached and the cycle flag is 'N' or 'Y'
   IF seq_rec.current_value >= seq_rec.max_value THEN
      IF seq_rec.CYCLE_FLAG = 'N' THEN
         --log error message for max value reached and raise flag
         l_error_code := -20101;
         l_err_msg := l_error ||'MAX VALUE FOR SEQUENCE REACHED, NO CYCLE SET';
         RAISE l_break_flag;
      ELSE
         --reset the current value to min value
         seq_rec.current_value := seq_rec.min_value;
      END IF;
   ELSE
      --set the current_value to current_value + 1
      seq_rec.current_value := seq_rec.current_value + 1;
   END IF;
   --update the counter table with the current value
   UPDATE kdd_counter
   SET    current_value = seq_rec.current_value
   WHERE  sequence_name = in_sequence_name;
   COMMIT;
   --set the out values
   out_next_value := seq_rec.current_value;
   out_error_code := l_error_code;
   out_error_msg  := l_err_msg;
EXCEPTION
   WHEN l_break_flag THEN
      ROLLBACK;
      out_error_code := l_error_code;
      out_error_msg := l_err_msg;
   WHEN OTHERS THEN
      ROLLBACK;
      l_err_msg := SUBSTR(l_err_msg || SQLERRM,1,200);
      out_error_code := SQLCODE;
      out_error_msg := l_err_msg;
END p_sequence_nextval;
/
