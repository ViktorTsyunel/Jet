create or replace package mantas.rf_pkg_request_imports is

  /* ������� ��������� ��������� ������
  *   
  * ��������� ������
  *   p_request - �������� ������ � ������� json
  *   p_user - ��� �������� ������������
  *   p_id_request - ���������������� ����� �������
  * ��������� ������
  *   rf_json - ����� � ������� json
  */
  function process_request(p_request    in clob,
                           p_user       in varchar2,
                           p_id_request in number) return rf_json;

  /* ����� ��� ��������� sql ������� � ����������. � ������� ������ ���� ������������� sys.anydata.convertXXX()
  *
  * ��������� ������
  *   p_sql - ������
  * ��������� ������
  *   sys.anydata
  */
  function execute_agg_sql(p_sql in varchar2) return sys.anydata;

  /* ����� ������ ������� ��������� ��� ������� ������ � � ����������� �� ���� ���������� ��������
  *
  * ��������� ������
  *   p_type - ��� �������
  *   p_date_act - ���� ������������ ������
  *   p_file_name - ��� �����
  *   p_note - �����������
  *   p_user - ��� ������������
  *   p_file - ���� � blob
  * ��������� ������
  *   clob - ����� � ������� json
  */
  function import_dbf(p_type      in mantas.rf_imp_history.imp_type_cd%type,
                      p_date_act  in mantas.rf_imp_history.actual_dt%type,
                      p_file_name in mantas.rf_imp_history.file_nm%type,
                      p_note      in mantas.rf_imp_history.note_tx%type,
                      p_user      in mantas.rf_imp_history.loaded_by%type,
                      p_file      in blob,
                      p_ip_address      IN mantas.rf_audit_event.ip_address%TYPE DEFAULT '0.0.0.0') return varchar2;

end;
/
create or replace package body mantas.rf_pkg_request_imports is

  /* ����� ��� ��������� ������� Imports read
   *
   * ��������� ������
   *   p_json - json ������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - json �����
   */
  function get_import_types( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000 char);
    v_bind_json rf_json:= rf_json();   
  begin
    v_sql := q'{
      select t.imp_type_cd, 
             t.imp_type_nm, 
             t.active_fl, 
             h.file_nm, 
             h.actual_dt, 
             case 
               when t.date_sql_tx is not null then sys.anydata.accessDate(mantas.rf_pkg_request_imports.execute_agg_sql(t.date_sql_tx)) 
               else null 
             end control_dt,
             h.load_dt, 
             h.load_status,
             decode(h.load_status,
                    'O', '�������',
                    'W', '��������������',
                    'E', '������',
                    null
                   ) load_status_tx, 
             h.err_msg_tx, 
             h.load_qty, 
             case
               when t.qty_sql_tx is not null then sys.anydata.accessNumber(mantas.rf_pkg_request_imports.execute_agg_sql(t.qty_sql_tx)) 
               else null 
             end control_qty,
             h.note_tx, 
             h.loaded_by, 
             t.params_tx,
             t.multiple_files_fl,
             case when mantas.rf_pkg_adm.get_imptp_access(:P_USER, t.imp_type_cd) = 2 -- ���� ������ � ���� ������� �� ������
                  then 1
                  else 0
             end as allow_editing
        from mantas.rf_imp_type t
        left join mantas.rf_imp_history h on h.imp_type_cd = t.imp_type_cd and h.last_load_flag = 1 and t.multiple_files_fl = 'N'
       where mantas.rf_pkg_adm.get_imptp_access(:P_USER, t.imp_type_cd) > 0 -- ���� ������ � ���� �������             
       order by decode(t.imp_type_cd, 'CBRF_BNKDEL', '0', 'CBRF_BNKSEEK', '1', t.imp_type_cd)
    }';

    v_bind_json.put( 'P_USER', p_user);   

    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );
  end;


  /* ����� ��� ��������� sql ������� � ����������. � ������� ������ ���� ������������� sys.anydata.convertXXX()
   *
   * ��������� ������
   *   p_sql - ������
   * ��������� ������
   *   sys.anydata
   */
  function execute_agg_sql( p_sql in varchar2 ) return sys.anydata
  is
    v_result sys.anydata;
  begin
    execute immediate p_sql into v_result;
    return v_result;
  end;


  function create_response (
    p_success boolean,
    p_message varchar2
  ) return varchar2
  is
    v_response rf_json;
  begin
    v_response := rf_json();
    v_response.put('success', p_success);
    v_response.put('message', p_message);
    return v_response.to_char();
  end;

  /* ����� ������ ������� ��������� ��� ������� ������ � � ����������� �� ���� ���������� ��������
   *
   * ��������� ������
   *   p_type - ��� �������
   *   p_date_act - ���� ������������ ������
   *   p_file_name - ��� �����
   *   p_note - �����������
   *   p_user - ��� ������������
   *   p_file - ���� � blob
   * ��������� ������
   *   clob - ����� � ������� json
   */
  function import_dbf ( 
    p_type      in mantas.rf_imp_history.imp_type_cd%type,
    p_date_act  in mantas.rf_imp_history.actual_dt%type,
    p_file_name in mantas.rf_imp_history.file_nm%type,
    p_note      in mantas.rf_imp_history.note_tx%type,
    p_user      in mantas.rf_imp_history.loaded_by%type,
    p_file      in blob,
    p_ip_address      IN mantas.rf_audit_event.ip_address%TYPE DEFAULT '0.0.0.0'
  ) return varchar2
  is
    v_json         rf_json;
    v_status       mantas.rf_imp_history.load_status%type;
    v_err_msg      mantas.rf_imp_history.err_msg_tx%type;
    v_load_qty     mantas.rf_imp_history.load_qty%type;
    v_owner_seq_id mantas.kdd_review_owner.owner_seq_id%type;
    v_audit_id rf_audit_event.audit_seq_id%TYPE;
    v_aud_json rf_json;

    v_return       varchar2(4000);
  begin
    v_aud_json:=rf_json();
    v_aud_json.put('actual_dt',TO_CHAR(p_date_act,'YYYY-MM-DD HH24:MI:SS'));
    v_aud_json.put('note_tx',p_note);
    v_aud_json.put('file',p_file_name);

    -- ��������� ��� �������
    if p_type not in ('CBRF_BNKSEEK', 'CBRF_BNKDEL') OR p_type IS NULL then
      v_audit_id:=rf_pkg_audit.record_event(
        par_user_login      => p_user
       ,par_ip_address      => p_ip_address
       ,par_event_type_cd   => 'GUI_IMP_UNKNOWN'
       ,par_err_type_cd     => 'T'
       ,par_event_params    => v_aud_json
       ,par_message_tx      =>  '����������� ��� �������: '||p_type
       ); 
      raise_application_error(-20001, '����������� ��� �������: '||p_type);
    end if;

    /* ���������� ID ������������ */
    select max(owner_seq_id) into v_owner_seq_id
      from mantas.kdd_review_owner
     where owner_id = p_user;
    if v_owner_seq_id is null then
      v_audit_id:=rf_pkg_audit.record_event(
        par_user_login      => NULL
       ,par_ip_address      => p_ip_address
       ,par_event_type_cd   => 'GUI_IMP_'||p_type
       ,par_err_type_cd     => 'T'
       ,par_event_params    => v_aud_json
       ,par_message_tx      =>  '������ �������� ������� ������������: '||p_user
       ); 
      raise_application_error(-20001, '������ �������� ������� ������������: '||p_user);
    end if; 

    -- ��������� ����� �� ������ ��� ���� �������
    if nvl(mantas.rf_pkg_adm.get_imptp_access(p_user, p_type), 0) <> 2 then
      v_audit_id:=rf_pkg_audit.record_event(
        par_user_login      => p_user
       ,par_ip_address      => p_ip_address
       ,par_event_type_cd   => 'GUI_IMP_'||p_type
       ,par_err_type_cd     => 'A'
       ,par_event_params    => v_aud_json
       ,par_message_tx      =>  '��� ���� �� ������ ��� ���� �������: '||p_type
       ); 
      return create_response(false, '��� ���� �� ������ ��� ���� �������: '||p_type);
    end if;

    -- ������� ������ ������
    v_audit_id:=rf_pkg_audit.record_event
        ( par_user_login => p_user
         ,par_ip_address => p_ip_address
         ,par_event_type_cd => 'GUI_IMP_'||p_type
         ,par_event_params  => v_aud_json);

    mantas.rf_pkg_import.save_dbf_to_file(
      p_type      => p_type,
      p_date_act  => p_date_act,
      p_file_name => p_file_name,
      p_note      => p_note,
      p_user      => p_user,
      p_file      => p_file,
      p_status    => v_status,
      p_err_msg   => v_err_msg,
      p_load_qty  => v_load_qty
    );
    
    v_json := rf_json();
    if v_status = 'O' then 
      v_return := create_response(true, '�������� ������ ������� ���������! '||chr(10)||'��������� '||v_load_qty||' �������.');
      rf_pkg_audit.set_event_result(v_audit_id,NULL,'�������� ������ ������� ���������! '||chr(10)||'��������� '||v_load_qty||' �������.');
    elsif v_status = 'W' then 
      v_return := create_response(true, '�������� ������ ��������� � ���������������! '||chr(10)||'��������� '||v_load_qty||' �������. '||chr(10)||v_err_msg);
      rf_pkg_audit.set_event_result(v_audit_id,NULL,'�������� ������ ��������� � ���������������! '||chr(10)||'��������� '||v_load_qty||' �������. '||chr(10)||v_err_msg);
    else
      v_return := create_response(false, '�������� ������ �������� ��-�� ������! '||chr(10)||v_err_msg);
      rf_pkg_audit.set_event_result(v_audit_id,'T','�������� ������ �������� ��-�� ������! '||chr(10)||v_err_msg);
    end if;
    
    return v_return;
    
    exception
      when others then
        return create_response(false, '������ ������� ������ ['||p_type||']. '||sqlerrm());
  end;

 /* 
   ����� ������ ������� ��������� ��� ������ � ��������� ��������� ����������� ������
   *
   * ��������� ������
   *   p_json - ������� ������
   *   p_user - ��� ������������
   
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */

 function get_loaded_files_history( p_json in rf_json, p_user in varchar2, p_id_request in number ) return rf_json
  is
    v_sql varchar2(32000 char):=null;
    v_date_dt_from varchar(50):=null;
    v_date_dt_to varchar(50):=null;
    v_tb_cd number:=null;
    v_loaded_by mantas.rf_imp_history.loaded_by%type:=null;
    v_imp_type_cd mantas.rf_imp_history.imp_type_cd%type:=null;
    v_bind_json rf_json:= rf_json();  

    v_json_out   rf_json := null;
  begin 
    v_date_dt_from:=rf_json_ext.get_string (p_json,'data_dt_from');
    v_date_dt_to:=rf_json_ext.get_string (p_json,'data_dt_to');
    v_tb_cd:=TO_CHAR(NULLIF(rf_json_ext.get_number (p_json,'tb_cd'),0));
    v_loaded_by:=rf_json_ext.get_string (p_json,'loaded_by');
    v_imp_type_cd:=rf_json_ext.get_string (p_json,'imp_type_cd');
  
    if v_imp_type_cd is null then
      v_json_out := rf_json();
      v_json_out.put( 'success', false );
      v_json_out.put( 'message', '�� ������ ������������ �������� - ��� �������' );
      return v_json_out;
    end if;
    
    -- �������� ����� ������������ �� ��������� ��� �������
    if nvl(mantas.rf_pkg_adm.get_imptp_access(p_user, v_imp_type_cd), 0) = 0 Then
      v_json_out := rf_json();
      v_json_out.put( 'success', false );
      v_json_out.put( 'message', '��� ���� �� ��� �������: '||v_imp_type_cd );
      return v_json_out;
    end if;
  
    v_sql := q'{
       select 
        f.dpt_cd as DPT_ID,
        f.sys_cd as SYS_CD,
        h.file_nm as FILE_NM,
        decode(h.load_status,
               'O', '�������',
               'W', '��������������',
               'E', '������',
                null
               ) as LOAD_STATUS,
        h.err_msg_tx as ERR_MSG_TX,
        h.load_qty as LOAD_QTY,
        f.data_dt as DATA_DT,
        f.order_nb as ORDER_NB,
        f.archive_nm as ARCHIVE_NM,
        h.loaded_by as LOADED_BY, 
        h.load_dt as LOAD_DT
       from mantas.rf_imp_file f 
        left join mantas.rf_imp_history h on h.imp_file_id = f.imp_file_id and h.last_load_flag=1
       where f.imp_type_cd = :IMP_TYPE_CD       
    }';  
    v_bind_json.put( 'IMP_TYPE_CD', v_imp_type_cd);   
    
    if v_date_dt_from is not null then
      v_sql:=v_sql||q'{ and f.data_dt >= TO_DATE(:DATE_DT_FROM, '##FORMATDATE##') }';  
      v_bind_json.put( 'DATE_DT_FROM',v_date_dt_from );    
    end if;   
    if v_date_dt_to is not null then
      v_sql:=v_sql||q'{ and f.data_dt <= TO_DATE(:DATE_DT_TO, '##FORMATDATE##') }';  
      v_bind_json.put( 'DATE_DT_TO', v_date_dt_to );
    end if;    
    
    if v_tb_cd is not null then
      v_sql:=v_sql||q'{ and f.dpt_cd like :TB_CD||'%' }';
      v_bind_json.put( 'TB_CD', v_tb_cd);
    end if;   
    if v_loaded_by is not null then
      v_sql:=v_sql||q'{ and upper(h.loaded_by) = upper(:LOADED_BY) }';
      v_bind_json.put( 'LOADED_BY', v_loaded_by ); 
    end if;              
    
    v_sql:=v_sql||q'{ order by h.load_status, f.dpt_cd,f.data_dt }';
    v_sql := replace(v_sql, '##FORMATDATE##', rf_json_ext.format_string);
    return rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );   
  end;

    /*
   ����� ������ ������� ��������� ��� ������ � ��������� ����������� ������ �� ��������� ������
   *
   * ��������� ������
   *   p_json - ������� ������
   *   p_user - ��� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */
  function get_history_from_last_session( p_json in rf_json, p_user in varchar2, p_id_request in number) return rf_json
  is
    v_sql varchar2(32000 char):=null;
    v_bind_json rf_json:= rf_json();
    v_loaded_by mantas.rf_imp_session.loaded_by%type:=null;
    v_imp_type_cd mantas.rf_imp_session.imp_type_cd%type:=null;
    v_start_dt mantas.rf_imp_session.start_dt%type:=null;
    v_end_dt mantas.rf_imp_session.end_dt%type:=null;
    v_extra_params_tx mantas.rf_imp_session.extra_params_tx%type:=null;
    v_note_tx mantas.rf_imp_session.note_tx%type:=null;
    v_session_id mantas.rf_imp_session.imp_session_id%type:=null;
    v_out_json rf_json:= rf_json();
    v_param_json rf_json; 
    v_owner varchar2(255);
  begin
    v_loaded_by:=rf_json_ext.get_string (p_json,'loaded_by');
    v_imp_type_cd:=rf_json_ext.get_string (p_json,'imp_type_cd');
            
    -- �������� ����� ������������ �� ��������� ��� �������
    if v_imp_type_cd is not null Then
      if nvl(mantas.rf_pkg_adm.get_imptp_access(p_user, v_imp_type_cd), 0) = 0 Then
        v_out_json := rf_json();
        v_out_json.put( 'success', false );
        v_out_json.put( 'message', '��� ���� �� ��� �������: '||v_imp_type_cd );
        return v_out_json;
      end if;
    end if;  

    select max(s.imp_session_id), max(s.start_dt), max(s.end_dt), max(s.extra_params_tx), max(s.note_tx) 
    into v_session_id, v_start_dt,v_end_dt, v_extra_params_tx, v_note_tx
        from mantas.rf_imp_session s, (
             select max(s.imp_session_id) keep(dense_rank first order by s.start_dt desc NULLS LAST) as id_ 
                    from mantas.rf_imp_session s
                         where s.loaded_by=v_loaded_by and s.imp_type_cd=v_imp_type_cd) id_ses
    where s.imp_session_id=id_ses.id_; 
 
    v_sql := q'{ 
         select 
          h.file_nm as FILE_NM, 
          decode(h.load_status,
               'O', '�������',
               'W', '��������������',
               'E', '������',
                null
               ) as LOAD_STATUS,
          h.err_msg_tx as ERR_MSG_TX, 
          h.load_qty as LOAD_QTY, 
          f.dpt_cd as DPT_ID,
          f.sys_cd as SYS_CD, 
          f.data_dt as DATA_DT, 
          f.order_nb as ORDER_NB, 
          f.archive_nm as ARCHIVE_NM
         from mantas.rf_imp_history h left join mantas.rf_imp_file f on h.imp_file_id=f.imp_file_id
         where h.imp_session_id=:IMP_SES_ID
         order by h.load_status, h.file_nm
    }';  
    v_bind_json.put('IMP_SES_ID', v_session_id );
    v_out_json:=mantas.rf_pkg_request.exec_sql_read_json( v_sql, p_id_request, v_bind_json );     
    v_out_json.put('loaded_by',v_loaded_by); 
    v_out_json.put('start_dt',TO_CHAR(v_start_dt,rf_json_ext.format_string)); 
    v_out_json.put('end_dt',TO_CHAR(v_end_dt,rf_json_ext.format_string)); 
    v_out_json.put('imp_type_cd',v_imp_type_cd); 
    v_out_json.put('note_tx', v_note_tx); 
    
    if(v_extra_params_tx is not null) then 
       v_param_json:= rf_json(v_extra_params_tx);
       v_owner :=rf_json_ext.get_string (v_param_json,'ownerId');  
       v_out_json.put('extra_params_tx','�������������: '||v_owner);  
    end if;
    
   return v_out_json;
  end; 
  
  /* ������� ��������� ��������� ������
   *   
   * ��������� ������
   *   p_request - �������� ������ � ������� json
   *   p_user - ��� �������� ������������
   *   p_id_request - ���������������� ����� �������
   * ��������� ������
   *   rf_json - ����� � ������� json
   */ 
  function process_request( p_request in clob, p_user in varchar2,  p_id_request in number ) return rf_json
  is
    v_request    clob := null;
    v_form_name  varchar2(64) := null;
    v_action     varchar2(64) := null;
    v_json_in    rf_json := null;
    v_json_out   rf_json := null;
  begin
    /* ������ ���� � ������ ��� "T" */
    v_request := regexp_replace(p_request, '([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})', '\1 \2');
    v_json_in := rf_json(v_request);
    v_form_name := rf_json_ext.get_string(v_json_in, 'form');
    v_action := rf_json_ext.get_string(v_json_in, 'action');

    CASE 
      WHEN v_form_name = 'ImpType' and v_action = 'read' THEN 
        v_json_out := get_import_types( v_json_in, p_user, p_id_request );
      WHEN v_form_name ='ImpFiles' and v_action = 'read' THEN
        v_json_out := get_loaded_files_history( v_json_in, p_user, p_id_request );  
      WHEN v_form_name ='ImpSession' and v_action = 'readLatest' THEN
        v_json_out :=get_history_from_last_session( v_json_in, p_user, p_id_request );
      ELSE NULL;  
    END CASE;
         
    return v_json_out;
    
    exception
      when others then
        rf_pkg_request.log_request_error( sqlerrm(), p_id_request );
        v_json_out := rf_json();
        v_json_out.put( 'success', false );
        v_json_out.put( 'message', '�� ������� ��������� ������! ��������� ���������� ������.'||sqlerrm() );
        return v_json_out;
  end;

end;
/
