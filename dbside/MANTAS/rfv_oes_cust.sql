begin
  execute immediate 'drop type mantas.rf_tab_oes_cust';
  exception
    when others then null;
end;
/

create or replace type mantas.rf_type_oes_cust as object (
  cust_seq_id number,

  tu    number,
  nameu varchar2(2000 char),
  nd    varchar2(2000 char),
  rg    varchar2(2000 char),
  gr    date,
  bp    varchar2(2000 char),
  
  kodcr  varchar2(5 char),
  amr_s  varchar2(2 char),
  amr_r  varchar2(2000 char),
  amr_g  varchar2(2000 char),
  amr_u  varchar2(2000 char),
  amr_d  varchar2(2000 char),
  amr_k  varchar2(2000 char),
  amr_o  varchar2(2000 char),
  amr_tx varchar2(2000 char),
  
  kodcn     varchar2(5 char),
  adress_s  varchar2(2 char),
  adress_r  varchar2(2000 char),
  adress_g  varchar2(2000 char),
  adress_u  varchar2(2000 char),
  adress_d  varchar2(2000 char),
  adress_k  varchar2(2000 char),
  adress_o  varchar2(2000 char),
  adress_tx varchar2(2000 char),
  
  kd  varchar2(2 char),
  sd  varchar2(2000 char),
  vd1 varchar2(2000 char),
  vd2 varchar2(2000 char),
  vd3 date,
  
  vd4 varchar2(2 char),
  vd5 varchar2(2000 char),
  vd6 date,
  vd7 date,
  
  mc1 varchar2(2000 char),
  mc2 date,
  mc3 date,
  
  src_cd varchar2(3 char)
)
/

create or replace type mantas.rf_tab_oes_cust as table of mantas.rf_type_oes_cust
/

create or replace function mantas.rfv_oes_cust(
  p_cust_seq_id mantas.rf_oes_321_party.cust_seq_id%type,
  p_tu          mantas.rf_oes_321_party.tu%type
) return mantas.rf_tab_oes_cust pipelined
is
  v_cust mantas.rf_type_oes_cust := mantas.rf_type_oes_cust(null,null,null,null,null,null,null,null,null,null,
                                                            null,null,null,null,null,null,null,null,null,null,
                                                            null,null,null,null,null,null,null,null,null,null,
                                                            null,null,null,null,null,null,null,null);
begin
  for cur in (
    select /*+ ORDERED USE_NL(orig orig_fl orig_addr orig_fl_addr orig_addr_state cntry_cr fl_cntry_cr cntry_ctz state_cr orig_addr_h orig_addr_state_h cntry_cn state_cn orig_doc iddoctp iddoc_stay iddoc_mc iddoctp_stay) */
           orig.cust_seq_id cust_seq_id,
           
           --����� ��������
           orig.tu as tu,
           case 
             when orig.tu = 3 then coalesce(orig_fl.full_nm, 
                                         trim(regexp_replace(replace(orig.full_nm, '�� '), '��������������\s+���������������|���.\s+���������������', '',  1, 0, 'i')),
                                         orig.full_nm
                                        )
             else orig.full_nm
           end as nameu,
           
           case 
             when regexp_like(trim(orig.tax_id), '^0+$') then null
             else orig.tax_id 
           end as nd,
           
           case 
             when orig.tu <> 2 then case 
                                   when regexp_like(trim(orig.rf_ogrn_nb), '^0+$') then null
                                   else orig.rf_ogrn_nb
                                 end
           end as rg,
           
           case 
             when orig.tu in (2, 3) then orig.birth_dt
             else orig.rf_reg_dt
           end as gr,
           
           orig.rf_birthplace_tx as bp,
    
           --�������� � ����� �����������
           nullif(case
                    when orig.tu in (2, 3) then nvl(fl_cntry_cr.iso_numeric_cd, cntry_cr.iso_numeric_cd)
                    when orig.tu in (1) then nvl(cntry_ctz.iso_numeric_cd, cntry_cr.iso_numeric_cd)
                    else cntry_cr.iso_numeric_cd
                  end||'00',
                  '00'
                 ) as kodcr,
                 
           substr(state_cr.okato_cd, 1, 2) as amr_s, 
           orig_addr.addr_rgn_nm as amr_r, 
           
           coalesce(case 
                      when orig_addr.rf_settlm_nm is not null and orig_addr.rf_settlm_type_cd is not null then orig_addr.rf_settlm_type_cd||'. '||orig_addr.rf_settlm_nm
                      else orig_addr.rf_settlm_nm
                    end,
                    case 
                      when orig_addr.addr_city_nm is not null and orig_addr.rf_city_type_cd is not null then orig_addr.rf_city_type_cd||'. '||orig_addr.addr_city_nm
                      else orig_addr.addr_city_nm
                    end
                  ) as amr_g, 
                  
           case 
             when orig_addr.addr_strt_line1_tx is not null and orig_addr.addr_strt_line2_tx is not null then orig_addr.addr_strt_line2_tx||'. '||orig_addr.addr_strt_line1_tx
             else orig_addr.addr_strt_line1_tx
           end as amr_u, 
           
           orig_addr.addr_strt_line3_tx as amr_d, 
           orig_addr.addr_strt_line4_tx as amr_k, 
           orig_addr.addr_strt_line5_tx as amr_o,
           nvl(business.rf_pkg_util.get_addr_text(orig_addr.cust_addr_seq_id, null), orig_addr.rf_addr_tx) as amr_tx,
           
           --�������� � ����� ����������
           nullif(case 
                    when orig.tu in (2, 3) then cntry_ctz.iso_numeric_cd 
                    else cntry_cn.iso_numeric_cd
                  end||'00',
                  '00'
                 ) as kodcn,
           
           substr(state_cn.okato_cd, 1, 2) as adress_s, 
           orig_addr_h.addr_rgn_nm  as adress_r, 
           
           coalesce(case 
                      when orig_addr_h.rf_settlm_nm is not null and orig_addr_h.rf_settlm_type_cd is not null then orig_addr_h.rf_settlm_type_cd||'. '||orig_addr_h.rf_settlm_nm
                      else orig_addr_h.rf_settlm_nm
                    end,
                    case 
                      when orig_addr_h.addr_city_nm is not null and orig_addr_h.rf_city_type_cd is not null then orig_addr_h.rf_city_type_cd||'. '||orig_addr_h.addr_city_nm
                      else orig_addr_h.addr_city_nm
                    end
                   ) as adress_g, 
                   
           case 
             when orig_addr_h.addr_strt_line1_tx is not null and orig_addr_h.addr_strt_line2_tx is not null then orig_addr_h.addr_strt_line2_tx||'. '||orig_addr_h.addr_strt_line1_tx
             else orig_addr_h.addr_strt_line1_tx
           end as adress_u, 
           
           orig_addr_h.addr_strt_line3_tx as adress_d, 
           orig_addr_h.addr_strt_line4_tx as adress_k, 
           orig_addr_h.addr_strt_line5_tx as adress_o,
           nvl(business.rf_pkg_util.get_addr_text(orig_addr_h.cust_addr_seq_id, null), orig_addr_h.rf_addr_tx) as adress_tx,
           
           --��������, �������������� ��������
           substr(case 
                    when orig.tu = 1 then null
                    when replace(trim(orig_doc.series_id), '0')||orig_doc.no_id||orig_doc.issued_by_tx||orig_doc.issue_dpt_cd is not null or orig_doc.issue_dt is not null then nvl(iddoctp.cbrf_cd, '21')
                    else iddoctp.cbrf_cd
                  end, 
                  1, 2
                 ) as kd,
           
           case 
             when orig.tu = 1 then case 
                                  when regexp_like(trim(orig.rf_okpo_nb), '^0+$') then null
                                  else orig.rf_okpo_nb
                                end
             else case 
                    when regexp_like(trim(orig_doc.series_id), '^0+$') then null
                    else orig_doc.series_id
                  end 
           end as sd,
           
           case 
             when orig.tu <> 1 then orig_doc.no_id 
           end as vd1, 
           
           case 
             when orig.tu <> 1 then orig_doc.issued_by_tx||nvl2(orig_doc.issue_dpt_cd, ', '||orig_doc.issue_dpt_cd, null) 
           end as vd2, 
           
           case 
             when orig.tu <> 1 then orig_doc.issue_dt
           end as vd3,
    
           
           --��������, �������������� ����� ����������
           substr(case 
                    when orig.tu = '1' or nvl(orig.rf_resident_fl, 'Y') = 'Y' then null
                    else iddoctp_stay.cbrf_cd 
                  end,
                  1, 2
                 ) vd4, 
                 
           case 
             when orig.tu = 1 or nvl(orig.rf_resident_fl, 'Y') = 'Y' then null 
             else iddoc_stay.series_id||iddoc_stay.no_id 
           end as vd5, 
           
           case 
             when orig.tu = 1 or nvl(orig.rf_resident_fl, 'Y') = 'Y' then null 
             else iddoc_stay.issue_dt
           end as vd6, 
           
           case 
             when orig.tu = 1 or nvl(orig.rf_resident_fl, 'Y') = 'Y' then null 
             else iddoc_stay.end_dt
           end as vd7,
    
           
           --������������ �����
           case 
             when orig.tu <> '1'then iddoc_mc.series_id||iddoc_mc.no_id 
           end as mc1, 
           
           case 
             when orig.tu <> 1 then iddoc_mc.issue_dt
           end as mc2, 
           
           case 
             when orig.tu <> 1 then iddoc_mc.end_dt
           end as mc3,
           
           orig.src_sys_cd src_cd

      from (select case
                     when p_tu is not null then p_tu
                     else case 
                            when t.cust_type_cd in ('ORG', 'FIN') then 1
                            when t.cust_type_cd = 'IND' then 2
                            when t.cust_type_cd = 'IND_ORG' then 3 
                            else 2
                          end
                     end tu,       
                   t.*
              from business.cust t
             where t.cust_seq_id = p_cust_seq_id
           ) orig
      left join business.cust orig_fl on orig_fl.cust_seq_id = orig.rf_ind_cust_seq_id
      left join business.cust_addr orig_addr on orig_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(orig.cust_seq_id, 'L')
      left join business.cust_addr orig_fl_addr on orig_fl_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(orig_fl.cust_seq_id, 'L')
      left join business.rf_addr_object orig_addr_state on orig_addr_state.addr_seq_id = coalesce(orig_addr.rf_state_addr_seq_id,
                                                                                                  case 
                                                                                                    when orig_addr.rf_state_nm is not null then std.aml_tools.recognise_region2(orig_addr.rf_state_nm)
                                                                                                   end,
                                                                                                   case 
                                                                                                     when orig_addr.addr_city_nm is not null then std.aml_tools.recognise_region2(orig_addr.addr_city_nm)
                                                                                                   end
                                                                                                  )
      left join business.rf_country cntry_cr on cntry_cr.cntry_cd = nvl(orig_addr_state.cntry_cd, orig_addr.addr_cntry_cd)
      left join business.rf_country fl_cntry_cr on fl_cntry_cr.cntry_cd = orig_fl_addr.addr_cntry_cd
      left join business.rf_country cntry_ctz on cntry_ctz.cntry_cd = orig.ctzshp_cntry1_cd
      left join business.rf_addr_object state_cr on state_cr.addr_seq_id = orig_addr_state.addr_seq_id
      left join business.cust_addr orig_addr_h on orig_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(orig.cust_seq_id, 'H')
      left join business.rf_addr_object orig_addr_state_h on orig_addr_state_h.addr_seq_id = coalesce(orig_addr_h.rf_state_addr_seq_id,
                                                                                                      case 
                                                                                                        when orig_addr_h.rf_state_nm is not null then std.aml_tools.recognise_region2(orig_addr_h.rf_state_nm)
                                                                                                      end,
                                                                                                      case 
                                                                                                        when orig_addr_h.addr_city_nm is not null then std.aml_tools.recognise_region2(orig_addr_h.addr_city_nm)
                                                                                                      end
                                                                                                     )
      left join business.rf_country cntry_cn on cntry_cn.cntry_cd = nvl(orig_addr_state_h.cntry_cd, orig_addr_h.addr_cntry_cd)
      left join business.rf_addr_object state_cn on state_cn.addr_seq_id = orig_addr_state_h.addr_seq_id
      left join business.rf_cust_id_doc orig_doc on orig_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(orig.cust_seq_id)
      left join business.rf_cust_id_doc_type iddoctp on iddoctp.doc_type_cd = orig_doc.doc_type_cd
      left join business.rf_cust_id_doc iddoc_stay on iddoc_stay.cust_doc_seq_id = to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, null, 'Y'))
      left join business.rf_cust_id_doc iddoc_mc on iddoc_mc.cust_doc_seq_id = to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, 39))
      left join business.rf_cust_id_doc_type iddoctp_stay on iddoctp_stay.doc_type_cd = iddoc_stay.doc_type_cd
  ) loop
    v_cust.cust_seq_id := cur.cust_seq_id;
    v_cust.tu          := cur.tu;
    v_cust.nameu       := cur.nameu;
    v_cust.nd          := cur.nd;
    v_cust.rg          := cur.rg;
    v_cust.gr          := cur.gr;
    v_cust.bp          := cur.bp;
    v_cust.kodcr       := cur.kodcr;
    v_cust.amr_s       := cur.amr_s;
    v_cust.amr_r       := cur.amr_r;
    v_cust.amr_g       := cur.amr_g;
    v_cust.amr_u       := cur.amr_u;
    v_cust.amr_d       := cur.amr_d;
    v_cust.amr_k       := cur.amr_k;
    v_cust.amr_o       := cur.amr_o;
    v_cust.amr_tx      := cur.amr_tx;
    v_cust.kodcn       := cur.kodcn;
    v_cust.adress_s    := cur.adress_s;
    v_cust.adress_r    := cur.adress_r;
    v_cust.adress_g    := cur.adress_g;
    v_cust.adress_u    := cur.adress_u;
    v_cust.adress_d    := cur.adress_d;
    v_cust.adress_k    := cur.adress_k;
    v_cust.adress_o    := cur.adress_o;
    v_cust.adress_tx   := cur.adress_tx;
    v_cust.kd          := cur.kd;
    v_cust.sd          := cur.sd;
    v_cust.vd1         := cur.vd1;
    v_cust.vd2         := cur.vd2;
    v_cust.vd3         := cur.vd3;
    v_cust.vd4         := cur.vd4;
    v_cust.vd5         := cur.vd5;
    v_cust.vd6         := cur.vd6;
    v_cust.vd7         := cur.vd7;
    v_cust.mc1         := cur.mc1;
    v_cust.mc2         := cur.mc2;
    v_cust.mc3         := cur.mc3;
    v_cust.src_cd      := cur.src_cd;
    
    pipe row(v_cust);
  end loop;
  
end;
/
