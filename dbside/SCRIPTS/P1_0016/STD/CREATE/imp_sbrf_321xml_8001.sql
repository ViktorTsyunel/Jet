begin execute immediate 'drop table STD.IMP_SBRF_321XML_8001'; exception when others then null; end;
/

/*==============================================================*/
/* Register DTD: courier.dtd                                    */
/*==============================================================*/
declare
  v_dtd_name varchar2(16) := '/courier.dtd';
  v_dtd      varchar2(32000);
  v_result   boolean;
begin
  begin
    dbms_xdb.deleteResource(v_dtd_name, dbms_xdb.DELETE_FORCE);
    exception
      when others then null;
  end;
  
  v_dtd := q'{
    <?xml version='1.0' encoding='UTF-8'?>

    <!ELEMENT doclist (document*)>
    
    <!ELEMENT document (fieldlist,attach*,link*,sign*)>
    <!ATTLIST document
       type  CDATA #REQUIRED
       id CDATA #REQUIRED
       creator  CDATA #REQUIRED
       created  CDATA #IMPLIED
    >
    
    <!ELEMENT fieldlist (field*)>
    
    <!ELEMENT field (#PCDATA)>
    <!ATTLIST field
       name  CDATA #REQUIRED
    >
    
    <!ELEMENT attach (field*,attdata)>
    <!ATTLIST attach
       type  CDATA #REQUIRED
    >
    
    <!ELEMENT attdata (#PCDATA)>
    <!ATTLIST attdata
       bin2ascii CDATA "Hex"
    >
    
    <!ELEMENT link (field*)>
    
    <!ELEMENT sign (signkey+,signdata)>
    
    <!ELEMENT signkey (#PCDATA)>
    <!ATTLIST signkey
       keyid CDATA #REQUIRED
       algorithm CDATA #REQUIRED
       provider CDATA #REQUIRED
    >
    
    <!ELEMENT signdata (#PCDATA)>
    <!ATTLIST signdata
       bin2ascii CDATA "Hex"
    >
  }';
  v_result := dbms_xdb.createResource(v_dtd_name,v_dtd);
end;
/

create global temporary table STD.IMP_SBRF_321XML_8001 (
  file_id          number not null,
  file_nm          varchar2(255 char) not null,
  archive_nm       varchar2(255 char),
  error_loading_fl number default 0 not null,
  err_msg_tx       varchar2(4000),
  file_body        XMLType
)
on commit preserve rows
XMLType column file_body store as clob;

comment on table STD.IMP_SBRF_321XML_8001 is '��������� ������� ��� �������� XML ������ � ����������.';
comment on column STD.IMP_SBRF_321XML_8001.FILE_ID is '���� �����. ����������� ����������� ������ ��� �� 1.';
comment on column STD.IMP_SBRF_321XML_8001.FILE_NM is '��� �����.';
comment on column STD.IMP_SBRF_321XML_8001.ARCHIVE_NM is '��� ������. � ����� ������ ����� ���� ������ ������ �����.';
comment on column STD.IMP_SBRF_321XML_8001.ERROR_LOADING_FL is '���� ���������� ��������. 0-�������, 1-������. �������� �� ��������� 0.';
comment on column STD.IMP_SBRF_321XML_8001.ERR_MSG_TX is '����� ������.';
comment on column STD.IMP_SBRF_321XML_8001.FILE_BODY is 'XML ����.';

grant select, insert, delete, update on STD.IMP_SBRF_321XML_8001 to mantas;
