begin execute immediate 'drop table STD.IMP_SBRF_321XML'; exception when others then null; end;
/

/*==============================================================*/
/* Register XSD: fns.esb.sbrf.ru                                */
/*==============================================================*/
declare
  v_schema_url constant varchar2(64) := 'fns.esb.sbrf.ru';
  v_xsd xmltype;
begin
  begin
    dbms_xmlschema.deleteschema(
      schemaurl => v_schema_url,
      delete_option => dbms_xmlschema.delete_cascade_force
    );
    exception
      when others then null;
  end;

  v_xsd := xmltype(
    '<xs:schema xmlns="fns.esb.sbrf.ru" 
	       xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	       targetNamespace="fns.esb.sbrf.ru">
      <xs:element name="idOperSystem" type="xs:string"/>
      <xs:element name="WORK">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="REPORT"/>
            <xs:element ref="DESCRIPTION"/>
            <xs:element ref="DATE"/>
            <xs:element ref="MESSAGEID"/>
            <xs:element ref="ASYSTEM"/>
            <xs:element ref="DEPARTMENT"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="VP" type="xs:string"/>
      <xs:element name="VO" type="xs:string"/>
      <xs:element name="VERSION" type="xs:string"/>
      <xs:element name="VD_7" type="xs:date"/>
      <xs:element name="VD_6" type="xs:date"/>
      <xs:element name="VD_5" type="xs:string"/>
      <xs:element name="VD_4" type="xs:string"/>
      <xs:element name="VD_3" type="xs:date"/>
      <xs:element name="VD_2" type="xs:string"/>
      <xs:element name="VD_1" type="xs:string"/>
      <xs:element name="VD">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="VD_1"/>
            <xs:element ref="VD_2"/>
            <xs:element ref="VD_3"/>
            <xs:element ref="VD_4"/>
            <xs:element ref="VD_5"/>
            <xs:element ref="VD_6"/>
            <xs:element ref="VD_7"/>
            <xs:element ref="MC_1"/>
            <xs:element ref="MC_2"/>
            <xs:element ref="MC_3"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="TYPE_INFO">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="VERSION"/>
            <xs:element ref="ACTION"/>
            <xs:element ref="NUMB_P"/>
            <xs:element ref="DATE_P"/>
            <xs:element ref="TEL"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="TUREAL" type="xs:string"/>
      <xs:element name="TERROR" type="xs:string"/>
      <xs:element name="TELEPHON" type="xs:string"/>
      <xs:element name="TEL" type="xs:string"/>
      <xs:element name="SUM_CON" type="xs:decimal"/>
      <xs:element name="SUME" type="xs:decimal"/>
      <xs:element name="SUM"  type="xs:decimal"/>
      <xs:element name="SOURCE_INFO">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="FILIAL"/>
            <xs:element ref="ASYSTEM"/>
            <xs:element ref="idOperSystem"/>
            <xs:element ref="OPERATOR_S"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="SENDER">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="TELEPHON"/>
            <xs:element ref="FIO"/>
            <xs:element ref="POST"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="SD" type="xs:string"/>
      <xs:element name="RG" type="xs:string"/>
      <xs:element name="REPORT" type="xs:string"/>
      <xs:element name="PRU"  type="xs:string"/>
      <xs:element name="PRMANUAL"  type="xs:string"/>
      <xs:element name="PRIZ_SD"  type="xs:string"/>
      <xs:element name="PRIZ6001"  type="xs:string"/>
      <xs:element name="PRIM" type="xs:string"/>
      <xs:element name="POST"  type="xs:string"/>
      <xs:element name="PART"  type="xs:string"/>
      <xs:element name="OPER_INFO">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="TERROR"/>
            <xs:element ref="VO"/>
            <xs:element ref="DOP_V"/>
            <xs:element ref="DATA"/>
            <xs:element ref="SUME"/>
            <xs:element ref="SUM"/>
            <xs:element ref="CURREN"/>
            <xs:element ref="PRIM"/>
            <xs:element ref="NUM_PAY_D"/>
            <xs:element ref="DATE_PAY_D"/>
            <xs:element ref="METAL"/>
            <xs:element ref="PRIZ6001"/>
            <xs:element ref="B_PAYER"/>
            <xs:element ref="B_RECIP"/>
            <xs:element ref="PART"/>
            <xs:element ref="DESCR"/>
            <xs:element ref="CURREN_CON"/>
            <xs:element ref="SUM_CON"/>
            <xs:element ref="PRIZ_SD"/>
            <xs:element ref="DATE_S"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="OPERATOR_S"  type="xs:string"/>
      <xs:element name="OPERATOR_M"  type="xs:string"/>
      <xs:element name="NUM_PAY_D"  type="xs:string"/>
      <xs:element name="NUMB_P"  type="xs:string"/>
      <xs:element name="NUMBF_SS"  type="xs:string"/>
      <xs:element name="ND"  type="xs:string"/>
      <xs:element name="NAME_R"  type="xs:string"/>
      <xs:element name="NAME_IS_B"  type="xs:string"/>
      <xs:element name="NAME_B"  type="xs:string"/>
      <xs:element name="NAMEU"  type="xs:string"/>
      <xs:element name="METAL"  type="xs:string"/>
      <xs:element name="MESSAGEID"  type="xs:string"/>
      <xs:element name="MEMBER_INFO">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="BLOCK"/>
            <xs:element ref="MEMBER"/>
            <xs:element ref="CO"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="MEMBER">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="TUREAL"/>
            <xs:element ref="PRU"/>
            <xs:element ref="NAMEU"/>
            <xs:element ref="KODCR"/>
            <xs:element ref="KODCN"/>
            <xs:element ref="AMR"/>
            <xs:element ref="ADRESS"/>
            <xs:element ref="KD"/>
            <xs:element ref="SD"/>
            <xs:element ref="RG"/>
            <xs:element ref="ND"/>
            <xs:element ref="VD"/>
            <xs:element ref="GR"/>
            <xs:element ref="BP"/>
            <xs:element ref="VP"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="MC_3" type="xs:date"/>
      <xs:element name="MC_2" type="xs:date"/>
      <xs:element name="MC_1" type="xs:string"/>
      <xs:element name="MAIN">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="WORK"/>
            <xs:element ref="INFO_PART" maxOccurs="unbounded"/>
            <xs:element ref="SENDER"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="KO_INFO">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="BRANCH"/>
            <xs:element ref="NUMBF_SS"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="KODCR" type="xs:string"/>
      <xs:element name="KODCN_R" type="xs:string"/>
      <xs:element name="KODCN_B" type="xs:string"/>
      <xs:element name="KODCN" type="xs:string"/>
      <xs:element name="KD" type="xs:string"/>
      <xs:element name="INFO_PART">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="TYPE_INFO"/>
            <xs:element ref="KO_INFO"/>
            <xs:element ref="OPER_INFO"/>
            <xs:element ref="DOP_OPER_INFO"/>
            <xs:element ref="MEMBER_INFO" maxOccurs="unbounded"/>
            <xs:element ref="SOURCE_INFO"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="GR"  type="xs:date"/>
      <xs:element name="FIO"  type="xs:string"/>
      <xs:element name="FILIAL" type="xs:string"/>
      <xs:element name="DOP_V" type="xs:string"/>
      <xs:element name="DOP_OPER_INFO">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="PRMANUAL"/>
            <xs:element ref="COMMENT"/>
            <xs:element ref="OPERATOR_M"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="DESCRIPTION" type="xs:string"/>
      <xs:element name="DESCR" type="xs:string"/>
      <xs:element name="DEPARTMENT" type="xs:string"/>
      <xs:element name="DATE_S" type="xs:date"/>
      <xs:element name="DATE_PAY_D" type="xs:date"/>
      <xs:element name="DATE_P" type="xs:date"/>
      <xs:element name="DATE" type="xs:date"/>
      <xs:element name="DATA" type="xs:date"/>
      <xs:element name="CURREN_CON" type="xs:string"/>
      <xs:element name="CURREN" type="xs:string"/>
      <xs:element name="COMMENT" type="xs:string"/>
      <xs:element name="CO">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="ACC_B"/>
            <xs:element ref="ACC_COR_B"/>
            <xs:element ref="NAME_IS_B"/>
            <xs:element ref="BIK_IS_B"/>
            <xs:element ref="CARD_B"/>
            <xs:element ref="NAME_B"/>
            <xs:element ref="KODCN_B"/>
            <xs:element ref="BIK_B"/>
            <xs:element ref="NAME_R"/>
            <xs:element ref="KODCN_R"/>
            <xs:element ref="BIK_R"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="CARD_B" type="xs:string"/>
      <xs:element name="B_RECIP" type="xs:string"/>
      <xs:element name="B_PAYER" type="xs:string"/>
      <xs:element name="BRANCH" type="xs:string"/>
      <xs:element name="BP" type="xs:string"/>
      <xs:element name="BLOCK">
        <xs:simpleType>
          <xs:restriction base="xs:byte">
            <xs:enumeration value="0"/>
            <xs:enumeration value="1"/>				
            <xs:enumeration value="2"/>				
            <xs:enumeration value="3"/>
            <xs:enumeration value="4"/>				
          </xs:restriction>
        </xs:simpleType>
      </xs:element>
      <xs:element name="BIK_R" type="xs:string"/>
      <xs:element name="BIK_IS_B" type="xs:string"/>
      <xs:element name="BIK_B" type="xs:string"/>
      <xs:element name="ASYSTEM" type="xs:string"/>
      <xs:element name="AMR">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="ADRESS_S"/>
            <xs:element ref="ADRESS_R"/>
            <xs:element ref="ADRESS_G"/>
            <xs:element ref="ADRESS_U"/>
            <xs:element ref="ADRESS_D"/>
            <xs:element ref="ADRESS_K"/>
            <xs:element ref="ADRESS_O"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="ADRESS_U" type="xs:string"/>
      <xs:element name="ADRESS_S" type="xs:string"/>
      <xs:element name="ADRESS_R" type="xs:string"/>
      <xs:element name="ADRESS_O" type="xs:string"/>
      <xs:element name="ADRESS_K" type="xs:string"/>
      <xs:element name="ADRESS_G" type="xs:string"/>
      <xs:element name="ADRESS_D" type="xs:string"/>
      <xs:element name="ADRESS">
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="ADRESS_S"/>
            <xs:element ref="ADRESS_R"/>
            <xs:element ref="ADRESS_G"/>
            <xs:element ref="ADRESS_U"/>
            <xs:element ref="ADRESS_D"/>
            <xs:element ref="ADRESS_K"/>
            <xs:element ref="ADRESS_O"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name="ACTION">
        <xs:simpleType>
          <xs:restriction base="xs:byte">
            <xs:enumeration value="1"/>
            <xs:enumeration value="2"/>
            <xs:enumeration value="3"/>				
            <xs:enumeration value="4"/>								
          </xs:restriction>
        </xs:simpleType>
      </xs:element>
      <xs:element name="ACC_COR_B" type="xs:string"/>
      <xs:element name="ACC_B" type="xs:string"/>
    </xs:schema>'
  );

  dbms_xmlschema.registerschema(
    schemaurl => v_schema_url,
    schemadoc => v_xsd,
    local => false,
    gentypes => false,
    gentables => false,
    owner => 'STD'
  );
end;
/

create global temporary table STD.IMP_SBRF_321XML (
  file_id          number not null,
  file_nm          varchar2(255 char) not null,
  archive_nm       varchar2(255 char),
  error_loading_fl number default 0 not null,
  err_msg_tx       varchar2(4000),
  file_body        XMLType
)
on commit preserve rows
XMLType column file_body store as clob 
xmlschema "fns.esb.sbrf.ru" element "MAIN";

comment on table STD.IMP_SBRF_321XML is '��������� ������� ��� �������� XML ������ � ����������. ����� ������ ��������������� ����� fns.esb.sbrf.ru.';
comment on column STD.IMP_SBRF_321XML.FILE_ID is '���� �����. ����������� ����������� ������ ��� �� 1.';
comment on column STD.IMP_SBRF_321XML.FILE_NM is '��� �����.';
comment on column STD.IMP_SBRF_321XML.ARCHIVE_NM is '��� ������. � ����� ������ ����� ���� ������ ������ �����.';
comment on column STD.IMP_SBRF_321XML.ERROR_LOADING_FL is '���� ���������� ��������. 0-�������, 1-������. �������� �� ��������� 0.';
comment on column STD.IMP_SBRF_321XML.ERR_MSG_TX is '����� ������.';
comment on column STD.IMP_SBRF_321XML.FILE_BODY is 'XML ���� � ������� XSD = fns.esb.sbrf.ru.';

grant select, insert, delete, update on std.imp_sbrf_321xml to mantas;
