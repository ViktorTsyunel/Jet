/* WIRE_TRXN */
/* ��� */
--drop index business.rf_wire_trxn_orig_inn_dt_am_i;
create index business.rf_wire_trxn_orig_inn_dt_am_i on business.wire_trxn(rf_orig_inn_nb, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_orig_inn_dt_am_i noparallel logging;
--drop index business.rf_wire_trxn_benef_inn_dt_am_i;
create index business.rf_wire_trxn_benef_inn_dt_am_i on business.wire_trxn(rf_benef_inn_nb, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_benef_inn_dt_am_i noparallel logging;

/*�������*/
--drop index business.rf_wire_trxn_orig_id_dt_am_i;
create index business.rf_wire_trxn_orig_id_dt_am_i on business.wire_trxn(rf_orig_cust_seq_id, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_orig_id_dt_am_i noparallel logging;
drop index business.rf_wire_trxn_orig_cust_fk_i;
--drop index business.rf_wire_trxn_benef_id_dt_am_i;
create index business.rf_wire_trxn_benef_id_dt_am_i on business.wire_trxn(rf_benef_cust_seq_id, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_benef_id_dt_am_i noparallel logging;
drop index business.rf_wire_trxn_benef_cust_fk_i;

/* ����� */
--drop index business.rf_wire_trxn_orig_acct_dt_am_i;
create index business.rf_wire_trxn_orig_acct_dt_am_i on business.wire_trxn(upper(rf_orig_acct_nb), trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_orig_acct_dt_am_i noparallel logging;
--drop index business.rf_wire_trxn_ben_acct_dt_am_i;
create index business.rf_wire_trxn_ben_acct_dt_am_i on business.wire_trxn(upper(rf_benef_acct_nb), trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_ben_acct_dt_am_i noparallel logging;
--drop index business.rf_wire_trxn_deb_dt_am_i;
create index business.rf_wire_trxn_deb_dt_am_i on business.wire_trxn(rf_debit_cd, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_deb_dt_am_i noparallel logging;
--drop index business.rf_wire_trxn_cred_dt_am_i;
create index business.rf_wire_trxn_cred_dt_am_i on business.wire_trxn(rf_credit_cd, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_cred_dt_am_i noparallel logging;

/*���� � �����*/
--drop index business.rf_wire_trxn_dt_am_i;
create index business.rf_wire_trxn_dt_am_i on business.wire_trxn(trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_wire_trxn_dt_am_i noparallel logging;


/* CASH_TRXN */
/* ��� */
--drop index business.rf_cash_trxn_inn_dt_am_i;
create index business.rf_cash_trxn_inn_dt_am_i on business.cash_trxn(rf_cust_inn_nb, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_cash_trxn_inn_dt_am_i noparallel logging;

/*�������*/
--drop index business.rf_cash_trxn_cust_id_dt_am_i;
create index business.rf_cash_trxn_cust_id_dt_am_i on business.cash_trxn(rf_cust_seq_id, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_cash_trxn_cust_id_dt_am_i noparallel logging;
drop index business.rf_cash_trxn_cust_fk_i;

/*���� � �����*/
--drop index business.rf_cash_trxn_dt_am_i;
create index business.rf_cash_trxn_dt_am_i on business.cash_trxn(trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_cash_trxn_dt_am_i noparallel logging;

/* ����� */
--drop index business.rf_cash_trxn_acct_dt_am_i;
create index business.rf_cash_trxn_acct_dt_am_i on business.cash_trxn(upper(rf_acct_nb), trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_cash_trxn_acct_dt_am_i noparallel logging;
--drop index business.rf_cash_trxn_deb_dt_am_i;
create index business.rf_cash_trxn_deb_dt_am_i on business.cash_trxn(rf_debit_cd, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_cash_trxn_deb_dt_am_i noparallel logging;
--drop index business.rf_cash_trxn_cred_dt_am_i;
create index business.rf_cash_trxn_cred_dt_am_i on business.cash_trxn(rf_credit_cd, trxn_exctn_dt, rf_trxn_opok_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_cash_trxn_cred_dt_am_i noparallel logging;


/* BACK_OFFICE_TRXN */
/* ����� */
--drop index business.rf_bo_trxn_deb_dt_am_i;
create index business.rf_bo_trxn_deb_dt_am_i on business.back_office_trxn(rf_debit_cd, exctn_dt, trxn_base_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_bo_trxn_deb_dt_am_i noparallel logging;
--drop index business.rf_bo_trxn_cred_dt_am_i;
create index business.rf_bo_trxn_cred_dt_am_i on business.back_office_trxn(rf_credit_cd, exctn_dt, trxn_base_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_bo_trxn_cred_dt_am_i noparallel logging;
--drop index business.rf_bo_trxn_acct_dt_am_i;
create index business.rf_bo_trxn_acct_dt_am_i on business.back_office_trxn(rf_acct_seq_id, exctn_dt, trxn_base_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_bo_trxn_acct_dt_am_i noparallel logging;
drop index business.rf_bo_trxn_acct_fk_i;

/*�������*/
--drop index business.rf_bo_trxn_cust_dt_am_i;
create index business.rf_bo_trxn_cust_dt_am_i on business.back_office_trxn(rf_cust_seq_id, exctn_dt, trxn_base_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_bo_trxn_cust_dt_am_i noparallel logging;
drop index business.rf_bo_trxn_cust_fk_i;

/*���� � �����*/
--drop index business.rf_bo_trxn_dt_am_i;
create index business.rf_bo_trxn_dt_am_i on business.back_office_trxn(exctn_dt, trxn_base_am) global compress 1 initrans 16 parallel 128 nologging tablespace bus_trxn_idx;
alter index business.rf_bo_trxn_dt_am_i noparallel logging;
