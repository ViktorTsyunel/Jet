/*==============================================================*/
/* Table: RF_IMP_OES                                            */
/*==============================================================*/
create table MANTAS.RF_IMP_OES 
(
   FORM_CD              VARCHAR2(64 CHAR)    not null,
   OES_ID               NUMBER(22)           not null,
   IMP_FILE_ID          NUMBER(22)           not null,
   OES_ORDER_NB         INTEGER              not null,
   constraint RF_IMPOES_PK primary key (FORM_CD, OES_ID)
         using index
   tablespace ALERT_INDEX
)
tablespace ALERT_DATA;

comment on column MANTAS.RF_IMP_OES.FORM_CD is
'��� (�����) ����� ���, � ������� ��������� ����������� ���. ������ � ID ��� �������������� ���������� ���. ���� ������ ������ ''321''.';

comment on column MANTAS.RF_IMP_OES.OES_ID is
'������������� (ID) ������������ ���. 
��� ����� � ����� 321 ������������� ������� RF_OES_321.OES_321_ID';

comment on column MANTAS.RF_IMP_OES.IMP_FILE_ID is
'ID �����, �� �������� ���� ��������� ������ ���';

comment on column MANTAS.RF_IMP_OES.OES_ORDER_NB is
'���������� ����� ������� ��� � ����� (��������� � 1)';

/*==============================================================*/
/* Index: RF_IMPOES_IMPFILE_FK_I                                */
/*==============================================================*/
create index MANTAS.RF_IMPOES_IMPFILE_FK_I on MANTAS.RF_IMP_OES (
   IMP_FILE_ID ASC
)
tablespace ALERT_INDEX;

alter table MANTAS.RF_IMP_OES
   add constraint RF_IMPOES_IMPFILE_FK foreign key (IMP_FILE_ID)
      references MANTAS.RF_IMP_FILE (IMP_FILE_ID);