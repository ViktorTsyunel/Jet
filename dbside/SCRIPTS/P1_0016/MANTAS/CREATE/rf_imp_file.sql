/*==============================================================*/
/* Table: RF_IMP_FILE                                           */
/*==============================================================*/
create table MANTAS.RF_IMP_FILE 
(
   IMP_FILE_ID          NUMBER(22)           not null,
   IMP_TYPE_CD          VARCHAR2(64 CHAR)    not null,
   FILE_NM              VARCHAR2(255 CHAR)   not null,
   DATA_DT              DATE,
   DPT_CD               VARCHAR2(64 CHAR),
   SYS_CD               VARCHAR2(64 CHAR),
   ORDER_NB             VARCHAR2(64 CHAR),
   ARCHIVE_NM           VARCHAR2(255 CHAR),
   ERROR_LOADING_FL     INTEGER              default 0 not null
      constraint RF_IMPFILE__ERROR_LOADING_CC check (ERROR_LOADING_FL in (1,0)),
   constraint RF_IMPFILE_PK primary key (IMP_FILE_ID)
         using index global
   tablespace ALERT_INDEX
)
tablespace ALERT_DATA;

comment on table MANTAS.RF_IMP_FILE is
'������� � �������� �������/��������� ����������� ������.
�������� �� ����� ������� �� ������ ����, ������� �������� ���������, � �. �. � ������� �������������� �����, �� ��������� �������� �������.
������ ������� ����������� ��� ����� �������, � ������� ������ ����������� �� ���������� ������, ��������, �������� ������ ��� �� ������, ����������� �� ������ ������-����������, ��� ������ ������������� �����.
���� ����� ������������������ ������ ��� ������� ����� ����: ����, ��� �������������, ��� �������-���������, ���������� ����� �����.
������ ������� �� ������������, ����, ��� � ������ �������� ����������� ���''��, ���� ������ ���� � �������� ���������� ����� ������������ "������" �����������.';

comment on column MANTAS.RF_IMP_FILE.IMP_FILE_ID is
'���������� ������������� �����, ���������� ����� ������������������ RF_IMP_FILE_SEQ';

comment on column MANTAS.RF_IMP_FILE.IMP_TYPE_CD is
'���������� ��������� ��� ���� , � �������� ��������� ������ ����
';

comment on column MANTAS.RF_IMP_FILE.FILE_NM is
'��� �������������� ����� - ��� ��� ������ ������������ ��� ������ ����� ��� ��������';

comment on column MANTAS.RF_IMP_FILE.DATA_DT is
'����, �� ������� ����������� ������. ����� ������������ �� ����� ����� ��� �� ��� �����������';

comment on column MANTAS.RF_IMP_FILE.DPT_CD is
'��� �������������, � �������� ��������� ����. ����� ������������ �� ����� ����� ��� �� ��� �����������.';

comment on column MANTAS.RF_IMP_FILE.SYS_CD is
'��� �������-��������� �����. ����� ������������ �� ����� ����� ��� �� ��� �����������.';

comment on column MANTAS.RF_IMP_FILE.ORDER_NB is
'���������� ����� ����� ����� ������ �� ��������� ����/�������������/�������-���������. ����� ������������ �� ����� ����� ��� �� ��� �����������.';

comment on column MANTAS.RF_IMP_FILE.ARCHIVE_NM is
'��� ��������� �����, �� �������� ������� ������ ����. ����������� ��� ������, ���������� �� �������.';

comment on column MANTAS.RF_IMP_FILE.ERROR_LOADING_FL is
'���� 1/0 - ��� ������� �������� ������� ����� ���� ��������� - ������ ���� �� ������ ����������� � ������ ���� �������.';


/*==============================================================*/
/* Index: RF_IMPFILE_NM_I                                       */
/*==============================================================*/
create index MANTAS.RF_IMPFILE_NM_I on MANTAS.RF_IMP_FILE (
   IMP_TYPE_CD ASC,
   FILE_NM ASC
)
tablespace ALERT_INDEX;

/*==============================================================*/
/* Index: RF_IMPFILE_DT_SRC_I                                   */
/*==============================================================*/
create index MANTAS.RF_IMPFILE_DT_SRC_I on MANTAS.RF_IMP_FILE (
   IMP_TYPE_CD ASC,
   DATA_DT ASC,
   DPT_CD ASC,
   SYS_CD ASC,
   ORDER_NB ASC
)
tablespace ALERT_INDEX;

alter table MANTAS.RF_IMP_FILE
   add constraint RF_IMPFILE_IMPTP_FK foreign key (IMP_TYPE_CD)
      references MANTAS.RF_IMP_TYPE (IMP_TYPE_CD);