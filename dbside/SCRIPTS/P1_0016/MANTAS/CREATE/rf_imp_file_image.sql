/*==============================================================*/
/* Table: RF_IMP_FILE_IMAGE                                     */
/*==============================================================*/
create table MANTAS.RF_IMP_FILE_IMAGE 
(
   IMP_SEQ_ID           NUMBER(22)           not null,
   LOAD_DT              DATE                 not null,
   CONTENT_DATA         BLOB                 not null,
   constraint RF_IMPIMG_PK primary key (IMP_SEQ_ID)
         using index
   tablespace ALERT_INDEX
)
tablespace ALERT_DATA;

comment on table MANTAS.RF_IMP_FILE_IMAGE is
'������� � �������� ������������� ������. �������� �����, ��������� �������� �������, ������� �������� ��������� (�, ��������, ������� ���������).
����� ������������� ��� �������� - ��� ���� � ����� � ��� ����������� � ���.';

comment on column MANTAS.RF_IMP_FILE_IMAGE.IMP_SEQ_ID is
'����������� ������������� ��������.
���������� ����� ������������������ RF_IMP_HISTORY_SEQ';

comment on column MANTAS.RF_IMP_FILE_IMAGE.LOAD_DT is
'���� � ����� �������� - �������������� �� RF_IMP_HISTORY ��� ��������� ������� ������ �������.';

comment on column MANTAS.RF_IMP_FILE_IMAGE.CONTENT_DATA is
'�������� ���������� �������������� �����';

alter table MANTAS.RF_IMP_FILE_IMAGE
   add constraint RF_IMPIMG_IMPHIST_FK foreign key (IMP_SEQ_ID)
      references MANTAS.RF_IMP_HISTORY (IMP_SEQ_ID);