/*==============================================================*/
/* Alter Table: RF_IMP_HISTORY                                  */
/*==============================================================*/
ALTER TABLE MANTAS.RF_IMP_HISTORY ADD IMP_FILE_ID          NUMBER(22);

comment on column MANTAS.RF_IMP_HISTORY.IMP_FILE_ID is
'ID �������������� ����� � ������������ � �������� RF_IMP_FILE. ����������� ��� ����� �������, "���������" �� ���������� ������.';

/*==============================================================*/
/* Index: RF_IMPHIST_AK1                                        */
/*==============================================================*/
drop  index MANTAS.RF_IMPHIST_AK1;
create unique index MANTAS.RF_IMPHIST_AK1 on MANTAS.RF_IMP_HISTORY (
   nvl2(LAST_LOAD_FLAG,IMP_TYPE_CD,null) ASC,
   nvl2(LAST_LOAD_FLAG,IMP_FILE_ID,null) ASC
)
tablespace ALERT_INDEX;


/*==============================================================*/
/* Index: RF_IMPHIST_IMPFILE_FK_I                               */
/*==============================================================*/
create index MANTAS.RF_IMPHIST_IMPFILE_FK_I on MANTAS.RF_IMP_HISTORY (
   IMP_FILE_ID ASC
)
tablespace ALERT_INDEX;

alter table MANTAS.RF_IMP_HISTORY
   add constraint RF_IMPHIST_IMPFILE_FK foreign key (IMP_FILE_ID)
      references MANTAS.RF_IMP_FILE (IMP_FILE_ID);
