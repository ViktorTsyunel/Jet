drop table MANTAS.RF_IMP_FILE_IMAGE cascade constraints;
drop table MANTAS.RF_IMP_OES cascade constraints;
drop table MANTAS.RF_IMP_FILE cascade constraints;
drop sequence MANTAS.RF_IMP_FILE_SEQ;

create sequence MANTAS.RF_IMP_FILE_SEQ
minvalue 1
maxvalue 9999999999999999999999999999
start with 101
increment by 1
cache 10;

/*==============================================================*/
/* Table: RF_IMP_FILE                                           */
/*==============================================================*/
create table MANTAS.RF_IMP_FILE 
(
   IMP_FILE_ID          NUMBER(22)           not null,
   IMP_TYPE_CD          VARCHAR2(64 CHAR)    not null,
   FILE_NM              VARCHAR2(255 CHAR)   not null,
   DATA_DT              DATE,
   DPT_CD               VARCHAR2(64 CHAR),
   SYS_CD               VARCHAR2(64 CHAR),
   ORDER_NB             VARCHAR2(64 CHAR),
   ARCHIVE_NM           VARCHAR2(255 CHAR),
   ERROR_LOADING_FL     INTEGER              default 0 not null
      constraint RF_IMPFILE__ERROR_LOADING_CC check (ERROR_LOADING_FL in (1,0)),
   constraint RF_IMPFILE_PK primary key (IMP_FILE_ID)
         using index global
   tablespace ALERT_INDEX
)
tablespace ALERT_DATA;

comment on table MANTAS.RF_IMP_FILE is
'������� � �������� �������/��������� ����������� ������.
�������� �� ����� ������� �� ������ ����, ������� �������� ���������, � �. �. � ������� �������������� �����, �� ��������� �������� �������.
������ ������� ����������� ��� ����� �������, � ������� ������ ����������� �� ���������� ������, ��������, �������� ������ ��� �� ������, ����������� �� ������ ������-����������, ��� ������ ������������� �����.
���� ����� ������������������ ������ ��� ������� ����� ����: ����, ��� �������������, ��� �������-���������, ���������� ����� �����.
������ ������� �� ������������, ����, ��� � ������ �������� ����������� ���''��, ���� ������ ���� � �������� ���������� ����� ������������ "������" �����������.';

comment on column MANTAS.RF_IMP_FILE.IMP_FILE_ID is
'���������� ������������� �����, ���������� ����� ������������������ RF_IMP_FILE_SEQ';

comment on column MANTAS.RF_IMP_FILE.IMP_TYPE_CD is
'���������� ��������� ��� ���� , � �������� ��������� ������ ����
';

comment on column MANTAS.RF_IMP_FILE.FILE_NM is
'��� �������������� ����� - ��� ��� ������ ������������ ��� ������ ����� ��� ��������';

comment on column MANTAS.RF_IMP_FILE.DATA_DT is
'����, �� ������� ����������� ������. ����� ������������ �� ����� ����� ��� �� ��� �����������';

comment on column MANTAS.RF_IMP_FILE.DPT_CD is
'��� �������������, � �������� ��������� ����. ����� ������������ �� ����� ����� ��� �� ��� �����������.';

comment on column MANTAS.RF_IMP_FILE.SYS_CD is
'��� �������-��������� �����. ����� ������������ �� ����� ����� ��� �� ��� �����������.';

comment on column MANTAS.RF_IMP_FILE.ORDER_NB is
'���������� ����� ����� ����� ������ �� ��������� ����/�������������/�������-���������. ����� ������������ �� ����� ����� ��� �� ��� �����������.';

comment on column MANTAS.RF_IMP_FILE.ARCHIVE_NM is
'��� ��������� �����, �� �������� ������� ������ ����. ����������� ��� ������, ���������� �� �������.';

comment on column MANTAS.RF_IMP_FILE.ERROR_LOADING_FL is
'���� 1/0 - ��� ������� �������� ������� ����� ���� ��������� - ������ ���� �� ������ ����������� � ������ ���� �������.';

/*==============================================================*/
/* Index: RF_IMPFILE_NM_I                                       */
/*==============================================================*/
create index MANTAS.RF_IMPFILE_NM_I on MANTAS.RF_IMP_FILE (
   IMP_TYPE_CD ASC,
   FILE_NM ASC
)
tablespace ALERT_INDEX;

/*==============================================================*/
/* Index: RF_IMPFILE_DT_SRC_I                                   */
/*==============================================================*/
create index MANTAS.RF_IMPFILE_DT_SRC_I on MANTAS.RF_IMP_FILE (
   IMP_TYPE_CD ASC,
   DATA_DT ASC,
   DPT_CD ASC,
   SYS_CD ASC,
   ORDER_NB ASC
)
tablespace ALERT_INDEX;

alter table MANTAS.RF_IMP_FILE
   add constraint RF_IMPFILE_IMPTP_FK foreign key (IMP_TYPE_CD)
      references MANTAS.RF_IMP_TYPE (IMP_TYPE_CD);

/*==============================================================*/
/* Table: RF_IMP_OES                                            */
/*==============================================================*/
create table MANTAS.RF_IMP_OES 
(
   FORM_CD              VARCHAR2(64 CHAR)    not null,
   OES_ID               NUMBER(22)           not null,
   IMP_FILE_ID          NUMBER(22)           not null,
   OES_ORDER_NB         INTEGER              not null,
   constraint RF_IMPOES_PK primary key (FORM_CD, OES_ID)
         using index
   tablespace ALERT_INDEX
)
tablespace ALERT_DATA;

comment on column MANTAS.RF_IMP_OES.FORM_CD is
'��� (�����) ����� ���, � ������� ��������� ����������� ���. ������ � ID ��� �������������� ���������� ���. ���� ������ ������ ''321''.';

comment on column MANTAS.RF_IMP_OES.OES_ID is
'������������� (ID) ������������ ���. 
��� ����� � ����� 321 ������������� ������� RF_OES_321.OES_321_ID';

comment on column MANTAS.RF_IMP_OES.IMP_FILE_ID is
'ID �����, �� �������� ���� ��������� ������ ���';

comment on column MANTAS.RF_IMP_OES.OES_ORDER_NB is
'���������� ����� ������� ��� � ����� (��������� � 1)';

/*==============================================================*/
/* Index: RF_IMPOES_IMPFILE_FK_I                                */
/*==============================================================*/
create index MANTAS.RF_IMPOES_IMPFILE_FK_I on MANTAS.RF_IMP_OES (
   IMP_FILE_ID ASC
)
tablespace ALERT_INDEX;

alter table MANTAS.RF_IMP_OES
   add constraint RF_IMPOES_IMPFILE_FK foreign key (IMP_FILE_ID)
      references MANTAS.RF_IMP_FILE (IMP_FILE_ID);

/*==============================================================*/
/* Table: RF_IMP_FILE_IMAGE                                     */
/*==============================================================*/
create table MANTAS.RF_IMP_FILE_IMAGE 
(
   IMP_SEQ_ID           NUMBER(22)           not null,
   LOAD_DT              DATE                 not null,
   CONTENT_DATA         BLOB                 not null,
   constraint RF_IMPIMG_PK primary key (IMP_SEQ_ID)
         using index
   tablespace ALERT_INDEX
)
tablespace ALERT_DATA;

comment on table MANTAS.RF_IMP_FILE_IMAGE is
'������� � �������� ������������� ������. �������� �����, ��������� �������� �������, ������� �������� ��������� (�, ��������, ������� ���������).
����� ������������� ��� �������� - ��� ���� � ����� � ��� ����������� � ���.';

comment on column MANTAS.RF_IMP_FILE_IMAGE.IMP_SEQ_ID is
'����������� ������������� ��������.
���������� ����� ������������������ RF_IMP_HISTORY_SEQ';

comment on column MANTAS.RF_IMP_FILE_IMAGE.LOAD_DT is
'���� � ����� �������� - �������������� �� RF_IMP_HISTORY ��� ��������� ������� ������ �������.';

comment on column MANTAS.RF_IMP_FILE_IMAGE.CONTENT_DATA is
'�������� ���������� �������������� �����';

alter table MANTAS.RF_IMP_FILE_IMAGE
   add constraint RF_IMPIMG_IMPHIST_FK foreign key (IMP_SEQ_ID)
      references MANTAS.RF_IMP_HISTORY (IMP_SEQ_ID);
--
-- ��������� � RF_IMP_HISTORY
--
ALTER TABLE MANTAS.RF_IMP_HISTORY ADD IMP_FILE_ID          NUMBER(22);

comment on column MANTAS.RF_IMP_HISTORY.IMP_FILE_ID is
'ID �������������� ����� � ������������ � �������� RF_IMP_FILE. ����������� ��� ����� �������, "���������" �� ���������� ������.';

/*==============================================================*/
/* Index: RF_IMPHIST_AK1                                        */
/*==============================================================*/
drop  index MANTAS.RF_IMPHIST_AK1;
create unique index MANTAS.RF_IMPHIST_AK1 on MANTAS.RF_IMP_HISTORY (
   nvl2(LAST_LOAD_FLAG,IMP_TYPE_CD,null) ASC,
   nvl2(LAST_LOAD_FLAG,IMP_FILE_ID,null) ASC
)
tablespace ALERT_INDEX;


/*==============================================================*/
/* Index: RF_IMPHIST_IMPFILE_FK_I                               */
/*==============================================================*/
create index MANTAS.RF_IMPHIST_IMPFILE_FK_I on MANTAS.RF_IMP_HISTORY (
   IMP_FILE_ID ASC
)
tablespace ALERT_INDEX;

alter table MANTAS.RF_IMP_HISTORY
   add constraint RF_IMPHIST_IMPFILE_FK foreign key (IMP_FILE_ID)
      references MANTAS.RF_IMP_FILE (IMP_FILE_ID);
