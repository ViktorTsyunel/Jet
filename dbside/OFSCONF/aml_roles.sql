create or replace force view ofsconf.aml_roles as
select v_group_code rolecode,
       v_group_name rolename
  from ofsconf.cssms_group_mast
 where upper(v_group_code) in ('AMANALYST1GRP','AMSUPVISRGRP', 
                               'AMANALYTICGRP', 'AMMAINANLTGRP', 'AMMETHODLGGRP', 
                               'AMMAINMETHGRP', 'AMRESPNSBLGRP', 'AMAUDITORGRP')
    or upper(v_group_code) like 'RF%'
    or upper(v_group_code) like 'ST%'
	or upper(v_group_code) like 'BL%'
	or upper(v_group_code) like 'CB%';