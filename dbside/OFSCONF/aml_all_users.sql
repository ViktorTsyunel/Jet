create or replace force view ofsconf.aml_all_users as
select u.v_usr_id as userguid,
       u.v_usr_id as userlogin,
       u.v_usr_name fullname,
       r.role_list roles,
       decode(u.f_usr_enabled, 'Y', 0, 1) active,
       to_char(trunc(to_date(u.d_lastlogin_date, 'MM/DD/YYYY HH12:MI:SS AM')), 'YYYYMMDD') lastaccessdate
  from ofsconf.cssms_usr_profile u
  left join (select v_usr_id, listagg(v_group_code, '|') within group (order by v_usr_id) role_list
               from ofsconf.cssms_usr_group_map
              group by v_usr_id
            ) r on r.v_usr_id = u.v_usr_id; 