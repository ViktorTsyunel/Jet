grant execute on mantas.rf_pkg_scnro to ofsconf;
grant execute on mantas.rf_pkg_audit to ofsconf;

create or replace package ofsconf.rf_pkg_sudir
is
  /* ����� ��� ���������� AML � ���-��
   * �������� ������ ����������, ����������, ��������� � ������������� �������������.
   * ����������� � ������� � ���� ������.
   */

  /* ����� ���������� ������ ������������ */
  procedure create_user (
    p_usr_id      in cssms_usr_profile.v_usr_id%type,
    p_usr_name    in cssms_usr_profile.v_usr_name%type,
    p_roles       in varchar2,
    p_email       in cssms_usr_profile.v_email%type default null,
    p_mobile_nbr  in cssms_usr_profile.v_mobile_nbr%type default null,
    p_usr_address in cssms_usr_profile.v_usr_address%type default null,
    p_rescode     out number,
    p_message     out varchar2 );

  /* ����� ���������� ���������� � ������������ */
  procedure update_user (
    p_usr_id      in cssms_usr_profile.v_usr_id%type,
    p_usr_name    in cssms_usr_profile.v_usr_name%type,
    p_roles       in varchar2,
    p_email       in cssms_usr_profile.v_email%type default null,
    p_mobile_nbr  in cssms_usr_profile.v_mobile_nbr%type default null,
    p_usr_address in cssms_usr_profile.v_usr_address%type default null,
    p_rescode     out number, 
    p_message     out varchar2 );
  
  /* ����� ���������� ������������ */
  procedure lock_user (
    p_usr_id   in cssms_usr_profile.v_usr_id%type,
    p_rescode  out number,
    p_message  out varchar2 );
  
  /* ����� ������������� ������������ */
  procedure unlock_user (
    p_usr_id   in cssms_usr_profile.v_usr_id%type,
    p_rescode  out number,
    p_message  out varchar2 );

end;
/

create or replace package body ofsconf.rf_pkg_sudir
is
  c_date_format constant varchar2(32) := 'dd/mm/yyyy hh:mi:ss AM';
  
  /* ����� ��� ���������� ����� ������������.
   * �������������� � ������������ ��������� ��� ����, � ����� ����������� �� �������� p_roles.
   *
   * ��������� ������
   *   p_usr_id - ����� ������������
   *   p_roles - ������ ����� ����� �������
   */
  procedure update_user_roles (
    p_usr_id in cssms_usr_profile.v_usr_id%type,
    p_roles  in varchar2,
    p_delete in boolean default false)
  is
  begin
    /* ������� ������ ���� */
    delete from cssms_usr_group_map
     where upper(v_usr_id) = upper(p_usr_id);
    delete from cssms_usr_group_map_unauth
     where upper(v_usr_id) = upper(p_usr_id);
    /* ��������� ����� ���� */
    insert all
      into cssms_usr_group_map (v_usr_id, v_group_code) values (v_usr_id, v_group_code)
      into cssms_usr_group_map_unauth (v_usr_id, v_group_code, recordstat) values (v_usr_id, v_group_code, recordstat)
    select upper(p_usr_id) v_usr_id, column_value v_group_code, 'A' recordstat
      from table(mantas.rf_pkg_scnro.list_to_tab(p_roles, '|')); 
  end;
  
  /* ����� ��� ���������� ���� � ������ ������.
   *
   * ��������� ������
   *   p_sqlcode - ��� ������ oracle
   *   p_sqlmess - ����� ������ oracle
   *   p_rescode - ��� ������ ��� ������
   *   p_message - ����� ������ ��� ������
   */
  procedure get_error_code(
    p_sqlcode in number, 
    p_sqlmess in varchar2, 
    p_rescode out number, 
    p_message out varchar2 )
  is
  begin
    case

      when p_sqlcode = 1031 then 
        begin
          p_rescode := 3;
          p_message := '� ������� ���������� ���������� ������������ ����������.';
        end;

      when p_sqlcode in (100, 1403) then 
        begin
          p_rescode := 4;
          p_message := '�� ������� ������� ������';
        end;

      when p_sqlcode = 1 then 
        begin
          if instr(upper(p_sqlmess), 'PK_CSSMS_USR_PROFILE') > 0 then
            p_rescode := 5;
            p_message := '��� ������� �������� ����� ������� ������ ��������� �������� � ����� ���������� ������� ������� � ����� �� ���������������.';
          else
            p_rescode := 0;
            p_message := p_sqlcode||': '||p_sqlmess;
          end if;
        end;

      when p_sqlcode = 932 then 
        begin
          p_rescode := 6;
          p_message := '�������� ������ ��������.';
        end;

      when p_sqlcode = 20001 then
        begin
          p_rescode := 7;
          p_message := replace(p_sqlmess, 'ORA-20001: ', '');
        end;

      else 
        begin
          p_rescode := 0;
          p_message := p_sqlcode||': '||p_sqlmess;
        end;
    end case;
  end;

  /* ������ ��� ������������ json �� ������� ����������
   *   p_action - ��� ��������
   *   p_usr_id - ����� ������������
   *   p_usr_name - ��� ������������
   *   p_roles - ������ ����� ����� �������
   *   p_email - email ����� ������������
   *   p_mobile_nbr - ��������� ����� ������������
   *   p_usr_address - ����� ������������
   *
   * ��������� ������
   *   json - json ������ �� ������� ����������
   */
  function create_json (
    p_usr_id      in cssms_usr_profile.v_usr_id%type,
    p_usr_name    in cssms_usr_profile.v_usr_name%type default null,
    p_roles       in varchar2 default null,
    p_email       in cssms_usr_profile.v_email%type default null,
    p_mobile_nbr  in cssms_usr_profile.v_mobile_nbr%type default null,
    p_usr_address in cssms_usr_profile.v_usr_address%type default null
  ) return rf_json
  is
    v_json rf_json;
  begin
    v_json := rf_json();
    if p_usr_id is not null then
      v_json.put('usr_id', p_usr_id);
    end if;
    if p_usr_name is not null then
      v_json.put('usr_name', p_usr_name);
    end if;  
    if p_roles is not null then
      v_json.put('roles', p_roles);
    end if;
    if p_email is not null then
      v_json.put('email', p_email);
    end if;
    if p_mobile_nbr is not null then
      v_json.put('mobile_nbr', p_mobile_nbr);
    end if;
    if p_usr_address is not null then 
      v_json.put('usr_address', p_usr_address);
    end if;
    
    return v_json;
  end;
  
  /* ����� ���������� ������ � �����
   *   p_event_type_cd - ��� �������
   *   p_json - json ������
   *
   * ��������� ������
   *   number - id ������ � ������� ������
   */
  function add_audit_event (
    p_event_type_cd in varchar2,
    p_json          in rf_json
  ) return number
  is
    v_type_cd  varchar2(32 char);
  begin
    /* ��������� ���� ������� - SUDIR_CREATE_USER, SUDIR_UPDATE_USER,  SUDIR_LOCK_USER, SUDIR_UNLOCK_USER */
    v_type_cd := 'SUDIR_'||upper(p_event_type_cd);
    return mantas.rf_pkg_audit.record_event (
             par_user_login     => sys_context('USERENV', 'SESSION_USER'),
             par_ip_address     => sys_context('USERENV', 'IP_ADDRESS'),
             par_event_type_cd  => v_type_cd,
             par_event_params   => p_json,
             par_gui_flag       => 0,
             par_event_descr_tx => null
           );
  end;
  
  /* ����� ���������� ������ ������������
   *   
   * ��������� ������
   *   p_usr_id - ����� ������������
   *   p_usr_name - ��� ������������
   *   p_roles - ������ ����� ����� �������
   *   p_email - email ����� ������������
   *   p_mobile_nbr - ��������� ����� ������������
   *   p_usr_address - ����� ������������
   *   p_rescode - ��� ������
   *   p_message - ����� ������
   * ��������� ������
   *   number - ��������� ���������� ������: 0-������, 1-�������
   */
  procedure create_user (
    p_usr_id      in cssms_usr_profile.v_usr_id%type,
    p_usr_name    in cssms_usr_profile.v_usr_name%type,
    p_roles       in varchar2,
    p_email       in cssms_usr_profile.v_email%type default null,
    p_mobile_nbr  in cssms_usr_profile.v_mobile_nbr%type default null,
    p_usr_address in cssms_usr_profile.v_usr_address%type default null,
    p_rescode     out number,
    p_message     out varchar2 )
  is
    v_sqlcode  number;
    v_audit_id number;
  begin
    --����� �������
    v_audit_id := add_audit_event(
                    p_event_type_cd => 'create_user',
                    p_json => create_json(
                                p_usr_id      => p_usr_id,
                                p_usr_name    => p_usr_name,
                                p_roles       => p_roles,
                                p_email       => p_email,
                                p_mobile_nbr  => p_mobile_nbr,
                                p_usr_address => p_usr_address
                              )  
                  );
  
    if p_usr_id is null or p_usr_name is null or p_roles is null then
      raise_application_error(
        -20001, 
        rtrim('����������� ����������� �������:'||
          case when p_usr_id is null then ' �����,' end||
          case when p_usr_name is null then ' ���,' end||
          case when p_roles is null then ' ����,' end,
        ',')||'.'
      );
    end if;
    
    /* ��������� ������ ������������ */
    insert into cssms_usr_profile(
      v_usr_id, v_usr_name, v_email, v_mobile_nbr, v_usr_address, v_usr_desg,
      f_usr_enabled, f_usr_delete, f_authorize_stat, v_created_date, v_time_diff, v_maker_id, n_nbr_invalid_logins
    ) values (
      upper(p_usr_id), p_usr_name, p_email, p_mobile_nbr, p_usr_address, null,
      'Y', 'N', 'A', to_char(sysdate, c_date_format), 0, 'SYSADMN', 0
    );
    /* ��������� ����� insert from select, �.�. ����� ��������� � ������� cssms_usr_profile ����������� ��� default */
    insert into cssms_usr_profile_unauth(
      v_usr_id, v_usr_name, v_email, v_mobile_nbr, v_usr_address, v_usr_desg,
      f_usr_enabled, f_usr_delete, f_authorize_stat, f_usr_login_stat, f_forced_pwd_change,
      f_login_holidays, v_profile_code, v_created_by, v_created_date, v_time_diff, v_maker_id, n_nbr_invalid_logins ) 
    select v_usr_id, v_usr_name, v_email, v_mobile_nbr, v_usr_address, v_usr_desg,
           f_usr_enabled, f_usr_delete, f_authorize_stat, f_usr_login_stat, f_forced_pwd_change,
           f_login_holidays, v_profile_code, v_created_by, v_created_date, v_time_diff, v_maker_id, n_nbr_invalid_logins
      from cssms_usr_profile where upper(v_usr_id) = upper(p_usr_id);
    
    /* ��������� ������ ����� */
    update_user_roles(p_usr_id, p_roles);
    
    commit;
    
    exception
      when others then
        get_error_code(abs(sqlcode()), sqlerrm(), p_rescode, p_message);
        rollback;
        
        --����� ������
        mantas.rf_pkg_audit.set_event_result(
          par_audit_seq_id => v_audit_id,
          par_err_type_cd  => 'T',
          par_message_tx   => p_message
        );
  end;

  /* ����� ���������� ���������� � ������������
   *   
   * ��������� ������
   *   p_usr_id - ����� ������������
   *   p_usr_name - ��� ������������
   *   p_roles - ������ ����� ����� �������
   *   p_email - email ����� ������������
   *   p_mobile_nbr - ��������� ����� ������������
   *   p_usr_address - ����� ������������
   *   p_rescode - ��� ������
   *   p_message - ����� ������
   * ��������� ������
   *   number - ��������� ���������� ������: 0-������, 1-�������
   */
  procedure update_user (
    p_usr_id      in cssms_usr_profile.v_usr_id%type,
    p_usr_name    in cssms_usr_profile.v_usr_name%type,
    p_roles       in varchar2,
    p_email       in cssms_usr_profile.v_email%type default null,
    p_mobile_nbr  in cssms_usr_profile.v_mobile_nbr%type default null,
    p_usr_address in cssms_usr_profile.v_usr_address%type default null,
    p_rescode     out number, 
    p_message     out varchar2 )
  is
    v_usr_id cssms_usr_profile.v_usr_id%type;
    v_audit_id number;
  begin
    --����� �������
    v_audit_id := add_audit_event(
                    p_event_type_cd => 'update_user',
                    p_json => create_json(
                                p_usr_id      => p_usr_id,
                                p_usr_name    => p_usr_name,
                                p_roles       => p_roles,
                                p_email       => p_email,
                                p_mobile_nbr  => p_mobile_nbr,
                                p_usr_address => p_usr_address
                              )  
                  );

    if p_usr_id is null or p_usr_name is null or p_roles is null then
      raise_application_error(
        -20001, 
        rtrim('����������� ����������� �������:'||
          case when p_usr_id is null then ' �����,' end||
          case when p_usr_name is null then ' ���,' end||
          case when p_roles is null then ' ����,' end,
        ',')||'.'
      );
    end if;
    
    /* ���������, ���� �� ����� ������������ */
    select v_usr_id into v_usr_id
      from cssms_usr_profile
     where upper(v_usr_id) = upper(p_usr_id);
  
    /* ��������� ������ � cssms_usr_profile */
    update cssms_usr_profile
       set v_usr_name = nvl(p_usr_name, v_usr_name),
           v_email = nvl(p_email, v_email),
           v_mobile_nbr = nvl(p_mobile_nbr, v_mobile_nbr),
           v_usr_address = nvl(p_usr_address, v_usr_address),
           v_last_modified_by = 'SYSADMN', -- ???
           v_last_modified_date = to_char(sysdate, c_date_format)
     where upper(v_usr_id) = upper(p_usr_id);
     
    /* ��������� ������ � cssms_usr_profile_unauth */
    update cssms_usr_profile_unauth
       set v_usr_name = nvl(p_usr_name, v_usr_name),
           v_email = nvl(p_email, v_email),
           v_mobile_nbr = nvl(p_mobile_nbr, v_mobile_nbr),
           v_usr_address = nvl(p_usr_address, v_usr_address),
           v_last_modified_by = 'SYSADMN', -- ???
           v_last_modified_date = to_char(sysdate, c_date_format)
     where upper(v_usr_id) = upper(p_usr_id);

    /* ��������� ������ ����� */
    update_user_roles(p_usr_id, p_roles);
    
    commit;
    
    exception
      when others then
        get_error_code(abs(sqlcode()), sqlerrm(), p_rescode, p_message);
        rollback;

        --����� ������
        mantas.rf_pkg_audit.set_event_result(
          par_audit_seq_id => v_audit_id,
          par_err_type_cd  => 'T',
          par_message_tx   => p_message
        );
  end;
  
  
  /* ����� ���������� ������������
   *   
   * ��������� ������
   *   p_usr_id - ����� ������������
   *   p_rescode - ��� ������
   *   p_message - ����� ������
   * ��������� ������
   *   number - ��������� ���������� ������: 0-������, 1-�������
   */
  procedure lock_user (
    p_usr_id   in cssms_usr_profile.v_usr_id%type,
    p_rescode  out number,
    p_message  out varchar2 )
  is
    v_usr_id cssms_usr_profile.v_usr_id%type;
    v_audit_id number;
  begin
    --����� �������
    v_audit_id := add_audit_event(
                    p_event_type_cd => 'lock_user',
                    p_json => create_json(
                                p_usr_id      => p_usr_id
                              )  
                  );

    /* ���������, ���� �� ����� ������������ */
    select v_usr_id into v_usr_id
      from cssms_usr_profile
     where upper(v_usr_id) = upper(p_usr_id);
  
    /* ��������� ������ � cssms_usr_profile */
    update cssms_usr_profile
       set f_usr_enabled = 'N',
           d_last_disabled_dte = to_char(sysdate, c_date_format),
           v_last_modified_by = 'SYSADMN',
           v_last_modified_date = to_char(sysdate, c_date_format)
     where upper(v_usr_id) = upper(p_usr_id);
     
    /* ��������� ������ � cssms_usr_profile_unauth */
    update cssms_usr_profile_unauth
       set f_usr_enabled = 'N',
           d_last_disabled_dte = to_char(sysdate, c_date_format),
           v_last_modified_by = 'SYSADMN',
           v_last_modified_date = to_char(sysdate, c_date_format)
     where upper(v_usr_id) = upper(p_usr_id);

    commit;
    
    exception
      when others then
        get_error_code(abs(sqlcode()), sqlerrm(), p_rescode, p_message);
        rollback;

        --����� ������
        mantas.rf_pkg_audit.set_event_result(
          par_audit_seq_id => v_audit_id,
          par_err_type_cd  => 'T',
          par_message_tx   => p_message
        );
  end;
  
  
  /* ����� ������������� ������������
   *   
   * ��������� ������
   *   p_usr_id - ����� ������������
   *   p_rescode - ��� ������
   *   p_message - ����� ������
   * ��������� ������
   *   number - ��������� ���������� ������: 0-������, 1-�������
   */
  procedure unlock_user (
    p_usr_id   in cssms_usr_profile.v_usr_id%type,
    p_rescode  out number,
    p_message  out varchar2 )
  is
    v_usr_id cssms_usr_profile.v_usr_id%type;
    v_audit_id number;
  begin
    --����� �������
    v_audit_id := add_audit_event(
                    p_event_type_cd => 'unlock_user',
                    p_json => create_json(
                                p_usr_id      => p_usr_id
                              )  
                  );

    /* ���������, ���� �� ����� ������������ */
    select v_usr_id into v_usr_id
      from cssms_usr_profile
     where upper(v_usr_id) = upper(p_usr_id);
  
    /* ��������� ������ � cssms_usr_profile */
    update cssms_usr_profile
       set f_usr_enabled = 'Y',
           d_last_enabled_dte = to_char(sysdate, c_date_format),
           v_last_modified_by = 'SYSADMN',
           v_last_modified_date = to_char(sysdate, c_date_format)
     where upper(v_usr_id) = upper(p_usr_id);
     
    /* ��������� ������ � cssms_usr_profile_unauth */
    update cssms_usr_profile_unauth
       set f_usr_enabled = 'Y',
           d_last_enabled_dte = to_char(sysdate, c_date_format),
           v_last_modified_by = 'SYSADMN',
           v_last_modified_date = to_char(sysdate, c_date_format)
     where upper(v_usr_id) = upper(p_usr_id);

    commit;
    
    exception
      when others then
        get_error_code(abs(sqlcode()), sqlerrm(), p_rescode, p_message);
        rollback;
        
        --����� ������
        mantas.rf_pkg_audit.set_event_result(
          par_audit_seq_id => v_audit_id,
          par_err_type_cd  => 'T',
          par_message_tx   => p_message
        );
  end;

end;
/