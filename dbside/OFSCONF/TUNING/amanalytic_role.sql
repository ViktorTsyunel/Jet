-- ������������ ����
MERGE INTO OFSCONF.CSSMS_ROLE_MAST t USING
(SELECT 'AMANALYTIC' as V_ROLE_CODE,
        '�������� (����)' as V_ROLE_NAME,
        '�������� (����)' as V_ROLE_DESC,
        'USER' as V_ROLE_TYPE
   FROM dual) v
 ON (v.V_ROLE_CODE = t.V_ROLE_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_ROLE_CODE, V_ROLE_NAME, V_ROLE_DESC, V_ROLE_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, ROLE_KEY, ROLE_DESC_KEY)
  VALUES (v.V_ROLE_CODE, v.V_ROLE_NAME, v.V_ROLE_DESC, v.V_ROLE_TYPE, null, sysdate, null, sysdate, null, null)
WHEN MATCHED THEN
  UPDATE 
     SET V_ROLE_NAME = v.V_ROLE_NAME,
         V_ROLE_DESC = v.V_ROLE_DESC,
         V_ROLE_TYPE = v.V_ROLE_TYPE,
         D_LAST_MODIFIED_DATE = sysdate;

-- ������������ ������         
MERGE INTO OFSCONF.CSSMS_GROUP_MAST t USING
(SELECT 'AMANALYTICGRP' as V_GROUP_CODE,
        '�������� (����)' as V_GROUP_NAME,
        '�������� (����)' as V_GROUP_DESC,
        'USER' as V_GROUP_TYPE,
        992 as N_PRECEDENCE
   FROM dual) v
 ON (v.V_GROUP_CODE = t.V_GROUP_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_GROUP_CODE, V_GROUP_NAME, V_GROUP_DESC, V_GROUP_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, GROUP_KEY, GROUP_DESC_KEY, N_PRECEDENCE)
  VALUES (v.V_GROUP_CODE, v.V_GROUP_NAME, v.V_GROUP_DESC, v.V_GROUP_TYPE, null, sysdate, null, sysdate, null, null, v.N_PRECEDENCE)
WHEN MATCHED THEN
  UPDATE 
     SET V_GROUP_NAME = v.V_GROUP_NAME,
         V_GROUP_DESC = v.V_GROUP_DESC,
         V_GROUP_TYPE = v.V_GROUP_TYPE,
         D_LAST_MODIFIED_DATE = sysdate,
         N_PRECEDENCE = v.N_PRECEDENCE;

-- ����������� ���� � ������
DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH WHERE V_GROUP_CODE = 'AMANALYTICGRP' and V_ROLE_CODE = 'AMANALYTIC';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH(V_GROUP_CODE, V_ROLE_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMANALYTICGRP', 'AMANALYTIC', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP WHERE V_GROUP_CODE = 'AMANALYTICGRP' and V_ROLE_CODE = 'AMANALYTIC';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP(V_GROUP_CODE, V_ROLE_CODE)
       VALUES('AMANALYTICGRP', 'AMANALYTIC');

-- ����������� ������� � ����
DELETE FROM OFSCONF.CSSMS_ROLE_FUNCTION_MAP WHERE v_role_code in ('AMANALYTIC');
INSERT INTO OFSCONF.CSSMS_ROLE_FUNCTION_MAP(v_role_code, v_function_code)
(
-- �� ������ ������: ������ � ������ �������� ���������� �����������
SELECT 'AMANALYTIC', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'AMANALYTIC', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'RFALERTS_R' /*������ � ������ �������*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFASGRL_W' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFASGRP_W' /*���������� ��������� �������������*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFASGLM_W' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������"*/ FROM dual UNION ALL 
SELECT 'AMANALYTIC', 'RFOESRL_R' /*���������� ��� "������� ��������"*/ FROM dual UNION ALL   
--SELECT 'AMANALYTIC', 'RFIMPTP_R' /*���������� "���� �������*/ FROM dual UNION ALL    
--SELECT 'AMANALYTIC', 'RFIMP1001R' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMANALYTIC', 'RFIMP1002R' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMANALYTIC', 'RFIMP1003R' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
--SELECT 'AMANALYTIC', 'RFIMP1004R' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
--SELECT 'AMANALYTIC', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
--SELECT 'AMANALYTIC', 'RFWLORG_R' /*���������� "������� �����������"*/ FROM dual UNION ALL 
SELECT 'AMANALYTIC', 'RFTRXNS_R' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMANALYTIC', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
SELECT 'AMANALYTIC', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL 
SELECT 'AMANALYTIC', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
SELECT 'AMANALYTIC', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYTIC', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'RFREP_1007' /*�����: �������� ������ � ��������� ����*/ FROM dual UNION ALL
SELECT 'AMANALYTIC', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
SELECT 'AMANALYTIC', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual 
);

-- ����������� ������ � �������
DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH 
 WHERE V_GROUP_CODE = 'AMANALYTICGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMANALYTICGRP', 'AMINFO', 'AMSEG', 'A', 'A', 'SYSADMN');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMANALYTICGRP', 'CMINFO', 'CMSEG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP 
 WHERE V_GROUP_CODE = 'AMANALYTICGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMANALYTICGRP', 'AMINFO', 'AMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMANALYTICGRP', 'CMINFO', 'CMSEG');

commit;
