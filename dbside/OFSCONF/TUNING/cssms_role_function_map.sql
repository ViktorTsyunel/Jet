-- ��������� �������������� ������� - ��. ���������������� ������
DELETE FROM ofsconf.cssms_role_function_map WHERE v_role_code in ('AMANALYST1', 'AMANALYST2', 'AMANALYST3', 'AMSUPVISR');

INSERT INTO ofsconf.cssms_role_function_map(v_role_code, v_function_code)
(
SELECT 'AMANALYST1', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'AMANALYST1', 'RFALERTS_W' /*������ � ������ �������*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMANALYST1', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������*/ FROM dual UNION ALL 
SELECT 'AMANALYST1', 'RFOESRL_R' /*���������� ��� "������� ��������*/ FROM dual UNION ALL 
SELECT 'AMANALYST1', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL  
SELECT 'AMANALYST1', 'RFIMP1001R' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMANALYST1', 'RFIMP1002R' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMANALYST1', 'RFIMP1003W' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
SELECT 'AMANALYST1', 'RFIMP1004W' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
--SELECT 'AMANALYST1', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
--SELECT 'AMANALYST1', 'RFWLORG_R' /*���������� "������� �����������"*/ FROM dual UNION ALL 
SELECT 'AMANALYST1', 'RFTRXNS_W' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMANALYST1', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
--SELECT 'AMANALYST1', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMANALYST1', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST1', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFALERTS_W' /*������ � ������ �������*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMANALYST2', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������*/ FROM dual UNION ALL 
SELECT 'AMANALYST2', 'RFOESRL_R' /*���������� ��� "������� ��������*/ FROM dual UNION ALL 
SELECT 'AMANALYST2', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL  
SELECT 'AMANALYST2', 'RFIMP1001R' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMANALYST2', 'RFIMP1002R' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMANALYST2', 'RFIMP1003W' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
SELECT 'AMANALYST2', 'RFIMP1004W' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
--SELECT 'AMANALYST2', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
--SELECT 'AMANALYST2', 'RFWLORG_R' /*���������� "������� �����������"*/ FROM dual UNION ALL 
--SELECT 'AMANALYST2', 'RFTRXNS_R' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMANALYST2', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
--SELECT 'AMANALYST2', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMANALYST2', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST2', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFALERTS_W' /*������ � ������ �������*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMANALYST3', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������*/ FROM dual UNION ALL 
SELECT 'AMANALYST3', 'RFOESRL_R' /*���������� ��� "������� ��������*/ FROM dual UNION ALL 
SELECT 'AMANALYST3', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL  
SELECT 'AMANALYST3', 'RFIMP1001R' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMANALYST3', 'RFIMP1002R' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMANALYST3', 'RFIMP1003W' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
SELECT 'AMANALYST3', 'RFIMP1004W' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
--SELECT 'AMANALYST3', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
--SELECT 'AMANALYST3', 'RFWLORG_R' /*���������� "������� �����������"*/ FROM dual UNION ALL 
--SELECT 'AMANALYST3', 'RFTRXNS_R' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMANALYST3', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
--SELECT 'AMANALYST3', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL UNION ALL
--SELECT 'AMANALYST3', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMANALYST3', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMANALYST3', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFALERTS_W' /*������ � ������ �������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFASGRL_W' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFASGRP_W' /*���������� ��������� �������������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFASGLM_W' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������*/ FROM dual UNION ALL 
SELECT 'AMSUPVISR', 'RFOESRL_R' /*���������� ��� "������� ��������*/ FROM dual UNION ALL 
SELECT 'AMSUPVISR', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL 
SELECT 'AMSUPVISR', 'RFIMP1001W' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMSUPVISR', 'RFIMP1002W' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMSUPVISR', 'RFIMP1003W' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
SELECT 'AMSUPVISR', 'RFIMP1004W' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
SELECT 'AMSUPVISR', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
SELECT 'AMSUPVISR', 'RFWLORG_R' /*���������� "������� �����������"*/ FROM dual UNION ALL 
SELECT 'AMSUPVISR', 'RFTRXNS_W' /*����� ��������*/ FROM dual UNION ALL 
SELECT 'AMSUPVISR', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMSUPVISR', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMSUPVISR', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual 
SELECT 'AMSUPVISR', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMSUPVISR', 'RFREP_1007' /*�����: �������� ������ � ��������� ����*/ FROM dual 
);

commit;

