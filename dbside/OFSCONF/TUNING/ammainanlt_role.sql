-- ������������ ����
MERGE INTO OFSCONF.CSSMS_ROLE_MAST t USING
(SELECT 'AMMAINANLT' as V_ROLE_CODE,
        '������� �������� (����)' as V_ROLE_NAME,
        '������� �������� (����)' as V_ROLE_DESC,
        'USER' as V_ROLE_TYPE
   FROM dual) v
 ON (v.V_ROLE_CODE = t.V_ROLE_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_ROLE_CODE, V_ROLE_NAME, V_ROLE_DESC, V_ROLE_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, ROLE_KEY, ROLE_DESC_KEY)
  VALUES (v.V_ROLE_CODE, v.V_ROLE_NAME, v.V_ROLE_DESC, v.V_ROLE_TYPE, null, sysdate, null, sysdate, null, null)
WHEN MATCHED THEN
  UPDATE 
     SET V_ROLE_NAME = v.V_ROLE_NAME,
         V_ROLE_DESC = v.V_ROLE_DESC,
         V_ROLE_TYPE = v.V_ROLE_TYPE,
         D_LAST_MODIFIED_DATE = sysdate;

-- ������������ ������         
MERGE INTO OFSCONF.CSSMS_GROUP_MAST t USING
(SELECT 'AMMAINANLTGRP' as V_GROUP_CODE,
        '������� �������� (����)' as V_GROUP_NAME,
        '������� �������� (����)' as V_GROUP_DESC,
        'USER' as V_GROUP_TYPE,
        993 as N_PRECEDENCE
   FROM dual) v
 ON (v.V_GROUP_CODE = t.V_GROUP_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_GROUP_CODE, V_GROUP_NAME, V_GROUP_DESC, V_GROUP_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, GROUP_KEY, GROUP_DESC_KEY, N_PRECEDENCE)
  VALUES (v.V_GROUP_CODE, v.V_GROUP_NAME, v.V_GROUP_DESC, v.V_GROUP_TYPE, null, sysdate, null, sysdate, null, null, v.N_PRECEDENCE)
WHEN MATCHED THEN
  UPDATE 
     SET V_GROUP_NAME = v.V_GROUP_NAME,
         V_GROUP_DESC = v.V_GROUP_DESC,
         V_GROUP_TYPE = v.V_GROUP_TYPE,
         D_LAST_MODIFIED_DATE = sysdate,
         N_PRECEDENCE = v.N_PRECEDENCE;

-- ����������� ���� � ������
DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH WHERE V_GROUP_CODE = 'AMMAINANLTGRP' and V_ROLE_CODE = 'AMMAINANLT';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH(V_GROUP_CODE, V_ROLE_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMAINANLTGRP', 'AMMAINANLT', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP WHERE V_GROUP_CODE = 'AMMAINANLTGRP' and V_ROLE_CODE = 'AMMAINANLT';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP(V_GROUP_CODE, V_ROLE_CODE)
       VALUES('AMMAINANLTGRP', 'AMMAINANLT');

-- ����������� ������� � ����
DELETE FROM OFSCONF.CSSMS_ROLE_FUNCTION_MAP WHERE v_role_code in ('AMMAINANLT');
INSERT INTO OFSCONF.CSSMS_ROLE_FUNCTION_MAP(v_role_code, v_function_code)
(
-- �� ������ ������: ������ � ������ �������� ���������� �����������
SELECT 'AMMAINANLT', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'AMMAINANLT', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'RFALERTS_R' /*������ � ������ �������*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFASGRL_W' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFASGRP_W' /*���������� ��������� �������������*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFASGLM_W' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������"*/ FROM dual UNION ALL 
SELECT 'AMMAINANLT', 'RFOESRL_R' /*���������� ��� "������� ��������"*/ FROM dual UNION ALL  
--SELECT 'AMMAINANLT', 'RFIMPTP_R' /*���������� "���� �������*/ FROM dual UNION ALL    
--SELECT 'AMMAINANLT', 'RFIMP1001R' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMMAINANLT', 'RFIMP1002R' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMMAINANLT', 'RFIMP1003R' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
--SELECT 'AMMAINANLT', 'RFIMP1004R' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
--SELECT 'AMMAINANLT', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
--SELECT 'AMMAINANLT', 'RFWLORG_W' /*���������� "������� �����������"*/ FROM dual UNION ALL 
SELECT 'AMMAINANLT', 'RFTRXNS_R' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMMAINANLT', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
SELECT 'AMMAINANLT', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL 
SELECT 'AMMAINANLT', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
SELECT 'AMMAINANLT', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMMAINANLT', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'RFREP_1007' /*�����: �������� ������ � ��������� ����*/ FROM dual UNION ALL
SELECT 'AMMAINANLT', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
SELECT 'AMMAINANLT', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual  
);

-- ����������� ������ � �������
DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH 
 WHERE V_GROUP_CODE = 'AMMAINANLTGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMAINANLTGRP', 'AMINFO', 'AMSEG', 'A', 'A', 'SYSADMN');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMAINANLTGRP', 'CMINFO', 'CMSEG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP 
 WHERE V_GROUP_CODE = 'AMMAINANLTGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMMAINANLTGRP', 'AMINFO', 'AMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMMAINANLTGRP', 'CMINFO', 'CMSEG');

commit;
