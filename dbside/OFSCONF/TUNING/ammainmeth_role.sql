-- ������������ ����
MERGE INTO OFSCONF.CSSMS_ROLE_MAST t USING
(SELECT 'AMMAINMETH' as V_ROLE_CODE,
        '������� ��������� (����)' as V_ROLE_NAME,
        '������� ��������� (����)' as V_ROLE_DESC,
        'USER' as V_ROLE_TYPE
   FROM dual) v
 ON (v.V_ROLE_CODE = t.V_ROLE_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_ROLE_CODE, V_ROLE_NAME, V_ROLE_DESC, V_ROLE_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, ROLE_KEY, ROLE_DESC_KEY)
  VALUES (v.V_ROLE_CODE, v.V_ROLE_NAME, v.V_ROLE_DESC, v.V_ROLE_TYPE, null, sysdate, null, sysdate, null, null)
WHEN MATCHED THEN
  UPDATE 
     SET V_ROLE_NAME = v.V_ROLE_NAME,
         V_ROLE_DESC = v.V_ROLE_DESC,
         V_ROLE_TYPE = v.V_ROLE_TYPE,
         D_LAST_MODIFIED_DATE = sysdate;

-- ������������ ������         
MERGE INTO OFSCONF.CSSMS_GROUP_MAST t USING
(SELECT 'AMMAINMETHGRP' as V_GROUP_CODE,
        '������� ��������� (����)' as V_GROUP_NAME,
        '������� ��������� (����)' as V_GROUP_DESC,
        'USER' as V_GROUP_TYPE,
        995 as N_PRECEDENCE
   FROM dual) v
 ON (v.V_GROUP_CODE = t.V_GROUP_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_GROUP_CODE, V_GROUP_NAME, V_GROUP_DESC, V_GROUP_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, GROUP_KEY, GROUP_DESC_KEY, N_PRECEDENCE)
  VALUES (v.V_GROUP_CODE, v.V_GROUP_NAME, v.V_GROUP_DESC, v.V_GROUP_TYPE, null, sysdate, null, sysdate, null, null, v.N_PRECEDENCE)
WHEN MATCHED THEN
  UPDATE 
     SET V_GROUP_NAME = v.V_GROUP_NAME,
         V_GROUP_DESC = v.V_GROUP_DESC,
         V_GROUP_TYPE = v.V_GROUP_TYPE,
         D_LAST_MODIFIED_DATE = sysdate,
         N_PRECEDENCE = v.N_PRECEDENCE;

-- ����������� ���� � ������
DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH WHERE V_GROUP_CODE = 'AMMAINMETHGRP' and V_ROLE_CODE = 'AMMAINMETH';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH(V_GROUP_CODE, V_ROLE_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMAINMETHGRP', 'AMMAINMETH', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP WHERE V_GROUP_CODE = 'AMMAINMETHGRP' and V_ROLE_CODE = 'AMMAINMETH';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP(V_GROUP_CODE, V_ROLE_CODE)
       VALUES('AMMAINMETHGRP', 'AMMAINMETH');

-- ����������� ������� � ����
DELETE FROM OFSCONF.CSSMS_ROLE_FUNCTION_MAP WHERE v_role_code in ('AMMAINMETH');
INSERT INTO OFSCONF.CSSMS_ROLE_FUNCTION_MAP(v_role_code, v_function_code)
(
-- �� ������ ������: ������ � ������ �������� ���������� �����������
SELECT 'AMMAINMETH', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'AMMAINMETH', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFALERTS_R' /*������ � ������ �������*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFOPOK_W' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFOPOKRL_W' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFOPOKCR_W' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFASGRL_W' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFASGRP_W' /*���������� ��������� �������������*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFASGLM_W' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFOESORG_W' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFOESPTY_W' /*���������� ��� "�� - ��������� ��������"*/ FROM dual UNION ALL 
SELECT 'AMMAINMETH', 'RFOESRL_W' /*���������� ��� "������� ��������"*/ FROM dual UNION ALL 
SELECT 'AMMAINMETH', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL    
SELECT 'AMMAINMETH', 'RFIMP1001W' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMMAINMETH', 'RFIMP1002W' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMMAINMETH', 'RFIMP1003R' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
--SELECT 'AMMAINMETH', 'RFIMP1004R' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
SELECT 'AMMAINMETH', 'RFWLTP_W' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
SELECT 'AMMAINMETH', 'RFWLORG_W' /*���������� "������� �����������"*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFTRXNS_R' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMMAINMETH', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
--SELECT 'AMMAINMETH', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMMAINMETH', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMMAINMETH', 'RFREP_1006' /*�����: ������� ������*/ FROM dual 
--SELECT 'AMMAINMETH', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMMAINMETH', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual  
);

-- ����������� ������ � �������
DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH 
 WHERE V_GROUP_CODE = 'AMMAINMETHGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMAINMETHGRP', 'AMINFO', 'AMSEG', 'A', 'A', 'SYSADMN');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMAINMETHGRP', 'CMINFO', 'CMSEG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP 
 WHERE V_GROUP_CODE = 'AMMAINMETHGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMMAINMETHGRP', 'AMINFO', 'AMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMMAINMETHGRP', 'CMINFO', 'CMSEG');

commit;
