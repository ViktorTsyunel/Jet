MERGE INTO OFSCONF.CSSMS_FUNCTION_MAST t USING
(SELECT 'RFNEWGUI' as V_FUNCTION_CODE,
        '������ � ������ ���������� �� �������' as V_FUNCTION_NAME,
        '������ � ���������� ��������� ������ ��������� (����� ����) - ���������� �������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFALERTS_R' as V_FUNCTION_CODE,
        '������ ������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ������ ���������� ��������/��������� ���' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFALERTS_W' as V_FUNCTION_CODE,
        '������ �������' as V_FUNCTION_NAME,
        '������ ������ � ������ ���������� ��������/��������� ���' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFALERTALL' as V_FUNCTION_CODE,
        '��� ������' as V_FUNCTION_NAME,
        '������ � ������� ������ ������� (����� - ������ � ���, �� ������� �������� �������������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFALASSIGN' as V_FUNCTION_CODE,
        '���������� ������������� �� ������' as V_FUNCTION_NAME,
        '����� ��������� ������ ����������� �������������� �� ������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOESREADY' as V_FUNCTION_CODE,
        '��������� ��������� �������� ���/�������' as V_FUNCTION_NAME,
        '����� �� ��������� ���/�������� ��� ��������, ���������� ��������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOESCREAT' as V_FUNCTION_CODE,
        '�������� ��� �������' as V_FUNCTION_NAME,
        '������ � ����������� ������� ��� ������� (� �� �������������, �� ������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOK_R' as V_FUNCTION_CODE,
        '���������� ����� ���� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ����� ��������, ���������� ��������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOK_W' as V_FUNCTION_CODE,
        '���������� ����� ����' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ����� ��������, ���������� ��������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOKRL_R' as V_FUNCTION_CODE,
        '���������� ������ ��������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ������ ��������� �������� (����. ��������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOKRL_T' as V_FUNCTION_CODE,
        '���������� ������ ��������� (������������)' as V_FUNCTION_NAME,
        '������ ��� ����������� �������� ����������� ��������� � ����������� ������ ��������� (��. ��������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOKRL_W' as V_FUNCTION_CODE,
        '���������� ������ ���������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ������ ��������� �������� (����. ��������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOKCR_R' as V_FUNCTION_CODE,
        '���������� ����������� ������� ��������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ����������� ������� ��������� �������� (����. ��������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOKCR_T' as V_FUNCTION_CODE,
        '���������� �����. ���. ��������� (������������)' as V_FUNCTION_NAME,
        '������ ��� ����������� �������� ����������� ��������� � ����������� (����. ��������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOKCR_W' as V_FUNCTION_CODE,
        '���������� ����������� ������� ���������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ����������� ������� ��������� �������� (����. ��������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOPOKFIND' as V_FUNCTION_CODE,
        '������ ������������ ������ ���������' as V_FUNCTION_NAME,
        '������ � ����� ������������ ������ ��������� (����� ��������, ��������������� ��������)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFASGRL_R' as V_FUNCTION_CODE,
        '���������� ������ ������. ������������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ������ ���������� �������������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFASGRL_W' as V_FUNCTION_CODE,
        '���������� ������ ������. �������������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ������ ���������� �������������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFASGRP_R' as V_FUNCTION_CODE,
        '���������� ��������� ������������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ��������� �������������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFASGRP_W' as V_FUNCTION_CODE,
        '���������� ��������� �������������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ��������� �������������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFASGLM_R' as V_FUNCTION_CODE,
        '���������� ������� ������. ������������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ������� ���������� �������������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFASGLM_W' as V_FUNCTION_CODE,
        '���������� ������� ������. �������������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ������� ���������� �������������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOESORG_R' as V_FUNCTION_CODE,
        '���������� ��� ''���������� � ��'' (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ��� ''���������� � ��''' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOESORG_W' as V_FUNCTION_CODE,
        '���������� ��� ''���������� � ��''' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ��� ''���������� � ��''' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual   
 UNION ALL
 SELECT 'RFOESPTY_R' as V_FUNCTION_CODE,
        '���������� ��� ''�� - ��������� ��������'' (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ��� ''�� - ��������� ��������''' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOESPTY_W' as V_FUNCTION_CODE,
        '���������� ��� ''�� - ��������� ��������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ��� ''�� - ��������� ��������''' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual   
 UNION ALL
 SELECT 'RFOESRL_R' as V_FUNCTION_CODE,
        '���������� ��� ''������� ��������'' (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ��� ''������� ��������''' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFOESRL_W' as V_FUNCTION_CODE,
        '���������� ��� ''������� ��������''' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ��� ''������� ��������''' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual   
 UNION ALL
 SELECT 'RFIMPTP_R' as V_FUNCTION_CODE,
        '���������� ������� ������ (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ������� ������ � �������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMPTP_W' as V_FUNCTION_CODE,
        '���������� ������� ������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ������� ������ � �������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1001R' as V_FUNCTION_CODE,
        '��� �������: ���������� ����������� ��� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ���� �������: ���������� ����������� ��� �� ��' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1001W' as V_FUNCTION_CODE,
        '��� �������: ���������� ����������� ���' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ���� �������: ���������� ����������� ��� �� ��' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1002R' as V_FUNCTION_CODE,
        '��� �������: ���������� ������������� ��� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ���� �������: ���������� ������������� ��� �� ��' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1002W' as V_FUNCTION_CODE,
        '��� �������: ���������� ������������� ���' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ���� �������: ���������� ������������� ��� �� ��' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1003R' as V_FUNCTION_CODE,
        '��� �������: ��� �� XML (��. ����� ��) (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ���� �������: ��� �� XML (��. ����� ��)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1003W' as V_FUNCTION_CODE,
        '��� �������: ��� �� XML (��. ����� ��)' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ���� �������: ��� �� XML (��. ����� ��)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1004R' as V_FUNCTION_CODE,
        '��� �������: ��� �� XML (8001 ���) (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ���� �������: ��� �� XML (8001 ���)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFIMP1004W' as V_FUNCTION_CODE,
        '��� �������: ��� �� XML (8001 ���)' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ���� �������: ��� �� XML (8001 ���)' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFWLTP_R' as V_FUNCTION_CODE,
        '���������� ����� �������� ��� � ���. (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ����� �������� ��� � �����������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFWLTP_W' as V_FUNCTION_CODE,
        '���������� ����� �������� ��� � ���.' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � ����������� ����� �������� ��� � �����������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFWLORG_R' as V_FUNCTION_CODE,
        '������� ����������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � �������� �����������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFWLORG_W' as V_FUNCTION_CODE,
        '������� �����������' as V_FUNCTION_NAME,
        '������ �� ������ � ������ � �������� �����������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFTRXNS_R' as V_FUNCTION_CODE,
        '����� �������� (������)' as V_FUNCTION_NAME,
        '������ �� ������ � ����������� ������ ��������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFTRXNS_W' as V_FUNCTION_CODE,
        '����� ��������' as V_FUNCTION_NAME,
        '������ ������ (����� � �������� �������) � ����������� ������ ��������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFREP_1001' as V_FUNCTION_CODE,
        '�������� ��������� ��������' as V_FUNCTION_NAME,
        '������ � ������: �������� ��������� ��������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFREP_1002' as V_FUNCTION_CODE,
        '�������� �� ����������� ��' as V_FUNCTION_NAME,
        '������ � ������: �������� �� ����������� ��' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFREP_1003' as V_FUNCTION_CODE,
        '��������� ����� �������� ��� ���' as V_FUNCTION_NAME,
        '������ � ������: ��������� ����� �������� ��� ���' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFMAIL1003' as V_FUNCTION_CODE,
        '�������� - ��������� ����� �������� ��� ���' as V_FUNCTION_NAME,
        '�������� �� ����� ������: ��������� ����� �������� ��� ���' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFREP_1004' as V_FUNCTION_CODE,
        '��������� ����� �������� ��� ���' as V_FUNCTION_NAME,
        '������ � ������: ��������� ����� �������� ��� ���' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFMAIL1004' as V_FUNCTION_CODE,
        '�������� - ��������� ����� �������� ��� ���' as V_FUNCTION_NAME,
        '�������� �� ����� ������: ��������� ����� �������� ��� ���' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFREP_1005' as V_FUNCTION_CODE,
        '���������� ��������� ��������' as V_FUNCTION_NAME,
        '������ � ������: ���������� ��������� ��������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFREP_1006' as V_FUNCTION_CODE,
        '������� ������' as V_FUNCTION_NAME,
        '������ � ������: ������� ������' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
 UNION ALL
 SELECT 'RFREP_1007' as V_FUNCTION_CODE,
        '�������� ������ � ��������� ����' as V_FUNCTION_NAME,
        '������ � ������: �������� ������ � ��������� ����' as V_FUNCTION_DESC,
        'USER' as V_FUNCTION_TYPE
   FROM dual
   ) v
 ON (v.V_FUNCTION_CODE = t.V_FUNCTION_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_FUNCTION_CODE, V_FUNCTION_NAME, V_FUNCTION_DESC, V_FUNCTION_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, FUNCTION_KEY, FUNCTION_DESC_KEY)
  VALUES (v.V_FUNCTION_CODE, v.V_FUNCTION_NAME, v.V_FUNCTION_DESC, v.V_FUNCTION_TYPE, null, sysdate, null, sysdate, to_char(null), to_char(null))
WHEN MATCHED THEN
  UPDATE 
     SET V_FUNCTION_NAME = v.V_FUNCTION_NAME,
         V_FUNCTION_DESC = v.V_FUNCTION_DESC,
         V_FUNCTION_TYPE = v.V_FUNCTION_TYPE,
         D_LAST_MODIFIED_DATE = sysdate
/
         
MERGE INTO OFSCONF.MENU_ITEMS t USING
(SELECT '9001' as MENU_ID,
        '2' as PARENTID,
        '10' as ORDER_NO,
        'RF/index.html' as FORM_CODE,
        '1' as LEVEL_NO,
        'N' as ISFORM,
        'AMINFO' as DSN_ID,
        'AMSEG' as SEGMENT_ID,
        'OFSAAI' as PAGE_CONTEXT,
        '' as FORM_PARAMETERS,
        '' as MENU_ITEM_DESCRIPTION,
        'N' as SOLUTION_LINK,
        '' as TARGET_DSN_ID,
        'N' as ISHOME
   FROM dual) v
 ON (v.MENU_ID = t.MENU_ID)
WHEN NOT MATCHED THEN
  INSERT (MENU_ID, PARENTID, ORDER_NO, FORM_CODE, LEVEL_NO, ISFORM, DSN_ID, SEGMENT_ID, PAGE_CONTEXT, FORM_PARAMETERS, MENU_ITEM_DESCRIPTION, SOLUTION_LINK, TARGET_DSN_ID, ISHOME)
  VALUES (v.MENU_ID, v.PARENTID, v.ORDER_NO, v.FORM_CODE, v.LEVEL_NO, v.ISFORM, v.DSN_ID, v.SEGMENT_ID, v.PAGE_CONTEXT, v.FORM_PARAMETERS, v.MENU_ITEM_DESCRIPTION, v.SOLUTION_LINK, v.TARGET_DSN_ID, v.ISHOME)
WHEN MATCHED THEN
  UPDATE 
     SET PARENTID = v.PARENTID,
         ORDER_NO = v.ORDER_NO,
         FORM_CODE = v.FORM_CODE,
         LEVEL_NO = v.LEVEL_NO,
         ISFORM = v.ISFORM,
         DSN_ID = v.DSN_ID,
         SEGMENT_ID = v.SEGMENT_ID,
         PAGE_CONTEXT = v.PAGE_CONTEXT,
         FORM_PARAMETERS = v.FORM_PARAMETERS,
         MENU_ITEM_DESCRIPTION = v.MENU_ITEM_DESCRIPTION,
         SOLUTION_LINK = v.SOLUTION_LINK,
         TARGET_DSN_ID = v.TARGET_DSN_ID,
         ISHOME = v.ISHOME
/         

DELETE FROM ofsconf.menu_locale_map WHERE menu_id > 9000;
INSERT INTO ofsconf.menu_locale_map (menu_id, dsn_id, locale_id, menu_description)
VALUES ('9001', 'AMINFO', 'en_US', '������� ��������� ��������')
/

DELETE FROM ofsconf.menu_function_map WHERE menu_id > 9000;
INSERT INTO ofsconf.menu_function_map (menu_id, function_code, dsn_id)
VALUES ('9001', 'RFNEWGUI', 'AMINFO')
/

COMMIT
/


