-- ������������ ����
MERGE INTO OFSCONF.CSSMS_ROLE_MAST t USING
(SELECT 'AMMETHODLG' as V_ROLE_CODE,
        '��������� (����)' as V_ROLE_NAME,
        '��������� (����)' as V_ROLE_DESC,
        'USER' as V_ROLE_TYPE
   FROM dual) v
 ON (v.V_ROLE_CODE = t.V_ROLE_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_ROLE_CODE, V_ROLE_NAME, V_ROLE_DESC, V_ROLE_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, ROLE_KEY, ROLE_DESC_KEY)
  VALUES (v.V_ROLE_CODE, v.V_ROLE_NAME, v.V_ROLE_DESC, v.V_ROLE_TYPE, null, sysdate, null, sysdate, null, null)
WHEN MATCHED THEN
  UPDATE 
     SET V_ROLE_NAME = v.V_ROLE_NAME,
         V_ROLE_DESC = v.V_ROLE_DESC,
         V_ROLE_TYPE = v.V_ROLE_TYPE,
         D_LAST_MODIFIED_DATE = sysdate;

-- ������������ ������         
MERGE INTO OFSCONF.CSSMS_GROUP_MAST t USING
(SELECT 'AMMETHODLGGRP' as V_GROUP_CODE,
        '��������� (����)' as V_GROUP_NAME,
        '��������� (����)' as V_GROUP_DESC,
        'USER' as V_GROUP_TYPE,
        994 as N_PRECEDENCE
   FROM dual) v
 ON (v.V_GROUP_CODE = t.V_GROUP_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_GROUP_CODE, V_GROUP_NAME, V_GROUP_DESC, V_GROUP_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, GROUP_KEY, GROUP_DESC_KEY, N_PRECEDENCE)
  VALUES (v.V_GROUP_CODE, v.V_GROUP_NAME, v.V_GROUP_DESC, v.V_GROUP_TYPE, null, sysdate, null, sysdate, null, null, v.N_PRECEDENCE)
WHEN MATCHED THEN
  UPDATE 
     SET V_GROUP_NAME = v.V_GROUP_NAME,
         V_GROUP_DESC = v.V_GROUP_DESC,
         V_GROUP_TYPE = v.V_GROUP_TYPE,
         D_LAST_MODIFIED_DATE = sysdate,
         N_PRECEDENCE = v.N_PRECEDENCE;

-- ����������� ���� � ������
DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH WHERE V_GROUP_CODE = 'AMMETHODLGGRP' and V_ROLE_CODE = 'AMMETHODLG';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH(V_GROUP_CODE, V_ROLE_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMETHODLGGRP', 'AMMETHODLG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP WHERE V_GROUP_CODE = 'AMMETHODLGGRP' and V_ROLE_CODE = 'AMMETHODLG';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP(V_GROUP_CODE, V_ROLE_CODE)
       VALUES('AMMETHODLGGRP', 'AMMETHODLG');

-- ����������� ������� � ����
DELETE FROM OFSCONF.CSSMS_ROLE_FUNCTION_MAP WHERE v_role_code in ('AMMETHODLG');
INSERT INTO OFSCONF.CSSMS_ROLE_FUNCTION_MAP(v_role_code, v_function_code)
(
-- �� ������ ������: ������ � ������ �������� ���������� �����������
SELECT 'AMMETHODLG', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'AMMETHODLG', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFALERTS_R' /*������ � ������ �������*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFOPOKRL_T' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFOPOKCR_T' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RFASGRL_W' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RFASGRP_W' /*���������� ��������� �������������*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RFASGLM_W' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFOESORG_W' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFOESPTY_W' /*���������� ��� "�� - ��������� ��������"*/ FROM dual UNION ALL 
SELECT 'AMMETHODLG', 'RFOESRL_W' /*���������� ��� "������� ��������"*/ FROM dual UNION ALL  
SELECT 'AMMETHODLG', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL    
SELECT 'AMMETHODLG', 'RFIMP1001W' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMMETHODLG', 'RFIMP1002W' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMMETHODLG', 'RFIMP1003R' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
--SELECT 'AMMETHODLG', 'RFIMP1004R' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
SELECT 'AMMETHODLG', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
SELECT 'AMMETHODLG', 'RFWLORG_W' /*���������� "������� �����������"*/ FROM dual UNION ALL 
SELECT 'AMMETHODLG', 'RFTRXNS_R' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMMETHODLG', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
--SELECT 'AMMETHODLG', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL 
--SELECT 'AMMETHODLG', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMMETHODLG', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMMETHODLG', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMMETHODLG', 'RFREP_1006' /*�����: ������� ������*/ FROM dual 
--SELECT 'AMMETHODLG', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMMETHODLG', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual 
);

-- ����������� ������ � �������
DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH 
 WHERE V_GROUP_CODE = 'AMMETHODLGGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMETHODLGGRP', 'AMINFO', 'AMSEG', 'A', 'A', 'SYSADMN');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMMETHODLGGRP', 'CMINFO', 'CMSEG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP 
 WHERE V_GROUP_CODE = 'AMMETHODLGGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMMETHODLGGRP', 'AMINFO', 'AMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMMETHODLGGRP', 'CMINFO', 'CMSEG');

commit;
