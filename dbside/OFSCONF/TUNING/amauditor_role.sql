-- ������������ ����
MERGE INTO OFSCONF.CSSMS_ROLE_MAST t USING
(SELECT 'AMAUDITOR' as V_ROLE_CODE,
        '�������/�������' as V_ROLE_NAME,
        '�������/�������' as V_ROLE_DESC,
        'USER' as V_ROLE_TYPE
   FROM dual) v
 ON (v.V_ROLE_CODE = t.V_ROLE_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_ROLE_CODE, V_ROLE_NAME, V_ROLE_DESC, V_ROLE_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, ROLE_KEY, ROLE_DESC_KEY)
  VALUES (v.V_ROLE_CODE, v.V_ROLE_NAME, v.V_ROLE_DESC, v.V_ROLE_TYPE, null, sysdate, null, sysdate, null, null)
WHEN MATCHED THEN
  UPDATE 
     SET V_ROLE_NAME = v.V_ROLE_NAME,
         V_ROLE_DESC = v.V_ROLE_DESC,
         V_ROLE_TYPE = v.V_ROLE_TYPE,
         D_LAST_MODIFIED_DATE = sysdate;

-- ������������ ������         
MERGE INTO OFSCONF.CSSMS_GROUP_MAST t USING
(SELECT 'AMAUDITORGRP' as V_GROUP_CODE,
        '�������/�������' as V_GROUP_NAME,
        '�������/�������' as V_GROUP_DESC,
        'USER' as V_GROUP_TYPE,
        996 as N_PRECEDENCE
   FROM dual) v
 ON (v.V_GROUP_CODE = t.V_GROUP_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_GROUP_CODE, V_GROUP_NAME, V_GROUP_DESC, V_GROUP_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, GROUP_KEY, GROUP_DESC_KEY, N_PRECEDENCE)
  VALUES (v.V_GROUP_CODE, v.V_GROUP_NAME, v.V_GROUP_DESC, v.V_GROUP_TYPE, null, sysdate, null, sysdate, null, null, v.N_PRECEDENCE)
WHEN MATCHED THEN
  UPDATE 
     SET V_GROUP_NAME = v.V_GROUP_NAME,
         V_GROUP_DESC = v.V_GROUP_DESC,
         V_GROUP_TYPE = v.V_GROUP_TYPE,
         D_LAST_MODIFIED_DATE = sysdate,
         N_PRECEDENCE = v.N_PRECEDENCE;

-- ����������� ���� � ������
DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH WHERE V_GROUP_CODE = 'AMAUDITORGRP' and V_ROLE_CODE = 'AMAUDITOR';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH(V_GROUP_CODE, V_ROLE_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMAUDITORGRP', 'AMAUDITOR', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP WHERE V_GROUP_CODE = 'AMAUDITORGRP' and V_ROLE_CODE = 'AMAUDITOR';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP(V_GROUP_CODE, V_ROLE_CODE)
       VALUES('AMAUDITORGRP', 'AMAUDITOR');

-- ����������� ������� � ����
DELETE FROM OFSCONF.CSSMS_ROLE_FUNCTION_MAP WHERE v_role_code in ('AMAUDITOR');
INSERT INTO OFSCONF.CSSMS_ROLE_FUNCTION_MAP(v_role_code, v_function_code)
(
-- �� ������ ������: ������ � ������ �������� ���������� �����������
SELECT 'AMAUDITOR', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'AMAUDITOR', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'RFALERTS_R' /*������ � ������ �������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFASGRL_R' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFASGRP_R' /*���������� ��������� �������������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFASGLM_R' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������"*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFOESRL_R' /*���������� ��� "������� ��������"*/ FROM dual UNION ALL  
--SELECT 'AMAUDITOR', 'RFIMPTP_R' /*���������� "���� �������*/ FROM dual UNION ALL    
--SELECT 'AMAUDITOR', 'RFIMP1001R' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMAUDITOR', 'RFIMP1002R' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
--SELECT 'AMAUDITOR', 'RFIMP1003R' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
--SELECT 'AMAUDITOR', 'RFIMP1004R' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
--SELECT 'AMAUDITOR', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFWLORG_R' /*���������� "������� �����������"*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFTRXNS_R' /*����� ��������*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
--SELECT 'AMAUDITOR', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'AMAUDITOR', 'RFREP_1006' /*�����: ������� ������*/ FROM dual 
--SELECT 'AMAUDITOR', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMAUDITOR', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual 
);

-- ����������� ������ � �������
DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH 
 WHERE V_GROUP_CODE = 'AMAUDITORGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMAUDITORGRP', 'AMINFO', 'AMSEG', 'A', 'A', 'SYSADMN');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMAUDITORGRP', 'CMINFO', 'CMSEG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP 
 WHERE V_GROUP_CODE = 'AMAUDITORGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMAUDITORGRP', 'AMINFO', 'AMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMAUDITORGRP', 'CMINFO', 'CMSEG');

commit;
