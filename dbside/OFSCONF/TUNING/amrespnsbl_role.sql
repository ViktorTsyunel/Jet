-- ������������ ����
MERGE INTO OFSCONF.CSSMS_ROLE_MAST t USING
(SELECT 'AMRESPNSBL' as V_ROLE_CODE,
        '�������������� (����)' as V_ROLE_NAME,
        '�������������� (����)' as V_ROLE_DESC,
        'USER' as V_ROLE_TYPE
   FROM dual) v
 ON (v.V_ROLE_CODE = t.V_ROLE_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_ROLE_CODE, V_ROLE_NAME, V_ROLE_DESC, V_ROLE_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, ROLE_KEY, ROLE_DESC_KEY)
  VALUES (v.V_ROLE_CODE, v.V_ROLE_NAME, v.V_ROLE_DESC, v.V_ROLE_TYPE, null, sysdate, null, sysdate, null, null)
WHEN MATCHED THEN
  UPDATE 
     SET V_ROLE_NAME = v.V_ROLE_NAME,
         V_ROLE_DESC = v.V_ROLE_DESC,
         V_ROLE_TYPE = v.V_ROLE_TYPE,
         D_LAST_MODIFIED_DATE = sysdate;

-- ������������ ������         
MERGE INTO OFSCONF.CSSMS_GROUP_MAST t USING
(SELECT 'AMRESPNSBLGRP' as V_GROUP_CODE,
        '�������������� ���/�� (����)' as V_GROUP_NAME,
        '�������������� ���/�� (����)' as V_GROUP_DESC,
        'USER' as V_GROUP_TYPE,
        991 as N_PRECEDENCE
   FROM dual) v
 ON (v.V_GROUP_CODE = t.V_GROUP_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_GROUP_CODE, V_GROUP_NAME, V_GROUP_DESC, V_GROUP_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, GROUP_KEY, GROUP_DESC_KEY, N_PRECEDENCE)
  VALUES (v.V_GROUP_CODE, v.V_GROUP_NAME, v.V_GROUP_DESC, v.V_GROUP_TYPE, null, sysdate, null, sysdate, null, null, v.N_PRECEDENCE)
WHEN MATCHED THEN
  UPDATE 
     SET V_GROUP_NAME = v.V_GROUP_NAME,
         V_GROUP_DESC = v.V_GROUP_DESC,
         V_GROUP_TYPE = v.V_GROUP_TYPE,
         D_LAST_MODIFIED_DATE = sysdate,
         N_PRECEDENCE = v.N_PRECEDENCE;

-- ����������� ���� � ������
DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH WHERE V_GROUP_CODE = 'AMRESPNSBLGRP' and V_ROLE_CODE = 'AMRESPNSBL';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH(V_GROUP_CODE, V_ROLE_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMRESPNSBLGRP', 'AMRESPNSBL', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP WHERE V_GROUP_CODE = 'AMRESPNSBLGRP' and V_ROLE_CODE = 'AMRESPNSBL';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP(V_GROUP_CODE, V_ROLE_CODE)
       VALUES('AMRESPNSBLGRP', 'AMRESPNSBL');

-- ����������� ������� � ����
DELETE FROM OFSCONF.CSSMS_ROLE_FUNCTION_MAP WHERE v_role_code in ('AMRESPNSBL');
INSERT INTO OFSCONF.CSSMS_ROLE_FUNCTION_MAP(v_role_code, v_function_code)
(
-- �� ������ ������: ������ � ������ �������� ���������� �����������
SELECT 'AMRESPNSBL', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'AMRESPNSBL', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFALERTS_W' /*������ � ������ �������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOPOK_R' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOPOKRL_R' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOPOKCR_R' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFASGRL_W' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFASGRP_W' /*���������� ��������� �������������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFASGLM_W' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOESORG_R' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFOESPTY_R' /*���������� ��� "�� - ��������� ��������"*/ FROM dual UNION ALL 
SELECT 'AMRESPNSBL', 'RFOESRL_R' /*���������� ��� "������� ��������"*/ FROM dual UNION ALL 
SELECT 'AMRESPNSBL', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL    
SELECT 'AMRESPNSBL', 'RFIMP1001W' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMRESPNSBL', 'RFIMP1002W' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'AMRESPNSBL', 'RFIMP1003W' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
SELECT 'AMRESPNSBL', 'RFIMP1004W' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
SELECT 'AMRESPNSBL', 'RFWLTP_R' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
SELECT 'AMRESPNSBL', 'RFWLORG_R' /*���������� "������� �����������"*/ FROM dual UNION ALL 
SELECT 'AMRESPNSBL', 'RFTRXNS_W' /*����� ��������*/ FROM dual UNION ALL 
SELECT 'AMRESPNSBL', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL 
--SELECT 'AMRESPNSBL', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL  
--SELECT 'AMRESPNSBL', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL  
--SELECT 'AMRESPNSBL', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
SELECT 'AMRESPNSBL', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
--SELECT 'AMRESPNSBL', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
--SELECT 'AMRESPNSBL', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual 
SELECT 'AMRESPNSBL', 'RFREP_1006' /*�����: ������� ������*/ FROM dual UNION ALL 
SELECT 'AMRESPNSBL', 'RFREP_1007' /*�����: �������� ������ � ��������� ����*/ FROM dual 
);

-- ����������� ������ � �������
DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH 
 WHERE V_GROUP_CODE = 'AMRESPNSBLGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMRESPNSBLGRP', 'AMINFO', 'AMSEG', 'A', 'A', 'SYSADMN');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('AMRESPNSBLGRP', 'CMINFO', 'CMSEG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP 
 WHERE V_GROUP_CODE = 'AMRESPNSBLGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMRESPNSBLGRP', 'AMINFO', 'AMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('AMRESPNSBLGRP', 'CMINFO', 'CMSEG');

commit;
