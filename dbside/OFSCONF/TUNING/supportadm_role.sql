-- ������������ ����
MERGE INTO OFSCONF.CSSMS_ROLE_MAST t USING
(SELECT 'SUPPORTADM' as V_ROLE_CODE,
        'AM Support Admin' as V_ROLE_NAME,
        'AM Support Admin' as V_ROLE_DESC,
        'USER' as V_ROLE_TYPE
   FROM dual) v
 ON (v.V_ROLE_CODE = t.V_ROLE_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_ROLE_CODE, V_ROLE_NAME, V_ROLE_DESC, V_ROLE_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, ROLE_KEY, ROLE_DESC_KEY)
  VALUES (v.V_ROLE_CODE, v.V_ROLE_NAME, v.V_ROLE_DESC, v.V_ROLE_TYPE, null, sysdate, null, sysdate, null, null)
WHEN MATCHED THEN
  UPDATE 
     SET V_ROLE_NAME = v.V_ROLE_NAME,
         V_ROLE_DESC = v.V_ROLE_DESC,
         V_ROLE_TYPE = v.V_ROLE_TYPE,
         D_LAST_MODIFIED_DATE = sysdate;

-- ������������ ������         
MERGE INTO OFSCONF.CSSMS_GROUP_MAST t USING
(SELECT 'SUPPORTADMGRP' as V_GROUP_CODE,
        'AM Support Admin User Group' as V_GROUP_NAME,
        'AM Support Admin User Group' as V_GROUP_DESC,
        'USER' as V_GROUP_TYPE,
        990 as N_PRECEDENCE
   FROM dual) v
 ON (v.V_GROUP_CODE = t.V_GROUP_CODE)
WHEN NOT MATCHED THEN
  INSERT (V_GROUP_CODE, V_GROUP_NAME, V_GROUP_DESC, V_GROUP_TYPE, V_CREATED_BY, D_CREATED_DATE, V_LAST_MODIFIED_BY, D_LAST_MODIFIED_DATE, GROUP_KEY, GROUP_DESC_KEY, N_PRECEDENCE)
  VALUES (v.V_GROUP_CODE, v.V_GROUP_NAME, v.V_GROUP_DESC, v.V_GROUP_TYPE, null, sysdate, null, sysdate, null, null, v.N_PRECEDENCE)
WHEN MATCHED THEN
  UPDATE 
     SET V_GROUP_NAME = v.V_GROUP_NAME,
         V_GROUP_DESC = v.V_GROUP_DESC,
         V_GROUP_TYPE = v.V_GROUP_TYPE,
         D_LAST_MODIFIED_DATE = sysdate,
         N_PRECEDENCE = v.N_PRECEDENCE;

-- ����������� ���� � ������
DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH WHERE V_GROUP_CODE = 'SUPPORTADMGRP' and V_ROLE_CODE = 'SUPPORTADM';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP_UNAUTH(V_GROUP_CODE, V_ROLE_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('SUPPORTADMGRP', 'SUPPORTADM', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_GROUP_ROLE_MAP WHERE V_GROUP_CODE = 'SUPPORTADMGRP' and V_ROLE_CODE = 'SUPPORTADM';
INSERT INTO OFSCONF.CSSMS_GROUP_ROLE_MAP(V_GROUP_CODE, V_ROLE_CODE)
       VALUES('SUPPORTADMGRP', 'SUPPORTADM');

-- ����������� ������� � ����
DELETE FROM OFSCONF.CSSMS_ROLE_FUNCTION_MAP WHERE v_role_code in ('SUPPORTADM');
INSERT INTO OFSCONF.CSSMS_ROLE_FUNCTION_MAP(v_role_code, v_function_code)
(
SELECT 'SUPPORTADM', 'ABOUT' /*Copyright Info For Both AM and CM*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'ADMIN' /*Administrator*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'ADMNTOOL' /*Admin Tools*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'ALRTCRTR' /*Admin Tools Creator*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'ALTSASSED' /*Admin Tools Assigner*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMACCESS' /*AM Access*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMACCTB' /*AM Account Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMACCTB2' /*AM Account Tab2*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMALTS' /*Alerts*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMALTSSUP' /*Alert Suppression*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMAUDTB' /*AM Audit Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMCBDTL' /*AM Correspondent Bank Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMCORTB' /*AM Correlation Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMCTRLCUST' /*Manage Controlling Customer*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMCUSTB' /*AM Customer Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMDERADDTB' /*AM Derived Address Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMDETTB' /*AM Detail Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMDISTB' /*AM Disposition Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMEMPTB' /*AM Employee Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMENGCONTB' /*AM Energy and Commodity Instrument   Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMENGCORTB' /*AM Energy and Commodity Trade Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMEVITB' /*AM Evidence Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMEXENTTB' /*AM External Entity Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMEXTNTB' /*AM Execution Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMFITB' /*The user mapped to this function can access Financial*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMHHTB' /*AM Household Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMIATAB' /*AM Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMIOSTB' /*AM IOS Review Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMLOANTB' /*AM Loan Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMMKTPRTB' /*AM Market Participant Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMMONTG' /*Mantas*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMNARTB' /*AM Narrative Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMNETTB' /*AM Network Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMNGFLWTB' /*AM Natural Gas Flow Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMNWALTS' /*New Alerts*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMORDRTB' /*AM Order Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMPASENCYT' /*AM Password Encryption*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMPTMNTB' /*AM Portfolio Manager Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMRBASIC' /*AM Basic Report */ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMRCSEACT' /*AM Case Activity*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMREGPSTTB' /*AM Registered Representative Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMRELTB' /*AM Relationship Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMREPTB' /*AM Market Replay Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMRFPMGT' /*AM False Positive Manangement*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMRLNDUE' /*AM Late Near due*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMRREGACT' /*AM Regulatory Activity*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMRREP' /*AM  Report*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMSCRTTB' /*AM Security Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMSECRST' /*Manage Security Restrictions*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMTBFLWTB' /*AM Trdae Blotter Follow Up Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMTBNEWTB' /*AM Trdae Blotter Un-reviewed Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMTBREWTB' /*AM Trdae Blotter Reviewed Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMTRADTB' /*AM Trade Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMTRDBDT' /*AM Trade Blotter*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMTRDRTB' /*AM Trader Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMTRSPRS' /*Trusted Pairs*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'AMUSRATT' /*Mantas User Att*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CCMENUHOME' /*AM CM Common Home*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CCPREF' /*User Preferences*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMACCESS' /*CM Access*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMACTTB' /*Case Account Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMADMNTULS' /*Case Management Admin Tools*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMASSGNED' /*Case Assigner Editor*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMAUDTB' /*Case Audit Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMCASE' /*Cases*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMCBTB' /*Case CB Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMCORTB' /*Case Correlation Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMCUSTTB' /*Case Customer Tab */ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMEMTB' /*Case Employee Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMEVITB' /*Case Evidence Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMEXTB' /*Case External Entity Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMFINTB' /*Case Financial Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMHHTB' /*Case House Hold Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMIATB' /*Case Investment Advisor Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMINVLPTB' /*Case Involved Party Tab */ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMMENUHOME' /*Case Home*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMMNPARAM' /*The user mapped to this function can access Common Parameters UI*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMNETTB' /*Case Network Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMNRTTB' /*Case Narrative Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMNWCASE' /*New Cases*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMRELTB' /*Case Relation Ship Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMSDETTB' /*Case Detail Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CMTRATB' /*Case Transaction Tab*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CWSDOCMGMT' /*The user mapped to this function can use Document Management APIS via Callable Services Framework*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CWSEXTWSAS' /*The user mapped to this function can call web services configured in the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CWSPR2ACCS' /*The user mapped to this function can execute runs and rules through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'CWS_STATUS' /*The user mapped to this function can access requests status through the Callable Services Framework*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'EXCELUPLOD' /*Excel Upload*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'FUNCMAINT' /*The user mapped to this function can access the Function Maintenance Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'FUNCROLE' /*The user mapped to this function can access the Function Role Map Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'HOLMAINT' /*The user mapped to this function can access the Holiday Maintenance Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'INSTLPARAM' /*The user mapped to this function can access Install Param UI*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'INVSTG' /*Investigation*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'OPREXEC' /*The user mapped to this function will have rights to run, restart and rerun batches*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'OTHREP' /*The user mapped to this function can define  Mapping*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RESTPASS' /*The user mapped to this function can access the Restricted Passwords Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RPS' /*Reports*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RSGNTALL' /*Reassign to all*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RSKSCRED' /*Admin Tools Scorer*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'SEGMAINT' /*The user mapped to this function can access the Segment Maintenance Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'TABACCESS' /*The user mapped to this function can access Tabs*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'UGDOMMAP' /*The user mapped to this function can access the User Group Domain Map Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'UGMAINT' /*The user mapped to this function can access the User Group Maintenance Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'UGMAP' /*The user mapped to this function can access the User Group User Map Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'UGROLMAP' /*The user mapped to this function can access the User Group Role Map Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'USRADMN' /*User Administration*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'USRATH' /*The user mapped to this function can access the User Authorization Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'USRATTUP' /*The user mapped to this function can access the User Attribute Upload Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'USRBATMAP' /*The user mapped to this function can access the User-Batch Execution Mapping Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'USRMAINT' /*The user mapped to this function can access the User Maintenance Screen*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'VIEW_HOME' /*The user mapped to this function can view main LHS menu*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'WLACCESS' /*Access to Watchlist Management*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'WLEDITOTH' /*Watch List Edit Others changes*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'WLEDITOWN' /*Watch List Edit  Own changes*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'WLMNGLIST' /*Access to Watchlist Manage List Menu*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'WLMNGMEMBR' /*Access to Watchlist Manage Members Menu*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'XLADMIN' /*The user mapped to this function can define Excel Mapping*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'XLUSER' /*The user mapped to this function can Upload Excel Data*/ FROM dual UNION ALL
-- ������ � ����� ��������
SELECT 'SUPPORTADM', 'RFNEWGUI' /*������ � ������ ����������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFALERTS_W' /*������ � ������ �������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFALERTALL' /*������ � ������� ������ �������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFALASSIGN' /*����� ��������� ������ ����������� �������������� �� ������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOESREADY' /*����� �� ��������� ���/�������� ��� ��������, ���������� ��������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOESCREAT' /*������ � �������� ��� �������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOPOK_W' /*���������� ����� ����*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOPOKRL_W' /*���������� ������ ���������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOPOKCR_W' /*���������� ����������� ������� ���������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOPOKFIND' /*������ ������������ ������ ���������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFASGRL_W' /*���������� ������ ���������� �������������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFASGRP_W' /*���������� ��������� �������������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFASGLM_W' /*���������� ������� ���������� �������������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOESORG_W' /*���������� ��� "���������� � ��"*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFOESPTY_W' /*���������� ��� "�� - ��������� ��������"*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFOESRL_W' /*���������� ��� "������� ��������"*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFIMPTP_W' /*���������� "���� �������*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFIMP1001W' /*��� �������: ���������� ����������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'SUPPORTADM', 'RFIMP1002W' /*��� �������: ���������� ������������� ��� �� ��*/ FROM dual UNION ALL  
SELECT 'SUPPORTADM', 'RFIMP1003W' /*��� �������: ��� �� XML (����������� ����� ��)*/ FROM dual UNION ALL  
SELECT 'SUPPORTADM', 'RFIMP1004W' /*��� �������: ��� �� XML (8001 ���)*/ FROM dual UNION ALL  
SELECT 'SUPPORTADM', 'RFWLTP_W' /*���������� "���� �������� ��� � �����������"*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFWLORG_W' /*���������� "������� �����������"*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFTRXNS_W' /*����� ��������*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFREP_1001' /*�����: �������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFREP_1002' /*�����: �������� �� ����������� ��*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFREP_1003' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFREP_1004' /*�����: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFMAIL1003' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL 
SELECT 'SUPPORTADM', 'RFMAIL1004' /*�������� ������: ��������� ����� �������� ��� ���*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFREP_1005' /*�����: ���������� ��������� ��������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFREP_1006' /*�����: ������� ������*/ FROM dual UNION ALL
SELECT 'SUPPORTADM', 'RFREP_1007' /*�����: �������� ������ � ��������� ����*/ FROM dual 
);

-- ����������� ������ � �������
DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH 
 WHERE V_GROUP_CODE = 'SUPPORTADMGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('SUPPORTADMGRP', 'AMINFO', 'AMSEG', 'A', 'A', 'SYSADMN');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_MAP_UNAUTH(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE, RECORDSTAT, OPERATION, MODIFIED_BY)
       VALUES('SUPPORTADMGRP', 'CMINFO', 'CMSEG', 'A', 'A', 'SYSADMN');

DELETE FROM OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP 
 WHERE V_GROUP_CODE = 'SUPPORTADMGRP' and V_DSNID in ('AMINFO', 'CMINFO') and V_SEGMENT_CODE in ('AMSEG', 'CMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('SUPPORTADMGRP', 'AMINFO', 'AMSEG');
INSERT INTO OFSCONF.CSSMS_USR_GROUP_DSN_SEG_MAP(V_GROUP_CODE, V_DSNID, V_SEGMENT_CODE)
       VALUES('SUPPORTADMGRP', 'CMINFO', 'CMSEG');

commit;
