drop type business.rf_tab_trxn_party_set
/

create or replace type business.rf_tab_trxn_party as object (
op_cat_cd varchar2(12),
trxn_seq_id number(22),
party_cd varchar2(40),
cust_seq_id number(22),
cust_fl_seq_id number(22),
cust_type_cd varchar2(40),
resident_fl varchar2(4),
cust_nm varchar2(4000),
cust_fl_nm varchar2(4000),
gender_cd varchar2(4),
inn_nb varchar2(256),
ogrn_nb varchar2(256),
birth_dt date,
birthplace_tx varchar2(4000),
kpp_nb varchar2(256),
citizenship_cd varchar2(12),
reg_org_nm varchar2(1020),
okved_nb varchar2(4000),
okpo_nb varchar2(256),
reg_dt date,
adr_country_cd varchar2(12),
adr_post_index varchar2(80),
adr_state_nm varchar2(1020),
adr_state_type_cd varchar2(256),
adr_state_seq_id number(22),
adr_district_type_cd varchar2(256),
adr_district_nm varchar2(1020),
adr_city_type_cd varchar2(256),
adr_city_nm varchar2(1020),
adr_settlm_type_cd varchar2(256),
adr_settlm_nm varchar2(1020),
adr_street_name_tx varchar2(1020),
adr_street_type_tx varchar2(240),
adr_house_number_tx varchar2(240),
adr_building_number_tx varchar2(240),
adr_flat_number_tx varchar2(240),
adr_unstruct_fl number(22),
adr_unstruct_tx varchar2(4000),
fl_adr_country_cd varchar2(12),
adh_from_adr_fl number(22),
adh_country_cd varchar2(12),
adh_post_index varchar2(80),
adh_state_nm varchar2(1020),
adh_state_type_cd varchar2(256),
adh_state_seq_id number(22),
adh_district_type_cd varchar2(256),
adh_district_nm varchar2(1020),
adh_city_type_cd varchar2(256),
adh_city_nm varchar2(1020),
adh_settlm_type_cd varchar2(256),
adh_settlm_nm varchar2(1020),
adh_street_name_tx varchar2(1020),
adh_street_type_tx varchar2(240),
adh_house_number_tx varchar2(240),
adh_building_number_tx varchar2(240),
adh_flat_number_tx varchar2(240),
adh_unstruct_fl number(22),
adh_unstruct_tx varchar2(4000),
doc_type_cd number(22),
document_series varchar2(256),
document_no varchar2(256),
document_issued_dpt_tx varchar2(1020),
document_issued_dpt_cd varchar2(256),
document_issued_dt date,
document_end_dt date,
valid_fl varchar2(4),
stay_cust_doc_seq_id number(22),
mc_cust_doc_seq_id number(22),
regist_addr_string_trxn varchar2(4000),
fact_addr_string_trxn varchar2(4000),
regist_addr_string_custaddr varchar2(4000),
fact_addr_string_custaddr varchar2(4000),
acct_seq_id number(22),
acct_nb varchar2(256),
goz_op_cd varchar2(64 char),
deb_cred_acct_nb varchar2(256),
instn_nm varchar2(1400),
instn_id varchar2(200),
instn_cntry_cd varchar2(3 char),
instn_acct_id varchar2(200),
branch_id                 INTEGER,
oes_org_id                NUMBER(22), 
kgrko_nm                  VARCHAR2(255 CHAR),
kgrko_id                  VARCHAR2(4 CHAR),
bank_card_id_nb number(22),
bank_card_trxn_dt date,
src_sys_cd varchar2(12)
)
/


create or replace type business.rf_tab_trxn_party_set as table of business.rf_tab_trxn_party
/


create or replace function business.rfv_trxn_party(p_op_cat_cd varchar2, p_trxn_id number, p_arh_dump_dt date default null)
return business.rf_tab_trxn_party_set pipelined is
result business.rf_tab_trxn_party := business.rf_tab_trxn_party(null,null,null,null,null,null,null,null,null,null,null,
       null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
       null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
       null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null, 
       null,null,null,null,null,null,null,null,null,null);
begin
  if (p_op_cat_cd = 'WT') and (p_trxn_id is not null) then
    for r in (
    with party_van as
    (select 1, 'ORIG' as party_cd from dual union all
     select 2, 'BENEF' as party_cd from dual union all
     select 3, 'SCND_ORIG' as party_cd from dual union all
     select 4, 'SCND_BENEF' as party_cd from dual
    )
    --orig_addr_f benef_addr_f scnd_orig_addr_f scnd_benef_addr_f orig_addr_h_f benef_addr_h_f scnd_orig_addr_h_f scnd_benef_addr_h_f
       SELECT
       /*+ ORDERED USE_NL(orig orig_fl benef benef_fl scnd_orig scnd_benef orig_addr orig_fl_addr orig_addr_state benef_addr benef_fl_addr benef_addr_state scnd_orig_addr scnd_orig_addr_state scnd_benef_addr scnd_benef_addr_state orig_addr_h orig_addr_state_h benef_addr_h benef_addr_state_h scnd_orig_addr_h scnd_orig_addr_state_h scnd_benef_addr_h scnd_benef_addr_state_h orig_doc benef_doc scnd_orig_doc scnd_benef_doc orig_addr_f orig_acct orig_tb orig_tb_cust benef_acct benef_tb benef_tb_cust trxe send_org rcv_org)*/
       'WT' as op_cat_cd,
       wt.fo_trxn_seq_id as trxn_seq_id,
       p.party_cd,
       --
       --�������� � �����������
       --
       -- id �������
       decode(p.party_cd, 'ORIG',       orig.cust_seq_id,
                          'BENEF',      benef.cust_seq_id,
                          'SCND_ORIG',  scnd_orig.cust_seq_id,
                          'SCND_BENEF', scnd_benef.cust_seq_id) as cust_seq_id,
       -- id ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       orig_fl.cust_seq_id,
                          'BENEF',      benef_fl.cust_seq_id,
                          'SCND_ORIG',  to_number(null),
                          'SCND_BENEF', to_number(null)) as cust_fl_seq_id,
       -- ��� �������
       decode(p.party_cd, 'ORIG',       orig.cust_type_cd,
                          'BENEF',      benef.cust_type_cd,
                          'SCND_ORIG',  scnd_orig.cust_type_cd,
                          'SCND_BENEF', scnd_benef.cust_type_cd) as cust_type_cd,
       -- ��������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_resident_fl, orig.rf_resident_fl),
                          'BENEF',      nvl(wt.rf_benef_resident_fl, benef.rf_resident_fl),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_resident_fl, scnd_orig.rf_resident_fl),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_resident_fl, scnd_benef.rf_resident_fl)) as resident_fl,
       -- ������������
       decode(p.party_cd, 'ORIG',       nvl(trim(wt.orig_nm || ' ' || wt.rf_orig_first_nm || ' ' || wt.rf_orig_midl_nm), orig.full_nm)||
                                        nvl2(wt.rf_orig_add_info_tx, ' ('||wt.rf_orig_add_info_tx||')', null),
                          'BENEF',      nvl(trim(wt.benef_nm || ' ' || wt.rf_benef_first_nm || ' ' || wt.rf_benef_midl_nm), benef.full_nm)||
                                        nvl2(wt.rf_benef_add_info_tx, ' ('||wt.rf_benef_add_info_tx||')', null),
                          'SCND_ORIG',  nvl(trim(wt.scnd_orig_nm || ' ' || wt.rf_scnd_orig_first_nm || ' ' || wt.rf_scnd_orig_midl_nm), scnd_orig.full_nm),
                          'SCND_BENEF', nvl(trim(wt.scnd_benef_nm || ' ' || wt.rf_scnd_benef_first_nm || ' ' || wt.rf_scnd_benef_midl_nm), scnd_benef.full_nm)) as cust_nm,
       -- ��� ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       orig_fl.full_nm,
                          'BENEF',      benef_fl.full_nm,
                          'SCND_ORIG',  to_char(null),
                          'SCND_BENEF', to_char(null)) as cust_fl_nm,
       -- ���
       decode(p.party_cd, 'ORIG',       coalesce(wt.rf_orig_gndr_cd, orig_fl.cust_gndr_cd, orig.cust_gndr_cd),
                          'BENEF',      coalesce(wt.rf_benef_gndr_cd, benef_fl.cust_gndr_cd, benef.cust_gndr_cd),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_gndr_cd, scnd_orig.cust_gndr_cd),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_gndr_cd, scnd_benef.cust_gndr_cd)) as gender_cd,
       -- ���
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_inn_nb, orig.tax_id),
                          'BENEF',      nvl(wt.rf_benef_inn_nb, benef.tax_id),
                          'SCND_ORIG',  scnd_orig.tax_id,
                          'SCND_BENEF', scnd_benef.tax_id) as inn_nb,
       -- ����
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_ogrn_nb, orig.rf_ogrn_nb),
                          'BENEF',      nvl(wt.rf_benef_ogrn_nb, benef.rf_ogrn_nb),
                          'SCND_ORIG',  scnd_orig.rf_ogrn_nb,
                          'SCND_BENEF', scnd_benef.rf_ogrn_nb) as ogrn_nb,
       -- ���� ��������
       decode(p.party_cd, 'ORIG',       coalesce(wt.rf_orig_birth_dt, orig_fl.birth_dt, orig.birth_dt),
                          'BENEF',      coalesce(wt.rf_benef_birth_dt, benef_fl.birth_dt, benef.birth_dt),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_birth_dt, scnd_orig.birth_dt),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_birth_dt, scnd_benef.birth_dt)) as birth_dt,
       -- ����� ��������
       decode(p.party_cd, 'ORIG',       coalesce(wt.rf_orig_birthplace_tx, orig_fl.rf_birthplace_tx, orig.rf_birthplace_tx),
                          'BENEF',      coalesce(wt.rf_benef_birthplace_tx, benef_fl.rf_birthplace_tx, benef.rf_birthplace_tx),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_birthplace_tx, scnd_orig.rf_birthplace_tx),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_birthplace_tx, scnd_benef.rf_birthplace_tx)) as birthplace_tx,
       -- ���
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_kpp_nb, orig.rf_kpp_nb),
                          'BENEF',      nvl(wt.rf_benef_kpp_nb, benef.rf_kpp_nb),
                          'SCND_ORIG',  scnd_orig.rf_kpp_nb,
                          'SCND_BENEF', scnd_benef.rf_kpp_nb) as kpp_nb,
       -- ����������� (��� ������� �����������, � ��� ���� ��������� ����� �����������)
       decode(p.party_cd, 'ORIG',       orig.ctzshp_cntry1_cd,
                          'BENEF',      benef.ctzshp_cntry1_cd,
                          'SCND_ORIG',  scnd_orig.ctzshp_cntry1_cd,
                          'SCND_BENEF', scnd_benef.ctzshp_cntry1_cd) as citizenship_cd,
       -- �������������� �����
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_reg_org_nm, orig.rf_reg_org_nm),
                          'BENEF',      nvl(wt.rf_benef_reg_org_nm, benef.rf_reg_org_nm),
                          'SCND_ORIG',  scnd_orig.rf_reg_org_nm,
                          'SCND_BENEF', scnd_benef.rf_reg_org_nm) as reg_org_nm,
       -- �����
       decode(p.party_cd, 'ORIG',       business.rf_pkg_util.get_list_clsf_code(orig.cust_seq_id,'1'),
                          'BENEF',      business.rf_pkg_util.get_list_clsf_code(benef.cust_seq_id,'1'),
                          'SCND_ORIG',  business.rf_pkg_util.get_list_clsf_code(scnd_orig.cust_seq_id,'1'),
                          'SCND_BENEF', business.rf_pkg_util.get_list_clsf_code(scnd_benef.cust_seq_id,'1')) as okved_nb,
       -- ����
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_okpo_nb, orig.rf_okpo_nb),
                          'BENEF',      nvl(wt.rf_benef_okpo_nb, benef.rf_okpo_nb),
                          'SCND_ORIG',  scnd_orig.rf_okpo_nb,
                          'SCND_BENEF', scnd_benef.rf_okpo_nb) as okpo_nb,
       -- ���� ����������� �������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_reg_dt, orig.rf_reg_dt),
                          'BENEF',      nvl(wt.rf_benef_reg_dt, benef.rf_reg_dt),
                          'SCND_ORIG',  scnd_orig.rf_reg_dt,
                          'SCND_BENEF', scnd_benef.rf_reg_dt) as reg_dt,
       --
       -- ����� �����������
       --
       -- ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.country,  nvl(orig_addr_state.cntry_cd, orig_addr.addr_cntry_cd)),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.country, nvl(benef_addr_state.cntry_cd, benef_addr.addr_cntry_cd)),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.country, nvl(scnd_orig_addr_state.cntry_cd, scnd_orig_addr.addr_cntry_cd)),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.country, nvl(scnd_benef_addr_state.cntry_cd, scnd_benef_addr.addr_cntry_cd))) as adr_country_cd,

       -- ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.post_index, orig_addr.addr_postl_cd),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.post_index, benef_addr.addr_postl_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.post_index, scnd_orig_addr.addr_postl_cd),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.post_index, scnd_benef_addr.addr_postl_cd)) as adr_post_index,
       -- ������������ �������
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.region_nm, nvl(orig_addr_state.object_nm, orig_addr.rf_state_nm)),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.region_nm, nvl(benef_addr_state.object_nm, benef_addr.rf_state_nm)),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_nm, nvl(scnd_orig_addr_state.object_nm, scnd_orig_addr.rf_state_nm)),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.region_nm, nvl(scnd_benef_addr_state.object_nm, scnd_benef_addr.rf_state_nm))) as adr_state_nm,
       -- ��� ������� - ���������� (��������, ���, ����, ��)
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.region_cd, nvl(orig_addr_state.addr_type_cd, orig_addr.rf_state_type_cd)),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.region_cd, nvl(benef_addr_state.addr_type_cd, benef_addr.rf_state_type_cd)),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_cd, nvl(scnd_orig_addr_state.addr_type_cd, scnd_orig_addr.rf_state_type_cd)),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.region_cd, nvl(scnd_benef_addr_state.addr_type_cd, scnd_benef_addr.rf_state_type_cd))) as adr_state_type_cd,
       -- ������ �� ��������� �������� ������, ��������������� ������� 
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.region_addr_seq_id, orig_addr_state.addr_seq_id),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.region_addr_seq_id, benef_addr_state.addr_seq_id),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_addr_seq_id, scnd_orig_addr_state.addr_seq_id),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.region_addr_seq_id, scnd_benef_addr_state.addr_seq_id)) as adr_state_seq_id,
       -- ��� ������ ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.district_cd, orig_addr.rf_rgn_type_cd),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.district_cd, benef_addr.rf_rgn_type_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.district_cd, scnd_orig_addr.rf_rgn_type_cd),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.district_cd, scnd_benef_addr.rf_rgn_type_cd)) as adr_district_type_cd,
       -- ����� ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.district_nm, orig_addr.addr_rgn_nm),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.district_nm, benef_addr.addr_rgn_nm),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.district_nm, scnd_orig_addr.addr_rgn_nm),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.district_nm, scnd_benef_addr.addr_rgn_nm)) as adr_district_nm,
       -- ��� ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.city_cd, orig_addr.rf_city_type_cd),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.city_cd, benef_addr.rf_city_type_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.city_cd, scnd_orig_addr.rf_city_type_cd),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.city_cd, scnd_benef_addr.rf_city_type_cd)) as adr_city_type_cd,
       -- �����
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.city_nm, orig_addr.addr_city_nm),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.city_nm, benef_addr.addr_city_nm),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.city_nm, scnd_orig_addr.addr_city_nm),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.city_nm, scnd_benef_addr.addr_city_nm)) as adr_city_nm,
       -- ��� ����������� ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.settlm_type_cd, orig_addr.rf_settlm_type_cd),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.settlm_type_cd, benef_addr.rf_settlm_type_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.settlm_type_cd, scnd_orig_addr.rf_settlm_type_cd),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.settlm_type_cd, scnd_benef_addr.rf_settlm_type_cd)) as adr_settlm_type_cd,
       -- ���������� �����
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.settlm_nm, orig_addr.rf_settlm_nm),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.settlm_nm, benef_addr.rf_settlm_nm),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.settlm_nm, scnd_orig_addr.rf_settlm_nm),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.settlm_nm, scnd_benef_addr.rf_settlm_nm)) as adr_settlm_nm,
       -- ����� (������������ ��� ����, ��������, ��������)
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.street_nm, orig_addr.addr_strt_line1_tx),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.street_nm, benef_addr.addr_strt_line1_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.street_nm, scnd_orig_addr.addr_strt_line1_tx),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.street_nm, scnd_benef_addr.addr_strt_line1_tx)) as adr_street_name_tx,
       -- ��� ����� - ���������� (��������, ��, �-�, ��-��, ������)
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.street_cd, orig_addr.addr_strt_line2_tx),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.street_cd, benef_addr.addr_strt_line2_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.street_cd, scnd_orig_addr.addr_strt_line2_tx),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.street_cd, scnd_benef_addr.addr_strt_line2_tx)) as adr_street_type_tx,
       -- ����� ����
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.house_nb, orig_addr.addr_strt_line3_tx),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.house_nb, benef_addr.addr_strt_line3_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.house_nb, scnd_orig_addr.addr_strt_line3_tx),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.house_nb, scnd_benef_addr.addr_strt_line3_tx)) as adr_house_number_tx,
       -- ����� �������/��������
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.building, orig_addr.addr_strt_line4_tx),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.building, benef_addr.addr_strt_line4_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.building, scnd_orig_addr.addr_strt_line4_tx),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.building, scnd_benef_addr.addr_strt_line4_tx)) as adr_building_number_tx,
       -- ����� ��������/�����
       decode(p.party_cd, 'ORIG',       decode(orig_addr.rowid, null, orig_addr_f.flat, orig_addr.addr_strt_line5_tx),
                          'BENEF',      decode(benef_addr.rowid, null, benef_addr_f.flat, benef_addr.addr_strt_line5_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.flat, scnd_orig_addr.addr_strt_line5_tx),
                          'SCND_BENEF', decode(scnd_benef_addr.rowid, null, scnd_benef_addr_f.flat, scnd_benef_addr.addr_strt_line5_tx)) as adr_flat_number_tx,
       -- ���� (0/1) �������������������� ������ �����������
       decode(p.party_cd, 'ORIG',       case when orig_addr.rowid is null or
                                                  (orig_addr.rf_addr_tx is not null and
                                                   orig_addr.addr_city_nm||orig_addr.rf_settlm_nm||orig_addr.addr_strt_line1_tx is null)
                                             then 1 else 0 end,
                          'BENEF',      case when benef_addr.rowid is null or
                                                  (benef_addr.rf_addr_tx is not null and
                                                   benef_addr.addr_city_nm||benef_addr.rf_settlm_nm||benef_addr.addr_strt_line1_tx is null)
                                             then 1 else 0 end,
                          'SCND_ORIG',  case when scnd_orig_addr.rowid is null or
                                                  (scnd_orig_addr.rf_addr_tx is not null and
                                                   scnd_orig_addr.addr_city_nm||scnd_orig_addr.rf_settlm_nm||scnd_orig_addr.addr_strt_line1_tx is null)
                                             then 1 else 0 end,
                          'SCND_BENEF', case when scnd_benef_addr.rowid is null or
                                                  (scnd_benef_addr.rf_addr_tx is not null and
                                                   scnd_benef_addr.addr_city_nm||scnd_benef_addr.rf_settlm_nm||scnd_benef_addr.addr_strt_line1_tx is null)
                                             then 1 else 0 end) as adr_unstruct_fl,
       -- ����� �������������������� ������ �����������
       decode(p.party_cd, 'ORIG',       nvl(orig_addr.rf_addr_tx,       wt.rf_orig_reg_addr_tx),
                          'BENEF',      nvl(benef_addr.rf_addr_tx,      wt.rf_benef_reg_addr_tx),
                          'SCND_ORIG',  nvl(scnd_orig_addr.rf_addr_tx,  wt.rf_scnd_orig_reg_addr_tx),
                          'SCND_BENEF', nvl(scnd_benef_addr.rf_addr_tx, wt.rf_scnd_benef_reg_addr_tx)) as adr_unstruct_tx,
       -- ������ �� ������ ����������� ������� - ���. ����, ���������������� ��
       decode(p.party_cd, 'ORIG',       orig_fl_addr.addr_cntry_cd,
                          'BENEF',      benef_fl_addr.addr_cntry_cd,
                          'SCND_ORIG',  to_char(null),
                          'SCND_BENEF', to_char(null)) as fl_adr_country_cd,
       --
       -- ����� ����� ���������� 
       --
       -- ����: ���������� � ����� ����� ���������� ����� �����������
       decode(p.party_cd, 'ORIG',       case when /*orig.cust_type_cd <> 'IND' and*/ orig_addr_h.rowid is null and wt.rf_orig_fact_addr_tx is null then 1 else 0 end,
                          'BENEF',      case when /*benef.cust_type_cd <> 'IND' and*/ benef_addr_h.rowid is null and wt.rf_benef_fact_addr_tx is null then 1 else 0 end,
                          'SCND_ORIG',  case when /*scnd_orig.cust_type_cd <> 'IND' and*/ scnd_orig_addr_h.rowid is null and wt.rf_scnd_orig_fact_addr_tx is null then 1 else 0 end,
                          'SCND_BENEF', case when /*scnd_benef.cust_type_cd <> 'IND' and*/ scnd_benef_addr_h.rowid is null and wt.rf_scnd_benef_fact_addr_tx is null then 1 else 0 end) as adh_from_adr_fl,
       -- ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.country, nvl(orig_addr_state_h.cntry_cd, orig_addr_h.addr_cntry_cd)),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.country, nvl(benef_addr_state_h.cntry_cd, benef_addr_h.addr_cntry_cd)),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.country, nvl(scnd_orig_addr_state_h.cntry_cd, scnd_orig_addr_h.addr_cntry_cd)),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.country, nvl(scnd_benef_addr_state_h.cntry_cd, scnd_benef_addr_h.addr_cntry_cd))) as adh_country_cd,
       -- ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.post_index, orig_addr_h.addr_postl_cd),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.post_index, benef_addr_h.addr_postl_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.post_index, scnd_orig_addr_h.addr_postl_cd),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.post_index, scnd_benef_addr_h.addr_postl_cd)) as adh_post_index,
       -- ������������ �������
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.region_nm, nvl(orig_addr_state_h.object_nm, orig_addr_h.rf_state_nm)),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.region_nm, nvl(benef_addr_state_h.object_nm, benef_addr_h.rf_state_nm)),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.region_nm, nvl(scnd_orig_addr_state_h.object_nm, scnd_orig_addr_h.rf_state_nm)),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.region_nm, nvl(scnd_benef_addr_state_h.object_nm, scnd_benef_addr_h.rf_state_nm))) as adh_state_nm,
       -- ��� ������� - ���������� (��������, ���, ����, ��)
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.region_cd, nvl(orig_addr_state_h.addr_type_cd, orig_addr_h.rf_state_type_cd)),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.region_cd, nvl(benef_addr_state_h.addr_type_cd, benef_addr_h.rf_state_type_cd)),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.region_cd, nvl(scnd_orig_addr_state_h.addr_type_cd, scnd_orig_addr_h.rf_state_type_cd)),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.region_cd, nvl(scnd_benef_addr_state_h.addr_type_cd, scnd_benef_addr_h.rf_state_type_cd))) as adh_state_type_cd,
       -- ������ �� ��������� �������� ������, ��������������� ������� 
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.region_addr_seq_id, orig_addr_state_h.addr_seq_id),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.region_addr_seq_id, benef_addr_state_h.addr_seq_id),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.region_addr_seq_id, scnd_orig_addr_state_h.addr_seq_id),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.region_addr_seq_id, scnd_benef_addr_state_h.addr_seq_id)) as adh_state_seq_id,
       -- ��� ������ ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.district_cd, orig_addr_h.rf_rgn_type_cd),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.district_cd, benef_addr_h.rf_rgn_type_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.district_cd, scnd_orig_addr_h.rf_rgn_type_cd),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.district_cd, scnd_benef_addr_h.rf_rgn_type_cd)) as adh_district_type_cd,
       -- ����� ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.district_nm, orig_addr_h.addr_rgn_nm),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.district_nm, benef_addr_h.addr_rgn_nm),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.district_nm, scnd_orig_addr_h.addr_rgn_nm),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.district_nm, scnd_benef_addr_h.addr_rgn_nm)) as adh_district_nm,
       -- ��� ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.city_cd, orig_addr_h.rf_city_type_cd),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.city_cd, benef_addr_h.rf_city_type_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.city_cd, scnd_orig_addr_h.rf_city_type_cd),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.city_cd, scnd_benef_addr_h.rf_city_type_cd)) as adh_city_type_cd,
       -- �����
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.city_nm, orig_addr_h.addr_city_nm),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.city_nm, benef_addr_h.addr_city_nm),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.city_nm, scnd_orig_addr_h.addr_city_nm),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.city_nm, scnd_benef_addr_h.addr_city_nm)) as adh_city_nm,
       -- ��� ����������� ������
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.settlm_type_cd, orig_addr_h.rf_settlm_type_cd),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.settlm_type_cd, benef_addr_h.rf_settlm_type_cd),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.settlm_type_cd, scnd_orig_addr_h.rf_settlm_type_cd),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.settlm_type_cd, scnd_benef_addr_h.rf_settlm_type_cd)) as adh_settlm_type_cd,
       -- ���������� �����
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.settlm_nm, orig_addr_h.rf_settlm_nm),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.settlm_nm, benef_addr_h.rf_settlm_nm),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.settlm_nm, scnd_orig_addr_h.rf_settlm_nm),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.settlm_nm, scnd_benef_addr_h.rf_settlm_nm)) as adh_settlm_nm,
       -- ����� (������������ ��� ����, ��������, ��������)
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.street_nm, orig_addr_h.addr_strt_line1_tx),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.street_nm, benef_addr_h.addr_strt_line1_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.street_nm, scnd_orig_addr_h.addr_strt_line1_tx),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.street_nm, scnd_benef_addr_h.addr_strt_line1_tx)) as adh_street_name_tx,
       -- ��� ����� - ���������� (��������, ��, �-�, ��-��, ������)
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.street_cd, orig_addr_h.addr_strt_line2_tx),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.street_cd, benef_addr_h.addr_strt_line2_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.street_cd, scnd_orig_addr_h.addr_strt_line2_tx),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.street_cd, scnd_benef_addr_h.addr_strt_line2_tx)) as adh_street_type_tx,
       -- ����� ����
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.house_nb, orig_addr_h.addr_strt_line3_tx),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.house_nb, benef_addr_h.addr_strt_line3_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.house_nb, scnd_orig_addr_h.addr_strt_line3_tx),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.house_nb, scnd_benef_addr_h.addr_strt_line3_tx)) as adh_house_number_tx,
       -- ����� �������/��������
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.building, orig_addr_h.addr_strt_line4_tx),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.building, benef_addr_h.addr_strt_line4_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.building, scnd_orig_addr_h.addr_strt_line4_tx),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.building, scnd_benef_addr_h.addr_strt_line4_tx)) as adh_building_number_tx,
       -- ����� ��������/�����
       decode(p.party_cd, 'ORIG',       decode(orig_addr_h.rowid, null, orig_addr_h_f.flat, orig_addr_h.addr_strt_line5_tx),
                          'BENEF',      decode(benef_addr_h.rowid, null, benef_addr_h_f.flat, benef_addr_h.addr_strt_line5_tx),
                          'SCND_ORIG',  decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_h_f.flat, scnd_orig_addr_h.addr_strt_line5_tx),
                          'SCND_BENEF', decode(scnd_benef_addr_h.rowid, null, scnd_benef_addr_h_f.flat, scnd_benef_addr_h.addr_strt_line5_tx)) as adh_flat_number_tx,
       -- ���� (0/1) �������������������� ������ ����� ����������
       decode(p.party_cd, 'ORIG',       case when orig_addr_h.rowid is null or
                                                  (orig_addr_h.rf_addr_tx is not null and
                                                   orig_addr_h.addr_city_nm||orig_addr_h.rf_settlm_nm||orig_addr_h.addr_strt_line1_tx is null)
                                             then 1 else 0 end,
                          'BENEF',      case when benef_addr_h.rowid is null or
                                                  (benef_addr_h.rf_addr_tx is not null and
                                                   benef_addr_h.addr_city_nm||benef_addr_h.rf_settlm_nm||benef_addr_h.addr_strt_line1_tx is null)
                                             then 1 else 0 end,
                          'SCND_ORIG',  case when scnd_orig_addr_h.rowid is null or
                                                  (scnd_orig_addr_h.rf_addr_tx is not null and
                                                   scnd_orig_addr_h.addr_city_nm||scnd_orig_addr_h.rf_settlm_nm||scnd_orig_addr_h.addr_strt_line1_tx is null)
                                             then 1 else 0 end,
                          'SCND_BENEF', case when scnd_benef_addr_h.rowid is null or
                                                  (scnd_benef_addr_h.rf_addr_tx is not null and
                                                   scnd_benef_addr_h.addr_city_nm||scnd_benef_addr_h.rf_settlm_nm||scnd_benef_addr_h.addr_strt_line1_tx is null)
                                             then 1 else 0 end) as adh_unstruct_fl,
       -- ����� �������������������� ������ ����� ����������
       decode(p.party_cd, 'ORIG',       nvl(orig_addr_h.rf_addr_tx,       wt.rf_orig_fact_addr_tx),
                          'BENEF',      nvl(benef_addr_h.rf_addr_tx,      wt.rf_benef_fact_addr_tx),
                          'SCND_ORIG',  nvl(scnd_orig_addr_h.rf_addr_tx,  wt.rf_scnd_orig_fact_addr_tx),
                          'SCND_BENEF', nvl(scnd_benef_addr_h.rf_addr_tx, wt.rf_scnd_benef_fact_addr_tx)) as adh_unstruct_tx,
       --
       -- �������������� ��������
       --
       -- ��� ���������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_doc_type_cd, orig_doc.doc_type_cd),
                          'BENEF',      nvl(wt.rf_benef_doc_type_cd, benef_doc.doc_type_cd),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_doc_type_cd, scnd_orig_doc.doc_type_cd),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_doc_type_cd, scnd_benef_doc.doc_type_cd)) as doc_type_cd,
       -- ����� ���������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_doc_series_id, orig_doc.series_id),
                          'BENEF',      nvl(wt.rf_benef_doc_series_id, benef_doc.series_id),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_doc_series_id, scnd_orig_doc.series_id),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_doc_series_id, scnd_benef_doc.series_id)) as document_series,
       -- ����� ���������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_doc_no_id, orig_doc.no_id),
                          'BENEF',      nvl(wt.rf_benef_doc_no_id, benef_doc.no_id),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_doc_no_id, scnd_orig_doc.no_id),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_doc_no_id, scnd_benef_doc.no_id)) as document_no,
       -- ����� �������� ��������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_doc_issued_by_tx, orig_doc.issued_by_tx),
                          'BENEF',      nvl(wt.rf_benef_doc_issued_by_tx, benef_doc.issued_by_tx),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_doc_issued_by_tx, scnd_orig_doc.issued_by_tx),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_doc_issued_by_tx, scnd_benef_doc.issued_by_tx)) as document_issued_dpt_tx,
       -- ��� �������������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_doc_dpt_cd,orig_doc.issue_dpt_cd),
                          'BENEF',      nvl(wt.rf_benef_doc_dpt_cd,benef_doc.issue_dpt_cd),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_doc_dpt_cd,scnd_orig_doc.issue_dpt_cd),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_doc_dpt_cd,scnd_benef_doc.issue_dpt_cd)) as document_issued_dpt_cd,
       -- ���� ������ ���������
       decode(p.party_cd, 'ORIG',       nvl(wt.rf_orig_doc_issue_dt, orig_doc.issue_dt),
                          'BENEF',      nvl(wt.rf_benef_doc_issue_dt, benef_doc.issue_dt),
                          'SCND_ORIG',  nvl(wt.rf_scnd_orig_doc_issue_dt,scnd_orig_doc.issue_dt),
                          'SCND_BENEF', nvl(wt.rf_scnd_benef_doc_issue_dt, scnd_benef_doc.issue_dt)) as document_issued_dt,
       -- ���� ��������� �������� ���������
       decode(p.party_cd, 'ORIG',       orig_doc.end_dt,
                          'BENEF',      benef_doc.end_dt,
                          'SCND_ORIG',  scnd_orig_doc.end_dt,
                          'SCND_BENEF', scnd_benef_doc.end_dt) as document_end_dt,
       -- ����������� �� ��������?
       decode(p.party_cd, 'ORIG',       orig_doc.valid_fl,
                          'BENEF',      benef_doc.valid_fl,
                          'SCND_ORIG',  scnd_orig_doc.valid_fl,
                          'SCND_BENEF', scnd_benef_doc.valid_fl) as valid_fl,
       --
       -- �������������� �������������� ���������
       --
       -- ��������, �������������� ����� �� ���������� - ??? �������� ???
       decode(p.party_cd, 'ORIG',       to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, null, 'Y')),
                          'BENEF',      to_number(business.rf_pkg_util.get_id_doc(benef.cust_seq_id, null, 'Y')),
                          'SCND_ORIG',  to_number(business.rf_pkg_util.get_id_doc(scnd_orig.cust_seq_id, null, 'Y')),
                          'SCND_BENEF', to_number(business.rf_pkg_util.get_id_doc(scnd_benef.cust_seq_id, null, 'Y'))) as stay_cust_doc_seq_id,
       -- ������������ ����� - ??? �������� ???
       decode(p.party_cd, 'ORIG',       to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, 39)),
                          'BENEF',      to_number(business.rf_pkg_util.get_id_doc(benef.cust_seq_id, 39)),
                          'SCND_ORIG',  to_number(business.rf_pkg_util.get_id_doc(scnd_orig.cust_seq_id, 39)),
                          'SCND_BENEF', to_number(business.rf_pkg_util.get_id_doc(scnd_benef.cust_seq_id, 39))) as mc_cust_doc_seq_id,
       -- ����� ����������� ������� �� ��������
       decode(p.party_cd, 'ORIG',       wt.rf_orig_reg_addr_tx,
                          'BENEF',      wt.rf_benef_reg_addr_tx,
                          'SCND_ORIG',  wt.rf_scnd_orig_reg_addr_tx,
                          'SCND_BENEF', wt.rf_scnd_benef_reg_addr_tx) as regist_addr_string_trxn,
       -- ����� ���������� ������� �� ��������
       decode(p.party_cd, 'ORIG',       wt.rf_orig_fact_addr_tx,
                          'BENEF',      wt.rf_benef_fact_addr_tx,
                          'SCND_ORIG',  wt.rf_scnd_orig_fact_addr_tx,
                          'SCND_BENEF', wt.rf_scnd_benef_fact_addr_tx) as fact_addr_string_trxn,
       -- ����� ����������� ������� �� ������� � ��������
       decode(p.party_cd, 'ORIG',       business.rf_pkg_util.get_addr_text(orig_addr.cust_addr_seq_id),
                          'BENEF',      business.rf_pkg_util.get_addr_text(benef_addr.cust_addr_seq_id),
                          'SCND_ORIG',  business.rf_pkg_util.get_addr_text(scnd_orig_addr.cust_addr_seq_id),
                          'SCND_BENEF', business.rf_pkg_util.get_addr_text(scnd_benef_addr.cust_addr_seq_id)) as regist_addr_string_custaddr,
       -- ����� ���������� ������� �� ������� � ��������
       decode(p.party_cd, 'ORIG',       business.rf_pkg_util.get_addr_text(orig_addr_h.cust_addr_seq_id),
                          'BENEF',      business.rf_pkg_util.get_addr_text(benef_addr_h.cust_addr_seq_id),
                          'SCND_ORIG',  business.rf_pkg_util.get_addr_text(scnd_orig_addr_h.cust_addr_seq_id),
                          'SCND_BENEF', business.rf_pkg_util.get_addr_text(scnd_benef_addr_h.cust_addr_seq_id)) as fact_addr_string_custaddr,
       -- ���������� ������������� �����. ����������� ������ ��� "����������" ������, ������������� ������� BUSINESS.ACCT.ACCT_SEQ_ID
       decode(p.party_cd, 'ORIG',       to_number(wt.rf_orig_acct_seq_id),
                          'BENEF',      wt.rf_benef_acct_seq_id,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as acct_seq_id,
       -- ����� �����
       decode(p.party_cd, 'ORIG',       nullif(wt.rf_orig_acct_nb, '-1'),
                          'BENEF',      nullif(wt.rf_benef_acct_nb, '-1'),
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as acct_nb,
       -- ��� �������� ���
       decode(p.party_cd, 'ORIG',       wt.rf_orig_goz_op_cd,
                          'BENEF',      wt.rf_benef_goz_op_cd,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as goz_op_cd,
       -- ���� ������ ��� �������, ��������� � ������ ���������
       decode(p.party_cd, 'ORIG',       wt.rf_debit_cd,
                          'BENEF',      wt.rf_credit_cd,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as deb_cred_acct_nb,
       -- ������������ �����
       decode(p.party_cd, 'ORIG',       coalesce(wt.send_instn_nm, orig_tb_cust.full_nm, orig_tb.org_nm),
                          'BENEF',      coalesce(wt.rcv_instn_nm, benef_tb_cust.full_nm, benef_tb.org_nm),
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_nm,
       -- ���/SWIFT �����
       decode(p.party_cd, 'ORIG',       coalesce(wt.send_instn_id, orig_tb_cust.rf_bic_nb, orig_tb.cstm_2_tx),
                          'BENEF',      coalesce(wt.rcv_instn_id, benef_tb_cust.rf_bic_nb, benef_tb.cstm_2_tx),
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_id,
       -- ������ �����
       decode(p.party_cd, 'ORIG',       coalesce(wt.rf_send_instn_cntry_cd,
                                                 case when regexp_like(trim(wt.send_instn_id),'^04\d{7}$') or 
                                                           orig_tb.rowid is not null
                                                      then 'RU' 
                                                      when length(trim(wt.send_instn_id)) in (8, 11) 
                                                      then substr(trim(wt.send_instn_id),5,2)
                                                 end),
                          'BENEF',      coalesce(wt.rf_rcv_instn_cntry_cd,
                                                 case when regexp_like(trim(wt.rcv_instn_id),'^04\d{7}$') or 
                                                           benef_tb.rowid is not null 
                                                      then 'RU' 
                                                      when length(trim(wt.rcv_instn_id)) in (8, 11) 
                                                      then substr(trim(wt.rcv_instn_id),5,2)
                                                 end),
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_cntry_cd,
       -- ����. ���� �����
       decode(p.party_cd, 'ORIG',       coalesce(wt.send_instn_acct_id, orig_tb.cstm_3_tx),
                          'BENEF',      coalesce(wt.rcv_instn_acct_id, benef_tb.cstm_3_tx),
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_acct_id,
       -- ��� ��/��� �� �����
       decode(p.party_cd, 'ORIG',       orig_acct.rf_branch_id,
                          'BENEF',      benef_acct.rf_branch_id,
                          'SCND_ORIG',  orig_acct.rf_branch_id,
                          'SCND_BENEF', benef_acct.rf_branch_id) as branch_id,
       -- ID ������� �����
       decode(p.party_cd, 'ORIG',       send_org.oes_321_org_id,
                          'BENEF',      rcv_org.oes_321_org_id,
                          'SCND_ORIG',  send_org.oes_321_org_id,
                          'SCND_BENEF', rcv_org.oes_321_org_id) as oes_org_id,
       -- ������������ ������� �����
       decode(p.party_cd, 'ORIG',       send_org.tb_name,
                          'BENEF',      rcv_org.tb_name,
                          'SCND_ORIG',  send_org.tb_name,
                          'SCND_BENEF', rcv_org.tb_name) as kgrko_nm,
       -- ����� ������� ����� (�������, ���� �������� ��)
       decode(p.party_cd, 'ORIG',       case when send_org.oes_321_org_id is not null
                                             then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                                                 '1', nullif(send_org.numbf_ss, '0')), '-') 
                                        end,
                          'BENEF',      case when rcv_org.oes_321_org_id is not null
                                             then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                                                '1', nullif(rcv_org.numbf_ss, '0')), '-') 
                                        end,
                          'SCND_ORIG',  case when send_org.oes_321_org_id is not null
                                             then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                                                 '1', nullif(send_org.numbf_ss, '0')), '-') 
                                        end,
                          'SCND_BENEF', case when rcv_org.oes_321_org_id is not null
                                             then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                                                '1', nullif(rcv_org.numbf_ss, '0')), '-') 
                                        end) as kgrko_id,       
       -- ����� ���������� �����
       decode(p.party_cd, 'ORIG',       wt.bank_card_id_nb,
                          'BENEF',      to_number(null),
                          'SCND_ORIG',  to_number(null),
                          'SCND_BENEF', to_number(null)) as bank_card_id_nb,
       -- ���� �������� �� ���������� �����
       decode(p.party_cd, 'ORIG',       wt.cstm_1_dt,
                          'BENEF',      to_date(null),
                          'SCND_ORIG',  to_date(null),
                          'SCND_BENEF', to_date(null)) as bank_card_trxn_dt,
      -----------------------------------
       wt.src_sys_cd as src_sys_cd

    from -- �������� �������� ������ �������� �� ��������� �������� ����, � ���� ����� ��� - ������� ������ ��������
         (select /*+ INDEX(wire_trxn PK_WIRE_TRXN)*/
                 fo_trxn_seq_id, trxn_exctn_dt,
                 rf_orig_cust_seq_id, rf_benef_cust_seq_id, rf_scnd_orig_cust_seq_id, rf_scnd_benef_cust_seq_id,
                 rf_orig_resident_fl, rf_benef_resident_fl, rf_scnd_orig_resident_fl, rf_scnd_benef_resident_fl,
                 orig_nm, benef_nm, scnd_orig_nm, scnd_benef_nm, 
                 rf_orig_midl_nm, rf_benef_midl_nm, rf_scnd_orig_midl_nm, rf_scnd_benef_midl_nm, 
                 rf_orig_first_nm, rf_benef_first_nm, rf_scnd_orig_first_nm, rf_scnd_benef_first_nm,
                 rf_orig_add_info_tx, rf_benef_add_info_tx, 
                 rf_orig_gndr_cd, rf_benef_gndr_cd, rf_scnd_orig_gndr_cd, rf_scnd_benef_gndr_cd,
                 rf_orig_inn_nb, rf_benef_inn_nb, 
                 rf_orig_ogrn_nb, rf_benef_ogrn_nb,
                 rf_orig_birth_dt, rf_benef_birth_dt, rf_scnd_orig_birth_dt, rf_scnd_benef_birth_dt,
                 rf_orig_birthplace_tx, rf_benef_birthplace_tx, rf_scnd_orig_birthplace_tx, rf_scnd_benef_birthplace_tx,
                 rf_orig_kpp_nb, rf_benef_kpp_nb,
                 rf_orig_reg_org_nm, rf_benef_reg_org_nm, 
                 rf_orig_okpo_nb, rf_benef_okpo_nb,
                 rf_orig_reg_dt, rf_benef_reg_dt,
                 rf_orig_reg_addr_tx, rf_benef_reg_addr_tx, rf_scnd_orig_reg_addr_tx, rf_scnd_benef_reg_addr_tx,
                 rf_orig_fact_addr_tx, rf_benef_fact_addr_tx, rf_scnd_orig_fact_addr_tx, rf_scnd_benef_fact_addr_tx,
                 rf_orig_doc_type_cd, rf_benef_doc_type_cd, rf_scnd_orig_doc_type_cd, rf_scnd_benef_doc_type_cd,
                 rf_orig_doc_series_id, rf_benef_doc_series_id, rf_scnd_orig_doc_series_id, rf_scnd_benef_doc_series_id, 
                 rf_orig_doc_no_id, rf_benef_doc_no_id, rf_scnd_orig_doc_no_id, rf_scnd_benef_doc_no_id,
                 rf_orig_doc_issued_by_tx, rf_benef_doc_issued_by_tx, rf_scnd_orig_doc_issued_by_tx, rf_scnd_benef_doc_issued_by_tx,
                 rf_orig_doc_dpt_cd, rf_benef_doc_dpt_cd, rf_scnd_orig_doc_dpt_cd, rf_scnd_benef_doc_dpt_cd,
                 rf_orig_doc_issue_dt, rf_benef_doc_issue_dt, rf_scnd_orig_doc_issue_dt, rf_scnd_benef_doc_issue_dt,
                 rf_orig_acct_seq_id, rf_benef_acct_seq_id, 
                 rf_orig_acct_nb, rf_benef_acct_nb,
                 rf_orig_goz_op_cd, rf_benef_goz_op_cd,
                 rf_debit_cd, rf_credit_cd,
                 send_instn_nm, rcv_instn_nm,
                 send_instn_id, rcv_instn_id,
                 rf_send_instn_cntry_cd, rf_rcv_instn_cntry_cd,  
                 send_instn_acct_id, rcv_instn_acct_id,
                 bank_card_id_nb, cstm_1_dt, src_sys_cd
            from business.wire_trxn        
           where fo_trxn_seq_id = p_trxn_id and
                 not exists(select null 
                              from business.wire_trxn_arh
                             where fo_trxn_seq_id = p_trxn_id and
                                   arh_dump_dt = p_arh_dump_dt)
          union all
          select /*+ INDEX(wire_trxn_arh PK_WIRE_TRXN_ARH)*/
                 fo_trxn_seq_id, trxn_exctn_dt, 
                 rf_orig_cust_seq_id, rf_benef_cust_seq_id, rf_scnd_orig_cust_seq_id, rf_scnd_benef_cust_seq_id,
                 rf_orig_resident_fl, rf_benef_resident_fl, rf_scnd_orig_resident_fl, rf_scnd_benef_resident_fl,
                 orig_nm, benef_nm, scnd_orig_nm, scnd_benef_nm, 
                 rf_orig_midl_nm, rf_benef_midl_nm, rf_scnd_orig_midl_nm, rf_scnd_benef_midl_nm, 
                 rf_orig_first_nm, rf_benef_first_nm, rf_scnd_orig_first_nm, rf_scnd_benef_first_nm,
                 rf_orig_add_info_tx, rf_benef_add_info_tx, 
                 rf_orig_gndr_cd, rf_benef_gndr_cd, rf_scnd_orig_gndr_cd, rf_scnd_benef_gndr_cd,
                 rf_orig_inn_nb, rf_benef_inn_nb, 
                 rf_orig_ogrn_nb, rf_benef_ogrn_nb,
                 rf_orig_birth_dt, rf_benef_birth_dt, rf_scnd_orig_birth_dt, rf_scnd_benef_birth_dt,
                 rf_orig_birthplace_tx, rf_benef_birthplace_tx, rf_scnd_orig_birthplace_tx, rf_scnd_benef_birthplace_tx,
                 rf_orig_kpp_nb, rf_benef_kpp_nb,
                 rf_orig_reg_org_nm, rf_benef_reg_org_nm, 
                 rf_orig_okpo_nb, rf_benef_okpo_nb,
                 rf_orig_reg_dt, rf_benef_reg_dt,
                 rf_orig_reg_addr_tx, rf_benef_reg_addr_tx, rf_scnd_orig_reg_addr_tx, rf_scnd_benef_reg_addr_tx,
                 rf_orig_fact_addr_tx, rf_benef_fact_addr_tx, rf_scnd_orig_fact_addr_tx, rf_scnd_benef_fact_addr_tx,
                 rf_orig_doc_type_cd, rf_benef_doc_type_cd, rf_scnd_orig_doc_type_cd, rf_scnd_benef_doc_type_cd,
                 rf_orig_doc_series_id, rf_benef_doc_series_id, rf_scnd_orig_doc_series_id, rf_scnd_benef_doc_series_id, 
                 rf_orig_doc_no_id, rf_benef_doc_no_id, rf_scnd_orig_doc_no_id, rf_scnd_benef_doc_no_id,
                 rf_orig_doc_issued_by_tx, rf_benef_doc_issued_by_tx, rf_scnd_orig_doc_issued_by_tx, rf_scnd_benef_doc_issued_by_tx,
                 rf_orig_doc_dpt_cd, rf_benef_doc_dpt_cd, rf_scnd_orig_doc_dpt_cd, rf_scnd_benef_doc_dpt_cd,
                 rf_orig_doc_issue_dt, rf_benef_doc_issue_dt, rf_scnd_orig_doc_issue_dt, rf_scnd_benef_doc_issue_dt,
                 rf_orig_acct_seq_id, rf_benef_acct_seq_id, 
                 rf_orig_acct_nb, rf_benef_acct_nb,
                 rf_orig_goz_op_cd, rf_benef_goz_op_cd,
                 rf_debit_cd, rf_credit_cd,
                 send_instn_nm, rcv_instn_nm,
                 send_instn_id, rcv_instn_id, 
                 rf_send_instn_cntry_cd, rf_rcv_instn_cntry_cd,  
                 send_instn_acct_id, rcv_instn_acct_id,
                 bank_card_id_nb, cstm_1_dt, src_sys_cd
            from business.wire_trxn_arh
           where fo_trxn_seq_id = p_trxn_id and
                 arh_dump_dt = p_arh_dump_dt) wt
       left join business.cust orig on orig.cust_seq_id = wt.rf_orig_cust_seq_id
       left join business.cust orig_fl on orig_fl.cust_seq_id = orig.rf_ind_cust_seq_id
       left join business.cust benef on benef.cust_seq_id = wt.rf_benef_cust_seq_id
       left join business.cust benef_fl on benef_fl.cust_seq_id = benef.rf_ind_cust_seq_id
       left join business.cust scnd_orig on scnd_orig.cust_seq_id =
                 nvl(wt.rf_scnd_orig_cust_seq_id, business.rf_pkg_util.get_representative(wt.rf_orig_cust_seq_id,wt.rf_scnd_orig_doc_type_cd, wt.rf_scnd_orig_doc_series_id, wt.rf_scnd_orig_doc_no_id))
       left join business.cust scnd_benef on scnd_benef.cust_seq_id =
                 nvl(wt.rf_scnd_benef_cust_seq_id, business.rf_pkg_util.get_representative(wt.rf_benef_cust_seq_id,wt.rf_scnd_benef_doc_type_cd, wt.rf_scnd_benef_doc_series_id, wt.rf_scnd_benef_doc_no_id))
       left join business.cust_addr orig_addr on orig_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(wt.rf_orig_cust_seq_id, 'L')
       left join business.cust_addr orig_fl_addr on orig_fl_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(orig_fl.cust_seq_id, 'L')   
       left join business.rf_addr_object orig_addr_state on orig_addr_state.addr_seq_id = coalesce(orig_addr.rf_state_addr_seq_id,
                                                                                                   case when orig_addr.rf_state_nm is not null
                                                                                                        then std.aml_tools.recognise_region2(orig_addr.rf_state_nm)
                                                                                                   end,
                                                                                                   case when orig_addr.addr_city_nm is not null
                                                                                                        then std.aml_tools.recognise_region2(orig_addr.addr_city_nm)
                                                                                                   end)
       left join business.cust_addr benef_addr on benef_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(wt.rf_benef_cust_seq_id, 'L')
       left join business.cust_addr benef_fl_addr on benef_fl_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(benef_fl.cust_seq_id, 'L')
       left join business.rf_addr_object benef_addr_state on benef_addr_state.addr_seq_id = coalesce(benef_addr.rf_state_addr_seq_id,
                                                                                                     case when benef_addr.rf_state_nm is not null
                                                                                                          then std.aml_tools.recognise_region2(benef_addr.rf_state_nm)
                                                                                                     end,
                                                                                                     case when benef_addr.addr_city_nm is not null
                                                                                                          then std.aml_tools.recognise_region2(benef_addr.addr_city_nm)
                                                                                                     end)
       left join business.cust_addr scnd_orig_addr on scnd_orig_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(scnd_orig.cust_seq_id, 'L')
       left join business.rf_addr_object scnd_orig_addr_state on scnd_orig_addr_state.addr_seq_id = coalesce(scnd_orig_addr.rf_state_addr_seq_id,
                                                                                                             case when scnd_orig_addr.rf_state_nm is not null
                                                                                                                  then std.aml_tools.recognise_region2(scnd_orig_addr.rf_state_nm)
                                                                                                             end,
                                                                                                             case when scnd_orig_addr.addr_city_nm is not null
                                                                                                                  then std.aml_tools.recognise_region2(scnd_orig_addr.addr_city_nm)
                                                                                                             end)
       left join business.cust_addr scnd_benef_addr on scnd_benef_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(scnd_benef.cust_seq_id, 'L')
       left join business.rf_addr_object scnd_benef_addr_state on scnd_benef_addr_state.addr_seq_id = coalesce(scnd_benef_addr.rf_state_addr_seq_id,
                                                                                                               case when scnd_benef_addr.rf_state_nm is not null
                                                                                                                    then std.aml_tools.recognise_region2(scnd_benef_addr.rf_state_nm)
                                                                                                               end,
                                                                                                               case when scnd_benef_addr.addr_city_nm is not null
                                                                                                                    then std.aml_tools.recognise_region2(scnd_benef_addr.addr_city_nm)
                                                                                                               end)
       left join business.cust_addr orig_addr_h on orig_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(wt.rf_orig_cust_seq_id, 'H')
       left join business.rf_addr_object orig_addr_state_h on orig_addr_state_h.addr_seq_id = coalesce(orig_addr_h.rf_state_addr_seq_id,
                                                                                                       case when orig_addr_h.rf_state_nm is not null
                                                                                                            then std.aml_tools.recognise_region2(orig_addr_h.rf_state_nm)
                                                                                                       end,
                                                                                                       case when orig_addr_h.addr_city_nm is not null
                                                                                                            then std.aml_tools.recognise_region2(orig_addr_h.addr_city_nm)
                                                                                                       end)
       left join business.cust_addr benef_addr_h on benef_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(wt.rf_benef_cust_seq_id, 'H')
       left join business.rf_addr_object benef_addr_state_h on benef_addr_state_h.addr_seq_id = coalesce(benef_addr_h.rf_state_addr_seq_id,
                                                                                                         case when benef_addr_h.rf_state_nm is not null
                                                                                                              then std.aml_tools.recognise_region2(benef_addr_h.rf_state_nm)
                                                                                                         end,
                                                                                                         case when benef_addr_h.addr_city_nm is not null
                                                                                                              then std.aml_tools.recognise_region2(benef_addr_h.addr_city_nm)
                                                                                                         end)
       left join business.cust_addr scnd_orig_addr_h on scnd_orig_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(scnd_orig.cust_seq_id, 'H')
       left join business.rf_addr_object scnd_orig_addr_state_h on scnd_orig_addr_state_h.addr_seq_id = coalesce(scnd_orig_addr_h.rf_state_addr_seq_id,
                                                                                                                 case when scnd_orig_addr_h.rf_state_nm is not null
                                                                                                                      then std.aml_tools.recognise_region2(scnd_orig_addr_h.rf_state_nm)
                                                                                                                 end,
                                                                                                                 case when scnd_orig_addr_h.addr_city_nm is not null
                                                                                                                      then std.aml_tools.recognise_region2(scnd_orig_addr_h.addr_city_nm)
                                                                                                                 end)
       left join business.cust_addr scnd_benef_addr_h on scnd_benef_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(scnd_benef.cust_seq_id, 'H')
       left join business.rf_addr_object scnd_benef_addr_state_h on scnd_benef_addr_state_h.addr_seq_id = coalesce(scnd_benef_addr_h.rf_state_addr_seq_id,
                                                                                                                   case when scnd_benef_addr_h.rf_state_nm is not null
                                                                                                                        then std.aml_tools.recognise_region2(scnd_benef_addr_h.rf_state_nm)
                                                                                                                   end,
                                                                                                                   case when scnd_benef_addr_h.addr_city_nm is not null
                                                                                                                        then std.aml_tools.recognise_region2(scnd_benef_addr_h.addr_city_nm)
                                                                                                                   end)
       left join business.rf_cust_id_doc orig_doc on orig_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(wt.rf_orig_cust_seq_id, wt.rf_orig_doc_type_cd, wt.rf_orig_doc_series_id, wt.rf_orig_doc_no_id)
       left join business.rf_cust_id_doc benef_doc on benef_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(wt.rf_benef_cust_seq_id, wt.rf_benef_doc_type_cd, wt.rf_benef_doc_series_id, wt.rf_benef_doc_no_id)
       left join business.rf_cust_id_doc scnd_orig_doc on scnd_orig_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(scnd_orig.cust_seq_id, wt.rf_scnd_orig_doc_type_cd, wt.rf_scnd_orig_doc_series_id, wt.rf_scnd_orig_doc_no_id)
       left join business.rf_cust_id_doc scnd_benef_doc on scnd_benef_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(scnd_benef.cust_seq_id, wt.rf_scnd_benef_doc_type_cd, wt.rf_scnd_benef_doc_series_id, wt.rf_scnd_benef_doc_no_id)
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_orig_reg_addr_tx)) orig_addr_f on 1=1
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_benef_reg_addr_tx)) benef_addr_f on 1=1
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_scnd_orig_reg_addr_tx)) scnd_orig_addr_f on 1=1
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_scnd_benef_reg_addr_tx)) scnd_benef_addr_f on 1=1
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_orig_fact_addr_tx)) orig_addr_h_f on 1=1
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_benef_fact_addr_tx)) benef_addr_h_f on 1=1
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_scnd_orig_fact_addr_tx)) scnd_orig_addr_h_f on 1=1
       left join table(business.rf_pkg_util.format_address(wt.src_sys_cd, wt.rf_scnd_benef_fact_addr_tx)) scnd_benef_addr_h_f on 1=1
       left join business.acct orig_acct on orig_acct.rowid = business.rf_pkg_util.get_acct2(wt.rf_orig_acct_seq_id, wt.rf_orig_acct_nb, wt.send_instn_id, 
                                                                                             wt.rf_orig_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)                    
       left join business.org  orig_tb on orig_tb.org_intrl_id = to_char(trunc(orig_acct.rf_branch_id/100000))
       left join business.cust orig_tb_cust on orig_tb_cust.cust_seq_id = orig_tb.cstm_1_rl       
       left join business.acct benef_acct on benef_acct.rowid = business.rf_pkg_util.get_acct2(wt.rf_benef_acct_seq_id, wt.rf_benef_acct_nb, wt.rcv_instn_id, 
                                                                                               wt.rf_benef_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)                    
       left join business.org  benef_tb on benef_tb.org_intrl_id = to_char(trunc(benef_acct.rf_branch_id/100000))
       left join business.cust benef_tb_cust on benef_tb_cust.cust_seq_id = benef_tb.cstm_1_rl       
       left join business.rf_trxn_extra trxe ON trxe.op_cat_cd = 'WT' and trxe.fo_trxn_seq_id = wt.fo_trxn_seq_id 
       left join mantas.rf_oes_321_org send_org ON send_org.oes_321_org_id = case when trxe.rowid is not null 
                                                                                  then trxe.send_oes_org_id
                                                                                  else mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(orig_acct.rf_branch_id/100000),  orig_acct.rf_branch_id  - round(orig_acct.rf_branch_id, -5),  trunc(wt.trxn_exctn_dt))
                                                                             end       
       left join mantas.rf_oes_321_org rcv_org  ON rcv_org.oes_321_org_id  = case when trxe.rowid is not null 
                                                                                  then trxe.rcv_oes_org_id
                                                                                  else mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(benef_acct.rf_branch_id/100000), benef_acct.rf_branch_id - round(benef_acct.rf_branch_id, -5), trunc(wt.trxn_exctn_dt))
                                                                             end       
       cross join party_van p
      ) loop           
           --
           -- ���� ������ ����� ���������� ���, �� ������������� ��� ������ ������ �����������
           --
           if r.adh_from_adr_fl = 1 Then
             result.adh_country_cd := r.adr_country_cd;
             result.adh_post_index := r.adr_post_index;
             result.adh_state_nm := r.adr_state_nm;
             result.adh_state_type_cd := r.adr_state_type_cd;
             result.adh_state_seq_id := r.adr_state_seq_id;
             result.adh_district_type_cd := r.adr_district_type_cd;
             result.adh_district_nm := r.adr_district_nm;
             result.adh_city_type_cd := r.adr_city_type_cd;
             result.adh_city_nm := r.adr_city_nm;
             result.adh_settlm_type_cd := r.adr_settlm_type_cd;
             result.adh_settlm_nm := r.adr_settlm_nm;
             result.adh_street_name_tx := r.adr_street_name_tx;
             result.adh_street_type_tx := r.adr_street_type_tx;
             result.adh_house_number_tx := r.adr_house_number_tx;
             result.adh_building_number_tx := r.adr_building_number_tx;
             result.adh_flat_number_tx := r.adr_flat_number_tx;
             result.adh_unstruct_fl := r.adr_unstruct_fl;
             result.adh_unstruct_tx := r.adr_unstruct_tx;
             result.fact_addr_string_trxn := r.regist_addr_string_trxn;
             result.fact_addr_string_custaddr := r.regist_addr_string_custaddr;
           else  
             result.adh_country_cd := r.adh_country_cd;
             result.adh_post_index := r.adh_post_index;
             result.adh_state_nm := r.adh_state_nm;
             result.adh_state_type_cd := r.adh_state_type_cd;
             result.adh_state_seq_id := r.adh_state_seq_id;
             result.adh_district_type_cd := r.adh_district_type_cd;
             result.adh_district_nm := r.adh_district_nm;
             result.adh_city_type_cd := r.adh_city_type_cd;
             result.adh_city_nm := r.adh_city_nm;
             result.adh_settlm_type_cd := r.adh_settlm_type_cd;
             result.adh_settlm_nm := r.adh_settlm_nm;
             result.adh_street_name_tx := r.adh_street_name_tx;
             result.adh_street_type_tx := r.adh_street_type_tx;
             result.adh_house_number_tx := r.adh_house_number_tx;
             result.adh_building_number_tx := r.adh_building_number_tx;
             result.adh_flat_number_tx := r.adh_flat_number_tx;
             result.adh_unstruct_fl := r.adh_unstruct_fl;
             result.adh_unstruct_tx := r.adh_unstruct_tx;
             result.fact_addr_string_trxn := r.fact_addr_string_trxn;
             result.fact_addr_string_custaddr := r.fact_addr_string_custaddr;
           end if;  
           result.adh_from_adr_fl := r.adh_from_adr_fl;               
           result.doc_type_cd := r.doc_type_cd;
           result.document_series := r.document_series;
           result.document_no := r.document_no;
           result.document_issued_dpt_tx := r.document_issued_dpt_tx;
           result.document_issued_dpt_cd := r.document_issued_dpt_cd;
           result.document_issued_dt := r.document_issued_dt;
           result.document_end_dt := r.document_end_dt;
           result.valid_fl := r.valid_fl;
           result.stay_cust_doc_seq_id := r.stay_cust_doc_seq_id;
           result.mc_cust_doc_seq_id := r.mc_cust_doc_seq_id;
           result.regist_addr_string_trxn := r.regist_addr_string_trxn;
           result.regist_addr_string_custaddr := r.regist_addr_string_custaddr;
           result.acct_seq_id := r.acct_seq_id;
           result.acct_nb := r.acct_nb;
           result.goz_op_cd := r.goz_op_cd;
           result.deb_cred_acct_nb := r.deb_cred_acct_nb;
           result.instn_nm := r.instn_nm;
           result.instn_id := r.instn_id;
           result.instn_cntry_cd := r.instn_cntry_cd;
           result.instn_acct_id := r.instn_acct_id;
           result.branch_id := r.branch_id;
           result.oes_org_id := r.oes_org_id;
           result.kgrko_nm := r.kgrko_nm;
           result.kgrko_id := r.kgrko_id;
           result.bank_card_id_nb := r.bank_card_id_nb;
           result.bank_card_trxn_dt := r.bank_card_trxn_dt;
           result.src_sys_cd := r.src_sys_cd;
           result.op_cat_cd := r.op_cat_cd;
           result.trxn_seq_id := r.trxn_seq_id;
           result.party_cd := r.party_cd;
           result.cust_seq_id := r.cust_seq_id;
           result.cust_fl_seq_id := r.cust_fl_seq_id;
           result.cust_type_cd := r.cust_type_cd;
           result.resident_fl := r.resident_fl;
           result.cust_nm := r.cust_nm;
           result.cust_fl_nm := r.cust_fl_nm;
           result.gender_cd := r.gender_cd;
           result.inn_nb := r.inn_nb;
           result.ogrn_nb := r.ogrn_nb;
           result.birth_dt := r.birth_dt;
           result.birthplace_tx := r.birthplace_tx;
           result.kpp_nb := r.kpp_nb;
           result.citizenship_cd := r.citizenship_cd;
           result.reg_org_nm := r.reg_org_nm;
           result.okved_nb := r.okved_nb;
           result.okpo_nb := r.okpo_nb;
           result.reg_dt := r.reg_dt;
           result.adr_country_cd := r.adr_country_cd;
           result.adr_post_index := r.adr_post_index;
           result.adr_state_nm := r.adr_state_nm;
           result.adr_state_type_cd := r.adr_state_type_cd;
           result.adr_state_seq_id := r.adr_state_seq_id;
           result.adr_district_type_cd := r.adr_district_type_cd;
           result.adr_district_nm := r.adr_district_nm;
           result.adr_city_type_cd := r.adr_city_type_cd;
           result.adr_city_nm := r.adr_city_nm;
           result.adr_settlm_type_cd := r.adr_settlm_type_cd;
           result.adr_settlm_nm := r.adr_settlm_nm;
           result.adr_street_name_tx := r.adr_street_name_tx;
           result.adr_street_type_tx := r.adr_street_type_tx;
           result.adr_house_number_tx := r.adr_house_number_tx;
           result.adr_building_number_tx := r.adr_building_number_tx;
           result.adr_flat_number_tx := r.adr_flat_number_tx;
           result.adr_unstruct_fl := r.adr_unstruct_fl;
           result.adr_unstruct_tx := r.adr_unstruct_tx;
           result.fl_adr_country_cd := r.fl_adr_country_cd;
           --
           -- ������� ��� ��������� �������, ���� ����� ��� ������������ ��� ��������:
           --  ������, �����, �����, ���������� �����, �����
           -- � ������� ����������� � ����� ����������
           --
           if result.adr_state_nm is null       then result.adr_state_type_cd := null; end if;
           if result.adr_district_nm is null    then result.adr_district_type_cd := null; end if;
           if result.adr_city_nm is null        then result.adr_city_type_cd := null; end if;
           if result.adr_settlm_nm is null      then result.adr_settlm_type_cd := null; end if;
           if result.adr_street_name_tx is null then result.adr_street_type_tx := null; end if;

           if result.adh_state_nm is null       then result.adh_state_type_cd := null; end if;
           if result.adh_district_nm is null    then result.adh_district_type_cd := null; end if;
           if result.adh_city_nm is null        then result.adh_city_type_cd := null; end if;
           if result.adh_settlm_nm is null      then result.adh_settlm_type_cd := null; end if;
           if result.adh_street_name_tx is null then result.adh_street_type_tx := null; end if;

           pipe row(result);
    end loop;
  end if;



--
--
--  ������ ������� ��� CASH_TRXN
--
--


  if (p_op_cat_cd = 'CT') and (p_trxn_id is not null) then
    for r in (
          with party_van as
          (select 1, 'ORIG' as party_cd from dual union all
          select 2, 'BENEF' as party_cd from dual union all
          select 3, 'SCND_ORIG' as party_cd from dual union all
          select 4, 'SCND_BENEF' as party_cd from dual
          )
       SELECT
       /*+ ORDERED USE_NL(orig orig_fl scnd_orig orig_addr orig_fl_addr orig_addr_state scnd_orig_addr scnd_orig_addr_state orig_addr_h orig_addr_state_h scnd_orig_addr_h scnd_orig_addr_state_h orig_doc scnd_orig_doc acct tb tb_cust trxe send_org rcv_org)*/
       'CT' as op_cat_cd,
       ct.fo_trxn_seq_id as trxn_seq_id,
       p.party_cd,
       --
       --�������� � �����������
       --
       -- id �������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then orig.cust_seq_id end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then orig.cust_seq_id end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.cust_seq_id end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.cust_seq_id end) as cust_seq_id,
       -- id ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then orig_fl.cust_seq_id end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then orig_fl.cust_seq_id end,
                          'SCND_ORIG',  to_number(null),
                          'SCND_BENEF', to_number(null)) as cust_fl_seq_id,
       -- ��� �������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then orig.cust_type_cd end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then orig.cust_type_cd end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.cust_type_cd end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.cust_type_cd end) as cust_type_cd,
       -- ��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_resident_fl, orig.rf_resident_fl) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_resident_fl, orig.rf_resident_fl) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_resident_fl, scnd_orig.rf_resident_fl) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_resident_fl, scnd_orig.rf_resident_fl) end) as resident_fl,
       -- ������������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(trim(ct.rf_cust_nm || ' ' || ct.rf_cust_first_nm || ' ' || ct.rf_cust_midl_nm), orig.full_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(trim(ct.rf_cust_nm || ' ' || ct.rf_cust_first_nm || ' ' || ct.rf_cust_midl_nm), orig.full_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(trim(ct.cndtr_nm || ' ' || ct.rf_cndtr_first_nm || ' ' || ct.rf_cndtr_midl_nm), scnd_orig.full_nm) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(trim(ct.cndtr_nm || ' ' || ct.rf_cndtr_first_nm || ' ' || ct.rf_cndtr_midl_nm), scnd_orig.full_nm) end) as cust_nm,
       -- ��� ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then orig_fl.full_nm end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then orig_fl.full_nm end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then to_char(null) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then to_char(null) end) as cust_fl_nm,
       -- ���
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then coalesce(ct.rf_cust_gndr_cd, orig_fl.cust_gndr_cd, orig.cust_gndr_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then coalesce(ct.rf_cust_gndr_cd, orig_fl.cust_gndr_cd, orig.cust_gndr_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_gndr_cd, scnd_orig.cust_gndr_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_gndr_cd, scnd_orig.cust_gndr_cd) end) as gender_cd,
       -- ���
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_inn_nb, orig.tax_id) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_inn_nb, orig.tax_id) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.tax_id end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.tax_id end) as inn_nb,
       -- ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_ogrn_nb, orig.rf_ogrn_nb) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_ogrn_nb, orig.rf_ogrn_nb) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.rf_ogrn_nb end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.rf_ogrn_nb end) as ogrn_nb,
       -- ���� ��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then coalesce(ct.rf_cust_birth_dt, orig_fl.birth_dt, orig.birth_dt) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then coalesce(ct.rf_cust_birth_dt, orig_fl.birth_dt, orig.birth_dt) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_birth_dt, scnd_orig.birth_dt) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_birth_dt, scnd_orig.birth_dt) end) as birth_dt,
       -- ����� ��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then coalesce(ct.rf_cust_birthplace_tx, orig_fl.rf_birthplace_tx, orig.rf_birthplace_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then coalesce(ct.rf_cust_birthplace_tx, orig_fl.rf_birthplace_tx, orig.rf_birthplace_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_birthplace_tx, scnd_orig.rf_birthplace_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_birthplace_tx, scnd_orig.rf_birthplace_tx) end) as birthplace_tx,
       -- ���
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_kpp_nb, orig.rf_kpp_nb) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_kpp_nb, orig.rf_kpp_nb) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.rf_kpp_nb end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.rf_kpp_nb end) as kpp_nb,
       -- ����������� (��� ������� �����������, � ��� ���� ��������� ����� �����������)
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_cntry_cd, orig.ctzshp_cntry1_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_cntry_cd, orig.ctzshp_cntry1_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_cntry_cd, scnd_orig.ctzshp_cntry1_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_cntry_cd, scnd_orig.ctzshp_cntry1_cd) end) as citizenship_cd,
       -- �������������� �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_reg_org_nm, orig.rf_reg_org_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_reg_org_nm, orig.rf_reg_org_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.rf_reg_org_nm end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.rf_reg_org_nm end) as reg_org_nm,
       -- �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then business.rf_pkg_util.get_list_clsf_code(orig.cust_seq_id,'1') end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then business.rf_pkg_util.get_list_clsf_code(orig.cust_seq_id,'1') end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then business.rf_pkg_util.get_list_clsf_code(scnd_orig.cust_seq_id,'1') end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then business.rf_pkg_util.get_list_clsf_code(scnd_orig.cust_seq_id,'1') end) as okved_nb,
       -- ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_okpo_nb, orig.rf_okpo_nb) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_okpo_nb, orig.rf_okpo_nb) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.rf_okpo_nb end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.rf_okpo_nb end) as okpo_nb,
       -- ���� ����������� �������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_reg_dt, orig.rf_reg_dt) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_reg_dt, orig.rf_reg_dt) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig.rf_reg_dt end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig.rf_reg_dt end) as reg_dt,
       --
       --����� �����������
       --
       -- ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.country, nvl(orig_addr_state.cntry_cd, orig_addr.addr_cntry_cd)) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.country, nvl(orig_addr_state.cntry_cd, orig_addr.addr_cntry_cd)) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.country, nvl(scnd_orig_addr_state.cntry_cd, scnd_orig_addr.addr_cntry_cd)) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.country, nvl(scnd_orig_addr_state.cntry_cd, scnd_orig_addr.addr_cntry_cd)) end) as adr_country_cd,
       -- ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.post_index, orig_addr.addr_postl_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.post_index, orig_addr.addr_postl_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.post_index, scnd_orig_addr.addr_postl_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.post_index, scnd_orig_addr.addr_postl_cd) end) as adr_post_index,
       -- ������������ �������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.region_nm, nvl(orig_addr_state.object_nm, orig_addr.rf_state_nm)) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.region_nm, nvl(orig_addr_state.object_nm, orig_addr.rf_state_nm)) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_nm, nvl(scnd_orig_addr_state.object_nm, scnd_orig_addr.rf_state_nm)) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_nm, nvl(scnd_orig_addr_state.object_nm, scnd_orig_addr.rf_state_nm)) end) as adr_state_nm,
       -- ��� ������� - ���������� (��������, ���, ����, ��)
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.region_cd, nvl(orig_addr_state.addr_type_cd, orig_addr.rf_state_type_cd)) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.region_cd, nvl(orig_addr_state.addr_type_cd, orig_addr.rf_state_type_cd)) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_cd, nvl(scnd_orig_addr_state.addr_type_cd, scnd_orig_addr.rf_state_type_cd)) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_cd, nvl(scnd_orig_addr_state.addr_type_cd, scnd_orig_addr.rf_state_type_cd)) end) as adr_state_type_cd,
       -- ������ �� ��������� �������� ������, ��������������� �������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.region_addr_seq_id, orig_addr_state.addr_seq_id) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.region_addr_seq_id, orig_addr_state.addr_seq_id) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_addr_seq_id, scnd_orig_addr_state.addr_seq_id) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.region_addr_seq_id, scnd_orig_addr_state.addr_seq_id) end) as adr_state_seq_id,
      -- ��� ������ ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.district_cd, orig_addr.rf_rgn_type_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.district_cd, orig_addr.rf_rgn_type_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.district_cd, scnd_orig_addr.rf_rgn_type_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.district_cd, scnd_orig_addr.rf_rgn_type_cd) end) as adr_district_type_cd,
       -- ����� ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.district_nm, orig_addr.addr_rgn_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.district_nm, orig_addr.addr_rgn_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.district_nm, scnd_orig_addr.addr_rgn_nm) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.district_nm, scnd_orig_addr.addr_rgn_nm) end) as adr_district_nm,
       -- ��� ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.city_cd, orig_addr.rf_city_type_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.city_cd, orig_addr.rf_city_type_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.city_cd, scnd_orig_addr.rf_city_type_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.city_cd, scnd_orig_addr.rf_city_type_cd) end) as adr_city_type_cd,
       -- �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.city_nm, orig_addr.addr_city_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.city_nm, orig_addr.addr_city_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.city_nm, scnd_orig_addr.addr_city_nm) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.city_nm, scnd_orig_addr.addr_city_nm) end) as adr_city_nm,
       -- ��� ����������� ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.settlm_type_cd, orig_addr.rf_settlm_type_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.settlm_type_cd, orig_addr.rf_settlm_type_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.settlm_type_cd, scnd_orig_addr.rf_settlm_type_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.settlm_type_cd, scnd_orig_addr.rf_settlm_type_cd) end) as adr_settlm_type_cd,
       -- ���������� �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.settlm_nm, orig_addr.rf_settlm_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.settlm_nm, orig_addr.rf_settlm_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.settlm_nm, scnd_orig_addr.rf_settlm_nm) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.settlm_nm, scnd_orig_addr.rf_settlm_nm) end) as adr_settlm_nm,
       -- ����� (������������ ��� ����, ��������, ��������)
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.street_nm, orig_addr.addr_strt_line1_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.street_nm, orig_addr.addr_strt_line1_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.street_nm, scnd_orig_addr.addr_strt_line1_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.street_nm, scnd_orig_addr.addr_strt_line1_tx) end) as adr_street_name_tx,
       -- ��� ����� - ���������� (��������, ��, �-�, ��-��, ������)
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.street_cd, orig_addr.addr_strt_line2_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.street_cd, orig_addr.addr_strt_line2_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.street_cd, scnd_orig_addr.addr_strt_line2_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.street_cd, scnd_orig_addr.addr_strt_line2_tx) end) as adr_street_type_tx,
       -- ����� ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.house_nb, orig_addr.addr_strt_line3_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.house_nb, orig_addr.addr_strt_line3_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.house_nb, scnd_orig_addr.addr_strt_line3_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.house_nb, scnd_orig_addr.addr_strt_line3_tx) end) as adr_house_number_tx,
       -- ����� �������/��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.building, orig_addr.addr_strt_line4_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.building, orig_addr.addr_strt_line4_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.building, scnd_orig_addr.addr_strt_line4_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.building, scnd_orig_addr.addr_strt_line4_tx) end) as adr_building_number_tx,
       -- ����� ��������/�����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr.rowid, null, orig_addr_f.flat, orig_addr.addr_strt_line5_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr.rowid, null, orig_addr_f.flat, orig_addr.addr_strt_line5_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.flat, scnd_orig_addr.addr_strt_line5_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr.rowid, null, scnd_orig_addr_f.flat, scnd_orig_addr.addr_strt_line5_tx) end) as adr_flat_number_tx,
       -- ���� (0/1) �������������������� ������ �����������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D'
                                             then case when orig_addr.rowid is null or
                                                            (orig_addr.rf_addr_tx is not null and
                                                             orig_addr.addr_city_nm||orig_addr.rf_settlm_nm||orig_addr.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C'
                                             then case when orig_addr.rowid is null or
                                                            (orig_addr.rf_addr_tx is not null and
                                                             orig_addr.addr_city_nm||orig_addr.rf_settlm_nm||orig_addr.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D'
                                             then case when scnd_orig_addr.rowid is null or
                                                            (scnd_orig_addr.rf_addr_tx is not null and
                                                             scnd_orig_addr.addr_city_nm||scnd_orig_addr.rf_settlm_nm||scnd_orig_addr.addr_strt_line1_tx is null)
                                                  then 1 else 0 end
                                        end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C'
                                             then case when scnd_orig_addr.rowid is null or
                                                            (scnd_orig_addr.rf_addr_tx is not null and
                                                             scnd_orig_addr.addr_city_nm||scnd_orig_addr.rf_settlm_nm||scnd_orig_addr.addr_strt_line1_tx is null)
                                                  then 1 else 0 end
                                        end) as adr_unstruct_fl,
       -- ����� �������������������� ������ �����������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(orig_addr.rf_addr_tx,       ct.rf_cust_reg_addr_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(orig_addr.rf_addr_tx,       ct.rf_cust_reg_addr_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(scnd_orig_addr.rf_addr_tx,  ct.rf_cndtr_reg_addr_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(scnd_orig_addr.rf_addr_tx,  ct.rf_cndtr_reg_addr_tx) end) as adr_unstruct_tx,
       -- ������ �� ������ ����������� ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then orig_fl_addr.addr_cntry_cd end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then orig_fl_addr.addr_cntry_cd end,
                          'SCND_ORIG',  to_char(null),
                          'SCND_BENEF', to_char(null)) as fl_adr_country_cd,
       --
       -- ����� ����� ����������
       --
       -- ����: ���������� � ����� ����� ���������� ����� �����������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' 
                                             then case when /*orig.cust_type_cd <> 'IND' and*/ orig_addr_h.rowid is null and ct.rf_cust_fact_addr_tx is null then 1 else 0 end
                                        end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' 
                                             then case when /*orig.cust_type_cd <> 'IND' and*/ orig_addr_h.rowid is null and ct.rf_cust_fact_addr_tx is null then 1 else 0 end
                                        end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' 
                                             then case when /*scnd_orig.cust_type_cd <> 'IND' and*/ scnd_orig_addr_h.rowid is null and ct.rf_cndtr_fact_addr_tx is null then 1 else 0 end
                                        end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' 
                                             then case when /*scnd_orig.cust_type_cd <> 'IND' and*/ scnd_orig_addr_h.rowid is null and ct.rf_cndtr_fact_addr_tx is null then 1 else 0 end
                                        end) as adh_from_adr_fl,
       -- ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.country, nvl(orig_addr_state_h.cntry_cd, orig_addr_h.addr_cntry_cd)) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.country, nvl(orig_addr_state_h.cntry_cd, orig_addr_h.addr_cntry_cd)) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.country, nvl(scnd_orig_addr_state_h.cntry_cd, scnd_orig_addr_h.addr_cntry_cd)) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.country, nvl(scnd_orig_addr_state_h.cntry_cd, scnd_orig_addr_h.addr_cntry_cd)) end) as adh_country_cd,
       -- ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.post_index, orig_addr_h.addr_postl_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.post_index, orig_addr_h.addr_postl_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.post_index, scnd_orig_addr_h.addr_postl_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.post_index, scnd_orig_addr_h.addr_postl_cd) end) as adh_post_index,
       -- ������������ �������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.region_nm, nvl(orig_addr_state_h.object_nm, orig_addr_h.rf_state_nm)) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.region_nm, nvl(orig_addr_state_h.object_nm, orig_addr_h.rf_state_nm)) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.region_nm, nvl(scnd_orig_addr_state_h.object_nm, scnd_orig_addr_h.rf_state_nm)) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.region_nm, nvl(scnd_orig_addr_state_h.object_nm, scnd_orig_addr_h.rf_state_nm)) end) as adh_state_nm,
       -- ��� ������� - ���������� (��������, ���, ����, ��)
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.region_cd, nvl(orig_addr_state_h.addr_type_cd, orig_addr_h.rf_state_type_cd)) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.region_cd, nvl(orig_addr_state_h.addr_type_cd, orig_addr_h.rf_state_type_cd)) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.region_cd, nvl(scnd_orig_addr_state_h.addr_type_cd, scnd_orig_addr_h.rf_state_type_cd)) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.region_cd, nvl(scnd_orig_addr_state_h.addr_type_cd, scnd_orig_addr_h.rf_state_type_cd)) end) as adh_state_type_cd,
       -- ������ �� ��������� �������� ������, ��������������� �������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.region_addr_seq_id, orig_addr_state_h.addr_seq_id) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.region_addr_seq_id, orig_addr_state_h.addr_seq_id) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.region_addr_seq_id, scnd_orig_addr_state_h.addr_seq_id) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.region_addr_seq_id, scnd_orig_addr_state_h.addr_seq_id) end) as adh_state_seq_id,
      -- ��� ������ ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.district_cd, orig_addr_h.rf_rgn_type_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.district_cd, orig_addr_h.rf_rgn_type_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.district_cd, scnd_orig_addr_h.rf_rgn_type_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.district_cd, scnd_orig_addr_h.rf_rgn_type_cd) end) as adh_district_type_cd,
       -- ����� ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.district_nm, orig_addr_h.addr_rgn_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.district_nm, orig_addr_h.addr_rgn_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.district_nm, scnd_orig_addr_h.addr_rgn_nm) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.district_nm, scnd_orig_addr_h.addr_rgn_nm) end) as adh_district_nm,
       -- ��� ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.city_cd, orig_addr_h.rf_city_type_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.city_cd, orig_addr_h.rf_city_type_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.city_cd, scnd_orig_addr_h.rf_city_type_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.city_cd, scnd_orig_addr_h.rf_city_type_cd) end) as adh_city_type_cd,
       -- �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.city_nm, orig_addr_h.addr_city_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.city_nm, orig_addr_h.addr_city_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.city_nm, scnd_orig_addr_h.addr_city_nm) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.city_nm, scnd_orig_addr_h.addr_city_nm) end) as adh_city_nm,
       -- ��� ����������� ������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.settlm_type_cd, orig_addr_h.rf_settlm_type_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.settlm_type_cd, orig_addr_h.rf_settlm_type_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.settlm_type_cd, scnd_orig_addr_h.rf_settlm_type_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.settlm_type_cd, scnd_orig_addr_h.rf_settlm_type_cd) end) as adh_settlm_type_cd,
       -- ���������� �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.settlm_nm, orig_addr_h.rf_settlm_nm) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.settlm_nm, orig_addr_h.rf_settlm_nm) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.settlm_nm, scnd_orig_addr_h.rf_settlm_nm) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.settlm_nm, scnd_orig_addr_h.rf_settlm_nm) end) as adh_settlm_nm,
       -- ����� (������������ ��� ����, ��������, ��������)
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.street_nm, orig_addr_h.addr_strt_line1_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.street_nm, orig_addr_h.addr_strt_line1_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.street_nm, scnd_orig_addr_h.addr_strt_line1_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.street_nm, scnd_orig_addr_h.addr_strt_line1_tx) end) as adh_street_name_tx,
       -- ��� ����� - ���������� (��������, ��, �-�, ��-��, ������)
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.street_cd, orig_addr_h.addr_strt_line2_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.street_cd, orig_addr_h.addr_strt_line2_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.street_cd, scnd_orig_addr_h.addr_strt_line2_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.street_cd, scnd_orig_addr_h.addr_strt_line2_tx) end) as adh_street_type_tx,
       -- ����� ����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.house_nb, orig_addr_h.addr_strt_line3_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.house_nb, orig_addr_h.addr_strt_line3_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.house_nb, scnd_orig_addr_h.addr_strt_line3_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.house_nb, scnd_orig_addr_h.addr_strt_line3_tx) end) as adh_house_number_tx,
       -- ����� �������/��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.building, orig_addr_h.addr_strt_line4_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.building, orig_addr_h.addr_strt_line4_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.building, scnd_orig_addr_h.addr_strt_line4_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.building, scnd_orig_addr_h.addr_strt_line4_tx) end) as adh_building_number_tx,
       -- ����� ��������/�����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then decode(orig_addr_h.rowid, null, orig_addr_hf.flat, orig_addr_h.addr_strt_line5_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then decode(orig_addr_h.rowid, null, orig_addr_hf.flat, orig_addr_h.addr_strt_line5_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.flat, scnd_orig_addr_h.addr_strt_line5_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then decode(scnd_orig_addr_h.rowid, null, scnd_orig_addr_hf.flat, scnd_orig_addr_h.addr_strt_line5_tx) end) as adh_flat_number_tx,
       -- ���� (0/1) �������������������� ������ ����� ����������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D'
                                             then case when orig_addr_h.rowid is null or
                                                            (orig_addr_h.rf_addr_tx is not null and
                                                             orig_addr_h.addr_city_nm||orig_addr_h.rf_settlm_nm||orig_addr_h.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C'
                                             then case when orig_addr_h.rowid is null or
                                                            (orig_addr_h.rf_addr_tx is not null and
                                                             orig_addr_h.addr_city_nm||orig_addr_h.rf_settlm_nm||orig_addr_h.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D'
                                             then case when scnd_orig_addr_h.rowid is null or
                                                            (scnd_orig_addr_h.rf_addr_tx is not null and
                                                             scnd_orig_addr_h.addr_city_nm||scnd_orig_addr_h.rf_settlm_nm||scnd_orig_addr_h.addr_strt_line1_tx is null)
                                                  then 1 else 0 end
                                        end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C'
                                             then case when scnd_orig_addr_h.rowid is null or
                                                            (scnd_orig_addr_h.rf_addr_tx is not null and
                                                             scnd_orig_addr_h.addr_city_nm||scnd_orig_addr_h.rf_settlm_nm||scnd_orig_addr_h.addr_strt_line1_tx is null)
                                                  then 1 else 0 end
                                        end) as adh_unstruct_fl,
       -- ����� �������������������� ������ �����������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(orig_addr_h.rf_addr_tx,       ct.rf_cust_fact_addr_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(orig_addr_h.rf_addr_tx,       ct.rf_cust_fact_addr_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(scnd_orig_addr_h.rf_addr_tx,  ct.rf_cndtr_fact_addr_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(scnd_orig_addr_h.rf_addr_tx,  ct.rf_cndtr_fact_addr_tx) end) as adh_unstruct_tx,
       --
       -- �������������� ��������
       --
       -- ��� ���������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_doc_type_cd, orig_doc.doc_type_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_doc_type_cd, orig_doc.doc_type_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_doc_type_cd, scnd_orig_doc.doc_type_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_doc_type_cd, scnd_orig_doc.doc_type_cd) end) as doc_type_cd,
       -- ����� ���������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_doc_series_id, orig_doc.series_id) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_doc_series_id, orig_doc.series_id) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_doc_series_id, scnd_orig_doc.series_id) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_doc_series_id, scnd_orig_doc.series_id) end) as document_series,
       -- ����� ���������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_doc_no_id, orig_doc.no_id) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_doc_no_id, orig_doc.no_id) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_doc_no_id, scnd_orig_doc.no_id) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_doc_no_id, scnd_orig_doc.no_id) end) as document_no,
       -- ����� �������� ��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_doc_issued_by_tx, orig_doc.issued_by_tx) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_doc_issued_by_tx, orig_doc.issued_by_tx) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_doc_issued_by_tx, scnd_orig_doc.issued_by_tx) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_doc_issued_by_tx, scnd_orig_doc.issued_by_tx) end) as document_issued_dpt_tx,
       -- ��� �������������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_doc_dpt_cd,orig_doc.issue_dpt_cd) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_doc_dpt_cd,orig_doc.issue_dpt_cd) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_doc_dpt_cd,scnd_orig_doc.issue_dpt_cd) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_doc_dpt_cd,scnd_orig_doc.issue_dpt_cd) end) as document_issued_dpt_cd,
       -- ���� ������ ���������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cust_doc_issue_dt, orig_doc.issue_dt) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cust_doc_issue_dt, orig_doc.issue_dt) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then nvl(ct.rf_cndtr_doc_issue_dt, scnd_orig_doc.issue_dt) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then nvl(ct.rf_cndtr_doc_issue_dt, scnd_orig_doc.issue_dt) end) as document_issued_dt,
       -- ���� ��������� �������� ���������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then orig_doc.end_dt end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then orig_doc.end_dt end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig_doc.end_dt end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig_doc.end_dt end) as document_end_dt,
       -- ����������� �� ��������?
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then orig_doc.valid_fl end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then orig_doc.valid_fl end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then scnd_orig_doc.valid_fl end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then scnd_orig_doc.valid_fl end) as valid_fl,
       --
       -- �������������� �������������� ���������
       --
       -- ��������, �������������� ����� �� ���������� - ??? �������� ???
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, null, 'Y')) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, null, 'Y')) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then to_number(business.rf_pkg_util.get_id_doc(scnd_orig.cust_seq_id, null, 'Y')) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then to_number(business.rf_pkg_util.get_id_doc(scnd_orig.cust_seq_id, null, 'Y')) end) as stay_cust_doc_seq_id,
       -- ������������ ����� - ??? �������� ???
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, 39)) end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, 39)) end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then to_number(business.rf_pkg_util.get_id_doc(scnd_orig.cust_seq_id, 39)) end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then to_number(business.rf_pkg_util.get_id_doc(scnd_orig.cust_seq_id, 39)) end) as mc_cust_doc_seq_id,
       -- ����� ����������� ������� �� ��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then ct.rf_cust_reg_addr_tx end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then ct.rf_cust_reg_addr_tx end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then ct.rf_cndtr_reg_addr_tx end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then ct.rf_cndtr_reg_addr_tx end) as regist_addr_string_trxn,
       -- ����� ���������� ������� �� ��������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then ct.rf_cust_fact_addr_tx end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then ct.rf_cust_fact_addr_tx end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then ct.rf_cndtr_fact_addr_tx end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then ct.rf_cndtr_fact_addr_tx end) as fact_addr_string_trxn,
       -- ����� ����������� ������� �� ������� � ��������
       decode(p.party_cd, 'ORIG',       business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'D' then orig_addr.cust_addr_seq_id end),
                          'BENEF',      business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'C' then orig_addr.cust_addr_seq_id end),
                          'SCND_ORIG',  business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'D' then scnd_orig_addr.cust_addr_seq_id end),
                          'SCND_BENEF', business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'C' then scnd_orig_addr.cust_addr_seq_id end)) as regist_addr_string_custaddr,
       -- ����� ���������� ������� �� ������� � ��������
       decode(p.party_cd, 'ORIG',       business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'D' then orig_addr_h.cust_addr_seq_id end),
                          'BENEF',      business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'C' then orig_addr_h.cust_addr_seq_id end),
                          'SCND_ORIG',  business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'D' then scnd_orig_addr_h.cust_addr_seq_id end),
                          'SCND_BENEF', business.rf_pkg_util.get_addr_text(case when ct.dbt_cdt_cd = 'C' then scnd_orig_addr_h.cust_addr_seq_id end)) as fact_addr_string_custaddr,
       -- ���������� ������������� �����. ����������� ������ ��� "����������" ������, ������������� ������� BUSINESS.ACCT.ACCT_SEQ_ID
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then ct.rf_acct_seq_id end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then ct.rf_acct_seq_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as acct_seq_id,
       -- ����� �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then nullif(ct.rf_acct_nb, '-1') end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then nullif(ct.rf_acct_nb, '-1') end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as acct_nb,
       -- ��� �������� ���
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then ct.rf_acct_goz_op_cd end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then ct.rf_acct_goz_op_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as goz_op_cd,
       -- ���� ������ ��� �������, ��������� � ������ ���������
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then ct.rf_debit_cd end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then ct.rf_credit_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as deb_cred_acct_nb,
       -- ������������ �����
       decode(p.party_cd, 'ORIG',       case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' then nvl(tb_cust.full_nm, tb.org_nm) end,
                          'BENEF',      case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' then nvl(tb_cust.full_nm, tb.org_nm) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_nm,
       -- ���/SWIFT �����
       decode(p.party_cd, 'ORIG',       case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx) end,
                          'BENEF',      case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_id,
       -- ������ �����
       decode(p.party_cd, 'ORIG',       case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' then 'RU' end,
                          'BENEF',      case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' then 'RU' end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_cntry_cd,
       -- ����. ���� �����
       decode(p.party_cd, 'ORIG',       case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' then tb.cstm_3_tx end,
                          'BENEF',      case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' then tb.cstm_3_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_acct_id,
       -- ��� ��/��� �� �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then acct.rf_branch_id end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then acct.rf_branch_id end,
                          'SCND_ORIG',  case when ct.dbt_cdt_cd = 'D' then acct.rf_branch_id end,
                          'SCND_BENEF', case when ct.dbt_cdt_cd = 'C' then acct.rf_branch_id end) as branch_id,
       -- ID ������� �����
       decode(p.party_cd, 'ORIG',       send_org.oes_321_org_id,
                          'BENEF',      rcv_org.oes_321_org_id,
                          'SCND_ORIG',  send_org.oes_321_org_id,
                          'SCND_BENEF', rcv_org.oes_321_org_id) as oes_org_id,
       -- ������������ ������� �����
       decode(p.party_cd, 'ORIG',       send_org.tb_name,
                          'BENEF',      rcv_org.tb_name,
                          'SCND_ORIG',  send_org.tb_name,
                          'SCND_BENEF', rcv_org.tb_name) as kgrko_nm,
       -- ����� ������� ����� (�������, ���� �������� ��)
       decode(p.party_cd, 'ORIG',       case when send_org.oes_321_org_id is not null
                                             then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                                                 '1', nullif(send_org.numbf_ss, '0')), '-') 
                                        end,
                          'BENEF',      case when rcv_org.oes_321_org_id is not null
                                             then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                                                '1', nullif(rcv_org.numbf_ss, '0')), '-') 
                                        end,
                          'SCND_ORIG',  case when send_org.oes_321_org_id is not null
                                             then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                                                 '1', nullif(send_org.numbf_ss, '0')), '-') 
                                        end,
                          'SCND_BENEF', case when rcv_org.oes_321_org_id is not null
                                             then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                                                '1', nullif(rcv_org.numbf_ss, '0')), '-') 
                                        end) as kgrko_id,       
       -- ����� ���������� �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then ct.bank_card_id_nb end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then ct.bank_card_id_nb end,
                          'SCND_ORIG',  to_number(null),
                          'SCND_BENEF', to_number(null)) as bank_card_id_nb,
       -- ���� �������� �� ���������� �����
       decode(p.party_cd, 'ORIG',       case when ct.dbt_cdt_cd = 'D' then ct.cstm_1_dt end,
                          'BENEF',      case when ct.dbt_cdt_cd = 'C' then ct.cstm_1_dt end,
                          'SCND_ORIG',  to_date(null),
                          'SCND_BENEF', to_date(null)) as bank_card_trxn_dt,
      -----------------------------------
       ct.src_sys_cd as src_sys_cd
  from -- �������� �������� ������ �������� �� ��������� �������� ����, � ���� ����� ��� - ������� ������ ��������
       (select /*+ INDEX(cash_trxn PK_CASH_TRXN)*/
               fo_trxn_seq_id, dbt_cdt_cd, trxn_exctn_dt,
               rf_cust_seq_id, rf_cndtr_cust_seq_id, 
               rf_cust_resident_fl, rf_cndtr_resident_fl,
               rf_cust_nm, cndtr_nm,
               rf_cust_midl_nm, rf_cndtr_midl_nm,
               rf_cust_first_nm, rf_cndtr_first_nm,
               rf_cust_gndr_cd, rf_cndtr_gndr_cd,
               rf_cust_inn_nb, 
               rf_cust_ogrn_nb, 
               rf_cust_birth_dt, rf_cndtr_birth_dt,
               rf_cust_birthplace_tx, rf_cndtr_birthplace_tx,
               rf_cust_kpp_nb,
               rf_cust_reg_org_nm, 
               rf_cust_okpo_nb,
               rf_cust_reg_dt,
               rf_cust_cntry_cd, rf_cndtr_cntry_cd,
               rf_cust_reg_addr_tx, rf_cndtr_reg_addr_tx,
               rf_cust_fact_addr_tx, rf_cndtr_fact_addr_tx,
               rf_cust_doc_type_cd, rf_cndtr_doc_type_cd, 
               rf_cust_doc_series_id, rf_cndtr_doc_series_id, 
               rf_cust_doc_no_id, rf_cndtr_doc_no_id,
               rf_cust_doc_issued_by_tx, rf_cndtr_doc_issued_by_tx, 
               rf_cust_doc_dpt_cd, rf_cndtr_doc_dpt_cd, 
               rf_cust_doc_issue_dt, rf_cndtr_doc_issue_dt, 
               rf_acct_seq_id, rf_acct_nb, rf_acct_goz_op_cd, rf_debit_cd, rf_credit_cd, rf_branch_id,
               bank_card_id_nb, cstm_1_dt, src_sys_cd
          from business.cash_trxn        
         where fo_trxn_seq_id = p_trxn_id and
               not exists(select null 
                            from business.cash_trxn_arh
                           where fo_trxn_seq_id = p_trxn_id and
                                 arh_dump_dt = p_arh_dump_dt)
        union all
        select /*+ INDEX(cash_trxn_arh PK_CASH_TRXN_ARH)*/
               fo_trxn_seq_id, dbt_cdt_cd, trxn_exctn_dt,
               rf_cust_seq_id, rf_cndtr_cust_seq_id, 
               rf_cust_resident_fl, rf_cndtr_resident_fl,
               rf_cust_nm, cndtr_nm,
               rf_cust_midl_nm, rf_cndtr_midl_nm,
               rf_cust_first_nm, rf_cndtr_first_nm,
               rf_cust_gndr_cd, rf_cndtr_gndr_cd,
               rf_cust_inn_nb, 
               rf_cust_ogrn_nb, 
               rf_cust_birth_dt, rf_cndtr_birth_dt,
               rf_cust_birthplace_tx, rf_cndtr_birthplace_tx,
               rf_cust_kpp_nb,
               rf_cust_reg_org_nm, 
               rf_cust_okpo_nb,
               rf_cust_reg_dt,
               rf_cust_cntry_cd, rf_cndtr_cntry_cd,
               rf_cust_reg_addr_tx, rf_cndtr_reg_addr_tx,
               rf_cust_fact_addr_tx, rf_cndtr_fact_addr_tx,
               rf_cust_doc_type_cd, rf_cndtr_doc_type_cd, 
               rf_cust_doc_series_id, rf_cndtr_doc_series_id, 
               rf_cust_doc_no_id, rf_cndtr_doc_no_id,
               rf_cust_doc_issued_by_tx, rf_cndtr_doc_issued_by_tx, 
               rf_cust_doc_dpt_cd, rf_cndtr_doc_dpt_cd, 
               rf_cust_doc_issue_dt, rf_cndtr_doc_issue_dt, 
               rf_acct_seq_id, rf_acct_nb, rf_acct_goz_op_cd, rf_debit_cd, rf_credit_cd, rf_branch_id, 
               bank_card_id_nb, cstm_1_dt, src_sys_cd                 
          from business.cash_trxn_arh
         where fo_trxn_seq_id = p_trxn_id and
               arh_dump_dt = p_arh_dump_dt) ct
       left join business.cust orig on orig.cust_seq_id = ct.rf_cust_seq_id
       left join business.cust orig_fl on orig_fl.cust_seq_id = orig.rf_ind_cust_seq_id
       left join business.cust scnd_orig on scnd_orig.cust_seq_id =
                 nvl(ct.rf_cndtr_cust_seq_id, business.rf_pkg_util.get_representative(ct.rf_cust_seq_id, ct.rf_cndtr_doc_type_cd, ct.rf_cndtr_doc_series_id, ct.rf_cndtr_doc_no_id))
       left join business.cust_addr orig_addr on orig_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(ct.rf_cust_seq_id, 'L')       
       left join business.cust_addr orig_fl_addr on orig_fl_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(orig_fl.cust_seq_id, 'L')       
       left join business.rf_addr_object orig_addr_state on orig_addr_state.addr_seq_id = coalesce(orig_addr.rf_state_addr_seq_id,
                                                                                                   case when orig_addr.rf_state_nm is not null
                                                                                                        then std.aml_tools.recognise_region2(orig_addr.rf_state_nm)
                                                                                                   end,
                                                                                                   case when orig_addr.addr_city_nm is not null
                                                                                                        then std.aml_tools.recognise_region2(orig_addr.addr_city_nm)
                                                                                                   end)
       left join business.cust_addr scnd_orig_addr on scnd_orig_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(scnd_orig.cust_seq_id, 'L')
       left join business.rf_addr_object scnd_orig_addr_state on scnd_orig_addr_state.addr_seq_id = coalesce(scnd_orig_addr.rf_state_addr_seq_id,
                                                                                                             case when scnd_orig_addr.rf_state_nm is not null
                                                                                                                  then std.aml_tools.recognise_region2(scnd_orig_addr.rf_state_nm)
                                                                                                             end,
                                                                                                             case when scnd_orig_addr.addr_city_nm is not null
                                                                                                                  then std.aml_tools.recognise_region2(scnd_orig_addr.addr_city_nm)
                                                                                                             end)
       left join business.cust_addr orig_addr_h on orig_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(ct.rf_cust_seq_id, 'H')
       left join business.rf_addr_object orig_addr_state_h on orig_addr_state_h.addr_seq_id = coalesce(orig_addr_h.rf_state_addr_seq_id,
                                                                                                       case when orig_addr_h.rf_state_nm is not null
                                                                                                            then std.aml_tools.recognise_region2(orig_addr_h.rf_state_nm)
                                                                                                       end,
                                                                                                       case when orig_addr_h.addr_city_nm is not null
                                                                                                            then std.aml_tools.recognise_region2(orig_addr_h.addr_city_nm)
                                                                                                       end)
       left join business.cust_addr scnd_orig_addr_h on scnd_orig_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(scnd_orig.cust_seq_id, 'H')
       left join business.rf_addr_object scnd_orig_addr_state_h on scnd_orig_addr_state_h.addr_seq_id = coalesce(scnd_orig_addr_h.rf_state_addr_seq_id,
                                                                                                                 case when scnd_orig_addr_h.rf_state_nm is not null
                                                                                                                      then std.aml_tools.recognise_region2(scnd_orig_addr_h.rf_state_nm)
                                                                                                                 end,
                                                                                                                 case when scnd_orig_addr_h.addr_city_nm is not null
                                                                                                                      then std.aml_tools.recognise_region2(scnd_orig_addr_h.addr_city_nm)
                                                                                                                 end)
       left join business.rf_cust_id_doc orig_doc on orig_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(ct.rf_cust_seq_id,ct.rf_cust_doc_type_cd, ct.rf_cust_doc_series_id, ct.rf_cust_doc_no_id)
       left join business.rf_cust_id_doc scnd_orig_doc on scnd_orig_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(scnd_orig.cust_seq_id,ct.rf_cndtr_doc_type_cd, ct.rf_cndtr_doc_series_id, ct.rf_cndtr_doc_no_id)
       left join table(business.rf_pkg_util.format_address(ct.src_sys_cd, ct.rf_cust_reg_addr_tx)) orig_addr_f on 1=1
       left join table(business.rf_pkg_util.format_address(ct.src_sys_cd, ct.rf_cndtr_reg_addr_tx)) scnd_orig_addr_f on 1=1
       left join table(business.rf_pkg_util.format_address(ct.src_sys_cd, ct.rf_cust_fact_addr_tx)) orig_addr_hf on 1=1
       left join table(business.rf_pkg_util.format_address(ct.src_sys_cd, ct.rf_cndtr_fact_addr_tx)) scnd_orig_addr_hf on 1=1                 
       left join business.acct acct on acct.rowid = business.rf_pkg_util.get_acct2(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF', 
                                                                                   ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd)                    
       left join business.org  tb on tb.org_intrl_id = to_char(trunc(nvl(acct.rf_branch_id, ct.rf_branch_id)/100000))
       left join business.cust tb_cust on tb_cust.cust_seq_id = tb.cstm_1_rl       
       left join business.rf_trxn_extra trxe ON trxe.op_cat_cd = 'CT' and trxe.fo_trxn_seq_id = ct.fo_trxn_seq_id 
       left join mantas.rf_oes_321_org send_org ON send_org.oes_321_org_id = case when trxe.rowid is not null 
                                                                                  then trxe.send_oes_org_id
                                                                                  when ct.dbt_cdt_cd = 'D'  
                                                                                  then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(ct.trxn_exctn_dt))
                                                                             end       
       left join mantas.rf_oes_321_org rcv_org  ON rcv_org.oes_321_org_id  = case when trxe.rowid is not null 
                                                                                  then trxe.rcv_oes_org_id
                                                                                  when ct.dbt_cdt_cd = 'C'  
                                                                                  then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(ct.trxn_exctn_dt))
                                                                             end       
       cross join party_van p
       where ct.fo_trxn_seq_id = p_trxn_id     
    ) loop
           if r.adh_from_adr_fl = 1 Then
             result.adh_country_cd := r.adr_country_cd;
             result.adh_post_index := r.adr_post_index;
             result.adh_state_nm := r.adr_state_nm;
             result.adh_state_type_cd := r.adr_state_type_cd;
             result.adh_state_seq_id := r.adr_state_seq_id;
             result.adh_district_type_cd := r.adr_district_type_cd;
             result.adh_district_nm := r.adr_district_nm;
             result.adh_city_type_cd := r.adr_city_type_cd;
             result.adh_city_nm := r.adr_city_nm;
             result.adh_settlm_type_cd := r.adr_settlm_type_cd;
             result.adh_settlm_nm := r.adr_settlm_nm;
             result.adh_street_name_tx := r.adr_street_name_tx;
             result.adh_street_type_tx := r.adr_street_type_tx;
             result.adh_house_number_tx := r.adr_house_number_tx;
             result.adh_building_number_tx := r.adr_building_number_tx;
             result.adh_flat_number_tx := r.adr_flat_number_tx;
             result.adh_unstruct_fl := r.adr_unstruct_fl;
             result.adh_unstruct_tx := r.adr_unstruct_tx;
             result.fact_addr_string_trxn := r.regist_addr_string_trxn;
             result.fact_addr_string_custaddr := r.regist_addr_string_custaddr;
           else  
             result.adh_country_cd := r.adh_country_cd;
             result.adh_post_index := r.adh_post_index;
             result.adh_state_nm := r.adh_state_nm;
             result.adh_state_type_cd := r.adh_state_type_cd;
             result.adh_state_seq_id := r.adh_state_seq_id;
             result.adh_district_type_cd := r.adh_district_type_cd;
             result.adh_district_nm := r.adh_district_nm;
             result.adh_city_type_cd := r.adh_city_type_cd;
             result.adh_city_nm := r.adh_city_nm;
             result.adh_settlm_type_cd := r.adh_settlm_type_cd;
             result.adh_settlm_nm := r.adh_settlm_nm;
             result.adh_street_name_tx := r.adh_street_name_tx;
             result.adh_street_type_tx := r.adh_street_type_tx;
             result.adh_house_number_tx := r.adh_house_number_tx;
             result.adh_building_number_tx := r.adh_building_number_tx;
             result.adh_flat_number_tx := r.adh_flat_number_tx;
             result.adh_unstruct_fl := r.adh_unstruct_fl;
             result.adh_unstruct_tx := r.adh_unstruct_tx;
             result.fact_addr_string_trxn := r.fact_addr_string_trxn;
             result.fact_addr_string_custaddr := r.fact_addr_string_custaddr;
           end if;  
           result.adh_from_adr_fl := r.adh_from_adr_fl;               
           result.doc_type_cd := r.doc_type_cd;
           result.document_series := r.document_series;
           result.document_no := r.document_no;
           result.document_issued_dpt_tx := r.document_issued_dpt_tx;
           result.document_issued_dpt_cd := r.document_issued_dpt_cd;
           result.document_issued_dt := r.document_issued_dt;
           result.document_end_dt := r.document_end_dt;
           result.valid_fl := r.valid_fl;
           result.stay_cust_doc_seq_id := r.stay_cust_doc_seq_id;
           result.mc_cust_doc_seq_id := r.mc_cust_doc_seq_id;
           result.regist_addr_string_trxn := r.regist_addr_string_trxn;
           result.regist_addr_string_custaddr := r.regist_addr_string_custaddr;
           result.acct_seq_id := r.acct_seq_id;
           result.acct_nb := r.acct_nb;
           result.goz_op_cd := r.goz_op_cd;
           result.deb_cred_acct_nb := r.deb_cred_acct_nb;
           result.instn_nm := r.instn_nm;
           result.instn_id := r.instn_id;
           result.instn_cntry_cd := r.instn_cntry_cd;
           result.instn_acct_id := r.instn_acct_id;
           result.branch_id := r.branch_id;
           result.oes_org_id := r.oes_org_id;
           result.kgrko_nm := r.kgrko_nm;
           result.kgrko_id := r.kgrko_id;
           result.bank_card_id_nb := r.bank_card_id_nb;
           result.bank_card_trxn_dt := r.bank_card_trxn_dt;
           result.src_sys_cd := r.src_sys_cd;
           result.op_cat_cd := r.op_cat_cd;
           result.trxn_seq_id := r.trxn_seq_id;
           result.party_cd := r.party_cd;
           result.cust_seq_id := r.cust_seq_id;
           result.cust_fl_seq_id := r.cust_fl_seq_id;
           result.cust_type_cd := r.cust_type_cd;
           result.resident_fl := r.resident_fl;
           result.cust_nm := r.cust_nm;
           result.cust_fl_nm := r.cust_fl_nm;
           result.gender_cd := r.gender_cd;
           result.inn_nb := r.inn_nb;
           result.ogrn_nb := r.ogrn_nb;
           result.birth_dt := r.birth_dt;
           result.birthplace_tx := r.birthplace_tx;
           result.kpp_nb := r.kpp_nb;
           result.citizenship_cd := r.citizenship_cd;
           result.reg_org_nm := r.reg_org_nm;
           result.okved_nb := r.okved_nb;
           result.okpo_nb := r.okpo_nb;
           result.reg_dt := r.reg_dt;
           result.adr_country_cd := r.adr_country_cd;
           result.adr_post_index := r.adr_post_index;
           result.adr_state_nm := r.adr_state_nm;
           result.adr_state_type_cd := r.adr_state_type_cd;
           result.adr_state_seq_id := r.adr_state_seq_id;
           result.adr_district_type_cd := r.adr_district_type_cd;
           result.adr_district_nm := r.adr_district_nm;
           result.adr_city_type_cd := r.adr_city_type_cd;
           result.adr_city_nm := r.adr_city_nm;
           result.adr_settlm_type_cd := r.adr_settlm_type_cd;
           result.adr_settlm_nm := r.adr_settlm_nm;
           result.adr_street_name_tx := r.adr_street_name_tx;
           result.adr_street_type_tx := r.adr_street_type_tx;
           result.adr_house_number_tx := r.adr_house_number_tx;
           result.adr_building_number_tx := r.adr_building_number_tx;
           result.adr_flat_number_tx := r.adr_flat_number_tx;
           result.adr_unstruct_fl := r.adr_unstruct_fl;
           result.adr_unstruct_tx := r.adr_unstruct_tx;
           result.fl_adr_country_cd := r.fl_adr_country_cd;
           --
           -- ������� ��� ��������� �������, ���� ����� ��� ������������ ��� ��������:
           --  ������, �����, �����, ���������� �����, �����
           -- � ������� ����������� � ����� ����������
           --
           if result.adr_state_nm is null       then result.adr_state_type_cd := null; end if;
           if result.adr_district_nm is null    then result.adr_district_type_cd := null; end if;
           if result.adr_city_nm is null        then result.adr_city_type_cd := null; end if;
           if result.adr_settlm_nm is null      then result.adr_settlm_type_cd := null; end if;
           if result.adr_street_name_tx is null then result.adr_street_type_tx := null; end if;

           if result.adh_state_nm is null       then result.adh_state_type_cd := null; end if;
           if result.adh_district_nm is null    then result.adh_district_type_cd := null; end if;
           if result.adh_city_nm is null        then result.adh_city_type_cd := null; end if;
           if result.adh_settlm_nm is null      then result.adh_settlm_type_cd := null; end if;
           if result.adh_street_name_tx is null then result.adh_street_type_tx := null; end if;

           pipe row(result);
    end loop;
  end if;
  
--
--
-- ������ ������� �� ������� BACK_OFFICE_TRXN
--
--  
  if (p_op_cat_cd = 'BOT') and (p_trxn_id is not null) then
    for r in (
          with party_van as
          (select 1, 'ORIG' as party_cd from dual union all
          select 2, 'BENEF' as party_cd from dual union all
          select 3, 'SCND_ORIG' as party_cd from dual union all
          select 4, 'SCND_BENEF' as party_cd from dual
          )
    SELECT
       /*+ ORDERED USE_NL(party_van acct orig orig_fl orig_addr orig_fl_addr orig_addr_state orig_addr_h orig_addr_state_h orig_doc tb tb_cust trxe send_org rcv_org)*/
       'BOT' as op_cat_cd,
       bot.bo_trxn_seq_id as trxn_seq_id,
       p.party_cd,
       --
       --�������� � �����������
       --
       -- id �������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.cust_seq_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.cust_seq_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as cust_seq_id,
       -- id ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_fl.cust_seq_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_fl.cust_seq_id end,
                          'SCND_ORIG',  to_number(null),
                          'SCND_BENEF', to_number(null)) as cust_fl_seq_id,
       -- ��� �������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.cust_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.cust_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as cust_type_cd,
       -- ��������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.rf_resident_fl end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.rf_resident_fl end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as resident_fl,
       -- ������������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.full_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.full_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as cust_nm,
       -- ��� ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_fl.full_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_fl.full_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as cust_fl_nm,
       -- ���
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_fl.cust_gndr_cd, orig.cust_gndr_cd) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_fl.cust_gndr_cd, orig.cust_gndr_cd) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as gender_cd,
       -- ���
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.tax_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.tax_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as inn_nb,
       -- ����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.rf_ogrn_nb end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.rf_ogrn_nb end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as ogrn_nb,
       -- ���� ��������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_fl.birth_dt, orig.birth_dt) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_fl.birth_dt, orig.birth_dt) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as birth_dt,
       -- ����� ��������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_fl.rf_birthplace_tx, orig.rf_birthplace_tx) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_fl.rf_birthplace_tx, orig.rf_birthplace_tx) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as birthplace_tx,
       -- ���
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.rf_kpp_nb end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.rf_kpp_nb end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as kpp_nb,
       -- ����������� (��� ������� �����������, � ��� ���� ��������� ����� �����������)
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.ctzshp_cntry1_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.ctzshp_cntry1_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as citizenship_cd,
       -- �������������� �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.rf_reg_org_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.rf_reg_org_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as reg_org_nm,
       -- �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then business.rf_pkg_util.get_list_clsf_code(orig.cust_seq_id,'1') end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then business.rf_pkg_util.get_list_clsf_code(orig.cust_seq_id,'1') end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as okved_nb,
       -- ����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.rf_okpo_nb end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.rf_okpo_nb end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as okpo_nb,
       -- ���� ����������� �������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig.rf_reg_dt end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig.rf_reg_dt end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as reg_dt,
       --
       --����� �����������
       --
       -- ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_addr_state.cntry_cd, orig_addr.addr_cntry_cd) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_addr_state.cntry_cd, orig_addr.addr_cntry_cd) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_country_cd,
       -- ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_postl_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_postl_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_post_index,
       -- ������������ �������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_addr_state.object_nm, orig_addr.rf_state_nm) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_addr_state.object_nm, orig_addr.rf_state_nm) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_state_nm,
       -- ��� ������� - ���������� (��������, ���, ����, ��)
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_addr_state.addr_type_cd, orig_addr.rf_state_type_cd) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_addr_state.addr_type_cd, orig_addr.rf_state_type_cd) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_state_type_cd,
       -- ������ �� ��������� �������� ������, ��������������� �������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_state.addr_seq_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_state.addr_seq_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_state_seq_id,
      -- ��� ������ ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.rf_rgn_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.rf_rgn_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_district_type_cd,
       -- ����� ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_rgn_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_rgn_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_district_nm,
       -- ��� ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.rf_city_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.rf_city_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_city_type_cd,
       -- �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_city_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_city_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_city_nm,
       -- ��� ����������� ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.rf_settlm_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.rf_settlm_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_settlm_type_cd,
       -- ���������� �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.rf_settlm_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.rf_settlm_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_settlm_nm,
       -- ����� (������������ ��� ����, ��������, ��������)
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_strt_line1_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_strt_line1_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_street_name_tx,
       -- ��� ����� - ���������� (��������, ��, �-�, ��-��, ������)
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_strt_line2_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_strt_line2_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_street_type_tx,
       -- ����� ����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_strt_line3_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_strt_line3_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_house_number_tx,
       -- ����� �������/��������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_strt_line4_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_strt_line4_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_building_number_tx,
       -- ����� ��������/�����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.addr_strt_line5_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.addr_strt_line5_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_flat_number_tx,
       -- ���� (0/1) �������������������� ������ ����� ����������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D'
                                             then case when orig_addr.rowid is null or
                                                            (orig_addr.rf_addr_tx is not null and
                                                             orig_addr.addr_city_nm||orig_addr.rf_settlm_nm||orig_addr.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C'
                                             then case when orig_addr.rowid is null or
                                                            (orig_addr.rf_addr_tx is not null and
                                                             orig_addr.addr_city_nm||orig_addr.rf_settlm_nm||orig_addr.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_unstruct_fl,
       -- ����� �������������������� ������ �����������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.rf_addr_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.rf_addr_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adr_unstruct_tx,
       -- ������ �� ������ ����������� ������� - ���. ����, ���������������� ������� ������� - ��
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_fl_addr.addr_cntry_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_fl_addr.addr_cntry_cd end,
                          'SCND_ORIG',  to_char(null),
                          'SCND_BENEF', to_char(null)) as fl_adr_country_cd,
       --
       -- ����� ����� ����������
       --
       -- ����: ���������� � ����� ����� ���������� ����� �����������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' 
                                             then case when /*orig.cust_type_cd <> 'IND' and*/ orig_addr_h.rowid is null then 1 else 0 end
                                        end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' 
                                             then case when /*orig.cust_type_cd <> 'IND' and*/ orig_addr_h.rowid is null then 1 else 0 end
                                        end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_from_adr_fl,
       -- ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_addr_state_h.cntry_cd, orig_addr_h.addr_cntry_cd) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_addr_state_h.cntry_cd, orig_addr_h.addr_cntry_cd) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_country_cd,
       -- ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_postl_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_postl_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_post_index,
       -- ������������ �������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_addr_state_h.object_nm, orig_addr_h.rf_state_nm) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_addr_state_h.object_nm, orig_addr_h.rf_state_nm) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_state_nm,
       -- ��� ������� - ���������� (��������, ���, ����, ��)
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nvl(orig_addr_state_h.addr_type_cd, orig_addr_h.rf_state_type_cd) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nvl(orig_addr_state_h.addr_type_cd, orig_addr_h.rf_state_type_cd) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_state_type_cd,
       -- ������ �� ��������� �������� ������, ��������������� �������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_state_h.addr_seq_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_state_h.addr_seq_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_state_seq_id,
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr.rf_rgn_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr.rf_rgn_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_district_type_cd,
       -- ����� ������� (�������), ������������ ��� ����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_rgn_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_rgn_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_district_nm,
       -- ��� ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.rf_city_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.rf_city_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_city_type_cd,
       -- �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_city_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_city_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_city_nm,
       -- ��� ����������� ������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.rf_settlm_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.rf_settlm_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_settlm_type_cd,
       -- ���������� �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.rf_settlm_nm end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.rf_settlm_nm end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_settlm_nm,
       -- ����� (������������ ��� ����, ��������, ��������)
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_strt_line1_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_strt_line1_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_street_name_tx,
       -- ��� ����� - ���������� (��������, ��, �-�, ��-��, ������)
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_strt_line2_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_strt_line2_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_street_type_tx,
       -- ����� ����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_strt_line3_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_strt_line3_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_house_number_tx,
       -- ����� �������/��������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_strt_line4_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_strt_line4_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_building_number_tx,
       -- ����� ��������/�����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.addr_strt_line5_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.addr_strt_line5_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_flat_number_tx,
       -- ���� (0/1) �������������������� ������ ����� ����������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D'
                                             then case when orig_addr_h.rowid is null or
                                                            (orig_addr_h.rf_addr_tx is not null and
                                                             orig_addr_h.addr_city_nm||orig_addr_h.rf_settlm_nm||orig_addr_h.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C'
                                             then case when orig_addr_h.rowid is null or
                                                            (orig_addr_h.rf_addr_tx is not null and
                                                             orig_addr_h.addr_city_nm||orig_addr_h.rf_settlm_nm||orig_addr_h.addr_strt_line1_tx is null)
                                                       then 1 else 0 end
                                        end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_unstruct_fl,
       -- ����� �������������������� ������ �����������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_addr_h.rf_addr_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_addr_h.rf_addr_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as adh_unstruct_tx,
       --
       -- �������������� ��������
       --
       -- ��� ���������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.doc_type_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.doc_type_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as doc_type_cd,
       -- ����� ���������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.series_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.series_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as document_series,
       -- ����� ���������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.no_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.no_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as document_no,
       -- ����� �������� ��������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.issued_by_tx end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.issued_by_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as document_issued_dpt_tx,
       -- ��� �������������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.issue_dpt_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.issue_dpt_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as document_issued_dpt_cd,
       -- ���� ������ ���������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.issue_dt end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.issue_dt end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as document_issued_dt,
       -- ���� ��������� �������� ���������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.end_dt end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.end_dt end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as document_end_dt,
       -- ����������� �� ��������?
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then orig_doc.valid_fl end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then orig_doc.valid_fl end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as valid_fl,
       --
       -- �������������� �������������� ���������
       --
       -- ��������, �������������� ����� �� ���������� - ??? �������� ???
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, null, 'Y')) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, null, 'Y')) end,
                          'SCND_ORIG',  case when bot.dbt_cdt_cd = 'D' then to_number(null) end,
                          'SCND_BENEF', case when bot.dbt_cdt_cd = 'C' then to_number(null) end) as stay_cust_doc_seq_id,
       -- ������������ ����� - ??? �������� ???
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, 39)) end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then to_number(business.rf_pkg_util.get_id_doc(orig.cust_seq_id, 39)) end,
                          'SCND_ORIG',  case when bot.dbt_cdt_cd = 'D' then to_number(null) end,
                          'SCND_BENEF', case when bot.dbt_cdt_cd = 'C' then to_number(null) end) as mc_cust_doc_seq_id,
       -- ����� ����������� ������� �� ��������
       decode(p.party_cd, 'ORIG',       null,
                          'BENEF',      null,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as regist_addr_string_trxn,
       -- ����� ���������� ������� �� ��������
       decode(p.party_cd, 'ORIG',       null,
                          'BENEF',      null,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as fact_addr_string_trxn,
       -- ����� ����������� ������� �� ������� � ��������
       decode(p.party_cd, 'ORIG',       business.rf_pkg_util.get_addr_text(case when bot.dbt_cdt_cd = 'D' then orig_addr.cust_addr_seq_id end),
                          'BENEF',      business.rf_pkg_util.get_addr_text(case when bot.dbt_cdt_cd = 'C' then orig_addr.cust_addr_seq_id end),
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as regist_addr_string_custaddr,
       -- ����� ���������� ������� �� ������� � ��������
       decode(p.party_cd, 'ORIG',       business.rf_pkg_util.get_addr_text(case when bot.dbt_cdt_cd = 'D' then orig_addr_h.cust_addr_seq_id end),
                          'BENEF',      business.rf_pkg_util.get_addr_text(case when bot.dbt_cdt_cd = 'C' then orig_addr_h.cust_addr_seq_id end),
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as fact_addr_string_custaddr,
       -- ���������� ������������� �����. ����������� ������ ��� "����������" ������, ������������� ������� BUSINESS.ACCT.ACCT_SEQ_ID
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then bot.rf_acct_seq_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then bot.rf_acct_seq_id end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as acct_seq_id,
       -- ����� �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then nullif(acct.alt_acct_id, '-1') end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then nullif(acct.alt_acct_id, '-1') end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as acct_nb,
       -- ��� �������� ���
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then bot.rf_acct_goz_op_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then bot.rf_acct_goz_op_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as goz_op_cd,
       -- ���� ������ ��� �������, ��������� � ������ ���������
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then bot.rf_debit_cd end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then bot.rf_credit_cd end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as deb_cred_acct_nb,
       -- ������������ �����
       decode(p.party_cd, 'ORIG',       case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' then nvl(tb_cust.full_nm, tb.org_nm) end,
                          'BENEF',      case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' then nvl(tb_cust.full_nm, tb.org_nm) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_nm,
       -- ���/SWIFT �����
       decode(p.party_cd, 'ORIG',       case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx) end,
                          'BENEF',      case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx) end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_id,
       -- ������ �����
       decode(p.party_cd, 'ORIG',       case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' then 'RU' end,
                          'BENEF',      case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' then 'RU' end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_cntry_cd,
       -- ����. ���� �����
       decode(p.party_cd, 'ORIG',       case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' then tb.cstm_3_tx end,
                          'BENEF',      case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' then tb.cstm_3_tx end,
                          'SCND_ORIG',  null,
                          'SCND_BENEF', null) as instn_acct_id,
       -- ��� ��/��� �� �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then acct.rf_branch_id end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then acct.rf_branch_id end,
                          'SCND_ORIG',  case when bot.dbt_cdt_cd = 'D' then acct.rf_branch_id end,
                          'SCND_BENEF', case when bot.dbt_cdt_cd = 'C' then acct.rf_branch_id end) as branch_id,
       -- ID ������� �����
       decode(p.party_cd, 'ORIG',       send_org.oes_321_org_id,
                          'BENEF',      rcv_org.oes_321_org_id,
                          'SCND_ORIG',  send_org.oes_321_org_id,
                          'SCND_BENEF', rcv_org.oes_321_org_id) as oes_org_id,
       -- ������������ ������� �����
       decode(p.party_cd, 'ORIG',       send_org.tb_name,
                          'BENEF',      rcv_org.tb_name,
                          'SCND_ORIG',  send_org.tb_name,
                          'SCND_BENEF', rcv_org.tb_name) as kgrko_nm,
       -- ����� ������� ����� (�������, ���� �������� ��)
       decode(p.party_cd, 'ORIG',       case when send_org.oes_321_org_id is not null
                                             then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                                                 '1', nullif(send_org.numbf_ss, '0')), '-') 
                                        end,
                          'BENEF',      case when rcv_org.oes_321_org_id is not null
                                             then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                                                '1', nullif(rcv_org.numbf_ss, '0')), '-') 
                                        end,
                          'SCND_ORIG',  case when send_org.oes_321_org_id is not null
                                             then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                                                 '1', nullif(send_org.numbf_ss, '0')), '-') 
                                        end,
                          'SCND_BENEF', case when rcv_org.oes_321_org_id is not null
                                             then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                                                '1', nullif(rcv_org.numbf_ss, '0')), '-') 
                                        end) as kgrko_id,       
       -- ����� ���������� �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then bot.bank_card_id_nb end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then bot.bank_card_id_nb end,
                          'SCND_ORIG',  to_number(null),
                          'SCND_BENEF', to_number(null)) as bank_card_id_nb,
       -- ���� �������� �� ���������� �����
       decode(p.party_cd, 'ORIG',       case when bot.dbt_cdt_cd = 'D' then bot.cstm_1_dt end,
                          'BENEF',      case when bot.dbt_cdt_cd = 'C' then bot.cstm_1_dt end,
                          'SCND_ORIG',  to_date(null),
                          'SCND_BENEF', to_date(null)) as bank_card_trxn_dt,
      -----------------------------------
       bot.src_sys_cd as src_sys_cd
  from -- �������� �������� ������ �������� �� ��������� �������� ����, � ���� ����� ��� - ������� ������ ��������
       (select /*+ INDEX(back_office_trxn PK_BO_TRXN)*/
               bo_trxn_seq_id, dbt_cdt_cd, exctn_dt,
               rf_acct_seq_id, rf_acct_goz_op_cd, rf_debit_cd, rf_credit_cd, rf_branch_id, 
               bank_card_id_nb, cstm_1_dt, src_sys_cd
          from business.back_office_trxn        
         where bo_trxn_seq_id = p_trxn_id and
               not exists(select null 
                            from business.back_office_trxn_arh
                           where bo_trxn_seq_id = p_trxn_id and
                                 arh_dump_dt = p_arh_dump_dt)
        union all
        select /*+ INDEX(back_office_trxn_arh PK_BO_TRXN_ARH)*/
               bo_trxn_seq_id, dbt_cdt_cd, exctn_dt,
               rf_acct_seq_id, rf_acct_goz_op_cd, rf_debit_cd, rf_credit_cd, rf_branch_id, 
               bank_card_id_nb, cstm_1_dt, src_sys_cd                 
          from business.back_office_trxn_arh
         where bo_trxn_seq_id = p_trxn_id and
               arh_dump_dt = p_arh_dump_dt) bot
       left join business.acct acct on acct.rowid = business.rf_pkg_util.get_acct2(bot.rf_acct_seq_id)                    
       left join business.cust orig on orig.cust_seq_id = acct.rf_prmry_cust_seq_id
       left join business.cust orig_fl on orig_fl.cust_seq_id = orig.rf_ind_cust_seq_id
       left join business.cust_addr orig_addr on orig_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(acct.rf_prmry_cust_seq_id, 'L')
       left join business.cust_addr orig_fl_addr on orig_fl_addr.rowid = business.rf_pkg_util.get_primary_cust_addr(orig_fl.cust_seq_id, 'L')
       left join business.rf_addr_object orig_addr_state on orig_addr_state.addr_seq_id = coalesce(orig_addr.rf_state_addr_seq_id,
                                                                                                   case when orig_addr.rf_state_nm is not null
                                                                                                        then std.aml_tools.recognise_region2(orig_addr.rf_state_nm)
                                                                                                   end,
                                                                                                   case when orig_addr.addr_city_nm is not null
                                                                                                        then std.aml_tools.recognise_region2(orig_addr.addr_city_nm)
                                                                                                   end)
       left join business.cust_addr orig_addr_h on orig_addr_h.rowid = business.rf_pkg_util.get_primary_cust_addr(acct.rf_prmry_cust_seq_id, 'H')
       left join business.rf_addr_object orig_addr_state_h on orig_addr_state_h.addr_seq_id = coalesce(orig_addr_h.rf_state_addr_seq_id,
                                                                                                       case when orig_addr_h.rf_state_nm is not null
                                                                                                            then std.aml_tools.recognise_region2(orig_addr_h.rf_state_nm)
                                                                                                       end,
                                                                                                       case when orig_addr_h.addr_city_nm is not null
                                                                                                            then std.aml_tools.recognise_region2(orig_addr_h.addr_city_nm)
                                                                                                       end)
       left join business.rf_cust_id_doc orig_doc on orig_doc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(acct.rf_prmry_cust_seq_id)
       left join business.org  tb on tb.org_intrl_id = to_char(trunc(nvl(acct.rf_branch_id, bot.rf_branch_id)/100000))
       left join business.cust tb_cust on tb_cust.cust_seq_id = tb.cstm_1_rl       
       left join business.rf_trxn_extra trxe ON trxe.op_cat_cd = 'BOT' and trxe.fo_trxn_seq_id = bot.bo_trxn_seq_id 
       left join mantas.rf_oes_321_org send_org ON send_org.oes_321_org_id = case when trxe.rowid is not null 
                                                                                  then trxe.send_oes_org_id
                                                                                  when bot.dbt_cdt_cd = 'D'  
                                                                                  then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(bot.exctn_dt))
                                                                             end       
       left join mantas.rf_oes_321_org rcv_org  ON rcv_org.oes_321_org_id  = case when trxe.rowid is not null 
                                                                                  then trxe.rcv_oes_org_id
                                                                                  when bot.dbt_cdt_cd = 'C'  
                                                                                  then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(bot.exctn_dt))
                                                                             end       
       cross join party_van p
       where bot.bo_trxn_seq_id = p_trxn_id
    ) loop
           if r.adh_from_adr_fl = 1 Then
             result.adh_country_cd := r.adr_country_cd;
             result.adh_post_index := r.adr_post_index;
             result.adh_state_nm := r.adr_state_nm;
             result.adh_state_type_cd := r.adr_state_type_cd;
             result.adh_state_seq_id := r.adr_state_seq_id;
             result.adh_district_type_cd := r.adr_district_type_cd;
             result.adh_district_nm := r.adr_district_nm;
             result.adh_city_type_cd := r.adr_city_type_cd;
             result.adh_city_nm := r.adr_city_nm;
             result.adh_settlm_type_cd := r.adr_settlm_type_cd;
             result.adh_settlm_nm := r.adr_settlm_nm;
             result.adh_street_name_tx := r.adr_street_name_tx;
             result.adh_street_type_tx := r.adr_street_type_tx;
             result.adh_house_number_tx := r.adr_house_number_tx;
             result.adh_building_number_tx := r.adr_building_number_tx;
             result.adh_flat_number_tx := r.adr_flat_number_tx;
             result.adh_unstruct_fl := r.adr_unstruct_fl;
             result.adh_unstruct_tx := r.adr_unstruct_tx;
             result.fact_addr_string_trxn := r.regist_addr_string_trxn;
             result.fact_addr_string_custaddr := r.regist_addr_string_custaddr;
           else  
             result.adh_country_cd := r.adh_country_cd;
             result.adh_post_index := r.adh_post_index;
             result.adh_state_nm := r.adh_state_nm;
             result.adh_state_type_cd := r.adh_state_type_cd;
             result.adh_state_seq_id := r.adh_state_seq_id;
             result.adh_district_type_cd := r.adh_district_type_cd;
             result.adh_district_nm := r.adh_district_nm;
             result.adh_city_type_cd := r.adh_city_type_cd;
             result.adh_city_nm := r.adh_city_nm;
             result.adh_settlm_type_cd := r.adh_settlm_type_cd;
             result.adh_settlm_nm := r.adh_settlm_nm;
             result.adh_street_name_tx := r.adh_street_name_tx;
             result.adh_street_type_tx := r.adh_street_type_tx;
             result.adh_house_number_tx := r.adh_house_number_tx;
             result.adh_building_number_tx := r.adh_building_number_tx;
             result.adh_flat_number_tx := r.adh_flat_number_tx;
             result.adh_unstruct_fl := r.adh_unstruct_fl;
             result.adh_unstruct_tx := r.adh_unstruct_tx;
             result.fact_addr_string_trxn := r.fact_addr_string_trxn;
             result.fact_addr_string_custaddr := r.fact_addr_string_custaddr;
           end if;  
           result.adh_from_adr_fl := r.adh_from_adr_fl;               
           result.doc_type_cd := r.doc_type_cd;
           result.document_series := r.document_series;
           result.document_no := r.document_no;
           result.document_issued_dpt_tx := r.document_issued_dpt_tx;
           result.document_issued_dpt_cd := r.document_issued_dpt_cd;
           result.document_issued_dt := r.document_issued_dt;
           result.document_end_dt := r.document_end_dt;
           result.valid_fl := r.valid_fl;
           result.stay_cust_doc_seq_id := r.stay_cust_doc_seq_id;
           result.mc_cust_doc_seq_id := r.mc_cust_doc_seq_id;
           result.regist_addr_string_trxn := r.regist_addr_string_trxn;
           result.regist_addr_string_custaddr := r.regist_addr_string_custaddr;
           result.acct_seq_id := r.acct_seq_id;
           result.acct_nb := r.acct_nb;
           result.goz_op_cd := r.goz_op_cd;
           result.deb_cred_acct_nb := r.deb_cred_acct_nb;
           result.instn_nm := r.instn_nm;
           result.instn_id := r.instn_id;
           result.instn_cntry_cd := r.instn_cntry_cd;
           result.instn_acct_id := r.instn_acct_id;
           result.branch_id := r.branch_id;
           result.oes_org_id := r.oes_org_id;
           result.kgrko_nm := r.kgrko_nm;
           result.kgrko_id := r.kgrko_id;           
           result.bank_card_id_nb := r.bank_card_id_nb;
           result.bank_card_trxn_dt := r.bank_card_trxn_dt;
           result.src_sys_cd := r.src_sys_cd;
           result.op_cat_cd := r.op_cat_cd;
           result.trxn_seq_id := r.trxn_seq_id;
           result.party_cd := r.party_cd;
           result.cust_seq_id := r.cust_seq_id;
           result.cust_fl_seq_id := r.cust_fl_seq_id;
           result.cust_type_cd := r.cust_type_cd;
           result.resident_fl := r.resident_fl;
           result.cust_nm := r.cust_nm;
           result.cust_fl_nm := r.cust_fl_nm;
           result.gender_cd := r.gender_cd;
           result.inn_nb := r.inn_nb;
           result.ogrn_nb := r.ogrn_nb;
           result.birth_dt := r.birth_dt;
           result.birthplace_tx := r.birthplace_tx;
           result.kpp_nb := r.kpp_nb;
           result.citizenship_cd := r.citizenship_cd;
           result.reg_org_nm := r.reg_org_nm;
           result.okved_nb := r.okved_nb;
           result.okpo_nb := r.okpo_nb;
           result.reg_dt := r.reg_dt;
           result.adr_country_cd := r.adr_country_cd;
           result.adr_post_index := r.adr_post_index;
           result.adr_state_nm := r.adr_state_nm;
           result.adr_state_type_cd := r.adr_state_type_cd;
           result.adr_state_seq_id := r.adr_state_seq_id;
           result.adr_district_type_cd := r.adr_district_type_cd;
           result.adr_district_nm := r.adr_district_nm;
           result.adr_city_type_cd := r.adr_city_type_cd;
           result.adr_city_nm := r.adr_city_nm;
           result.adr_settlm_type_cd := r.adr_settlm_type_cd;
           result.adr_settlm_nm := r.adr_settlm_nm;
           result.adr_street_name_tx := r.adr_street_name_tx;
           result.adr_street_type_tx := r.adr_street_type_tx;
           result.adr_house_number_tx := r.adr_house_number_tx;
           result.adr_building_number_tx := r.adr_building_number_tx;
           result.adr_flat_number_tx := r.adr_flat_number_tx;
           result.adr_unstruct_fl := r.adr_unstruct_fl;
           result.adr_unstruct_tx := r.adr_unstruct_tx;
           result.fl_adr_country_cd := r.fl_adr_country_cd;
           --
           -- ������� ��� ��������� �������, ���� ����� ��� ������������ ��� ��������:
           --  ������, �����, �����, ���������� �����, �����
           -- � ������� ����������� � ����� ����������
           --
           if result.adr_state_nm is null       then result.adr_state_type_cd := null; end if;
           if result.adr_district_nm is null    then result.adr_district_type_cd := null; end if;
           if result.adr_city_nm is null        then result.adr_city_type_cd := null; end if;
           if result.adr_settlm_nm is null      then result.adr_settlm_type_cd := null; end if;
           if result.adr_street_name_tx is null then result.adr_street_type_tx := null; end if;

           if result.adh_state_nm is null       then result.adh_state_type_cd := null; end if;
           if result.adh_district_nm is null    then result.adh_district_type_cd := null; end if;
           if result.adh_city_nm is null        then result.adh_city_type_cd := null; end if;
           if result.adh_settlm_nm is null      then result.adh_settlm_type_cd := null; end if;
           if result.adh_street_name_tx is null then result.adh_street_type_tx := null; end if;

           pipe row(result);
    end loop;
  end if;
            
  return;
exception
  when others then
    null;
end;
/

grant EXECUTE on BUSINESS.rfv_trxn_party to DATA_READER;

grant EXECUTE on BUSINESS.rfv_trxn_party to KDD_ALGORITHM;

grant EXECUTE on BUSINESS.rfv_trxn_party to KDD_ANALYST;

grant EXECUTE on BUSINESS.rfv_trxn_party to KDD_MINER;

grant EXECUTE on BUSINESS.rfv_trxn_party to RF_RSCHEMA_ROLE;

grant EXECUTE on BUSINESS.rfv_trxn_party to CMREVMAN;

grant EXECUTE on BUSINESS.rfv_trxn_party to KDD_MNR with grant option;

grant EXECUTE on BUSINESS.rfv_trxn_party to KDD_REPORT with grant option;

grant EXECUTE on BUSINESS.rfv_trxn_party to KYC;

grant EXECUTE on BUSINESS.rfv_trxn_party to MANTAS with grant option;

grant EXECUTE on BUSINESS.rfv_trxn_party to STD;



