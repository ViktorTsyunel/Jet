/* RF_CBRF_BNKSEEK_DBF */
begin
  execute immediate 'drop table business.rf_cbrf_bnkseek_dbf';
  exception
    when others then null;
end;
/

create table business.rf_cbrf_bnkseek_dbf (
  vkey     varchar2(8 char),
  real     varchar2(4 char),
  pzn      varchar2(2 char),
  uer      varchar2(1 char),
  rgn      varchar2(2 char),
  ind      varchar2(6 char),
  tnp      varchar2(1 char),
  nnp      varchar2(25 char),
  adr      varchar2(30 char),
  rkc      varchar2(9 char),
  namep    varchar2(45 char),
  namen    varchar2(30 char),
  newnum   varchar2(9 char),
  newks    varchar2(9 char),
  permfo   varchar2(6 char),
  srok     varchar2(2 char),
  at1      varchar2(7 char),
  at2      varchar2(7 char),
  telef    varchar2(25 char),
  regn     varchar2(9 char),
  okpo     varchar2(8 char),
  dt_izm   varchar2(8 char),
  cks      varchar2(6 char),
  ksnp     varchar2(20 char),
  date_in  varchar2(8 char),
  date_ch  varchar2(8 char),
  vkeydel  varchar2(8 char),
  dt_izmr  varchar2(8 char)
) organization external (
  type oracle_loader
  default directory aml_imp_cbrf_bnk_dir
  access parameters (
    records fixed 312
    characterset ru8pc866
    badfile aml_imp_cbrf_bnk_dir:'bad_bnkseek.log'
    logfile aml_imp_cbrf_bnk_dir:'log_bnkseek.log'
    preprocessor aml_sh_dir:'dbfFix.sh'
    fields rtrim (
      vkey     char(8),
      real     char(4),
      pzn      char(2),
      uer      char(1),
      rgn      char(2),
      ind      char(6),
      tnp      char(1),
      nnp      char(25),
      adr      char(30),
      rkc      char(9),
      namep    char(45),
      namen    char(30),
      newnum   char(9),
      newks    char(9),
      permfo   char(6),
      srok     char(2),
      at1      char(7), 
      at2      char(7),
      telef    char(25),
      regn     char(9),
      okpo     char(8),
      dt_izm   char(8),
      cks      char(6),
      ksnp     char(20),
      date_in  char(8),
      date_ch  char(8),
      vkeydel  char(8),
      dt_izmr  char(8)
    )
  )
  location (aml_imp_cbrf_bnk_dir:'bnkseek.dbf')
);

grant select on business.rf_cbrf_bnkseek_dbf to mantas;
/* ========================================================================== */


/* RF_CBRF_BNKDEL_DBF */
begin
  execute immediate 'drop table business.rf_cbrf_bnkdel_dbf';
  exception
    when others then null;
end;
/

create table business.rf_cbrf_bnkdel_dbf (
  vkey    varchar2(8 char),
  vkeydel varchar2(8 char),
  pzn     varchar2(2 char),
  uer     varchar2(1 char),
  rgn     varchar2(2 char),
  ind     varchar2(6 char),
  tnp     varchar2(1 char),
  nnp     varchar2(25 char),
  adr     varchar2(30 char),
  rkc     varchar2(9 char),
  namep   varchar2(45 char),
  namen   varchar2(30 char),
  newnum  varchar2(9 char),
  newks   varchar2(9 char),
  permfo  varchar2(6 char),
  srok    varchar2(2 char),
  at1     varchar2(7 char),
  at2     varchar2(7 char),
  telef   varchar2(25 char),
  regn    varchar2(9 char),
  okpo    varchar2(8 char),
  datedel varchar2(8 char),
  dt_izm  varchar2(8 char),
  cks     varchar2(6 char),
  ksnp    varchar2(20 char),
  r_close varchar2(2 char),
  date_in varchar2(8 char)
) organization external (
  type oracle_loader
  default directory aml_imp_cbrf_bnk_dir
  access parameters (
    records fixed 302
    characterset ru8pc866
    badfile aml_imp_cbrf_bnk_dir:'bad_bnkdel.log'
    logfile aml_imp_cbrf_bnk_dir:'log_bnkdel.log'
    preprocessor aml_sh_dir:'dbfFix.sh'
    fields rtrim (
      vkey    char(8),
      vkeydel char(8),
      pzn     char(2),
      uer     char(1),
      rgn     char(2),
      ind     char(6),
      tnp     char(1),
      nnp     char(25),
      adr     char(30),
      rkc     char(9),
      namep   char(45),
      namen   char(30),
      newnum  char(9),
      newks   char(9),
      permfo  char(6),
      srok    char(2),
      at1     char(7),
      at2     char(7),
      telef   char(25),
      regn    char(9),
      okpo    char(8),
      datedel char(8),
      dt_izm  char(8),
      cks     char(6),
      ksnp    char(20),
      r_close char(2),
      date_in char(8)
    )
  )
  location (aml_imp_cbrf_bnk_dir:'bnkdel.dbf')
);

grant select on business.rf_cbrf_bnkdel_dbf to mantas;
/* ========================================================================== */


/* RF_CBRF_BNKSEEK_DBF_2 */
begin
  execute immediate 'drop table business.rf_cbrf_bnkseek_dbf_2';
  exception
    when others then null;
end;
/

create table business.rf_cbrf_bnkseek_dbf_2 (
  vkey varchar2(8 char), 
  real varchar2(4 char), 
  pzn varchar2(2 char), 
  uer varchar2(1 char), 
  rgn varchar2(2 char), 
  ind varchar2(6 char), 
  tnp varchar2(1 char), 
  nnp varchar2(25 char), 
  adr varchar2(30 char), 
  rkc varchar2(9 char), 
  namep varchar2(45 char), 
  namen varchar2(30 char), 
  newnum varchar2(9 char), 
  newks varchar2(9 char), 
  permfo varchar2(6 char), 
  srok varchar2(2 char), 
  at1 varchar2(7 char), 
  at2 varchar2(7 char), 
  telef varchar2(25 char), 
  regn varchar2(9 char), 
  okpo varchar2(8 char), 
  dt_izm varchar2(8 char), 
  cks varchar2(6 char), 
  ksnp varchar2(20 char), 
  date_in varchar2(8 char), 
  vkeydel varchar2(8 char), 
  date_ch varchar2(8 char)
) 
organization external (
  type oracle_loader
  default directory aml_imp_cbrf_bnk_dir
  access parameters (
    records fixed 304
    characterset ru8pc866
    badfile aml_imp_cbrf_bnk_dir:'bad_bnkseek_2.log'
    logfile aml_imp_cbrf_bnk_dir:'log_bnkseek_2.log'
    preprocessor aml_sh_dir:'dbfFix.sh'
    fields rtrim (
      vkey char(8),
      real char(4),
      pzn char(2),
      uer char(1),
      rgn char(2),
      ind char(6),
      tnp char(1),
      nnp char(25),
      adr char(30),
      rkc char(9),
      namep char(45),
      namen char(30),
      newnum char(9),
      newks char(9),
      permfo char(6),
      srok char(2),
      at1 char(7),
      at2 char(7),
      telef char(25),
      regn char(9),
      okpo char(8),
      dt_izm char(8),
      cks char(6),
      ksnp char(20),
      date_in char(8),
      vkeydel char(8),
      date_ch char(8)
    )
  )
  location(aml_imp_cbrf_bnk_dir:'bnkseek_2.dbf' )
);

grant select on business.rf_cbrf_bnkseek_dbf_2 to mantas;
/* ========================================================================== */

begin
  execute immediate 'drop table business.rf_cbrf_bnkdel_dbf_2';
  exception
    when others then null;
end;
/

create table business.rf_cbrf_bnkdel_dbf_2 (
  vkey    varchar2(8 char),
  vkeydel varchar2(8 char),
  pzn     varchar2(2 char),
  uer     varchar2(1 char),
  rgn     varchar2(2 char),
  ind     varchar2(6 char),
  tnp     varchar2(1 char),
  nnp     varchar2(25 char),
  adr     varchar2(30 char),
  rkc     varchar2(9 char),
  namep   varchar2(45 char),
  namen   varchar2(30 char),
  newnum  varchar2(9 char),
  newks   varchar2(9 char),
  srok    varchar2(2 char),
  permfo  varchar2(6 char),
  at1     varchar2(7 char),
  at2     varchar2(7 char),
  telef   varchar2(25 char),
  regn    varchar2(9 char),
  okpo    varchar2(8 char),
  datedel varchar2(8 char),
  dt_izm  varchar2(8 char),
  cks     varchar2(6 char),
  ksnp    varchar2(20 char),
  r_close varchar2(2 char),
  date_in varchar2(8 char)
) organization external (
  type oracle_loader
  default directory aml_imp_cbrf_bnk_dir
  access parameters (
    records fixed 302
    characterset ru8pc866
    badfile aml_imp_cbrf_bnk_dir:'bad_bnkdel_2.log'
    logfile aml_imp_cbrf_bnk_dir:'log_bnkdel_2.log'
    preprocessor aml_sh_dir:'dbfFix.sh'
    fields rtrim (
      vkey    char(8),
      vkeydel char(8),
      pzn     char(2),
      uer     char(1),
      rgn     char(2),
      ind     char(6),
      tnp     char(1),
      nnp     char(25),
      adr     char(30),
      rkc     char(9),
      namep   char(45),
      namen   char(30),
      newnum  char(9),
      newks   char(9),
      srok    char(2),
      permfo  char(6),
      at1     char(7),
      at2     char(7),
      telef   char(25),
      regn    char(9),
      okpo    char(8),
      datedel char(8),
      dt_izm  char(8),
      cks     char(6),
      ksnp    char(20),
      r_close char(2),
      date_in char(8)
    )
  )
  location (aml_imp_cbrf_bnk_dir:'bnkdel_2.dbf')
);

grant select on business.rf_cbrf_bnkdel_dbf_2 to mantas;
/* ========================================================================== */
