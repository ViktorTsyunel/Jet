begin 
  execute immediate 'drop table business.rf_cust_change_regdt#wrk';
exception when others then null; end;
/

begin 
  execute immediate 
'create global temporary table business.rf_cust_change_regdt#wrk
 (
   row_id       ROWID,
   data_dump_dt DATE
 ) 
 on commit preserve rows';
exception when others then null; end;
/

begin 
  execute immediate 'drop table business.rf_parallel_execute#wrk';
exception when others then null; end;
/

create table business.rf_parallel_execute#wrk
(
  thread_name  VARCHAR2(255 CHAR),
  val1         NUMBER,
  val2         NUMBER,
  val3         NUMBER
);

GRANT SELECT ON std.recode_acct TO business;
GRANT SELECT, INSERT, DELETE, UPDATE ON std.locks TO business;
GRANT SELECT ON reks000ibs.z#ac_fin TO business;

GRANT EXECUTE ON mantas.rf_pkg_parallel TO BUSINESS;
GRANT EXECUTE ON mantas.rf_pkg_rule TO BUSINESS;
GRANT ALL ON business.rf_cust_change_regdt#wrk to mantas;
GRANT ALL ON business.rf_cust_change_regdt_log to mantas;
GRANT ALL ON business.rf_cust_changes to mantas;
GRANT SELECT ON business.rf_cust_changes to mantas with grant option;
GRANT ALL ON business.rf_cust_changes_seq to mantas;
GRANT ALL ON business.rf_parallel_execute#wrk to mantas;

create or replace package business.rf_pkg_postload IS
--
-- �������� ���������� � ������� ������� � ���
--
pck_last_days_qty   INTEGER := 32; -- ���������� ������������� ���� �� ������� ���� � �������, �� ��������� - 32 (����� � ���� ����)
FUNCTION get_last_days_qty RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE;
PROCEDURE set_last_days_qty(par_last_days_qty  INTEGER);
--
-- ��������� ����������� ��� ��������� ��������� ��������� ������ - ���������� ����� �������� �����/���������� ������, ����� ����������
--
FUNCTION scnro_preprocess RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
--
-- ���������� � �������� ���� ������ �������� �� ����� 
-- ��������! ��������� ��������� COMMIT!
--
FUNCTION calc_acct_first_actvy_dt RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
--
-- ������� ������� ���������� �������� �����������:
-- ���������� ������ �� ���������� ������ � ���������� �������� ��� ������� ������������/����������� � ���������,
-- ���� ��� ����� �����������/���������� �������� ����� �� ����� �� �� � ����� ����� �����������/���������� ���������� � ����������� ������
-- (�. �. ��� ��������� ��� ���������� ������, ������� ��� �������� ��� �������)
-- ��������! ��������� ��������� COMMIT!
--
FUNCTION set_ext_cust_as_int RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
--
-- �������� �������� � ���������� ��������� � ����� � ���������� ��� ����������� �������� "������ ������"
-- (� ������������ � ��������� ��������� 4005 ���� �������� �������� ��� �������� �� ������ ����� 600000 � �������
--  3 ������� � ���� �����������/��������������� ��)
--
FUNCTION mark_repeat_cust_regdt RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
--
-- ���������� � �������� ���� ������� ���������� �� ��������� ���� ��� � ����� ���������� ����� ���,
-- � ������ ���������/���������/������������ ���� ���� ��� ����� �������� ��������������� �������� � �������� �������� 
-- (�������� �� ���������������� ����� �� ������ ��� ����� ��������� ����)
-- ��������! ��������� ��������� COMMIT!
--
FUNCTION calc_acct_goz_first_dt RETURN INTEGER; -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
end rf_pkg_postload;
/
create or replace package body business.rf_pkg_postload IS
--
-- ������� ������� � �������� ����������
--
FUNCTION get_last_days_qty RETURN INTEGER DETERMINISTIC PARALLEL_ENABLE IS BEGIN return pck_last_days_qty; END;
PROCEDURE set_last_days_qty(par_last_days_qty  INTEGER) IS BEGIN pck_last_days_qty := par_last_days_qty; END; 
--
-- ��������� ����������� ��������� ����� �������� �����/���������� ������, ����� ����������
--
FUNCTION scnro_preprocess RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
  var_err_exists  INTEGER := 0;
  var_err_exists2 INTEGER := 0;
  var_err_exists3 INTEGER := 0;
  var_err_exists4 INTEGER := 0;
BEGIN
  --
  -- �������� ������� ���������� ����������� �������� � ������������ ��������/������ �� ������ ����� (��� ������ �� ��)
  --
  std.pkg_log.info('����������� ������� ���������� �������� � ������������ ��������/������ �� ������ ����� (��� ������ �� ��)', 'business.rf_pkg_postload.scnro_preprocess');
  var_err_exists := set_ext_cust_as_int;
  std.pkg_log.info('��������� ����������� ������� ���������� �������� � ������������ ��������/������', 'business.rf_pkg_postload.scnro_preprocess');
  COMMIT;
  --
  -- ������� ���� ������ �������� �� ������ (����� ��� ��������� ���� ���� 4006 - ������ �������� �� ����� ��)
  --
  std.pkg_log.info('��������� ���� ������ �������� �� ������...', 'business.rf_pkg_postload.scnro_preprocess');
  var_err_exists2 := calc_acct_first_actvy_dt;
  std.pkg_log.info('��������� ��������� ���� ������ �������� �� ������', 'business.rf_pkg_postload.scnro_preprocess');
  COMMIT;
  --
  -- �������� �������� � ���������� ���������, ���� � ���������� ���������� ���� ����������� (����� ��� ���� 4005 - ������ 3 ������ � ���� �����������)
  --
  std.pkg_log.info('�������� �������� � ���������� ��������� � ����� � ���������� ��� ����������� ��������...', 'business.rf_pkg_postload.scnro_preprocess');
  var_err_exists3 := mark_repeat_cust_regdt;
  std.pkg_log.info('��������� �������� �������� � ���������� ��������� � ����� � ���������� ��� ����������� ��������', 'business.rf_pkg_postload.scnro_preprocess');  
  --
  -- ������� ���� ������� ���������� �� ��������� ����� ��� � ���� ��������� ������ ��� (����� ��� ��������� ����� ���� 4102, 4103 - ������, ����������� ���������� ...)
  --
  std.pkg_log.info('��������� ���� ������� ���������� �� ��������� ����� ��� � ���� ��������� ������ ���...', 'business.rf_pkg_postload.scnro_preprocess');
  var_err_exists4 := calc_acct_goz_first_dt;
  std.pkg_log.info('��������� ��������� ���� ������� ���������� �� ��������� ����� ��� � ���� ��������� ������ ���', 'business.rf_pkg_postload.scnro_preprocess');  
  --
  -- ������������� ������ ��������: ������ � ���������
  -- �������������� ��� ����������� - ������ "����������" ����� ���������� ����� ���������, ������ � ��������� ������ �� �������� ��������
  --
/*
  UPDATE --+ PARALLEL(t 32)
         business.wire_trxn t
     SET rf_opok_status = 1
   WHERE rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and --������ �������� ��������
         rf_opok_status = 0;
  COMMIT;

  UPDATE --+ PARALLEL(t 32) 
         business.cash_trxn t
     SET rf_opok_status = 1
   WHERE rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and --������ �������� ��������
         rf_opok_status = 0;
  COMMIT;

  UPDATE --+ PARALLEL(t 32) 
         business.back_office_trxn t
     SET rf_opok_status = 1
   WHERE rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and --������ �������� ��������
         rf_opok_status = 0;
  COMMIT;
*/  
  if var_err_exists <> 0 or var_err_exists2 <> 0 or var_err_exists3 <> 0 or var_err_exists4 <> 0 Then
    return 1;
  else
    return 0;
  end if;    
END scnro_preprocess;
--
-- ���������� � �������� ���� ������ �������� �� ����� 
-- ��������! ��������� ��������� COMMIT!
--
FUNCTION calc_acct_first_actvy_dt RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������

 var_sql  CLOB := 
q'{DECLARE
  var_last_days_qty   INTEGER := 32;  -- ���������� ���� �� ������� ���� � �������, � ������� ������� ����� ���������� ����� ����������� �������� (32 = ����� � ���� ����)
BEGIN
  --
  -- ��������� ���� ������ �������� ��� ������, � ������� ��� ���� ������� �� ������� ���� � ������� �� �����, ��� �� var_last_days_qty ����
  --
  MERGE /*+ INDEX(t RF_ACCT_SEQ_AK1)*/ INTO business.acct t USING 
   (SELECT acct_seq_id, rf_first_actvy_dt, min(trxn_exctn_dt) as trxn_exctn_dt
      FROM (select /*+ ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(wt) INDEX(wt RF_WIRE_TRXN_ORIG_ACCT_FK_I)*/
                   wt.rf_orig_acct_seq_id as acct_seq_id, wt.trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct #PART_SUBPART# acct,
                   business.wire_trxn wt
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   wt.rf_orig_acct_seq_id = acct.acct_seq_id and 
                   nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                   wt.src_sys_cd = acct.src_sys_cd and -- ��������� ������ �������� �� ��� �� �������-���������, ��� � ��� ���� (��������� �������� "�������" ���������� ���� ������ ��������)
                   wt.rf_debit_cd = acct.alt_acct_id   -- ���� ����������� (����������) ��������� � ������� (��������) �������� (������ �����, ��������, ������� ������� �� ���� "�� ���������" � ������ ����� ���� ��������� - ��� ��� �������� �� ������ ��� � ���������� ������ ����������)
            union all
            select /*+ ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(wt) INDEX(wt RF_WIRE_TRXN_BENEF_ACCT_FK_I)*/
                   wt.rf_benef_acct_seq_id as acct_seq_id, wt.trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct #PART_SUBPART# acct,
                   business.wire_trxn wt
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   wt.rf_benef_acct_seq_id = acct.acct_seq_id and 
                   nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                   wt.src_sys_cd = acct.src_sys_cd and
                   wt.rf_credit_cd = acct.alt_acct_id
            union all
            select /*+ ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(ct) INDEX(ct RF_CASH_TRXN_ACCT_FK_I)*/ 
                   ct.rf_acct_seq_id as acct_seq_id, ct.trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct #PART_SUBPART# acct,
                   business.cash_trxn ct 
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   ct.rf_acct_seq_id = acct.acct_seq_id and 
                   nvl(ct.rf_canceled_fl, 'N') <> 'Y' and
                   ct.src_sys_cd = acct.src_sys_cd and
                   case when ct.dbt_cdt_cd = 'D' then ct.rf_debit_cd else ct.rf_credit_cd end = acct.alt_acct_id
            union all
            select /*+ ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(bot) INDEX(bot RF_BO_TRXN_ACCT_FK_I)*/ 
                   bot.rf_acct_seq_id as acct_seq_id, bot.exctn_dt as trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct #PART_SUBPART# acct,
                   business.back_office_trxn bot
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   bot.rf_acct_seq_id = acct.acct_seq_id and 
                   nvl(bot.rf_canceled_fl, 'N') <> 'Y' and
                   bot.src_sys_cd = acct.src_sys_cd and
                   case when bot.dbt_cdt_cd = 'D' then bot.rf_debit_cd else bot.rf_credit_cd end = acct.alt_acct_id)  
     GROUP BY acct_seq_id, rf_first_actvy_dt
    HAVING coalesce(rf_first_actvy_dt, to_date('31.12.9999', 'dd.mm.yyyy')) <> coalesce(min(trxn_exctn_dt), to_date('31.12.9999', 'dd.mm.yyyy'))) v
  ON (v.acct_seq_id = t.acct_seq_id)
    WHEN MATCHED THEN
      UPDATE
         SET t.rf_first_actvy_dt = v.trxn_exctn_dt;     

  COMMIT;         
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ���������� ���� ������ �������� ��� ������ (������� BUSINESS.ACCT �������� #PARTITION_NAME#)'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.calc_acct_first_actvy_dt'); 
    RAISE;
END;
}';

 var_migr_sql  CLOB := 
q'{BEGIN
  UPDATE /*+ INDEX(acct)*/ business.acct #PART_SUBPART# acct
     SET rf_first_actvy_dt = to_date('01.01.1900', 'dd.mm.yyyy')
   WHERE case when nvl(acct.src_sys_cd, '?') <> 'BCK' 
              then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) 
         end = to_date('31.12.9999', 'dd.mm.yyyy') and   -- ���� ������ �������� �� �������
         rf_src_cd like 'EKS|%' and                      -- ���� �� ���
         trunc(rf_branch_id/100000) not in (38, 99) and  -- ���� ��������� � ����������� ���������: ��� ��, ����� ����������� � ��
         EXISTS(select null
                  from reks000ibs.z#ac_fin a
                 where a.id = to_number(substr(acct.RF_SRC_CD, 5)) and
                       (a.C_DATE_LAST is not null or a.C_DATE_L_NT is not null)); -- � ������� ��� ����� ������� ���� ��������� �������� 

  COMMIT;         
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ����������� ���� ������ �������� ��� ������ ��� ��� �������� (������� BUSINESS.ACCT �������� #PARTITION_NAME#)'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.calc_acct_first_actvy_dt'); 
    RAISE;
END;
}';
 
  var_migr_flag       INTEGER;
  var_err_count       INTEGER := 0;  
  var_err_count2      INTEGER := 0;  
BEGIN
  --
  -- ��������� ����: "���� �������� ������ ���"
  --
  SELECT count(*)
    INTO var_migr_flag
    FROM std.locks
   WHERE lock_name = 'MIGRATION_EKS';
  --
  -- ���� ����������� �������� ������ ���, �� ���������� ���� ������ �������� ��� ������ � ��������� ����� ��������� �������� � �������
  -- 
  if var_migr_flag > 0 Then
    std.pkg_log.info('���������� ���� ������ �������� ��� ������ ��� � ��������� ����� ��������� �������� � ������� (��� ��������) ...', 'business.rf_pkg_postload.calc_acct_first_actvy_dt');

    var_err_count := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_migr_sql,  
                                                             par_degree         => 128, 
                                                             par_task_name      => 'aml_acct_first_dt_migr',
                                                             par_table_owner    => 'BUSINESS', 
                                                             par_table_name     => 'ACCT', 
                                                             par_chunk_type     => 'P',
                                                             par_log_level      => 'ERROR');  
    -- ������� ���� �������� ������ ���
    DELETE FROM std.locks WHERE lock_name = 'MIGRATION_EKS';
    COMMIT;

    std.pkg_log.info('��������� ���������� ���� ������ �������� ��� ������ ��� � ��������� ����� ��������� �������� � ������� (��� ��������)', 'business.rf_pkg_postload.calc_acct_first_actvy_dt');
  end if;
  --
  -- ������������ ���� ������ �������� ��� "��������" ������ (����� ��� �������� ��� � ������ ��������� �� ����� 32 ���� �� �������� ���)
  --  
  var_err_count2 := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_sql,  
                                                            par_degree         => 128, 
                                                            par_task_name      => 'aml_acct_first_dt',
                                                            par_table_owner    => 'BUSINESS', 
                                                            par_table_name     => 'ACCT', 
                                                            par_chunk_type     => 'P',
                                                            par_log_level      => 'ERROR');
  
  if var_err_count > 0 or var_err_count2 > 0 Then
    return 1; -- ���� ������
  else  
    return 0; -- ��� ������
  end if;  
EXCEPTION
  WHEN OTHERS THEN 
    std.pkg_log.error('������ ��� ���������� ���� ������ �������� ��� ������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.calc_acct_first_actvy_dt'); 
    return 1; -- ���� ������
END calc_acct_first_actvy_dt;
--
-- ���������� � �������� ���� ������� ���������� �� ��������� ���� ��� � ����� ���������� ����� ���,
-- � ������ ���������/���������/������������ ���� ���� ��� ����� �������� ��������������� �������� � �������� �������� 
-- (�������� �� ���������������� ����� �� ������ ��� ����� ��������� ����)
-- ��������! ��������� ��������� COMMIT!
--
FUNCTION calc_acct_goz_first_dt RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������

 var_sql  CLOB := 
q'{DECLARE
  var_last_days_qty   INTEGER := 32;  -- ���������� ���� �� ������� ���� � �������, � ������� ������� ����� ���������� ����� ����������� �������� (32 = ����� � ���� ����)
BEGIN
  --
  -- ���� �� ��������� ������ ���, � ������� ������� ���� ������� ���������� � ���������� ����� ��� ������� �� ������� ���� � ������� �� �����, ��� �� var_last_days_qty ����
  -- ��� ������, � ������� ��� ���� ���������/����������/������� ��������� ���� ������ �������� ��� � ����������� 
  -- ��������������� �������� � �������� �������� ��� ���������� ���������
  --
  FOR r IN (SELECT acct_seq_id, 
                   rf_goz_first_dt as old_goz_first_dt, 
                   min(trxn_exctn_dt) as new_goz_first_dt
              FROM (select /*+ ORDERED INDEX(acct RF_ACCT_GOZ_FIRST_DT_I) USE_NL(wt) INDEX(wt RF_WIRE_TRXN_BENEF_ACCT_FK_I)*/
                           wt.rf_benef_acct_seq_id as acct_seq_id, wt.trxn_exctn_dt, acct.rf_goz_first_dt
                      from business.acct #PART_SUBPART# acct
                           left join business.wire_trxn wt on wt.rf_benef_acct_seq_id = acct.acct_seq_id and -- ���������� �� ���� ����
                                                              mantas.rf_pkg_rule.is_goz_acct(substr(wt.rf_orig_acct_nb, 1, 5),
                                                                                             case when wt.rf_orig_acct_seq_id <> -1
                                                                                                  then 'SBRF'
                                                                                                  else wt.send_instn_id
                                                                                             end) = 1 and -- � ����� ���������� ����� ��� � �� ��
                                                              wt.rf_orig_acct_nb <> nvl(wt.rf_benef_acct_nb, '#%?') and                                
                                                              nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                                                              wt.src_sys_cd = acct.src_sys_cd and -- ��������� ������ �������� �� ��� �� �������-���������, ��� � ��� ���� (��������� �������� "�������" ���������� ���� ������ ��������)
                                                              wt.rf_credit_cd = acct.alt_acct_id -- ���� ���������� ��������� � �������� �������� (������ �����, ��������, ������� ������� �� ���� "�� ���������" � ������ ����� ���� ��������� - ��� ��� �������� �� ������ ��� � ���������� ������ ����������) 
                     where acct.rf_goz_first_dt_i >= trunc(sysdate) - var_last_days_qty) -- ��������� ����� ��� � ������ ��� �������� ������ ����� ���������� � ���������� ����� ���
            GROUP BY acct_seq_id, rf_goz_first_dt
            HAVING coalesce(rf_goz_first_dt, to_date('31.12.9999', 'dd.mm.yyyy')) <> coalesce(min(trxn_exctn_dt), to_date('31.12.9999', 'dd.mm.yyyy'))) LOOP
    --
    -- �������� ���� ������� ���������� ��� �� �����
    --
    UPDATE business.acct
       SET rf_goz_first_dt = r.new_goz_first_dt
     WHERE acct_seq_id = r.acct_seq_id;     
    --
    -- ����������� � �������� �������� ���������� �� ���� ���� �� ������ � � ����� ������ ������ �������� ���
    -- �. �. �� �������� ������ ����������, �� ������������ ������ �� ����� ���������� - ���������� �� ������ ����� ��� ����������� ������ ��������
    -- 
    UPDATE /*+ INDEX(wt RF_WIRE_TRXN_BENEF_ACCT_FK_I)*/
           business.wire_trxn wt
       SET rf_active_partition_flag = 1,
           rf_opok_status = 0,
           data_dump_dt = SYSDATE
     WHERE rf_benef_acct_seq_id = r.acct_seq_id and
           (trxn_exctn_dt+0 = r.old_goz_first_dt or trxn_exctn_dt+0 = r.new_goz_first_dt) and
           (rf_active_partition_flag <> 1 or rf_opok_status <> 0);
  END LOOP;
  COMMIT;         
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ���������� ���� ������� ���������� �� ��������� ���� ��� � ����� ���������� ����� ��� (������� BUSINESS.ACCT �������� #PARTITION_NAME#)'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.calc_acct_goz_first_dt'); 
    RAISE;
END;
}';
  var_err_count       INTEGER := 0;  
BEGIN
  --
  -- ������������ ���� ������ �������� ��� "��������" ������ (����� ��� �������� ��� � ������ ��������� �� ����� 32 ���� �� �������� ���)
  --  
  var_err_count := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_sql,  
                                                           par_degree         => 128, 
                                                           par_task_name      => 'aml_acct_goz_first_dt',
                                                           par_table_owner    => 'BUSINESS', 
                                                           par_table_name     => 'ACCT', 
                                                           par_chunk_type     => 'P',
                                                           par_log_level      => 'ERROR');
  
  if var_err_count > 0 Then
    return 1; -- ���� ������
  else  
    return 0; -- ��� ������
  end if;  
EXCEPTION
  WHEN OTHERS THEN 
    std.pkg_log.error('������ ��� ���������� ���� ������� ���������� �� ��������� ���� ��� � ����� ���������� ����� ���. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.calc_acct_goz_first_dt'); 
    return 1; -- ���� ������
END calc_acct_goz_first_dt;

-- ������� �������
FUNCTION OLD_calc_acct_first_actvy_dt RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
 var_last_days_qty   INTEGER := 32;  -- ���������� ���� �� ������� ���� � �������, � ������� ������� ����� ���������� ����� ����������� �������� (32 = ����� � ���� ����)
 var_open_days_qty   INTEGER := 18*30; -- ���������� ���� �������� ������ �������� �� ���� �������� ����� (18 �������)
BEGIN
  EXECUTE IMMEDIATE 'alter session force parallel dml parallel 64';
  EXECUTE IMMEDIATE 'alter session force parallel query parallel 64';
  --
  -- ������������� ���� ������ �������� � ��������� ���� ��� �������� ������ � ������, �������� �����, ��� var_open_days_qty ���� �����
  --    
  --UPDATE /*+ PARALLEL(64)*/ business.acct acct
  --   SET rf_first_actvy_dt = to_date('01.01.1900', 'dd.mm.yyyy')
  -- WHERE case when nvl(acct.src_sys_cd, '?') <> 'BCK'
  --            then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
  --       end = to_date('31.12.9999', 'dd.mm.yyyy') and
  --       (acct_stat_cd = 'C' or acct_close_dt is not null or acct_open_dt < trunc(sysdate) - var_open_days_qty);
  --
  -- ��������� ��������� ����� ������������� ��������� ������� ������
  --
  --COMMIT;         
  --
  -- ��������� ���� ������ �������� ��� ������, � ������� ��� ���� ������� �� ������� ���� � ������� �� �����, ��� �� var_last_days_qty ����
  --
  MERGE /*+ INDEX(t RF_ACCT_SEQ_AK1)*/ INTO business.acct t USING 
   (SELECT acct_seq_id, rf_first_actvy_dt, min(trxn_exctn_dt) as trxn_exctn_dt
      FROM (select /*+ PARALLEL(64) ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(wt) INDEX(wt RF_WIRE_TRXN_ORIG_ACCT_FK_I)*/
                   wt.rf_orig_acct_seq_id as acct_seq_id, wt.trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct acct,
                   business.wire_trxn wt
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   wt.rf_orig_acct_seq_id = acct.acct_seq_id and 
                   nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                   wt.src_sys_cd = acct.src_sys_cd and -- ��������� ������ �������� �� ��� �� �������-���������, ��� � ��� ���� (��������� �������� "�������" ���������� ���� ������ ��������)
                   wt.rf_debit_cd = acct.alt_acct_id   -- ���� ����������� (����������) ��������� � ������� (��������) �������� (������ �����, ��������, ������� ������� �� ���� "�� ���������" � ������ ����� ���� ��������� - ��� ��� �������� �� ������ ��� � ���������� ������ ����������)
            union all
            select /*+ PARALLEL(64) ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(wt) INDEX(wt RF_WIRE_TRXN_BENEF_ACCT_FK_I)*/
                   wt.rf_benef_acct_seq_id as acct_seq_id, wt.trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct acct,
                   business.wire_trxn wt
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   wt.rf_benef_acct_seq_id = acct.acct_seq_id and 
                   nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                   wt.src_sys_cd = acct.src_sys_cd and
                   wt.rf_credit_cd = acct.alt_acct_id
            union all
            select /*+ PARALLEL(64) ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(ct) INDEX(ct RF_CASH_TRXN_ACCT_FK_I)*/ 
                   ct.rf_acct_seq_id as acct_seq_id, ct.trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct acct,
                   business.cash_trxn ct 
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   ct.rf_acct_seq_id = acct.acct_seq_id and 
                   nvl(ct.rf_canceled_fl, 'N') <> 'Y' and
                   ct.src_sys_cd = acct.src_sys_cd and
                   case when ct.dbt_cdt_cd = 'D' then ct.rf_debit_cd else ct.rf_credit_cd end = acct.alt_acct_id
            union all
            select /*+ PARALLEL(64) ORDERED INDEX(acct RF_ACCT_FIRST_ACTVY_I) USE_NL(bot) INDEX(bot RF_BO_TRXN_ACCT_FK_I)*/ 
                   bot.rf_acct_seq_id as acct_seq_id, bot.exctn_dt as trxn_exctn_dt, acct.rf_first_actvy_dt
              from business.acct acct,
                   business.back_office_trxn bot
             where case when nvl(acct.src_sys_cd, '?') <> 'BCK'
                        then coalesce(acct.rf_first_actvy_dt, acct.acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))
                   end >= trunc(sysdate) - var_last_days_qty and
                   bot.rf_acct_seq_id = acct.acct_seq_id and 
                   nvl(bot.rf_canceled_fl, 'N') <> 'Y' and
                   bot.src_sys_cd = acct.src_sys_cd and
                   case when bot.dbt_cdt_cd = 'D' then bot.rf_debit_cd else bot.rf_credit_cd end = acct.alt_acct_id)  
     GROUP BY acct_seq_id, rf_first_actvy_dt
    HAVING coalesce(rf_first_actvy_dt, to_date('31.12.9999', 'dd.mm.yyyy')) <> coalesce(min(trxn_exctn_dt), to_date('31.12.9999', 'dd.mm.yyyy'))) v
  ON (v.acct_seq_id = t.acct_seq_id)
    WHEN MATCHED THEN
      UPDATE
         SET t.rf_first_actvy_dt = v.trxn_exctn_dt;     
  --
  -- ��������� ��������� ����� ������������� ��������� ������� ������
  --
  COMMIT;         
  EXECUTE IMMEDIATE 'alter session enable parallel dml';
  EXECUTE IMMEDIATE 'alter session enable parallel query';
  
  return 0; -- ��� ������
EXCEPTION
  WHEN OTHERS THEN 
    std.pkg_log.error('������ ��� ���������� ���� ������ �������� ��� ������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.calc_acct_first_actvy_dt'); 
    BEGIN
      EXECUTE IMMEDIATE 'alter session enable parallel dml';
      EXECUTE IMMEDIATE 'alter session enable parallel query';
    EXCEPTION
      WHEN OTHERS THEN
        null;
    END;      
    return 1; -- ���� ������
END OLD_calc_acct_first_actvy_dt;
--
-- ������� ������� ���������� �������� �����������:
-- ���������� ������ �� ���������� ������ � ���������� �������� ��� ������� ������������/����������� � ���������,
-- ���� ��� ����� �����������/���������� �������� ����� �� ����� �� �� � ����� ����� �����������/���������� ���������� � ����������� ������
-- (�. �. ��� ��������� ��� ���������� ������, ������� ��� �������� ��� �������)
-- ��������! ��������� ��������� COMMIT!
--
FUNCTION set_ext_cust_as_int RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
 var_sql  CLOB := 
q'{BEGIN
  --
  -- ����������� � ����������� ���������
  --  
  MERGE INTO business.wire_trxn t USING 
   (WITH sb_bic as
    (select distinct cstm_2_tx as bic
       from business.org
      where org_type_cd = '��'
        and cstm_2_tx is not null 
     union
     select distinct cstm_4_tx as bic
       from business.org
      where org_type_cd = '��'
        and cstm_4_tx is not null) 
    SELECT /*+ LEADING(wt) USE_NL(ac)*/ 
           ac.acct_seq_id as rf_orig_acct_seq_id, 
           ac.rf_prmry_cust_seq_id as rf_orig_cust_seq_id, 
           wt.fo_trxn_seq_id
      FROM sb_bic
           JOIN business.wire_trxn #PART_SUBPART# wt ON wt.send_instn_id = sb_bic.bic
           JOIN business.acct ac ON ac.rowid = business.rf_pkg_util.get_acct(wt.rf_orig_acct_nb, trunc(wt.trxn_exctn_dt), wt.src_sys_cd)
     WHERE --wt.rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and /*������ �������� ��������*/
           wt.rf_active_partition_flag = 1 and
           wt.rf_opok_status = 0 and /*������ ��� �� ������������ ������*/
           wt.rf_orig_cust_seq_id is null and
           wt.rf_orig_acct_seq_id is null and
           wt.rf_orig_acct_nb not like '3%' and /*�������� ����. �����, ����-������ � �. �. - �� �������� �� �������� ������������/�����������*/     
           ac.acct_seq_id <> -1 and
           ac.rf_prmry_cust_seq_id <> -1) v
  ON (v.fo_trxn_seq_id = t.fo_trxn_seq_id)
    WHEN MATCHED THEN
      UPDATE
         SET t.rf_orig_acct_seq_id  = v.rf_orig_acct_seq_id,
             t.rf_orig_cust_seq_id  = v.rf_orig_cust_seq_id,
             t.orig_acct_id         = to_char(v.rf_orig_acct_seq_id),
             t.orig_acct_id_type_cd = 'IA';       
  COMMIT;
  --
  -- ���������� � ����������� ���������
  --  
  MERGE INTO business.wire_trxn t USING 
   (WITH sb_bic as
    (select distinct cstm_2_tx as bic
       from business.org
      where org_type_cd = '��'
        and cstm_2_tx is not null 
     union
     select distinct cstm_4_tx as bic
       from business.org
      where org_type_cd = '��'
        and cstm_4_tx is not null) 
    SELECT /*+ LEADING(wt) USE_NL(ac)*/ 
           ac.acct_seq_id as rf_benef_acct_seq_id, 
           ac.rf_prmry_cust_seq_id as rf_benef_cust_seq_id, 
           wt.fo_trxn_seq_id
      FROM sb_bic
           JOIN business.wire_trxn #PART_SUBPART# wt ON wt.rcv_instn_id = sb_bic.bic
           JOIN business.acct ac ON ac.rowid = business.rf_pkg_util.get_acct(wt.rf_benef_acct_nb, trunc(wt.trxn_exctn_dt), wt.src_sys_cd)
     WHERE --wt.rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and /*������ �������� ��������*/
           wt.rf_active_partition_flag = 1 and
           wt.rf_opok_status = 0 and /*������ ��� �� ������������ ������*/
           wt.rf_benef_cust_seq_id is null and
           wt.rf_benef_acct_seq_id is null and
           wt.rf_benef_acct_nb not like '3%' and /*�������� ����. �����, ����-������ � �. �. - �� �������� �� �������� ������������/�����������*/     
           ac.acct_seq_id <> -1 and
           ac.rf_prmry_cust_seq_id <> -1) v
  ON (v.fo_trxn_seq_id = t.fo_trxn_seq_id)
    WHEN MATCHED THEN
      UPDATE
         SET t.rf_benef_acct_seq_id  = v.rf_benef_acct_seq_id,
             t.rf_benef_cust_seq_id  = v.rf_benef_cust_seq_id,
             t.benef_acct_id         = to_char(v.rf_benef_acct_seq_id),
             t.benef_acct_id_type_cd = 'IA';       
  COMMIT;         
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ������������ ������� ���������� �������� � ������������ ��������/������ (������� BUSINESS.WIRE_TRXN ����������� #SUBPARTITION_NAME#)'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.set_ext_cust_as_int'); 
    RAISE;
END;  
}';
 
  var_err_count     INTEGER;  
BEGIN
  var_err_count := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_sql,  
                                                           par_degree         => 128, 
                                                           par_task_name      => 'aml_set_ext_cust',
                                                           par_table_owner    => 'BUSINESS', 
                                                           par_table_name     => 'WIRE_TRXN', 
                                                           par_partition_name => 'P_ACTIVE',
                                                           par_chunk_type     => 'SP',
                                                           par_log_level      => 'ERROR');
  
  if var_err_count > 0 Then
    return 1; -- ���� ������
  else  
    return 0; -- ��� ������
  end if;  
EXCEPTION
  WHEN OTHERS THEN 
    std.pkg_log.error('������ ��� ������������ ������� ���������� �������� � ������������ ��������/������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.set_ext_cust_as_int'); 
    return 1; -- ���� ������
END set_ext_cust_as_int;
--
-- �������� �������� � ���������� ��������� � ����� � ���������� ��� ����������� �������� "������ ������"
-- (� ������������ � ��������� ��������� 4005 ���� �������� �������� ��� �������� �� ������ ����� 600000 � �������
--  3 ������� � ���� �����������/��������������� ��)
--
FUNCTION mark_repeat_cust_regdt RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
  var_sql    CLOB :=
q'{DECLARE
  var_min_limit     NUMBER := 600000; -- ����� �� ���������
  var_months_qty    NUMBER := 3; -- ���������� ������� �� ����� ���� ����������� 
                                 -- (� ���������� ��������� ���������� �������� � ������� �� ����� ���� ����������� �� ����� ���� ����������� + ��� ���������� �������)
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
  var_rows_qty  INTEGER := 0;
BEGIN
  --
  -- �������������� ��������� ��� ����������� �������� � ������� ��������� ��������� �������
  -- ���������� ������������ ������� � ��� �� ��������/����� ��������, ���� � ���� ������� ��� �� ���������������� ��������� ���� �����������
  --
  MERGE INTO business.rf_cust_changes t USING
  (SELECT /*+ LEADING(l) USE_NL(c)*/
          l.cust_seq_id, l.data_dump_dt, l.old_reg_dt, l.new_reg_dt,
          c.record_seq_id
     FROM (select l.*,
                  row_number() over(partition by cust_seq_id, data_dump_dt order by record_seq_id) as rn
             from business.rf_cust_change_regdt_log #PART_SUBPART# l) l
          left join (select /*+ PUSH_PRED(c)*/ c.*,
                            row_number() over(partition by cust_seq_id, data_dump_dt order by record_seq_id) as rn
                       from business.rf_cust_changes c
                      where c.old_reg_dt is null and
                            c.new_reg_dt is null) c on c.cust_seq_id = l.cust_seq_id and
                                                       c.data_dump_dt = l.data_dump_dt and
                                                       c.rn = l.rn) v
  ON (t.record_seq_id = v.record_seq_id)                                                  
  WHEN MATCHED THEN
    UPDATE
       SET t.old_reg_dt = v.old_reg_dt,
           t.new_reg_dt = v.new_reg_dt
  WHEN NOT MATCHED THEN
    INSERT(record_seq_id, cust_seq_id, data_dump_dt, old_reg_dt, new_reg_dt)
    VALUES(business.rf_cust_changes_seq.NextVal, v.cust_seq_id, v.data_dump_dt, v.old_reg_dt, v.new_reg_dt);
    
  COMMIT;  
  --
  -- ��������� ����������� ����� ����� �������� ��� 4005 ����
  --
  SELECT nvl(min(limit_am), var_min_limit)
    INTO var_min_limit
    FROM mantas.rf_opok_details
   WHERE opok_nb = 4005 and
         active_fl = 'Y'; 
  ----------------------- ����������� �������� ----------------------------------------------------------
  --
  -- ������� �������� ����������� ��������, ��� ������� ����� ��������� ��������� ���������
  -- TO DO: �������� ������ ������ �������� �� ��� ����� ���������� �������� ��� ����������� ������ ��������!
  --
  DELETE FROM business.rf_cust_change_regdt#wrk;
  
  INSERT INTO business.rf_cust_change_regdt#wrk(row_id, data_dump_dt)
    WITH change_log as
    (select cust_seq_id, data_dump_dt, new_reg_dt
       from (select t.*,
                    row_number() over(partition by cust_seq_id order by record_seq_id desc) as rn
               from business.rf_cust_change_regdt_log #PART_SUBPART# t)
      where rn = 1 and 
            new_reg_dt is not null) 
    SELECT row_id            as row_id, 
           max(data_dump_dt) as data_dump_dt
      FROM (select row_id,               
                   trxn_dump_dt,
                   greatest(data_dump_dt,
                            (select nvl(max(rf_data_dump_dt) + 1/24/3600, to_date('01.01.1900', 'dd.mm.yyyy')) -- ������������ ���� �� ������� �� �������� + 1 �������
                               from mantas.kdd_review
                              where rf_op_cat_cd = 'WT' and
                                    rf_trxn_seq_id = trxn_seq_id)) as data_dump_dt,
                   active_partition_flag,
                   opok_status                  
              from (select /*+ LEADING(cl) USE_NL(wt) INDEX(wt RF_WIRE_TRXN_ORIG_CUST_FK_I)*/
                           wt.rowid as row_id,
                           wt.fo_trxn_seq_id as trxn_seq_id,
                           wt.data_dump_dt as trxn_dump_dt,
                           greatest(nvl(cl.data_dump_dt, to_date('01.01.1900', 'dd.mm.yyyy')), wt.data_dump_dt) as data_dump_dt,
                           wt.rf_active_partition_flag as active_partition_flag,
                           nvl(wt.rf_opok_status, 2) as opok_status
                      from change_log cl
                           join business.wire_trxn wt on wt.rf_orig_cust_seq_id = cl.cust_seq_id and
                                                         wt.rf_trxn_opok_am >= var_min_limit and
                                                         wt.trxn_exctn_dt between trunc(cl.new_reg_dt) and add_months(trunc(cl.new_reg_dt), var_months_qty) and
                                                         nvl(wt.rf_canceled_fl, 'N') <> 'Y'
                    union all
                    select /*+ LEADING(cl) USE_NL(wt) INDEX(wt RF_WIRE_TRXN_BENEF_CUST_FK_I)*/
                           wt.rowid as row_id,
                           wt.fo_trxn_seq_id as trxn_seq_id,
                           wt.data_dump_dt as trxn_dump_dt,
                           greatest(nvl(cl.data_dump_dt, to_date('01.01.1900', 'dd.mm.yyyy')), wt.data_dump_dt) as data_dump_dt,
                           wt.rf_active_partition_flag as active_partition_flag,
                           nvl(wt.rf_opok_status, 2) as opok_status 
                      from change_log cl
                           join business.wire_trxn wt on wt.rf_benef_cust_seq_id = cl.cust_seq_id and
                                                         wt.rf_trxn_opok_am >= var_min_limit and
                                                         wt.trxn_exctn_dt between trunc(cl.new_reg_dt) and add_months(trunc(cl.new_reg_dt), var_months_qty) and
                                                         nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                                                         nvl(wt.rf_orig_cust_seq_id, -1) <> cl.cust_seq_id
                    union all
                    select /*+ LEADING(cl) USE_NL(a wt) INDEX(a) INDEX(wt RF_WIRE_TRXN_ORIG_ACCT_FK_I)*/
                           wt.rowid as row_id,
                           wt.fo_trxn_seq_id as trxn_seq_id,
                           wt.data_dump_dt as trxn_dump_dt,
                           greatest(nvl(cl.data_dump_dt, to_date('01.01.1900', 'dd.mm.yyyy')), wt.data_dump_dt) as data_dump_dt,
                           wt.rf_active_partition_flag as active_partition_flag,
                           nvl(wt.rf_opok_status, 2) as opok_status 
                      from change_log cl
                           join business.acct a on a.rf_prmry_cust_seq_id = cl.cust_seq_id
                           join business.wire_trxn wt on wt.rf_orig_acct_seq_id = a.acct_seq_id and
                                                         wt.rf_trxn_opok_am >= var_min_limit and
                                                         wt.trxn_exctn_dt between trunc(cl.new_reg_dt) and add_months(trunc(cl.new_reg_dt), var_months_qty) and
                                                         nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                                                         nvl(wt.rf_orig_cust_seq_id, -1) <> cl.cust_seq_id and
                                                         nvl(wt.rf_benef_cust_seq_id, -1) <> cl.cust_seq_id
                    union all
                    select /*+ LEADING(cl) USE_NL(a wt) INDEX(a) INDEX(wt RF_WIRE_TRXN_BENEF_ACCT_FK_I)*/
                           wt.rowid as row_id,
                           wt.fo_trxn_seq_id as trxn_seq_id,
                           wt.data_dump_dt as trxn_dump_dt,
                           greatest(nvl(cl.data_dump_dt, to_date('01.01.1900', 'dd.mm.yyyy')), wt.data_dump_dt) as data_dump_dt,
                           wt.rf_active_partition_flag as active_partition_flag,
                           nvl(wt.rf_opok_status, 2) as opok_status 
                      from change_log cl
                           join business.acct a on a.rf_prmry_cust_seq_id = cl.cust_seq_id
                           join business.wire_trxn wt on wt.rf_benef_acct_seq_id = a.acct_seq_id and
                                                         wt.rf_trxn_opok_am >= var_min_limit and
                                                         wt.trxn_exctn_dt between trunc(cl.new_reg_dt) and add_months(trunc(cl.new_reg_dt), var_months_qty) and
                                                         nvl(wt.rf_canceled_fl, 'N') <> 'Y' and
                                                         nvl(wt.rf_orig_cust_seq_id, -1) <> cl.cust_seq_id and
                                                         nvl(wt.rf_benef_cust_seq_id, -1) <> cl.cust_seq_id and
                                                         nvl(wt.rf_orig_acct_seq_id, -1) <> a.acct_seq_id                   
                   )) 
    GROUP BY row_id                                                                 
    HAVING max(active_partition_flag) <> 1 or       -- �������� �� � �������� ��������
           max(opok_status) <> 0 or                 -- ��� �� �������� "� ���������"
           max(trxn_dump_dt) <> max(data_dump_dt); -- ��� ���� �������� �� ��������� � ��������� (������ �����, ������ ���� �������� �������� ������ ����� ���� �������� �������)
  COMMIT;
  --
  -- ��������� ��������� ��������� ����������� �������� - ������� �� � �������� ��������, ������� ���� ��������, ������� ���� ���������
  -- �. �. ������������ ������, �������������� ������ ��������, ����� ���������� �� ����� � ��� �� ���������, ��
  -- ��������� ��������� �������� � ��������������� �� ����������� (FOR UPDATE SKIP LOCKED), ����� �������� ��������
  --  
  var_cycles := 0;
  var_rows_qty := 0;
  WHILE true LOOP
    -- �������� rowid �����, ������� ����� ��������,
    -- ��� ���� ��������� ��, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM business.wire_trxn
     WHERE rowid in (select row_id from business.rf_cust_change_regdt#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;

    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM business.wire_trxn
       WHERE rowid in (select row_id from business.rf_cust_change_regdt#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ����� ������� BUSINESS.WIRE_TRXN'||
                          ' (�������� #PARTITION_NAME# ������� BUSINESS.RF_CUST_CHANGE_REGDT_LOG)'||
                          ' ����� �������� �������� � ���������� ���������', 'business.rf_pkg_postload.mark_repeat_cust_regdt');
      end if;

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      MERGE INTO business.wire_trxn t USING
        (SELECT row_id, data_dump_dt
           FROM business.rf_cust_change_regdt#wrk
          WHERE row_id in (select chartorowid(column_value) from table(var_rowids))) v
      ON (t.rowid = v.row_id)
      WHEN MATCHED THEN
        UPDATE
           SET t.rf_active_partition_flag = 1,
               t.rf_opok_status = 0,
               t.data_dump_dt = v.data_dump_dt;

      var_rows_qty := var_rows_qty + SQL%ROWCOUNT;
               
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM business.rf_cust_change_regdt#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
       
      COMMIT;          
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� �������� � ���������� ��������� � ������� BUSINESS.WIRE_TRXN'||
                                      ' - ����� ����� ������������� ������� �������� ���������� ����� (business.rf_pkg_postload.mark_repeat_cust_regdt)');
    end if;
  END LOOP;

  INSERT INTO business.rf_parallel_execute#wrk(thread_name, val1) VALUES('WT|#PARTITION_NAME#', var_rows_qty);
  COMMIT;
  
  --------------------------�������� �������� ----------------------------------------------------------
  --
  -- ������� �������� �������� ��������, ��� ������� ����� ��������� ��������� ���������
  -- TO DO: �������� ������ ������ �������� �� ��� ����� ���������� �������� ��� ����������� ������ ��������!
  --
  DELETE FROM business.rf_cust_change_regdt#wrk;
  
  INSERT INTO business.rf_cust_change_regdt#wrk(row_id, data_dump_dt)
    WITH change_log as
    (select cust_seq_id, data_dump_dt, new_reg_dt
       from (select t.*,
                    row_number() over(partition by cust_seq_id order by record_seq_id desc) as rn
               from business.rf_cust_change_regdt_log #PART_SUBPART# t)
      where rn = 1 and 
            new_reg_dt is not null) 
    SELECT row_id            as row_id, 
           max(data_dump_dt) as data_dump_dt
      FROM (select row_id,               
                   trxn_dump_dt,
                   greatest(data_dump_dt,
                            (select nvl(max(rf_data_dump_dt) + 1/24/3600, to_date('01.01.1900', 'dd.mm.yyyy')) -- ������������ ���� �� ������� �� �������� + 1 �������
                               from mantas.kdd_review
                              where rf_op_cat_cd = 'CT' and
                                    rf_trxn_seq_id = trxn_seq_id)) as data_dump_dt,
                   active_partition_flag,
                   opok_status                  
              from (select /*+ LEADING(cl) USE_NL(ct) INDEX(ct RF_CASH_TRXN_CUST_FK_I)*/
                           ct.rowid as row_id,
                           ct.fo_trxn_seq_id as trxn_seq_id,
                           ct.data_dump_dt as trxn_dump_dt,
                           greatest(nvl(cl.data_dump_dt, to_date('01.01.1900', 'dd.mm.yyyy')), ct.data_dump_dt) as data_dump_dt,
                           ct.rf_active_partition_flag as active_partition_flag,
                           nvl(ct.rf_opok_status, 2) as opok_status
                      from change_log cl
                           join business.cash_trxn ct on ct.rf_cust_seq_id = cl.cust_seq_id and
                                                         ct.rf_trxn_opok_am >= var_min_limit and
                                                         ct.trxn_exctn_dt between trunc(cl.new_reg_dt) and add_months(trunc(cl.new_reg_dt), var_months_qty) and
                                                         nvl(ct.rf_canceled_fl, 'N') <> 'Y' 
                    union all
                    select /*+ LEADING(cl) USE_NL(a ct) INDEX(a) INDEX(ct RF_CASH_TRXN_ACCT_FK_I)*/
                           ct.rowid as row_id,
                           ct.fo_trxn_seq_id as trxn_seq_id,
                           ct.data_dump_dt as trxn_dump_dt,
                           greatest(nvl(cl.data_dump_dt, to_date('01.01.1900', 'dd.mm.yyyy')), ct.data_dump_dt) as data_dump_dt,
                           ct.rf_active_partition_flag as active_partition_flag,
                           nvl(ct.rf_opok_status, 2) as opok_status
                      from change_log cl
                           join business.acct a on a.rf_prmry_cust_seq_id = cl.cust_seq_id
                           join business.cash_trxn ct on ct.rf_acct_seq_id = a.acct_seq_id and
                                                         ct.rf_trxn_opok_am >= var_min_limit and
                                                         ct.trxn_exctn_dt between trunc(cl.new_reg_dt) and add_months(trunc(cl.new_reg_dt), var_months_qty) and 
                                                         nvl(ct.rf_canceled_fl, 'N') <> 'Y' and
                                                         nvl(ct.rf_cust_seq_id, -1) <> cl.cust_seq_id 
                   )) 
    GROUP BY row_id                                                                 
    HAVING max(active_partition_flag) <> 1 or       -- �������� �� � �������� ��������
           max(opok_status) <> 0 or                 -- ��� �� �������� "� ���������"
           max(trxn_dump_dt) <> max(data_dump_dt); -- ��� ���� �������� �� ��������� � ��������� (������ �����, ������ ���� �������� �������� ������ ����� ���� �������� �������)
  COMMIT;
  --
  -- ��������� ��������� ��������� �������� �������� - ������� �� � �������� ��������, ������� ���� ��������, ������� ���� ���������
  -- �. �. ������������ ������, �������������� ������ ��������, ����� ���������� �� ����� � ��� �� ���������, ��
  -- ��������� ��������� �������� � ��������������� �� ����������� (FOR UPDATE SKIP LOCKED), ����� �������� ��������
  --  
  var_cycles := 0;
  var_rows_qty := 0;
  WHILE true LOOP
    -- �������� rowid �����, ������� ����� ��������,
    -- ��� ���� ��������� ��, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM business.cash_trxn
     WHERE rowid in (select row_id from business.rf_cust_change_regdt#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;

    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM business.cash_trxn
       WHERE rowid in (select row_id from business.rf_cust_change_regdt#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ����� ������� BUSINESS.CASH_TRXN'||
                          ' (�������� #PARTITION_NAME# ������� BUSINESS.RF_CUST_CHANGE_REGDT_LOG)'||
                          ' ����� �������� �������� � ���������� ���������', 'business.rf_pkg_postload.mark_repeat_cust_regdt');
      end if;

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      MERGE INTO business.cash_trxn t USING
        (SELECT row_id, data_dump_dt
           FROM business.rf_cust_change_regdt#wrk
          WHERE row_id in (select chartorowid(column_value) from table(var_rowids))) v
      ON (t.rowid = v.row_id)
      WHEN MATCHED THEN
        UPDATE
           SET t.rf_active_partition_flag = 1,
               t.rf_opok_status = 0,
               t.data_dump_dt = v.data_dump_dt;

      var_rows_qty := var_rows_qty + SQL%ROWCOUNT;
               
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM business.rf_cust_change_regdt#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
       
      COMMIT;          
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� �������� � ���������� ��������� � ������� BUSINESS.CASH_TRXN'||
                                      ' - ����� ����� ������������� ������� �������� ���������� ����� (business.rf_pkg_postload.mark_repeat_cust_regdt)');
    end if;
  END LOOP;

  INSERT INTO business.rf_parallel_execute#wrk(thread_name, val1) VALUES('CT|#PARTITION_NAME#', var_rows_qty);
  COMMIT;

  --------------------------���������� �������� --------------------------------------------------------
  --
  -- �. �. ������������ ������, �������������� ������ ��������, �� ����� ���������� �� ����� � ��� ��
  -- ���������� ���������, �� �������� ���������� �������� � ���������� ��������� ��-��������
  --
  MERGE INTO business.back_office_trxn t USING
   (WITH change_log as
    (select cust_seq_id, data_dump_dt, new_reg_dt
       from (select t.*,
                    row_number() over(partition by cust_seq_id order by record_seq_id desc) as rn
               from business.rf_cust_change_regdt_log #PART_SUBPART# t)
      where rn = 1 and 
            new_reg_dt is not null) 
    SELECT row_id            as row_id, 
           max(data_dump_dt) as data_dump_dt
      FROM (select row_id,               
                   trxn_dump_dt,
                   greatest(data_dump_dt,
                            (select nvl(max(rf_data_dump_dt) + 1/24/3600, to_date('01.01.1900', 'dd.mm.yyyy')) -- ������������ ���� �� ������� �� �������� + 1 �������
                               from mantas.kdd_review
                              where rf_op_cat_cd = 'BOT' and
                                    rf_trxn_seq_id = trxn_seq_id)) as data_dump_dt,
                   active_partition_flag,
                   opok_status                  
              from (select /*+ LEADING(cl) USE_NL(a bot) INDEX(a) INDEX(bot RF_BO_TRXN_ACCT_FK_I)*/
                           bot.rowid as row_id,
                           bot.bo_trxn_seq_id as trxn_seq_id,
                           bot.data_dump_dt as trxn_dump_dt,
                           greatest(nvl(cl.data_dump_dt, to_date('01.01.1900', 'dd.mm.yyyy')), bot.data_dump_dt) as data_dump_dt,
                           bot.rf_active_partition_flag as active_partition_flag,
                           nvl(bot.rf_opok_status, 2) as opok_status
                      from change_log cl
                           join business.acct a on a.rf_prmry_cust_seq_id = cl.cust_seq_id
                           join business.back_office_trxn bot on bot.rf_acct_seq_id = a.acct_seq_id and
                                                                 bot.trxn_base_am >= var_min_limit and
                                                                 bot.exctn_dt between trunc(cl.new_reg_dt) and add_months(trunc(cl.new_reg_dt), var_months_qty) and 
                                                                 nvl(bot.rf_canceled_fl, 'N') <> 'Y' 
                   )) 
    GROUP BY row_id                                                                 
    HAVING max(active_partition_flag) <> 1 or        -- �������� �� � �������� ��������
           max(opok_status) <> 0 or                  -- ��� �� �������� "� ���������"
           max(trxn_dump_dt) <> max(data_dump_dt)) v -- ��� ���� �������� �� ��������� � ��������� (������ �����, ������ ���� �������� �������� ������ ����� ���� �������� �������)
  ON (t.rowid = v.row_id)
  WHEN MATCHED THEN
    UPDATE
       SET t.rf_active_partition_flag = 1,
           t.rf_opok_status = 0,
           t.data_dump_dt = v.data_dump_dt;

  var_rows_qty := SQL%ROWCOUNT;

  INSERT INTO business.rf_parallel_execute#wrk(thread_name, val1) VALUES('BOT|#PARTITION_NAME#', var_rows_qty);
  COMMIT;
  --
  -- ������� ������������ ������ �� ���� ���������
  --
  DELETE FROM business.rf_cust_change_regdt_log #PART_SUBPART#;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error('������ ��� ��������� �������� � ���������� ��������� � ����� � ���������� ���� ����������� ������� (�������� #PARTITION_NAME# ������� BUSINESS.RF_CUST_CHANGE_REGDT_LOG)'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.mark_repeat_cust_regdt'); 
    RAISE;
END;  
}';
  var_err_count     INTEGER; 
  var_wt_count      INTEGER; 
  var_ct_count      INTEGER; 
  var_bot_count     INTEGER;
BEGIN
  --
  -- ������� �������, ���� ������������ ������ ���������� �����
  --
  DELETE FROM business.rf_parallel_execute#wrk;
  COMMIT;
  
  var_err_count := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_sql,  
                                                           par_degree         => 128, 
                                                           par_task_name      => 'aml_mark_repeat_cust_regdt',
                                                           par_table_owner    => 'BUSINESS', 
                                                           par_table_name     => 'RF_CUST_CHANGE_REGDT_LOG', 
                                                           par_partition_name => null,
                                                           par_chunk_type     => 'P',
                                                           par_log_level      => 'ERROR');
  --
  -- ���������� � ������� � ��� ����� �����
  --
  SELECT nvl(sum(case when thread_name like 'WT|%'
                      then val1
                 end), 0),
         nvl(sum(case when thread_name like 'CT|%'
                      then val1
                 end), 0),
         nvl(sum(case when thread_name like 'BOT|%'
                      then val1
                 end), 0)         
    INTO var_wt_count, var_ct_count, var_bot_count
    FROM business.rf_parallel_execute#wrk;
  
  std.pkg_log.info('�������� �� ���� ������������ ������� �������� � ���������� ��������� (� ����� � ���������� ���� ����������� �������) -'||
                   ' �����������: '||to_char(var_wt_count)||','||
                   ' ��������: '||to_char(var_ct_count)||','||
                   ' ����������: '||to_char(var_bot_count)||
                   case when var_wt_count > 1
                        then '. ����������� ���������� ���������� ����������� �������� ����� ���� ��������� ������ �� ���� ��������,'||
                             ' � ������� � � �����������, � � ���������� ���������� ���� ����������� (����� �������� ����� ���� ���������� ��� ����)' 
                   end, 'business.rf_pkg_postload.mark_repeat_cust_regdt');

  DELETE FROM business.rf_parallel_execute#wrk;
  COMMIT;

  if var_err_count > 0 Then
    return 1; -- ���� ������
  else  
    return 0; -- ��� ������
  end if;  
EXCEPTION
  WHEN OTHERS THEN 
    std.pkg_log.error('������ ��� ��������� �������� � ���������� ��������� � ����� � ���������� ���� ����������� �������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.mark_repeat_cust_regdt'); 
    return 1; -- ���� ������
END mark_repeat_cust_regdt;

  /*
FUNCTION set_ext_cust_as_int RETURN INTEGER IS -- ���� 1/0 ������: 0 - ��� ������, 1 - ���� ������
BEGIN
  EXECUTE IMMEDIATE 'alter session force parallel dml parallel 64';
  EXECUTE IMMEDIATE 'alter session force parallel query parallel 64';
  --
  -- ����������� � ����������� ���������
  --  
  MERGE INTO business.wire_trxn t USING 
   (WITH sb_bic as
    (select distinct cstm_2_tx as bic
       from business.org
      where org_type_cd = '��'
        and cstm_2_tx is not null) 
    SELECT --+ PARALLEL(wt 64) LEADING(wt) USE_NL(ac)
           ac.acct_seq_id as rf_orig_acct_seq_id, 
           ac.rf_prmry_cust_seq_id as rf_orig_cust_seq_id, 
           wt.fo_trxn_seq_id
      FROM sb_bic
           JOIN business.wire_trxn wt ON wt.send_instn_id = sb_bic.bic
           --JOIN std.recode_acct ra ON ra.source_id_type = 'N20' and ra.source_id = wt.rf_orig_acct_nb
           JOIN business.acct ac ON ac.rowid = business.rf_pkg_util.get_acct(wt.rf_orig_acct_nb, trunc(wt.trxn_exctn_dt), wt.src_sys_cd)
     WHERE wt.rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and -- ������ �������� ��������
           wt.rf_opok_status = 0 and -- ������ ��� �� ������������ ������
           wt.rf_orig_cust_seq_id is null and
           wt.rf_orig_acct_seq_id is null and
           wt.rf_orig_acct_nb not like '3%' and -- �������� ����. �����, ����-������ � �. �. - �� �������� �� �������� ������������/�����������
           ac.acct_seq_id <> -1 and
           ac.rf_prmry_cust_seq_id <> -1) v
  ON (v.fo_trxn_seq_id = t.fo_trxn_seq_id)
    WHEN MATCHED THEN
      UPDATE
         SET t.rf_orig_acct_seq_id  = v.rf_orig_acct_seq_id,
             t.rf_orig_cust_seq_id  = v.rf_orig_cust_seq_id,
             t.orig_acct_id         = to_char(v.rf_orig_acct_seq_id),
             t.orig_acct_id_type_cd = 'IA';       
  --
  -- ��������� ��������� ����� ������������� ��������� ������� ��������
  --
  COMMIT;         
  --
  -- ���������� � ����������� ���������
  --  
  MERGE INTO business.wire_trxn t USING 
   (WITH sb_bic as
    (select distinct cstm_2_tx as bic
       from business.org
      where org_type_cd = '��'
        and cstm_2_tx is not null) 
    SELECT --+ PARALLEL(wt 64) LEADING(wt) USE_NL(ac)
           ac.acct_seq_id as rf_benef_acct_seq_id, 
           ac.rf_prmry_cust_seq_id as rf_benef_cust_seq_id, 
           wt.fo_trxn_seq_id
      FROM sb_bic
           JOIN business.wire_trxn wt ON wt.rcv_instn_id = sb_bic.bic
           --JOIN std.recode_acct ra ON ra.source_id_type = 'N20' and ra.source_id = wt.rf_benef_acct_nb
           JOIN business.acct ac ON ac.rowid = business.rf_pkg_util.get_acct(wt.rf_benef_acct_nb, trunc(wt.trxn_exctn_dt), wt.src_sys_cd)
     WHERE wt.rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and -- ������ �������� ��������
           wt.rf_opok_status = 0 and -- ������ ��� �� ������������ ������
           wt.rf_benef_cust_seq_id is null and
           wt.rf_benef_acct_seq_id is null and
           wt.rf_benef_acct_nb not like '3%' and -- �������� ����. �����, ����-������ � �. �. - �� �������� �� �������� ������������/�����������
           ac.acct_seq_id <> -1 and
           ac.rf_prmry_cust_seq_id <> -1) v
  ON (v.fo_trxn_seq_id = t.fo_trxn_seq_id)
    WHEN MATCHED THEN
      UPDATE
         SET t.rf_benef_acct_seq_id  = v.rf_benef_acct_seq_id,
             t.rf_benef_cust_seq_id  = v.rf_benef_cust_seq_id,
             t.benef_acct_id         = to_char(v.rf_benef_acct_seq_id),
             t.benef_acct_id_type_cd = 'IA';       
  --
  -- ��������� ��������� ����� ������������� ��������� ������� ��������
  --
  COMMIT;         
  EXECUTE IMMEDIATE 'alter session enable parallel dml';
  EXECUTE IMMEDIATE 'alter session enable parallel query';
  
  return 0;
EXCEPTION
  WHEN OTHERS THEN 
    std.pkg_log.error('������ ��� ������������ ������� ���������� �������� � ������������ ��������/������. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'business.rf_pkg_postload.set_ext_cust_as_int'); 
    BEGIN
      EXECUTE IMMEDIATE 'alter session enable parallel dml';
      EXECUTE IMMEDIATE 'alter session enable parallel query';
    EXCEPTION
      WHEN OTHERS THEN
        null;
    END;      
    return 1; -- ���� ������
END set_ext_cust_as_int;*/
/*
--
-- ����������� ������� ������ ��� ������ (������� RF_ACCT_DAILY_SUMMARY)
--
PROCEDURE calc_acct_daily_summary
(  
  par_all_flag       VARCHAR2   DEFAULT 'N' -- ����: Y - ���� ����������� ������ � ������������ �� ����� ���������� � �� (������������ ��� ��������������� ���������� ������)
                                            --       N - ������ � ������������ � ������/������������� ���������� � �������� ��������
)    IS
BEGIN
  -- ����������: �� �������� �������� ����� �������� ���� ����-����, ������� ���� �����������
  -- ����� ���� ������������ ���!!! �������� �� ���� ������-�����, � �� ������ ��, ������� � �������� ��������
  --
  -- ���� ������ ������� "��� ��������" � �������������, ��� "������" ��� ����-���� � ������� ���
  MERGE INTO business.rf_acct_daily_summary t USING 
   (SELECT --+ PARALLEL
           acct.alt_acct_id as acct_nb,
           acct.acct_rptng_crncy_cd as acct_crncy_cd,
           acct.rf_branch_id as branch_id,
           trunc(acct.rf_branch_id/100000)*1000 + ora_hash(acct.acct_seq_id,  63) + 1 as subpartition_key,
           smry.*
      FROM (SELECT acct_seq_id, 
                   trunc(date_dt) as date_dt,
                   nvl(sum(decode(in_out_flag, 'IN', 1)), 0) as in_ct,
                   nvl(sum(decode(in_out_flag, 'IN', acct_am)), 0) as in_acct_am,
                   nvl(sum(decode(in_out_flag, 'IN', base_am)), 0) as in_base_am,
                   nvl(sum(decode(in_out_flag, 'OUT', 1)), 0) as out_ct,
                   nvl(sum(decode(in_out_flag, 'OUT', acct_am)), 0) as out_acct_am,
                   nvl(sum(decode(in_out_flag, 'OUT', base_am)), 0) as out_base_am
              FROM (select rf_orig_acct_seq_id as acct_seq_id, 
                           'OUT' as in_out_flag,
                           trxn_exctn_dt as date_dt,
                           orig_trxn_actvy_am as acct_am,
                           trxn_base_am as base_am                   
                      from business.wire_trxn
                     where rf_orig_acct_seq_id <> -1 and
                           rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and --������ �������� ��������
                           rf_opok_status = 0 and --������ ��� �� ������������ ������
                           nvl(rf_canceled_fl, 'N') <> 'Y'
                    union all
                    select rf_benef_acct_seq_id as acct_seq_id, 
                           'IN' as in_out_flag,
                           trxn_exctn_dt as date_dt,
                           benef_trxn_actvy_am as acct_am,
                           trxn_base_am as base_am                   
                      from business.wire_trxn
                     where rf_benef_acct_seq_id <> -1 and
                           rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and
                           rf_opok_status = 0 and
                           nvl(rf_canceled_fl, 'N') <> 'Y' 
                    union all
                    select rf_acct_seq_id as acct_seq_id, 
                           case when dbt_cdt_cd = 'C' then 'IN' else 'OUT' end as in_out_flag, 
                           trxn_exctn_dt as date_dt,
                           rf_trxn_acct_am as acct_am,
                           trxn_base_am as base_am 
                      from business.cash_trxn  
                     where rf_acct_seq_id <> -1 and
                           rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and
                           rf_opok_status = 0 and
                           nvl(rf_canceled_fl, 'N') <> 'Y' 
                    union all
                    select rf_acct_seq_id as acct_seq_id, 
                           case when dbt_cdt_cd = 'C' then 'IN' else 'OUT' end as in_out_flag, 
                           exctn_dt as date_dt,
                           trxn_rptng_am as acct_am,
                           trxn_base_am as base_am
                      from business.back_office_trxn
                     where rf_acct_seq_id <> -1 and
                           rf_partition_date = to_date('01.01.9999', 'dd.mm.yyyy') and
                           rf_opok_status = 0 and
                           nvl(rf_canceled_fl, 'N') <> 'Y')   
             GROUP BY acct_seq_id, trunc(date_dt)) smry,
           business.acct acct
     WHERE acct.acct_seq_id = smry.acct_seq_id) v
  ON (v.acct_seq_id = t.acct_seq_id and
      v.date_dt     = t.date_dt)
  WHEN NOT MATCHED THEN
    INSERT (date_dt, acct_seq_id, acct_nb, acct_crncy_cd, in_ct, in_acct_am, in_base_am,
            out_ct, out_acct_am, out_base_am, branch_id, subpartition_key)
    VALUES (v.date_dt, v.acct_seq_id, v.acct_nb, v.acct_crncy_cd, v.in_ct, v.in_acct_am, v.in_base_am,
            v.out_ct, v.out_acct_am, v.out_base_am, v.branch_id, v.subpartition_key)        
  WHEN MATCHED THEN
    UPDATE
       SET t.acct_nb = v.acct_nb,
           t.acct_crncy_cd = v.acct_crncy_cd,
           t.in_ct = v.in_ct,
           t.in_acct_am = v.in_acct_am,
           t.in_base_am = v.in_base_am,
           t.out_ct = v.out_ct,
           t.out_acct_am = v.out_acct_am,
           t.out_base_am = v.out_base_am,
           t.branch_id = v.branch_id,
           t.subpartition_key = v.subpartition_key;       
END calc_acct_daily_summary;
*/
end rf_pkg_postload;
/

grant EXECUTE on BUSINESS.rf_pkg_postload to DATA_READER;

grant EXECUTE on BUSINESS.rf_pkg_postload to KDD_ALGORITHM;

grant EXECUTE on BUSINESS.rf_pkg_postload to KDD_ANALYST;

grant EXECUTE on BUSINESS.rf_pkg_postload to KDD_MINER;

grant EXECUTE on BUSINESS.rf_pkg_postload to RF_RSCHEMA_ROLE;

grant EXECUTE on BUSINESS.rf_pkg_postload to CMREVMAN;

grant EXECUTE on BUSINESS.rf_pkg_postload to KDD_MNR with grant option;

grant EXECUTE on BUSINESS.rf_pkg_postload to KDD_REPORT with grant option;

grant EXECUTE on BUSINESS.rf_pkg_postload to KYC;

grant EXECUTE on BUSINESS.rf_pkg_postload to MANTAS;

grant EXECUTE on BUSINESS.rf_pkg_postload to STD;


