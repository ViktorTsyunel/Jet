-- ���������� ����������� business.rf_currency
-- ������ ��� ������������
--
-- ������� ������������ ���� (iso_num, num_code):
-- 9+���_�����_����+�����_����
-- ��� ���_�����_���� = "A" - 0, "B" - 1, "C" - 2, "E" - 3, "H" - 4, "K" - 5, "M" - 6, "P" - 7, "T" - 8, "X" - 9
--
-- ������� ������ ��������
DELETE FROM business.rf_currency WHERE metal_flag=1 AND  metal_code IS NOT NULL;
-- ��������� �����
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('A20',9020,9020,'������� � �������� ����',1,1,'A20');
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('A30',9030,9030,'����� � �������',1,1,'A30');  
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('A31',9031,9031,'����� � ������',1,1,'A31');  
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('A34',9034,9034,'�������� � ������',1,1,'A34');   
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('A90',9090,9090,'������ � ������',1,1,'A90');   
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('A91',9091,9091,'������� � ������',1,1,'A91');   
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('A92',9092,9092,'������� � ������',1,1,'A92');   
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('B69',9169,9169,'��������� �������',1,1,'B69');   
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('B98',9198,9198,'������ � �������� ������',1,1,'B98');   
INSERT INTO business.rf_currency(iso_alfa, iso_num, num_code, name, qty, metal_flag, metal_code)
  VALUES('B99',9199,9199,'����������� �����',1,1,'B99');   
--
COMMIT;
