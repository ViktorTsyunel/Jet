--
-- ��������! ��� ��������� ��������� ������� ������� ����������� �������, ������������ ��� �������:
--   BUSINESS: RF_CUSTIDDOC_NO_I
--   MANTAS: RF_OES321P_SD_VD1_I
-- ���������: ��� �� ������ ����� �������� !!!
--
-- ������� ���������� ��������������� ������ ����� (�����+�����) ���������:
--   - ������� �� �����/������ ��������� ������� � ����. �������, ��������� �� � ������� �������
--   - �����/�����, ������ 0, ���������� � null
--   - ��� �������� �� � �������� ���� ��������� ����� �� 6 ���� ����� ������ (���� ����� 6 ��������)
--   - ���������� ������ ����� - ����������� ����� � ������
-- ������������ � �������� �� ��� ��� ������ ������� ��� � ����������� ��������, ��� � � ���
--
CREATE OR REPLACE 
FUNCTION business.rf_normalize_docno
(
  par_series_id         business.rf_cust_id_doc.series_id%TYPE,
  par_no_id             business.rf_cust_id_doc.no_id%TYPE,
  par_doc_type_cd       business.rf_cust_id_doc.doc_type_cd%TYPE,
  par_doc_type_cbrf_cd  business.rf_cust_id_doc_type.cbrf_cd%TYPE DEFAULT null,
  par_tu                INTEGER DEFAULT null -- 1-��, 2-��, 3-��, 4-����������, 0-��� ���������, null - �� ���
)
RETURN VARCHAR2 DETERMINISTIC IS
  var_series_id       business.rf_cust_id_doc.series_id%TYPE;
  var_no_id           business.rf_cust_id_doc.no_id%TYPE;
BEGIN
  --
  -- ���� ��� �� ��� ������������� �������� - ������� null
  --
  if par_tu in (1, 0) Then
    return null;
  end if;
    
  var_series_id := upper(translate(par_series_id, 'A !@#$%^&*()_-+={}[]:;"''<>,.?/|\`~', 'A'));
  var_series_id := nullif(var_series_id, '0');
  
  var_no_id     := upper(translate(par_no_id,     'A !@#$%^&*()_-+={}[]:;"''<>,.?/|\`~', 'A'));
  var_no_id     := nullif(var_no_id, '0');

  if (par_doc_type_cd in (1, 21) /*������� ����, ��*/ or
      (par_doc_type_cd is null and par_doc_type_cbrf_cd in ('21') /*������� ��*/)) and 
     length(var_no_id) < 6 Then
    var_no_id := lpad(var_no_id, 6, '0');
  end if;
    
  return var_series_id||var_no_id;
END rf_normalize_docno;
/

grant EXECUTE on BUSINESS.rf_normalize_docno to MANTAS with grant option;
grant EXECUTE on BUSINESS.rf_normalize_docno to DATA_READER;
grant EXECUTE on BUSINESS.rf_normalize_docno to KDD_ALGORITHM;
grant EXECUTE on BUSINESS.rf_normalize_docno to KDD_ANALYST;
grant EXECUTE on BUSINESS.rf_normalize_docno to KDD_MINER;
grant EXECUTE on BUSINESS.rf_normalize_docno to RF_RSCHEMA_ROLE;
grant EXECUTE on BUSINESS.rf_normalize_docno to CMREVMAN;
grant EXECUTE on BUSINESS.rf_normalize_docno to KDD_MNR with grant option;
grant EXECUTE on BUSINESS.rf_normalize_docno to KDD_REPORT with grant option;
grant EXECUTE on BUSINESS.rf_normalize_docno to KYC;
grant EXECUTE on BUSINESS.rf_normalize_docno to STD;

