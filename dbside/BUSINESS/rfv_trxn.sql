GRANT SELECT ON mantas.rf_oes_321_org TO BUSINESS;
GRANT EXECUTE ON mantas.rf_pkg_kgrko TO BUSINESS;

DROP TYPE BUSINESS.RF_TAB_TRXN
/

CREATE OR REPLACE TYPE BUSINESS.RF_TYPE_TRXN AS OBJECT 
(
 op_cat_cd                     VARCHAR2(3 CHAR),
 dbt_cdt_cd                    VARCHAR2(20 CHAR),
 trxn_seq_id                   NUMBER(22),
 trxn_dt                       DATE,
 branch_id                     NUMBER(22),
 org_intrl_id                  VARCHAR2(64 CHAR),
 trxn_crncy_am                 NUMBER,
 trxn_base_am                  NUMBER,
 trxn_crncy_cd                 VARCHAR2(3 CHAR),
 trxn_crncy_rate_am            NUMBER,
 trxn_crncy_old_rate_fl        VARCHAR2(1 CHAR),
 trxn_desc                     VARCHAR2(2000 CHAR),
 nsi_op_id                     VARCHAR2(20 CHAR),
 orig_nm                       VARCHAR2(2000 CHAR),
 orig_inn_nb                   VARCHAR2(255 CHAR),
 orig_kpp_nb                   VARCHAR2(255 CHAR),
 orig_acct_nb                  VARCHAR2(255 CHAR),
 orig_own_fl                   VARCHAR2(1 CHAR),
 debit_cd                      VARCHAR2(255 CHAR),
 orig_addr_tx                  VARCHAR2(2000 CHAR),
 orig_iddoc_tx                 VARCHAR2(2000 CHAR),
 orig_reg_dt                   DATE, 
 orig_goz_op_cd                VARCHAR2(64 CHAR),
 send_instn_nm                 VARCHAR2(2000 CHAR),
 send_instn_id                 VARCHAR2(255 CHAR),
 send_instn_cntry_cd           VARCHAR2(3 CHAR),
 send_instn_acct_id            VARCHAR2(255 CHAR),
 send_branch_id                INTEGER,
 send_oes_org_id               NUMBER(22), 
 send_kgrko_nm                 VARCHAR2(255 CHAR),
 send_kgrko_id                 VARCHAR2(4 CHAR),
 benef_nm                      VARCHAR2(2000 CHAR),
 benef_inn_nb                  VARCHAR2(255 CHAR),
 benef_kpp_nb                  VARCHAR2(255 CHAR),
 benef_acct_nb                 VARCHAR2(255 CHAR),
 benef_own_fl                  VARCHAR2(1 CHAR),
 credit_cd                     VARCHAR2(255 CHAR),
 benef_addr_tx                 VARCHAR2(2000 CHAR),
 benef_iddoc_tx                VARCHAR2(2000 CHAR),
 benef_reg_dt                  DATE,
 benef_goz_op_cd               VARCHAR2(64 CHAR),
 rcv_instn_nm                  VARCHAR2(2000 CHAR),
 rcv_instn_id                  VARCHAR2(255 CHAR),
 rcv_instn_cntry_cd            VARCHAR2(3 CHAR),
 rcv_instn_acct_id             VARCHAR2(255 CHAR),
 rcv_branch_id                 INTEGER,
 rcv_oes_org_id                NUMBER(22), 
 rcv_kgrko_nm                  VARCHAR2(255 CHAR),
 rcv_kgrko_id                  VARCHAR2(4 CHAR),
 trxn_doc_id                   VARCHAR2(255 CHAR),
 trxn_doc_dt                   DATE,
 trxn_conv_crncy_cd            VARCHAR2(3 CHAR),
 trxn_conv_am                  NUMBER,
 trxn_conv_base_am             NUMBER,
 trxn_cash_symbols_tx          VARCHAR2(2000 CHAR),
 bank_card_id_nb               NUMBER,
 bank_card_trxn_dt             DATE,
 merchant_no                   VARCHAR2(255 CHAR),
 atm_id                        VARCHAR2(255 CHAR),
 tb_id                         NUMBER(22),
 osb_id                        NUMBER(22),
 src_sys_cd                    VARCHAR2(3 CHAR),
 src_change_dt                 DATE, 
 src_user_nm                   VARCHAR2(255 CHAR),
 src_cd                        VARCHAR2(255 CHAR),
 canceled_fl                   VARCHAR2(1 CHAR),
 data_dump_dt                  DATE
)
/

CREATE OR REPLACE TYPE BUSINESS.RF_TAB_TRXN AS TABLE OF BUSINESS.RF_TYPE_TRXN
/

CREATE OR REPLACE 
FUNCTION business.rfv_trxn
(
  par_op_cat_cd        VARCHAR2,         -- ��������� ��������: WT - �����������, CT - ��������, BOT - ����������
  par_trxn_seq_id      NUMBER,           -- ID ��������
  par_arh_dump_dt      DATE DEFAULT null -- ���� �������� ������, ���� null - ������������ ������� ������ ��������
)
return BUSINESS.RF_TAB_TRXN pipelined is

  var_row     BUSINESS.RF_TYPE_TRXN := BUSINESS.RF_TYPE_TRXN(null,null,null,null,null,null,null,null,null,null,
                                                             null,null,null,null,null,null,null,null,null,null,
                                                             null,null,null,null,null,null,null,null,null,null,
                                                             null,null,null,null,null,null,null,null,null,null,
                                                             null,null,null,null,null,null,null,null,null,null,
                                                             null,null,null,null,null,null,null,null,null,null,
                                                             null,null,null,null,null,null,null);
BEGIN
  if par_trxn_seq_id is null or par_op_cat_cd not in ('WT', 'CT', 'BOT') Then
    return;
  end if;
     
  if par_op_cat_cd = 'WT' Then
    
    FOR r IN (SELECT /*+ ORDERED USE_NL(orig benef orig_addr benef_addr orig_iddoc benef_iddoc orig_acct orig_tb orig_tb_cust benef_acct benef_tb benef_tb_cust trxe send_org rcv_org)*/
                    'WT' as op_cat_cd,
                    to_char(null) as dbt_cdt_cd, 
                    wt.fo_trxn_seq_id as trxn_seq_id,
                    wt.trxn_exctn_dt as trxn_dt,
                    wt.rf_branch_id as branch_id,
                    to_char(null) as org_intrl_id,
                    wt.rf_trxn_crncy_am as trxn_crncy_am,
                    wt.trxn_base_am as trxn_base_am,
                    wt.rf_trxn_crncy_cd as trxn_crncy_cd,
                    wt.rf_trxn_crncy_rate_am as trxn_crncy_rate_am, 
                    wt.rf_trxn_crncy_old_rate_fl as trxn_crncy_old_rate_fl, 
                    wt.orig_to_benef_instr_tx as trxn_desc,
                    wt.trxn_type1_cd as nsi_op_id,
                    nvl(trim(wt.orig_nm||' '||wt.rf_orig_first_nm||' '||wt.rf_orig_midl_nm), orig.full_nm)||
                      nvl2(wt.rf_orig_add_info_tx, ' ('||wt.rf_orig_add_info_tx||')', null)  as orig_nm,
                    nvl(wt.rf_orig_inn_nb, orig.tax_id) as orig_inn_nb,
                    nvl(wt.rf_orig_kpp_nb, orig.rf_kpp_nb) as orig_kpp_nb,
                    nullif(wt.rf_orig_acct_nb, '-1') as orig_acct_nb,
                    case when wt.rf_orig_acct_seq_id <> -1 
                         then 'Y'
                    end as orig_own_fl,
                    wt.rf_debit_cd as debit_cd,
                    case when wt.src_sys_cd = 'GAT' and orig_addr.addr_tx is null
                         then business.rf_pkg_util.format_address_text('GAT', nvl(wt.rf_orig_reg_addr_tx, wt.rf_orig_fact_addr_tx)) 
                         else coalesce(orig_addr.addr_tx, wt.rf_orig_reg_addr_tx, wt.rf_orig_fact_addr_tx)
                    end as orig_addr_tx,       
                    coalesce(business.rf_pkg_util.format_iddoc_text(wt.rf_orig_doc_type_cd, wt.rf_orig_doc_series_id, wt.rf_orig_doc_no_id,
                                                                    wt.rf_orig_doc_issued_by_tx, wt.rf_orig_doc_dpt_cd, wt.rf_orig_doc_issue_dt), 
                             orig_iddoc.iddoc_tx) as orig_iddoc_tx,
                    nvl(wt.rf_orig_reg_dt, orig.rf_reg_dt) as orig_reg_dt, 
                    wt.rf_orig_goz_op_cd as orig_goz_op_cd,        
                    coalesce(wt.send_instn_nm, orig_tb_cust.full_nm, orig_tb.org_nm) as send_instn_nm,
                    coalesce(wt.send_instn_id, orig_tb_cust.rf_bic_nb, orig_tb.cstm_2_tx) as send_instn_id,
                    coalesce(wt.rf_send_instn_cntry_cd,
                             case when regexp_like(trim(wt.send_instn_id),'^04\d{7}$') or 
                                       orig_tb.rowid is not null 
                                  then 'RU' 
                                  when length(trim(wt.send_instn_id)) in (8, 11) 
                                  then substr(trim(wt.send_instn_id),5,2)
                             end) as send_instn_cntry_cd,
                    coalesce(wt.send_instn_acct_id, orig_tb.cstm_3_tx) as send_instn_acct_id,
                    orig_acct.rf_branch_id  as send_branch_id,
                    send_org.oes_321_org_id as send_oes_org_id,
                    send_org.tb_name        as send_kgrko_nm,
                    case when send_org.oes_321_org_id is not null
                         then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                             '1', nullif(send_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                    end as send_kgrko_id,                    
                    nvl(trim(wt.benef_nm||' '||wt.rf_benef_first_nm||' '||wt.rf_benef_midl_nm), benef.full_nm)||
                      nvl2(wt.rf_benef_add_info_tx, ' ('||wt.rf_benef_add_info_tx||')', null) as benef_nm,
                    nvl(wt.rf_benef_inn_nb, benef.tax_id) as benef_inn_nb,
                    nvl(wt.rf_benef_kpp_nb, benef.rf_kpp_nb) as benef_kpp_nb,
                    nullif(wt.rf_benef_acct_nb, '-1') as benef_acct_nb,
                    case when wt.rf_benef_acct_seq_id <> -1 
                         then 'Y'
                    end as benef_own_fl,
                    wt.rf_credit_cd as credit_cd,
                    case when wt.src_sys_cd = 'GAT' and benef_addr.addr_tx is null
                         then business.rf_pkg_util.format_address_text('GAT', nvl(wt.rf_benef_reg_addr_tx, wt.rf_benef_fact_addr_tx)) 
                         else coalesce(benef_addr.addr_tx, wt.rf_benef_reg_addr_tx, wt.rf_benef_fact_addr_tx)
                    end as benef_addr_tx,
                    coalesce(business.rf_pkg_util.format_iddoc_text(wt.rf_benef_doc_type_cd, wt.rf_benef_doc_series_id, wt.rf_benef_doc_no_id,
                                                                    wt.rf_benef_doc_issued_by_tx, wt.rf_benef_doc_dpt_cd, wt.rf_benef_doc_issue_dt), 
                             benef_iddoc.iddoc_tx) as benef_iddoc_tx,
                    nvl(wt.rf_benef_reg_dt, benef.rf_reg_dt) as benef_reg_dt,         
                    wt.rf_benef_goz_op_cd as benef_goz_op_cd,        
                    coalesce(wt.rcv_instn_nm, benef_tb_cust.full_nm, benef_tb.org_nm) as rcv_instn_nm,
                    coalesce(wt.rcv_instn_id, benef_tb_cust.rf_bic_nb, benef_tb.cstm_2_tx) as rcv_instn_id,
                    coalesce(wt.rf_rcv_instn_cntry_cd,
                             case when regexp_like(trim(wt.rcv_instn_id),'^04\d{7}$') or 
                                       benef_tb.rowid is not null  
                                  then 'RU' 
                                  when length(trim(wt.rcv_instn_id)) in (8, 11) 
                                  then substr(trim(wt.rcv_instn_id),5,2)
                             end) as rcv_instn_cntry_cd,
                    coalesce(wt.rcv_instn_acct_id, benef_tb.cstm_3_tx) as rcv_instn_acct_id,
                    benef_acct.rf_branch_id as rcv_branch_id,
                    rcv_org.oes_321_org_id  as rcv_oes_org_id,
                    rcv_org.tb_name         as rcv_kgrko_nm,
                    case when rcv_org.oes_321_org_id is not null
                         then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                            '1', nullif(rcv_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                    end as rcv_kgrko_id,
                    wt.rf_trxn_doc_id as trxn_doc_id,
                    wt.rf_trxn_doc_dt as trxn_doc_dt,
                    to_char(null) as trxn_conv_crncy_cd,
                    to_number(null) as trxn_conv_am,
                    to_number(null) as trxn_conv_base_am,
                    to_char(null) as trxn_cash_symbols_tx,
                    wt.bank_card_id_nb as bank_card_id_nb,
                    wt.cstm_1_dt as bank_card_trxn_dt,
                    wt.mantas_trxn_chanl_dtl_1_tx as merchant_no,
                    wt.mantas_trxn_chanl_dtl_2_tx as atm_id,
                    trunc(wt.rf_branch_id / 100000) as tb_id,
                    wt.rf_branch_id - round(wt.rf_branch_id, -5) as osb_id,
                    wt.src_sys_cd,
                    wt.rf_src_change_dt as src_change_dt,
                    wt.rf_src_user_nm as src_user_nm,
                    wt.rf_src_cd as src_cd,
                    wt.rf_canceled_fl as canceled_fl,
                    wt.data_dump_dt as data_dump_dt
               FROM (select /*+ INDEX(wire_trxn PK_WIRE_TRXN)*/
                            fo_trxn_seq_id, trxn_exctn_dt, rf_branch_id, rf_trxn_crncy_am, trxn_base_am, rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, rf_trxn_crncy_old_rate_fl, 
                            orig_to_benef_instr_tx, trxn_type1_cd, 
                            rf_orig_cust_seq_id, orig_nm, rf_orig_first_nm, rf_orig_midl_nm, rf_orig_add_info_tx, rf_orig_inn_nb, rf_orig_kpp_nb, 
                            rf_orig_acct_nb, rf_orig_acct_seq_id, rf_debit_cd,
                            rf_orig_reg_addr_tx, rf_orig_fact_addr_tx, 
                            rf_orig_doc_type_cd, rf_orig_doc_series_id, rf_orig_doc_no_id, rf_orig_doc_issued_by_tx, rf_orig_doc_dpt_cd, rf_orig_doc_issue_dt, 
                            rf_orig_reg_dt, rf_orig_goz_op_cd,
                            send_instn_nm, send_instn_id, rf_send_instn_cntry_cd, send_instn_acct_id, 
                            rf_benef_cust_seq_id, benef_nm, rf_benef_first_nm, rf_benef_midl_nm, rf_benef_add_info_tx, rf_benef_inn_nb, rf_benef_kpp_nb, 
                            rf_benef_acct_nb, rf_benef_acct_seq_id, rf_credit_cd,
                            rf_benef_reg_addr_tx, rf_benef_fact_addr_tx, 
                            rf_benef_doc_type_cd, rf_benef_doc_series_id, rf_benef_doc_no_id, rf_benef_doc_issued_by_tx, rf_benef_doc_dpt_cd, rf_benef_doc_issue_dt, 
                            rf_benef_reg_dt, rf_benef_goz_op_cd,
                            rcv_instn_nm, rcv_instn_id, rf_rcv_instn_cntry_cd, rcv_instn_acct_id, 
                            rf_trxn_doc_id, rf_trxn_doc_dt, bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx, 
                            src_sys_cd, rf_src_change_dt, rf_src_user_nm, rf_src_cd, rf_canceled_fl, data_dump_dt
                       from business.wire_trxn
                      where fo_trxn_seq_id = par_trxn_seq_id and
                            not exists(select null 
                                         from business.wire_trxn_arh
                                        where fo_trxn_seq_id = par_trxn_seq_id and
                                              arh_dump_dt = par_arh_dump_dt)
                     union all
                     select /*+ INDEX(wire_trxn_arh PK_WIRE_TRXN_ARH)*/
                            fo_trxn_seq_id, trxn_exctn_dt, rf_branch_id, rf_trxn_crncy_am, trxn_base_am, rf_trxn_crncy_cd, rf_trxn_crncy_rate_am, rf_trxn_crncy_old_rate_fl, 
                            orig_to_benef_instr_tx, trxn_type1_cd, 
                            rf_orig_cust_seq_id, orig_nm, rf_orig_first_nm, rf_orig_midl_nm, rf_orig_add_info_tx, rf_orig_inn_nb, rf_orig_kpp_nb, 
                            rf_orig_acct_nb, rf_orig_acct_seq_id, rf_debit_cd,
                            rf_orig_reg_addr_tx, rf_orig_fact_addr_tx, 
                            rf_orig_doc_type_cd, rf_orig_doc_series_id, rf_orig_doc_no_id, rf_orig_doc_issued_by_tx, rf_orig_doc_dpt_cd, rf_orig_doc_issue_dt, 
                            rf_orig_reg_dt, rf_orig_goz_op_cd,
                            send_instn_nm, send_instn_id, rf_send_instn_cntry_cd, send_instn_acct_id, 
                            rf_benef_cust_seq_id, benef_nm, rf_benef_first_nm, rf_benef_midl_nm, rf_benef_add_info_tx, rf_benef_inn_nb, rf_benef_kpp_nb, 
                            rf_benef_acct_nb, rf_benef_acct_seq_id, rf_credit_cd,
                            rf_benef_reg_addr_tx, rf_benef_fact_addr_tx, 
                            rf_benef_doc_type_cd, rf_benef_doc_series_id, rf_benef_doc_no_id, rf_benef_doc_issued_by_tx, rf_benef_doc_dpt_cd, rf_benef_doc_issue_dt, 
                            rf_benef_reg_dt, rf_benef_goz_op_cd,
                            rcv_instn_nm, rcv_instn_id, rf_rcv_instn_cntry_cd, rcv_instn_acct_id, 
                            rf_trxn_doc_id, rf_trxn_doc_dt, bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx, 
                            src_sys_cd, rf_src_change_dt, rf_src_user_nm, rf_src_cd, rf_canceled_fl, data_dump_dt
                       from business.wire_trxn_arh
                      where fo_trxn_seq_id = par_trxn_seq_id and
                            arh_dump_dt = par_arh_dump_dt) wt
                    LEFT JOIN business.cust orig ON orig.cust_seq_id = wt.rf_orig_cust_seq_id
                    LEFT JOIN business.cust benef ON benef.cust_seq_id = wt.rf_benef_cust_seq_id
                    LEFT JOIN (select business.rf_pkg_util.get_addr_text(cust_addr_seq_id) as addr_tx
                                 from business.cust_addr) orig_addr 
                           ON orig_addr.rowid = coalesce(business.rf_pkg_util.get_primary_cust_addr(wt.rf_orig_cust_seq_id, 'L'), -- ����� ����������� 
                                                         business.rf_pkg_util.get_primary_cust_addr(wt.rf_orig_cust_seq_id, 'H')) -- ���� �� �����, �� ����� �����������      
                    LEFT JOIN (select business.rf_pkg_util.get_addr_text(cust_addr_seq_id) as addr_tx
                                 from business.cust_addr) benef_addr 
                           ON benef_addr.rowid = coalesce(business.rf_pkg_util.get_primary_cust_addr(wt.rf_benef_cust_seq_id, 'L'), -- ����� ����������� 
                                                          business.rf_pkg_util.get_primary_cust_addr(wt.rf_benef_cust_seq_id, 'H')) -- ���� �� �����, �� ����� �����������      
                    LEFT JOIN (select business.rf_pkg_util.get_iddoc_text(cust_doc_seq_id) iddoc_tx
                                 from business.rf_cust_id_doc) orig_iddoc
                           ON orig_iddoc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(wt.rf_orig_cust_seq_id, wt.rf_orig_doc_type_cd, wt.rf_orig_doc_series_id, wt.rf_orig_doc_no_id)       
                    LEFT JOIN (select business.rf_pkg_util.get_iddoc_text(cust_doc_seq_id) iddoc_tx
                                 from business.rf_cust_id_doc) benef_iddoc
                           ON benef_iddoc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(wt.rf_benef_cust_seq_id, wt.rf_benef_doc_type_cd, wt.rf_benef_doc_series_id, wt.rf_benef_doc_no_id)
                    LEFT JOIN business.acct orig_acct ON orig_acct.rowid = business.rf_pkg_util.get_acct2(wt.rf_orig_acct_seq_id, wt.rf_orig_acct_nb, wt.send_instn_id, 
                                                                                                          wt.rf_orig_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)                    
                    LEFT JOIN business.org  orig_tb ON orig_tb.org_intrl_id = to_char(trunc(orig_acct.rf_branch_id/100000))
                    LEFT JOIN business.cust orig_tb_cust ON orig_tb_cust.cust_seq_id = orig_tb.cstm_1_rl       
                    LEFT JOIN business.acct benef_acct ON benef_acct.rowid = business.rf_pkg_util.get_acct2(wt.rf_benef_acct_seq_id, wt.rf_benef_acct_nb, wt.rcv_instn_id, 
                                                                                                            wt.rf_benef_cust_seq_id, wt.trxn_exctn_dt, wt.src_sys_cd)                    
                    LEFT JOIN business.org  benef_tb ON benef_tb.org_intrl_id = to_char(trunc(benef_acct.rf_branch_id/100000))
                    LEFT JOIN business.cust benef_tb_cust ON benef_tb_cust.cust_seq_id = benef_tb.cstm_1_rl       
                    LEFT JOIN business.rf_trxn_extra trxe ON trxe.op_cat_cd = 'WT' and trxe.fo_trxn_seq_id = wt.fo_trxn_seq_id 
                    LEFT JOIN mantas.rf_oes_321_org send_org ON send_org.oes_321_org_id = case when trxe.rowid is not null 
                                                                                               then trxe.send_oes_org_id
                                                                                               else mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(orig_acct.rf_branch_id/100000),  orig_acct.rf_branch_id  - round(orig_acct.rf_branch_id, -5),  trunc(wt.trxn_exctn_dt))
                                                                                          end       
                    LEFT JOIN mantas.rf_oes_321_org rcv_org  ON rcv_org.oes_321_org_id  = case when trxe.rowid is not null 
                                                                                               then trxe.rcv_oes_org_id
                                                                                               else mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(benef_acct.rf_branch_id/100000), benef_acct.rf_branch_id - round(benef_acct.rf_branch_id, -5), trunc(wt.trxn_exctn_dt))
                                                                                          end       
                    ) LOOP       
      var_row.op_cat_cd := r.op_cat_cd;
      var_row.dbt_cdt_cd := r.dbt_cdt_cd;
      var_row.trxn_seq_id := r.trxn_seq_id;
      var_row.trxn_dt := r.trxn_dt;
      var_row.branch_id := r.branch_id;
      var_row.org_intrl_id := r.org_intrl_id;
      var_row.trxn_crncy_am := r.trxn_crncy_am;
      var_row.trxn_base_am := r.trxn_base_am;
      var_row.trxn_crncy_cd := r.trxn_crncy_cd;
      var_row.trxn_crncy_rate_am := r.trxn_crncy_rate_am;
      var_row.trxn_crncy_old_rate_fl := r.trxn_crncy_old_rate_fl;
      var_row.trxn_desc := r.trxn_desc;
      var_row.nsi_op_id := r.nsi_op_id;
      var_row.orig_nm := r.orig_nm;
      var_row.orig_inn_nb := r.orig_inn_nb;
      var_row.orig_kpp_nb := r.orig_kpp_nb;
      var_row.orig_acct_nb := r.orig_acct_nb;
      var_row.orig_own_fl := r.orig_own_fl;
      var_row.debit_cd := r.debit_cd;
      var_row.orig_addr_tx := r.orig_addr_tx;
      var_row.orig_iddoc_tx := r.orig_iddoc_tx;
      var_row.orig_reg_dt := r.orig_reg_dt;      
      var_row.orig_goz_op_cd := r.orig_goz_op_cd;
      var_row.send_instn_nm := r.send_instn_nm;
      var_row.send_instn_id := r.send_instn_id;
      var_row.send_instn_cntry_cd := r.send_instn_cntry_cd;
      var_row.send_instn_acct_id := r.send_instn_acct_id;
      var_row.send_branch_id := r.send_branch_id;
      var_row.send_oes_org_id := r.send_oes_org_id;
      var_row.send_kgrko_nm := r.send_kgrko_nm;
      var_row.send_kgrko_id := r.send_kgrko_id;
      var_row.benef_nm := r.benef_nm;
      var_row.benef_inn_nb := r.benef_inn_nb;
      var_row.benef_kpp_nb := r.benef_kpp_nb;
      var_row.benef_acct_nb := r.benef_acct_nb;
      var_row.benef_own_fl := r.benef_own_fl;
      var_row.credit_cd := r.credit_cd;
      var_row.benef_addr_tx := r.benef_addr_tx;
      var_row.benef_iddoc_tx := r.benef_iddoc_tx;
      var_row.benef_reg_dt := r.benef_reg_dt;
      var_row.benef_goz_op_cd := r.benef_goz_op_cd;
      var_row.rcv_instn_nm := r.rcv_instn_nm;
      var_row.rcv_instn_id := r.rcv_instn_id;
      var_row.rcv_instn_cntry_cd := r.rcv_instn_cntry_cd;
      var_row.rcv_instn_acct_id := r.rcv_instn_acct_id;
      var_row.rcv_branch_id := r.rcv_branch_id;
      var_row.rcv_oes_org_id := r.rcv_oes_org_id;
      var_row.rcv_kgrko_nm := r.rcv_kgrko_nm;
      var_row.rcv_kgrko_id := r.rcv_kgrko_id;
      var_row.trxn_doc_id := r.trxn_doc_id;
      var_row.trxn_doc_dt := r.trxn_doc_dt;
      var_row.trxn_conv_crncy_cd := r.trxn_conv_crncy_cd;
      var_row.trxn_conv_am := r.trxn_conv_am;
      var_row.trxn_conv_base_am := r.trxn_conv_base_am;
      var_row.trxn_cash_symbols_tx := r.trxn_cash_symbols_tx;
      var_row.bank_card_id_nb := r.bank_card_id_nb;
      var_row.bank_card_trxn_dt := r.bank_card_trxn_dt;
      var_row.merchant_no := r.merchant_no;
      var_row.atm_id := r.atm_id;
      var_row.tb_id := r.tb_id;
      var_row.osb_id := r.osb_id;
      var_row.src_sys_cd := r.src_sys_cd;
      var_row.src_change_dt := r.src_change_dt;
      var_row.src_user_nm := r.src_user_nm;
      var_row.src_cd := r.src_cd;
      var_row.canceled_fl := r.canceled_fl;
      var_row.data_dump_dt := r.data_dump_dt;
      
      PIPE ROW(var_row);
    END LOOP;
  
  elsif  par_op_cat_cd = 'CT' Then

    FOR r IN (SELECT /*+ ORDERED USE_NL(cust cust_addr cust_iddoc acct tb tb_cust trxe send_org rcv_org)*/
                     'CT' as op_cat_cd,
                     ct.dbt_cdt_cd as dbt_cdt_cd, 
                     ct.fo_trxn_seq_id as trxn_seq_id,
                     ct.trxn_exctn_dt as trxn_dt,
                     ct.rf_branch_id as branch_id,
                     ct.trxn_brnch_id as org_intrl_id,
                     ct.rf_trxn_acct_am as trxn_crncy_am,
                     ct.trxn_base_am as trxn_base_am,
                     ct.rf_acct_crncy_cd as trxn_crncy_cd,
                     ct.rf_trxn_crncy_rate_am as trxn_crncy_rate_am, 
                     ct.rf_trxn_crncy_old_rate_fl as trxn_crncy_old_rate_fl, 
                     ct.desc_tx as trxn_desc,
                     ct.trxn_type1_cd as nsi_op_id,
                     case when ct.dbt_cdt_cd = 'D' 
                          then nvl(trim(ct.rf_cust_nm||' '||ct.rf_cust_first_nm||' '||ct.rf_cust_midl_nm), cust.full_nm)
                     end as orig_nm,
                     case when ct.dbt_cdt_cd = 'D' 
                          then nvl(ct.rf_cust_inn_nb, cust.tax_id)
                     end as orig_inn_nb,
                     case when ct.dbt_cdt_cd = 'D' 
                          then nvl(ct.rf_cust_kpp_nb, cust.rf_kpp_nb)
                     end as orig_kpp_nb,
                     case when ct.dbt_cdt_cd = 'D' 
                          then nullif(ct.rf_acct_nb, '-1')
                     end as orig_acct_nb,
                     case when ct.dbt_cdt_cd = 'D' 
                          then case when ct.rf_acct_seq_id <> -1 
                                    then 'Y'
                               end
                     end as orig_own_fl,
                     ct.rf_debit_cd as debit_cd,
                     case when ct.dbt_cdt_cd = 'D' 
                          then case when cust_addr.addr_tx is null and ct.src_sys_cd = 'GAT'
                                    then business.rf_pkg_util.format_address_text('GAT', nvl(ct.rf_cust_reg_addr_tx, ct.rf_cust_fact_addr_tx)) 
                                    else coalesce(cust_addr.addr_tx, ct.rf_cust_reg_addr_tx, ct.rf_cust_fact_addr_tx)
                               end        
                     end as orig_addr_tx,
                     case when ct.dbt_cdt_cd = 'D' 
                          then coalesce(business.rf_pkg_util.format_iddoc_text(ct.rf_cust_doc_type_cd, ct.rf_cust_doc_series_id, ct.rf_cust_doc_no_id,
                                                                               ct.rf_cust_doc_issued_by_tx, ct.rf_cust_doc_dpt_cd, ct.rf_cust_doc_issue_dt), 
                                        cust_iddoc.iddoc_tx)
                     end as orig_iddoc_tx,                     
                     case when ct.dbt_cdt_cd = 'D' 
                          then nvl(ct.rf_cust_reg_dt, cust.rf_reg_dt)                     
                     end as orig_reg_dt,                     
                     case when ct.dbt_cdt_cd = 'D' 
                          then ct.rf_acct_goz_op_cd                    
                     end as orig_goz_op_cd,                     
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' 
                          then nvl(tb_cust.full_nm, tb.org_nm)
                     end as send_instn_nm,
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' 
                          then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx) 
                     end as send_instn_id,
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' 
                          then 'RU' 
                     end as send_instn_cntry_cd,
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'D' 
                          then tb.cstm_3_tx 
                     end as send_instn_acct_id,                     
                     case when ct.dbt_cdt_cd = 'D' 
                          then acct.rf_branch_id
                     end as send_branch_id,
                     send_org.oes_321_org_id as send_oes_org_id,
                     send_org.tb_name        as send_kgrko_nm,
                     case when send_org.oes_321_org_id is not null 
                          then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                              '1', nullif(send_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                     end as send_kgrko_id,                     
                     case when ct.dbt_cdt_cd = 'C' 
                          then nvl(trim(ct.rf_cust_nm||' '||ct.rf_cust_first_nm||' '||ct.rf_cust_midl_nm), cust.full_nm)
                     end as benef_nm,
                     case when ct.dbt_cdt_cd = 'C' 
                          then nvl(ct.rf_cust_inn_nb, cust.tax_id)
                     end as benef_inn_nb,
                     case when ct.dbt_cdt_cd = 'C' 
                          then nvl(ct.rf_cust_kpp_nb, cust.rf_kpp_nb)
                     end as benef_kpp_nb,
                     case when ct.dbt_cdt_cd = 'C' 
                          then nullif(ct.rf_acct_nb, '-1')
                     end as benef_acct_nb,
                     case when ct.dbt_cdt_cd = 'C' 
                          then case when ct.rf_acct_seq_id <> -1 
                                    then 'Y'
                               end
                     end as benef_own_fl,
                     ct.rf_credit_cd as credit_cd,
                     case when ct.dbt_cdt_cd = 'C' 
                          then case when cust_addr.addr_tx is null and ct.src_sys_cd = 'GAT'
                                    then business.rf_pkg_util.format_address_text('GAT', nvl(ct.rf_cust_reg_addr_tx, ct.rf_cust_fact_addr_tx)) 
                                    else coalesce(cust_addr.addr_tx, ct.rf_cust_reg_addr_tx, ct.rf_cust_fact_addr_tx)
                               end        
                     end as benef_addr_tx,
                     case when ct.dbt_cdt_cd = 'C' 
                          then coalesce(business.rf_pkg_util.format_iddoc_text(ct.rf_cust_doc_type_cd, ct.rf_cust_doc_series_id, ct.rf_cust_doc_no_id,
                                                                               ct.rf_cust_doc_issued_by_tx, ct.rf_cust_doc_dpt_cd, ct.rf_cust_doc_issue_dt), 
                                        cust_iddoc.iddoc_tx)
                     end as benef_iddoc_tx,
                     case when ct.dbt_cdt_cd = 'C' 
                          then nvl(ct.rf_cust_reg_dt, cust.rf_reg_dt)                     
                     end as benef_reg_dt,                     
                     case when ct.dbt_cdt_cd = 'C' 
                          then ct.rf_acct_goz_op_cd                    
                     end as benef_goz_op_cd,                     
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' 
                          then nvl(tb_cust.full_nm, tb.org_nm) 
                     end as rcv_instn_nm,
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' 
                          then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx)
                     end as rcv_instn_id,
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' 
                          then 'RU' 
                     end as rcv_instn_cntry_cd,
                     case when ct.rf_acct_seq_id <> -1 and ct.dbt_cdt_cd = 'C' 
                          then tb.cstm_3_tx 
                     end as rcv_instn_acct_id,
                     case when ct.dbt_cdt_cd = 'C' 
                          then acct.rf_branch_id
                     end as rcv_branch_id,
                     rcv_org.oes_321_org_id as rcv_oes_org_id,
                     rcv_org.tb_name        as rcv_kgrko_nm,
                     case when rcv_org.oes_321_org_id is not null 
                          then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                             '1', nullif(rcv_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                     end as rcv_kgrko_id,
                     ct.rf_trxn_doc_id as trxn_doc_id,
                     ct.rf_trxn_doc_dt as trxn_doc_dt,
                     ct.rf_conv_crncy_cd as trxn_conv_crncy_cd,
                     ct.rf_trxn_conv_am as trxn_conv_am,
                     ct.rf_trxn_conv_base_am as trxn_conv_base_am,
                     ct.rf_cash_symbols_tx as trxn_cash_symbols_tx,
                     ct.bank_card_id_nb as bank_card_id_nb,
                     ct.cstm_1_dt as bank_card_trxn_dt,
                     ct.mantas_trxn_chanl_dtl_1_tx as merchant_no,
                     ct.mantas_trxn_chanl_dtl_2_tx as atm_id,
                     trunc(ct.rf_branch_id / 100000) as tb_id,
                     ct.rf_branch_id - round(ct.rf_branch_id, -5) as osb_id,
                     ct.src_sys_cd,
                     ct.rf_src_change_dt as src_change_dt,
                     ct.rf_src_user_nm as src_user_nm,
                     ct.rf_src_cd as src_cd,
                     ct.rf_canceled_fl as canceled_fl,
                     ct.data_dump_dt as data_dump_dt
                FROM (select /*+ INDEX(cash_trxn PK_CASH_TRXN)*/
                             fo_trxn_seq_id, dbt_cdt_cd, trxn_exctn_dt, rf_branch_id, trxn_brnch_id, rf_trxn_acct_am, trxn_base_am, rf_acct_crncy_cd, rf_trxn_crncy_rate_am, rf_trxn_crncy_old_rate_fl, 
                             desc_tx, trxn_type1_cd, 
                             rf_cust_seq_id, rf_cust_nm, rf_cust_first_nm, rf_cust_midl_nm, rf_cust_inn_nb, rf_cust_kpp_nb,
                             rf_acct_nb, rf_acct_seq_id, rf_acct_goz_op_cd, rf_debit_cd, 
                             rf_cust_reg_addr_tx, rf_cust_fact_addr_tx, 
                             rf_cust_doc_type_cd, rf_cust_doc_series_id, rf_cust_doc_no_id, rf_cust_doc_issued_by_tx, rf_cust_doc_dpt_cd, rf_cust_doc_issue_dt, 
                             rf_cust_reg_dt, rf_credit_cd, rf_trxn_doc_id, rf_trxn_doc_dt, rf_conv_crncy_cd, rf_trxn_conv_am, rf_trxn_conv_base_am, rf_cash_symbols_tx, 
                             bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx, 
                             src_sys_cd, rf_src_change_dt, rf_src_user_nm, rf_src_cd, rf_canceled_fl, data_dump_dt
                        from business.cash_trxn
                       where fo_trxn_seq_id = par_trxn_seq_id and
                             not exists(select null 
                                          from business.cash_trxn_arh
                                         where fo_trxn_seq_id = par_trxn_seq_id and
                                               arh_dump_dt = par_arh_dump_dt)
                      union all
                      select/*+ INDEX(cash_trxn_arh PK_CASH_TRXN_ARH)*/
                             fo_trxn_seq_id, dbt_cdt_cd, trxn_exctn_dt, rf_branch_id, trxn_brnch_id, rf_trxn_acct_am, trxn_base_am, rf_acct_crncy_cd, rf_trxn_crncy_rate_am, rf_trxn_crncy_old_rate_fl, 
                             desc_tx, trxn_type1_cd, 
                             rf_cust_seq_id, rf_cust_nm, rf_cust_first_nm, rf_cust_midl_nm, rf_cust_inn_nb, rf_cust_kpp_nb,
                             rf_acct_nb, rf_acct_seq_id, rf_acct_goz_op_cd, rf_debit_cd, 
                             rf_cust_reg_addr_tx, rf_cust_fact_addr_tx, 
                             rf_cust_doc_type_cd, rf_cust_doc_series_id, rf_cust_doc_no_id, rf_cust_doc_issued_by_tx, rf_cust_doc_dpt_cd, rf_cust_doc_issue_dt, 
                             rf_cust_reg_dt, rf_credit_cd, rf_trxn_doc_id, rf_trxn_doc_dt, rf_conv_crncy_cd, rf_trxn_conv_am, rf_trxn_conv_base_am, rf_cash_symbols_tx,
                             bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx, 
                             src_sys_cd, rf_src_change_dt, rf_src_user_nm, rf_src_cd, rf_canceled_fl, data_dump_dt
                        from business.cash_trxn_arh
                       where fo_trxn_seq_id = par_trxn_seq_id and
                             arh_dump_dt = par_arh_dump_dt) ct
                     LEFT JOIN business.cust cust ON cust.cust_seq_id = ct.rf_cust_seq_id
                     LEFT JOIN (select business.rf_pkg_util.get_addr_text(cust_addr_seq_id) as addr_tx
                                  from business.cust_addr) cust_addr 
                            ON cust_addr.rowid = coalesce(business.rf_pkg_util.get_primary_cust_addr(ct.rf_cust_seq_id, 'L'), -- ����� ����������� 
                                                          business.rf_pkg_util.get_primary_cust_addr(ct.rf_cust_seq_id, 'H')) -- ���� �� �����, �� ����� �����������      
                     LEFT JOIN (select business.rf_pkg_util.get_iddoc_text(cust_doc_seq_id) iddoc_tx
                                  from business.rf_cust_id_doc) cust_iddoc
                            ON cust_iddoc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(ct.rf_cust_seq_id,ct.rf_cust_doc_type_cd, ct.rf_cust_doc_series_id, ct.rf_cust_doc_no_id)
                     LEFT JOIN business.acct acct ON acct.rowid = business.rf_pkg_util.get_acct2(ct.rf_acct_seq_id, ct.rf_acct_nb, 'SBRF', 
                                                                                                 ct.rf_cust_seq_id, ct.trxn_exctn_dt, ct.src_sys_cd)                    
                     LEFT JOIN business.org  tb ON tb.org_intrl_id = to_char(trunc(nvl(acct.rf_branch_id, ct.rf_branch_id)/100000))
                     LEFT JOIN business.cust tb_cust ON tb_cust.cust_seq_id = tb.cstm_1_rl
                     LEFT JOIN business.rf_trxn_extra trxe ON trxe.op_cat_cd = 'CT' and trxe.fo_trxn_seq_id = ct.fo_trxn_seq_id 
                     LEFT JOIN mantas.rf_oes_321_org send_org ON send_org.oes_321_org_id = case when trxe.rowid is not null 
                                                                                                then trxe.send_oes_org_id
                                                                                                when ct.dbt_cdt_cd = 'D'  
                                                                                                then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(ct.trxn_exctn_dt))
                                                                                           end       
                     LEFT JOIN mantas.rf_oes_321_org rcv_org  ON rcv_org.oes_321_org_id  = case when trxe.rowid is not null 
                                                                                                then trxe.rcv_oes_org_id
                                                                                                when ct.dbt_cdt_cd = 'C'  
                                                                                                then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(ct.trxn_exctn_dt))
                                                                                           end       
                     ) LOOP       
      var_row.op_cat_cd := r.op_cat_cd;
      var_row.dbt_cdt_cd := r.dbt_cdt_cd;
      var_row.trxn_seq_id := r.trxn_seq_id;
      var_row.trxn_dt := r.trxn_dt;
      var_row.branch_id := r.branch_id;
      var_row.org_intrl_id := r.org_intrl_id;
      var_row.trxn_crncy_am := r.trxn_crncy_am;
      var_row.trxn_base_am := r.trxn_base_am;
      var_row.trxn_crncy_cd := r.trxn_crncy_cd;
      var_row.trxn_crncy_rate_am := r.trxn_crncy_rate_am;
      var_row.trxn_crncy_old_rate_fl := r.trxn_crncy_old_rate_fl;
      var_row.trxn_desc := r.trxn_desc;
      var_row.nsi_op_id := r.nsi_op_id;
      var_row.orig_nm := r.orig_nm;
      var_row.orig_inn_nb := r.orig_inn_nb;
      var_row.orig_kpp_nb := r.orig_kpp_nb;
      var_row.orig_acct_nb := r.orig_acct_nb;
      var_row.orig_own_fl := r.orig_own_fl;
      var_row.debit_cd := r.debit_cd;
      var_row.orig_addr_tx := r.orig_addr_tx;
      var_row.orig_iddoc_tx := r.orig_iddoc_tx;
      var_row.orig_reg_dt := r.orig_reg_dt;
      var_row.orig_goz_op_cd := r.orig_goz_op_cd;
      var_row.send_instn_nm := r.send_instn_nm;
      var_row.send_instn_id := r.send_instn_id;
      var_row.send_instn_cntry_cd := r.send_instn_cntry_cd;
      var_row.send_instn_acct_id := r.send_instn_acct_id;
      var_row.send_branch_id := r.send_branch_id;
      var_row.send_oes_org_id := r.send_oes_org_id;
      var_row.send_kgrko_nm := r.send_kgrko_nm;
      var_row.send_kgrko_id := r.send_kgrko_id;
      var_row.benef_nm := r.benef_nm;
      var_row.benef_inn_nb := r.benef_inn_nb;
      var_row.benef_kpp_nb := r.benef_kpp_nb;
      var_row.benef_acct_nb := r.benef_acct_nb;
      var_row.benef_own_fl := r.benef_own_fl;
      var_row.credit_cd := r.credit_cd;
      var_row.benef_addr_tx := r.benef_addr_tx;
      var_row.benef_iddoc_tx := r.benef_iddoc_tx;
      var_row.benef_reg_dt := r.benef_reg_dt;
      var_row.benef_goz_op_cd := r.benef_goz_op_cd;
      var_row.rcv_instn_nm := r.rcv_instn_nm;
      var_row.rcv_instn_id := r.rcv_instn_id;
      var_row.rcv_instn_cntry_cd := r.rcv_instn_cntry_cd;
      var_row.rcv_instn_acct_id := r.rcv_instn_acct_id;
      var_row.rcv_branch_id := r.rcv_branch_id;
      var_row.rcv_oes_org_id := r.rcv_oes_org_id;
      var_row.rcv_kgrko_nm := r.rcv_kgrko_nm;
      var_row.rcv_kgrko_id := r.rcv_kgrko_id;
      var_row.trxn_doc_id := r.trxn_doc_id;
      var_row.trxn_doc_dt := r.trxn_doc_dt;
      var_row.trxn_conv_crncy_cd := r.trxn_conv_crncy_cd;
      var_row.trxn_conv_am := r.trxn_conv_am;
      var_row.trxn_conv_base_am := r.trxn_conv_base_am;
      var_row.trxn_cash_symbols_tx := r.trxn_cash_symbols_tx;
      var_row.bank_card_id_nb := r.bank_card_id_nb;
      var_row.bank_card_trxn_dt := r.bank_card_trxn_dt;
      var_row.merchant_no := r.merchant_no;
      var_row.atm_id := r.atm_id;
      var_row.tb_id := r.tb_id;
      var_row.osb_id := r.osb_id;
      var_row.src_sys_cd := r.src_sys_cd;
      var_row.src_change_dt := r.src_change_dt;
      var_row.src_user_nm := r.src_user_nm;
      var_row.src_cd := r.src_cd;
      var_row.canceled_fl := r.canceled_fl;
      var_row.data_dump_dt := r.data_dump_dt;
      
      PIPE ROW(var_row);
    END LOOP;

  elsif  par_op_cat_cd = 'BOT' Then

    FOR r IN (SELECT /*+ ORDERED USE_NL(acct cust cust_addr cust_iddoc tb tb_cust trxe send_org rcv_org)*/
                     'BOT' as op_cat_cd,
                     bot.dbt_cdt_cd as dbt_cdt_cd, 
                     bot.bo_trxn_seq_id as trxn_seq_id,
                     bot.exctn_dt as trxn_dt,
                     bot.rf_branch_id as branch_id,
                     to_char(null) as org_intrl_id,
                     bot.trxn_rptng_am as trxn_crncy_am,
                     bot.trxn_base_am as trxn_base_am,
                     bot.rptng_crncy_cd as trxn_crncy_cd,
                     null as trxn_crncy_rate_am, 
                     null as trxn_crncy_old_rate_fl, 
                     bot.dscr_tx as trxn_desc,
                     bot.trxn_type1_cd as nsi_op_id,
                     case when bot.dbt_cdt_cd = 'D' 
                          then cust.full_nm
                     end as orig_nm,
                     case when bot.dbt_cdt_cd = 'D' 
                          then cust.tax_id
                     end as orig_inn_nb,
                     case when bot.dbt_cdt_cd = 'D' 
                          then cust.rf_kpp_nb
                     end as orig_kpp_nb,
                     case when bot.dbt_cdt_cd = 'D' 
                          then nullif(acct.alt_acct_id, '-1')
                     end as orig_acct_nb,
                     case when bot.dbt_cdt_cd = 'D' 
                          then 'Y'
                     end as orig_own_fl,
                     bot.rf_debit_cd as debit_cd,
                     case when bot.dbt_cdt_cd = 'D' 
                          then cust_addr.addr_tx
                     end as orig_addr_tx,
                     case when bot.dbt_cdt_cd = 'D' 
                          then cust_iddoc.iddoc_tx
                     end as orig_iddoc_tx,
                     case when bot.dbt_cdt_cd = 'D' 
                          then cust.rf_reg_dt
                     end as orig_reg_dt,       
                     case when bot.dbt_cdt_cd = 'D' 
                          then bot.rf_acct_goz_op_cd                    
                     end as orig_goz_op_cd,                     
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' 
                          then nvl(tb_cust.full_nm, tb.org_nm)
                     end as send_instn_nm,
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' 
                          then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx) 
                     end as send_instn_id,
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' 
                          then 'RU' 
                     end as send_instn_cntry_cd,
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'D' 
                          then tb.cstm_3_tx 
                     end as send_instn_acct_id,
                     case when bot.dbt_cdt_cd = 'D' 
                          then acct.rf_branch_id
                     end as send_branch_id,
                     send_org.oes_321_org_id as send_oes_org_id,
                     send_org.tb_name        as send_kgrko_nm,
                     case when send_org.oes_321_org_id is not null  
                          then nvl(decode(send_org.branch_fl, '0', nullif(send_org.numbf_s, '0'), 
                                                              '1', nullif(send_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                     end as send_kgrko_id,
                     case when bot.dbt_cdt_cd = 'C' 
                          then cust.full_nm
                     end as benef_nm,
                     case when bot.dbt_cdt_cd = 'C' 
                          then cust.tax_id
                     end as benef_inn_nb,
                     case when bot.dbt_cdt_cd = 'C' 
                          then cust.rf_kpp_nb
                     end as benef_kpp_nb,
                     case when bot.dbt_cdt_cd = 'C' 
                          then acct.alt_acct_id
                     end as benef_acct_nb,
                     case when bot.dbt_cdt_cd = 'C' 
                          then 'Y'
                     end as benef_own_fl,
                     bot.rf_credit_cd as credit_cd,
                     case when bot.dbt_cdt_cd = 'C' 
                          then cust_addr.addr_tx
                     end as benef_addr_tx,
                     case when bot.dbt_cdt_cd = 'C' 
                          then cust_iddoc.iddoc_tx
                     end as benef_iddoc_tx, 
                     case when bot.dbt_cdt_cd = 'C' 
                          then cust.rf_reg_dt
                     end as benef_reg_dt,       
                     case when bot.dbt_cdt_cd = 'C' 
                          then bot.rf_acct_goz_op_cd                    
                     end as benef_goz_op_cd,                     
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' 
                          then nvl(tb_cust.full_nm, tb.org_nm)
                     end as rcv_instn_nm,
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' 
                          then nvl(tb_cust.rf_bic_nb, tb.cstm_2_tx) 
                     end as rcv_instn_id,
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' 
                          then 'RU' 
                     end as rcv_instn_cntry_cd,
                     case when bot.rf_acct_seq_id <> -1 and bot.dbt_cdt_cd = 'C' 
                          then tb.cstm_3_tx 
                     end as rcv_instn_acct_id,
                     case when bot.dbt_cdt_cd = 'C' 
                          then acct.rf_branch_id
                     end as rcv_branch_id,
                     rcv_org.oes_321_org_id as rcv_oes_org_id,
                     rcv_org.tb_name        as rcv_kgrko_nm,
                     case when rcv_org.oes_321_org_id is not null 
                          then nvl(decode(rcv_org.branch_fl, '0', nullif(rcv_org.numbf_s, '0'), 
                                                             '1', nullif(rcv_org.numbf_ss, '0')), '-') -- ������� �������� �������� �� (��� ������ �������)
                     end as rcv_kgrko_id,
                     to_char(null) as trxn_doc_id,
                     to_date(null) as trxn_doc_dt,
                     to_char(null) as trxn_conv_crncy_cd,
                     to_number(null) as trxn_conv_am,
                     to_number(null) as trxn_conv_base_am,
                     to_char(null) as trxn_cash_symbols_tx,
                     bot.bank_card_id_nb as bank_card_id_nb,
                     bot.cstm_1_dt as bank_card_trxn_dt,
                     bot.mantas_trxn_chanl_dtl_1_tx as merchant_no,
                     bot.mantas_trxn_chanl_dtl_2_tx as atm_id,
                     trunc(bot.rf_branch_id / 100000) as tb_id,
                     bot.rf_branch_id - round(bot.rf_branch_id, -5) as osb_id,
                     bot.src_sys_cd,
                     bot.rf_src_change_dt as src_change_dt,
                     bot.rf_src_user_nm as src_user_nm,
                     bot.rf_src_cd as src_cd,
                     bot.rf_canceled_fl as canceled_fl,
                     bot.data_dump_dt as data_dump_dt
                FROM (select /*+ INDEX(back_office_trxn PK_BO_TRXN)*/ 
                             bo_trxn_seq_id, dbt_cdt_cd, exctn_dt, rf_branch_id, trxn_rptng_am, trxn_base_am, rptng_crncy_cd, 
                             dscr_tx, trxn_type1_cd, 
                             rf_acct_seq_id, rf_acct_goz_op_cd, rf_debit_cd, rf_credit_cd, 
                             bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx, 
                             src_sys_cd, rf_src_change_dt, rf_src_user_nm, rf_src_cd, rf_canceled_fl, data_dump_dt
                        from business.back_office_trxn 
                       where bo_trxn_seq_id = par_trxn_seq_id and
                             not exists(select null 
                                          from business.back_office_trxn_arh
                                         where bo_trxn_seq_id = par_trxn_seq_id and
                                               arh_dump_dt = par_arh_dump_dt)
                      union all
                      select /*+ INDEX(back_office_trxn_arh PK_BO_TRXN_ARH)*/
                             bo_trxn_seq_id, dbt_cdt_cd, exctn_dt, rf_branch_id, trxn_rptng_am, trxn_base_am, rptng_crncy_cd, 
                             dscr_tx, trxn_type1_cd, 
                             rf_acct_seq_id, rf_acct_goz_op_cd, rf_debit_cd, rf_credit_cd, 
                             bank_card_id_nb, cstm_1_dt, mantas_trxn_chanl_dtl_1_tx, mantas_trxn_chanl_dtl_2_tx, 
                             src_sys_cd, rf_src_change_dt, rf_src_user_nm, rf_src_cd, rf_canceled_fl, data_dump_dt
                        from business.back_office_trxn_arh 
                       where bo_trxn_seq_id = par_trxn_seq_id and
                             arh_dump_dt = par_arh_dump_dt) bot
                     LEFT JOIN business.acct acct ON acct.rowid = business.rf_pkg_util.get_acct2(bot.rf_acct_seq_id)                    
                     LEFT JOIN business.cust cust ON cust.cust_seq_id = acct.rf_prmry_cust_seq_id
                     LEFT JOIN (select business.rf_pkg_util.get_addr_text(cust_addr_seq_id) as addr_tx
                                  from business.cust_addr) cust_addr 
                            ON cust_addr.rowid = coalesce(business.rf_pkg_util.get_primary_cust_addr(cust.cust_seq_id, 'L'), -- ����� ����������� 
                                                          business.rf_pkg_util.get_primary_cust_addr(cust.cust_seq_id, 'H')) -- ���� �� �����, �� ����� �����������      
                     LEFT JOIN (select business.rf_pkg_util.get_iddoc_text(cust_doc_seq_id) iddoc_tx
                                  from business.rf_cust_id_doc) cust_iddoc
                            ON cust_iddoc.rowid = business.rf_pkg_util.get_primary_cust_iddoc(cust.cust_seq_id)
                     LEFT JOIN business.org  tb ON tb.org_intrl_id = to_char(trunc(nvl(acct.rf_branch_id, bot.rf_branch_id)/100000))
                     LEFT JOIN business.cust tb_cust ON tb_cust.cust_seq_id = tb.cstm_1_rl
                     LEFT JOIN business.rf_trxn_extra trxe ON trxe.op_cat_cd = 'BOT' and trxe.fo_trxn_seq_id = bot.bo_trxn_seq_id 
                     LEFT JOIN mantas.rf_oes_321_org send_org ON send_org.oes_321_org_id = case when trxe.rowid is not null 
                                                                                                then trxe.send_oes_org_id
                                                                                                when bot.dbt_cdt_cd = 'D'  
                                                                                                then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(bot.exctn_dt))
                                                                                           end       
                     LEFT JOIN mantas.rf_oes_321_org rcv_org  ON rcv_org.oes_321_org_id  = case when trxe.rowid is not null 
                                                                                                then trxe.rcv_oes_org_id
                                                                                                when bot.dbt_cdt_cd = 'C'  
                                                                                                then mantas.rf_pkg_kgrko.get_oes_321_org_id(trunc(acct.rf_branch_id/100000), acct.rf_branch_id - round(acct.rf_branch_id, -5), trunc(bot.exctn_dt))
                                                                                           end       
                     ) LOOP
      var_row.op_cat_cd := r.op_cat_cd;
      var_row.dbt_cdt_cd := r.dbt_cdt_cd;
      var_row.trxn_seq_id := r.trxn_seq_id;
      var_row.trxn_dt := r.trxn_dt;
      var_row.branch_id := r.branch_id;
      var_row.org_intrl_id := r.org_intrl_id;
      var_row.trxn_crncy_am := r.trxn_crncy_am;
      var_row.trxn_base_am := r.trxn_base_am;
      var_row.trxn_crncy_cd := r.trxn_crncy_cd;
      var_row.trxn_crncy_rate_am := r.trxn_crncy_rate_am;
      var_row.trxn_crncy_old_rate_fl := r.trxn_crncy_old_rate_fl;
      var_row.trxn_desc := r.trxn_desc;
      var_row.nsi_op_id := r.nsi_op_id;
      var_row.orig_nm := r.orig_nm;
      var_row.orig_inn_nb := r.orig_inn_nb;
      var_row.orig_kpp_nb := r.orig_kpp_nb;
      var_row.orig_acct_nb := r.orig_acct_nb;
      var_row.orig_own_fl := r.orig_own_fl;
      var_row.debit_cd := r.debit_cd;
      var_row.orig_addr_tx := r.orig_addr_tx;
      var_row.orig_iddoc_tx := r.orig_iddoc_tx;
      var_row.orig_reg_dt := r.orig_reg_dt;
      var_row.orig_goz_op_cd := r.orig_goz_op_cd;
      var_row.send_instn_nm := r.send_instn_nm;
      var_row.send_instn_id := r.send_instn_id;
      var_row.send_instn_cntry_cd := r.send_instn_cntry_cd;
      var_row.send_instn_acct_id := r.send_instn_acct_id;
      var_row.send_branch_id := r.send_branch_id;
      var_row.send_oes_org_id := r.send_oes_org_id;
      var_row.send_kgrko_nm := r.send_kgrko_nm;
      var_row.send_kgrko_id := r.send_kgrko_id;
      var_row.benef_nm := r.benef_nm;
      var_row.benef_inn_nb := r.benef_inn_nb;
      var_row.benef_kpp_nb := r.benef_kpp_nb;
      var_row.benef_acct_nb := r.benef_acct_nb;
      var_row.benef_own_fl := r.benef_own_fl;
      var_row.credit_cd := r.credit_cd;
      var_row.benef_addr_tx := r.benef_addr_tx;
      var_row.benef_iddoc_tx := r.benef_iddoc_tx;
      var_row.benef_reg_dt := r.benef_reg_dt;
      var_row.benef_goz_op_cd := r.benef_goz_op_cd;
      var_row.rcv_instn_nm := r.rcv_instn_nm;
      var_row.rcv_instn_id := r.rcv_instn_id;
      var_row.rcv_instn_cntry_cd := r.rcv_instn_cntry_cd;
      var_row.rcv_instn_acct_id := r.rcv_instn_acct_id;
      var_row.rcv_branch_id := r.rcv_branch_id;
      var_row.rcv_oes_org_id := r.rcv_oes_org_id;
      var_row.rcv_kgrko_nm := r.rcv_kgrko_nm;
      var_row.rcv_kgrko_id := r.rcv_kgrko_id;
      var_row.trxn_doc_id := r.trxn_doc_id;
      var_row.trxn_doc_dt := r.trxn_doc_dt;
      var_row.trxn_conv_crncy_cd := r.trxn_conv_crncy_cd;
      var_row.trxn_conv_am := r.trxn_conv_am;
      var_row.trxn_conv_base_am := r.trxn_conv_base_am;
      var_row.trxn_cash_symbols_tx := r.trxn_cash_symbols_tx;
      var_row.bank_card_id_nb := r.bank_card_id_nb;
      var_row.bank_card_trxn_dt := r.bank_card_trxn_dt;
      var_row.merchant_no := r.merchant_no;
      var_row.atm_id := r.atm_id;
      var_row.tb_id := r.tb_id;
      var_row.osb_id := r.osb_id;
      var_row.src_sys_cd := r.src_sys_cd;
      var_row.src_change_dt := r.src_change_dt;
      var_row.src_user_nm := r.src_user_nm;
      var_row.src_cd := r.src_cd;
      var_row.canceled_fl := r.canceled_fl;
      var_row.data_dump_dt := r.data_dump_dt;
      
      PIPE ROW(var_row);
    END LOOP;

  end if;
END rfv_trxn; 
/

grant EXECUTE on BUSINESS.rfv_trxn to MANTAS with grant option;

grant EXECUTE on BUSINESS.rfv_trxn to DATA_READER;

grant EXECUTE on BUSINESS.rfv_trxn to KDD_ALGORITHM;

grant EXECUTE on BUSINESS.rfv_trxn to KDD_ANALYST;

grant EXECUTE on BUSINESS.rfv_trxn to KDD_MINER;

grant EXECUTE on BUSINESS.rfv_trxn to RF_RSCHEMA_ROLE;

grant EXECUTE on BUSINESS.rfv_trxn to CMREVMAN;

grant EXECUTE on BUSINESS.rfv_trxn to KDD_MNR with grant option;

grant EXECUTE on BUSINESS.rfv_trxn to KDD_REPORT with grant option;

grant EXECUTE on BUSINESS.rfv_trxn to KYC;

grant EXECUTE on BUSINESS.rfv_trxn to STD;

GRANT EXECUTE ON BUSINESS.RF_TAB_TRXN TO MANTAS;
GRANT EXECUTE ON BUSINESS.RF_TYPE_TRXN TO MANTAS;

