grant select on mantas.rf_oes_321_party to business;
grant execute on mantas.rf_pkg_rule to business;

drop type business.rf_tab_type_address_set
/

create or replace type business.rf_tab_type_address as object (
  country             varchar2(255),
  post_index          varchar2(255),
  region_cd           varchar2(255),
  region_nm           varchar2(255),
  region_addr_seq_id  number(22),
  district_cd         varchar2(255),
  district_nm         varchar2(255),
  city_cd             varchar2(255),
  city_nm             varchar2(255),
  settlm_type_cd      varchar2(255),
  settlm_nm           varchar2(255),
  street_cd           varchar2(255),
  street_nm           varchar2(255),
  house_nb            varchar2(255),
  building            varchar2(255),
  flat                varchar2(255)
)
/

create or replace type business.rf_tab_type_address_set as table of business.rf_tab_type_address
/

create or replace package business.rf_pkg_util is
  --
  -- �������� ���� ������/����. ������� �� ��������� ���� (���� �� ��������� ���� ���, ������������ ��������� ���������, �� � �������!)
  --
  function get_crncy_rate(p_tb_id      integer, -- ��� ��������
                          p_iso_alfa   char, --�������������� ��� ISO ������
                          p_rate_date  date, -- ���� �����
                          p_metal_flag integer, -- ���� 1/0 - ������� ���� ����. �������/������� ������
                          p_sign       integer -- ��� ������������� ����� ����. �������: 0 - ��, -1 - ���� ������� ����. ������� (������), 1 - ���� ������� ����. ������� (������)
                          ) return number result_cache; -- ���� (���� "������", �. �. �� ���� �� ���������� ���, �� �������������)
  --
  -- �������� ����� � ���� ����� ������
  --
  function get_addr_text(par_cust_addr_seq_id business.cust_addr.cust_addr_seq_id%type, -- ������������� ������ ������
                         par_arc_dt           date default null -- ���� ������ �������� ������ � ������ (������� _ARC), ������������ ������ ������ - ���� �� �����������, �. �. ��� ������� CUST_ADDR_ARC
                         ) return varchar2; -- ������ ������ ����: ������, ������, �����, ���������� �����, �����, ���, ��������/������, ����/��������
  --
  -- �������� ������ � ��������� � ���� ����� ������
  --
  function get_iddoc_text(par_cust_doc_seq_id business.rf_cust_id_doc.cust_doc_seq_id%type, -- ������������� ������ ������
                          par_arc_dt          date default null -- ���� ������ �������� ������ � ������ (������� _ARC), ������������ ������ ������ - ���� �� �����������, �. �. ��� ������� RF_CUST_ID_DOC_ARC
                          ) return varchar2; -- ������ ��������������� ��������� ����: ���, �����, �����, ����� <���> <�����> (��������� �� <���� ��������>)
  --
  -- �� �������� ��������� ��������� ��������� �������� ������
  --
  function format_iddoc_text(p_typedoc       varchar2 default null, -- ��� ���� ���������
                             p_serial        varchar2 default null, -- ����� ���������
                             p_numberdoc     varchar2 default null, -- ����� ���������
                             p_exe_dept      varchar2 default null, -- ��� ����� ��������
                             p_exe_dept_code varchar2 default null, -- ��� �������������, ��������� ��������
                             p_exe_date      date default null, -- ���� ������ ��������
                             p_last_date     date default null -- ���� �������� ���������
                             ) return varchar2; -- ������ ��������������� ��������� ����: ���, �����, �����, ����� <���> <�����> (��������� �� <���� ��������>)
  --
  -- ������� ��������� �� ���� ������ � ���������� ����������������� ������������, ���� � ������ ��� ������������
  --
  function format_account(p_acc varchar2) return varchar2 deterministic
    parallel_enable;

  --
  -- ������� ��������� �� ���� ID ������� � ������� � ��� ���� ��������������
  -- ���������� �������� ��������������� ����� �������
  --
  function get_list_clsf_code(p_cust_id business.rf_cust_clsf_code.cust_seq_id%type,
                              p_code    business.rf_cust_clsf_code.clsf_type_cd%type)
    return varchar2;
  --
  -- ��������������� ������� ��� ������ business.rfv_trxn_party
  -- ��������� �� ���� id ������� � ��� ������, ���������� rowid ������� business.cust_addr
  -- ���� �����a �����������, ���� �����a ����� ����������
  --
  function get_primary_cust_addr(p_cust_id       business.cust_addr.rf_cust_seq_id%type,
                                 p_addr_usage_cd business.cust_addr.addr_usage_cd%type)
    return rowid;

  --
  -- ��������������� ������� ��� ������ business.rfv_trxn_party
  -- ��������� �� ���� id �������, ���������� rowid ��������� � ������� business.rf_cust_id_doc
  -- ��������������� ����������� �������� ���, ����� � ����� ���������
  -- ���� ������ �������� � �������������� ���������� - �� ��������� �������� ������������
  --
  function get_primary_cust_iddoc(p_cust_id    business.rf_cust_id_doc.cust_seq_id%type,
                                  p_doc_type   business.rf_cust_id_doc.doc_type_cd%type default null,
                                  p_doc_series business.rf_cust_id_doc.series_id%type default null,
                                  p_doc_no     business.rf_cust_id_doc.no_id%type default null)
    return rowid;
  --
  -- ������� �������� �� ���� ��� ������� ('GAT', 'NAV', 'EKS' etc..) � ������ ������
  -- �� ������ �������� ����������� �����
  -- � ������ ������ ����������� ������ ������ ��� 'GAT'
  --
  function format_address(p_system varchar2, p_string varchar2)
    return business.rf_tab_type_address_set pipelined;
  --
  -- ��������� ������ ������, ���������� �� ������������ �������-��������� � ������� ����� � ����� ������, ������������ �� ����������
  -- ������� �������� �� ���� ��� ������� ('GAT', 'NAV', 'EKS' etc..) � ������ ������
  -- ������� ���������� ����� ������ ��� ����������� �� ����������
  -- � ������ ������ ����������� ������ ������ ��� 'GAT'
  --  
  FUNCTION format_address_text
  (
    par_sys_cd   VARCHAR2,  -- ��� �������-���������, ���� ������ GAT
    par_addr     VARCHAR2   -- ������ ������
   ) 
  RETURN VARCHAR2;          -- ����� ������ ��� ����������� �� ����������
  --
  -- ����� ������������� ��� ���������� ��/�� (� ��������� �������������� ����������, ���� �� ��������)
  --
  FUNCTION get_representative
  (
    par_cust_seq_id           cust.cust_seq_id%TYPE,                        -- ID ��/��, ��� �������� ������� ����� �������������  
    par_doc_type_cd           rf_cust_id_doc.doc_type_cd%TYPE DEFAULT NULL, -- ��� ��������������� ��������� �������������  
    par_doc_series_id         rf_cust_id_doc.series_id%TYPE DEFAULT NULL,   -- ����� ��������������� ��������� �������������  
    par_doc_no_id             rf_cust_id_doc.no_id%TYPE DEFAULT NULL        -- ����� ��������������� ��������� �������������  
  ) 
  RETURN cust.cust_seq_id%TYPE; -- ID ���������� �������������

   -- ����� ��������� �������� �� ���������� id_cust, ��� ��������� (����� ���� �� ������), ���� ��� �������� ������������ ����� ���������� (�� ������������!)
   -- � ������� ����� �������������� ����������, ������ �� �����������
   -- ����� ������ ������������ �����, ����� ���������� �������� ��� ����� ���������� ��� ��
   --�������� ��� ����������� ����� ��� ?39?! � � ������ ���, ��� ����!
   function get_id_doc(p_id_cust   number,
                          p_doc_type  number default null,
                          p_rights_fl varchar2 default 'Y')
     return number;
  --
  -- ������������ �������� ������ ������/���� ������ ����� � ��������� ��������
  --
  FUNCTION get_trxn_scrty_desc
  (
    par_op_cat_cd             VARCHAR2,                                     -- ��������� ��������: WT - �����������, CT - ��������  
    par_trxn_seq_id           NUMBER,                                       -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN)
    par_trxn_scrty_seq_id     NUMBER    DEFAULT NULL                        -- ID ������ ������ � �������� (PK ������ RF_WIRE_TRXN_SCRTY/RF_CASH_TRXN_SCRTY)
                                                                            -- ���� NULL, �� ������������ �������� ���� ������ ����� � ��������
  ) 
  RETURN VARCHAR2; -- �������� ������ ������/���� ������ ����� � ��������� ��������
  --
  -- �������� ����� ����� ������� ��� ���������� �� ��������� ���
  --
  function get_oes_addr_tx (
    par_oes_321_id in mantas.rf_oes_321_party.oes_321_id%type, --ID ��������� ���
    par_block_nb   in mantas.rf_oes_321_party.block_nb%type,   --��� ��������� ��������� ���
    par_addr_tp    in varchar2 default 'AMR'                   --��� ������ - ����������� / ���������� 
  ) return varchar2;
  --
  -- �������� ����� � ��������� ������� ������� (�� �����, ��� ��������� ���������� �������, ���������, ���������� ����)
  --
  function get_last_addresses_text(
    par_cust_seq_id   business.cust_addr.rf_cust_seq_id%type,
    par_addr_count    number,
    par_addr_usage_cd business.cust_addr.addr_usage_cd%type default null
  ) return varchar;
  --
  -- �������� rowid ����� (ACCT.ROWID) �� ��� ������ �� ��������� ����
  -- (���� � ��� �� ����� ����� ���� ���������������, �. �. ����� ���� ��������� ������ � ����� � ��� �� �������,
  --  ������� ������ ������� ��������)
  --
  FUNCTION get_acct
  (
    par_alt_acct_id       business.acct.ALT_ACCT_ID%TYPE,            -- ����� ����� (������, 20 ����)
    par_date              DATE,                                      -- ����, �� ������� ���� ����� ���� 
    par_primary_sys_cd    business.acct.SRC_SYS_CD%TYPE DEFAULT null -- ��� ���������������� ������� ��������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
  ) 
  RETURN ROWID PARALLEL_ENABLE; -- rowid ������, ��������������� ����� (ACCT.ROWID), null, ���� ���� �� ������
  --
  -- �������� rowid ����� (ACCT.ROWID) �� ��������� ���������� 
  -- ���� ����� �����������:
  --   - ��������������� ACCT_SEQ_ID
  --   - ������� � ���/SWIFT �����
  --   - ������� � ��������������� CUST_SEQ_ID �������-��������� 
  -- ������� ���������� null ��� ������, �� ����������� � �� ��
  --
  FUNCTION get_acct2
  (
    par_acct_seq_id       business.acct.ACCT_SEQ_ID%TYPE,              -- ID ����� (�� ����������� ������)
    par_alt_acct_id       business.acct.ALT_ACCT_ID%TYPE DEFAULT null, -- ����� ����� (������, 20 ����)
    par_bic_nb            VARCHAR2 DEFAULT null,                       -- ���/SWIFT �����, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
    par_cust_seq_id       business.cust.CUST_SEQ_ID%TYPE DEFAULT null, -- ID ������� - ��������� �����
    par_date              DATE DEFAULT null,                           -- ����, �� ������� ���� ����� ���� �� ������
    par_primary_sys_cd    business.acct.SRC_SYS_CD%TYPE DEFAULT null   -- ��� ���������������� ������� ��������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
  ) 
  RETURN ROWID PARALLEL_ENABLE;  -- rowid �����, null, ���� ���� �� ������
  --
  -- ���������� ��� BRANCH_ID (��� �� * 100000 + ��� ���) ��� ���������� ����� 
  -- ���� ����� �����������:
  --   - ��������������� ACCT_SEQ_ID
  --   - ������� � ���/SWIFT �����
  --   - ������� � ��������������� CUST_SEQ_ID �������-��������� 
  -- ������� ���������� null ��� ������, �� ����������� � �� ��
  --
  FUNCTION get_acct_branch_id
  (
    par_acct_seq_id       business.acct.ACCT_SEQ_ID%TYPE,              -- ID ����� (�� ����������� ������)
    par_alt_acct_id       business.acct.ALT_ACCT_ID%TYPE DEFAULT null, -- ����� ����� (������, 20 ����)
    par_bic_nb            VARCHAR2 DEFAULT null,                       -- ���/SWIFT �����, � ������� ������ ����             
    par_cust_seq_id       business.cust.CUST_SEQ_ID%TYPE DEFAULT null, -- ID ������� - ��������� �����
    par_date              DATE DEFAULT null,                           -- ����, �� ������� ���� ����� ���� �� ������
    par_primary_sys_cd    business.acct.SRC_SYS_CD%TYPE DEFAULT null   -- ��� ���������������� ������� ��������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
  ) 
  RETURN business.acct.RF_BRANCH_ID%TYPE PARALLEL_ENABLE;  -- ��� BRANCH_ID, ��������������� �����, null, ���� ���� �� ������
  --
  -- ��������� �������� �������������, � ������� ��������� ��������
  -- � ����������� �� ������� � �������� ��������� par_branch_id ����������:
  --  - ������������ ��� - ���� par_branch_id ��������� � ���� �� ��-���, ��� � par_org_intrl_id
  --  - ������������ ���, ������������ ��� - ���� par_branch_id ��������� � ���� �� ��, ��� � par_org_intrl_id, �� � ������� ���
  --  - ������������ ��, ������������ ���, ������������ ��� - ���� par_branch_id �� ������ ��� ��������� � ������� ��, ��� par_org_intrl_id
  -- ������� ������������ �� ���������� ������������, ��� ������ � ������������ ��/��� � ������������ � par_branch_id ��� ������������  
  --
  FUNCTION get_trxn_org_desc
  (
    par_org_intrl_id      business.cash_trxn.TRXN_BRNCH_ID%TYPE,             -- ORG_INTRL_ID �������������, � ������� ��������� ��������
    par_branch_id         business.cash_trxn.RF_BRANCH_ID%TYPE DEFAULT null  -- BRANCH_ID �������� - ������������ ��� ���������� � �������������� �������� ������������� ������������ ������������� (��, ���)
  ) 
  RETURN VARCHAR2 RESULT_CACHE PARALLEL_ENABLE;  -- �������� �������������, ��� �������� ������ � par_org_intrl_id (� ����������� �������� ������������� � par_branch_id)
end rf_pkg_util;
/

create or replace package body business.RF_PKG_UTIL is

  --
  -- �������� ���� ������/����. ������� �� ��������� ���� (���� �� ��������� ���� ���, ������������ ��������� ���������, �� � �������!)
  --
  function get_crncy_rate(p_tb_id      integer, -- ��� ��������
                          p_iso_alfa   char, --�������������� ��� ISO ������
                          p_rate_date  date, -- ���� �����
                          p_metal_flag integer, -- ���� 1/0 - ������� ���� ����. �������/������� ������
                          p_sign       integer -- ��� ������������� ����� ����. �������: 0 - ��, -1 - ���� ������� ����. ������� (������), 1 - ���� ������� ����. ������� (������)
                          ) return number result_cache is
    -- ���� (���� "������", �. �. �� ���� �� ���������� ���, �� �������������)
    result number;
  begin
    if (p_metal_flag = 0) then
      -- ��� ����� (�� ����. ��������) ������ ���������� ���� �� - ����� ��� �� ����������� ����� (38)
      select max(cr.RATE / cr.QTY)
        into result
        from business.rf_crncy_rates cr
       where cr.tb_id = 38 -- ������ �� ����������� �����
         and cr.rate_date = P_rate_date
         and cr.iso_alfa = p_iso_alfa;
      --���� �� �������� ���� ���� �� ��� ������, ���� ��������� ��������� ����
      if Result is null then
        select -1 * max(cr.rate / cr.qty)
          into REsult
          from business.rf_crncy_rates cr
         where cr.rate_date = (select max(r.rate_date)
                                 from business.rf_crncy_rates r
                                where r.rate_date < p_rate_date
                                  and r.iso_alfa = p_iso_alfa
                                  and r.tb_id = 38)
           and cr.iso_alfa = p_iso_alfa
           and cr.tb_id = 38;
      end if;
    else
      --���� ������ -  �����
      select max(decode(p_sign, -1, mr.buy_rate, 1, mr.sell_rate, mr.rate))
        into result
        from business.rf_metal_rates mr
       where mr.tb_id = p_tb_id
         and mr.rate_date = p_rate_date
         and mr.iso_alfa = p_iso_alfa;
      --���� �� �������� ���� ���� �� ��� ������, ���� ��������� ��������� ����
      if result is null then
        select -1 *
               max(decode(p_sign, -1, mr.buy_rate, 1, mr.sell_rate, mr.rate))
          into result
          from business.rf_metal_rates mr
         where mr.tb_id = p_tb_id
           and mr.rate_date = (select max(t.rate_date)
                                 from business.rf_metal_rates t
                                where t.tb_id = p_tb_id
                                  and t.rate_date < p_rate_date
                                  and t.iso_alfa = p_iso_alfa)
           and mr.iso_alfa = p_iso_alfa;
      end if;
    end if;

    return result;
  end get_crncy_rate;
  --
  -- �������� ����� � ���� ����� ������
  --
  function get_addr_text(par_cust_addr_seq_id business.cust_addr.cust_addr_seq_id%type, -- ������������� ������ ������
                         par_arc_dt           date default null -- ���� ������ �������� ������ � ������ (������� _ARC), ������������ ������ ������ - ���� �� �����������, �. �. ��� ������� CUST_ADDR_ARC
                         ) return varchar2 is
    result varchar2(4000) default null;
  begin

    if par_cust_addr_seq_id is not null then
      select upper(decode(coalesce(rc.cntry_nm, rc.long_nm, rc.eng_cntry_nm),
                          null,
                          null,
                          initcap(coalesce(rc.cntry_nm,
                                           rc.long_nm,
                                           rc.eng_cntry_nm))) ||
                   decode(a.addr_postl_cd,
                          null,
                          null,
                          ', ' || a.addr_postl_cd) || case
                     when (trim(upper(a.rf_state_nm)) not in
                          ('������',
                            '����� ������',
                            '�����-���������')) then
                      decode(a.rf_state_nm,
                             null,
                             null,
                             ', ' || trim(a.rf_state_nm || ' ' || a.rf_state_type_cd))
                   end || decode(a.addr_rgn_nm,
                                 null,
                                 null,
                                 ', ' || trim(a.rf_rgn_type_cd || ' ' || initcap(a.addr_rgn_nm))) ||
                   decode(a.addr_city_nm,
                          null,
                          null,
                          ', ' ||
                          trim(trim('.' from(lower(a.rf_city_type_cd) || '. ' ||
                                         initcap(a.addr_city_nm))))) ||
                   decode(a.rf_settlm_nm,
                          null,
                          null,
                          ', ' ||
                          trim(trim('.' from(lower(a.rf_settlm_type_cd) || '. ' ||
                                         initcap(a.rf_settlm_nm))))) ||
                   decode(a.addr_strt_line1_tx,
                          null,
                          null,
                          ', ' || decode(a.addr_strt_line2_tx, null, null, a.addr_strt_line2_tx || '. ') ||
                          initcap(a.addr_strt_line1_tx)) ||
                   decode(a.addr_strt_line3_tx,
                          null,
                          null,
                          ', �. ' || a.addr_strt_line3_tx) ||
                   decode(a.addr_strt_line4_tx,
                          null,
                          null,
                          ', ����/���' || a.addr_strt_line4_tx) ||
                   decode(a.addr_strt_line5_tx,
                          null,
                          null,
                          ', ��. ' || a.addr_strt_line5_tx))
        into result
        from business.cust_addr a
        left join business.rf_country rc
          on a.addr_cntry_cd = rc.cntry_cd
       where a.cust_addr_seq_id = par_cust_addr_seq_id;
    end if;

    return result;
  exception
    when others then
      return null;
  end get_addr_text;
  --
  -- �������� ������ � ��������� � ���� ����� ������
  --
  function get_iddoc_text(par_cust_doc_seq_id business.rf_cust_id_doc.cust_doc_seq_id%type, -- ������������� ������ ������
                          par_arc_dt          date default null -- ���� ������ �������� ������ � ������ (������� _ARC), ������������ ������ ������ - ���� �� �����������, �. �. ��� ������� RF_CUST_ID_DOC_ARC
                          ) return varchar2 is
    -- ������ ��������������� ��������� ����: ���, �����, �����, ����� <���> <�����> (��������� �� <���� ��������>)
    result varchar2(4000) default null;
  begin

    if par_cust_doc_seq_id is not null then
      select max(format_iddoc_text(d.doc_type_cd,
                                   d.series_id,
                                   d.no_id,
                                   d.issued_by_tx,
                                   d.issue_dpt_cd,
                                   d.issue_dt,
                                   d.end_dt))
        into result
        from business.rf_cust_id_doc d
       where d.cust_doc_seq_id = par_cust_doc_seq_id;
    end if;

    return result;
  exception
    when others then
      return null;
  end get_iddoc_text;
  --
  -- �� �������� ��������� ��������� ��������� �������� ������
  --
  function format_iddoc_text(p_typedoc       varchar2 default null, -- ��� ���� ���������
                             p_serial        varchar2 default null, -- ����� ���������
                             p_numberdoc     varchar2 default null, -- ����� ���������
                             p_exe_dept      varchar2 default null, -- ��� ����� ��������
                             p_exe_dept_code varchar2 default null, -- ��� �������������, ��������� ��������
                             p_exe_date      date default null, -- ���� ������ ��������
                             p_last_date     date default null -- ���� �������� ���������
                             ) return varchar2 is
    -- ������ ��������������� ��������� ����: ���, �����, �����, ����� <���> <�����> (��������� �� <���� ��������>)
    result    varchar2(4000) default null;
    v_doctype varchar2(200) default null;
  begin
    begin
      if p_typedoc is not null then
        select dt.doc_type_nm
          into v_doctype
          from business.rf_cust_id_doc_type dt
         where dt.doc_type_cd = p_typedoc;
        result := result || v_doctype;
      end if;
    exception
      when no_data_found then
        null;
    end;
    if p_serial is not null then
      result := result || ', �����: ' || p_serial;
    end if;
    if p_numberdoc is not null then
      result := result || ' �����: ' || p_numberdoc;
    end if;
    if p_exe_dept is not null then
      result := result || ' �����: "' || initcap(p_exe_dept) || '"';
    end if;
    if p_exe_dept_code is not null then
      result := result || ', ��� �������������: ' || p_exe_dept_code;
    end if;
    if p_exe_date is not null then
      result := result || ', ���� ������: ' ||
                to_char(p_exe_date, 'DD.MM.YYYY');
    end if;
    if ((p_last_date is not null) and
       (p_last_date > to_date('01.01.1901', 'DD.MM.YYYY'))) then
      result := result || ', ���� ��������� ��������: ' ||
                to_char(p_last_date, 'DD.MM.YYYY');
    end if;
    return upper(trim(trim(',' from result)));
  exception
    when others then
      return null;
  end format_iddoc_text;
  --
  -- ������� ��������� �� ���� ������ � ���������� ����������������� ������������, ���� � ������ ��� ������������
  --
  function format_account(p_acc varchar2) return varchar2 deterministic
    parallel_enable as
    result varchar2(4000) default null;
  begin
    -- �������, ��� ������������� ���� ������ ��������� �� ���� � ������ �� ����� 20 ��������.
    -- ��� ���� ������ ������ ����� ���� A (��������)
    if regexp_like(p_acc, '^\d{5}(A{1}|\d{1})\d{14,}+$') then
      result := trim(substr(p_acc, 0, 5) || ' ' || substr(p_acc, 6, 3) || ' ' ||
                     substr(p_acc, 9, 1) || ' ' || substr(p_acc, 10, 4) || ' ' ||
                     substr(p_acc, 14, 7) || ' ' ||
                     substr(p_acc, 21, (length(p_acc) - 20)));
    else
      result := p_acc;
    end if;
    return result;
  exception
    when others then
      return p_acc;
  end format_account;
  --
  -- ������� ��������� �� ���� ID ������� � ������� � ��� ���� ��������������
  -- ���������� �������� ��������������� ����� �������
  --
  function get_list_clsf_code(p_cust_id business.rf_cust_clsf_code.cust_seq_id%type,
                              p_code    business.rf_cust_clsf_code.clsf_type_cd%type)
    return varchar2 is
    result varchar2(4000) default null;
  begin
    -- ��� ����� = 1
    -- ��� ����� = 2
    select max(res)
      into result
      from (select /*+ INDEX(c RF_CUSTCLSFCD_CUST_FK_I)*/
                   listagg(c.clsf_code, '; ') within group(order by c.clsf_code) as res
              from business.rf_cust_clsf_code c
             where c.cust_seq_id = p_cust_id
               and c.clsf_type_cd = p_code);
               
    return result;
  exception
    when others then
      return null;
  end get_list_clsf_code;
  --
  -- ��������������� ������� ��� ������ business.rfv_trxn_party
  -- ��������� �� ���� id ������� � ��� ������, ���������� rowid ������� business.cust_addr
  -- ���� �����a �����������, ���� �����a ����� ����������
  --
  function get_primary_cust_addr(p_cust_id       business.cust_addr.rf_cust_seq_id%type,
                                 p_addr_usage_cd business.cust_addr.addr_usage_cd%type)
    return rowid is
    result rowid default null;
  begin
    if (p_cust_id is not null and p_addr_usage_cd is not null) then
      select max(z.rowid)
        into result
        from (select a.rowid,
                     a.rf_cust_seq_id,
                     a.addr_usage_cd,
                     row_number() over(order by decode(rf_active_fl, 'Y', 1, null, 2, 3), -- ������� �������� ������
                                                decode(rf_primary_fl, 'Y', 1, 2), -- ������� ��������
                                                rf_start_dt desc nulls last, -- ��������� �� ���� ��� id
                                                rf_end_dt desc nulls last, 
                                                case 
                                                  --���� ����� �� ��, �� ���� ���������� = [�������� � ������ 3] * 1000000000 + [�������� � ������ 4]
                                                  when substr(a.rf_src_cd, 1, 4) in ('BCKA', 'BCKP') 
                                                    then to_number(regexp_substr(a.rf_src_cd, '[[:digit:]]+', instr(a.rf_src_cd, '|', 1, 2))) * 1000000000 +
                                                         to_number(regexp_substr(a.rf_src_cd, '[[:digit:]]+$'))
                                                  --� ��������� ������� ���� ���������� = [�������� � ������ 2] (��� ����� � ����� ������)
                                                  else to_number(regexp_substr(a.rf_src_cd, '[[:digit:]]+$'))
                                                end desc) as priority
                from business.cust_addr a
               where a.rf_cust_seq_id = p_cust_id
                 and a.addr_usage_cd = p_addr_usage_cd) z
       where z.priority = 1;
    end if;
    return result;
  exception
    when others then
      return null;
  end get_primary_cust_addr;
  --
  -- ��������������� ������� ��� ������ business.rfv_trxn_party
  -- ��������� �� ���� id �������, ���������� rowid ��������� � ������� business.rf_cust_id_doc
  -- ��������������� ����������� �������� ���, ����� � ����� ���������
  -- ���� ������ �������� � �������������� ���������� - �� ��������� �������� ������������
  --
  function get_primary_cust_iddoc(p_cust_id    business.rf_cust_id_doc.cust_seq_id%type,
                                  p_doc_type   business.rf_cust_id_doc.doc_type_cd%type default null,
                                  p_doc_series business.rf_cust_id_doc.series_id%type default null,
                                  p_doc_no     business.rf_cust_id_doc.no_id%type default null)
    return rowid is
    result rowid default null;
  begin
    if (p_cust_id is not null) then
      select max(z.rowid)
        into result
        from (select d.cust_seq_id,
                     d.doc_type_cd,
                     d.series_id,
                     d.no_id,
                     d.issued_by_tx,
                     d.issue_dpt_cd,
                     d.issue_dt,
                     d.end_dt,
                     business.rf_pkg_util.get_iddoc_text(d.cust_doc_seq_id) as iddoc_tx,
                     d.identity_prm_fl,
                     row_number() over(order by case when business.rf_normalize_docno(d.series_id, d.no_id, d.doc_type_cd) = 
                                                          business.rf_normalize_docno(p_doc_series, p_doc_no, p_doc_type) and
                                                          nvl(d.doc_type_cd, -1) = nvl(p_doc_type, -1)
                                                          /*replace(d.doc_type_cd, ' ') = replace(p_doc_type, ' ') and
                                                          nvl(replace(d.series_id, ' '), '#&*') = nvl(replace(p_doc_series, ' '), '#&*') and                                                    
                                                          replace(d.no_id, ' ') = replace(p_doc_no, ' ')*/
                                                     then 1
                                                     else 2
                                                end, -- ������� - ��������� ��������              
                                                decode(d.valid_fl, 'Y', 1, 2), -- ������� �����������
                                                decode(d.identity_prm_fl, 'Y', 1, null, 2, 3), -- ������� �������� ���., ��. ��������
                                                decode(dt.identity_fl, 'Y', 1, null, 2, 3), -- ������� ��� ���-��, ��. ��������
                                                dt.priority_nb nulls last, -- � ������� ����������� ����� ���-���
                                                d.issue_dt desc nulls last, -- ��������� �� ���� ��� id
                                                d.start_dt desc nulls last, 
                                                d.end_dt desc nulls last, 
                                                case 
                                                  --���� ����� �� ��, �� ���� ���������� = [�������� � ������ 3] * 1000000000 + [�������� � ������ 3]
                                                  when substr(d.src_cd, 1, 3) in ('BCK') 
                                                    then to_number(regexp_substr(d.src_cd, '[[:digit:]]+', instr(d.src_cd, '|', 1, 2))) * 1000000000 +
                                                         to_number(regexp_substr(d.src_cd, '[[:digit:]]+$'))
                                                  --� ��������� ������� ���� ���������� = [�������� � ������ 2] (��� ����� � ����� ������)
                                                  else to_number(regexp_substr(d.src_cd, '[[:digit:]]+$'))
                                                end desc) as priority
                from business.rf_cust_id_doc d
                join business.rf_cust_id_doc_type dt
                  on dt.doc_type_cd = d.doc_type_cd
               where d.cust_seq_id = p_cust_id or
                     -- �������� ������� - ���. ����, ���������������� ��
                     d.cust_seq_id = (select rf_ind_cust_seq_id
                                        from business.cust
                                       where cust_seq_id = p_cust_id)) z
       where z.priority = 1;
    end if;
    return result;
  exception
    when others then
      return null;
  end get_primary_cust_iddoc;
  --
  -- ������� �������� �� ���� ��� ������� ('GAT', 'NAV', 'EKS' etc..) � ������ ������
  -- �� ������ �������� ����������� �����
  -- � ������ ������ ����������� ������ ������ ��� 'GAT'
  --
  function format_address(p_system varchar2, p_string varchar2)
    return business.rf_tab_type_address_set
    pipelined is
    result business.rf_tab_type_address := business.rf_tab_type_address(null, null, null, null, null,
                                                                        null, null, null, null, null,
                                                                        null, null, null, null, null, 
                                                                        null);
    var_cntry_cd   business.rf_country.cntry_cd%TYPE;
  begin
    if (p_system = 'GAT' and p_string is not null) then
      FOR r in (SELECT num, trim(txt) as txt
                  FROM (select level as num,
                               trim(regexp_substr(replace(p_string, '!', ' ! '), '[^!]+', 1, level)) as txt
                          from dual
                        connect by level <= 11) adr_seg
                ORDER BY num /*������� �����!*/) LOOP
                
          if r.num = 1 then result.post_index := r.txt;
          elsif r.num = 2 then
            -- ���� ��� ������ - ���������� ������� ������, ����� - ���������� ������� ������� 
            var_cntry_cd := std.aml_tools.get_cntry_cd(r.txt);
            if var_cntry_cd is not null then 
              result.region_addr_seq_id := null; 
              result.region_nm          := null; 
              result.region_cd          := null;
              result.country            := var_cntry_cd;
            else 
              -- �������� ���������� ������ � �������� ��� ������������� � ����������� �������� ��������
              -- ���� �� ���������� - ���������� �������� ������������ ������� � ������������� ������ = ��
              SELECT max(addr_seq_id), nvl(max(object_nm), r.txt), max(addr_type_cd), nvl(max(cntry_cd), 'RU')
                INTO result.region_addr_seq_id, 
                     result.region_nm, 
                     result.region_cd,
                     result.country
                FROM business.rf_addr_object t
               WHERE rowid = std.aml_tools.recognise_region(r.txt);
            end if;  
          elsif r.num = 3 then result.district_cd := r.txt;
          elsif r.num = 4 then result.district_nm := r.txt;
          elsif r.num = 5 then result.city_cd := r.txt;
          elsif r.num = 6 then 
            result.city_nm := r.txt;            
            -- ���� �������� ������� �� ���� � ��� ������, �� ��������� ���������� ������ ����� ��� ������
            if result.region_nm is null and result.country = 'RU' Then
              SELECT max(addr_seq_id), nvl(max(object_nm), r.txt), max(addr_type_cd), nvl(max(cntry_cd), 'RU')
                INTO result.region_addr_seq_id, 
                     result.region_nm, 
                     result.region_cd,
                     result.country
                FROM business.rf_addr_object t
               WHERE rowid = std.aml_tools.recognise_region(r.txt);
            end if;  
          elsif r.num = 7 then result.street_cd := r.txt;
          elsif r.num = 8 then result.street_nm := r.txt;
          elsif r.num = 9 then result.house_nb := r.txt;
          elsif r.num = 10 then result.building := r.txt;
          elsif r.num = 11 then result.flat := r.txt;
        end if;
      END LOOP;
      
      PIPE ROW(result);
    end if;

    return;

  exception
    when others then
      null;
  end format_address;
  --
  -- ��������� ������ ������, ���������� �� ������������ �������-��������� � ������� ����� � ����� ������, ������������ �� ����������
  -- ������� �������� �� ���� ��� ������� ('GAT', 'NAV', 'EKS' etc..) � ������ ������
  -- ������� ���������� ����� ������ ��� ����������� �� ����������
  -- � ������ ������ ����������� ������ ������ ��� 'GAT'
  --  
  FUNCTION format_address_text
  (
    par_sys_cd   VARCHAR2,  -- ��� �������-���������, ���� ������ GAT
    par_addr     VARCHAR2   -- ������ ������
   ) 
  RETURN VARCHAR2 IS     -- ����� ������ ��� ����������� �� ����������
    var_result   VARCHAR2(2000 CHAR);
  BEGIN
    if par_addr is null or par_sys_cd <> 'GAT' Then
      return null;
    end if;

    SELECT trim(',' from
                trim(decode(max(decode(adr_seg.num, 1, adr_seg.txt)),
                            null,
                            null,
                            max(decode(adr_seg.num, 1, adr_seg.txt)) || ', ') || -- ����. ������
                       decode(max(decode(adr_seg.num, 2, adr_seg.txt)),
                            null,
                            null,
                            max(decode(adr_seg.num, 2, adr_seg.txt)) || ', ') || -- ������ ��
                     decode(max(decode(adr_seg.num, 4, adr_seg.txt)) ||
                            max(decode(adr_seg.num, 3, adr_seg.txt)),
                            null,
                            null,
                            max(decode(adr_seg.num, 4, adr_seg.txt)) || ' ' ||
                            max(decode(adr_seg.num, 3, adr_seg.txt)) || ', ') || -- �����
                     decode(max(decode(adr_seg.num, 6, adr_seg.txt)) ||
                            max(decode(adr_seg.num, 5, adr_seg.txt)),
                            null,
                            null,
                            max(decode(adr_seg.num, 6, adr_seg.txt)) || ' ' ||
                            max(decode(adr_seg.num, 5, adr_seg.txt)) || ', ') || -- �����, ���. �����
                     decode(max(decode(adr_seg.num, 8, adr_seg.txt)) ||
                            max(decode(adr_seg.num, 7, adr_seg.txt)),
                            null,
                            null,
                            max(decode(adr_seg.num, 8, adr_seg.txt)) || ' ' ||
                            max(decode(adr_seg.num, 7, adr_seg.txt)) || ', ') || -- �����
                     decode(max(decode(adr_seg.num, 9, adr_seg.txt)),
                            null,
                            null,
                            '��� ' || max(decode(adr_seg.num, 9, adr_seg.txt)) || ', ') ||
                     decode(max(decode(adr_seg.num, 10, adr_seg.txt)),
                            null,
                            null,
                            '���. ' ||
                            max(decode(adr_seg.num, 10, adr_seg.txt)) || ', ') ||
                     decode(max(decode(adr_seg.num, 11, adr_seg.txt)),
                            null,
                            null,
                            '��. ' ||
                            max(decode(adr_seg.num, 11, adr_seg.txt))))) as addr
      INTO var_result                      
      FROM (SELECT level as num,
                 trim(regexp_substr(replace(par_addr,
                                            '!',
                                            ' ! '),
                                    '[^!]+',
                                    1,
                                    level)) as txt
            FROM dual
          CONNECT BY LEVEL <= 11) adr_seg;

    return var_result;
  EXCEPTION
    WHEN OTHERS THEN
      return null;  
  END format_address_text;
  --
  -- ����� ������������� ��� ���������� ��/�� (� ��������� �������������� ����������, ���� �� ��������)
  --
  FUNCTION get_representative
  (
    par_cust_seq_id           cust.cust_seq_id%TYPE,                        -- ID ��/��, ��� �������� ������� ����� �������������  
    par_doc_type_cd           rf_cust_id_doc.doc_type_cd%TYPE DEFAULT NULL, -- ��� ��������������� ��������� �������������  
    par_doc_series_id         rf_cust_id_doc.series_id%TYPE DEFAULT NULL,   -- ����� ��������������� ��������� �������������  
    par_doc_no_id             rf_cust_id_doc.no_id%TYPE DEFAULT NULL        -- ����� ��������������� ��������� �������������  
  ) 
  RETURN cust.cust_seq_id%TYPE IS -- ID ���������� �������������
    
    var_result   cust.cust_seq_id%TYPE;
  BEGIN
    --
    -- �������� ����������
    --
    if par_cust_seq_id is null Then
      return null;
    end if;
    --
    -- ���� ������������� ���������� �� (��������, � ��������� ��. ����������)
    --
    SELECT max(cust_seq_id)
      INTO var_result
      FROM (select cc.rf_rltd_cust_seq_id as cust_seq_id,
                   row_number() over(order by case when doc.cust_doc_seq_id is not null then 1 else 2 end, -- ������� � ��������� ����������
                                              cc.rlshp_efctv_dt desc nulls last,     -- ������� � ������������ ����� ������ �������� �����
                                              cc.rf_rltd_cust_seq_id desc,           -- ������� � ������������ ��������������� �������������  
                                              case 
                                                --���� ����� �� ��, �� ���� ���������� = [�������� � ������ 3] * 1000000000 + [�������� � ������ 3]
                                                when substr(cc.rf_src_cd, 1, 3) in ('BCK') 
                                                  then to_number(regexp_substr(cc.rf_src_cd, '[[:digit:]]+', instr(cc.rf_src_cd, '|', 1, 2))) * 1000000000 +
                                                       to_number(regexp_substr(cc.rf_src_cd, '[[:digit:]]+$'))
                                                --� ��������� ������� ���� ���������� = [�������� � ������ 2] (��� ����� � ����� ������)
                                                else to_number(regexp_substr(cc.rf_src_cd, '[[:digit:]]+$'))
                                              end desc) as priority,
                   doc.cust_doc_seq_id                           
              from cust c
                   join cust_cust cc on cc.rf_cust_seq_id = c.cust_seq_id and cc.rf_rltd_cust_seq_id is not null
                   join rf_cust_rlshp_type rlt on rlt.rlshp_tp_seq_id = cc.rf_rlshp_tp_seq_id  
                   left join rf_cust_id_doc doc on doc.cust_seq_id = cc.rf_rltd_cust_seq_id and
                                                   business.rf_normalize_docno(doc.series_id, doc.no_id, doc.doc_type_cd) = 
                                                   business.rf_normalize_docno(par_doc_series_id, par_doc_no_id, par_doc_type_cd) and
                                                   nvl(doc.doc_type_cd, -1) = nvl(par_doc_type_cd, -1)
                                                   /*replace(doc.doc_type_cd, ' ') = replace(par_doc_type_cd, ' ') and
                                                   nvl(replace(doc.series_id, ' '), '#&*') = nvl(replace(par_doc_series_id, ' '), '#&*') and                                                    
                                                   replace(doc.no_id, ' ') = replace(par_doc_no_id, ' ')*/ 
             where c.cust_seq_id = par_cust_seq_id and
                   c.cust_type_cd <> 'IND' and
                   rlt.src_cd in ('EKSP|CHIEF_GA', 'EKSP|CHIEF') and
                   -- �� ������� �������������� ���. ����, ��������������� ������� �� (�� �������� �������� ����������)
                   nvl(c.rf_ind_cust_seq_id, -2) <> cc.rf_rltd_cust_seq_id
                   ) 
      WHERE priority = 1 and
            NOT(trim(par_doc_type_cd) is not null and   -- ���� ������ ��. �������� � �� ������, �� ����� ������������� �� �����
                trim(par_doc_series_id) is not null and
                trim(par_doc_no_id) is not null and
                cust_doc_seq_id is null);
      
    return var_result;
  EXCEPTION
    WHEN OTHERS THEN
      return null;  
  END get_representative;
    
   -- ����� ��������� �������� �� ���������� id_cust, ��� ��������� (����� ���� �� ������), ���� ��� �������� ������������ ����� ���������� (�� ������������!)
   -- � ������� ����� �������������� ����������, ������ �� �����������
   -- ����� ������ ������������ �����, ����� ���������� �������� ��� ����� ���������� ��� ��
   --�������� ��� ����������� ����� ��� ?39?! � � ������ ���, ��� ����!
  function get_id_doc(p_id_cust   number,
                         p_doc_type  number default null,
                         p_rights_fl varchar2 default 'Y')
    return number is
  begin
  -- ��� ���� ����������
  -- ���� ����� �� ������� business.rf_cust_id_doc_type
  -- � ���� �� ���������� � ��� ��������� 0 - ���� 12 � 34 ��������������
  -- ������������� ������� ��������� 1 - ��� 13
  -- ������������� � ����������� ����������� � ��������� ���������� �������� ��������� 2 - ��� 11
  -- ������� ������������� ���������� ��������� 3 - ��� 10
  -- ���� ��������� �������������� �������� �� ������� ��������� 4 - ���� 32, 35, 40, 99
  -- ������������ ����� ��������� 5 - ��� 39
  -- null ��������� 7 - ��� null
    for r in (select d.cust_doc_seq_id
                   from business.rf_cust_id_doc d
                 where (d.cust_seq_id = p_id_cust or
                        -- �������� ������� - ���. ����, ���������������� ��
                        d.cust_seq_id = (select rf_ind_cust_seq_id
                                           from business.cust
                                          where cust_seq_id = p_id_cust)) and 
                       (p_doc_type is null or d.doc_type_cd = p_doc_type)
             order by decode(d.valid_fl, 'Y', 1, 2), -- ������� �����������
                      case
                        when p_rights_fl = 'Y' then
                         decode(d.stay_rights_prm_fl, null, '2', 'Y', '1', 'N', '3')
                        else
                         decode(d.stay_rights_prm_fl, null, '2', 'N', '1', 'Y', '3')
                      end,
                      decode(d.doc_type_cd, null, 7, 12, 0, 34, 0, 13, 1, 11, 2, 10, 3, 32, 4, 35, 4, 40, 4, 99, 4, 39, 5),
                      d.issue_dt desc nulls last, -- ��������� �� ���� ��� id
                      d.start_dt desc nulls last, 
                      d.end_dt desc nulls last, 
                      case 
                        --���� ����� �� ��, �� ���� ���������� = [�������� � ������ 3] * 1000000000 + [�������� � ������ 3]
                        when substr(d.src_cd, 1, 3) in ('BCK') 
                          then to_number(regexp_substr(d.src_cd, '[[:digit:]]+', instr(d.src_cd, '|', 1, 2))) * 1000000000 +
                               to_number(regexp_substr(d.src_cd, '[[:digit:]]+$'))
                        --� ��������� ������� ���� ���������� = [�������� � ������ 2] (��� ����� � ����� ������)
                        else to_number(regexp_substr(d.src_cd, '[[:digit:]]+$'))
                      end desc) loop
      return r.cust_doc_seq_id;
    end loop;
    return null;
  exception
    when others then
      null;
  end get_id_doc;
  --
  -- ������������ �������� ������ ������/���� ������ ����� � ��������� ��������
  --
  FUNCTION get_trxn_scrty_desc
  (
    par_op_cat_cd             VARCHAR2,                                     -- ��������� ��������: WT - �����������, CT - ��������  
    par_trxn_seq_id           NUMBER,                                       -- ID �������� (PK ������ WIRE_TRXN/CASH_TRXN)
    par_trxn_scrty_seq_id     NUMBER    DEFAULT NULL                        -- ID ������ ������ � �������� (PK ������ RF_WIRE_TRXN_SCRTY/RF_CASH_TRXN_SCRTY)
                                                                            -- ���� NULL, �� ������������ �������� ���� ������ ����� � ��������
  ) 
  RETURN VARCHAR2 IS -- �������� ������ ������/���� ������ ����� � ��������� ��������
    var_result    VARCHAR2(2000 CHAR);
  BEGIN
    SELECT trim(listagg(scrty_tp_desc||' '||scrty_list, '; ') within group(order by scrty_tp_desc))
      INTO var_result
      FROM (SELECT scrty_tp.code_desc_tx as scrty_tp_desc,
                   listagg(nvl(scrty.scrty_id, '�'||scrty.scrty_nb)||' �����: '||to_char(scrty.scrty_base_am), ', ') within group(order by nvl(scrty.scrty_id, '�'||scrty.scrty_nb)) as scrty_list
              FROM (select scrty_type_cd, scrty_id, scrty_nb, scrty_base_am
                      from business.rf_wire_trxn_scrty 
                     where par_op_cat_cd = 'WT' and
                           fo_trxn_seq_id = par_trxn_seq_id and
                           trxn_scrty_seq_id = nvl(par_trxn_scrty_seq_id, trxn_scrty_seq_id) and
                           nvl(canceled_fl, 'N') <> 'Y'
                    union all       
                    select scrty_type_cd, scrty_id, scrty_nb, scrty_base_am
                      from business.rf_cash_trxn_scrty 
                     where par_op_cat_cd = 'CT' and
                           fo_trxn_seq_id = par_trxn_seq_id and
                           trxn_scrty_seq_id = nvl(par_trxn_scrty_seq_id, trxn_scrty_seq_id) and
                           nvl(canceled_fl, 'N') <> 'Y') scrty
                   left join ref_table_detail scrty_tp on scrty_tp.code_set_id = 'RF_SCRTY_PROD_TYPE' and scrty_tp.code_val1_nm = scrty.scrty_type_cd        
            GROUP BY scrty.scrty_type_cd, scrty_tp.code_desc_tx);           
    
    return var_result;
  END get_trxn_scrty_desc;  

  --
  -- �������� ����� ����� ������� ��� ���������� �� ��������� ���
  --
  function get_oes_addr_tx (
    par_oes_321_id in mantas.rf_oes_321_party.oes_321_id%type,
    par_block_nb   in mantas.rf_oes_321_party.block_nb%type,
    par_addr_tp    in varchar2 default 'AMR' )
  return varchar2 
  is
    v_addr_tx varchar2(4000) := null;
  begin
    if par_oes_321_id is not null and par_block_nb is not null then
      if par_addr_tp = 'AMR' then
        select rtrim(nvl2(c.cntry_nm, c.cntry_nm||', ', null)||
                   nvl2(a.postl_cd, a.postl_cd||' ', null)||
                     nvl2(a.object_nm, a.object_nm||' '||a.addr_type_cd||', ', null)||
                     decode(p.amr_r, '0', null, p.amr_r||', ')||
                     decode(p.amr_g, '0', null, p.amr_g||', ')||
                     decode(p.amr_u, '0', null, p.amr_u||' ')||
                     decode(p.amr_d, '0', null, '��� '||p.amr_d||', ')||
                     decode(p.amr_k, '0', null, '������/���. '||p.amr_k||', ')||
                     decode(p.amr_o, '0', null, '��./��. '||p.amr_o||', '),
                     ', ') addr_tx
               into v_addr_tx      
          from mantas.rf_oes_321_party p
          left join business.rf_country c on c.iso_numeric_cd = substr(p.kodcr, 1, 3)
          left join (select t.object_nm, t.postl_cd, t.addr_type_cd,
                            case 
                              when length(rtrim(t.okato_cd, '0')) < 2 then rpad(rtrim(t.okato_cd, '0'), 2, '0')
                              else rtrim(t.okato_cd, '0')
                            end as region_okato_cd
                       from business.rf_addr_object t) a on trim(p.amr_s) like region_okato_cd||'%'
         where p.oes_321_id = par_oes_321_id
           and p.block_nb = par_block_nb;
      elsif par_addr_tp = 'ADDR' then
        select rtrim(nvl2(c.cntry_nm, c.cntry_nm||', ', null)||
                     nvl2(a.postl_cd, a.postl_cd||' ', null)||
                     nvl2(a.object_nm, a.object_nm||' '||a.addr_type_cd||', ', null)||
                     decode(p.adress_r, '0', null, p.adress_r||', ')||
                     decode(p.adress_g, '0', null, p.adress_g||', ')||
                     decode(p.adress_u, '0', null, p.adress_u||' ')||
                     decode(p.adress_d, '0', null, '��� '||p.adress_d||', ')||
                     decode(p.adress_k, '0', null, '������/���. '||p.adress_k||', ')||
                     decode(p.adress_o, '0', null, '��./��. '||p.adress_o||', '),
                     ', ') addr_tx
          into v_addr_tx      
          from mantas.rf_oes_321_party p
          left join business.rf_country c on c.iso_numeric_cd = substr(p.kodcr, 1, 3)
          left join (select t.object_nm, t.postl_cd, t.addr_type_cd,
                            case 
                              when length(rtrim(t.okato_cd, '0')) < 2 then rpad(rtrim(t.okato_cd, '0'), 2, '0')
                              else rtrim(t.okato_cd, '0')
                            end as region_okato_cd
                       from business.rf_addr_object t) a on trim(p.adress_s) like region_okato_cd||'%'
         where p.oes_321_id = par_oes_321_id
           and p.block_nb = par_block_nb;
      end if;
    end if;
    
    return upper(v_addr_tx);
    
    exception
      when others then 
        return null;
  end;

  --
  -- �������� ����� � ��������� ������� ������� (�� �����, ��� ��������� ���������� �������, ���������, ���������� ����)
  --
  function get_last_addresses_text(
    par_cust_seq_id   business.cust_addr.rf_cust_seq_id%type,
    par_addr_count    number,
    par_addr_usage_cd business.cust_addr.addr_usage_cd%type default null
  ) return varchar
  is
    v_addr_tx varchar2(4000) := null;
  begin

    select listagg(decode(a.addr_usage_cd, 'L', '����� �����������',
                                           'H', '����� ����������',
                                           'O', '������ �����',
                                           'B', '������� �����',
                                           'M', '�������� �����',
                                                '������ �����')||': '||
                     business.rf_pkg_util.get_addr_text(a.cust_addr_seq_id),
                   chr(13)||chr(10))
           within group (order by priority) addr_tx  
           into v_addr_tx
      from (select *
              from (select t.addr_usage_cd,
                           t.cust_addr_seq_id,
                           row_number() over(order by decode(t.rf_active_fl, 'Y', 1, null, 2, 3), -- ������� �������� ������
                                                      decode(t.rf_primary_fl, 'Y', 1, 2), -- ������� ��������
                                                      t.rf_start_dt desc nulls last, -- ��������� �� ���� ��� id
                                                      t.rf_end_dt desc nulls last, 
                                                      case 
                                                        --���� ����� �� ��, �� ���� ���������� = [�������� � ������ 3] * 1000000000 + [�������� � ������ 4]
                                                        when substr(t.rf_src_cd, 1, 4) in ('BCKA', 'BCKP') 
                                                          then to_number(regexp_substr(t.rf_src_cd, '[[:digit:]]+', instr(t.rf_src_cd, '|', 1, 2))) * 1000000000 +
                                                               to_number(regexp_substr(t.rf_src_cd, '[[:digit:]]+$'))
                                                        --� ��������� ������� ���� ���������� = [�������� � ������ 2] (��� ����� � ����� ������)
                                                        else to_number(regexp_substr(t.rf_src_cd, '[[:digit:]]+$'))
                                                      end desc) as priority
                      from business.cust_addr t
                     where t.rf_cust_seq_id = par_cust_seq_id
                       and t.addr_usage_cd = nvl(par_addr_usage_cd, t.addr_usage_cd) ) z
              where z.priority <= par_addr_count ) a;

    return v_addr_tx;
    
    exception
      when others
        then return null;
  end;
--
-- �������� rowid ����� (ACCT.ROWID) �� ��� ������ �� ��������� ����
-- (���� � ��� �� ����� ����� ���� ���������������, �. �. ����� ���� ��������� ������ � ����� � ��� �� �������,
--  ������� ������ ������� ��������)
--
FUNCTION get_acct
(
  par_alt_acct_id       business.acct.ALT_ACCT_ID%TYPE,            -- ����� ����� (������, 20 ����)
  par_date              DATE,                                      -- ����, �� ������� ���� ����� ���� 
  par_primary_sys_cd    business.acct.SRC_SYS_CD%TYPE DEFAULT null -- ��� ���������������� ������� ��������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
) 
RETURN ROWID PARALLEL_ENABLE IS -- rowid ������, ��������������� ����� (ACCT.ROWID), null, ���� ���� �� ������
  var_rowid   ROWID;
BEGIN
  --
  -- ���� �� ������ � ��������� �������, ����������� �� ��������� ���� � ������� ����������:
  --  - ������� ����� �� ��������� ���������������� �������-���������
  --  - ������� - ����� ������� ����� (�� ���� ��������/��������, ��������������)
  --
  FOR r IN (SELECT /*+ NO_PARALLEL INDEX(a)*/ rowid as row_id
              FROM business.acct a
             WHERE alt_acct_id = par_alt_acct_id and
                   par_date between coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) and
                                    coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) and
                   nvl(acct_stat_cd, '?') <> 'X' -- ���� �� ������
            ORDER BY case when src_sys_cd = par_primary_sys_cd then 1 else 2 end,
                     coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) desc,
                     coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) desc,
                     acct_seq_id desc) LOOP
    var_rowid := r.row_id;
    EXIT;         
  END LOOP;  
  
  return var_rowid;         
END get_acct;
--
-- �������� rowid ����� (ACCT.ROWID) �� ��������� ���������� 
-- ���� ����� �����������:
--   - ��������������� ACCT_SEQ_ID
--   - ������� � ���/SWIFT �����
--   - ������� � ��������������� CUST_SEQ_ID �������-��������� 
-- ������� ���������� null ��� ������, �� ����������� � �� ��
--
FUNCTION get_acct2
(
  par_acct_seq_id       business.acct.ACCT_SEQ_ID%TYPE,              -- ID ����� (�� ����������� ������)
  par_alt_acct_id       business.acct.ALT_ACCT_ID%TYPE DEFAULT null, -- ����� ����� (������, 20 ����)
  par_bic_nb            VARCHAR2 DEFAULT null,                       -- ���/SWIFT �����, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
  par_cust_seq_id       business.cust.CUST_SEQ_ID%TYPE DEFAULT null, -- ID ������� - ��������� �����
  par_date              DATE DEFAULT null,                           -- ����, �� ������� ���� ����� ���� �� ������
  par_primary_sys_cd    business.acct.SRC_SYS_CD%TYPE DEFAULT null   -- ��� ���������������� ������� ��������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
) 
RETURN ROWID PARALLEL_ENABLE IS  -- rowid �����, null, ���� ���� �� ������
  var_rowid   ROWID;
BEGIN
  --
  -- ���� �� ������� ������� ����������, �� ������� ����� ���������������� ���� - ������� null
  --
  if par_acct_seq_id is null and                               -- ������ ����� ���� �� ��������������
     (par_alt_acct_id is null or par_bic_nb is null) and       -- ������ ����� ���� �� ������ � ���
     (par_alt_acct_id is null or par_cust_seq_id is null) Then -- ������ ����� ���� �� ������ � ��������� 
    return null;
  end if;
  --
  -- �����������: ��������� �������� ROWID ������ �� �������������� ����� (��� �������������� ������ ������)
  --
  if par_acct_seq_id <> -1 Then
    SELECT /*+ NO_PARALLEL INDEX(a)*/ max(rowid)
      INTO var_rowid
      FROM business.acct a
     WHERE acct_seq_id = par_acct_seq_id and
           (par_alt_acct_id is null or alt_acct_id = par_alt_acct_id); -- ��������� ���������� ������ �����

    if var_rowid is not null Then
      return var_rowid;         
    end if;  
  end if;             
  --
  -- ������ ������ ������
  -- ���� �� ���������� ��� ��������� ������, ����������� �� ��������� ���� � ������� ����������:
  --  - ������� ���� � ��������� ���������������, ���� ��� ����� �� ������������ ����������
  --  - ������� ����� �� ��������� ���������������� �������-���������
  --  - ������� - ����� ������� ����� (�� ���� ��������/��������, ��������������)
  --
  FOR r IN (SELECT /*+ NO_PARALLEL INDEX(a)*/ 
                   rowid as row_id,
                   case when par_alt_acct_id is null or alt_acct_id = par_alt_acct_id
                        then 1 
                        else 4
                   end as priority,
                   coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) as acct_open_dt,
                   coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) as acct_close_dt,
                   acct_seq_id
              FROM business.acct a
             WHERE acct_seq_id = par_acct_seq_id and
                   par_acct_seq_id <> -1 -- ��������� ���� �� ���������
            UNION ALL 
            SELECT /*+ NO_PARALLEL INDEX(a)*/ 
                   rowid as row_id,
                   case when src_sys_cd = par_primary_sys_cd 
                        then 2 
                        else 3 
                   end as priority,
                   coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) as acct_open_dt,
                   coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) as acct_close_dt,
                   acct_seq_id
              FROM business.acct a
             WHERE alt_acct_id = par_alt_acct_id and
                   par_bic_nb is not null and
                   (par_bic_nb = 'SBRF' or mantas.rf_pkg_rule.is_sbrf(par_bic_nb) = 1) and -- ���� ������ � �� ��
                   (par_date is null or
                    par_date between coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) and
                                     coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))) and
                   nvl(acct_stat_cd, '?') <> 'X' -- ���� �� ������
            UNION ALL 
            SELECT /*+ NO_PARALLEL INDEX(a)*/ 
                   rowid as row_id,
                   case when src_sys_cd = par_primary_sys_cd 
                        then 2 
                        else 3 
                   end as priority,
                   coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) as acct_open_dt,
                   coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) as acct_close_dt,
                   acct_seq_id
              FROM business.acct a
             WHERE alt_acct_id = par_alt_acct_id and
                   par_cust_seq_id is not null and 
                   rf_prmry_cust_seq_id = par_cust_seq_id and
                   (par_date is null or
                    par_date between coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) and
                                     coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))) and
                   nvl(acct_stat_cd, '?') <> 'X' -- ���� �� ������
            ORDER BY priority,
                     acct_open_dt desc,
                     acct_close_dt desc,
                     acct_seq_id desc) LOOP
    var_rowid := r.row_id;
    EXIT;
  END LOOP;
  
  return var_rowid;         
END get_acct2;
--
-- ���������� ��� BRANCH_ID (��� �� * 100000 + ��� ���) ��� ���������� ����� 
-- ���� ����� �����������:
--   - ��������������� ACCT_SEQ_ID
--   - ������� � ���/SWIFT �����
--   - ������� � ��������������� CUST_SEQ_ID �������-��������� 
-- ������� ���������� null ��� ������, �� ����������� � �� ��
--
FUNCTION get_acct_branch_id
(
  par_acct_seq_id       business.acct.ACCT_SEQ_ID%TYPE,              -- ID ����� (�� ����������� ������)
  par_alt_acct_id       business.acct.ALT_ACCT_ID%TYPE DEFAULT null, -- ����� ����� (������, 20 ����)
  par_bic_nb            VARCHAR2 DEFAULT null,                       -- ���/SWIFT �����, � ������� ������ ����, ������ ��������� 'SBRF' ������������, ��� ��������� ����� ����� �������������� ��������� � �� ��, ��������������, ���/SWIFT ��������� �� �����
  par_cust_seq_id       business.cust.CUST_SEQ_ID%TYPE DEFAULT null, -- ID ������� - ��������� �����
  par_date              DATE DEFAULT null,                           -- ����, �� ������� ���� ����� ���� �� ������
  par_primary_sys_cd    business.acct.SRC_SYS_CD%TYPE DEFAULT null   -- ��� ���������������� ������� ��������� - ���� ����� ��������� ����������� ������ � ����� �������, �� ������������ ����� ������ �����, ������������ �� ��������� �������
) 
RETURN business.acct.RF_BRANCH_ID%TYPE PARALLEL_ENABLE IS  -- ��� BRANCH_ID, ��������������� �����, null, ���� ���� �� ������
  var_result    business.acct.RF_BRANCH_ID%TYPE;
BEGIN
  --
  -- ���� �� ������� ������� ����������, �� ������� ����� ���������������� ���� - ������� null
  --
  if par_acct_seq_id is null and                               -- ������ ����� ���� �� ��������������
     (par_alt_acct_id is null or par_bic_nb is null) and       -- ������ ����� ���� �� ������ � ���
     (par_alt_acct_id is null or par_cust_seq_id is null) Then -- ������ ����� ���� �� ������ � ��������� 
    return null;
  end if;
  --
  -- �����������: ��������� �������� BRANCH_ID ������ �� �������������� ����� (��� �������������� ������ ������)
  --
  if par_acct_seq_id <> -1 Then
    SELECT /*+ NO_PARALLEL INDEX(a)*/ max(rf_branch_id)
      INTO var_result
      FROM business.acct a
     WHERE acct_seq_id = par_acct_seq_id and
           (par_alt_acct_id is null or alt_acct_id = par_alt_acct_id); -- ��������� ���������� ������ �����

    if var_result is not null Then
      return var_result;
    end if;  
  end if;             
  --
  -- ������ ������ ������
  -- ���� �� ���������� ��� ��������� ������, ����������� �� ��������� ���� � ������� ����������:
  --  - ������� ���� � ��������� ���������������, ���� ��� ����� �� ������������ ����������
  --  - ������� ����� �� ��������� ���������������� �������-���������
  --  - ������� - ����� ������� ����� (�� ���� ��������/��������, ��������������)
  --
  FOR r IN (SELECT /*+ NO_PARALLEL INDEX(a)*/ 
                   rf_branch_id,
                   case when par_alt_acct_id is null or alt_acct_id = par_alt_acct_id
                        then 1 
                        else 4
                   end as priority,
                   coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) as acct_open_dt,
                   coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) as acct_close_dt,
                   acct_seq_id
              FROM business.acct a
             WHERE acct_seq_id = par_acct_seq_id and
                   par_acct_seq_id <> -1 -- ��������� ���� �� ���������
            UNION ALL 
            SELECT /*+ NO_PARALLEL INDEX(a)*/ 
                   rf_branch_id,
                   case when src_sys_cd = par_primary_sys_cd 
                        then 2 
                        else 3 
                   end as priority,
                   coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) as acct_open_dt,
                   coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) as acct_close_dt,
                   acct_seq_id
              FROM business.acct a
             WHERE alt_acct_id = par_alt_acct_id and
                   par_bic_nb is not null and
                   (par_bic_nb = 'SBRF' or mantas.rf_pkg_rule.is_sbrf(par_bic_nb) = 1) and -- ���� ������ � �� ��
                   (par_date is null or
                    par_date between coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) and
                                     coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))) and
                   nvl(acct_stat_cd, '?') <> 'X' -- ���� �� ������
            UNION ALL 
            SELECT /*+ NO_PARALLEL INDEX(a)*/ 
                   rf_branch_id,
                   case when src_sys_cd = par_primary_sys_cd 
                        then 2 
                        else 3 
                   end as priority,
                   coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) as acct_open_dt,
                   coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy')) as acct_close_dt,
                   acct_seq_id
              FROM business.acct a
             WHERE alt_acct_id = par_alt_acct_id and
                   par_cust_seq_id is not null and 
                   rf_prmry_cust_seq_id = par_cust_seq_id and
                   (par_date is null or
                    par_date between coalesce(acct_open_dt, to_date('01.01.0001', 'dd.mm.yyyy')) and
                                     coalesce(acct_close_dt, to_date('31.12.9999', 'dd.mm.yyyy'))) and
                   nvl(acct_stat_cd, '?') <> 'X' -- ���� �� ������
            ORDER BY priority,
                     acct_open_dt desc,
                     acct_close_dt desc,
                     acct_seq_id desc) LOOP
    var_result := r.rf_branch_id;
    EXIT;
  END LOOP;
  
  return var_result;  
END get_acct_branch_id;
--
-- ��������� �������� �������������, � ������� ��������� ��������
-- � ����������� �� ������� � �������� ��������� par_branch_id ����������:
--  - ������������ ��� - ���� par_branch_id ��������� � ���� �� ��-���, ��� � par_org_intrl_id
--  - ������������ ���, ������������ ��� - ���� par_branch_id ��������� � ���� �� ��, ��� � par_org_intrl_id, �� � ������� ���
--  - ������������ ��, ������������ ���, ������������ ��� - ���� par_branch_id �� ������ ��� ��������� � ������� ��, ��� par_org_intrl_id
-- ������� ������������ �� ���������� ������������, ��� ������ � ������������ ��/��� � ������������ � par_branch_id ��� ������������  
--
FUNCTION get_trxn_org_desc
(
  par_org_intrl_id      business.cash_trxn.TRXN_BRNCH_ID%TYPE,             -- ORG_INTRL_ID �������������, � ������� ��������� ��������
  par_branch_id         business.cash_trxn.RF_BRANCH_ID%TYPE DEFAULT null  -- BRANCH_ID �������� - ������������ ��� ���������� � �������������� �������� ������������� ������������ ������������� (��, ���)
) 
RETURN VARCHAR2 RESULT_CACHE PARALLEL_ENABLE IS -- �������� �������������, ��� �������� ������ � par_org_intrl_id (� ����������� �������� ������������� � par_branch_id)
  var_result   VARCHAR2(2000 CHAR);
BEGIN
  if par_org_intrl_id is null Then
    return null;  
  end if;
  
  SELECT case when t.tb = t.b_tb and t.osb = t.b_osb
              then nvl(vsp.org_nm, t.vsp)
              when t.tb = t.b_tb
              then trim(',' from trim(nvl(osb.org_nm, t.osb)||', '||nvl(vsp.org_nm, t.vsp)))
              else trim(',' from trim(nvl(tb.org_nm, t.tb)||', '||nvl(osb.org_nm, t.osb)||', '||nvl(vsp.org_nm, t.vsp)))
         end
    INTO var_result     
    FROM (select regexp_substr(par_org_intrl_id, '\d+', 1, 1) as tb,
                 regexp_substr(par_org_intrl_id, '\d+', 1, 2) as osb,
                 regexp_substr(par_org_intrl_id, '\d+', 1, 3) as vsp,
                 to_char(trunc(par_branch_id/100000))              as b_tb,
                 to_char(par_branch_id - trunc(par_branch_id, -5)) as b_osb
            from dual) t
         left join business.org tb  on  tb.org_intrl_id = t.tb
         left join business.org osb on osb.org_intrl_id = t.tb||'-'||t.osb
         left join business.org vsp on vsp.org_intrl_id = t.tb||'-'||t.osb||'-'||t.vsp;

  return var_result;        
END get_trxn_org_desc;
end RF_PKG_UTIL;
/

grant EXECUTE on BUSINESS.RF_PKG_UTIL to DATA_READER;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to KDD_ALGORITHM;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to KDD_ANALYST;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to KDD_MINER;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to RF_RSCHEMA_ROLE;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to CMREVMAN;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to KDD_MNR with grant option;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to KDD_REPORT with grant option;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to KYC;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to MANTAS;

grant EXECUTE on BUSINESS.RF_PKG_UTIL to STD;

