create or replace package business.rf_pkg_import
is
  /* ����� ���������� ����� � ��������.
   *   business.rf_cbrf_bnkseek
   *   business.rf_cbrf_bnkdel
   */
  procedure imp_cbrf_bnk;
  
end;
/

create or replace package body business.rf_pkg_import
is

  /* ����� ���������� ����� � ��������.
   *   business.rf_cbrf_bnkseek
   *   business.rf_cbrf_bnkdel
   */
  procedure imp_cbrf_bnk
  is
  begin
    delete /*+ PARALLEL(4) */ from business.rf_cbrf_bnkseek;
    insert /*+ PARALLEL(4) */ into business.rf_cbrf_bnkseek
    select vkey, real, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen, newnum,
           newks, permfo, srok, at1, at2, telef, regn, okpo, to_date(dt_izm, 'YYYYMMDD'),
           cks,ksnp, to_date(date_in, 'YYYYMMDD'), to_date(date_ch, 'YYYYMMDD'), vkeydel,
           to_date(dt_izmr, 'YYYYMMDD')
    from business.rf_cbrf_bnkseek_dbf;
      
    delete /*+ PARALLEL(4) */ from business.rf_cbrf_bnkdel;
    insert /*+ PARALLEL(4) */ into business.rf_cbrf_bnkdel
    select vkey, vkeydel, pzn, uer, rgn, ind, tnp, nnp, adr, rkc, namep, namen,
           newnum, newks, permfo, srok, at1, at2, telef, regn, okpo, 
           to_date(datedel, 'YYYYMMDD'), to_date(dt_izm, 'YYYYMMDD'), cks, ksnp, 
           r_close, to_date(date_in, 'YYYYMMDD') 
      from business.rf_cbrf_bnkdel_dbf;
    
    commit;
    
    exception
      when others then
        rollback;
        raise;
  end;
  
end;
/