--
-- ��������! ��� ��������� ��������� ������� ������� ����������� �������, ������������ ��� �������:
--   BUSINESS: RF_CUST_FIO_BIRTH_I, RF_CUST_FI_BIRTH_I, RF_CUST_F_BIRTH_I
--   MANTAS: RF_OES321P_NAMEU_GR_I
-- ���������: ��� �� ������ ����� �������� !!!
--
-- ������� ������� �� ���������� ������ ������� � ����. �������, ��������� ������ � ������� �������,
-- ���� ������ ������� �� ���� (0), �� ���������� �� � null
-- ������������ � �������� �� ��� ��� ������ �������
--
CREATE OR REPLACE 
FUNCTION business.rf_normalize_name
(
  par_name      VARCHAR2
)
RETURN VARCHAR2 DETERMINISTIC IS
  var_name      VARCHAR2(2000 CHAR);
BEGIN
  var_name := upper(translate(par_name, 'A !@#$%^&*()_-+={}[]:;"''<>,.?/|\`~', 'A'));
  return nullif(var_name, '0');
END rf_normalize_name;
/

grant EXECUTE on BUSINESS.rf_normalize_name to MANTAS with grant option;
grant EXECUTE on BUSINESS.rf_normalize_name to DATA_READER;
grant EXECUTE on BUSINESS.rf_normalize_name to KDD_ALGORITHM;
grant EXECUTE on BUSINESS.rf_normalize_name to KDD_ANALYST;
grant EXECUTE on BUSINESS.rf_normalize_name to KDD_MINER;
grant EXECUTE on BUSINESS.rf_normalize_name to RF_RSCHEMA_ROLE;
grant EXECUTE on BUSINESS.rf_normalize_name to CMREVMAN;
grant EXECUTE on BUSINESS.rf_normalize_name to KDD_MNR with grant option;
grant EXECUTE on BUSINESS.rf_normalize_name to KDD_REPORT with grant option;
grant EXECUTE on BUSINESS.rf_normalize_name to KYC;
grant EXECUTE on BUSINESS.rf_normalize_name to STD;

