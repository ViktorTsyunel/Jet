-- ��������� ����� ������� ������� �� ��:
-- UPDATE std.src_replicas
--   SET repl_name = 'AML_EKS'
-- WHERE repl_name = 'T54AML_EKS';
-- commit;

BEGIN
delete from std.src_replica_consistency;
delete from std.src_replicas;
--
-- RCOD - ���� �� 14 ��������� (= 14 ������� ������-������)
--
FOR r IN (SELECT decode(level, 1,13,  2,16,  3,18,  4,38,  5,40,  6,42, 7,44, 8,49,
                               9,52, 10,54, 11,55, 12,67, 13,70, 14,77) as tb_id 
            FROM dual
          CONNECT BY level <= 14) LOOP
          
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##CLIENT', '##', to_char(r.tb_id)), 'ADDRESS', 'R', 11, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##CLIENT', '##', to_char(r.tb_id)), 'EDBO', 'R', 13, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##CLIENT', '##', to_char(r.tb_id)), 'PB_ELADR', 'A', null, null, null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##CLIENT', '##', to_char(r.tb_id)), 'PERSON', 'R', 10, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##CLIENT', '##', to_char(r.tb_id)), 'PERSONCARD', 'R', 12, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'CLERK_FIO', 'A', null, null, null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'DEPOHEIR', 'R', 21, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'DEPOHIST_EXT', 'R', 30, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'DEPOINVESTOR', 'R', 22, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'DEPOKEY98', 'A', null, null, null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'DEPOSIT', 'R', 20, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'DEPOTRUSTED', 'R', 23, 'ID_MEGA', 'ID_MAJOR', 'ID_MINOR');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##DEPOSIT', '##', to_char(r.tb_id)), 'PD', 'A', null, null, null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##INTEGRATOR', '##', to_char(r.tb_id)), 'PAYMENT', 'A', null, null, null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'CPRICE', 'R', null, 'PR_DATE', 'PR_XISO', null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'CURRENCY', 'R', null, 'CR_ISO', null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'METAL', 'R', null, 'MT_NUM', null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'MMPRICE', 'R', null, 'PR_DATE', 'PR_NUM', null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'NEW_QVB', 'R', null, 'QDTN1', 'QDTSUB', 'QVAL');
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'PBAGENCY', 'R', null, 'AGBRANCH', 'AGNUMINT', null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'PBBRANCH', 'R', null, 'BRNUM', null, null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'TERBNK', 'R', null, 'TB_CODREG', 'TB_CODOSB', null);
  insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values (replace('RCOD0##NSI', '##', to_char(r.tb_id)), 'TERBNKN', 'R', null, 'ID_MAJOR', 'ID_MINOR', null);

  -- �������, ������������ ������ � ������� ����������� �����
  if r.tb_id = 38 Then
    insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RCOD038NSI', 'BNKBICN', 'A', null, null, null, null);
    insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RCOD038NSI', 'CAS_PB_DOCUMENT', 'R', null, 'CODE', null, null);
    insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RCOD038NSI', 'COUNTRY2', 'R', null, 'ISO2', null, null);
    insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RCOD038NSI', 'NCIOD', 'R', null, 'KOD_OP', null, null);
    insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RCOD038NSI', 'NCIOP', 'R', null, 'SHFR0', null, null);
    insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RCOD038NSI', 'SUBBRANCH', 'R', null, 'ID_MAJOR', 'ID_MINOR', null);
  end if;
END LOOP;  
--
-- REKS
--
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'STATES', 'R', null, 'CLASS_ID', 'ID', null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#AC_ARREST', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#AC_FIN', 'R', 30, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#ADDRESS_TYPE', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#BASE_VAL_OP', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#BRANCH', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CASTA', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CERTIFICATE', 'R', 13, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CERTIFIC_TYPE', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CL_BANK', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CL_BANK_N', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CL_CORP', 'R', 10, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CLIENT', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CL_LINK', 'R', 20, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CL_ORG', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CL_PRIV', 'R', 11, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#COGNATE', 'R', 21, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#COM_STATUS_PRD', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#COMUNICATION', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#CONTACTS', 'R', 17, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#COUNTRY', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#DEPART', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#DOCUM_RC', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#EDICATION', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#FAMILY_TYPE', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#FORM_PROPERTY', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#FT_MONEY', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#GOZ_DOC_IN', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#GOZ_DOC_REQS', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#GOZ_VID_OPER', 'R', null, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#GOZ_VOP_HISTORY', 'R', 51, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#LINKS_CL', 'R', 22, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'R', 40, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#MIGRATION_CARD', 'R', 14, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#NAME_PAYDOC', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#NAMES_CITY', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#OKOGU', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#OKONH', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#OKONH_REF', 'R', 15, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#OKVED', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#OKVED_REF', 'R', 16, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#OWNER_COM', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#OWNERSHIP_TYPE', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#PEOPLE_PLACE', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'R', 12, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#PERSONS_POS', 'R', 23, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#REGION', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#RELATIVES', 'R', 24, 'ID', null, null, 'T54AML_EKS', 1);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#SEX', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#SPRAV_STREET', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#SPR_SUB_SYM_KS', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#SPR_SYM_KS', 'R', null, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#STREET_NAME', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#SUBBRANCH', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#SUM_SYMKS', 'R', 50, 'ID', null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#TERRITORY', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#TYPE_PAYDOC', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#USER', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#VIP_CLIENT', 'A', null, null, null, null, 'T54AML_EKS', 0);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03, repl_name, consistency_obligatory_flag) values ('REKS000IBS', 'Z#WORK_ACTIV', 'R', 9, 'ID', null, null, 'T54AML_EKS', 0);

-- ����������� ���������������
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#AC_ARREST', '������ �� ������ ����� ������ �� ����� (���� �� ������� "�� ������ ������")');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#CLIENT', '��������� �����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#COM_STATUS_PRD', '������� ������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#DEPART', '������������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#FT_MONEY', '������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#SUBBRANCH', '������������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#AC_FIN', 'REKS000IBS', 'Z#USER', '������������ - ������������ ���������, ����������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CERTIFICATE', 'REKS000IBS', 'Z#CERTIFIC_TYPE', '���� ��. ���������� - ��������� ���������� + ��� ���������� ������� ������������� ����� ��� ��������� ����������� �� �����');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CERTIFICATE', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#CL_BANK', '������������ ��������� ���������� ������. ������ ������ �� �������: ��� SWIFT �������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#CL_BANK_N', '������������ ��������� ���������� ������. ������ ������ �� �������: ��� ��� �������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#CLIENT', '��������� �����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#CL_ORG', '������ ������ �� �������: ��� ��� �������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#COUNTRY', '������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#FORM_PROPERTY', '��������������-�������� ����� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#OKOGU', '������������� ����� - ��������� ���������� (������-�� ����)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#OKVED', '������������� ����� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#OWNERSHIP_TYPE', '����� ������������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#USER', '������������ - ������������ ���������, ����������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_CORP', 'REKS000IBS', 'Z#VIP_CLIENT', '������ ������ �� �������: ���� VIP-�������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#CASTA', '������ ������ �� �������: ��������� (��� ���. ���)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#CLIENT', '��������� �����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#COUNTRY', '������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#EDICATION', '������ ����������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#FAMILY_TYPE', '�������� ��������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#NAMES_CITY', '������ (������� ���������� ����������), ������ �� �������: ����� ��������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#PEOPLE_PLACE', '���� ���������� ������� - ��������� ����������, ������ �� �������: ����� ��������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#REGION', '������ (�������) - ��������� ����������, ������ �� �������: ����� ��������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#SEX', '��� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#TERRITORY', '������ �� �������: ����� ��������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#USER', '������������ - ������������ ���������, ����������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#VIP_CLIENT', '������ ������ �� �������: ���� VIP-�������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CL_PRIV', 'REKS000IBS', 'Z#WORK_ACTIV', '������ ������ �� �������: ��������� (��� �������������� ��)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CONTACTS', 'REKS000IBS', 'Z#CLIENT', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CONTACTS', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CONTACTS', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CONTACTS', 'REKS000IBS', 'Z#COMUNICATION', '���� ��������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#CONTACTS', 'REKS000IBS', 'Z#OWNER_COM', '���� ��������� � ������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#GOZ_VOP_HISTORY', 'REKS000IBS', 'Z#MAIN_DOCUM', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#LINKS_CL', 'REKS000IBS', 'Z#CLIENT', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#LINKS_CL', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#LINKS_CL', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#LINKS_CL', 'REKS000IBS', 'Z#CL_LINK', '��������� �����������. ���� ������ ����� ��������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#AC_FIN', '��������� �����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#BRANCH', '������������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#CERTIFIC_TYPE', '���� ��. ���������� - ��������� ���������� + ��� ���������� ������� ������������� ����� ��� ��������� �������������� �� �����');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#CL_BANK_N', '��������� �����������'); 
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#CLIENT', '��������� �����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#COUNTRY', '������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#DEPART', '������������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#FT_MONEY', '������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#NAME_PAYDOC', '���� ���������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#SUBBRANCH', '������������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MAIN_DOCUM', 'REKS000IBS', 'Z#USER', '������������ - ������������ ���������, ����������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#MIGRATION_CARD', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKONH_REF', 'REKS000IBS', 'Z#CLIENT', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKONH_REF', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKONH_REF', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKONH_REF', 'REKS000IBS', 'Z#OKONH', '������������� ����� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKVED_REF', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKVED_REF', 'REKS000IBS', 'Z#CLIENT', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKVED_REF', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#OKVED_REF', 'REKS000IBS', 'Z#OKVED', '������������� ����� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#ADDRESS_TYPE', '���� ������� - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#CLIENT', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, ��������� ����������� �� Z#CLIENT');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#COUNTRY', '������ - ��������� ����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#NAMES_CITY', '��������� �����������'); 
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#PEOPLE_PLACE', '��������� �����������'); 
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#REGION', '��������� �����������'); 
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#SPRAV_STREET', '��������� �����������'); 
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#STREET_NAME', '��������� �����������'); 
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONAL_ADDRESS', 'REKS000IBS', 'Z#TERRITORY', '��������� �����������'); 
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONS_POS', 'REKS000IBS', 'Z#CASTA', '������ �� �������: ��������� (��� �������������� ��)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONS_POS', 'REKS000IBS', 'Z#CL_CORP', '��������� �����������, �� ��������� �������� (���������)');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#PERSONS_POS', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������');
insert into std.src_replica_consistency(owner, table_name, consist_owner, consist_table_name, note) values ('REKS000IBS', 'Z#RELATIVES', 'REKS000IBS', 'Z#CL_PRIV', '��������� �����������, �� ��������� �������� (���������)');
--
-- RMDC
--
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDADMINSYSTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDCLIENTIMPTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDCLIENTSTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDCOUNTRYTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDENDREASONTP', 'F', null, 'MDM_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDOKFSTP', 'F', null, 'MDM_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDORGCATTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDORGNAMETP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDORGTP', 'F', null, 'MDM_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDRELTP', 'F', null, 'MDM_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDSBCREDITCAPACITYTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDSBCREDITRISKTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDSBINDUSTRYTP', 'F', null, 'MDM_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDSBORGBUSSEGMENTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDSBORGCODETP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDSBORGSTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CDSBORGTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CONTACT', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CONTACTREL', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'CONTEQUIV', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'ORG', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'ORGNAME', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'PERSON', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'PERSONNAME', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDC000MDM', 'SBORGCODES', 'A', null, null, null, null);
--
-- RMDR
--
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'ADDRESS', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'ADDRESSGROUP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDADDRUSAGETP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDADMINSYSTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDAGREEMENTSTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDAGREEMENTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCLIENTIMPTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCLIENTSTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCONTMETHCAT', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCONTMETHTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCONTRACTROLETP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCONTRACTSTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCONTRCOMPTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCOUNTRYTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDCURRENCYTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDDOMAINVALUETP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDHIGHESTEDUTP', 'F', null, 'MDM_ID', 'LANG_TP_CD', null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDIDSTATUSTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDIDTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDLANGTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDMARITALSTTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDMETHODSTATUSTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDPRODTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDSBBANKRELTP', 'F', null, 'MDM_ID', 'LANG_TP_CD', null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDSBSOCIALGROUPTP', 'F', null, 'MDM_ID', 'LANG_TP_CD', null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CDSBSUBDIVISIONTP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CONTACT', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CONTACTMETHOD', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CONTACTMETHODGROUP', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'CONTEQUIV', 'F', null, 'ADMIN_SYS_TP_CD', 'ADMIN_CLIENT_ID', 'CONT_ID');
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'IDENTIFIER', 'F', 12, 'IDENTIFIER_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'LOCATIONGROUP', 'F', 11, 'LOCATION_GROUP_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'PERSON', 'F', 10, 'CONT_ID', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RMDR000MDM', 'PERSONNAME', 'A', null, null, null, null);
--
-- RNAR
--
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'ACCOUNT', 'R', 20, 'CLASSIFIED', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'ACCOUNTTYPE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'BALANCE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'BANKCODE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CLBANKREL', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CLIENT', 'R', 10, 'CLASSIFIED', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CLIENTREGIONTREE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CLIENTTYPE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CODESYSTEM', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CORRACCOUNT', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CURRENCY', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'CUSTOMERTRANSFER_EXT', 'R', 30, 'CLASSIFIED_DOCTREE', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'DESCLIST', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'DOCCATEGORY', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'DOCSTATE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'DOCTREE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'DOCTREE_DICTS', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'DOCTYPE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'ENUMTYPEVALUE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'LINECODE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'OBJDESC', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'OBJENUM', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'PAYMENTORDER_EXT', 'R', 31, 'CLASSIFIED_DOCTREE', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'PROPLIST', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'QUESTVALUE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'REPORTSCHEMA', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'RO', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'TYPETREE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAR000OD', 'USERS', 'A', null, null, null, null);
--
-- RNAV
--
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'ACCOUNT', 'R', 20, 'CLASSIFIED', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'ACCOUNTTYPE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'BALANCE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'BANKCODE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CLBANKREL', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CLIENT', 'R', 10, 'CLASSIFIED', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CLIENTREGIONTREE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CLIENTTYPE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CODESYSTEM', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CORRACCOUNT', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CURRENCY', 'R', null, 'CLASSIFIED', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CURREXCHANGE', 'R', 30, 'CLASSIFIED', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'CUSTOMERTRANSFER_EXT', 'R', 40, 'CLASSIFIED_DOCTREE', null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'DESCLIST', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'DOCCATEGORY', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'DOCSTATE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'DOCTREE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'DOCTREE_DICTS', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'DOCTYPE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'ENUMTYPEVALUE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'LINECODE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'OBJDESC', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'OBJENUM', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'PROPLIST', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'QUESTVALUE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'REPORTSCHEMA', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'RO', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'TYPETREE', 'A', null, null, null, null);
insert into std.src_replicas(owner, table_name, load_type, order_number, source_id01, source_id02, source_id03) values ('RNAV000OD', 'USERS', 'A', null, null, null, null);

commit;
END;
/
----------------------------------------
-- SQL/PLSQL �������� ��������������� --
----------------------------------------
--
-- REKS000IBS.Z#MAIN_DOCUM
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#MAIN_DOCUM, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(md) USE_NL(orig_c orig_corp orig_priv benef_c benef_corp benef_priv orig_a benef_a deb_a cred_a orig_b benef_b orig_bc benef_bc)*/
           md.rowid
      FROM reks000ibs.z#main_docum #PART_SUBPART# md
           left join reks000ibs.z#client  orig_c    on orig_c.id     = md.c_kl_dt#1#1
           left join reks000ibs.z#cl_corp orig_corp on orig_corp.id  = md.c_kl_dt#1#1
           left join reks000ibs.z#cl_priv orig_priv on orig_priv.id  = md.c_kl_dt#1#1
           left join reks000ibs.z#client benef_c    on benef_c.id    = md.c_kl_kt#1#1
           left join reks000ibs.z#client benef_corp on benef_corp.id = md.c_kl_kt#1#1
           left join reks000ibs.z#client benef_priv on benef_priv.id = md.c_kl_kt#1#1
           left join reks000ibs.z#ac_fin orig_a  on orig_a.id  = md.c_kl_dt#1#2
           left join reks000ibs.z#ac_fin benef_a on benef_a.id = md.c_kl_kt#1#2
           left join reks000ibs.z#ac_fin deb_a  on deb_a.id  = md.c_acc_dt
           left join reks000ibs.z#ac_fin cred_a on cred_a.id = md.c_acc_kt
           left join reks000ibs.z#cl_bank_n orig_b  on orig_b.id   = md.c_kl_dt#2#3
           left join reks000ibs.z#client orig_bc    on orig_bc.id  = md.c_kl_dt#2#3
           left join reks000ibs.z#cl_bank_n benef_b on benef_b.id  = md.c_kl_kt#2#3
           left join reks000ibs.z#client benef_bc   on benef_bc.id = md.c_kl_kt#2#3
     WHERE md.aml_load_status = 1 and
           md.aml_active_partition_flag = 1 and 
           nvl(md.aml_repeat_load_flag, 0) = 0 and
           ((md.c_kl_dt#1#1 is not null and orig_c.rowid is null) or
            (md.c_kl_dt#1#1 is not null and orig_corp.rowid is null and
             orig_c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or 
            (md.c_kl_dt#1#1 is not null and orig_priv.rowid is null and
             orig_c.class_id in ('CL_PRIV')) or 
            (md.c_kl_kt#1#1 is not null and benef_c.rowid is null) or
            (md.c_kl_kt#1#1 is not null and benef_corp.rowid is null and
             benef_c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
            (md.c_kl_kt#1#1 is not null and benef_priv.rowid is null and
             benef_c.class_id in ('CL_PRIV')) or                        
            (md.c_kl_dt#1#2 is not null and orig_a.rowid is null) or
            (md.c_kl_kt#1#2 is not null and benef_a.rowid is null) or
            (md.c_acc_dt is not null and deb_a.rowid is null) or
            (md.c_acc_kt is not null and cred_a.rowid is null) or            
            (md.c_kl_dt#2#3 is not null and orig_b.rowid is null) or
            (md.c_kl_dt#2#3 is not null and orig_b.rowid is not null and orig_bc.rowid is null) or
            (md.c_kl_kt#2#3 is not null and benef_b.rowid is null) or
            (md.c_kl_kt#2#3 is not null and benef_b.rowid is not null and benef_bc.rowid is null));
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#MAIN_DOCUM - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN c_kl_dt#1#1_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_kl_dt#1#1)
    WHEN c_kl_dt#1#1_corp_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_CORP', c_kl_dt#1#1)
    WHEN c_kl_dt#1#1_priv_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_PRIV', c_kl_dt#1#1)
    WHEN c_kl_kt#1#1_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_kl_kt#1#1)
    WHEN c_kl_kt#1#1_corp_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_CORP', c_kl_kt#1#1)
    WHEN c_kl_kt#1#1_priv_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_PRIV', c_kl_kt#1#1)
    WHEN c_kl_dt#1#2_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#AC_FIN', c_kl_dt#1#2)
    WHEN c_kl_kt#1#2_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#AC_FIN', c_kl_kt#1#2)
    WHEN c_acc_dt_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#AC_FIN', c_acc_dt)
    WHEN c_acc_kt_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#AC_FIN', c_acc_kt)
    WHEN c_kl_dt#2#3_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_kl_dt#2#3)
    WHEN c_kl_dt#2#3_cflag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_BANK_N', 'REKS000IBS', 'Z#CLIENT', c_kl_dt#2#3)
    WHEN c_kl_kt#2#3_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_kl_kt#2#3)
    WHEN c_kl_kt#2#3_cflag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_BANK_N', 'REKS000IBS', 'Z#CLIENT', c_kl_kt#2#3)
    (SELECT /*+ LEADING(md) USE_NL(orig_c orig_corp orig_priv benef_c benef_corp benef_priv orig_a benef_a deb_a cred_a orig_b benef_b orig_bc benef_bc)*/
            md.rowid as row_id,
            md.*,
            case when md.c_kl_dt#1#1 is not null and orig_c.rowid is null  then 1 end as c_kl_dt#1#1_flag,
            case when md.c_kl_dt#1#1 is not null and orig_corp.rowid is null and
                      orig_c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')
                 then 1
            end as c_kl_dt#1#1_corp_flag,           
            case when md.c_kl_dt#1#1 is not null and orig_priv.rowid is null and
                      orig_c.class_id in ('CL_PRIV')
                 then 1
            end as c_kl_dt#1#1_priv_flag,           
            case when md.c_kl_kt#1#1 is not null and benef_c.rowid is null then 1 end as c_kl_kt#1#1_flag,
            case when md.c_kl_kt#1#1 is not null and benef_corp.rowid is null and
                      benef_c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')
                 then 1
            end as c_kl_kt#1#1_corp_flag,           
            case when md.c_kl_kt#1#1 is not null and benef_priv.rowid is null and
                      benef_c.class_id in ('CL_PRIV')
                 then 1
            end as c_kl_kt#1#1_priv_flag,           
            case when md.c_kl_dt#1#2 is not null and orig_a.rowid is null  then 1 end as c_kl_dt#1#2_flag,
            case when md.c_kl_kt#1#2 is not null and benef_a.rowid is null then 1 end as c_kl_kt#1#2_flag,
            case when md.c_acc_dt is not null and deb_a.rowid is null  then 1 end as c_acc_dt_flag,
            case when md.c_acc_kt is not null and cred_a.rowid is null then 1 end as c_acc_kt_flag,
            case when md.c_kl_dt#2#3 is not null and orig_b.rowid is null  then 1 end as c_kl_dt#2#3_flag,
            case when md.c_kl_dt#2#3 is not null and orig_b.rowid is not null and orig_bc.rowid is null   then 1 end as c_kl_dt#2#3_cflag,            
            case when md.c_kl_kt#2#3 is not null and benef_b.rowid is null then 1 end as c_kl_kt#2#3_flag,
            case when md.c_kl_kt#2#3 is not null and benef_b.rowid is not null and benef_bc.rowid is null then 1 end as c_kl_kt#2#3_cflag
       FROM reks000ibs.z#main_docum #PART_SUBPART# md
            left join reks000ibs.z#client  orig_c    on orig_c.id     = md.c_kl_dt#1#1
            left join reks000ibs.z#cl_corp orig_corp on orig_corp.id  = md.c_kl_dt#1#1
            left join reks000ibs.z#cl_priv orig_priv on orig_priv.id  = md.c_kl_dt#1#1
            left join reks000ibs.z#client benef_c    on benef_c.id    = md.c_kl_kt#1#1
            left join reks000ibs.z#client benef_corp on benef_corp.id = md.c_kl_kt#1#1
            left join reks000ibs.z#client benef_priv on benef_priv.id = md.c_kl_kt#1#1
            left join reks000ibs.z#ac_fin orig_a  on orig_a.id  = md.c_kl_dt#1#2
            left join reks000ibs.z#ac_fin benef_a on benef_a.id = md.c_kl_kt#1#2
            left join reks000ibs.z#ac_fin deb_a  on deb_a.id  = md.c_acc_dt
            left join reks000ibs.z#ac_fin cred_a on cred_a.id = md.c_acc_kt
            left join reks000ibs.z#cl_bank_n orig_b  on orig_b.id   = md.c_kl_dt#2#3
            left join reks000ibs.z#client orig_bc    on orig_bc.id  = md.c_kl_dt#2#3
            left join reks000ibs.z#cl_bank_n benef_b on benef_b.id  = md.c_kl_kt#2#3
            left join reks000ibs.z#client benef_bc   on benef_bc.id = md.c_kl_kt#2#3
      WHERE md.aml_load_status = 1 and
            md.aml_active_partition_flag = 1 and 
            nvl(md.aml_repeat_load_flag, 0) = 0 and
            ((md.c_kl_dt#1#1 is not null and orig_c.rowid is null) or
             (md.c_kl_dt#1#1 is not null and orig_corp.rowid is null and
              orig_c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or 
             (md.c_kl_dt#1#1 is not null and orig_priv.rowid is null and
              orig_c.class_id in ('CL_PRIV')) or 
             (md.c_kl_kt#1#1 is not null and benef_c.rowid is null) or
             (md.c_kl_kt#1#1 is not null and benef_corp.rowid is null and
              benef_c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
             (md.c_kl_kt#1#1 is not null and benef_priv.rowid is null and
              benef_c.class_id in ('CL_PRIV')) or                        
             (md.c_kl_dt#1#2 is not null and orig_a.rowid is null) or
             (md.c_kl_kt#1#2 is not null and benef_a.rowid is null) or
             (md.c_acc_dt is not null and deb_a.rowid is null) or
             (md.c_acc_kt is not null and cred_a.rowid is null) or            
             (md.c_kl_dt#2#3 is not null and orig_b.rowid is null) or
             (md.c_kl_dt#2#3 is not null and orig_b.rowid is not null and orig_bc.rowid is null) or
             (md.c_kl_kt#2#3 is not null and benef_b.rowid is null) or
             (md.c_kl_kt#2#3 is not null and benef_b.rowid is not null and benef_bc.rowid is null)));
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ���� ��������������� ������ - ��������� ��������� �������� �������� ������ (���������)
  -- ���� ��� - �������
  if var_count > 0 Then
    UPDATE std.mark_data_list 
       SET repair_mode = 'SOFT'
     WHERE mark_type = 'LOADED' and
           owner = 'REKS000IBS' and
           table_name in ('Z#GOZ_VOP_HISTORY') and
           repair_mode is null;    
    COMMIT;
  else
    return;
  end if;  
  
  WHILE true LOOP
    -- ��������� ������ Z#MAIN_DOCUM, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#main_docum
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#main_docum
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#MAIN_DOCUM ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#main_docum
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#MAIN_DOCUM � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#MAIN_DOCUM � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#MAIN_DOCUM � �������� ������ (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name in ('Z#MAIN_DOCUM', 'Z#GOZ_VOP_HISTORY') and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#MAIN_DOCUM'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#MAIN_DOCUM';   
commit;
END;
/
--
-- REKS000IBS.Z#AC_FIN
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#AC_FIN, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(a) USE_NL(c1 c1_corp c1_priv c2 c2_corp c2_priv)*/
           a.rowid
      FROM reks000ibs.z#ac_fin #PART_SUBPART# a
           left join reks000ibs.z#client  c1      on c1.id       = a.c_client_v
           left join reks000ibs.z#cl_corp c1_corp on c1_corp.id  = a.c_client_v
           left join reks000ibs.z#cl_priv c1_priv on c1_priv.id  = a.c_client_v
           left join reks000ibs.z#client  c2      on c2.id       = a.c_client_r
           left join reks000ibs.z#cl_corp c2_corp on c2_corp.id  = a.c_client_r
           left join reks000ibs.z#cl_priv c2_priv on c2_priv.id  = a.c_client_r
     WHERE a.aml_load_status = 1 and
           a.aml_active_partition_flag = 1 and 
           nvl(a.aml_repeat_load_flag, 0) = 0 and
           ((a.c_client_v is not null and c1.rowid is null) or
            (a.c_client_v is not null and c1_corp.rowid is null and
             c1.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or 
            (a.c_client_v is not null and c1_priv.rowid is null and
             c1.class_id in ('CL_PRIV')) or 
            (a.c_client_r is not null and c2.rowid is null) or
            (a.c_client_r is not null and c2_corp.rowid is null and
             c2.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
            (a.c_client_r is not null and c2_priv.rowid is null and
             c2.class_id in ('CL_PRIV')));
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#AC_FIN - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN c_client_v_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_client_v)
    WHEN c_client_v_corp_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_CORP', c_client_v)
    WHEN c_client_v_priv_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_PRIV', c_client_v)
    WHEN c_client_r_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_client_r)
    WHEN c_client_r_corp_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_CORP', c_client_r)
    WHEN c_client_r_priv_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_PRIV', c_client_r)
    (SELECT /*+ LEADING(a) USE_NL(c1 c1_corp c1_priv c2 c2_corp c2_priv)*/
            a.rowid as row_id,
            a.*,
            case when a.c_client_v is not null and c1.rowid is null then 1 end as c_client_v_flag,
            case when a.c_client_v is not null and c1_corp.rowid is null and
                      c1.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')
                 then 1
            end as c_client_v_corp_flag,     
            case when a.c_client_v is not null and c1_priv.rowid is null and
                      c1.class_id in ('CL_PRIV')
                 then 1
            end as c_client_v_priv_flag,     
            case when a.c_client_r is not null and c2.rowid is null then 1 end as c_client_r_flag,            
            case when a.c_client_r is not null and c2_corp.rowid is null and
                      c2.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')
                 then 1
            end as c_client_r_corp_flag,     
            case when a.c_client_r is not null and c2_priv.rowid is null and
                      c2.class_id in ('CL_PRIV')
                 then 1
            end as c_client_r_priv_flag     
       FROM reks000ibs.z#ac_fin #PART_SUBPART# a
            left join reks000ibs.z#client  c1      on c1.id       = a.c_client_v
            left join reks000ibs.z#cl_corp c1_corp on c1_corp.id  = a.c_client_v
            left join reks000ibs.z#cl_priv c1_priv on c1_priv.id  = a.c_client_v
            left join reks000ibs.z#client  c2      on c2.id       = a.c_client_r
            left join reks000ibs.z#cl_corp c2_corp on c2_corp.id  = a.c_client_r
            left join reks000ibs.z#cl_priv c2_priv on c2_priv.id  = a.c_client_r
      WHERE a.aml_load_status = 1 and
            a.aml_active_partition_flag = 1 and 
            nvl(a.aml_repeat_load_flag, 0) = 0 and
            ((a.c_client_v is not null and c1.rowid is null) or
             (a.c_client_v is not null and c1_corp.rowid is null and
              c1.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or 
             (a.c_client_v is not null and c1_priv.rowid is null and
              c1.class_id in ('CL_PRIV')) or 
             (a.c_client_r is not null and c2.rowid is null) or
             (a.c_client_r is not null and c2_corp.rowid is null and
              c2.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
             (a.c_client_r is not null and c2_priv.rowid is null and
              c2.class_id in ('CL_PRIV'))));  
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ��� ��������������� ����� - �������
  if var_count = 0 Then
    return;
  end if;  
  
  WHILE true LOOP
    -- ��������� ������ Z#AC_FIN, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#ac_fin
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#ac_fin
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#AC_FIN ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#ac_fin
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#AC_FIN � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#AC_FIN � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#AC_FIN (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name = 'Z#AC_FIN' and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#AC_FIN'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#AC_FIN';   
commit;
END;
/
--
-- REKS000IBS.Z#CL_CORP
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#CL_CORP, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(t) USE_NL(c)*/
           t.rowid
      FROM reks000ibs.z#cl_corp #PART_SUBPART# t
           left join reks000ibs.z#client c on c.id = t.id
     WHERE t.aml_load_status = 1 and
           t.aml_active_partition_flag = 1 and 
           nvl(t.aml_repeat_load_flag, 0) = 0 and
           c.rowid is null;
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#CL_CORP - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN 1=1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', id)
    (SELECT /*+ LEADING(t) USE_NL(c)*/
            t.rowid as row_id,
            t.id     
       FROM reks000ibs.z#cl_corp #PART_SUBPART# t
            left join reks000ibs.z#client c on c.id = t.id
      WHERE t.aml_load_status = 1 and
            t.aml_active_partition_flag = 1 and 
            nvl(t.aml_repeat_load_flag, 0) = 0 and
            c.rowid is null);  
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ���� ��������������� ������ - ��������� ��������� �������� �������� ������ (���������)
  -- ���� ��� - �������
  if var_count > 0 Then
    UPDATE std.mark_data_list 
       SET repair_mode = 'SOFT'
     WHERE mark_type = 'LOADED' and
           owner = 'REKS000IBS' and
           table_name in ('Z#CERTIFICATE','Z#CONTACTS','Z#LINKS_CL','Z#MIGRATION_CARD','Z#OKONH_REF','Z#OKVED_REF',
                          'Z#PERSONAL_ADDRESS','Z#PERSONS_POS','Z#RELATIVES') and
           repair_mode is null;    
    COMMIT;
  else
    return;
  end if;  
  
  
  WHILE true LOOP
    -- ��������� ������ Z#CL_CORP, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#cl_corp
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#cl_corp
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#CL_CORP ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#cl_corp
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#CL_CORP � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#CL_CORP � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#CL_CORP � �������� ������ (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name in ('Z#CL_CORP', 'Z#CERTIFICATE','Z#CONTACTS','Z#LINKS_CL','Z#MIGRATION_CARD','Z#OKONH_REF','Z#OKVED_REF',
                            'Z#PERSONAL_ADDRESS','Z#PERSONS_POS','Z#RELATIVES') and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#CL_CORP � �������� ���������� ������'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#CL_CORP';   
commit;
END;
/
--
-- REKS000IBS.Z#CL_PRIV
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#CL_PRIV, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(t) USE_NL(c)*/
           t.rowid
      FROM reks000ibs.z#cl_priv #PART_SUBPART# t
           left join reks000ibs.z#client c on c.id = t.id
     WHERE t.aml_load_status = 1 and
           t.aml_active_partition_flag = 1 and 
           nvl(t.aml_repeat_load_flag, 0) = 0 and
           c.rowid is null;
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#CL_PRIV - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN 1=1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', id)
    (SELECT /*+ LEADING(t) USE_NL(c)*/
            t.rowid as row_id,
            t.id     
       FROM reks000ibs.z#cl_priv #PART_SUBPART# t
            left join reks000ibs.z#client c on c.id = t.id
      WHERE t.aml_load_status = 1 and
            t.aml_active_partition_flag = 1 and 
            nvl(t.aml_repeat_load_flag, 0) = 0 and
            c.rowid is null);  
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ���� ��������������� ������ - ��������� ��������� �������� �������� ������ (���������)
  -- ���� ��� - �������
  if var_count > 0 Then
    UPDATE std.mark_data_list 
       SET repair_mode = 'SOFT'
     WHERE mark_type = 'LOADED' and
           owner = 'REKS000IBS' and
           table_name in ('Z#CERTIFICATE','Z#CONTACTS','Z#LINKS_CL','Z#MIGRATION_CARD','Z#OKONH_REF','Z#OKVED_REF',
                          'Z#PERSONAL_ADDRESS','Z#PERSONS_POS','Z#RELATIVES') and
           repair_mode is null;    
    COMMIT;
  else
    return;
  end if;  
  
  
  WHILE true LOOP
    -- ��������� ������ Z#CL_PRIV, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#cl_priv
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#cl_priv
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#CL_PRIV ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#cl_priv
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#CL_PRIV � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#CL_PRIV � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#CL_PRIV � �������� ������ (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name in ('Z#CL_PRIV', 'Z#CERTIFICATE','Z#CONTACTS','Z#LINKS_CL','Z#MIGRATION_CARD','Z#OKONH_REF','Z#OKVED_REF',
                            'Z#PERSONAL_ADDRESS','Z#PERSONS_POS','Z#RELATIVES') and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#CL_PRIV � �������� ���������� ������'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#CL_PRIV';   
commit;
END;
/
--
-- REKS000IBS.Z#LINKS_CL
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#LINKS_CL, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(t) USE_NL(c c_corp c_priv l)*/
           t.rowid
      FROM reks000ibs.z#links_cl #PART_SUBPART# t
           left join reks000ibs.z#client  c      on c.id      = t.c_partner
           left join reks000ibs.z#cl_corp c_corp on c_corp.id = t.c_partner
           left join reks000ibs.z#cl_priv c_priv on c_priv.id = t.c_partner
           left join reks000ibs.z#cl_link l on l.id = t.c_vid_link
     WHERE t.aml_load_status = 1 and
           t.aml_active_partition_flag = 1 and 
           nvl(t.aml_repeat_load_flag, 0) = 0 and
           ((t.c_partner is not null and c.rowid is null) or
            (t.c_partner is not null and c_corp.rowid is null and
             c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
            (t.c_partner is not null and c_priv.rowid is null and
             c.class_id in ('CL_PRIV')) or
            (t.c_vid_link is not null and l.rowid is null));
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#LINKS_CL - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN c_partner_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_partner)
    WHEN c_partner_corp_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_CORP', c_partner)
    WHEN c_partner_priv_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_PRIV', c_partner)
    WHEN c_vid_link_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_LINK', c_vid_link)
    (SELECT /*+ LEADING(t) USE_NL(c l)*/
            t.rowid as row_id,
            t.*,
            case when t.c_partner is not null and c.rowid is null then 1 end as c_partner_flag,
            case when t.c_partner is not null and c_corp.rowid is null and
                      c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG') 
                 then 1 
            end as c_partner_corp_flag,
            case when t.c_partner is not null and c_priv.rowid is null and
                      c.class_id in ('CL_PRIV')
                 then 1 
            end as c_partner_priv_flag,            
            case when t.c_vid_link is not null and l.rowid is null then 1 end as c_vid_link_flag
       FROM reks000ibs.z#links_cl #PART_SUBPART# t
            left join reks000ibs.z#client  c      on c.id      = t.c_partner
            left join reks000ibs.z#cl_corp c_corp on c_corp.id = t.c_partner
            left join reks000ibs.z#cl_priv c_priv on c_priv.id = t.c_partner
            left join reks000ibs.z#cl_link l on l.id = t.c_vid_link
      WHERE t.aml_load_status = 1 and
            t.aml_active_partition_flag = 1 and 
            nvl(t.aml_repeat_load_flag, 0) = 0 and
            ((t.c_partner is not null and c.rowid is null) or
             (t.c_partner is not null and c_corp.rowid is null and
              c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
             (t.c_partner is not null and c_priv.rowid is null and
              c.class_id in ('CL_PRIV')) or
             (t.c_vid_link is not null and l.rowid is null)));  
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ��� ��������������� ����� - �������
  if var_count = 0 Then
    return;
  end if;    
  
  WHILE true LOOP
    -- ��������� ������ Z#LINKS_CL, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#links_cl
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#links_cl
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#LINKS_CL ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#links_cl
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#LINKS_CL � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#LINKS_CL � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#LINKS_CL (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name = 'Z#LINKS_CL' and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#LINKS_CL'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#LINKS_CL';   
commit;
END;
/
--
-- REKS000IBS.Z#PERSONS_POS
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#PERSONS_POS, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(t) USE_NL(c c_corp c_priv ca)*/
           t.rowid
      FROM reks000ibs.z#persons_pos #PART_SUBPART# t
           left join reks000ibs.z#client  c      on c.id      = t.c_fase
           left join reks000ibs.z#cl_corp c_corp on c_corp.id = t.c_fase
           left join reks000ibs.z#cl_priv c_priv on c_priv.id = t.c_fase
           left join reks000ibs.z#casta ca on ca.id = t.c_range
     WHERE t.aml_load_status = 1 and
           t.aml_active_partition_flag = 1 and 
           nvl(t.aml_repeat_load_flag, 0) = 0 and
           ((t.c_fase is not null and c.rowid is null) or
            (t.c_fase is not null and c_corp.rowid is null and
             c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
            (t.c_fase is not null and c_priv.rowid is null and
             c.class_id in ('CL_PRIV')) or
            (t.c_range is not null and ca.rowid is null));
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#PERSONS_POS - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN c_fase_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_fase)
    WHEN c_fase_corp_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_CORP', c_fase)
    WHEN c_fase_priv_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_PRIV', c_fase)
    WHEN c_range_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CASTA', c_range)
    (SELECT /*+ LEADING(t) USE_NL(c c_corp c_priv ca)*/
            t.rowid as row_id,
            t.*,
            case when t.c_fase is not null and c.rowid is null then 1 end as c_fase_flag,
            case when t.c_fase is not null and c_corp.rowid is null and
                      c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')
                 then 1 
            end as c_fase_corp_flag,
            case when t.c_fase is not null and c_priv.rowid is null and
                      c.class_id in ('CL_PRIV')
                 then 1 
            end as c_fase_priv_flag,           
            case when t.c_range is not null and ca.rowid is null then 1 end as c_range_flag
       FROM reks000ibs.z#persons_pos #PART_SUBPART# t
            left join reks000ibs.z#client  c      on c.id      = t.c_fase
            left join reks000ibs.z#cl_corp c_corp on c_corp.id = t.c_fase
            left join reks000ibs.z#cl_priv c_priv on c_priv.id = t.c_fase
            left join reks000ibs.z#casta ca on ca.id = t.c_range
      WHERE t.aml_load_status = 1 and
            t.aml_active_partition_flag = 1 and 
            nvl(t.aml_repeat_load_flag, 0) = 0 and
            ((t.c_fase is not null and c.rowid is null) or
             (t.c_fase is not null and c_corp.rowid is null and
              c.class_id in ('CL_PART','CL_BANK_F','CL_BANK_N','CL_ORG')) or
             (t.c_fase is not null and c_priv.rowid is null and
              c.class_id in ('CL_PRIV')) or
             (t.c_range is not null and ca.rowid is null)));  
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ��� ��������������� ����� - �������
  if var_count = 0 Then
    return;
  end if;    
  
  WHILE true LOOP
    -- ��������� ������ Z#PERSONS_POS, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#persons_pos
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#persons_pos
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#PERSONS_POS ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#persons_pos
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#PERSONS_POS � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#PERSONS_POS � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#PERSONS_POS (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name = 'Z#PERSONS_POS' and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#PERSONS_POS'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#PERSONS_POS';   
commit;
END;
/
--
-- REKS000IBS.Z#RELATIVES
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#RELATIVES, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(t) USE_NL(c_priv c)*/
           t.rowid
      FROM reks000ibs.z#relatives t
           left join reks000ibs.z#cl_priv c_priv on c_priv.id = t.c_cl_priv
           left join reks000ibs.z#client  c      on c.id      = t.c_cl_priv
     WHERE t.aml_load_status = 1 and
           nvl(t.aml_repeat_load_flag, 0) = 0 and
           ((t.c_cl_priv is not null and c_priv.rowid is null) or
            (t.c_cl_priv is not null and c_priv.rowid is not null and c.rowid is null));
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#RELATIVES - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN c_cl_priv_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CL_PRIV', c_cl_priv)
    WHEN c_cl_priv_cflag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#CLIENT', c_cl_priv)
    (SELECT /*+ LEADING(t) USE_NL(c_priv c)*/
            t.rowid as row_id,
            t.*,
            case when t.c_cl_priv is not null and c_priv.rowid is null then 1 end as c_cl_priv_flag,
            case when t.c_cl_priv is not null and c_priv.rowid is not null and c.rowid is null then 1 end as c_cl_priv_cflag
       FROM reks000ibs.z#relatives t
            left join reks000ibs.z#cl_priv c_priv on c_priv.id = t.c_cl_priv
            left join reks000ibs.z#client  c      on c.id      = t.c_cl_priv
      WHERE t.aml_load_status = 1 and
            nvl(t.aml_repeat_load_flag, 0) = 0 and
            ((t.c_cl_priv is not null and c_priv.rowid is null) or
             (t.c_cl_priv is not null and c_priv.rowid is not null and c.rowid is null)));  
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ��� ��������������� ����� - �������
  if var_count = 0 Then
    return;
  end if;    
  
  WHILE true LOOP
    -- ��������� ������ Z#RELATIVES, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#relatives
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#relatives
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#RELATIVES ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#relatives
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#RELATIVES � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#RELATIVES � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#RELATIVES (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name = 'Z#RELATIVES' and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#RELATIVES'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#RELATIVES';   
commit;
END;
/
--
-- REKS000IBS.Z#PERSONAL_ADDRESS
--
DECLARE
var_clob  CLOB := 
q'{DECLARE
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
BEGIN
  -- ��������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(true);

  -- �������� rowid ��������������� ����� Z#PERSONAL_ADDRESS, ������� ����� �������� � ��������� �������� 
  -- ���������� �� �� ��������� �������
  -- � ������ ������������� �������� ���������� ����� �������������� ������������� ������� � parent-��������
  DELETE FROM std.rowids#wrk;

  if #CONSISTENCY_FLAG# = 0 Then -- ����� ��������������� ��������
    INSERT INTO std.rowids#wrk(row_id)
    SELECT /*+ LEADING(t) USE_NL(a r n c tr p r2 s sn n2 p2)*/
           t.rowid
      FROM reks000ibs.z#personal_address #PART_SUBPART# t
           left join reks000ibs.z#address_type a  on a.id  = t.c_type
           left join reks000ibs.z#region r        on r.id  = t.c_district
           left join reks000ibs.z#names_city n    on n.id  = t.c_city
           left join reks000ibs.z#country c       on c.id  = n.c_country
           left join reks000ibs.z#territory tr    on tr.id = n.c_region
           left join reks000ibs.z#people_place p  on p.id  = n.c_people_place
           left join reks000ibs.z#region r2       on r2.id = n.c_area
           left join reks000ibs.z#sprav_street s  on s.id  = t.c_sprav_street
           left join reks000ibs.z#street_name sn  on sn.id = s.c_street_name
           left join reks000ibs.z#names_city n2   on n2.id = s.c_city
           left join reks000ibs.z#people_place p2 on p2.id = n2.c_people_place
     WHERE t.aml_load_status = 1 and
           t.aml_active_partition_flag = 1 and 
           nvl(t.aml_repeat_load_flag, 0) = 0 and
           ((t.c_type is not null          and a.rowid is null) or
            (t.c_district is not null      and r.rowid is null) or
            (t.c_city is not null          and n.rowid is null) or
            (n.c_country is not null       and c.rowid is null) or
            (n.c_region is not null        and tr.rowid is null) or
            (n.c_people_place is not null  and p.rowid is null) or
            (n.c_area is not null          and r2.rowid is null) or
            (t.c_sprav_street is not null  and s.rowid is null) or
            (s.c_street_name is not null   and sn.rowid is null) or
            (s.c_city is not null          and n2.rowid is null) or
            (n2.c_people_place is not null and p2.rowid is null));
    var_count := SQL%ROWCOUNT;

    if var_count > 0 Then
      std.pkg_log.warn('���������� ��������������� ������ � ������� REKS000IBS.Z#PERSONAL_ADDRESS - �������� �� � ��������� ��������. ���������� �����: '||to_char(var_count), 'std.pkg_load.check_consistency');
    end if;      
  else   
    INSERT ALL
    WHEN 1=1 THEN
      INTO std.rowids#wrk(row_id) VALUES(row_id)
    WHEN c_type_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#ADDRESS_TYPE', c_type)
    WHEN c_district_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#REGION', c_district)
    WHEN c_city_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#NAMES_CITY', c_city)
    WHEN nc_country_flag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#NAMES_CITY', 'REKS000IBS', 'Z#COUNTRY', nc_country)
    WHEN nc_region_flag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#NAMES_CITY', 'REKS000IBS', 'Z#TERRITORY', nc_region)
    WHEN nc_people_place_flag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#NAMES_CITY', 'REKS000IBS', 'Z#PEOPLE_PLACE', nc_people_place)
    WHEN nc_area_flag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#NAMES_CITY', 'REKS000IBS', 'Z#REGION', nc_area)
    WHEN c_sprav_street_flag = 1 THEN
      INTO std.consistency_errors#wrk(parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#SPRAV_STREET', c_sprav_street)      
    WHEN sc_street_name_flag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#SPRAV_STREET', 'REKS000IBS', 'Z#STREET_NAME', sc_street_name)
    WHEN sc_city_flag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#SPRAV_STREET', 'REKS000IBS', 'Z#NAMES_CITY', sc_city)
    WHEN n2c_people_place_flag = 1 THEN
      INTO std.consistency_errors#wrk(owner, table_name, parent_owner, parent_table_name, parent_id01) 
      VALUES('REKS000IBS', 'Z#NAMES_CITY', 'REKS000IBS', 'Z#PEOPLE_PLACE', n2c_people_place)
    (SELECT /*+ LEADING(t) USE_NL(a r n c tr p r2 s sn n2 p2)*/
            t.rowid as row_id,
            t.*,
            n.c_country as nc_country,
            n.c_region as nc_region,
            n.c_people_place as nc_people_place,
            n.c_area as nc_area,
            s.c_street_name as sc_street_name,
            s.c_city as sc_city,
            n2.c_people_place as n2c_people_place,
            case when t.c_type is not null          and a.rowid is null then 1 end as c_type_flag,
            case when t.c_district is not null      and r.rowid is null then 1 end as c_district_flag,
            case when t.c_city is not null          and n.rowid is null then 1 end as c_city_flag,            
            case when n.c_country is not null       and c.rowid is null then 1 end as nc_country_flag,
            case when n.c_region is not null        and tr.rowid is null then 1 end as nc_region_flag,            
            case when n.c_people_place is not null  and p.rowid is null then 1 end as nc_people_place_flag,
            case when n.c_area is not null          and r2.rowid is null then 1 end as nc_area_flag,            
            case when t.c_sprav_street is not null  and s.rowid is null then 1 end as c_sprav_street_flag,            
            case when s.c_street_name is not null   and sn.rowid is null then 1 end as sc_street_name_flag,            
            case when s.c_city is not null          and n2.rowid is null then 1 end as sc_city_flag,            
            case when n2.c_people_place is not null and p2.rowid is null then 1 end as n2c_people_place_flag
       FROM reks000ibs.z#personal_address #PART_SUBPART# t
            left join reks000ibs.z#address_type a  on a.id  = t.c_type
            left join reks000ibs.z#region r        on r.id  = t.c_district
            left join reks000ibs.z#names_city n    on n.id  = t.c_city
            left join reks000ibs.z#country c       on c.id  = n.c_country
            left join reks000ibs.z#territory tr    on tr.id = n.c_region
            left join reks000ibs.z#people_place p  on p.id  = n.c_people_place
            left join reks000ibs.z#region r2       on r2.id = n.c_area
            left join reks000ibs.z#sprav_street s  on s.id  = t.c_sprav_street
            left join reks000ibs.z#street_name sn  on sn.id = s.c_street_name
            left join reks000ibs.z#names_city n2   on n2.id = s.c_city
            left join reks000ibs.z#people_place p2 on p2.id = n2.c_people_place
      WHERE t.aml_load_status = 1 and
            t.aml_active_partition_flag = 1 and 
            nvl(t.aml_repeat_load_flag, 0) = 0 and
            ((t.c_type is not null          and a.rowid is null) or
             (t.c_district is not null      and r.rowid is null) or
             (t.c_city is not null          and n.rowid is null) or
             (n.c_country is not null       and c.rowid is null) or
             (n.c_region is not null        and tr.rowid is null) or
             (n.c_people_place is not null  and p.rowid is null) or
             (n.c_area is not null          and r2.rowid is null) or
             (t.c_sprav_street is not null  and s.rowid is null) or
             (s.c_street_name is not null   and sn.rowid is null) or
             (s.c_city is not null          and n2.rowid is null) or
             (n2.c_people_place is not null and p2.rowid is null)));  
    var_count := SQL%ROWCOUNT;
  end if;
  COMMIT;
  
  -- ���� ��� ��������������� ����� - �������
  if var_count = 0 Then
    return;
  end if;    
  
  WHILE true LOOP
    -- ��������� ������ Z#PERSONAL_ADDRESS, ������� ���������� ��������, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM reks000ibs.z#personal_address
     WHERE rowid in (select row_id from std.rowids#wrk) and 
           rownum <= 1000000
    FOR UPDATE SKIP LOCKED;
          
    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM reks000ibs.z#personal_address
       WHERE rowid in (select row_id from std.rowids#wrk) and 
             rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ��������������� ����� ������� REKS000IBS.Z#PERSONAL_ADDRESS ��� �� ��������� � ��������� ��������', 'std.pkg_load.check_consistency');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE reks000ibs.z#personal_address
         SET aml_repeat_load_flag = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));
      
      -- ������� �� ��������� ������� ������, ��������������� ��� ������������
      DELETE FROM std.rowids#wrk
       WHERE row_id in (select chartorowid(column_value) from table(var_rowids));
      
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� �������� ��������������� ������ � ������� REKS000IBS.Z#PERSONAL_ADDRESS � ��������� �������� - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.check_consistency)');
    end if;
  END LOOP;
  
  -- �������� ��� ������ ������ �������� ������� ��������
  std.pkg_load.set_is_preload(false);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_load.set_is_preload(false);
    std.pkg_log.fatal('������ ��� ��������� ��������������� ������� � ������� REKS000IBS.Z#PERSONAL_ADDRESS � ��������� ��������'||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    -- ������������� ����� �������������� ��� ������� Z#PERSONAL_ADDRESS (����� ����������� ��������)
    begin
      UPDATE std.mark_data_list 
         SET repair_mode = 'SOFT'
       WHERE mark_type = 'LOADED' and
             owner = 'REKS000IBS' and
             table_name = 'Z#PERSONAL_ADDRESS' and
             repair_mode is null;

      COMMIT;         
    exception
      when others then 
        std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� ������� REKS000IBS.Z#PERSONAL_ADDRESS'||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
    end; 
END;
}';
BEGIN
UPDATE std.src_replicas
   SET check_consistency_sql = var_clob
 WHERE owner = 'REKS000IBS' and
       table_name = 'Z#PERSONAL_ADDRESS';   
commit;
END;
/
