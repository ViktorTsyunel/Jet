grant select, insert, update, delete on business.rf_crncy_rates to std with grant option;
grant select, insert, update, delete on business.rf_metal_rates to std with grant option;
grant select, insert, update, delete on stg.dublication_problem to std with grant option;
grant select, insert, update, delete on stg.etalon_changing to std with grant option;
grant select, insert, update, delete on business.cust_addr to std with grant option;
grant select, insert, update, delete on business.cust_phon to std with grant option;
grant select, insert, update, delete on business.cust_email_addr to std with grant option;
grant select, insert, update, delete on business.rf_cust_id_doc to std with grant option;
grant select on rcod038client.person to std with grant option;
grant select on rcod038deposit.clerk_fio to std with grant option;
grant select, insert, update, delete on business.wire_trxn to std;
grant select, insert, update, delete on business.cash_trxn to std;
grant select, insert, update, delete on business.back_office_trxn to std;
grant select, insert, update, delete on business.cust_acct to std;
--grant select on dba_tab_columns to std;
--grant select on dba_tables to std;

create or replace package std.aml_tools is

  -- author  : vkoren
  -- created : 07.02.2014 17:04:12
  -- purpose : 

  -- public type declarations

  -- public constant declarations

  -- public variable declarations

  -- public function and procedure declarations
  procedure add_crncy_rates(date_from in date,
                            date_to   in date,
                            tb_id     in number);
  procedure add_metal_rates(date_from in date,
                            date_to   in date,
                            tb_id     in number);
  function get_acct_id_by_param(bck_id in varchar2, n20_id in varchar2)
    return number;
  function get_new_acct_seq_id return number parallel_enable;
  function collapse_recode_acct(bck_id in varchar2,
                                eks_id in varchar2,
                                nav_id in varchar2,
                                nar_id in varchar2,
                                n20_id in varchar2) return varchar2;
  function get_new_cust_cust_seq_id return number;
  function get_new_cust_acct_seq_id return number;
  function get_new_org_seq_id return number;
  function get_new_org_rlshp_seq_id return number;
  function get_new_cust_seq_id return number;
  function get_new_cust_doc_seq_id return number;
  function get_new_addr_seq_id return number;
  function get_new_cust_phon_seq return number;
  function get_new_trxn_seq return number;
  function get_new_email_seq return number;
  function get_new_clsf_code_seq_id return number;
  function get_new_cust_rlshp_type_seq return number;
  function get_new_scrty_seq_id return number;
  function get_new_trxn_scrty_seq_id return number; --����� �������� ������������������ ��� ������ ������ ������ - �������� (trxn_scrty_seq_id)
  procedure change_etalon_flag(p_mdmtype varchar2 default 'MDR');
  procedure load_rmdr000mdm_contequiv(p_last_loaded_date date,
                                      p_mdmtype          varchar2 default 'MDR');
  procedure solve_dublicate_problem(p_mdmtype varchar2 default 'MDR');
  procedure load_mdmr_recode_cust(p_last_loaded_date date,
                                  p_mdmtype          varchar2 default 'MDR');
  --������������� ��������. ��. �������� � ������� std.recogn_state
  function get_regn_name(p_regn_name varchar2) return varchar2 deterministic;
  --
  -- �� ������������ ������ (��������, � ���������� � ������� ���������� ���������) ���������� �������������� ��� ������ �� iso
  -- ��������� ������������ ������ ���������� �� ������� business.rf_country, ������������� ��������� - �� std.recogn_country
  --
  function get_cntry_cd(par_cntry_name varchar2)
    return business.rf_country.cntry_cd%type RESULT_CACHE;
  --����� ������ ������ �� ��������� ����������
  function get_scrty_id(full_num   in business.scrty.alt_scrty_intrl_id%type, -- ������ ����� ��
                        num        in business.scrty.cstm_6_tx%type, -- ����� �� ��� �����
                        scrty_date in business.scrty.scrty_issue_dt%type, -- ���� ������� ��
                        nominal    in business.scrty.cstm_1_rl%type, --������� ��
                        stype      in business.scrty.prod_type_cd%type --��� ��
                       ) return number deterministic;
--
-- �������� rowid ������� (������� RF_ADDR_OBJECT) � ������������ � ����� ����� ��� ����� �����
--
FUNCTION get_region(
  par_kladr_cd    VARCHAR2,
  par_okato_cd    VARCHAR2
  )
RETURN ROWID RESULT_CACHE;
--
-- �������� rowid ������� (������� RF_ADDR_OBJECT) � ������������ � ��� �������������, ��������, �� ������ ����������
-- (������������ ������������ ������������ ����� ������� STD.RECOGN_STATE)
--
FUNCTION recognise_region(
  par_region_name    VARCHAR2
  )
RETURN ROWID RESULT_CACHE;
--
-- �������� addr_seq_id ������� (������� RF_ADDR_OBJECT) � ������������ � ��� �������������, ��������, �� ������ ����������
-- (������������ ������������ ������������ ����� ������� STD.RECOGN_STATE)
--
FUNCTION recognise_region2(
  par_region_name    VARCHAR2
  )
RETURN NUMBER RESULT_CACHE;
--
-- ���������� ID ������� �� ��� ����������� ������ (UNTB)
--
FUNCTION get_cust_by_untb(
  par_untb        INTEGER,             -- ���������� ����� �������
  par_id_mega     INTEGER DEFAULT null -- ����� ��������, � �������� ��������� ������, ���� NULL - ����������� ����� �� ���� ��
) 
RETURN NUMBER; -- ������������� ������� (business.cust.cust_seq_id)
--
-- �������� ������������ ������������ BCK �� ��� �������������
--
FUNCTION get_clerk_fio(
  par_id_mega     INTEGER, -- ����� ��������, � �������� ��������� ������������ - clerk_fio.id_mega
  par_branchno    INTEGER, -- ����� ��� - clerk_fio.branchno 
  par_office      INTEGER, -- ����� ��� - clerk_fio.office
  par_clerk       INTEGER  -- ID ������������ - clerk_fio.clerk
) 
RETURN VARCHAR2; -- ��� ������������ - clerk_fio.fio
--
-- �������� �� ���������� ������ �������� �������� � ������������ � ��������� PL/SQL ����������
-- ������� ������������ ��� ��������� �������� ����������� �������� ��������/��������� �� ����� ���������
-- � ������������ � ����������� (LOAD_STEP_LOG_ITEMS)
-- 
FUNCTION get_number_from_text
(
  par_text        VARCHAR2, -- �����, �� �������� ����� �������� �������� ��������
  par_expr        VARCHAR2  -- PL/SQl ���������, ���������� �� ������ �����, �� ����� ��������� � ������� ������� #TEXT#
)
return NUMBER; -- ���������� �����, � ������ ������������� ������ - null
end aml_tools;
/
create or replace package body std.AML_TOOLS is
  --
  -- ��������� ��� ���������� �������� ������ ����� �� ����������� ���.
  -- ��� ������������ ��� ������� ���� �� ��������� ���������� ����, �� ������� ����� ���� ������ ������, ��� ������� ��� �������� �������� �� ���������������.
  --
  procedure add_crncy_rates(date_from in date, --���� ������ �������, �� ������� ������������ ������, ���� null, �� ����� 30 ���� �� ������� ����
                            date_to   in date, --���� ��������� �������, �� ������� ������������ ������, ���� null, �� ������� ����
                            tb_id     in number) is --��� ��������, ��� �������� ������������ ����������, ���� null - ��� ���� ��

    var_date_from   DATE := nvl(trunc(date_from), trunc(sysdate)-30);
    var_date_to     DATE := nvl(trunc(date_to), trunc(sysdate)); 
    var_tb_id       INTEGER := tb_id; 
  begin
    --
    -- ��������/������� �������������� ����� ����� �� ��������� ������ ��� ���������� ��
    --
    MERGE INTO business.rf_crncy_rates t USING
    (SELECT new_cr.iso_alfa, new_cr.tb_id, new_cr.rate_date, 
            src_cr.qty, src_cr.rate, src_cr.buy_rate, src_cr.sell_rate, src_cr.cross_rate
       FROM (select tb_crn.iso_alfa, tb_crn.tb_id, dt.rate_date, 
                    -- ���������� ����, � ������� ������� ����������� ����� ��� ������� ��/������/����:
                    -- ����� ��������� � ������� ����, ��� ������� �������� ���� � �� �� ��������������
                    lag(case when cr.auto_flag = 1 
                             then to_date(null) 
                             else cr.rate_date 
                        end ignore nulls) over(partition by tb_crn.iso_alfa, tb_crn.tb_id order by dt.rate_date) as source_rate_date,
                    cr.auto_flag                   
               from -- ������ ���� ��� � ��������������� �������                
                    (select var_date_from + level - 1 as rate_date
                       from dual
                     connect by level <= var_date_to - var_date_from + 1) dt
                    -- ������ ���� ��/�����, ��� ������� � ��������������� ������� ���� �����
                    cross join (select distinct tb_id, iso_alfa
                                  from business.rf_crncy_rates
                                 where rate_date between var_date_from and var_date_to and
                                       tb_id = nvl(var_tb_id, tb_id)) tb_crn
                    -- ��� ������������ ����� ��� ��/����� � ��������������� �������
                    left join business.rf_crncy_rates cr on cr.iso_alfa = tb_crn.iso_alfa and
                                                            cr.tb_id = tb_crn.tb_id and
                                                            cr.rate_date = dt.rate_date) new_cr
            join business.rf_crncy_rates src_cr on src_cr.iso_alfa = new_cr.iso_alfa and
                                                   src_cr.tb_id = new_cr.tb_id and
                                                   src_cr.rate_date = new_cr.source_rate_date  
      WHERE nvl(new_cr.auto_flag, 1) = 1 and       -- �� ���� ��� �����, ���� �� ��������������
            new_cr.source_rate_date is not null) v -- ���� ����, � ������� ����� ����������� ����
    ON (t.iso_alfa  = v.iso_alfa and
        t.tb_id     = v.tb_id and
        t.rate_date = v.rate_date) 
    WHEN NOT MATCHED THEN
      INSERT (iso_alfa, rate_date, tb_id, qty, rate, buy_rate, sell_rate, cross_rate, auto_flag)
      VALUES (v.iso_alfa, v.rate_date, v.tb_id, v.qty, v.rate, v.buy_rate, v.sell_rate, v.cross_rate, 1)
    WHEN MATCHED THEN
      UPDATE
         SET t.qty        = v.qty, 
             t.rate       = v.rate, 
             t.buy_rate   = v.buy_rate, 
             t.sell_rate  = v.sell_rate, 
             t.cross_rate = v.cross_rate, 
             t.auto_flag  = 1;
  
    commit;
  end add_crncy_rates;
  --
  -- ��������� ��� ���������� �������� ������ �������� �� ����������� ���.
  -- ��� ������������ ��� ������� ���� �� ��������� ���������� ����, �� ������� ����� ���� ������ ������, ��� ������� ��� �������� �������� �� ���������������.
  --
  procedure add_metal_rates(date_from in date, --���� ������ �������, �� ������� ������������ ������, ���� null, �� ����� 30 ���� �� ������� ����
                            date_to   in date, --���� ��������� �������, �� ������� ������������ ������, ���� null, �� ������� ����
                            tb_id     in number) is --��� ��������, ��� �������� ������������ ����������, ���� null - ��� ���� ��

    var_date_from   DATE := nvl(trunc(date_from), trunc(sysdate)-30);
    var_date_to     DATE := nvl(trunc(date_to), trunc(sysdate)); 
    var_tb_id       INTEGER := tb_id;
  begin
    --
    -- ��������/������� �������������� ����� ����� �� ��������� ������ ��� ���������� ��
    --
    MERGE INTO business.rf_metal_rates t USING
    (SELECT new_mr.iso_alfa, new_mr.tb_id, new_mr.rate_date, 
            src_mr.rate, src_mr.buy_rate, src_mr.sell_rate
       FROM (select tb_crn.iso_alfa, tb_crn.tb_id, dt.rate_date, 
                    -- ���������� ����, � ������� ������� ����������� ����� ��� ������� ��/������/����:
                    -- ����� ��������� � ������� ����, ��� ������� �������� ���� � �� �� ��������������
                    lag(case when mr.auto_flag = 1 
                             then to_date(null) 
                             else mr.rate_date 
                        end ignore nulls) over(partition by tb_crn.iso_alfa, tb_crn.tb_id order by dt.rate_date) as source_rate_date,
                    mr.auto_flag                   
               from -- ������ ���� ��� � ��������������� �������                
                    (select var_date_from + level - 1 as rate_date
                       from dual
                     connect by level <= var_date_to - var_date_from + 1) dt
                    -- ������ ���� ��/�����, ��� ������� � ��������������� ������� ���� �����
                    cross join (select distinct tb_id, iso_alfa
                                  from business.rf_metal_rates
                                 where rate_date between var_date_from and var_date_to and
                                       tb_id = nvl(var_tb_id, tb_id)) tb_crn
                    -- ��� ������������ ����� ��� ��/����� � ��������������� �������
                    left join business.rf_metal_rates mr on mr.iso_alfa = tb_crn.iso_alfa and
                                                            mr.tb_id = tb_crn.tb_id and
                                                            mr.rate_date = dt.rate_date) new_mr
            join business.rf_metal_rates src_mr on src_mr.iso_alfa = new_mr.iso_alfa and
                                                   src_mr.tb_id = new_mr.tb_id and
                                                   src_mr.rate_date = new_mr.source_rate_date  
      WHERE nvl(new_mr.auto_flag, 1) = 1 and       -- �� ���� ��� �����, ���� �� ��������������
            new_mr.source_rate_date is not null) v -- ���� ����, � ������� ����� ����������� ����
    ON (t.iso_alfa  = v.iso_alfa and
        t.tb_id     = v.tb_id and
        t.rate_date = v.rate_date) 
    WHEN NOT MATCHED THEN
      INSERT (iso_alfa, rate_date, tb_id, rate, buy_rate, sell_rate, auto_flag)
      VALUES (v.iso_alfa, v.rate_date, v.tb_id, v.rate, v.buy_rate, v.sell_rate, 1)
    WHEN MATCHED THEN
      UPDATE
         SET t.rate       = v.rate, 
             t.buy_rate   = v.buy_rate, 
             t.sell_rate  = v.sell_rate, 
             t.auto_flag  = 1;
  
    commit;
  end add_metal_rates;
  
  --������� ����  �������� 
  function get_acct_id_by_param(bck_id in varchar2, n20_id in varchar2)
    return number is
    ret_id        number;
    cnt           number;
    main_rec      number;
    collapsed_rec number;
  begin
    ---------------------
    --
    -- ������� ���������!
    --
    RETURN NULL;
    ---------------------
    
    cnt := 2;
    loop
      -- ���� ��������� ������ ��������������� ��������� ��������������� � ������� �������������
      select count(distinct t.mantas_seq_id)
        into cnt
        from std.recode_acct t
       where ((bck_id = t.source_id and t.source_id_type = 'BCK') or
             (n20_id = t.source_id and t.source_id_type = 'N20'))
         and coalesce(bck_id, n20_id, '0') != '0';
      case
        when cnt = 0 then
          -- ���� ������ ������� ����������� � ������� ������������� �� ���������� - ���������� ����� id
          ret_id := business.acct_seq.nextval;
        
        when cnt = 1 then
          -- ���� ���������� ������������� ���� id - ������ ���
          select distinct t.mantas_seq_id
            into ret_id
            from std.recode_acct t
           where ((bck_id = t.source_id and t.source_id_type = 'BCK') or
                 (n20_id = t.source_id and t.source_id_type = 'N20'))
             and coalesce(bck_id, n20_id, '0') != '0';
        
        when cnt > 1 then
          -- ���� ���������� ������������� ������ ������ ����� - ����������� �� � ����
          begin
            -- ��������� ������������ ����
            select min(t.mantas_seq_id), max(t.mantas_seq_id)
              into main_rec, collapsed_rec
              from std.recode_acct t
             where ((bck_id = t.source_id and t.source_id_type = 'BCK') or
                   (n20_id = t.source_id and t.source_id_type = 'N20'))
               and coalesce(bck_id, n20_id, '0') != '0';
          
            update std.recode_acct t -- ���������� ������������� ������
               set t.mantas_seq_id = main_rec,
                   t.collapsed_ids = nvl2(collapsed_ids,
                                          collapsed_ids || ', ',
                                          null) || collapsed_rec
             where t.mantas_seq_id = collapsed_rec;
          
            /*            update business.acct t
               set t.collapsed_flag = 1
                  ,collapse_is_code = (select main_is_code from dwh.ref_contractors  where cnt_id = main_rec)
                  ,collapse_time = sysdate
            where cnt_id = collapsed_rec; */
          end;
          ret_id := -1;
      end case;
      if cnt <= 1 then
        exit;
      end if;
    end loop;
    return(ret_id);
  end;
  
  function get_new_acct_seq_id return number parallel_enable is
    result number;
  begin
    select business.acct_seq.nextval into result from dual;
    return(result);
  end get_new_acct_seq_id;
  
  function get_new_clsf_code_seq_id return number is
    result number;
  begin
    select business.rf_cust_clsf_code_seq.nextval into result from dual;
    return result;
  end;
    
    --CUST_CUST_SEQ
 function get_new_cust_cust_seq_id return number is
   result number;
 begin
   select business.cust_cust_seq.nextval into result from dual;
   return result;
 end;
    
  function collapse_recode_acct(bck_id in varchar2, eks_id in varchar2, nav_id in varchar2, nar_id in varchar2, n20_id in varchar2)
    return varchar2 is
    return_var    varchar2(100);
    main_rec      number;
    collapsed_rec number;
  begin
    ---------------------
    --
    -- ������� ���������!
    --
    RETURN NULL;
    ---------------------
    --
    -- ������ ���� ������ ����� ����� n20_id � ����� ���� ������������� �������-��������� 
    --
    if trim(n20_id) is null or
       case when trim(bck_id) is not null then 1 else 0 end + 
       case when trim(eks_id) is not null then 1 else 0 end + 
       case when trim(nav_id) is not null then 1 else 0 end + 
       case when trim(nar_id) is not null then 1 else 0 end <> 1 Then
      std.pkg_log.error('�������� �������� ���������: ������ ���� ������ ����� ����� (n20_id) � ����� ���� ������������� �� �������-���������', 'std.aml_tools.collapse_recode_acct');
      return null;
    end if;      
    --
    -- ��������, ��� ������������� ���� ��������, ��������� �������������� ������
    -- �� ���� ������ �������� ���, �� ������� "�������" ������������� N20
    --
    select max(decode(t.source_id_type, 'N20', t.mantas_seq_id)), 
           max(decode(t.source_id_type, 'N20', to_number(null), t.mantas_seq_id)) 
      into main_rec, collapsed_rec
      from std.recode_acct t
     where ((trim(bck_id) is not null and t.source_id_type = 'BCK' and t.source_id = trim(bck_id)) or
            (trim(eks_id) is not null and t.source_id_type = 'EKS' and t.source_id = trim(eks_id)) or
            (trim(nav_id) is not null and t.source_id_type = 'NAV' and t.source_id = trim(nav_id)) or
            (trim(nar_id) is not null and t.source_id_type = 'NAR' and t.source_id = trim(nar_id)) or
                                         (t.source_id_type = 'N20' and t.source_id = trim(n20_id)));

    -- ������ �������� ������������� mantas_seq_id �� ������ �����
    if main_rec is null Then
      std.pkg_log.warn('������� ��������� ����������� ������, ��� ���� �� ��������������� ����� ����� (N20) � ������� �������������. N20: '||n20_id||
                       case when trim(bck_id) is not null then ', BCK: '||bck_id end||
                       case when trim(eks_id) is not null then ', EKS: '||eks_id end||
                       case when trim(nav_id) is not null then ', NAV: '||nav_id end||
                       case when trim(nar_id) is not null then ', NAR: '||nar_id end, 'std.aml_tools.collapse_recode_acct');
      return null;
    end if;

    -- ������ �������� ������������� mantas_seq_id �� �������������� ����� � ���������
    if collapsed_rec is null Then
      std.pkg_log.warn('������� ��������� ����������� ������, ��� ���� �� ��������������� ������������� ����� � ������� �������������. N20: '||n20_id||
                       case when trim(bck_id) is not null then ', BCK: '||bck_id end||
                       case when trim(eks_id) is not null then ', EKS: '||eks_id end||
                       case when trim(nav_id) is not null then ', NAV: '||nav_id end||
                       case when trim(nar_id) is not null then ', NAR: '||nar_id end, 'std.aml_tools.collapse_recode_acct');
      return null;
    end if;

    -- �������������� ������ ���� ������, ����� ���������� ������
    if main_rec = collapsed_rec Then
      std.pkg_log.warn('������� ��������� ����������� ������, ��� ���� � ����������� ��� �������������. N20: '||n20_id||
                       case when trim(bck_id) is not null then ', BCK: '||bck_id end||
                       case when trim(eks_id) is not null then ', EKS: '||eks_id end||
                       case when trim(nav_id) is not null then ', NAV: '||nav_id end||
                       case when trim(nar_id) is not null then ', NAR: '||nar_id end, 'std.aml_tools.collapse_recode_acct');
      return null;
    end if;      

    std.pkg_log.info('��������� ������ �� ����� � id: '||to_char(collapsed_rec)||' �� ���� � id: '||to_char(main_rec), 'std.aml_tools.collapse_recode_acct');
    --
    -- ���������� ���� � ������� �������������
    --
    update std.recode_acct t 
       set t.mantas_seq_id = main_rec,
           t.collapsed_ids = nvl2(collapsed_ids, collapsed_ids|| ', ', null) || to_char(collapsed_rec)
     where t.mantas_seq_id = collapsed_rec;
    --
    -- ���������� ���� � �������� ��������
    -- ARH-������� �� ��������, ����� ����������� ��������� �������� �� ������ �� ���������!
    --
    -- �����������
    update /*+ INDEX(wt)*/ business.wire_trxn wt
       set wt.rf_benef_acct_seq_id = main_rec,
           wt.benef_acct_id = decode(wt.benef_acct_id_type_cd, 'IA', to_char(main_rec), wt.benef_acct_id)           
     where rf_benef_acct_seq_id = collapsed_rec;

    update /*+ INDEX(wt)*/ business.wire_trxn wt
       set wt.rf_orig_acct_seq_id = main_rec,
           wt.orig_acct_id = decode(wt.orig_acct_id_type_cd, 'IA', to_char(main_rec), wt.orig_acct_id)           
     where rf_orig_acct_seq_id = collapsed_rec;

    -- ��������
    update /*+ INDEX(ct)*/ business.cash_trxn ct
       set rf_acct_seq_id = main_rec,
           acct_intrl_id = to_char(main_rec)           
     where rf_acct_seq_id = collapsed_rec;

    -- ����������
    update /*+ INDEX(bot)*/ business.back_office_trxn bot
       set acct_intrl_id = to_char(main_rec)           
     where rf_acct_seq_id = collapsed_rec;

    -- �������������� ������� �� �����
    -- ���������� ��� ����������� ����� ��������/����, ������� ���� �� ������������ ����� � ��� �� ����������
    insert into business.cust_acct(cust_intrl_id,acct_intrl_id, cust_acct_role_cd, data_dump_dt, cust_acct_seq_id, 
                                   src_sys_cd, prcsng_batch_nm, rf_partition_key, rf_subpartition_key, rf_src_cd)  
           (select /*+ INDEX(ca ca2)*/ca.cust_intrl_id, to_char(main_rec), ca.cust_acct_role_cd, ca.data_dump_dt, std.aml_tools.get_new_cust_acct_seq_id, 
                   ca.src_sys_cd, ca.prcsng_batch_nm, ca.rf_partition_key, ca.rf_subpartition_key, ca.rf_src_cd
              from business.cust_acct ca
                   left join business.cust_acct ca2 on ca2.cust_intrl_id = ca.cust_intrl_id and
                                                       ca2.acct_intrl_id = to_char(main_rec) and
                                                       ca2.cust_acct_role_cd = ca.cust_acct_role_cd
             where ca.rf_acct_seq_id = collapsed_rec and
                   ca2.cust_intrl_id is null);                    

    -- ���������  ���������
    COMMIT;

    std.pkg_log.info('��������� ���������� ������ �� ����� � id: '||to_char(collapsed_rec)||' �� ���� � id: '||to_char(main_rec), 'std.aml_tools.collapse_recode_acct');
    
    return_var := 'Success';
    return(return_var);
  end collapse_recode_acct;

  function get_new_cust_acct_seq_id return number is
    result number;
  begin
    select business.cust_acct_seq.nextval into result from dual;
    return(result);
  end get_new_cust_acct_seq_id;

  function get_new_org_seq_id return number is
    result number;
  begin
    select business.org_seq.nextval into result from dual;
    return(result);
  end get_new_org_seq_id;

  function get_new_org_rlshp_seq_id return number is
    result number;
  begin
    select business.org_rlshp_seq.nextval into result from dual;
    return(result);
  end get_new_org_rlshp_seq_id;

  function get_new_cust_seq_id return number is
    result number;
  begin
    select business.cust_seq.nextval into result from dual;
    return(result);
  end get_new_cust_seq_id;

  function get_new_cust_doc_seq_id return number is
    result number;
  begin
    select business.rf_cust_id_doc_seq.nextval into result from dual;
    return result;
  end get_new_cust_doc_seq_id;

  function get_new_addr_seq_id return number is
    result number;
  begin
    select business.cust_addr_seq.nextval into result from dual;
    return result;
  end get_new_addr_seq_id;

  function get_new_cust_phon_seq return number is
    result number;
  begin
    select business.cust_phon_seq.nextval into result from dual;
    return result;
  end get_new_cust_phon_seq;

  function get_new_trxn_seq return number is
    result number;
  begin
    select business.trxn_seq.nextval into result from dual;
    return result;
  end get_new_trxn_seq;

  function get_new_email_seq return number is
    result number;
  begin
    select business.cust_email_addr_seq.nextval into result from dual;
    return result;
  end get_new_email_seq;
  
   function get_new_cust_rlshp_type_seq return number is
    result number;
  begin
    select business.rf_cust_rlshp_type_seq.nextval into result from dual;
    return result;
  end get_new_cust_rlshp_type_seq;
  
   function get_new_scrty_seq_id return number is--����� �������� ������������������ ��� ������ ����� (scrty_seq_id)
    result number;
  begin
    select business.scrty_seq.nextval into result from dual;
    return result;
  end get_new_scrty_seq_id;

  function get_new_trxn_scrty_seq_id return number is--����� �������� ������������������ ��� ������ ������ ������ - �������� (trxn_scrty_seq_id)
    result number;
  begin
    select business.rf_trxn_scrty_seq.nextval into result from dual;
    return result;
  end get_new_trxn_scrty_seq_id;
  
  procedure load_rmdr000mdm_contequiv(p_last_loaded_date date,  p_mdmtype varchar2 default 'MDR') as
  begin
    insert all
    --insert mdm_id
    when
      (new_mdm_flag = 1  and dublicate_flag = 0 and dub_cont_id_number = 1) then into std.recode_cust
      (source_id_type, source_id, mantas_seq_id, etalon_flag, insert_sys_cd, subpartition_key)
    values
      (p_mdmtype, cont_id, common_mantas_id, 0,  p_mdmtype, aml_partition_key)
    -- insert source_id
    when
      (new_src_flag = 1 and dublicate_flag = 0) then into std.recode_cust
      (source_id_type, source_id, mantas_seq_id, etalon_flag, source_id_type, subpartition_key)
    values
      (sys_inst_cd,
       source_id,
       common_mantas_id,
       0,
       sys_inst_cd,
       common_partition_key)
       
    -- change etalon
   when
      (new_mdm_flag = 1  and dublicate_flag = 0 and dub_cont_id_number = 1) then into stg.ETALON_CHANGING
      (
       mdm_mantas_seq_id) --�������_�����_�������
    values
      (common_mantas_id)
       
    --dublicate problem
    when
      (dublicate_flag = 1) then into STG.dublication_problem
      (mdm_id,
       source_id,
       source_id_type,
       mdm_mantas_seq_id,
       src_mantas_seq_id,
       subpartition_key) -- ������� ����������
    values
      (cont_id,
       source_id,
       sys_inst_cd,
       mdm_mantas_id,
       source_mantas_id,
       common_partition_key)
    SELECT cont_id,
       source_id,
       sys_inst_cd,
       mdm_mantas_id,
       source_mantas_id,
       dub_cont_id_number,
       new_mdm_flag,
       new_src_flag,
       --CNT_S_MANTAS_ID,
       dublicate_flag,
       aml_partition_key,
       case when new_mdm_flag =1  and dublicate_flag !=1 then first_value(common_mantas_id ignore nulls) over(partition by cont_id)
        else  common_mantas_id  end as common_mantas_id,
       common_partition_key   
    FROM (select ce.cont_id,
             ce.admin_client_id aci,
             ce.aml_partition_key,
             decode(ssi.sys_inst_cd, 'BCK', p.id_mega || '|' || p.id_major || '|' || p.id_minor, ce.admin_client_id) as source_id,
             case
               when ssi.sys_cd = 'BCK' and p.id_mega is not null then
                p.id_mega || '|' || p.id_major || '|' || p.id_minor end as bck_id,
             ssi.sys_inst_cd,
             rc1.mantas_seq_id as mdm_mantas_id,
             rc2.mantas_seq_id as source_mantas_id,
             row_number() over(partition by ce.cont_id order by ce.h_create_dt desc) as dub_cont_id_number,
             decode(rc1.mantas_seq_id, null, 1, 0) as new_mdm_flag,
             decode(rc2.mantas_seq_id, null, 1, 0) as new_src_flag,
             case when count(distinct rc2.mantas_seq_id) over(partition by ce.cont_id)>1 
               or(  count(distinct rc2.mantas_seq_id) over(partition by ce.cont_id)=1 and rc1.mantas_seq_id != rc2.mantas_seq_id) then  1 else 0 end as dublicate_flag,
               
              case 
         --���� ��� �� ������ ���������� �� source_id, �� ������-�� ���� ����������� mdm_id
          when (count(distinct rc2.mantas_seq_id) over(partition by ce.cont_id)) = 0 and rc1.mantas_seq_id is not null then rc1.mantas_seq_id
         --���� ��� �� ������ ���������� �� source_id, �� �� mdm_id �� ������ ������ ����������� ����� ������� 
          when (count(distinct rc2.mantas_seq_id) over(partition by ce.cont_id)) = 0 and rc1.mantas_seq_id is null 
                  and  (row_number() over(partition by ce.cont_id order by ce.h_create_dt desc) )=1 
            then std.aml_tools.get_new_cust_SEQ_id()
         --���� ��� source_id ������� �� ���� mantas_id, �� ���� source_id ����� ���� mdm_Id
          when (count(distinct rc2.mantas_seq_id) over(partition by ce.cont_id)) = 1 and ( rc1.mantas_seq_id is null or rc2.mantas_seq_id is null  )
            then first_value( rc2.mantas_seq_id ignore nulls) over(partition by ce.cont_id order by ce.cont_id)
            end as common_mantas_id,
          case when (ssi.sys_cd = 'EKS') and (nvl(nvl(nvl(p.aml_subpartition_key, rc1.subpartition_key), rc2.subpartition_key), p1.aml_subpartition_key)) is null
            then ora_hash(ce.admin_client_id ,  31) + 1 end as common_partition_key   
      from contequiv ce 
     inner join contact c
        on ce.cont_id = c.cont_id
     inner join std.src_sys_instances ssi on ssi.mdm_sys_tp_cd = ce.admin_sys_tp_cd
      left join rcod038client.person p on ssi.sys_cd = 'BCK' and to_char(p.untb) = to_char(ce.admin_client_id)
      left join rcod038client.person p1 on ssi.sys_cd = 'EKS' and to_char(p.untb) = to_char(ce.admin_client_id)
      left join std.recode_cust rc1 on rc1.source_id_type =  p_mdmtype and rc1.source_id = to_char(ce.cont_id)
      left join std.recode_cust rc2
        on rc2.source_id_type = ssi.sys_cd
       and rc2.source_id = decode(ssi.sys_cd, 'BCK',  p.id_mega || '|' || p.id_major || '|' || p.id_minor, ce.admin_client_id)
     where ce.mdmtype = p_mdmtype and c.mdmtype = p_mdmtype and
           c.client_st_tp_cd != 1 --������ �������
           and ssi.mdm_sys_tp_cd <> 3 and sys_inst_cd in ('EKS', 'BCK', 'MDR', 'MDC', 'NAV', 'NAR')
           and ( (ssi.sys_cd = 'BCK' and p.id_mega is not null) or ssi.sys_cd != 'BCK' ));
    commit;
  end load_rmdr000mdm_contequiv;

  procedure change_etalon_flag(p_mdmtype varchar2 default 'MDR') as
  begin
    --�������� ������� ��������, � ������� �������� ������
    delete from business.cust_addr a
     where a.cust_intrl_id in
           (select ec.mdm_mantas_seq_id from stg.etalon_changing ec);
    --�������� ��������� ��������, � ������� �������� ������  
    delete from business.cust_phon a
     where a.cust_intrl_id in
           (select ec.mdm_mantas_seq_id from stg.etalon_changing ec);
    --�������� ������� ��������, � ������� �������� ������
    delete from business.cust_email_addr a
     where a.cust_intrl_id in
           (select ec.mdm_mantas_seq_id from stg.etalon_changing ec);
    --�������� �������������� ���������� ��������, � ������� �������� ������
    delete from business.rf_cust_id_doc a
     where a.cust_seq_id in
           (select ec.mdm_mantas_seq_id from stg.etalon_changing ec);
    --����� � ��� ���� �������
      update std.recode_cust rc
      set rc.etalon_flag=0
      where rc.mantas_seq_id in ( select mdm_mantas_seq_id  from stg.etalon_changing e);
      --���������� ����� ���������� ���� �������

      merge into std.recode_cust T
      using ( 
      select distinct e.mdm_mantas_seq_id, --rc.source_id, rc.etalon_flag, c.client_st_tp_cd,
             last_value(rc.source_id) over( partition by  e.mdm_mantas_seq_id order by nvl( decode(c.client_st_tp_cd,1,-1,c.client_st_tp_cd) ,2), c.h_create_dt, c.cont_id 
                                      rows between unbounded preceding and unbounded following) etalon_mdm_id
      from stg.etalon_changing e  
      join std.recode_cust rc 
        on rc.mantas_seq_id = e.mdm_mantas_seq_id 
       and rc.source_id_type= p_mdmtype
      left 
      join rmdr000mdm.contact c on c.cont_id = rc.source_id ) S
        on (t.source_id= s.etalon_mdm_id and t.source_id_type= p_mdmtype)
      when matched then update set t.etalon_flag=1;
      commit; 
  end;

  procedure solve_dublicate_problem(p_mdmtype varchar2 default 'MDR') as
  begin
    --������ ���� ������ � �������� �������.
    update std.recode_cust rc
       set rc.etalon_flag = 0
     where rc.mantas_seq_id in
           (select common_mantas_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                    --1 - �����������
                    --0 - ����������
                           case when dp.src_mantas_seq_id = (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0);
             
    insert all
    --insert new mdm_id
    when
      (dub_num = 1) then into std.recode_cust
      (source_id_type, source_id, mantas_seq_id, etalon_flag, subpartition_key)
    values
      (p_mdmtype, mdm_id, common_mantas_id, 0, ora_hash(common_mantas_id,  63) + 1)
    --insert new source_id
    when
      (src_mantas_seq_id is null) then into std.recode_cust
      (source_id_type, source_id, mantas_seq_id, etalon_flag, subpartition_key)
    values
      (source_id_type, source_id, common_mantas_id, 0, subpartition_key)
      select dp.*,
             min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
             row_number() over(partition by dp.mdm_id order by dp.source_id desc) as dub_num
        from STG.dublication_problem dp;
/*�������� ������_���� � ������� ��������� � ������� �� ��������, ��������� ��������� �� ��������� �������*/  
    update std.recode_cust rc
       set rc.mantas_seq_id =
           (select common_mantas_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0
               and rc.mantas_seq_id = to_char(t.src_mantas_seq_id)),
           rc.etalon_flag   = 0
     where rc.mantas_seq_id in
           (select src_mantas_seq_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0);
  
    --ACCT-
    update business.acct a
       set a.prmry_cust_intrl_id =
           (select common_mantas_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0
               and t.src_mantas_seq_id = a.prmry_cust_intrl_id)
     where a.prmry_cust_intrl_id in
           (select src_mantas_seq_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0);
    commit;
  
    --CASH
    update business.cash_trxn c
       set c.cndtr_acct_id = case
                               when c.cndtr_acct_id_type_cd = 'CU' then
                                (select to_char(common_mantas_id)
                                   from (select mdm_id,
                                                dp.source_id,
                                                dp.source_id_type,
                                                dp.src_mantas_seq_id,
                                                min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                case when dp.src_mantas_seq_id =
                                                       (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                         --1 - �����������
                                         --0 - ����������
                                           from STG.dublication_problem dp
                                          where dp.src_mantas_seq_id is not null) t
                                  where flag = 0
                                    and c.cndtr_acct_id =
                                        to_char(t.src_mantas_seq_id))
                               else
                                c.cndtr_acct_id
                             end,
           c.rf_cust_seq_id       = nvl((select to_char(common_mantas_id)
                                          from (select mdm_id,
                                                       dp.source_id,
                                                       dp.source_id_type,
                                                       dp.src_mantas_seq_id,
                                                       min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                       case  when dp.src_mantas_seq_id =
                                                              (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                                --1 - �����������
                                                --0 - ����������
                                                  from STG.dublication_problem dp
                                                 where dp.src_mantas_seq_id is not null) t
                                         where flag = 0
                                           and c.rf_cust_seq_id =
                                               to_char(t.src_mantas_seq_id)),
                                        c.rf_cust_seq_id),
           c.rf_cndtr_cust_seq_id = nvl((select to_char(common_mantas_id)
                                          from (select mdm_id,
                                                       dp.source_id,
                                                       dp.source_id_type,
                                                       dp.src_mantas_seq_id,
                                                       min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                       case when dp.src_mantas_seq_id =
                                                              (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                                --1 - �����������
                                                --0 - ����������
                                                  from STG.dublication_problem dp
                                                 where dp.src_mantas_seq_id is not null) t
                                         where flag = 0
                                           and c.rf_cndtr_cust_seq_id =
                                               to_char(t.src_mantas_seq_id)),
                                        c.rf_cndtr_cust_seq_id)
     where c.cndtr_acct_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or c.rf_cust_seq_id in
           (select src_mantas_seq_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or c.rf_cndtr_cust_seq_id in
           (select src_mantas_seq_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0);
    commit;
  
    --CUST_ACCT
    update business.cust_acct ca
       set ca.cust_intrl_id = nvl((select to_char(common_mantas_id)
                                    from (select mdm_id,
                                                 dp.source_id,
                                                 dp.source_id_type,
                                                 dp.src_mantas_seq_id,
                                                 min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                 case when dp.src_mantas_seq_id =
                                                        (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                          --1 - �����������
                                          --0 - ����������
                                            from STG.dublication_problem dp
                                           where dp.src_mantas_seq_id is not null) t
                                   where flag = 0
                                     and ca.cust_intrl_id =
                                         to_char(t.src_mantas_seq_id)),
                                  ca.cust_intrl_id)
     where ca.cust_intrl_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0);
  
    --WIRE
    update business.wire_trxn w
       set w.benef_acct_id = case
                               when w.benef_acct_id_type_cd = 'CU' then
                                (select to_char(common_mantas_id)
                                   from (select mdm_id,
                                                dp.source_id,
                                                dp.source_id_type,
                                                dp.src_mantas_seq_id,
                                                min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                case when dp.src_mantas_seq_id =
                                                       (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                         --1 - �����������
                                         --0 - ����������
                                           from STG.dublication_problem dp
                                          where dp.src_mantas_seq_id is not null) t
                                  where flag = 0
                                    and w.benef_acct_id =
                                        to_char(t.src_mantas_seq_id))
                               else
                                w.benef_acct_id
                             end,
           w.orig_acct_id = case
                              when w.orig_acct_id_type_cd = 'CU' then
                               (select to_char(common_mantas_id)
                                  from (select mdm_id,
                                               dp.source_id,
                                               dp.source_id_type,
                                               dp.src_mantas_seq_id,
                                               min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                               case when dp.src_mantas_seq_id =
                                                      (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                        --1 - �����������
                                        --0 - ����������
                                          from STG.dublication_problem dp
                                         where dp.src_mantas_seq_id is not null) t
                                 where flag = 0
                                   and w.orig_acct_id =
                                       to_char(t.src_mantas_seq_id))
                              else
                               w.orig_acct_id
                            end,
           w.scnd_benef_acct_id = case
                                    when w.scnd_benef_acct_id_type_cd = 'CU' then
                                     (select to_char(common_mantas_id)
                                        from (select mdm_id,
                                                     dp.source_id,
                                                     dp.source_id_type,
                                                     dp.src_mantas_seq_id,
                                                     min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                     case when dp.src_mantas_seq_id =
                                                            (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                              --1 - �����������
                                              --0 - ����������
                                                from STG.dublication_problem dp
                                               where dp.src_mantas_seq_id is not null) t
                                       where flag = 0
                                         and w.scnd_benef_acct_id =
                                             to_char(t.src_mantas_seq_id))
                                    else
                                     w.scnd_benef_acct_id
                                  end,
           w.scnd_orig_acct_id = case
                                   when w.scnd_orig_acct_id_type_cd = 'CU' then
                                    (select to_char(common_mantas_id)
                                       from (select mdm_id,
                                                    dp.source_id,
                                                    dp.source_id_type,
                                                    dp.src_mantas_seq_id,
                                                    min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                    case when dp.src_mantas_seq_id =
                                                           (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                             --1 - �����������
                                             --0 - ����������
                                               from STG.dublication_problem dp
                                              where dp.src_mantas_seq_id is not null) t
                                      where flag = 0
                                        and w.scnd_orig_acct_id =
                                            to_char(t.src_mantas_seq_id))
                                   else
                                    w.scnd_orig_acct_id
                                 end,
           w.rf_benef_cust_seq_id      = nvl((select to_char(common_mantas_id)
                                               from (select mdm_id,
                                                            dp.source_id,
                                                            dp.source_id_type,
                                                            dp.src_mantas_seq_id,
                                                            min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                            case when dp.src_mantas_seq_id =
                                                                   (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                                     --1 - �����������
                                                     --0 - ����������
                                                       from STG.dublication_problem dp
                                                      where dp.src_mantas_seq_id is not null) t
                                              where flag = 0
                                                and w.rf_benef_cust_seq_id =
                                                    to_char(t.src_mantas_seq_id)),
                                             w.rf_benef_cust_seq_id),
           w.rf_orig_cust_seq_id       = nvl((select to_char(common_mantas_id)
                                               from (select mdm_id,
                                                            dp.source_id,
                                                            dp.source_id_type,
                                                            dp.src_mantas_seq_id,
                                                            min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                            case when dp.src_mantas_seq_id =
                                                                   (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                                     --1 - �����������
                                                     --0 - ����������
                                                       from STG.dublication_problem dp
                                                      where dp.src_mantas_seq_id is not null) t
                                              where flag = 0
                                                and w.rf_orig_cust_seq_id =
                                                    to_char(t.src_mantas_seq_id)),
                                             w.rf_orig_cust_seq_id),
           w.rf_scnd_benef_cust_seq_id = nvl((select to_char(common_mantas_id)
                                               from (select mdm_id,
                                                            dp.source_id,
                                                            dp.source_id_type,
                                                            dp.src_mantas_seq_id,
                                                            min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                            case when dp.src_mantas_seq_id =
                                                                   (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                                     --1 - �����������
                                                     --0 - ����������
                                                       from STG.dublication_problem dp
                                                      where dp.src_mantas_seq_id is not null) t
                                              where flag = 0
                                                and w.rf_scnd_benef_cust_seq_id =
                                                    to_char(t.src_mantas_seq_id)),
                                             w.rf_scnd_benef_cust_seq_id),
           w.rf_scnd_orig_cust_seq_id  = nvl((select to_char(common_mantas_id)
                                               from (select mdm_id,
                                                            dp.source_id,
                                                            dp.source_id_type,
                                                            dp.src_mantas_seq_id,
                                                            min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                                                            case  when dp.src_mantas_seq_id =
                                                                   (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                                                     --1 - �����������
                                                     --0 - ����������
                                                       from STG.dublication_problem dp
                                                      where dp.src_mantas_seq_id is not null) t
                                              where flag = 0
                                                and w.rf_scnd_orig_cust_seq_id =
                                                    to_char(t.src_mantas_seq_id)),
                                             w.rf_scnd_orig_cust_seq_id)
     where w.benef_acct_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or w.orig_acct_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or w.scnd_orig_acct_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or w.scnd_benef_acct_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or w.rf_benef_cust_seq_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or w.rf_orig_cust_seq_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or w.rf_scnd_benef_cust_seq_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0)
        or w.rf_scnd_orig_cust_seq_id in
           (select to_char(src_mantas_seq_id)
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0);
    commit;
    ---� "��������" ������� � ������� CUST ����������� ���� ��������
  merge into business.cust T
   using ( select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                    --1 - �����������
                    --0 - ����������
                           case when dp.src_mantas_seq_id = (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) s
                     on (t.cust_seq_id = s.src_mantas_seq_id and s.flag=0)
 when matched then update set t.rf_collapsed_fl='Y';
 --  � ������� ������������� � �������� ������ � ������ ���������� 
     --������������ �������������� �������� �������.
 merge into std.recode_cust T
 using (
       select decode( flag, 1, rtrim( listagg( src_mantas_seq_id, ',') within group (order by flag ) 
        over ( partition by common_mantas_id ), ','||to_char(src_mantas_seq_id)), 
        listagg( src_mantas_seq_id, ',') within group (order by flag ) 
        over ( partition by common_mantas_id )) as collapsed_ids
        , a.*
  from ( select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                    --1 - �����������
                    --0 - ����������
                           case when dp.src_mantas_seq_id = (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) a) S
   on (t.mantas_seq_id = s.common_mantas_id and s.flag=1)
   when matched then update set t.collapsed_ids=t.collapsed_ids||','||s.collapsed_ids;

     --����������� ���� ������� ��� �������� �������
     insert into stg.etalon_changing(mdm_mantas_seq_id)
     select common_mantas_id
      from ( select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                    --1 - �����������
                    --0 - ����������
                           case when dp.src_mantas_seq_id = (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) a
            where flag=1;
            
      change_etalon_flag(p_mdmtype);
/*
    update std.recode_cust rc
       set rc.mantas_seq_id =
           (select common_mantas_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0
               and rc.mantas_seq_id = to_char(t.src_mantas_seq_id)),
           rc.etalon_flag   = 0
     where rc.mantas_seq_id in
           (select src_mantas_seq_id
              from (select mdm_id,
                           dp.source_id,
                           dp.source_id_type,
                           dp.src_mantas_seq_id,
                           min(dp.src_mantas_seq_id) over(partition by dp.mdm_id) common_mantas_id,
                           case when dp.src_mantas_seq_id =
                                  (min(dp.src_mantas_seq_id) over(partition by dp.mdm_id)) then 1 else 0 end as flag
                    --1 - �����������
                    --0 - ����������
                      from STG.dublication_problem dp
                     where dp.src_mantas_seq_id is not null) t
             where flag = 0);*/
    commit;
  end;

  procedure load_mdmr_recode_cust(p_last_loaded_date date, p_mdmtype varchar2 default 'MDR') as
  begin
    execute immediate 'truncate table stg.etalon_changing';
    execute immediate 'truncate table stg.dublication_problem';
    load_rmdr000mdm_contequiv(p_last_loaded_date, p_mdmtype);
    dbms_stats.gather_schema_stats(ownname => 'STG', no_invalidate => false);
    --change_etalon_flag(p_mdmtype);
    solve_dublicate_problem(p_mdmtype);
  end load_mdmr_recode_cust;


--������������� ��������. ��. �������� � ������� STD.RECOGN_STATE
function get_regn_name(p_regn_name varchar2) return varchar2 deterministic is
  result varchar2(200);
  v_cnt  number;
begin
  with rgn_templ as
   (select '%' || upper(object_nm) || '%' as template_nm, object_nm et_nm
      from business.rf_addr_object
     where level_nb = 1
    union
    select s.recogn_template, s.state_nm from std.recogn_state s)
  select distinct a.et_nm, count(distinct a.et_nm) over(partition by a.et_nm) as cnt
    into result, v_cnt
    from rgn_templ a
   where p_regn_name like a.template_nm and
  --����� "����������" �������� ������������� ����� ��, � ������� ����� ������ �����������
  length(template_nm) = (
  select max(length(template_nm))
    from rgn_templ a
   where p_regn_name like a.template_nm);
  return result;
exception
  when too_many_rows then
    return null;
end get_regn_name;
--
-- �� ������������ ������ (��������, � ���������� � ������� ���������� ���������) ���������� �������������� ��� ������ �� ISO
-- ��������� ������������ ������ ���������� �� ������� BUSINESS.RF_COUNTRY, ������������� ��������� - �� STD.RECOGN_COUNTRY
--
function get_cntry_cd(par_cntry_name varchar2)
  return business.rf_country.cntry_cd%type RESULT_CACHE is
  var_cntry_cd business.rf_country.cntry_cd%type;
begin
  select max(cntry_cd)
    into var_cntry_cd
    from (select first_value(cntry_cd) over(order by priority) as cntry_cd
            from (select cntry_cd, 1 as priority
                    from business.rf_country
                   where upper(cntry_nm) = upper(trim(par_cntry_name))
                  union all
                  select cntry_cd, 2 as priority
                    from std.recogn_country
                   where upper(src_cntry_name) = upper(trim(par_cntry_name))));
  return var_cntry_cd;
end get_cntry_cd;
--
--����� ������ ������ �� ��������� ����������
function get_scrty_id(full_num   in business.scrty.alt_scrty_intrl_id%type, -- ������ ����� ��
                      num        in business.scrty.cstm_6_tx%type, -- ����� �� ��� �����
                      scrty_date in business.scrty.scrty_issue_dt%type, -- ���� ������� ��
                      nominal    in business.scrty.cstm_1_rl%type, --������� ��
                      stype      in business.scrty.prod_type_cd%type --��� ��
                      ) return number deterministic is
  ret_val number;
  CURSOR get_scrty(num business.scrty.cstm_6_tx%type, scrty_date business.scrty.scrty_issue_dt%type, nominal business.scrty.cstm_1_rl%type, stype business.scrty.prod_type_cd%type) is
         SELECT scrty_seq_id as ret_val
         from business.scrty s
         where s.cstm_6_tx = num 
           and (nvl(s.scrty_issue_dt, s.cstm_5_dt) = scrty_date or scrty_date is null)
           and (s.cstm_1_rl = nominal or nominal is null)
           and (s.prod_type_cd = stype or stype is null)
         order by s.scrty_intrl_id desc; 
         
  CURSOR get_scrty_full_nm(full_num business.scrty.alt_scrty_intrl_id%type) is 
         select s.scrty_seq_id as ret_val
           from business.scrty s
          where s.alt_scrty_intrl_id = full_num
          order by s.scrty_intrl_id desc;
begin
  if full_num is not null then
     OPEN get_scrty_full_nm(full_num);
     FETCH get_scrty_full_nm into ret_val;
     CLOSE get_scrty_full_nm;
  else
     if ret_val is null and num is not null then
       OPEN get_scrty(num, scrty_date, nominal, stype);
       FETCH get_scrty into ret_val;
       CLOSE get_scrty;
     end if;
  end if;
    return ret_val;
end get_scrty_id;
--
-- �������� rowid ������� (������� RF_ADDR_OBJECT) � ������������ � ����� ����� ��� ����� �����
--
FUNCTION get_region(
  par_kladr_cd    VARCHAR2,
  par_okato_cd    VARCHAR2
  )
RETURN ROWID RESULT_CACHE IS
  var_result    ROWID := null;
BEGIN
  --
  -- ���� ������ ���������� - ����� ���������� NULL
  --
  if trim(par_kladr_cd) is null and trim(par_okato_cd) is null Then
    return null;
  end if;  
  --
  -- � ������ ������� ���� �� ���� ����� (������ ��� ����� ������������� �������)
  --
  if trim(par_kladr_cd) is not null Then
    FOR r IN (SELECT rowid as row_id
                FROM business.rf_addr_object
               WHERE level_nb = 1 and 
                     substr(kladr_cd, 1, 2) = substr(trim(par_kladr_cd), 1, 2)
              ORDER BY 1) LOOP
      var_result := r.row_id;
      EXIT;
    END LOOP;
    
    if var_result is not null Then
      return var_result;
    end if;                   
  end if;  
  --
  -- �� ������ ������� ���� �� ���� �����
  -- � ������� ��� ����� ����� ����� ������ 2 �������� ���� � ������ ���� ���� ������������ ��� �������������:
  -- 71 -	���������	���
  -- 711 - �����-���������� ���������� ����� - ����
  -- 7114 - �����-��������	��
  -- �� ���������� ���� ����� ���� �������� ���������� ������
  --
  if trim(par_okato_cd) is not null Then
    FOR r IN (SELECT row_id
                FROM (select rowid as row_id,
                             case when length(rtrim(okato_cd, '0')) < 2 -- �������� �������� ����, �� ������ �������� �� ����� 2 ����
                                  then rpad(rtrim(okato_cd, '0'), 2, '0')
                                  else rtrim(okato_cd, '0')
                             end as region_okato_cd 
                        from business.rf_addr_object
                       where level_nb = 1)
               WHERE trim(par_okato_cd) like region_okato_cd||'%'        
              ORDER BY length(region_okato_cd) desc, row_id) LOOP
      var_result := r.row_id;
      EXIT;
    END LOOP;
  end if;  

  return var_result;
END get_region;
--
-- �������� rowid � addr_seq_id ������� (������� RF_ADDR_OBJECT) � ������������ � ��� �������������, ��������, �� ������ ����������
-- (������������ ������������ ������������ ����� ������� STD.RECOGN_STATE)
--
PROCEDURE recognise_region_internal(
  par_region_name       VARCHAR2,
  par_rowid        OUT  ROWID,
  par_addr_seq_id  OUT  NUMBER
  ) IS
  var_rowid          ROWID  := null;
  var_addr_seq_id    NUMBER := null;
BEGIN  
  --
  -- ���� ���� ������ ����� � ����� ������ - ���������� ��� rowid
  --
  SELECT max(rowid), max(addr_seq_id)
    INTO var_rowid, var_addr_seq_id
    FROM business.rf_addr_object
   WHERE level_nb = 1 and
         upper(object_nm) = upper(trim(par_region_name));

  if var_rowid is not null Then
    par_rowid := var_rowid;
    par_addr_seq_id := var_addr_seq_id;
    return;
  end if;                   
  --
  -- ���������� ������ ����� ������� �������� �������������:
  -- 1) �������� �������, ������� ������������� �������������� ������������ ������� � ������� ��������� LIKE
  -- 2) ����� "����������" �������� ������������� ����� ��, � ������� ����� ������ �����������
  -- 3) ���� ���� ����� ���� "����������" ������ � ������������ ������ ������ ��� ��� ������� � ������������ ������ 
  --    ������������� ������ � ���� �� ���������� ������������ �������, �� ������������� �������, 
  --    ����� - ������������� ���������
  -- ������ ����� ���������� ���� 1 ������, ���� 0
  --         
  FOR r IN (SELECT distinct
                   count(distinct ao.object_nm) over()                       as cnt, -- ���������� ������������ ��������
                   first_value(ao.rowid)        over(order by ao.rowid desc) as row_id,
                   first_value(ao.addr_seq_id)  over(order by ao.rowid desc) as addr_seq_id
              FROM (SELECT distinct 
                           state_nm, 
                           length(trim(rs.recogn_template)) as templ_len,
                           max(length(trim(rs.recogn_template))) over() as max_templ_len
                      FROM std.recogn_state rs
                     WHERE upper(trim(par_region_name)) like upper(trim(rs.recogn_template))) t
                   join business.rf_addr_object ao on upper(ao.object_nm) = upper(trim(t.state_nm))  
             WHERE templ_len = max_templ_len /*����� ������ "������������" � ������������ ������ �������*/) LOOP
    --
    -- ���� ��������� ����� ���� ������ - ���������� ��� rowid, ����� - ���������� null
    -- 
    --  
    if r.cnt = 1 Then
      par_rowid := r.row_id;
      par_addr_seq_id := r.addr_seq_id;
    end if;
  END LOOP;
END recognise_region_internal;
--
-- �������� rowid ������� (������� RF_ADDR_OBJECT) � ������������ � ��� �������������, ��������, �� ������ ����������
-- (������������ ������������ ������������ ����� ������� STD.RECOGN_STATE)
--
FUNCTION recognise_region(
  par_region_name    VARCHAR2
  )
RETURN ROWID RESULT_CACHE IS
  var_rowid          ROWID;
  var_addr_seq_id    NUMBER;
BEGIN
  if trim(par_region_name) is null then return null; end if;
   
  recognise_region_internal(par_region_name, var_rowid, var_addr_seq_id);
  return var_rowid;
END recognise_region;  
--
-- �������� addr_seq_id ������� (������� RF_ADDR_OBJECT) � ������������ � ��� �������������, ��������, �� ������ ����������
-- (������������ ������������ ������������ ����� ������� STD.RECOGN_STATE)
--
FUNCTION recognise_region2(
  par_region_name    VARCHAR2
  )
RETURN NUMBER RESULT_CACHE IS
  var_rowid          ROWID;
  var_addr_seq_id    NUMBER;
BEGIN
  if trim(par_region_name) is null then return null; end if;

  recognise_region_internal(par_region_name, var_rowid, var_addr_seq_id);
  return var_addr_seq_id;
END recognise_region2;  
--
-- ���������� ID ������� �� ��� ����������� ������ (UNTB)
--
FUNCTION get_cust_by_untb(
  par_untb        INTEGER,             -- ���������� ����� �������
  par_id_mega     INTEGER DEFAULT null -- ����� ��������, � �������� ��������� ������, ���� NULL - ����������� ����� �� ���� ��
) 
RETURN NUMBER IS -- ������������� ������� (business.cust.cust_seq_id)
  var_result   NUMBER;
BEGIN
  if par_untb is null Then
    return null;
  end if;

  if par_id_mega = 13 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod013client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 13 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 16 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod016client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 16 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 18 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod018client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 18 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 38 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod038client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 38 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 40 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod040client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 40 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 42 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod042client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 42 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 44 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod044client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 44 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 49 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod049client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 49 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 52 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod052client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 52 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 54 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod054client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 54 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 55 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod055client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 55 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 67 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod067client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 67 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 70 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod070client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 70 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega = 77 Then
    FOR r IN (SELECT rc.mantas_seq_id
                FROM rcod077client.person p
                     join std.recode_cust rc on rc.source_id_type = 'BCK' and
                                                rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
               WHERE p.id_mega = 77 and
                     p.untb = par_untb
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;
  elsif par_id_mega is null Then      
    FOR r IN (SELECT /*+ ORDERED INDEX(rc)*/
                     rc.mantas_seq_id
                FROM (select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod013client.person p
                       where p.id_mega = 13 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod016client.person p
                       where p.id_mega = 16 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod018client.person p
                       where p.id_mega = 18 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod038client.person p
                       where p.id_mega = 38 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod040client.person p
                       where p.id_mega = 40 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod042client.person p
                       where p.id_mega = 42 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod044client.person p
                       where p.id_mega = 44 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod049client.person p
                       where p.id_mega = 49 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod052client.person p
                       where p.id_mega = 52 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod054client.person p
                       where p.id_mega = 54 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod055client.person p
                       where p.id_mega = 55 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod067client.person p
                       where p.id_mega = 67 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod070client.person p
                       where p.id_mega = 70 and
                             p.untb = par_untb
                      union all
                      select /*+ INDEX(p)*/ id_mega, id_major, id_minor
                        from rcod077client.person p
                       where p.id_mega = 77 and
                             p.untb = par_untb) p,
                     std.recode_cust rc
               WHERE rc.source_id_type = 'BCK' and
                     rc.source_id = to_char(p.id_mega)||'|'||to_char(p.id_major)||'|'||to_char(p.id_minor)
              ORDER BY 1) LOOP
      var_result := r.mantas_seq_id;
      EXIT;
    END LOOP;               
  end if;
  
  return var_result;                                      
END get_cust_by_untb;
--
-- �������� ������������ ������������ BCK �� ��� �������������
--
FUNCTION get_clerk_fio(
  par_id_mega     INTEGER, -- ����� ��������, � �������� ��������� ������������ - clerk_fio.id_mega
  par_branchno    INTEGER, -- ����� ��� - clerk_fio.branchno 
  par_office      INTEGER, -- ����� ��� - clerk_fio.office
  par_clerk       INTEGER  -- ID ������������ - clerk_fio.clerk
) 
RETURN VARCHAR2 IS -- ��� ������������ - clerk_fio.fio
  var_result    VARCHAR2(255 CHAR);
BEGIN
  if par_id_mega is null or par_branchno is null or par_office is null or par_clerk is null Then
    return null;
  end if;
    
  if par_id_mega = 13 Then
    FOR r IN (SELECT fio
                FROM rcod013deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 16 Then
    FOR r IN (SELECT fio
                FROM rcod016deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 18 Then
    FOR r IN (SELECT fio
                FROM rcod018deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 38 Then
    FOR r IN (SELECT fio
                FROM rcod038deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 40 Then
    FOR r IN (SELECT fio
                FROM rcod040deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 42 Then
    FOR r IN (SELECT fio
                FROM rcod042deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 44 Then
    FOR r IN (SELECT fio
                FROM rcod044deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 49 Then
    FOR r IN (SELECT fio
                FROM rcod049deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 52 Then
    FOR r IN (SELECT fio
                FROM rcod052deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 54 Then
    FOR r IN (SELECT fio
                FROM rcod054deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 55 Then
    FOR r IN (SELECT fio
                FROM rcod055deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 67 Then
    FOR r IN (SELECT fio
                FROM rcod067deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 70 Then
    FOR r IN (SELECT fio
                FROM rcod070deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  elsif par_id_mega = 77 Then
    FOR r IN (SELECT fio
                FROM rcod077deposit.clerk_fio
               WHERE id_mega = par_id_mega and
                     branchno = par_branchno and
                     office = par_office and
                     clerk = par_clerk
              ORDER BY rowid) LOOP
      var_result := r.fio;
      EXIT;
    END LOOP;
  end if;
  
  return var_result;
END get_clerk_fio;
--
-- �������� �� ���������� ������ �������� �������� � ������������ � ��������� PL/SQL ����������
-- ������� ������������ ��� ��������� �������� ����������� �������� ��������/��������� �� ����� ���������
-- � ������������ � ����������� (LOAD_STEP_LOG_ITEMS)
-- 
FUNCTION get_number_from_text
(
  par_text        VARCHAR2, -- �����, �� �������� ����� �������� �������� ��������
  par_expr        VARCHAR2  -- PL/SQl ���������, ���������� �� ������ �����, �� ����� ��������� � ������� ������� #TEXT#
)
return NUMBER is -- ���������� �����, � ������ ������������� ������ - null
  var_result     NUMBER;
  var_sql        CLOB;
BEGIN
  var_sql := 'begin :res := '||replace(par_expr, '#TEXT#', ''''||replace(par_text, '''', '''''')||'''')||'; end;';
  EXECUTE IMMEDIATE var_sql USING OUT var_result;
  
  return var_result;
EXCEPTION  
  WHEN OTHERS THEN
    return null;
END get_number_from_text;  
end aml_tools;
/


grant execute on std.aml_tools to data_loader;
grant execute on std.aml_tools to business with grant option;
--grant execute on std.aml_tools to amladm with grant option;
grant execute on std.aml_tools to mantas with grant option;
grant execute on std.aml_tools to rcod038deposit with grant option;
grant execute on std.aml_tools to rcod038integrator with grant option;
grant execute on std.aml_tools to rcod038nsi with grant option;
grant execute on std.aml_tools to rcod038client with grant option;
grant execute on std.aml_tools to reks000ibs with grant option;
grant execute on std.aml_tools to rgat000gate with grant option;
grant execute on std.aml_tools to rifb000main with grant option;
grant execute on std.aml_tools to rmdc000mdm with grant option;
grant execute on std.aml_tools to rmdr000mdm with grant option;
grant execute on std.aml_tools to rnar000od with grant option;
grant execute on std.aml_tools to rnav000od with grant option;
grant execute on std.aml_tools to rstl000stoplist with grant option;
grant execute on std.aml_tools to rway000way4 with grant option;

