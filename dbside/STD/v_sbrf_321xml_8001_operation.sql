  CREATE OR REPLACE FORCE VIEW STD.V_SBRF_321XML_8001_OPERATION
  (  FILE_ID, FILE_NM, NUMB_P, DATE_P, REGN, ND_KO, KTU_S, BIK_S, NUMBF_S, BRANCH, KTU_SS, BIK_SS, NUMBF_SS, TEL, DATA, DATE_S, TERROR, VO, DOP_V
     , SUME, SUM, CURREN, PRIM_1, PRIM_2, NUM_PAY_D, DATE_PAY_D, METAL, PRIZ6001, B_PAYER, B_RECIP, PART, DESCR_1, DESCR_2, CURREN_CON, SUM_CON
     , PRIZ_SD, REFER_R2, VERSION, TU0, PRU0, NAMEU0, KODCR0, KODCN0, KD0, SD0, RG0, ND0, VD01, VD02, VD03, VD04, VD05, VD06, VD07, MC_01
     , MC_02, MC_03, GR0, BP_0, VP_0, ACC_B0, ACC_COR_B0, AMR_S0, AMR_R0, AMR_G0, AMR_U0, AMR_D0, AMR_K0, AMR_O0, ADRESS_S0, ADRESS_R0
     , ADRESS_G0, ADRESS_U0, ADRESS_D0, ADRESS_K0, ADRESS_O0, NAME_B0, KODCN_B0, BIK_B0, CARD_B0, NAME_IS_B0, BIK_IS_B0, NAME_R0, KODCN_R0, BIK_R0
     , TU1, PRU1, NAMEU1, KODCR1, KODCN1, KD1, SD1, RG1, ND1, VD11, VD12, VD13, VD14, VD15, VD16, VD17, MC_11, MC_12, MC_13, GR1, BP_1, AMR_S1
     , AMR_R1, AMR_G1, AMR_U1, AMR_D1, AMR_K1, AMR_O1, ADRESS_S1, ADRESS_R1, ADRESS_G1, ADRESS_U1, ADRESS_D1, ADRESS_K1, ADRESS_O1 
     , TU2, PRU2, NAMEU2, KODCR2, KODCN2, KD2, SD2, RG2, ND2, VD21, VD22, VD23, VD24, VD25, VD26, VD27, MC_21, MC_22, MC_23, GR2, BP_2, AMR_S2
     , AMR_R2, AMR_G2, AMR_U2, AMR_D2, AMR_K2, AMR_O2, ADRESS_S2, ADRESS_R2, ADRESS_G2, ADRESS_U2, ADRESS_D2, ADRESS_K2, ADRESS_O2
     , TU3, PRU3, NAMEU3, KODCR3, KODCN3, KD3, SD3, RG3, ND3, VD31, VD32, VD33, VD34, VD35, VD36, VD37, MC_31, MC_32, MC_33, GR3
     , BP_3, VP_3, ACC_B3, ACC_COR_B3, AMR_S3, AMR_R3, AMR_G3, AMR_U3, AMR_D3, AMR_K3, AMR_O3, ADRESS_S3, ADRESS_R3, ADRESS_G3, ADRESS_U3
     , ADRESS_D3, ADRESS_K3, ADRESS_O3, NAME_B3, KODCN_B3, BIK_B3, CARD_B3, NAME_IS_B3, BIK_IS_B3, NAME_R3, KODCN_R3, BIK_R3
     , TU4, PRU4, NAMEU4, KODCR4, KODCN4, KD4, SD4, RG4, ND4, VD41, VD42, VD43, VD44, VD45, VD46, VD47 , MC_41, MC_42, MC_43, GR4, BP_4   
     , AMR_S4, AMR_R4, AMR_G4, AMR_U4, AMR_D4, AMR_K4, AMR_O4, ADRESS_S4, ADRESS_R4, ADRESS_G4, ADRESS_U4, ADRESS_D4, ADRESS_K4        
     , ADRESS_O4, RESERV612, NAME_S, RESRV22, RESRV12, RESRV02, RESRV_B32, RESRV_B02, RESRV42, RESRV32
  ) 
  as 
  select x.file_id
       , x.file_nm
       ---�������� �� �������� 
       , w.numb_p
       , w.date_p
       , w.regn
       , w.nd_ko
       , w.ktu_s
       , w.bik_s
       , w.numbf_s
       , w.branch
       , w.ktu_ss
       , w.bik_ss
       , w.numbf_ss
       , w.tel
       , w.data
       , w.date_s
       , w.terror
       , w.vo
       , w.dop_v
       , w.sm
       , w.smv
       , w.codval
       , w.prim_1
       , w.prim_2  
       , w.num_pay_d
       , w.date_pay_d
       , w.metal  
       , w.priz6001
       , w.b_payer
       , w.b_recip
       , w.part
       , w.descr_1
       , w.descr_2
       , w.curren_con
       , w.sum_con
       , w.priz_sd
       , w.refer_r2
       , w.version
       -- 0 �������� � ����, ����������� �������� � ��������� ���������� ��� ���� ����������
       , w.tu0
       , w.pru0
       , w.nameu0
       , w.kodcr0
       , w.kodcn0
       , w.kd0
       , w.sd0
       , w.rg0
       , w.nd0
       , w.vd01
       , w.vd02
       , w.vd03
       , w.vd04
       , w.vd05
       , w.vd06
       , w.vd07
       , w.mc_01
       , w.mc_02
       , w.mc_03
       , w.gr0
       , w.bp_0
       , w.vp_0
       , w.acc_b0
       , w.acc_cor_b0
       , w.amr_s0
       , w.amr_r0
       , w.amr_g0
       , w.amr_u0
       , w.amr_d0
       , w.amr_k0
       , w.amr_o0
       , w.adress_s0
       , w.adress_r0
       , w.adress_g0
       , w.adress_u0
       , w.adress_d0
       , w.adress_k0
       , w.adress_o0
       , w.name_b0
       , w.kodcn_b0
       , w.bik_b0
       , w.card_b0
       , w.name_is_b0
       , w.bik_is_b0
       , w.name_r0
       , w.kodcn_r0
       , w.bik_r0
   
       -- 1 �������� � ������������� ����, ������������ �������� � ��������� ����������
       , w.tu1
       , w.pru1
       , w.nameu1
       , w.kodcr1
       , w.kodcn1
       , w.kd1
       , w.sd1
       , w.rg1
       , w.nd1
       , w.vd11
       , w.vd12
       , w.vd13
       , w.vd14
       , w.vd15
       , w.vd16
       , w.vd17
       , w.mc_11
       , w.mc_12
       , w.mc_13
       , w.gr1
       , w.bp_1
       , w.amr_s1
       , w.amr_r1
       , w.amr_g1
       , w.amr_u1
       , w.amr_d1
       , w.amr_k1
       , w.amr_o1
       , w.adress_s1
       , w.adress_r1
       , w.adress_g1
       , w.adress_u1
       , w.adress_d1
       , w.adress_k1
       , w.adress_o1 
       -- 2 �������� � ������������� ���������� �� �������� � ��������� ���������� ��� ���� ����������,
       , w.tu2
       , w.pru2
       , w.nameu2
       , w.kodcr2
       , w.kodcn2
       , w.kd2
       , w.sd2
       , w.rg2
       , w.nd2
       , w.vd21
       , w.vd22
       , w.vd23
       , w.vd24
       , w.vd25
       , w.vd26
       , w.vd27
       , w.mc_21
       , w.mc_22
       , w.mc_23
       , w.gr2
       , w.bp_2
       , w.amr_s2
       , w.amr_r2
       , w.amr_g2
       , w.amr_u2
       , w.amr_d2
       , w.amr_k2
       , w.amr_o2
       , w.adress_s2
       , w.adress_r2
       , w.adress_g2
       , w.adress_u2
       , w.adress_d2
       , w.adress_k2    
       , w.adress_o2
       
        -- 3 �������� � ���������� �� �������� � ��������� ���������� ��� ���� ����������
       , w.tu3
       , w.pru3
       , w.nameu3
       , w.kodcr3
       , w.kodcn3 
       , w.kd3
       , w.sd3
       , w.rg3
       , w.nd3
       , w.vd31
       , w.vd32
       , w.vd33
       , w.vd34
       , w.vd35
       , w.vd36
       , w.vd37
       , w.mc_31
       , w.mc_32
       , w.mc_33
       , w.gr3
       , w.bp_3
       , w.vp_3
       , w.acc_b3
       , w.acc_cor_b3
       , w.amr_s3
       , w.amr_r3
       , w.amr_g3
       , w.amr_u3
       , w.amr_d3
       , w.amr_k3
       , w.amr_o3
       , w.adress_s3
       , w.adress_r3
       , w.adress_g3
       , w.adress_u3
       , w.adress_d3
       , w.adress_k3
       , w.adress_o3
       , w.name_b3
       , w.kodcn_b3
       , w.bik_b3
       , w.card_b3
       , w.name_is_b3
       , w.bik_is_b3
       , w.name_r3
       , w.kodcn_r3   
       , w.bik_r3

       -- 4 �������� � ����, �� ���������� �����, ����������� ��������, ��� ����������� �� �������� � ��������� ���������� ��� ���� ����������, �� ��������� � �� ����� �������� ����������� ��������
       , w.tu4
       , w.pru4
       , w.nameu4
       , w.kodcr4
       , w.kodcn4
       , w.kd4
       , w.sd4
       , w.rg4
       , w.nd4
       , w.vd41
       , w.vd42
       , w.vd43
       , w.vd44
       , w.vd45
       , w.vd46
       , w.vd47 
       , w.mc_41
       , w.mc_42
       , w.mc_43
       , w.gr4
       , w.bp_4   
       , w.amr_s4
       , w.amr_r4            
       , w.amr_g4
       , w.amr_u4
       , w.amr_d4
       , w.amr_k4
       , w.amr_o4
       , w.adress_s4 
       , w.adress_r4
       , w.adress_g4
       , w.adress_u4       
       , w.adress_d4
       , w.adress_k4        
       , w.adress_o4
       , w.reserv612
       -- �� ������� � �������� rf_oes_321 � rf_oes_321_party
       , w.name_s    
       , w.resrv22
       , w.resrv12     
       , w.resrv02
       , w.resrv_b32
       , w.resrv_b02
       , w.resrv42
       , w.resrv32
       
  from std.imp_sbrf_321xml_8001 x
  , xmltable('/document/fieldlist' 
             passing x.file_body
             columns 
             dpd_cd varchar2(2000 char) path 'field[@name="DEPT_NAME"]'
             , name_b3 varchar2(2000 char) path 'field[@name="NAME_B3"]'
             , name_b0 varchar2(2000 char) path 'field[@name="NAME_B0"]'
             , bik_ss varchar2(2000 char) path 'field[@name="BIK_SS"]'
             , nameu4 varchar2(2000 char) path 'field[@name="NAMEU4"]'
             , nameu3 varchar2(2000 char) path 'field[@name="NAMEU3"]'
             , nameu2 varchar2(2000 char) path 'field[@name="NAMEU2"]'
             , nameu1 varchar2(2000 char) path 'field[@name="NAMEU1"]'
             , nameu0 varchar2(2000 char) path 'field[@name="NAMEU0"]'
             , acc_b3 varchar2(2000 char) path 'field[@name="ACC_B3"]'
             , acc_b0 varchar2(2000 char) path 'field[@name="ACC_B0"]'
             , amr_s4 varchar2(2000 char) path 'field[@name="AMR_S4"]'
             , amr_s3 varchar2(2000 char) path 'field[@name="AMR_S3"]'
             , amr_s2 varchar2(2000 char) path 'field[@name="AMR_S2"]'
             , amr_s1 varchar2(2000 char) path 'field[@name="AMR_S1"]'
             , amr_s0 varchar2(2000 char) path 'field[@name="AMR_S0"]'
             , amr_r4 varchar2(2000 char) path 'field[@name="AMR_R4"]'
             , amr_r3 varchar2(2000 char) path 'field[@name="AMR_R3"]'
             , amr_r2 varchar2(2000 char) path 'field[@name="AMR_R2"]'
             , amr_r1 varchar2(2000 char) path 'field[@name="AMR_R1"]'
             , amr_r0 varchar2(2000 char) path 'field[@name="AMR_R0"]'
             , num_pay_d varchar2(2000 char) path 'field[@name="NUM_PAY_D"]'
             , bik_r3 varchar2(2000 char) path 'field[@name="BIK_R3"]'
             , bik_r0 varchar2(2000 char) path 'field[@name="BIK_R0"]'
             , bik_s varchar2(2000 char) path 'field[@name="BIK_S"]'
             , mc_43 varchar2(2000 char) path 'field[@name="MC_43"]'
             , mc_42 varchar2(2000 char) path 'field[@name="MC_42"]'
             , mc_41 varchar2(2000 char) path 'field[@name="MC_41"]'
             , adress_g4 varchar2(2000 char) path 'field[@name="ADRESS_G4"]'
             , adress_g3 varchar2(2000 char) path 'field[@name="ADRESS_G3"]'
             , adress_g2 varchar2(2000 char) path 'field[@name="ADRESS_G2"]'
             , adress_g1 varchar2(2000 char) path 'field[@name="ADRESS_G1"]'
             , adress_g0 varchar2(2000 char) path 'field[@name="ADRESS_G0"]'
             , resrv_b32 varchar2(2000 char) path 'field[@name="RESRV_B32"]'
             , mc_33 varchar2(2000 char) path 'field[@name="MC_33"]'
             , mc_32 varchar2(2000 char) path 'field[@name="MC_32"]'
             , mc_31 varchar2(2000 char) path 'field[@name="MC_31"]'
             , sd4 varchar2(2000 char) path 'field[@name="SD4"]'
             , sd3 varchar2(2000 char) path 'field[@name="SD3"]'
             , sd2 varchar2(2000 char) path 'field[@name="SD2"]'
             , sd1 varchar2(2000 char) path 'field[@name="SD1"]'
             , sd0 varchar2(2000 char) path 'field[@name="SD0"]'
             , amr_o4 varchar2(2000 char) path 'field[@name="AMR_O4"]'
             , amr_o3 varchar2(2000 char) path 'field[@name="AMR_O3"]'
             , amr_o2 varchar2(2000 char) path 'field[@name="AMR_O2"]'
             , amr_o1 varchar2(2000 char) path 'field[@name="AMR_O1"]'
             , amr_o0 varchar2(2000 char) path 'field[@name="AMR_O0"]'
             , bik_is_b3 varchar2(2000 char) path 'field[@name="BIK_IS_B3"]'
             , bik_is_b0 varchar2(2000 char) path 'field[@name="BIK_IS_B0"]'
             , kd4 varchar2(2000 char) path 'field[@name="KD4"]'
             , kd3 varchar2(2000 char) path 'field[@name="KD3"]'
             , kd2 varchar2(2000 char) path 'field[@name="KD2"]'
             , kd1 varchar2(2000 char) path 'field[@name="KD1"]'
             , kd0 varchar2(2000 char) path 'field[@name="KD0"]'
             , mc_23 varchar2(2000 char) path 'field[@name="MC_23"]'
             , mc_22 varchar2(2000 char) path 'field[@name="MC_22"]'
             , mc_21 varchar2(2000 char) path 'field[@name="MC_21"]'
             , kodcn_b3 varchar2(2000 char) path 'field[@name="KODCN_B3"]'
             , kodcn_b0 varchar2(2000 char) path 'field[@name="KODCN_B0"]'
             , tu4 varchar2(2000 char) path 'field[@name="TU4"]'
             , tu3 varchar2(2000 char) path 'field[@name="TU3"]'
             , tu2 varchar2(2000 char) path 'field[@name="TU2"]'
             , tu1 varchar2(2000 char) path 'field[@name="TU1"]'
             , tu0 varchar2(2000 char) path 'field[@name="TU0"]'
             , mc_13 varchar2(2000 char) path 'field[@name="MC_13"]'
             , mc_12 varchar2(2000 char) path 'field[@name="MC_12"]'
             , mc_11 varchar2(2000 char) path 'field[@name="MC_11"]'
             , nd_ko varchar2(2000 char) path 'field[@name="ND_KO"]'
             , adress_d4 varchar2(2000 char) path 'field[@name="ADRESS_D4"]'
             , adress_d3 varchar2(2000 char) path 'field[@name="ADRESS_D3"]'
             , adress_d2 varchar2(2000 char) path 'field[@name="ADRESS_D2"]'
             , adress_d1 varchar2(2000 char) path 'field[@name="ADRESS_D1"]'
             , adress_d0 varchar2(2000 char) path 'field[@name="ADRESS_D0"]'
             , resrv_b02 varchar2(2000 char) path 'field[@name="RESRV_B02"]'
             , numbf_ss varchar2(2000 char) path 'field[@name="NUMBF_SS"]'
             , mc_03 varchar2(2000 char) path 'field[@name="MC_03"]'
             , mc_02 varchar2(2000 char) path 'field[@name="MC_02"]'
             , mc_01 varchar2(2000 char) path 'field[@name="MC_01"]'
             , vd47 varchar2(2000 char) path 'field[@name="VD47"]'
             , vd46 varchar2(2000 char) path 'field[@name="VD46"]'
             , vd45 varchar2(2000 char) path 'field[@name="VD45"]'
             , vd44 varchar2(2000 char) path 'field[@name="VD44"]'
             , vd43 varchar2(2000 char) path 'field[@name="VD43"]'
             , vd42 varchar2(2000 char) path 'field[@name="VD42"]'
             , vd41 varchar2(2000 char) path 'field[@name="VD41"]'
             , rg4 varchar2(2000 char) path 'field[@name="RG4"]'
             , rg3 varchar2(2000 char) path 'field[@name="RG3"]'
             , rg2 varchar2(2000 char) path 'field[@name="RG2"]'
             , rg1 varchar2(2000 char) path 'field[@name="RG1"]'
             , rg0 varchar2(2000 char) path 'field[@name="RG0"]'
             , priz6001 varchar2(2000 char) path 'field[@name="PRIZ6001"]'
             , vd37 varchar2(2000 char) path 'field[@name="VD37"]'
             , vd36 varchar2(2000 char) path 'field[@name="VD36"]'
             , vd35 varchar2(2000 char) path 'field[@name="VD35"]'
             , vd34 varchar2(2000 char) path 'field[@name="VD34"]'
             , vd33 varchar2(2000 char) path 'field[@name="VD33"]'
             , vd32 varchar2(2000 char) path 'field[@name="VD32"]'
             , vd31 varchar2(2000 char) path 'field[@name="VD31"]'
             , bp_4 varchar2(2000 char) path 'field[@name="BP_4"]'
             , bp_3 varchar2(2000 char) path 'field[@name="BP_3"]'
             , bp_2 varchar2(2000 char) path 'field[@name="BP_2"]'
             , bp_1 varchar2(2000 char) path 'field[@name="BP_1"]'
             , bp_0 varchar2(2000 char) path 'field[@name="BP_0"]'
             , codval varchar2(2000 char) path 'field[@name="CODVAL"]'
             , amr_k4 varchar2(2000 char) path 'field[@name="AMR_K4"]'
             , amr_k3 varchar2(2000 char) path 'field[@name="AMR_K3"]'
             , amr_k2 varchar2(2000 char) path 'field[@name="AMR_K2"]'
             , amr_k1 varchar2(2000 char) path 'field[@name="AMR_K1"]'
             , amr_k0 varchar2(2000 char) path 'field[@name="AMR_K0"]'
             , name_r3 varchar2(2000 char) path 'field[@name="NAME_R3"]'
             , name_r0 varchar2(2000 char) path 'field[@name="NAME_R0"]'
             , vd27 varchar2(2000 char) path 'field[@name="VD27"]'
             , vd26 varchar2(2000 char) path 'field[@name="VD26"]'
             , vd25 varchar2(2000 char) path 'field[@name="VD25"]'
             , vd24 varchar2(2000 char) path 'field[@name="VD24"]'
             , vd23 varchar2(2000 char) path 'field[@name="VD23"]'
             , vd22 varchar2(2000 char) path 'field[@name="VD22"]'
             , vd21 varchar2(2000 char) path 'field[@name="VD21"]'
             , sum_con varchar2(2000 char) path 'field[@name="SUM_CON"]'
             , vo varchar2(2000 char) path 'field[@name="VO"]'
             , vd17 varchar2(2000 char) path 'field[@name="VD17"]'
             , vd16 varchar2(2000 char) path 'field[@name="VD16"]'
             , vd15 varchar2(2000 char) path 'field[@name="VD15"]'
             , vd14 varchar2(2000 char) path 'field[@name="VD14"]'
             , vd13 varchar2(2000 char) path 'field[@name="VD13"]'
             , vd12 varchar2(2000 char) path 'field[@name="VD12"]'
             , vd11 varchar2(2000 char) path 'field[@name="VD11"]'
             , card_b3 varchar2(2000 char) path 'field[@name="CARD_B3"]'
             , card_b0 varchar2(2000 char) path 'field[@name="CARD_B0"]'
             , nd4 varchar2(2000 char) path 'field[@name="ND4"]'
             , nd3 varchar2(2000 char) path 'field[@name="ND3"]'
             , nd2 varchar2(2000 char) path 'field[@name="ND2"]'
             , nd1 varchar2(2000 char) path 'field[@name="ND1"]'
             , nd0 varchar2(2000 char) path 'field[@name="ND0"]'
             , vd07 varchar2(2000 char) path 'field[@name="VD07"]'
             , vd06 varchar2(2000 char) path 'field[@name="VD06"]'
             , vd05 varchar2(2000 char) path 'field[@name="VD05"]'
             , vd04 varchar2(2000 char) path 'field[@name="VD04"]'
             , vd03 varchar2(2000 char) path 'field[@name="VD03"]'
             , vd02 varchar2(2000 char) path 'field[@name="VD02"]'
             , vd01 varchar2(2000 char) path 'field[@name="VD01"]'
             , pru4 varchar2(2000 char) path 'field[@name="PRU4"]'
             , pru3 varchar2(2000 char) path 'field[@name="PRU3"]'
             , pru2 varchar2(2000 char) path 'field[@name="PRU2"]'
             , pru1 varchar2(2000 char) path 'field[@name="PRU1"]'
             , pru0 varchar2(2000 char) path 'field[@name="PRU0"]'
             , prim_2 varchar2(2000 char) path 'field[@name="PRIM_2"]'
             , prim_1 varchar2(2000 char) path 'field[@name="PRIM_1"]'
             , b_recip varchar2(2000 char) path 'field[@name="B_RECIP"]'
             , amr_g4 varchar2(2000 char) path 'field[@name="AMR_G4"]'
             , amr_g3 varchar2(2000 char) path 'field[@name="AMR_G3"]'
             , amr_g2 varchar2(2000 char) path 'field[@name="AMR_G2"]'
             , amr_g1 varchar2(2000 char) path 'field[@name="AMR_G1"]'
             , amr_g0 varchar2(2000 char) path 'field[@name="AMR_G0"]'
             , vp_3 varchar2(2000 char) path 'field[@name="VP_3"]'
             , vp_0 varchar2(2000 char) path 'field[@name="VP_0"]'
             , sm varchar2(2000 char) path 'field[@name="SM"]'
             , name_s varchar2(2000 char) path 'field[@name="NAME_S"]'
             , adress_u4 varchar2(2000 char) path 'field[@name="ADRESS_U4"]'
             , adress_u3 varchar2(2000 char) path 'field[@name="ADRESS_U3"]'
             , adress_u2 varchar2(2000 char) path 'field[@name="ADRESS_U2"]'
             , adress_u1 varchar2(2000 char) path 'field[@name="ADRESS_U1"]'
             , adress_u0 varchar2(2000 char) path 'field[@name="ADRESS_U0"]'
             , terror varchar2(2000 char) path 'field[@name="TERROR"]'
             , kodcn_r3 varchar2(2000 char) path 'field[@name="KODCN_R3"]'
             , kodcn_r0 varchar2(2000 char) path 'field[@name="KODCN_R0"]'
             , b_payer varchar2(2000 char) path 'field[@name="B_PAYER"]'
             , resrv42 varchar2(2000 char) path 'field[@name="RESRV42"]'
             , regn varchar2(2000 char) path 'field[@name="REGN"]'
             , amr_d4 varchar2(2000 char) path 'field[@name="AMR_D4"]'
             , amr_d3 varchar2(2000 char) path 'field[@name="AMR_D3"]'
             , amr_d2 varchar2(2000 char) path 'field[@name="AMR_D2"]'
             , amr_d1 varchar2(2000 char) path 'field[@name="AMR_D1"]'
             , amr_d0 varchar2(2000 char) path 'field[@name="AMR_D0"]'
             , resrv32 varchar2(2000 char) path 'field[@name="RESRV32"]'
             , adress_s4 varchar2(2000 char) path 'field[@name="ADRESS_S4"]'
             , adress_s3 varchar2(2000 char) path 'field[@name="ADRESS_S3"]'
             , adress_s2 varchar2(2000 char) path 'field[@name="ADRESS_S2"]'
             , adress_s1 varchar2(2000 char) path 'field[@name="ADRESS_S1"]'
             , adress_s0 varchar2(2000 char) path 'field[@name="ADRESS_S0"]'
             , gr4 varchar2(2000 char) path 'field[@name="GR4"]'
             , gr3 varchar2(2000 char) path 'field[@name="GR3"]'
             , gr2 varchar2(2000 char) path 'field[@name="GR2"]'
             , gr1 varchar2(2000 char) path 'field[@name="GR1"]'
             , gr0 varchar2(2000 char) path 'field[@name="GR0"]'
             , descr_2 varchar2(2000 char) path 'field[@name="DESCR_2"]'
             , descr_1 varchar2(2000 char) path 'field[@name="DESCR_1"]'
             , kodcr4 varchar2(2000 char) path 'field[@name="KODCR4"]'
             , kodcr3 varchar2(2000 char) path 'field[@name="KODCR3"]'
             , kodcr2 varchar2(2000 char) path 'field[@name="KODCR2"]'
             , kodcr1 varchar2(2000 char) path 'field[@name="KODCR1"]'
             , kodcr0 varchar2(2000 char) path 'field[@name="KODCR0"]'
             , dop_v varchar2(2000 char) path 'field[@name="DOP_V"]'
             , metal varchar2(2000 char) path 'field[@name="METAL"]'
             , resrv22 varchar2(2000 char) path 'field[@name="RESRV22"]'
             , adress_r4 varchar2(2000 char) path 'field[@name="ADRESS_R4"]'
             , adress_r3 varchar2(2000 char) path 'field[@name="ADRESS_R3"]'
             , adress_r2 varchar2(2000 char) path 'field[@name="ADRESS_R2"]'
             , adress_r1 varchar2(2000 char) path 'field[@name="ADRESS_R1"]'
             , adress_r0 varchar2(2000 char) path 'field[@name="ADRESS_R0"]'
             , date_pay_d varchar2(2000 char) path 'field[@name="DATE_PAY_D"]'
             , ktu_s varchar2(2000 char) path 'field[@name="KTU_S"]'
             , resrv12 varchar2(2000 char) path 'field[@name="RESRV12"]'
             , bik_b3 varchar2(2000 char) path 'field[@name="BIK_B3"]'
             , bik_b0 varchar2(2000 char) path 'field[@name="BIK_B0"]'
             , branch varchar2(2000 char) path 'field[@name="BRANCH"]'
             , resrv02 varchar2(2000 char) path 'field[@name="RESRV02"]'
             , smv varchar2(2000 char) path 'field[@name="SMV"]'
             , numbf_s varchar2(2000 char) path 'field[@name="NUMBF_S"]'
             , curren_con varchar2(2000 char) path 'field[@name="CURREN_CON"]'
             , date_s varchar2(2000 char) path 'field[@name="DATE_S"]'
             , date_p varchar2(2000 char) path 'field[@name="DATE_P"]'
             , adress_o4 varchar2(2000 char) path 'field[@name="ADRESS_O4"]'
             , adress_o3 varchar2(2000 char) path 'field[@name="ADRESS_O3"]'
             , adress_o2 varchar2(2000 char) path 'field[@name="ADRESS_O2"]'
             , adress_o1 varchar2(2000 char) path 'field[@name="ADRESS_O1"]'
             , adress_o0 varchar2(2000 char) path 'field[@name="ADRESS_O0"]'
             , acc_cor_b3 varchar2(2000 char) path 'field[@name="ACC_COR_B3"]'
             , acc_cor_b0 varchar2(2000 char) path 'field[@name="ACC_COR_B0"]'
             , kodcn4 varchar2(2000 char) path 'field[@name="KODCN4"]'
             , kodcn3 varchar2(2000 char) path 'field[@name="KODCN3"]'
             , kodcn2 varchar2(2000 char) path 'field[@name="KODCN2"]'
             , kodcn1 varchar2(2000 char) path 'field[@name="KODCN1"]'
             , kodcn0 varchar2(2000 char) path 'field[@name="KODCN0"]'
             , part varchar2(2000 char) path 'field[@name="PART"]'
             , tel varchar2(2000 char) path 'field[@name="TEL"]'
             , ktu_ss varchar2(2000 char) path 'field[@name="KTU_SS"]'
             , name_is_b3 varchar2(2000 char) path 'field[@name="NAME_IS_B3"]'
             , name_is_b0 varchar2(2000 char) path 'field[@name="NAME_IS_B0"]'
             , data varchar2(2000 char) path 'field[@name="DATA"]'
             , refer_r2 varchar2(2000 char) path 'field[@name="REFER_R2"]'
             , amr_u4 varchar2(2000 char) path 'field[@name="AMR_U4"]'
             , amr_u3 varchar2(2000 char) path 'field[@name="AMR_U3"]'
             , amr_u2 varchar2(2000 char) path 'field[@name="AMR_U2"]'
             , amr_u1 varchar2(2000 char) path 'field[@name="AMR_U1"]'
             , amr_u0 varchar2(2000 char) path 'field[@name="AMR_U0"]'
             , numb_p varchar2(2000 char) path 'field[@name="NUMB_P"]'
             , reserv612 varchar2(2000 char) path 'field[@name="RESERV612"]'
             , adress_k4 varchar2(2000 char) path 'field[@name="ADRESS_K4"]'
             , adress_k3 varchar2(2000 char) path 'field[@name="ADRESS_K3"]'
             , adress_k2 varchar2(2000 char) path 'field[@name="ADRESS_K2"]'
             , adress_k1 varchar2(2000 char) path 'field[@name="ADRESS_K1"]'
             , adress_k0 varchar2(2000 char) path 'field[@name="ADRESS_K0"]'
             , version varchar2(2000 char) path 'field[@name="VERSION"]'
             , priz_sd varchar2(2000 char) path 'field[@name="PRIZ_SD"]'
            )(+) w;
            
            GRANT SELECT on std.v_sbrf_321xml_8001_operation to MANTAS;
            
            
            
