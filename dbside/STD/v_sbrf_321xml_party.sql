create or replace force view std.v_sbrf_321xml_party as

select x.file_id
       , x.file_nm
       , x.operation_id
       , mi.*
  from std.v_sbrf_321xml_operation x
  , xmltable(xmlnamespaces('fns.esb.sbrf.ru' as "f"), 'for $m in /*/f:MEMBER_INFO return $m'
             passing x.info_part
             columns
               party_id  for ordinality
             , block     varchar2(2000 char) path 'f:BLOCK'
             , tureal    varchar2(2000 char) path 'f:MEMBER/f:TUREAL'
             , pru       varchar2(2000 char) path 'f:MEMBER/f:PRU'
             , nameu     varchar2(2000 char) path 'f:MEMBER/f:NAMEU'
             , kodcr     varchar2(2000 char) path 'f:MEMBER/f:KODCR'
             , kodcn     varchar2(2000 char) path 'f:MEMBER/f:KODCN'
             , kd        varchar2(2000 char) path 'f:MEMBER/f:KD'
             , sd        varchar2(2000 char) path 'f:MEMBER/f:SD'
             , rg        varchar2(2000 char) path 'f:MEMBER/f:RG'
             , nd        varchar2(2000 char) path 'f:MEMBER/f:ND'
             
             , vd_1      varchar2(2000 char) path 'f:MEMBER/f:VD/f:VD_1'
             , vd_2      varchar2(2000 char) path 'f:MEMBER/f:VD/f:VD_2'
             , vd_3      varchar2(2000 char) path 'f:MEMBER/f:VD/f:VD_3'
             , vd_4      varchar2(2000 char) path 'f:MEMBER/f:VD/f:VD_4'
             , vd_5      varchar2(2000 char) path 'f:MEMBER/f:VD/f:VD_5'
             , vd_6      varchar2(2000 char) path 'f:MEMBER/f:VD/f:VD_6'
             , vd_7      varchar2(2000 char) path 'f:MEMBER/f:VD/f:VD_7'
             , mc_1      varchar2(2000 char) path 'f:MEMBER/f:VD/f:MC_1'
             , mc_2      varchar2(2000 char) path 'f:MEMBER/f:VD/f:MC_2'
             , mc_3      varchar2(2000 char) path 'f:MEMBER/f:VD/f:MC_3'
             
             , gr        varchar2(2000 char) path 'f:MEMBER/f:GR'
             , bp        varchar2(2000 char) path 'f:MEMBER/f:BP'
             , vp        varchar2(2000 char) path 'f:MEMBER/f:VP'

             , amr_s     varchar2(2000 char) path 'f:MEMBER/f:AMR/f:ADRESS_S'
             , amr_r     varchar2(2000 char) path 'f:MEMBER/f:AMR/f:ADRESS_R'
             , amr_g     varchar2(2000 char) path 'f:MEMBER/f:AMR/f:ADRESS_G'
             , amr_u     varchar2(2000 char) path 'f:MEMBER/f:AMR/f:ADRESS_U'
             , amr_d     varchar2(2000 char) path 'f:MEMBER/f:AMR/f:ADRESS_D'
             , amr_k     varchar2(2000 char) path 'f:MEMBER/f:AMR/f:ADRESS_K'
             , amr_o     varchar2(2000 char) path 'f:MEMBER/f:AMR/f:ADRESS_O'

             , address_s varchar2(2000 char) path 'f:MEMBER/f:ADRESS/f:ADRESS_S'
             , address_r varchar2(2000 char) path 'f:MEMBER/f:ADRESS/f:ADRESS_R'
             , address_g varchar2(2000 char) path 'f:MEMBER/f:ADRESS/f:ADRESS_G'
             , address_u varchar2(2000 char) path 'f:MEMBER/f:ADRESS/f:ADRESS_U'
             , address_d varchar2(2000 char) path 'f:MEMBER/f:ADRESS/f:ADRESS_D'
             , address_k varchar2(2000 char) path 'f:MEMBER/f:ADRESS/f:ADRESS_K'
             , address_o varchar2(2000 char) path 'f:MEMBER/f:ADRESS/f:ADRESS_O'

             , acc_b     varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:ACC_B'
             , acc_cor_b varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:ACC_COR_B'
             , name_is_b varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:NAME_IS_B'
             , bik_is_b  varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:BIK_IS_B'
             , card_b    varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:CARD_B'
             , name_b    varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:NAME_B'
             , kodcn_b   varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:KODCN_B'
             , bik_b     varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:BIK_B'
             , name_r    varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:NAME_R'
             , kodcn_r   varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:KODCN_R'
             , bik_r     varchar2(2000 char) path '/f:MEMBER_INFO/f:CO/f:BIK_R'
            ) mi;

show errors;

grant select on std.v_sbrf_321xml_party to mantas;