create or replace context log using std.pkg_log accessed globally;


create or replace package std.pkg_log as

-- ������� ��� ������������ ����������� �����������
type level_type is table of number index by varchar2(7);
  logging_level level_type;
  i             varchar2(7);

-- ������-������� ��� ������������ ����������� �����������
type reverse_level_type is table of varchar2(7) index by pls_integer;
  level_name reverse_level_type;
  i             pls_integer;

-- ������������� ���������� � �������� ���������� ��������� log
procedure set_cont(p_name varchar2, p_value varchar2);
-- ������������� � null ���������� ��������� log - log_id � log_level
procedure initialize_context;
--
-- ������� (�������) ����� ��������
--
function open_log
(
  par_file_flag    number                      default 0,      -- ���� - ������� �� ���������� ��������� ����� � ����
  par_file_name    std.logs.file_name%type     default null,   -- ��� �����, ���� ������� ���������� ���������, ���� null � ���������� ����������� ������� � ����/����� �������� + id ����
  par_log_level    std.logs.log_level%type     default 'WARN', -- ������� ��������� ���������
  par_expired_time std.logs.expired_time%type  default null,    -- ����, �� �������� ����������� ��������, ���� null, �� ���������
  p_wfr_id         number -- workflow_run_id �� �����������     
)
return logs.log_id%type; -- id ������ ���������
--
-- ��������� ���������� ��������� log log_id � log_level ���������� �� ��������� ���������
-- ���� ��������� ��������� ��� - ������������ null
--
procedure set_log; -- id ���������
--
-- ������� id �������� (���������) ���������
--
function get_log return logs.log_id%type;
--
-- ������� ���������� ����� ������ ����������� �� ������� logging_level ������ std.pkg_log
--
function get_level_number(p_level varchar2) return number;
--
-- ������� ���������� ��� ������ ����������� �� ������� level_name ������ std.pkg_log
--
function get_level(p_level number) return varchar2;
--
-- ������� ������������ �������� ��������
--
PROCEDURE close_log
(
  par_raise_flag       INTEGER DEFAULT 1 -- ���� 1/0 - ������� �� ��������� exception, ���� � ���� ���� ������ (ERROR � FATAL)
);
--
-- �������� ��������� ���������� ������ � ��������
-- ����� ��� ��������� �������� ��� ��������� ��������� ������ ��������� � ���
--
procedure write_log
(
  par_log_level    log_items.log_level%type,      -- ������� ������������� ���������
  par_log_text     log_items.log_text%type,       -- ����� ������������� ���������
  par_log_orig     log_items.log_orig%type        -- �������� ������������� ���������
);
--
-- �������� ��������� ������ trace
--
procedure trace
(
  par_log_text     log_items.log_text%type,                  -- ����� ������������� ���������
  par_log_orig     log_items.log_orig%type  default null     -- �������� ������������� ���������
);
--
-- �������� ��������� ������ debug
--
procedure debug
(
  par_log_text     log_items.log_text%type,                  -- ����� ������������� ���������
  par_log_orig     log_items.log_orig%type  default null     -- �������� ������������� ���������
);
--
-- �������� ��������� ������ info
--
procedure info
(
  par_log_text     log_items.log_text%type,                  -- ����� ������������� ���������
  par_log_orig     log_items.log_orig%type  default null     -- �������� ������������� ���������
);

--
-- �������� ��������� ������ warn
--
procedure warn
(
  par_log_text     log_items.log_text%type,                  -- ����� ������������� ���������
  par_log_orig     log_items.log_orig%type  default null     -- �������� ������������� ���������
);

--
-- �������� ��������� ������ error
--
procedure error
(
  par_log_text     log_items.log_text%type,                  -- ����� ������������� ���������
  par_log_orig     log_items.log_orig%type  default null     -- �������� ������������� ���������
);

--
-- �������� ��������� ������ fatal
--
procedure fatal
(
  par_log_text     log_items.log_text%type,                  -- ����� ������������� ���������
  par_log_orig     log_items.log_orig%type  default null     -- �������� ������������� ���������
);
--
-- ��������� ����������� ������ ����� ���������� ��������� ������������ ������ � �������� �� � ���
--
FUNCTION log_parallel_task_errors
(
  par_task_owner         VARCHAR2, -- �������� ������ (sys.dba_parallel_execute_tasks.task_owner)
  par_task_name          VARCHAR2, -- ��� ������ (sys.dba_parallel_execute_tasks.task_name)
  par_log_level          log_items.log_level%TYPE,                   -- ������� ������������� ���������
  par_log_orig           log_items.log_orig%type                     -- �������� ������������� ���������
)
RETURN INTEGER; -- ���������� ��������� ������ (0, ���� ������ ���)
end pkg_log;
/
create or replace package body std.pkg_log is

procedure set_cont(p_name varchar2, p_value varchar2) is
begin
  dbms_session.set_context('log', p_name, p_value);
exception
  when others then
    raise_application_error(-20001,
                            'It wasn`t succeeded to set a context variable ' ||
                            p_name || ' value ' || p_value ||
                            '. Error: ' || chr(13) || SQLERRM);
end;

procedure initialize_context as
begin
  -- ����������, �������� logs.log_id - ������������ ���������
  set_cont('log_id', null);
  -- ����������, �������� std.logs.log_level - ������� ��������� ���������
  set_cont('log_level', null);
exception
  when others then
    raise_application_error(-20001,
                            'It wasn`t succeeded to nullify context variables! ' ||
                            SQLERRM);
end;

function open_log(par_file_flag    number default 0, -- ���� - ������� �� ���������� ��������� ����� � ����
                  par_file_name    std.logs.file_name%type default null, -- ��� �����, ���� ������� ���������� ���������, ���� null � ���������� ����������� ������� � ����/����� �������� + id ����
                  par_log_level    std.logs.log_level%type default 'WARN', -- ������� ��������� ���������
                  par_expired_time std.logs.expired_time%type default null, -- ����, �� �������� ����������� ��������, ���� null, �� ���������
                  p_wfr_id         number -- workflow_run_id �� �����������     
                  ) return logs.log_id%type is
  pragma autonomous_transaction;
  var_count INTEGER;
  v_result number;
begin
  --
  -- ���� ��� ���� �������� �������� � �� ����������� ���������� �������� - ��������� ���, �������������� ������� � ���� �������������� �� ����
  --
  if get_log() is not null Then
    SELECT count(*)
      INTO var_count
      FROM std.locks
     WHERE lock_name = 'LOAD';
     
    if var_count = 0 Then
      warn('��������� �������� ������������� ��-�� ������������� ������� ����� (��� ���� �� ����������� ���������� �������� �������� - LOAD)', 'std.pkg_log.open_log');  
      std.pkg_log.close_log(par_raise_flag => 0);
    else
      raise_application_error(-20001, 'It looks like the previous loading process is currently running or has been aborted!'||
                                      ' If previous loading process has been aborted provide that there are no active sessions in Oracle database related to previous loading process!'||
                                      ' After that execute via SQL Plus command: exec std.pkg_load.finish_load_force;');



    end if;    
  end if;  

  initialize_context;

  insert into std.logs
    (log_id, open_time, log_level, file_name, expired_time, workflow_run_id)
  values
    (std.log_seq.nextval,
     sysdate,
     par_log_level,
     case when par_file_flag = 1 then case when par_file_name is not null then
     par_file_name else to_char(sysdate, 'YYYY-MM-DD-HH24-MM-SS') || '_' ||
     to_char(std.log_seq.currval) || '.log' end else to_char(null) end,
     par_expired_time,
     p_wfr_id)
  returning log_id into v_result;
  commit;

  set_cont('log_id', v_result);
  set_cont('log_level', par_log_level);
  -- ���� �� ������� ��������� ���� �������� �� ����� �������, �� �� ������ ��������� ������
  -- � ������ ����� ������ ���� ��� ���� ��������, � ��� ������ ����� ������ �����, � ������ ������ �� �����������
  -- ����� ������ ������ � �����������
  insert into std.log_items
    (log_item_id, log_id, log_level, log_text, log_orig)
  values
    (std.log_items_seq.nextval,
     v_result,
     'INFO',
     to_char(p_wfr_id),
     'WORKFLOW_RUN_ID');
  commit;

  return sys_context('log', 'log_id');

exception
  when others then
    if SQLCODE < -20000 Then
      RAISE;
    else  
      raise_application_error(-20001, 'Error when trying to open a log: '||dbms_utility.format_error_backtrace||' '||SQLERRM);
    end if;      
end;




procedure set_log is
  v_log_id    number;
  v_log_level varchar2(7);
begin

  if ((sys_context('log', 'log_id') is null) or
     (sys_context('log', 'log_level') is null)) then

      select max(log_id), max(log_level)
             into v_log_id, v_log_level
      from std.logs
      where closed_flag = 0;

      set_cont('log_id', v_log_id);
      set_cont('log_level', v_log_level);

  end if;

end;
--
-- ������� id �������� (���������) ���������
--
function get_log return logs.log_id%type is
begin
  set_log;
  return to_number(sys_context('log', 'log_id'));
end get_log;

function get_level_number(p_level varchar2) return number is
begin
  return logging_level(p_level);
exception
  when others then
    return 99999;
end;

function get_level(p_level number) return varchar2 is
begin
  return level_name(p_level);
exception
  when others then
    return 'INFO';
end;

PROCEDURE close_log
(
  par_raise_flag       INTEGER DEFAULT 1 -- ���� 1/0 - ������� �� ��������� exception, ���� � ���� ���� ������ (ERROR � FATAL)
)
--��������� ���� ������� log, ����������� ��� ��������, �������� �������� � ��������������� ����
 is
  pragma autonomous_transaction;

  var_log_id            std.logs.log_id%TYPE;
  var_fatal_qty         INTEGER;
  var_error_qty         INTEGER;
  var_warn_qty          INTEGER;
BEGIN

  BEGIN
    set_log;
    var_log_id := to_number(sys_context('log', 'log_id'));

    if var_log_id is null Then
      return; -- ��� ��������� ���������, ��������� ������
    end if;
    --
    -- ��������� ���������� ������/�������������� � ����
    --
    SELECT nvl(sum(case when log_level = 'WARN' then 1 end), 0),
           nvl(sum(case when log_level = 'ERROR' then 1 end), 0),
           nvl(sum(case when log_level = 'FATAL' then 1 end), 0)
      INTO var_warn_qty, var_error_qty, var_fatal_qty
      FROM std.log_items
     WHERE log_id = var_log_id and
           log_level in ('WARN', 'ERROR', 'FATAL');
    --
    -- ������� ���� � ��������� ����
    --
    UPDATE std.logs l
       SET l.closed_flag = 1,
           l.close_time  = sysdate,
           l.error_qty   = var_error_qty + var_fatal_qty,
           l.warn_qty    = var_warn_qty,
           l.status      = case when var_fatal_qty <> 0 then 'FATAL'
                                when var_error_qty <> 0 then 'ERROR'
                                when var_warn_qty <> 0  then 'WARN'
                                                        else 'INFO'
                           end
     WHERE log_id = var_log_id;
    --
    -- ���������� ��������
    --
    initialize_context;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      initialize_context;
      RAISE;
  END;
  --
  -- ���� ���� - ������������� �� ������� � ���� � ������� exception
  --
  if par_raise_flag = 1 and (var_fatal_qty <> 0 or var_error_qty <> 0) Then
    raise_application_error(-20001, 'There are errors in data loading process. Number of errors by severity: '||trim(case when var_fatal_qty <> 0 then 'FATAL - '||to_char(var_fatal_qty) end||' '||
                                                                                    case when var_error_qty <> 0 then 'ERROR - '||to_char(var_error_qty) end||' '||
                                                                                    case when var_warn_qty <> 0  then 'WARN - '||to_char(var_warn_qty)   end)||'. '||
                                    'See details in the protocol �'||to_char(var_log_id)||' (tables: std.log_items, std.errors_summary, std.errors, log_id = '||to_char(var_log_id)||')');
  end if;
END close_log;

procedure write_log(par_log_level log_items.log_level%type, -- ������� ������������� ���������
                    par_log_text  log_items.log_text%type, -- ����� ������������� ���������
                    par_log_orig  log_items.log_orig%type -- �������� ������������� ���������
                    ) is
  --����� �������� ��������, �������� ��������� ������ � id ������ (����� ������� ����������� ����)
  pragma autonomous_transaction;

begin

  set_log;

  -- ��������� ������������� ������� �����������
  if get_level_number(sys_context('log', 'log_level')) <
     get_level_number(par_log_level) then
    return;
  end if;

  if sys_context('log', 'log_id') is null then
    set_log;
    if sys_context('log', 'log_id') is null then
     -- raise_application_error(-20001,
     --                         '��� �������� ����������(�����)!' || chr(13) ||
     --                         '������� ������� �������� ����� �������� �����������.');
	  return;
    end if;
  end if;

  if par_log_level is null then
    raise_application_error(-20001,
                            'The type of the logged message isn`t known!');
  end if;

  if par_log_text is null then
    raise_application_error(-20001,
                            'It is necessary to specify the text of the message when logging!');
  end if;

  insert into std.log_items
    (log_item_id, log_id, log_level, log_text, log_orig)
  values
    (std.log_items_seq.nextval,
     sys_context('log', 'log_id'),
     par_log_level,
     par_log_text,
     par_log_orig);

  commit;
end;

procedure trace
(par_log_text log_items.log_text%type, -- ����� ������������� ���������
 par_log_orig log_items.log_orig%type default null -- �������� ������������� ���������
 ) is
begin
  if par_log_text is not null then
    std.pkg_log.write_log(par_log_level => 'TRACE',
                          par_log_text  => par_log_text,
                          par_log_orig  => par_log_orig);
  end if;
end;

--
-- �������� ��������� ������ debug
--
procedure debug(par_log_text log_items.log_text%type, -- ����� ������������� ���������
                par_log_orig log_items.log_orig%type default null -- �������� ������������� ���������
                ) is
begin
  if par_log_text is not null then
    std.pkg_log.write_log(par_log_level => 'DEBUG',
                          par_log_text  => par_log_text,
                          par_log_orig  => par_log_orig);
  end if;
end;
--
-- �������� ��������� ������ info
--
procedure info(par_log_text log_items.log_text%type, -- ����� ������������� ���������
               par_log_orig log_items.log_orig%type default null -- �������� ������������� ���������
               ) is
begin
  if par_log_text is not null then
    std.pkg_log.write_log(par_log_level => 'INFO',
                          par_log_text  => par_log_text,
                          par_log_orig  => par_log_orig);
  end if;
end;

--
-- �������� ��������� ������ warn
--
procedure warn(par_log_text log_items.log_text%type, -- ����� ������������� ���������
               par_log_orig log_items.log_orig%type default null -- �������� ������������� ���������
               ) is
begin
  if par_log_text is not null then
    std.pkg_log.write_log(par_log_level => 'WARN',
                          par_log_text  => par_log_text,
                          par_log_orig  => par_log_orig);
  end if;
end;
--
-- �������� ��������� ������ error
--
procedure error(par_log_text log_items.log_text%type, -- ����� ������������� ���������
                par_log_orig log_items.log_orig%type default null -- �������� ������������� ���������
                ) is
begin
  if par_log_text is not null then
    std.pkg_log.write_log(par_log_level => 'ERROR',
                          par_log_text  => par_log_text,
                          par_log_orig  => par_log_orig);
  end if;
end;
--
-- �������� ��������� ������ fatal
--
procedure fatal(par_log_text log_items.log_text%type, -- ����� ������������� ���������
                par_log_orig log_items.log_orig%type default null -- �������� ������������� ���������
                ) is
begin
  if par_log_text is not null then
    std.pkg_log.write_log(par_log_level => 'FATAL',
                          par_log_text  => par_log_text,
                          par_log_orig  => par_log_orig);
  end if;
end;
--
-- ��������� ����������� ������ ����� ���������� ��������� ������������ ������ � �������� �� � ���
--
FUNCTION log_parallel_task_errors
(
  par_task_owner         VARCHAR2, -- �������� ������ (sys.dba_parallel_execute_tasks.task_owner)
  par_task_name          VARCHAR2, -- ��� ������ (sys.dba_parallel_execute_tasks.task_name)
  par_log_level          log_items.log_level%TYPE,                   -- ������� ������������� ���������
  par_log_orig           log_items.log_orig%type                     -- �������� ������������� ���������
)
RETURN INTEGER IS -- ���������� ��������� ������ (0, ���� ������ ���)
  var_count    INTEGER := 0;
BEGIN
  --
  -- ���������� ��������� � ������������� chunk'��
  --
  FOR r IN (SELECT status, error_code, error_message, count(*) as cnt
              FROM sys.dba_parallel_execute_chunks
             WHERE task_owner = par_task_owner and
                   task_name = par_task_name and
                   status <> 'PROCESSED'
            GROUP BY status, error_code, error_message
            ORDER BY decode(status, 'PROCESSED_WITH_ERROR', 1, 'ASSIGNED', 2, 3)) LOOP

    write_log(par_log_level,
              case r.status
                when 'UNASSIGNED'
                then '������ �� ���� ��������� ���������. ���������� ������������� ������ (chunk): '||to_char(r.cnt)||' (������ UNASSIGNED). ������: '||par_task_owner||'.'||par_task_name
                when 'ASSIGNED'
                then '�� ���� ��������� ��������� ������ (chunk). ���������� ������������� ������: '||to_char(r.cnt)||' (������ ASSIGNED). ������: '||par_task_owner||'.'||par_task_name
                when 'PROCESSED_WITH_ERROR'
                then case when r.cnt = 1
                          then '��������� ������ ��� ��������� ����� (chunk). ��� ������: '||r.error_code||', ����� ������: '||r.error_message||'. ������: '||par_task_owner||'.'||par_task_name
                          else '��������� ������ ��� ��������� ������ (chunk). ���������� ������: '||to_char(r.cnt)||', ��� ������: '||r.error_code||', ����� ������: '||r.error_message||'. ������: '||par_task_owner||'.'||par_task_name
                     end
              end,
              par_log_orig);
    var_count := var_count + 1;
  END LOOP;
  --
  -- ���������� ������ �� ������
  --
  if var_count > 0 Then
    FOR r IN (SELECT j.status, j.error#, j.additional_info, count(*) as cnt
                FROM sys.dba_parallel_execute_tasks t
                     join sys.dba_scheduler_job_run_details j on j.owner = t.task_owner and j.job_name like t.job_prefix||'%' and t.job_prefix is not null
               WHERE t.task_owner = par_task_owner and
                     t.task_name = par_task_name and
                     j.status in ('FAILED')
              GROUP BY j.status, j.error#, j.additional_info) LOOP

      write_log(par_log_level,
                case when r.cnt = 1
                     then '������ ��� ���������� ������ �� ������ (job). ��� ������: '||r.error#||', ����� ������: '||r.additional_info||'. ������: '||par_task_owner||'.'||par_task_name
                     else '������ ��� ���������� ���������� ������ (job). ���������� ������: '||to_char(r.cnt)||', ��� ������: '||r.error#||', ����� ������: '||r.additional_info||'. ������: '||par_task_owner||'.'||par_task_name
                end,
                par_log_orig);
    END LOOP;
  end if;

  return var_count;
END log_parallel_task_errors;

begin

  logging_level('TRACE') := 6;
  logging_level('DEBUG') := 5;
  logging_level('INFO') := 4;
  logging_level('WARN') := 3;
  logging_level('ERROR') := 2;
  logging_level('FATAL') := 1;


  level_name(6) := 'TRACE';
  level_name(5) := 'DEBUG';
  level_name(4) := 'INFO';
  level_name(3) := 'WARN';
  level_name(2) := 'ERROR';
  level_name(1) := 'FATAL';

end pkg_log;
/
grant execute on STD.pkg_log to data_loader;
grant execute on std.pkg_log to business with grant option;
--grant execute on std.pkg_log to amladm with grant option;
grant execute on std.pkg_log to mantas with grant option;
grant execute on std.pkg_log to rcod038deposit with grant option;
grant execute on std.pkg_log to rcod038integrator with grant option;
grant execute on std.pkg_log to rcod038nsi with grant option;
grant execute on std.pkg_log to rcod038client with grant option;
grant execute on std.pkg_log to reks000ibs with grant option;
grant execute on std.pkg_log to rgat000gate with grant option;
grant execute on std.pkg_log to rifb000main with grant option;
grant execute on std.pkg_log to rmdc000mdm with grant option;
grant execute on std.pkg_log to rmdr000mdm with grant option;
grant execute on std.pkg_log to rnar000od with grant option;
grant execute on std.pkg_log to rnav000od with grant option;
grant execute on std.pkg_log to rstl000stoplist with grant option;
grant execute on std.pkg_log to rway000way4 with grant option;





