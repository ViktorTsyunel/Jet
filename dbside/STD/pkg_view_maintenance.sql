--grant create any view to std; 
--grant select on dba_tab_privs to std;
--grant select on dba_views to std;
--grant select on dba_users to std;
--grant select on dba_objects to std;

create or replace package std.pkg_view_maintenance authid current_user is

  /*
   * ����� ��� ���������� view � ������ ��������.
   * ����� ���������� view ��� �� � ����� 38, ��� ���������.
   *
   * ��� ������ ������ ����� ������������� �������������� ����������
   *   grant create any view to std; 
   *   grant select on dba_tab_privs to std;
   *   grant select on dba_views to std;
   *   grant select on dba_users to std;
   *   grant select on dba_objects to std;
   *
   * ��� ����� ������� ���� ����� ���������� � �������.
   */
  
  /* ����� ���������� view �������� ������ Back Office*/
  procedure update_views;
  
  -- �������� view �������� ������ �� ����
  procedure update_views_gat;
end;
/

create or replace package body std.pkg_view_maintenance is

  /* ������ view, ������� ��������� ��������� */
  c_view_names constant varchar2(255) := 'V_ADDRESS,V_ADDRESS_PHONE,V_PERSON,V_PERSONCARD,V_PERSON_ADDRESS,V_PERSON_EMAIL,V_PERSON_PHONE,V_CUST_ACCT,V_DEPOHIST,V_DEPOSIT,V_MMPRICE';

  /* ��� ��� �������� ������� � ������ ������ view */
  type r_view is record (
    p_owner dba_views.owner%type,
    p_name  dba_views.view_name%type,
    p_sql   clob
  );

  type t_views is table of r_view;
  v_views t_views := t_views();

  /* ����� ����������� ������ view �� long � clob
   *
   * ��������� ������
   *   p_owner - �����
   *   p_view_name - ��� view
   *   p_text_length - ����� ������ view
   *
   * ��������� ������
   *  clob - ����� view � clob
   */
  function viewtext_to_clob (
    p_owner       in dba_views.owner%type,
    p_view_name   in dba_views.view_name%type,
    p_text_length in dba_views.text_length%type ) return clob
  is
    v_sql    varchar2(256);
    v_cursor number;
    v_text   varchar2(32760);
    v_length number;
    v_clob   clob;
  begin
    v_sql := 'select text from dba_views where view_name = '''||p_view_name||''' and owner = '''||p_owner||'''';
    v_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(v_cursor, v_sql, dbms_sql.native);
    dbms_sql.define_column_long(v_cursor, 1);
    if dbms_sql.execute_and_fetch(v_cursor) = 1 then
      dbms_sql.column_value_long(v_cursor, 1, p_text_length, 0, v_text, v_length);
    end if;
    if dbms_sql.is_open(v_cursor) then
      dbms_sql.close_cursor(v_cursor);
    end if;
    
    dbms_lob.createtemporary(v_clob, false, dbms_lob.call);
    dbms_lob.write(v_clob, length(v_text), 1, v_text);

    return v_clob;
  end;
  
  /* ����� �������� �������������� ��������� view (�� = 38) */
  procedure compile_views
  is
    cursor cur_views is
      select owner, object_name 
        from dba_objects
       where object_type = 'VIEW'
         and regexp_like(owner, '^(RCOD)[(0-9)]{3}(CLIENT|DEPOSIT|INTEGRATOR|NSI)$')
         and to_number(regexp_substr(owner, '[0-9]+', 5)) = 38
         and status <> 'VALID';
  begin
    for cur in cur_views loop
      begin
        execute immediate 'alter view '||cur.owner||'.'||cur.object_name||' compile';
        exception
          when others then null;
      end;  
    end loop;
  end;


  /* ����� �������� ������ view, ������� ���������� ��������, � ��������� ������ v_views */
  procedure get_view_list
  is
    cursor cur_views is
      select v.owner, v.view_name, v.text_length
        from dba_views v
        join table(mantas.rf_pkg_scnro.list_to_tab(c_view_names)) t on t.column_value = v.view_name
       where regexp_like(v.owner, '^(RCOD)[(0-9)]{3}(CLIENT|DEPOSIT|INTEGRATOR|NSI)$')
         and to_number(regexp_substr(v.owner, '[0-9]+', 5)) = 38
       order by v.owner, v.view_name;
  begin
    v_views.delete;
    for cur in cur_views loop
      v_views.extend;
      v_views(v_views.count).p_owner := regexp_replace(cur.owner, '[(0-9){3}]', '#');
      v_views(v_views.count).p_name := cur.view_name;
      v_views(v_views.count).p_sql := viewtext_to_clob(cur.owner, cur.view_name, cur.text_length);
    end loop;
  end;


  /* ����� ������������� ���������� �� ������ ��������� ���������� ������� (�� = 38)
   *
   * ��������� ������
   *   p_src_owner - �����-�������, � view ������� ������� ����������� �����
   *   p_dest_owner - �����-��������, �� view ������� ������� ������ �����
   *   p_view_name - ��� view (���������� ��� � �����-�������, ��� � � �����-���������)
   */
  procedure set_grants (
    p_src_owner      in dba_views.owner%type,
    p_dest_owner     in dba_views.owner%type,
    p_view_name      in dba_views.view_name%type )
  is
    v_ddl varchar2(256);
    cursor cur_grants is
      select grantee, privilege, grantable
        from dba_tab_privs 
       where owner = p_src_owner and table_name = p_view_name;
  begin
    for cur in cur_grants loop
      v_ddl := 'grant '||cur.privilege||' on '||p_dest_owner||'.'||p_view_name||' to '||cur.grantee||
               case 
                 when cur.grantable = 'YES' then 'with grant option'
               end;              
      execute immediate v_ddl;
    end loop;
  end;


  /* ����� ��������� ������ view. ���� ���� ���� ���� �� �������� INVALID, �� �������� ������. */
  procedure check_views
  is
    v_cnt number := 0;
  begin
    select count(*) into v_cnt
      from dba_objects o
      join table(mantas.rf_pkg_scnro.list_to_tab(c_view_names)) t on t.column_value = o.object_name
     where o.object_type = 'VIEW'
       and o.status <> 'VALID'
       and regexp_like(owner, '^(RCOD)[(0-9)]{3}(CLIENT|DEPOSIT|INTEGRATOR|NSI)$');
    
    if v_cnt > 0 then
      raise_application_error('-20001', 'Find '||v_cnt||' invalid view(s).');
    end if;
  end;
  
  /* ����� ���������� view */
  procedure update_views
  is
    v_sql       clob;
    v_view_name varchar2(64);
    cursor cur_tb is 
      select to_number(regexp_substr(username, '[0-9]+', 5)) tb_id
        from dba_users
       where regexp_like(username, '^(RCOD)[(0-9)]{3}(CLIENT|DEPOSIT|INTEGRATOR|NSI)$')
         and to_number(regexp_substr(username, '[0-9]+', 5)) <> 38 -- !!! <> !!!
       group by to_number(regexp_substr(username, '[0-9]+', 5))
      having count(*) = 4
       order by 1;
  begin
    get_view_list;
    
    for cur in cur_tb loop
      for i in v_views.first..v_views.last loop
        v_view_name := replace(v_views(i).p_owner, '###', lpad(cur.tb_id, 3, '0'))||'.'||v_views(i).p_name;
        v_sql := v_views(i).p_sql;
        v_sql := 'CREATE OR REPLACE FORCE VIEW '||v_view_name||' as '||v_sql;
        v_sql := regexp_replace(v_sql, 'RCOD038CLIENT', 'RCOD0'||cur.tb_id||'CLIENT', 1, 0, 'i');
        v_sql := regexp_replace(v_sql, 'RCOD038DEPOSIT', 'RCOD0'||cur.tb_id||'DEPOSIT', 1, 0, 'i');
        v_sql := regexp_replace(v_sql, 'RCOD038INTEGRATOR', 'RCOD0'||cur.tb_id||'INTEGRATOR', 1, 0, 'i');
        v_sql := regexp_replace(v_sql, 'RCOD038NSI', 'RCOD0'||cur.tb_id||'NSI', 1, 0, 'i');
        v_sql := replace(v_sql, '/*##*/38/*##*/', '/*##*/'||cur.tb_id||'/*##*/');
        
        --dbms_output.put_line('Replace view '||v_view_name);
        begin
          execute immediate v_sql;
        --  exception
        --    when others then null;
        end;
        
        set_grants(replace(v_views(i).p_owner, '###', '038'), 
                   replace(v_views(i).p_owner, '###', lpad(cur.tb_id, 3, '0')), 
                   v_views(i).p_name);
      end loop;
    end loop;
    
    compile_views;

    check_views;
    
    v_views.delete;
  end;
-- 
-- �������� view �������� ������ �� ����
-- 
PROCEDURE update_views_gat IS
  var_view_sql  CLOB;
  var_sql       CLOB;
  var_all_sql   CLOB;
BEGIN
  --
  -- ���� �� views-��������
  --
  FOR r IN (SELECT owner, view_name, text_length 
              FROM dba_views v
             WHERE owner = 'RGAT000GATE' and
                   view_name in ('V_CUSTOMERS', 
                                 'V_PARSE_OPERATIONS', 'V_OPERATIONS', 'V_CASH_TRXN', 
                                 'V_CERTIFICATES', 'V_OPERATIONS_CERT', 'V_SCRTY_TO_TRXN')
            ORDER BY decode(view_name, 
                            'V_CUSTOMERS', 1,
                            'V_PARSE_OPERATIONS', 2, 'V_OPERATIONS', 3, 'V_CASH_TRXN', 4, 
                            'V_CERTIFICATES', 5, 'V_OPERATIONS_CERT', 6, 'V_SCRTY_TO_TRXN', 7)) LOOP 

    var_view_sql := viewtext_to_clob(r.owner, r.view_name, r.text_length);
    --
    -- ���� �� ���������
    --     
    var_all_sql := null;             
    FOR r2 IN (SELECT to_char(decode(level, 1, 13, 
                                            2, 16, 
                                            3, 18,
                                            4, 38,
                                            5, 40,
                                            6, 42,
                                            7, 44,
                                            8, 49,
                                            9, 52,
                                           10, 54,
                                           11, 55,
                                           12, 67,
                                           13, 70,
                                           14, 77)) as tb
                 FROM dual
               CONNECT BY level <= 14) LOOP
      --
      -- ������� ��������������� view ��� �������� ��������
      --
      var_sql := 'CREATE OR REPLACE FORCE VIEW RGAT000GATE.'||r.view_name||r2.tb||' as '|| 
                 replace(replace(var_view_sql, '/*##*/', r2.tb), '/*PARTITION (P_##)*/', 'PARTITION (P_'||r2.tb||')');
      EXECUTE IMMEDIATE var_sql;
      --
      -- ����������� ������ ��� ������������ ����-view
      --
      if r.view_name in ('V_CASH_TRXN', 'V_SCRTY_TO_TRXN') Then 
        var_all_sql := var_all_sql||case when var_all_sql is not null then ' UNION ALL ' end||
                       'SELECT /*+ ORDERED*/ v.*, d.tb_partition_key FROM (select '||r2.tb||' as tb_partition_key from dual where rownum <= 1) d '||
                                                                         'cross join RGAT000GATE.'||r.view_name||r2.tb||' v';
        --var_all_sql := var_all_sql||case when var_all_sql is not null then ' UNION ALL ' end||
        --               'SELECT v.*, '||r2.tb||' as tb_partition_key FROM RGAT000GATE.'||r.view_name||r2.tb||' v';
      end if;  
    END LOOP;      
    --
    -- ������� ������������ ����-view
    --
    if r.view_name in ('V_CASH_TRXN', 'V_SCRTY_TO_TRXN') Then 
      var_sql := 'CREATE OR REPLACE FORCE VIEW RGAT000GATE.'||r.view_name||'_ALL as '||var_all_sql;
      EXECUTE IMMEDIATE var_sql;      
      --
      -- ������ �� ����-view �� �� �����, ��� ���� �� view-�������
      --
      FOR r2 IN (SELECT grantee, privilege, grantable
                   FROM dba_tab_privs
                  WHERE owner = r.owner and 
                        table_name = r.view_name) LOOP

        var_sql := 'GRANT '||r2.privilege||' ON '||r.owner||'.'||r.view_name||'_ALL to '||r2.grantee||
                   case when r2.grantable = 'YES' then 'WITH GRANT OPTION' end;
        EXECUTE IMMEDIATE var_sql;
      END LOOP;
    end if;
  END LOOP;
END update_views_gat;
end pkg_view_maintenance;
/

--grant execute on std.pkg_view_maintenance to amladm;
