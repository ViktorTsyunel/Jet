--
-- ���������� ����� ������ �������� ������ ��� ���������� �������� ��������/��������� (����������������� ID ���������)
-- 
CREATE OR REPLACE 
FUNCTION std.get_step_version_nb
(
  par_log_id      std.logs.log_id%TYPE  -- ID ���������
)
return std.load_steps.step_version_nb%TYPE is -- ����� ������ ��������
  var_result     std.load_steps.step_version_nb%TYPE;
BEGIN
  --
  -- ���������� ����� ������ �� ������� ����������� ������, ���� ��� ��� ������� - ����� ������� (������������) ����� ������
  --
  SELECT max(step_version_nb)
    INTO var_result       
    FROM std.load_step_values
   WHERE log_id = par_log_id and
         rownum <= 1; 
  
  if var_result is null Then
    SELECT max(step_version_nb)
      INTO var_result
      FROM std.load_steps;      
  end if;
  
  return var_result;
END get_step_version_nb;  
/

grant EXECUTE on std.get_step_version_nb to MANTAS with grant option;
grant EXECUTE on std.get_step_version_nb to DATA_READER;
grant EXECUTE on std.get_step_version_nb to KDD_ALGORITHM;
grant EXECUTE on std.get_step_version_nb to KDD_ANALYST;
grant EXECUTE on std.get_step_version_nb to KDD_MINER;
grant EXECUTE on std.get_step_version_nb to RF_RSCHEMA_ROLE;
grant EXECUTE on std.get_step_version_nb to CMREVMAN;
grant EXECUTE on std.get_step_version_nb to KDD_MNR with grant option;
grant EXECUTE on std.get_step_version_nb to KDD_REPORT with grant option;
grant EXECUTE on std.get_step_version_nb to KYC;
grant EXECUTE on std.get_step_version_nb to AMLREP;
