grant select on infa_rep.rep_sess_log to std;
grant select on infa_rep.opb_session to std;
grant select on infa_rep.opb_mapping to std;
grant select on infa_rep.opb_subject to std;

grant select on ggdirect.hb to std;

grant execute on MANTAS.RF_TAB_VARCHAR to STD;
grant execute on MANTAS.RF_TAB_NUMBER to STD;
grant execute on MANTAS.RF_PKG_REPORT to STD;
grant execute on MANTAS.RF_PKG_SCNRO_RUN to STD;


CREATE OR REPLACE SYNONYM std.repl_source_times FOR ggdirect.hb;

begin 
  execute immediate 'drop table std.src_replica_source_times#wrk';
exception when others then null; end;
/

begin 
  execute immediate 
'create global temporary table std.src_replica_source_times#wrk
 (
   owner        VARCHAR2(30 CHAR) not null, 
   table_name   VARCHAR2(30 CHAR) not null, 
   source_time  TIMESTAMP,
   constraint repl_source_time#wrk_pk unique (owner, table_name)
 ) 
 on commit preserve rows';
exception when others then null; end;
/

begin 
  execute immediate 'drop table std.rowids#wrk';
exception when others then null; end;
/

begin 
  execute immediate 
'create global temporary table std.rowids#wrk
 (
   row_id    ROWID
 ) 
 on commit preserve rows';
exception when others then null; end;
/

begin 
  execute immediate 'drop table std.consistency_errors#wrk';
exception when others then null; end;
/

begin 
  execute immediate 
'create table std.consistency_errors#wrk
 (
   owner               VARCHAR2(30 CHAR), 
   table_name          VARCHAR2(30 CHAR), 
   parent_owner        VARCHAR2(30 CHAR) not null, 
   parent_table_name   VARCHAR2(30 CHAR) not null, 
   parent_id01         VARCHAR2(64 CHAR),
   parent_id02         VARCHAR2(64 CHAR),
   parent_id03         VARCHAR2(64 CHAR)
 )';
exception when others then null; end;
/

GRANT SELECT, INSERT, UPDATE, DELETE ON std.src_replica_source_times#wrk TO MANTAS;
GRANT SELECT, INSERT, UPDATE, DELETE ON std.rowids#wrk TO MANTAS;
GRANT SELECT, INSERT, UPDATE, DELETE ON std.consistency_errors#wrk TO MANTAS;

CREATE OR REPLACE PACKAGE std.pkg_load IS
-- ���� "���� ������� ������������"
pck_is_preload boolean := false;
-----------------------------------------------------------------------------------------------------------
-- �������� ��������� (���������� ������������ ��� ����������)
-----------------------------------------------------------------------------------------------------------
--  
-- ������� ���� ����� ������� ������������
--
FUNCTION is_preload RETURN BOOLEAN;
--
-- ���������� ���� ����� ������� ������������, ���� ���� ���� ����������, �������� �� "�������" ���������
--
PROCEDURE set_is_preload(par_is_preload BOOLEAN);
--
-- ������������ ������� �������� ������ - ���������� ����������, �������� ��������, "���������" ���������
-- 
PROCEDURE start_load;
--
-- ��������� ����-��������� ������ � ��������-�������� 
--  - ��������� ������
--  - �������� ������ ������������
--  - ��������� ��������, "���������" ���������
--  - �������� ������ � ��������
--  - ��������� ��������������
--
PROCEDURE mark_data_processing(p_workflow_id NUMBER);
--
-- ��������� ������� ��������:
--  - ��������� ������� � ���������� ��� ������ ��������/���������
--  - ����� ����������
--  - ��������� ������, ������������ �� ��������� ��������/���������
--
PROCEDURE finish_load;
--
-- ��������� ������� �������� ����� ������� �����������
--
PROCEDURE finish_load_force;
-----------------------------------------------------------------------------------------------------------
-- ��������������� ��������� - ���������� �� ��������
-----------------------------------------------------------------------------------------------------------
--
-- ���������� ���������� - ���������� �� �������� ��������� ������ ��������� �������� ��������
--
PROCEDURE get_lock
(
  par_lock_name      std.locks.lock_name%TYPE,
  par_expired_sec    INTEGER DEFAULT null
);
--
-- ����� ����������
--
PROCEDURE release_lock(par_lock_name std.locks.lock_name%TYPE);
--
-- ����������� �������� �� ��������-��������, "���������" ��������� � �������� �������� (��������� � ����������� ���������)
--
PROCEDURE create_load_triggers;
--
-- ��������/��������� ��������, "���������" ��������� � �������� �������� (�� �������� ������ � ������������)
--
PROCEDURE set_load_triggers
(
  par_state VARCHAR2
);
--
-- ��������� ��������������� �������� ��������� ��������� �������� ������������� ������� � ������������
-- ��������� �� �������� ������ (� ������������ � SRC_REPLICA_CONSISTENCY). ��������������� ������������
-- �� �������: ������� ����� ������� ������ �������/����� ��������� ������ ����� ������� ������ �� ��������
-- ������ �� ������ ��������� ���������. ���� ��������������� ��� - ��������� ��. 
--
PROCEDURE wait_for_consistency
(
  par_owner        VARCHAR2, -- ����� �� � ��� �������� ������������� �������
  par_table_name   VARCHAR2, -- ��� ������� ���������� ���������/��������� ���������������
  par_repl_name    VARCHAR2  -- ������������ ��������� ��� ��������� ������� (� ������������ � ggdirect.hb.instance_name)
);
--
-- ���������� � ��������� �� ��������� ������� ������� ����� ������� (max(aml_source_time)) 
-- ��� ��������� ������� 
--
PROCEDURE save_source_time
(
  par_owner        VARCHAR2, -- ����� �� � ��� �������� ������������� �������
  par_table_name   VARCHAR2  -- ��� ������� ���������� ���������� � ��������� ������� ����� �������
);
--
-- ��������� ��������������� �������� ��������� �������������� ������ (LOAD_TYPE = A) � 
-- ������������ ��������� �������� ������ ��� ��������� �������-���������
-- ��������������� ������������ �� �������: ������� ����� ������� ���� ���������� ������� ��������� ������ 
-- ����� ������� ������ �� �������� ������ ����� ��������� �� ������ ��������� ���������. 
-- ���� ��������������� ��� - ��������� ��. 
--
PROCEDURE wait_for_consistency
(
  par_src_sys_cd      VARCHAR2,  -- ��� �������-��������� - EKS, etc
  par_tb_id           VARCHAR2   -- ��� �� (� ������ Back Office) - 38, 40, etc
);
--
-- ��������� ��������������� ������������ ��������� ������ ���������� ��������� 
-- ��������������� ������ ������������ �� ���������� ������ �� ������������ �������
-- �������� ��������������� ������ � ��������� ��������
--
PROCEDURE check_consistency
(
  par_src_sys_cd      VARCHAR2,  -- ��� �������-��������� - EKS, etc
  par_tb_id           VARCHAR2   -- ��� �� (� ������ Back Office) - 38, 40, etc
);
--
-- ��������� ������ �������� (�����������/��������/������) ��� ����������� ��������� - ��������� ������ ������������/� ��������
--
PROCEDURE fill_mark_data_list;
--
-- ��������� ������ �������� ������, ���������� ������������, � ��������� ������� ������
-- � ���������� ������� ������������: ������ � ������ ���������������� �� ����� ������-������ �� ���� ���������
--
PROCEDURE set_exception_rows(p_workflow_id number);
--
-- �������� ������������ ������ - ������������ ��� ������������ ������ - � �������� ��� ��� �� ������������ ��������
-- � ������ (������� mark_data_list), ���� ���� - ��������� ��������������
--
PROCEDURE mark_data
(
  par_mark_type     mark_data_list.mark_type%TYPE,                -- LOADED - �������� ������ ������������, TO_LOAD - � ��������
  par_repair_mode   mark_data_list.repair_mode%TYPE DEFAULT null, -- ����� �������������� SOFT/HARD (��� ���� ��������� LOADED), ���������������� ����������� ���� � ������ mark_data_list - ���� �������� ������, �� ����������� �������������� � ��������� ������
  par_log_id        logs.log_id%TYPE DEFAULT null                 -- ID ���������, � �������� ���������� ��������� ������ (���� null - ������ �� ������������)
);
--
-- �������� ������ ������������/� �������� ��� ������ ������� ������ (������� mark_data_list)
-- ��������! ��������� �� ������������ ������� � ������� �������������� HARD,  �. �. � ���� ������ �������������� ������� �������!
--
PROCEDURE process_mdlist_item
(
  par_reg_id             mark_data_list.reg_id%TYPE,
  par_repair_flag        INTEGER   -- 0 - ������� ���������/�������������� (� ������������ � mark_data_list), 
                                   -- 1 - �������������� �������������� � ������ SOFT
);
--
-- ��������� �������������� � ������ HARD ��� ��������� �������
--
FUNCTION repair_table_hard
(
  par_mark_type          mark_data_queries.mark_type%TYPE,  -- ���� ������ LOADED - �������� ������ ������������
  par_owner              mark_data_queries.owner%TYPE,
  par_table_name         mark_data_queries.table_name%TYPE
) 
RETURN INTEGER; -- ���������� ���������� ����� (null, ���� �������������� �� �����������, -1 ���� �������� ������)
END pkg_load;
/

CREATE OR REPLACE PACKAGE BODY std.pkg_load IS
--
-- ����� 0/1 �������� � ������������� ������ ��� ���������� (�������-�������� + �� ��� Back Office)
-- (null - �������� ����� ����������)
--
TYPE TAB_CONSISTENCY_FLAGS IS TABLE OF INTEGER INDEX BY VARCHAR2(16 CHAR);
pck_consistency_flags  TAB_CONSISTENCY_FLAGS;
-----------------------------------------------------------------------------------------------------------
-- �������� ��������� (���������� ������������ ��� ����������)
-----------------------------------------------------------------------------------------------------------
--  
-- ������� ���� ����� ������� ������������
--
FUNCTION is_preload RETURN BOOLEAN IS BEGIN  return pck_is_preload; END;
--
-- ���������� ���� ����� ������� ������������, ���� ���� ���� ����������, �������� �� "�������" ���������
--
PROCEDURE set_is_preload(par_is_preload  BOOLEAN) IS BEGIN  pck_is_preload := par_is_preload; END;
--
-- ������������ ������� �������� ������ - ���������� ����������, �������� ��������, "���������" ���������
-- 
PROCEDURE start_load is
  var_count INTEGER;
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.start_load';
BEGIN
  pkg_log.info('��������� ���������������� ���� �������� �������� ������', prcname);

  -- !!! ������ ���� ���� ����� ��������� � ����� ������ ������� ������ !!! ---------
  BEGIN
    insert into std.error_processing_log(sess_inst_id, sess_start_time, processing_date)
           values(null, systimestamp, sysdate);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      std.pkg_log.error('��������� ������ ��� ������ ������� ������� � ������� std.error_processing_log. ' ||
                        chr(13) || '������: ' || sqlerrm,
                        prcname);
  END;
  -----------------------------------------------------------------------------------
  --
  -- �������� ���������� ���������� �������� ��������, ���� ������� �������� ��� ���� - ����� ������
  --
  get_lock('LOAD');
  --
  -- ���� �����, ��������� �������������� ����� ����������� �������� �������� ������
  --
  pkg_log.info('���� �����, �������� �������������� ����� ����������� �������� �������� ������', prcname);
  
  -- ��������� ���������� ����������� �������� ������� �������� ������
  set_load_triggers('DISABLE');
  -- ��������� �������������� ��� ������ ������ - ���������� aml_new_row_flag
  mark_data(par_mark_type => 'LOADED', par_repair_mode => 'HARD', par_log_id => null /*�� ���������� ������*/);
  -- �������� � �������� ������, ���������� ���������� ������� �������� 
  mark_data(par_mark_type => 'TO_LOAD', par_repair_mode => null, par_log_id => null /*�� ���������� ������*/);  
  -- ���� �������� �������������� ������� � ����� ��������� LOADED - �� ��������� ����� ��������
  -- ��� ������� ���, ��� ������������ aml_new_row_flag � ������� ���������� ����� (� ��������������� ���������� ������) 
  -- ����� �������� � ��������������� ������ ������ ��������
  SELECT count(*)
    INTO var_count
    FROM std.mark_data_list
   WHERE mark_type = 'LOADED' and 
         done_flag = 0; 

  if var_count = 0 Then
    pkg_log.info('��������� ��������� �������������� ����� ����������� �������� �������� ������', prcname);
  else
    pkg_log.fatal('�� ������� ��������� �������������� ����� ����������� �������� �������� ������.'||
                  ' ���������� �������� (�����������/��������/������) � �������� (������� std.mark_data_list, done_flag = 0): '||to_char(var_count)||
                  '. ������ �������� ������ ����������!', prcname);
    raise_application_error(-20001, 'There are errors when recovering. It is impossible to start new data loading process!'||
                                    ' See details in the protocol �'||to_char(pkg_log.get_log)||' (table: std.log_items, log_id = '||to_char(pkg_log.get_log)||')');
  end if;  
  --
  -- ������� ����������� �������� ������� �������� ������ (��������� "�������" ���������)
  --
  create_load_triggers;
  --
  -- ��������� ������ �������� (�����������/��������/������) ��� ����������� ��������� - ��������� ������ ������������/� ��������
  --
  fill_mark_data_list;
  --
  -- ������� ��������������� �������, ������������ ��� ����-��������/���������/����-���������
  --
  mantas.rf_pkg_scnro_run.truncate_wrk_tables;
  --
  -- �������� �������� ������� �������� ������
  --
  set_load_triggers('ENABLE');
  --
  -- �������� � �������� ������ �� ������ ��������
  --
  mark_data(par_mark_type => 'TO_LOAD_BEFORE', par_repair_mode => null, par_log_id => std.pkg_log.get_log);    
  
  pkg_log.info('�������� ���������������� ���� �������� �������� ������', prcname);
EXCEPTION
  WHEN OTHERS THEN
    -- ���������� ��������� � ��������
    if SQLCODE < -20000 Then
      null;
    else  
      std.pkg_log.fatal('������ ��� ���������� ����������������� ����� �������� �������� ������'||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    end if;   
    -- ��������� ��������, ��������� ���������
    begin set_load_triggers('DISABLE'); exception  when others then null; end;                     
    -- ������� ����������
    begin release_lock('LOAD'); exception when others then null; end;                     
    -- ����������� exception
    RAISE;
END start_load;
--
-- ��������� ����-��������� ������ � ��������-�������� 
--  - ��������� ������
--  - �������� ������ ������������
--  - ��������� ��������, "���������" ���������
--  - �������� ������ � ��������
--  - ��������� ��������������
--
PROCEDURE mark_data_processing(p_workflow_id NUMBER) IS
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.mark_data_processing';
BEGIN
  std.pkg_log.info('��������� ������� ����-��������� ������-������ - ��������� ������ ��������, �������� ������ ������������/� �������� � �. �.', prcname);
  --
  -- ��������� ������ ������ ��������
  --
  set_exception_rows(p_workflow_id);
  --
  -- ���������� � ����� SOFT-�������������� �������, � ������� �������� ���� ERRORS.ALL_ROWS_FLAG
  --
  /* !!! ���������������� ����� ��������� � ����� ������ ������� ������ !!!
  UPDATE mark_data_list
     SET repair_mode = 'SOFT'
   WHERE mark_type = 'LOADED' and
         done_flag = 0 and
         repair_mode is null and
         (owner, table_name) in (select source_schema, source_table
                                   from std.errors partition (p_active) e
                                  where all_rows_flag = 1);

  if SQL%ROWCOUNT > 0 Then
    std.pkg_log.warn('����� ������� ������ �������� ������ ���������� ����� �������������� (��������� �������� ���� ������������� ������) ��� ��������� �������� (�����������/��������/������). ���������� ��������: '||to_char(SQL%ROWCOUNT), prcname);
  end if;

  COMMIT;                                  
  */
  --
  -- ���������� � ����� HARD-�������������� ������������� �������, ��� ������� ��� ����������� ��������, ���������� ���������
  --
  UPDATE mark_data_list l
     SET repair_mode = 'HARD'
   WHERE mark_type = 'LOADED' and
         done_flag = 0 and
         -- ������� - �������������
         EXISTS(select null
                  from std.src_replicas sr
                 where sr.owner = l.owner and
                       sr.table_name = l.table_name and
                       sr.load_type = 'R') and
         -- ����������� �������� - ���              
         NOT EXISTS(select null
                      from dba_triggers t
                     where t.table_owner = l.owner and
                           t.table_name = l.table_name and
                           t.trigger_name like 'TRGL%' and
                           t.status = 'ENABLED');

  if SQL%ROWCOUNT > 0 Then
    std.pkg_log.warn('� ����� � ����������� ���������� ��������� ������� �������� ���������� ����� �������������� (��������� �������� ���� ������������� ������) ��� ��������� �������� (�����������/��������/������). ���������� ��������: '||to_char(SQL%ROWCOUNT), prcname);
  end if;
  
  COMMIT;                                  
  --
  -- �������� ������ ������������
  --
  mark_data(par_mark_type => 'LOADED', par_repair_mode => null, par_log_id => pkg_log.get_log);
  --
  -- ��������� ��������, ��������� ���������
  --
  set_load_triggers('DISABLE');
  --
  -- �������� ������ � ��������
  --
  mark_data(par_mark_type => 'TO_LOAD', par_repair_mode => null, par_log_id => pkg_log.get_log);

  pkg_log.info('��������� ������� ����-��������� ������-������', prcname);
EXCEPTION
  WHEN OTHERS THEN
    pkg_log.fatal('������ � �������� �������� ����-��������� ������-������'||
                  '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
END mark_data_processing;
--
-- ��������� ������� ��������:
--  - ��������� ������� � ���������� ��� ������ ��������/���������
--  - ����� ����������
--  - ��������� ������, ������������ �� ��������� ��������/���������
--
PROCEDURE finish_load IS
  var_tmp     INTEGER;
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.finish_load';
BEGIN
  --
  -- ������� � �������� ������� ������/���������, �������, �������� ���������� ��� ������ ��������/���������
  --
  pkg_log.info('������� � �������� ������� ������/���������, �������, �������� ���������� ��� ������ ��������/���������...', prcname);

  INSERT INTO std.load_step_values(log_id, load_date, start_time, end_time, step_version_nb, step_cd, step_status_cd, 
                                   src_sys_cd, tb_id, op_cat_cd, table_nm, instance_nm, partition_id, trxn_count, trxn_scrty_count, 
                                   rows_count, alert_create_count, trxn_repeat_count, alert_repeat_count, 
                                   alert_delete_count, files_count, files_size, files_read_rows_count, files_trxn_count, 
                                   files_trxn_scrty_count, files_rows_count, errors_count, error_msg, infa_full_task_name, 
                                   infa_run_status_code)
  SELECT t.log_id, t.load_date, t.start_time, t.end_time, t.step_version_nb, t.step_cd, t.step_status_cd, 
         t.src_sys_cd, t.tb_id, t.op_cat_cd, t.table_nm, t.instance_nm, t.partition_id, t.trxn_count, t.trxn_scrty_count, 
         t.rows_count, t.alert_create_count, t.trxn_repeat_count, t.alert_repeat_count, 
         t.alert_delete_count, t.files_count, t.files_size, t.files_read_rows_count, t.files_trxn_count, 
         t.files_trxn_scrty_count, t.files_rows_count, t.errors_count, t.error_msg, t.infa_full_task_name, 
         t.infa_run_status_code 
    FROM std.logs l 
         join table(std.get_load_step_values(l.log_id, 'LOG', 1)) t on 1=1
   WHERE l.log_id = std.pkg_log.get_log;
   
  COMMIT;   

  pkg_log.info('��������� ��������� ������� ������/���������, �������, �������� ���������� ��� ������ ��������/���������', prcname);
  --
  -- ������� ���������� �������� ��������/���������
  --
  release_lock('LOAD');
  --
  -- �������� � �������� �� ����� �������������� ������ � �������� ���������� "�� ��������� �������� ��������/���������"
  -- ��������� ������ �� ������� ���������� ��� ����������� ���������
  --
  var_tmp := mantas.rf_pkg_report.auto_report('AFTER_FULL_LOAD');
END finish_load;
--
-- ��������� ������� �������� ����� ������� �����������
--
PROCEDURE finish_load_force IS
BEGIN
  release_lock('LOAD');
  std.pkg_log.close_log(par_raise_flag => 0);
END finish_load_force;
-----------------------------------------------------------------------------------------------------------
-- ��������������� ��������� - ���������� �� ��������
-----------------------------------------------------------------------------------------------------------
--
-- ���������� ���������� - ���������� �� �������� ��������� ������ ��������� �������� ��������
--
PROCEDURE get_lock
(
  par_lock_name       std.locks.lock_name%TYPE,
  par_expired_sec     INTEGER DEFAULT null
) IS
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.get_lock';
BEGIN
  --
  -- ������� ������������ ���������� (����� �������� � ����������� ������� ��������)
  --
  std.pkg_log.debug(par_log_text => '������� ������������ ����������', par_log_orig => prcname);
  DELETE FROM std.locks WHERE expired_time < sysdate;
  COMMIT;
  --
  -- �������� ���������� ����������� ����������
  --
  std.pkg_log.debug(par_log_text => '������������� ����������: '||par_lock_name, par_log_orig => prcname);
  BEGIN  
    INSERT INTO std.locks(lock_name, lock_time, expired_time)
           VALUES(par_lock_name, sysdate, case when par_expired_sec is not null then sysdate + par_expired_sec * 1/86400 end);  
    COMMIT;
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      std.pkg_log.fatal(par_log_text => '����������: '||par_lock_name||' ��� �����������! ��������, �� �������� ���������� ������� �������� ������!', par_log_orig => prcname);
      raise_application_error(-20001, '����������: '||par_lock_name||' ��� �����������! ��������, �� �������� ���������� ������� �������� ������! '||prcname);
    WHEN OTHERS THEN
      std.pkg_log.fatal(par_log_text => '������ ��� ������� ���������� ���������� '||nvl(par_lock_name, '<null>')||': '||SQLERRM, par_log_orig => prcname);
      raise_application_error(-20001, '������ ��� ������� ���������� ���������� '||nvl(par_lock_name, '<null>')||': '||SQLERRM||' '||prcname);
  END;
END get_lock;
--
-- ����� ����������
--
PROCEDURE release_lock
(
  par_lock_name std.locks.lock_name%TYPE
) IS
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.release_lock';
BEGIN  
  std.pkg_log.debug(par_log_text => '������� ����������: ' || par_lock_name, par_log_orig => prcname);
  
  DELETE FROM std.locks WHERE lock_name = par_lock_name;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.error(par_log_text => '������ ��� ������ ����������: ' ||nvl(par_lock_name, '<null>')||': '||SQLERRM, par_log_orig => prcname);
END release_lock;
--
-- ����������� �������� �� ��������-��������, "���������" ��������� � �������� �������� (��������� � ����������� ���������)
--
PROCEDURE create_load_triggers IS
  var_sql   CLOB;
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.create_load_triggers';  
BEGIN
  --
  -- ������� ��������, ��������� ��������� ������ � ������� �������� ��������, ����:
  -- CREATE OR REPLACE TRIGGER <owner>.trgl_<tablename>_biu 
  -- BEFORE INSERT OR UPDATE on <owner>.<tablename> FOR EACH ROW DISABLE
  -- BEGIN 
  --   if std.pkg_load.is_preload() then 
  --     return;
  --   end if;
  -- 
  --   if (inserting and :new.aml_load_status = 1) or 
  --      (updating and nvl(:old.aml_load_status, -1) <> 1 and :new.aml_load_status = 1) Then 
  --     :new.aml_load_status := 9;
  --   elsif updating and :old.aml_load_status = 1 and :new.aml_load_status = 1 Then 
  --     :new.aml_repeat_load_flag := 1; 
  --   end if;
  -- END;
  --  
  -- ���� �� �������� ������������� ��������-�������� (std.src_replicas.load_type = 'R') 
  -- � ��������������� ��������� (���� ������� aml_load_status)
  --
  std.pkg_log.info(par_log_text => '�������/����������� ��������, ��������� ����������', par_log_orig => prcname);
  
  FOR r in (SELECT sr.owner,
                   sr.table_name,
                   decode(rlf.column_name, null, 0, rlf.column_name, 1) as is_repeat_load_flag
              FROM std.src_replicas sr
                   join dba_tab_columns ls on ls.owner = sr.owner and
                                              ls.table_name = sr.table_name and
                                              ls.column_name = 'AML_LOAD_STATUS'
                   left join dba_tab_columns rlf on rlf.owner = sr.owner and
                                                    rlf.table_name = sr.table_name and
                                                    rlf.column_name = 'AML_REPEAT_LOAD_FLAG'
             WHERE sr.load_type = 'R') LOOP

    -- ���������� ����� ��������    
    var_sql := 'create or replace trigger '||lower(r.owner)||'.trgl_'||lower(r.table_name)||'_biu '||chr(13)||
               'before insert or update on '||lower(r.owner)||'.'||lower(r.table_name)||' for each row disable '||chr(13)||
               'begin '||chr(13)||
               '  if std.pkg_load.is_preload() Then '||chr(13)||
               '    return;'||chr(13)||
               '  end if;'||chr(13)||chr(13)||
               
               '  if (inserting and :new.aml_load_status = 1) or '||chr(13)||
               '     (updating and nvl(:old.aml_load_status, -1) <> 1 and :new.aml_load_status = 1) Then '||chr(13)||
               '    :new.aml_load_status := 9;'||chr(13);
      
    if r.is_repeat_load_flag > 0 then
      var_sql := var_sql||
               '  elsif updating and :old.aml_load_status = 1 and :new.aml_load_status = 1 Then '||chr(13)||
               '    :new.aml_repeat_load_flag := 1; '||chr(13)||
               '  end if;'||chr(13)||
               'end; ';
    else
      var_sql := var_sql||
               '  end if;'||chr(13)||
               'end; ';
    end if;
      
    -- ������ ����� ��������� ����� �������� �� ���������� std.pkg_load.is_preload
    EXECUTE IMMEDIATE 'GRANT EXECUTE on std.pkg_load TO ' || r.owner;
 
   -- �������/����������� �������
    std.pkg_log.trace(par_log_text => to_char(substr(var_sql, 1, 2000)), par_log_orig => prcname);
    EXECUTE IMMEDIATE var_sql;
  END LOOP;
  
  std.pkg_log.info(par_log_text => '��������� ���������/������������� ��������, ��������� ����������', par_log_orig => prcname);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.fatal(par_log_text => '��� �������� ���������, ��������� ���������, ��������� ������: '||SQLERRM||
                                      ' ������ �������� ������ ����������!',
                      par_log_orig => prcname);
    raise_application_error(-20001, '��� �������� ���������, ��������� ���������, ��������� ������: '||SQLERRM||
                                    ' ������ �������� ������ ����������! '||prcname);
END create_load_triggers;
--
-- ��������/��������� ��������, "���������" ��������� � �������� �������� (�� �������� ������ � ������������)
--
PROCEDURE set_load_triggers
(
  par_state     VARCHAR2 -- ENABLE ��� DISABLE
) IS
  var_count INTEGER := 0;
  var_errm  VARCHAR2(32000);

  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.set_load_triggers';  
BEGIN
  if par_state is null or par_state not in ('ENABLE', 'DISABLE') Then
    raise_application_error(-20001, '�������� �������� ��������� '||prcname);
  end if;  
  
  if par_state = 'ENABLE' Then
    -- �� ������ ������, ������� ��������� ������� � ������� ������� �� ������ ��������� ��������,
    -- ������� �������� ���������� � ������� ���������������
    DELETE FROM src_replica_source_times#wrk;
    COMMIT;
    pck_consistency_flags.DELETE;
    
    std.pkg_log.info(par_log_text => '�������� ��������, ��������� ���������', par_log_orig => prcname);
  else
    std.pkg_log.info(par_log_text => '��������� ��������, ��������� ���������', par_log_orig => prcname);
  end if;  
  
  FOR r in (SELECT 'ALTER TRIGGER '||t.owner||'.'||t.trigger_name||' '||par_state as sql_tx,
                   sr.owner, 
                   sr.table_name,
                   substr(sr.owner, 2, 3) as src_sys_cd,
                   case when substr(sr.owner, 2, 3) = 'COD' 
                          then substr(sr.owner, 6, 2) 
                   end as tb_id, -- ����� �������� Back Office (������ ������� - ��������� ��������)
                   case when substr(sr.owner, 2, 3) in ('EKS')
                          then 1
                          else 0
                   end as async_flag, -- ���� ����������� ���������� �������
                   sr.repl_name,
                   case when substr(sr.owner, 2, 3)||case when substr(sr.owner, 2, 3) = 'COD' 
                                                          then substr(sr.owner, 6, 2) 
                                                     end <> 
                             nvl(lead(substr(sr.owner, 2, 3)||case when substr(sr.owner, 2, 3) = 'COD' 
                                                                   then substr(sr.owner, 6, 2) 
                                                              end) 
                                 over(order by -- ������ ���� ��� �� ������� ����������, ��� � �� ���� �������!
                                               case when substr(sr.owner, 2, 3) in ('EKS')
                                                    then 1
                                                    else 2
                                               end,                                 
                                               substr(sr.owner, 2, 3),  
                                               case when substr(sr.owner, 2, 3) = 'COD' 
                                                    then substr(sr.owner, 6, 2) 
                                               end,        
                                               sr.order_number desc nulls last, 
                                               t.owner, 
                                               t.trigger_name), '?')
                        then 1
                        else 0
                   end as last_table_flag -- ���� ��������� ������� ��� ��������� � ������ ������
              FROM std.src_replicas sr
                   join dba_triggers t on t.table_owner = sr.owner and
                                          t.table_name = sr.table_name
             WHERE sr.load_type = 'R' and 
                   t.trigger_name like 'TRGL%' and
                   t.status <> decode(par_state, 'DISABLE', 'DISABLED', 'ENABLE', 'ENABLED')
            ORDER BY case when substr(sr.owner, 2, 3) in ('EKS')
                          then 1
                          else 2
                     end,                             -- ������� �������-���������, ������������� ����������, ����� - ��� ��������� (��� ������������ ��������� ��������� ����� ��������, ����� �������� �������� ��������� ������, ��������� ��������)    
                     substr(sr.owner, 2, 3),          -- ������ �������-�������� - ��������
                     case when substr(sr.owner, 2, 3) = 'COD' -- ������ ������� Back Office - ��������
                          then substr(sr.owner, 6, 2) 
                     end,
                     sr.order_number desc nulls last, -- �����! ������� ��������� ��������� �� �������� ������ � ������������ (���������� - �� �����)
                     t.owner, 
                     t.trigger_name) LOOP    
    --
    -- ��� ��������� ��������� ���������/���������� ��������������� ��������������� �������
    -- (��� ������� � ����������� ����������� �, ��������������, ������������ �������� �������)
    --
    if par_state = 'ENABLE' and r.async_flag = 1 Then
      wait_for_consistency(r.owner, r.table_name, r.repl_name);
    end if;
        
    BEGIN      
      -- ����������� "�������" ������� REKS000IBS.Z#MAIN_DOCUM:
      -- � �� �������� ������ Z#BASE_VAL_OP, Z#DOCUM_RC, Z#GOZ_DOC_IN, Z#GOZ_DOC_REQS �� ��� ��������, "�������������" � Z#MAIN_DOCUM
      -- ��������������� ������ � �������� �������� - ���� ��� �������� ������, ������ - ��� ������� ��������
      -- (��� ��������� �������� �� ����������� ��� ��������� Z#MAIN_DOCUM � �� �������� ������ � ������ ������� ����������� ����������)

      -- �������� �������� ������� ��������
      if par_state = 'ENABLE' and r.owner = 'REKS000IBS' and r.table_name = 'Z#MAIN_DOCUM' Then
      
        pkg_log.trace('�������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP_WL ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP_WL ENABLE';
        pkg_log.trace('�������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC_WL ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC_WL ENABLE';
        pkg_log.trace('�������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN_WL ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN_WL ENABLE';
        pkg_log.trace('�������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS_WL ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS_WL ENABLE';

      -- �������� �������� �������� �������  
      elsif par_state = 'DISABLE' and r.owner = 'REKS000IBS' and r.table_name = 'Z#MAIN_DOCUM' Then
      
        pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP ENABLE';
        pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC ENABLE';
        pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN ENABLE';
        pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS ENABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS ENABLE';
      
      end if;

      -- ����������, ���������/���������� TRGL-��������
      std.pkg_log.trace(par_log_text => r.sql_tx, par_log_orig => prcname);
      EXECUTE IMMEDIATE r.sql_tx;
      var_count := var_count + 1;
      
      -- ��������� �������� �������� ������ ��� �������� ������ Z#BASE_VAL_OP, Z#DOCUM_RC, Z#GOZ_DOC_IN, Z#GOZ_DOC_REQS ������� REKS000IBS.Z#MAIN_DOCUM
      if par_state = 'ENABLE' and r.owner = 'REKS000IBS' and r.table_name = 'Z#MAIN_DOCUM' Then

        pkg_log.trace('��������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP DISABLE';
        pkg_log.trace('��������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC DISABLE';
        pkg_log.trace('��������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN DISABLE';
        pkg_log.trace('��������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS DISABLE';

      -- ��������� �������� ������� ��������
      elsif par_state = 'DISABLE' and r.owner = 'REKS000IBS' and r.table_name = 'Z#MAIN_DOCUM' Then

        pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP_WL DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP_WL DISABLE';
        pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC_WL DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC_WL DISABLE';
        pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN_WL DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN_WL DISABLE';
        pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS_WL DISABLE', prcname);
        EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS_WL DISABLE';

      end if;
    EXCEPTION
      WHEN OTHERS THEN
        var_errm := SQLERRM;
        -- �������� ������
        if par_state = 'ENABLE' then          
          std.pkg_log.fatal(par_log_text => '������ ��� ��������� ��������, ���������� ���������: '||var_errm||' ����� �������: '||r.sql_tx||
                                            ' ������ �������� ������ ����������!',
                            par_log_orig => prcname);                            
        else                                  
          std.pkg_log.error(par_log_text => '������ ��� ���������� ��������, ���������� ���������: '||var_errm||' ����� �������: '||r.sql_tx||
                                            ' ������� ��������� ���� ������� ��� ����� �������!',
                            par_log_orig => prcname);
        end if;
        -- ���������� �������� �������� ������ Z#MAIN_DOCUM � ��������� �������� �������
        if r.owner = 'REKS000IBS' and r.table_name = 'Z#MAIN_DOCUM' Then
          begin
            pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP ENABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP ENABLE';
            pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC ENABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC ENABLE';
            pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN ENABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN ENABLE';
            pkg_log.trace('�������� ������� �������� �������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS ENABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS ENABLE';

            pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP_WL DISABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#BASE_VAL_OP_WL DISABLE';
            pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC_WL DISABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#DOCUM_RC_WL DISABLE';
            pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN_WL DISABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_IN_WL DISABLE';
            pkg_log.trace('��������� ����������� ������� ������� ��������: ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS_WL DISABLE', prcname);
            EXECUTE IMMEDIATE 'ALTER TRIGGER REKS000IBS.TRG_Z#GOZ_DOC_REQS_WL DISABLE';
          exception
            when others then
              std.pkg_log.fatal('������ ��� ���������/���������� ��������� ������ REKS000IBS.Z#BASE_VAL_OP, REKS000IBS.Z#DOCUM_RC, REKS000IBS.Z#GOZ_DOC_IN, REKS000IBS.Z#GOZ_DOC_REQS: '||SQLERRM, prcname);
          end;
        end if;
        -- ���� ��� ������ �������� - ����������� exception
        if par_state = 'ENABLE' then          
          raise_application_error(-20001, '������ ��� ��������� ��������, ���������� ���������: '||var_errm||' ����� �������: '||r.sql_tx||
                                          ' ������ �������� ������ ����������! '||prcname);
        end if;
    END;
    --
    -- ���������� ����� ������� �� ������ ��������� �������� ��� ������� � ����������� �����������
    --
    if par_state = 'ENABLE' and r.async_flag = 1 Then
      save_source_time(r.owner, r.table_name);
      --
      -- ���� ��� ��������� ������� � ������ ���������:
      -- - ���������� ��������������� �������������� ������
      -- - ��������� ��������������� ������������ ���������
      --
      if r.last_table_flag = 1 Then
        wait_for_consistency(r.src_sys_cd, r.tb_id);
        check_consistency(r.src_sys_cd, r.tb_id);
      end if;
    end if;  
  END LOOP;
  
  if par_state = 'ENABLE' Then
    std.pkg_log.info(par_log_text => '��������� �������� ��������, ��������� ���������, �������� ���������: '||to_char(var_count), par_log_orig => prcname);
  else
    std.pkg_log.info(par_log_text => '��������� ��������� ��������, ��������� ���������, ��������� ���������: '||to_char(var_count), par_log_orig => prcname);
  end if;  
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      if par_state = 'ENABLE' then
        std.pkg_log.fatal(par_log_text => '������ ��� ��������� ���������, ��������� ���������: '||SQLERRM||' ������ �������� ������ ����������!', par_log_orig => prcname);
        raise_application_error(-20001,'������ ��� ��������� ���������, ��������� ���������: '||SQLERRM||' ������ �������� ������ ����������! '||prcname);
      else                                  
        std.pkg_log.error(par_log_text => '������ ��� ���������� ���������, ��������� ���������: '||SQLERRM||
                                          ' ������� ��������� ��������, ��������� ���������, ��� ����� �������!',
                          par_log_orig => prcname);
      end if;
    end if;
END set_load_triggers;
--
-- ��������� ��������������� �������� ��������� ��������� �������� ������������� ������� � ������������
-- ��������� �� �������� ������ (� ������������ � SRC_REPLICA_CONSISTENCY). ��������������� ������������
-- �� �������: ������� ����� ������� ������ �������/����� ��������� ������ ����� ������� ������ �� ��������
-- ������ �� ������ ��������� ���������. ���� ��������������� ��� - ��������� ��. 
--
PROCEDURE wait_for_consistency
(
  par_owner        VARCHAR2, -- ����� �� � ��� �������� ������������� �������
  par_table_name   VARCHAR2, -- ��� ������� ���������� ���������/��������� ���������������
  par_repl_name    VARCHAR2  -- ������������ ��������� ��� ��������� ������� (� ������������ � ggdirect.hb.instance_name)
) IS

  var_src_cd               VARCHAR2(16 CHAR);
  var_child_source_time    TIMESTAMP;
  var_repl_source_time     TIMESTAMP;
  var_table_source_time    TIMESTAMP;
  var_count                INTEGER;

  TYPE c_cursor_type IS REF CURSOR;
  c_cursor                 c_cursor_type;
  
  var_cycles               INTEGER := 0;
  
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.wait_for_consistency';  
BEGIN
  if par_owner is null or par_table_name is null or par_repl_name is null Then
    std.pkg_log.fatal('�������� �������� ���������', prcname);
    raise_application_error(-20001, '�������� �������� ��������� '||prcname);
  end if;  
  --
  -- ���� ��� ��������� ���������� ����� �������� ��� ����������� ��������������� - �������
  --
  var_src_cd := substr(par_owner, 2, 3)||case when substr(par_owner, 2, 3) = 'COD' -- ��� ������� + ��� �� ��� Back Office
                                              then substr(par_owner, 6, 2) 
                                         end;
                                         
  if pck_consistency_flags.EXISTS(var_src_cd) Then
    if pck_consistency_flags(var_src_cd) = 0 Then
      return;
    end if;
  else
    pck_consistency_flags(var_src_cd) := null; -- ���� ���� ��������������� ����������
  end if;
  --
  -- ���������� ������������ ����� ������� ��� �������� ������ ��������� �������
  --
  SELECT max(t.source_time), 
         sum(case when t.source_time is null
                  then 1
                  else 0
             end)     
    INTO var_child_source_time, var_count
    FROM src_replica_consistency c
         left join src_replica_source_times#wrk t on t.owner = c.owner and
                                                     t.table_name = c.table_name
   WHERE c.consist_owner = par_owner and
         c.consist_table_name = par_table_name;
  --
  -- ���� �� ��� ���� �������� ������ �������� ����� ������� - ������
  --     
  if var_count > 0 Then
    std.pkg_log.fatal('�� ��� ���� �������� ������ �������: '||par_owner||'.'||par_table_name||
                      ' �������� ����� ������� �� ������ ��������� ��������, ���������� ���������. ��������� �������� � ���������� ������ ������ � STD.SRC_REPLICAS � ����������� ������ � STD.SRC_REPLICA_CONSISTENCY', prcname);
    raise_application_error(-20001, '�� ��� ���� �������� ������ �������: '||par_owner||'.'||par_table_name||
                                    ' �������� ����� ������� �� ������ ��������� ��������, ���������� ���������. ��������� �������� � ���������� ������ ������ � STD.SRC_REPLICAS � ����������� ������ � STD.SRC_REPLICA_CONSISTENCY '||prcname);
  end if;  
  --
  -- ���� �������� ������ ��� - �������
  --
  if var_child_source_time is null Then
    pkg_log.debug('��� �������� �������� ������ ��� �������: '||par_owner||'.'||par_table_name||
                  '. ������� ��������������� ���� ������� �� ���������', prcname);
    return;
  end if;
  --
  -- ���������� � ��������� ���������� ����� ������� ��������� � ������� ����� ������� ��� ��������� �������,
  -- ���� ������������ �� ��� ������������ ������ (������ ��� ����� ����� ������� �������� ������), �� ����
  --  
  WHILE true LOOP
    --
    -- ���������� ���������� ����� ������� ���������
    --
    SELECT max(src_timestamp)
      INTO var_repl_source_time
      FROM repl_source_times
     WHERE instance_name = par_repl_name;
     
    if var_repl_source_time is null Then
      std.pkg_log.fatal('�� ������� ���������� ���������� ����� ������� ��� ���������: '||par_repl_name||
                        '. ��������� ����������� �������: '||par_owner||'.'||par_table_name||' � STD.SRC_REPLICAS', prcname);
      raise_application_error(-20001, '�� ������� ���������� ���������� ����� ������� ��� ���������: '||par_repl_name||
                                      '. ��������� ����������� �������: '||par_owner||'.'||par_table_name||' � STD.SRC_REPLICAS '||prcname);
    end if;
    --
    -- ���� ���������� ����� ������� ��������� ������ ����� ������� �������� ������ - ��������������� ����������
    -- 
    if var_repl_source_time > var_child_source_time Then
      pkg_log.debug('���������� ��������������� ��� �������: '||par_owner||'.'||par_table_name||
                    '. �� �������� ������� ����� ������������ ����� �������: '||to_char(var_child_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ��������: '||par_repl_name||' ����� ���������� ����� �������: '||to_char(var_repl_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6'), prcname);
      EXIT;
    end if;
    --
    -- ���������� ����� ������� ��� ��������� �������
    -- ��������! � ������� ����������� ������ ���� ������� AML_SOURCE_TIME
    --  
    OPEN c_cursor FOR 'SELECT max(aml_source_time) FROM '||par_owner||'.'||par_table_name;
    FETCH c_cursor INTO var_table_source_time;
    CLOSE c_cursor;
    --
    -- ���� ������� ����� ������� ������ ������� ������ ����� ������� �������� ������ - ��������������� ����������
    -- 
    if var_table_source_time > var_child_source_time Then
      pkg_log.debug('���������� ��������������� ��� �������: '||par_owner||'.'||par_table_name||
                    '. �� �������� ������� ����� ������������ ����� �������: '||to_char(var_child_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ��������: '||par_repl_name||' ����� ���������� ����� �������: '||to_char(var_repl_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ������ ������� ����� ����� �������: '||to_char(var_table_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6'), prcname);
      EXIT;
    end if;

    if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
      pkg_log.debug('������� ��������������� ��� �������: '||par_owner||'.'||par_table_name||
                    '. �� �������� ������� ����� ������������ ����� �������: '||to_char(var_child_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ��������: '||par_repl_name||' ����� ���������� ����� �������: '||to_char(var_repl_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ������ ������� ����� ����� �������: '||to_char(var_table_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6'), prcname);
    end if;    
    --
    -- �������� �� 1 ������� - ���� ���� ��������� ������� "�������" ��������
    --
    mantas.rf_sleep(1);
    
    -- ������������ �� ����� ��������
    var_cycles := var_cycles + 1;    
    if var_cycles > 3000 Then -- ���� ����� ���
      pkg_log.warn('�� ������� ��������� ��������������� ��� �������: '||par_owner||'.'||par_table_name||
                   '. �� �������� ������� ����� ������������ ����� �������: '||to_char(var_child_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                   '. ��������: '||par_repl_name||' ����� ���������� ����� �������: '||to_char(var_repl_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                   '. ������ ������� ����� ����� �������: '||to_char(var_table_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                   '. ��������, �������� ���������� ��� �����. �������� ����� ����������� ��� ����������� ��������������� ����������� ������.', prcname);
      pck_consistency_flags(var_src_cd) := 0; -- ����������� � ����� ��������������� ��������
      EXIT;
    end if;    
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      std.pkg_log.fatal('������ ��� ��������/�������� ��������������� ��� �������: '||par_owner||'.'||par_table_name||', ��������: '||par_repl_name||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
      raise_application_error(-20001, '������ ��� ��������/�������� ��������������� ��� �������: '||par_owner||'.'||par_table_name||', ��������: '||par_repl_name||
                                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||' '||prcname);
    end if;
END wait_for_consistency;
--
-- ���������� � ��������� �� ��������� ������� ������� ����� ������� (max(aml_source_time)) 
-- ��� ��������� ������� 
--
PROCEDURE save_source_time
(
  par_owner        VARCHAR2, -- ����� �� � ��� �������� ������������� �������
  par_table_name   VARCHAR2  -- ��� ������� ���������� ���������� � ��������� ������� ����� �������
) IS

  var_src_cd               VARCHAR2(16 CHAR);

  TYPE c_cursor_type IS REF CURSOR;
  c_cursor                 c_cursor_type;

  var_table_source_time    TIMESTAMP;
  
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.save_source_time';  
BEGIN
  if par_owner is null or par_table_name is null Then
    std.pkg_log.fatal('�������� �������� ���������', prcname);
    raise_application_error(-20001, '�������� �������� ��������� '||prcname);
  end if;  
  --
  -- ���� ��� ��������� ���������� ����� �������� ��� ����������� ��������������� - �������
  --
  var_src_cd := substr(par_owner, 2, 3)||case when substr(par_owner, 2, 3) = 'COD' -- ��� ������� + ��� �� ��� Back Office
                                              then substr(par_owner, 6, 2) 
                                         end;
                                         
  if pck_consistency_flags.EXISTS(var_src_cd) Then
    if pck_consistency_flags(var_src_cd) = 0 Then
      return;
    end if;
  else
    pck_consistency_flags(var_src_cd) := null; -- ���� ���� ��������������� ����������
  end if;
  --
  -- ���������� ������� ����� ������� ��� ��������� �������
  --
  OPEN c_cursor FOR 'SELECT max(aml_source_time) FROM '||par_owner||'.'||par_table_name;
  FETCH c_cursor INTO var_table_source_time;
  CLOSE c_cursor;
  --
  -- ���� ����� ������� ����� (����� ����� ���� ��� ������ ��� ����� ����� ���������� �������),
  -- ���������� ��������� ����� ������� � �������� �������
  --
  if var_table_source_time is null Then
    var_table_source_time := to_timestamp(to_date('01.01.1900', 'dd.mm.yyyy'));
  end if;
  --
  -- ��������� ����� �������
  --    
  DELETE FROM src_replica_source_times#wrk where owner = par_owner and table_name = par_table_name;
  INSERT INTO src_replica_source_times#wrk(owner, table_name, source_time) VALUES(par_owner, par_table_name, var_table_source_time);
  COMMIT;

  pkg_log.debug('��������� ����� �������: '||to_char(var_table_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                ' ��� �������: '||par_owner||'.'||par_table_name, prcname);
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      std.pkg_log.fatal('������ ��� ���������� ������� ����� ������� ��� �������: '||par_owner||'.'||par_table_name||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
      raise_application_error(-20001, '������ ��� ���������� ������� ����� ������� ��� �������: '||par_owner||'.'||par_table_name||
                                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||' '||prcname);
    end if;
END save_source_time;  
--
-- ��������� ��������������� �������� ��������� �������������� ������ (LOAD_TYPE = A) � 
-- ������������ ��������� �������� ������ ��� ��������� �������-���������
-- ��������������� ������������ �� �������: ������� ����� ������� ���� ���������� ������� ��������� ������ 
-- ����� ������� ������ �� �������� ������ ����� ��������� �� ������ ��������� ���������. 
-- ���� ��������������� ��� - ��������� ��. 
--
PROCEDURE wait_for_consistency
(
  par_src_sys_cd      VARCHAR2,  -- ��� �������-��������� - EKS, etc
  par_tb_id           VARCHAR2   -- ��� �� (� ������ Back Office) - 38, 40, etc
) IS

  var_src_cd               VARCHAR2(16 CHAR);
  var_tables_source_time   TIMESTAMP;
  var_repl_source_time     TIMESTAMP;
  var_count                INTEGER;

  var_cycles               INTEGER := 0;

  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.wait_for_consistency';  
BEGIN
  if par_src_sys_cd is null or (par_src_sys_cd = 'COD' and par_tb_id is null) Then
    pkg_log.fatal('�������� �������� ���������', prcname);
    raise_application_error(-20001, '�������� �������� ��������� '||prcname);
  end if;  
  --
  -- ���� ��� ��������� ���������� ����� �������� ��� ����������� ��������������� - �������
  --
  var_src_cd := par_src_sys_cd||case when par_src_sys_cd = 'COD' -- ��� ������� + ��� �� ��� Back Office
                                     then par_tb_id 
                                end;
                                         
  if pck_consistency_flags.EXISTS(var_src_cd) Then
    if pck_consistency_flags(var_src_cd) = 0 Then
      return;
    end if;
  else
    pck_consistency_flags(var_src_cd) := null; -- ���� ���� ��������������� ����������
  end if;
  --
  -- ���������� ������������ ����� ������� ��� �������� ������ ���������� ���������
  --
  SELECT max(t.source_time), 
         sum(case when t.source_time is null
                  then 1
                  else 0
             end)     
    INTO var_tables_source_time, var_count
    FROM src_replica_source_times#wrk t 
   WHERE substr(t.owner, 2, 3) = par_src_sys_cd and
         (par_src_sys_cd <> 'COD' or
          (par_src_sys_cd = 'COD' and substr(t.owner, 6, 2) = par_tb_id));
  --
  -- ���� �� ��� ���� ������ �������� ����� ������� - ������
  --     
  if var_count > 0 Then
    std.pkg_log.fatal('�� ��� ���� �������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                      ' �������� ����� ������� �� ������ ��������� ��������, ���������� ���������. ��������� �������� � ���������� ������ ������ � STD.SRC_REPLICAS � ����������� ������ � STD.SRC_REPLICA_CONSISTENCY', prcname);
    raise_application_error(-20001, '�� ��� ���� �������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                                    ' �������� ����� ������� �� ������ ��������� ��������, ���������� ���������. ��������� �������� � ���������� ������ ������ � STD.SRC_REPLICAS � ����������� ������ � STD.SRC_REPLICA_CONSISTENCY '||prcname);
  end if;  
  --
  -- ���� �� ������� ���������� ������������ ����� �������  - ������
  --
  if var_tables_source_time is null Then
    std.pkg_log.fatal('�� ������� ���������� ������������ ����� ������� �� ������ ��������� ���������, ��������� ���������, ��� �������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                      '. ��������� �������� � ���������� ������ ������ � STD.SRC_REPLICAS � ����������� ������ � STD.SRC_REPLICA_CONSISTENCY', prcname);
    raise_application_error(-20001, '�� ������� ���������� ������������ ����� ������� �� ������ ��������� ���������, ��������� ���������, ��� �������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                                    '. ��������� �������� � ���������� ������ ������ � STD.SRC_REPLICAS � ����������� ������ � STD.SRC_REPLICA_CONSISTENCY '||prcname);
  end if;
  --
  -- ���������� � ��������� ���������� ����� ������� ���������� ������� ���������,
  -- ���� ����������� �� ��� ������������ ������ (������ ��� ����� ������������ ����� ������� �������� ������), 
  -- �� ����
  --  
  WHILE true LOOP
    --
    -- ���������� ����������� ����� ����� ���������� ����� ������� ���������� ������� ���������
    --
    SELECT min(max(src_timestamp))
      INTO var_repl_source_time
      FROM repl_source_times
     WHERE instance_name in (select distinct repl_name
                               from std.src_replicas
                              where load_type = 'R' and
                                    substr(owner, 2, 3) = par_src_sys_cd and                                  
                                    (par_src_sys_cd <> 'COD' or
                                     (par_src_sys_cd = 'COD' and substr(owner, 6, 2) = par_tb_id)))                                    
    GROUP BY instance_name;
     
    if var_repl_source_time is null Then
      std.pkg_log.fatal('�� ������� ���������� ���������� ����� ������� ���������� ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                        '. ��������� ����������� ������ ��������� � STD.SRC_REPLICAS ', prcname);
      raise_application_error(-20001, '�� ������� ���������� ���������� ����� ������� ���������� ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                                      '. ��������� ����������� ������ ��������� � STD.SRC_REPLICAS '||prcname);
    end if;
    --
    -- ���� ���������� ����� ������� ���������� ������ ����� ������� �������� ������ ��������� - ��������������� ����������
    -- 
    if var_repl_source_time > var_tables_source_time Then
      pkg_log.debug('���������� ��������������� ��� �������������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                    '. �������� ������� ��������� ����� ������������ ����� �������: '||to_char(var_tables_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ��������� ��������� ����� ���������� ����� �������: '||to_char(var_repl_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6'), prcname);
      EXIT;
    end if;

    if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
      pkg_log.debug('������� ��������������� ��� �������������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                    '. �������� ������� ��������� ����� ������������ ����� �������: '||to_char(var_tables_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ��������� ��������� ����� ���������� ����� �������: '||to_char(var_repl_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6'), prcname);
    end if;    
    --
    -- �������� �� 1 ������� - ���� ���� �������������� ������� "�������" ��������
    --
    mantas.rf_sleep(1);
    
    -- ������������ �� ����� ��������
    var_cycles := var_cycles + 1;    
    if var_cycles > 3000 Then -- ���� ����� ���
      pkg_log.warn('�� ������� ��������� ��������������� ��� �������������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                    '. �������� ������� ��������� ����� ������������ ����� �������: '||to_char(var_tables_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ��������� ��������� ����� ���������� ����� �������: '||to_char(var_repl_source_time, 'dd.mm.yyyy hh24:mi:ss.ff6')||
                    '. ��������, �������� ���������� ��� �����. �������� ����� ����������� ��� ����������� ��������������� ����������� ������.', prcname);
      pck_consistency_flags(var_src_cd) := 0; -- ����������� � ����� ��������������� ��������
      EXIT;
    end if;    
  END LOOP;
  --
  -- ���� ���� ��������������� ��� �� ����������, ������ ���� ��� ���������� � 1 - �������������� ����
  --
  if pck_consistency_flags(var_src_cd) is null Then
    pck_consistency_flags(var_src_cd) := 1;
  end if;
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      std.pkg_log.fatal('������ ��� ��������/�������� ��������������� ��� �������������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
      raise_application_error(-20001, '������ ��� ��������/�������� ��������������� ��� �������������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||' '||prcname);
    end if;  
END wait_for_consistency;
--
-- ��������� ��������������� ������������ ��������� ������ ���������� ��������� 
-- ��������������� ������ ������������ �� ���������� ������ �� ������������ �������
-- �������� ��������������� ������ � ��������� ��������
--
PROCEDURE check_consistency
(
  par_src_sys_cd      VARCHAR2,  -- ��� �������-��������� - EKS, etc
  par_tb_id           VARCHAR2   -- ��� �� (� ������ Back Office) - 38, 40, etc
) IS

  var_src_cd               VARCHAR2(16 CHAR);
  var_consistency_flag     INTEGER;
  var_sql                  CLOB;
  var_repair_sql           CLOB;

  var_err_count            INTEGER; 
  var_log_id               std.logs.log_id%TYPE;

  var_repair_template_sql CLOB := 
q'{begin
  UPDATE std.mark_data_list 
     SET repair_mode = 'SOFT'
   WHERE mark_type = 'LOADED' and
         owner = '#PAR_OWNER#' and
         table_name = '#PAR_TABLE_NAME#' and
         subpartition_name = '#SUBPARTITION_NAME#' and
         repair_mode is null;

  COMMIT;         
exception
  when others then 
    std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� �������: #PAR_OWNER#.#PAR_TABLE_NAME# (����������� #SUBPARTITION_NAME#). ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, 'std.pkg_load.check_consistency');
end; 
}'; 
  
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.check_consistency';  
BEGIN
  if par_src_sys_cd is null or (par_src_sys_cd = 'COD' and par_tb_id is null) Then
    pkg_log.fatal('�������� �������� ���������', prcname);
    raise_application_error(-20001, '�������� �������� ��������� '||prcname);
  end if;  

  -- ��������� ���� ������ ������������� ��������
  var_src_cd := par_src_sys_cd||case when par_src_sys_cd = 'COD' -- ��� ������� + ��� �� ��� Back Office
                                     then par_tb_id 
                                end;

  if pck_consistency_flags.EXISTS(var_src_cd) Then
    var_consistency_flag := pck_consistency_flags(var_src_cd);
  end if;
  
  if var_consistency_flag is null Then
    std.pkg_log.error('����������� ������: �� ���������� (is null) ���� ������ ������������� �������� ��� ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                      '. �������� ����� ����������� � ��������������� ������!', prcname);
    var_consistency_flag := 0;
  end if;
  --
  -- � ��������������� ������ �������� ����� �������������� (��������� �������� ���� �������) 
  -- ��� ������ � ������������� ������ ������������ ��������������� 
  --
  if var_consistency_flag = 0 Then
    UPDATE std.mark_data_list
       SET repair_mode = 'SOFT'
     WHERE mark_type = 'LOADED' and
           repair_mode is null and
           (owner, table_name) in (SELECT owner, table_name
                                     FROM std.src_replicas
                                    WHERE substr(owner, 2, 3) = par_src_sys_cd and                                  
                                          (par_src_sys_cd <> 'COD' or
                                           (par_src_sys_cd = 'COD' and substr(owner, 6, 2) = par_tb_id)) and 
                                          consistency_obligatory_flag = 1);           
    COMMIT;
  end if;
  --
  -- ���� �� �������� ���������� ��������� � �������� SQL �������� ���������������,
  -- � ��������������� ������ ��������� ������� � ������������� ������ ������������ ��������������� 
  --
  var_log_id := pkg_log.get_log;
  FOR r IN (SELECT sr.owner, sr.table_name, sr.check_consistency_sql, p.partition_name
              FROM std.src_replicas sr
                   left join dba_tab_partitions p on p.table_owner = upper(sr.owner) and
                                                     p.table_name = upper(sr.table_name) and
                                                     p.partition_name = 'P_ACTIVE'
             WHERE substr(owner, 2, 3) = par_src_sys_cd and                                  
                   (par_src_sys_cd <> 'COD' or
                    (par_src_sys_cd = 'COD' and substr(owner, 6, 2) = par_tb_id)) and
                   check_consistency_sql is not null and
                   length(check_consistency_sql) > 0 and
                   (var_consistency_flag = 1 or nvl(consistency_obligatory_flag, 0) = 0)
            ORDER BY substr(sr.owner, 2, 3),          -- ������ �������-�������� - ��������
                     case when substr(sr.owner, 2, 3) = 'COD' -- ������ ������� Back Office - ��������
                          then substr(sr.owner, 6, 2) 
                     end,
                     sr.order_number desc nulls last) LOOP -- �� �������� ������ � ������������ (������ ���, ��� ������������)
    BEGIN
      -- ������� �������, ���� ������������ ��������������� ������
      EXECUTE IMMEDIATE 'TRUNCATE TABLE std.consistency_errors#wrk';
          
      -- ��������� SQL/PLSQL �������� ��������������� 
      var_sql := replace(r.check_consistency_sql, '#CONSISTENCY_FLAG#', to_char(var_consistency_flag));
      
      std.pkg_log.debug('��������� PL/SQL �������� ��������������� ��� �������: '||r.owner||'.'||r.table_name||'...', prcname);

      if r.partition_name is null Then -- ������� ������������������, ��������� � ���� �����
        EXECUTE IMMEDIATE var_sql;
      else -- ��������� ����� mantas.rf_pkg_parallel
        var_repair_sql := replace(replace(var_repair_template_sql, '#PAR_OWNER#', r.owner),
                                                                   '#PAR_TABLE_NAME#', r.table_name);
      
        var_err_count := mantas.rf_pkg_parallel.exec_in_parallel(par_sql            => var_sql,  
                                                                 par_degree         => 128, 
                                                                 par_task_name      => 'aml_check_consistency',
                                                                 par_table_owner    => r.owner, 
                                                                 par_table_name     => r.table_name, 
                                                                 par_partition_name => r.partition_name, 
                                                                 par_chunk_type     => 'SP',
                                                                 par_log_level      => 'FATAL',
                                                                 par_repair_sql     => var_repair_sql);
        
        -- ���� ���� ������, ������������� ����� �������������� ��� ���� ������� (����� ����������� ��������)
        if var_err_count > 0 Then
          UPDATE std.mark_data_list 
             SET repair_mode = 'SOFT'
           WHERE mark_type = 'LOADED' and
                 owner = r.owner and
                 table_name = r.table_name and
                 repair_mode is null;
          COMMIT;         
        end if;
      end if;

      std.pkg_log.debug('��������� ��������� PL/SQL �������� ��������������� ��� �������: '||r.owner||'.'||r.table_name, prcname);
      
      -- ��������� ������� � �������� ������ ���������������  
      if var_log_id is not null Then
        INSERT INTO std.consistency_errors(log_id, owner, table_name, parent_owner, parent_table_name, 
                                           parent_id01, parent_id02, parent_id03)
        SELECT distinct 
               var_log_id, nvl(owner, r.owner), nvl(table_name, r.table_name), parent_owner, parent_table_name, 
               parent_id01, parent_id02, parent_id03
          FROM std.consistency_errors#wrk; 
        
        if SQL%ROWCOUNT > 0 Then  
          pkg_log.warn('���������� ������ ��������������� ��� �������: '||r.owner||'.'||r.table_name||
                            '. ���������� ������: '||to_char(SQL%ROWCOUNT)||
                            ', �������� �������� ��. � ������� std.consistency_errors (log_id = '||to_char(var_log_id)||')', prcname);
        end if;
          
        COMMIT;     
      end if;
    EXCEPTION
      WHEN OTHERS THEN
        std.pkg_log.error('������ ��� ���������� PL/SQL �������� ��������������� ��� �������: '||r.owner||'.'||r.table_name||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
        -- ������������� ����� �������������� ��� ���� ������� (����� ����������� ��������)
        begin
          UPDATE std.mark_data_list 
             SET repair_mode = 'SOFT'
           WHERE mark_type = 'LOADED' and
                 owner = r.owner and
                 table_name = r.table_name and
                 repair_mode is null;
          COMMIT;         
        exception
          when others then 
            std.pkg_log.fatal('������ ��� ��������� ������ �������������� ��� �������: '||r.owner||'.'||r.table_name||
                              '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
        end;     
    END;    
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    if SQLCODE < -20000 Then
      RAISE;
    else
      std.pkg_log.fatal('������ ��� �������� ��������������� ������������ ��������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                        '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
      raise_application_error(-20001, '������ ��� �������� ��������������� ������������ ��������� ������ ���������: '||par_src_sys_cd||case when par_src_sys_cd = 'COD' then ', ��� ��: '||par_tb_id end||
                                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||' '||prcname);
    end if;  
END check_consistency;
--
-- ��������� ������ �������� (�����������/��������/������) ��� ����������� ��������� - ��������� ������ ������������/� ��������
--
PROCEDURE fill_mark_data_list IS
  var_template_sql    CLOB :=
q'{declare
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
  var_rows_qty  INTEGER := 0;
begin
  WHILE true LOOP
    -- �������� rowid �����, ������� ����� ��������,
    -- ��� ���� ��������� ��, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM #FULL_TAB_PART_NAME#
     WHERE #WHERE_CONDITIONS# #AND_NOT_IN_ERRORS# and rownum <= 1000000
    FOR UPDATE SKIP LOCKED;

    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT count(*)
        INTO var_count
        FROM #FULL_TAB_PART_NAME#
       WHERE #WHERE_CONDITIONS# #AND_NOT_IN_ERRORS# and rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ����� ������� #SCHEMA#.#TABLE#'||
                          case when '#SUBPARTITION_NAME#' is not null
                               then ' (����������� #SUBPARTITION_NAME#)' 
                               when '#PARTITION_NAME#' is not null
                               then ' (�������� #PARTITION_NAME#)'
                          end||' ����� #LOG_MARK_DATA_ACTION#', 'std.pkg_load.mark_data');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE #FULL_TAB_PART_NAME#
         SET #UPDATE_SET_PHRASE#
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids)) and
             #WHERE_CONDITIONS#;

      var_rows_qty := var_rows_qty + SQL%ROWCOUNT;
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 10000 Then
      raise_application_error(-20001, '�� ������� #LOG_MARK_DATA_ACTION# ������� #SCHEMA#.#TABLE#'||
                                      case when '#SUBPARTITION_NAME#' is not null
                                           then ' (����������� #SUBPARTITION_NAME#)' 
                                           when '#PARTITION_NAME#' is not null
                                           then ' (�������� #PARTITION_NAME#)'
                                      end||' - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.mark_data)');
    end if;
  END LOOP;
  
 #RESET_NEW_ROW_FLAG_START#
  -- ������� aml_new_row_flag ��� �����, ��� �������� ������� �������� ������ 
  var_cycles := 0;
  WHILE true LOOP
    -- �������� rowid �����, ������� ����� ������������,
    -- ��� ���� ��������� ��, ��������� ������, ��� ��������������� ������� �������
    SELECT rowidtochar(rowid)
      BULK COLLECT INTO var_rowids
      FROM #FULL_TAB_PART_NAME#
     WHERE #IN_ERRORS# and aml_new_row_flag is not null and rownum <= 1000000
    FOR UPDATE SKIP LOCKED;

    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ������������
      SELECT count(*)
        INTO var_count
        FROM #FULL_TAB_PART_NAME#
       WHERE #IN_ERRORS# and aml_new_row_flag is not null and rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ����� ������� #SCHEMA#.#TABLE#'||
                          case when '#SUBPARTITION_NAME#' is not null
                               then ' (����������� #SUBPARTITION_NAME#)' 
                               when '#PARTITION_NAME#' is not null
                               then ' (�������� #PARTITION_NAME#)'
                          end||' ����� �������� aml_new_row_flag', 'std.pkg_load.mark_data');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- ������������ ��������������� ������
      UPDATE #FULL_TAB_PART_NAME#
         SET aml_new_row_flag = null
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids)) and
             #IN_ERRORS# and aml_new_row_flag is not null;

      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 10000 Then
      raise_application_error(-20001, '�� ������� �������� aml_new_row_flag ��� ����� ������� #SCHEMA#.#TABLE#'||
                        case when '#SUBPARTITION_NAME#' is not null
                             then ' (����������� #SUBPARTITION_NAME#)' 
                             when '#PARTITION_NAME#' is not null
                             then ' (�������� #PARTITION_NAME#)'
                        end||', ��� �������� ������� ��������� ������ - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.mark_data)');
    end if;
  END LOOP;  
 #RESET_NEW_ROW_FLAG_END#

 :par_rows_qty_out := var_rows_qty;
end;
}';

  var_templ_hard_repair_sql    CLOB :=
q'{declare
  var_rows_qty       INTEGER;
begin
  LOCK TABLE #SCHEMA#.#TABLE# IN EXCLUSIVE MODE;
  
  UPDATE /*+ PARALLEL(64)*/ #SCHEMA#.#TABLE#
     SET aml_new_row_flag = null
   WHERE #WHERE_CONDITIONS#; 
  
  var_rows_qty := SQL%ROWCOUNT;
  COMMIT; 

 :par_rows_qty_out := var_rows_qty;
end;
}';

  var_templ_idtable_sql    CLOB :=
q'{declare
  var_rowids    mantas.RF_TAB_VARCHAR;
  var_ids       mantas.RF_TAB_NUMBER;
  var_count     INTEGER;
  var_cycles    INTEGER := 0;
  var_rows_qty  INTEGER := 0;
begin
  -- ��������� ��� ������ ������ �������� ������� ��������
  #BEFORE_ONLY_LINE#std.pkg_load.set_is_preload(true);

  -- ������� �������������� ��� �����, ������� � ��� �������� � �������� ��� ����������� � #SCHEMA#.#TABLE#
  -- � ������ TO_LOAD_BEFORE ������, ���������� � "�������" �������� ��������� � ����� � ������ ��������� �������� � ������� ��������, 
  -- �. �. ���������, ��������������� �������� ��������������� ��������� �� ������ �������� ��������
  DELETE FROM #FULL_IDTAB_PART_NAME#
   WHERE rowid in (SELECT /*+ ORDERED USE_NL(t) INDEX(t)*/ idt.rowid
                     FROM (select distinct id from #FULL_IDTAB_PART_NAME#) idt
                          left join #SCHEMA#.#TABLE# t on t.id = idt.id
                    WHERE t.id is null or 
                          (t.aml_active_partition_flag = 1 and 
                           #BEFORE_ONLY_LINE#t.aml_load_status = 1 -- ������ ��� �������� � �������� (TO_LOAD_BEFORE)
                           #AFTER_ONLY_LINE#t.aml_load_status in (1, 9) -- ������ ��� �������� � ��������/"�������" �������� (TO_LOAD - "�������" ������ ���������� � �������� ��� ��������� ����� ������� #SCHEMA#.#TABLE#)
                          ));  
  COMMIT;
    
  WHILE true LOOP
    -- �������� rowid �����, ������� ����� ��������,
    -- ��� ���� ��������� ��, ��������� ������, ��� ��������������� ������� �������
    SELECT /*+ ORDERED USE_NL(t) INDEX(t)*/ rowidtochar(t.rowid), t.id
      BULK COLLECT INTO var_rowids, var_ids
      FROM (select distinct id from #FULL_IDTAB_PART_NAME#) idt
           join #SCHEMA#.#TABLE# t on t.id = idt.id
     WHERE rownum <= 1000000
    FOR UPDATE OF t.id SKIP LOCKED;

    if var_rowids is null or var_rowids.count = 0 Then -- �� ������� ������������� �� ����� ������
      -- ��������� - ���� �� ��� ������, ������� ���� ��������
      SELECT /*+ ORDERED USE_NL(t) INDEX(t)*/ count(*)
        INTO var_count
        FROM (select distinct id from #FULL_IDTAB_PART_NAME#) idt
             join #SCHEMA#.#TABLE# t on t.id = idt.id
       WHERE rownum <= 1;

      if var_count = 0 Then -- ��� ������ ����������
        COMMIT;
        EXIT;
      end if;

      if mod(var_cycles, 600) = 0 Then -- ��� ������ �������� � ������ 10 ����� ����� � ���
        std.pkg_log.debug('������� ������������ ��������������� ����� ������� #SCHEMA#.#TABLE#'||
                          ' ����� #LOG_MARK_DATA_ACTION#', 'std.pkg_load.mark_data');
      end if;    

      -- �������� �� 1 ������� - ���� ������������ ��������������� �����
      mantas.rf_sleep(1);
    else
      -- �������� ��������������� ������
      UPDATE #SCHEMA#.#TABLE#
         SET aml_active_partition_flag = 1,
             aml_load_status = 1
       WHERE rowid in (select chartorowid(column_value) from table(var_rowids));

      var_rows_qty := var_rows_qty + SQL%ROWCOUNT;
      
      -- ������� ������������ �������������� �� ������� ���������������
      DELETE FROM #FULL_IDTAB_PART_NAME# WHERE id in (select column_value from table(var_ids));
      COMMIT;
    end if;

    -- ������ �� ������������ �����
    var_cycles := var_cycles + 1;
    if var_cycles > 3000 Then
      raise_application_error(-20001, '�� ������� #LOG_MARK_DATA_ACTION# ������� #SCHEMA#.#TABLE#'||
                                      ' - ����� ����� ������������� ������� �������� ���������� ����� (std.pkg_load.mark_data)');
    end if;
  END LOOP;

  -- �������� ��� ������ ������ �������� ������� ��������
  #BEFORE_ONLY_LINE#std.pkg_load.set_is_preload(false);
  
 :par_rows_qty_out := var_rows_qty;
end;
}';

  var_template_wout_nrf_sql    CLOB;

  var_mark_sql          CLOB;
  var_soft_repair_sql   CLOB;
  var_hard_repair_sql   CLOB;
  
  var_reg_id            INTEGER;

  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.fill_mark_data_list';
BEGIN
  pkg_log.info('��������� ������ �������� (�����������/��������/������) ��� ����������� ��������� - ��������� ������ ������������/� ��������', prcname);
  --
  -- ������� ������� ������ ��������� ������
  --
  DELETE FROM std.mark_data_list;
  DELETE FROM std.mark_data_queries;
  --
  -- ���������� ����� ������ � ������������ � �������� ���������-���������
  --
  var_template_wout_nrf_sql := substr(var_template_sql, 1, instr(var_template_sql, '#RESET_NEW_ROW_FLAG_START#') - 1)||
                               substr(var_template_sql, instr(var_template_sql, '#RESET_NEW_ROW_FLAG_END#') + length('#RESET_NEW_ROW_FLAG_END#'));
  var_template_sql := replace(replace(var_template_sql, '#RESET_NEW_ROW_FLAG_START#'), '#RESET_NEW_ROW_FLAG_END#');
  
  var_reg_id := 0;                               
  FOR r IN (SELECT m.mark_type, sr.owner, sr.table_name, 
                   case when sp.subpartition_name is null
                        then p.partition_name
                   end as partition_name, 
                   sp.subpartition_name, 
                   sr.owner||'.'||sr.table_name||case when sp.subpartition_name is not null
                                                      then ' SUBPARTITION (#SUBPARTITION_NAME#)'
                                                      when p.partition_name is not null
                                                      then ' PARTITION (#PARTITION_NAME#)'                                                      
                                                 end as full_tab_part_name,    
                   row_number() over(partition by m.mark_type, sr.owner, sr.table_name 
                                     order by p.partition_name, sp.subpartition_name) as rn,
                   -- ������� ��� WHERE
                   case m.mark_type
                     when 'LOADED' then 'aml_load_status = 1'||
                                        case when apf.column_name is not null
                                             then ' and aml_active_partition_flag = 1'
                                        end||
                                        case when rlf.column_name is not null
                                             then ' and aml_repeat_load_flag is null '
                                        end 
                     when 'TO_LOAD' then case when rlf.column_name is not null
                                              then '(aml_load_status = 9 or aml_repeat_load_flag is not null)'
                                              else 'aml_load_status = 9'
                                         end||
                                         case when apf.column_name is not null
                                              then ' and aml_active_partition_flag = 1'
                                         end
                   end as where_conditions,
                   -- ������� ��� UPDATE SET 
                   case m.mark_type
                     when 'LOADED' then 'aml_load_status = 2'||
                                        case when apf.column_name is not null
                                             then ', aml_active_partition_flag = 0'
                                        end||
                                        case when nrf.column_name is not null
                                             then ', aml_new_row_flag = null'
                                        end 
                     when 'TO_LOAD' then 'aml_load_status = 1'||
                                         case when rlf.column_name is not null
                                              then ', aml_repeat_load_flag = null'
                                         end||
                                         case when nrf.column_name is not null and
                                                   rlf.column_name is not null
                                              then ', aml_new_row_flag = decode(aml_repeat_load_flag, null, aml_new_row_flag, null)'
                                         end     
                   end as update_set_phrase,
                   -- �������� �������, ������������ PK � ������� - ������ ����: (ID_MEGA, ID_MAJOR, ID_MINOR), ����������� � nvl �������������� �������
                   case when m.mark_type = 'LOADED' and id1.column_name is not null
                        then '('||case id1.data_type
                                    when 'NUMBER' then decode(id1.nullable, 'N', id1.column_name, 
                                                                          'nvl('||id1.column_name||', -1)')    
                                    when 'DATE'   then decode(id1.nullable, 'N', id1.column_name, 
                                                                          'nvl('||id1.column_name||', to_date(''01.01.0001'', ''dd.mm.yyyy''))')
                                    else decode(id1.nullable, 'N', id1.column_name, 
                                                            'nvl('||id1.column_name||', ''#NULL#'')')    
                                  end||
                                  case id2.data_type
                                    when 'NUMBER' then ', '||decode(id2.nullable, 'N', id2.column_name, 
                                                                                'nvl('||id2.column_name||', -1)')    
                                    when 'DATE'   then ', '||decode(id2.nullable, 'N', id2.column_name, 
                                                                                'nvl('||id2.column_name||', to_date(''01.01.0001'', ''dd.mm.yyyy''))')
                                    else case when id2.column_name is not null 
                                              then ', '||decode(id2.nullable, 'N', id2.column_name, 
                                                                            'nvl('||id2.column_name||', ''#NULL#'')')    
                                         end
                                  end||
                                  case id3.data_type
                                    when 'NUMBER' then ', '||decode(id3.nullable, 'N', id3.column_name, 
                                                                                'nvl('||id3.column_name||', -1)')    
                                    when 'DATE'   then ', '||decode(id3.nullable, 'N', id3.column_name, 
                                                                                'nvl('||id3.column_name||', to_date(''01.01.0001'', ''dd.mm.yyyy''))')
                                    else case when id3.column_name is not null 
                                              then ', '||decode(id3.nullable, 'N', id3.column_name, 
                                                                            'nvl('||id3.column_name||', ''#NULL#'')')    
                                         end
                                  end||')'
                   end as pk_list,               
                   -- ���������, ������������ �������������� �����, ��� �������� ������� �������� ������
                   -- ���������� ����� ����: (select to_number(e.id01), to_number(e.id02), to_number(e.id03) from std.errors partition(p_active) e where e.source_schema = 'RCOD013DEPOSIT' and e.source_table = 'DEPOSIT')
                   -- ����������� � nvl �������������� �������
                   case when m.mark_type = 'LOADED' and id1.column_name is not null
                        then '(select '||case id1.data_type
                                           when 'NUMBER' then decode(id1.nullable, 'N', 'to_number(trim(e.id01))', 
                                                                                     'nvl(to_number(trim(e.id01)), -1)')    
                                           when 'DATE'   then decode(id1.nullable, 'N', 'to_date(trim(e.id01), ''dd.mm.yyyy hh24:mi:ss'')', 
                                                                                     'nvl(to_date(trim(e.id01), ''dd.mm.yyyy hh24:mi:ss''), to_date(''01.01.0001'', ''dd.mm.yyyy''))')
                                           else decode(id1.nullable, 'N', 'e.id01', 
                                                                       'nvl(e.id01, ''#NULL#'')')    
                                         end||
                                         case id2.data_type
                                           when 'NUMBER' then decode(id2.nullable, 'N', ', to_number(trim(e.id02))', 
                                                                                     ', nvl(to_number(trim(e.id02)), -1)')     
                                           when 'DATE'   then decode(id2.nullable, 'N', ', to_date(trim(e.id02), ''dd.mm.yyyy hh24:mi:ss'')', 
                                                                                     ', nvl(to_date(trim(e.id02), ''dd.mm.yyyy hh24:mi:ss''), to_date(''01.01.0001'', ''dd.mm.yyyy''))')
                                           else case when id2.column_name is not null 
                                                     then decode(id2.nullable, 'N', ', e.id02', 
                                                                                 ', nvl(e.id02, ''#NULL#'')')    
                                                end
                                         end||
                                         case id3.data_type
                                           when 'NUMBER' then decode(id3.nullable, 'N', ', to_number(trim(e.id03))', 
                                                                                     ', nvl(to_number(trim(e.id03)), -1)')     
                                           when 'DATE'   then decode(id3.nullable, 'N', ', to_date(trim(e.id03), ''dd.mm.yyyy hh24:mi:ss'')', 
                                                                                     ', nvl(to_date(trim(e.id03), ''dd.mm.yyyy hh24:mi:ss''), to_date(''01.01.0001'', ''dd.mm.yyyy''))')
                                           else case when id3.column_name is not null 
                                                     then decode(id3.nullable, 'N', ', e.id03', 
                                                                                 ', nvl(e.id03, ''#NULL#'')')    
                                                end
                                         end||
                               ' from std.errors /*partition (p_active)*/ e'|| -- !!! ��������������� partition (p_active) ����� ��������� � ����� ������ ������� ������ !!!
                              ' where e.source_schema = '''||sr.owner||''' and'||
                                    ' e.source_table = '''||sr.table_name||''')'                                                                                                                       
                   end as errors_subquery,
                   -- ����� ����� ��������� �� ������ ��� ������ � ���
                   case m.mark_type
                     when 'LOADED'  then '�������� ������������ ��� ������������ ������'
                     when 'TO_LOAD' then '�������� � �������� ��� �����������/������������ � �������� �������� ������'
                   end as log_mark_data_action,
                   -- �������, ��� � ������� ���� aml_new_row_flag
                   case when nrf.column_name is not null
                        then 1
                        else 0
                   end as nrf_flag,     
                   -- ������� ��� WHERE � ������ �������������� SOFT
                   case when m.mark_type = 'LOADED' and nrf.column_name is not null
                        then 'aml_load_status = 1 and aml_new_row_flag is not null'||
                             case when apf.column_name is not null
                                  then ' and aml_active_partition_flag = 1'
                             end
                   end as soft_repair_where_conditions,
                   -- ������� ��� WHERE � ������ �������������� HARD
                   case when m.mark_type = 'LOADED' and nrf.column_name is not null
                        then 'aml_load_status = 1 and aml_new_row_flag is not null'||
                             case when pd.column_name is not null
                                  then ' and aml_partition_date = to_date(''01.01.9999'', ''dd.mm.yyyy'')'
                                  when apf.column_name is not null and pd.column_name is null
                                  then ' and aml_active_partition_flag = 1'
                             end
                   end as hard_repair_where_conditions
              FROM std.src_replicas sr
                   join dba_tab_columns c on c.owner = sr.owner and
                                             c.table_name = sr.table_name and
                                             c.column_name = 'AML_LOAD_STATUS'
                   left join dba_tab_columns rlf on rlf.owner = upper(sr.owner) and
                                                    rlf.table_name = upper(sr.table_name) and
                                                    rlf.column_name = 'AML_REPEAT_LOAD_FLAG'
                   left join dba_tab_columns apf on apf.owner = upper(sr.owner) and
                                                    apf.table_name = upper(sr.table_name) and
                                                    apf.column_name = 'AML_ACTIVE_PARTITION_FLAG'
                   left join dba_tab_columns pd on pd.owner = upper(sr.owner) and
                                                   pd.table_name = upper(sr.table_name) and
                                                   pd.column_name = 'AML_PARTITION_DATE'
                   left join dba_tab_columns nrf on nrf.owner = upper(sr.owner) and
                                                    nrf.table_name = upper(sr.table_name) and
                                                    nrf.column_name = 'AML_NEW_ROW_FLAG'
                   left join dba_tab_columns id1 on id1.owner = upper(sr.owner) and
                                                    id1.table_name = upper(sr.table_name) and
                                                    id1.column_name = upper(sr.source_id01)
                   left join dba_tab_columns id2 on id2.owner = upper(sr.owner) and
                                                    id2.table_name = upper(sr.table_name) and
                                                    id2.column_name = upper(sr.source_id02)
                   left join dba_tab_columns id3 on id3.owner = upper(sr.owner) and
                                                    id3.table_name = upper(sr.table_name) and
                                                    id3.column_name = upper(sr.source_id03)
                   left join dba_tab_partitions p on p.table_owner = upper(sr.owner) and
                                                     p.table_name = upper(sr.table_name)
                   left join dba_tab_subpartitions sp on sp.table_owner = upper(sr.owner) and
                                                         sp.table_name = upper(sr.table_name) and
                                                         sp.partition_name = upper(p.partition_name)
                   join (select 'LOADED' as mark_type
                           from dual
                         union all
                         select 'TO_LOAD' as mark_type
                           from dual) m on (m.mark_type = 'LOADED' or (m.mark_type = 'TO_LOAD' and sr.load_type = 'R'))
             WHERE sr.load_type in ('R', 'F') and
                   ((sr.load_type = 'R' and nvl(p.partition_name, 'NULL') in ('P_ACTIVE', 'NULL')) or
                    (sr.load_type = 'F')) 
            ORDER BY m.mark_type, sr.owner, sr.table_name, p.partition_name, sp.subpartition_name /*������� �����!*/) LOOP 
    --
    -- ���� ��� ������ �������� ��� ��������� ������� - ���������� � ���������� SQL-�������
    --
    if r.rn = 1 Then
      if r.nrf_flag = 1 and r.mark_type = 'LOADED' Then
        var_mark_sql := var_template_sql;

        var_soft_repair_sql := var_template_wout_nrf_sql;
        var_soft_repair_sql := replace(var_soft_repair_sql, '#FULL_TAB_PART_NAME#', r.full_tab_part_name);
        var_soft_repair_sql := replace(var_soft_repair_sql, '#WHERE_CONDITIONS#', r.soft_repair_where_conditions);
        var_soft_repair_sql := replace(var_soft_repair_sql, '#UPDATE_SET_PHRASE#', 'aml_new_row_flag = null');
        var_soft_repair_sql := replace(var_soft_repair_sql, '#AND_NOT_IN_ERRORS#', '');
        var_soft_repair_sql := replace(var_soft_repair_sql, '#IN_ERRORS#', '');
        var_soft_repair_sql := replace(var_soft_repair_sql, '#SCHEMA#', r.owner);
        var_soft_repair_sql := replace(var_soft_repair_sql, '#TABLE#', r.table_name);
        var_soft_repair_sql := replace(var_soft_repair_sql, '#LOG_MARK_DATA_ACTION#', '�������� aml_new_row_flag ��� �����');

        var_hard_repair_sql := var_templ_hard_repair_sql;
        var_hard_repair_sql := replace(var_hard_repair_sql, '#SCHEMA#', r.owner);
        var_hard_repair_sql := replace(var_hard_repair_sql, '#TABLE#', r.table_name);
        var_hard_repair_sql := replace(var_hard_repair_sql, '#WHERE_CONDITIONS#', r.hard_repair_where_conditions);
      else   
        var_mark_sql := var_template_wout_nrf_sql;
        var_soft_repair_sql := null;
        var_hard_repair_sql := null;
      end if;  
      
      var_mark_sql := replace(var_mark_sql, '#FULL_TAB_PART_NAME#', r.full_tab_part_name);
      var_mark_sql := replace(var_mark_sql, '#WHERE_CONDITIONS#', r.where_conditions);
      var_mark_sql := replace(var_mark_sql, '#UPDATE_SET_PHRASE#', r.update_set_phrase);
      var_mark_sql := replace(var_mark_sql, '#AND_NOT_IN_ERRORS#', case when r.mark_type = 'LOADED' then 'and '||r.pk_list||' not in '||r.errors_subquery end);
      var_mark_sql := replace(var_mark_sql, '#IN_ERRORS#', r.pk_list||' in '||r.errors_subquery);
      var_mark_sql := replace(var_mark_sql, '#SCHEMA#', r.owner);
      var_mark_sql := replace(var_mark_sql, '#TABLE#', r.table_name);
      var_mark_sql := replace(var_mark_sql, '#LOG_MARK_DATA_ACTION#', r.log_mark_data_action);              
      
      INSERT INTO std.mark_data_queries(mark_type, owner, table_name, mark_sql, soft_repair_sql, hard_repair_sql)
             VALUES(r.mark_type, r.owner, r.table_name, var_mark_sql, var_soft_repair_sql, var_hard_repair_sql);
    end if;
    --
    -- ������������ ��������� �����������/��������/������� � ������ ���������
    --  
    var_reg_id := var_reg_id + 1;
    INSERT INTO std.mark_data_list(reg_id, mark_type, owner, table_name, partition_name, subpartition_name, 
                                   repair_mode, done_flag, err_flag, start_time, end_time, rows_qty)
           VALUES(var_reg_id, r.mark_type, r.owner, r.table_name, r.partition_name, r.subpartition_name, 
                  null, 0, 0, null, null, null);                        
  END LOOP;  
  --
  -- ��������� � ������ ��������� ������ � ���������������� - ��������� � �������� - �� ������ �������� � ����� 
  --
  FOR r IN (SELECT m.mark_type, 
                   case m.mark_type 
                     when 'TO_LOAD_BEFORE' then sr.idtab_owner
                     when 'TO_LOAD'        then sr.idtab_owner_wl
                   end as idtab_owner,
                   case m.mark_type 
                     when 'TO_LOAD_BEFORE' then sr.idtab_table_name
                     when 'TO_LOAD'        then sr.idtab_table_name_wl
                   end as idtab_table_name, 
                   p.partition_name as idtab_partition_name,
                   case m.mark_type 
                     when 'TO_LOAD_BEFORE' then sr.idtab_owner   ||'.'||sr.idtab_table_name   ||' PARTITION (#PARTITION_NAME#)'
                     when 'TO_LOAD'        then sr.idtab_owner_wl||'.'||sr.idtab_table_name_wl||' PARTITION (#PARTITION_NAME#)'
                   end as full_idtab_part_name,
                   sr.owner, 
                   sr.table_name,
                   row_number() over(partition by m.mark_type, sr.owner, sr.table_name 
                                     order by p.partition_name) as rn,
                   -- ����� ����� ��������� �� ������ ��� ������ � ���
                   case m.mark_type
                     when 'TO_LOAD_BEFORE'  then '�������� � �������� ������ ����� ������� ��������'
                     when 'TO_LOAD'         then '�������� � �������� ��� �����������/������������ � �������� �������� ������'
                   end as log_mark_data_action
              FROM (-- �����, ��������, ��� ����� ����������� � std.src_replicas
                    select 'REKS000IBS' as owner,
                           'Z#MAIN_DOCUM' as table_name,
                           'REKS000IBS' as idtab_owner,
                           'AML_MAIN_DOCUM_ID' as idtab_table_name,
                           'REKS000IBS' as idtab_owner_wl,
                           'AML_MAIN_DOCUM_ID_WL' as idtab_table_name_wl
                      from dual) sr
                   cross join (select 'TO_LOAD_BEFORE' as mark_type
                                 from dual
                               union all
                               select 'TO_LOAD' as mark_type
                                 from dual) m
                   join dba_tab_partitions p on p.table_owner = case m.mark_type 
                                                                  when 'TO_LOAD_BEFORE' then upper(sr.idtab_owner)
                                                                  when 'TO_LOAD'        then upper(sr.idtab_owner_wl)
                                                                end and  
                                                p.table_name = case m.mark_type 
                                                                 when 'TO_LOAD_BEFORE' then upper(sr.idtab_table_name)
                                                                 when 'TO_LOAD'        then upper(sr.idtab_table_name_wl)
                                                               end
            ORDER BY m.mark_type desc, sr.owner, sr.table_name, p.partition_name /*������� �����!*/) LOOP
    --
    -- ���� ��� ������ �������� ��� ��������� ������� - ���������� � ���������� SQL-�������
    --
    if r.rn = 1 Then
      var_mark_sql := var_templ_idtable_sql;
      var_mark_sql := replace(var_mark_sql, '#FULL_IDTAB_PART_NAME#', r.full_idtab_part_name);
      var_mark_sql := replace(var_mark_sql, '#SCHEMA#', r.owner);
      var_mark_sql := replace(var_mark_sql, '#TABLE#', r.table_name);
      var_mark_sql := replace(var_mark_sql, '#LOG_MARK_DATA_ACTION#', r.log_mark_data_action);              
      
      if r.mark_type = 'TO_LOAD_BEFORE' Then
        var_mark_sql := replace(var_mark_sql, '#BEFORE_ONLY_LINE#', '');
        var_mark_sql := replace(var_mark_sql, '#AFTER_ONLY_LINE#', '--');
      elsif r.mark_type = 'TO_LOAD' Then
        var_mark_sql := replace(var_mark_sql, '#BEFORE_ONLY_LINE#', '--');
        var_mark_sql := replace(var_mark_sql, '#AFTER_ONLY_LINE#', '');
      end if;
                  
      INSERT INTO std.mark_data_queries(mark_type, owner, table_name, mark_sql, soft_repair_sql, hard_repair_sql)
             VALUES(r.mark_type, r.idtab_owner, r.idtab_table_name, var_mark_sql, null, null);
    end if;
    --
    -- ������������ ��������� �������� � ������ ���������
    --  
    var_reg_id := var_reg_id + 1;
    INSERT INTO std.mark_data_list(reg_id, mark_type, owner, table_name, partition_name, subpartition_name, 
                                   repair_mode, done_flag, err_flag, start_time, end_time, rows_qty)
           VALUES(var_reg_id, r.mark_type, r.idtab_owner, r.idtab_table_name, r.idtab_partition_name, null, 
                  null, 0, 0, null, null, null);                        
  END LOOP;     
                                                            
  COMMIT;
  pkg_log.info('��������� ��������� ������ �������� (�����������/��������/������) ��� ����������� ��������� - ��������� ������ ������������/� ��������', prcname);
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.fatal(par_log_text => '��� ����������� ������ �������� (�����������/��������/������) ��� ����������� ��������� - ��������� ������ ������������/� ��������, ��������� ������: '||SQLERRM||
                                      ' ������ �������� ������ ����������!',
                      par_log_orig => prcname);
    raise_application_error(-20001, '��� ����������� ������ �������� (�����������/��������/������) ��� ����������� ��������� - ��������� ������ ������������/� ��������, ��������� ������: '||SQLERRM||
                                    ' ������ �������� ������ ����������! '||prcname);      
END fill_mark_data_list;

  -- ��������� ������ ����������� � ��������� ������� ������
  procedure set_exception_rows(p_workflow_id number) is
    v_list varchar2(4000) := null;
  begin
  
    begin
      std.pkg_log.info(par_log_text => '������� ������� � ������������� �������� ������������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
      execute immediate 'truncate table std.errors';
      std.pkg_log.info(par_log_text => '��������� ������� ������� � ������������� �������� ������������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    exception
      when others then
        std.pkg_log.error(par_log_text => '��������� ������ ��� ������� �������� ������� � ������������� �������� ������������.' ||
                                          chr(13) || SQLERRM,
                          par_log_orig => 'std.pkg_load.set_exception_rows');
    end;
  
    begin
      std.pkg_log.debug(par_log_text => '������������ ������-�������, ������������ regexp_substr, ��� ������� ������ �����������.',
                        par_log_orig => 'std.pkg_load.set_exception_rows');
    
      select listagg('(' || er.regexp_string || ')', '|') within group(order by 1)
        into v_list
        from (select distinct r.regexp_string from std.error_rules r) er
       group by 1;
    
      std.pkg_log.debug(par_log_text => '��������� ������������ ������-�������, ������������ regexp_substr, ��� ������� ������ �����������.',
                        par_log_orig => 'std.pkg_load.set_exception_rows');
    
      std.pkg_log.trace(par_log_text => v_list,
                        par_log_orig => 'std.pkg_load.set_exception_rows');
    
    exception
      when others then
        std.pkg_log.error(par_log_text => '��������� ������ ��� ������� ������������ ������-�������,' ||
                                          ' ������������ regexp_substr, ��� ������� ������ �����������. �� ������� ������������ ������-�������.' ||
                                          chr(13) || SQLERRM,
                          par_log_orig => 'std.pkg_load.set_exception_rows');
    end;
  
    begin
      std.pkg_log.info(par_log_text => '����������� ������� � ������������� �������� ������������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    
      insert into std.errors
        (source_schema,
         source_table,
         id01,
         id02,
         id03,
         sess_inst_id,
         sess_start_time,
         workflow_run_id,
         trans_row_id,
         trans_name,
         trans_part_index)
        select distinct er.source_schema,
                        er.source_table,
                        to_char(substr(regexp_substr(regexp_substr(fd.trans_rowdata,
                                                    v_list),
                                      '[^\|]+',
                                      1,
                                      2), 1, 2000)) id1,
                        to_char(substr(regexp_substr(regexp_substr(fd.trans_rowdata,
                                                    v_list),
                                      '[^\|]+',
                                      1,
                                      3), 1, 2000)) id2,
                        to_char(substr(regexp_substr(regexp_substr(fd.trans_rowdata,
                                                    v_list),
                                      '[^\|]+',
                                      1,
                                      4), 1, 2000)) id3,
                        fs.sess_inst_id,
                        fs.sess_start_time,
                        fd.workflow_run_id,
                        fd.trans_row_id,
                        fd.trans_name,
                        fd.trans_part_index
          from std.infa_pmerr_data fd
          join std.infa_pmerr_sess fs
            on fs.sess_inst_id = fd.sess_inst_id
           and fs.workflow_run_id = fd.workflow_run_id
          join std.error_rules er
            on er.source_id_type =
               to_char(substr(regexp_substr(regexp_substr(fd.trans_rowdata, v_list),
                             '\w{3,4}'), 1, 2000))
           and er.infa_table_name = fd.trans_name
         where fd.workflow_run_id = p_workflow_id;
      commit;
    
      -- �������� ����������� ������ �� ������ RMDC000MDM, RMDR000MDM, RCOD038NSI
      -- �� ��� ���� ���������� ������ ���� ������, �� ������ ������������ ��� � std.error_processing_log\
      -- �� ��������� ����� ������ �� ���� ���� �� ������ ���� �������� ������������
      -- �� ���� ����� � ���, ��� �� �� �������� ��� ������ ������ �����-������� ����������� �� ��� ���
      -- ���� ���� ���� �� ���� ������ �� ���� �����
      -- ��� ��� ��� ������ � ����� RCOD ������������ ����� ���������
      insert into std.errors
        (source_schema,
         source_table,
         id01,
         id02,
         id03,
         sess_inst_id,
         sess_start_time,
         workflow_run_id,
         trans_row_id,
         trans_name,
         trans_part_index)
        select distinct case
                          when fs.folder_name not in
                               ('RMDC000MDM', 'RMDR000MDM') then
                           'RCOD038NSI'
                          else
                           fs.folder_name
                        end as source_schema,
                        null as source_table,
                        null as id01,
                        null as id02,
                        null as id03,
                        fs.sess_inst_id,
                        fs.sess_start_time,
                        fd.workflow_run_id,
                        fd.trans_row_id,
                        fd.trans_name,
                        fd.trans_part_index
          from std.infa_pmerr_data fd
          join std.infa_pmerr_sess fs
            on fs.sess_inst_id = fd.sess_inst_id
           and fs.workflow_run_id = fd.workflow_run_id
         where fd.workflow_run_id = p_workflow_id
           and fs.sess_start_time >=
               (select nvl(max(epl.sess_start_time),
                           to_timestamp('2000-01-01', 'YYYY-MM-DD'))
                  from std.error_processing_log epl)
           and (fs.mapping_name in
               ('sm_RCOD_ORG',
                 'sm_RCOD_ORG_RLSHP',
                 'sm_RCOD_RF_CRNCY_RATES',
                 'sm_RCOD_RF_CURRENCY',
                 'sm_RCOD_RF_METAL_RATES',
                 'sm_RCOD_RF_NSI_OP_TYPES',
                 'sm_RCOD_RF_NSI_TR_TYPES',
                 'sm_RCOD_RF_PROD_TYPE',
                 'sm_RCOD_RF_COUNTRY',
                 'sm_RCOD_RF_CUST_ID_DOC_TYPE') or
               fs.folder_name in ('RMDC000MDM', 'RMDR000MDM'));
    
      commit;
    
      std.pkg_log.info(par_log_text => '��������� ������������ ������� � ������������� �������� ������������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    exception
      when others then
        std.pkg_log.error(par_log_text => '��������� ������ ��� ������� ��������� ������� � ������������ �������� �����������. ��������� ������ ����������� ����������.' ||
                                          chr(13) || SQLERRM,
                          par_log_orig => 'std.pkg_load.set_exception_rows');
    end;
  
    begin
      std.pkg_log.info(par_log_text => '����������� ������� ����������� ������� ������ �����������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    
      insert into std.error_processing_log
        (sess_inst_id, sess_start_time, processing_date)
        select z.sess_inst_id, z.sess_start_time, sysdate
          from (select ep.sess_inst_id,
                       ep.sess_start_time,
                       row_number() over(order by ep.sess_start_time desc, ep.sess_inst_id desc) rn
                  from std.errors ep) z
         where z.rn = 1;
    
      commit;
    
      std.pkg_log.info(par_log_text => '��������� ������������ ������� ����������� ������� ������ �����������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    exception
      when others then
        std.pkg_log.error(par_log_text => '��������� ������ ��� ������� ��������� ������� ����������� ������� ������ �����������.' ||
                                          chr(13) || SQLERRM,
                          par_log_orig => 'std.pkg_load.set_exception_rows');
    end;
  
    begin
      std.pkg_log.info(par_log_text => '������� ������� ������ ������������� ������ �����������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    
      for r in (select substr('�� ������� ��������� ������ �����������! ' ||
                              chr(13) || 'sess_inst_id = ' ||
                              fd.sess_inst_id || ' and workflow_run_id = ' ||
                              fd.workflow_run_id || ' and trans_row_id = ' ||
                              fd.trans_row_id || chr(13) || '������: ' ||
                              to_char(substr(ms.error_msg, 1, 300)) || '...' ||
                              chr(13) ||
                              '������ ������ ������, ��������� ������:' ||
                              chr(13) || to_char(substr(fd.trans_rowdata, 1, 2000)),
                              0,
                              2000) err_msg
                  from std.infa_pmerr_data fd
                  left join std.infa_pmerr_msg ms
                    on ms.workflow_run_id = fd.workflow_run_id
                   and ms.sess_inst_id = fd.sess_inst_id
                   and ms.trans_row_id = fd.trans_row_id
                   and ms.line_no = 1
                 where fd.workflow_run_id = p_workflow_id
                   and (fd.sess_inst_id, fd.workflow_run_id, fd.trans_row_id,
                        fd.trans_part_index, fd.trans_name) in
                       (select fd.sess_inst_id,
                               fd.workflow_run_id,
                               fd.trans_row_id,
                               fd.trans_part_index,
                               fd.trans_name
                          from std.infa_pmerr_data fd
                         where fd.workflow_run_id = p_workflow_id
                        minus
                        select er.sess_inst_id,
                               er.workflow_run_id,
                               er.trans_row_id,
                               er.trans_part_index,
                               er.trans_name
                          from std.errors er)
                   and fd.line_no = 1
                 order by fd.sess_inst_id,
                          fd.workflow_run_id,
                          fd.trans_row_id) loop
      
        std.pkg_log.error(par_log_text => r.err_msg,
                          par_log_orig => 'std.pkg_load.set_exception_rows');
      
      end loop;
    
      std.pkg_log.info(par_log_text => '������� ������ ������������� ������ ����������� ��������.',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    
    exception
      when others then
        std.pkg_log.error(par_log_text => '��� ������ ������������� ������ ����������� ��������� ������! ' ||
                                          chr(13) || SQLERRM,
                          par_log_orig => 'std.pkg_load.set_exception_rows');
    end;
  
    begin
      std.pkg_log.info(par_log_text => '����� ������� ���������� ������ �����������!',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    
      for r in (select substr('��������� ' || z.cnt ||
                              ' ������ �� ��������� ' || z.source_name ||
                              ' ��� ������ � ' || z.target_name,
                              0,
                              2000) as msg_str
                  from (select count(*) cnt,
                               e.source_schema || '.' || e.source_table as source_name,
                               fd.trans_name as target_name
                          from std.errors e
                          join std.infa_pmerr_data fd
                            on e.sess_inst_id = fd.sess_inst_id
                           and e.workflow_run_id = fd.workflow_run_id
                           and e.trans_row_id = fd.trans_row_id
                           and e.trans_part_index = fd.trans_part_index
                           and e.trans_name = fd.trans_name
                           and fd.line_no = 1
                         where fd.workflow_run_id = p_workflow_id
                         group by e.source_schema || '.' || e.source_table,
                                  fd.trans_name) z) loop
        std.pkg_log.warn(par_log_text => r.msg_str,
                         par_log_orig => 'std.pkg_load.set_exception_rows');
      end loop;
    
      std.pkg_log.info(par_log_text => '��������� ������� ���������� ������ �����������!',
                       par_log_orig => 'std.pkg_load.set_exception_rows');
    
    exception
      when others then
        std.pkg_log.warn(par_log_text => '��� �������� ���������� ������ ����������� ��������� ������! ' ||
                                         chr(13) || SQLERRM,
                         par_log_orig => 'std.pkg_load.set_exception_rows');
    end;
  
  end;
--
-- �������� ������������ ������ - ������������ ��� ������������ ������ - � �������� ��� ��� �� ������������ ��������
-- � ������ (������� mark_data_list)
--
PROCEDURE mark_data
(
  par_mark_type     mark_data_list.mark_type%TYPE,                -- LOADED - �������� ������ ������������, TO_LOAD - � ��������, TO_LOAD_BEFORE - � �������� ����� ������� �������� ��������
  par_repair_mode   mark_data_list.repair_mode%TYPE DEFAULT null, -- ����� �������������� SOFT/HARD (��� ���� ��������� LOADED), ���������������� ����������� ���� � ������ mark_data_list - ���� �������� ������, �� ����������� �������������� � ��������� ������
  par_log_id        logs.log_id%TYPE DEFAULT null                 -- ID ���������, � �������� ���������� ��������� ������ (���� null - ������ �� ������������)
) IS

  var_chunk_cursor CLOB := 
q'{SELECT reg_id as start_id, #REPAIR_FLAG# as end_id 
  FROM std.mark_data_list
 WHERE mark_type = '#MARK_TYPE#' and
       done_flag = 0 and
       nvl(repair_mode, '?') <> 'HARD'
ORDER BY 1}';
  var_chunk_sql    CLOB := 'begin std.pkg_load.process_mdlist_item(:start_id, :end_id); end;';
  
  var_task_name            VARCHAR2(64 CHAR);

  var_count                INTEGER;
  var_err_count            INTEGER;
  var_object_count         INTEGER;
  var_table_count          INTEGER; 
  var_repair_object_count  INTEGER;
  var_repair_table_count   INTEGER; 
  
  var_parallel_level       INTEGER;   
  
  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.mark_data';
BEGIN
  if par_mark_type is null or par_mark_type not in ('LOADED', 'TO_LOAD', 'TO_LOAD_BEFORE') or
     par_repair_mode not in ('SOFT', 'HARD') or
     (par_repair_mode is not null and par_mark_type <> 'LOADED') Then 
    std.pkg_log.fatal('����������� ������: �������� �������� ���������. par_mark_type: '||par_mark_type||', par_repair_mode: '||par_repair_mode, prcname);
    return;
  end if;
  --
  -- ��������� ��������� �������� � ������� ������/������ �������������� SOFT
  --
  if nvl(par_repair_mode, '?') <> 'HARD' Then
    -- ���������: ���� �� ���� ���� ������ ��� ���������
    SELECT count(*)
      INTO var_count
      FROM std.mark_data_list
     WHERE mark_type = par_mark_type and
           done_flag = 0 and
           nvl(repair_mode, '?') <> 'HARD' and
           rownum <= 1;
           
    if var_count > 0 Then         
      -- ������� ��������� ���������� �� �������������� ��������
      SELECT count(*) as table_count,
             nvl(sum(object_count), 0) as object_count,
             nvl(sum(repair_object_count), 0) as repair_objects_count,
             sum(case when repair_object_count > 0 then 1 else 0 end) as repair_table_count
        INTO var_table_count, var_object_count, var_repair_object_count, var_repair_table_count
        FROM (select owner, table_name,
                     count(*) as object_count,
                     nvl(sum(case when repair_mode is not null then 1 end), 0) as repair_object_count
                from std.mark_data_list
               where mark_type = par_mark_type and
                     done_flag = 0 and
                     nvl(repair_mode, '?') <> 'HARD'
              group by owner, table_name);
      
      if par_mark_type = 'LOADED' Then
        std.pkg_log.info('�������� ������ ������������ � ��������-�������� (������������� �������� dbms_parallel_execute).'||
                         ' ���������� ������: '||to_char(var_table_count)||
                         ', ���������� �������� (�����������/��������/�������������������� ������): '||to_char(var_object_count)||
                         ', �� ��� �������� � ������ �������������� (��������� �������� ���� ������������� �����): '||to_char(var_repair_object_count)||
                         ', ������ � ������ ��������������: '||to_char(var_repair_table_count), prcname);
      elsif par_mark_type = 'TO_LOAD' Then 
        std.pkg_log.info('�������� ������ � �������� � ��������-�������� (������������� �������� dbms_parallel_execute).'||
                         ' ���������� ������: '||to_char(var_table_count)||
                         ', ���������� �������� (�����������/��������/�������������������� ������): '||to_char(var_object_count), prcname);      
      elsif par_mark_type = 'TO_LOAD_BEFORE' Then 
        std.pkg_log.info('�������� ������ � �������� � ��������-�������� ����� ������� �������� (������������� �������� dbms_parallel_execute).'||
                         ' ���������� ������: '||to_char(var_table_count)||
                         ', ���������� �������� (�����������/��������/�������������������� ������): '||to_char(var_object_count), prcname);      
      end if;
      
      -- ������� ������� ������������
      if par_mark_type = 'TO_LOAD_BEFORE' Then
        var_parallel_level := 128;
      else
        var_parallel_level := 64;
      end if;
        
      var_task_name := 'aml_mark_data_'||lower(par_mark_type);
      
      -- ������� task, ���� �� ������� � ����������� �������
      begin dbms_parallel_execute.drop_task(var_task_name); exception when others then null; end;
      -- ������� task
      dbms_parallel_execute.create_task(var_task_name);
      -- ������� chunk'�
      var_chunk_cursor := replace(var_chunk_cursor, '#MARK_TYPE#', par_mark_type);
      var_chunk_cursor := replace(var_chunk_cursor, '#REPAIR_FLAG#', case when par_repair_mode is null
                                                                          then '0'
                                                                          else '1'
                                                                     end);      
      dbms_parallel_execute.create_chunks_by_sql(var_task_name, var_chunk_cursor, false);

      -- ��������� task
      dbms_parallel_execute.run_task(var_task_name, var_chunk_sql, DBMS_SQL.NATIVE, parallel_level => var_parallel_level);

      -- ���������, ��� ��� ����������� ������� ���������, ���� ��� - ���������� ��������� � ���
      var_err_count := std.pkg_log.log_parallel_task_errors('STD', var_task_name, 'FATAL', prcname);

      -- ������� task
      dbms_parallel_execute.drop_task(var_task_name);

      if par_mark_type = 'LOADED' Then
        std.pkg_log.info('��������� �������� ������ ������������ � ��������-�������� (������������� �������� dbms_parallel_execute)', prcname);
      elsif par_mark_type = 'TO_LOAD' Then  
        std.pkg_log.info('��������� �������� ������ � �������� � ��������-�������� (������������� �������� dbms_parallel_execute)', prcname);
      elsif par_mark_type = 'TO_LOAD_BEFORE' Then  
        std.pkg_log.info('��������� �������� ������ � �������� � ��������-�������� ����� ������� �������� (������������� �������� dbms_parallel_execute)', prcname);
      end if;
    end if;
  end if;
  --
  -- ��������� ��������� ��������, ���������� ���������������, � ������ �������������� HARD (������ ��� ���� ��������� LOADED)
  --
  if par_mark_type = 'LOADED' Then
    FOR r IN (SELECT distinct owner, table_name
                FROM std.mark_data_list 
               WHERE mark_type = par_mark_type and
                     done_flag = 0
              ORDER BY 1, 2) LOOP
      
      BEGIN                  
        var_count := repair_table_hard(par_mark_type, r.owner, r.table_name);

        -- ��������� ����� "����������?", "������?" � ��������������� ��������� ������
        UPDATE std.mark_data_list 
           SET done_flag = case when var_count < 0 then 0 else 1 end,
               err_flag = case when err_flag = 1 or var_count < 0 then 1 else 0 end             
         WHERE mark_type = par_mark_type and
               done_flag = 0 and
               owner = r.owner and
               table_name = r.table_name;
        COMMIT;       
      EXCEPTION
        WHEN OTHERS THEN 
          std.pkg_log.fatal('������ ��� ���������� �������������� � ������ HARD - �������: '||r.owner||'.'||r.table_name||
                            '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
      END;  
    END LOOP;
  end if;  
  --
  -- ��������� ������ ���������
  --
  if par_log_id is not null Then
    DELETE FROM std.mark_data_summary WHERE log_id = par_log_id and mark_type = par_mark_type;
    INSERT INTO std.mark_data_summary(log_id, mark_type, owner, table_name, repair_mode, err_flag, 
                                      part_qty, rows_qty, durations_sum)
           (SELECT par_log_id, mark_type, owner, table_name, repair_mode, err_flag,
                   count(*) as part_qty,
                   sum(rows_qty) as rows_qty,
                   sum(extract(day from end_time - start_time)*24*3600 + 
                       extract(hour from end_time - start_time)*3600 + 
                       extract(minute from end_time - start_time)*60 + 
                       extract(second from end_time - start_time)) as durations_sum
              FROM std.mark_data_list
             WHERE mark_type = par_mark_type
            GROUP BY owner, mark_type, table_name, repair_mode, err_flag); 
    COMMIT;                                  
  end if;
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.fatal('������ ��� ��������� ������ ������������/� ��������. par_mark_type: '||par_mark_type||', par_repair_mode: '||par_repair_mode||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
END mark_data;
--
-- �������� ������ ������������/� �������� ��� ������ ������� ������ (������� mark_data_list)
-- ��������! ��������� �� ������������ ������� � ������� �������������� HARD,  �. �. � ���� ������ �������������� ������� �������!
--
PROCEDURE process_mdlist_item
(
  par_reg_id             mark_data_list.reg_id%TYPE,
  par_repair_flag        INTEGER   -- 0 - ������� ���������/�������������� (� ������������ � mark_data_list), 
                                   -- 1 - �������������� �������������� � ������ SOFT
) IS

  var_sql          CLOB;
  var_repair_sql   CLOB;
  var_repair_flag  INTEGER;
  var_mark_type    std.mark_data_list.mark_type%TYPE;
  var_table_name   VARCHAR2(255 CHAR);
  
  var_done_flag    INTEGER := 1;
  var_err_flag     INTEGER := 0;
  var_start_time   TIMESTAMP := SYSTIMESTAMP;
  var_rows_qty     INTEGER;
  var_exec_flag    INTEGER := 0;

  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.process_mdlist_item';
BEGIN
  -- �������� ������� ���������
  if par_reg_id is null or par_repair_flag is null or par_repair_flag not in (0, 1) Then
    std.pkg_log.fatal('����������� ������: �������� �������� ���������. par_reg_id: '||to_char(par_reg_id)||', par_repair_flag: '||to_char(par_repair_flag), prcname);
    goto on_error; 
  end if;   
  
  -- ������� ������ SQL-�������� ���������/��������������, ���� ���������� � ������ �������������� � ������ ����������
  BEGIN
    SELECT replace(replace(
                   case when l.repair_mode = 'HARD' 
                        then to_clob(null)
                        when l.repair_mode = 'SOFT' or par_repair_flag = 1
                        then q.soft_repair_sql
                        else q.mark_sql
                   end,
                   '#SUBPARTITION_NAME#', l.subpartition_name),
                   '#PARTITION_NAME#',    l.partition_name)  as sql_text, 
           replace(replace(
                   case when l.repair_mode is not null or par_repair_flag <> 0
                        then to_clob(null) -- �� ������ �������������� ��� ������� �� ��������������
                        else q.soft_repair_sql -- ������������� ������ SOFT-�������������� ��� ������� �� ������� ���������
                   end,
                   '#SUBPARTITION_NAME#', l.subpartition_name),
                   '#PARTITION_NAME#',    l.partition_name) as repair_sql, 
           case when l.repair_mode = 'HARD' 
                then 2
                when l.repair_mode = 'SOFT' or par_repair_flag = 1
                then 1
                else 0
           end as repair_flag,
           l.mark_type,
           l.owner||'.'||l.table_name||case when l.subpartition_name is not null
                                            then ' (����������� '||l.subpartition_name||')'
                                            when l.partition_name is not null
                                            then ' (�������� '||l.partition_name||')'
                                       end     
      INTO var_sql, var_repair_sql, var_repair_flag, var_mark_type, var_table_name               
      FROM std.mark_data_list l
           join std.mark_data_queries q on q.mark_type = l.mark_type and
                                           q.owner = l.owner and
                                           q.table_name = l.table_name
     WHERE l.reg_id = par_reg_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      std.pkg_log.fatal('����������� ������: ������� �������������� ����� ������� � ������ std.mark_data_list - par_reg_id: '||to_char(par_reg_id), prcname);
      goto on_error; 
  END;
  
  if var_repair_flag = 2 Then
    std.pkg_log.fatal('����������� ������: ����� ��� ������� � ������������� ������� �������������� HARD � ������ std.mark_data_list - reg_id: '||to_char(par_reg_id)||', mark_type: '||var_mark_type||', �������: '||var_table_name, prcname);
    goto on_error; 
  end if;   

  if var_repair_flag = 0 and (var_sql is null or length(var_sql) = 0) Then
    std.pkg_log.fatal('����������� ������: ������� ������ ����� ������� ��� ��������� ������ ������������/� �������� - reg_id: '||to_char(par_reg_id)||', mark_type: '||var_mark_type||', �������: '||var_table_name, prcname);
    goto on_error; 
  end if;
    
  -- ��������� ������  
  if var_sql is not null and length(var_sql) > 0 Then
    BEGIN
      var_exec_flag := 1;
      EXECUTE IMMEDIATE var_sql USING OUT var_rows_qty;
    EXCEPTION
      WHEN OTHERS THEN
        var_done_flag := 0;
        var_err_flag := 1;     
        var_rows_qty := null;    
        
        if var_repair_flag = 0 Then
          std.pkg_log.fatal('������ ��� ���������� ������� ��������� ������ ������������/� �������� - reg_id: '||to_char(par_reg_id)||', mark_type: '||var_mark_type||', �������: '||var_table_name||
                            '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. ����� ������� ��. � ��������� ������� ���������', prcname);
        else
          std.pkg_log.fatal('������ ��� ���������� ������� �������������� (������ ��������� ������ ������������) - reg_id: '||to_char(par_reg_id)||', mark_type: '||var_mark_type||', �������: '||var_table_name||
                            '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. ����� ������� ��. � ��������� ������� ���������', prcname);
        end if; 
        -- ������� � ��� ������������� ������                                     
        for i in 1..trunc(length(var_sql)/2000) + 1 loop
          std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);      
        end loop;         
    END;
  end if;  
  
  -- ���� ���� ������, ��� �� ����� �������������� � ���� ������ �������������� - �������� ���
  if var_err_flag = 1 and var_repair_flag = 0 and var_repair_sql is not null and length(var_repair_sql) > 0 Then
    BEGIN
      var_exec_flag := 1;
      std.pkg_log.info('� ����� � ������� ��� ���������� ������� ��������� ������ ������������ ��������� ������ �������������� - reg_id: '||to_char(par_reg_id)||', mark_type: '||var_mark_type||', �������: '||var_table_name, prcname);
      
      EXECUTE IMMEDIATE var_repair_sql USING OUT var_rows_qty;
      
      var_done_flag := 1;
    EXCEPTION
      WHEN OTHERS THEN
        var_done_flag := 0;
        var_rows_qty := null;    
        std.pkg_log.fatal('������ ��� ���������� ������� �������������� (� ����� � ������� ��� ���������� ������� ��������� ������ ������������) - reg_id: '||to_char(par_reg_id)||', mark_type: '||var_mark_type||', �������: '||var_table_name||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. ����� ������� ��. � ��������� ������� ���������', prcname);
        -- ������� � ��� ������������� ������                                     
        for i in 1..trunc(length(var_repair_sql)/2000) + 1 loop
          std.pkg_log.info(substr(var_repair_sql, (i - 1)*2000 + 1, 2000), prcname);      
        end loop; 
    END;
  end if;  
  
  -- ��������� ���������  
  UPDATE std.mark_data_list
     SET done_flag  = var_done_flag,
         err_flag   = var_err_flag,
         start_time = case when var_exec_flag = 1 then var_start_time end,
         end_time   = case when var_exec_flag = 1 then SYSTIMESTAMP   end,
         rows_qty   = case when var_exec_flag = 1 then var_rows_qty   end
   WHERE reg_id = par_reg_id;
  COMMIT;
  
  -- �������
  return;
  
  <<on_error>>          
  UPDATE std.mark_data_list
     SET done_flag  = 0,
         err_flag   = 1,
         start_time = null,
         end_time   = null,
         rows_qty   = null
   WHERE reg_id = par_reg_id;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.fatal('������ ��� ��������� �������� ������ std.mark_data_list - par_reg_id: '||to_char(par_reg_id)||', par_repair_flag: '||to_char(par_repair_flag)||', mark_type: '||var_mark_type||', �������: '||var_table_name||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
    BEGIN
      UPDATE std.mark_data_list
         SET done_flag  = 0,
             err_flag   = 1
       WHERE reg_id = par_reg_id;
      COMMIT;
    EXCEPTION 
      WHEN OTHERS THEN 
        null; 
    END;  
END process_mdlist_item;
--
-- ��������� �������������� � ������ HARD ��� ��������� �������
--
FUNCTION repair_table_hard
(
  par_mark_type          mark_data_queries.mark_type%TYPE,  -- ���� ������ LOADED - �������� ������ ������������
  par_owner              mark_data_queries.owner%TYPE,
  par_table_name         mark_data_queries.table_name%TYPE
) 
RETURN INTEGER IS -- ���������� ���������� ����� (null, ���� �������������� �� �����������, -1 ���� �������� ������)
  var_sql          CLOB;  
  var_rows_qty     INTEGER;

  prcname   VARCHAR2(64 CHAR) := 'std.pkg_load.repair_table_hard';
BEGIN
  -- �������� ������� ���������
  if par_mark_type is null or par_mark_type not in ('LOADED') or par_owner is null or par_table_name is null Then
    std.pkg_log.fatal('����������� ������: �������� �������� ���������. par_mark_type: '||par_mark_type||', par_owner: '||par_owner||', par_table_name: '||par_table_name, prcname);
    return -1; 
  end if;   

  -- ������� ����� SQL-�������
  BEGIN
    SELECT hard_repair_sql
      INTO var_sql               
      FROM std.mark_data_queries 
     WHERE mark_type = par_mark_type and
           owner = par_owner and
           table_name = par_table_name;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      std.pkg_log.fatal('����������� ������: ������� �������������� � ������ std.mark_data_queries ������� - par_mark_type: '||par_mark_type||', par_owner: '||par_owner||', par_table_name: '||par_table_name, prcname);
      return -1; 
  END;

  -- ��������� ������  
  if var_sql is not null and length(var_sql) > 0 Then
    std.pkg_log.info('��������� ������ �������������� � ������ HARD - �������: '||par_owner||'.'||par_table_name, prcname);

    BEGIN
      EXECUTE IMMEDIATE var_sql USING OUT var_rows_qty;
    EXCEPTION
      WHEN OTHERS THEN
        var_rows_qty := -1;    
        
        std.pkg_log.fatal('������ ��� ���������� ������� �������������� � ������ HARD - �������: '||par_owner||'.'||par_table_name||
                          '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM||'. ����� ������� ��. � ��������� ������� ���������', prcname);
        -- ������� � ��� ������������� ������                                     
        for i in 1..trunc(length(var_sql)/2000) + 1 loop
          std.pkg_log.info(substr(var_sql, (i - 1)*2000 + 1, 2000), prcname);      
        end loop;         
    END;
    std.pkg_log.info('��������� ��������� ������ �������������� � ������ HARD - �������: '||par_owner||'.'||par_table_name||
                     ', �������� �����: '||to_char(nullif(var_rows_qty, -1)), prcname);
  end if;  
  
  return var_rows_qty;  
EXCEPTION
  WHEN OTHERS THEN
    std.pkg_log.fatal('������ ��� ���������� �������������� � ������ HARD - �������: '||par_owner||'.'||par_table_name||
                      '. ����� ������: '||dbms_utility.format_error_backtrace||' '||SQLERRM, prcname);
END repair_table_hard;
END pkg_load;
/

grant execute on std.pkg_load to data_loader;
grant execute on std.pkg_load to business with grant option;
--grant execute on std.pkg_load to amladm with grant option;
grant execute on std.pkg_load to mantas with grant option;
grant execute on std.pkg_load to rcod038deposit with grant option;
grant execute on std.pkg_load to rcod038integrator with grant option;
grant execute on std.pkg_load to rcod038nsi with grant option;
grant execute on std.pkg_load to rcod038client with grant option;
grant execute on std.pkg_load to reks000ibs with grant option;
grant execute on std.pkg_load to rgat000gate with grant option;
grant execute on std.pkg_load to rifb000main with grant option;
grant execute on std.pkg_load to rmdc000mdm with grant option;
grant execute on std.pkg_load to rmdr000mdm with grant option;
grant execute on std.pkg_load to rnar000od with grant option;
grant execute on std.pkg_load to rnav000od with grant option;
grant execute on std.pkg_load to rstl000stoplist with grant option;
grant execute on std.pkg_load to rway000way4 with grant option;



