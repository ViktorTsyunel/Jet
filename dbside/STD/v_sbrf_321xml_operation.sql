create or replace force view std.v_sbrf_321xml_operation as

select x.file_id
       , x.file_nm
       , x_ip.operation_id
       , x_ip.action
       , x_ip.numb_p
       , x_ip.date_p
       , x_ip.branch
       , x_ip.numbf_ss
       , x_ip.terror
       , x_ip.vo
       , x_ip.dop_v
       , x_ip.data
       , x_ip.sume
       , x_ip.sum
       , x_ip.curren
       , x_ip.prim
       , x_ip.num_pay_d
       , x_ip.date_pay_d
       , x_ip.metal
       , x_ip.priz6001
       , x_ip.b_payer
       , x_ip.b_recip
       , x_ip.part
       , x_ip.descr
       , x_ip.curren_con
       , x_ip.sum_con
       , x_ip.priz_sd
       , x_ip.date_s
       , x_ip.info_part 
  from std.imp_sbrf_321xml x
  , xmltable(xmlnamespaces('fns.esb.sbrf.ru' as "f"), 'f:MAIN/f:INFO_PART' 
             passing x.file_body
             columns 
               info_part xmltype path '/'
             , operation_id for ordinality
             , action     varchar2(2000 char) path 'f:TYPE_INFO/f:ACTION'
             , numb_p     varchar2(2000 char) path 'f:TYPE_INFO/f:NUMB_P' 
             , date_p     varchar2(2000 char) path 'f:TYPE_INFO/f:DATE_P'
             , branch     varchar2(2000 char) path 'f:KO_INFO/f:BRANCH'
             , numbf_ss   varchar2(2000 char) path 'f:KO_INFO/f:NUMBF_SS'
             , terror     varchar2(2000 char) path 'f:OPER_INFO/f:TERROR'
             , vo         varchar2(2000 char) path 'f:OPER_INFO/f:VO'
             , dop_v      varchar2(2000 char) path 'f:OPER_INFO/f:DOP_V'
             , data       varchar2(2000 char) path 'f:OPER_INFO/f:DATA'
             , sume       varchar2(2000 char) path 'f:OPER_INFO/f:SUME'
             , sum        varchar2(2000 char) path 'f:OPER_INFO/f:SUM'
             , curren     varchar2(2000 char) path 'f:OPER_INFO/f:CURREN'
             , prim       varchar2(2000 char) path 'f:OPER_INFO/f:PRIM'
             , num_pay_d  varchar2(2000 char) path 'f:OPER_INFO/f:NUM_PAY_D'
             , date_pay_d varchar2(2000 char) path 'f:OPER_INFO/f:DATE_PAY_D'
             , metal      varchar2(2000 char) path 'f:OPER_INFO/f:METAL'
             , priz6001   varchar2(2000 char) path 'f:OPER_INFO/f:PRIZ6001'
             , b_payer    varchar2(2000 char) path 'f:OPER_INFO/f:B_PAYER'
             , b_recip    varchar2(2000 char) path 'f:OPER_INFO/f:B_RECIP'
             , part       varchar2(2000 char) path 'f:OPER_INFO/f:PART'
             , descr      varchar2(2000 char) path 'f:OPER_INFO/f:DESCR'
             , curren_con varchar2(2000 char) path 'f:OPER_INFO/f:CURREN_CON'
             , sum_con    varchar2(2000 char) path 'f:OPER_INFO/f:SUM_CON'
             , priz_sd    varchar2(2000 char) path 'f:OPER_INFO/f:PRIZ_SD'
             , date_s     varchar2(2000 char) path 'f:OPER_INFO/f:DATE_S'
            )(+) x_ip;

show errors;

grant select on std.v_sbrf_321xml_operation to mantas;