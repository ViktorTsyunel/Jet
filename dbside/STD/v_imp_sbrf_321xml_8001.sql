  CREATE OR REPLACE FORCE VIEW STD.V_IMP_SBRF_321XML_8001 ( FILE_ID, FILE_NM, ARCHIVE_NM, ERROR_LOADING_FL, ERR_MSG_TX, FILE_BODY, DATA_DT, ORDER_NB, SYS_CD, DPD_CD ) AS 
  select x.file_id
         , x.file_nm
         , x.archive_nm
         , x.error_loading_fl
         , x.err_msg_tx
         , x.file_body
         , w.data_dt
         , w.order_nb
         , 97 sys_cd
         , w.dpd_cd
  from std.imp_sbrf_321xml_8001 x
  , xmltable('document' 
             passing x.file_body
             columns 
               data_dt varchar2(2000 char) path '//fieldlist/field[@name="DATA"]'
             , order_nb varchar2(2000 char) path '@id'
             , dpd_cd varchar2(2000 char) path '//fieldlist/field[@name="DEPT_NAME"]'
            )(+) w;
 GRANT SELECT on std.v_imp_sbrf_321xml_8001 to MANTAS;
