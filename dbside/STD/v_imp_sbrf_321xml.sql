create or replace force view std.v_imp_sbrf_321xml as 

select x.file_id
       , x.file_nm
       , x.archive_nm
       , x.error_loading_fl
       , x.err_msg_tx
       , x.file_body
       , w.*
  from std.imp_sbrf_321xml x
  , xmltable(xmlnamespaces('fns.esb.sbrf.ru' as "f"), 'f:MAIN' 
             passing x.file_body
             columns 
               data_dt varchar2(2000 char) path 'f:WORK/f:DATE'
             , order_nb varchar2(2000 char) path 'f:WORK/f:MESSAGEID'
             , sys_cd varchar2(2000 char) path 'f:WORK/f:ASYSTEM'
             , dpd_cd varchar2(2000 char) path 'f:WORK/f:DEPARTMENT'
            )(+) w;

show errors;

grant select on std.v_imp_sbrf_321xml to mantas;