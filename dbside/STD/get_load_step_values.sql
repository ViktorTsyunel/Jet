GRANT SELECT ON rgat000gate.loading_files TO STD;

CREATE OR REPLACE SYNONYM std.opb_task_inst_run FOR infa_rep.opb_task_inst_run;
GRANT SELECT ON std.opb_task_inst_run TO STD;

CREATE OR REPLACE SYNONYM std.opb_sess_task_log FOR infa_rep.opb_sess_task_log;
GRANT SELECT ON std.opb_sess_task_log TO STD;

CREATE OR REPLACE SYNONYM std.opb_swidginst_log FOR infa_rep.opb_swidginst_log;
GRANT SELECT ON std.opb_swidginst_log TO STD;

CREATE OR REPLACE SYNONYM std.opb_key_range     FOR infa_rep.opb_key_range;
GRANT SELECT ON std.opb_key_range TO STD;

begin EXECUTE IMMEDIATE('DROP TYPE std.TAB_LOAD_STEP_VALUES'); exception when others then null; end;
/

CREATE OR REPLACE TYPE std.TYPE_LOAD_STEP_VALUES AS OBJECT 
(
   LOG_ID               NUMBER(22),
   LOAD_DATE            DATE,
   START_TIME           DATE,
   END_TIME             DATE,
   STEP_VERSION_NB      INTEGER,
   STEP_CD              VARCHAR2(64 CHAR),
   STEP_STATUS_CD       VARCHAR2(64 CHAR),
   SRC_SYS_CD           VARCHAR2(3 CHAR),
   TB_ID                INTEGER,
   OP_CAT_CD            VARCHAR2(4 CHAR),
   TABLE_NM             VARCHAR2(255 CHAR),
   INSTANCE_NM          VARCHAR2(255 CHAR),
   PARTITION_ID         INTEGER,
   TRXN_COUNT           INTEGER,
   TRXN_SCRTY_COUNT     INTEGER,
   ROWS_COUNT           INTEGER,
   ALERT_CREATE_COUNT   INTEGER,
   TRXN_REPEAT_COUNT    INTEGER,
   ALERT_REPEAT_COUNT   INTEGER,
   ALERT_DELETE_COUNT   INTEGER,
   FILES_COUNT          INTEGER,
   FILES_SIZE           NUMBER,
   FILES_READ_ROWS_COUNT INTEGER,
   FILES_TRXN_COUNT     INTEGER,
   FILES_TRXN_SCRTY_COUNT INTEGER,
   FILES_ROWS_COUNT     INTEGER,
   ERRORS_COUNT         INTEGER,
   ERROR_MSG            CLOB,
   INFA_FULL_TASK_NAME  VARCHAR2(2000 CHAR),
   INFA_RUN_STATUS_CODE INTEGER
)
/

CREATE OR REPLACE TYPE std.TAB_LOAD_STEP_VALUES AS TABLE OF std.TYPE_LOAD_STEP_VALUES
/
--
-- ������� �������, ���������� ������� ���������� � �������� ���������� ��� ������ ���������� �������� ��������/���������
-- ������� ��������/��������� ���������������� ID ���������
-- ������� ���������� ���� ��������������� ������ �� ������� STD.LOAD_STEP_VALUES, ���� ������ �� ����� ����������� � ��������� ��������
-- (��. �������� ������� par_source)
-- 
CREATE OR REPLACE 
FUNCTION std.get_load_step_values
(
  par_log_id         logs.log_id%TYPE, -- ID ��������� ��������
  par_source         VARCHAR2 DEFAULT 'AUTO', -- ������ �������� ������:
                                              -- LOG - �������� ������ �� ����� ����������� � ��������� ��������
                                              -- TAB - �������� ������ �� ������� STD.LOAD_STEP_VALUES
                                              -- AUTO - ���������� �������� �������������: ���� ������� �������� (�������� ������) � 
                                              --        � STD.LOAD_STEP_VALUES ���� ��� ���� ������, �� TAB, ����� - LOG
  par_full_load_fl   INTEGER DEFAULT 0 -- ���� 0/1, ��� ����� ����������� �� ��������� ���������� ��������, ����� ��������� �������� (�� ����� ��������� ���������)
                                       -- ���� �� ����������, �� ����� �� ����� ����� ������ RUNNING
)
return TAB_LOAD_STEP_VALUES PIPELINED is
  var_source           VARCHAR2(4 CHAR);
  var_load_date        DATE;
  var_step_version_nb  load_steps.step_version_nb%TYPE;
  var_workflow_run_id  NUMBER;
  
  var_row     std.TYPE_LOAD_STEP_VALUES := std.TYPE_LOAD_STEP_VALUES(null,null,null,null,null,null,null,null,null,null,
                                                                     null,null,null,null,null,null,null,null,null,null,
                                                                     null,null,null,null,null,null,null,null,null,null);

  prcname     VARCHAR2(32 CHAR) := ' (std.get_load_step_values)';
BEGIN
  --
  -- �������� ���������
  --
  if par_log_id is null or par_source is null or par_source not in ('LOG', 'TAB', 'AUTO') or
     par_full_load_fl is null or par_full_load_fl not in (1, 0) Then
    raise_application_error(-20001, '�������� �������� ���������'||prcname);
  end if;  
  --
  -- ��������� �������� ������
  --
  if par_source = 'AUTO' Then    
    SELECT case when max(l.closed_flag) = 1 and max(lsv.log_id) is not null -- �������� ������, � ������� ���� ������
                then 'TAB'
                when max(l.log_id) is not null -- ��������� �������� ����������
                then 'LOG'    
           end,
           max(trunc(l.open_time)),
           max(l.workflow_run_id)       
      INTO var_source, var_load_date, var_workflow_run_id
      FROM std.logs l
           left join std.load_step_values lsv on lsv.log_id = l.log_id
     WHERE l.log_id = par_log_id and
           rownum <= 1; 
           
    if var_source is null Then
      raise_application_error(-20001, '������ �������������� log_id: '||to_char(par_log_id)||prcname);
    end if;         
  else 
    var_source := par_source;
  end if;  
  --
  -- � ����������� �� ��������� ������� ������ � ������������ ���������� ������ ��������/���������
  --
  if var_source = 'LOG' Then
    --
    -- ��������� ������� (���������) ������ �������� ������ ��������/���������
    --
    SELECT max(step_version_nb)
      INTO var_step_version_nb
      FROM std.load_steps;  
    --
    -- ��������� ���� �������� � ID ������� �����������(���� ��� ����� �� �������)
    --  
    if var_load_date is null or var_workflow_run_id is null Then
      SELECT max(trunc(open_time)),
             max(workflow_run_id)       
        INTO var_load_date, var_workflow_run_id
        FROM std.logs l
       WHERE l.log_id = par_log_id; 
           
      if var_load_date is null Then
        raise_application_error(-20001, '������ �������������� log_id: '||to_char(par_log_id)||prcname);
      end if;         
    end if; 
    --
    -- �� ������ ������: ���� �� ������� ���������� workflow_run_id ����������� - ��������� ����� ��� � ������� ���������
    --
    if var_workflow_run_id is null Then
      SELECT max(to_number(log_text))
        INTO var_workflow_run_id
        FROM std.log_items
       WHERE log_id = par_log_id and 
             log_orig = 'WORKFLOW_RUN_ID' and
             rownum <= 1;     
    end if;         
    --
    -- ���� �� ������� �� �����
    --
    FOR r IN (SELECT start_time, 
                     nullif(end_time, to_date('01.01.1753', 'dd.mm.yyyy')) as end_time,
                     step_cd,
                     step_status_cd,
                     src_sys_cd, 
                     tb_id,
                     op_cat_cd,
                     target_name as table_nm,
                     instance_name as instance_nm,
                     partition_id,
                     case when unit = 'TRXN'       and dest_type = 'TARGET' then rows_count end as trxn_count,
                     case when unit = 'TRXN_SCRTY' and dest_type = 'TARGET' then rows_count end as trxn_scrty_count,
                     case when unit = 'ROWS'       and dest_type = 'TARGET' then rows_count end as rows_count,
                     to_number(null) as alert_create_count,  
                     to_number(null) as trxn_repeat_count,  
                     to_number(null) as alert_repeat_count,  
                     to_number(null) as alert_delete_count,
                     files_count,
                     files_size,
                     files_read_rows_count,
                     case when unit = 'TRXN'       and dest_type = 'REPLICA' then rows_count end as files_trxn_count,
                     case when unit = 'TRXN_SCRTY' and dest_type = 'REPLICA' then rows_count end as files_trxn_scrty_count,
                     case when unit = 'ROWS'       and dest_type = 'REPLICA' then rows_count end as files_rows_count,
                     err_rows_count as errors_count,
                     run_err_msg as error_msg,
                     full_task_name as infa_full_task_name,
                     run_status_code as infa_run_status_code  
                FROM (-- ��������� ����������� ����������� �������� �������� �� �����������
                      SELECT t.*,
                             case when t.full_task_name like '%RCOD%' then 'BCK'
                                  when t.full_task_name like '%REKS%' then 'EKS'
                                  when t.full_task_name like '%RGAT%' then 'GAT'
                                  when t.full_task_name like '%RNAV%' then 'NAV'
                                  when t.full_task_name like '%RNAR%' then 'NAR'
                                  when t.full_task_name like '%RWAY%' then 'WAY'
                                  when t.full_task_name like '%RIFB%' then 'IFB'
                                  when t.full_task_name like '%RMDC%' then 'MDC'
                                  when t.full_task_name like '%RMDR%' then 'MDR'
                                  when t.full_task_name like '%RSTL%' then 'STL'              
                              end as src_sys_cd,
                              case when t.full_task_name like '%RCOD%' 
                                   then to_number(substr(regexp_substr(t.full_task_name, 'RCOD_\d+_\d{3}'), -3))
                                   when t.full_task_name like '%RGAT%' and t.full_task_name like '%COPY%'
                                   then to_number(substr(regexp_substr(t.full_task_name, 'COPY_\d{3}'), -3))               
                                   else t.tb_id_0  
                              end as tb_id,
                              case when target_name like '%TRXN_SCRTY%' then 'TRXN_SCRTY' 
                                   when target_name like '%TRXN%'       then 'TRXN'
                                   when target_name = 'OPERATIONS'      then 'TRXN'             
                                   else 'ROWS'    
                              end as unit,
                              case when target_name like '%WIRE_TRXN%' then 'WT' 
                                   when target_name like '%CASH_TRXN%' then 'CT'
                                   when target_name like '%BACK_OFFICE_TRXN%' then 'BOT'
                              end as op_cat_cd,             
                              case when t.full_task_name like '%LOAD%FILES%RGAT%'
                                   then 'REPLICA'
                                   else 'TARGET'
                              end as dest_type,
                              lsit.step_cd,
                              case when t.run_status_code in (3 /*Failed*/)
                                   then 'FATAL'
                                   when t.err_rows_count > 0 or run_err_msg is not null
                                   then 'ERROR'
                                   when 1 <> 1 -- ������ ������� �� ������ ����������� ����������� �� ������
                                   then 'WARN'
                                   when t.run_status_code in (1 /*Suceeded*/)
                                   then 'SUCCESS'
                                   when t.run_status_code in (2 /*Disabled*/)
                                   then 'DISABLED'
                                   when t.run_status_code in (6 /*Running*/, 11 /*Waiting*/) and par_full_load_fl <> 1
                                   then 'RUNNING'
                                   when t.run_status_code in (4 /*Stopped*/, 5 /*Aborted*/, 7 /*Suspending*/, 8/*Suspended*/, 9 /*Stopping*/, 10 /*Aborting*/, 15 /*Terminated*/)
                                   then 'ABORTED'  
                                   when t.run_status_code in (12 /*Scheduled*/, 13 /*Unscheduled*/, 14 /*Unknown*/)
                                   then to_char(null) -- ���� ��� �� ����������, ������ ����������
                              end as step_status_cd      
                        FROM (-- �������� ����� - ����������� � ��������� ������/���������, ������������/�������� ������, �� ��� ��������� �������, ��������, ������� � �. �.
                              SELECT tir.full_task_name,
                                     tir.start_time,
                                     tir.end_time,
                                     tir.run_status_code,
                                     tir.run_err_msg,
                                     to_number(null) as rows_count,
                                     to_number(null) as err_rows_count,
                                     lf.files_count as files_count,
                                     lf.files_size as files_size,
                                     to_number(null) as files_read_rows_count,
                                     to_char(null) as target_name,
                                     to_char(null) as instance_name,
                                     to_number(null) as partition_id,
                                     to_number(null) as tb_id_0
                                FROM (select upper(substr(SYS_CONNECT_BY_PATH(instance_name, '|'), 2)) as full_task_name, t.*
                                        from std.opb_task_inst_run t
                                      start with workflow_run_id = var_workflow_run_id and worklet_run_id = 0
                                      connect by worklet_run_id = prior nullif(child_run_id, 0)) tir
                                     left join (select id_mega as tb_id, count(*) as files_count, sum(file_size)*512 as files_size  
                                                  from rgat000gate.loading_files
                                                 where wf_run_id = var_workflow_run_id 
                                                group by id_mega) lf on tir.full_task_name like '%RGAT%' and tir.full_task_name like '%COPY%' and
                                                                        lf.tb_id = to_number(substr(regexp_substr(tir.full_task_name, 'COPY_\d{3}'), -3))
                              UNION ALL
                              -- �������� �����/�������� � ��������������� ���������������� (��������, ������, ������)
                              SELECT tir.full_task_name,
                                     swl.start_time as start_time,
                                     swl.end_time as end_time,
                                     to_number(null) as run_status_code,
                                     case when lower(swl.last_err_msg) not like 'no errors%'
                                          then swl.last_err_msg
                                     end as run_err_msg,
                                     case when swl.rowid is not null and swl.widget_type = 2 -- ������ � ������
                                          then swl.affected_rows 
                                          when swl.rowid is null  
                                          then stl.targ_success_rows 
                                     end as rows_count,
                                     case when swl.rowid is not null and swl.widget_type = 2  
                                          then swl.rejected_rows 
                                          when swl.rowid is null  
                                          then stl.targ_failed_rows
                                     end as err_rows_count,
                                     to_number(null) as files_count,
                                     to_number(null) as files_size,
                                     case when swl.widget_type = 3  -- ������ �� ��������� (����� �� ����)
                                          then swl.affected_rows
                                     end as files_read_rows_count,
                                     swl.widget_name as target_name,
                                     swl.instance_name,
                                     swl.partition_id as partition_id,
                                     to_number(substr(kr.start_range, 1, 2)) as tb_id_0
                                FROM (select upper(substr(SYS_CONNECT_BY_PATH(instance_name, '|'), 2)) as full_task_name, t.*
                                        from std.opb_task_inst_run t
                                      start with workflow_run_id = var_workflow_run_id and worklet_run_id = 0
                                      connect by worklet_run_id = prior nullif(child_run_id, 0)) tir
                                     left join std.opb_sess_task_log stl on stl.workflow_run_id = tir.workflow_run_id and
                                                                            stl.worklet_run_id = tir.worklet_run_id and
                                                                            stl.instance_id = tir.instance_id  
                                     left join std.opb_swidginst_log swl on swl.workflow_run_id = tir.workflow_run_id and
                                                                            swl.worklet_run_id = tir.worklet_run_id and
                                                                            swl.task_instance_id = tir.instance_id and
                                                                            case when swl.widget_type = 2
                                                                                 then 1
                                                                                 when swl.widget_type = 3 and 
                                                                                      tir.full_task_name like '%LOAD%FILES%RGAT%' and -- �������� ������ �� ����
                                                                                      swl.widget_name = 'SQ_OPER'
                                                                                 then 1
                                                                            end = 1            
                                     left join std.opb_swidginst_log swl_src on swl_src.workflow_run_id = swl.workflow_run_id and
                                                                                swl_src.worklet_run_id = swl.worklet_run_id and
                                                                                swl_src.task_instance_id = swl.task_instance_id and
                                                                                swl_src.session_id = swl.session_id and
                                                                                swl_src.partition_id = swl.partition_id and
                                                                                swl_src.widget_type = 3 and -- ������
                                                                                swl_src.instance_name in ('SQ_V_TRXN', 'SQ_V_CASH_TRXN', 'SQ_V_SCRTY_TO_TRXN') -- �������� ��� � �� ���� - ��� �� ����� �������� ������ �� ��������
                                     left join std.opb_key_range kr on kr.session_id = swl_src.session_id and
                                                                       kr.sess_widg_inst_id = swl_src.sesswidg_inst_id and
                                                                       kr.partition_id = swl_src.partition_id and
                                                                       kr.version_number = tir.version_number                
                               WHERE (stl.rowid is not null or swl.rowid is not null)) t
                             left join std.load_step_infa_tasks lsit on lsit.infa_full_task_name = case when t.full_task_name like 'WL_LOAD_REKS_EXT%' 
                                                                                                        then 'WL_LOAD_REKS_EXT' -- ������ ������������ ������ �� ���� ����
                                                                                                        else t.full_task_name
                                                                                                   end and
                                                                        lsit.step_version_nb = var_step_version_nb 
                       WHERE nvl(lsit.ignore_fl, 0) <> 1)
              UNION ALL 
              SELECT case when min(li.log_item_id) = max(li.log_item_id) -- ������ ���� ������ � ��������� ��� ����� - ���� ������, ���� �����
                          then case when max(case when li.log_text like lsli.log_text_end_like then li.log_time end) is null -- �� �����, ������ - ������
                                    then min(log_time)
                               end       
                          else min(log_time)
                     end as start_time,       
                     max(case when li.log_text like lsli.log_text_end_like then li.log_time end) as end_time,
                     lsli.step_cd, 
                     case when (l.closed_flag = 0 and par_full_load_fl <> 1) and                                   -- ������� ����, 
                               max(case when li.log_text like lsli.log_text_end_like then li.log_time end) is null -- ���� �� ����������
                          then case when min(log_time) is not null                                                 -- ��� ���� ���� ��� �������
                                    then 'RUNNING'  
                                    else '' -- ������� ��� �� ����������
                               end         
                          when sum(decode(li.log_level, 'FATAL', 1)) > 0
                          then 'FATAL'
                          when sum(decode(li.log_level, 'ERROR', 1)) > 0
                          then 'ERROR'
                          when sum(decode(li.log_level, 'WARN', 1)) > 0
                          then 'WARN'
                          else 'SUCCESS'
                     end as step_status_cd,
                     to_char(null) as src_sys_cd,
                     to_number(null) as tb_id,
                     to_char(null) as op_cat_cd,
                     to_char(null) as table_nm,
                     to_char(null) as instance_name,
                     to_number(null) as partition_id,
                     sum(case when lsli.trxn_count_expr is not null
                              then std.aml_tools.get_number_from_text(li.log_text, lsli.trxn_count_expr)
                         end) as trxn_count,
                     sum(case when lsli.trxn_scrty_count_expr is not null
                              then std.aml_tools.get_number_from_text(li.log_text, lsli.trxn_scrty_count_expr)
                         end) as trxn_scrty_count,
                     sum(case when lsli.rows_count_expr is not null
                              then std.aml_tools.get_number_from_text(li.log_text, lsli.rows_count_expr)
                         end) as rows_count,
                     sum(case when lsli.alert_create_count_expr is not null
                              then std.aml_tools.get_number_from_text(li.log_text, lsli.alert_create_count_expr)
                         end) as alert_create_count,
                     sum(case when lsli.alert_repeat_count_expr is not null
                              then std.aml_tools.get_number_from_text(li.log_text, lsli.alert_repeat_count_expr)
                         end) as alert_repeat_count,
                     sum(case when lsli.trxn_repeat_count_expr is not null
                              then std.aml_tools.get_number_from_text(li.log_text, lsli.trxn_repeat_count_expr)
                         end) as trxn_repeat_count,
                     sum(case when lsli.alert_delete_count_expr is not null
                              then std.aml_tools.get_number_from_text(li.log_text, lsli.alert_delete_count_expr)
                         end) as alert_delete_count,
                     to_number(null) as files_count,
                     to_number(null) as files_size,
                     to_number(null) as files_read_rows_count,
                     to_number(null) as files_trxn_count,
                     to_number(null) as files_trxn_scrty_count,
                     to_number(null) as files_rows_count,
                     sum(case when li.log_level in ('FATAL', 'ERROR') then 1 end) as errors_count,
                     to_char(null) as error_msg,
                     to_char(null) as infa_full_task_name,
                     to_number(null) as infa_run_status_code
                FROM std.load_step_log_items lsli
                     join std.log_items li on li.log_id = par_log_id and li.log_text like lsli.log_text_like
                     join std.logs l on l.log_id = par_log_id 
                     left join std.load_steps ls on ls.step_cd = lsli.step_cd
               WHERE lsli.step_version_nb = var_step_version_nb 
              GROUP BY lsli.step_cd, ls.step_nm, l.closed_flag) LOOP          

      var_row.log_id                 := par_log_id;
      var_row.load_date              := var_load_date;
      var_row.start_time             := r.start_time;
      var_row.end_time               := r.end_time;
      var_row.step_version_nb        := var_step_version_nb;
      var_row.step_cd                := r.step_cd;
      var_row.step_status_cd         := r.step_status_cd;
      var_row.src_sys_cd             := r.src_sys_cd;
      var_row.tb_id                  := r.tb_id;
      var_row.op_cat_cd              := r.op_cat_cd;
      var_row.table_nm               := r.table_nm;
      var_row.instance_nm            := r.instance_nm;
      var_row.partition_id           := r.partition_id;
      var_row.trxn_count             := r.trxn_count;
      var_row.trxn_scrty_count       := r.trxn_scrty_count;
      var_row.rows_count             := r.rows_count;
      var_row.alert_create_count     := r.alert_create_count;
      var_row.trxn_repeat_count      := r.trxn_repeat_count;
      var_row.alert_repeat_count     := r.alert_repeat_count;
      var_row.alert_delete_count     := r.alert_delete_count;
      var_row.files_count            := r.files_count;
      var_row.files_size             := r.files_size;
      var_row.files_read_rows_count  := r.files_read_rows_count;
      var_row.files_trxn_count       := r.files_trxn_count;
      var_row.files_trxn_scrty_count := r.files_trxn_scrty_count;
      var_row.files_rows_count       := r.files_rows_count;
      var_row.errors_count           := r.errors_count;
      var_row.error_msg              := r.error_msg;
      var_row.infa_full_task_name    := r.infa_full_task_name;
      var_row.infa_run_status_code   := r.infa_run_status_code;
      
      PIPE ROW(var_row);
    END LOOP;
  elsif var_source = 'TAB' Then
    --
    -- ���� �� ������� �� �������
    --
    FOR r IN (SELECT * 
                FROM std.load_step_values
               WHERE log_id = par_log_id) LOOP 
      var_row.log_id                 := par_log_id;
      var_row.load_date              := r.load_date;
      var_row.start_time             := r.start_time;
      var_row.end_time               := r.end_time;
      var_row.step_version_nb        := r.step_version_nb;
      var_row.step_cd                := r.step_cd;
      var_row.step_status_cd         := r.step_status_cd;
      var_row.src_sys_cd             := r.src_sys_cd;
      var_row.tb_id                  := r.tb_id;
      var_row.op_cat_cd              := r.op_cat_cd;
      var_row.table_nm               := r.table_nm;
      var_row.instance_nm            := r.instance_nm;
      var_row.partition_id           := r.partition_id;
      var_row.trxn_count             := r.trxn_count;
      var_row.trxn_scrty_count       := r.trxn_scrty_count;
      var_row.rows_count             := r.rows_count;
      var_row.alert_create_count     := r.alert_create_count;
      var_row.trxn_repeat_count      := r.trxn_repeat_count;
      var_row.alert_repeat_count     := r.alert_repeat_count;
      var_row.alert_delete_count     := r.alert_delete_count;
      var_row.files_count            := r.files_count;
      var_row.files_size             := r.files_size;
      var_row.files_read_rows_count  := r.files_read_rows_count;
      var_row.files_trxn_count       := r.files_trxn_count;
      var_row.files_trxn_scrty_count := r.files_trxn_scrty_count;
      var_row.files_rows_count       := r.files_rows_count;
      var_row.errors_count           := r.errors_count;
      var_row.error_msg              := r.error_msg;
      var_row.infa_full_task_name    := r.infa_full_task_name;
      var_row.infa_run_status_code   := r.infa_run_status_code;
      
      PIPE ROW(var_row);
    END LOOP;
  end if;    
END get_load_step_values;  
/

grant EXECUTE on std.get_load_step_values to MANTAS with grant option;
grant EXECUTE on std.get_load_step_values to DATA_READER;
grant EXECUTE on std.get_load_step_values to KDD_ALGORITHM;
grant EXECUTE on std.get_load_step_values to KDD_ANALYST;
grant EXECUTE on std.get_load_step_values to KDD_MINER;
grant EXECUTE on std.get_load_step_values to RF_RSCHEMA_ROLE;
grant EXECUTE on std.get_load_step_values to CMREVMAN;
grant EXECUTE on std.get_load_step_values to KDD_MNR with grant option;
grant EXECUTE on std.get_load_step_values to KDD_REPORT with grant option;
grant EXECUTE on std.get_load_step_values to KYC;
grant EXECUTE on std.get_load_step_values to AMLREP;

GRANT EXECUTE ON std.TAB_LOAD_STEP_VALUES TO MANTAS;
GRANT EXECUTE ON std.TYPE_LOAD_STEP_VALUES TO MANTAS;

