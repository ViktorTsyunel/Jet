CREATE OR REPLACE FUNCTION STD.GET_ACCT_ID_BY_PARAM (
       BCK_ID IN VARCHAR2,
       N20_ID IN VARCHAR2,
       ACCT_ID OUT NUMBER)
RETURN VARCHAR2 IS
       RETURN_VAR          varchar2(100);
       RET_ID              NUMBER;
       CNT                 NUMBER;
       MAIN_REC            NUMBER;
       COLLAPSED_REC       NUMBER;
  begin
    ---------------------
    --
    -- ������� ���������!
    --
    RETURN NULL;
    ---------------------
    CNT := 2;
    LOOP -- ���� ��������� ������ ��������������� ��������� ��������������� � ������� �������������
      SELECT COUNT(distinct T.MANTAS_SEQ_ID)
      INTO CNT
      FROM STD.RECODE_ACCT T
      WHERE((BCK_ID = T.SOURCE_ID and t.source_id_type = 'BCK')
         OR (N20_ID = T.SOURCE_ID and t.source_id_type = 'N20'))
        AND COALESCE(BCK_ID,N20_ID,'0')!='0';
      CASE
        WHEN CNT = 0 THEN -- ���� ������ ������� ����������� � ������� ������������� �� ���������� - ���������� ����� ID
          RET_ID:=BUSINESS.ACCT_SEQ.NEXTVAL;

        WHEN CNT = 1 THEN -- ���� ���������� ������������� ���� ID - ������ ���
          SELECT DISTINCT T.MANTAS_SEQ_ID
          INTO RET_ID
          FROM STD.RECODE_ACCT T
          WHERE((BCK_ID = T.SOURCE_ID and t.source_id_type = 'BCK')
             OR (N20_ID = T.SOURCE_ID and t.source_id_type = 'N20'))
            AND COALESCE(BCK_ID,N20_ID,'0')!='0';

        WHEN CNT >1 THEN -- ���� ���������� ������������� ������ ������ ����� - ����������� �� � ����
          BEGIN -- ��������� ������������ ����
            SELECT min(t.mantas_seq_id)
                  ,max(t.mantas_seq_id)
            INTO MAIN_REC, COLLAPSED_REC
            FROM STD.RECODE_ACCT T
            WHERE((BCK_ID = T.SOURCE_ID and t.source_id_type = 'BCK')
               OR (N20_ID = T.SOURCE_ID and t.source_id_type = 'N20'))
              AND COALESCE(BCK_ID,N20_ID,'0')!='0';

            UPDATE STD.RECODE_ACCT T -- ���������� ������������� ������
               SET T.MANTAS_SEQ_ID = MAIN_REC
                  ,T.collapsed_ids = NVL2(collapsed_ids,collapsed_ids||', ',null)||COLLAPSED_REC
            WHERE  T.MANTAS_SEQ_ID = COLLAPSED_REC;

/*            UPDATE BUSINESS.ACCT T
               SET T.collapsed_flag = 1
                  ,collapse_is_code = (SELECT Main_Is_Code FROM DWH.Ref_Contractors  WHERE Cnt_Id = MAIN_REC)
                  ,collapse_time = SYSDATE
            WHERE cnt_id = COLLAPSED_REC; */
          END;
          RET_ID:= -1;
      END CASE;
      IF CNT <=1 THEN
        EXIT;
      END IF;
    END LOOP;
  ACCT_ID:=RET_ID;
  RETURN_VAR := 'Success';
  RETURN (RETURN_VAR);
END;
/
grant execute on STD.GET_ACCT_ID_BY_PARAM to data_loader;
