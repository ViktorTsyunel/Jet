--������� � �������������
-- ����� mantas
GRANT SELECT ON mantas.rf_oes_321 TO amlrep;
GRANT SELECT ON mantas.kdd_review TO amlrep;
GRANT SELECT ON mantas.rf_oes_321_org TO amlrep;
GRANT SELECT ON mantas.rf_opok TO amlrep;
GRANT SELECT ON mantas.kdd_review_owner TO amlrep;
GRANT SELECT ON MANTAS.RF_AUDIT_EVENT_CATEGORY TO AMLREP;
GRANT SELECT ON MANTAS.RF_AUDIT_EVENT_TYPE TO AMLREP;
GRANT SELECT ON MANTAS.RF_AUDIT_OBJECT_TYPE TO AMLREP;
GRANT SELECT ON MANTAS.RF_AUDIT_EVENT TO AMLREP;
GRANT SELECT ON MANTAS.RF_AUDIT_SEARCH_PARAMS TO AMLREP;
GRANT SELECT ON MANTAS.KDD_CODE_SET_TRNLN TO AMLREP;
GRANT SELECT ON mantas.rf_watch_list TO AMLREP;
-- ����� business
GRANT SELECT ON business.org TO amlrep;
GRANT SELECT ON business.rf_nsi_op_types TO amlrep;
GRANT SELECT ON business.rf_nsi_op_type_changes TO amlrep;
GRANT SELECT ON business.rf_nsi_tr_types TO amlrep;
GRANT SELECT ON business.rf_nsi_tr_type_changes TO amlrep;
-- ���������
GRANT EXECUTE ON mantas.rf_add_work_days TO amlrep;
GRANT EXECUTE ON mantas.rf_pkg_adm TO amlrep;
GRANT EXECUTE ON mantas.rf_pkg_scnro TO amlrep;
-- STD � �������
GRANT SELECT ON std.logs TO AMLREP;
GRANT SELECT ON std.load_steps TO AMLREP;
GRANT SELECT ON std.load_step_log_items TO AMLREP;
GRANT SELECT ON std.log_items TO AMLREP;
GRANT SELECT ON rgat000gate.loading_files TO AMLREP;
