GRANT EXECUTE ON business.rf_normalize_name TO AMLREP;
GRANT EXECUTE ON business.rf_normalize_docno TO AMLREP;

GRANT SELECT ON mantas.RF_AUDIT_EVENT TO AMLREP;
GRANT SELECT ON mantas.RF_AUDIT_EVENT_TYPE TO AMLREP;
GRANT SELECT ON mantas.RF_AUDIT_OBJECT_TYPE TO AMLREP;
GRANT SELECT ON mantas.RF_AUDIT_SEARCH_PARAMS TO AMLREP;

CREATE OR REPLACE
FUNCTION amlrep.get_rep_audit_sql
(
  p_change_from    VARCHAR2   DEFAULT null,
  p_change_to      VARCHAR2   DEFAULT null,
  p_user_login     VARCHAR2   DEFAULT null,
  p_ip_address     VARCHAR2   DEFAULT null,
  p_event_type_cd  VARCHAR2   DEFAULT null,
  p_event_cat_cd   VARCHAR2   DEFAULT null,
  p_just_read_fl   VARCHAR2   DEFAULT null,
  p_object_type_cd VARCHAR2   DEFAULT null,
  p_err_type_cd    VARCHAR2   DEFAULT null, -- ��� ������ A/T ��� ALL - ����� �� ������
  p_object_id      VARCHAR2   DEFAULT null,
  p_audit_seq_id   VARCHAR2   DEFAULT null,
  p_last_nm        VARCHAR2   DEFAULT null,
  p_first_nm       VARCHAR2   DEFAULT null,
  p_midl_nm        VARCHAR2   DEFAULT null,
  p_birth_dt       VARCHAR2   DEFAULT null,
  p_doc_type_cd    VARCHAR2   DEFAULT null,
  p_doc_series_id  VARCHAR2   DEFAULT null,
  p_doc_no_id      VARCHAR2   DEFAULT null,
  p_acct_nb        VARCHAR2   DEFAULT null,
  p_inn_nb         VARCHAR2   DEFAULT null,
  p_ogrn_nb        VARCHAR2   DEFAULT null,
  p_okpo_nb        VARCHAR2   DEFAULT null,
  p_watch_list_cd  VARCHAR2   DEFAULT null,
  p_cust_seq_id    VARCHAR2   DEFAULT null
) 
RETURN CLOB IS
  var_template1    CLOB :=
q'{
SELECT e.AUDIT_SEQ_ID as AUDIT_SEQ_ID,
       e.EVENT_TIME as EVENT_TIME,
       e.USER_LOGIN as USER_LOGIN,
       e.IP_ADDRESS as IP_ADDRESS,
       NVL(et.event_type_nm,e.EVENT_TYPE_CD) as EVENT_TYPE_CD,
       NVL(ot.object_type_nm,e.OBJECT_TYPE_CD) as OBJECT_TYPE_CD,
       e.OBJECT_ID as OBJECT_ID,
       e.EVENT_DESCR_TX as EVENT_DESCR_TX,
       NVL2(e.EVENT_PARAMS,'���������','') as EVENT_PARAMS,
       NVL2(e.OLD_VALUES,'���������','') as OLD_VALUES,
       e.AUDIT_MONTH as AUDIT_MONTH,
       e.ERR_TYPE_CD as ERR_TYPE_CD,
       e.MESSAGE_TX as MESSAGE_TX,
       NVL2(sp.audit_seq_id,'���������','') as SRC_PARAMS
  FROM mantas.RF_AUDIT_EVENT e
       left join mantas.RF_AUDIT_EVENT_TYPE et on et.event_type_cd = e.event_type_cd
       left join mantas.RF_AUDIT_OBJECT_TYPE ot on ot.object_type_cd = e.object_type_cd
       left join mantas.RF_AUDIT_SEARCH_PARAMS sp on sp.audit_seq_id = e.audit_seq_id
 WHERE e.event_time BETWEEN to_date(:p_change_from, 'dd.mm.yyyy hh24:mi:ss') and to_date(:p_change_to, 'dd.mm.yyyy hh24:mi:ss') 
}';
 
  var_template2    CLOB :=
q'{
SELECT e.AUDIT_SEQ_ID as AUDIT_SEQ_ID,
       e.EVENT_TIME as EVENT_TIME,
       e.USER_LOGIN as USER_LOGIN,
       e.IP_ADDRESS as IP_ADDRESS,
       NVL(et.event_type_nm,e.EVENT_TYPE_CD) as EVENT_TYPE_CD,
       NVL(ot.object_type_nm,e.OBJECT_TYPE_CD) as OBJECT_TYPE_CD,
       e.OBJECT_ID as OBJECT_ID,
       e.EVENT_DESCR_TX as EVENT_DESCR_TX,
       NVL2(e.EVENT_PARAMS,'���������','') as EVENT_PARAMS,
       NVL2(e.OLD_VALUES,'���������','') as OLD_VALUES,
       e.AUDIT_MONTH as AUDIT_MONTH,
       e.ERR_TYPE_CD as ERR_TYPE_CD,
       e.MESSAGE_TX as MESSAGE_TX,
       NVL2(sp.audit_seq_id,'���������','') as SRC_PARAMS
  FROM (#SEARCH_SQL#) sp
       join mantas.RF_AUDIT_EVENT e on e.audit_seq_id = sp.audit_seq_id
       left join mantas.RF_AUDIT_EVENT_TYPE et on et.event_type_cd = e.event_type_cd
       left join mantas.RF_AUDIT_OBJECT_TYPE ot on ot.object_type_cd = e.object_type_cd
 WHERE 1 = 1 
}'; 
  var_search_sql   CLOB;
  var_add_sql      CLOB;

  var_sql          CLOB;
BEGIN
  --
  -- ���� �� ������� ���������� ������, ��������������� ���������� ������ ���������� ���������� - 
  -- ����� ������ ������ SQL, ����� - ������
  --
  if trim(p_last_nm||p_first_nm||p_midl_nm||p_birth_dt||p_doc_type_cd||p_doc_series_id||p_doc_no_id||
          p_acct_nb||p_inn_nb||p_ogrn_nb||p_okpo_nb||p_watch_list_cd||p_cust_seq_id) is null Then
    var_sql := var_template1;
  else
    --
    -- ��������� ���������� SELECT, ���������� ������ ������� RF_AUDIT_SEARCH_PARAMS
    --    
    -- ���. ������� �� ���� ������� � ������������
    var_add_sql := ' and event_time BETWEEN to_date(:p_change_from, ''dd.mm.yyyy hh24:mi:ss'') and to_date(:p_change_to, ''dd.mm.yyyy hh24:mi:ss'')';
    if p_user_login is not null Then
      var_add_sql := var_add_sql||' and user_login = :p_user_login';
    end if;
    
    -- ����� �� ��� � ���� ��������
    if trim(p_last_nm||p_first_nm||p_midl_nm||p_birth_dt) is not null Then
      if trim(p_last_nm) is not null and trim(p_first_nm) is not null and trim(p_midl_nm) is not null Then
        var_search_sql := var_search_sql||' union '||
                          'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                           'where business.rf_normalize_name(last_nm||first_nm||midl_nm) = '||
                                 'business.rf_normalize_name(:p_last_nm||:p_first_nm||:p_midl_nm)'||
                          case when p_birth_dt is not null
                               then ' and birth_dt = to_date(:p_birth_dt, ''dd.mm.yyyy'')'
                          end||
                          var_add_sql;
      elsif trim(p_last_nm) is not null and trim(p_first_nm) is not null Then                           
        var_search_sql := var_search_sql||' union '||
                          'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                           'where business.rf_normalize_name(last_nm||first_nm) = '||
                                 'business.rf_normalize_name(:p_last_nm||:p_first_nm)'||
                          case when p_birth_dt is not null
                               then ' and birth_dt = to_date(:p_birth_dt, ''dd.mm.yyyy'')'
                          end||
                          var_add_sql;
      elsif trim(p_last_nm) is not null Then                           
        var_search_sql := var_search_sql||' union '||
                          'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                           'where business.rf_normalize_name(last_nm) = '||
                                 'business.rf_normalize_name(:p_last_nm)'||
                          case when p_birth_dt is not null
                               then ' and birth_dt = to_date(:p_birth_dt, ''dd.mm.yyyy'')'
                          end||
                          var_add_sql;
      elsif trim(p_birth_dt) is not null Then                           
        var_search_sql := var_search_sql||' union '||
                          'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                           'where birth_dt = to_date(:p_birth_dt, ''dd.mm.yyyy'')'||var_add_sql;
      end if;
    end if;
    -- ����� �� ���
    if trim(p_doc_no_id) is not null Then
      var_search_sql := var_search_sql||' union '||
                        'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                         'where business.rf_normalize_docno(doc_series_id, doc_no_id, null, doc_type_cd) = '||
                               'business.rf_normalize_docno(:p_doc_series_id, :p_doc_no_id, null, :p_doc_type_cd)'||
                               case when trim(p_doc_type_cd) is not null
                                    then ' and doc_type_cd = :p_doc_type_cd'
                               end||var_add_sql;
    end if;
    -- ����� �� ������ �����
    if trim(p_acct_nb) is not null Then
      var_search_sql := var_search_sql||' union '||
                        'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                         'where acct_nb = :p_acct_nb'||var_add_sql;
    end if;
    -- ����� �� ���
    if trim(p_inn_nb) is not null Then
      var_search_sql := var_search_sql||' union '||
                        'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                         'where inn_nb = :p_inn_nb'||var_add_sql;
    end if;
    -- ����� �� ����
    if trim(p_ogrn_nb) is not null Then
      var_search_sql := var_search_sql||' union '||
                        'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                         'where ogrn_nb = :p_ogrn_nb'||var_add_sql;
    end if;
    -- ����� �� ����
    if trim(p_okpo_nb) is not null Then
      var_search_sql := var_search_sql||' union '||
                        'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                         'where okpo_nb = :p_okpo_nb'||var_add_sql;
    end if;        
    -- ����� �� �������???
    if trim(p_watch_list_cd) is not null Then
      var_search_sql := var_search_sql||' union '||
                        'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                         'where watch_list_cd = :p_watch_list_cd'||var_add_sql;
    end if;
    -- ����� �� ID �������
    if trim(p_cust_seq_id) is not null Then
      var_search_sql := var_search_sql||' union '||
                        'select * from mantas.RF_AUDIT_SEARCH_PARAMS '||
                         'where cust_seq_id = :p_cust_seq_id'||var_add_sql;
    end if;
    
    var_search_sql := substr(var_search_sql, length(' union ') + 1);
    
    -- ���� ������� ��� (����� ����� ����, ���� ������ ����� �������, ��������, ������ �� ��� ���), 
    -- ���������� ������� �� ��������� ������
    if var_search_sql is null Then
      var_sql := var_template1;
    else
      var_sql := var_template2;
      var_sql := replace(var_sql, '#SEARCH_SQL#', var_search_sql);
    end if;  
  end if;
  --
  -- ���������� ������� � ���� WHERE
  --
  if p_user_login is not null Then
    var_sql := var_sql||' and e.user_login = :p_user_login';
  end if;
  
  if p_ip_address is not null Then
    var_sql := var_sql||' and e.ip_address = :p_ip_address';
  end if;
    
  if p_event_type_cd is not null Then
    var_sql := var_sql||' and e.event_type_cd = :p_event_type_cd';
  end if;

  if p_event_cat_cd is not null Then
    var_sql := var_sql||' and et.event_cat_cd = :p_event_cat_cd';
  end if;

  if p_just_read_fl is not null Then
    var_sql := var_sql||' and et.just_read_fl = to_number(:p_just_read_fl)';
  end if;

  if p_object_type_cd is not null Then
    var_sql := var_sql||' and e.object_type_cd = :p_object_type_cd';
  end if;

  if p_err_type_cd <> 'ALL' Then    
    var_sql := var_sql||' and e.err_type_cd = :p_err_type_cd';
  elsif p_err_type_cd = 'ALL' Then    
    var_sql := var_sql||' and e.err_type_cd is not null';
  end if;

  if p_object_id is not null Then
    var_sql := var_sql||' and e.object_id = :p_object_id';
  end if;

  if p_audit_seq_id is not null Then
    var_sql := var_sql||' and e.audit_seq_id = to_number(:p_audit_seq_id)';
  end if;
  --
  -- ���������� ����������
  --
  var_sql := var_sql||' ORDER BY e.AUDIT_SEQ_ID';
  
  return var_sql;  
END get_rep_audit_sql;
/

