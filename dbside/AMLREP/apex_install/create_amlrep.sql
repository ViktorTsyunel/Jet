-- �������� ������������, ���������� ����� �������� ������� ��� APEX �������
CREATE USER amlrep IDENTIFIED BY amlrep54321 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
GRANT CREATE SESSION TO amlrep;
--������� � �������������
-- ����� mantas
GRANT SELECT ON mantas.rf_oes_321 TO amlrep;
GRANT SELECT ON mantas.kdd_review TO amlrep;
GRANT SELECT ON mantas.rf_oes_321_org TO amlrep;
GRANT SELECT ON mantas.rf_opok TO amlrep;
GRANT SELECT ON mantas.kdd_review_owner TO amlrep;
-- ����� business
GRANT SELECT ON business.org TO amlrep;
GRANT SELECT ON business.rf_nsi_op_types TO amlrep;
GRANT SELECT ON business.rf_nsi_op_type_changes TO amlrep;
GRANT SELECT ON business.rf_nsi_tr_types TO amlrep;
GRANT SELECT ON business.rf_nsi_tr_type_changes TO amlrep;
-- ���������
GRANT EXECUTE ON mantas.rf_add_work_days TO amlrep;
GRANT EXECUTE ON mantas.rf_pkg_adm TO amlrep;
GRANT EXECUTE ON mantas.rf_pkg_scnro TO amlrep;

