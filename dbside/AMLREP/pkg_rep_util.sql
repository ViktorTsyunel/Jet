GRANT EXECUTE ON mantas.rf_tab_varchar TO AMLREP;
GRANT EXECUTE ON rf_json TO AMLREP;
GRANT EXECUTE ON rf_json_list TO AMLREP;
GRANT EXECUTE ON rf_json_value TO AMLREP;

CREATE OR REPLACE PACKAGE amlrep.pkg_rep_util IS
--
-- �������� ��������� JSON � ���� ������ (������ � ���������)
--
FUNCTION show_json_as_tree
(
  par_json      CLOB,
  par_level     INTEGER DEFAULT 0
)
RETURN mantas.rf_tab_varchar PIPELINED;  
--
-- �������� ��������� JSON_VALUE � ���� ������ (������ � ���������)
--
FUNCTION show_json_value_as_tree
(
  par_json_value      rf_json_value,
  par_level           INTEGER,
  par_item_header     VARCHAR2 DEFAULT null
)
RETURN mantas.rf_tab_varchar PIPELINED;
END pkg_rep_util;
/

CREATE OR REPLACE PACKAGE BODY amlrep.pkg_rep_util IS
--
-- �������� ��������� JSON � ���� ������ (������ � ���������)
--
FUNCTION show_json_as_tree
(
  par_json      CLOB,
  par_level     INTEGER DEFAULT 0
)
RETURN mantas.rf_tab_varchar PIPELINED IS  
  var_json        rf_json;
  var_json_list   rf_json_list;
  var_json_value  rf_json_value;

  var_dummy       INTEGER;
BEGIN
  if par_json is null Then return; end if;
  --
  -- ������ ���������� JSON
  --
  var_json := rf_json(par_json);
  if var_json is null Then return; end if;
  if var_json.rf_json_data is null Then return; end if;
  if var_json.rf_json_data.COUNT = 0 Then return; end if;
  --
  -- �������� ���������� JSON: 
  --  1) ������ �������� form, action
  --
  var_json.remove('form');
  var_json.remove('action'); 
  -- 
  --  2) ���� ������� ������ ������ �� ������ �������� - ����� ������ ���� �������
  --
  if var_json.rf_json_data.COUNT = 1 Then -- ���� �������
    if var_json.rf_json_data(var_json.rf_json_data.FIRST).is_array Then -- ���� ������
      var_dummy := var_json.rf_json_data(var_json.rf_json_data.FIRST).object_or_array.getobject(var_json_list);
      if var_json_list is not null Then 
        if var_json_list.list_data is not null Then
          if var_json_list.list_data.COUNT = 1 Then -- � ������� ���� �������
            var_json := rf_json(var_json_list.list_data(var_json_list.list_data.FIRST));
            if var_json is null Then return; end if;
            if var_json.rf_json_data is null Then return; end if;
            if var_json.rf_json_data.COUNT = 0 Then return; end if;
          end if;
        end if;
      end if;
    end if;
  end if;  
  --
  -- ���� �� ������� ������ ����������� JSON
  --  
  FOR i IN var_json.rf_json_data.FIRST..var_json.rf_json_data.LAST LOOP    
    if var_json.rf_json_data.EXISTS(i) Then
      var_json_value := var_json.rf_json_data(i);

      FOR r IN (SELECT * FROM table(pkg_rep_util.show_json_value_as_tree(var_json_value, 0))) LOOP
        PIPE ROW(r.column_value);
      END LOOP;        
    end if;  
  END LOOP;    
EXCEPTION
  WHEN OTHERS THEN
    null;  
END show_json_as_tree;
--
-- �������� ��������� JSON_VALUE � ���� ������ (������ � ���������)
--
FUNCTION show_json_value_as_tree
(
  par_json_value      rf_json_value,
  par_level           INTEGER,
  par_item_header     VARCHAR2 DEFAULT null
)
RETURN mantas.rf_tab_varchar PIPELINED IS  
  var_indent      VARCHAR2(255 CHAR);
  var_equal_sign  VARCHAR2(255 CHAR);
  
  var_json_value  rf_json_value;
  var_json_list   rf_json_list;
  var_json        rf_json;

  var_dummy       INTEGER;
BEGIN
  if par_json_value is null Then return; end if;

  var_indent := lpad(' ', par_level*2, ' ');
  var_equal_sign := case when par_json_value.mapname is not null then ' = ' end;
  
  if par_json_value.is_string Then
    PIPE ROW(var_indent||par_json_value.mapname||var_equal_sign||par_json_value.get_string);
  elsif par_json_value.is_number Then
    PIPE ROW(var_indent||par_json_value.mapname||var_equal_sign||to_number(par_json_value.get_number));
  elsif par_json_value.is_bool Then
    if par_json_value.get_bool Then
      PIPE ROW(var_indent||par_json_value.mapname||var_equal_sign||'true');
    else
      PIPE ROW(var_indent||par_json_value.mapname||var_equal_sign||'false');
    end if;  
  elsif par_json_value.is_null Then
    PIPE ROW(var_indent||par_json_value.mapname||var_equal_sign||'null');
  elsif par_json_value.is_array Then
    PIPE ROW(var_indent||par_json_value.mapname);

    var_dummy := par_json_value.object_or_array.getobject(var_json_list);
    if var_json_list is null Then return; end if;

    FOR i IN var_json_list.list_data.FIRST..var_json_list.list_data.LAST LOOP    
      if var_json_list.list_data.EXISTS(i) Then
        FOR r IN (SELECT * FROM table(pkg_rep_util.show_json_value_as_tree(var_json_list.list_data(i), par_level + 1, par_json_value.mapname||'['||to_char(i)||']'))) LOOP
          PIPE ROW(r.column_value);
        END LOOP;                
      end if;
    END LOOP;
  elsif par_json_value.is_object Then
    PIPE ROW(var_indent||trim(par_item_header||' '||par_json_value.mapname));

    var_dummy := par_json_value.object_or_array.getobject(var_json);
    if var_json is null Then return; end if;

    FOR i IN var_json.rf_json_data.FIRST..var_json.rf_json_data.LAST LOOP    
      if var_json.rf_json_data.EXISTS(i) Then
        FOR r IN (SELECT * FROM table(pkg_rep_util.show_json_value_as_tree(var_json.rf_json_data(i), par_level + 1))) LOOP
          PIPE ROW(r.column_value);
        END LOOP;                
      end if;
    END LOOP;
  end if;      
END show_json_value_as_tree;
END pkg_rep_util;
/

/*

select *
  from table(amlrep.pkg_rep_util.show_json_as_tree('{"data":[{"NEW_OWNER_SEQ_ID":10009,"OWNER_DSPLY_NM":"\u041a\u043e\u043d\u0442\u0440\u043e\u043b\u0435\u0440","NEW_OWNER_DSPLY_NM":"Kulimov P.S.","_id_":294}],"form":"AssignReplacement","action":"update", "test_array":[1,2,3], "test_object":{"col1":"Qwert", "col2":234234}}'))

select *
  from table(amlrep.pkg_rep_util.show_json_as_tree('{"data":[{"NEW_OWNER_SEQ_ID":10009,"OWNER_DSPLY_NM":"\u041a\u043e\u043d\u0442\u0440\u043e\u043b\u0435\u0440","NEW_OWNER_DSPLY_NM":"Kulimov P.S.","_id_":294}],"form":"AssignReplacement","action":"update"}'))

select *
  from table(amlrep.pkg_rep_util.show_json_as_tree('{"NEW_OWNER_SEQ_ID":10009,"OWNER_DSPLY_NM":"\u041a\u043e\u043d\u0442\u0440\u043e\u043b\u0435\u0440","NEW_OWNER_DSPLY_NM":"Kulimov P.S.","_id_":294}'))
  
select *
  from table(amlrep.pkg_rep_util.show_json_as_tree('{"head":[{"col":1},2,3]}'))

  
  
select t.*
  from mantas.rf_audit_event e
       join table(amlrep.pkg_rep_util.show_json_as_tree(e.event_params)) t on 1=1
 where e.audit_seq_id = 367 and
       e.event_params is not null        

select t.*
  from mantas.rf_audit_event e
       join table(amlrep.pkg_rep_util.show_json_as_tree(e.old_values)) t on 1=1
 where --e.audit_seq_id = 11708 and
       e.old_values is not null        

 */
