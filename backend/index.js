// Добавим внешний конфиг
global.config = require('./config.json');

var express = require('express');
var app = express();

// СТАТИКА
app.use('/', express.static(config['env']['static']));

// СПЕЦ. ОБРАБОТЧИКИ
app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.session({
	secret: config['app']['secret'] || 'secret'
}));
app.use(app.router);
app.post(new RegExp('^(' + config['env']['service'].join('|') + ')'), function (request, response) {
	var json = request.body;
	console.log(json);
	// сделаем задержку, для эмуляции того что сервер загружен
	setTimeout(function () {
		try {
			var path = './request/' + json.form + '.js';
			require(path).response.call(this, request, response, json.action, json.block_nb);
            // TODO: Су...Чка! Больше так не делай!:(
            // var rjn = response;
            // console.log(rjn);
		} catch (e) {
			response.send(500, {
				success: false,
				message: "Ошибка выполнения запроса: " + e
			});
		}
	}, 1000);
});

// Добавим переадресацию на приложение
if (config['app']['path']) {
	app.get('/', function (req, res) {
		res.writeHead(302, {
			'Location': config['app']['path']
		});
		res.end();
	});
}
var port = config['app']['port'] || 8888;
app.listen(port);
console.log(
	"Сервер запущен на порту: " + port + "\n" +
	"Приложение доступно по адресу: http://localhost:" + port + config['app']['path']
);
