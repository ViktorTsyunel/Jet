var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
	if (action == 'getDefault') {
		response.send({
			success: true,
			"_id_": 9001
		});
	} else if (action == 'read') {
		response.send({
	        success: true,
		    totalCount: 6,
			data: [
				{
					"v_$0": 1,
					"_id_": 9002,
					"QUEUE_DISPLAY_NM": "1. Срочная обработка",
					"IS_DEFAULT": 0
				},
				{
					"v_$0": 2,
					"_id_": 9001,
					"QUEUE_DISPLAY_NM": "2. Требующие обработки",
					"IS_DEFAULT": 1
				},
				{
					"v_$0": 3,
					"_id_": 9003,
					"QUEUE_DISPLAY_NM": "3. Экспорт в Комиту",
					"IS_DEFAULT": 0
				},
				{
					"v_$0": 4,
					"_id_": 9004,
					"QUEUE_DISPLAY_NM": "4. Не подлежат контролю (за сегодня)",
					"IS_DEFAULT": 0
				},
				{
					"v_$0": 5,
					"_id_": 9005,
					"QUEUE_DISPLAY_NM": "5. Отправлены (за сегодня)",
					"IS_DEFAULT": 0
				},
				{
					"v_$0": 6,
					"_id_": 9006,
					"QUEUE_DISPLAY_NM": "6. Просроченные",
					"IS_DEFAULT": 0
				}
			]
	    });
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;
