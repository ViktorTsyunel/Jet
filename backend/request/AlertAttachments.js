var querystring = require("querystring");
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'create') {
        response.send({
            success: true,
            data: request.body.data.map(function (i) {
                return {
                    "_id_": Math.floor(Math.random() * 100000),
                    "#clientId#": i["_id_"]
                };
            })
        });
    } else if (action == 'delete') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId') {
        response.send(
            {
                "success" : true,
                "totalCount" : 3,
                "data" : [{
                    "_id_" : 281,
                    "ATTCH_NM" : "sqlFile",
                    "FILE_NM" : "rf_pkg_oes.sql",
                    "CMMNT" : "test pk; \u041D\u0435 \u043F\u043E\u0434\u043B\u0435\u0436\u0438\u0442 \u043A\u043E\u043D\u0442\u0440\u043E\u043B\u044E, \u043B\u043E\u0436\u043D\u043E\u0435 \u0441\u0440\u0430\u0431\u0430\u0442\u044B\u0432\u0430\u043D\u0438\u0435 \u0438\u0437-\u0437\u0430 \u043D\u0435\u0432\u0435\u0440\u043D\u043E\u0439 \u043B\u0435\u043A\u0441\u0435\u043C\u044B",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-12-22 13:14:27",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }, {
                    "_id_" : 282,
                    "ATTCH_NM" : "\u041D\u0430\u0438\u043C\u0435\u043D\u043E\u0432\u0430\u043D\u0438\u0435 \u043D\u043E\u0432\u043E\u0435",
                    "FILE_NM" : "test.log",
                    "CMMNT" : "test comments; \u041E\u0436\u0438\u0434\u0430\u0435\u0442\u0441\u044F \u043E\u0442\u0432\u0435\u0442",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-12-22 13:29:09",
                    "MODIFIED_BY" : "OFMSUPERVISOR",
                    "MODIFIED_DATE" : "2015-12-23 18:09:44",
                    "FILE_SIZE" : "5.5 \u041A\u0431"
                }, {
                    "_id_" : 283,
                    "ATTCH_NM" : "tests files",
                    "FILE_NM" : "test.xls",
                    "CMMNT" : "test comments; \u041E\u0436\u0438\u0434\u0430\u0435\u0442\u0441\u044F \u043E\u0442\u0432\u0435\u0442",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-12-22 13:29:09",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }]
            }
        );
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
