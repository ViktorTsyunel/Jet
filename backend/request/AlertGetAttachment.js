var querystring = require("querystring");
var fs = require('fs');
var path = require('path');

function response(request, response, action) {
    var file = __dirname + '/../config.json.example';
    var filename = path.basename(file);
    var mimetype = 'text/plain';

    response.setHeader('Content-disposition', 'attachment; filename=' + filename);
    response.setHeader('Content-type', mimetype);

    var filestream = fs.createReadStream(file);
    filestream.pipe(response);
}
exports.response = response;