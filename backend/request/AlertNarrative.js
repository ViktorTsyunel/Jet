var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'updateByAlertId') {
        response.send({
            success: true
        });
    } else if (action == 'read' || action == 'readByAlertId') {
        response.send({
            "success" : true,
            "totalCount" : 1,
            "data" : [{
                "_id_" : 1,
                "MODIFIED_DATE" : "2015-02-11 13:07:25",
                "MODIFIED_BY" : "OFMSUPERVISOR",
                "NOTE_TX" : "\u0421\u043E\u0437\u0434\u0430\u043D\u0438\u0435"
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
