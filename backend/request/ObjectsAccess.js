var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
			success: true,
			data: [
				{
					"_id_": "OPOK",
					"OBJECT_CODE": "OPOK",
					"ACCESS_MODE": "F"
				},
				{
					"_id_": "OPOKCriteria",
					"OBJECT_CODE": "OPOKCriteria",
					"ACCESS_MODE": "F"
				},
				{
					"_id_": "AssignRule",
					"OBJECT_CODE": "AssignRule",
					"ACCESS_MODE": "F"
				},
                {
                    "_id_": "AssignReplacement",
                    "OBJECT_CODE": "AssignReplacement",
                    "ACCESS_MODE": "F"
                },
                {
                    "_id_": "AssignLimit",
                    "OBJECT_CODE": "AssignLimit",
                    "ACCESS_MODE": "F"
                },
                {
					"_id_": "OPOKTest",
					"OBJECT_CODE": "OPOKTest",
					"ACCESS_MODE": "F"
				},
				{
					"_id_": "OPOKRule",
					"OBJECT_CODE": "OPOKRule",
					"ACCESS_MODE": "F"
				},
				{
					"_id_": "OES321Org",
					"OBJECT_CODE": "OES321Org",
					"ACCESS_MODE": "F"
				},
				{
					"_id_": "OES321PartyOrg",
					"OBJECT_CODE": "OES321PartyOrg",
					"ACCESS_MODE": "F"
				},
				{
					"_id_": "OESCheckRule",
					"OBJECT_CODE": "OESCheckRule",
					"ACCESS_MODE": "F"
				}
			]
		});
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
