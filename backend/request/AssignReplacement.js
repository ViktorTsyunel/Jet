var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				return {
					"_id_": Math.floor(Math.random() * 100000),
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send(
            {
                "success" : true,
                "totalCount" : 3,
                "data" : [{
                    "_id_" : 102,
                    "REPLACEMENT_SEQ_ID" : 102,
                    "OWNER_SEQ_ID" : 10004,
                    "OWNER_DSPLY_NM" : "\u041F\u0435\u0442\u0440\u043E\u0432 \u041F. \u041F. (PETROV)",
                    "NEW_OWNER_SEQ_ID" : 10003,
                    "NEW_OWNER_DSPLY_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0418. \u0418. (IVANOV)",
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "2015-10-01 00:00:00",
                    "END_DT" : "2015-11-30 00:00:00",
                    "CREATED_BY" : "PETROV",
                    "CREATED_DATE" : "2015-10-22 00:00:00",
                    "MODIFIED_BY" : "SIDOROV",
                    "MODIFIED_DATE" : "2015-10-23 00:00:00"
                }, {
                    "_id_" : 101,
                    "REPLACEMENT_SEQ_ID" : 101,
                    "OWNER_SEQ_ID" : 10003,
                    "OWNER_DSPLY_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0418. \u0418. (IVANOV)",
                    "NEW_OWNER_SEQ_ID" : 10005,
                    "NEW_OWNER_DSPLY_NM" : "\u0421\u0438\u0434\u043E\u0440\u043E\u0432 \u0421. \u0421. (SIDOROV)",
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "2015-10-08 00:00:00",
                    "END_DT" : "2015-10-22 00:00:00",
                    "CREATED_BY" : "IVANOV",
                    "CREATED_DATE" : "2015-10-13 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }, {
                    "_id_" : 103,
                    "REPLACEMENT_SEQ_ID" : 103,
                    "OWNER_SEQ_ID" : 10002,
                    "OWNER_DSPLY_NM" : "\u0421\u0443\u043F\u0435\u0440\u0432\u0438\u0437\u043E\u0440 \u043E\u0442\u0434\u0435\u043B\u0430 \u0424\u041C (OFMSUPERVISOR)",
                    "NEW_OWNER_SEQ_ID" : 10004,
                    "NEW_OWNER_DSPLY_NM" : "\u041F\u0435\u0442\u0440\u043E\u0432 \u041F. \u041F. (PETROV)",
                    "ACTIVE_FL" : "N",
                    "START_DT" : "2015-09-02 00:00:00",
                    "END_DT" : "2015-09-03 00:00:00",
                    "CREATED_BY" : "SIDOROV",
                    "CREATED_DATE" : "2015-10-01 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }]
            }
        );
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;
