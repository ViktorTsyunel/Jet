var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				return {
					"_id_": Math.floor(Math.random() * 100000),
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send(
            {
                "success" : true,
                "totalCount" : 4,
                "data" : [{
                    "_id_" : 103,
                    "LIMIT_SEQ_ID" : 103,
                    "LIMIT_TP_CD" : "R",
                    "ROLE_CD" : "AMSUPVISR",
                    "OWNER_SEQ_ID" : null,
                    "OWNER_DSPLY_NM" : "",
                    "ORDER1_LIMIT_QT" : 100,
                    "FULL_LIMIT_QT" : 200,
                    "ACTIVE_FL" : "N",
                    "START_DT" : "",
                    "END_DT" : "",
                    "CREATED_BY" : "SIDOROV",
                    "CREATED_DATE" : "2015-10-01 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }, {
                    "_id_" : 102,
                    "LIMIT_SEQ_ID" : 102,
                    "LIMIT_TP_CD" : "R",
                    "ROLE_CD" : "AMANALYST1",
                    "OWNER_SEQ_ID" : null,
                    "OWNER_DSPLY_NM" : "",
                    "ORDER1_LIMIT_QT" : 400,
                    "FULL_LIMIT_QT" : 600,
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "",
                    "END_DT" : "2015-10-23 00:00:00",
                    "CREATED_BY" : "SIDOROV",
                    "CREATED_DATE" : "2015-10-01 00:00:00",
                    "MODIFIED_BY" : "PETROV",
                    "MODIFIED_DATE" : "2015-10-15 00:00:00"
                }, {
                    "_id_" : 101,
                    "LIMIT_SEQ_ID" : 101,
                    "LIMIT_TP_CD" : "D",
                    "ROLE_CD" : "",
                    "OWNER_SEQ_ID" : null,
                    "OWNER_DSPLY_NM" : "",
                    "ORDER1_LIMIT_QT" : 300,
                    "FULL_LIMIT_QT" : 500,
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "2015-10-01 00:00:00",
                    "END_DT" : "",
                    "CREATED_BY" : "SIDOROV",
                    "CREATED_DATE" : "2015-10-01 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }, {
                    "_id_" : 104,
                    "LIMIT_SEQ_ID" : 104,
                    "LIMIT_TP_CD" : "U",
                    "ROLE_CD" : "",
                    "OWNER_SEQ_ID" : 10004,
                    "OWNER_DSPLY_NM" : "\u041F\u0435\u0442\u0440\u043E\u0432 \u041F. \u041F. (PETROV)",
                    "ORDER1_LIMIT_QT" : 500,
                    "FULL_LIMIT_QT" : 500,
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "2015-10-02 00:00:00",
                    "END_DT" : "2015-10-28 00:00:00",
                    "CREATED_BY" : "IVANOV",
                    "CREATED_DATE" : "2015-10-14 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }]
            }
        );
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;
