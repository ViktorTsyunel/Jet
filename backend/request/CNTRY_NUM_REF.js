var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
            "success" : true,
            "totalCount" : 2,
            "data" : [{
                "_ID_" : "195",
                "ISO_NUMERIC_CD" : "195",
                "CNTRY_NM" : "Зимбабве"
            },{
                "_ID_" : "196",
                "ISO_NUMERIC_CD" : "196",
                "CNTRY_NM" : "КИПР"
            }]
        });
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
