var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				return {
					"_id_": Math.floor(Math.random() * 100000),
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send(
            {
                "success" : true,
                "totalCount" : 5,
                "data" : [{
                    "_id_" : 104,
                    "RULE_SEQ_ID" : 104,
                    "OWNER_SEQ_ID" : 10005,
                    "ORDER2_FL" : "N",
                    "ANY_FL" : "N",
                    "OPOK_NB" : 1002,
                    "TB_ID" : 38,
                    "OSB_ID" : null,
                    "ACTIVE_FL" : "N",
                    "START_DT" : "2015-10-01 00:00:00",
                    "END_DT" : "2015-12-31 00:00:00",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-10-09 00:00:00",
                    "MODIFIED_BY" : "PETROV",
                    "MODIFIED_DATE" : "2015-10-10 00:00:00"
                }, {
                    "_id_" : 103,
                    "RULE_SEQ_ID" : 103,
                    "OWNER_SEQ_ID" : 10004,
                    "ORDER2_FL" : "N",
                    "ANY_FL" : "N",
                    "OPOK_NB" : null,
                    "TB_ID" : 38,
                    "OSB_ID" : 6901,
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "",
                    "END_DT" : "",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-10-09 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }, {
                    "_id_" : 102,
                    "RULE_SEQ_ID" : 102,
                    "OWNER_SEQ_ID" : 10003,
                    "ORDER2_FL" : "Y",
                    "ANY_FL" : "N",
                    "OPOK_NB" : 9001,
                    "TB_ID" : null,
                    "OSB_ID" : null,
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "",
                    "END_DT" : "2015-10-31 00:00:00",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-10-09 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }, {
                    "_id_" : 101,
                    "RULE_SEQ_ID" : 101,
                    "OWNER_SEQ_ID" : 10003,
                    "ORDER2_FL" : "N",
                    "ANY_FL" : "N",
                    "OPOK_NB" : 1001,
                    "TB_ID" : null,
                    "OSB_ID" : null,
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "2015-10-01 00:00:00",
                    "END_DT" : "",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-10-09 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }, {
                    "_id_" : 105,
                    "RULE_SEQ_ID" : 105,
                    "OWNER_SEQ_ID" : 10002,
                    "ORDER2_FL" : "Y",
                    "ANY_FL" : "Y",
                    "OPOK_NB" : null,
                    "TB_ID" : 49,
                    "OSB_ID" : null,
                    "ACTIVE_FL" : "Y",
                    "START_DT" : "",
                    "END_DT" : "",
                    "CREATED_BY" : "OFMSUPERVISOR",
                    "CREATED_DATE" : "2015-10-09 00:00:00",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }]
            }
        );
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;
