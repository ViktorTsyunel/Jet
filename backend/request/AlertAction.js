var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action === 'readById'|| action === 'readByFilter') {
        response.send({
            "success" : true,
            "totalCount" : 9,
            "data" : [{
                "ACTVY_TYPE_CD" : "RF_SETOPOK",
                "ACTVY_TYPE_NM" : "\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u043A\u043E\u0434(\u044B) \u0432\u0438\u0434\u0430 \u043E\u043F\u0435\u0440\u0430\u0446\u0438\u0438",
                "ALERT_EDIT_FL" : "Y",
                "REQ_CMMNT_FL" : "Y"
            }, {
                "ACTVY_TYPE_CD" : "RF_RUN",
                "ACTVY_TYPE_NM" : "\u041F\u0440\u043E\u0432\u0435\u0440\u0438\u0442\u044C \u043E\u043F\u0435\u0440\u0430\u0446\u0438\u044E (\u0432\u044B\u044F\u0432\u043B\u0435\u043D\u0438\u0435)",
                "ALERT_EDIT_FL" : "N",
                "REQ_CMMNT_FL" : "N"
            }, {
                "ACTVY_TYPE_CD" : "RF_OES#",
                "ACTVY_TYPE_NM" : "\u0417\u0430\u043F\u0440\u043E\u0441\u0438\u0442\u044C \u0437\u0430\u043C\u0435\u043D\u0443 \u041E\u042D\u0421",
                "ALERT_EDIT_FL" : "N",
                "REQ_CMMNT_FL" : "N"
            }, {
                "ACTVY_TYPE_CD" : "RF_CANCEL#",
                "ACTVY_TYPE_NM" : "\u0417\u0430\u043F\u0440\u043E\u0441\u0438\u0442\u044C \u0443\u0434\u0430\u043B\u0435\u043D\u0438\u0435 \u041E\u042D\u0421",
                "ALERT_EDIT_FL" : "N",
                "REQ_CMMNT_FL" : "Y"
            }, {
                "ACTVY_TYPE_CD" : "RF_IGNORE#",
                "ACTVY_TYPE_NM" : "\u0417\u0430\u043C\u0435\u043D\u0430/\u0443\u0434\u0430\u043B\u0435\u043D\u0438\u0435 \u041E\u042D\u0421 \u043D\u0435 \u0442\u0440\u0435\u0431\u0443\u0435\u0442\u0441\u044F",
                "ALERT_EDIT_FL" : "N",
                "REQ_CMMNT_FL" : "N"
            }, {
                "ACTVY_TYPE_CD" : "MTS003",
                "ACTVY_TYPE_NM" : "\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u043E\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0435\u043D\u043D\u043E\u0433\u043E",
                "ALERT_EDIT_FL" : "Y",
                "REQ_CMMNT_FL" : "N"
            }, {
                "ACTVY_TYPE_CD" : "MTS110",
                "ACTVY_TYPE_NM" : "\u041E\u0436\u0438\u0434\u0430\u0435\u0442\u0441\u044F \u043E\u0442\u0432\u0435\u0442",
                "ALERT_EDIT_FL" : "N",
                "REQ_CMMNT_FL" : "N"
            }, {
                "ACTVY_TYPE_CD" : "MTS018",
                "ACTVY_TYPE_NM" : "\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u0441\u0440\u043E\u043A \u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0438",
                "ALERT_EDIT_FL" : "Y",
                "REQ_CMMNT_FL" : "N"
            }, {
                "ACTVY_TYPE_CD" : "MTS112",
                "ACTVY_TYPE_NM" : "\u0422\u0440\u0435\u0431\u0443\u0435\u0442\u0441\u044F \u0434\u043E\u043F. \u0430\u043D\u0430\u043B\u0438\u0437",
                "ALERT_EDIT_FL" : "N",
                "REQ_CMMNT_FL" : "N"
            }],
            "alertCount" : 103
        });
    } else if (action == 'execById' || action == 'execByFilter') {
        response.send({
            "success" : true,
            "message" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \"\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u043A\u043E\u0434(\u044B) \u0432\u0438\u0434\u0430 \u043E\u043F\u0435\u0440\u0430\u0446\u0438\u0438\" \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u043E \u0434\u043B\u044F 5 \u043E\u043F\u0435\u0440\u0430\u0446\u0438\u0439\r\n\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \"\u0417\u0430\u043F\u0440\u043E\u0441\u0438\u0442\u044C \u0437\u0430\u043C\u0435\u043D\u0443 \u041E\u042D\u0421\" \u043D\u0435 \u043F\u0440\u0438\u043C\u0435\u043D\u0438\u043C\u043E (\u0438 \u043D\u0435 \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u043E) \u0434\u043B\u044F \u043E\u043F\u0435\u0440\u0430\u0446\u0438\u0438 \u21161280001\r\n\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \"\u0417\u0430\u043F\u0440\u043E\u0441\u0438\u0442\u044C \u0437\u0430\u043C\u0435\u043D\u0443 \u041E\u042D\u0421\" \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u043E \u0434\u043B\u044F 4 \u043E\u043F\u0435\u0440\u0430\u0446\u0438\u0439",
            "alertIds": [ 4297, 4298, 4299, 4300, 4366 ]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
