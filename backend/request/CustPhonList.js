var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId' || action == 'readByTrxnId') {
        response.send({
            "success" : true,
            "totalCount" : 3,
            "data" : [{
                "USAGE_CD" : "\u041F\u0440\u043E\u0447\u0438\u0439",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "PHON_NB" : "83422517564",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_CD" : "EKS|523110287823",
                "SRC_CHANGE_DT" : "25.11.2015 20:24:55",
                "SRC_USER_NM" : "",
                "CUST_PHON_SEQ_ID" : 7967594
            }, {
                "USAGE_CD" : "\u041F\u0440\u043E\u0447\u0438\u0439",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "PHON_NB" : "83422502333",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_CD" : "EKS|180404278245",
                "SRC_CHANGE_DT" : "25.11.2015 20:24:55",
                "SRC_USER_NM" : "",
                "CUST_PHON_SEQ_ID" : 7967593
            }, {
                "USAGE_CD" : "\u041F\u0440\u043E\u0447\u0438\u0439",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "PHON_NB" : "\u043D\u0435\u0442",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_CD" : "EKS|5738298947",
                "SRC_CHANGE_DT" : "25.11.2015 20:24:55",
                "SRC_USER_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0421\u0438\u0434\u043E\u0440 \u041F\u0435\u0442\u0440\u043E\u0432\u0438\u0447",
                "CUST_PHON_SEQ_ID" : 7950905
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
