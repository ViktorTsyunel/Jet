var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
            "success" : true,
            "totalCount" : 2,
            "data" : [{
                "IMP_TYPE_CD" : "CBRF_BNKDEL",
                "IMP_TYPE_NM" : "\u0418\u043C\u043F\u043E\u0440\u0442 \u0441\u043F\u0440\u0430\u0432\u043E\u0447\u043D\u0438\u043A\u0430 \u0443\u0434\u0430\u043B\u0435\u043D\u043D\u044B\u0445 (\u043D\u0435\u0434\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0445) \u0411\u0418\u041A \u0426\u0411 \u0420\u0424",
                "ACTIVE_FL" : "Y",
                "FILE_NM" : "test.dbf",
                "ACTUAL_DT" : "2016-04-11 00:00:00",
                "CONTROL_DT" : "2016-04-11 00:00:00",
                "LOAD_DT" : "2016-04-13 11:28:11",
                "LOAD_STATUS" : "W",
                "LOAD_STATUS_TX" : "\u041F\u0440\u0435\u0434\u0443\u043F\u0440\u0435\u0436\u0434\u0435\u043D\u0438\u0435",
                "ERR_MSG_TX" : "\u0414\u0430\u0442\u0430 \u0430\u043A\u0442\u0443\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0438 \u0444\u0430\u0439\u043B\u0430 [11.04.2016] \u043C\u0435\u043D\u044C\u0448\u0435 \u0443\u043A\u0430\u0437\u0430\u043D\u043D\u043E\u0439 \u0434\u0430\u0442\u044B [15.04.2016].",
                "LOAD_QTY" : 18283,
                "CONTROL_QTY" : 2593,
                "NOTE_TX" : "",
                "LOADED_BY" : "OFMSUPERVISOR"
            }, {
                "IMP_TYPE_CD" : "CBRF_BNKSEEK",
                "IMP_TYPE_NM" : "\u0418\u043C\u043F\u043E\u0440\u0442 \u0441\u043F\u0440\u0430\u0432\u043E\u0447\u043D\u0438\u043A\u0430 \u0434\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0445 \u0411\u0418\u041A \u0426\u0411 \u0420\u0424",
                "ACTIVE_FL" : "Y",
                "FILE_NM" : "test.dbf",
                "ACTUAL_DT" : "2016-04-08 00:00:00",
                "CONTROL_DT" : "2016-04-08 00:00:00",
                "LOAD_DT" : "2016-04-12 13:40:41",
                "LOAD_STATUS" : "O",
                "LOAD_STATUS_TX" : "\u0423\u0441\u043F\u0435\u0448\u043D\u043E",
                "ERR_MSG_TX" : "",
                "LOAD_QTY" : 2593,
                "CONTROL_QTY" : 2593,
                "NOTE_TX" : "",
                "LOADED_BY" : "OFMSUPERVISOR"
            }]
        });
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
