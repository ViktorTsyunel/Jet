var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				return {
					"_id_": Math.floor(Math.random() * 100000),
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send(
            {
                "success" : true,
                "totalCount" : 2,
                "data" : [{
                    "_id_" : "AMANALYST1",
                    "ROLE_CD" : "AMANALYST1",
                    "ROLE_NM" : "\u041A\u043E\u043D\u0442\u0440\u043E\u043B\u0435\u0440"
                }, {
                    "_id_" : "AMSUPVISR",
                    "ROLE_CD" : "AMSUPVISR",
                    "ROLE_NM" : "\u0421\u0443\u043F\u0435\u0440\u0432\u0438\u0437\u043E\u0440"
                }]
            }
        );
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;
