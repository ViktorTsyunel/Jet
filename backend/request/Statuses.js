var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
			success: true,
			data: [
				{
					"v_$0": 23,
					"_id_": "RF_REPEAT",
					"STATUS_DISPL_ORDER_NB": 10,
					"STATUS_CD": "RF_REPEAT",
					"CODE_DISP_TX": "Повторное рассмотрение"
				},
				{
					"v_$0": 4,
					"_id_": "RA",
					"STATUS_DISPL_ORDER_NB": 10,
					"STATUS_CD": "RA",
					"CODE_DISP_TX": "Новый ответственный"
				},
				{
					"v_$0": 2,
					"_id_": "NW",
					"STATUS_DISPL_ORDER_NB": 40,
					"STATUS_CD": "NW",
					"CODE_DISP_TX": "Новая"
				},
				{
					"v_$0": 3,
					"_id_": "OP",
					"STATUS_DISPL_ORDER_NB": 50,
					"STATUS_CD": "OP",
					"CODE_DISP_TX": "В работе"
				},
				{
					"v_$0": 1,
					"_id_": "FL",
					"STATUS_DISPL_ORDER_NB": 60,
					"STATUS_CD": "FL",
					"CODE_DISP_TX": "Доп. анализ"
				},
				{
					"v_$0": 15,
					"_id_": "RF_OES",
					"STATUS_DISPL_ORDER_NB": 110,
					"STATUS_CD": "RF_OES",
					"CODE_DISP_TX": "Подлежит контролю"
				},
				{
					"v_$0": 11,
					"_id_": "RF_DONE",
					"STATUS_DISPL_ORDER_NB": 111,
					"STATUS_CD": "RF_DONE",
					"CODE_DISP_TX": "Отправлена"
				},
				{
					"v_$0": 5,
					"_id_": "RF_CANCEL",
					"STATUS_DISPL_ORDER_NB": 112,
					"STATUS_CD": "RF_CANCEL",
					"CODE_DISP_TX": "НЕ подлежит контролю"
				},
				{
					"v_$0": 8,
					"_id_": "RF_DELETE-",
					"STATUS_DISPL_ORDER_NB": 120,
					"STATUS_CD": "RF_DELETE-",
					"CODE_DISP_TX": "Удаление"
				},
				{
					"v_$0": 21,
					"_id_": "RF_RA-",
					"STATUS_DISPL_ORDER_NB": 121,
					"STATUS_CD": "RF_RA-",
					"CODE_DISP_TX": "Удаление (новый ответственный)"
				},
				{
					"v_$0": 18,
					"_id_": "RF_OP-",
					"STATUS_DISPL_ORDER_NB": 122,
					"STATUS_CD": "RF_OP-",
					"CODE_DISP_TX": "Удаление (в работе)"
				},
				{
					"v_$0": 13,
					"_id_": "RF_FL-",
					"STATUS_DISPL_ORDER_NB": 123,
					"STATUS_CD": "RF_FL-",
					"CODE_DISP_TX": "Удаление (доп. анализ)"
				},
				{
					"v_$0": 24,
					"_id_": "RF_REPEAT+",
					"STATUS_DISPL_ORDER_NB": 130,
					"STATUS_CD": "RF_REPEAT+",
					"CODE_DISP_TX": "!Отправлена (повторное рассмотрение)"
				},
				{
					"v_$0": 20,
					"_id_": "RF_RA+",
					"STATUS_DISPL_ORDER_NB": 131,
					"STATUS_CD": "RF_RA+",
					"CODE_DISP_TX": "!Отправлена (повторное рассмотрение, новый ответственный)"
				},
				{
					"v_$0": 17,
					"_id_": "RF_OP+",
					"STATUS_DISPL_ORDER_NB": 132,
					"STATUS_CD": "RF_OP+",
					"CODE_DISP_TX": "!Отправлена (повторное рассмотрение, в работе)"
				},
				{
					"v_$0": 12,
					"_id_": "RF_FL+",
					"STATUS_DISPL_ORDER_NB": 133,
					"STATUS_CD": "RF_FL+",
					"CODE_DISP_TX": "!Отправлена (повторное рассмотрение, доп. анализ)"
				},
				{
					"v_$0": 16,
					"_id_": "RF_OES+",
					"STATUS_DISPL_ORDER_NB": 134,
					"STATUS_CD": "RF_OES+",
					"CODE_DISP_TX": "Запрос замены"
				},
				{
					"v_$0": 6,
					"_id_": "RF_CANCEL+",
					"STATUS_DISPL_ORDER_NB": 135,
					"STATUS_CD": "RF_CANCEL+",
					"CODE_DISP_TX": "Запрос удаления (повторное рассмотрение)"
				},
				{
					"v_$0": 9,
					"_id_": "RF_DELETE=",
					"STATUS_DISPL_ORDER_NB": 140,
					"STATUS_CD": "RF_DELETE=",
					"CODE_DISP_TX": "!Отправлена (удаление)"
				},
				{
					"v_$0": 22,
					"_id_": "RF_RA=",
					"STATUS_DISPL_ORDER_NB": 141,
					"STATUS_CD": "RF_RA=",
					"CODE_DISP_TX": "!Отправлена (удаление, новый ответственный)"
				},
				{
					"v_$0": 19,
					"_id_": "RF_OP=",
					"STATUS_DISPL_ORDER_NB": 142,
					"STATUS_CD": "RF_OP=",
					"CODE_DISP_TX": "!Отправлена (удаление, в работе)"
				},
				{
					"v_$0": 14,
					"_id_": "RF_FL=",
					"STATUS_DISPL_ORDER_NB": 143,
					"STATUS_CD": "RF_FL=",
					"CODE_DISP_TX": "!Отправлена (удаление, доп. анализ)"
				},
				{
					"v_$0": 7,
					"_id_": "RF_CANCEL=",
					"STATUS_DISPL_ORDER_NB": 144,
					"STATUS_CD": "RF_CANCEL=",
					"CODE_DISP_TX": "Запрос удаления (удаление)"
				},
				{
					"v_$0": 10,
					"_id_": "RF_DLTD-",
					"STATUS_DISPL_ORDER_NB": 190,
					"STATUS_CD": "RF_DLTD-",
					"CODE_DISP_TX": "Удалена"
				}
			]
		});
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
