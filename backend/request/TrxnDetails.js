var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId' || action == 'scope') {
        response.send({
            "success" : true,
            "totalCount" : 1,
            "data" : [{
                "TRXN_DT" : "11.12.2013",
                "TRXN_DOC_ID" : "7543",
                "TRXN_DOC_DT" : "11.12.2013",
                "OP_CAT_CD" : "\u0411\u0435\u0437\u043D\u0430\u043B\u0438\u0447\u043D\u044B\u0435",
                "TRXN_DESC" : "\u0426\u0435\u043B.\u0441\u0440-\u0432\u0430 \u041E\u041C\u0421 \u043D\u0430 \u043E\u043F\u043B\u0430\u0442\u0443 \u043C\u0435\u0434.\u043F\u043E\u043C\u043E\u0449\u0438 \u0437\u0430 \u0434\u0435\u043A\u0430\u0431\u0440\u044C13\u0433. (\u0430\u0432\u0430\u043D\u0441) \u0414\u043E\u0433.N 50313/76-53193 \u043E\u0442 01.02.13 \u0411\u0435\u0437 \u041D\u0414\u0421.",
                "TRXN_CRNCY_AM" : 680000,
                "TRXN_BASE_AM" : 680000,
                "TRXN_CRNCY_CD" : "RUR",
                "TRXN_CRNCY_RATE_AM" : 65.1234,
                "TRXN_CRNCY_OLD_RATE_FL" : "Y",
                "ORIG_NM" : "\u0417\u0410\u041A\u0420\u042B\u0422\u041E\u0415 \u0410\u041A\u0426\u0418\u041E\u041D\u0415\u0420\u041D\u041E\u0415 \u041E\u0411\u0429\u0415\u0421\u0422\u0412\u041E \"\u041C\u0415\u0414\u0418\u0426\u0418\u041D\u0421\u041A\u0410\u042F \u0410\u041A\u0426\u0418\u041E\u041D\u0415\u0420\u041D\u0410\u042F \u0421\u0422\u0420\u0410\u0425\u041E\u0412\u0410\u042F \u041A\u041E\u041C\u041F\u0410\u041D\u0418\u042F\"",
                "ORIG_INN_NB" : "7702030351",
                "ORIG_KPP_NB" : "774401001",
                "ORIG_ACCT_NB" : "40701 810 6 3818 0120034",
                "SEND_INSTN_NM" : "\u041E\u0410\u041E \u0421\u0443\u043F\u0435\u0440\u0431\u0430\u043D\u043A \u0422\u0430\u0432\u0440\u0438\u0447\u0435\u0441\u043A\u0438\u0439",
                "SEND_INSTN_CNTRY_CD" : "RU",
                "SEND_INSTN_ID" : "044525225",
                "SEND_INSTN_ACCT_ID" : "30101 810 4 0000 0000225",
                "DEBIT_CD" : "40701 810 6 3818 0120034",
                "BENEF_NM" : "\u0423\u0424\u041A \u043F\u043E \u041C\u043E\u0441\u043A\u043E\u0432\u0441\u043A\u043E\u0439 \u043E\u0431\u043B\u0430\u0441\u0442\u0438 (\u041C\u0423\u0417 \"\u0421\u0421\u041C\u041F\") \u043B/\u0441 22486X30080",
                "BENEF_INN_NB" : "5032057055",
                "BENEF_KPP_NB" : "503201001",
                "BENEF_ACCT_NB" : "40701 810 3 0000 1000073",
                "RCV_INSTN_NM" : "\u0412\u0442\u043E\u0440\u0447\u0435\u0440\u043C\u0435\u0442\u0431\u0440\u0435\u0434\u0431\u0430\u043D\u043A",
                "RCV_INSTN_CNTRY_CD" : "RU",
                "RCV_INSTN_ID" : "044583001",
                "RCV_INSTN_ACCT_ID" : "30101 810 4 0000 0000999",
                "CREDIT_CD" : "30301 810 4 3800 6000000",
                "NSI_OP_ID" : "4321 - \u041F\u043E\u043A\u0443\u043F\u043A\u0430 \u0447\u0435\u0433\u043E-\u043D\u0438-\u043F\u043E\u043F\u0430\u0434\u044F \u0443 \u0431\u0430\u043D\u043A\u0430",
                "TRXN_CONV_CRNCY_CD" : "FFF",
                "TRXN_CONV_AM" : 1234.98,
                "TRXN_CONV_BASE_AM" : 1234567.98,
                "TRXN_CASH_SYMBOLS_TX" : "53, 54, 65",
                "BANK_CARD_ID_NB" : 12345678901234567890,
                "BANK_CARD_TRXN_DT" : "18.11.2015 17:43:28",
                "ATM_ID" : "123456",
                "MERCHANT_NO" : "780000002724",
                "TB_ID" : 38,
                "OSB_ID" : 7970,
                "TB_NM" : "\u0421\u0431\u0435\u0440\u0435\u0433\u0430\u0442\u0435\u043B\u044C\u043D\u044B\u0439 \u0431\u0430\u043D\u043A",
                "OSB_NM" : "\u0412\u0415\u0420\u041D\u0410\u0414\u0421\u041A\u041E\u0415 \u041E\u0421\u0411 7970",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "18.11.2015 17:43:28",
                "SRC_USER_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432\u0430 \u0418\u043D\u043D\u0430 \u041F\u0435\u0442\u0440\u043E\u0432\u043D\u0430",
                "SRC_CD" : "EKS|1334466142085",
                "CANCELED_FL" : "Y",
                "TRXN_DESC_LEX" : "\u043C\u0435\u0442\u0430\u043B,\u043A\u0443\u0440\u0441,\u043E\u0431\u0435\u0437\u043B\u0438\u0447,\u043C\u0435\u0442\u0430\u043B,\u043E\u0431\u0435\u0437\u043B\u0438\u0447,\u0441\u0447\u0435\u0442,\u043E\u043C\u0441,0\u043C\u0441,omc,\u0430\u043B\u043C\u0430\u0437,\u0438\u0437\u0443\u043C\u0440\u0443\u0434,\u0440\u0443\u0431\u0438\u043D,\u0430\u043B\u0435\u043A\u0441\u0430\u043D\u0434\u0440\u0438\u0442,\u0436\u0435\u043C\u0447\u0443\u0433,\u043F\u043B\u0430\u0442\u0438\u043D,\u0434\u0440\u0430\u0433,\u043C\u0435\u0442,\u044E\u0432\u0435\u043B\u0438\u0440,\u0438\u0440\u0438\u0434\u0438,\u043E\u0441\u043C\u0438,\u0434\u0440\u0430\u0433\u043E\u0446\u0435\u043D,\u0446\u0435\u043D\u043D\u043E\u0441\u0442,\u0430\u043B\u043C\u0430\u0437,\u0431\u0440\u0438\u043B\u043B,\u0441\u0430\u043F\u0444\u0438\u0440,\u0437\u043E\u043B\u043E\u0442,\u0441\u0435\u0440\u0435\u0431\u0440,\u043F\u0430\u043B\u043B\u0430\u0434,\u0440\u043E\u0434\u0438,\u0440\u0443\u0442\u0435\u043D\u0438|\u0441\u0441\u0443\u0434,\u0444\u0438\u043D\u0430\u043D\u0441\u043E\u0432\u0430\u044F,\u043F\u043E\u043C\u043E\u0449\u044C,\u0437\u0430\u0439\u043C,\u0437\u0430\u0435\u043C,\u0437\u0430\u0438\u043C,\u043A\u0440\u0435\u0434,\u043F\u043E\u043C\u043E\u0449,\u0444\u0438\u043D,\u043F\u043E\u043C,loan,zaim,3\u0430\u0439\u043C,\u0437\u0430\u0451\u043C,\u0431\u04353,\u043F\u0440\u043E\u0446,\u0431\u04353\u043F\u0440\u043E\u0446,\u0431\u0435\u0437,\u043F\u0440\u043E\u0446,\u0431\u0435\u0437\u043F\u0440\u043E\u0446,\u0431\u0435\u0441,\u043F\u0440\u043E\u0446,\u0431\u0435\u0441\u043F\u0440\u043E\u0446,^\u0432\u0437\u0430\u0438\u043C",
                "ORIG_LEX" : "\u043E\u0431\u043B",
                "BENEF_LEX" : "\u043E\u0431\u0449"
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
