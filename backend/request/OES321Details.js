var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'read' || action == 'scope' || action == 'readByOesId') {
		response.send({
            "success" : true,
            "totalCount" : 1,
            "data" : [{
                "_id_" : 81,
                "OES_321_ID" : 81,
                "REVIEW_ID" : 61,
                "OES_321_ORG_ID" : 81,
                "SEND_FL" : 1,
                "CHECK_STATUS" : "E",
                "CHECK_DT" : "2014-09-05 00:00:00",
                "SEND_DT" : "",
                "NUMB_P" : null,
                "DATE_P" : "",
                "ACTION" : "1",
                "REGN" : "1481",
                "ND_KO" : "7707083893",
                "KTU_S" : "45",
                "BIK_S" : "44525225",
                "NUMBF_S" : "0",
                "BRANCH" : "0",
                "KTU_SS" : "0",
                "BIK_SS" : "0",
                "NUMBF_SS" : "0",
                "TEL" : "+79261234567",
                "DATA" : "2014-09-05 00:00:00",
                "DATE_S" : "2014-09-05 00:00:00",
                "TERROR" : "0",
                "VO" : 4001,
                "DOP_V" : "4005,4004",
                "SUMME" : 2669000,
                "SUM" : 2669000,
                "CURREN" : "620",
                "PRIM_1" : "\u041F\u0440\u0438\u043E\u0431\u0440\u0435\u0442\u0435\u043D\u0438\u0435 \u0444\u0438\u0437\u0438\u0447\u0435\u0441\u043A\u0438\u043C \u043B\u0438\u0446\u043E\u043C \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043A\u0430\u0442\u043E\u0432 \u0421\u0411 \u0420\u0424",
                "PRIM_2" : "0",
                "NUM_PAY_D" : 0,
                "DATE_PAY_D" : "2014-09-05 00:00:00",
                "METAL" : "99",
                "PRIZ6001" : "0",
                "B_PAYER" : "0",
                "B_RECIP" : "0",
                "PART" : "0",
                "DESCR_1" : "\u0421\u0426_1695163 \u0441\u0443\u043C\u043C\u0430: 2669000",
                "DESCR_2" : "0",
                "CURREN_CON" : "0",
                "SUM_CON" : 0,
                "PRIZ_SD" : "0",
                "MANUAL_CMNT" : ""
            }]
        });
    } else if (action == 'readCheckStatus') {
        response.send({
            "success" : true,
            "totalCount" : 1,
            "data" : [{
                "_id_" : 81,
                "OES_321_ID" : 81,
                "CHECK_DT" : "2016-02-05 18:24:39",
                "CHECK_STATUS" : "E"
            }]
        });
	} else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
	}
}
exports.response = response;
