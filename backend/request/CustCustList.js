var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId' || action == 'readByTrxnId') {
        response.send({
            "success" : true,
            "totalCount" : 4,
            "data" : [{
                "DECODE(CC.RF_CUST_SEQ_ID,C.RF_IND_CUST_SEQ_ID,'\u0424\u041B:')||RTP.RLSHP_TP_NM" : "\u0420\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u044C \u043E\u0440\u0433\u0430\u043D\u0438\u0437\u0430\u0446\u0438\u0438",
                "JOB_TITL_NM" : "\u0433\u0435\u043D\u0435\u0440\u0430\u043B\u044C\u043D\u044B\u0439 \u0434\u0438\u0440\u0435\u043A\u0442\u043E\u0440",
                "FULL_NM" : "\u0416\u0418\u0413\u0423\u041D\u041E\u0412 \u041D\u0418\u041A\u041E\u041B\u0410\u0419 \u0412\u0418\u041A\u0422\u041E\u0420\u041E\u0412\u0418\u0427",
                "BIRTH_DT" : "",
                "RLSHP_REASON_TX" : "",
                "STATUS_TX" : "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "START_DT" : "21.04.2014",
                "END_DT" : "21.04.2019",
                "END_REASON_TX" : "",
                "SRC_CD" : "EKSP|2801416298848",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_CUST_SEQ_ID" : 441938
            }, {
                "DECODE(CC.RF_CUST_SEQ_ID,C.RF_IND_CUST_SEQ_ID,'\u0424\u041B:')||RTP.RLSHP_TP_NM" : "\u0414\u043E\u043B\u0436\u043D\u043E\u0441\u0442\u043D\u043E\u0435 \u043B\u0438\u0446\u043E (\u0438\u043D\u043E\u0435)",
                "JOB_TITL_NM" : "\u0411\u0435\u043D\u0435\u0444\u0438\u0446\u0438\u0430\u0440",
                "FULL_NM" : "\u0416\u0418\u0413\u0423\u041D\u041E\u0412 \u041D\u0418\u041A\u041E\u041B\u0410\u0419 \u0412\u0418\u041A\u0422\u041E\u0420\u041E\u0412\u0418\u0427",
                "BIRTH_DT" : "",
                "RLSHP_REASON_TX" : "",
                "STATUS_TX" : "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "START_DT" : "",
                "END_DT" : "",
                "END_REASON_TX" : "",
                "SRC_CD" : "EKSP|2801563642639",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_CUST_SEQ_ID" : 441937
            }, {
                "DECODE(CC.RF_CUST_SEQ_ID,C.RF_IND_CUST_SEQ_ID,'\u0424\u041B:')||RTP.RLSHP_TP_NM" : "\u0414\u043E\u043B\u0436\u043D\u043E\u0441\u0442\u043D\u043E\u0435 \u043B\u0438\u0446\u043E (\u0438\u043D\u043E\u0435)",
                "JOB_TITL_NM" : "\u0414\u043E\u0432\u0435\u0440\u0435\u043D\u043D\u043E\u0435 \u043B\u0438\u0446\u043E",
                "FULL_NM" : "\u041C\u0410\u0420\u041A\u041E\u0412\u0410 \u0410\u041D\u0410\u0421\u0422\u0410\u0421\u0418\u042F \u0411\u0410\u0422\u042B\u0420\u041E\u0412\u041D\u0410",
                "BIRTH_DT" : "1988-07-10 00:00:00",
                "RLSHP_REASON_TX" : "\u0414\u043E\u0432\u0435\u0440\u0435\u043D\u043D\u043E\u0441\u0442\u044C \u2116 123 \u043E\u0442 14.09.2014",
                "STATUS_TX" : "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "START_DT" : "09.06.2014",
                "END_DT" : "09.06.2015",
                "END_REASON_TX" : "\u041F\u0440\u0435\u043A\u0440\u0430\u0449\u0435\u043D\u0438\u0435 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044F \u0434\u043E\u0432\u0435\u0440\u0435\u043D\u043D\u043E\u0441\u0442\u0438",
                "SRC_CD" : "EKSP|2809589726639",
                "SRC_CHANGE_DT" : "26.11.2015 16:33:34",
                "SRC_USER_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0421\u0438\u0434\u043E\u0440 \u041F\u0435\u0442\u0440\u043E\u0432\u0438\u0447",
                "CUST_CUST_SEQ_ID" : 441939
            }, {
                "DECODE(CC.RF_CUST_SEQ_ID,C.RF_IND_CUST_SEQ_ID,'\u0424\u041B:')||RTP.RLSHP_TP_NM" : "\u0414\u043E\u043B\u0436\u043D\u043E\u0441\u0442\u043D\u043E\u0435 \u043B\u0438\u0446\u043E (\u0438\u043D\u043E\u0435)",
                "JOB_TITL_NM" : "\u0414\u043E\u0432\u0435\u0440\u0435\u043D\u043D\u043E\u0435 \u043B\u0438\u0446\u043E",
                "FULL_NM" : "\u041C\u0410\u0420\u041A\u041E\u0412\u0410 \u0410\u041D\u0410\u0421\u0422\u0410\u0421\u0418\u042F \u0411\u0410\u0422\u042B\u0420\u041E\u0412\u041D\u0410",
                "BIRTH_DT" : "",
                "RLSHP_REASON_TX" : "",
                "STATUS_TX" : "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "START_DT" : "09.06.2014",
                "END_DT" : "09.06.2015",
                "END_REASON_TX" : "",
                "SRC_CD" : "EKSP|2808042051166",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_CUST_SEQ_ID" : 441723
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
