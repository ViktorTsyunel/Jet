var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				return {
					"_id_": Math.floor(Math.random() * 100000),
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send(
            {
                "success": true,
                "totalCount": 17,
                "data": [{
                    "_id_": 69,
                    "OES_321_ORG_ID": 69,
                    "TB_CODE": 49,
                    "TEL": "+79261234560",
                    "TB_NAME": "\u0417\u0430\u043F\u0430\u0434\u043D\u043E-\u0423\u0440\u0430\u043B\u044C\u0441\u043A\u0438\u0439 \u0431\u0430\u043D\u043A",
                    "REGN": "1481",
                    "ND_KO": "7707083893",
                    "KTU_S": "45",
                    "BIK_S": "44525225",
                    "NUMBF_S": "0",
                    "KTU_SS": "0",
                    "BIK_SS": "045773603",
                    "NUMBF_SS": "0",
                    "ACTIVE_FL": "Y",
                    "START_DT": "2010-01-01 00:00:00",
                    "END_DT": "2099-01-01 00:00:00",
                    "CREATED_BY": "\u0421\u0438\u0441\u0442\u0435\u043C\u0430",
                    "CREATED_DATE": "2015-11-30 16:51:04",
                    "MODIFIED_BY": "OFMSUPERVISOR",
                    "MODIFIED_DATE": "2015-12-21 14:36:08",
                    "BRANCH_FL": "Y"
                }]
            }
        );
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;
