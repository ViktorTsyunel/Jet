var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId' || action == 'readByTrxnId') {
        response.send({
            "success" : true,
            "totalCount" : 3,
            "data" : [{
                "USAGE_CD" : "\u0430\u0434\u0440\u0435\u0441 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438",
                "COUNTRY_CD" : "\u0420\u041E\u0421\u0421\u0418\u042F",
                "STATE_NM" : "\u043A\u0440\u0430\u0439 \u041F\u0435\u0440\u043C\u0441\u043A\u0438\u0439",
                "DISTRICT_NM" : "",
                "CITY_NM" : "\u0413 \u041F\u0415\u0420\u041C\u042C",
                "SETTLM_NM" : "",
                "STREET_NAME_TX" : "\u041A\u041E\u0421\u0422\u042B\u0427\u0415\u0412\u0410",
                "HOUSE_NUMBER_TX" : "42",
                "BUILDING_NUMBER_TX" : "",
                "FLAT_NUMBER_TX" : "114",
                "STATUS_TX" : "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "25.11.2015 17:15:17",
                "SRC_USER_NM" : "",
                "CUST_ADDR_SEQ_ID" : 13944336
            }, {
                "USAGE_CD" : "\u0424\u041B: \u0430\u0434\u0440\u0435\u0441 \u043F\u0440\u0435\u0431\u044B\u0432\u0430\u043D\u0438\u044F",
                "COUNTRY_CD" : "\u0420\u041E\u0421\u0421\u0418\u042F",
                "STATE_NM" : "\u043A\u0440\u0430\u0439 \u041F\u0435\u0440\u043C\u0441\u043A\u0438\u0439",
                "DISTRICT_NM" : "\u0414\u0417\u0415\u0420\u0416\u0418\u041D\u0421\u041A\u0418\u0419",
                "CITY_NM" : "\u0413 \u041F\u0415\u0420\u041C\u042C",
                "SETTLM_NM" : "\u043F\u043E\u0441. \u041A\u043B\u044E\u043A\u043E\u0432\u043E",
                "STREET_NAME_TX" : "\u041A\u041E\u0421\u0422\u042B\u0427\u0415\u0412\u0410",
                "HOUSE_NUMBER_TX" : "42",
                "BUILDING_NUMBER_TX" : "1",
                "FLAT_NUMBER_TX" : "114",
                "STATUS_TX" : "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "25.11.2015 17:15:17",
                "SRC_USER_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0421\u0438\u0434\u043E\u0440 \u041F\u0435\u0442\u0440\u043E\u0432\u0438\u0447",
                "CUST_ADDR_SEQ_ID" : 9872121
            }, {
                "USAGE_CD" : "\u0424\u041B: \u0430\u0434\u0440\u0435\u0441 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438",
                "COUNTRY_CD" : "\u0420\u041E\u0421\u0421\u0418\u042F",
                "STATE_NM" : "\u043A\u0440\u0430\u0439 \u041F\u0435\u0440\u043C\u0441\u043A\u0438\u0439",
                "DISTRICT_NM" : "\u0414\u0417\u0415\u0420\u0416\u0418\u041D\u0421\u041A\u0418\u0419",
                "CITY_NM" : "\u0413 \u041F\u0415\u0420\u041C\u042C",
                "SETTLM_NM" : "",
                "STREET_NAME_TX" : "\u041A\u041E\u0421\u0422\u042B\u0427\u0415\u0412\u0410",
                "HOUSE_NUMBER_TX" : "42",
                "BUILDING_NUMBER_TX" : "",
                "FLAT_NUMBER_TX" : "114",
                "STATUS_TX" : "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "25.11.2015 17:15:17",
                "SRC_USER_NM" : "",
                "CUST_ADDR_SEQ_ID" : 9872005
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
