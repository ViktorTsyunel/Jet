var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
	if (action == 'read') {
		response.send({
	        success: true,
		    totalCount: 1,
			data: [
				{
					"WORK_QTY": 23,
					"NEARDUE_QTY": 0,
					"OVERDUE_QTY": 1
				}
			]
	    });
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;
