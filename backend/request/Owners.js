var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
            "success" : true,
            "totalCount" : 4,
            "data" : [{
                "v_$0" : 3,
                "_id_" : 10003,
                "OWNER_SEQ_ID" : 10003,
                "OWNER_DSPLY_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0418. \u0418.",
                "OWNER_ID" : "IVANOV"
            }, {
                "v_$0" : 2,
                "_id_" : 10004,
                "OWNER_SEQ_ID" : 10004,
                "OWNER_DSPLY_NM" : "\u041F\u0435\u0442\u0440\u043E\u0432 \u041F. \u041F.",
                "OWNER_ID" : "PETROV"
            }, {
                "v_$0" : 1,
                "_id_" : 10005,
                "OWNER_SEQ_ID" : 10005,
                "OWNER_DSPLY_NM" : "\u0421\u0438\u0434\u043E\u0440\u043E\u0432 \u0421. \u0421.",
                "OWNER_ID" : "SIDOROV"
            }, {
                "v_$0" : 4,
                "_id_" : 10002,
                "OWNER_SEQ_ID" : 10002,
                "OWNER_DSPLY_NM" : "\u0421\u0443\u043F\u0435\u0440\u0432\u0438\u0437\u043E\u0440 \u043E\u0442\u0434\u0435\u043B\u0430 \u0424\u041C",
                "OWNER_ID" : "OFMSUPERVISOR"
            }]
        });
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
