var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
            "success" : true,
            "totalCount" : 3,
            "data" : [{
                "_ID_" : 1,
                "OES_321_ID" : 81,
                "ERROR_TEXT" : "\u0417\u043D\u0430\u0447\u0435\u043D\u0438\u0435 \u043F\u043E\u043B\u044F \u043C\u043E\u0436\u0435\u0442 \u0431\u044B\u0442\u044C \u0442\u043E\u043B\u044C\u043A\u043E 1,2,3,4",
                "FIELD_NAME" : "ACTION",
                "CRITICAL" : "1"
            }, {
                "_ID_" : 2,
                "OES_321_ID" : 81,
                "ERROR_TEXT" : "\u0417\u043D\u0430\u0447\u0435\u043D\u0438\u0435 \u043F\u043E\u043B\u044F \u043C\u043E\u0436\u0435\u0442 \u0431\u044B\u0442\u044C \u0442\u043E\u043B\u044C\u043A\u043E 1,0",
                "FIELD_NAME" : "BRANCH",
                "CRITICAL" : "1"
            }, {
                "_ID_" : 3,
                "OES_321_ID" : 81,
                "ERROR_TEXT" : "\u041D\u0435 \u0437\u0430\u043F\u043E\u043B\u0435\u043D\u043D\u043E \u043F\u043E\u043B\u0435, \u0435\u0441\u043B\u0438 \u043F\u043E\u043B\u0435 \u043D\u0435 \u0438\u043C\u0435\u0435\u0442 \u0437\u043D\u0430\u0447\u0435\u043D\u0438\u044F \u0442\u043E \u0434\u043E\u043B\u0436\u0435\u043D \u043F\u043E\u0434\u0441\u0442\u0430\u0432\u043B\u044F\u044C\u0441\u044F \u0441\u0438\u043C\u0432\u043E\u043B '0'",
                "FIELD_NAME" : "AMR_R",
                "CRITICAL" : "2"
            }]
        });
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
