var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				return {
					"_id_": Math.floor(Math.random() * 100000),
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send(
            {
                "success" : true,
                "totalCount" : 1,
                "data" : [{
                    "_ID_" : 104,
                    "OES_321_P_ORG_ID" : 104,
                    "TB_CODE" : 38,
                    "TU" : 1,
                    "PR" : "0",
                    "NAMEU" : "\u041C\u043E\u0441\u043A\u043E\u0432\u0441\u043A\u0438\u0439 \u0411\u0430\u043D\u043A \u041F\u0410\u041E \u0421\u0431\u0435\u0440\u0431\u0430\u043D\u043A",
                    "KODCR" : "64300",
                    "KODCN" : "64300",
                    "KD" : "0",
                    "SD" : "00032537",
                    "RG" : "1027700132195",
                    "ND" : "7707083893",
                    "VD1" : "0",
                    "VD2" : "0",
                    "VD3" : "2099-01-01 00:00:00",
                    "VD4" : "0",
                    "VD5" : "0",
                    "VD6" : "2099-01-01 00:00:00",
                    "VD7" : "2099-01-01 00:00:00",
                    "MC1" : "0",
                    "MC2" : "2099-01-01 00:00:00",
                    "MC3" : "2099-01-01 00:00:00",
                    "GR" : "1991-06-20 00:00:00",
                    "BP" : "0",
                    "VP" : "0",
                    "ACC_B" : "30101810400000000225",
                    "ACC_COR_B" : "0",
                    "AMR_S" : "45",
                    "AMR_R" : "0",
                    "AMR_G" : "\u041C\u043E\u0441\u043A\u0432\u0430",
                    "AMR_U" : "\u0412\u0430\u0432\u0438\u043B\u043E\u0432\u0430",
                    "AMR_D" : "19",
                    "AMR_K" : "0",
                    "AMR_O" : "0",
                    "ADRESS_S" : "45",
                    "ADRESS_R" : "0",
                    "ADRESS_G" : "\u041C\u043E\u0441\u043A\u0432\u0430",
                    "ADRESS_U" : "\u0411\u043E\u043B\u044C\u0448\u0430\u044F \u0410\u043D\u0434\u0440\u043E\u043D\u044C\u0435\u0432\u0441\u043A\u0430\u044F",
                    "ADRESS_D" : "6",
                    "ADRESS_K" : "0",
                    "ADRESS_O" : "0",
                    "NAME_B" : "0",
                    "KODCN_B" : "0",
                    "BIK_B" : "0",
                    "CARD_B" : "0",
                    "NAME_IS" : "0",
                    "BIK_IS" : "0",
                    "NAME_R" : "0",
                    "KODCN_R" : "0",
                    "BIK_R" : "0",
                    "RESERV02" : "0",
                    "NOTE" : "",
                    "ACTIVE_FL" : "1",
                    "START_DT" : "2015-01-01 00:00:00",
                    "END_DT" : "2999-12-01 00:00:00",
                    "CREATED_BY" : "admin",
                    "CREATED_DATE" : "2016-01-14 12:30:33",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }]
            }
        );
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;