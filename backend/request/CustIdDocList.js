var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId' || action == 'readByTrxnId') {
        response.send({
            "success" : true,
            "totalCount" : 6,
            "data" : [{
                "DOC_TYPE_CD" : "\u041F\u0430\u0441\u043F\u043E\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043D\u0438\u043D\u0430 \u0420\u0424",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "SERIES_ID" : "45 13",
                "NO_ID" : "491343",
                "ISSUE_DT" : "20.02.2014",
                "END_DT" : "01.01.2050",
                "ISSUED_BY_TX" : "\u043E\u0443\u0444\u043C\u0441 \u0440\u043E\u0441\u0441\u0438\u0438 \u043F\u043E \u0433 \u043C\u043E\u0441\u043A\u0432\u0430 \u0440\u0430\u0439\u043E\u043D \u043B\u0435\u0444\u043E\u0440\u0442\u043E\u0432\u043E",
                "ISSUE_DPT_CD" : "770-104",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_DOC_SEQ_ID" : 11578443
            }, {
                "DOC_TYPE_CD" : "\u041F\u0430\u0441\u043F\u043E\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043D\u0438\u043D\u0430 \u0420\u0424",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "SERIES_ID" : "57 08",
                "NO_ID" : "286128",
                "ISSUE_DT" : "16.01.2009",
                "END_DT" : "",
                "ISSUED_BY_TX" : "\u041E\u0442\u0434\u0435\u043B\u0435\u043D\u0438\u0435\u043C \u0423\u0424\u041C\u0421 \u0420\u043E\u0441\u0441\u0438\u0438 \u043F\u043E \u041F\u0435\u0440\u043C\u0441\u043A\u043E\u043C\u0443 \u043A\u0440\u0430\u044E \u0432 \u041E\u0441\u0438\u043D\u0441\u043A\u043E\u043C \u0440\u0430\u0439\u043E\u043D\u0435",
                "ISSUE_DPT_CD" : "",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_DOC_SEQ_ID" : 10677133
            }, {
                "DOC_TYPE_CD" : "\u041F\u0430\u0441\u043F\u043E\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043D\u0438\u043D\u0430 \u0420\u0424",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "SERIES_ID" : "57 04",
                "NO_ID" : "274253",
                "ISSUE_DT" : "11.11.2003",
                "END_DT" : "",
                "ISSUED_BY_TX" : "\u0423\u0412\u0414 \u0421\u0412\u0415\u0420\u0414\u041B\u041E\u0412\u0421\u041A\u041E\u0413\u041E \u0420-\u041D\u0410 \u041F\u0415\u0420\u041C\u0418",
                "ISSUE_DPT_CD" : "",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_DOC_SEQ_ID" : 10399724
            }, {
                "DOC_TYPE_CD" : "\u041F\u0430\u0441\u043F\u043E\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043D\u0438\u043D\u0430 \u0420\u0424",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "SERIES_ID" : "57 03",
                "NO_ID" : "569541",
                "ISSUE_DT" : "15.12.2002",
                "END_DT" : "",
                "ISSUED_BY_TX" : "\u041E\u0412\u0414 \u041F\u0415\u0420\u041C\u0421\u041A\u041E\u0413\u041E \u0420\u0410\u0419\u041E\u041D\u0410 \u041F\u0415\u0420\u041C\u0421\u041A\u041E\u0419 \u041E\u0411\u041B\u0410\u0421\u0422\u0418",
                "ISSUE_DPT_CD" : "",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_DOC_SEQ_ID" : 10379314
            }, {
                "DOC_TYPE_CD" : "\u041F\u0430\u0441\u043F\u043E\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043D\u0438\u043D\u0430 \u0420\u0424",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "SERIES_ID" : "94 00",
                "NO_ID" : "254520",
                "ISSUE_DT" : "24.07.2001",
                "END_DT" : "",
                "ISSUED_BY_TX" : "\u0421\u0410\u0420\u0410\u041F\u0423\u041B\u042C\u0421\u041A\u0418\u041C \u0413\u041E\u0412\u0414 \u0423\u0420",
                "ISSUE_DPT_CD" : "",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "",
                "SRC_USER_NM" : "",
                "CUST_DOC_SEQ_ID" : 11184967
            }, {
                "DOC_TYPE_CD" : "\u0424\u041B: \u041F\u0430\u0441\u043F\u043E\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043D\u0438\u043D\u0430 \u0420\u0424",
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "SERIES_ID" : "57 05",
                "NO_ID" : "776704",
                "ISSUE_DT" : "17.11.2005",
                "END_DT" : "",
                "ISSUED_BY_TX" : "\u0423\u0412\u0414 \u041C\u043E\u0442\u043E\u0432\u0438\u043B\u0438\u0445\u0438\u043D\u0441\u043A\u043E\u0433\u043E \u0440\u0430\u0439\u043E\u043D\u0430 \u0433. \u041F\u0435\u0440\u043C\u0438",
                "ISSUE_DPT_CD" : "",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "25.11.2015 18:45:44",
                "SRC_USER_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0421\u0438\u0434\u043E\u0440 \u041F\u0435\u0442\u0440\u043E\u0432\u0438\u0447",
                "CUST_DOC_SEQ_ID" : 10726569
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
