var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
            "success" : true,
            "totalCount" : 55,
            "data" : [
                {
                    CUR_CODE: "0",
                    CUR_NAME: "пусто",
                    METAL_FLAG: 0,
                    _ID_: 0
                },{
                    CUR_CODE: "0",
                    CUR_NAME: "пусто",
                    METAL_FLAG: 1,
                    _ID_: -1
                },{
                "_ID_" : 33,
                "CUR_CODE" : 33,
                "CUR_NAME" : "\u041F\u0430\u043B\u043B\u0430\u0434\u0438\u0439 (PAD)",
                "METAL_FLAG" : 1
            }, {
                "_ID_" : 56,
                "CUR_CODE" : 56,
                "CUR_NAME" : "\u0411\u0435\u043B\u044C\u0433\u0438\u0439\u0441\u043A\u0438\u0439 \u0444\u0440\u0430\u043D (BEF)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 246,
                "CUR_CODE" : 246,
                "CUR_NAME" : "\u0424\u0438\u043D\u043B\u044F\u043D\u0434\u0441\u043A\u0430\u044F \u043C\u0430\u0440\u043A (FIM)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 428,
                "CUR_CODE" : 428,
                "CUR_NAME" : "\u041B\u0430\u0442\u0432\u0438\u0439\u0441\u043A\u0438\u0439 \u043B\u0430\u0442 (LVL)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 702,
                "CUR_CODE" : 702,
                "CUR_NAME" : "\u0421\u0438\u043D\u0433\u0430\u043F\u0443\u0440\u0441\u043A\u0438\u0439 \u0434\u043E\u043B (SGD)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 724,
                "CUR_CODE" : 724,
                "CUR_NAME" : "\u0418\u0441\u043F\u0430\u043D\u0441\u043A\u0438\u0435 \u043F\u0435\u0441\u0435\u0442\u044B (ESP)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 972,
                "CUR_CODE" : 972,
                "CUR_NAME" : "\u0422\u0430\u0434\u0436\u0438\u043A\u0441\u043A\u0438\u0435 \u0441\u043E\u043C\u043E\u043D (TJS)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 99,
                "CUR_CODE" : 99,
                "CUR_NAME" : "\u0421\u0435\u0440\u0435\u0431\u0440\u043E (AGR)",
                "METAL_FLAG" : 1
            }, {
                "_ID_" : 348,
                "CUR_CODE" : 348,
                "CUR_NAME" : "\u0412\u0435\u043D\u0433\u0435\u0440\u0441\u043A\u0438\u0439 \u0444\u043E\u0440\u0438\u043D (HUF)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 440,
                "CUR_CODE" : 440,
                "CUR_NAME" : "\u041B\u0438\u0442\u043E\u0432\u0441\u043A\u0438\u0439 \u043B\u0438\u0442 (LTL)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 792,
                "CUR_CODE" : 792,
                "CUR_NAME" : "\u0422\u0443\u0440\u0435\u0446\u043A\u0430\u044F \u043B\u0438\u0440\u0430 (TRL)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 881,
                "CUR_CODE" : 881,
                "CUR_NAME" : "abd (ABD)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 949,
                "CUR_CODE" : 949,
                "CUR_NAME" : "\u041D\u043E\u0432\u0430\u044F \u0442\u0443\u0440\u0435\u0446\u043A\u0430\u044F \u043B (TRY)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 980,
                "CUR_CODE" : 980,
                "CUR_NAME" : "\u0423\u043A\u0440\u0430\u0438\u043D\u0441\u043A\u0430\u044F \u0433\u0440\u0438\u0432\u043D (UAH)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 40,
                "CUR_CODE" : 40,
                "CUR_NAME" : "\u0410\u0432\u0441\u0442\u0440\u0438\u0439\u0441\u043A\u0438\u0439 \u0448\u0438\u043B\u043B (ATS)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 380,
                "CUR_CODE" : 380,
                "CUR_NAME" : "\u0418\u0442\u0430\u043B\u044C\u044F\u043D\u0441\u043A\u0430\u044F \u043B\u0438\u0440\u0430 (ITL)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 442,
                "CUR_CODE" : 442,
                "CUR_NAME" : "\u041B\u044E\u043A\u0441\u0435\u043C\u0431\u0443\u0440\u0433\u0441\u043A\u0438\u0439 \u0444 (LUF)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 498,
                "CUR_CODE" : 498,
                "CUR_NAME" : "\u041C\u043E\u043B\u0434\u0430\u0432\u0441\u043A\u0438\u0439 \u043B\u0435\u0439 (MDL)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 752,
                "CUR_CODE" : 752,
                "CUR_NAME" : "\u0428\u0432\u0435\u0434\u0441\u043A\u0430\u044F \u043A\u0440\u043E\u043D\u0430 (SEK)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 756,
                "CUR_CODE" : 756,
                "CUR_NAME" : "\u0428\u0432\u0435\u0439\u0446\u0430\u0440\u0441\u043A\u0438\u0439 \u0444\u0440\u0430\u043D (CHF)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 762,
                "CUR_CODE" : 762,
                "CUR_NAME" : "\u0422\u0430\u0434\u0436\u0438\u043A\u0441\u043A\u0438\u0439 \u0440\u0443\u0431\u043B (TJR)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 840,
                "CUR_CODE" : 840,
                "CUR_NAME" : "\u0414\u043E\u043B\u043B\u0430\u0440 \u0421\u0428\u0410 (USD)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 985,
                "CUR_CODE" : 985,
                "CUR_NAME" : "\u041F\u043E\u043B\u044C\u0441\u043A\u0438\u0439 \u0437\u043B\u043E\u0442\u044B\u0439 (PLN)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 203,
                "CUR_CODE" : 203,
                "CUR_NAME" : "\u0427\u0435\u0448\u0441\u043A\u0430\u044F \u043A\u0440\u043E\u043D\u0430 (CZK)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 810,
                "CUR_CODE" : 810,
                "CUR_NAME" : "\u0420\u043E\u0441\u0441\u0438\u0439\u0441\u043A\u0438\u0439 \u0440\u0443\u0431\u043B\u044C (RUR)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 954,
                "CUR_CODE" : 954,
                "CUR_NAME" : "\u042D\u041A\u042E (XEU)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 98,
                "CUR_CODE" : 98,
                "CUR_NAME" : "\u0417\u043E\u043B\u043E\u0442\u043E (AUR)",
                "METAL_FLAG" : 1
            }, {
                "_ID_" : 208,
                "CUR_CODE" : 208,
                "CUR_NAME" : "\u0414\u0430\u0442\u0441\u043A\u0430\u044F \u043A\u0440\u043E\u043D\u0430 (DKK)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 392,
                "CUR_CODE" : 392,
                "CUR_NAME" : "\u042F\u043F\u043E\u043D\u0441\u043A\u0438\u0435 \u0439\u0435\u043D\u044B (JPY)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 398,
                "CUR_CODE" : 398,
                "CUR_NAME" : "\u041A\u0430\u0437\u0430\u0445\u0441\u043A\u0438\u0439 \u0442\u0435\u043D\u0433\u0435 (KZT)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 528,
                "CUR_CODE" : 528,
                "CUR_NAME" : "\u041D\u0438\u0434\u0435\u0440\u043B\u0430\u043D\u0434\u0441\u043A\u0438\u0439 \u0433\u0443 (NLG)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 578,
                "CUR_CODE" : 578,
                "CUR_NAME" : "\u041D\u043E\u0440\u0432\u0435\u0436\u0441\u043A\u0430\u044F \u043A\u0440\u043E\u043D\u0430 (NOK)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 795,
                "CUR_CODE" : 795,
                "CUR_NAME" : "\u0422\u0443\u0440\u043A\u043C\u0435\u043D\u0441\u043A\u0438\u0439 \u043C\u0430\u043D\u0430 (TMM)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 978,
                "CUR_CODE" : 978,
                "CUR_NAME" : "\u0415\u0432\u0440\u043E (EUR)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 112,
                "CUR_CODE" : 112,
                "CUR_NAME" : "\u0411\u0435\u043B\u043E\u0440\u0443\u0441\u0441\u043A\u0438\u0439 \u0440\u0443\u0431\u043B (BYB)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 233,
                "CUR_CODE" : 233,
                "CUR_NAME" : "\u042D\u0441\u0442\u043E\u043D\u0441\u043A\u0430\u044F \u043A\u0440\u043E\u043D\u0430 (EEK)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 250,
                "CUR_CODE" : 250,
                "CUR_NAME" : "\u0424\u0440\u0430\u043D\u0446\u0443\u0437\u0441\u043A\u0438\u0439 \u0444\u0440\u0430\u043D (FRF)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 276,
                "CUR_CODE" : 276,
                "CUR_NAME" : "\u041D\u0435\u043C\u0435\u0446\u043A\u0430\u044F \u043C\u0430\u0440\u043A\u0430 (DEM)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 417,
                "CUR_CODE" : 417,
                "CUR_NAME" : "\u041A\u0438\u0440\u0433\u0438\u0437\u0441\u043A\u0438\u0439 \u0441\u043E\u043C (KGS)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 975,
                "CUR_CODE" : 975,
                "CUR_NAME" : "\u0411\u043E\u043B\u0433\u0430\u0440\u0441\u043A\u0438\u0439 \u043B\u0435\u0432 (BGN)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 124,
                "CUR_CODE" : 124,
                "CUR_NAME" : "\u041A\u0430\u043D\u0430\u0434\u0441\u043A\u0438\u0439 \u0434\u043E\u043B\u043B\u0430\u0440 (CAD)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 156,
                "CUR_CODE" : 156,
                "CUR_NAME" : "\u041A\u0438\u0442\u0430\u0439\u0441\u043A\u0438\u0439 \u044E\u0430\u043D\u044C (CNY)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 245,
                "CUR_CODE" : 245,
                "CUR_NAME" : "\u0418\u043D\u0434\u0438\u0439\u0441\u043A\u0430\u044F \u0440\u0443\u043F\u0438\u044F  (C45)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 372,
                "CUR_CODE" : 372,
                "CUR_NAME" : "\u0418\u0440\u043B\u0430\u043D\u0434\u0441\u043A\u0438\u0439 \u0444\u0443\u043D\u0442 (IEP)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 410,
                "CUR_CODE" : 410,
                "CUR_NAME" : "\u0412\u043E\u043D\u0430 (KRW)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 616,
                "CUR_CODE" : 616,
                "CUR_NAME" : "\u041F\u043E\u043B\u044C\u0441\u043A\u0438\u0439 \u0437\u043B\u043E\u0442\u044B\u0439 (PLZ)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 620,
                "CUR_CODE" : 620,
                "CUR_NAME" : "\u041F\u043E\u0440\u0442\u0443\u0433\u0430\u043B\u044C\u0441\u043A\u043E\u0435 \u044D\u0441 (PTE)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 974,
                "CUR_CODE" : 974,
                "CUR_NAME" : "\u0411\u0435\u043B\u043E\u0440\u0443\u0441\u0441\u043A\u0438\u0439 \u0440\u0443\u0431\u043B (BYR)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 36,
                "CUR_CODE" : 36,
                "CUR_NAME" : "\u0410\u0432\u0441\u0442\u0440\u0430\u043B\u0438\u0439\u0441\u043A\u0438\u0439 \u0434\u043E (AUD)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 76,
                "CUR_CODE" : 76,
                "CUR_NAME" : "\u041F\u043B\u0430\u0442\u0438\u043D\u0430 (PAT)",
                "METAL_FLAG" : 1
            }, {
                "_ID_" : 100,
                "CUR_CODE" : 100,
                "CUR_NAME" : "\u0411\u043E\u043B\u0433\u0430\u0440\u0441\u043A\u0438\u0439 \u043B\u0435\u0432 (BGL)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 300,
                "CUR_CODE" : 300,
                "CUR_NAME" : "\u0413\u0440\u0435\u0447\u0435\u0441\u043A\u0430\u044F \u0414\u0440\u0430\u0445\u043C\u0430 (GRD)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 356,
                "CUR_CODE" : 356,
                "CUR_NAME" : "\u0418\u043D\u0434\u0438\u0439\u0441\u043A\u0430\u044F \u0440\u0443\u043F\u0438\u044F (INR)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 826,
                "CUR_CODE" : 826,
                "CUR_NAME" : "\u0424\u0443\u043D\u0442 \u0441\u0442\u0435\u0440\u043B\u0438\u043D\u0433\u043E\u0432 (GBP)",
                "METAL_FLAG" : 0
            }, {
                "_ID_" : 860,
                "CUR_CODE" : 860,
                "CUR_NAME" : "\u0423\u0437\u0431\u0435\u043A\u0441\u043A\u0438\u0439 \u0441\u0443\u043C (UZS)",
                "METAL_FLAG" : 0
            }]
        });
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
