var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId' || action == 'readByTrxnId') {
        response.send({
            "success" : true,
            "totalCount" : 2,
            "data" : [{
                "STATUS_TX" : "\u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "EMAIL_ADDR_TX" : "avtodinastiya@mail.ru",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "25.11.2015 20:41:54",
                "SRC_USER_NM" : "",
                "CUST_EMAIL_SEQ_ID" : 134289
            }, {
                "STATUS_TX" : "\u0424\u041B: \u0414\u0435\u0439\u0441\u0442\u0432\u0443\u044E\u0449\u0438\u0439",
                "EMAIL_ADDR_TX" : "avtodinastiya@mail.ru",
                "START_DT" : "17.08.2015",
                "END_DT" : "04.03.2016",
                "SRC_SYS_CD" : "EKS",
                "SRC_CHANGE_DT" : "25.11.2015 20:41:54",
                "SRC_USER_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432 \u0421\u0438\u0434\u043E\u0440 \u041F\u0435\u0442\u0440\u043E\u0432\u0438\u0447",
                "CUST_EMAIL_SEQ_ID" : 110159
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
