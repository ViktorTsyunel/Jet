var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'read') {
		response.send({
			success: true,
			data: [
				{
					"_id_": 0,
					"TB_ID": 0,
					"TB_NAME": "Все тербанки"
				},
				{
					"_id_": 49,
					"TB_ID": 49,
					"TB_NAME": "Западно-Уральский банк"
				},
				{
					"_id_": 60,
					"TB_ID": 60,
					"TB_NAME": "Северо-Кавказский банк"
				},
				{
					"_id_": 13,
					"TB_ID": 13,
					"TB_NAME": "Центрально-Черноземный банк"
				},
				{
					"_id_": 36,
					"TB_ID": 36,
					"TB_NAME": "Северо-Восточный банк"
				},
				{
					"_id_": 54,
					"TB_ID": 54,
					"TB_NAME": "Поволжский банк"
				},
				{
					"_id_": 70,
					"TB_ID": 70,
					"TB_NAME": "Дальневосточный банк"
				},
				{
					"_id_": 18,
					"TB_ID": 18,
					"TB_NAME": "Байкальский банк"
				},
				{
					"_id_": 40,
					"TB_ID": 40,
					"TB_NAME": "Среднерусский банк"
				},
				{
					"_id_": 42,
					"TB_ID": 42,
					"TB_NAME": "Волго-Вятский банк"
				},
				{
					"_id_": 31,
					"TB_ID": 31,
					"TB_NAME": "Восточно-Сибирский банк"
				},
				{
					"_id_": 55,
					"TB_ID": 55,
					"TB_NAME": "Северо-Западный банк"
				},
				{
					"_id_": 67,
					"TB_ID": 67,
					"TB_NAME": "Западно-Сибирский банк"
				},
				{
					"_id_": 38,
					"TB_ID": 38,
					"TB_NAME": "Сберегательный банк"
				},
				{
					"_id_": 16,
					"TB_ID": 16,
					"TB_NAME": "Уральский банк"
				},
				{
					"_id_": 44,
					"TB_ID": 44,
					"TB_NAME": "Сибирский банк"
				},
				{
					"_id_": 52,
					"TB_ID": 52,
					"TB_NAME": "Юго-Западный банк"
				},
				{
					"_id_": 77,
					"TB_ID": 77,
					"TB_NAME": "Северный банк"
				}
			]
		});
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
