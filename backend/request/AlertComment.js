var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'read') {
        response.send({
            "success" : true,
            "totalCount" : 2,
            "data" : [{
                "_id_" : 101,
                "CMMNT_ID" : 101,
                "CMMNT_TX" : "\u041D\u0435 \u043F\u043E\u0434\u043B\u0435\u0436\u0438\u0442 \u043A\u043E\u043D\u0442\u0440\u043E\u043B\u044E, \u043B\u043E\u0436\u043D\u043E\u0435 \u0441\u0440\u0430\u0431\u0430\u0442\u044B\u0432\u0430\u043D\u0438\u0435 \u0438\u0437-\u0437\u0430 \u043D\u0435\u0432\u0435\u0440\u043D\u043E\u0439 \u043B\u0435\u043A\u0441\u0435\u043C\u044B",
                "DISPL_ORDER_NB" : 5,
                "SCNRO_CLASS_CD" : "ML",
                "CMMNT_CAT_CD" : "SDCT"
            }, {
                "_id_" : 7996,
                "CMMNT_ID" : 7996,
                "CMMNT_TX" : "\u041E\u0436\u0438\u0434\u0430\u0435\u0442\u0441\u044F \u043E\u0442\u0432\u0435\u0442",
                "DISPL_ORDER_NB" : 10,
                "SCNRO_CLASS_CD" : "ML",
                "CMMNT_CAT_CD" : "RES"
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
