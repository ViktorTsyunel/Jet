var querystring = require("querystring");
function response(request, response, action) {
    if (action == 'read') {
        response.send(

            {
                "success" : true,
                "totalCount" : 83,
                "data" : [{
                    "_ID_" : "73",
                    "OKATO_OBJ_NM" : "\u0423\u043B\u044C\u044F\u043D\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "73"
                }, {
                    "_ID_" : "75",
                    "OKATO_OBJ_NM" : "\u0427\u0435\u043B\u044F\u0431\u0438\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "75"
                }, {
                    "_ID_" : "76",
                    "OKATO_OBJ_NM" : "\u0417\u0430\u0431\u0430\u0439\u043A\u0430\u043B\u044C\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "76"
                }, {
                    "_ID_" : "78",
                    "OKATO_OBJ_NM" : "\u042F\u0440\u043E\u0441\u043B\u0430\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "78"
                }, {
                    "_ID_" : "45",
                    "OKATO_OBJ_NM" : "\u041C\u043E\u0441\u043A\u0432\u0430 \u0433",
                    "OKATO_CD" : "45"
                }, {
                    "_ID_" : "40",
                    "OKATO_OBJ_NM" : "\u0421\u0430\u043D\u043A\u0442-\u041F\u0435\u0442\u0435\u0440\u0431\u0443\u0440\u0433 \u0433",
                    "OKATO_CD" : "40"
                }, {
                    "_ID_" : "99",
                    "OKATO_OBJ_NM" : "\u0415\u0432\u0440\u0435\u0439\u0441\u043A\u0430\u044F \u0410\u043E\u0431\u043B",
                    "OKATO_CD" : "99"
                }, {
                    "_ID_" : "77",
                    "OKATO_OBJ_NM" : "\u0427\u0443\u043A\u043E\u0442\u0441\u043A\u0438\u0439 \u0410\u041E",
                    "OKATO_CD" : "77"
                }, {
                    "_ID_" : "55",
                    "OKATO_OBJ_NM" : "\u0411\u0430\u0439\u043A\u043E\u043D\u0443\u0440 \u0433",
                    "OKATO_CD" : "55"
                }, {
                    "_ID_" : "79",
                    "OKATO_OBJ_NM" : "\u0410\u0434\u044B\u0433\u0435\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "79"
                }, {
                    "_ID_" : "80",
                    "OKATO_OBJ_NM" : "\u0411\u0430\u0448\u043A\u043E\u0440\u0442\u043E\u0441\u0442\u0430\u043D \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "80"
                }, {
                    "_ID_" : "81",
                    "OKATO_OBJ_NM" : "\u0411\u0443\u0440\u044F\u0442\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "81"
                }, {
                    "_ID_" : "84",
                    "OKATO_OBJ_NM" : "\u0410\u043B\u0442\u0430\u0439 \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "84"
                }, {
                    "_ID_" : "82",
                    "OKATO_OBJ_NM" : "\u0414\u0430\u0433\u0435\u0441\u0442\u0430\u043D \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "82"
                }, {
                    "_ID_" : "26",
                    "OKATO_OBJ_NM" : "\u0418\u043D\u0433\u0443\u0448\u0435\u0442\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "26"
                }, {
                    "_ID_" : "83",
                    "OKATO_OBJ_NM" : "\u041A\u0430\u0431\u0430\u0440\u0434\u0438\u043D\u043E-\u0411\u0430\u043B\u043A\u0430\u0440\u0441\u043A\u0430\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "83"
                }, {
                    "_ID_" : "85",
                    "OKATO_OBJ_NM" : "\u041A\u0430\u043B\u043C\u044B\u043A\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "85"
                }, {
                    "_ID_" : "91",
                    "OKATO_OBJ_NM" : "\u041A\u0430\u0440\u0430\u0447\u0430\u0435\u0432\u043E-\u0427\u0435\u0440\u043A\u0435\u0441\u0441\u043A\u0430\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "91"
                }, {
                    "_ID_" : "86",
                    "OKATO_OBJ_NM" : "\u041A\u0430\u0440\u0435\u043B\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "86"
                }, {
                    "_ID_" : "87",
                    "OKATO_OBJ_NM" : "\u041A\u043E\u043C\u0438 \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "87"
                }, {
                    "_ID_" : "88",
                    "OKATO_OBJ_NM" : "\u041C\u0430\u0440\u0438\u0439 \u042D\u043B \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "88"
                }, {
                    "_ID_" : "89",
                    "OKATO_OBJ_NM" : "\u041C\u043E\u0440\u0434\u043E\u0432\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "89"
                }, {
                    "_ID_" : "98",
                    "OKATO_OBJ_NM" : "\u0421\u0430\u0445\u0430 /\u042F\u043A\u0443\u0442\u0438\u044F/ \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "98"
                }, {
                    "_ID_" : "90",
                    "OKATO_OBJ_NM" : "\u0421\u0435\u0432\u0435\u0440\u043D\u0430\u044F \u041E\u0441\u0435\u0442\u0438\u044F - \u0410\u043B\u0430\u043D\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "90"
                }, {
                    "_ID_" : "92",
                    "OKATO_OBJ_NM" : "\u0422\u0430\u0442\u0430\u0440\u0441\u0442\u0430\u043D \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "92"
                }, {
                    "_ID_" : "93",
                    "OKATO_OBJ_NM" : "\u0422\u044B\u0432\u0430 \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "93"
                }, {
                    "_ID_" : "94",
                    "OKATO_OBJ_NM" : "\u0423\u0434\u043C\u0443\u0440\u0442\u0441\u043A\u0430\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "94"
                }, {
                    "_ID_" : "95",
                    "OKATO_OBJ_NM" : "\u0425\u0430\u043A\u0430\u0441\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "95"
                }, {
                    "_ID_" : "96",
                    "OKATO_OBJ_NM" : "\u0427\u0435\u0447\u0435\u043D\u0441\u043A\u0430\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "96"
                }, {
                    "_ID_" : "97",
                    "OKATO_OBJ_NM" : "\u0427\u0443\u0432\u0430\u0448\u0441\u043A\u0430\u044F \u0420\u0435\u0441\u043F\u0443\u0431\u043B\u0438\u043A\u0430 - \u0427\u0443\u0432\u0430\u0448\u0438\u044F \u0420\u0435\u0441\u043F",
                    "OKATO_CD" : "97"
                }, {
                    "_ID_" : "01",
                    "OKATO_OBJ_NM" : "\u0410\u043B\u0442\u0430\u0439\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "01"
                }, {
                    "_ID_" : "03",
                    "OKATO_OBJ_NM" : "\u041A\u0440\u0430\u0441\u043D\u043E\u0434\u0430\u0440\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "03"
                }, {
                    "_ID_" : "04",
                    "OKATO_OBJ_NM" : "\u041A\u0440\u0430\u0441\u043D\u043E\u044F\u0440\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "04"
                }, {
                    "_ID_" : "05",
                    "OKATO_OBJ_NM" : "\u041F\u0440\u0438\u043C\u043E\u0440\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "05"
                }, {
                    "_ID_" : "07",
                    "OKATO_OBJ_NM" : "\u0421\u0442\u0430\u0432\u0440\u043E\u043F\u043E\u043B\u044C\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "07"
                }, {
                    "_ID_" : "08",
                    "OKATO_OBJ_NM" : "\u0425\u0430\u0431\u0430\u0440\u043E\u0432\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "08"
                }, {
                    "_ID_" : "10",
                    "OKATO_OBJ_NM" : "\u0410\u043C\u0443\u0440\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "10"
                }, {
                    "_ID_" : "11",
                    "OKATO_OBJ_NM" : "\u0410\u0440\u0445\u0430\u043D\u0433\u0435\u043B\u044C\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "11"
                }, {
                    "_ID_" : "12",
                    "OKATO_OBJ_NM" : "\u0410\u0441\u0442\u0440\u0430\u0445\u0430\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "12"
                }, {
                    "_ID_" : "14",
                    "OKATO_OBJ_NM" : "\u0411\u0435\u043B\u0433\u043E\u0440\u043E\u0434\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "14"
                }, {
                    "_ID_" : "15",
                    "OKATO_OBJ_NM" : "\u0411\u0440\u044F\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "15"
                }, {
                    "_ID_" : "17",
                    "OKATO_OBJ_NM" : "\u0412\u043B\u0430\u0434\u0438\u043C\u0438\u0440\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "17"
                }, {
                    "_ID_" : "18",
                    "OKATO_OBJ_NM" : "\u0412\u043E\u043B\u0433\u043E\u0433\u0440\u0430\u0434\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "18"
                }, {
                    "_ID_" : "19",
                    "OKATO_OBJ_NM" : "\u0412\u043E\u043B\u043E\u0433\u043E\u0434\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "19"
                }, {
                    "_ID_" : "20",
                    "OKATO_OBJ_NM" : "\u0412\u043E\u0440\u043E\u043D\u0435\u0436\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "20"
                }, {
                    "_ID_" : "24",
                    "OKATO_OBJ_NM" : "\u0418\u0432\u0430\u043D\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "24"
                }, {
                    "_ID_" : "25",
                    "OKATO_OBJ_NM" : "\u0418\u0440\u043A\u0443\u0442\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "25"
                }, {
                    "_ID_" : "27",
                    "OKATO_OBJ_NM" : "\u041A\u0430\u043B\u0438\u043D\u0438\u043D\u0433\u0440\u0430\u0434\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "27"
                }, {
                    "_ID_" : "29",
                    "OKATO_OBJ_NM" : "\u041A\u0430\u043B\u0443\u0436\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "29"
                }, {
                    "_ID_" : "30",
                    "OKATO_OBJ_NM" : "\u041A\u0430\u043C\u0447\u0430\u0442\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "30"
                }, {
                    "_ID_" : "32",
                    "OKATO_OBJ_NM" : "\u041A\u0435\u043C\u0435\u0440\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "32"
                }, {
                    "_ID_" : "33",
                    "OKATO_OBJ_NM" : "\u041A\u0438\u0440\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "33"
                }, {
                    "_ID_" : "34",
                    "OKATO_OBJ_NM" : "\u041A\u043E\u0441\u0442\u0440\u043E\u043C\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "34"
                }, {
                    "_ID_" : "37",
                    "OKATO_OBJ_NM" : "\u041A\u0443\u0440\u0433\u0430\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "37"
                }, {
                    "_ID_" : "38",
                    "OKATO_OBJ_NM" : "\u041A\u0443\u0440\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "38"
                }, {
                    "_ID_" : "41",
                    "OKATO_OBJ_NM" : "\u041B\u0435\u043D\u0438\u043D\u0433\u0440\u0430\u0434\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "41"
                }, {
                    "_ID_" : "42",
                    "OKATO_OBJ_NM" : "\u041B\u0438\u043F\u0435\u0446\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "42"
                }, {
                    "_ID_" : "44",
                    "OKATO_OBJ_NM" : "\u041C\u0430\u0433\u0430\u0434\u0430\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "44"
                }, {
                    "_ID_" : "46",
                    "OKATO_OBJ_NM" : "\u041C\u043E\u0441\u043A\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "46"
                }, {
                    "_ID_" : "47",
                    "OKATO_OBJ_NM" : "\u041C\u0443\u0440\u043C\u0430\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "47"
                }, {
                    "_ID_" : "22",
                    "OKATO_OBJ_NM" : "\u041D\u0438\u0436\u0435\u0433\u043E\u0440\u043E\u0434\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "22"
                }, {
                    "_ID_" : "49",
                    "OKATO_OBJ_NM" : "\u041D\u043E\u0432\u0433\u043E\u0440\u043E\u0434\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "49"
                }, {
                    "_ID_" : "50",
                    "OKATO_OBJ_NM" : "\u041D\u043E\u0432\u043E\u0441\u0438\u0431\u0438\u0440\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "50"
                }, {
                    "_ID_" : "52",
                    "OKATO_OBJ_NM" : "\u041E\u043C\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "52"
                }, {
                    "_ID_" : "53",
                    "OKATO_OBJ_NM" : "\u041E\u0440\u0435\u043D\u0431\u0443\u0440\u0433\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "53"
                }, {
                    "_ID_" : "54",
                    "OKATO_OBJ_NM" : "\u041E\u0440\u043B\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "54"
                }, {
                    "_ID_" : "56",
                    "OKATO_OBJ_NM" : "\u041F\u0435\u043D\u0437\u0435\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "56"
                }, {
                    "_ID_" : "57",
                    "OKATO_OBJ_NM" : "\u041F\u0435\u0440\u043C\u0441\u043A\u0438\u0439 \u043A\u0440\u0430\u0439",
                    "OKATO_CD" : "57"
                }, {
                    "_ID_" : "58",
                    "OKATO_OBJ_NM" : "\u041F\u0441\u043A\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "58"
                }, {
                    "_ID_" : "60",
                    "OKATO_OBJ_NM" : "\u0420\u043E\u0441\u0442\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "60"
                }, {
                    "_ID_" : "61",
                    "OKATO_OBJ_NM" : "\u0420\u044F\u0437\u0430\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "61"
                }, {
                    "_ID_" : "36",
                    "OKATO_OBJ_NM" : "\u0421\u0430\u043C\u0430\u0440\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "36"
                }, {
                    "_ID_" : "63",
                    "OKATO_OBJ_NM" : "\u0421\u0430\u0440\u0430\u0442\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "63"
                }, {
                    "_ID_" : "64",
                    "OKATO_OBJ_NM" : "\u0421\u0430\u0445\u0430\u043B\u0438\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "64"
                }, {
                    "_ID_" : "65",
                    "OKATO_OBJ_NM" : "\u0421\u0432\u0435\u0440\u0434\u043B\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "65"
                }, {
                    "_ID_" : "66",
                    "OKATO_OBJ_NM" : "\u0421\u043C\u043E\u043B\u0435\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "66"
                }, {
                    "_ID_" : "68",
                    "OKATO_OBJ_NM" : "\u0422\u0430\u043C\u0431\u043E\u0432\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "68"
                }, {
                    "_ID_" : "28",
                    "OKATO_OBJ_NM" : "\u0422\u0432\u0435\u0440\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "28"
                }, {
                    "_ID_" : "69",
                    "OKATO_OBJ_NM" : "\u0422\u043E\u043C\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "69"
                }, {
                    "_ID_" : "70",
                    "OKATO_OBJ_NM" : "\u0422\u0443\u043B\u044C\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "70"
                }, {
                    "_ID_" : "71",
                    "OKATO_OBJ_NM" : "\u0422\u044E\u043C\u0435\u043D\u0441\u043A\u0430\u044F \u043E\u0431\u043B",
                    "OKATO_CD" : "71"
                }, {
                    "_ID_" : "0",
                    "OKATO_OBJ_NM" : "\u041F\u0443\u0441\u0442\u043E",
                    "OKATO_CD" : "0"
                }, {
                    "_ID_" : "00",
                    "OKATO_OBJ_NM" : "\u0418\u043D\u043E\u0441\u0442\u0440\u0430\u043D\u043D\u043E\u0435 \u0433\u043E\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u043E",
                    "OKATO_CD" : "00"
                }]
            });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
