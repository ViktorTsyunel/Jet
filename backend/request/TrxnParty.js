var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'readByAlertId' || action == 'scope') {
        response.send({
            "success" : true,
            "totalCount" : 1,
            "data" : [{
                "CUST_TYPE_CD" : "\u042E\u0440\u0438\u0434\u0438\u0447\u0435\u0441\u043A\u043E\u0435 \u043B\u0438\u0446\u043E",
                "CUST_NM" : "\u0417\u0410\u041A\u0420\u042B\u0422\u041E\u0415 \u0410\u041A\u0426\u0418\u041E\u041D\u0415\u0420\u041D\u041E\u0415 \u041E\u0411\u0429\u0415\u0421\u0422\u0412\u041E \"\u041C\u0415\u0414\u0418\u0426\u0418\u041D\u0421\u041A\u0410\u042F \u0410\u041A\u0426\u0418\u041E\u041D\u0415\u0420\u041D\u0410\u042F \u0421\u0422\u0420\u0410\u0425\u041E\u0412\u0410\u042F \u041A\u041E\u041C\u041F\u0410\u041D\u0418\u042F\"",
                "INN_NB" : "7702030351",
                "KPP_NB" : "774401001",
                "OKVED_NB" : "66.03.1",
                "OKPO_NB" : "40102302",
                "CUST_FL_NM" : "\u0422\u0435\u0441\u0442\u043E\u0432 \u0418\u0432\u0430\u043D \u0418\u0432\u0430\u043D\u043E\u0432\u0438\u0447",
                "RESIDENT_FL" : "\u0414\u0430",
                "GENDER_CD" : "\u041D\u0435 \u0438\u0437\u0432\u0435\u0441\u0442\u0435\u043D",
                "BIRTH_DT" : "24.11.2015",
                "BIRTHPLACE_TX" : "\u0420\u041E\u0421\u0421\u0418\u0419\u0421\u041A\u0410\u042F \u0424\u0415\u0414\u0415\u0420\u0410\u0426\u0418\u042F, \u041E\u0420\u0415\u041D\u0411\u0423\u0420\u0413\u0421\u041A\u0410\u042F \u041E\u0411\u041B\u0410\u0421\u0422\u042C, \u0413\u041E\u0420\u041E\u0414 \u0411\u0423\u0417\u0423\u041B\u0423\u041A",
                "CITIZENSHIP_CD" : "\u0420\u041E\u0421\u0421\u0418\u042F",
                "REG_DT" : "21.08.2002",
                "OGRN_NB" : "1027739099772",
                "REG_ORG_NM" : "\u041A\u0430\u043A\u043E\u0439-\u0442\u043E \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0443\u044E\u0449\u0438\u0439 \u043E\u0440\u0433\u0430\u043D",
                "ADR_COUNTRY_CD" : "\u0420\u041E\u0421\u0421\u0418\u042F",
                "ADR_STATE_NM" : "\u0433 \u041C\u043E\u0441\u043A\u0432\u0430",
                "ADR_DISTRICT_NM" : "\u0437\u0434\u0435\u0441\u044C \u0431\u0443\u0434\u0435\u0442 \u0440\u0430\u0439\u043E\u043D",
                "ADR_CITY_NM" : "\u0413 \u041C\u041E\u0421\u041A\u0412\u0410",
                "ADR_SETTLM_NM" : "\u043F\u043E\u0441. \u0412\u043D\u0443\u043A\u043E\u0432\u043E",
                "ADR_UNSTRUCT_TX" : "115184, \u0413 \u041C\u041E\u0421\u041A\u0412\u0410, \u0423\u041B. \u041E\u0420\u0414\u042B\u041D\u041A\u0410 \u041C., \u0414\u041E\u041C 50",
                "ADR_OKATO_CD" : "45000000000",
                "ADR_STREET_NAME_TX" : "115184, \u0413 \u041C\u041E\u0421\u041A\u0412\u0410, \u0423\u041B. \u041E\u0420\u0414\u042B\u041D\u041A\u0410 \u041C., \u0414\u041E\u041C 50",
                "ADR_HOUSE_NUMBER_TX" : "50",
                "ADR_BUILDING_NUMBER_TX" : "\u0441\u0442\u0440. 888",
                "ADR_FLAT_NUMBER_TX" : "\u043A\u0432. 15",
                "ADH_COUNTRY_CD" : "\u0420\u041E\u0421\u0421\u0418\u042F",
                "ADH_STATE_NM" : "\u0433 \u041C\u043E\u0441\u043A\u0432\u0430",
                "ADH_DISTRICT_NM" : "\u0437\u0434\u0435\u0441\u044C \u0431\u0443\u0434\u0435\u0442 \u0440\u0430\u0439\u043E\u043D2",
                "ADH_CITY_NM" : "\u0413 \u041C\u041E\u0421\u041A\u0412\u0410",
                "ADH_SETTLM_NM" : "\u043F\u043E\u0441. \u0416\u0443\u043A\u043E\u0432\u043E",
                "ADH_UNSTRUCT_TX" : "115184, \u0413 \u041C\u041E\u0421\u041A\u0412\u0410, \u0423\u041B. \u041E\u0420\u0414\u042B\u041D\u041A\u0410 \u041C., \u0414\u041E\u041C 50",
                "ADH_OKATO_CD" : "45000000000",
                "ADH_STREET_NAME_TX" : "115184, \u0413 \u041C\u041E\u0421\u041A\u0412\u0410, \u0423\u041B. \u041E\u0420\u0414\u042B\u041D\u041A\u0410 \u041C., \u0414\u041E\u041C 50",
                "ADH_HOUSE_NUMBER_TX" : "50",
                "ADH_BUILDING_NUMBER_TX" : "\u0441\u0442\u0440. 889",
                "ADH_FLAT_NUMBER_TX" : "\u043A\u0432. 765",
                "DOC_TYPE_CD" : "\u041F\u0430\u0441\u043F\u043E\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043D\u0438\u043D\u0430 \u0420\u0424",
                "DOCUMENT_SERIES" : "45 03",
                "DOCUMENT_ISSUED_DPT_TX" : "\u041F\u0430\u0441\u043F\u043E\u0440\u0442\u043D\u044B\u043C \u0441\u0442\u043E\u043B\u043E\u043C \u2116 2 \u041E\u0412\u0414 \u0422\u0432\u0435\u0440\u0441\u043A\u043E\u0433\u043E \u0440-\u043D\u0430 \u0423\u0412\u0414 \u0426\u0410\u041E \u0433.\u041C\u043E\u0441\u043A\u0432\u044B",
                "DOCUMENT_ISSUED_DPT_CD" : "772-115",
                "DOCUMENT_NO" : "418989",
                "DOCUMENT_ISSUED_DT" : "24.11.2015",
                "DOCUMENT_END_DT" : "24.11.2015",
                "CUST_SEQ_ID" : 179568496
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
