var querystring = require("querystring");
var fs = require('fs');
function response(request, response, action) {
    if (action == 'update') {
        response.send({
            success: true
        });
    } else if (action == 'read' || action == 'readByAlertId') {
        response.send({
            "success" : true,
            "totalCount" : 42,
            "data" : [{
                "_id_" : 2,
                "START_DT" : "2015-02-11 13:07:25",
                "ACTVY_TYPE_NM" : "\u0421\u043E\u0437\u0434\u0430\u043D\u0438\u0435",
                "OWNER_ID" : "OFMSUPERVISOR",
                "NEW_STATUS_TX" : "\u041D\u043E\u0432\u0430\u044F",
                "CMMNT" : "--",
                "ATTCH_NM" : "Плат. пор 2980",
                "FILE_NM" : "2980.txt",
                "OES_DESC" : "\u2116 ... \u043E\u0442 ...(...)",
                "CONTENT_TYPE_TX" : "",
                "ATTCH_SEQ_ID" : null,
                "FILE_SIZE" : "206.15 \u041A\u0431",
                "DELETED_FL" : "Y"
            }, {
                "_id_" : 1,
                "START_DT" : "2015-02-11 17:23:29",
                "ACTVY_TYPE_NM" : "\u041E\u0442\u043A\u0440\u044B\u0442\u0430",
                "OWNER_ID" : "OFMSUPERVISOR",
                "NEW_STATUS_TX" : "\u0412 \u0440\u0430\u0431\u043E\u0442\u0435",
                "CMMNT" : "--",
                "ATTCH_NM" : "",
                "FILE_NM" : "",
                "OES_DESC" : "\u2116 ... \u043E\u0442 ...(...)",
                "CONTENT_TYPE_TX" : "",
                "ATTCH_SEQ_ID" : null,
                "FILE_SIZE" : "123.12 \u041A\u0431",
                "DELETED_FL" : ""
            }]
        });
    } else {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    }
}
exports.response = response;
