var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				return {
					"_id_": Math.floor(Math.random() * 100000),
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send(
            {
                "success" : true,
                "totalCount" : 1,
                "data" : [{
                    "_ID_" : 101,
                    "RULE_SEQ_ID" : 101,
                    "FORM_CD" : "321",
                    "DESC_TX" : "'Неверно заполнено поле \"#FIELD_NAME# (#FIELD_CODE#)\": '||#FIELD_SQL#||'. Должно быть значение: 2'",
                    "CHECK_SQL_TX" : "VERSION должен быть 2",
                    "MSG_SQL_TX" : "oes.VERSION is null or oes.VERSION not in ('2')",
                    "COL_LIST_TX" : "VERSION",
                    "BLOCK_LIST_TX": "" ,
                    "CRITICAL_FL": "N",
                    "ACTIVE_FL" : "Y",
                    "ERR_FL": "N",
                    "ERR_MESS_TX": "110701,110702,11703",
                    "NOTE_TX" : "110101",
                    "CREATED_BY" : "admin",
                    "CREATED_DATE" : "2016-01-14 12:30:33",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                },
                {
                    "_ID_" : 102,
                    "RULE_SEQ_ID" : 102,
                    "FORM_CD" : "321",
                    "DESC_TX" : "'Неверно заполнено поле \"#FIELD_NAME# (#FIELD_CODE#)\": '||#FIELD_SQL#||'. Должно быть значение: 2'",
                    "CHECK_SQL_TX" : "VERSION должен быть 2",
                    "MSG_SQL_TX" : "oes.VERSION is null or oes.VERSION not in ('2')",
                    "COL_LIST_TX" : "VERSION",
                    "BLOCK_LIST_TX": "" ,
                    "CRITICAL_FL": "N",
                    "ACTIVE_FL" : "Y",
                    "ERR_FL": "Y",
                    "ERR_MESS_TX": "110701,110702,11703",
                    "NOTE_TX" : "110101",
                    "CREATED_BY" : "admin",
                    "CREATED_DATE" : "2016-01-14 12:30:33",
                    "MODIFIED_BY" : "",
                    "MODIFIED_DATE" : ""
                }
                ]
            }
        );
	} else {
	    response.send({
		    success: false,
		    message: 'This action unavailable'
	    });
	}
}
exports.response = response;