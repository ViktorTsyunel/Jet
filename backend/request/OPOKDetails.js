var querystring = require("querystring");
function response(request, response, action) {
	if (action == 'update') {
		response.send({
			success: true
		});
	} else if (action == 'create') {
		response.send({
			success: true,
			data: request.body.data.map(function (i) {
				var newId = Math.floor(Math.random() * 100000);
				return {
					"_id_": newId,
					"#clientId#": i["_id_"]
				};
			})
		});
	} else if (action == 'delete') {
		response.send({
			success: true
		});
	} else if (action == 'read') {
		response.send({
			success: true,
			totalCount: 30,
			data: [
				{
					"_id_": 131,
					"RECORD_SEQ_ID": 131,
					"OPOK_NB": 1001,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 132,
					"RECORD_SEQ_ID": 132,
					"OPOK_NB": 1002,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 133,
					"RECORD_SEQ_ID": 133,
					"OPOK_NB": 1003,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 134,
					"RECORD_SEQ_ID": 134,
					"OPOK_NB": 1004,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 135,
					"RECORD_SEQ_ID": 135,
					"OPOK_NB": 1005,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 136,
					"RECORD_SEQ_ID": 136,
					"OPOK_NB": 1006,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 137,
					"RECORD_SEQ_ID": 137,
					"OPOK_NB": 1007,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 138,
					"RECORD_SEQ_ID": 138,
					"OPOK_NB": 1008,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 139,
					"RECORD_SEQ_ID": 139,
					"OPOK_NB": 3001,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 140,
					"RECORD_SEQ_ID": 140,
					"OPOK_NB": 3011,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 141,
					"RECORD_SEQ_ID": 141,
					"OPOK_NB": 3021,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 142,
					"RECORD_SEQ_ID": 142,
					"OPOK_NB": 4001,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 143,
					"RECORD_SEQ_ID": 143,
					"OPOK_NB": 4002,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 144,
					"RECORD_SEQ_ID": 144,
					"OPOK_NB": 4003,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 145,
					"RECORD_SEQ_ID": 145,
					"OPOK_NB": 4004,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 146,
					"RECORD_SEQ_ID": 146,
					"OPOK_NB": 4005,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 147,
					"RECORD_SEQ_ID": 147,
					"OPOK_NB": 4006,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 148,
					"RECORD_SEQ_ID": 148,
					"OPOK_NB": 4007,
					"LIMIT_AM": 50000000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 149,
					"RECORD_SEQ_ID": 149,
					"OPOK_NB": 5001,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 150,
					"RECORD_SEQ_ID": 150,
					"OPOK_NB": 5002,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 151,
					"RECORD_SEQ_ID": 151,
					"OPOK_NB": 5003,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 152,
					"RECORD_SEQ_ID": 152,
					"OPOK_NB": 5004,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 153,
					"RECORD_SEQ_ID": 153,
					"OPOK_NB": 5005,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 154,
					"RECORD_SEQ_ID": 154,
					"OPOK_NB": 5006,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 155,
					"RECORD_SEQ_ID": 155,
					"OPOK_NB": 5007,
					"LIMIT_AM": 600000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 156,
					"RECORD_SEQ_ID": 156,
					"OPOK_NB": 6001,
					"LIMIT_AM": 0,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 157,
					"RECORD_SEQ_ID": 157,
					"OPOK_NB": 7001,
					"LIMIT_AM": 0,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 158,
					"RECORD_SEQ_ID": 158,
					"OPOK_NB": 8001,
					"LIMIT_AM": 3000000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 159,
					"RECORD_SEQ_ID": 159,
					"OPOK_NB": 9001,
					"LIMIT_AM": 100000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 160,
					"RECORD_SEQ_ID": 160,
					"OPOK_NB": 9002,
					"LIMIT_AM": 200000,
					"START_DT": '2014-01-01 00:00:00',
					"END_DT": '2014-12-31 00:00:00',
					"ACTIVE_FL": "Y"
				},
				{
					"_id_": 161,
					"RECORD_SEQ_ID": 161,
					"OPOK_NB": 9002,
					"LIMIT_AM": 100000,
					"START_DT": '2015-01-01 00:00:00',
					"END_DT": null,
					"ACTIVE_FL": "Y"
				}
			]
		});
	} else {
		response.send({
			success: false,
			message: 'This action unavailable'
		});
	}
}
exports.response = response;
