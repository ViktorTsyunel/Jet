var querystring = require("querystring");
function response(request, response, action) {
    if (action != 'read') {
        response.send({
            success: false,
            message: 'This action unavailable'
        });
    } else {
        response.send(
            {
                "success": true,
                "data": [{
                    "COLUMN_NAME": "5005, \u043F\u0440\u0430\u0432\u0438\u043B\u043E \u0432\u044B\u044F\u0432\u043B\u0435\u043D\u0438\u044F \u2116 54, \u0432\u0435\u0440\u0441\u0438\u044F 1",
                    "COLUMN_VALUE": "",
                    "expanded": true,
                    "children": [{
                        "COLUMN_NAME": "\u041E\u043F\u0435\u0440\u0430\u0446\u0438\u044F",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u041A\u0430\u0442\u0435\u0433\u043E\u0440\u0438\u044F",
                            "COLUMN_VALUE": "\u0411\u0435\u0437\u043D\u0430\u043B\u0438\u0447\u043D\u044B\u0435",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u041F\u043E\u0440\u043E\u0433\u043E\u0432\u0430\u044F \u0441\u0443\u043C\u043C\u0430",
                            "COLUMN_VALUE": "600 000.00",
                            "leaf": true
                        }]
                    }, {
                        "COLUMN_NAME": "\u0421\u0447\u0435\u0442 \u043F\u043B\u0430\u0442\u0435\u043B\u044C\u0449\u0438\u043A\u0430",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u0423\u0441\u043B\u043E\u0432\u0438\u0435",
                            "COLUMN_VALUE": "401-426,474,603",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u041E\u0431\u043B\u0430\u0441\u0442\u044C \u043F\u043E\u0438\u0441\u043A\u0430",
                            "COLUMN_VALUE": "\u0421\u0447\u0435\u0442 \u0443\u0447\u0430\u0441\u0442\u043D\u0438\u043A\u0430",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043D\u044B\u0439 \u0441\u0447\u0435\u0442",
                            "COLUMN_VALUE": "\u041D\u0435\u0442",
                            "leaf": true
                        }]
                    }, {
                        "COLUMN_NAME": "\u041E\u0431\u0440\u0430\u0442\u043D\u044B\u0435 \u0443\u0441\u043B\u043E\u0432\u0438\u044F \u043D\u0430 \u0441\u0447\u0435\u0442\u0430",
                        "COLUMN_VALUE": "\u0414\u0430",
                        "leaf": true
                    }, {
                        "COLUMN_NAME": "\u041B\u0435\u043A\u0441\u0435\u043C\u044B",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u041D\u0430\u0437\u043D\u0430\u0447\u0435\u043D\u0438\u0435 \u043F\u043B\u0430\u0442\u0435\u0436\u0430",
                            "COLUMN_VALUE": "\u043C\u0435\u0442\u0430\u043B&\u043A\u0443\u0440\u0441,\u043E\u0431\u0435\u0437\u043B\u0438\u0447&\u043C\u0435\u0442\u0430\u043B,\u043E\u0431\u0435\u0437\u043B\u0438\u0447&\u0441\u0447\u0435\u0442,\u043E\u043C\u0441,0\u043C\u0441,omc,\u0430\u043B\u043C\u0430\u0437,\u0438\u0437\u0443\u043C\u0440\u0443\u0434,\u0440\u0443\u0431\u0438\u043D,\u0430\u043B\u0435\u043A\u0441\u0430\u043D\u0434\u0440\u0438\u0442,\u0436\u0435\u043C\u0447\u0443\u0433,\u043F\u043B\u0430\u0442\u0438\u043D,\u0434\u0440\u0430\u0433&\u043C\u0435\u0442,\u044E\u0432\u0435\u043B\u0438\u0440,\u0438\u0440\u0438\u0434\u0438,\u043E\u0441\u043C\u0438,\u0434\u0440\u0430\u0433\u043E\u0446\u0435\u043D,\u0446\u0435\u043D\u043D\u043E\u0441\u0442,\u0430\u043B\u043C\u0430\u0437&\u0431\u0440\u0438\u043B\u043B,\u0441\u0430\u043F\u0444\u0438\u0440,\u0437\u043E\u043B\u043E\u0442,\u0441\u0435\u0440\u0435\u0431\u0440,\u043F\u0430\u043B\u043B\u0430\u0434,\u0440\u043E\u0434\u0438,\u0440\u0443\u0442\u0435\u043D\u0438",
                            "leaf": true
                        }]
                    }, {
                        "COLUMN_NAME": "\u0418\u043D\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u044F \u043E \u043F\u0440\u0430\u0432\u0438\u043B\u0435",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u041F\u0440\u0438\u043E\u0440\u0438\u0442\u0435\u0442",
                            "COLUMN_VALUE": "1",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u0412\u0438\u0434 \u0437\u0430\u043F\u0440\u043E\u0441\u0430",
                            "COLUMN_VALUE": "\u0421\u0422\u0410\u041D\u0414\u0410\u0420\u0422\u041D\u042B\u0419",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u0410\u043A\u0442\u0443\u0430\u043B\u044C\u043D\u0430\u044F \u0432\u0435\u0440\u0441\u0438\u044F",
                            "COLUMN_VALUE": "\u041D\u0435\u0442",
                            "leaf": true
                        }]
                    }]
                }, {
                    "COLUMN_NAME": "5007, \u043F\u0440\u0430\u0432\u0438\u043B\u043E \u0432\u044B\u044F\u0432\u043B\u0435\u043D\u0438\u044F \u2116 62, \u0432\u0435\u0440\u0441\u0438\u044F 1",
                    "COLUMN_VALUE": "",
                    "expanded": true,
                    "children": [{
                        "COLUMN_NAME": "\u041E\u043F\u0435\u0440\u0430\u0446\u0438\u044F",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u041A\u0430\u0442\u0435\u0433\u043E\u0440\u0438\u044F",
                            "COLUMN_VALUE": "\u0411\u0435\u0437\u043D\u0430\u043B\u0438\u0447\u043D\u044B\u0435",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u041F\u043E\u0440\u043E\u0433\u043E\u0432\u0430\u044F \u0441\u0443\u043C\u043C\u0430",
                            "COLUMN_VALUE": "600 000.00",
                            "leaf": true
                        }]
                    }, {
                        "COLUMN_NAME": "\u0421\u0447\u0435\u0442 \u043F\u043B\u0430\u0442\u0435\u043B\u044C\u0449\u0438\u043A\u0430",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u0423\u0441\u043B\u043E\u0432\u0438\u0435",
                            "COLUMN_VALUE": "401-426,30102,30302",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u041E\u0431\u043B\u0430\u0441\u0442\u044C \u043F\u043E\u0438\u0441\u043A\u0430",
                            "COLUMN_VALUE": "\u0421\u0447\u0435\u0442 \u0443\u0447\u0430\u0441\u0442\u043D\u0438\u043A\u0430",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043D\u044B\u0439 \u0441\u0447\u0435\u0442",
                            "COLUMN_VALUE": "\u041D\u0435\u0442",
                            "leaf": true
                        }]
                    }, {
                        "COLUMN_NAME": "\u041E\u0431\u0440\u0430\u0442\u043D\u044B\u0435 \u0443\u0441\u043B\u043E\u0432\u0438\u044F \u043D\u0430 \u0441\u0447\u0435\u0442\u0430",
                        "COLUMN_VALUE": "\u0414\u0430",
                        "leaf": true
                    }, {
                        "COLUMN_NAME": "\u041B\u0435\u043A\u0441\u0435\u043C\u044B",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u041D\u0430\u0437\u043D\u0430\u0447\u0435\u043D\u0438\u0435 \u043F\u043B\u0430\u0442\u0435\u0436\u0430",
                            "COLUMN_VALUE": "\u0441\u0441\u0443\u0434,\u0444\u0438\u043D\u0430\u043D\u0441\u043E\u0432\u0430\u044F&\u043F\u043E\u043C\u043E\u0449\u044C,\u0437\u0430\u0439\u043C,\u0437\u0430\u0435\u043C,\u0437\u0430\u0438\u043C,\u043A\u0440\u0435\u0434,\u043F\u043E\u043C\u043E\u0449,\u0444\u0438\u043D&\u043F\u043E\u043C,loan,zaim,3\u0430\u0439\u043C,\u0437\u0430\u0451\u043C,\u0431\u04353&\u043F\u0440\u043E\u0446,\u0431\u04353\u043F\u0440\u043E\u0446,\u0431\u0435\u0437&\u043F\u0440\u043E\u0446,\u0431\u0435\u0437\u043F\u0440\u043E\u0446,\u0431\u0435\u0441&\u043F\u0440\u043E\u0446,\u0431\u0435\u0441\u043F\u0440\u043E\u0446,^\u0432\u0437\u0430\u0438\u043C",
                            "leaf": true
                        }]
                    }, {
                        "COLUMN_NAME": "\u0418\u043D\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u044F \u043E \u043F\u0440\u0430\u0432\u0438\u043B\u0435",
                        "COLUMN_VALUE": "",
                        "expanded": true,
                        "children": [{
                            "COLUMN_NAME": "\u041F\u0440\u0438\u043E\u0440\u0438\u0442\u0435\u0442",
                            "COLUMN_VALUE": "1",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u0412\u0438\u0434 \u0437\u0430\u043F\u0440\u043E\u0441\u0430",
                            "COLUMN_VALUE": "\u0421\u0422\u0410\u041D\u0414\u0410\u0420\u0422\u041D\u042B\u0419",
                            "leaf": true
                        }, {
                            "COLUMN_NAME": "\u0410\u043A\u0442\u0443\u0430\u043B\u044C\u043D\u0430\u044F \u0432\u0435\u0440\u0441\u0438\u044F",
                            "COLUMN_VALUE": "\u041D\u0435\u0442",
                            "leaf": true
                        }]
                    }]
                }]
            }
        );
    }
}
exports.response = response;