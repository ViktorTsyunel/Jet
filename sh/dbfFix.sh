#!/bin/bash
#
# OS Solaris
#
export PATH=$ORACLE_HOME/bin:/usr/local/bin:/usr/bin:/bin:$PATH
dbfile=$1
if [[ "$dbfile" == *bnkdel.dbf ]]; then
  hsize=899
elif [[ "$dbfile" == *bnkdel_2.dbf ]]; then
  hsize=899
elif [[ "$dbfile" == *bnkseek.dbf ]]; then
  hsize=931
elif [[ "$dbfile" == *bnkseek_2.dbf ]]; then
  hsize=899
else
  hsize=0
fi

#hsize=`od -t u2 -j 8 -N 2 -A n "$dbfile" | bc`
#hsize=`echo $hsize+2 | bc`
/usr/xpg4/bin/tail -c +$hsize "$dbfile"
#
#
# OS Linux
#
#export PATH=$ORACLE_HOME/bin:/usr/local/bin:/usr/bin:/bin:$PATH
#dbfile=$1
#hsize=`od -t u2 -j 8 -N 2 -A n "$dbfile"`
#let hsize=hsize+2
#tail -c +$hsize "$dbfile" | head -c -0
