<%@page 
import="org.apache.commons.io.FilenameUtils"
import="java.util.ArrayList"
import="java.text.SimpleDateFormat"
import="java.text.DateFormat"
import="org.json.JSONObject"
import="java.sql.Date"
import="javax.naming.InitialContext"
import="javax.naming.Context"
import="oracle.jdbc.driver.Message"
import="java.io.IOException"
import="org.apache.commons.io.IOUtils"
import="java.io.ByteArrayInputStream"
import="java.sql.DriverManager"
import="javax.sql.DataSource"
import="java.sql.SQLException"
import="java.sql.Types"
import="java.sql.CallableStatement"
import="java.sql.Connection"
import="java.io.InputStream"
import="org.apache.commons.fileupload.util.Streams"
import="org.apache.commons.fileupload.FileItemStream"
import="org.apache.commons.fileupload.FileItemIterator"
import="org.apache.commons.fileupload.servlet.ServletFileUpload"
language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	class File {
		private String fileName;
		private byte[] fileBody;

		File (String fileName, InputStream fileBody) throws IOException {
			this.fileName = FilenameUtils.getName(fileName);
			this.fileBody = IOUtils.toByteArray(fileBody);
		}

		public String getFileName() {
			return fileName;
		}

		public byte[] getFileBody() {
			return fileBody;
		}      
	}
%>

<%
	String result = "";
	String userId = session.getAttribute("gsUsrID").toString();
	String ipAddress = request.getRemoteAddr();

	/* Проверка, что сообщение multipart */
	if (ServletFileUpload.isMultipartContent(request) == true) {

		String impType = null;
		String dateAct = null;
		String fileName = null;
		String noteTx = null;
		byte[] fileBody = null;
		String ownerId = null;
		ArrayList<File> fileList = new ArrayList<File>();

		ServletFileUpload upload = new ServletFileUpload();
		FileItemIterator iter =  null;

		/* Читаем значения ключей в запросе */
		iter = upload.getItemIterator(request);
		while (iter.hasNext()) {
			FileItemStream fileItemStream = iter.next();
			String fieldName = fileItemStream.getFieldName();
			String fieldValue = null;
			InputStream inputStream = fileItemStream.openStream();
			if (fileItemStream.isFormField()) {
				if (fieldName.equals("impType"))
				    impType = Streams.asString(inputStream);  
				else if (fieldName.equals("dateAct"))
				    dateAct = Streams.asString(inputStream); 
				else if (fieldName.equals("fileName"))
				    fileName = Streams.asString(inputStream);
				else if (fieldName.equals("noteTx"))
					noteTx = Streams.asString(inputStream);
				else if (fieldName.equals("ownerId"))
                    ownerId = Streams.asString(inputStream);
			} else {
			    fileList.add( new File(fileItemStream.getName(), inputStream) );
			}
		}

		/* Передаем файл в БД */
		int code = 200;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		String userName = session.getAttribute("gsUsrID").toString();
 		try {
			Context initContext = new InitialContext();
			DataSource ds = (DataSource)initContext.lookup("jdbc/AMINFO");
			conn = ds.getConnection();
			conn.setAutoCommit(false);
			
			// IMP_TYPE - CBRF_BNKDEL, CBRF_BNKSEEK
			if (impType.equals("CBRF_BNKDEL") || impType.equals("CBRF_BNKSEEK")) {
				final String sqlCBRF = "{ ? = call mantas.rf_pkg_request_imports.import_dbf(?,?,?,?,?,?,?) }";

				File file = fileList.get(0);
				cs = conn.prepareCall(sqlCBRF);
				cs.registerOutParameter(1, Types.VARCHAR);
				cs.setString(2, impType);
				cs.setDate(3, new java.sql.Date(df.parse(dateAct).getTime()));
				cs.setString(4, file.getFileName());
				if (noteTx.isEmpty()) {
					cs.setNull(5, Types.NULL);
				} else {
					cs.setString(5, noteTx);
				}
				cs.setString(6, userName);
				cs.setBinaryStream(7, (InputStream)(new ByteArrayInputStream(file.getFileBody())), file.getFileBody().length);
				cs.setString(8, ipAddress);
				cs.execute();
				result = cs.getString(1);
			
			// IMP_TYPE - SBRF_321XML, SBRF_321XML_8001
 			} else if (impType.equals("SBRF_321XML") || impType.equals("SBRF_321XML_8001")) {
				JSONObject j = new JSONObject();
				j.put("ownerId", ownerId);
				String paramsTx = j.toString(); 

				// Запускаем сессию
				cs = conn.prepareCall("{ ? = call mantas.rf_pkg_import.start_imp_session(?, ?, ?, ?, ?, ?) }");
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setString(2, impType);
				cs.setString(3, userName);
				cs.setString(4, null);
				cs.setString(5, paramsTx);
				cs.setString(6, noteTx);
				cs.setString(7, ipAddress);
				cs.execute();
				Integer sessionId = cs.getInt(1);
				
				// Добавляем файлы
				cs = conn.prepareCall("{ ? = call mantas.rf_pkg_import.add_file(?, ?, ?, ?) }");
				cs.registerOutParameter(1, Types.INTEGER);
				for (File f : fileList) {
				    cs.setString(2, f.getFileName());
				    cs.setNull(3, Types.NULL);
				    cs.setBinaryStream(4, (InputStream)(new ByteArrayInputStream(f.getFileBody())), f.getFileBody().length);
				    cs.registerOutParameter(5, Types.VARCHAR);
				    cs.execute();
				}
				
				// Запускаем загрузку файлов
				cs = conn.prepareCall("{ ? = call mantas.rf_pkg_import.process_files(?)}");
				cs.registerOutParameter(1, Types.INTEGER);
				cs.registerOutParameter(2, Types.VARCHAR);
				cs.execute();
				
				// Формируем ответ
				j = new JSONObject();
                j.put("success", true);
                j.put("message", cs.getString(2));
                result = j.toString();
				
                // Закрываем сесиию
				cs = conn.prepareCall("{ call mantas.rf_pkg_import.end_imp_session() }");
				cs.execute();
				conn.commit();	
			}
			
		} catch (SQLException e) {
			code = 500;	
			JSONObject j = new JSONObject();
			j.put("success", false);
			j.put("message", "Ошибка импорта данных!. "+e.getMessage().replace("\"", "'"));
			result = j.toString();
		} finally {
			try {
				cs.close();
			} catch(Exception e) {}
			try {
				conn.close();
			} catch (Exception e) {}
		}
		response.setStatus(code);
	}
%>

<%= result %>