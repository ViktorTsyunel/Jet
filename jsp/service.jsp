<%@ page pageEncoding="UTF-8" contentType="application/json" session="true" import="javax.naming.Context,javax.sql.DataSource,java.io.BufferedReader,java.io.InputStreamReader,java.sql.Connection,java.sql.CallableStatement,java.sql.Types,java.sql.SQLException,javax.naming.NamingException,javax.naming.InitialContext" %>
<%
	int code = 200;
	String result = "";
	StringBuilder sBuilder = new StringBuilder();
	String line = null;
	String userId = session.getAttribute("gsUsrID").toString();
	String ipAddress = request.getRemoteAddr();
	try {
		BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
		while ((line = in.readLine()) != null) {
			sBuilder.append(line);
		}
	} catch (Exception e) {
		code = 500;
		result = "{\"success\":\"false\",\"message\":\"" + e.getMessage() + "\"}";
	}
	Connection conn = null;
	CallableStatement cs = null;
	try {
		final String sql = "{ ? = call mantas.rf_pkg_request.process_request(?,?,?) }";
		Context initContext = new InitialContext();
		DataSource ds = (DataSource)initContext.lookup("jdbc/AMINFO");
		conn = ds.getConnection();
		cs = conn.prepareCall(sql);
		cs.registerOutParameter(1, Types.CLOB);
		cs.setString(2, sBuilder.toString());
		cs.setString(3,userId);
		cs.setString(4,ipAddress);
		cs.execute();
		result = cs.getString(1);
	} catch (SQLException e) {
		code = 500;
		result = "{\"success\":\"false\",\"request\":" + sBuilder.toString() + ","
			+ "\"message\":\"" + e.getMessage() + "\"}";
	} catch (NamingException e) {
		code = 500;
		result = "{\"success\":\"false\",\"request\":" + sBuilder.toString() + ","
			+ "\"message\":\"" + e.getMessage() + "\"}";
	} finally {
		try {
			cs.close();
		} catch(Exception e) {}
		try {
			conn.close();
		} catch (Exception e) {}
	}
	response.setStatus(code);
%>
<% out.println(result); %>
