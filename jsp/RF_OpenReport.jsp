﻿<%@page
import="javax.naming.InitialContext"
import="javax.naming.Context"
import="javax.sql.DataSource"
import="java.sql.SQLException"
import="java.sql.DriverManager"
import="java.text.DateFormat"
import="java.sql.Connection"
import="java.sql.CallableStatement"
language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
  final String sql = "{ call mantas.rf_pkg_apex_adapter.create_entry_ticket(?,?,?) }";

  String reportId, ipAddress, userName, url, errMsg;

  reportId = request.getParameter("rep_id");  
  ipAddress = request.getRemoteAddr();
  userName = session.getAttribute("gsUsrID").toString();
  errMsg = "";

  if (reportId != null && !reportId.isEmpty() &&
      ipAddress != null && !ipAddress.isEmpty() &&
      userName != null && !userName.isEmpty()) {

	Connection conn = null;
	CallableStatement cs = null;

    try {
      Context initContext = new InitialContext();
      DataSource ds = (DataSource)initContext.lookup("jdbc/AMINFO");
      conn = ds.getConnection();
    
      cs = conn.prepareCall(sql);
      cs.setString(1, ipAddress);
      cs.setString(2, reportId);
      cs.setString(3, userName);
      cs.execute();
      
      url = "/apex/f?p=" + reportId;
      response.sendRedirect(url);
    } 
    catch (Exception e) {
      errMsg = "Ошибка при открытии отчета <nb>" + e.getMessage().replace("\"", "'");
    } 
    finally {
      try {
        cs.close();
      } 
      catch(Exception e) {}
      try {
        conn.close();
      } 
      catch (Exception e) {}
		}      
  }  
  else {  
    errMsg = "Ошибка при определении параметров, необходимых для открытия отчета: reportId = " + reportId + 
             ", ipAddress = " + ipAddress + ", userName = " + userName;
  }
%>

<%
	if (errMsg != null && !errMsg.isEmpty()) {
%>
	<%=errMsg%>
<%
	}
%>
