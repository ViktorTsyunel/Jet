<%@page 
  import="java.io.InputStreamReader"
  import="java.io.InputStreamReader"
  import="java.io.BufferedReader"
  import="java.io.OutputStream"
  import="javax.naming.InitialContext"
  import="javax.naming.Context"
  import="javax.sql.DataSource"
  import="org.apache.commons.io.IOUtils"
  import="java.sql.Blob"
  import="java.sql.Types"
  import="java.sql.DriverManager"
  import="java.sql.Connection"
  import="java.sql.CallableStatement"
  import="org.apache.commons.fileupload.util.Streams"
  import="java.io.InputStream"
  import="org.apache.commons.fileupload.FileItemStream"
  import="org.apache.commons.fileupload.FileItemIterator"
  import="org.apache.commons.fileupload.servlet.ServletFileUpload"
  language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%
	String form = null;
	String action = null;
	String attchId = null;
	String userId = session.getAttribute("gsUsrID").toString();
	String ipAddress = request.getRemoteAddr();

	/* Проверка, что сообщение multipart */
	if (ServletFileUpload.isMultipartContent(request) == true) {

		ServletFileUpload upload = new ServletFileUpload();
		FileItemIterator iter =  null;

		/* Читаем значения ключей в запросе. Файлы сохраняем в массиве */
		iter = upload.getItemIterator(request);
		while (iter.hasNext()) {
			FileItemStream fileItemStream = iter.next();
			String fieldName = fileItemStream.getFieldName();
			String fieldValue = null;
			InputStream inputStream = fileItemStream.openStream();
			if (fileItemStream.isFormField()) {
				if (fieldName.equals("form"))
					form = Streams.asString(inputStream);  
				else if (fieldName.equals("action"))
					action = Streams.asString(inputStream); 
				else if (fieldName.equals("attchId"))
					attchId = Streams.asString(inputStream);
			}
		}
		
		/* Читаем файл из БД */
		int code = 200;
		CallableStatement cs = null;
		Connection conn = null;
		String userName = session.getAttribute("gsUsrID").toString();
		try { 
			final String sql = "{ ? = call mantas.rf_pkg_review.get_attachment(?,?,?,?,?,?,?) }";

			Context initContext = new InitialContext();
			DataSource ds = (DataSource)initContext.lookup("jdbc/AMINFO");
			conn = ds.getConnection();
			cs = conn.prepareCall(sql);

			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setInt(2, Integer.parseInt(attchId));
			cs.registerOutParameter(3, Types.VARCHAR);
			cs.registerOutParameter(4, Types.VARCHAR);
			cs.registerOutParameter(5, Types.BLOB);
			cs.registerOutParameter(6, Types.VARCHAR);
			cs.setString(7, userName);
			cs.setString(8, ipAddress);
			cs.execute();
			String message = cs.getString(1);
			String fileName = cs.getString(3);
			String contentType = cs.getString(4);
			Blob contentData = cs.getBlob(5);
			String charset = cs.getString(6);
			
			response.setContentType(contentType);
			response.setCharacterEncoding(charset);
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileName);
			response.setHeader(headerKey, headerValue);
			
			InputStream is = contentData.getBinaryStream();
			OutputStream os = response.getOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = is.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}	
			is.close();
			os.close();
		
		} catch (Exception e) {
			code = 500;
			response.getWriter().print("Ошибка чтения файла: " + e.getMessage());
		} finally {
			try {
				cs.close();
			} catch(Exception e) {}
			try {
				conn.close();
			} catch (Exception e) {}
		}
		response.setStatus(code);
	}
%>
