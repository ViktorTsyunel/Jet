<%@page 
import="org.apache.commons.io.FilenameUtils"
import="org.apache.commons.io.FilenameUtils"
import="org.json.JSONObject"
import="oracle.jdbc.driver.Message"
import="java.io.IOException"
import="org.apache.commons.io.IOUtils"
import="java.io.ByteArrayInputStream"
import="java.util.ArrayList"
import="java.io.File"
import="java.sql.DriverManager"
import="javax.sql.DataSource"
import="javax.naming.NamingException"
import="java.sql.SQLException"
import="java.sql.Types"
import="javax.naming.InitialContext"
import="javax.naming.Context"
import="java.sql.CallableStatement"
import="java.sql.Connection"
import="java.io.InputStream"
import="org.apache.commons.fileupload.util.Streams"
import="org.apache.commons.fileupload.FileItemStream"
import="org.apache.commons.fileupload.FileItemIterator"
import="org.apache.commons.fileupload.servlet.ServletFileUpload"
language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	class Attachment {
		private String name;
		private String type;
		private String charset;
		private byte[] body;

		Attachment (String name, String type, InputStream body) throws IOException {
			this.name = FilenameUtils.getName(name);
			this.type = type.substring(0, type.concat(";").indexOf(";"));
			if (type.indexOf("charset=") > 0 ) {
				this.charset = type.substring(type.indexOf("charset=")+8, type.concat(";").indexOf(";", type.indexOf("charset=")));
			} else {
				this.type = type;
				this.charset = "";
			}
			this.body = IOUtils.toByteArray(body);
		}

		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		public String getCharset() {
			return charset;
		}

		public byte[] getBody() {
			return body;
		}
	}
%>

<%
	JSONObject j = new JSONObject();
	String result = "";
	String userId = session.getAttribute("gsUsrID").toString();
	String ipAddress = request.getRemoteAddr();

	/* Проверка, что сообщение multipart */
	if (ServletFileUpload.isMultipartContent(request) == true) {

		String form = null;
		String alertId = null;
		String fileName = null;
		String attachmentName = null;
		String cmmntId = null;
		String noteTx = null;
		ArrayList<Attachment> attachList = new ArrayList<Attachment>();

		ServletFileUpload upload = new ServletFileUpload();
		FileItemIterator iter =  null;

		/* Читаем значения ключей в запросе. Файлы сохраняем в массиве */
		iter = upload.getItemIterator(request);
		while (iter.hasNext()) {
			FileItemStream fileItemStream = iter.next();
			String fieldName = fileItemStream.getFieldName();
			String fieldValue = null;
			InputStream inputStream = fileItemStream.openStream();
			if (fileItemStream.isFormField()) {
				if (fieldName.equals("form"))
					form = Streams.asString(inputStream);  
				else if (fieldName.equals("alertId"))
					alertId = Streams.asString(inputStream); 
				else if (fieldName.equals("fileName"))
					fileName = Streams.asString(inputStream);
				else if (fieldName.equals("attachmentName"))
					attachmentName = Streams.asString(inputStream);
				else if (fieldName.equals("cmmntId"))
					cmmntId = Streams.asString(inputStream);
				else if (fieldName.equals("noteTx"))
					noteTx = Streams.asString(inputStream);
			} else {
				attachList.add( new Attachment( 
					fileItemStream.getName(), 
					fileItemStream.getContentType(), 
					inputStream ) );
			}
		}

		/* Сохраням файлы в БД */
		int code = 200;
		String errorMessages = " ";
		CallableStatement cs = null;
		Connection conn = null;
		String userName = session.getAttribute("gsUsrID").toString();
		try { 
			final String sql = "{ ? = call mantas.rf_pkg_review.add_attachment(?,?,?,?,?,?,?,?,?,?) }";

			Context initContext = new InitialContext();
			DataSource ds = (DataSource)initContext.lookup("jdbc/AMINFO");
			conn = ds.getConnection();
			cs = conn.prepareCall(sql);

			cs.registerOutParameter(1, Types.VARCHAR);
			if (alertId.isEmpty()) {
				cs.setNull(2, Types.NULL);
			} else {
				cs.setInt(2, Integer.parseInt(alertId));
			}
			cs.setString(3, userName);
			cs.setString(4, attachmentName);
			if (cmmntId.isEmpty()) {
				cs.setNull(5, Types.NULL);
			} else {
				cs.setInt(5, Integer.parseInt(cmmntId));
			}
			cs.setString(6, noteTx);

			for (Attachment a : attachList) {
				if (a.getBody().length > 0 ) {
					cs.setString(7, a.getName());
					cs.setString(8, a.getType());
					cs.setString(9, a.getCharset());
					cs.setBinaryStream(10, (InputStream)(new ByteArrayInputStream(a.getBody())), a.getBody().length);
					cs.setString(11, ipAddress);
					cs.execute();
					String message = cs.getString(1);
					if (message != null) {
					    errorMessages = errorMessages + "\n" + message;
					} else {
					    result = result.isEmpty() 
							? "Файл(ы) успешно добавлен(ы): " + a.getName() 
							: result + ", " + a.getName();  
					}
				}
			}

			if (errorMessages != null) {
				result = result + "\n" + errorMessages;
			}

			/* Request Json */
			j.put("success", true);
			j.put("message", result.trim());

		} catch (SQLException e) {
			code = 500;
			j.put("success", false);
			j.put("message", result.trim() + "\n" + e.getMessage().replace("\"", "'"));
		} catch (Exception e) {
			code = 500;
			j.put("success", false);
			j.put("message", result.trim() + "\n" + e.getMessage().replace("\"", "'"));
		} finally {
			try {
				cs.close();
			} catch(Exception e) {}
			try {
				conn.close();
			} catch (Exception e) {}
		}
		response.setStatus(code);
	}
%>

<%= j.toString() %>
