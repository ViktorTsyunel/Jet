Ext.define('AML.ux.mixins.App', {
	requires: [
		'Ext.window.*',
		'Ext.util.MixedCollection',
		'Ext.util.Cookies'
	],
	msg: {
		_getType: function (type) {
			var result = {};
			switch (type) {
				case 'error':
					result = {
						key: 'info',
						icon: Ext.Msg.ERROR,
						color: '#f00',
						title: 'Ошибка'
					};
					break;
				case 'warning':
					result = {
						key: 'info',
						icon: Ext.Msg.WARNING,
						color: '#ff0',
						title: 'Предупреждение'
					};
					break;
				case 'question':
					result = {
						key: 'info',
						icon: Ext.Msg.QUESTION,
						color: '#fff',
						title: 'Подтверждение'
					};
					break;
				case 'info':
				default:
					result = {
						key: 'info',
						icon: Ext.Msg.INFO,
						color: '#fff',
						title: 'Информация'
					};
					break;
			}
			return result;
		},
		/**
		 * Всплывашка подтверждения
		 * @param opt
		 * @param scope
		 */
		confirm: function (opt, scope) {
			var title = opt.title,
				message = opt.message || 'Вы уверены что хотите продолжить?',
				fnYes = opt.fnYes,
				fnNo = opt.fnNo,
				type = AML.app.msg._getType(opt.type || 'question');
			Ext.Msg.show({
				title: title /*|| type.title*/,
				message: message,
				buttons: Ext.Msg.YESNO,
				icon: type.icon,
				draggable: false,
				closable: false,
				fn: function (btn) {
					if (btn === 'yes') {
						if (fnYes && Ext.isFunction(fnYes)) {
							fnYes();
						}
					} else {
						if (fnNo && Ext.isFunction(fnNo)) {
							fnNo();
						}
					}
				}
			});
		},
		/**
		 * Всплывашка "тост"
		 * @param opt
		 * @param scope
		 */
		toast: function (opt, scope) {
			var message = (Ext.isObject(opt) ? opt.message : opt),
				type = AML.app.msg._getType(opt.type);
			Ext.toast({
				title: opt.title || type.title,
				html: message,
				listeners: {
					show: function (component, eOpts) {
						component.body.animate({
							to: {
								backgroundColor: opt.animColorStart || type.color
							}
						}).animate({
							duration: 500,
							to: {
								backgroundColor: opt.animColorEnd || '#fff'
							}
						});
					}
				}
			});
		}
	},
	page: {
		/**
		 * Показываем страницу в нужном контейнере
		 * @param name
		 * @param container
		 */
		show: function (name, container, defaultPage) {
			// Получаем явно указанную страницу или открытую в прошлый раз или по умолчанию
			var pageName = name || Ext.util.Cookies.get('aml-page-last') || defaultPage,
				page = (pageName ? AML.app.widget.get(pageName, 'page') : undefined);
			if (page) {
				// Записываем открываемую страницу
				Ext.util.Cookies.set(
					'aml-page-last',
					pageName,
					// Добавляем 1 год к текущей дате -
					// это время жизни сохраненного состояния
					Ext.Date.add(
						new Date(),
						Ext.Date.YEAR,
						1
					)
				);
				// Скрываем остальные страницы
				AML.app.widget.hideOthersInGroup(pageName, 'page');
				// Добавляем страницу в контейнер
				container.add(page.widget);
				// Отображаем контейнер
				container.show();
			}
		},
		clearLast: function () {
			// Чистим последнюю страницу
			Ext.util.Cookies.set(
				'aml-page-last',
				''
			);
		}
	},
	window: {
		/**
		 * Показываем окно
		 * @param name
		 */
		show: function (name) {
			// Получаем явно указанное окно или открытое в прошлый раз
			var windowName = name || Ext.util.Cookies.get('aml-window-last'),
				window = (windowName ? AML.app.widget.get(windowName, 'window') : undefined);
			if (window) {
				// Записываем открываемое окно
				Ext.util.Cookies.set(
					'aml-window-last',
					windowName,
					// Добавляем 1 год к текущей дате -
					// это время жизни сохраненного состояния
					Ext.Date.add(
						new Date(),
						Ext.Date.YEAR,
						1
					)
				);
				// Отображаем окно
				window.widget.show();
			}
		},
		clearLast: function () {
			// Чистим последнее окно
			Ext.util.Cookies.set(
				'aml-window-last',
				''
			);
		}
	},
	widget: {
		/**
		 * Создаем коллекцию для виджетов
		 */
		collection: Ext.create('Ext.util.MixedCollection'),
		/**
		 * Получаем виджет
		 * @param widget
		 * @param group
		 * @returns {*}
		 */
		get: function (widget, group) {
			return this.collection.get(widget) || this.add(widget, group);
		},
		/**
		 * Добавляем виджет
		 * @param widget
		 * @param group
		 * @returns {*}
		 */
		add: function (widget, group) {
			return this.collection.add(widget, { widget: Ext.widget(widget), group: group });
		},
		/**
		 * Скрываем остальные виджеты в группе
		 * @param widget
		 * @param group
		 */
		hideOthersInGroup: function (widget, group) {
			this.collection.eachKey(function (key, item) {
				if (item.group == group) {
					item.widget.setVisible(key == widget);
				}
			}, this);
		}
	},
	lexeme: {
		/**
		 * Функции подаются параметры:
		 *  1) Строка текста, в которой надо раскрасить встретившиеся лексемы
		 *  2) Строка лексем вида "<перечень лексем набора 1 через запятую>|<перечень лексем набора 2 через запятую>|и т. д.",
		 * например: "страх,жизн,прем,выпл,пенс,страх,накоп|займ,заим,ссуд,кред,фин,пом"
		 * Функция раскрашивает лексемы в строке текста, каждый набор - своим цветом и возвращает соответствующий HTML
		 * Основная идея алгоритма:
		 *  1) ищем лексемы в тексте
		 *  2) заменяем очередную найденную лексему на строку вида [#<номер>#], запоминаем - какой текст и каким цветом должен быть на этом месте
		 *  3) отдельным проходом заменяем строки [#<номер>#] на соответствующие тэги span
		 * Запоминание замен и отдельный проход нужны для корректной работы в случае, если лексемы пересекаются в исходном тексте по своим позициям
		 * ВНИМАНИЕ! Функция не вполне корректно работает, если в качестве лексем используются числа (0, 1, 2, 3 и т. д.) и при этом они повторяются в строке лексем!
		 * - Дмитрий Дубренский
		 *
		 * @param p_fieldText
		 * @param p_lexList
		 * @returns {*}
		 */
		colored: function (p_fieldText, p_lexList) {
			if (!p_fieldText || !p_lexList || p_fieldText.length == 0 || p_lexList.length == 0) {
				return p_fieldText;
			}
			var resText = p_fieldText;
			var lexReplace = [];
			var color = [
				"red",
				"green",
				"blue",
				"brown"
			];
			var ind = -1, k = 0;
			var lexSet = p_lexList.split("|");
			// Цикл по наборам лексем
			for (i = 0; i < lexSet.length; i++) {
				var lex = lexSet[i].split(",");
				// Цикл по лексемам
				for (j = 0; j < lex.length; j++) {
					lex[j] = lex[j].replace(" ", ""); // trim() doesnt work in IE8
					// Заменяем лексему пока она встречается в строке текста
					do {
						// Лексема не должна быть пустой
						if (!lex[j] || lex[j].length == 0) {
							ind = -1;
						} else {
							// Ищем лексему, начиная с позиции после последней найденной
							if (ind < 0) {
								ind = 0;
							}
							ind = resText.toUpperCase().indexOf(lex[j].toUpperCase(), ind);
						}
						// Заменяем лексему на порядковый номер замены, например, [#1#]
						if (ind >= 0) {
							lexReplace[k] = "<span class=\"lexeme " + color[i % color.length] + "\">" + resText.substring(ind, ind + lex[j].length) + "</span>";
							resText = resText.substring(0, ind) + "[#" + k.toString() + "#]" + resText.substring(ind + lex[j].length);
							ind = ind + 4 + k.toString().length;
							k++;
						}
					} while (ind >= 0)
				}
			}
			// Заменяем порядковые номера на лексемы с соответствующим цветом
			for (k = 0; k < lexReplace.length; k++) {
				resText = resText.replace("[#" + k.toString() + "#]", lexReplace[k]);
			}
			return resText;
		}
	}
});