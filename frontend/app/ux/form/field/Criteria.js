Ext.define('AML.ux.form.field.Criteria', {
	requires: [
		'Ext.button.Button',
		'Ext.container.Container',
		'Ext.data.StoreManager',
		'Ext.form.field.ComboBox',
		'Ext.layout.container.HBox'
	],
	extend: 'Ext.form.FieldContainer',
	xtype: 'criteriafield',
	layout: 'hbox',
	initComponent: function () {
		this.items = [
			{
				name: this.name,
				xtype: 'combobox',
				store: this.store,
				editable: false,
				queryMode: 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{CRIT_NM}</div>',
					'</tpl>'
				),
				matchFieldWidth: false,
				displayField: 'CRIT_NM',
				valueField: 'CRIT_CD',
				labelWidth: this.labelWidth,
				labelAlign: this.labelAlign,
				bind: '{record.' + this.name + '}',
				fieldLabel: 'Стандартное условие',
				value: this.value,
				triggerClear: this.triggerClear,
				flex: this.flex,
				listeners: {
					afterrender: function (component, eOpts) {
						component.ownerCt.down('button').setDisabled(!this.getValue());
					},
					change: function (field, newValue, oldValue, eOpts) {
						field.ownerCt.down('button').setDisabled(!newValue);
					}
				}
			},
			{
				xtype: 'container',
				width: 5
			},
			{
				xtype: 'button',
				iconCls: 'icon-applications',
				handler: function (button, e) {
                    // перезагрузка хранилища:
                    Ext.data.StoreManager.lookup('OPOKCriteria').reload();

                    var store = Ext.data.StoreManager.lookup('OPOKCriteria'),
					    lexeme = store.findRecord('CRIT_CD', button.ownerCt.down('combobox').getValue(), 0, false, false, true);
					if (lexeme) {
                        var form = Ext.widget({
                            xtype: 'app-settings-opokcriteria-form',
                            viewModel: {
                                data: {
                                    record: lexeme,
                                    isOpenReadOnly: true
                                },
                                parent: this.getViewModel()
                            }
                        }).show();
					}
				}
			}
		];
		this.callParent();
	}
});
