Ext.define('AML.ux.form.field.Rule', {
	requires: [
		'Ext.button.Button',
		'Ext.container.Container',
		'Ext.data.StoreManager',
		'Ext.form.field.ComboBox',
		'Ext.layout.container.HBox'
	],
	extend: 'Ext.form.FieldContainer',
	xtype: 'rulefield',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	initComponent: function () {
		var status = Ext.data.StoreManager.lookup('OPOKRuleStatus'),
			category = Ext.data.StoreManager.lookup('OPOKRuleOperationCategory');
		this.items = [
			{
				name: this.name,
				xtype: 'combobox',
				store: this.store,
				editable: false,
				queryMode: 'local',
				displayTpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
						'№: {RULE_CD} ({VERS_NUMBER}), ОПОК: {OPOK_NB}, {[ this.getStatus(values.VERS_STATUS_CD) ]}, {[ this.getCategory(values.OP_CAT_CD) ]}',
						'<tpl if="NOTE_TX">',
							', {NOTE_TX}',
						'</tpl>',
					'</tpl>',
					{
						disableFormats: true,
						getStatus: function (id) {
							return (status && status.getById(id).get('label')) || 'Нет';
						},
						getCategory: function (id) {
							return (category && category.getById(id).get('label')) || 'Нет';
						}
					}
				),
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
						'<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">',
							'№: {RULE_CD} ({VERS_NUMBER}), ОПОК: {OPOK_NB}, {[ this.getStatus(values.VERS_STATUS_CD) ]}, {[ this.getCategory(values.OP_CAT_CD) ]}',
							'<tpl if="NOTE_TX">',
								', {NOTE_TX}',
							'</tpl>',
						'</div>',
					'</tpl>',
					{
						disableFormats: true,
						getStatus: function (id) {
							return (status && status.getById(id).get('label')) || 'Нет';
						},
						getCategory: function (id) {
							return (category && category.getById(id).get('label')) || 'Нет';
						}
					}
				),
				displayField: 'RULE_CD',
				valueField: '_id_',
				labelWidth: this.labelWidth,
				labelAlign: this.labelAlign,
				fieldLabel: this.fieldLabel,
				emptyText: this.emptyText,
				tooltip: this.tooltip,
				value: this.value,
				allowBlank: false,
				triggerClear: true,
				flex: this.flex,
				listeners: {
					afterrender: function (component, eOpts) {
						component.ownerCt.down('button').setDisabled(!this.getValue());
					},
					change: function (field, newValue, oldValue, eOpts) {
						field.ownerCt.down('button').setDisabled(!newValue);
					}
				}
			},
			{
				xtype: 'container',
				width: 5
			},
			{
				xtype: 'button',
				iconCls: 'icon-applications',
				handler: function (button, e) {
					var rule = Ext.data.StoreManager.lookup('OPOKRule').findRecord('_id_', button.ownerCt.down('combobox').getValue());
					if (rule) {
						var form = Ext.widget('app-settings-opokrule-form'),
							formViewModel = form.getViewModel();
						formViewModel.setData(Ext.apply(formViewModel.getData(), {record: rule, isOpenReadOnly: true}));
						/*form.down('form').cascade(function (f) {
							f.setReadOnly && f.setReadOnly(true);
						});
						form.down('#saveForm').setDisabled(true);
						form.down('#createVersion').setDisabled(true);
						form.down('#runTest').setDisabled(true);*/
						form.show();
					}
				}
			}
		];
		this.callParent();
	}
});
