// кастомные настройки приложения:
Ext.override(Ext.data.proxy.Ajax, { timeout: common.globalinit.ajaxTimeOut });
Ext.override(Ext.form.Basic, { timeout: common.globalinit.ajaxTimeOut / 1000 });
Ext.override(Ext.data.proxy.Server, { timeout: common.globalinit.ajaxTimeOut });
Ext.override(Ext.data.Connection, { timeout: common.globalinit.ajaxTimeOut });
// приложение:
Ext.define('AML.Application', {
	extend: 'Ext.app.Application',
	name: 'AML',
	mixins: [
		'AML.ux.mixins.App'
	],
	requires: [
		'common.globalinit',
		'AML.store.ObjectsAccess',
		'AML.store.Roles',
		'AML.store.OPOKCriteriaGrid',
        'AML.store.OPOKCriteriaT1',
		'AML.store.OPOKCriteriaT2',
		'AML.store.OPOKCriteriaT3',
		'AML.store.OPOKCriteriaT4',
		'AML.store.OPOKCriteriaT5',
		'AML.store.OPOKDetailsGrid',
		'AML.store.OPOKGrid',
		'AML.store.OPOKRuleField',
		'AML.store.OPOKTestGrid',
		'Ext.Ajax',
		'Ext.Date',
		'Ext.JSON',
		'Ext.data.*',
		'Ext.grid.RowEditor',
		'Ext.state.CookieProvider',
		'Ext.state.Manager'
	],
	// todo: поменял сортировку для хранилищ, но вариант не идеален, нужно покурить в сторону зависимостей моделей/хранилищ
	stores: [
	    // Права доступа
		'ObjectsAccess',
		'Roles',

		// Справочники
		'Owners',
		'SearchDirection',
		'SearchTypes',
		'Statuses',
		'TB',
		'OES321Org',
		'TBOSB',
        'DOCTYPEREF',
        'OKATO',
        'Coutries',
		'TBFULL',
		'CUR',
		'OPOKaddZero',

		// Детали для ОПОК'ов
		'OPOKDetails',

		// Коды ОПОК
		'OPOK',

		// Стандартные условия
		'OPOKCriteriaTypes',
		'OPOKCriteriaUses',
		'OPOKCriteria',

		// Правила выявления
		'OPOKRuleAccountSearchArea',
		'OPOKRuleInOut',
		'OPOKRuleLexemeSearchArea',
		'OPOKRuleOperationCategory',
		'OPOKRuleQuery',
		'OPOKRuleKGRKO',
		'OPOKRuleStatus',
		'OPOKRule',

		// Поиск
		'OPOKTest',

		// Выявленные операции
		'OPOKEdit'
	],
	views: [
		'main.Main',
		'main.MainAbout',
		'settings.OPOK',
		'settings.OPOKForm',
		'settings.OPOKCriteria',
		'settings.OPOKCriteriaForm',
        'settings.OPOKRule',
        'settings.OPOKRuleForm',
        'settings.OPOKTest',
        'assign.AssignRule',
        'assign.AssignRuleForm',
        'assign.AssignRuleFormController',
        'assign.AssignReplacement',
        'assign.AssignReplacementController',
        'assign.AssignReplacementForm',
        'assign.AssignReplacementFormController',
        'assign.AssignLimit',
        'assign.AssignLimitController',
        'assign.AssignLimitForm',
        'assign.AssignLimitFormController',

        'oes.OES321Org.List',
        'oes.OES321Org.ListController',
        'oes.OES321Org.ListModel',
        'oes.OES321Org.Form',
        'oes.OES321Org.FormController',
        'oes.OES321Org.FormModel',

		'oes.OES321PartyOrg.List',
		'oes.OES321PartyOrg.ListController',
		'oes.OES321PartyOrg.ListModel',
		'oes.OES321PartyOrg.Form',
		'oes.OES321PartyOrg.FormController',
		'oes.OES321PartyOrg.FormModel',

		'oes.OESCheckRule.List',
		'oes.OESCheckRule.ListController',
		'oes.OESCheckRule.ListModel',
		'oes.OESCheckRule.Form',
		'oes.OESCheckRule.FormController',
		'oes.OESCheckRule.FormModel',

        // Мониторинг
		'monitoring.alerts.Alerts.List',
		'monitoring.alerts.AlertsSnapshotForm',
		'monitoring.alerts.OPOKEditForm',
		'monitoring.alerts.Create.CreateAlertForm',
        'monitoring.alerts.OES321Details.Tabs.Forms.AddressSelection',
        'monitoring.alerts.OES321Details.Tabs.Forms.SelectDocument',
        'monitoring.alerts.OES321Details.Tabs.Forms.ChoosingRepresentative',
        'monitoring.alerts.OES321Details.Tabs.Forms.SearchClient',
        'monitoring.alerts.OES321Details.Tabs.Forms.SearchClientTopbar',

		// импорт
		'typeimport.List',
		'typeimport.ListController',
		'typeimport.ListModel',
		'typeimport.AddImportAttachmentForm.Form',
		'typeimport.AddImportAttachmentForm.FormController',
		'typeimport.AddImportAttachmentForm.FormModel'
	],
	init: function () {
        // вызываем инициализация общих настроек для всего приложения:
        common.globalinit.init();
        Ext.override(Ext.util.Format, { thousandSeparator: ' ' });

        // Получим права пользователя на объекты (синхронным запросом)
		var store = Ext.getStore('ObjectsAccess');
		var async = Ext.Ajax.getAsync();
		if (store) {
			Ext.Ajax.setAsync(false);
			store.load();
			Ext.Ajax.setAsync(async);
			}
		var storeRoles = Ext.getStore('Roles');
		if (storeRoles) {
			Ext.Ajax.setAsync(false);
			storeRoles.load();
			Ext.Ajax.setAsync(async);
		}
		// Зеркальные хранилища
		// todo: только так смог починить ошибку ChainedStore в IE8, нужно еще думать/разбераться...
		Ext.data.StoreManager.add('OPOKDetailsGrid', Ext.create('AML.store.OPOKDetailsGrid'));
		Ext.data.StoreManager.add('OPOKGrid', Ext.create('AML.store.OPOKGrid'));
        Ext.data.StoreManager.add('OPOKCriteriaT1', Ext.create('AML.store.OPOKCriteriaT1'));
		Ext.data.StoreManager.add('OPOKCriteriaT2', Ext.create('AML.store.OPOKCriteriaT2'));
		Ext.data.StoreManager.add('OPOKCriteriaT3', Ext.create('AML.store.OPOKCriteriaT3'));
		Ext.data.StoreManager.add('OPOKCriteriaT4', Ext.create('AML.store.OPOKCriteriaT4'));
		Ext.data.StoreManager.add('OPOKCriteriaT5', Ext.create('AML.store.OPOKCriteriaT5'));
		Ext.data.StoreManager.add('OPOKRuleField', Ext.create('AML.store.OPOKRuleField'));
		Ext.data.StoreManager.add('OPOKTestGrid', Ext.create('AML.store.OPOKTestGrid'));
		Ext.data.StoreManager.add('LimitTypes', Ext.create('AML.store.LimitTypes'));
        // Ext.data.StoreManager.add('Roles', Ext.create('AML.store.Roles'));
		/**
		 * Определяем провайдера (в нашем случае печеньки)
		 * для хранения состояний пользовательского интерфейса
		 */
		Ext.state.Manager.setProvider(
			new Ext.state.CookieProvider({
				prefix: 'aml-state-',
				// Добавляем 1 год к текущей дате -
				// это время жизни сохраненного состояния
				expires: Ext.Date.add(
					new Date(),
					Ext.Date.YEAR,
					1
				)
			})
		);

		/**
		* Полифил для map
		*
		*/
		// Шаги алгоритма ECMA-262, 5-е издание, 15.4.4.19
		// http://es5.github.com/#x15.4.4.19
		if (!Array.prototype.map) {

			Array.prototype.map = function(callback, thisArg) {

				var T, A, k;

				if (this == null) {
					throw new TypeError(' this is null or not defined');
				}

				// 1. Положим O равным результату вызова ToObject с передачей ему
				//    значения |this| в качестве аргумента.
				var O = Object(this);

				// 2. Положим lenValue равным результату вызова внутреннего метода Get
				//    объекта O с аргументом "length".
				// 3. Положим len равным ToUint32(lenValue).
				var len = O.length >>> 0;

				// 4. Если вызов IsCallable(callback) равен false, выкидываем исключение TypeError.
				// Смотрите (en): http://es5.github.com/#x9.11
				// Смотрите (ru): http://es5.javascript.ru/x9.html#x9.11
				if (typeof callback !== 'function') {
					throw new TypeError(callback + ' is not a function');
				}

				// 5. Если thisArg присутствует, положим T равным thisArg; иначе положим T равным undefined.
				if (arguments.length > 1) {
					T = thisArg;
				}

				// 6. Положим A равным новому масиву, как если бы он был создан выражением new Array(len),
				//    где Array является стандартным встроенным конструктором с этим именем,
				//    а len является значением len.
				A = new Array(len);

				// 7. Положим k равным 0
				k = 0;

				// 8. Пока k < len, будем повторять
				while (k < len) {

					var kValue, mappedValue;

					// a. Положим Pk равным ToString(k).
					//   Это неявное преобразование для левостороннего операнда в операторе in
					// b. Положим kPresent равным результату вызова внутреннего метода HasProperty
					//    объекта O с аргументом Pk.
					//   Этот шаг может быть объединён с шагом c
					// c. Если kPresent равен true, то
					if (k in O) {

						// i. Положим kValue равным результату вызова внутреннего метода Get
						//    объекта O с аргументом Pk.
						kValue = O[k];

						// ii. Положим mappedValue равным результату вызова внутреннего метода Call
						//     функции callback со значением T в качестве значения this и списком
						//     аргументов, содержащим kValue, k и O.
						mappedValue = callback.call(T, kValue, k, O);

						// iii. Вызовем внутренний метод DefineOwnProperty объекта A с аргументами
						// Pk, Описатель Свойства
						// { Value: mappedValue,
						//   Writable: true,
						//   Enumerable: true,
						//   Configurable: true }
						// и false.

						// В браузерах, поддерживающих Object.defineProperty, используем следующий код:
						// Object.defineProperty(A, k, {
						//   value: mappedValue,
						//   writable: true,
						//   enumerable: true,
						//   configurable: true
						// });

						// Для лучшей поддержки браузерами, используем следующий код:
						A[k] = mappedValue;
					}
					// d. Увеличим k на 1.
					k++;
				}

				// 9. Вернём A.
				return A;
			};
		}
		/* Полифилл для indexOf */
		// http://es5.github.io/#x15.4.4.14
		if (!Array.prototype.indexOf) {
			Array.prototype.indexOf = function(searchElement, fromIndex) {
				var k;

				// 1. Положим O равным результату вызова ToObject с передачей ему
				//    значения this в качестве аргумента.
				if (this == null) {
					throw new TypeError('"this" is null or not defined');
				}

				var O = Object(this);

				// 2. Положим lenValue равным результату вызова внутреннего метода Get
				//    объекта O с аргументом "length".
				// 3. Положим len равным ToUint32(lenValue).
				var len = O.length >>> 0;

				// 4. Если len равен 0, вернём -1.
				if (len === 0) {
					return -1;
				}

				// 5. Если был передан аргумент fromIndex, положим n равным
				//    ToInteger(fromIndex); иначе положим n равным 0.
				var n = +fromIndex || 0;

				if (Math.abs(n) === Infinity) {
					n = 0;
				}

				// 6. Если n >= len, вернём -1.
				if (n >= len) {
					return -1;
				}

				// 7. Если n >= 0, положим k равным n.
				// 8. Иначе, n<0, положим k равным len - abs(n).
				//    Если k меньше нуля 0, положим k равным 0.
				k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

				// 9. Пока k < len, будем повторять
				while (k < len) {
					// a. Положим Pk равным ToString(k).
					//   Это неявное преобразование для левостороннего операнда в операторе in
					// b. Положим kPresent равным результату вызова внутреннего метода
					//    HasProperty объекта O с аргументом Pk.
					//   Этот шаг может быть объединён с шагом c
					// c. Если kPresent равен true, выполним
					//    i.  Положим elementK равным результату вызова внутреннего метода Get
					//        объекта O с аргументом ToString(k).
					//   ii.  Положим same равным результату применения
					//        Алгоритма строгого сравнения на равенство между
					//        searchElement и elementK.
					//  iii.  Если same равен true, вернём k.
					if (k in O && O[k] === searchElement) {
						return k;
					}
					k++;
				}
				return -1;
			};
		}
		/* Полифилл для filter */
		if (!Array.prototype.filter) {
			Array.prototype.filter = function(fun/*, thisArg*/) {
				'use strict';

				if (this === void 0 || this === null) {
					throw new TypeError();
				}

				var t = Object(this);
				var len = t.length >>> 0;
				if (typeof fun !== 'function') {
					throw new TypeError();
				}

				var res = [];
				var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
				for (var i = 0; i < len; i++) {
					if (i in t) {
						var val = t[i];

						// ПРИМЕЧАНИЕ: Технически, здесь должен быть Object.defineProperty на
						//             следующий индекс, поскольку push может зависеть от
						//             свойств на Object.prototype и Array.prototype.
						//             Но этот метод новый и коллизии должны быть редкими,
						//             так что используем более совместимую альтернативу.
						if (fun.call(thisArg, val, i, t)) {
							res.push(val);
						}
					}
				}

				return res;
			};
		}
		// Полифилл для forEach
		// http://es5.github.io/#x15.4.4.18
        if (!Array.prototype.forEach) {
            Array.prototype.forEach = function (callback, thisArg) {
                var T, k;
                if (this == null) {
                    throw new TypeError(' this is null or not defined');
                }
                var O = Object(this);
                var len = O.length >>> 0;
                if (typeof callback !== 'function') {
                    throw new TypeError(callback + ' is not a function');
                }
                if (arguments.length > 1) {
                    T = thisArg;
                }
                k = 0;
                while (k < len) {
                    var kValue;
                    if (k in O) {
                        kValue = O[k];
                        callback.call(T, kValue, k, O);
                    }
                    k++;
                }
                // Вернём undefined.
            };
        }

// Полифилл для closest & matches
		(function(e){
			e.closest = e.closest || function(css){
					var node = this;

					while (node) {
						if (node.matches(css)) return node;
						else node = node.parentElement;
					}
					return null;
				};

			e.matches || (e.matches=e.matchesSelector||function(selector){
					var matches = document.querySelectorAll(selector), th = this;
					return Array.prototype.some.call(matches, function(e){
						return e === th;
					});
				});

		})(Element.prototype);

	},
	launch: function () {

	}
});