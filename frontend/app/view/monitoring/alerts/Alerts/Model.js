/**
 * Модель представления для выявленных операций
 */
Ext.define('AML.view.monitoring.alerts.Alerts.Model', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alerts',
    data: {
        title: 'Выявленные операции',
        footerRecord: null,
        showFilter: false,
        showBBar: false,
        selectedAlert: null,
        scopeFilters: '9001',
        newCountAlert: 0,
        newRvTrxnSeqIDs: [],
        filteredCount: 0,
        sumRUB: 0,
        search: {
            dtFrom: null,
            dtTo: null,
            ownerIds: null,
            tbIds: null,
            osbIds: null,
            opok: null,
            opokType: 'ALL',
            operationNumber: null,
            status: null,
            amountFrom: null,
            amountTo: null
        },
        searchRaw: {
            dtFrom: '',
            dtTo: '',
            ownerIds: '',
            tbIds: '',
            osbIds: '',
            opok: '',
            opokType: '',
            operationNumber: '',
            status: '',
            amountFrom: '',
            amountTo: ''
        },
        statistics: {
            "WORK_QTY": 0,
            "NEARDUE_QTY": 0,
            "OVERDUE_QTY": 0
        },
        fetchId: null
    },
    formulas: {
        footerRecord: function (get) {
            return get('selectedAlert');
        },
        coloredTRXN_DESC: function (get) {
            var text = get('footerRecord.TRXN_DESC'),
                lexeme = get('footerRecord.TRXN_DESC_LEX');
            return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
        },
        coloredORIG_NM: function (get) {
            var text = get('footerRecord.ORIG_NM'),
                lexeme = get('footerRecord.ORIG_LEX');
            return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
        },
        coloredBENEF_NM: function (get) {
            var text = get('footerRecord.BENEF_NM'),
                lexeme = get('footerRecord.BENEF_LEX');
            return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
        },
        RV_TRXN_SEQ_ID: function(get) {
            return get('selectedAlert.RV_TRXN_SEQ_ID') <= 0;
        }
    },

    stores: {
        // Выявленные операции
        alertsStore: {
            model: 'AML.model.Alerts',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                beforeload: function(store, operation) {
                    // если мы пришли после создания сообщения - грузим его для обработки
                    var viewAppMain = Ext.getCmp('app-main-id'),
                        vmAppMain = (viewAppMain) ? viewAppMain.getViewModel() : false,
                        alertId = (vmAppMain) ? vmAppMain.get('createAlertReviewId') : false,
                        alertId2 = (vmAppMain) ? vmAppMain.get('createAlertReviewId2') : false,
                        alIds = [];
                    if (alertId) {
                        alIds.push(alertId);
                    }
                    if (alertId2) {
                        alIds.push(alertId2);
                    }
                    if (alertId || alertId2) {
                        operation.setParams({alertIds: alIds});
                    }
                },
                load: 'onLoadAlerts'
            },
            sorters: [
                {
                    property: 'SORT_KEY',
                    direction: 'ASC'
                }
            ]
        },

        alertsFiltersCollection: {
            model: 'AML.model.AlertsFiltersCollection',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'AlertsFiltersCollection',
                    action: 'read'
                }
            },
            listeners: {
                load: 'onLoadFiltersCollection'
            }
        },

        systemViewStore: {
            fields: ['id', 'label'],
            data: [
                {id: 0, label: 'EKS'},
                {id: 1, label: 'BCK'},
                {id: 2, label: 'NAV'},
                {id: 3, label: 'GAT'}
            ]
        },

        regionSearch: {
            fields: ['id', 'all', 'label', 'short'],
            data: [
                {id: 0, all: 'Счет проводки - СП', label: 'Счет проводки', short: 'СП'},
                {id: 1, all: 'Счет участника - СУ', label: 'Счет участника', short: 'СУ'}
            ]
        }
    }
});