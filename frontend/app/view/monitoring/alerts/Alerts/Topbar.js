Ext.define('AML.view.monitoring.alerts.Alerts.Topbar', {
    extend: 'Ext.container.Container',
    xtype: 'app-monitoring-alerts-topbar',
    reestablishView: false,
    reference: 'monitoringAlertsTopbar',
    items: [
        {
            xtype: 'fieldcontainer',
            isFilters: true,
            hidden: true,
            bind: {
                hidden: '{!showFilter}'
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'column',
                        pack: 'bottom'
                    },
                    flex: 1,
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 100,
                        margin: '2px 2px 2px 2px',
                        flex: 1
                    },
                    items: [
                        {
                            width: 160,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Вид операции',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 55
                                    },
                                    items: [
                                        {
                                            name: 'opok',
                                            reference: 'opok',
                                            xtype: 'combobox',
                                            store: 'OPOK',
                                            editable: true,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                                '</tpl>'
                                            ),
                                            displayField: 'OPOK_NB',
                                            valueField: 'OPOK_NB',
                                            fieldLabel: 'Код',
                                            bind: '{search.opok}',
                                            flex: 1,
                                            triggerClear: true,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                }
                                            }
                                        },
                                        {
                                            name: 'opokType',
                                            reference: 'opokType',
                                            xtype: 'combobox',
                                            store: 'SearchTypes',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'искать в',
                                            displayField: 'label',
                                            valueField: 'id',
                                            triggerClear: true,
                                            bind: '{search.opokType}',
                                            flex: 1,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 350,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'left',
                                labelWidth: 150
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Выявленная операция',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 45
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'left',
                                                labelWidth: 45
                                            },
                                            items: [
                                                {
                                                    name: 'operationNumber',
                                                    reference: 'operationNumber',
                                                    xtype: 'numberfield',
                                                    hideTrigger: true,
                                                    keyNavEnabled: false,
                                                    mouseWheelEnabled: false,
                                                    allowBlank: true,
                                                    minValue: 1,
                                                    step: 1,
                                                    fieldLabel: 'Номер',
                                                    bind: '{search.operationNumber}',
                                                    padding: '0 5 0 0',
                                                    width: 120,
                                                    listeners: {
                                                        change: function(field, value) {
                                                            var pvalue = parseInt(value, 10);
                                                            if (pvalue <= 0) {
                                                                field.setValue(1);
                                                            } else {
                                                                field.setValue(pvalue);
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    name: 'ownerIds',
                                                    reference: 'ownerIds',
                                                    xtype: 'combobox',
                                                    store: 'Owners',
                                                    editable: true,
                                                    typeAhead: true,
                                                    typeAheadDelay: 500,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                                        '</tpl>'
                                                    ),
                                                    fieldLabel: 'Ответственный',
                                                    labelWidth: 90,
                                                    displayField: 'OWNER_DSPLY_NM', //'login',
                                                    valueField: 'id',
                                                    triggerClear: true,
                                                    // typeAhead and multiSelect are mutually exclusive options :(
                                                    // multiSelect: true,
                                                    bind: {
                                                        value: '{search.ownerIds}'
                                                    },
                                                    flex: 1,
                                                    forceSelection: true,
                                                    matchFieldWidth: false
                                                    // listeners: {
                                                    //     expand: function (field) {
                                                    //         field.getStore().currentUser = field.getStore().findRecord('IS_CURRENT', true);
                                                    //         var is_supervis = (field.getStore().currentUser.get('IS_SUPERVISOR') === "true");
                                                    //         if (!is_supervis) {
                                                    //             field.getStore().addFilter({
                                                    //                 property: 'IS_CURRENT',
                                                    //                 value: true
                                                    //             });
                                                    //         }
                                                    //         field.getStore().addFilter([{property: "IS_ALLOW_ALERT_EDITING", value: "Y"}]);
                                                    //         field.getPicker().refresh();
                                                    //     }
                                                    // }
                                                }
                                            ]
                                        },
                                        {
                                            name: 'status',
                                            reference: 'status',
                                            xtype: 'combobox',
                                            store: 'Statuses',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'Статус',
                                            displayField: 'label',
                                            valueField: '_id_',
                                            triggerClear: true,
                                            multiSelect: true,
                                            bind: '{search.status}',
                                            padding: '5 0 0 0',
                                            flex: 1,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 140,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Сумма операции',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 20
                                    },
                                    items: [
                                        {
                                            name: 'amountFrom',
                                            reference: 'amountFrom',
                                            xtype: 'textfield',
                                            fieldLabel: 'от',
                                            bind: '{search.amountFrom}',
                                            enableKeyEvents: true,
                                            maskRe: new RegExp('[0-9,.]'),
                                            //preventMark: false,
                                            validateOnChange: false,
                                            validateOnBlur: false,
                                            listeners: {
                                                /*change: function (field, newValue, oldValue, eOpts) {
                                                 this.suspendEvent('change');
                                                 field.setValue(Ext.util.Format.number(parseFloat(newValue.replace(new RegExp(' ', 'g'), '')), '0,000.00'));
                                                 field.nextSibling('textfield').focus();
                                                 field.focus();
                                                 this.resumeEvent('change');
                                                 },*/
                                                dblclick : {
                                                    fn: 'onDbClickAmountFrom',
                                                    element: 'el'
                                                },
                                                blur: function (container, value, eOpts) {
                                                    if (value) {
                                                        container.clearInvalid();
                                                        if (container.nextSibling('textfield').getValue()) {
                                                            container.nextSibling('textfield').clearInvalid();
                                                            if (parseFloat(container.nextSibling('textfield').getRawValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) < parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))) {
                                                                container.markInvalid('Значение "от" не может быть больше "до"');
                                                            }
                                                        }
                                                    }
                                                    container.setValue(Ext.util.Format.number(String(parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                                }
                                            },
                                            flex: 1
                                        },
                                        {
                                            name: 'amountTo',
                                            reference: 'amountTo',
                                            xtype: 'textfield',
                                            fieldLabel: 'до',
                                            bind: '{search.amountTo}',
                                            maskRe: new RegExp('[0-9,.]'),
                                            validateOnChange: false,
                                            validateOnBlur: false,
                                            listeners: {
                                                /*change: function (field, newValue, oldValue, eOpts) {
                                                 this.suspendEvent('change');
                                                 field.setValue(Ext.util.Format.number(parseFloat(newValue.replace(new RegExp(' ', 'g'), '')), '0,000.00'));
                                                 field.previousSibling('textfield').focus();
                                                 field.focus();
                                                 this.resumeEvent('change');
                                                 },*/
                                                dblclick : {
                                                    fn: 'onDbClickAmountTo',
                                                    element: 'el'
                                                },
                                                blur: function (container, value, eOpts) {
                                                    if (value) {
                                                        container.clearInvalid();
                                                        if (container.previousSibling('textfield').getValue()) {
                                                            container.previousSibling('textfield').clearInvalid();
                                                            if (parseFloat(container.previousSibling('textfield').getRawValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) > parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))) {
                                                                container.markInvalid('Значение "от" не может быть больше "до"');
                                                            }
                                                        }
                                                        container.setValue(Ext.util.Format.number(String(parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                                    }
                                                }
                                            },
                                            flex: 1
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 140,
                            xtype: 'fieldcontainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Дата операции',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 20
                                    },
                                    items: [
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'от',
                                            name: 'dtFrom',
                                            reference: 'dtFrom',
                                            bind: '{search.dtFrom}',
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: true,
                                            listeners: {
                                                scope: this,
                                                change: function (field, newValue, oldValue, eOpts) {
                                                    var toDate = field.up('fieldset').down('datefield[name=dtTo]');
                                                    toDate.setMinValue(newValue);
                                                    if (Ext.Date.diff(newValue, toDate.getValue(), Ext.Date.DAY) < 0) {
                                                        toDate.setValue(newValue);
                                                    }
                                                }
                                            },
                                            flex: 1
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'до',
                                            name: 'dtTo',
                                            reference: 'dtTo',
                                            bind: '{search.dtTo}',
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: true,
                                            flex: 1
                                        }
                                    ]
                                }
                            ],
                            flex: 1
                        },
                        {
                            width: 190,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Подразделение',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 35
                                    },
                                    items: [
                                        {
                                            name: 'tbIds',
                                            reference: 'tbIds',
                                            xtype: 'combobox',
                                            store: 'TB',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                                '<tpl if="id &gt; 0">{id} - {label}' +
                                                '<tpl else>{label}</tpl>' +
                                                '</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'ТБ',
                                            displayField: 'label',
                                            valueField: 'id',
                                            triggerClear: true,
                                            multiSelect: true,
                                            matchFieldWidth: false,
                                            listeners: {
                                                scope: this,
                                                change: function (field, newValue, oldValue, eOpts) {
                                                    var osbIds = field.up('fieldset').down('combobox[name=osbIds]'),
                                                        tbosb = Ext.data.StoreManager.lookup('TBOSB');
                                                    tbosb.clearFilter();
                                                    osbIds.clearValue();
                                                    if (newValue) {
                                                        tbosb.filterBy(function (record) {
                                                            return Ext.Array.indexOf(newValue, record.get("tb_id")) !== -1;
                                                        }, this);
                                                    }
                                                    osbIds.setDisabled(!newValue || tbosb.getData().length < 1);
                                                    var tooltip = null;
                                                    tooltip = new Ext.ToolTip({
                                                        target: field.getEl(),
                                                        anchor: 'top',
                                                        anchorToTarget: true,
                                                        //html: c.getRawValue(),
                                                        listeners: {
                                                            scope: this,
                                                            beforeshow: function(tip) {
                                                                tip.setHtml(field.getRawValue());
                                                            }
                                                        }
                                                    });
                                                    return tooltip;
                                                }
                                            },
                                            bind: '{search.tbIds}',
                                            flex: 1
                                        },
                                        {
                                            name: 'osbIds',
                                            reference: 'osbIds',
                                            xtype: 'combobox',
                                            store: 'TBOSB',
                                            disabled: true,
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'ОСБ',
                                            displayField: 'label',
                                            valueField: 'id',
                                            triggerClear: true,
                                            multiSelect: true,
                                            matchFieldWidth: false,
                                            bind: '{search.osbIds}',
                                            flex: 1,
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                },
                                                change: function(c) {
                                                    var tooltip = null;
                                                    tooltip = new Ext.ToolTip({
                                                        target: c.getEl(),
                                                        anchor: 'top',
                                                        anchorToTarget: true,
                                                        //html: c.getRawValue(),
                                                        listeners: {
                                                            scope: this,
                                                            beforeshow: function(tip) {
                                                                tip.setHtml(c.getRawValue());
                                                            }
                                                        }
                                                    });
                                                    return tooltip;
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            width: 240,
                            layout: {
                                type: 'vbox'
                            },
                            padding: '2 0 0',
                            layoutConfig: {
                                align: 'left'
                            },
                            items: [
                                {
                                    name: 'oldAlert',
                                    reference: 'oldAlert',
                                    xtype: 'checkboxfield',
                                    fieldLabel: 'Давно обработанные операции',
                                    labelAlign: 'left',
                                    bind: '{search.oldAlert}',
                                    labelWidth: 210
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    margin: '0 0 0 0',
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'vbox'
                                            },
                                            margin: '0 0 0 0',
                                            items: [
                                                {
                                                    xtype: 'label',
                                                    bind: {
                                                        text: 'Кол-во операций: {filteredCount}'
                                                    },
                                                    layout: {
                                                        align: 'center',
                                                        pack: 'center'
                                                    },
                                                    margin: '0 0 0 0',
                                                    width: 160
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'button',
                                            flex: 1,
                                            align: 'right',
                                            margin: '0 0 3 0',
                                            text: 'Подсчитать',
                                            handler: 'countFilterToggle'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'label',
                                    bind: {
                                        text: 'Сумма в рублях: {sumRUB}'
                                    },
                                    layout: {
                                        align: 'center',
                                        pack: 'center'
                                    },
                                    margin: '0 0 0 0'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            width: 15,
                            height: 80,
                            layout: {
                                type: 'hbox',
                                align: 'bottom',
                                pack: 'bottom'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    width: 15,
                                    tooltip: 'Дополнительные фильтры',
                                    reference: 'buttomFilterExpand1',
                                    height: 15,
                                    //padding: 1,
                                    border: 0,
                                    text: '',
                                    cls: 'arrow-r-ico closed reverse',
                                    enableToggle: true,
                                    buttonAlign: 'bottom',
                                    listeners: {
                                        toggle: 'toggleAddedFilters'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldcontainer',
            isFilters: true,
            hidden: true,
            bind: {
                hidden: '{!showAddedFilter}'
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'column',
                        pack: 'bottom'
                    },
                    flex: 1,
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 100,
                        margin: '2px 2px 2px 2px',
                        flex: 1
                    },
                    items: [
                        {
                            width: 300,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Плательщик',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 50
                                    },
                                    items: [
                                        {
                                            name: 'payerINNAddedFilter',
                                            reference: 'payerINNAddedFilter',
                                            xtype: 'textfield',
                                            hideTrigger: true,
                                            allowBlank: true,
                                            fieldLabel: 'ИНН плательщика',
                                            labelWidth: 120,
                                            width: 300
                                            //padding: '0 0 2 0'
                                        },
                                        {
                                            name: 'payerAccountNumberAddedFilter',
                                            reference: 'payerAccountNumberAddedFilter',
                                            xtype: 'textfield',
                                            hideTrigger: true,
                                            allowBlank: true,
                                            fieldLabel: 'Номер счета',
                                            labelWidth: 120,
                                            width: 300
                                            //padding: '0 0 2 0'
                                        },
                                        {
                                            name: 'payerRegionSearchddedFilter',
                                            reference: 'payerRegionSearchddedFilter',
                                            xtype: 'combobox',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{all}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'Область поиска',
                                            labelWidth: 120,
                                            displayField: 'label',
                                            valueField: 'short',
                                            triggerClear: true,
                                            multiSelect: true,
                                            bind: {
                                                store: '{regionSearch}'
                                            },
                                            flex: 1,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            //padding: '0 0 2 0',
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                },
                                                change: function(c) {
                                                    var tooltip = new Ext.ToolTip({
                                                        target: c.getEl(),
                                                        html: c.getRawValue()
                                                    });
                                                    return tooltip;
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 300,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'left'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Получатель',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 45
                                    },
                                    items: [
                                        {
                                            name: 'recieverINNAddedFilter',
                                            reference: 'recieverINNAddedFilter',
                                            xtype: 'textfield',
                                            hideTrigger: true,
                                            allowBlank: true,
                                            fieldLabel: 'ИНН получателя',
                                            labelWidth: 120,
                                            width: 300
                                            //padding: '0 0 2 0'
                                        },
                                        {
                                            name: 'recieverAccountNumberAddedFilter',
                                            reference: 'recieverAccountNumberAddedFilter',
                                            xtype: 'textfield',
                                            hideTrigger: true,
                                            allowBlank: true,
                                            fieldLabel: 'Номер счета',
                                            labelWidth: 120,
                                            width: 300
                                            //padding: '0 0 2 0'
                                        },
                                        {
                                            name: 'recieverRegionSearchddedFilter',
                                            reference: 'recieverRegionSearchddedFilter',
                                            xtype: 'combobox',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{all}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'Область поиска',
                                            labelWidth: 120,
                                            displayField: 'label',
                                            valueField: 'short',
                                            triggerClear: true,
                                            multiSelect: true,
                                            bind: {
                                                store: '{regionSearch}'
                                            },
                                            flex: 1,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            //padding: '0 0 2 0',
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                },
                                                change: function(c) {
                                                    var tooltip = new Ext.ToolTip({
                                                        target: c.getEl(),
                                                        html: c.getRawValue()
                                                    });
                                                    return tooltip;
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 650,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Платежный документ',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left'
                                    },
                                    items: [
                                        {
                                            name: 'paydocAccountNumberAddedFilter',
                                            reference: 'paydocAccountNumberAddedFilter',
                                            xtype: 'textfield',
                                            hideTrigger: true,
                                            allowBlank: true,
                                            fieldLabel: 'Номер документа',
                                            labelWidth: 140
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'left',
                                                labelWidth: 140
                                            },
                                            items: [
                                                {
                                                    xtype: 'datefield',
                                                    width: 300,
                                                    fieldLabel: 'Дата документа',
                                                    name: 'paydocDateAddedFilter',
                                                    reference: 'paydocDateAddedFilter',
                                                    format: 'd.m.Y',
                                                    startDay: 1,
                                                    padding: '0 5 0 0',
                                                    allowBlank: true
                                                },
                                                {
                                                    name: 'paydocSystemAddedFilter',
                                                    reference: 'paydocSystemAddedFilter',
                                                    xtype: 'combobox',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    fieldLabel: 'Система-источник',
                                                    labelWidth: 110,
                                                    displayField: 'label',
                                                    valueField: 'label',
                                                    triggerClear: true,
                                                    multiSelect: true,
                                                    bind: {
                                                        store: '{systemViewStore}'
                                                    },
                                                    flex: 1,
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    //padding: '0 0 2 0',
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function(c) {
                                                            var tooltip = new Ext.ToolTip({
                                                                target: c.getEl(),
                                                                html: c.getRawValue()
                                                            });
                                                            return tooltip;
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            name: 'paydocPayRuleAddedFilter',
                                            reference: 'paydocPayRuleAddedFilter',
                                            xtype: 'textfield',
                                            hideTrigger: true,
                                            allowBlank: true,
                                            fieldLabel: 'Назначение платежа',
                                            labelWidth: 140,
                                            padding: '5 0 0 0'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'buttongroup',
            layout: {
                type: 'hbox'
            },
            defaults: {
                margin: '1px 3px 1px 3px'
            },
            items: [
                {
                    xtype: 'button',
                    reference: 'subjectToControlBtn',
                    text: 'Подлежит <span style="text-decoration:underline;">к</span>онтролю',
                    tooltip: 'Подлежит контролю (Alt + к)',
                    bind: {
                        disabled: '{Alerts_READONLY}'
                    },
                    margin: '1px 3px 1px 1px',
                    iconCls: 'icon-document-exclamation',
                    handler: 'subjectToControlAction'
                },
                {
                    xtype: 'button',
                    reference: 'cancelInvalidTokenBtn',
                    text: 'Отменить (неверная <span style="text-decoration:underline;">л</span>ексема)',
                    tooltip: 'Отменить (неверная лексема) (Alt + л)',
                    bind: {
                        disabled: '{Alerts_READONLY}'
                    },
                    iconCls: 'icon-document-minus',
                    handler: 'cancelInvalidTokenAction'
                },
                {
                    xtype: 'button',
                    reference: 'editCancelBtn',
                    text: '<span style="text-decoration:underline;">О</span>тменить',
                    tooltip: 'Отменить (Alt + о)',
                    bind: {
                        disabled: '{Alerts_READONLY}'
                    },
                    iconCls: 'icon-document-minus',
                    handler: 'editCancelAction'
                },
                {
                    xtype: 'button',
                    text: '<span style="text-decoration:underline;">Н</span>а отправку',
                    tooltip: 'На отправку (Alt + н)',
                    bind: {
                        disabled: '{Alerts_READONLY}'
                    },
                    iconCls: 'icon-document-to-send',
                    handler: 'toSendAction'
                },
                {
                    xtype: 'button',
                    text: '<span style="text-decoration:underline;">Д</span>ействия',
                    tooltip: 'Действия (Alt + д)',
                    bind: {
                        disabled: '{Alerts_READONLY}'
                    },
                    iconCls: 'icon-gear',
                    handler: 'editAction'
                },
                {
                    xtype: 'button',
                    text: 'Дополнительно',
                    bind: {
                        disabled: '{Alerts_READONLY}'
                    },
                    iconCls: 'icon-big-plus',
                    menu:{
                        items:[
                            {
                                text: 'Создать копию ОЭС',
                                tooltip: 'Создать копию ОЭС',
                                bind: {
                                    disabled: '{Alerts_READONLY}'
                                },
                                iconCls: 'icon-applications-stack',
                                handler: 'copySelectedOES'
                            },
                            {
                                text: 'Пр<span style="text-decoration:underline;">и</span>крепить файл',
                                tooltip: 'Прикрепить файл(Alt + и)',
                                bind: {
                                    disabled: '{Alerts_READONLY}'
                                },
                                iconCls: 'icon-document-plus',
                                handler: 'attachAction'
                            },
                            {
                                text: 'Указать филиалы КГРКО',
                                tooltip: 'Указать филиалы КГРКО',
                                bind: {
                                    disabled:  '{Alerts_READONLY}'
                                },
                                iconCls: 'icon-border-color',
                                handler: 'setFilialsKGRKO'
                            },
                            {
                                text: 'Подлежит контролю в 2-х филиалах КГРКО (2 ОЭС)',
                                tooltip: 'Подлежит контролю в 2-х филиалах КГРКО (2 ОЭС)',
                                bind: {
                                    disabled:  '{Alerts_READONLY}'
                                },
                                iconCls: 'blue-document-convert',
                                handler: 'control2FilialsKGRKO'
                            }
                        ]
                    }
                },
                {
                    xtype: 'button',
                    text: '<span style="text-decoration:underline;">Э</span>кспорт',
                    tooltip: 'Экспорт (Alt + э)',
                    bind: {
                        disabled: '{Alerts_READONLY}'
                    },
                    iconCls: 'icon-export',
                    handler: 'exportAction'
                },
                {
                    name: 'scopeFilters',
                    reference: 'scopeFilters',
                    xtype: 'combobox',
                    editable: false,
                    queryMode: 'local',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{QUEUE_DISPLAY_NM}</div>',
                        '</tpl>'
                    ),
                    displayField: 'QUEUE_DISPLAY_NM',
                    valueField: '_id_',
                    triggerClear: true,
                    bind: {
                        store: '{alertsFiltersCollection}',
                        value: '{scopeFilter}'
                    },
                    flex: 1
                },
                {
                    xtype: 'button',
                    text: 'Показа<span style="text-decoration:underline;">т</span>ь',
                    tooltip: 'Показать (Alt + т)',
                    margin: '1px 1px 1px 3px',
                    iconCls: 'icon-document-search',
                    reference: 'showListMainBtn',
                    handler: 'showList'
                },
                {
                    xtype: 'button',
                    text: '>>',
                    tooltip: 'Загрузить следующую страницу',
                    margin: '1px 1px 1px 3px',
                    reference: 'readNextListPageBtn',
                    handler: 'readNextListPage'
                },
                {
                    xtype: 'button',
                    text: '>>|',
                    tooltip: 'Загрузить оставшиеся страницы',
                    margin: '1px 1px 1px 3px',
                    reference: 'readLastListPageBtn',
                    handler: 'readLastListPage'
                },
                {
                    xtype: 'button',
                    text: 'Детали',
                    enableToggle: true,
                    reference: 'toggleBBarBtn',
                    bind: {
                        disabled: '{!selectedAlert}'
                    },
                    listeners: {
                        toggle: 'toggleBBar'
                    }
                }
            ]
        }
    ]
});