/**
 * Представление для выявленных операций
 */
Ext.define('AML.view.monitoring.alerts.Alerts.List', {
    extend: 'Ext.Panel',
    requires: [
        'AML.view.monitoring.alerts.Alerts.Controller',
        'AML.view.monitoring.alerts.Alerts.Model',
        'Ext.Array',
        'Ext.button.Button',
        'Ext.container.ButtonGroup',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.Label',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Date',
        'Ext.grid.column.Number',
        'Ext.grid.column.Template',
        'Ext.grid.plugin.RowEditing',
        'Ext.layout.container.Border',
        'Ext.layout.container.HBox',
        'Ext.layout.container.Table',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        'Ext.toolbar.Fill',
        'Ext.data.StoreManager'
    ],
    xtype: 'app-monitoring-alerts',
    controller: 'alerts',
    viewModel: {
        type: 'alerts'
    },
    layout: 'border',
    border: false,
    cls: 'app-monitoring-alerts',
    reference: 'alertsList',
    items: [
        {
            xtype: 'gridpanel',
            region: 'center',
            reference: 'alertsGrid',
            bind: {
                store: {
                    bindTo: '{alertsStore}',
                    deep: true
                },
                selection: {
                    bindTo: '{selectedAlert}',
                    deep: true
                },
                hidden: '{menu.Alerts}' ? false : true
            },
            listeners: {
                selectionChange: function (selModel, record, index, options) {
                    var numbSelecRows = this.down('#numberSelectedRows');
                    numbSelecRows.removeAll();
                    if (selModel.getCount() > 1) {
                        numbSelecRows.add({
                            xtype: 'container',
                            bind: {
                                html: '<div class="counter">Кол-во выделенных операций: <b>' + selModel.getCount() + '</b></div>'
                            }
                        });
                    }
                }
            },
            stateful: false,
            stateId: 'alerts',
            header:{
                xtype: 'toolbar',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'label',
                        style: {
                            fontWeight: 'bold'
                        },
                        bind: {
                            text: '{title}'
                        }
                    },
                    '->',
                    {
                        xtype: 'button',
                        itemId: 'showFilterForMainAlerts',
                        text: 'Показать <span style="text-decoration:underline;">ф</span>ильтры',
                        reference: 'toggleFiltersBtn',
                        tooltip: 'Показать фильтры (Alt + ф)',
                        enableToggle: true,
                        listeners: {
                            toggle: 'toggleFilters'
                        }

                    },
                    {
                        xtype: 'button',
                        text: '<span style="text-decoration:underline;">У</span>читывать фильтры',
                        tooltip: 'Учитывать фильтры (Alt + у)',
                        reference: 'applyFiltersBtn',
                        enableToggle: true,
                        listeners: {
                            toggle: 'applyFilters'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Очистить фильтры',
                        reference: 'toggleClearFiltersBtn',
                        tooltip: 'Очистить фильтры',
                        //enableToggle: true,
                        handler: 'toggleClearFilters'
                    }
                ]
            },
            tbar: [
                {
                    xtype: 'app-monitoring-alerts-topbar',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    flex: 1
                }
            ],
            bbar: [
                '->',
                {
                    xtype: 'container',
                    id: 'numberSelectedRows',
                    bind: {
                        hidden: '{OES_READONLY_FL_CONVERSION}'
                    }
                },
                {
                    xtype: 'container',
                    bind: {
                        hidden: '{!newCountAlert}',
                        html: '<div class="counter">Создано сообщений: <b>{newCountAlert}</b></div>'
                    }
                },
                {
                    xtype: 'container',
                    bind: {
                        html: '<div class="counter">Загружено операций: <b>{totalCount}</b> из <b>{grandTotal}</b></div>'
                    }
                },
                {
                    xtype: 'container',
                    bind: {
                        html: '<div class="counter">Кол-во необработанных операций: <b>{statistics.WORK_QTY}</b></div>'
                    }
                },
                {
                    xtype: 'container',
                    bind: {
                        html: '<div class="counter">Кол-во операций, которые скоро будут просрочены: <b>{statistics.NEARDUE_QTY}</b></div>'
                    }
                },
                {
                    xtype: 'container',
                    bind: {
                        html: '<div class="counter">Кол-во просроченных операций: <b>{statistics.OVERDUE_QTY}</b></div>'
                    }
                },
                '->'
            ],
            plugins: {
                ptype: 'rowediting',
                errorSummary: false,
                // clicksToEdit: 2,
                listeners: {
                    beforeedit: 'beforeEditRow',
                    edit: 'afterEditRow',
                    setButtonPosition: 'left'
                }
            },
            columns: [
                {
                    text: 'Номер операции',
                    tooltip: 'Номер операции',
                    stateId: 'REVIEW_ID',
                    dataIndex: 'REVIEW_ID',
                    minWidth: 50,
                    flex: 0.5
                },
                {
                    text: 'Статус операции',
                    tooltip: 'Статус операции',
                    stateId: 'RV_STATUS_NM',
                    dataIndex: 'RV_STATUS_NM',
                    xtype: 'templatecolumn',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl if="RV_STATUS_HIGHLIGHT == 1">',
                        '<span><b>{RV_STATUS_NM}</b></span>',
                        '<tpl else>',
                        '<span>{RV_STATUS_NM}</span>',
                        '</tpl>'
                    ),
                    width: 150
                },
                {
                    text: 'Вид операции',
                    tooltip: 'Вид операции',
                    stateId: 'OPOK_NB',
                    dataIndex: 'OPOK_NB',
                    editor: {
                        xtype: 'combobox',
                        store: 'OPOK',
                        editable: true,
                        queryMode: 'local',
                        displayField: 'OPOK_NB',
                        valueField: 'OPOK_NB',
                        tpl: Ext.create('Ext.XTemplate',
                            '<tpl for=".">',
                            '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                            '</tpl>'
                        ),
                        triggerClear: true,
                        matchFieldWidth: false
                    },
                    flex: 0.5
                },
                {
                    text: 'Доп. вид операции',
                    stateId: 'ADD_OPOKS_TX',
                    dataIndex: 'ADD_OPOKS_TX',
                    tooltip: 'Дополнительный вид операции',
                    editor: {
                        xtype: 'combobox',
                        store: 'OPOK',
                        editable: true,
                        queryMode: 'local',
                        displayField: 'OPOK_NB',
                        valueField: 'OPOK_NB',
                        tpl: Ext.create('Ext.XTemplate',
                            '<tpl for=".">',
                            '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                            '</tpl>'
                        ),
                        triggerClear: true,
                        multiSelect : true,
                        matchFieldWidth: false
                    },
                    flex: 0.5
                },
                {
                    text: 'Статус сообщения',
                    tooltip: 'Статус сообщения',
                    stateId: 'CHECK_STATUS',
                    dataIndex: 'CHECK_STATUS',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        switch (value) {
                            case 'W':
                                metaData.tdStyle = 'background:yellow;font-weight:bold;';
                                return 'Есть предупреждения';
                                break;
                            case 'E':
                                metaData.tdStyle = 'background:red;font-weight:bold;';
                                return 'Требует доработки';
                                break;
                            case 'O':
                                metaData.tdStyle = 'background:green;font-weight:bold;';
                                return 'Контроль пройден';
                                break;
                            case 'S':
                                return 'Отправлено';
                                break;
                            case null:
                                if(record.getData().OES321ID) {
                                    return 'Не проверялось';
                                } else {
                                    return '';
                                }
                                break;
                            case '':
                                if(record.getData().OES321ID) {
                                    return 'Не проверялось';
                                } else {
                                    return '';
                                }
                                break;
                            default:
                                return '';
                                break;
                        }
                    },
                    minWidth: 160,
                    flex: 1
                },
                {
                    text: 'Сумма в рублях',
                    tooltip: 'Сумма в рублях',
                    stateId: 'TRXN_BASE_AM',
                    dataIndex: 'TRXN_BASE_AM',
                    xtype: 'numbercolumn',
                    format: '0.00',
                    align: 'right',
                    flex: 1,
                    renderer: function (value) {
                        if (value && value !== '') {
                            value = value.toString();
                            // return Ext.util.Format.number(parseFloat(value.replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')), '0,000.00');
                            return Ext.util.Format.number(String(parseFloat(value.replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00');
                        } else {
                            return value;
                        }
                    }
                },
                {
                    text: 'Сумма в валюте',
                    tooltip: 'Сумма в валюте',
                    stateId: 'TRXN_CRNCY_AM',
                    dataIndex: 'TRXN_CRNCY_AM',
                    xtype: 'numbercolumn',
                    format: '0.00',
                    align: 'right',
                    flex: 1,
                    renderer: function (value) {
                        if (value && value !== '') {
                            value = value.toString();
                            // return Ext.util.Format.number(parseFloat(value.replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')), '0,000.00');
                            return Ext.util.Format.number(String(parseFloat(value.replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00');
                        } else {
                            return value;
                        }
                    }
                },
                {
                    text: 'Валюта',
                    tooltip: 'Валюта',
                    stateId: 'TRXN_CRNCY_CD',
                    dataIndex: 'TRXN_CRNCY_CD',
                    flex: 0.5
                },
                {
                    text: 'Дата операции',
                    tooltip: 'Дата операции',
                    stateId: 'TRXN_DT',
                    dataIndex: 'TRXN_DT',
                    xtype: 'datecolumn',
                    format: 'd.m.Y',
                    flex: 1
                },
                {
                    text: 'Обработать к...',
                    tooltip: 'Обработать к...',
                    stateId: 'RV_DUE_DT',
                    dataIndex: 'RV_DUE_DT',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        if (record.data.RV_DUE_DT_HIGHLIGHT == 'OVERDUE') {
                            metaData.style = "background-color: #FF0000;";
                        } else if (record.data.RV_DUE_DT_HIGHLIGHT == 'NEARDUE') {
                            metaData.style = "background-color: #FF9999;";
                        }
                        return Ext.Date.format(value, 'd.m.Y');
                    },
                    flex: 1
                },
                {
                    text: 'Ответственный',
                    tooltip: 'Ответственный',
                    stateId: 'RV_OWNER_ID',
                    dataIndex: 'RV_OWNER_ID',
                    width: 100
                },
                {
                    text: 'Комментарий',
                    tooltip: 'Комментарий',
                    stateId: 'RV_NOTE_TX',
                    dataIndex: 'RV_NOTE_TX',
                    renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                        if (record.data.RV_NOTE_TX) {
                            metadata.tdAttr = metadata.tdAttr + ' data-qtip="' + Ext.util.Format.stripTags(Ext.util.Format.stripScripts(record.data.RV_NOTE_TX)) + '"';
                        }
                        return Ext.util.Format.stripScripts(value);
                    },
                    flex: 3
                },
                {
                    text: 'Нал/Бнал',
                    tooltip: 'Нал/Бнал',
                    stateId: 'OP_CAT_CD',
                    dataIndex: 'OP_CAT_CD',
                    flex: 0.5
                },
                {
                    text: 'Доп.инф.',
                    tooltip: 'Доп.инф.',
                    stateId: '',
                    dataIndex: 'RENDER_ATTACHMENTS',
                    flex: 0.5,
                    renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                        if (record.data.IS_INTER_KGRKO && record.data.HAS_ATTACHMENTS) {
                            metadata.tdCls = 'icon-document-kgrko-attach';
                            record.data.RENDER_ATTACHMENTS = 3;
                            return '';
                        }
                        if (record.data.IS_INTER_KGRKO && !record.data.HAS_ATTACHMENTS) {
                            metadata.tdCls = 'icon-document-kgrko';
                            record.data.RENDER_ATTACHMENTS = 2;
                            return '';
                        }
                        if (!record.data.IS_INTER_KGRKO && record.data.HAS_ATTACHMENTS) {
                            metadata.tdCls = 'icon-paper-clip-kgrko';
                            record.data.RENDER_ATTACHMENTS = 1;
                            return '';
                        }
                        if (!record.data.IS_INTER_KGRKO && !record.data.HAS_ATTACHMENTS) {
                            metadata.tdCls = '';
                            record.data.RENDER_ATTACHMENTS = 0;
                            return '';
                        }
                    }
                },
                {
                    text: 'Назначение платежа',
                    tooltip: 'Назначение платежа',
                    stateId: 'TRXN_DESC',
                    dataIndex: 'TRXN_DESC',
                    minWidth: 150,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Номер документа',
                    tooltip: 'Номер документа',
                    stateId: 'TRXN_DOC_ID',
                    dataIndex: 'TRXN_DOC_ID',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    xtype: 'datecolumn',
                    format: 'd.m.Y',
                    text: 'Дата документа',
                    tooltip: 'Дата документа',
                    stateId: 'TRXN_DOC_DT',
                    dataIndex: 'TRXN_DOC_DT',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Счет плательщика',
                    tooltip: 'Счет плательщика',
                    stateId: 'ORIG_ACCT_NB',
                    dataIndex: 'ORIG_ACCT_NB',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Счет получателя',
                    tooltip: 'Счет получателя',
                    stateId: 'BENEF_ACCT_NB',
                    dataIndex: 'BENEF_ACCT_NB',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'ИНН плательщика',
                    tooltip: 'ИНН плательщика',
                    stateId: 'ORIG_INN_NB',
                    dataIndex: 'ORIG_INN_NB',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'ИНН получателя',
                    tooltip: 'ИНН получателя',
                    stateId: 'BENEF_INN_NB',
                    dataIndex: 'BENEF_INN_NB',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Система-источник',
                    tooltip: 'Источник',
                    stateId: 'SRC_SYS_CD',
                    dataIndex: 'SRC_SYS_CD',
                    minWidth: 60,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Счет дебета',
                    tooltip: 'Счет дебета',
                    stateId: 'DEBIT_CD',
                    dataIndex: 'DEBIT_CD',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Счет кредита',
                    tooltip: 'Счет кредита',
                    stateId: 'CREDIT_CD',
                    dataIndex: 'CREDIT_CD',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Наименование плательщика',
                    tooltip: 'Наименование плательщика',
                    stateId: 'ORIG_NM',
                    dataIndex: 'ORIG_NM',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Наименование получателя',
                    tooltip: 'Наименование получателя',
                    stateId: 'BENEF_NM',
                    dataIndex: 'BENEF_NM',
                    minWidth: 100,
                    flex: 0.5,
                    hidden: true
                },
                {
                    text: 'Номер цен. бум.',
                    tooltip: 'Номер цен. бум.',
                    stateId: 'SCRTY_NB',
                    dataIndex: 'SCRTY_NB',
                    flex: 1,
                    hidden: true
                },
                {
                    text: 'Сумма цен. бум.',
                    tooltip: 'Сумма цен. бум.',
                    stateId: 'SCRTY_BASE_AM',
                    dataIndex: 'SCRTY_BASE_AM',
                    xtype: 'numbercolumn',
                    format: '0.00',
                    align: 'right',
                    flex: 1,
                    hidden: true,
                    renderer: function (value) {
                        if (value && value !== '') {
                            value = value.toString();
                            return Ext.util.Format.number(String(parseFloat(value.replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00');
                        } else {
                            return value;
                        }
                    }
                },
                {
                    text: 'Вид операции ГОЗ (списание)',
                    tooltip: 'Вид операции ГОЗ (списание)',
                    stateId: 'ORIG_GOZ_OP_CD',
                    dataIndex: 'ORIG_GOZ_OP_CD',
                    flex: 1,
                    hidden: true,
                    renderer: function(value, metaData) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    }
                },
                {
                    text: 'Вид операции ГОЗ (зачисление)',
                    tooltip: 'Вид операции ГОЗ (зачисление)',
                    stateId: 'BENEF_GOZ_OP_CD',
                    dataIndex: 'BENEF_GOZ_OP_CD',
                    flex: 1,
                    hidden: true,
                    renderer: function(value, metaData) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    }
                }
            ]
        },
        {
            xtype: 'app-monitoring-alerts-bottompanel',
            isFooter: true,
            region: 'south',
            height: '55%',
            maxHeight: 450,
            minHeight: 130,
            collapsed: true,
            collapsible: false,
            collapseMode: 'mini',
            header: false,
            resizable: true,
            cls: 'app-opoktest-footer',
            split: false,
            layout: {
                type: 'vbox'
            },
            bodyStyle: {
                padding: '5px'
            },
            defaults: {
                bodyStyle: {
                    padding: '3px',
                    border: 0
                }
            },
            scrollable: true,
            bind: {
                collapsed: '{!showBBar}'
            }
        }
    ]
});