/**
 * Контроллер представления для выявленных операций
 */
Ext.define('AML.view.monitoring.alerts.Alerts.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alerts',
    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                'app-monitoring-alerts': {
                    /**
                     * Обрабатываем событие отрисовки
                     * @param panel
                     */
                    afterrender: function (panel) {
                        var self = this,
                            vm = self.getViewModel(),
                            toggleFiltersBtn = this.lookupReference('toggleFiltersBtn'),
                            applyFiltersBtn = self.lookupReference('applyFiltersBtn'),
                            showListMainBtn = self.lookupReference('showListMainBtn'),
                            toggleBBarBtn = this.lookupReference('toggleBBarBtn');

                        Ext.getBody().addKeyMap({
                            eventName: "keydown",
                            binding: [
                                {
                                    key: Ext.event.Event.ESC, // Вешаем hotkey ESC для закрытия подвала
                                    ctrl: false,
                                    shift: false,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        toggleBBarBtn.setPressed(false);
                                    }
                                },
                                {
                                    key: Ext.event.Event.ENTER, // Вешаем hotkey ENTER для применения активного фильтра
                                    ctrl: false,
                                    shift: false,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        if (applyFiltersBtn.pressed && toggleFiltersBtn.pressed && !showListMainBtn.disabled) {
                                            self.showList();
                                        }
                                    }
                                },
                                {
                                    key: "f", // Вешаем hotkey Ctrl+f для открытия фильтров
                                    ctrl: true,
                                    shift: false,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        toggleFiltersBtn.setPressed(true);
                                    }
                                },
                                {
                                    key: "1", // Вешаем hotkey Alt + 1 на вкладку "Сведения об операции"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        //'oes321detailspanel-operationtab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(0);
                                    }
                                },
                                {
                                    key: "2", // Вешаем hotkey Alt + 2 на вкладку "Плательщик"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-payertab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(1);
                                    }
                                },
                                {
                                    key: "3", // Вешаем hotkey Alt + 3 на вкладку "Получатель"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-recipienttab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(2);
                                    }
                                },
                                {
                                    key: "4", // Вешаем hotkey Alt + 4 на вкладку "От имени и по поруч."
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-behalftab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(3);

                                    }
                                },
                                {
                                    key: "5", // Вешаем hotkey Alt + 5 на вкладку "Предст. плат-ка"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        //'oes321detailspanel-representativetab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(4);
                                    }
                                },
                                {
                                    key: "6", // Вешаем hotkey Alt + 6 на вкладку "Предст. Получ-ля"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-destinationtab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(5);
                                    }
                                },
                                {
                                    key: "k", // Вешаем hotkey Alt + Л на кнопку "Отменить (неверная лексема)"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1))) {
                                            return;
                                        }
                                        if (panel.isHidden()
                                            || self.getViewModel().get('Alerts_READONLY')
                                            || self.getViewModel().get('OES_READONLY_FL_CONVERSION')) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.cancelInvalidTokenAction();
                                    }
                                },
                                {
                                    key: "j", // Вешаем hotkey Alt + О на кнопку "Отменить"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');

                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || _vmpanel.get('Alerts_READONLY')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1))) {
                                            return;
                                        }
                                        if (panel.isHidden()
                                            || self.getViewModel().get('Alerts_READONLY')
                                            || self.getViewModel().get('OES_READONLY_FL_CONVERSION')) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.editCancelAction();
                                    }
                                },
                                {
                                    key: "d", // Вешаем hotkey Alt + В на кнопку "Вложения"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            _panel.controller.editAttachAction();
                                            return;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel
                                            && _panel.controller) {
                                            _panel.controller.editAttachAction();
                                        }

                                    }
                                },
                                {
                                    key: "l", // Вешаем hotkey Alt + Д на кнопку "Действия"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel')
                                            || self.getView().down('app-alerts-alertdetailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1))) {
                                            return;
                                        }
                                        if (panel.isHidden()
                                            || self.getViewModel().get('Alerts_READONLY')
                                            || self.getViewModel().get('OES_READONLY_FL_CONVERSION')) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.editAction();
                                    }
                                },
                                {
                                    key: "p", // Вешаем hotkey Alt + З на кнопку "Закрыть (детали)"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (self.getView().down('app-alerts-oes321detailspanel')) {
                                            self.getView().down('app-alerts-oes321detailspanel').controller.closePanel();
                                            return;
                                        }
                                        if (self.getView().down('app-alerts-alertdetailspanel')) {
                                            self.getView().down('app-alerts-alertdetailspanel').controller.closePanel();
                                            return;
                                        }
                                    }
                                },
                                {
                                    key: "b", // Вешаем hotkey Alt + И на кнопку "Прикрепить"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel')
                                            || self.getView().down('app-alerts-alertdetailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1))) {
                                            return;
                                        }
                                        if (panel.isHidden()
                                            || self.getViewModel().get('Alerts_READONLY')
                                            || self.getViewModel().get('OES_READONLY_FL_CONVERSION')) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.attachAction();
                                    }
                                },
                                {
                                    key: "r", // Вешаем hotkey Alt + К на кнопку "Подлежит контролю"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel')
                                            || self.getView().down('app-alerts-alertdetailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1)
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_OES') !== -1))) {
                                            return;
                                        }
                                        if (panel.isHidden()
                                            || self.getViewModel().get('Alerts_READONLY')
                                            || self.getViewModel().get('OES_READONLY_FL_CONVERSION')) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.subjectToControlAction();
                                    }
                                },
                                {
                                    key: "v", // Вешаем hotkey Alt + M на кнопку "Заметки (детали операции)"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            _panel.controller.editNoteAction();
                                            return;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel
                                            && _panel.controller) {
                                            _panel.controller.editNoteAction();
                                        }
                                    }
                                },
                                {
                                    key: "y", // Вешаем hotkey Alt + H на кнопку "На отправку"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-alertdetailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1))) {
                                            return;
                                        }
                                        if (panel.isHidden()
                                            || self.getViewModel().get('Alerts_READONLY')
                                            || self.getViewModel().get('OES_READONLY_FL_CONVERSION')) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.toSendAction();
                                    }
                                },
                                {
                                    key: "g", // Вешаем hotkey Alt + П на кнопку "Применить (детали)"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-alertdetailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1))) {
                                            return;
                                        }
                                        if (self.getView().down('app-alerts-oes321detailspanel')) {
                                            self.getView().down('app-alerts-oes321detailspanel').controller.applyChanges();
                                        }
                                    }
                                },
                                {
                                    key: "c", // Вешаем hotkey Alt + С на кнопку "История изменений"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            _panel.controller.showHistoryAction();
                                            return;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel
                                            && _panel.controller) {
                                            _panel.controller.showHistoryAction();
                                        }
                                    }
                                },
                                {
                                    key: "t", // Вешаем hotkey Alt + Е на кнопку "История изменений ОЭС"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        var _panel, _vmpanel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-alertdetailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            _panel.controller.showHistoryLogAction();
                                        }
                                    }
                                },
                                {
                                    key: "n", // Вешаем hotkey Alt + Т на кнопку "Показать (лупа)"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel')
                                            || self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel) {
                                            return false;
                                        }
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.showList();
                                    }
                                },
                                {
                                    key: "e", // Вешаем hotkey Alt + У на кнопку "Учитывать фильтры"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel')
                                            || self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel) {
                                            return false;
                                        }
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        var btn = self.lookupReference('applyFiltersBtn');
                                        // скрыть панель фильтра, если фильтр не применяется:
                                        btn.setPressed(!btn.pressed);
                                    }
                                },
                                {
                                    key: "a", // Вешаем hotkey Alt + Ф на кнопку "Показать фильтры"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel')
                                            || self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel) {
                                            return false;
                                        }
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();

                                        var btn = self.lookupReference('toggleFiltersBtn');
                                        btn.setPressed(!btn.pressed);
                                    }
                                },
                                {
                                    key: [222], // Вешаем hotkey Alt + Э на кнопку "Экспорт"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel')
                                            || self.getView().down('app-alerts-alertdetailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_vmpanel
                                            && (_vmpanel.get('historyAlert')
                                            || _vmpanel.get('OES_READONLY_FL_CONVERSION')
                                            || (_vmpanel.get('detailsTable.RV_STATUS_CD').indexOf('RF_REPEAT+') !== -1))) {
                                            return;
                                        }
                                        if (panel.isHidden()
                                            || self.getViewModel().get('Alerts_READONLY')
                                            || self.getViewModel().get('OES_READONLY_FL_CONVERSION')) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        self.exportAction();
                                    }
                                },
                                {
                                    key: "h", // Вешаем hotkey Alt + Р на кнопку "Результат контроля"
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        var _panel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-alertdetailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        if (_panel) {
                                            _panel.controller.openControlResult();
                                        }
                                    }
                                }
                            ]
                        });
                        this.getStatistics();
                    }
                },
                'gridpanel': {
                    /**
                     * Обрабатываем событие после отрисовки сетки
                     * @param grid
                     */
                    afterrender: function (grid) {
                        var self = this,
                            toggleBBarBtn = self.lookupReference('toggleBBarBtn'),
                            monitoringAlertsTopbar = self.lookupReference('monitoringAlertsTopbar'),
                            vm = this.getViewModel();
                        // добавляем пункты в контекстное меню:
                        grid.showOES321DetailsBtn = Ext.create('Ext.Action', {
                            iconCls: 'oes321-details-button',
                            text: 'Детали сообщения',
                            viewModel: {
                                parent: vm
                            },
                            bind: {
                                disabled: '{!selectedAlert.OES321ID}'
                            },
                            handler: function (widget, event) {
                                // если подвал открыт запоминаем активную запись
                                if (toggleBBarBtn.pressed) {
                                    monitoringAlertsTopbar.reestablishView = true;
                                }
                                // закрываем нижную панель детали операции:
                                toggleBBarBtn.setPressed(false);
                                self.getView().add({
                                    xtype: 'app-alerts-oes321detailspanel',
                                    viewModel: {
                                        parent: vm,
                                        type: 'oes321detailspanelviewmodel',
                                        data: {
                                            alertId: vm.get('selectedAlert.REVIEW_ID'),
                                            oes321Id: vm.get('selectedAlert.OES321ID')
                                        }
                                    }
                                });
                            }
                        });
                        grid.showAlertDetailsBtn = Ext.create('Ext.Action', {
                            iconCls: 'alert-details-button',
                            text: 'Детали операции',
                            viewModel: {
                                parent: vm
                            },
                            disabled: false,
                            bind: {
                                disabled: {
                                    bindTo: '{RV_TRXN_SEQ_ID}',
                                    deep: true
                                }
                            },
                            handler: function (widget, event) {
                                // если подвал открыт запоминаем активную запись
                                if (toggleBBarBtn.pressed) {
                                    monitoringAlertsTopbar.reestablishView = true;
                                }
                                // закрываем нижную панель детали операции:
                                toggleBBarBtn.setPressed(false);

                                self.getView().add({
                                    xtype: 'app-alerts-alertdetailspanel',
                                    viewModel: {
                                        parent: vm,
                                        data: {
                                            alertId: vm.get('selectedAlert.REVIEW_ID')
                                        }
                                    }
                                });
                            }
                        });
                        grid.showAlertDetailsOldBtn = Ext.create('Ext.Action', {
                            iconCls: 'alert-details-button',
                            text: 'Детали операции (старый интерфейс)',
                            disabled: false,
                            handler: function (widget, event) {
                                var selectedRecordData = grid.getSelectionModel().getSelection()[0] && grid.getSelectionModel().getSelection()[0].getData();
                                window.open(
                                    '/OFSAAI/formsFramework/menu/RF_index.jsp?Sol=AML&origin=MAIN&dsn=AMINFO&cssFileName=CSS_OFSAAI&AlertId=' + vm.get('selectedAlert.REVIEW_ID'),
                                    'AlertId',
                                    'width=' + window.screen.width + ',height=' + window.screen.height
                                );
                            }
                        });
                        if (grid.getReference() === 'alertsGrid') {
                            grid.contextMenu = Ext.create('Ext.menu.Menu', {
                                items: [
                                    grid.showOES321DetailsBtn,
                                    grid.showAlertDetailsBtn,
                                    grid.showAlertDetailsOldBtn
                                ]
                            });
                        }

                        // устанавливаем фокус на грид:
                        grid.getView().focus();
                    },
                    /**
                     * Отображаем кастомное контекстное меню
                     * @param view
                     * @param rec
                     * @param node
                     * @param index
                     * @param e
                     * @returns {boolean}
                     */
                    itemcontextmenu: function (view, rec, node, index, e) {
                        e.stopEvent();
                        if (view.panel && view.panel.contextMenu) {
                            view.panel.contextMenu.showAt(e.getXY());
                        }
                        return false;
                    },
                    /**
                     * Обрабатываем событие нажатия кнопок клавиатуры
                     * @param table
                     * @param record
                     * @param tr
                     * @param rowIndex
                     * @param e
                     * @param eOpts
                     */
                    rowkeydown: function (table, record, tr, rowIndex, e, eOpts) {
                        var self = this,
                            vm = this.getViewModel(),
                            toggleBBarBtn = self.lookupReference('toggleBBarBtn'),
                            monitoringAlertsTopbar = self.lookupReference('monitoringAlertsTopbar');

                        switch (e.getKey()) {
                            case e.SPACE:
                                if (this.getView().getReference() === "alertsList") {
                                    // выставляем select
                                    table.focusRow(record);
                                    table.setSelection(record);
                                    table.getSelectionModel().select(rowIndex, true);

                                    // ставим флаг открытия футера
                                    if (toggleBBarBtn.pressed) {
                                        monitoringAlertsTopbar.reestablishView = true;
                                    }
                                    // закрываем нижную панель детали операции:
                                    toggleBBarBtn.setPressed(false);
                                    // открываем правую панель ОЭС - Редактирование - Сведения об операции:
                                    if (!Ext.isEmpty(record.getData().OES321ID)) {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            self.getView().add({
                                                xtype: 'app-alerts-oes321detailspanel',
                                                viewModel: {
                                                    data: {
                                                        alertId: vm.get('selectedAlert.REVIEW_ID'),
                                                        oes321Id: vm.get('selectedAlert.OES321ID'),
                                                        selectedAlerts: self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [record]
                                                    },
                                                    parent: vm
                                                }
                                            });
                                        }
                                    } else {
                                        if (!self.getView().down('app-alerts-alertdetailspanel')) {
                                            // открываем правую панель детали операции:
                                            self.getView().add({
                                                xtype: 'app-alerts-alertdetailspanel',
                                                viewModel: {
                                                    data: {
                                                        alertId: vm.get('selectedAlert.REVIEW_ID')
                                                        // selectedAlerts: self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [record]
                                                    },
                                                    parent: vm
                                                }
                                            });
                                        }
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    },

    init: function () {
        var vm = this.getViewModel();

        vm.bind('{search}', function () {
            vm.set('filteredCount', '');
            vm.set('sumRUB', '');
        }, this, {deep: true});
    },

    editItem: function (grid, rowIndex, colIndex, btn, e, rec) {
        var store = Ext.data.StoreManager.lookup('OPOKEdit'),
            data = {
                'id': rec.get('REVIEW_ID'),
                OPOK_NB: rec.get('OPOK_NB'),
                ADD_OPOKS_TX: (rec.get('ADD_OPOKS_TX') || '').replace(/ /g, ''),
                // я отлично понимаю, что протаскивать значение фильтра в модель создаваемой формы
                // это "как-то не очень", но забрать его напрямую что-то слишком долго не получалось
                where: this.getView().down('combobox[name=scopeFilters]').getValue() || null,
                filters: this.getViewModel().getData().search || null
            };
        store.loadRawData([data]);
        Ext.widget({
            xtype: 'app-alerts-editopokform-form',
            viewModel: {
                type: 'opokeditformviewmodel',
                parent: this.getViewModel()
            }
        }).show();
    },

    editCancelAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.editAction(true);
    },

    editAction: function (isCancelAction) {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        var self = this,
            vm = self.getViewModel(),
            fields = {
                dtFrom: self.lookupReference('dtFrom'),
                dtTo: self.lookupReference('dtTo'),
                ownerIds: self.lookupReference('ownerIds'),
                tbIds: self.lookupReference('tbIds'),
                osbIds: self.lookupReference('osbIds'),
                opok: self.lookupReference('opok'),
                opokType: self.lookupReference('opokType'),
                operationNumber: self.lookupReference('operationNumber'),
                status: self.lookupReference('status'),
                amountFrom: self.lookupReference('amountFrom'),
                amountTo: self.lookupReference('amountTo'),
                scopeFilters: self.lookupReference('scopeFilters')
            },
            searchRaw = {
                dt: '',
                dtFrom: '',
                dtTo: '',
                ownerIds: '',
                tbIds: '',
                osbIds: '',
                opok: '',
                opokType: '',
                operationNumber: '',
                status: '',
                amount: '',
                amountFrom: '',
                amountTo: ''
            },
            applyFiltersBtn = self.lookupReference('applyFiltersBtn'),
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel || self.getView().down('app-alerts-oes321detailspanel'),
            search = {};


        if (applyFiltersBtn.pressed) {
            searchRaw = {
                dt: '',
                dtFrom: fields.dtFrom.getRawValue() ? ('c ' + fields.dtFrom.getRawValue() + ' ') : '',
                dtTo: fields.dtTo.getRawValue() ? ('по ' + fields.dtTo.getRawValue()) : '',
                ownerIds: fields.ownerIds.getRawValue() ? ('Ответственный: ' + fields.ownerIds.getRawValue() + '; ') : '',
                tbIds: fields.tbIds.getRawValue() ? ('Филиал: ' + fields.tbIds.getRawValue() + '; ') : '',
                osbIds: fields.osbIds.getRawValue() ? ('Отделение: ' + fields.osbIds.getRawValue() + '; ') : '',
                opok: fields.opok.getRawValue() ? ('Код: ' + fields.opok.getRawValue() + '; ') : '',
                opokType: fields.opokType.getRawValue() ? ('Искать в: ' + fields.opokType.getRawValue() + '; ') : '',
                operationNumber: fields.operationNumber.getRawValue() ? ('Номер операции: ' + fields.operationNumber.getRawValue() + '; ') : '',
                status: fields.status.getRawValue() ? ('Статус операции: ' + fields.status.getRawValue() + '; ') : '',
                amount: '',
                amountFrom: fields.amountFrom.getRawValue() ? ('от: ' + fields.amountFrom.getRawValue() + ' ') : '',
                amountTo: fields.amountTo.getRawValue() ? ('до ' + fields.amountTo.getRawValue()) : ''
            };

            if (searchRaw.dtFrom || searchRaw.dtTo) {
                searchRaw.dt = 'Дата операции ' + searchRaw.dtFrom + searchRaw.dtTo + '; ';
            }
            if (searchRaw.amountFrom || searchRaw.amountTo) {
                searchRaw.amount = 'Сумма операции ' + searchRaw.amountFrom + searchRaw.amountTo + '; ';
            }
            if (parseFloat(fields.amountFrom.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) > parseFloat(fields.amountTo.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))) {
                //console.log('check summ');
            }
            search = {
                dtFrom: fields.dtFrom.getValue(),
                dtTo: fields.dtTo.getValue(),
                ownerIds: fields.ownerIds.getValue(),
                tbIds: fields.tbIds.getValue(),
                osbIds: fields.osbIds.getValue(),
                opok: fields.opok.getValue(),
                opokType: fields.opokType.getValue(),
                operationNumber: fields.operationNumber.getValue(),
                status: fields.status.getValue(),
                amountFrom: parseFloat(fields.amountFrom.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountTo.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                oldAlert: vm.get('search.oldAlert') ? 'Y' : ''
            };

            for (var key in search) {
                if (Ext.isEmpty(search[key])) {
                    delete search[key];
                }
            }
        }

        var alMostVar, alertDP, oesDP;
        searchRaw.scopeFilters = fields.scopeFilters.getRawValue() ? ('"' + fields.scopeFilters.getRawValue() + '"; ') : '';
        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            if (alertDetailsPanel.getViewModel()) {
                alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
                selectedAlertsCnt = 1;
                alertDP = alertDetailsPanel.getViewModel().get('alertId');
            }
        }
        if (oes321DetailsPanel) {
            if (oes321DetailsPanel.getViewModel()) {
                alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
                selectedAlertsCnt = 1;
                oesDP = oes321DetailsPanel.getViewModel().get('alertId');
            }
        }
        if (!alertDetailsModel.get('REVIEW_ID')) {
            selectedAlerts = self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [alertDetailsModel];
        }
        alMostVar = ((alertDetailsPanel && alertDP !== null) || (oes321DetailsPanel && oesDP !== null));
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-settings-inspectionresults-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistorylog-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
            Ext.widget({
                xtype: 'app-alerts-alertactionform-form',
                id: 'app-alerts-alertactionform-form',
                viewModel: {
                    type: 'alertactionformviewmodel',
                    parent: vm,
                    data: {
                        allAlertsCnt: self.getStore('alertsStore').getTotalCount(),
                        selectedAlertsCnt: self.lookupReference('alertsGrid').getSelectionModel().getSelected().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelected().length : +(alertDetailsModel.getData().REVIEW_ID !== null),
                        selectedAlerts: self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [alertDetailsModel],
                        isCancel: (typeof isCancelAction === 'boolean' && isCancelAction),
                        search: search,
                        scopeFilters: fields.scopeFilters.getValue(),
                        searchRaw: searchRaw,
                        isAlertDetailsPanel: alMostVar // если открыта детальная панель операции
                    }
                }
            }).show();
        }
    },

    attachAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        var self = this,
            selectedAlertsCnt = self.lookupReference('alertsGrid').getSelectionModel().getSelected().length,
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel || self.getView().down('app-alerts-oes321detailspanel');

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            if (alertDetailsPanel.getViewModel()) {
                alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
                selectedAlertsCnt = 1;
            }
        }
        if (oes321DetailsPanel) {
            if (oes321DetailsPanel.getViewModel()) {
                alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
                selectedAlertsCnt = 1;
            }
        }
        if (!alertDetailsModel.get('REVIEW_ID')) {
            selectedAlerts = self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [alertDetailsModel];
        }
        switch (true) {
            case selectedAlertsCnt === 0:
                Ext.Msg.alert('', 'Нет выделенной операции');
                break;
            case selectedAlertsCnt > 1:
                Ext.Msg.alert('', 'Выделено более одной операции');
                break;
            case selectedAlertsCnt === 1:
                var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-narrative-form')
                    || Ext.getCmp('app-settings-inspectionresults-form')
                    || Ext.getCmp('app-monitoring-alerts-documenthistory-form')
                    || Ext.getCmp('app-monitoring-alerts-documenthistorylog-form')
                    || Ext.getCmp('app-alerts-alertaddattachment-form')
                    || Ext.getCmp('app-alerts-alertactionform-form');

                if (!isOpenedWindow) {
                    Ext.widget({
                        xtype: 'app-alerts-alertaddattachment-form',
                        id: 'app-alerts-alertaddattachment-form',
                        viewModel: {
                            type: 'alertaddattachmentformviewmodel',
                            parent: this.getViewModel(),
                            //parent: vm,
                            data: {
                                selectedAlerts: self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [alertDetailsModel]
                            }
                        }
                    }).show();
                }
        }
    },

    checkIfGridSelected: function () {
        var self = this,
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel') || self.getView(),
            oes321DetailsPanel = self.oes321DetailsPanel || self.getView().down('app-alerts-oes321detailspanel'),
            selectedAlertsCnt = self.lookupReference('alertsGrid') ? self.lookupReference('alertsGrid').getSelectionModel().getSelection().length : self.getView().up().lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length;

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel && oes321DetailsPanel.getViewModel()) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        if (alertDetailsModel.getData().REVIEW_ID === null && !selectedAlertsCnt) {
            Ext.Msg.alert('', 'Пожалуйста, выберите операцию из списка.');
            return false;
        }
        return true;
    },

    // выбор и передача фокуса записи в гриде
    selectRowGrid: function (alertId) {
        var self = this,
            dataArr = [],
            grid = self.lookupReference('alertsGrid') || self.lookupReference('alertsOperSearchGrid'),
            gridStore = grid.getStore(),
            vm = self.getViewModel(),
            selectedAlertIndex = 0,
            alertId = alertId || vm.get('selectedAlert.REVIEW_ID') || vm.get('selectedOSAlert.REVIEW_ID'),
            view = grid.getView();

        if (alertId) {
            Ext.Array.forEach(gridStore.getRange(), function (item, index) {
                dataArr.push(item.get('REVIEW_ID'));
            });
            selectedAlertIndex = Ext.Array.indexOf(dataArr, parseInt(alertId, 10)) != -1 ? Ext.Array.indexOf(dataArr, parseInt(alertId, 10)) : 0;
        }
        // выставляем select
        if (gridStore.data.length > 0) {
            view.focusRow(gridStore.getAt(selectedAlertIndex));
            view.setSelection(gridStore.getAt(selectedAlertIndex));
        }
    },

    subjectToControlAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.execByIdAction(['RF_OES']);
    },

    cancelInvalidTokenAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.execByIdAction(['RF_CANCEL']);
    },

    exportAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        var type = AML.app.msg._getType('question'), self = this;
        if (!this.checkIfGridSelected()) {
            return;
        }
        Ext.Msg.show({
            title: 'Внимание!',
            msg: 'Настоящим действием Вы подтверждаете принятие решения о направлении данных в Уполномоченный орган.',
            buttonText: {
                yes: 'OK', no: 'Отмена'
            },
            icon: type.icon,
            buttons: Ext.Msg.YESNO,
            fn: function (answer) {
                if (answer === 'yes') {
                    self.execByIdAction(['RF_EXPORT']);
                }
            }
        });
    },

    toSendAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.execByIdAction(['RF_READY']);
    },

    execByIdAction: function (actvyTypeCds) {
        var vm = this.getViewModel(),
            self = this,
            store = this.getStore('alertsStore'),
            fields = {
                dtFrom: this.lookupReference('dtFrom'),
                dtTo: this.lookupReference('dtTo'),
                ownerIds: this.lookupReference('ownerIds'),
                tbIds: this.lookupReference('tbIds'),
                osbIds: this.lookupReference('osbIds'),
                opok: this.lookupReference('opok'),
                opokType: this.lookupReference('opokType'),
                operationNumber: this.lookupReference('operationNumber'),
                status: this.lookupReference('status'),
                amountFrom: this.lookupReference('amountFrom'),
                amountTo: this.lookupReference('amountTo'),
                scopeFilters: this.lookupReference('scopeFilters'),
                payerINNAddedFilter: this.lookupReference('payerINNAddedFilter'),
                payerAccountNumberAddedFilter: this.lookupReference('payerAccountNumberAddedFilter'),
                payerRegionSearchddedFilter: this.lookupReference('payerRegionSearchddedFilter'),
                recieverINNAddedFilter: this.lookupReference('recieverINNAddedFilter'),
                recieverAccountNumberAddedFilter: this.lookupReference('recieverAccountNumberAddedFilter'),
                recieverRegionSearchddedFilter: this.lookupReference('recieverRegionSearchddedFilter'),
                paydocAccountNumberAddedFilter: this.lookupReference('paydocAccountNumberAddedFilter'),
                paydocDateAddedFilter: this.lookupReference('paydocDateAddedFilter'),
                paydocSystemAddedFilter: this.lookupReference('paydocSystemAddedFilter'),
                paydocPayRuleAddedFilter: this.lookupReference('paydocPayRuleAddedFilter')
            },
            search = {},
            applyFiltersBtn = self.lookupReference('applyFiltersBtn'),
            // alertDetailsPanel =  self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel || self.getView().down('app-alerts-oes321detailspanel'),
            alertIds = [],
            alertId,
            nextAlertId = 0,
            selectedAlertIndex = 0,
            nextSelectedAlertIndex = 0,
            refleshFooter = false,
            params,
            selectedAlerts;

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel && oes321DetailsPanel.getViewModel()) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        if (self.getView().down('app-alerts-oes321detailspanel') || self.getView().down('app-alerts-alertdetailspanel')) {
            selectedAlerts = [alertDetailsModel];
        }
        if (!alertDetailsModel.get('REVIEW_ID')) {
            selectedAlerts = self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [alertDetailsModel];
        }

        if (selectedAlerts) {
            for (var i = 0; i < selectedAlerts.length; i++) {
                alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
                if (actvyTypeCds[0] === 'RF_CANCEL' || actvyTypeCds[0] === 'RF_READY' || actvyTypeCds[0] === 'RF_EXPORT') {
                    refleshFooter = true;
                    alertId = selectedAlerts[i].get('REVIEW_ID');
                    if (alertId !== null && alertId > 0) {
                        if (store.data.items.length > 0) {
                            Ext.Array.forEach(store.data.items, function (item, index) {
                                if (selectedAlertIndex === 1) {
                                    nextAlertId = parseInt(item.id, 10);
                                    // выставляем индекс следущей записи с учетом сдвижки
                                    nextSelectedAlertIndex = index - 1;
                                    selectedAlertIndex = 2;
                                }
                                if (parseInt(item.id, 10) === alertId) {
                                    selectedAlertIndex = 1;
                                }
                            })
                        }
                    }
                }
            }
        }

        params = {
            form: 'AlertAction',
            action: 'execById',
            actvyTypeCds: (actvyTypeCds && actvyTypeCds.shift) ? actvyTypeCds : '',
            alertIds: alertIds,
            cmmntId: (actvyTypeCds && actvyTypeCds.shift && Ext.Array.indexOf(actvyTypeCds, 'RF_CANCEL') !== -1) ? 101 : ''
        };

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }

        if (applyFiltersBtn.pressed) {
            search = {
                dtFrom: fields.dtFrom.getValue(),
                dtTo: fields.dtTo.getValue(),
                ownerIds: fields.ownerIds.getValue(),
                tbIds: fields.tbIds.getValue(),
                osbIds: fields.osbIds.getValue(),
                opok: fields.opok.getValue(),
                opokType: fields.opokType.getValue(),
                operationNumber: fields.operationNumber.getValue(),
                status: fields.status.getValue(),
                amountFrom: parseFloat(fields.amountFrom.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountTo.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                origInnNumber: fields.payerINNAddedFilter.getValue(), //"7716515821",
                origAcctNumber: fields.payerAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                origAcctArea: fields.payerRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                benefInnNumber: fields.recieverINNAddedFilter.getValue(), //"7716515821",
                benefAcctNumber: fields.recieverAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                benefAcctArea: fields.recieverRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                trxnDocNumber: fields.paydocAccountNumberAddedFilter.getValue(), //"158-78",
                trxnDocDate: fields.paydocDateAddedFilter.getValue(), //"2010-06-01T00:00:00",
                trxnDesc: fields.paydocPayRuleAddedFilter.getValue(), //"Покупка Луны для строительства казино с блекджеком и куртизанками.",
                srcSysCd: fields.paydocSystemAddedFilter.getValue(), //["EKS", "BCK"]
                oldAlert: vm.get('search.oldAlert') ? 'Y' : ''
            };

            for (var tkey in search) {
                if (Ext.isEmpty(search[tkey])) {
                    delete search[tkey];
                }
            }
            Ext.apply(params, {
                filters: search,
                where: fields.scopeFilters.getValue()
            });
        }

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                // переподкачка списка операций:
                var options = {
                    alertIds: responseObj.alertIds,
                    filters: search,
                    where: fields.scopeFilters.getValue(),
                    addRecords: true,
                    refleshFooter: refleshFooter,
                    nextSelectedAlertIndex: nextSelectedAlertIndex
                };
                // if ((actvyTypeCds.indexOf('RF_CANCEL') !== -1) || (actvyTypeCds.indexOf('RF_READY') !== -1) || (actvyTypeCds.indexOf('RF_EXPORT') !== -1)) {
                //     if(vm.getParent().getView().down('app-alerts-oes321detailspanel')) {
                //         vm.getParent().getView().down('app-alerts-oes321detailspanel').getController().closePanel();
                //     }
                // }
                if (!Ext.isEmpty(responseObj.alertIds)) {
                    self.refreshAlertsList(options);
                }
            }
        });
    },

    refreshAlertsList: function (options) {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertsStore'),
            // alertDetailsPanelCtrl = self.lookupReference('alertDetailsPanel') && self.lookupReference('alertDetailsPanel').getController(),
            alertDetailsPanelCtrl = self.getView().down('app-alerts-alertdetailspanel') && self.getView().down('app-alerts-alertdetailspanel').getController(),
            oes321DetailsPanelCtrl = self.oes321DetailsPanel && self.oes321DetailsPanel.getController(),
            toggleBBarBtn = self.lookupReference('toggleBBarBtn'),
            params = {
                form: 'AlertList',
                action: 'read',
                alertIds: options.alertIds
            },
            alertId,
            fields = {
                dtFrom: self.lookupReference('dtFrom'),
                dtTo: self.lookupReference('dtTo'),
                ownerIds: self.lookupReference('ownerIds'),
                tbIds: self.lookupReference('tbIds'),
                osbIds: self.lookupReference('osbIds'),
                opok: self.lookupReference('opok'),
                opokType: self.lookupReference('opokType'),
                operationNumber: self.lookupReference('operationNumber'),
                status: self.lookupReference('status'),
                amountFrom: self.lookupReference('amountFrom'),
                amountTo: self.lookupReference('amountTo'),

                payerINNAddedFilter: self.lookupReference('payerINNAddedFilter'),
                payerAccountNumberAddedFilter: self.lookupReference('payerAccountNumberAddedFilter'),
                payerRegionSearchddedFilter: self.lookupReference('payerRegionSearchddedFilter'),
                recieverINNAddedFilter: self.lookupReference('recieverINNAddedFilter'),
                recieverAccountNumberAddedFilter: self.lookupReference('recieverAccountNumberAddedFilter'),
                recieverRegionSearchddedFilter: self.lookupReference('recieverRegionSearchddedFilter'),
                paydocAccountNumberAddedFilter: self.lookupReference('paydocAccountNumberAddedFilter'),
                paydocDateAddedFilter: self.lookupReference('paydocDateAddedFilter'),
                paydocSystemAddedFilter: self.lookupReference('paydocSystemAddedFilter'),
                paydocPayRuleAddedFilter: self.lookupReference('paydocPayRuleAddedFilter'),

                scopeFilters: self.lookupReference('scopeFilters')
            },
            search = {},
            applyFiltersBtn = self.lookupReference('applyFiltersBtn');

        if (applyFiltersBtn.pressed) {
            search = {
                dtFrom: fields.dtFrom.getValue(),
                dtTo: fields.dtTo.getValue(),
                ownerIds: fields.ownerIds.getValue(),
                tbIds: fields.tbIds.getValue(),
                osbIds: fields.osbIds.getValue(),
                opok: fields.opok.getValue(),
                opokType: fields.opokType.getValue(),
                operationNumber: fields.operationNumber.getValue(),
                status: fields.status.getValue(),
                amountFrom: parseFloat(fields.amountFrom.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountTo.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),

                origInnNumber: fields.payerINNAddedFilter.getValue(), //"7716515821",
                origAcctNumber: fields.payerAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                origAcctArea: fields.payerRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                benefInnNumber: fields.recieverINNAddedFilter.getValue(), //"7716515821",
                benefAcctNumber: fields.recieverAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                benefAcctArea: fields.recieverRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                trxnDocNumber: fields.paydocAccountNumberAddedFilter.getValue(), //"158-78",
                trxnDocDate: fields.paydocDateAddedFilter.getValue(), //"2010-06-01T00:00:00",
                trxnDesc: fields.paydocPayRuleAddedFilter.getValue(), //"Покупка Луны для строительства казино с блекджеком и куртизанками.",
                srcSysCd: fields.paydocSystemAddedFilter.getValue(), //["EKS", "BCK"]

                oldAlert: vm.get('search.oldAlert') ? 'Y' : ''
            };

            for (var tkey in search) {
                if (Ext.isEmpty(search[tkey])) {
                    delete search[tkey];
                }
            }
            Ext.apply(params, {
                filters: search//,
                // where: fields.scopeFilters.getValue()
            });
        }

        Ext.apply(params, {
            cancelFetchId: options.cancelFetchId,
            where: fields.scopeFilters.getValue()
        });

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }
        if (options.alertIds) {
            // if (options.alertIds && statusOptions === 'NW') {
            var prevNextRecs = this.getNextPreviousAlertIDs(options.alertIds[0], store);
            if (prevNextRecs.nextSelectedAlertIndex && options.nextSelectedAlertIndex !== (prevNextRecs.nextSelectedAlertIndex - 1)) {
                options.nextSelectedAlertIndex = (prevNextRecs.nextSelectedAlertIndex - 1);
            }
        }
        store.load({
            params: params,
            addRecords: options.addRecords ? true : false,
            scope: self,
            callback: function (records, operation, success) {
                var responseText = Ext.decode(operation.getResponse().responseText),
                    store = this.getStore('alertsStore');

                // переподкачка статистики:
                //self.getStatistics();

                // если данные по операции не пришли после обновления списка, то закрываем панель:
                if (alertDetailsPanelCtrl) {
                    // обновляем правую панель детали операции, если есть данные для обновления и она открыта:
                    alertDetailsPanelCtrl && !Ext.isEmpty(records) && alertDetailsPanelCtrl.refreshDetailsTable(records[0].data);
                    if (alertDetailsPanelCtrl.getView()) {
                        alertDetailsPanelCtrl && Ext.isEmpty(records) && alertDetailsPanelCtrl.getView().close();
                    }
                }
                if (oes321DetailsPanelCtrl) {
                    // обновляем правую панель детали операции, если есть данные для обновления и она открыта:
                    oes321DetailsPanelCtrl && !Ext.isEmpty(records) && oes321DetailsPanelCtrl.refreshDetailsTable(records[0].data);
                    if (oes321DetailsPanelCtrl.getView()) {
                        oes321DetailsPanelCtrl && Ext.isEmpty(records) && oes321DetailsPanelCtrl.getView().close();
                    }
                }

                // если данные по операции не пришли после обновления списка, то закрываем нижную панель детали операции:
                vm.get('totalCount') <= 0 && toggleBBarBtn.setPressed(false);

                if (options.refleshFooterAct) {
                    for (var i = 0; i < store.length; i++) {
                        alertId = store[i].get('REVIEW_ID');
                        if (alertId !== null && alertId > 0) {
                            if (store.data.items.length > 0) {
                                Ext.Array.forEach(store.data.items, function (item, index) {
                                    if (parseInt(item.id, 10) === parseInt(options.alertId, 10)) {
                                        options.refleshFooterAct = false;
                                    }
                                })
                            }
                        }
                    }
                }
                // перегружаем подвал при условии что была операция (отменить(неверная лексема))
                if (!alertDetailsPanelCtrl && !oes321DetailsPanelCtrl && (options.refleshFooter || options.refleshFooterAct) && vm.getView().down('grid').store.totalCount > 0) {
                    if (options.nextSelectedAlertIndex >= 0) {
                        // перепроверка открытия подвала для деталей операции
                        var monitoringAlertsTopbar = self.lookupReference('monitoringAlertsTopbar');
                        // проверяем был ли открыт подвал
                        if (monitoringAlertsTopbar.reestablishView && !self.getView().down('app-alerts-oes321detailspanel')) {
                            toggleBBarBtn.setPressed(true);
                            monitoringAlertsTopbar.reestablishView = false;
                        }
                        self.lookupReference('alertsGrid').getView().focusRow(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                        self.lookupReference('alertsGrid').getView().setSelection(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                    }
                }
                // set cancelFetchId
                if (responseText && responseText.fetchId) {
                    vm.set('fetchId', responseText.fetchId);
                }
                if (!responseText.data.length && (options.alertId || options.alertIds)) {
                    var recId = options.alertId ? options.alertId : options.alertIds[0],
                        deletedRecord = store.findRecord('_id_', recId);

                    store.remove(deletedRecord);
                    if (store.getCount()) {
                        if (!options.nextSelectedAlertIndex) {
                            options.nextSelectedAlertIndex = 0;
                        }
                        if (options.nextSelectedAlertIndex && !this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex)) {
                            options.nextSelectedAlertIndex = 0;
                        }

                        self.lookupReference('alertsGrid').getView().focusRow(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                        self.lookupReference('alertsGrid').getView().setSelection(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                        if (this.getView().down('app-alerts-oes321detailspanel')) {
                            this.getView().down('app-alerts-oes321detailspanel').close();
                        }
                        if (this.getView().down('app-alerts-alertdetailspanel')) {
                            this.getView().down('app-alerts-alertdetailspanel').close();
                        }
                    }
                }
            }
        });
    },

    readNextListPage: function (btn) {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertsStore'),
            readLastListPageBtn = self.lookupReference('readLastListPageBtn'),
            params = {
                form: 'AlertList',
                action: 'readNext',
                fetchId: vm.get('fetchId')
            };

        // если fetchId пустой (были загружены все операции с сервера):
        if (Ext.isEmpty(params.fetchId)) {
            Ext.Msg.alert('', 'Все операции уже загружены!');
            return;
        }

        if (btn.disabled !== true) {
            btn.disable(true);
            readLastListPageBtn.disable(true);
            store.load({
                params: params,
                addRecords: true,
                scope: self,
                callback: function (records, operation, success) {
                    // переподкачка статистики:
                    // self.getStatistics();
                    btn.enable(true);
                    readLastListPageBtn.enable(true);
                }
            });
        }
    },

    readLastListPage: function (btn) {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertsStore'),
            readNextListPageBtn = self.lookupReference('readNextListPageBtn'),
            showListMainBtn = self.lookupReference('showListMainBtn'),
            params = {
                form: 'AlertList',
                action: 'readLast',
                fetchId: vm.get('fetchId')
            };

        // если fetchId пустой (были загружены все операции с сервера):
        if (Ext.isEmpty(params.fetchId)) {
            Ext.Msg.alert('', 'Все операции уже загружены!');
            return;
        }

        if (vm.get('grandTotal') === '...' || vm.get('grandTotal') - vm.get('totalCount') > 2000) {
            showListMainBtn.disable(true);
            Ext.Msg.confirm('', 'Будет выполнена загрузка большого количества операций. Это может занять много времени и потребовать значительных ресурсов Вашего компьютера. Вы уверены?', function (answer) {
                if (answer === 'yes') {
                    if (btn.disabled !== true) {
                        btn.disable(true);
                        readNextListPageBtn.disable(true);
                        store.load({
                            params: params,
                            addRecords: true,
                            scope: self,
                            callback: function (records, operation, success) {
                                // переподкачка статистики:
                                // self.getStatistics();
                                btn.enable(true);
                                readNextListPageBtn.enable(true);
                            }
                        });
                    }
                }
                showListMainBtn.enable(true);
            });
        } else if (vm.get('grandTotal') - vm.get('totalCount') <= 2000) {
            if (btn.disabled !== true) {
                btn.disable(true);
                readNextListPageBtn.disable(true);
                showListMainBtn.disable(true);
                store.load({
                    params: params,
                    addRecords: true,
                    scope: self,
                    callback: function (records, operation, success) {
                        // переподкачка статистики:
                        // self.getStatistics();
                        btn.enable(true);
                        readNextListPageBtn.enable(true);
                    }
                });
                showListMainBtn.enable(true);
            }
        }
    },

    /**
     * Получаем статистику
     */
    getStatistics: function () {
        var ajax = Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            async: false,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.JSON.encode({
                form: 'AlertsStatistics',
                action: 'read'
            })
        });
        this.getViewModel().set('statistics', Ext.JSON.decode(ajax.responseText).data[0]);
    },
    /**
     * Раскрываем подвал
     */
    footerPanelExpand: function () {
        /*var panel = this.getView().down('panel[isFooter=true]');
         if (panel.collapsed
         ) {
         panel.expand();
         }*/
    },

    /**
     * Получаем хранилище
     */
    getListStore: function () {
        return this.getStore('alertsStore');
    },
    /**
     * Загружаем хранилище
     */
    loadListStore: function () {
        this.getListStore().load();
    },
    /**
     * Обновляем хранилище
     */
    reloadListStore: function () {
        this.getListStore().reload();
    },

    toggleClearFilters: function (btn) {
        var self = this,
            vm = this.getViewModel(),
            applyFiltersBtn = this.lookupReference('applyFiltersBtn'),
            toggleClearFiltersBtn = this.lookupReference('toggleClearFiltersBtn'),
            buttomFilterExpand1 = this.lookupReference('buttomFilterExpand1'),

        fields = {
            dtFrom: self.lookupReference('dtFrom'),
            dtTo: self.lookupReference('dtTo'),
            ownerIds: self.lookupReference('ownerIds'),
            tbIds: self.lookupReference('tbIds'),
            osbIds: self.lookupReference('osbIds'),
            opok: self.lookupReference('opok'),
            opokType: self.lookupReference('opokType'),
            operationNumber: self.lookupReference('operationNumber'),
            status: self.lookupReference('status'),
            amountFrom: self.lookupReference('amountFrom'),
            amountTo: self.lookupReference('amountTo'),

            payerINNAddedFilter: self.lookupReference('payerINNAddedFilter'),
            payerAccountNumberAddedFilter: self.lookupReference('payerAccountNumberAddedFilter'),
            payerRegionSearchddedFilter: self.lookupReference('payerRegionSearchddedFilter'),
            recieverINNAddedFilter: self.lookupReference('recieverINNAddedFilter'),
            recieverAccountNumberAddedFilter: self.lookupReference('recieverAccountNumberAddedFilter'),
            recieverRegionSearchddedFilter: self.lookupReference('recieverRegionSearchddedFilter'),
            paydocAccountNumberAddedFilter: self.lookupReference('paydocAccountNumberAddedFilter'),
            paydocDateAddedFilter: self.lookupReference('paydocDateAddedFilter'),
            paydocSystemAddedFilter: self.lookupReference('paydocSystemAddedFilter'),
            paydocPayRuleAddedFilter: self.lookupReference('paydocPayRuleAddedFilter'),

            oldAlert: self.lookupReference('oldAlert')
        };
        for (var key in fields) {
            // skip loop if the property is from prototype
            if (!fields.hasOwnProperty(key)) continue;

            var obj = fields[key];
            if(obj){
                if(key === 'opokType'){
                    obj.setValue('ALL');
                } else {
                    obj.setValue('');
                }
            }
        }

        vm.set('filteredCount', '');
        vm.set('search.oldAlert', '');
        vm.set('sumRUB', '');
    },

    toggleFilters: function (btn) {
        var vm = this.getViewModel(),
            applyFiltersBtn = this.lookupReference('applyFiltersBtn'),
            buttomFilterExpand1 = this.lookupReference('buttomFilterExpand1');
        vm.set('showFilter', btn.pressed);
        if(!btn.pressed) {
            vm.set('showAddedFilter', btn.pressed);
            buttomFilterExpand1.setPressed(btn.pressed);
        }
        applyFiltersBtn.setPressed(btn.pressed);
    },

    toggleAddedFilters: function (btn) {
        var vm = this.getViewModel();

        btn.pressed ? btn.removeCls('closed') && btn.addCls('opened') : btn.removeCls('opened') && btn.addCls('closed');
        vm.set('showAddedFilter', btn.pressed);
    },

    applyFilters: function (btn) {
        var self = this,
            toggleFiltersBtn = self.lookupReference('toggleFiltersBtn');
        // скрыть панель фильтра, если фильтр не применяется:
        if (!btn.pressed) {
            toggleFiltersBtn.setPressed(false);
        }
    },

    toggleBBar: function (btn) {
        var self = this,
            alertId = (self.getViewModel().get('selectedAlert')) ? self.getViewModel().get('selectedAlert').get('REVIEW_ID') : false;

        this.getViewModel().set('showBBar', btn.pressed);
        if (alertId) {
            self.selectRowGrid(alertId);
        }
    },

    countFilterToggle: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            fields = {
                dtFrom: self.lookupReference('dtFrom'),
                dtTo: self.lookupReference('dtTo'),
                ownerIds: self.lookupReference('ownerIds'),
                tbIds: self.lookupReference('tbIds'),
                osbIds: self.lookupReference('osbIds'),
                opok: self.lookupReference('opok'),
                opokType: self.lookupReference('opokType'),
                operationNumber: self.lookupReference('operationNumber'),
                status: self.lookupReference('status'),
                amountFrom: self.lookupReference('amountFrom'),
                amountTo: self.lookupReference('amountTo'),

                payerINNAddedFilter: self.lookupReference('payerINNAddedFilter'),
                payerAccountNumberAddedFilter: self.lookupReference('payerAccountNumberAddedFilter'),
                payerRegionSearchddedFilter: self.lookupReference('payerRegionSearchddedFilter'),
                recieverINNAddedFilter: self.lookupReference('recieverINNAddedFilter'),
                recieverAccountNumberAddedFilter: self.lookupReference('recieverAccountNumberAddedFilter'),
                recieverRegionSearchddedFilter: self.lookupReference('recieverRegionSearchddedFilter'),
                paydocAccountNumberAddedFilter: self.lookupReference('paydocAccountNumberAddedFilter'),
                paydocDateAddedFilter: self.lookupReference('paydocDateAddedFilter'),
                paydocSystemAddedFilter: self.lookupReference('paydocSystemAddedFilter'),
                paydocPayRuleAddedFilter: self.lookupReference('paydocPayRuleAddedFilter'),

                scopeFilters: self.lookupReference('scopeFilters')
            },
            search = {
                dtFrom: fields.dtFrom.getValue(),
                dtTo: fields.dtTo.getValue(),
                ownerIds: fields.ownerIds.getValue(),
                tbIds: fields.tbIds.getValue(),
                osbIds: fields.osbIds.getValue(),
                opok: fields.opok.getValue(),
                opokType: fields.opokType.getValue(),
                operationNumber: fields.operationNumber.getValue(),
                status: fields.status.getValue(),
                amountFrom: parseFloat(fields.amountFrom.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountTo.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),

                origInnNumber: fields.payerINNAddedFilter.getValue(), //"7716515821",
                origAcctNumber: fields.payerAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                origAcctArea: fields.payerRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                benefInnNumber: fields.recieverINNAddedFilter.getValue(), //"7716515821",
                benefAcctNumber: fields.recieverAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                benefAcctArea: fields.recieverRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                trxnDocNumber: fields.paydocAccountNumberAddedFilter.getValue(), //"158-78",
                trxnDocDate: fields.paydocDateAddedFilter.getValue(), //"2010-06-01T00:00:00",
                trxnDesc: fields.paydocPayRuleAddedFilter.getValue(), //"Покупка Луны для строительства казино с блекджеком и куртизанками.",
                srcSysCd: fields.paydocSystemAddedFilter.getValue(), //["EKS", "BCK"]

                oldAlert: vm.get('search.oldAlert') ? 'Y' : ''
            };

        Ext.Object.each(search, function (key, value) {
            if (Ext.isEmpty(value)) {
                delete search[key];
            }
        });

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode({
                form: 'AlertList',
                action: 'countAlert',
                where: fields.scopeFilters.getValue(),
                filters: search
            }),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                vm.set('filteredCount', responseObj.count || 0);
                vm.set('sumRUB', responseObj.sume || 0);
            }
        });

    },

    showList: function () {
        var self = this,
            vm = self.getViewModel(),
            store = self.getStore('alertsStore'),
            applyFiltersBtn = self.lookupReference('applyFiltersBtn'),
            options = {alertIds: null},
            fields = {
                dtFrom: self.lookupReference('dtFrom'),
                dtTo: self.lookupReference('dtTo'),
                ownerIds: self.lookupReference('ownerIds'),
                tbIds: self.lookupReference('tbIds'),
                osbIds: self.lookupReference('osbIds'),
                opok: self.lookupReference('opok'),
                opokType: self.lookupReference('opokType'),
                operationNumber: self.lookupReference('operationNumber'),
                status: self.lookupReference('status'),
                amountFrom: self.lookupReference('amountFrom'),
                amountTo: self.lookupReference('amountTo'),

                payerINNAddedFilter: self.lookupReference('payerINNAddedFilter'),
                payerAccountNumberAddedFilter: self.lookupReference('payerAccountNumberAddedFilter'),
                payerRegionSearchddedFilter: self.lookupReference('payerRegionSearchddedFilter'),
                recieverINNAddedFilter: self.lookupReference('recieverINNAddedFilter'),
                recieverAccountNumberAddedFilter: self.lookupReference('recieverAccountNumberAddedFilter'),
                recieverRegionSearchddedFilter: self.lookupReference('recieverRegionSearchddedFilter'),
                paydocAccountNumberAddedFilter: self.lookupReference('paydocAccountNumberAddedFilter'),
                paydocDateAddedFilter: self.lookupReference('paydocDateAddedFilter'),
                paydocSystemAddedFilter: self.lookupReference('paydocSystemAddedFilter'),
                paydocPayRuleAddedFilter: self.lookupReference('paydocPayRuleAddedFilter'),

                scopeFilters: self.lookupReference('scopeFilters')
            },
            search = {
                dtFrom: fields.dtFrom.getValue(),
                dtTo: fields.dtTo.getValue(),
                ownerIds: fields.ownerIds.getValue(),
                tbIds: fields.tbIds.getValue(),
                osbIds: fields.osbIds.getValue(),
                opok: fields.opok.getValue(),
                opokType: fields.opokType.getValue(),
                operationNumber: fields.operationNumber.getValue(),
                status: fields.status.getValue(),
                amountFrom: parseFloat(fields.amountFrom.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountTo.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),

                origInnNumber: fields.payerINNAddedFilter.getValue(), //"7716515821",
                origAcctNumber: fields.payerAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                origAcctArea: fields.payerRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                benefInnNumber: fields.recieverINNAddedFilter.getValue(), //"7716515821",
                benefAcctNumber: fields.recieverAccountNumberAddedFilter.getValue(), //"40703810138090113396",
                benefAcctArea: fields.recieverRegionSearchddedFilter.getValue(), //["СП", "СУ"],
                trxnDocNumber: fields.paydocAccountNumberAddedFilter.getValue(), //"158-78",
                trxnDocDate: fields.paydocDateAddedFilter.getValue(), //"2010-06-01T00:00:00",
                trxnDesc: fields.paydocPayRuleAddedFilter.getValue(), //"Покупка Луны для строительства казино с блекджеком и куртизанками.",
                srcSysCd: fields.paydocSystemAddedFilter.getValue(), //["EKS", "BCK"]

                oldAlert: vm.get('search.oldAlert') ? 'Y' : ''
            };

        for (var key in search) {
            if (Ext.isEmpty(search[key])) {
                delete search[key];
            }
        }

        Ext.apply(options, {
            filters: applyFiltersBtn.pressed ? search : '',
            where: fields.scopeFilters.getValue(),
            cancelFetchId: vm.get('fetchId')
        });
        // обнуляем инфу по вновь созданным операциям
        vm.set('newCountAlert', 0);
        // переподкачка списка операций:
        self.refreshAlertsList(options);
        // переподкачка статистики:
        self.getStatistics();
        //self.countFilterToggle();
    },

    showAlertReasons: function () {
        var self = this,
            vm = self.getViewModel(),
            grid = this.lookupReference('alertsGrid'),
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel;

        grid.getView().scrollRowIntoView(grid.getStore().indexOf(vm.get('selectedAlert')));

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        Ext.widget({
            xtype: 'app-alerts-alertssnapshotform-form',
            viewModel: {
                type: 'alertssnapshotformviewmodel',
                parent: vm,
                data: {
                    selectedAlerts: self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [alertDetailsModel]
                }
            }
        }).show();
    },

    beforeEditRow: function (editor, context) {
        var self = this,
            toggleBBarBtn = self.lookupReference('toggleBBarBtn'),
            monitoringAlertsTopbar = self.lookupReference('monitoringAlertsTopbar');

        if (context.record.data.ADD_OPOKS_TX.split) {
            context.record.data.ADD_OPOKS_TX = context.record.data.ADD_OPOKS_TX.replace(/\s/g, '').split(',');
        }
        if ((context.field !== 'ADD_OPOKS_TX' && context.field !== 'OPOK_NB') || (self.getViewModel().get('Alerts_READONLY'))) {
            // ставим флаг открытия футера
            if (toggleBBarBtn.pressed) {
                monitoringAlertsTopbar.reestablishView = true;
            }
            // закрываем нижную панель детали операции:
            toggleBBarBtn.setPressed(false);
            // открываем правую панель ОЭС - Редактирование - Сведения об операции:
            if (!Ext.isEmpty(context.record.getData().OES321ID)) {
                self.getView().add({
                    xtype: 'app-alerts-oes321detailspanel',
                    viewModel: {
                        data: {
                            alertId: context.record.getData().REVIEW_ID,
                            oes321Id: context.record.getData().OES321ID
                        }
                    }
                });
            } else {
                // открываем правую панель детали операции:
                self.getView().add({
                    xtype: 'app-alerts-alertdetailspanel',
                    viewModel: {
                        data: {
                            alertId: context.record.getData().REVIEW_ID
                        }
                    }
                });
            }
            return false;
        }
    },

    afterEditRow: function (editor, e) {
        var self = this,
            alertIds = [e.record.get('REVIEW_ID')],
            actvyTypeCds = ['RF_SETOPOK'],
            opok = e.record.get('OPOK_NB'),
            addOpoks = e.record.get('ADD_OPOKS_TX');

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode({
                form: 'AlertAction',
                action: 'execById',
                actvyTypeCds: actvyTypeCds,
                opok: opok,
                addOpoks: addOpoks,
                alertIds: alertIds
            }),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                // переподкачка списка операций:
                if (!Ext.isEmpty(alertIds)) {
                    self.refreshAlertsList({
                        alertIds: alertIds,
                        addRecords: true
                    });
                }
            }
        });
    },

    onLoadAlerts: function (store, records, successful, operation) {
        var self = this,
            grid = self.lookupReference('alertsGrid'),
            gridStore = grid.getStore(),
            vm = self.getViewModel(),
            selectedAlertIndex = gridStore.indexOf(vm.get('selectedAlert')) != -1 ? gridStore.indexOf(vm.get('selectedAlert')) : 0,
            responseObj = operation.getResponse() ? Ext.JSON.decode(operation.getResponse().responseText) : '',
            grandTotal = '...',
            viewAppMain = Ext.getCmp('app-main-id'),
            vmAppMain = (viewAppMain) ? viewAppMain.getViewModel() : false,
            toggleBBarBtn = self.lookupReference('toggleBBarBtn'),
            reviewId = (vmAppMain) ? vmAppMain.get('createAlertReviewId') : false,
            oes321Id = (vmAppMain) ? vmAppMain.get('createAlertOES321Id') : false,
            reviewId2 = (vmAppMain) ? vmAppMain.get('createAlertReviewId2') : false,
            oes321Id2 = (vmAppMain) ? vmAppMain.get('createAlertOES321Id2') : false,
            oes321ADD = (vmAppMain) ? vmAppMain.get('createAlertADD') : false;

        if (oes321ADD) {
            var recOES = gridStore.findRecord('REVIEW_ID', reviewId, 0, false, false, true);
            if (recOES) {
                vmAppMain.set('createAlertADD', null);
                oes321Id = recOES.get('OES321ID');
            }
        }
        //eсли пришли после создания межфилиальных ОЭС
        if (reviewId && oes321Id && reviewId2 && oes321Id2) {
            vmAppMain.set('createAlertReviewId', null);
            vmAppMain.set('createAlertOES321Id', null);
            vmAppMain.set('createAlertReviewId2', null);
            vmAppMain.set('createAlertOES321Id2', null);
            self.oes321DetailsPanel = self.getView().add({
                xtype: 'app-alerts-oes321detailspanel',
                viewModel: {
                    type: 'oes321detailspanelviewmodel',
                    data: {
                        alertId: reviewId,
                        oes321Id: oes321Id
                    }
                }
            });
            var oes321DetailsPanelVM2 = self.oes321DetailsPanel.getViewModel();
            if (oes321DetailsPanelVM2) {
                oes321DetailsPanelVM2.set('RV_TRXN_SEQ_ID', false);
                var newRvTrxnSeqIDs2 = vm.get('newRvTrxnSeqIDs');
                newRvTrxnSeqIDs2.push(reviewId);
                vm.set('newRvTrxnSeqIDs', newRvTrxnSeqIDs2);
            }
            if (oes321DetailsPanelVM2) {
                oes321DetailsPanelVM2.set('RV_TRXN_SEQ_ID', false);
                var newRvTrxnSeqIDs3 = vm.get('newRvTrxnSeqIDs');
                newRvTrxnSeqIDs3.push(reviewId2);
                vm.set('newRvTrxnSeqIDs', newRvTrxnSeqIDs3);
            }
            // закрываем нижную панель детали операции:
            toggleBBarBtn.setPressed(false);

            selectedAlertIndex = gridStore.find('REVIEW_ID', reviewId);
            var selectedAlertIndex2 = gridStore.find('REVIEW_ID', reviewId2);
            if (selectedAlertIndex) {
                if (gridStore.getAt(selectedAlertIndex)) {
                    grid.getView().focusRow(gridStore.getAt(selectedAlertIndex));
                    grid.getView().setSelection(gridStore.getAt(selectedAlertIndex));
                }
            }
            if (selectedAlertIndex2) {
                if (gridStore.getAt(selectedAlertIndex2)) {
                    grid.getView().focusRow(gridStore.getAt(selectedAlertIndex2));
                    grid.getView().setSelection(gridStore.getAt(selectedAlertIndex2));
                }
            }

            // Создано сообщений
            vm.set('newCountAlert', parseInt(vm.get('newCountAlert'), 10) + 1);
        }

        // если пришли после создания записи
        if (reviewId && oes321Id) {
            vmAppMain.set('createAlertReviewId', null);
            vmAppMain.set('createAlertOES321Id', null);
            // грузим для редактирования
            self.oes321DetailsPanel = self.getView().add({
                xtype: 'app-alerts-oes321detailspanel',
                viewModel: {
                    type: 'oes321detailspanelviewmodel',
                    data: {
                        alertId: reviewId,
                        oes321Id: oes321Id
                    }
                }
            });
            var oes321DetailsPanelVM = self.oes321DetailsPanel.getViewModel();
            if (oes321DetailsPanelVM) {
                oes321DetailsPanelVM.set('RV_TRXN_SEQ_ID', false);
                var newRvTrxnSeqIDs = vm.get('newRvTrxnSeqIDs');
                newRvTrxnSeqIDs.push(reviewId);
                vm.set('newRvTrxnSeqIDs', newRvTrxnSeqIDs);
            }

            // закрываем нижную панель детали операции:
            toggleBBarBtn.setPressed(false);

            selectedAlertIndex = gridStore.find('REVIEW_ID', reviewId);
            if (gridStore.getAt(selectedAlertIndex)) {
                grid.getView().focusRow(gridStore.getAt(selectedAlertIndex));
                grid.getView().setSelection(gridStore.getAt(selectedAlertIndex));
            }

            // Создано сообщений
            vm.set('newCountAlert', parseInt(vm.get('newCountAlert'), 10) + 1);
        }

        // для отображения количества загруженных записей в футере:
        if (responseObj.grandTotal || responseObj.grandTotal === 0 || responseObj.grandTotal === null) {
            if (responseObj.grandTotal === 0) {
                grandTotal = '0';
            } else if (responseObj.grandTotal > 0) {
                grandTotal = responseObj.grandTotal;
            }
            vm.set('grandTotal', grandTotal);
            vm.set('totalCount', responseObj.totalCount);
            // для порционной подрузки списка:
            vm.set('fetchId', responseObj.fetchId ? responseObj.fetchId : '');
            if (responseObj.grandTotal === 0) {
                // закрываем нижную панель детали операции:
                toggleBBarBtn.setPressed(false);
            }
        }

        // удаление filtered записей:
        if (responseObj) {
            if (!Ext.isEmpty(responseObj.filtered) && !Ext.isEmpty(responseObj.filtered[0])) {
                store.remove(store.queryBy(function (record) {
                    return Ext.Array.contains(responseObj.filtered, record.get('REVIEW_ID'));
                }).getRange());
                // по выполнению операции обновить количество операций(totalCount):
                vm.set('totalCount', vm.get('totalCount') - responseObj.filtered.length);
            }
            // выбираем заново строку из грида для обновления подвала детали:
            if ((selectedAlertIndex >= 0) && responseObj.data && (gridStore.data.length > 0)) {
                if (!gridStore.getAt(selectedAlertIndex)) {
                    selectedAlertIndex = 0;
                }
                grid.getView().focusRow(gridStore.getAt(selectedAlertIndex));
                grid.getView().setSelection(gridStore.getAt(selectedAlertIndex));
            }
        }
    },

    onLoadFiltersCollection: function (store) {
        var self = this,
            vm = this.getViewModel(),
            defaultFilter = store.findRecord('IS_DEFAULT', 1),
            alertsStore = this.getStore('alertsStore');

        if (defaultFilter) {
            vm.set('scopeFilter', defaultFilter.get('_id_'));
            alertsStore.load({
                params: {
                    where: defaultFilter.get('_id_')
                }
            });
        } else {
            alertsStore.load();
        }
    },

    onDbClickAmountFrom: function () {
        var vm = this.getViewModel(),
            valTo;

        if (vm.get('search.amountTo') && vm.get('search.amountTo') !== '') {
            valTo = Ext.util.Format.number(parseFloat(vm.get('search.amountTo').replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')), '0,000.00');
            vm.set('search.amountFrom', valTo);
        }
    },

    onDbClickAmountTo: function () {
        var self = this,
            vm = this.getViewModel(),
            valFrom;

        if (vm.get('search.amountFrom') && vm.get('search.amountFrom') !== '') {
            valFrom = Ext.util.Format.number(parseFloat(vm.get('search.amountFrom').replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')), '0,000.00');
            vm.set('search.amountTo', valFrom);
        }
    },

    // получаем Next and Previous => REVIEW_ID(alertID) и OES321ID для деталий сообщения
    getNextPreviousAlerts: function (alertId, store) {
        var selectedAlertIndex = false,
            nextAlertId,
            nextOES321Id,
            nextSelectedAlertIndex,
            previousAlertId,
            previousOES321Id,
            previousSelectedAlertIndex,
            dataNext,
            dataPrevious,
            thisIndex,
            thisAlertId,
            thisOES321Id,
            thisData;

        if (alertId !== null && alertId > 0) {
            if (store.data.items.length > 0) {
                Ext.Array.forEach(store.data.items, function (item, index) {
                    if (selectedAlertIndex) {
                        // обработка next or previous
                        if (store.findRecord('REVIEW_ID', parseInt(item.id, 10))) {
                            dataNext = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                            if (!nextOES321Id && dataNext.OES321ID) {
                                nextAlertId = parseInt(item.id, 10);
                                nextOES321Id = dataNext.OES321ID;
                                nextSelectedAlertIndex = index;
                            }
                        }
                    } else {
                        if (parseInt(item.id, 10) == alertId) {
                            selectedAlertIndex = true;
                            thisIndex = index;
                            thisAlertId = parseInt(item.id, 10);
                            thisData = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                            thisOES321Id = thisData.OES321ID;
                        } else {
                            if (!thisOES321Id && item.id) {
                                if (store.findRecord('REVIEW_ID', parseInt(item.id, 10))) {
                                    dataPrevious = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                                    if (dataPrevious.OES321ID) {
                                        previousAlertId = parseInt(item.id, 10);
                                        previousOES321Id = dataPrevious.OES321ID;
                                        previousSelectedAlertIndex = index;
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }

        return {
            thisIndex: thisIndex,
            thisAlertId: thisAlertId,
            thisOES321Id: thisOES321Id,
            nextAlertId: nextAlertId,
            nextOES321Id: nextOES321Id,
            nextSelectedAlertIndex: nextSelectedAlertIndex,
            previousAlertId: previousAlertId,
            previousOES321Id: previousOES321Id,
            previousSelectedAlertIndex: previousSelectedAlertIndex
        }
    },

    // получаем Next and Previous => REVIEW_ID(alertID) для деталий сообщения
    getNextPreviousAlertIDs: function (alertId, store) {
        var nextAlertId,
            nextOES321Id,
            nextSelectedAlertIndex,
            previousAlertId,
            previousOES321Id,
            previousSelectedAlertIndex,
            dataNext,
            dataPrevious,
            thisIndex,
            thisAlertId,
            thisOES321Id,
            thisData;

        if (alertId !== null && alertId > 0) {
            if (store.data.items.length > 0) {
                Ext.Array.forEach(store.data.items, function (item, index) {
                    if (parseInt(item.id, 10) === parseInt(alertId, 10)) {
                        selectedAlertIndex = true;
                        thisIndex = index;
                        thisAlertId = parseInt(item.id, 10);
                        thisData = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                        thisOES321Id = thisData.OES321ID;

                        if (store.getAt(thisIndex + 1)) {
                            dataNext = store.getAt(thisIndex + 1).getData();
                            if (dataNext) {
                                nextAlertId = parseInt(dataNext._id_, 10);
                                nextOES321Id = dataNext.OES321ID;
                                nextSelectedAlertIndex = thisIndex + 1;
                            }
                        }
                        if (store.getAt(thisIndex - 1)) {
                            dataPrevious = store.getAt(thisIndex - 1).getData();
                            if (dataPrevious) {
                                previousAlertId = parseInt(dataPrevious._id_, 10);
                                previousOES321Id = dataPrevious.OES321ID;
                                previousSelectedAlertIndex = thisIndex - 1;
                            }
                        }
                    }

                })
            }
        }

        return {
            thisIndex: thisIndex,
            thisAlertId: thisAlertId,
            thisOES321Id: thisOES321Id,
            nextAlertId: nextAlertId,
            nextOES321Id: nextOES321Id,
            nextSelectedAlertIndex: nextSelectedAlertIndex,
            previousAlertId: previousAlertId,
            previousOES321Id: previousOES321Id,
            previousSelectedAlertIndex: previousSelectedAlertIndex
        }
    },

    loadToTabsAlert: function (to, nextPreviousAlert) {
        var self = this,
            grid = self.lookupReference('alertsGrid'),
            gridStore = grid.getStore(),
            alertId, OES321Id, indexAlert, tabs;

        if (!Ext.isEmpty(nextPreviousAlert)) {
            if (to === 1 && nextPreviousAlert.nextAlertId && nextPreviousAlert.nextOES321Id && nextPreviousAlert.nextSelectedAlertIndex) { // next
                alertId = nextPreviousAlert.nextAlertId;
                OES321Id = nextPreviousAlert.nextOES321Id;
                indexAlert = nextPreviousAlert.nextSelectedAlertIndex;
            } else if (to === -1 && nextPreviousAlert.previousAlertId && nextPreviousAlert.previousOES321Id
                && (nextPreviousAlert.previousSelectedAlertIndex || nextPreviousAlert.previousSelectedAlertIndex === 0)) { // previous
                alertId = nextPreviousAlert.previousAlertId;
                OES321Id = nextPreviousAlert.previousOES321Id;
                indexAlert = nextPreviousAlert.previousSelectedAlertIndex;
            }
            if (alertId && OES321Id && (indexAlert || indexAlert === 0)) {
                tabs = self.getView().getComponent('appAlertsOES321detailsPanel');
                self.getView().remove(tabs);
                self.getView().add({
                    xtype: 'app-alerts-oes321detailspanel',
                    viewModel: {
                        type: 'oes321detailspanelviewmodel',
                        data: {
                            alertId: alertId,
                            oes321Id: OES321Id
                        }
                    }
                });

                var last = gridStore.getAt(indexAlert);
                grid.getView().focusRow(last);
                grid.getSelectionModel().select(last);

            }
        }
    },

    copySelectedOES: function () {
        var self = this,
            alertIds = [],
            oesIds = [],
            actvyTypeCds = ['RF_OESCOPY'],
            selectedAlertsCnt = self.lookupReference('alertsGrid').getSelectionModel().getSelected().length,
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel,
            oes321DetailsPanel,
            alertId,
            params,
            type = AML.app.msg._getType('question'),
            msgText,
            selectedAlerts;

        alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel');
        oes321DetailsPanel = self.oes321DetailsPanel || self.getView().down('app-alerts-oes321detailspanel');

        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }

        if (selectedAlertsCnt > 1) {
            Ext.Msg.alert('', 'Выделено более одной операции');
            return;
        }

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel && oes321DetailsPanel.getViewModel()) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        if (self.getView().down('app-alerts-oes321detailspanel') || self.getView().down('app-alerts-alertdetailspanel')) {
            selectedAlerts = [alertDetailsModel];
        }
        if (!alertDetailsModel.get('REVIEW_ID')) {
            selectedAlerts = self.lookupReference('alertsGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsGrid').getSelectionModel().getSelection() : [alertDetailsModel];
        }

        if (selectedAlerts) {
            for (var i = 0; i < selectedAlerts.length; i++) {
                alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
                oesIds.push(selectedAlerts[i].get('OES321ID'));
            }
        }

        params = {
            form: 'AlertAction',
            action: 'execById',
            actvyTypeCds: actvyTypeCds,
            alertIds: alertIds
        };

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }

        if (alertIds[0] && oesIds[0]) {
            msgText = 'Вы действительно хотите создать копию операции № ' + alertIds[0] + ', ОЭС № ' + oesIds[0] + '?';

        } else {
            msgText = 'Вы действительно хотите создать копию операции?';
        }
        Ext.Msg.show({
            title: 'Внимание!',
            msg: msgText,
            buttonText: {
                yes: 'Создать', no: 'Отмена'
            },
            icon: type.icon,
            buttons: Ext.Msg.YESNO,
            fn: function (answer) {
                if (answer === 'yes') {

                    // добавим loading обработки:
                    if (!common.globalinit.loadingMaskCmp) {
                        common.globalinit.loadingMaskCmp = self.getView();
                    }
                    Ext.Ajax.request({
                        url: common.globalinit.ajaxUrl,
                        method: common.globalinit.ajaxMethod,
                        headers: common.globalinit.ajaxHeaders,
                        timeout: common.globalinit.ajaxTimeOut,
                        params: Ext.encode({
                            form: 'AlertAction',
                            action: 'execById',
                            actvyTypeCds: actvyTypeCds,
                            alertIds: alertIds
                        }),
                        success: function (response) {
                            var responseObj = Ext.JSON.decode(response.responseText);
                            if (responseObj.message) {
                                Ext.Msg.alert('Внимание!', responseObj.message);
                            }
                            if (responseObj.success && responseObj.alertIds && (responseObj.alertIds.length >= 1)) {
                                var viewAppMain = Ext.getCmp('app-main-id'),
                                    parentModel = (self.getViewModel().getParent()) ? self.getViewModel().getParent() : false,
                                    parentView = (parentModel) ? parentModel.getView() : false,
                                    // alertsList = (parentView) ? parentView.lookupReference('alertsList') : false,
                                    alertsList = (parentView) ? parentView.down('app-monitoring-alerts') : false,
                                    alertsListVM = (alertsList) ? alertsList.getViewModel() : false,
                                    alertsListStore = (alertsListVM) ? alertsListVM.getStore('alertsStore') : false,
                                    vmAppMain = (viewAppMain) ? viewAppMain.getViewModel() : false;

                                // фиксируем id созданой записи и подгружаем грид для нее
                                if (vmAppMain) {
                                    vmAppMain.set('createAlertReviewId', responseObj.alertIds[0]);
                                    vmAppMain.set('createAlertADD', true);
                                    vmAppMain.set('newCountAlert', 0);
                                }

                                if (alertsListStore) {
                                    alertsList.getViewModel().getStore('alertsStore').load({
                                        alertIds: responseObj.alertIds[0],
                                        addRecords: true,
                                        //scope: self,
                                        callback: function () {
                                            // прогружаем грид
                                            AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                                        }
                                    });
                                } else {
                                    // прогружаем грид
                                    AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                                }
                            }
                        }
                    });
                }
            }
        });
    },

    setFilialsKGRKO: function () {
        var self = this,
            vm = self.getViewModel(),
            selectedAlerts = vm.get('selectedAlert'),
            selectedGrid = (self) ? self.lookupReference('alertsGrid') : false,
            selectedGridModel = (selectedGrid) ? selectedGrid.getSelectionModel() : false,
            selectedGridSelect = (selectedGridModel) ? selectedGridModel.getSelected() : false,
            selectedAlertsCnt = (selectedGridSelect) ? selectedGridSelect.length : false,
            params, opCatCd, trxnSeqId;

        if (selectedAlertsCnt === 0) {
            Ext.Msg.alert('', 'Не выбрано ни одной операции!');
            return;
        }

        if (selectedAlertsCnt > 1) {
            Ext.Msg.alert('', 'Выделено более одной операции!');
            return;
        }
        if (selectedAlertsCnt === 1) {
            selectedAlerts = (vm) ? vm.get('selectedAlert') : false;
        }

        opCatCd = (selectedAlerts) ? selectedAlerts.get('RV_OP_CAT_CD') : null;
        trxnSeqId = (selectedAlerts) ? selectedAlerts.get('RV_TRXN_SEQ_ID') : null;
        params = {
            form: 'TrxnExtra',
            action: 'read',
            op_cat_cd: opCatCd,
            trxn_seq_id: (trxnSeqId) ? parseInt(trxnSeqId, 10) : null
        };

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText), rec, rec2,
                    storeOSB = Ext.data.StoreManager.lookup('OES321Org');
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                if (responseObj.success) {
                    var dataResp = (responseObj.data) ? responseObj.data[0] : false;
                    if (dataResp) {
                        var double_kgrko_fl = (dataResp.SEND_OES_ORG_ID) ? 1 : 0,
                            double_kgrko_fl2 = (dataResp.RCV_OES_ORG_ID) ? 1 : 0,
                            SEND_ID = (dataResp.SEND_OES_ORG_ID) ? dataResp.SEND_OES_ORG_ID : null,
                            RCV_ID = (dataResp.RCV_OES_ORG_ID) ? dataResp.RCV_OES_ORG_ID : null,
                            CREATED_BY = (dataResp.CREATED_BY) ? dataResp.CREATED_BY : null,
                            CREATED_DATE = (dataResp.CREATED_DATE) ? dataResp.CREATED_DATE : null,
                            MODIFIED_BY = (dataResp.MODIFIED_BY) ? dataResp.MODIFIED_BY : null,
                            MODIFIED_DATE = (dataResp.MODIFIED_DATE) ? dataResp.MODIFIED_DATE : null;
                        if (double_kgrko_fl && SEND_ID) {
                            rec = (storeOSB) ? storeOSB.findRecord('OES_321_ORG_ID', SEND_ID, 0, false, false, true) : null;
                        }
                        if (double_kgrko_fl2 && RCV_ID) {
                            rec2 = (storeOSB) ? storeOSB.findRecord('OES_321_ORG_ID', RCV_ID, 0, false, false, true) : null;
                        }
                        Ext.widget({
                            xtype: 'app-alerts-alertactionform-kgrkofilials',
                            viewModel: {
                                type: 'alertactionkgrkofilialsviewmodel',
                                parentVM: vm,
                                data: {
                                    record: {
                                        double_kgrko_fl: double_kgrko_fl,
                                        double_kgrko_fl2: double_kgrko_fl2,
                                        tbIDForPayerKGRKOFilials: (rec) ? rec.get('TB_CODE') : null,
                                        tbOsbForPayerKGRKOFilials: (rec) ? rec.get('OSB_CODE') : null,
                                        tbIDForPayerKGRKOFilials2: (rec2) ? rec2.get('TB_CODE') : null,
                                        tbOsbForPayerKGRKOFilials2: (rec2) ? rec2.get('OSB_CODE') : null,
                                        CREATED_BY: CREATED_BY,
                                        CREATED_DATE: CREATED_DATE,
                                        MODIFIED_BY: MODIFIED_BY,
                                        MODIFIED_DATE: MODIFIED_DATE,
                                        oes_321_org_id: SEND_ID,
                                        oes_321_org_id2: RCV_ID
                                    },
                                    control2KGRKO: true,
                                    control2KGRKO2: true
                                }
                            }
                        }).show();
                    }
                }
            }
        });
    },

    control2FilialsKGRKO: function () {
        var self = this,
            vm = self.getViewModel(),
            selectedAlerts = vm.get('selectedAlert'),
            selectedGrid = (self) ? self.lookupReference('alertsGrid') : false,
            selectedGridModel = (selectedGrid) ? selectedGrid.getSelectionModel() : false,
            selectedGridSelect = (selectedGridModel) ? selectedGridModel.getSelected() : false,
            selectedAlertsCnt = (selectedGridSelect) ? selectedGridSelect.length : false,
            OPOK_NB, ADD_OPOKS_TX, DATE_S, OWNER_ID, params, oes321Id;


        if (selectedAlertsCnt === 0) {
            Ext.Msg.alert('', 'Не выбрано ни одной операции!');
            return;
        }

        if (selectedAlertsCnt > 1) {
            Ext.Msg.alert('', 'Выделено более одной операции!');
            return;
        }
        if (selectedAlertsCnt === 1) {
            selectedAlerts = (vm) ? vm.get('selectedAlert') : false;
        }
        oes321Id = (selectedAlerts) ? selectedAlerts.get('OES321ID') : null;
        params = {
            form: 'OES321Details',
            action: 'readByOesId',
            oes321Id: oes321Id
        };

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText), dateSArr, prevDATE_S, DATE_S;
                //if (responseObj.success) {
                OPOK_NB = (selectedAlerts) ? selectedAlerts.get('OPOK_NB') : null;
                ADD_OPOKS_TX = (selectedAlerts) ? selectedAlerts.get('ADD_OPOKS_TX').toString() : null;
                dateSArr = (responseObj.data) ? responseObj.data[0] : null;
                prevDATE_S = (dateSArr) ? dateSArr.DATE_S : null;
                if (prevDATE_S) {
                    var a = prevDATE_S.split(" ");
                    if (a) {
                        var d = a[0].split("-");
                        if(d && d[0] === '2099') {
                            DATE_S = null;
                        } else if(d && d[0] !== '2099') {
                            DATE_S = new Date(d[0], (d[1] - 1), d[2]);
                        }
                    }
                } else {
                    DATE_S = null;
                }
                OWNER_ID = (selectedAlerts) ? selectedAlerts.get('RV_OWNER_ID') : null;
                var storeOwners = Ext.data.StoreManager.lookup('Owners'),
                    recOwner = (storeOwners) ? storeOwners.findRecord('OWNER_ID', OWNER_ID) : null;

                Ext.widget({
                    xtype: 'app-alerts-alertactionform-control2kgrkofilials',
                    viewModel: {
                        type: 'alertactioncontrol2kgrkofilialsviewmodel',
                        data: {
                            VO_CONVERSION: OPOK_NB,
                            DOP_V_CONVERSION: (ADD_OPOKS_TX) ? ADD_OPOKS_TX.split(',') : null,
                            OWNER_ID: (recOwner) ? recOwner.get('id') : null,
                            // DATE_S: (DATE_S) ? Ext.Date.format(new Date(DATE_S), 'd.m.Y') : null
                            DATE_S: (DATE_S) ? DATE_S : null
                        },
                        parentVM: selectedAlerts,
                        parentList: vm
                    }
                }).show();
            }
        });
    },

    openGOZChanges: function (opCatCd, trxnId) {
        var vm = this.getViewModel();

        Ext.create({
            xtype: 'app-monitoring-alerts-documentgozhistory-form',
            viewModel: {
                xtype: 'app-monitoring-alerts-documentgozhistory-form',
                data: {
                    isOpenReadOnly: true,
                    opCatCd: opCatCd,
                    trxnId: trxnId
                }
            }
        }).show();
    }
});