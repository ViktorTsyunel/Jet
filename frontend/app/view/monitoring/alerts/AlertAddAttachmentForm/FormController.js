Ext.define('AML.view.monitoring.alerts.AlertAddAttachmentForm.FormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertaddattachmentform',

    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],

    config: {
        listen: {
            component: {
                '#attachForm': {
                    click: 'onClickAttachForm'
                },
                '#cancelAttachForm': {
                    click: 'onClickCancelAttachForm'
                }
            }
        }
    },
    onClickAttachForm: function () {
        var self = this,
            vm = self.getViewModel(),
            fileUploadInput = this.lookupReference('fileUploadInput'),
            form = this.lookupReference('fileUploadForm'),
            selectedAlerts = vm.get('selectedAlerts'),
            alertIds = [];

        for (var i = 0; i < selectedAlerts.length; i++) {
            alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
        }

        vm.set('alertIds', alertIds);
        vm.set('filePath', fileUploadInput.getValue());

        form.submit({
            scope: self,
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: {
                "Content-Type": null
            }
        });
    },

    onClickSubmitAttachForm: function(form, success, me) {
        var self = me.scope,
            vm = self.getViewModel(),
            files = form['upload_file'].files,
            fileUploadInput = self.lookupReference('fileUploadInput'),
            ajaxOptions,
            finalCallbackSuccess,
            finalCallbackFailure,
            totalSize = 0;

        Ext.Array.each(files, function(f) {
            totalSize += f.size;
        });

        if (totalSize > me.files_size * 1024 * 1024) {
            Ext.Msg.alert('','Суммарный размер файлов больше ' + me.files_size + 'Мб!');
            me.failure();
            return;
        }

        finalCallbackSuccess = function(response) {
            var result = Ext.decode(response.responseText),
                callbackFn = vm.get('attachActionCallback');
                selectedAlerts = vm.get('selectedAlerts'),
                alertsGridVm = vm.getParent(),
                alertsGridCtrl = alertsGridVm && alertsGridVm.getView().getController();

            if (result.success == true && result.message) {
                Ext.Msg.alert(' ',result.message);
            }
            if(!result.success) {
                // реинициализация множественного выбора файлов:
                self.fileFieldAfterRender(fileUploadInput);
            }
            // переподкачка списка операций:
            if(!Ext.isEmpty(selectedAlerts[0].id) && callbackFn === null &&
                    alertsGridVm.getView().reference !== 'main-search-operations-form') {
                alertsGridCtrl.refreshAlertsList({
                    alertIds: selectedAlerts[0].get('REVIEW_ID'),
                    addRecords: true
                });
            }
            // обратный вызов функции для выполнения произвольных действий вне формы:
            callbackFn && callbackFn();
        };

        finalCallbackFailure = function() {
            // реинициализация множественного выбора файлов:
            self.fileFieldAfterRender(fileUploadInput);
        };

        ajaxOptions = {
            url: common.globalinit.ajaxAlertAddAttachmentUrl,
            timeout: common.globalinit.ajaxTimeOut,
            method: me.getMethod(),
            headers: me.headers,
            form: form,
            isUpload: true,
            success: finalCallbackSuccess,
            failure: finalCallbackFailure
        };

        // добавим loading обработки:
        if(!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request(ajaxOptions);
    },

    onInputFileChange: function (fileInput, value) {
        var newValue = value.split('\\'),
            files = fileInput.fileInputEl.dom.files,
            fileNames = [];

        Ext.Array.each(files, function(f) {
            fileNames.push(f.name);
        });

        if(fileNames.length)
            fileInput.setRawValue(fileNames.join(', '));
        else
            fileInput.setRawValue(newValue[newValue.length-1]);
    },

    fileFieldAfterRender: function (cmp) {
        if (Ext.isIE8 || Ext.isIE9) {
            cmp.inputEl.set({ disabled: 'disabled' });
        }
        cmp.fileInputEl.set({
            multiple: ''
        });
    },

    onClickCancelAttachForm: function () {
        this.getView().close();
    }
});
