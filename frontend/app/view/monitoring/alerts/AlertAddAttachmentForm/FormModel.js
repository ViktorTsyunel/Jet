Ext.define('AML.view.monitoring.alerts.AlertAddAttachmentForm.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alertaddattachmentformviewmodel',
    data: {
        title: 'Добавить вложение',
        formName: 'AlertAddAttachment'
        //record: null
    },
    stores: {
        alertCommentStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'AlertComment',
                    action: 'read'
                }
            }
        }
    }
});