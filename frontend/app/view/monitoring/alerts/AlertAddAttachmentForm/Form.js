Ext.define('AML.view.monitoring.alerts.AlertAddAttachmentForm.Form', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.form.field.File',
        'Ext.form.action.DirectSubmit',
        'Ext.form.field.Hidden'
    ],
    xtype: 'app-alerts-alertaddattachment-form',
    controller: 'alertaddattachmentform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertaddattachmentformviewmodel'
    },
    bind: {
        title: '{title}'
    },
    width: 450,
    height: 350,
    scrollable: 'vertical',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    initComponent: function () {
        var self = this;

        self.items = [
            {
                xtype: 'form',
                reference: 'fileUploadForm',
                border: 0,
                defaults: {
                    flex: 1,
                    width: '100%'
                },
                layout: {
                    type: 'vbox'
                },
                api: {
                    submit: self.getController().onClickSubmitAttachForm
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'form',
                        bind: '{formName}'
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'alertId',
                        bind: '{alertIds}'
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'fileName',
                        bind: '{filePath}'
                    },
                    {
                        xtype: 'fieldset',
                        title: 'Файл вложения',
                        margin: '10 0 0 0',
                        padding: '10 20 10 20',
                        defaults: {
                            flex: 1,
                            width: '100%'
                        },
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'filefield',
                                reference: 'fileUploadInput',
                                name: 'upload_file',
                                fieldLabel: 'Файл',
                                labelWidth: 35,
                                buttonText: 'Выбрать',
                                msgTarget: 'side',
                                buttonOnly: false,
                                listeners: {
                                    change: 'onInputFileChange',
                                    afterrender: 'fileFieldAfterRender'
                                }
                            },
                            {
                                xtype: 'textfield',
                                reference: 'fileName',
                                name: 'attachmentName',
                                fieldLabel: 'Наименование',
                                allowBlank: false,
                                labelWidth: 85
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        title: 'Комментарий',
                        margin: '10 0 0 0',
                        padding: '10 20 10 20',
                        defaults: {
                            flex: 1,
                            width: '100%'
                        },
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'combobox',
                                name: 'cmmntId',
                                reference: 'commentStd',
                                editable: false,
                                triggerClear: true,
                                queryMode: 'local',
                                tpl: Ext.create('Ext.XTemplate',
                                    '<tpl for=".">',
                                    '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{CMMNT_TX}</div>',
                                    '</tpl>'
                                ),
                                displayField: 'CMMNT_TX',
                                valueField: 'CMMNT_ID',
                                fieldLabel: 'Стандартный комментарий',
                                width: '100%',
                                labelWidth: 80,
                                bind: {
                                    store: '{alertCommentStore}',
                                    value: '{record.CMMNT_ID}'
                                }
                            },
                            {
                                xtype: 'textareafield',
                                grow: true,
                                name: 'noteTx',
                                reference: 'comment',
                                fieldLabel: 'Произвольный комментарий',
                                labelAlign: 'top',
                                listeners: {
                                    afterrender: function (field) {
                                        field.focus();
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ];

        self.callParent(arguments);
    },

    buttons: [
        {
            itemId: 'attachForm',
            iconCls: 'icon-save-data',
            text: 'Прикрепить'
        },
        {
            itemId: 'cancelAttachForm',
            iconCls: 'icon-backtothe-primitive',
            text: 'Отмена'
        }
    ]
});