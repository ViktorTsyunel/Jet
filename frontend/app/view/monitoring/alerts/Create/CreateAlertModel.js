Ext.define('AML.view.monitoring.alerts.Create.CreateAlertModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.createalertformvm',
	data: {
		title: 'Создание ОЭС'
	},
    record: {
        opok_nb: null,
        tb_id: null,
        osb_id: null,
        due_dt: null,
        owner_id: null,
        note_tx: null,
        opok_nb2: null,
        tb_id2: null,
        osb_id2: null,
        due_dt2: null,
        owner_id2: null,
        note_tx2: null,
        oes_321_org_id: null,
        oes_321_org_id2: null,
        double_kgrko_fl: 0
    },
    formulas: {
        DOP_V_CONVERSION: {
            bind: {
                DOP_V: '{record.add_opoks_tx}'
            },
            get: function (dataObject) {
                var dataDopV = (dataObject.DOP_V) ? dataObject.DOP_V.split(',') : ['0'],
                    newDataDopV = [];
                if (dataDopV && Ext.isArray(dataDopV) && dataDopV.length === 1 && dataDopV[0] === '0') {
                    return dataDopV;
                } else if (dataDopV && Ext.isArray(dataDopV)) {
                    Ext.Array.each(dataDopV, function (item) {
                        if (item !== '0') {
                            newDataDopV.push(item);
                        }
                    });
                }
                return newDataDopV;
            },
            set: function (value) {
                if (value) {
                    var dataDopV = [];
                    if (Ext.isArray(value) && value.length === 1 && value[0] === 0) {
                        this.set('record.add_opoks_tx', '0');
                    } else if (Ext.isArray(value)) {
                        Ext.Array.each(value, function (item) {
                            if (item !== 0) {
                                dataDopV.push(item);
                            }
                        });
                        this.set('record.add_opoks_tx', dataDopV.join(','));
                    }
                }
            }
        },
        DOP_V_CONVERSION2: {
            bind: {
                DOP_V2: '{record.add_opoks_tx2}'
            },
            get: function (dataObject) {
                var dataDopV = (dataObject.DOP_V2) ? dataObject.DOP_V2.split(',') : ['0'],
                    newDataDopV = [];
                if (dataDopV && Ext.isArray(dataDopV) && dataDopV.length === 1 && dataDopV[0] === '0') {
                    return dataDopV;
                } else if (dataDopV && Ext.isArray(dataDopV)) {
                    Ext.Array.each(dataDopV, function (item) {
                        if (item !== '0') {
                            newDataDopV.push(item);
                        }
                    });
                }
                return newDataDopV;
            },
            set: function (value) {
                if (value) {
                    var dataDopV = [];
                    if (Ext.isArray(value) && value.length === 1 && value[0] === 0) {
                        this.set('record.add_opoks_tx2', '0');
                    } else if (Ext.isArray(value)) {
                        Ext.Array.each(value, function (item) {
                            if (item !== 0) {
                                dataDopV.push(item);
                            }
                        });
                        this.set('record.add_opoks_tx2', dataDopV.join(','));
                    }
                }
            }
        },
        filters: {
            bind: {
                TB_CODE: '{record.tb_id}'
            },
            get: function(data) {
                var store = this.getStore('oes321org'),
                    filters = [],
                    TBCODE = data.TB_CODE;

                if (TBCODE) {
                    filters.push({
                        property: 'TB_CODE',
                        value: TBCODE
                    });
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        },
        filters2: {
            bind: {
                TB_CODE2: '{record.tb_id2}'
            },
            get: function(data) {
                var store = this.getStore('oes321org2'),
                    filters = [],
                    TBCODE2 = data.TB_CODE2;

                if (TBCODE2) {
                    filters.push({
                        property: 'TB_CODE',
                        value: TBCODE2
                    });
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник OKOP
        OPOK: {
            model: 'AML.model.OPOK',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: {
                property: 'OPOK_NB',
                direction: 'ASC'
            },
            listeners: {
                load: function () {
                    this.add({
                        GROUP_NB: null,
                        LIMIT_AM: 100000,
                        NOTE: null,
                        OES_PARTIES_TX: null,
                        OPOK_NB: '0',
                        OPOK_NM: 'Пусто',
                        ORDER_NB: '0',
                        PRIORITY_NB: '0'
                    })
                }
            }
        },
        // Справочник Owners
        Owners: {
            model: 'AML.model.Owners',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    transform: {
                        fn: function (data) {
                            if (Ext.isArray(data.data)) {
                                Ext.Array.forEach(data.data, function (i) {
                                    if (i.IS_CURRENT != undefined) {
                                        i.IS_CURRENT = (i.IS_CURRENT == 'Y');
                                    }
                                });
                                Ext.Array.forEach(data.data, function (i) {
                                    if (i.IS_SUPERVISOR != undefined) {
                                        i.IS_SUPERVISOR = (i.IS_SUPERVISOR == 'Y');
                                    }
                                });
                            }
                            return data;
                        },
                        scope: this
                    },
                    rootProperty: 'data',
                    messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'Owners',
                    action: 'read'
                }
            }
        },
        // Справочник Информация о КО
        oes321org: {
            model: 'AML.model.OES321Org',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OES_321_ORG_ID',
                    direction: 'ASC'
                }
            ],
            filters: '{filters}'
        },
        oes321org2: {
            model: 'AML.model.OES321Org',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OES_321_ORG_ID',
                    direction: 'ASC'
                }
            ],
            filters: '{filters2}'
        }
    }
});
