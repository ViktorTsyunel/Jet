Ext.define('AML.view.monitoring.alerts.Create.CreateAlertController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.createalertform',
    requires: [
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#createForm': {
                    click: 'onClickCreatForm'
                },
                '#cancellationForm': {
                    click: 'onClickCancellationForm'
                },
                '#clearnigForm': {
                    click: 'onClickClearnigForm'
                }
            }
        }
    },

    init: function () {
        //AML.app.window.clearLast();
    },

    onClickCreatForm: function (button) {
        var self = this,
            window = this.getView(),
            form = window.down('form'),
            vm = window.getViewModel(),
            record = (vm) ? vm.get('record') : false,
            type = AML.app.msg._getType('question'),
            fieldsArray = {
                opok_nb2: null,
                due_dt2: null,
                owner_id2: null,
                note_tx2: null
            };

        // если не проходим валидацию - ничего не делаем
        if (!form || !form.isValid()) {
            return;
        }
        if ((record['tb_id2'] === record['tb_id']) && (record['osb_id'] === record['osb_id2'])) {
            Ext.Msg.show({
                title: 'Внимание!',
                msg: 'Для межфилиальной операции указаны одинаковые филиалы КГРКО. ' +
                '<br>Создать два сообщения с одинаковыми филиалами?',
                buttonText: {
                    yes: 'Создать', no: 'Отмена'
                },
                icon: type.icon,
                buttons: Ext.Msg.YESNO,
                fn: function (answer) {
                        if (answer === 'yes') {
                            if (!record['due_dt'] || record['due_dt'] === '') {
                                record['due_dt'] = new Date();
                            }
                            if (!record['due_dt2'] || record['due_dt2'] === '') {
                                record['due_dt2'] = new Date();
                            }
                            if (record['double_kgrko_fl']) {
                                record['double_kgrko_fl'] = 1;
                            } else if (!record['double_kgrko_fl'] || record['double_kgrko_fl'] === '') {
                                record['double_kgrko_fl'] = 0;
                                for (var key in fieldsArray) {
                                    record[key] = null;
                                }
                            }
                            // сохраняем данные
                            self.createAlert(record);
                        }
                }});
        } else {
            if (!record['due_dt'] || record['due_dt'] === '') {
                record['due_dt'] = new Date();
            }
            if (!record['due_dt2'] || record['due_dt2'] === '') {
                record['due_dt2'] = new Date();
            }
            if (record['double_kgrko_fl']) {
                record['double_kgrko_fl'] = 1;
            } else if (!record['double_kgrko_fl'] || record['double_kgrko_fl'] === '') {
                record['double_kgrko_fl'] = 0;
                for (var key in fieldsArray) {
                    record[key] = null;
                }
            }
            // сохраняем данные
            self.createAlert(record);
        }
    },

    copyKGRKOPayerToReciever: function () {
        var window = this.getView(),
            vm = window.getViewModel(),
            fieldsArray = {
                opok_nb2: 'opok_nb',
                // tb_id2: 'tb_id',
                // osb_id2: 'osb_id',
                due_dt2: 'due_dt',
                refCreateAlertOwnerId2: 'refCreateAlertOwnerId',
                DOP_V_CONVERSION2: 'DOP_V_CONVERSION',
                note_tx2: 'note_tx'
            },
            fieldInto, fieldFrom;

        Ext.Object.each(fieldsArray, function (key, value, myself) {
            fieldInto = vm.getView().lookupReference(key);
            fieldFrom = vm.getView().lookupReference(value);
            if (value === 'DOP_V_CONVERSION') {
                vm.set(key, vm.get(value));
            }
            if (fieldInto && fieldFrom) {
                fieldInto.setValue(fieldFrom.getValue());
            }
        });


    },

    onClickCancellationForm: function() {
        this.getView().close();
    },

    onClickCancelForm: function() {
        var window = this.getView(),
            form = window.down('form'),
            vm = window.getViewModel(),
            record = (vm) ? vm.get('record') : false;

        if (this.isChangeRecord(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    // если не проходим валидацию - ничего не делаем
                    if (!form || !form.isValid()) {
                        return;
                    }
                    this.createAlert(record);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    },

    isChangeRecord: function(window) {
        var record = window.getViewModel().get('record');
        if(Ext.Object.isEmpty(record)) {
            return false;
        }

        return true;
    },

    createAlert: function(record, vm, form) {
        var self = this,
            params;

        for(var key in record) {
            if(Ext.isEmpty(record[key])) {
                delete record[key];
            }
        }
        params = {
            form: 'OES321Create',
            action:'create',
            data: [record || {}]
        };
        // добавим loading обработки:
        if(!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            params: Ext.encode(params),
            timeout: common.globalinit.ajaxTimeOut,
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                    return;
                }
                if(responseObj.success && responseObj.review_id && responseObj.oes_321_id) {
                    var viewAppMain = Ext.getCmp('app-main-id'),
                        parentModel = (self.getViewModel().getParent()) ? self.getViewModel().getParent() : false,
                        parentView = (parentModel) ? parentModel.getView() : false,
                        // alertsList = (parentView) ? parentView.lookupReference('alertsList') : false,
                        alertsList = (parentView) ? parentView.down('app-monitoring-alerts') : false,
                        alertsListVM = (alertsList) ? alertsList.getViewModel() : false,
                        alertsListStore = (alertsListVM) ? alertsListVM.getStore('alertsStore') : false,
                        vmAppMain = (viewAppMain) ? viewAppMain.getViewModel() : false;

                    // фиксируем id созданой записи и подгружаем грид для нее
                    if (vmAppMain) {
                        vmAppMain.set('createAlertReviewId', responseObj.review_id);
                        vmAppMain.set('createAlertOES321Id', responseObj.oes_321_id);
                        vmAppMain.set('createAlertReviewId2', responseObj.review_id2);
                        vmAppMain.set('createAlertOES321Id2', responseObj.oes_321_id2);
                        vmAppMain.set('newCountAlert', 0);
                    }

                    if (alertsListStore) {
                        alertsList.getViewModel().getStore('alertsStore').load({
                            alertIds: responseObj.review_id,
                            addRecords: true,
                            //scope: self,
                            callback: function() {
                                // прогружаем грид
                                AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                            }
                        });
                    } else {
                        // прогружаем грид
                        AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                    }
                    // clean form
                    self.resetForm(self);
                    // close
                    self.getView().close();
                }
            }
        });
    },

    onClickClearnigForm: function(button, eOpts) {
        this.resetForm(this);
    },

    // clean form, set dafault values
    resetForm: function(that) {
        var window = that.getView(),
            vm = that.getViewModel(),
            form = window.down('form'),
            vmMain = window.lookupReferenceHolder(true).getViewModel(),
            vmMainData = (vmMain.getData()) ? vmMain.getData() : false,
            currentUser = (vmMainData.currentUser) ? vmMainData.currentUser : false,
            refCreateAlertOwnerOSB = (window.lookupReference('refCreateAlertOSB')) ? window.lookupReference('refCreateAlertOSB') : false,
            refCreateAlertOwnerId = (window.lookupReference('refCreateAlertOwnerId')) ? window.lookupReference('refCreateAlertOwnerId') : false;
        // set disable OSB select
        if (refCreateAlertOwnerOSB) {
            refCreateAlertOwnerOSB.disable();
        }
        form.reset();
        vm.set('record', {});
        // set default value owner
        if (refCreateAlertOwnerId && currentUser) {
            refCreateAlertOwnerId.setValue(currentUser.get('OWNER_SEQ_ID'));
        }
    }
});
