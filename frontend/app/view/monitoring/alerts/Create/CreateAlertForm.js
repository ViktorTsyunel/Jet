Ext.define('AML.view.monitoring.alerts.Create.CreateAlertForm', {
    extend: 'Ext.window.Window',
    closeAction: 'hide',
    xtype: 'app-monitoring-create-createalertform',
    controller: 'createalertform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'createalertformvm'
    },
    requires: [
        'Ext.layout.container.Fit',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'AML.view.monitoring.alerts.Create.CreateAlertController',
        'AML.view.monitoring.alerts.Create.CreateAlertModel',
        'Ext.data.StoreManager'
    ],
    bind: {
        title: '{title}'
    },
    width: "80%",
    closable: true,
    hidden: true,
    itemId: 'createFormAlertItemId',
    listeners: {
        resize: function (wnd) {
            wnd.center();
        },
        close: function () {
            // Чистим метку о последнем открытом окне
            AML.app.window.clearLast();
        },
        show: function(window) {
            window.focus();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            formBind: true,
            readOnly: true,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Филиал Плательщика/Получателя',
                    reference: 'payerKGRKOCreateOES',
                    itemId: 'payerKGRKOCreateOES',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 20
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '0 10px 0 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 'opok_nb',
                                    flex: 1,
                                    labelWidth: 240,
                                    fieldLabel: 'Основной код вида операции (VO)',
                                    editable: false,
                                    queryMode: 'local',
                                    displayField: 'OPOK_NB',
                                    valueField: 'OPOK_NB',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                        '</tpl>'
                                    ),
                                    forceSelection: true,
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    store: 'OPOK',
                                    bind: {
                                        value: '{record.opok_nb}'
                                    },
                                    allowBlank: false
                                },
                                {width: 10},
                                {
                                    xtype: 'combobox',
                                    reference: 'tb_id',
                                    flex: 1,
                                    name: 'TB_CODE',
                                    labelWidth: 180,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="id &gt; 0">{id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Тербанк',
                                    displayField: 'label',
                                    valueField: 'id',
                                    triggerClear: true,
                                    store: 'TB',
                                    bind: {
                                        value: '{record.tb_id}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        scope: this,
                                        change: function (field, newValue, oldValue, eOpts) {
                                            var nextField = field.up('app-monitoring-create-createalertform').down('combobox[name=tbosboes]'),
                                                form = field.up('app-monitoring-create-createalertform'),
                                                vmMain = form.getViewModel();
                                            if (nextField) {
                                                if (newValue != oldValue) {
                                                    nextField.clearValue();
                                                    nextField.reset();
                                                    if (newValue === null || newValue === "") {
                                                        nextField.clearValue();
                                                        nextField.reset();
                                                        nextField.disable();
                                                    } else {
                                                        nextField.enable();
                                                        vmMain.set('focusEnable', 'true');
                                                        nextField.focus(false, 700);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '10px 10px 0 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 'add_opoks_tx',
                                    flex: 1,
                                    labelWidth: 240,
                                    fieldLabel: 'Доп. коды вида операции (DOP_V)',
                                    editable: false,
                                    queryMode: 'local',
                                    displayField: 'OPOK_NB',
                                    valueField: 'OPOK_NB',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                        '</tpl>'
                                    ),
                                    forceSelection: true,
                                    multiSelect: true,
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    allowBlank: true,
                                    bind: {
                                        value: {
                                            bindTo: '{DOP_V_CONVERSION}',
                                            deep: true
                                        },
                                        store: '{OPOK}'
                                    },
                                    listeners: {
                                        change: function (field, newValue, oldValue) {
                                            var dataDopV = [];
                                            if (field.lookupReferenceHolder().getView().isVisible()) {
                                                if (newValue && Ext.isArray(newValue) && newValue.length === 1 && newValue[0] === 0) {
                                                    field.setValue([0]);
                                                } else if (newValue && Ext.isArray(newValue)) {
                                                    Ext.Array.each(newValue, function (item) {
                                                        if (item !== 0) {
                                                            dataDopV.push(item);
                                                        }
                                                    });
                                                    field.setValue(dataDopV);
                                                }
                                            } else {
                                                field.clearValue();
                                                field.reset();
                                                field.clearInvalid();
                                            }
                                        },
                                        afterrender: function (field) {
                                            //field.focus();
                                        },
                                        validitychange: function (field, isValid) {
                                            if (!field.lookupReferenceHolder().getView().isVisible()) {
                                                field.reset();
                                                field.clearInvalid();
                                            }
                                        }
                                    }
                                },
                                {width: 10},
                                {
                                    xtype: 'combobox',
                                    reference: 'osb_id',
                                    name: 'tbosboes',
                                    flex: 1,
                                    labelWidth: 180,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{TB_NAME}</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Филиал',
                                    displayField: 'TB_NAME',
                                    valueField: 'OSB_CODE',
                                    triggerClear: true,
                                    forceSelection: true,
                                    disabled: true,
                                    bind: {
                                        store: '{oes321org}',
                                        value: '{record.osb_id}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        focus: function (combo) {
                                            var form = combo.up('app-monitoring-create-createalertform'),
                                                vmMain = form.getViewModel();
                                            if(vmMain.get('focusEnable')) {
                                                vmMain.set('focusEnable', null);
                                                combo.setValue(0);
                                            }
                                        },
                                        change: function(combo, newValue, oldValue) {
                                            var form = combo.up('app-monitoring-create-createalertform'),
                                                vmMain = form.getViewModel();
                                            if(!oldValue && (oldValue !== 0) && newValue === 0) {
                                                var storeOSB = combo.getStore(), rec;
                                                if(storeOSB){
                                                    rec = storeOSB.findRecord('_id_', newValue, 0, false, false, true);
                                                    if(rec){
                                                        vmMain.set('record.oes_321_org_id', rec.get('OES_321_ORG_ID'));
                                                    }
                                                }
                                            }
                                        },
                                        select: function (combo, record, index) {
                                            var form = combo.up('app-monitoring-create-createalertform'),
                                                vmMain = (form) ? form.getViewModel() : false;

                                            if(vmMain) {
                                                vmMain.set('record.oes_321_org_id', record.get('OES_321_ORG_ID'));
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '10px 10px 0 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    reference: 'due_dt',
                                    flex: 1,
                                    labelWidth: 240,
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    fieldLabel: 'Дата выявления (DATE_S)',
                                    value: new Date(),
                                    bind: {
                                        value: '{record.due_dt}'
                                    },
                                    maxValue:new Date(),
                                    allowBlank: false
                                },
                                {width: 10},
                                {
                                    xtype: 'combobox',
                                    reference: 'refCreateAlertOwnerId',
                                    flex: 1,
                                    labelWidth: 180,
                                    editable: true,
                                    typeAhead: true,
                                    typeAheadDelay: 500,
                                    forceSelection: true,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Ответственный',
                                    displayField: 'OWNER_DSPLY_NM',
                                    valueField: 'id',
                                    triggerClear: true,
                                    matchFieldWidth: false,
                                    bind: {
                                        store: '{Owners}',
                                        value: '{record.owner_id}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().currentUser = field.getStore().findRecord('IS_CURRENT', true);
                                            var is_supervis = (field.getStore().currentUser.get('IS_SUPERVISOR') === 'true');
                                            if (!is_supervis) {
                                                field.getStore().addFilter({
                                                    property: 'IS_CURRENT',
                                                    value: true
                                                });
                                            }
                                            field.getStore().addFilter([{property: 'IS_ALLOW_ALERT_EDITING', value: 'Y'}]);
                                            field.getPicker().refresh();
                                        },
                                        afterrender: function (field) {
                                            var form = field.up('app-monitoring-create-createalertform'),
                                                vmMain = form.getViewModel(),
                                                store = Ext.data.StoreManager.lookup('Owners');

                                            this.currentUser = store.findRecord('IS_CURRENT', true);
                                            // set default value owner
                                            if (field && this.currentUser) {
                                                field.setValue(this.currentUser.get('OWNER_SEQ_ID'));
                                            }
                                            // set default value currentUser to MainViewModel
                                            if (vmMain) {
                                                vmMain.setData({currentUser: this.currentUser});
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '10px 10px 10px 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'textareafield',
                                    reference: 'note_tx',
                                    width: '100%',
                                    labelWidth: 240,
                                    allowBlank: true,
                                    fieldLabel: 'Комментарий ОЭС',
                                    bind: {
                                        value: '{record.note_tx}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'combobox',
                            reference: 'osb_id222',
                            name: 'tbosboes222',
                            flex: 1,
                            labelWidth: 180,
                            editable: false,
                            queryMode: 'local',
                            fieldLabel: 'Филиал',
                            displayField: 'TB_NAME',
                            valueField: 'OSB_CODE',
                            triggerClear: true,
                            forceSelection: true,
                            hidden: true,
                            bind: {
                                store: '{oes321org2}',
                                value: '{record.osb_id2}'
                            },
                            allowBlank: true
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: '',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    collapsible: false,
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [
                        {
                            name: 'KGRKO_CHECKED_FL',
                            xtype: 'checkbox',
                            fieldLabel: 'Подлежит контролю в 2-х филиалах КГРКО (2 ОЭС)',
                            labelWidth: 300,
                            padding: '5 5 5 5',
                            flex: 0.5,
                            inputValue: '1',
                            uncheckedValue: '0',
                            allowBlank: true,
                            bind: {
                                value: '{record.double_kgrko_fl}'
                            },
                            listeners: {
                                change: function(checkbox, newValue) {
                                    var copyButton = checkbox.up('form').down('#buttonCopyKGRKOCreateOES'),
                                        recieverBlock = checkbox.up('form').down('#recieverKGRKOCreateOES'),
                                        mainBlock = checkbox.up('form').down('#payerKGRKOCreateOES'),
                                        titleMainBlock = Ext.getCmp(mainBlock.id + '-legendTitle').el.dom;

                                    if (newValue) {
                                        titleMainBlock.innerHTML = 'Филиал Плательщика';
                                        recieverBlock.show();
                                        recieverBlock.enable();
                                        copyButton.show();
                                    } else {
                                        titleMainBlock.innerHTML = 'Филиал Плательщика/Получателя';
                                        recieverBlock.hide();
                                        recieverBlock.disable();
                                        copyButton.hide();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            xtype: 'button',
                            reference: 'buttonCopyKGRKOCreateOES',
                            itemId: 'buttonCopyKGRKOCreateOES',
                            align: 'right',
                            margin: '7 0 7 0',
                            hidden: true,
                            text: 'Копировать',
                            handler: 'copyKGRKOPayerToReciever'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Филиал Получателя',
                    reference: 'recieverKGRKOCreateOES',
                    itemId: 'recieverKGRKOCreateOES',
                    hidden: true,
                    disabled: true,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 20
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '0 10px 0 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 'opok_nb2',
                                    flex: 1,
                                    labelWidth: 240,
                                    fieldLabel: 'Основной код вида операции (VO2)',
                                    editable: false,
                                    queryMode: 'local',
                                    displayField: 'OPOK_NB',
                                    valueField: 'OPOK_NB',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                        '</tpl>'
                                    ),
                                    forceSelection: true,
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    store: 'OPOK',
                                    bind: {
                                        value: '{record.opok_nb2}'
                                    },
                                    allowBlank: false
                                },
                                {width: 10},
                                {
                                    xtype: 'combobox',
                                    reference: 'tb_id2',
                                    flex: 1,
                                    name: 'TB_CODE2',
                                    labelWidth: 180,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="id &gt; 0">{id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Тербанк',
                                    displayField: 'label',
                                    valueField: 'id',
                                    triggerClear: true,
                                    store: 'TB',
                                    bind: {
                                        value: '{record.tb_id2}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        scope: this,
                                        change: function (field, newValue, oldValue) {
                                            var nextField = field.up('app-monitoring-create-createalertform').down('combobox[name=tbosboes2]'),
                                                form = field.up('app-monitoring-create-createalertform'),
                                                vmMain = form.getViewModel();
                                            if (nextField) {
                                                if (newValue !== oldValue) {
                                                    nextField.clearValue();
                                                    nextField.reset();
                                                    if (newValue === null || newValue === "") {
                                                        nextField.clearValue();
                                                        nextField.reset();
                                                        nextField.disable();
                                                    } else {
                                                        nextField.enable();
                                                        vmMain.set('focusEnable2', 'true');
                                                        nextField.focus(false, 700);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '10px 10px 0 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 'add_opoks_tx2',
                                    flex: 1,
                                    labelWidth: 240,
                                    fieldLabel: 'Доп. коды вида операции (DOP_V2)',
                                    editable: false,
                                    queryMode: 'local',
                                    displayField: 'OPOK_NB',
                                    valueField: 'OPOK_NB',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                        '</tpl>'
                                    ),
                                    forceSelection: true,
                                    multiSelect: true,
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    allowBlank: true,
                                    bind: {
                                        value: {
                                            bindTo: '{DOP_V_CONVERSION2}',
                                            deep: true
                                        },
                                        store: '{OPOK}'
                                    },
                                    listeners: {
                                        change: function (field, newValue, oldValue) {
                                            var dataDopV = [];
                                            if (field.lookupReferenceHolder().getView().isVisible()) {
                                                if (newValue && Ext.isArray(newValue) && newValue.length === 1 && newValue[0] === 0) {
                                                    field.setValue([0]);
                                                } else if (newValue && Ext.isArray(newValue)) {
                                                    Ext.Array.each(newValue, function (item) {
                                                        if (item !== 0) {
                                                            dataDopV.push(item);
                                                        }
                                                    });
                                                    field.setValue(dataDopV);
                                                }
                                            } else {
                                                field.clearValue();
                                                field.reset();
                                                field.clearInvalid();
                                            }
                                        },
                                        validitychange: function (field, isValid) {
                                            if (!field.lookupReferenceHolder().getView().isVisible()) {
                                                field.reset();
                                                field.clearInvalid();
                                            }
                                        }
                                    }
                                },
                                {width: 10},
                                {
                                    xtype: 'combobox',
                                    reference: 'osb_id2',
                                    name: 'tbosboes2',
                                    flex: 1,
                                    labelWidth: 180,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{TB_NAME}</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Филиал',
                                    displayField: 'TB_NAME',
                                    valueField: 'OSB_CODE',
                                    triggerClear: true,
                                    forceSelection: true,
                                    disabled: true,
                                    bind: {
                                        store: '{oes321org2}',
                                        value: '{record.osb_id2}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        focus: function (combo) {
                                            var form = combo.up('app-monitoring-create-createalertform'),
                                                vmMain = form.getViewModel();
                                            if(vmMain.get('focusEnable2')) {
                                                vmMain.set('focusEnable2', null);
                                                combo.setValue(0);
                                            }
                                        },
                                        change: function(combo, newValue, oldValue) {
                                            var form = combo.up('app-monitoring-create-createalertform'),
                                                vmMain = form.getViewModel();
                                            if(!oldValue && (oldValue !== 0) && newValue === 0) {
                                                var storeOSB = combo.getStore(), rec;
                                                if(storeOSB){
                                                    rec = storeOSB.findRecord('_id_', newValue, 0, false, false, true);
                                                    if(rec){
                                                        vmMain.set('record.oes_321_org_id2', rec.get('OES_321_ORG_ID'));
                                                    }
                                                }
                                            }
                                        },
                                        select: function (combo, record, index) {
                                            var form = combo.up('app-monitoring-create-createalertform'),
                                                vmMain = (form) ? form.getViewModel() : false;

                                            if(vmMain) {
                                                vmMain.set('record.oes_321_org_id2', record.get('OES_321_ORG_ID'));
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '10px 10px 0 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    reference: 'due_dt2',
                                    flex: 1,
                                    labelWidth: 240,
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    fieldLabel: 'Дата выявления (DATE_S2)',
                                    value: new Date(),
                                    bind: {
                                        value: '{record.due_dt2}'
                                    },
                                    maxValue:new Date(),
                                    allowBlank: false
                                },
                                {width: 10},
                                {
                                    xtype: 'combobox',
                                    reference: 'refCreateAlertOwnerId2',
                                    flex: 1,
                                    labelWidth: 180,
                                    editable: true,
                                    typeAhead: true,
                                    typeAheadDelay: 500,
                                    forceSelection: true,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Ответственный',
                                    displayField: 'OWNER_DSPLY_NM',
                                    valueField: 'id',
                                    triggerClear: true,
                                    bind: {
                                        store: '{Owners}',
                                        value: '{record.owner_id2}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().currentUser = field.getStore().findRecord('IS_CURRENT', true);
                                            var is_supervis = (field.getStore().currentUser.get('IS_SUPERVISOR') === 'true');
                                            if (!is_supervis) {
                                                field.getStore().addFilter({
                                                    property: 'IS_CURRENT',
                                                    value: true
                                                });
                                            }
                                            field.getStore().addFilter([{property: 'IS_ALLOW_ALERT_EDITING', value: 'Y'}]);
                                            field.getPicker().refresh();
                                        },
                                        afterrender: function (field) {
                                            var form = field.up('app-monitoring-create-createalertform'),
                                                vmMain = form.lookupReferenceHolder(true).getViewModel(),
                                                store = Ext.data.StoreManager.lookup('Owners');

                                            this.currentUser = store.findRecord('IS_CURRENT', true);
                                            // set default value owner
                                            if (field && this.currentUser) {
                                                field.setValue(this.currentUser.get('OWNER_SEQ_ID'));
                                            }
                                            // set default value currentUser to MainViewModel
                                            if (vmMain) {
                                                vmMain.setData({currentUser: this.currentUser});
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretchmax'
                            },
                            margin: '10px 10px 10px 10px',
                            defaults: {
                                border: 0,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'textareafield',
                                    reference: 'note_tx2',
                                    width: '100%',
                                    labelWidth: 240,
                                    allowBlank: true,
                                    fieldLabel: 'Комментарий ОЭС',
                                    bind: {
                                        value: '{record.note_tx2}'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'clearnigForm',
                    text: 'Очистить'
                },
                {
                    xtype: 'container',
                    flex: 1
                },
                {
                    itemId: 'createForm',
                    text: 'Создать'
                },
                {
                    itemId: 'cancellationForm',
                    text: 'Отмена'
                }
            ]
        }
    ]
});