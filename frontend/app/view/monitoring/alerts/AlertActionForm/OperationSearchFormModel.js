Ext.define('AML.view.monitoring.alerts.AlertAction.OperationSearchFormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alertactionoperationsearchformviewmodel',
    data: {
        title: 'Операции подлежат контролю по коду',
        selectedAlertsRb: true,
        record: {
            forBetweenModalComboOperSearch: 0,
            CMMNT_ID: null
        }
    },
    formulas: {

    },
    stores: {
        alertActionStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'alertCount'
                }
            }
        },
        alertCommentStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'AlertComment',
                    action: 'read'
                }
            }
        },

        forBetweenStore: {
            fields: ['id', 'all', 'label'],
            data: [
                {id: 0, all: '0 - в обоих филиалах', label: 'в обоих филиалах'},
                {id: 1, all: '1 - в филиале по дебету', label: 'в филиале по дебету'},
                {id: 2, all: '2 - в филиале по кредиту', label: 'в филиале по кредиту'}
            ]
        }
    }
});