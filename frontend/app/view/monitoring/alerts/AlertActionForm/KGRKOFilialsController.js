Ext.define('AML.view.monitoring.alerts.AlertActionForm.KGRKOFilialsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertactionkgrkofilials',

    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],

    config: {
        listen: {
            component: {
                '#execute': {
                    click: 'onClickExecute'
                },
                '#cancelForm': {
                    click: 'onClickCancelForm'
                }
            }
        }
    },

    init: function () {
        var vm = this.getViewModel();

        var tbid1 = this.getView().down('combobox[name=tbIDForPayerKGRKOFilials]'),
            tbid2 = this.getView().down('combobox[name=tbIDForPayerKGRKOFilials2]'),
            checkBox1 = vm.get('record.double_kgrko_fl'),
            checkBox2 = vm.get('record.double_kgrko_fl2');

        if (checkBox1) {
            tbid1.enable();
        } else {
            tbid1.disable();
        }

        if (checkBox2) {
            tbid2.enable();
        } else {
            tbid2.disable();
        }
    },

    onClickExecute: function () {
        var self = this,
            vm = self.getViewModel(),
            checkField1 = this.lookupReference('KGRKO_CHECKED_FL'),
            checkTB1 = this.lookupReference('tbIDForPayerKGRKOFilials'),
            checkOSB1 = this.lookupReference('tbOsbForPayerKGRKOFilials'),
            checkField2 = this.lookupReference('KGRKO_CHECKED_FL2'),
            checkTB2 = this.lookupReference('tbIDForPayerKGRKOFilials2'),
            checkOSB2 = this.lookupReference('tbOsbForPayerKGRKOFilials2'),
            checkField1V = (checkField1) ? checkField1.value : 0,
            checkField2V = (checkField2) ? checkField2.value : 0,
            comboConnect = this.lookupReference('comboOperationsConnectedTo'),
            oesOrgId = vm.get('record.oes_321_org_id'),
            oesOrgId2 = vm.get('record.oes_321_org_id2'),
            // alertIds = [],
            // oesIds = [],
            parentVM = (vm) ? vm.parentVM : false,
            parentView = (parentVM) ? parentVM.getView() : false,
            // selectedAlerts = (parentVM) ? parentVM.get('selectedAlert') : false,
            selectedAlerts,
            params,
            selectedGrid,
            selectedGridModel,
            selectedGridSelect,
            selectedAlertsCnt,
            refName = (parentView) ? parentView.getReference() : false,
            opCatCd, trxnSeqId;
        // type = AML.app.msg._getType('question'),
        // msgText;

        if (parentView) {
            selectedGrid = parentView.lookupReference('alertsOperSearchGrid') || parentView.lookupReference('alertsGrid');
        }
        selectedGridModel = (selectedGrid) ? selectedGrid.getSelectionModel() : false;
        selectedGridSelect = (selectedGridModel) ? selectedGridModel.getSelected() : false;
        selectedAlertsCnt = (selectedGridSelect) ? selectedGridSelect.length : false;

        if (selectedAlertsCnt === 0) {
            Ext.Msg.alert('', 'Не выбрано ни одной операции!');
            return;
        }

        if (selectedAlertsCnt > 1) {
            Ext.Msg.alert('', 'Выделено более одной операции!');
            return;
        }
        if (checkField1V){
            if (Ext.isEmpty(checkTB1.value) || Ext.isEmpty(checkOSB1.value)) {
                Ext.Msg.alert('', 'Заполните обязательные поля!');
                return;
            }
        }
        if (checkField2V){
            if (Ext.isEmpty(checkTB2.value) || Ext.isEmpty(checkOSB2.value)) {
                Ext.Msg.alert('', 'Заполните обязательные поля!');
                return;
            }
        }
        if (selectedAlertsCnt === 1) {
            if (refName === 'alertsList') {
                selectedAlerts = (parentVM) ? parentVM.get('selectedAlert') : false;
                opCatCd = (selectedAlerts) ? selectedAlerts.get('RV_OP_CAT_CD') : null;
                trxnSeqId = (selectedAlerts) ? selectedAlerts.get('RV_TRXN_SEQ_ID') : null;

                if (!checkField1V) {
                    oesOrgId = null;
                }
                if (!checkField2V) {
                    oesOrgId2 = null;
                }
                params = {
                    form: 'AlertAction',
                    action: 'setKGRKO',
                    op_cat_cd: opCatCd,
                    trxn_seq_id: trxnSeqId,
                    send_filial_fl: (checkField1V) ? 1 : 0,
                    send_oes_org_id: (oesOrgId) ? parseInt(oesOrgId, 10) : null,
                    rcv_filial_fl: (checkField2V) ? 1 : 0,
                    rcv_oes_org_id: (oesOrgId2) ? parseInt(oesOrgId2, 10) : null,
                    alert_party_fl: (comboConnect) ? comboConnect.getValue() : null
                };

                for (var mkey in params) {
                    if (Ext.isEmpty(params[mkey])) {
                        delete params[mkey];
                    }
                }

                // добавим loading обработки:
                if (!common.globalinit.loadingMaskCmp) {
                    common.globalinit.loadingMaskCmp = self.getView();
                }
                Ext.Ajax.request({
                    url: common.globalinit.ajaxUrl,
                    method: common.globalinit.ajaxMethod,
                    headers: common.globalinit.ajaxHeaders,
                    timeout: common.globalinit.ajaxTimeOut,
                    params: Ext.encode(params),
                    success: function (response) {
                        var responseObj = Ext.JSON.decode(response.responseText), idsArray;
                        if (responseObj.message) {
                            Ext.Msg.alert('Внимание!', responseObj.message);
                        }
                        if (responseObj.success) {

                            idsArray = responseObj.alertIds;
                            // переподкачка списка операций:
                            if(!Ext.isEmpty(idsArray)) {
                                self.getViewModel().parentVM.getView().controller.refreshAlertsList({
                                    alertIds: idsArray,
                                    addRecords: true
                                });
                            }
                        }
                        // close
                        self.getView().close();
                    }
                });
            }
            if (refName === 'main-search-operations-form') {
                selectedAlerts = (parentVM) ? parentVM.get('selectedOSAlert') : false;
                opCatCd = (selectedAlerts) ? selectedAlerts.get('OP_CAT_CD') : null;
                trxnSeqId = (selectedAlerts) ? selectedAlerts.get('TRXN_SEQ_ID') : null;
                if (!checkField1V) {
                    oesOrgId = null;
                }
                if (!checkField2V) {
                    oesOrgId2 = null;
                }
                params = {
                    form: 'TrxnAction',
                    action: 'setKGRKO',
                    op_cat_cd: opCatCd,
                    trxn_seq_id: trxnSeqId,
                    send_filial_fl: (checkField1V) ? 1 : 0,
                    send_oes_org_id: (oesOrgId) ? parseInt(oesOrgId, 10) : null,
                    rcv_filial_fl: (checkField2V) ? 1 : 0,
                    rcv_oes_org_id: (oesOrgId2) ? parseInt(oesOrgId2, 10) : null,
                    alert_party_fl: (comboConnect) ? comboConnect.getValue() : null
                };

                for (var lkey in params) {
                    if (Ext.isEmpty(params[lkey])) {
                        delete params[lkey];
                    }
                }

                // добавим loading обработки:
                if (!common.globalinit.loadingMaskCmp) {
                    common.globalinit.loadingMaskCmp = self.getView();
                }
                Ext.Ajax.request({
                    url: common.globalinit.ajaxUrl,
                    method: common.globalinit.ajaxMethod,
                    headers: common.globalinit.ajaxHeaders,
                    timeout: common.globalinit.ajaxTimeOut,
                    params: Ext.encode(params),
                    success: function (response) {
                        var responseObj = Ext.JSON.decode(response.responseText), idsArray;
                        if (responseObj.message) {
                            Ext.Msg.alert('Внимание!', responseObj.message);
                        }
                        if (responseObj.success) {
                            idsArray = responseObj.trxnIds;
                            // переподкачка списка операций:
                            if(!Ext.isEmpty(idsArray)) {
                                self.getViewModel().parentVM.getView().controller.refreshAlertsList({
                                    alertIds: idsArray,
                                    addRecords: true
                                });
                            }
                        }
                        // close
                        self.getView().close();
                    }
                });
            }
        }
    },

    onClickCancelForm: function () {
        this.getView().close();
    }
});
