Ext.define('AML.view.monitoring.alerts.AlertAction.KGRKOFilialsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alertactionkgrkofilialsviewmodel',
    data: {
        title: 'Указать филиалы КГРКО',
        selectedAlertsRb: true,
        record: {
            tbIDForPayerKGRKOFilials: null,
            tbOsbForPayerKGRKOFilials: null,
            double_kgrko_fl: 1,
            tbIDForPayerKGRKOFilials2: null,
            tbOsbForPayerKGRKOFilials2: null,
            double_kgrko_fl2: 1,
            comboOperationsConnectedTo: 3
        }
    },
    formulas: {
        filters: {
            bind: {
                tbIDForPayerKGRKOFilials: '{record.tbIDForPayerKGRKOFilials}'
            },
            get: function(data) {
                var store = this.getStore('oes321org'),
                    filters = [],
                    TBCODE = data.tbIDForPayerKGRKOFilials;

                if (TBCODE) {
                    filters.push({
                        property: 'TB_CODE',
                        value: TBCODE
                    });
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        },
        filters2: {
            bind: {
                TB_CODE2: '{record.tbIDForPayerKGRKOFilials2}'
            },
            get: function(data) {
                var store = this.getStore('oes321org2'),
                    filters = [],
                    TBCODE2 = data.TB_CODE2;

                if (TBCODE2) {
                    filters.push({
                        property: 'TB_CODE',
                        value: TBCODE2
                    });
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник Информация о КО
        oes321org: {
            model: 'AML.model.OES321Org',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OES_321_ORG_ID',
                    direction: 'ASC'
                }
            ],
            filters: '{filters}'
        },
        oes321org2: {
            model: 'AML.model.OES321Org',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OES_321_ORG_ID',
                    direction: 'ASC'
                }
            ],
            filters: '{filters2}'
        },

        operationsConnectedTo: {
            fields: ['id', 'all', 'label'],
            data: [
                {id: 1, all: '1 - к филиалу КГРКО плательщика', label: 'к филиалу КГРКО плательщика'},
                {id: 2, all: '2 - к филиалу КГРКО получателя', label: 'к филиалу КГРКО получателя'},
                {id: 3, all: '3 - определить автоматически', label: 'определить автоматически'}
            ]
        }
    }
});