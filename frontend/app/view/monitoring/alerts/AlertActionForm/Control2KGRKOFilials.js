Ext.define('AML.view.monitoring.alerts.AlertActionForm.Control2KGRKOFilials', {
    extend: 'Ext.window.Window',
    closeAction: 'hide',
    xtype: 'app-alerts-alertactionform-control2kgrkofilials',
    controller: 'alertactioncontrol2kgrkofilials',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertactioncontrol2kgrkofilialsviewmodel'
    },
    requires: [
        'Ext.layout.container.Fit',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'AML.view.monitoring.alerts.AlertActionForm.Control2KGRKOFilialsController',
        'AML.view.monitoring.alerts.AlertAction.Control2KGRKOFilialsModel',
        'Ext.data.StoreManager'
    ],
    bind: {
        title: '{title}'
    },
    margin: '0 0 15px 0',
    defaults: {
        padding: '5px 5px 0 0'
    },
    width: '70%',
    closable: true,
    hidden: true,
    listeners: {
        resize: function (wnd) {
            wnd.center();
        },
        close: function () {
            // Чистим метку о последнем открытом окне
            AML.app.window.clearLast();
        },
        show: function(window) {
            window.focus();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            formBind: true,
            readOnly: true,
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: '',
                            flex: 0.5,
                            border: 0,
                            reference: 'control2KGRKOCreateOES',
                            itemId: 'control2KGRKOCreateOES',
                            margin: '5px',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'right',
                                labelWidth: 200
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 'control2Base_VO',
                                    labelWidth: 200,
                                    labelAlign: 'right',
                                    fieldLabel: 'Код вида операции (VO)',
                                    tooltip: {
                                        text: 'Код вида операции.'
                                    },
                                    editable: true,
                                    queryMode: 'local',
                                    store: 'OPOK',
                                    displayField: 'OPOK_NB',
                                    valueField: 'OPOK_NB',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                        '</tpl>'
                                    ),
                                    forceSelection: true,
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{VO_CONVERSION}'
                                    },
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength: true,
                                    maxLength: 4,
                                    vtype: 'vRegexCountries',
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'control2Base_DOP_V',
                                    labelWidth: 200,
                                    labelAlign: 'right',
                                    fieldLabel: 'Доп. коды вида операции (DOP_V)',
                                    tooltip: {
                                        text: 'Код вида операции, отличающийся от вида операции, код которого указан в поле VO.'
                                    },
                                    editable: true,
                                    queryMode: 'local',
                                    displayField: 'OPOK_NB',
                                    valueField: 'OPOK_NB',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                        '</tpl>'
                                    ),
                                    forceSelection: true,
                                    multiSelect: true,
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: {
                                            bindTo: '{DOP_V_CONVERSION}',
                                            deep: true
                                        },
                                        store: '{OPOKaddEmpty}'
                                    },
                                    listeners: {
                                        change: function (field, newValue) {
                                            var dataDopV = [];
                                            if (newValue
                                                && Ext.isArray(newValue)
                                                && newValue.length === 1
                                                && newValue[0] === 0) {
                                                field.setValue([0]);
                                            } else if (newValue
                                                && Ext.isArray(newValue)) {
                                                Ext.Array.each(newValue, function (item) {
                                                    if (item !== 0) {
                                                        dataDopV.push(item);
                                                    }
                                                });
                                                field.setValue(dataDopV);
                                            }
                                        }
                                    },
                                    allowBlank: true,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: '',
                            margin: '5px',
                            reference: 'control2KGRKOCreateOES2',
                            itemId: 'control2KGRKOCreateOES2',
                            flex: 0.5,
                            border: 0,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'right',
                                labelWidth: 200
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    reference: 'control2Base_DATE_S',
                                    labelWidth: 200,
                                    labelAlign: 'right',
                                    fieldLabel: 'Дата выявления (DATE_S)',
                                    tooltip: {
                                        anchor: 'left',
                                        text: 'В данном поле указывается дата выявления операции.'
                                    },
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{DATE_S}'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'responsibleControl2KRGKO',
                                    store: 'Owners',
                                    // editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Ответственный',
                                    displayField: 'OWNER_DSPLY_NM',
                                    valueField: 'id',
                                    triggerClear: false,
                                    typeAhead: true,
                                    allowBlank: false,
                                    labelWidth: 200,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{OWNER_ID}'
                                    },
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().currentUser = field.getStore().findRecord('IS_CURRENT', true);
                                            var is_supervis = (field.getStore().currentUser.get('IS_SUPERVISOR') === 'true');
                                            if (!is_supervis) {
                                                field.getStore().addFilter({
                                                    property: 'IS_CURRENT',
                                                    value: true
                                                });
                                            }
                                            field.getStore().addFilter([{property: 'IS_ALLOW_ALERT_EDITING', value: 'Y'}]);
                                            field.getPicker().refresh();
                                        },
                                        afterrender: function (field) {
                                            var form = field.up('app-alerts-alertactionform-control2kgrkofilials'),
                                                vmMain = form.getViewModel(),
                                                store = Ext.data.StoreManager.lookup('Owners');

                                            this.currentUser = store.findRecord('IS_CURRENT', true);
                                            // set default value owner
                                            if (field && this.currentUser) {
                                                field.setValue(this.currentUser.get('OWNER_SEQ_ID'));
                                            }
                                            // set default value currentUser to MainViewModel
                                            if (vmMain) {
                                                vmMain.setData({currentUser: this.currentUser});
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Комментарий',
                    margin: '10 0 0 0',
                    padding: '10 20 10 20',
                    defaults: {
                        flex: 1,
                        width: '100%'
                    },
                    layout: {
                        type: 'vbox'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            name: 'commentStd',
                            reference: 'commentStd',
                            editable: false,
                            triggerClear: true,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{CMMNT_TX}</div>',
                                '</tpl>'
                            ),
                            displayField: 'CMMNT_TX',
                            valueField: 'CMMNT_ID',
                            fieldLabel: 'Стандартный комментарий',
                            labelWidth: 200,
                            bind: {
                                store: '{alertCommentStore}',
                                value: '{CMMNT_ID}'
                            }
                        },
                        {
                            xtype: 'textareafield',
                            grow      : true,
                            name      : 'comment',
                            reference: 'comment',
                            fieldLabel: 'Произвольный комментарий',
                            labelAlign: 'top',
                            listeners: {
                                afterrender: function (field) {
                                    field.focus();
                                }
                            }
                        }
                    ]
                }
            ],
            buttons: [
                {
                    xtype: 'container',
                    flex: 1
                },
                {
                    itemId: 'execute',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});