Ext.define('AML.view.monitoring.alerts.AlertActionForm.ContainerMTS003', {
    extend: 'Ext.container.Container',
    xtype: 'app-alerts-alertactionform-container-mts003',
    items: [
        {
            border: 0,
            bind: {
                hidden: '{!isMTS003}'
            },
            items: [
                {
                    xtype: 'checkboxfield',
                    name: 'setResponsible',
                    bind: {
                        boxLabel: '{MTS003_NM}',
                        value: '{responsibleEdit}'
                    }
                },
                {
                    xtype: 'panel',
                    collapsible: true,
                    collapsed: true,
                    border: 0,
                    header: false,
                    bind: {
                        collapsed: '{!responsibleEdit}'
                    },
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            reference: 'responsibleEditCombo',
                            store: 'Owners',
                            // editable: false,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                '</tpl>'
                            ),
                            fieldLabel: 'Ответственный',
                            displayField: 'OWNER_DSPLY_NM',
                            valueField: 'id',
                            triggerClear: false,
                            typeAhead: true,
                            typeAheadDelay: 500,
                            forceSelection: true,
                            matchFieldWidth: false,
                            // minChars: 1,
                            allowBlank: true,
                            padding: '0 0 0 30',
                            listeners: {
                                expand: function (field) {
                                    field.getStore().filter([{property: "IS_ALLOW_ALERT_EDITING", value: "Y"}])
                                    field.getPicker().refresh();
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
});