Ext.define('AML.view.monitoring.alerts.AlertActionForm.Form', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.form.field.Radio',
        'Ext.form.RadioGroup'
    ],
    xtype: 'app-alerts-alertactionform-form',
    controller: 'alertactionform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertactionformviewmodel'
    },
    bind: {
        title: '{title}'
    },
    width: 650,
    height: 570,
    scrollable: 'vertical',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'fieldset',
            title: 'Выбор области применения',
            defaults: {
                flex: 1
            },
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'fieldcontainer',
                    defaultType: 'radiofield',
                    layout: 'vbox',
                    items: [
                        {
                            xtype: 'radiogroup',
                            reference: 'selectAreaRadioGr',
                            vertical: true,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    bind: {
                                        boxLabel: 'Выделенные операции ({selectedAlertsCnt} шт.)',
                                        value: '{selectedAlertsRb}'
                                    },
                                    width: 300,
                                    name: 'rb',
                                    inputValue: '1'
                                },
                                {
                                    bind: {
                                        boxLabel: 'Удовлетворяющие фильтрам ({allAlertsCnt} шт.)',
                                        disabled: '{isAlertDetailsPanel}'
                                    },
                                    width: 300,
                                    name: 'rb',
                                    inputValue: '2'
                                }
                            ],
                            listeners: {
                                change: 'onChangeArea'
                            }
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    readOnly: true,
                    cls: 'dotted-field',
                    bind: {
                        value: '{searchRaw.scopeFilters}{searchRaw.dt}{searchRaw.ownerIds}{searchRaw.tbIds}{searchRaw.osbIds}{searchRaw.opok}{searchRaw.opokType}{searchRaw.operationNumber}{searchRaw.status}{searchRaw.amount}'
                    },
                    listeners: {
                        afterrender: 'afterRenderFiltersTip'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Редактирование операций',
                    reference: 'editActionFieldSet',
                    flex: 1.5,
                    margin: '0 10 0 0',
                    defaults: {
                        width: '100%',
                        xtype: 'container',
                        defaults: {
                            width: '100%'
                        }
                    },
                    items: []
                },
                {
                    xtype: 'fieldset',
                    title: 'Выбор действия',
                    flex: 1,
                    margin: 0,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype      : 'fieldcontainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [{
                                xtype: 'radiogroup',
                                vertical: true,
                                columns: 1,
                                defaults: {
                                    name: 'ActionSelect'
                                },
                                reference: 'selectActionRadioGr',
                                items: []
                            }]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Комментарий',
            margin: '10 0 0 0',
            padding: '10 20 10 20',
            defaults: {
                flex: 1,
                width: '100%'
            },
            layout: {
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'combobox',
                    name: 'commentStd',
                    reference: 'commentStd',
                    editable: false,
                    triggerClear: true,
                    queryMode: 'local',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{CMMNT_TX}</div>',
                        '</tpl>'
                    ),
                    displayField: 'CMMNT_TX',
                    valueField: 'CMMNT_ID',
                    fieldLabel: 'Стандартный комментарий',
                    width: '70%',
                    labelWidth: 80,
                    bind: {
                        store: '{alertCommentStore}',
                        value: '{record.CMMNT_ID}'
                    }
                },
                {
                    xtype: 'textareafield',
                    grow      : true,
                    name      : 'comment',
                    reference: 'comment',
                    fieldLabel: 'Произвольный комментарий',
                    labelAlign: 'top',
                    listeners: {
                        afterrender: function (field) {
                            field.focus();
                        }
                    }
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'execute',
            iconCls: 'icon-save-data',
            text: 'Выполнить'
        },
        {
            itemId: 'cancelForm',
            iconCls: 'icon-backtothe-primitive',
            text: 'Отмена'
        }
    ]
});