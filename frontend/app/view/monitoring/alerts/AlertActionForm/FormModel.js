Ext.define('AML.view.monitoring.alerts.AlertAction.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alertactionformviewmodel',
    data: {
        title: 'Действия над операциями',
        selectedAlertsRb: true
        //record: null
    },
    formulas: {

    },
    stores: {
        alertActionStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'alertCount'
                }
            }
        },
        alertCommentStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'AlertComment',
                    action: 'read'
                }
            }
        }
    }
});