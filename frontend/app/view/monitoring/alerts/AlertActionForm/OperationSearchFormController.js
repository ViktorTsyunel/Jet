Ext.define('AML.view.monitoring.alerts.AlertActionForm.OperationSearchFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertactionoperationsearchform',

    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],

    config: {
        listen: {
            component: {
                '#execute': {
                    click: 'onClickExecute'
                },
                '#cancelForm': {
                    click: 'onClickCancelForm'
                }
            }
        }
    },

    onClickExecute: function () {
        var self = this,
            vm = self.getViewModel(),
            alertsStore = vm.getParent().getStore('alertsStore'),
            vmParent = vm.getParent(),
            vmParentView = (vmParent) ? vmParent.getView() : null,
            selectRadioOperSearch = this.lookupReference('selectRadioOperSearch'),
            comment = this.lookupReference('commentOperSearch'),
            commentStd = this.lookupReference('commentStdOperSearch'),
            mainOpokCD = this.lookupReference('mainOpokCDOperSearch'),
            responsibleEditCombo = this.lookupReference('responsibleOperSearch'),
            forBetweenModalComboOperSearch = this.lookupReference('forBetweenModalComboOperSearch'),
            forBetweenModalCheckBoxOperSearch = this.lookupReference('forBetweenModalCheckBoxOperSearch'),
            forOperationModalCheckBoxOperSearch = this.lookupReference('forOperationModalCheckBoxOperSearch'),
            termEdit = this.lookupReference('termEditOperSearch'),
            isValid = true,
            selectedAlerts = vm.get('selectedAlerts'),
            alertIds = [],
            alertId,
            refleshFooterAct = false, fields, search;

        // очистить обязательность полей:
        mainOpokCD && mainOpokCD.clearInvalid();
        responsibleEditCombo && responsibleEditCombo.clearInvalid();
        termEdit && termEdit.clearInvalid();
        comment.clearInvalid();
        commentStd.clearInvalid();
        fields = {
            payerINNOperSearch: (vmParentView) ? vmParentView.lookupReference('payerINNOperSearch') : null,
            payerINNOperSearchAdd: (vmParentView) ? vmParentView.lookupReference('payerINNOperSearchAdd') : null,
            dtFromOperSearch: (vmParentView) ? vmParentView.lookupReference('dtFromOperSearch') : null,
            dtToOperSearch: (vmParentView) ? vmParentView.lookupReference('dtToOperSearch') : null,
            amountFromOperSearch: (vmParentView) ? vmParentView.lookupReference('amountFromOperSearch') : null,
            amountToOperSearch: (vmParentView) ? vmParentView.lookupReference('amountToOperSearch') : null,
            standartConditionOperSearch: (vmParentView) ? vmParentView.lookupReference('standartConditionOperSearch') : null,
            conditionOperSearch: (vmParentView) ? vmParentView.lookupReference('conditionOperSearch') : null,
            tbIdsOperSearch: (vmParentView) ? vmParentView.lookupReference('tbIdsOperSearch') : null,
            osbIdsOperSearch: (vmParentView) ? vmParentView.lookupReference('osbIdsOperSearch') : null,
            notCodeOperSearch: (vmParentView) ? vmParentView.lookupReference('notCodeOperSearch') : null,
            categoryOperSearch: (vmParentView) ? vmParentView.lookupReference('categoryOperSearch') : null,
            systemOperSearch: (vmParentView) ? vmParentView.lookupReference('systemOperSearch') : null,
            forBetweenOperSearch: (vmParentView) ? vmParentView.lookupReference('forBetweenOperSearch') : null,
            payerNameOperSearch: (vmParentView) ? vmParentView.lookupReference('payerNameOperSearch') : null,
            payerCountryOperSearch: (vmParentView) ? vmParentView.lookupReference('payerCountryOperSearch') : null,
            payerNameOperSearchAdd: (vmParentView) ? vmParentView.lookupReference('payerNameOperSearchAdd') : null,
            payerCountryOperSearchAdd: (vmParentView) ? vmParentView.lookupReference('payerCountryOperSearchAdd') : null,
            payerStandartConditions: (vmParentView) ? vmParentView.lookupReference('payerStandartConditions') : null,
            payerAccountNumber: (vmParentView) ? vmParentView.lookupReference('payerAccountNumber') : null,
            payerConditions: (vmParentView) ? vmParentView.lookupReference('payerConditions') : null,
            payerRegionSearch: (vmParentView) ? vmParentView.lookupReference('payerRegionSearch') : null,
            payerStandartConditionsAdd: (vmParentView) ? vmParentView.lookupReference('payerStandartConditionsAdd') : null,
            payerAccountNumberAdd: (vmParentView) ? vmParentView.lookupReference('payerAccountNumberAdd') : null,
            payerConditionsAdd: (vmParentView) ? vmParentView.lookupReference('payerConditionsAdd') : null,
            payerRegionSearchAdd: (vmParentView) ? vmParentView.lookupReference('payerRegionSearchAdd') : null,
            checkPayerCondition: (vmParent) ? vmParent.get('record.checkPayerCondition') : null
        };

        if (!mainOpokCD.getValue()) {
            mainOpokCD.markInvalid('нужно заполнить');
            isValid = false;
        }
        if (!responsibleEditCombo.getValue()) {
            responsibleEditCombo.markInvalid('нужно заполнить');
            isValid = false;
        }
        search = {
            dtFrom: (fields.dtFromOperSearch) ? fields.dtFromOperSearch.getValue() : null,
            dtTo: (fields.dtToOperSearch) ? fields.dtToOperSearch.getValue() : null,
            tbIds: (fields.tbIdsOperSearch) ? fields.tbIdsOperSearch.getValue() : null,
            osbIds: (fields.osbIdsOperSearch) ? fields.osbIdsOperSearch.getValue() : null,
            amountFrom: fields.amountFromOperSearch.getValue() ? parseFloat(fields.amountFromOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
            amountTo: fields.amountToOperSearch.getValue() ? parseFloat(fields.amountToOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
            lexPurposeCrit: (fields.standartConditionOperSearch) ? fields.standartConditionOperSearch.getValue() : null,
            lexPurposeAdd: (fields.conditionOperSearch) ? fields.conditionOperSearch.getValue() : null,
            notOpok: (fields.notCodeOperSearch) ? fields.notCodeOperSearch.getValue() : null,
            notOpokTp: (fields.forBetweenOperSearch.getValue()) ? parseInt(fields.forBetweenOperSearch.getValue(), 10) : null,
            opCat: (fields.categoryOperSearch) ? fields.categoryOperSearch.getValue() : null,
            srcSys: (fields.systemOperSearch) ? fields.systemOperSearch.getValue() : null,
            partyType: (fields.checkPayerCondition) ? fields.checkPayerCondition : null,
            custNm: (fields.payerNameOperSearch) ? fields.payerNameOperSearch.getValue() : null,
            custInn: (fields.payerINNOperSearch) ? fields.payerINNOperSearch.getValue() : null,
            custResident: (fields.payerCountryOperSearch) ? fields.payerCountryOperSearch.getValue() : null,
            acctCrit: (fields.payerStandartConditions) ? fields.payerStandartConditions.getValue() : null,
            acctAdd: (fields.payerConditions) ? fields.payerConditions.getValue() : null,
            acctNb: (fields.payerAccountNumber) ? fields.payerAccountNumber.getValue() : null,
            acctArea: (fields.payerRegionSearch) ? fields.payerRegionSearch.getValue() : null,
            cust2Nm: (fields.payerNameOperSearchAdd) ? fields.payerNameOperSearchAdd.getValue() : null,
            cust2Inn: (fields.payerINNOperSearchAdd) ? fields.payerINNOperSearchAdd.getValue() : null,
            cust2Resident: (fields.payerCountryOperSearchAdd) ? fields.payerCountryOperSearchAdd.getValue() : null,
            acct2Crit: (fields.payerStandartConditionsAdd) ? fields.payerStandartConditionsAdd.getValue() : null,
            acct2Add: (fields.payerConditionsAdd) ? fields.payerConditionsAdd.getValue() : null,
            acct2Nb: (fields.payerAccountNumberAdd) ? fields.payerAccountNumberAdd.getValue() : null,
            acct2Area: (fields.payerRegionSearchAdd) ? fields.payerRegionSearchAdd.getValue() : null,
            custWatchList: (vmParent) ? vmParent.get('record.oes321TypeSender') : null,
            cust2WatchList: (vmParent) ? vmParent.get('record.oes321TypeTaker') : null,
            custNotWatchListFl: (vmParent) ? +vmParent.get('record.excludeSenderInn') : null,
            cust2NotWatchListFl: (vmParent) ? +vmParent.get('record.excludeTakerInn') : null
        };

        for (var tkey in search) {
            if (Ext.isEmpty(search[tkey])) {
                delete search[tkey];
            }
        }

        if (isValid) {
            var params = {
                form: 'TrxnAction',
                //actvyTypeCds: actvyTypeCds,
                opokNb: mainOpokCD.getValue(),
                // addOpoks: additionalOpokCDs.getValue() && additionalOpokCDs.getValue().join(','),
                ownerId: responsibleEditCombo.getValue(),
                dateS: termEdit.getValue(),
                cmmntId: commentStd.getValue(),
                noteTx: comment.getValue(),
                inter_skip_fl: (forBetweenModalCheckBoxOperSearch.getValue()) ? 1 : 0,
                kgrko_party_cd: (forBetweenModalComboOperSearch) ? forBetweenModalComboOperSearch.getValue() : null,
                changeOwnerFl: (forOperationModalCheckBoxOperSearch.getValue()) ? 1 : 0
            };

            for (var key in params) {
                if (Ext.isEmpty(params[key]))
                    delete params[key];
            }
            switch (selectRadioOperSearch.getValue().rb) {
                case "1":
                    for (var i = 0; i < selectedAlerts.length; i++) {
                        alertIds.push(selectedAlerts[i].get('_id_'));
                    }
                    Ext.apply(params, {
                        action: 'execById',
                        trxnIds: alertIds
                    });
                    break;
                case "2":
                    Ext.apply(params, {
                        action: 'execByFilter',
                        filters: search,
                        where: vm.get('scopeFilters')
                    });
                    break;
            }

            // получние следующей записи за текущей в гриде
            if (selectedAlerts !== null) {

                if (!common.globalinit.loadingMaskCmp) {
                    common.globalinit.loadingMaskCmp = self.getView();
                }

                Ext.Ajax.request({
                    url: common.globalinit.ajaxUrl,
                    method: common.globalinit.ajaxMethod,
                    headers: common.globalinit.ajaxHeaders,
                    timeout: common.globalinit.ajaxTimeOut,
                    params: Ext.encode(params),
                    success: function (response) {
                        var responseObj = Ext.JSON.decode(response.responseText), changedRecord, trxnIds = [], oldIds = [], chooseNoNull;
                        if (responseObj.message)
                            Ext.Msg.alert('', responseObj.message);

                        if (responseObj.success && responseObj.trxnIds && responseObj.trxnIds.length > 0) {
                            if (responseObj.trxnIds.length > 0) {
                                // перезапись id
                                for (var i = 0; i < responseObj.trxnIds.length; i++) {
                                    //if (responseObj.trxnIds[i]['_id_'] !== responseObj.trxnIds[i]['client_id']) {
                                        trxnIds.push(responseObj.trxnIds[i]['_id_']);
                                        chooseNoNull = responseObj.trxnIds[i]['client_id'] ? responseObj.trxnIds[i]['client_id'] : null;
                                        oldIds.push(chooseNoNull);
                                        changedRecord = alertsStore.findRecord('_id_', responseObj.trxnIds[i]['_id_'], 0, false, false, true);
                                        if (changedRecord) {
                                            changedRecord.set('_id_', responseObj.trxnIds[i]['client_id']);
                                        }
                                    //}
                                }
                                vm.getParent().getView().getController().refreshAlertsList({
                                    alertIds: trxnIds,
                                    oldIds: oldIds,
                                    addRecords: true
                                });

                                self.getView().close();
                            }
                        }
                    }
                });
            }
        }
    },

    onClickCancelForm: function () {
        this.getView().close();
    },

    onChangeRadio: function (radiogroup, newValue) {
        var self = this,
            vm = self.getViewModel(),
            store = self.getStore('alertActionStore'),
            selectedAlerts = vm.get('selectedOSAlert'),
            alertIds = [],
            params;

        if(selectedAlerts.length > 1) {
            for (var i = 0; i < selectedAlerts.length; i++) {
                alertIds.push(selectedAlerts[i].get('_id_'));
            }
        } else {
            alertIds.push(selectedAlerts.get('_id_'));
        }

        if (newValue.rb === '1') {
            params = {
                form: 'TrxnList',
                action: 'readById',
                trxnIds: alertIds
            };
        } else if (newValue.rb === '2') {
            params = {
                form: 'TrxnList',
                action: 'count',
                filters: vm.get('search')
            };
        }

        for (var key in params) {
            if (Ext.isEmpty(params[key]))
                delete params[key];
        }

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        store.load({
            params: params,
            callback: self.generateFields,
            scope: self
        });
    },

    generateFields: function (dataSource, requestSource) {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertActionStore'),
            selectAreaRadioGr = self.lookupReference('selectRadioOperSearch'),
            reqResponse = (requestSource) ? requestSource.getResponse() : null,
            reqResponseText = (reqResponse) ? reqResponse.responseText : null,
            responseObj = (reqResponse) ? Ext.JSON.decode(reqResponseText) : null,
            totalCount = (responseObj) ? responseObj.count : null;

        if(selectAreaRadioGr && selectAreaRadioGr.getValue().rb === "2" && totalCount) {
            vm.set('allAlertsCnt', totalCount);
        }
    }
});
