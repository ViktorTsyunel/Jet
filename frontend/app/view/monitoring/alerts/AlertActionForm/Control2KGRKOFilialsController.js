Ext.define('AML.view.monitoring.alerts.AlertActionForm.Control2KGRKOFilialsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertactioncontrol2kgrkofilials',

    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],

    config: {
        listen: {
            component: {
                '#execute': {
                    click: 'onClickExecute'
                },
                '#cancelForm': {
                    click: 'onClickCancelForm'
                }
            }
        }
    },

    onClickExecute: function () {
        var self = this,
            vm = self.getViewModel(),
            opok_nb = this.lookupReference('control2Base_VO'),
            add_opoks_tx = this.lookupReference('control2Base_DOP_V'),
            owner_id = this.lookupReference('responsibleControl2KRGKO'),
            due_dt = this.lookupReference('control2Base_DATE_S'),
            cmmnt_id = this.lookupReference('commentStd'),
            note_tx = this.lookupReference('comment'),
            parentVM = (vm) ? vm.parentVM : false,
            first_review_id = (parentVM) ? parentVM.get('REVIEW_ID') : null,
            params, isInvalid = false;

        if (Ext.isEmpty(opok_nb.getValue())) {
            opok_nb.markInvalid('Заполните обязательные поля!');
            isInvalid = true;
        }
        if (Ext.isEmpty(due_dt.getValue())) {
            due_dt.markInvalid('Заполните обязательные поля!');
            isInvalid = true;
        }
        if (Ext.isEmpty(owner_id.getValue())) {
            owner_id.markInvalid('Заполните обязательные поля!');
            isInvalid = true;
        }
        if (isInvalid) {
            Ext.Msg.alert('', 'Заполните обязательные поля!');
            return;
        }

        params = {
            form: 'AlertAction',
            action: 'doubleOES',
            first_review_id: first_review_id,
            opok_nb: (opok_nb) ? opok_nb.getValue() : null,
            add_opoks_tx: (add_opoks_tx.getValue()) ? add_opoks_tx.getValue().toString() : null,
            owner_id: (owner_id) ? owner_id.getValue() : null,
            due_dt: (due_dt) ? due_dt.getValue() : null,
            cmmnt_id: (cmmnt_id) ? cmmnt_id.getValue() : null,
            note_tx: (note_tx) ? note_tx.getValue() : null
        };

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }
        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText), idsArray = [];
                if (responseObj.message) {
                    Ext.Msg.alert('Внимание!', responseObj.message);
                }
                if (responseObj.success) {
                    idsArray.push(responseObj.second_review_id, responseObj.first_review_id);
                    var parentModel = (self.getViewModel().getParent()) ? self.getViewModel().getParent() : false,
                        parentView = (parentModel) ? parentModel.getView() : false,
                        alertsList = (parentView) ? parentView.down('app-monitoring-alerts') : false,
                        alertsListVM = (alertsList) ? alertsList.getViewModel() : false,
                        alertsListStore = (alertsListVM) ? alertsListVM.getStore('alertsStore') : false;
                    if (alertsListStore) {
                        alertsListStore.load({
                            params: {
                                alertIds: idsArray
                            },
                            addRecords: true,
                            //scope: self,
                            callback: function() {
                                // прогружаем грид
                                AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                            }
                        });
                        var parentList = (self.getViewModel()) ? self.getViewModel().parentList : false;

                        parentList.set('newCountAlert', 1);
                    } else {
                        // прогружаем грид
                        AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                    }
                }
                // close
                self.getView().close();
            }
        });
    },

    onClickCancelForm: function () {
        this.getView().close();
    }
});
