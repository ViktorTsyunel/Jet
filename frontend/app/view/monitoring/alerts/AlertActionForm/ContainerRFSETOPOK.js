Ext.define('AML.view.monitoring.alerts.AlertActionForm.ContainerRFSETOPOK', {
    extend: 'Ext.container.Container',
    xtype: 'app-alerts-alertactionform-container-rfsetopok',
    items: [
        {
            border: 0,
            bind: {
                hidden: '{!isSETOPOK}'
            },
            items: [
                {
                    xtype: 'checkboxfield',
                    name: 'setOpok',
                    bind: {
                        boxLabel: '{SETOPOK_NM}',
                        value: '{codeEdit}'
                    }
                },
                {
                    xtype: 'panel',
                    collapsible: true,
                    collapsed: true,
                    border: 0,
                    header: false,
                    bind: {
                        collapsed: '{!codeEdit}'
                    },
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            store: 'OPOK',
                            name: 'mainOpokCD',
                            reference: 'mainOpokCD',
                            editable: false,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                '</tpl>'
                            ),
                            displayTpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">' +
                                '{OPOK_NB} - {OPOK_NM}' +
                                '</tpl>'
                            ),
                            matchFieldWidth: false,
                            displayField: 'OPOK_NB',
                            valueField: 'OPOK_NB',
                            triggerClear: true,
                            fieldLabel: 'Основной код',
                            padding: '0 0 0 30'
                        },
                        {
                            xtype: 'combobox',
                            store: 'OPOK',
                            name: 'additionalOpokCDs',
                            reference: 'additionalOpokCDs',
                            editable: false,
                            triggerClear: true,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                '</tpl>'
                            ),
                            matchFieldWidth: false,
                            displayField: 'OPOK_NB',
                            valueField: 'OPOK_NB',
                            fieldLabel: 'Доп. коды',
                            padding: '0 0 0 30',
                            multiSelect : true
                        }
                    ]
                }
            ]
        }
    ]
});