Ext.define('AML.view.monitoring.alerts.AlertActionForm.ContainerMTS018', {
    extend: 'Ext.container.Container',
    xtype: 'app-alerts-alertactionform-container-mts018',
    items: [
        {
            border: 0,
            bind: {
                hidden: '{!isMTS018}'
            },
            items: [
                {
                    xtype: 'checkboxfield',
                    name: 'setTerm',
                    bind: {
                        boxLabel: '{MTS018_NM}',
                        value: '{termEdit}'
                    }
                },
                {
                    xtype: 'panel',
                    collapsible: true,
                    collapsed: true,
                    border: 0,
                    header: false,
                    bind: {
                        collapsed: '{!termEdit}'
                    },
                    layout: {
                        type: 'vbox'
                    },
                    items: [
                        {
                            xtype: 'datefield',
                            reference: 'termEdit',
                            format : 'd.m.Y',
                            startDay: 1,
                            fieldLabel: 'Срок обработки',
                            padding: '0 0 0 30',
                            width: 200
                        }
                    ]
                }
            ]
        }
    ]
});