Ext.define('AML.view.monitoring.alerts.AlertActionForm.KGRKOFilials', {
    extend: 'Ext.window.Window',
    closeAction: 'hide',
    xtype: 'app-alerts-alertactionform-kgrkofilials',
    controller: 'alertactionkgrkofilials',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertactionkgrkofilialsviewmodel'
    },
    requires: [
        'Ext.layout.container.Fit',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'AML.view.monitoring.alerts.AlertActionForm.KGRKOFilialsController',
        'AML.view.monitoring.alerts.AlertAction.KGRKOFilialsModel',
        'Ext.data.StoreManager'
    ],
    bind: {
        title: '{title}'
    },
    margin: '0 0 15px 0',
    defaults: {
        padding: '5px 5px 0 0'
    },
    width: '70%',
    closable: true,
    hidden: true,
    listeners: {
        resize: function (wnd) {
            wnd.center();
        },
        close: function () {
            // Чистим метку о последнем открытом окне
            AML.app.window.clearLast();
        },
        show: function(window) {
            window.focus();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            formBind: true,
            readOnly: true,
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Плательщик',
                            flex: 0.5,
                            reference: 'payerKGRKOCreateOES',
                            itemId: 'payerKGRKOCreateOES',
                            margin: '5px',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'right',
                                labelWidth: 20
                            },
                            items: [
                                {
                                    name: 'KGRKO_CHECKED_FL',
                                    reference: 'KGRKO_CHECKED_FL',
                                    xtype: 'checkbox',
                                    fieldLabel: 'КО/Филиал КО',
                                    labelWidth: 150,
                                    // padding: '5 5 5 5',
                                    flex: 0.5,
                                    inputValue: '1',
                                    uncheckedValue: '0',
                                    allowBlank: true,
                                    bind: {
                                        value: '{record.double_kgrko_fl}'
                                    },
                                    listeners: {
                                        change: function(checkbox, newValue) {
                                            var formM = checkbox.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (formM) ? formM.getViewModel() : null,
                                                tbid = (formM) ? formM.down('combobox[name=tbIDForPayerKGRKOFilials]') : null,
                                                osbid = (formM) ? formM.down('combobox[name=tbOsbForPayerKGRKOFilials]') : null;
                                            if (tbid && osbid) {
                                                if (newValue) {
                                                    tbid.enable();
                                                    if(tbid && tbid.getValue()) {
                                                        osbid.enable();
                                                    }
                                                    if (vmMain.get('control2KGRKO')) {
                                                        vmMain.set('control2KGRKO', null);
                                                    }
                                                } else {
                                                    tbid.disable();
                                                    osbid.disable();
                                                    if (vmMain.get('control2KGRKO')) {
                                                        vmMain.set('control2KGRKO', null);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'tbIDForPayerKGRKOFilials',
                                    flex: 1,
                                    name: 'tbIDForPayerKGRKOFilials',
                                    labelWidth: 150,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="id &gt; 0">{id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Тербанк',
                                    displayField: 'label',
                                    valueField: 'id',
                                    triggerClear: true,
                                    store: 'TB',
                                    // matchFieldWidth: false,
                                    bind: {
                                        value: '{record.tbIDForPayerKGRKOFilials}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        scope: this,
                                        change: function (field, newValue, oldValue) {
                                            var formM = field.up('app-alerts-alertactionform-kgrkofilials'),
                                                nextField = (formM) ? formM.down('combobox[name=tbOsbForPayerKGRKOFilials]') : null,
                                                vmMain = (formM) ? formM.getViewModel() : null;
                                            if (nextField) {
                                                if (newValue !== oldValue) {
                                                    nextField.clearValue();
                                                    nextField.reset();
                                                    if (newValue === null || newValue === '') {
                                                        nextField.clearValue();
                                                        nextField.reset();
                                                        nextField.disable();
                                                    } else {
                                                        nextField.enable();
                                                        vmMain.set('focusEnable', 'true');
                                                        nextField.focus(false, 700);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'tbOsbForPayerKGRKOFilials',
                                    name: 'tbOsbForPayerKGRKOFilials',
                                    flex: 1,
                                    labelWidth: 150,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{TB_NAME}</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Филиал',
                                    displayField: 'TB_NAME',
                                    valueField: 'OSB_CODE',
                                    // matchFieldWidth: false,
                                    triggerClear: true,
                                    forceSelection: true,
                                    disabled: true,
                                    bind: {
                                        store: '{oes321org}',
                                        value: '{record.tbOsbForPayerKGRKOFilials}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        focus: function (combo) {
                                            var form = combo.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (form) ? form.getViewModel() : null;
                                            if(vmMain) {
                                                if(vmMain.get('focusEnable')) {
                                                    if (vmMain.get('control2KGRKO')) {
                                                        vmMain.set('control2KGRKO', null);
                                                    } else {
                                                        vmMain.set('focusEnable', null);
                                                        if(!combo.getValue()) {
                                                            combo.setValue(0);
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        change: function(combo, newValue, oldValue) {
                                            var form = combo.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (form) ? form.getViewModel() : null;
                                            if(!oldValue && (oldValue !== 0) && newValue === 0) {
                                                var storeOSB = combo.getStore(), rec;
                                                if(storeOSB){
                                                    var tbid = form.down('combobox[name=tbIDForPayerKGRKOFilials]');
                                                    rec = storeOSB.findRecord('TB_CODE', tbid.getValue(), 0, false, false, true);
                                                    if(vmMain && rec){
                                                        vmMain.set('record.oes_321_org_id', rec.get('OES_321_ORG_ID'));
                                                        vmMain.set('focusEnable', null);
                                                    }
                                                }
                                            }
                                        },
                                        select: function (combo, record) {
                                            var form = combo.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (form) ? form.getViewModel() : false;

                                            if(vmMain) {
                                                vmMain.set('record.oes_321_org_id', record.get('OES_321_ORG_ID'));
                                                vmMain.set('focusEnable', null);
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Получатель',
                            margin: '5px',
                            reference: 'recipientKGRKOCreateOES',
                            itemId: 'recipientKGRKOCreateOES',
                            flex: 0.5,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'right',
                                labelWidth: 20
                            },
                            items: [
                                {
                                    name: 'KGRKO_CHECKED_FL2',
                                    reference: 'KGRKO_CHECKED_FL2',
                                    xtype: 'checkbox',
                                    fieldLabel: 'КО/Филиал КО',
                                    labelWidth: 150,
                                    flex: 0.5,
                                    inputValue: '1',
                                    uncheckedValue: '0',
                                    allowBlank: true,
                                    bind: {
                                        value: '{record.double_kgrko_fl2}'
                                    },
                                    listeners: {
                                        change: function(checkbox, newValue) {
                                            var formM = checkbox.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (formM) ? formM.getViewModel() : null,
                                                tbid = (formM) ? formM.down('combobox[name=tbIDForPayerKGRKOFilials2]') : null,
                                                osbid = (formM) ? formM.down('combobox[name=tbOsbForPayerKGRKOFilials2]') : null;
                                            if (tbid && osbid) {
                                                if (newValue) {
                                                    tbid.enable();
                                                    if(tbid && tbid.getValue()) {
                                                        osbid.enable();
                                                    }
                                                    if (vmMain.get('control2KGRKO2')) {
                                                        vmMain.set('control2KGRKO2', null);
                                                    }
                                                } else {
                                                    tbid.disable();
                                                    osbid.disable();
                                                    if (vmMain.get('control2KGRKO2')) {
                                                        vmMain.set('control2KGRKO2', null);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'tbIDForPayerKGRKOFilials2',
                                    flex: 1,
                                    name: 'tbIDForPayerKGRKOFilials2',
                                    labelWidth: 150,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="id &gt; 0">{id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Тербанк',
                                    displayField: 'label',
                                    valueField: 'id',
                                    triggerClear: true,
                                    store: 'TB',
                                    // matchFieldWidth: false,
                                    bind: {
                                        value: '{record.tbIDForPayerKGRKOFilials2}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        scope: this,
                                        change: function (field, newValue, oldValue, eOpts) {
                                            var formM = field.up('app-alerts-alertactionform-kgrkofilials'),
                                                nextField = (formM) ? formM.down('combobox[name=tbOsbForPayerKGRKOFilials2]') : null,
                                                vmMain = (formM) ? formM.getViewModel() : null;
                                            if (nextField) {
                                                if (newValue !== oldValue) {
                                                    nextField.clearValue();
                                                    nextField.reset();
                                                    if (newValue === null || newValue === '') {
                                                        nextField.clearValue();
                                                        nextField.reset();
                                                        nextField.disable();
                                                    } else {
                                                        nextField.enable();
                                                        vmMain.set('focusEnable2', 'true');
                                                        nextField.focus(false, 700);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'tbOsbForPayerKGRKOFilials2',
                                    name: 'tbOsbForPayerKGRKOFilials2',
                                    flex: 1,
                                    labelWidth: 150,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{TB_NAME}</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Филиал',
                                    displayField: 'TB_NAME',
                                    valueField: 'OSB_CODE',
                                    // matchFieldWidth: false,
                                    triggerClear: true,
                                    forceSelection: true,
                                    disabled: true,
                                    bind: {
                                        store: '{oes321org2}',
                                        value: '{record.tbOsbForPayerKGRKOFilials2}'
                                    },
                                    allowBlank: false,
                                    listeners: {
                                        focus: function (combo) {
                                            var form = combo.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (form) ? form.getViewModel() : null;
                                            if(vmMain) {
                                                if(vmMain.get('focusEnable2')) {
                                                    if (vmMain.get('control2KGRKO2')) {
                                                        vmMain.set('control2KGRKO2', null);
                                                    } else {
                                                        vmMain.set('focusEnable2', null);
                                                        if(!combo.getValue()) {
                                                            combo.setValue(0);
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        change: function(combo, newValue, oldValue) {
                                            var form = combo.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (form) ? form.getViewModel() : null;
                                            if(!oldValue && (oldValue !== 0) && newValue === 0) {
                                                var storeOSB = combo.getStore(), rec;
                                                if(storeOSB){
                                                    var tbid = form.down('combobox[name=tbIDForPayerKGRKOFilials2]');
                                                    rec = storeOSB.findRecord('TB_CODE', tbid.getValue(), 0, false, false, true);
                                                    if(vmMain && rec){
                                                        vmMain.set('record.oes_321_org_id2', rec.get('OES_321_ORG_ID'));
                                                        vmMain.set('focusEnable2', null);
                                                    }
                                                }
                                            }
                                        },
                                        select: function (combo, record) {
                                            var form = combo.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmMain = (form) ? form.getViewModel() : false;

                                            if(vmMain) {
                                                vmMain.set('record.oes_321_org_id2', record.get('OES_321_ORG_ID'));
                                                vmMain.set('focusEnable2', null);
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Выявленные',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    margin: '5px',
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            reference: 'comboOperationsConnectedTo',
                            flex: 1,
                            name: 'comboOperationsConnectedTo',
                            labelWidth: 260,
                            editable: false,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                '<tpl if="id &gt; 0">{id} - {label}' +
                                '<tpl else>{label}</tpl>' +
                                '</div>',
                                '</tpl>'
                            ),
                            fieldLabel: 'Выявленные уже операции относятся',
                            displayField: 'label',
                            valueField: 'id',
                            triggerClear: true,
                            // matchFieldWidth: false,
                            bind: {
                                store: '{operationsConnectedTo}',
                                value: '{record.comboOperationsConnectedTo}'
                            },
                            allowBlank: false
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Аудит',
                    layout: {
                        type: 'hbox'
                    },
                    // padding: 10,
                    flex: 1,
                    margin: '5px',
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто создал',
                                    bind: {
                                        value: '{record.CREATED_BY}'
                                    }
                                },
                                {
                                    name: 'CREATED_DATE',
                                    fieldLabel: 'Когда создал',
                                    bind: {
                                        value: '{record.CREATED_DATE}'
                                        // value: '{record.CREATED_DATE:date("d.m.Y h:m:s")}'
                                    },
                                    listeners: {
                                        render: function (field) {
                                            var doc = field.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmDoc = (doc) ? doc.getViewModel() : false,
                                                docData = (vmDoc) ? vmDoc.getData() : false,
                                                rec = (docData) ? docData.record : false,
                                                fValue = (rec) ? rec['CREATED_DATE'] : false;
                                            if (typeof fValue === 'string') {
                                                var a = fValue.split(' '),
                                                    d = a[0].split('-'),
                                                    c = a[1].split(':'),
                                                    dd, mm, hh, minuts, mss;
                                                dd = (d[2].length === 1) ? ('0' + d[2]) : d[2];
                                                mm = (d[1].length === 1) ? ('0' + d[1]) : d[1];
                                                hh = (c[0].length === 1) ? ('0' + c[0]) : c[0];
                                                minuts = (c[1].length === 1) ? ('0' + c[1]) : c[1];
                                                mss = (c[2].length === 1) ? ('0' + c[2]) : c[2];

                                                field.setValue(dd + '.' + mm + '.' + d[0] + ' ' + hh + ':' + minuts + ':' + mss);
                                            }
                                        }
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто изменил',
                                    bind: {
                                        value: '{record.MODIFIED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда изменил',
                                    bind: {
                                        value: '{record.MODIFIED_DATE}'
                                    },
                                    listeners: {
                                        render: function (field) {
                                            var doc = field.up('app-alerts-alertactionform-kgrkofilials'),
                                                vmDoc = (doc) ? doc.getViewModel() : false,
                                                docData = (vmDoc) ? vmDoc.getData() : false,
                                                rec = (docData) ? docData.record : false,
                                                fValue = (rec) ? rec['MODIFIED_DATE'] : false;
                                            if (typeof fValue === 'string') {
                                                var a = fValue.split(' '),
                                                    d = a[0].split('-'),
                                                    c = a[1].split(':'),
                                                    dd, mm, hh, minuts, mss;
                                                dd = (d[2].length === 1) ? ('0' + d[2]) : d[2];
                                                mm = (d[1].length === 1) ? ('0' + d[1]) : d[1];
                                                hh = (c[0].length === 1) ? ('0' + c[0]) : c[0];
                                                minuts = (c[1].length === 1) ? ('0' + c[1]) : c[1];
                                                mss = (c[2].length === 1) ? ('0' + c[2]) : c[2];

                                                field.setValue(dd + '.' + mm + '.' + d[0] + ' ' + hh + ':' + minuts + ':' + mss);
                                            }
                                        }
                                    }
                                }

                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    xtype: 'container',
                    flex: 1
                },
                {
                    itemId: 'execute',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});