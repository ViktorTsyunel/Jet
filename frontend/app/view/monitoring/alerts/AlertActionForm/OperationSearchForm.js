Ext.define('AML.view.monitoring.alerts.AlertActionForm.OperationSearchForm', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.form.field.Radio',
        'Ext.form.RadioGroup'
    ],
    xtype: 'app-alerts-alertactionform-operationsearchform',
    controller: 'alertactionoperationsearchform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertactionoperationsearchformviewmodel'
    },
    bind: {
        title: '{title}'
    },
    width: 650,
    height: 570,
    scrollable: 'vertical',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'fieldset',
            title: 'Выбор области применения',
            defaults: {
                flex: 1
            },
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'fieldcontainer',
                    defaultType: 'radiofield',
                    layout: 'vbox',
                    items: [
                        {
                            xtype: 'radiogroup',
                            reference: 'selectRadioOperSearch',
                            vertical: true,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    bind: {
                                        boxLabel: 'Выделенные операции ({selectedAlertsCnt} шт.)',
                                        value: '{selectedAlertsRb}'
                                    },
                                    width: 300,
                                    name: 'rb',
                                    inputValue: '1'
                                },
                                {
                                    bind: {
                                        boxLabel: 'Удовлетворяющие фильтрам ({allAlertsCnt} шт.)',
                                        disabled: '{isAlertDetailsPanel}'
                                    },
                                    width: 300,
                                    name: 'rb',
                                    inputValue: '2'
                                }
                            ],
                            listeners: {
                                change: 'onChangeRadio'
                            }
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Операция',
                    reference: 'operationOperSearch',
                    flex: 1,
                    // margin: '0 10 0 0',
                    defaults: {
                        width: '100%',
                        xtype: 'container',
                        defaults: {
                            width: '100%',
                            labelAlign: 'left',
                            labelWidth: 165
                        }
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            store: 'OPOK',
                            name: 'mainOpokCDOperSearch',
                            reference: 'mainOpokCDOperSearch',
                            editable: false,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                '</tpl>'
                            ),
                            displayTpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">' +
                                '{OPOK_NB} - {OPOK_NM}' +
                                '</tpl>'
                            ),
                            matchFieldWidth: false,
                            displayField: 'OPOK_NB',
                            valueField: 'OPOK_NB',
                            triggerClear: true,
                            allowBlank: false,
                            fieldLabel: 'Код вида операции',
                            labelWidth: 165
                        },
                        {
                            xtype: 'datefield',
                            reference: 'termEditOperSearch',
                            format : 'd.m.Y',
                            startDay: 1,
                            fieldLabel: 'Дата выявления',
                            // padding: '0 0 0 30',
                            width: 300,
                            labelWidth: 165
                        },
                        {
                            xtype: 'combobox',
                            reference: 'responsibleOperSearch',
                            store: 'Owners',
                            // editable: false,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                '</tpl>'
                            ),
                            fieldLabel: 'Ответственный',
                            displayField: 'OWNER_DSPLY_NM',
                            valueField: 'id',
                            triggerClear: false,
                            typeAhead: true,
                            allowBlank: false,
                            matchFieldWidth: false,
                            labelWidth: 165,
                            listeners: {
                                expand: function (field) {
                                    field.getStore().filter([{property: "IS_ALLOW_ALERT_EDITING", value: "Y"}])
                                    field.getPicker().refresh();
                                }
                            }
                        },
                        {
                            xtype: 'checkboxfield',
                            boxLabel : 'Изменить ответственного для требующих обработки операций',
                            name: 'forOperationModalCheckBoxOperSearch',
                            reference: 'forOperationModalCheckBoxOperSearch',
                            inputValue: '1'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Межфилиальная операция',
                    reference: 'betweenOperSearch',
                    flex: 1,
                    // margin: '0 10 0 0',
                    defaults: {
                        width: '100%',
                        xtype: 'container',
                        defaults: {
                            width: '100%',
                            labelAlign: 'left',
                            labelWidth: 150
                        }
                    },
                    items: [
                        {
                            xtype: 'checkboxfield',
                            boxLabel : 'Пропустить(не обрабатывать)',
                            name: 'forBetweenModalCheckBoxOperSearch',
                            reference: 'forBetweenModalCheckBoxOperSearch',
                            inputValue: '1',
                            listeners: {
                                change: function(checkbox, newValue) {
                                    var dialogBox = (checkbox) ? checkbox.up('app-alerts-alertactionform-operationsearchform') : false,
                                        comboField = (dialogBox) ? dialogBox.lookupReference('forBetweenModalComboOperSearch') : false;
                                    if (comboField) {
                                        if (newValue) {
                                            comboField.disable();
                                        } else {
                                            comboField.enable();
                                        }
                                    }
                                }
                            }
                        },
                        {
                            name: 'forBetweenModalComboOperSearch',
                            reference: 'forBetweenModalComboOperSearch',
                            xtype: 'combobox',
                            editable: false,
                            queryMode: 'local',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{all}</div>',
                                '</tpl>'
                            ),
                            tooltip: {
                                text: 'Учитывается только для межфилиальных операций'
                            },
                            fieldLabel: 'Подлежит контролю',
                            labelWidth: 130,
                            displayField: 'label',
                            valueField: 'id',
                            defaultValue: 0,
                            triggerClear: true,
                            multiSelect: false,
                            bind: {
                                value: '{record.forBetweenModalComboOperSearch}',
                                store: '{forBetweenStore}'
                            },
                            flex: 1,
                            forceSelection: true,
                            matchFieldWidth: false,
                            // padding: '0 0 5 0',
                            listeners: {
                                expand: function (field) {
                                    field.getPicker().refresh();
                                },
                                change: function(c) {
                                    var tooltip = new Ext.ToolTip({
                                        target: c.getEl(),
                                        html: c.getRawValue()
                                    });
                                    return tooltip;
                                }
                            }
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Комментарий',
            margin: '10 0 0 0',
            padding: '10 20 10 20',
            defaults: {
                flex: 1,
                width: '100%'
            },
            layout: {
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'combobox',
                    name: 'commentStdOperSearch',
                    reference: 'commentStdOperSearch',
                    editable: false,
                    triggerClear: true,
                    queryMode: 'local',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{CMMNT_TX}</div>',
                        '</tpl>'
                    ),
                    displayField: 'CMMNT_TX',
                    valueField: 'CMMNT_ID',
                    fieldLabel: 'Стандартный комментарий',
                    flex: 1,
                    labelWidth: 165,
                    bind: {
                        store: '{alertCommentStore}',
                        value: '{record.CMMNT_ID}'
                    }
                },
                {
                    xtype: 'textareafield',
                    grow      : true,
                    name      : 'commentOperSearch',
                    reference: 'commentOperSearch',
                    fieldLabel: 'Произвольный комментарий',
                    labelAlign: 'top',
                    listeners: {
                        afterrender: function (field) {
                            field.focus();
                        }
                    }
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'execute',
            iconCls: 'icon-save-data',
            text: 'Выполнить'
        },
        {
            itemId: 'cancelForm',
            iconCls: 'icon-backtothe-primitive',
            text: 'Отмена'
        }
    ]
});