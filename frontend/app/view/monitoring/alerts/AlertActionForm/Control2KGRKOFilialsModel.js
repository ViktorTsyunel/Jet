Ext.define('AML.view.monitoring.alerts.AlertAction.Control2KGRKOFilialsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alertactioncontrol2kgrkofilialsviewmodel',
    data: {
        title: 'Подлежит контролю в 2-х филиалах КГРКО (2 ОЭС)',
        selectedAlertsRb: true,
        VO_CONVERSION: null,
        DOP_V_CONVERSION: null,
        DATE_S: null,
        OWNER_ID: null,
        CMMNT_ID: null
    },
    formulas: {

    },
    stores: {
        alertCommentStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'AlertComment',
                    action: 'read'
                }
            }
        },

        // OPOK
        OPOKaddEmpty: {
            type: 'chained',
            source: 'OPOKaddZero'
        }
    }
});