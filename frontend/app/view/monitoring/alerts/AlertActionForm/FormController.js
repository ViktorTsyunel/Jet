Ext.define('AML.view.monitoring.alerts.AlertActionForm.FormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertactionform',

    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],

    config: {
        listen: {
            component: {
                '#execute': {
                    click: 'onClickExecute'
                },
                '#cancelForm': {
                    click: 'onClickCancelForm'
                }
            }
        }
    },
    onClickExecute: function (window) {
        var self = this,
            vm = self.getViewModel(),
            alertsStore = vm.getParent().getStore('alertsStore'),
            store = self.getStore('alertActionStore'),
            selectActionRadioGr = this.lookupReference('selectActionRadioGr'),
            selectAreaRadioGr = this.lookupReference('selectAreaRadioGr'),
            comment = this.lookupReference('comment'),
            commentStd = this.lookupReference('commentStd'),
            mainOpokCD = this.lookupReference('mainOpokCD'),
            additionalOpokCDs = this.lookupReference('additionalOpokCDs'),
            responsibleEditCombo = this.lookupReference('responsibleEditCombo'),
            termEdit = this.lookupReference('termEdit'),
            needCmnt = false,
            isValid = true,
            isOpEditChecked = false,
            selectedAlerts = vm.get('selectedAlerts'),
            SETOPOK_CD = vm.get('SETOPOK_CD'),
            MTS003_CD = vm.get('MTS003_CD'),
            MTS018_CD = vm.get('MTS018_CD'),
            ActionSelect = selectActionRadioGr.getValue().ActionSelect,
            alertIds = [],
            actvyTypeCds = [],
            alertId,
            nextAlertId = 0,
            selectedAlertIndex = 0,
            nextSelectedAlertIndex = 0,
            refleshFooterAct = false,
            toggleBBarBtn = vm.getParent().getView().lookupReference('toggleBBarBtn'),
            monitoringAlertsTopbar = vm.getParent().getView().lookupReference('monitoringAlertsTopbar');

        // очистить обязательность полей:
        mainOpokCD && mainOpokCD.clearInvalid();
        responsibleEditCombo && responsibleEditCombo.clearInvalid();
        termEdit && termEdit.clearInvalid();
        comment.clearInvalid();
        commentStd.clearInvalid();

        // проверки выбранных чекбоксов:
        if (vm.get('codeEdit')) {
            if (store.findRecord('ACTVY_TYPE_CD', SETOPOK_CD).get('REQ_CMMNT_FL') === 'Y')
                needCmnt = true;
            isOpEditChecked = true;
            if (!mainOpokCD.getValue()) {
                mainOpokCD.markInvalid('нужно заполнить');
                isValid = false;
            }
            actvyTypeCds.push(SETOPOK_CD);
        }
        if (vm.get('responsibleEdit')) {
            if (store.findRecord('ACTVY_TYPE_CD', MTS003_CD).get('REQ_CMMNT_FL') === 'Y')
                needCmnt = true;
            isOpEditChecked = true;
            if (!responsibleEditCombo.getValue()) {
                responsibleEditCombo.markInvalid('нужно заполнить');
                isValid = false;
            }
            actvyTypeCds.push(MTS003_CD);
        }
        if (vm.get('termEdit')) {
            if (store.findRecord('ACTVY_TYPE_CD', MTS018_CD) && store.findRecord('ACTVY_TYPE_CD', MTS018_CD).get('REQ_CMMNT_FL') === 'Y')
                needCmnt = true;
            isOpEditChecked = true;
            if (termEdit && !termEdit.getValue()) {
                termEdit.markInvalid('нужно заполнить');
                isValid = false;
            }
            actvyTypeCds.push(MTS018_CD);
        }

        // проверка выбранного радиобаттона:
        if (ActionSelect) {
            if (store.findRecord('ACTVY_TYPE_CD', ActionSelect) && store.findRecord('ACTVY_TYPE_CD', ActionSelect).get('REQ_CMMNT_FL') === 'Y')
                needCmnt = true;
            if (!isOpEditChecked && ActionSelect === 'NONE') {
                Ext.Msg.alert('Внимание!', 'Не выбрано ни одного действия для выполнения');
                isValid = false;
            }
            if (ActionSelect !== 'NONE') {
                actvyTypeCds.push(ActionSelect);
            }
        }

        // Если был выбран хотябы 1 чекбокс или радиобаттон с параметром REQ_CMMNT_FL = Y,
        // то делаем проверку на заполненность одного из полей комментариев:
        if (needCmnt) {
            if (!commentStd.getValue() && !comment.getValue()) {
                commentStd.markInvalid('нужно заполнить');
                isValid = false;
            }
            if (!comment.getValue() && !commentStd.getValue()) {
                comment.markInvalid('нужно заполнить');
                isValid = false;
            }
        }
        if (ActionSelect === 'RF_EXPORT') {
            var type = AML.app.msg._getType('question');

            Ext.Msg.show({
                title: 'Внимание!',
                msg: 'Настоящим действием Вы подтверждаете принятие решения о направлении данных в Уполномоченный орган.',
                buttonText: {
                    yes: 'OK', no: 'Отмена'
                },
                icon: type.icon,
                buttons: Ext.Msg.YESNO,
                fn: function (answer) {
                    if (answer === 'yes') {
                        if (isValid) {
                            var valTermEdit = termEdit ? termEdit.getValue() : '',
                                params = {
                                    form: 'AlertAction',
                                    actvyTypeCds: actvyTypeCds,
                                    opok: vm.get('codeEdit') ? mainOpokCD.getValue() : '',
                                    addOpoks: vm.get('codeEdit') ? additionalOpokCDs.getValue() && additionalOpokCDs.getValue().join(',') : '',
                                    ownerId: vm.get('responsibleEdit') ? responsibleEditCombo.getValue() : '',
                                    dueDt:  vm.get('termEdit') ? valTermEdit : '',
                                    cmmntId: commentStd.getValue(),
                                    noteTx: comment.getValue()
                                };
                            for(var key in params)
                            {
                                if(Ext.isEmpty(params[key]))
                                    delete params[key];
                            }

                            switch (selectAreaRadioGr.getValue().rb) {
                                case "1":
                                    for (var i = 0; i < selectedAlerts.length; i++) {
                                        alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
                                    }
                                    Ext.apply(params, {
                                        action: 'execById',
                                        alertIds: alertIds
                                    });
                                    break;
                                case "2":
                                    Ext.apply(params, {
                                        action: 'execByFilter',
                                        filters: vm.get('search'),
                                        where: vm.get('scopeFilters')
                                    });
                                    break;
                            }

                            // получние следующей записи за текущей в гриде
                            if (selectedAlerts !== null) {
                                for (var i = 0; i < selectedAlerts.length; i++) {
                                    refleshFooterAct = true;
                                    alertId = selectedAlerts[i].get('REVIEW_ID');
                                    if (alertId !== null && alertId > 0) {
                                        if (alertsStore.data.items.length > 0) {
                                            Ext.Array.forEach(alertsStore.data.items, function (item, index) {
                                                if (selectedAlertIndex === 1) {
                                                    nextAlertId = parseInt(item.id, 10);
                                                    // выставляем индекс следущей записи с учетом сдвижки
                                                    nextSelectedAlertIndex = index - 1;
                                                    selectedAlertIndex = 2;
                                                }
                                                if (parseInt(item.id, 10) === alertId) {
                                                    selectedAlertIndex = 1;
                                                }
                                            })
                                        }
                                    }
                                }
                            }

                            // добавим loading обработки:
                            if(!common.globalinit.loadingMaskCmp) {
                                common.globalinit.loadingMaskCmp = self.getView();
                            }

                            Ext.Ajax.request({
                                url: common.globalinit.ajaxUrl,
                                method: common.globalinit.ajaxMethod,
                                headers: common.globalinit.ajaxHeaders,
                                timeout: common.globalinit.ajaxTimeOut,
                                params: Ext.encode(params),
                                success: function (response) {
                                    var responseObj = Ext.JSON.decode(response.responseText);
                                    if (responseObj.message)
                                        Ext.Msg.alert('', responseObj.message);

                                    if(responseObj.success && responseObj.alertIds) {
                                        // переподкачка списка операций:
                                        var options = {
                                            alertIds: responseObj.alertIds,
                                            filters: vm.get('search'),
                                            where: vm.get('scopeFilters'),
                                            addRecords: true,
                                            nextAlertId: nextAlertId,
                                            nextSelectedAlertIndex: nextSelectedAlertIndex,
                                            refleshFooterAct: refleshFooterAct,
                                            alertId: alertId
                                        };
                                        // если подвал открыт запоминаем активную запись
                                        if (toggleBBarBtn && toggleBBarBtn.pressed && monitoringAlertsTopbar) {
                                            monitoringAlertsTopbar.reestablishView = true;
                                        }
                                        if (params.action !== 'execByFilter') {
                                            vm.getParent().getView().getController().refreshAlertsList(options);
                                        }
                                        self.getView().close();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        } else {
            if (isValid) {
                var valTermEdit = termEdit ? termEdit.getValue() : '',
                    params = {
                        form: 'AlertAction',
                        actvyTypeCds: actvyTypeCds,
                        opok: vm.get('codeEdit') ? mainOpokCD.getValue() : '',
                        addOpoks: vm.get('codeEdit') ? additionalOpokCDs.getValue() && additionalOpokCDs.getValue().join(',') : '',
                        ownerId: vm.get('responsibleEdit') ? responsibleEditCombo.getValue() : '',
                        dueDt:  vm.get('termEdit') ? valTermEdit : '',
                        cmmntId: commentStd.getValue(),
                        noteTx: comment.getValue()
                    };
                for(var key in params)
                {
                    if(Ext.isEmpty(params[key]))
                        delete params[key];
                }

                switch (selectAreaRadioGr.getValue().rb) {
                    case "1":
                        for (var i = 0; i < selectedAlerts.length; i++) {
                            alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
                        }
                        Ext.apply(params, {
                            action: 'execById',
                            alertIds: alertIds
                        });
                        break;
                    case "2":
                        Ext.apply(params, {
                            action: 'execByFilter',
                            filters: vm.get('search'),
                            where: vm.get('scopeFilters')
                        });
                        break;
                }

                // получние следующей записи за текущей в гриде
                if (selectedAlerts !== null) {
                    for (var i = 0; i < selectedAlerts.length; i++) {
                        refleshFooterAct = true;
                        alertId = selectedAlerts[i].get('REVIEW_ID');
                        if (alertId !== null && alertId > 0) {
                            if (alertsStore.data.items.length > 0) {
                                Ext.Array.forEach(alertsStore.data.items, function (item, index) {
                                    if (selectedAlertIndex === 1) {
                                        nextAlertId = parseInt(item.id, 10);
                                        // выставляем индекс следущей записи с учетом сдвижки
                                        nextSelectedAlertIndex = index - 1;
                                        selectedAlertIndex = 2;
                                    }
                                    if (parseInt(item.id, 10) === alertId) {
                                        selectedAlertIndex = 1;
                                    }
                                })
                            }
                        }
                    }
                }

                // добавим loading обработки:
                if(!common.globalinit.loadingMaskCmp) {
                    common.globalinit.loadingMaskCmp = self.getView();
                }

                Ext.Ajax.request({
                    url: common.globalinit.ajaxUrl,
                    method: common.globalinit.ajaxMethod,
                    headers: common.globalinit.ajaxHeaders,
                    timeout: common.globalinit.ajaxTimeOut,
                    params: Ext.encode(params),
                    success: function (response) {
                        var responseObj = Ext.JSON.decode(response.responseText);
                        if (responseObj.message)
                            Ext.Msg.alert('', responseObj.message);

                        if(responseObj.success && responseObj.alertIds) {
                            // переподкачка списка операций:
                            var options = {
                                alertIds: responseObj.alertIds,
                                filters: vm.get('search'),
                                where: vm.get('scopeFilters'),
                                addRecords: true,
                                nextAlertId: nextAlertId,
                                nextSelectedAlertIndex: nextSelectedAlertIndex,
                                refleshFooterAct: refleshFooterAct,
                                alertId: alertId
                            };
                            // если подвал открыт запоминаем активную запись
                            if (toggleBBarBtn && toggleBBarBtn.pressed && monitoringAlertsTopbar) {
                                monitoringAlertsTopbar.reestablishView = true;
                            }
                            if (params.action !== 'execByFilter') {
                                vm.getParent().getView().getController().refreshAlertsList(options);
                            }
                            self.getView().close();
                        }
                    }
                });
            }
        }
    },

    onClickCancelForm: function () {
        this.getView().close();
    },

    onChangeArea: function (radiogroup, newValue) {
        var self = this,
            vm = self.getViewModel(),
            store = self.getStore('alertActionStore'),
            selectedAlerts = vm.get('selectedAlerts'),
            alertIds = [],
            params;

        for(var i=0;i < selectedAlerts.length;i++) {
            alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
        }

        if (newValue.rb === '1') {
            params = {
                form: 'AlertAction',
                action: 'readById',
                alertIds: alertIds
            };
        } else if (newValue.rb === '2') {
            params = {
                form: 'AlertAction',
                action: 'readByFilter',
                filters: vm.get('search'),
                where: vm.get('scopeFilters')
            };
        }

        for(var key in params)
        {
            if(Ext.isEmpty(params[key]))
                delete params[key];
        }

        vm.set({
            isSETOPOK: false,
            isMTS003: false,
            isMTS018: false
        });

        self.lookupReference('selectActionRadioGr').removeAll();
        self.lookupReference('editActionFieldSet').removeAll();

        store.load({
            params: params,
            callback: self.generateFields,
            scope: self
        });
    },

    generateFields: function () {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertActionStore'),
            selectAreaRadioGr = self.lookupReference('selectAreaRadioGr'),
            selectActionRadioGr = self.lookupReference('selectActionRadioGr'),
            editActionFieldSet = self.lookupReference('editActionFieldSet');

        if(selectAreaRadioGr && selectAreaRadioGr.getValue().rb === "2") {
            vm.set('allAlertsCnt', store.getTotalCount());
        }

        store.each(function (record) {
            var type = record.get('ACTVY_TYPE_CD');
            if(record.get('ALERT_EDIT_FL')==='Y') {
                switch (type)
                {
                    case 'RF_SETOPOK':
                        vm.set({
                            'isSETOPOK': true,
                            'SETOPOK_CD': type,
                            'SETOPOK_NM': record.get('ACTVY_TYPE_NM')
                        });
                        editActionFieldSet.add({xtype: 'app-alerts-alertactionform-container-rfsetopok'});
                        break;
                    case 'MTS003':
                        vm.set({
                            'isMTS003': true,
                            'MTS003_CD': type,
                            'MTS003_NM': record.get('ACTVY_TYPE_NM')
                        });
                        editActionFieldSet.add({xtype: 'app-alerts-alertactionform-container-mts003'});
                        break;
                    case 'MTS018':
                        vm.set({
                            'isMTS018': true,
                            'MTS018_CD': type,
                            'MTS018_NM': record.get('ACTVY_TYPE_NM')
                        });
                        editActionFieldSet.add({xtype: 'app-alerts-alertactionform-container-mts018'});
                        break;
                    default:
                        editActionFieldSet.add({
                            xtype: 'checkboxfield',
                            name: type,
                            boxLabel: record.get('ACTVY_TYPE_NM')
                        });
                        break;
                }
            }
            else {
                selectActionRadioGr.add({
                    boxLabel: record.get('ACTVY_TYPE_NM'),
                    inputValue: record.get('ACTVY_TYPE_CD'),
                    checked: (type === 'RF_CANCEL' && vm.get('isCancel'))
                });
            }
        });
        // особый пункт "Не выполнять действий" для "Выбор действия", выбранный по-умолчанию:
        if(selectActionRadioGr) {
            selectActionRadioGr.add({
                boxLabel: 'Не выполнять действий',
                inputValue: 'NONE',
                checked: (selectActionRadioGr.getValue().ActionSelect != 'RF_CANCEL' && vm.get('isCancel') || !vm.get('isCancel'))
            });
        }
    },

    afterRenderFiltersTip: function (comp) {
        Ext.create('Ext.tip.ToolTip', {
            target : comp.el,
            minWidth: 300,
            autoHide : false,
            tpl : [
                '<div>{scopeFilters}</div>',
                '<div>{dt}</div>',
                '<div>{ownerIds}</div>',
                '<div>{tbIds}</div>',
                '<div>{osbIds}</div>',
                '<div>{opok}</div>',
                '<div>{opokType}</div>',
                '<div>{operationNumber}</div>',
                '<div>{status}</div>',
                '<div>{amount}</div>'
            ],
            bind: {
                data: this.getViewModel().get('searchRaw')
            }
        });
    }
});
