Ext.define('AML.view.monitoring.alerts.OES321Details.PanelModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321detailspanelviewmodel',
    data: {
        modeName: 'Редактирование',
        activeTabName: 'Сведения об операции',
        detailsTable: {},
        OES321DetailsTable: {},
        OES_READONLY_FL_CONVERSION_MANUAL: false,
        flagSave: false,
        BATCH_DATE: null,
        RV_TRXN_SEQ_ID: true,
        nextPreviousAlert: {
            thisIndex: null,
            thisAlertId: null,
            thisOES321Id: null,
            nextAlertId: null,
            nextOES321Id: null,
            nextSelectedAlertIndex: null,
            previousAlertId: null,
            previousOES321Id: null,
            previousSelectedAlertIndex: null
        },
        goRows: false,
        fieldsMap: {}
    },
    formulas: {
        PRIM_CONCAT: {
            bind: {
                PRIM_1: '{OES321DetailsTable.PRIM_1}',
                PRIM_2: '{OES321DetailsTable.PRIM_2}'
            },
            get: function (data) {
                return data.PRIM_1 + (data.PRIM_2 != 0 ? data.PRIM_2 : '');
            },
            set: function (value) {
                if(value.length <= 254) {
                    this.set('OES321DetailsTable.PRIM_1', value);
                    this.set('OES321DetailsTable.PRIM_2', '0');
                } else {
                    this.set('OES321DetailsTable.PRIM_1', value.substr(0, 254));
                    this.set('OES321DetailsTable.PRIM_2', value.substr(254));
                }
            }
        },
        DESCR_CONCAT: {
            bind: {
                DESCR_1: '{OES321DetailsTable.DESCR_1}',
                DESCR_2: '{OES321DetailsTable.DESCR_2}'
            },
            get: function (data) {
                return data.DESCR_1 + (data.DESCR_2 != 0 ? data.DESCR_2 : '');
            },
            set: function (value) {
                if(value.length <= 254) {
                    this.set('OES321DetailsTable.DESCR_1', value);
                    this.set('OES321DetailsTable.DESCR_2', '0');
                } else {
                    this.set('OES321DetailsTable.DESCR_1', value.substr(0, 254));
                    this.set('OES321DetailsTable.DESCR_2', value.substr(254));
                }
            }
        },
        OES_READONLY_FL_CONVERSION: {
            bind: {
                OES_READONLY_FL: '{OES321DetailsTable.OES_READONLY_FL}',
                OES_READONLY_FL_CONVERSION_MANUAL: '{OES_READONLY_FL_CONVERSION_MANUAL}',
                OES_READONLY_ALERTS: '{Alerts_READONLY}',
                OES_READONLY_STATUS: '{OES321DetailsTable.CHECK_STATUS}',
                ALLOW_ALERT_EDITING: '{detailsTable.ALLOW_ALERT_EDITING}'
            },
            get: function (dataObject) {
                var alertReadOnly = dataObject.ALLOW_ALERT_EDITING === 0,
                    vmParent = this.getParent(),
                    viewVM = (vmParent) ? vmParent.getView() : false,
                    refView = (viewVM) ? viewVM.reference : false;
                return (dataObject.OES_READONLY_FL && dataObject.OES_READONLY_FL === 'Y') ||
                    dataObject.OES_READONLY_FL_CONVERSION_MANUAL || dataObject.OES_READONLY_ALERTS ||
                    dataObject.OES_READONLY_STATUS === 'S' || alertReadOnly || refView === 'main-search-operations-form';
            }
        },
        OES_READONLY_HISTORY_ALERT: {
            bind: {
                HISTORY_ALERT: '{historyAlert}'
            },
            get: function (dataObject) {
                // var vmParent = this.getParent(),
                //     viewVM = (vmParent) ? vmParent.getView() : false,
                //     refView = (viewVM) ? viewVM.reference : false;
                return dataObject.HISTORY_ALERT;
            }
        },
        STATUS_ALERT_PANEL: {
            bind: {
                value: '{OES321DetailsTable.CHECK_STATUS}'
            },
            get: function (data) {
                var ret = {};
                switch (data.value) {
                    case 'W':
                        ret.background = 'yellow';
                        ret.color = 'black';
                        ret.fontWeight = 'bold';
                        ret.value = 'Есть предупреждения';
                        break;
                    case 'E':
                        ret.background = 'red';
                        ret.color = 'black';
                        ret.fontWeight = 'bold';
                        ret.value = 'Требует доработки';
                    break;
                    case 'O':
                        ret.background = 'green';
                        ret.color = 'black';
                        ret.fontWeight = 'bold';
                        ret.value = 'Контроль пройден';
                        break;
                    case 'NULL':
                        ret.color = 'black';
                        ret.fontWeight = 'bold';
                        ret.value = 'Сообщение не проверялось';
                        break;
                    default:
                        ret.color = 'black';
                        ret.fontWeight = 'bold';
                        ret.value = 'Сообщение не проверялось';
                        break;
                }

                return ret;
            }
        },
        RVTRXNSEQID: {
            bind: {
                RV_TRXN_SEQ_ID: '{RV_TRXN_SEQ_ID}'
            },
            get: function (value) {
                var vmAlerts = this.getParent(),
                    alertId = this.get('alertId'),
                    newRvTrxnSeqIDs = (vmAlerts) ? vmAlerts.get('newRvTrxnSeqIDs') : false;
                if (alertId && newRvTrxnSeqIDs && newRvTrxnSeqIDs.length > 0) {
                    return (Ext.Array.indexOf(newRvTrxnSeqIDs, alertId) !== -1) ? false : true;
                }
                return value;
            }
        },
        CHECK_DT: {
            bind: {
                CHECK_DT: '{OES321DetailsTable.CHECK_DT}'
            },
            get: function (value) {
                return Ext.Date.format(value.CHECK_DT, 'd.m.Y H:i:s');
            }
        },
        ICON_CLS_STATUS: {
            bind: {
                value: '{OES321DetailsTable.CHECK_STATUS}'
            },
            get: function (data) {
                var iconCls =  '';
                switch (data.value) {
                    case 'W':
                        iconCls = 'icon-error-button';
                        //'Есть предупреждения';
                        break;
                    case 'E':
                        iconCls = 'x-form-invalid-icon-default';
                        //'Требует доработки';
                        break;
                    case 'O':
                        iconCls = 'icon-cute-ball-go';
                        //'Контроль пройден';
                        break;
                    case 'NULL':
                        iconCls = '';
                        //'Сообщение не проверялось';
                        break;
                    default:
                        iconCls = '';
                        break;
                }
                return iconCls  ;
            }
        }
    },
    stores: {
        // поле Признак владельца банковской карты
        CARDBHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'без БК'},
                {id: 1, text: 'клиент Банка'},
                {id: 2, text: 'не клиент Банка'},
                {id: 3, text: 'иное'}
            ]
        },
        // поле Признак участника
        PRHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'участник является ФЛ или ЮЛ, имеющим регистрацию или место жительства в государстве, не участвующем в международном сотрудничестве в сфере ПОД/ФТ'},
                {id: 1, text: 'участник является ФЛ или ЮЛ, имеющим место нахождения в государстве, не участвующем в международном сотрудничестве в сфере ПОД/ФТ'},
                {id: 2, text: 'если участник операции является лицом, владеющим счетом в банке, зарегистрированном в государстве, не участвующем в международном сотрудничестве в сфере ПОД/ФТ'}
            ]
        },
        // поле Признак выгодоприобретателя
        VPHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'нет выгодоприобретателя'},
                {id: 1, text: 'идентификация завершена'},
                {id: 2, text: 'идентификация не завершена'}
            ]
        },
        // поле Тип сообщения
        ACTIONHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 1, text: 'добавление новой записи'},
                {id: 2, text: 'исправление записи'},
                {id: 3, text: 'запрос замены записи'},
                {id: 4, text: 'запрос удаления записи'}
            ]
        },
        // поле Тип участника для Вкладок плательщик, представитель плательщика, представитель получателя, получатель, от имени и по поручению
        TUHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'участник отсутствует'},
                {id: 1, text: 'ЮЛ'},
                {id: 2, text: 'ФЛ'},
                {id: 3, text: 'ИП'},
                {id: 4, text: 'Участник не определен'}
            ]
        },
        // информация об ОЭС
        OES321Details: {
            model: 'AML.model.OES321Details',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'OES321Details',
                    action: 'readByOesId'
                }
            }
        },
        // Справочник Результат контроля
        OESCheckStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'OESCheck',
                    action: 'read',
                    formCd: '321'
                }
            }
        },
        // Справочник Стран
        CountriesStore: {
            type: 'chained',
            source: 'Coutries'
        }
    }
});