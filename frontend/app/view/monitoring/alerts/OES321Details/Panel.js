Ext.define('AML.view.monitoring.alerts.OES321Details.Panel', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-alerts-oes321detailspanel',
    itemId: 'appAlertsOES321detailsPanel',
    requires: [
        'AML.view.monitoring.alerts.OES321Details.Tabs.Payer',
        'AML.view.monitoring.alerts.OES321Details.Tabs.PayerController',
        'AML.view.monitoring.alerts.OES321Details.Tabs.PayerModel',
        'Ext.tip.ToolTip'
    ],
    controller: 'oes321detailspanel',
    viewModel: {
        type: 'oes321detailspanelviewmodel'
    },
    bind: {
        title: '{modeName} ОЭС №{oes321Id}. {activeTabName}'
    },
    // reference: 'oes321DetailsPanel',
    region: 'east',
    width: '100%',
    header: true,
    scrollable: true,
    split: false,
    cls: 'app-alert-oes321detailspanel',
    bodyStyle: {
        padding: '0 20px 0 10px'
    },
    tools: [
        {
            type: 'close',
            tooltip: 'Закрыть (Alt + з)',
            tabIndex: 0,
            callback: 'closePanel'
        }
    ],
    initComponent: function () {
        var parentCtrl;
        if (this.up('app-monitoring-alerts')) {
            parentCtrl = this.up('app-monitoring-alerts').getController();
        } else if (this.up('main-search-operations-form')) {
            parentCtrl = this.up('main-search-operations-form').getController();
        }

        this.tbar = [
            {
                xtype: 'buttongroup',
                flex: 1,
                layout: {
                    type: 'hbox'
                },
                defaults: {
                    margin: '1px 3px 1px 3px'
                },
                items: [
                    {
                        xtype: 'button',
                        text: '<span style="text-decoration:underline;">Н</span>а отправку',
                        tooltip: 'На отправку (Alt + н)',
                        iconCls: 'icon-document-to-send',
                        handler: 'toSendAction',
                        scope: parentCtrl,
                        bind: {
                            hidden: '{OES_READONLY_FL_CONVERSION}'
                        }
                    },
                    {
                        xtype: 'button',
                        text: '<span style="text-decoration:underline;">Д</span>ействия',
                        tooltip: 'Действия (Alt + д)',
                        iconCls: 'icon-gear',
                        handler: 'editAction',
                        scope: parentCtrl,
                        bind: {
                            hidden: '{OES_READONLY_FL_CONVERSION}'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Дополнительно',
                        bind: {
                            hidden: '{OES_READONLY_FL_CONVERSION}'
                        },
                        iconCls: 'icon-big-plus',
                        menu:{
                            items:[
                                {
                                    text: 'Создать копию ОЭС',
                                    tooltip: 'Создать копию ОЭС',
                                    bind: {
                                        hidden: '{OES_READONLY_FL_CONVERSION}'
                                    },
                                    //scope: parentCtrl,
                                    iconCls: 'icon-applications-stack',
                                    handler: 'copyOpenedOES'
                                },
                                {
                                    text: 'Пр<span style="text-decoration:underline;">и</span>крепить файл',
                                    tooltip: 'Прикрепить файл(Alt + и)',
                                    bind: {
                                        hidden: '{OES_READONLY_FL_CONVERSION}'
                                    },
                                    scope: parentCtrl,
                                    iconCls: 'icon-document-plus',
                                    handler: 'attachAction'
                                }
                            ]
                        }
                    },
                    {
                        xtype: 'button',
                        text: '<span style="text-decoration:underline;">Э</span>кспорт',
                        tooltip: 'Экспорт (Alt + э)',
                        iconCls: 'icon-export',
                        handler: 'exportAction',
                        scope: parentCtrl,
                        bind: {
                            hidden: '{OES_READONLY_FL_CONVERSION}'
                        }
                    },
                    {
                        xtype: 'tbfill'
                    },
                    {
                        xtype: 'button',
                        text: '',
                        tooltip: 'За<span style="text-decoration:underline;">м</span>етки (Alt + м)',
                        iconCls: 'icon-notebook-pencil',
                        handler: 'editNoteAction',
                        bind: {
                            hidden: '{OES_READONLY_HISTORY_ALERT}'
                            // hidden: '{!detailsTable.ALLOW_ALERT_EDITING}'
                        }
                    },
                    {
                        xtype: 'button',
                        text: '',
                        tooltip: '<span style="text-decoration:underline;">В</span>ложения (Alt + в)',
                        iconCls: 'icon-paper-clip',
                        handler: 'editAttachAction',
                        bind: {
                            hidden: '{OES_READONLY_HISTORY_ALERT}'
                            // hidden: '{!detailsTable.ALLOW_ALERT_EDITING}'
                        }
                    },
                    {
                        xtype: 'button',
                        text: '',
                        tooltip: 'И<span style="text-decoration:underline;">с</span>тория изменений (Alt + с)',
                        iconCls: 'icon-document-list',
                        handler: 'showHistoryAction',
                        bind: {
                            hidden: '{OES_READONLY_HISTORY_ALERT}'
                            // hidden: '{!detailsTable.ALLOW_ALERT_EDITING}'
                        }
                    },
                    {
                        xtype: 'button',
                        text: '',
                        tooltip: 'История изм<span style="text-decoration:underline;">е</span>нений ОЭС (Alt + е)',
                        iconCls: 'icon-document-history',
                        handler: 'showHistoryLogAction',
                        bind: {
                            hidden: '{OES_READONLY_HISTORY_ALERT}'
                            // hidden: '{!detailsTable.ALLOW_ALERT_EDITING}'
                        }
                    }
                ]
            }
        ];

        this.callParent(arguments);
    },
    items: [
        {
            title: 'Общие сведения. Операция № ',
            xtype: 'panel',
            margin: '5px 0 0 0',
            layout: 'hbox',
            border: 0,
            bodyStyle: {
                padding: '5px 0 0 0'
            },
            defaults: {
                border: 0
            },
            collapsed: true,
            collapsible: true,
            bind: {
                title: {
                    text:'Общие сведения. Операция № {OES321DetailsTable.REVIEW_ID}',
                    flex: 0
                }
            },
            header: {
                items:[
                    {
                        xtype:'container',
                        width: 15
                    },
                    {
                        xtype: 'buttongroup',
                        layout: {
                            type: 'hbox',
                            align: 'left'
                        },
                        defaults: {
                            margin: '1px 1px 1px 1px'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: '',
                                border: 1,
                                iconCls: 'icon-button-page-previous-alert',
                                tooltip: 'Предыдущий ОЭС',
                                handler: 'loadPagePreviousAlert',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                xtype: 'button',
                                text: '',
                                border: 1,
                                iconCls: 'icon-button-page-next-alert',
                                tooltip: 'Следующий ОЭС',
                                handler: 'loadPageNextAlert',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            }
                        ]
                    },
                    {
                        xtype:'container',
                        flex: 1
                    },
                    {
                        xtype: 'textfield',
                        reference: 'controlResultField',
                        readOnly: true,
                        inputWrapCls: 'border-disable',
                        labelWidth: 120,
                        width: 320,
                        labelAlign: 'right',
                        fieldLabel: '<span style="text-decoration:underline;">Р</span>езультат контроля',
                        tooltip: {
                            text: ''
                        },
                        bind: {
                            statusAlertPanel: '{STATUS_ALERT_PANEL}'
                        },
                        setStatusAlertPanel: function(opts) {
                            var cmp = this;
                            if (opts.background) {
                                cmp.inputEl.dom.style.background = opts.background;
                            }
                            if (opts.color) {
                                cmp.inputEl.dom.style.color = opts.color;
                            }
                            if (opts.fontWeight) {
                                cmp.inputEl.dom.style.fontWeight = opts.fontWeight;
                            }
                            if (opts.value) {
                                cmp.inputEl.dom.value = opts.value;
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        margin: '0 10px 0 5px',
                        tooltip: 'Результат контроля (Alt + р)',
                        text: '',
                        border: 1,
                        bind:{
                            iconCls: '{ICON_CLS_STATUS}'
                        },
                        handler: 'openControlResult'
                    },
                    {
                        xtype: 'textfield',
                        readOnly: true,
                        inputWrapCls: 'border-disable bold-text',
                        bind: {
                            value: '{CHECK_DT}'
                        }
                    }
                ]
            },
            items: [
                {
                    xtype: 'panel',
                    flex: 1,
                    bind: {
                        data: '{detailsTable}'
                    },
                    tpl: [
                        '<table class="details-table">',
                        '<tr>',
                        '<td>Номер операции {REVIEW_ID}</td>',
                        '<td style="text-align: right;">Ответственный {RV_OWNER_ID}</td>',
                        '</tr>',
                        '<tr>',
                        '<td>Срок обработки {RV_DUE_DT:date("d.m.Y")}</td>',
                        '<td style="text-align: right;">Статус <b>{RV_STATUS_NM}</b></td>',
                        '</tr>',
                        '</table>'
                    ],
                    listeners: {
                        afterrender: function (cmp) {
                            cmp.body.addCls('border-right-1');
                        }
                    }
                },
                {
                    xtype: 'panel',
                    defaults: {
                        flex: 1
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                pack: 'center'
                            },
                            padding: 5,
                            items: [
                                {
                                    xtype: 'textfield',
                                    readOnly: true,
                                    inputWrapCls: 'border-disable',
                                    labelWidth: 200,
                                    labelAlign: 'right',
                                    fieldLabel: 'Номер ОЭС (NUMB_P)',
                                    tooltip: {
                                        title: 'Номер ОЭС (NUMB_P): 1',
                                        text: 'Порядковый номер сведений формируется автоматически в порядке возрастания, начиная с номера «1» в течение календарного года представления сведений.<br>Для операций исправления, запроса замены и удаления записи порядковый номер является номером ОПОК, сведения о которой были представлены в ФСФМ ранее.'
                                    },
                                    bind: {
                                        value: '{OES321DetailsTable.NUMB_P}'
                                    }
                                },
                                {xtype: 'container', width: 30},
                                {
                                    xtype: 'combobox',
                                    readOnly: true,
                                    inputWrapCls: 'border-disable',
                                    labelWidth: 160,
                                    labelAlign: 'right',
                                    fieldLabel: 'Тип сообщения (ACTION)',
                                    tooltip: {
                                        title: 'Тип сообщения (ACTION): 3',
                                        text: 'Тип операции.<br>1 – добавление новой записи;<br>2 – исправление записи;<br>3 – запрос замены записи;<br>4 – запрос удаления записи.'
                                    },
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{id} - {text}',
                                        '</tpl>'
                                    ],
                                    displayField: 'text',
                                    valueField: 'id',
                                    bind: {
                                        store: '{ACTIONHardStore}',
                                        value: '{OES321DetailsTable.ACTION}'
                                    },
                                    forceSelection: true
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                pack: 'center'
                            },
                            padding: 5,
                            items: [
                                {
                                    xtype: 'datefield',
                                    readOnly: true,
                                    inputWrapCls: 'border-disable',
                                    labelWidth: 200,
                                    labelAlign: 'right',
                                    fieldLabel: 'Дата представления (DATE_P)',
                                    tooltip: {
                                        title: 'Дата представления (DATE_P): 2',
                                        text: 'Дата представления сведения.<br>Для операций исправления, запроса замены и удаления записи данная дата является датой представления сведений, которые были переданы в ФСФМ ранее.'
                                    },
                                    bind: {
                                        value: '{OES321DetailsTable.DATE_P}'
                                    }
                                },
                                {xtype: 'container', width: 10}
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'tabpanel',
            //reference: 'oes321DetailsTabPanel',
            name: 'oes321DetailsTabPanel',
            margin: '5px 0 0 0',
            listeners: {
                boxready: 'detailsTabPanelReadyHandler',
                tabchange: 'detailsTabPanelChangeHandler'
            }
        }
    ],
    buttons: [
        {
            text: '<span style="text-decoration:underline;">П</span>рименить',
            itemId: 'buttonApplyOES321Panel',
            handler: 'applyChanges',
            //tooltip: 'Применить (Alt + п)',
            bind: {
                hidden: '{OES_READONLY_FL_CONVERSION}'
            },
            listeners: {
                afterrender: function(c){
                    Ext.create('Ext.tip.ToolTip',{
                        target:  c.getEl(),//'buttonApplyOES321Panel',
                        html: 'Применить (Alt + п)',
                        //anchor: 'top',
                        showDelay: 3000
                    });
                }
            }
        },
        {
            text: '<span style="text-decoration:underline;">З</span>акрыть',
            itemId: 'buttonCloseOES321Panel',
            handler: 'closePanel',
            //tooltip: 'Закрыть (Alt + з)',
            listeners: {
                afterrender: function(c) {
                    Ext.create('Ext.tip.ToolTip',{
                        target: c.getEl(), //'buttonCloseOES321Panel',
                        html: 'Закрыть (Alt + з)',
                        //anchor: 'top',
                        showDelay: 3000
                    });
                }
            }
        }
    ]
});