Ext.define('AML.view.settings.InspectionResultsForm', {
    extend: 'Ext.window.Window',
	xtype: 'app-settings-inspectionresults-form',
	controller: 'inspectionresultsform',
	iconCls: 'icon-report',
	viewModel: {
		type: 'inspectionresultsformviewmodel'
    },
	bind: {
		title: '{title}'
	},
    width: Ext.getBody().getViewSize().width*0.8,
    minHeight: 120,
    maxHeight: Ext.getBody().getViewSize().height*0.8,
	closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
	items: [
        {
            xtype: 'gridpanel',
            bind: {
                store: '{OESCheckStore}'
            },
            columns: [
                {
                    text: 'Наименование поля',
                    tooltip: 'Наименование поля',
                    stateId: 'FIELD_NM',
                    dataIndex: 'FIELD_NM',
                    minWidth: 50,
                    tdCls: 'v-align-middle',
                    flex: 0.4,
                    cellWrap: true
                },
                {
                    text: 'Код поля',
                    tooltip: 'Код поля',
                    stateId: 'FIELD_CD',
                    dataIndex: 'FIELD_CD',
                    minWidth: 50,
                    tdCls: 'v-align-middle',
                    flex: 0.1,
                    sortable: false,
                    xtype: 'templatecolumn',
                    tpl: ['<span style="text-decoration: underline; cursor: pointer;">{FIELD_CD}</span>'],
                    listeners: {
                        click: 'clickGoError'
                    }
                },
                {
                    text: 'Сообщение',
                    tooltip: 'Сообщение',
                    stateId: 'MSG_TX',
                    dataIndex: 'MSG_TX',
                    minWidth: 50,
                    tdCls: 'v-align-middle',
                    flex: 1,
                    cellWrap: true
                },
                {
                    text: 'Критичность',
                    tooltip: 'Критичность',
                    stateId: 'CRITICAL_FL',
                    dataIndex: 'CRITICAL_FL',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        switch (value) {
                            case 'Y':
                                return 'ОШИБКА';
                                break;
                            default:
                                return 'ПРЕДУПРЕЖДЕНИЕ';
                                break;
                        }
                    },
                    minWidth: 50,
                    tdCls: 'v-align-middle',
                    flex: 0.1
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Отмена',
            handler: 'cancelForm'
        }
    ]
});