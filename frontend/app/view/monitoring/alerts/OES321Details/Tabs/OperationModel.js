Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.OperationModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321detailsoperationtabvm',
    data: {
        title: 'Сведения об операции',
        tabVM: 'RF_OES_321'
    },
    formulas: {
        DOP_V_CONVERSION: {
            bind: {
                DOP_V: '{OES321DetailsTable.DOP_V}'
            },
            get: function (dataObject) {
                var dataDopV = dataObject.DOP_V.split(','),
                    newDataDopV = [];
                if (dataDopV && Ext.isArray(dataDopV) && dataDopV.length === 1 && dataDopV[0] === '0') {
                    return dataDopV;
                } else if (dataDopV && Ext.isArray(dataDopV)) {
                    Ext.Array.each(dataDopV, function (item) {
                        if (item !== '0') {
                            newDataDopV.push(item);
                        }
                    });
                }
                return newDataDopV;
            },
            set: function (value) {
                if (value && value !== '') {
                    var dataDopV = [];
                    if (value && Ext.isArray(value) && value.length === 1 && value[0] === 0) {
                        this.set('OES321DetailsTable.DOP_V', '0');
                    } else if (value && Ext.isArray(value)) {
                        Ext.Array.each(value, function (item) {
                            if (item !== 0) {
                                dataDopV.push(item);
                            }
                        });
                        this.set('OES321DetailsTable.DOP_V', dataDopV.join(','));
                    }
                }
            }
        },
        CUR_CODE_CONVERSION: {
            bind: {
                CUR_CODE: '{OES321DetailsTable.CURREN}'
            },
            get: function (dataObject) {
                return dataObject.CUR_CODE;
            },
            set: function (value) {
                if (value && value !== '') {
                    this.set('OES321DetailsTable.CURREN', value);
                }
            }
        },
        CURREN_CON_CONVERSION: {
            bind: {
                CUR_CODE: '{OES321DetailsTable.CURREN_CON}'
            },
            get: function (dataObject) {
                return dataObject.CUR_CODE;
            },
            set: function (value) {
                if (value && value !== '') {
                    this.set('OES321DetailsTable.CURREN_CON', value);
                }
            }
        },
        VO_CONVERSION: {
            bind: {
                VO: '{OES321DetailsTable.VO}'
            },
            get: function (dataObject) {
                return dataObject.VO;
            },
            set: function (value) {
                if (value && value !== '') {
                    this.set('OES321DetailsTable.VO', value);
                }
            }
        }
    },
    stores: {
        // Территориальный банк
        TBStore: {
            type: 'chained',
            source: 'TBFULL'
        },
        // Cправочник валют
        CURStore: {
            type: 'chained',
            source: 'CUR'
        },
        // Cправочник валют
        CURStoreCon: {
            type: 'chained',
            source: 'CUR'
        },
        // Cправочник драг. металлов
        CURStoreMetal: {
            type: 'chained',
            source: 'CUR'
        },
        // поле Признак Банка плательщика
        BPAYERHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'не клиент Банка, предоставляющего сведения об операции'},
                {id: 1, text: 'плательщик на обслуживании в Банке, предоставляющем сведения об операции'},
                {id: 2, text: 'плательщик является Банком, предоставляющим сведения об операции'}
            ]
        },
        // поле Признак Банка получателя
        BRECIPHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'не клиент Банка, предоставляющего сведения об операции'},
                {id: 1, text: 'получатель – клиент Банка, предоставляющего сведения об операции'},
                {id: 2, text: 'получатель – Банк, предоставляющий сведения'}
            ]
        },
        // поле Операция совершается по поручению и от имени
        PARTHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'нет'},
                {id: 1, text: 'плательщик действует от имени и по поручению 3-его лица'},
                {id: 2, text: 'получатель действует от имени и по поручению 3-его лица'}
            ]
        },
        // поле Признак операции, связанной с финансированием терроризма
        TERRORHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'операция не связана с финансированием терроризма'},
                {id: 1, text: 'Приостановление операции с ДС или иным имуществом'},
                {id: 2, text: 'Совершение операции по зачислению ДС, поступивших на счет ФЛ или ЮЛ'}
            ]
        },
        // поле Объект сообщения
        PRIZSDHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'операция совершается с денежными средствами'},
                {id: 1, text: 'операция совершается с иным имуществом'}
            ]
        },
        // OPOK
        OPOKaddEmpty: {
            type: 'chained',
            source: 'OPOKaddZero'
        }
    }
});