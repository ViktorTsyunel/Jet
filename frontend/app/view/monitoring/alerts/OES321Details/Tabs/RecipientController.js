Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.RecipientController', {
    extend: 'AML.view.monitoring.alerts.OES321Details.PanelController',
    alias: 'controller.oes321detailsrecipienttab',

    init: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            OES321PartyStore = vm.getStore('OES321PartyStore');

        // добавим loading обработку:
        if(!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = vm.getParent().getParent().getView();
        }

        // загрузка информации о получателе:
        OES321PartyStore.load({
            params: {
                form: 'OES321Party',
                action: 'read',
                oes321Id: vm.get('oes321Id'),
                block_nb: 3
            },
            callback: function(records, operation, success) {
                vm.set('OES321PartyData', this.getAt(0));
            }
        });
    },

    checkFieldServerError: function (container, value, eOpts) {
        var ref = container.getReference(),
            fieldsCombo = [
                't3_KODCR_2',
                't3_KODCN_2',
                't3_KODCN_B_2',
                't3_KODCN_R_2'
            ],
            fieldsComboSlice = [
                't3_KODCR',
                't3_KODCN',
                't3_KODCN_B',
                't3_KODCN_R'
            ];
        if (value) {
            var self = this,
                objMsg = self.getViewModel().getParent().getView().lookupController().objMsg;
            if (ref && Ext.Array.indexOf(fieldsCombo, ref) !== -1) {
                ref = ref.slice(0,-2);
            }
            if (objMsg && ref && objMsg[ref]) {
                container.markInvalid(objMsg[ref]);
                if (ref && Ext.Array.indexOf(fieldsComboSlice, ref) !== -1) {
                    var field = container.previousSibling('combobox').getActionEl();
                    field.el.addCls(Ext.baseCSSPrefix + 'form-invalid-field');
                    field.el.addCls(Ext.baseCSSPrefix + 'form-invalid-field-default');
                    field.up().el.addCls(Ext.baseCSSPrefix + 'form-text-wrap-invalid');
                }
            }
        }
    }
});
