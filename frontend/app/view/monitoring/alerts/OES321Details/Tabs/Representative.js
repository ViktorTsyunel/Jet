Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Representative', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-oes321detailspanel-representativetab',
    controller: 'oes321detailsrepresentativetab',
    viewModel: {
        type: 'oes321detailsrepresentativetabvm'
    },
    /*bind: {
        title: '{title}'
    },*/
    // Предст. плат-ка block_nb = 1
    cls: 'app-oes321detailspanel-representativetab',
    layout: 'vbox',
    padding: 5,
    defaults: {
        width: '100%',
        border: 0
    },
    tabP: 4,
    reference: 'ref-oes321detailspanel-representativetab',
    items: [
        {
            xtype: 'fieldset',
            title: '<b>Общие сведения об участнике</b>',
            createTitleCmp: function() {
                var me  = this,
                    cfg = {
                        xtype: 'component',
                        html: me.title,
                        ui: me.ui,
                        cls: me.baseCls + '-header-text',
                        id: me.id + '-legendTitle'
                    };
                var titleCmp = {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'icon-button-menu-down',
                            margin: 0,
                            text: '',
                            border: false,
                            plain: true,
                            bind: {
                                hidden: '{OES_READONLY_FL_CONVERSION}'
                            },
                            layout: 'vbox',
                            layoutConfig: {
                                align: 'stretch',
                                pack: 'start'
                            },
                            forceLayout: true,
                            arrowVisible: false,
                            flex: 1,
                            menu: {
                                xtype: 'menu',
                                items: [
                                    {
                                        text: 'Очистить блок',
                                        scale: 'small',
                                        iconCls: 'alert-details-button',
                                        typeBlock: 'about-cust',
                                        handler: 'cleanBlock',
                                        bind: {
                                            hidden: '{OES_READONLY_FL_CONVERSION}'
                                        }
                                    }
                                ]
                            }
                        }, cfg
                    ]
                };

                return (me.titleCmp = Ext.widget(titleCmp));
            },
            border: 1,
            //padding: 5,
            margin: '0 0 15px 0',
            defaults: {
                padding: '5px 0 0 0'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            reference: 't1_TU',
                            //flex: 1,
                            width: '50%',
                            labelWidth: 160,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Тип участника операции<br>В случае если участник находится на обслуживании в Банке (филиале Банка), представляющем сведения, или участником является Банк (филиал Банка), представляющий сведения (B_PAYER <>0 (Признак банка плательщика)):<br>•	ЮЛ, филиал ЮЛ (Значение для списка: 1 - ЮЛ);<br>•	ФЛ (Значение для списка: 2 - ФЛ);<br>•	ИП (Значение для списка: 3 – ИП).<br>В ином случае (B_PAYER = '0'):<br>•	ЮЛ, филиал ЮЛ (Значение для списка: 1 - ЮЛ);<br>•	ФЛ (Значение для списка: 2 - ФЛ);<br>•	ИП (Значение для списка: 3 - ИП);<br>•	Невозможно установить тип участника операции (Значение для списка: 4 – не определен)."
                            },
                            editable: false,
                            queryMode: 'local',
                            forceSelection: true,
                            multiSelect: false,
                            matchFieldWidth: false,
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{id}',
                                '</tpl>'
                            ],
                            displayField: 'text',
                            valueField: 'id',
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Тип участника (TU{OES321PartyData.BLOCK_NB})',
                                store: '{TUHardStore}',
                                value: '{OES321PartyData.TU}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {columnWidth: 0.5},
                        {
                            xtype: 'textfield',
                            reference: 't1_PR',
                            width: '50%',
                            labelWidth: 160,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Признак участника операции - 6 цифр, заполняется для 3000-х и 7000-х кодов.</br>3000-е коды: 1 разряд - содержит 1, 2 или 3 - признак адреса регистрации, адреса нахождения или счета в банке соответственно, в государстве, не участвующем в противодействии легализации доходов</br>2-4 разряды - содержат код по ОКСМ страны, не участвующей в противодействии легализации доходов</br>5-6 разряды - содержат порядковый номер территории в соответствии с Перечнем несотрудничающих государств (для территорий, отсутствующих в ОКСМ, иначе - 00)</br>7000-е коды: 1 разряд - содержит 1, 2 или 3:</br>1 - участник - лицо, причастное к терроризму</br>2 - участник - ЮЛ, находящееся в собственности лица, причастного к терроризму</br>3 - участник - ФЛ/ЮЛ, действующее от имени или по указанию лица, причастного к терроризму</br>2-6 разряды - номер ФЛ/ЮЛ в соответствии с Перечнем лиц, причастных к терроризму"
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Признак уч-ка (PRU{OES321PartyData.BLOCK_NB})',
                                value: '{OES321PartyData.PR}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 't1_NAMEU',
                            width: '100%',
                            labelWidth: 250,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ - наименование ЮЛ.<br>Если участником операции является филиал ЮЛ - наименование филиала ЮЛ и наименование ЮЛ.<br>Для ФЛ, индивидуального предпринимателя (далее – ИП) - фамилия, имя, отчество (при наличии последнего) полностью, в указанном порядке, с разделением слов символом 'пробел'.<br>Если значение поля «тип участника операции» = 4 (невозможно установить тип участника операции), указывается значение поля \"Плательщик\" или иного соответствующего ему поля из расчетного документа."
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Наименование ЮЛ или ФИО ФЛ (NAMEU{OES321PartyData.BLOCK_NB})',
                                value: '{OES321PartyData.NAMEU}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 't1_ND',
                            width: 300,
                            labelWidth: 160,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Идентификационный номер участника операции.<br>Для резидента:<br>ЮЛ – ИНН – 10 символов, при отсутствии ИНН у ЮЛ на законном основании – 0;<br>ФЛ, ИП – ИНН (при его наличии) – 12 символов, при отсутствии ИНН у ФЛ – 0.<br>Для нерезидента:<br>ЮЛ (филиала ЮЛ) – ИНН (при его наличии) – 10 символов, при отсутствии ИНН – код иностранной организации (КИО) – 5 символов;<br>ФЛ, ИП – ИНН (при его наличии) – 12 символов, при отсутствии ИНН у ФЛ, ИП – 0."
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'ИНН (ND{OES321PartyData.BLOCK_NB})',
                                value: '{OES321PartyData.ND}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'datefield',
                            reference: 't1_GR',
                            width: 270,
                            labelWidth: 140,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ - резидента: дата регистрации ЮЛ.<br>Для ЮЛ - нерезидента: если участником операции является филиал (представительство) иностранного ЮЛ, аккредитованный (аккредитованное) на территории РФ – дата выдачи свидетельства об аккредитации (без учета процедур продления аккредитации);<br>в ином случае – дата регистрации ЮЛ по месту учреждения и регистрации.<br>Для ФЛ, ИП - дата рождения в соответствии с документом, удостоверяющим личность."
                            },
                            format: 'd.m.Y',
                            startDay: 1,
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Дата рег./ рожд. (GR{OES321PartyData.BLOCK_NB})',
                                value: '{OES321PartyData.GR}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        //{flex: 1},
                        {
                            xtype: 'textfield',
                            reference: 't1_RG',
                            width: 300,
                            labelWidth: 140,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ - резидента: основной государственный регистрационный номер (далее – ОГРН).<br>Для ЮЛ - нерезидента:<br>Если участником операции является филиал (представительство) иностранного ЮЛ, аккредитованный (аккредитованное) на территории Российской Федерации (далее - РФ) - номер свидетельства об аккредитации филиала иностранного ЮЛ (номер свидетельства об аккредитации представительства иностранного ЮЛ) (присваивается федеральным государственным учреждением Государственная регистрационная палата при Министерстве юстиции РФ (далее - Государственная регистрационная палата при Минюсте России);<br>в ином случае - регистрационный номер ЮЛ по месту учреждения и регистрации.<br>Для ФЛ – 0.<br>Для ИП – гражданина РФ, иностранного гражданина или лица без гражданства – основной государственный регистрационный номер индивидуального предпринимателя (далее – ОГРНИП) (при его наличии). При отсутствии ОГРНИП – регистрационный номер по месту учреждения и регистрации."
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Рег. номер (RG{OES321PartyData.BLOCK_NB})',
                                value: '{OES321PartyData.RG}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        //{flex: 0.1},
                        {
                            xtype: 'textfield',
                            reference: 't1_BP',
                            flex: 1,
                            labelWidth: 150,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ - заполняется значением '0'.<br>Для ФЛ, ИП - место рождения в соответствии с документом, удостоверяющим личность."
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Место рождения (BP_{OES321PartyData.BLOCK_NB})',
                                value: '{OES321PartyData.BP}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    rawWidth: 0,
                    border: 0
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            defaults: {
                border: 0
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: '<b>Информация о месте регистрации/жительства</b>',
                    createTitleCmp: function() {
                        var me  = this,
                            cfg = {
                                xtype: 'component',
                                html: me.title,
                                ui: me.ui,
                                cls: me.baseCls + '-header-text',
                                id: me.id + '-legendTitle'
                            };
                        var titleCmp = {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-button-menu-down',
                                    margin: 0,
                                    text: '',
                                    border: false,
                                    plain: true,
                                    bind: {
                                        hidden: '{OES_READONLY_FL_CONVERSION}'
                                    },
                                    layout: 'vbox',
                                    layoutConfig: {
                                        align: 'stretch',
                                        pack: 'start'
                                    },
                                    forceLayout: true,
                                    arrowVisible: false,
                                    flex: 1,
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                text: 'Выбрать адрес',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'register',
                                                handler: 'openFormAddressSelection',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Скопировать из адреса местонахождения',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'register',
                                                handler: 'copyOfLocationOrRegistration',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Очистить блок',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'register',
                                                handler: 'cleanBlock',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            }
                                        ]
                                    }
                                }, cfg
                            ]
                        };

                        return (me.titleCmp = Ext.widget(titleCmp));
                    },
                    flex: 1,
                    border: 1,
                    //padding: 5,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 't1_KODCR_1',
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    flex: 1.1,
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{ISO_NUMERIC_CD} - {CNTRY_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{ISO_NUMERIC_CD}',
                                        '</tpl>'
                                    ],
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    displayField: 'ISO_NUMERIC_CD',
                                    triggerClear: true,
                                    valueField: 'ISO_NUMERIC_CD',
                                    matchFieldWidth: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Код места рег./жит. (KODCR{OES321PartyData.BLOCK_NB})',
                                        store: '{CountriesStore}',
                                        value: '{OES321PartyData.KODCR_1}'
                                    },
                                    vtype : 'vRegexCountries',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 3,
                                    allowBlank: false
                                },
                                {
                                    xtype: 'container',
                                    width: 10
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 't1_KODCR_2',
                                    width: 65,
                                    tooltip: {
                                        text: "Для ЮЛ - код места регистрации.<br>Если участником операции является филиал ЮЛ, указывается код места регистрации головной организации.<br>Для ФЛ, ИП - код места жительства.<br>Для стран и территорий, включенных в ОКСМ, 1 - 3 разряды содержат цифровой код страны регистрации (проживания) в соответствии с ОКСМ.<br>При регистрации (жительстве) на территории, которая не участвует в международном сотрудничестве в сфере ПОД/ФТ и не включена в ОКСМ, разряды 1 - 3 содержат цифровой код по ОКСМ страны, в которой находится территория, в разрядах 4 - 5 указывается порядковый номер этой территории в соответствии с Перечнем несотрудничающих государств, в ином случае разряды 4 - 5 содержат ‘00’."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{OES321PartyData.KODCR_2}'
                                    },
                                    vtype : 'vRegexCountriesTwo',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 2,
                                    errorDismissDelay: 0,
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 't1_AMR_S',
                                    flex: 0.9,
                                    labelWidth: 120,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Код субъекта РФ по ОКАТО. Для лиц, зарегистрированных в иностранном государстве – ‘00’."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{OKATO_CD}',
                                        '</tpl>'
                                    ],
                                    displayField: 'OKATO_CD',
                                    valueField: 'OKATO_CD',
                                    matchFieldWidth: false,
                                    //forceSelection: true,
                                    typeAhead: true,
                                    triggerClear: true,
                                    bind: {
                                        store: '{OKATOStoreAmrS1}',
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'ОКАТО (AMR_S{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.AMR_S}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_AMR_R',
                                    flex: 1,
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Район (регион) республиканского и областного значения. Для лиц, зарегистрированных в иностранном государстве, в данном поле проставляется субъект территориального деления иностранного государства (при наличии)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Район (AMR_R{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.AMR_R}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_AMR_G',
                                    flex: 1,
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Населенный пункт (город, ПГТ, сельский населенный пункт и т.п.)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Город/Нас. пункт (AMR_G{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.AMR_G}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_AMR_U',
                                    flex: 1,
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Наименование улицы."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Ул. (AMR_U{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.AMR_U}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'column'
                            },
                            padding: 0,
                            defaults: {
                                border: 0,
                                margin: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_AMR_D',
                                    // width: 190,
                                    labelWidth: 85,
                                    columnWidth: 0.33,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Номер дома (номер владения)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Д. (AMR_D{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.AMR_D}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                // {columnWidth: 0.2},
                                {
                                    xtype: 'textfield',
                                    reference: 't1_AMR_K',
                                    // width: 170,
                                    columnWidth: 0.33,
                                    labelWidth: 105,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Номер корпуса (номер строения)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'К./Стр. (AMR_K{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.AMR_K}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                // {columnWidth: 0.2},
                                {
                                    xtype: 'textfield',
                                    reference: 't1_AMR_O',
                                    // width: 190,
                                    columnWidth: 0.34,
                                    labelWidth: 110,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Номер офиса (номер квартиры)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Оф./Кв. (AMR_O{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.AMR_O}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            rawWidth: 0,
                            border: 0
                        }
                    ]
                },
                {width: 10},
                {
                    xtype: 'fieldset',
                    title: '<b>Информация о месте нахождения/пребывания</b>',
                    createTitleCmp: function() {
                        var me  = this,
                            cfg = {
                                xtype: 'component',
                                html: me.title,
                                ui: me.ui,
                                cls: me.baseCls + '-header-text',
                                id: me.id + '-legendTitle'
                            };
                        var titleCmp = {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-button-menu-down',
                                    margin: 0,
                                    text: '',
                                    border: false,
                                    plain: true,
                                    bind: {
                                        hidden: '{OES_READONLY_FL_CONVERSION}'
                                    },
                                    layout: 'vbox',
                                    layoutConfig: {
                                        align: 'stretch',
                                        pack: 'start'
                                    },
                                    forceLayout: true,
                                    arrowVisible: false,
                                    flex: 1,
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                text: 'Выбрать адрес',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'visit',
                                                handler: 'openFormAddressSelection',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Скопировать из адреса регистрации',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'visit',
                                                handler: 'copyOfLocationOrRegistration',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Очистить блок',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'visit',
                                                handler: 'cleanBlock',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            }
                                        ]
                                    }
                                }, cfg
                            ]
                        };

                        return (me.titleCmp = Ext.widget(titleCmp));
                    },
                    flex: 1,
                    border: 1,
                    //padding: 5,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 't1_KODCN_1',
                                    labelWidth: 190,
                                    labelAlign: 'right',
                                    width: 290,
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{ISO_NUMERIC_CD} - {CNTRY_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{ISO_NUMERIC_CD}',
                                        '</tpl>'
                                    ],
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    displayField: 'ISO_NUMERIC_CD',
                                    triggerClear: true,
                                    valueField: 'ISO_NUMERIC_CD',
                                    matchFieldWidth: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Код места нах./Гр. (KODCN{OES321PartyData.BLOCK_NB})',
                                        store: '{CountriesStore}',
                                        value: '{OES321PartyData.KODCN_1}'
                                    },
                                    vtype : 'vRegexCountries',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 3,
                                    allowBlank: false
                                },
                                {
                                    xtype: 'container',
                                    width: 8
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 't1_KODCN_2',
                                    width: 65,
                                    tooltip: {
                                        text: "Для ЮЛ - код места нахождения.<br>Если участником операции является филиал ЮЛ, указывается код места нахождения филиала ЮЛ.<br>Для стран и территорий, включенных в ОКСМ, разряды 1 - 3 содержат цифровой код страны нахождения в соответствии с ОКСМ.<br>При нахождении ЮЛ (филиала ЮЛ) на территории, которая не участвует в международном сотрудничестве в сфере ПОД/ФТ  и не включена в ОКСМ, разряды 1 - 3 содержат цифровой код по ОКСМ страны, в которой находится территория, в разрядах 4 - 5 указывается порядковый номер этой территории в соответствии с Перечнем несотрудничающих государств, в ином случае разряды 4 - 5 содержат ‘00’.<br>Для ФЛ, ИП - гражданство.<br>Разряды 1 - 3 поля содержат код страны в соответствии с ОКСМ. Для лиц без гражданства, разряды 1-3 заполняются значением ‘000’.<br>Разряды 4 – 5 содержат: для иностранных публичных должностных лиц (далее ИПДЛ) – ‘01’; для родственников ИПДЛ – ‘02’; в ином случае – ‘00’."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{OES321PartyData.KODCN_2}'
                                    },
                                    vtype : 'vRegexCountriesTwo',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 2,
                                    errorDismissDelay: 0,
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 't1_ADRESS_S',
                                    flex: 1,
                                    labelWidth: 130,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Код субъекта РФ по ОКАТО.<br>Для лиц, находящихся в иностранном государстве – ‘00’."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{OKATO_CD}',
                                        '</tpl>'
                                    ],
                                    displayField: 'OKATO_CD',
                                    valueField: 'OKATO_CD',
                                    matchFieldWidth: false,
                                    //forceSelection: true,
                                    typeAhead: true,
                                    triggerClear: true,
                                    bind: {
                                        store: '{OKATOStoreAdressS1}',
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'ОКАТО (ADRESS_S{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.ADRESS_S}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_ADRESS_R',
                                    flex: 1,
                                    labelWidth: 190,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Район (регион) республиканского и областного значения. Для лиц, зарегистрированных в иностранном государстве, в данном поле проставляется субъект территориального деления иностранного государства (при наличии)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Район (ADRESS_R{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.ADRESS_R}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_ADRESS_G',
                                    flex: 1,
                                    labelWidth: 190,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Населенный пункт (город, ПГТ, сельский населенный пункт и т.п.)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Город/Нас. пункт (ADRESS_G{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.ADRESS_G}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_ADRESS_U',
                                    flex: 1,
                                    labelWidth: 190,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Наименование улицы."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Ул. (ADRESS_U{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.ADRESS_U}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'column'
                            },
                            padding: 0,
                            defaults: {
                                border: 0,
                                margin: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_ADRESS_D',
                                    // width: 165,
                                    columnWidth: 0.33,
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Номер дома (номер владения)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Д. (ADRESS_D{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.ADRESS_D}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                // {columnWidth: 0.5},
                                {
                                    xtype: 'textfield',
                                    reference: 't1_ADRESS_K',
                                    // width: 210,
                                    columnWidth: 0.33,
                                    labelWidth: 125,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Номер корпуса (номер строения)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'К./Стр. (ADRESS_K{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.ADRESS_K}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                // {columnWidth: 0.5},
                                {
                                    xtype: 'textfield',
                                    reference: 't1_ADRESS_O',
                                    // width: 195,
                                    columnWidth: 0.34,
                                    labelWidth: 130,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Номер офиса (номер квартиры)."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Оф./Кв. (ADRESS_O{OES321PartyData.BLOCK_NB})',
                                        value: '{OES321PartyData.ADRESS_O}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            rawWidth: 0,
                            border: 0
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: '<b>Документ, удостоверяющий личность</b>',
            createTitleCmp: function() {
                var me  = this,
                    cfg = {
                        xtype: 'component',
                        html: me.title,
                        ui: me.ui,
                        cls: me.baseCls + '-header-text',
                        id: me.id + '-legendTitle'
                    };
                var titleCmp = {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'icon-button-menu-down',
                            margin: 0,
                            text: '',
                            border: false,
                            plain: true,
                            bind: {
                                hidden: '{OES_READONLY_FL_CONVERSION}'
                            },
                            layout: 'vbox',
                            layoutConfig: {
                                align: 'stretch',
                                pack: 'start'
                            },
                            forceLayout: true,
                            arrowVisible: false,
                            flex: 1,
                            menu: {
                                xtype: 'menu',
                                items: [
                                    {
                                        text: 'Выбрать документ',
                                        scale: 'small',
                                        iconCls: 'alert-details-button',
                                        typeBlock: 'personality',
                                        handler: 'openFormSelectDocument',
                                        bind: {
                                            hidden: '{OES_READONLY_FL_CONVERSION}'
                                        }
                                    },
                                    {
                                        text: 'Скопировать из ДПП',
                                        scale: 'small',
                                        iconCls: 'alert-details-button',
                                        typeBlock: 'personality',
                                        handler: 'copyOfDPP',
                                        bind: {
                                            hidden: '{OES_READONLY_FL_CONVERSION}'
                                        }
                                    },
                                    {
                                        text: 'Очистить блок',
                                        scale: 'small',
                                        iconCls: 'alert-details-button',
                                        typeBlock: 'personality',
                                        handler: 'cleanBlock',
                                        bind: {
                                            hidden: '{OES_READONLY_FL_CONVERSION}'
                                        }
                                    }
                                ]
                            }
                        }, cfg
                    ]
                };

                return (me.titleCmp = Ext.widget(titleCmp));
            },
            border: 1,
            //padding: 5,
            margin: '5px 0 20px 0',
            defaults: {
                padding: '5px 0 0 0'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            reference: 't1_KD',
                            width: 500,
                            labelWidth: 260,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - код вида документа, удостоверяющего личность (Справочник кодов видов документов см. в Приложении 10 к Положению № 321-П)."
                            },
                            editable: true,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{DOC_TYPE_CD} - {DOC_TYPE_NM}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{DOC_TYPE_CD}',
                                '</tpl>'
                            ],
                            displayField: 'DOC_TYPE_CD',
                            valueField: 'DOC_TYPE_CD',
                            forceSelection: true,
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Документ, удостоверяющий личность (KD{OES321PartyData.BLOCK_NB})',
                                store: '{DOCTYPEStoreChainedKD}',
                                value: '{OES321PartyData.KD}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            maskRe: new RegExp('^[0-9]$'),
                            enforceMaxLength:true,
                            maxLength: 2,
                            vtype : 'vRegexCountries',
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'textfield',
                            reference: 't1_SD',
                            flex: 1,
                            labelWidth: 200,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ - код ОКПО, при отсутствии у ЮЛ кода ОКПО на законном основании – ‘0’;<br>Для ФЛ, ИП - серия документа, удостоверяющего личность.<br>Если в серии присутствуют римские цифры, то при заполнении данного показателя используются заглавные буквы латинского алфавита (при этом для обозначения цифры 5 – V); русские буквы в серии передаются заглавными русскими буквами. Если в документе отсутствует серия, в поле проставляется ‘0’."
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'ОКПО/Серия документа (SD{OES321PartyData.BLOCK_NB})',
                                value: '{OES321PartyData.SD}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'textfield',
                            reference: 't1_VD1',
                            width: 340,
                            labelWidth: 170,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - номер документа, удостоверяющего личность. При заполнении пробелы не допускаются."
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Номер документа (VD{OES321PartyData.BLOCK_NB}1)',
                                value: '{OES321PartyData.VD1}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 't1_VD2',
                            flex: 2,
                            labelWidth: 260,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - наименование органа, выдавшего документ, удостоверяющий личность ФЛ, ИП и код подразделения (если имеется)."
                            },
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Орган, выдавший документ (VD{OES321PartyData.BLOCK_NB}2)',
                                value: '{OES321PartyData.VD2}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'datefield',
                            reference: 't1_VD3',
                            width: 340,
                            labelWidth: 170,
                            labelAlign: 'right',
                            tooltip: {
                                text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - дата выдачи документа, удостоверяющего личность ФЛ, ИП."
                            },
                            format: 'd.m.Y',
                            startDay: 1,
                            setFieldLabel: common.util.component.setFieldLabel,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                fieldLabel: 'Дата выдачи (VD{OES321PartyData.BLOCK_NB}3)',
                                value: '{OES321PartyData.VD3}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    rawWidth: 0,
                    border: 0
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            defaults: {
                border: 0
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: '<b>Документ, подтверждающий право на пребывание (проживание) в РФ</b>',
                    createTitleCmp: function() {
                        var me  = this,
                            cfg = {
                                xtype: 'component',
                                html: me.title,
                                ui: me.ui,
                                cls: me.baseCls + '-header-text',
                                id: me.id + '-legendTitle'
                            };
                        var titleCmp = {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-button-menu-down',
                                    margin: 0,
                                    text: '',
                                    border: false,
                                    plain: true,
                                    bind: {
                                        hidden: '{OES_READONLY_FL_CONVERSION}'
                                    },
                                    layout: 'vbox',
                                    layoutConfig: {
                                        align: 'stretch',
                                        pack: 'start'
                                    },
                                    forceLayout: true,
                                    arrowVisible: false,
                                    flex: 1,
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                text: 'Выбрать документ',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'right-to-live',
                                                handler: 'openFormSelectDocument',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Скопировать из ДУЛ',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                handler: 'copyOfDUL',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Скопировать из МК',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                handler: 'copyOfMK',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Очистить блок',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'right-to-live',
                                                handler: 'cleanBlock',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            }
                                        ]
                                    }
                                }, cfg
                            ]
                        };

                        return (me.titleCmp = Ext.widget(titleCmp));
                    },
                    flex: 1,
                    border: 1,
                    //padding: 5,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 't1_VD4',
                                    flex: 1,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - гражданина РФ - заполняется значением '0'.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - код вида документа, подтверждающего право на пребывание (проживание) в РФ (Справочник кодов видов документов см. в Приложении 10 к Положению № 321-П)."
                                    },
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{DOC_TYPE_CD} - {DOC_TYPE_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{DOC_TYPE_CD}',
                                        '</tpl>'
                                    ],
                                    displayField: 'DOC_TYPE_CD',
                                    valueField: 'DOC_TYPE_CD',
                                    forceSelection: true,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Код вида документа (VD{OES321PartyData.BLOCK_NB}4)',
                                        store: '{DOCTYPEStoreChainedVD4}',
                                        value: '{OES321PartyData.VD4}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 2,
                                    vtype : 'vRegexCountries',
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 't1_VD5',
                                    flex: 1,
                                    labelWidth: 160,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - гражданина РФ заполняется значением '0'.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - серия и номер документа, подтверждающего право на пребывание (проживание) в РФ.<br>Серия и номер заполняются без пробелов.<br>Для заполнения римских цифр используются заглавные буквы латинского алфавита (при этом для обозначения цифры 5 - V); русские буквы в серии передаются заглавными русскими буквами. Если в документе отсутствует серия, в поле проставляется ‘0’."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Серия и номер (VD{OES321PartyData.BLOCK_NB}5)',
                                        value: '{OES321PartyData.VD5}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    reference: 't1_VD6',
                                    width: 340,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата начала срока действия права на пребывание (проживание) в РФ.<br>Если в документе отсутствует соответствующая дата, в поле проставляется значение 01012099"
                                    },
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Срок действия с (VD{OES321PartyData.BLOCK_NB}6)',
                                        value: '{OES321PartyData.VD6}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'datefield',
                                    reference: 't1_VD7',
                                    width: 240,
                                    labelWidth: 70,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата окончания срока действия права на пребывание (проживание) в РФ.<br>Если в документе отсутствует соответствующая дата, в поле проставляется значение 01012099.<br>Формат поля даты - ДДММГГГГ, где ДД - день месяца, ММ - месяц, ГГГГ - год."
                                    },
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'по (VD{OES321PartyData.BLOCK_NB}7)',
                                        value: '{OES321PartyData.VD7}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            rawWidth: 0,
                            border: 0
                        }
                    ]
                },
                {width: 10},
                {
                    xtype: 'fieldset',
                    title: '<b>Миграционная карта</b>',
                    createTitleCmp: function() {
                        var me  = this,
                            cfg = {
                                xtype: 'component',
                                html: me.title,
                                ui: me.ui,
                                cls: me.baseCls + '-header-text',
                                id: me.id + '-legendTitle'
                            };
                        var titleCmp = {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-button-menu-down',
                                    margin: 0,
                                    text: '',
                                    border: false,
                                    plain: true,
                                    bind: {
                                        hidden: '{OES_READONLY_FL_CONVERSION}'
                                    },
                                    layout: 'vbox',
                                    layoutConfig: {
                                        align: 'stretch',
                                        pack: 'start'
                                    },
                                    forceLayout: true,
                                    arrowVisible: false,
                                    flex: 1,
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                text: 'Выбрать документ',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'migration-card',
                                                handler: 'openFormSelectDocument',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Скопировать из ДПП',
                                                scale: 'small',
                                                typeBlock: 'migration-card',
                                                iconCls: 'alert-details-button',
                                                handler: 'copyOfDPP',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            },
                                            {
                                                text: 'Очистить блок',
                                                scale: 'small',
                                                iconCls: 'alert-details-button',
                                                typeBlock: 'migration-card',
                                                handler: 'cleanBlock',
                                                bind: {
                                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                                }
                                            }
                                        ]
                                    }
                                }, cfg
                            ]
                        };

                        return (me.titleCmp = Ext.widget(titleCmp));
                    },
                    flex: 1,
                    border: 1,
                    //padding: 5,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 't1_MC1',
                                    flex: 1,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением '0' .<br>Для ФЛ, ИП - гражданина РФ заполняется значением '0'.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - номер миграционной карты.<br>При отсутствии миграционной карты – ‘0’."
                                    },
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Номер (MC_{OES321PartyData.BLOCK_NB}1)',
                                        value: '{OES321PartyData.MC1}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    reference: 't1_MC2',
                                    width: 340,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата начала срока пребывания в РФ в соответствии с миграционной картой.<br>При отсутствии миграционной карты заполняется значением 01012099."
                                    },
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'Срок пребывания с (MC_{OES321PartyData.BLOCK_NB}2)',
                                        value: '{OES321PartyData.MC2}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'datefield',
                                    reference: 't1_MC3',
                                    width: 250,
                                    labelWidth: 80,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата окончания срока пребывания в РФ в соответствии с миграционной картой.<br>При отсутствии миграционной карты заполняется значением 01012099."
                                    },
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        fieldLabel: 'по (MC_{OES321PartyData.BLOCK_NB}3)',
                                        value: '{OES321PartyData.MC3}'
                                    },
                                    listeners: {
                                        validitychange: 'checkFieldServerError'
                                    },
                                    allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            rawWidth: 0,
                            border: 0
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox'
            },
            margin: '5px 0 0 0',
            defaults: {
                border: 0
            },
            items: [
                {
                    xtype: 'textareafield',
                    reference: 't1_NOTE',
                    width: '100%',
                    labelWidth: 80,
                    labelAlign: 'right',
                    fieldLabel: '<b>Примечание</b>',
                    tooltip: {
                        text: ""
                    },
                    bind: {
                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                        value: '{OES321PartyData.NOTE}'
                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                }
            ]
        }
    ]
});