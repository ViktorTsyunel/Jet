Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.PayerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321detailspayertabvm',
    data: {
        title: 'Плательщик',
        tabVM: '0'
    },
    formulas: {
        OKATO_AMR_CONVERSION: {
            bind: {
                OKATO_CD: '{OES321PartyData.AMR_S}'
            },
            get: function (dataObject) {
                return dataObject.OKATO_CD;
            },
            set: function (value) {
                if (value && value !== '' || value == '0') {
                    this.set('OES321PartyData.AMR_S', value);
                }
            }
        },
        OKATO_ADRESS_CONVERSION: {
            bind: {
                OKATO_CD: '{OES321PartyData.ADRESS_S}'
            },
            get: function (dataObject) {
                return dataObject.OKATO_CD;
            },
            set: function (value) {
                if (value && value !== '' || value == '0') {
                    this.set('OES321PartyData.ADRESS_S', value);
                }
            }
        },
        KD_CONVERSION: {
            bind: {
                DOC_TYPE_CD: '{OES321PartyData.KD}'
            },
            get: function (dataObject) {
                return dataObject.DOC_TYPE_CD;
            },
            set: function (value) {
                if (value && value !== '' || value == '0') {
                    this.set('OES321PartyData.KD', value);
                }
            }
        },
        VD_CONVERSION: {
            bind: {
                DOC_TYPE_CD: '{OES321PartyData.VD4}'
            },
            get: function (dataObject) {
                return dataObject.DOC_TYPE_CD;
            },
            set: function (value) {
                if (value && value !== '' || value == '0') {
                    this.set('OES321PartyData.VD4', value);
                }
            }
        }
    },
    stores: {
        // Плательщик block_nb = 0
        OES321PartyStore: {
            model: 'AML.model.OES321Party',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'OES321Party',
                    action: 'read'
                }
            }
        },
        OKATOStoreAmrS0: {
            type: 'chained',
            source: 'OKATO'
        },
        OKATOStoreAdressS0: {
            type: 'chained',
            source: 'OKATO'
        },
        DOCTYPEStoreChainedKD: {
            type: 'chained',
            source: 'DOCTYPEREF'
        },
        DOCTYPEStoreChainedVD4: {
            type: 'chained',
            source: 'DOCTYPEREF'
        }
    }
});