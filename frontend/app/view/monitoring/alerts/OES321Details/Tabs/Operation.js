Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Operation', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-oes321detailspanel-operationtab',
    controller: 'oes321detailsoperationtab',
    viewModel: {
        type: 'oes321detailsoperationtabvm'
    },
    bind: {
        title: '{title}'
    },
    cls: 'app-oes321detailspanel-operationtab',
    layout: 'vbox',
    padding: 15,
    defaults: {
        width: '100%',
        border: 0
    },
    tabP: 0,
    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            cls: 'header credit-institution',
            padding: '10px 0 5px 0',
            defaults: {
                margin: '1px 3px 1px 3px'
            },
            items: [
                {
                    width: 200,
                    border: 0,
                    html: '<b style="text-decoration: underline">Кредитная организация</b>'
                },
                {
                    xtype: 'button',
                    width: 20,
                    reference: 'buttomExpand1',
                    height: 20,
                    padding: 1,
                    border: 0,
                    text: '',
                    cls: 'arrow-ico closed',
                    enableToggle: true,
                    listeners: {
                        toggle: 'toggleCreditInstitution'
                    }
                },
                {
                    xtype: 'combobox',
                    reference: 'base_OES_321_ORG_ID',
                    margin: '0 0 0 30px',
                    width: 650,
                    editable: false,
                    tooltip: {
                        title: 'Территориальный банк',
                        text: 'Информация предоставлена в соответствии с настройками в справочнике «Информация о КО» (Администрирование – ОЭС – Информация о КО)'
                    },
                    queryMode: 'local',
                    tpl: [
                        '<tpl for=".">',
                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: pre; text-overflow: ellipsis;">{DISPLAY_NAME}</div>',
                        '</tpl>'
                    ],
                    displayTpl: [
                        '<tpl for=".">',
                        '{DISPLAY_NAME}',
                        '</tpl>'
                    ],
                    valueField: 'OES_321_ORG_ID',
                    displayField: 'DISPLAY_NAME',
                    bind: {
                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                        store: '{TBStore}',
                        value: '{OES321DetailsTable.OES_321_ORG_ID}'
                    },
                    listeners: {
                        change: 'TBChange',
                        validitychange: 'checkFieldServerError'
                    },
                    forceSelection: true,
                    allowBlank: false,
                    errorDismissDelay: 0
                }
            ]
        },
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            cls: 'content credit-institution',
            padding: '10px 0 0 0',
            collapsible: true,
            collapsed: true,
            header: false,
            animCollapse: false,
            bind: {
                collapsed: '{!showCreditInstitutionContent}'
            },
            defaults: {
                maxWidth: 400,
                border: 0
            },
            items: [
                {
                    xtype: 'textfield',
                    reference: 'base_NUMBF_S',
                    labelWidth: 200,
                    labelAlign: 'right',
                    fieldLabel: '№ филиала (NUMBF_S)',
                    tooltip: {
                        text: 'Порядковый номер филиала Банка по КГРКО, передающего ОЭС в ФСФМ или «0», если ОЭС передается Банком.'
                    },
                    readOnly: true,
                    bind: {
                        value: {
                            bindTo: '{selectedTB.NUMBF_S}',
                            deep: true
                        }
                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                },
                {flex: 1},
                {
                    xtype: 'textfield',
                    reference: 'base_BIK_S',
                    labelWidth: 150,
                    labelAlign: 'right',
                    fieldLabel: 'БИК (BIK_S)',
                    tooltip: {
                        text: 'БИК Банка (филиала Банка), передающего ОЭС в ФСФМ.'
                    },
                    readOnly: true,
                    bind: {
                        value: {
                            bindTo: '{selectedTB.BIK_S}',
                            deep: true
                        }
                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                },
                {flex: 1},
                {
                    xtype: 'combobox',
                    reference: 'base_KTU_S',
                    labelWidth: 250,
                    labelAlign: 'right',
                    fieldLabel: 'Код по ОКАТО (KTU_S)',
                    tooltip: {
                        text: 'Код территории по ОКАТО для территориального учреждения Банка России, которое осуществляет надзор за деятельностью Банка (филиала  Банка), передающего ОЭС в ФСФМ.'
                    },
                    editable: false,
                    queryMode: 'local',
                    tpl: [
                        '<tpl for=".">',
                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                        '</tpl>'
                    ],
                    displayTpl: [
                        '<tpl for=".">',
                        '{OKATO_CD}',
                        '</tpl>'
                    ],
                    displayField: 'OKATO_OBJ_NM',
                    valueField: 'OKATO_CD',
                    readOnly: true,
                    bind: {
                        store: 'OKATO',
                        value: {
                            bindTo: '{selectedTB.KTU_S}',
                            deep: true
                        }
                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                },
                {flex: 0.3}
            ]
        },
        {
            xtype: 'panel',
            layout: {
                type: 'hbox'
            },
            cls: 'content credit-institution',
            padding: '10px 0 0 0',
            collapsible: true,
            collapsed: true,
            header: false,
            animCollapse: false,
            bind: {
                collapsed: '{!showCreditInstitutionContent}'
            },
            defaults: {
                maxWidth: 400,
                border: 0
            },
            items: [
                {
                    xtype: 'textfield',
                    reference: 'base_REGN',
                    labelWidth: 200,
                    labelAlign: 'right',
                    fieldLabel: 'Рег. номер (REGN)',
                    tooltip: {
                        text: 'Регистрационный номер Банка по КГРКО без указания порядкового номера филиала и признака Банка.'
                    },
                    readOnly: true,
                    bind: {
                        value: {
                            bindTo: '{selectedTB.REGN}',
                            deep: true
                        }

                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                },
                {flex: 1},
                {
                    xtype: 'textfield',
                    reference: 'base_ND_KO',
                    labelWidth: 150,
                    labelAlign: 'right',
                    fieldLabel: 'ИНН (ND_KO)',
                    tooltip: {
                        text: 'Идентификационный номер налогоплательщика Банка.'
                    },
                    readOnly: true,
                    bind: {
                        value: {
                            bindTo: '{selectedTB.ND_KO}',
                            deep: true
                        }
                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                },
                {flex: 1},
                {
                    xtype: 'textfield',
                    reference: 'base_TEL',
                    labelWidth: 250,
                    labelAlign: 'right',
                    fieldLabel: 'Телефон ответственного (TEL)',
                    tooltip: {
                        text: ''
                    },
                    readOnly: true,
                    bind: {
                        value: {
                            bindTo: '{selectedTB.TEL}',
                            deep: true
                        }
                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                },
                {flex: 0.3}
            ]
        },
        {
            xtype: 'panel',
            layout: {
                type: 'vbox'
            },
            cls: 'content credit-institution',
            padding: '10px 0 0 0',
            collapsible: true,
            collapsed: true,
            header: false,
            animCollapse: false,
            bind: {
                collapsed: '{!showCreditInstitutionContent}'
            },
            defaults: {
                width: '100%',
                border: 0
            },
            items: [
                {
                    xtype: 'container',
                    layout: 'hbox',
                    cls: 'header filial-ko',
                    padding: '10px 0 5px 0',
                    items: [
                        {
                            width: 200,
                            border: 0,
                            html: '<b style="text-decoration: underline">Филиал КО</b>'
                        },
                        {
                            xtype: 'button',
                            width: 20,
                            height: 20,
                            padding: 1,
                            reference: 'buttomExpand2',
                            border: 0,
                            text: '',
                            cls: 'arrow-ico closed',
                            enableToggle: true,
                            listeners: {
                                toggle: 'toggleFilialKO'
                            }
                        },
                        {
                            xtype: 'checkbox',
                            reference: 'base_BRANCH_FL',
                            margin: '0 0 0 30px',
                            width: 350,
                            boxLabel: 'Сведения предоставлены филиалом Банка (BRANCH)',
                            tooltip: {
                                text: "Признак представления сведений в ФСФМ филиалом Банка, не передающим самостоятельно ОЭС в ФСФМ. Флаг выставлен (1), если сведения представлены филиалом Банка, не передающим самостоятельно ОЭС через территориальное учреждение Банка России."
                            },
                            setChecked: function (value) {
                                this.setValue(value);
                            },
                            readOnly: true,
                            bind: {
                                checked: {
                                    bindTo: '{selectedTB.BRANCH_FL}',
                                    deep: true
                                }
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    layout: {
                        type: 'hbox'
                    },
                    cls: 'content filial-ko',
                    padding: '10px 0 0 0',
                    collapsible: true,
                    collapsed: true,
                    header: false,
                    animCollapse: false,
                    bind: {
                        collapsed: '{!showFilialKOContent}'
                    },
                    defaults: {
                        maxWidth: 400,
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'base_NUMBF_SS',
                            labelWidth: 200,
                            labelAlign: 'right',
                            fieldLabel: '№ филиала (NUMBF_SS)',
                            tooltip: {
                                text: "Порядковый номер филиала Банка по КГРКО, представляющего сведения, но не передающего самостоятельно ОЭС в ФСФМ, или '0' - в ином случае."
                            },
                            readOnly: true,
                            bind: {
                                value: {
                                    bindTo: '{selectedTB.NUMBF_SS}',
                                    deep: true
                                }
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            errorDismissDelay: 0
                        },
                        {flex: 1},
                        {
                            xtype: 'textfield',
                            reference: 'base_BIK_SS',
                            labelWidth: 150,
                            labelAlign: 'right',
                            fieldLabel: 'БИК (BIK_SS)',
                            tooltip: {
                                text: "БИК филиала Банка, представляющего сведения, но не передающего самостоятельно ОЭС в ФСФМ, или '0' - в ином случае."
                            },
                            readOnly: true,
                            bind: {
                                value: {
                                    bindTo: '{selectedTB.BIK_SS}',
                                    deep: true
                                }
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            errorDismissDelay: 0
                        },
                        {flex: 1},
                        {
                            xtype: 'combobox',
                            reference: 'base_KTU_SS',
                            labelWidth: 250,
                            labelAlign: 'right',
                            fieldLabel: 'Код по ОКАТО (KTU_SS)',
                            tooltip: {
                                text: "Код территории по ОКАТО для территориального учреждения Банка России, которое осуществляет надзор за деятельностью филиала Банка  представляющей сведения, но не передающей самостоятельно ОЭС в ФСФМ, или '0' - в ином случае."
                            },
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{OKATO_CD}',
                                '</tpl>'
                            ],
                            displayField: 'OKATO_OBJ_NM',
                            valueField: 'OKATO_CD',
                            //forceSelection: true,
                            readOnly: true,
                            bind: {
                                store: 'OKATO',
                                value: {
                                    bindTo: '{selectedTB.KTU_SS}',
                                    deep: true
                                }
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            errorDismissDelay: 0
                        },
                        {flex: 0.3}
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: '<b>Признаки операции</b>',
            border: 1,
            padding: 10,
            margin: '10px 0 0 0',
            defaults: {
                padding: '5px 0 0 0'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'datefield',
                            reference: 'base_DATA',
                            width: 340,
                            labelWidth: 200,
                            labelAlign: 'right',
                            fieldLabel: 'Дата операции (DATA)',
                            tooltip: {
                                anchor: 'left',
                                text: "Дата совершения операции (сделки)."
                            },
                            format: 'd.m.Y',
                            startDay: 1,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.DATA}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'combobox',
                            reference: 'base_B_PAYER',
                            width: 310,
                            labelWidth: 230,
                            labelAlign: 'right',
                            fieldLabel: 'Признак Банка плательщика (B_PAYER)',
                            tooltip: {
                                text: "Признак банка лица, совершающего операцию:<br>•	Лицо, совершающее операцию, находится на обслуживании в Банке (филиале Банка), представляющем сведения об операции (значение 1 - плательщик на обслуживании в Банке, предоставляющем сведения об операции);<br>•	Лицом, совершающим операцию, является Банк (филиал), представляющий сведения об этой операции в ФСФМ (значение 2 - плательщик является Банком, предоставляющим сведения об операции);<br>•	В ином случае – 0."
                            },
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{id}',
                                '</tpl>'
                            ],
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{BPAYERHardStore}',
                                value: '{OES321DetailsTable.B_PAYER}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'textfield',
                            reference: 'base_PRIZ6001',
                            width: 470,
                            labelWidth: 330,
                            labelAlign: 'right',
                            fieldLabel: 'Код признаков необычных операций и сделок (PRIZ6001)',
                            tooltip: {
                                text: "Коды видов признаков, указывающих на необычный характер сделки. При необходимости указания нескольких кодов видов признаков, коды указываются через запятую.<br>Заполняется только в случае указания в поле VO или DOP_V кода группы операции 60.<br>В ином случае – '0'."
                            },
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.PRIZ6001}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            reference: 'base_VO',
                            width: 340,
                            labelWidth: 200,
                            labelAlign: 'right',
                            fieldLabel: 'Код вида операции (VO)',
                            tooltip: {
                                text: "Код вида операции.<br>При соответствии характера операции нескольким видам операций, в данном поле указывается код для одного вида операции, при этом:<br>– более высокий приоритет при выборе кода имеют операции с кодом группы операций 70;<br>– более низкий приоритет при выборе кода имеют операции с кодом группы операций 60."
                            },
                            editable: true,
                            queryMode: 'local',
                            store: 'OPOK',
                            displayField: 'OPOK_NB',
                            valueField: 'OPOK_NB',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                '</tpl>'
                            ),
                            forceSelection: true,
                            matchFieldWidth: false,
                            triggerClear: true,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{VO_CONVERSION}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            maskRe: new RegExp('^[0-9]$'),
                            enforceMaxLength: true,
                            maxLength: 4,
                            vtype: 'vRegexCountries',
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'combobox',
                            reference: 'base_B_RECIP',
                            width: 310,
                            labelWidth: 230,
                            labelAlign: 'right',
                            fieldLabel: 'Признак Банка получателя (B_RECIP)',
                            tooltip: {
                                text: "Признак банка получателя:<br>•	Получатель по операции находится на обслуживании в Банке (филиале), представляющем<br>сведения об операции (Значение для списка: 1 – получатель – клиент Банка, предоставляющего сведения об операции);<br>•	Получателем по операции является Банк (филиал), представляющий сведения об операции в ФСФМ (Значение для списка: 2 – получатель – Банк, предоставляющий сведения);<br>•	В ином случае (Значение для списка: 0 – не клиент Банка, предоставляющего сведения об операции)."
                            },
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{id}',
                                '</tpl>'
                            ],
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{BRECIPHardStore}',
                                value: '{OES321DetailsTable.B_RECIP}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'datefield',
                            reference: 'base_DATE_S',
                            width: 470,
                            labelWidth: 330,
                            labelAlign: 'right',
                            fieldLabel: 'Дата выявления (DATE_S)',
                            tooltip: {
                                anchor: 'left',
                                text: "В случае если поле VO равно '6001', то в данном поле указывается дата выявления операции.<br>В случае если поле VO равно '5003', или '5007', или '8001', то в данном поле указывается дата, когда Банку стало известно о получении или предоставлении имущества по договору финансовой аренды (лизинга), сделке с недвижимым имуществом, операции предоставления ЮЛ, не являющимися кредитными организациями (далее - КО), беспроцентных займов ФЛ и (или) другим ЮЛ, а также о получении такого займа (дата получения Банком сведений и (или) документа, подтверждающих факт совершения операции (сделки)).<br>В ином случае заполняется значением 01012099."
                            },
                            format: 'd.m.Y',
                            startDay: 1,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.DATE_S}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            reference: 'base_DOP_V',
                            width: 340,
                            labelWidth: 200,
                            labelAlign: 'right',
                            fieldLabel: 'Доп. коды вида операции (DOP_V)',
                            tooltip: {
                                text: "Код вида операции, отличающийся от вида операции, код которого указан в поле VO. В ином случае указывается '0'.<br>При необходимости указания нескольких кодов видов операций коды указываются через запятую.<br>Объект операции:<br>0 – операция совершается с денежными средствами;<br>1 – операция совершается с иным имуществом."
                            },
                            editable: true,
                            queryMode: 'local',
                            displayField: 'OPOK_NB',
                            valueField: 'OPOK_NB',
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                '</tpl>'
                            ),
                            forceSelection: true,
                            multiSelect: true,
                            matchFieldWidth: false,
                            triggerClear: true,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: {
                                    bindTo: '{DOP_V_CONVERSION}',
                                    deep: true
                                },
                                store: '{OPOKaddEmpty}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError',
                                change: function (field, newValue, oldValue, eOpts) {
                                    var dataDopV = [];
                                    if (newValue && Ext.isArray(newValue) && newValue.length === 1 && newValue[0] === 0) {
                                        field.setValue([0]);
                                    } else if (newValue && Ext.isArray(newValue)) {
                                        Ext.Array.each(newValue, function (item) {
                                            if (item !== 0) {
                                                dataDopV.push(item);
                                            }
                                        });
                                        field.setValue(dataDopV);
                                    }
                                }
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'combobox',
                            reference: 'base_PART',
                            width: 310,
                            labelWidth: 230,
                            labelAlign: 'right',
                            fieldLabel: 'Операция совершается по поручению и от имени (PART)',
                            tooltip: {
                                text: "Признак совершения операции по поручению и от имени третьего лица: <br>Лицо, совершающее операцию, действует от имени и по поручению третьего лица (значение 1– плательщик действует от имени и по поручению 3-его лица);<br>Получатель по операции действует от имени и по поручению третьего лица (значение 2 – получатель действует от имени и по поручению 3-его лица);<br>•	В ином случае (0 – Нет)."
                            },
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{id}',
                                '</tpl>'
                            ],
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{PARTHardStore}',
                                value: '{OES321DetailsTable.PART}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'combobox',
                            reference: 'base_TERROR',
                            width: 470,
                            labelWidth: 330,
                            labelAlign: 'right',
                            fieldLabel: 'Признак операции, связанной с финансированием терроризма (TERROR)',
                            tooltip: {
                                text: "Признак операции, связанной с финансированием терроризма:<br>•	В случае приостановления операции с денежными средствами (далее - ДС) или иным имуществом, если хотя бы одной из сторон является организация или физическое лицо (далее - ФЛ), в отношении которых имеются полученные в установленном в соответствии с пунктом 2 статьи 6 Закона № 115-ФЗ порядке сведения об их участии в террористической деятельности, либо юридическое лицо (далее - ЮЛ), прямо или косвенно находящееся в собственности или под контролем таких организаций или лица, либо ФЛ или ЮЛ, действующее от имени или по указанию таких организаций или лица (значение - 1)<br>•	В случае совершения операции по зачислению ДС, поступивших на счет ФЛ или ЮЛ, если хотя бы одной из сторон является организация или ФЛ, в отношении которых имеются полученные в установленном в соответствии с пунктом 2 статьи 6 Закона № 115-ФЗ порядке сведения об их участии в террористической деятельности, либо ЮЛ, прямо или косвенно находящееся в собственности или под контролем таких организаций или лица, либо ФЛ или ЮЛ, действующее от имени или по указанию таких организаций или лица (значение для списка (значение - 2);<br>•	В ином случае – 0."
                            },
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{id}',
                                '</tpl>'
                            ],
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{TERRORHardStore}',
                                value: '{OES321DetailsTable.TERROR}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    margin: '-10px 0 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            reference: 'base_PRIZ_SD',
                            width: 340,
                            labelWidth: 200,
                            labelAlign: 'right',
                            fieldLabel: 'Объект сообщения (PRIZ_SD)',
                            tooltip: {
                                text: "Объект операции:<br>0 – операция совершается с денежными средствами;<br>1 – операция совершается с иным имуществом."
                            },
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{id}',
                                '</tpl>'
                            ],
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{PRIZSDHardStore}',
                                value: '{OES321DetailsTable.PRIZ_SD}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: '<b>Реквизиты платежного документа</b>',
            border: 1,
            padding: 10,
            margin: '10px 0 0 0',
            defaults: {
                padding: '5px 0 0 0'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'base_NUM_PAY_D',
                            width: 450,
                            labelWidth: 270,
                            labelAlign: 'right',
                            fieldLabel: 'Номер платежного документа (NUM_PAY_D)',
                            tooltip: {
                                text: "Номер платежного документа, на основании которого осуществляется операция, в ином случае – '0'."
                            },
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.NUM_PAY_D}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'textfield',
                            reference: 'base_REFER_R2',
                            width: 330,
                            labelWidth: 100,
                            labelAlign: 'right',
                            fieldLabel: '(REFER_R2)',
                            tooltip: {
                                text: "Нечисловой или длинный (> 12 символов) номер платежного документа, на основании которого осуществляется операция, в ином случае - '0'."
                            },
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.REFER_R2}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'datefield',
                            reference: 'base_DATE_PAY_D',
                            width: 380,
                            labelWidth: 245,
                            labelAlign: 'right',
                            fieldLabel: 'Дата платеж. документа (DATE_PAY_D)',
                            tooltip: {
                                text: "Дата платежного документа, на основании которого осуществляется операция."
                            },
                            format: 'd.m.Y',
                            startDay: 1,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.DATE_PAY_D}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1}
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'base_SUME',
                            width: 450,
                            labelWidth: 270,
                            labelAlign: 'right',
                            fieldLabel: 'Сумма в рублях (SUME)',
                            tooltip: {
                                text: "Сумма операции в рублевом эквиваленте (по официальному курсу Банка России, действующему на дату совершения операции)."
                            },
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.SUME}'
                                // value: '{SUME_NUM}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError',
                                blur: function (field) {
                                    field.setValue(Ext.util.Format.number(String(parseFloat(field.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                }
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'label',
                            width: 330
                        },
                        {flex: 0.1},
                        {
                            xtype: 'combobox',
                            reference: 'base_METAL',
                            width: 380,
                            labelWidth: 245,
                            labelAlign: 'right',
                            fieldLabel: 'Код драгоценного металла (METAL)',
                            tooltip: {
                                text: "Код драгоценного металла, драгоценного камня, ювелирного изделия из них по Классификатору клиринговых валют – в случае указания в поле VO или DOP_V кода 5005, в ином случае – 0."
                            },
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="',
                                '<tpl if="METAL_FLAG == 0">',
                                'display:none;',
                                '</tpl>',
                                'overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{CUR_CODE} - {CUR_NAME}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{CUR_CODE}',
                                '</tpl>'
                            ],
                            displayField: 'CUR_NAME',
                            valueField: 'CUR_CODE',
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{CURStoreMetal}',
                                value: '{OES321DetailsTable.METAL}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1}
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'base_SUM',
                            width: 450,
                            labelWidth: 270,
                            labelAlign: 'right',
                            fieldLabel: 'Сумма в валюте проведения (SUM)',
                            tooltip: {
                                text: "Сумма операции в валюте ее проведения.<br>В случае проведения конверсионной операции указывается сумма иностранной валюты, покупаемой ФЛ."
                            },
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.SUM}'
                                // value: '{SUM_NUM}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError',
                                blur: function (field) {
                                    field.setValue(Ext.util.Format.number(String(parseFloat(field.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                }
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'label',
                            width: 330
                        },
                        {flex: 0.1},
                        {
                            xtype: 'combobox',
                            reference: 'base_CURREN',
                            width: 380,
                            labelWidth: 245,
                            labelAlign: 'right',
                            fieldLabel: 'Валюта проведения (CURREN)',
                            tooltip: {
                                text: "Трехзначный цифровой код валюты операции в соответствии с Общероссийским классификатором валют.<br>В случае совершения конверсионной операции указывается код иностранной валюты, покупаемой ФЛ."
                            },
                            editable: true,
                            queryMode: 'local',
                            displayField: 'CUR_CODE',
                            valueField: 'CUR_CODE',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="',
                                '<tpl if="METAL_FLAG == 1">',
                                'display:none;',
                                '</tpl>',
                                'overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{CUR_CODE} - {CUR_NAME}</div>',
                                '</tpl>'
                            ],
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: {
                                    bindTo: '{CUR_CODE_CONVERSION}',
                                    deep: true
                                },
                                store: '{CURStore}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            maskRe: new RegExp('^[0-9]$'),
                            enforceMaxLength: true,
                            maxLength: 3,
                            vtype: 'vRegexCountries',
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1}
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'base_SUM_CON',
                            width: 450,
                            labelWidth: 270,
                            labelAlign: 'right',
                            fieldLabel: 'Сумма в валюте конверсии (SUM_CON)',
                            tooltip: {
                                text: "Для конверсионной операции указывается сумма продаваемой ФЛ иностранной валюты.<br>В ином случае - '0'."
                            },
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{OES321DetailsTable.SUM_CON}'
                                // value: '{SUM_CON_NUM}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError',
                                blur: function (field) {
                                    if(field.getValue() === 0 || field.getValue() === '0') {
                                        field.setValue(0);
                                    } else {
                                        field.setValue(Ext.util.Format.number(String(parseFloat(field.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                    }
                                }
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'label',
                            width: 330
                        },
                        {flex: 0.1},
                        {
                            xtype: 'combobox',
                            reference: 'base_CURREN_CON',
                            width: 380,
                            labelWidth: 245,
                            labelAlign: 'right',
                            fieldLabel: 'Валюта конверсии (CURREN_CON)',
                            tooltip: {
                                text: "Для конверсионной операции указывается трехзначный цифровой код продаваемой ФЛ иностранной валюты в соответствии с Общероссийским классификатором валют. В ином случае – 0."
                            },
                            editable: true,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="',
                                '<tpl if="METAL_FLAG == 1">',
                                'display:none;',
                                '</tpl>',
                                'overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{CUR_CODE} - {CUR_NAME}</div>',
                                '</tpl>'
                            ],
                            displayField: 'CUR_CODE',
                            valueField: 'CUR_CODE',
                            forceSelection: true,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{CURStoreCon}',
                                value: '{CURREN_CON_CONVERSION}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            maskRe: new RegExp('^[0-9]$'),
                            enforceMaxLength: true,
                            maxLength: 3,
                            vtype: 'vRegexCountries',
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1}
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            reference: 'base_PRIM_CONCAT',
                            itemId: 'PRIM_CONCAT_field',
                            width: '100%',
                            labelWidth: 270,
                            labelAlign: 'right',
                            fieldLabel: 'Основание совершения операции (PRIM_1) <br>(PRIM_2)',
                            tooltip: {
                                text: "Назначение платежа, основание проведения операции. Указывается содержание соответствующего поля расчетного документа, на основании которого осуществляется операция, либо текстовое описание основания проведения операции, если расчетный документ не используется при проведении операции."
                            },
                            //triggerClear: true,
                            value: ' ',
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{PRIM_CONCAT}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: '<b>Дополнительная информация о сведениях</b>',
            border: 1,
            padding: 10,
            margin: '10px 0 0 0',
            defaults: {
                padding: '5px 0 0 0'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            reference: 'base_DESCR_CONCAT',
                            itemId: 'DESCR_CONCAT_field',
                            width: '100%',
                            labelWidth: 270,
                            labelAlign: 'right',
                            fieldLabel: 'Дополнительная информация (DESCR_1) <br>(DESCR_2)',
                            tooltip: {
                                text: "Для ОПОК – информация, раскрывающая суть проводимой операции.<br>Для операций с кодом группы операций 60 – текстовое описание критерия, на основании которого операция отнесена к разряду подозрительных, а также иная информация, раскрывающая суть проводимой операции.<br>Если дополнительная информация отсутствует (за исключением операций с кодом группы операций 60) – ‘0’.<br>Для сообщений с ACTION = 3, 4 указываются причины замены, удаления записи."
                            },
                            value: ' ',
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{DESCR_CONCAT}'
                            },
                            listeners: {
                                validitychange: 'checkFieldServerError'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox'
            },
            padding: 10,
            margin: '10px 0 0 0',
            defaults: {
                border: 0
            },
            items: [
                {
                    xtype: 'textareafield',
                    reference: 'base_MANUAL_CMNT',
                    width: '100%',
                    labelWidth: 270,
                    labelAlign: 'right',
                    fieldLabel: '<b>Комментарий к сообщению</b>',
                    tooltip: {
                        text: "Комментарий для ОЭС, создаваемого вручную."
                    },
                    bind: {
                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                        value: '{OES321DetailsTable.MANUAL_CMNT}'
                    },
                    listeners: {
                        validitychange: 'checkFieldServerError'
                    },
                    errorDismissDelay: 0
                }
            ]
        }
    ]
});