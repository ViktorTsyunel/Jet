Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.BehalfModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321detailsbehalftabvm',
    data: {
        title: 'От имени и по поруч.',
        tabVM: '4'
    },
    stores: {
        // От имени и по поруч. block_nb = 4
        OES321PartyStore: {
            model: 'AML.model.OES321Party',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'OES321Party',
                    action: 'read'
                }
            }
        },
        OKATOStoreAmrS4: {
            type: 'chained',
            source: 'OKATO'
        },
        OKATOStoreAdressS4: {
            type: 'chained',
            source: 'OKATO'
        },
        DOCTYPEStoreChainedKD: {
            type: 'chained',
            source: 'DOCTYPEREF'
        },
        DOCTYPEStoreChainedVD4: {
            type: 'chained',
            source: 'DOCTYPEREF'
        }
    }
});