Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ViewClientFromSearchController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.viewclientfromsearchcontroller',
    config: {
        listen: {
            component: {
                '#cancelWindowSeachClient': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    init: function () {

    },

    onClickCancelWindow: function (button, eOpts) {
        var window = this.getView(),
            controller = this;
        window.close();
    }
});