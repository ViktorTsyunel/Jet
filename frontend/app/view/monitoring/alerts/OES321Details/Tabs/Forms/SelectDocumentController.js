Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SelectDocumentController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oes321detailspaneltabsselectdocument',
    requires: [
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#selectedDataDocument': {
                    click: 'onCopyDataToTab'
                },
                '#cancelWindow': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    init: function () {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel(),
            store = vm.getStore('DocumentListStoreForms');
        if (vm && vm.get('CUST_SEQ_ID')) {
            // загрузка справочника: // "form":"CustIdDocList","action":"readByCustId","custSeqId":179529031
            store.load({
                scope: self,
                params: {
                    custSeqId: vm.get('CUST_SEQ_ID')
                },
                callback: function(data, respond) {
                    if (data.length <= 0) {
                        Ext.Msg.alert('Внимание!', 'Нет данных для обработки.');
                        setTimeout(function () {
                            window.close();
                        }, 500);
                    }
                }
            });
        }
    },

    onCopyDataToTab: function() {
        var self = this,
            window = this.getView(),
            documentSelection = self.lookupReference('documentSelectionGrid').getSelectionModel().getSelection(),
            vm = self.getViewModel(),
            data, fieldInto, setVal,
            typeBlock = (vm) ? vm.get('typeBlock') : false,
            objKeyPersonality = {
                KD: 'CBRF_DOC_TYPE_CD',
                SD: 'SERIES_ID',
                VD1: 'NO_ID',
                VD2: 'ISSUED_BY_TX',
                VD3: 'ISSUE_DT'
            },
            objKeyRightToLive = {
                VD4: 'CBRF_DOC_TYPE_CD',
                VD5: '', // data.get('SERIES_ID') + data.get('NO_ID'));
                VD6: 'ISSUE_DT',
                VD7: 'END_DT'
            },
            objKeyMigrationCard = {
                MC1: 'NO_ID',
                MC2: 'ISSUE_DT',
                MC3: 'END_DT'
            },
            defKeysDate = ['VD3', 'VD6', 'VD7', 'MC2', 'MC3'],
            defVal = '0',
            defDate = new Date('2099', '0', '1', '0', '0', '0'),
            vmTab = (vm) ? vm.getParent() : false,
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            tabVmS = (vmTab) ? vmTab.getStore('OES321PartyStore') : false,
            tabVmStore = (tabVmS) ? tabVmS : storeOES321Details,
            tabD = (vmTab) ? vmTab.get('OES321PartyData') : false,
            tabDT = (vmTab) ? vmTab.get('OES321DetailsTable') : false,
            tabData = (tabD) ? tabD.getData() : tabDT,
            record;

        if (vm && vmTab && documentSelection.length == 1 && numberTab) {
            data = documentSelection[0];
            switch (true) {
                case (typeBlock === 'personality'):
                    Ext.Object.each(objKeyPersonality, function (key, value, myself) {
                        fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + key);
                        switch (true) {
                            case (Ext.Array.indexOf(defKeysDate, key) !== -1):
                                setVal = (data.get(value)) ? data.get(value) : defDate;
                                break;
                            default:
                                setVal = (data.get(value)) ? data.get(value) : defVal;
                                break;
                        }
                        fieldInto.setValue(setVal);
                        if (tabData) {
                            tabData[key] = setVal; 
                        }
                    });
                    break;
                case (typeBlock === 'right-to-live'):
                    Ext.Object.each(objKeyRightToLive, function (key, value, myself) {
                        fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + key);
                        if (key === 'VD5') {
                            if ((!data.get('SERIES_ID') || data.get('SERIES_ID') == '') && (!data.get('NO_ID') || data.get('NO_ID') == '')) {
                                setVal = defVal;
                            } else {
                                setVal = data.get('SERIES_ID') + data.get('NO_ID');
                            }
                            setVal = (data.get(value)) ? data.get(value) : defVal;
                        } else {
                            switch (true) {
                                case (Ext.Array.indexOf(defKeysDate, key) !== -1):
                                    setVal = (data.get(value)) ? data.get(value) : defDate;
                                    break;
                                default:
                                    setVal = (data.get(value)) ? data.get(value) : defVal;
                                    break;
                            }
                        }
                        fieldInto.setValue(setVal);
                        if (tabData) {
                            tabData[key] = setVal;
                        }
                    });
                    break;
                case (typeBlock === 'migration-card'):
                    Ext.Object.each(objKeyMigrationCard, function (key, value, myself) {
                        fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + key);
                        switch (true) {
                            case (Ext.Array.indexOf(defKeysDate, key) !== -1):
                                setVal = (data.get(value)) ? data.get(value) : defDate;
                                break;
                            default:
                                setVal = (data.get(value)) ? data.get(value) : defVal;
                                break;
                        }
                        fieldInto.setValue(setVal);
                        if (tabData) {
                            tabData[key] = setVal;
                        }
                    });
                    break;
            }
            tabVmStore.getAt(0).set(tabData);
            record = tabVmStore.getAt(0);
            record.set(tabData);
            window.close();
        } else if (documentSelection.length <= 0) {
            Ext.Msg.alert('Внимание!', 'Нет выделенного значения.');
            return false;
        } else if (documentSelection.length > 1) {
            Ext.Msg.alert('Внимание!', 'Выделенно больше 1 значения.');
            return false;
        }
    },

    onClickCancelWindow: function (button, eOpts) {
        var window = this.getView(),
            controller = this;
        window.close();
    }
});
