Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ViewClientFromSearch', {
    extend: 'Ext.window.Window',
    xtype: 'app-oes321detailspanel-viewclientfromsearch',
    controller: 'viewclientfromsearchcontroller',
    viewModel: {
        type: 'payertabviewmodel'
    },
    cls: 'app-oes321detailspanel-viewclientfromsearch',
    layout: 'vbox',
    padding: 5,
    defaults: {
        width: '100%',
        border: 0
    },
    // tabP: 1,
    reference: 'ref-app-oes321detailspanel-viewclientfromsearch',
    items: [
        {
            xtype: 'fieldset',
            title: '<b>Общие сведения об участнике</b>',
            border: 1,
            margin: '0 0 15px 0',
            defaults: {
                padding: '5px 0 0 0'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'TU_1',
                            width: 225,
                            labelWidth: 160,
                            labelAlign: 'right',
                            editable: false,
                            matchFieldWidth: false,
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Тип участника (TU)',
                                value: '{TU_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1}
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'NAMEU_1',
                            width: '100%',
                            labelWidth: 250,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Наименование ЮЛ или ФИО ФЛ (NAMEU)',
                                value: '{NAMEU_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'ND_1',
                            width: 300,
                            labelWidth: 160,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'ИНН (ND)',
                                value: '{ND_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {flex: 0.1},
                        {
                            xtype: 'datefield',
                            reference: 'GR_1',
                            width: 250,
                            labelWidth: 140,
                            format: 'd.m.Y',
                            startDay: 1,
                            setFieldLabel: common.util.component.setFieldLabel,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Дата рег./ рожд. (GR)',
                                value: '{GR_1}'
                            },
                            allowBlank: false,
                            errorDismissDelay: 0
                        },
                        //{flex: 0.1},
                        {
                            xtype: 'textfield',
                            reference: 'RG_1',
                            width: 300,
                            labelWidth: 140,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Рег. номер (RG)',
                                value: '{RG_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        },
                        //{flex: 0.1},
                        {
                            xtype: 'textfield',
                            reference: 'BP_1',
                            flex: 1,
                            labelWidth: 150,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Место рождения (BP)',
                                value: '{BP_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    rawWidth: 0,
                    border: 0
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            defaults: {
                border: 0
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: '<b>Информация о месте регистрации/жительства</b>',
                    flex: 1,
                    border: 1,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'KODCR_1_1',
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    flex: 1,
                                    editable: false,
                                    displayField: 'ISO_NUMERIC_CD',
                                    triggerClear: true,
                                    valueField: 'ISO_NUMERIC_CD',
                                    matchFieldWidth: false,
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Код места рег./жит. (KODCR)',
                                        value: '{KODCR_1_1}'
                                    },
                                    enforceMaxLength:true,
                                    maxLength: 3//,
                                    // allowBlank: false
                                },
                                {
                                    xtype: 'container',
                                    width: 10
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 'KODCR_2_1',
                                    width: 55,
                                    enforceMaxLength:true,
                                    bind: {
                                        readOnly: true,
                                        value: '{KODCR_2_1}'
                                    },
                                    maxLength: 2,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 'AMR_S_1',
                                    flex: 1,
                                    labelWidth: 140,
                                    labelAlign: 'right',
                                    editable: false,
                                    displayField: 'OKATO_CD',
                                    valueField: 'OKATO_CD',
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'ОКАТО (AMR_S)',
                                        value: '{AMR_S_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'AMR_R_1',
                                    flex: 1,
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Район/Область (AMR_R)',
                                        value: '{AMR_R_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'AMR_G_1',
                                    flex: 1,
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Город/Нас. пункт (AMR_G)',
                                        value: '{AMR_G_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'AMR_U_1',
                                    flex: 1,
                                    labelWidth: 180,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Ул. (AMR_U)',
                                        value: '{AMR_U_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'column'
                            },
                            padding: 0,
                            defaults: {
                                border: 0,
                                margin: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'AMR_D_1',
                                    width: 190,
                                    labelWidth: 125,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Д. (AMR_D)',
                                        value: '{AMR_D_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {columnWidth: 0.5},
                                {
                                    xtype: 'textfield',
                                    reference: 'AMR_K_1',
                                    width: 190,
                                    labelWidth: 125,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Корп./Стр. (AMR_K)',
                                        value: '{AMR_K_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {columnWidth: 0.5},
                                {
                                    xtype: 'textfield',
                                    reference: 'AMR_O_1',
                                    width: 190,
                                    labelWidth: 125,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Оф./Кв. (AMR_O)',
                                        value: '{AMR_O_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            rawWidth: 0,
                            border: 0
                        }
                    ]
                },
                {width: 10},
                {
                    xtype: 'fieldset',
                    title: '<b>Информация о месте нахождения/пребывания</b>',
                    flex: 1,
                    border: 1,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'KODCN_1_1',
                                    labelWidth: 200,
                                    labelAlign: 'right',
                                    // flex: 1,
                                    width: 250,
                                    editable: false,
                                    displayField: 'ISO_NUMERIC_CD',
                                    triggerClear: true,
                                    valueField: 'ISO_NUMERIC_CD',
                                    matchFieldWidth: false,
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Код места нах./Гражд. (KODCN)',
                                        value: '{KODCN_1_1}'
                                    },
                                    enforceMaxLength:true,
                                    maxLength: 3
                                    // allowBlank: false
                                },
                                {
                                    xtype: 'container',
                                    width: 8
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 'KODCN_2_1',
                                    width: 55,
                                    enforceMaxLength:true,
                                    bind: {
                                        readOnly: true,
                                        value: '{KODCN_2_1}'
                                    },
                                    maxLength: 2,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 'ADRESS_S_1',
                                    flex: 1,
                                    labelWidth: 130,
                                    labelAlign: 'right',
                                    editable: false,
                                    displayField: 'OKATO_CD',
                                    valueField: 'OKATO_CD',
                                    matchFieldWidth: false,
                                    triggerClear: true,
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'ОКАТО (ADRESS_S)',
                                        value: '{ADRESS_S_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'ADRESS_R_1',
                                    flex: 1,
                                    labelWidth: 190,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Район/Область (ADRESS_R)',
                                        value: '{ADRESS_R_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'ADRESS_G_1',
                                    flex: 1,
                                    labelWidth: 190,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Город/Нас. пункт (ADRESS_G)',
                                        value: '{ADRESS_G_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'ADRESS_U_1',
                                    flex: 1,
                                    labelWidth: 190,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Ул. (ADRESS_U)',
                                        value: '{ADRESS_U_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'column'
                            },
                            padding: 0,
                            defaults: {
                                border: 0,
                                margin: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'ADRESS_D_1',
                                    width: 165,
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Д. (ADRESS_D)',
                                        value: '{ADRESS_D_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {columnWidth: 0.5},
                                {
                                    xtype: 'textfield',
                                    reference: 'ADRESS_K_1',
                                    width: 210,
                                    labelWidth: 145,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Корп./Стр. (ADRESS_K)',
                                        value: '{ADRESS_K_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {columnWidth: 0.5},
                                {
                                    xtype: 'textfield',
                                    reference: 'ADRESS_O_1',
                                    width: 195,
                                    labelWidth: 130,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Оф./Кв. (ADRESS_O)',
                                        value: '{ADRESS_O_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            rawWidth: 0,
                            border: 0
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: '<b>Документ, удостоверяющий личность</b>',
            border: 1,
            margin: '5px 0 20px 0',
            defaults: {
                padding: '5px 0 0 0'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'KD_1',
                            width: 420,
                            labelWidth: 260,
                            labelAlign: 'right',
                            editable: false,
                            displayField: 'DOC_TYPE_CD',
                            valueField: 'DOC_TYPE_CD',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Документ, удостоверяющий личность (KD)',
                                value: '{KD_1}'
                            },
                            enforceMaxLength:true,
                            maxLength: 2,
                            // allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'textfield',
                            reference: 'SD_1',
                            flex: 1,
                            labelWidth: 200,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'ОКПО/Серия документа (SD)',
                                value: '{SD_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'textfield',
                            reference: 'VD1_1',
                            width: 280,
                            labelWidth: 170,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Номер документа (VD)',
                                value: '{VD1_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'VD2_1',
                            flex: 2,
                            labelWidth: 260,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Орган, выдавший документ (VD2)',
                                value: '{VD2_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        },
                        {
                            xtype: 'datefield',
                            reference: 'VD3_1',
                            width: 340,
                            labelWidth: 170,
                            format: 'd.m.Y',
                            startDay: 1,
                            setFieldLabel: common.util.component.setFieldLabel,
                            labelAlign: 'right',
                            bind: {
                                readOnly: true,
                                fieldLabel: 'Дата выдачи (VD3)',
                                value: '{VD3_1}'
                            },
                            // allowBlank: false,
                            errorDismissDelay: 0
                        }
                    ]
                },
                {
                    rawWidth: 0,
                    border: 0
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            defaults: {
                border: 0
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: '<b>Документ, подтверждающий право на пребывание (проживание) в РФ</b>',
                    flex: 1,
                    border: 1,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'VD4_1',
                                    flex: 1,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    editable: false,
                                    displayField: 'DOC_TYPE_CD',
                                    valueField: 'DOC_TYPE_CD',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Код вида документа (VD4)',
                                        value: '{VD4_1}'
                                    },
                                    enforceMaxLength:true,
                                    maxLength: 2,
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'textfield',
                                    reference: 'VD5_1',
                                    flex: 1,
                                    labelWidth: 160,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Серия и номер (VD5)',
                                        value: '{VD5_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    reference: 'VD6_1',
                                    width: 340,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Срок действия с (VD6)',
                                        value: '{VD6_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'datefield',
                                    reference: 'VD7_1',
                                    //width: 240,
                                    flex: 1,
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    labelWidth: 70,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'по (VD7)',
                                        value: '{VD7_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            width: 10
                        }
                    ]
                },
                {width: 10},
                {
                    xtype: 'fieldset',
                    title: '<b>Миграционная карта</b>',
                    flex: 1,
                    border: 1,
                    margin: '5px 0 20px 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    reference: 'MC1_1',
                                    flex: 1,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Номер (MC1)',
                                        value: '{MC1_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    reference: 'MC2_1',
                                    width: 340,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'Срок пребывания с (MC2)',
                                        value: '{MC2_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                },
                                {
                                    xtype: 'datefield',
                                    reference: 'MC3_1',
                                    // width: 250,
                                    flex: 1,
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    setFieldLabel: common.util.component.setFieldLabel,
                                    labelWidth: 80,
                                    labelAlign: 'right',
                                    bind: {
                                        readOnly: true,
                                        fieldLabel: 'по (MC3)',
                                        value: '{MC3_1}'
                                    },
                                    // allowBlank: false,
                                    errorDismissDelay: 0
                                }
                            ]
                        },
                        {
                            rawWidth: 0,
                            border: 0
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'cancelWindowSeachClient',
            text: 'Закрыть'
        }
    ]
});