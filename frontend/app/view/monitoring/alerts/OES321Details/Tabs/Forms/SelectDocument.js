Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SelectDocument', {
    extend: 'Ext.window.Window',
    xtype: 'oes321detailspanel-tabs-select-document-form',
    controller: 'oes321detailspaneltabsselectdocument',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oes321detailspaneltabsselectdocumentvm'
    },
    bind: {
        title: '{title}'
    },
    width: "80%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{DocumentListStoreForms}'
                    },
                    listeners : {
                        itemdblclick: function(grid, rowIndex, columnIndex, e) {
                            var view = (grid.up()) ? ((grid.up().up()) ? ((grid.up().up().up()) ? grid.up().up().up() : false) : false) : false,
                                controller = (view) ? view.getController() : false;
                            if (controller) {
                                controller.onCopyDataToTab();
                            }
                        }
                    },
                    reference: 'documentSelectionGrid',
                    /*

                     {
                     "DOC_TYPE_CD":"ФЛ: Паспорт гражданина СССР",
                     "STATUS_TX":"Действующий",
                     "SERIES_ID":"X-ВГ",
                     "NO_ID":"703699",
                     "ISSUE_DT":"16.04.1982",
                     "END_DT":"",
                     "ISSUED_BY_TX":"ОВД Г.БЕРЕЗНИКИ",
                     "ISSUE_DPT_CD":"",
                     "SRC_SYS_CD":"EKS",
                     "SRC_CHANGE_DT":"",
                     "SRC_USER_NM":"",
                     "CUST_DOC_SEQ_ID":11309554,
                     "CBRF_DOC_TYPE_CD":"28"
                     }

                     */
                    columns: [
                        {
                            text: 'Тип документа',
                            stateId: 'DOC_TYPE_CD',
                            dataIndex: 'DOC_TYPE_CD',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Серия',
                            stateId: 'SERIES_ID',
                            dataIndex: 'SERIES_ID',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {

                            text: 'Номер',
                            stateId: 'NO_ID',
                            dataIndex: 'NO_ID',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            text: 'Дата выдачи',
                            stateId: 'ISSUE_DT',
                            dataIndex: 'ISSUE_DT',
                            tdCls: 'v-align-middle',
                            minWidth: 115,
                            flex: 1
                        },
                        {
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            text: 'Дата окончания',
                            stateId: 'END_DT',
                            dataIndex: 'END_DT',
                            tdCls: 'v-align-middle',
                            minWidth: 115,
                            flex: 1
                        },
                        {
                            text: 'Кем выдан',
                            stateId: 'ISSUED_BY_TX',
                            dataIndex: 'ISSUED_BY_TX',
                            tdCls: 'v-align-middle',
                            flex: 2
                        },
                        {
                            text: 'Система источник',
                            stateId: 'SRC_CD',
                            dataIndex: 'SRC_CD',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Дата изменения',
                            stateId: 'SRC_CHANGE_DT',
                            dataIndex: 'SRC_CHANGE_DT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Пользователь, внесший изменения',
                            stateId: 'SRC_USER_NM',
                            dataIndex: 'SRC_USER_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'selectedDataDocument',
            text: 'Выбрать'
        },
        {
            itemId: 'cancelWindow',
            text: 'Закрыть'
        }
    ]
});