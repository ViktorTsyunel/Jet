Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.AddressSelection', {
    extend: 'Ext.window.Window',
    xtype: 'oes321detailspanel-tabs-address-selection-form',
    controller: 'oes321detailspaneltabsaddressselection',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oes321detailspaneltabsaddressselectionvm'
    },
    bind: {
        title: '{title}'
    },
    // width: "80%",
    width: 1000,
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{AddressListStoreForms}'
                    },
                    listeners : {
                        itemdblclick: function(grid, rowIndex, columnIndex, e) {
                            var view = (grid.up()) ? ((grid.up().up()) ? ((grid.up().up().up()) ? grid.up().up().up() : false) : false) : false,
                                controller = (view) ? view.getController() : false;
                            if (controller) {
                                controller.onCopyDataToTab();
                            }
                        }
                    },
                    reference: 'addressSelectionGrid',
                    /*

                     "USAGE_CD" : "адрес пребывания",
                     "COUNTRY_CD" : "РОССИЯ",
                     "STATE_NM" : "г Москва",
                     "DISTRICT_NM" : "Район",
                     "CITY_NM" : "Город",
                     "SETTLM_NM" : "Населенный пункт",
                     "STREET_NAME_TX" : "Улица",
                     "HOUSE_NUMBER_TX" : "Дом",
                     "BUILDING_NUMBER_TX" : "Строение/Корпус",
                     "FLAT_NUMBER_TX" : "Квартира/Офис",
                     "STATUS_TX" : "Неизвестен",
                     "START_DT" : "",
                     "END_DT" : "",
                     "SRC_SYS_CD" : "EKS",
                     "SRC_CHANGE_DT" : "",
                     "SRC_USER_NM" : "",
                     "CUST_ADDR_SEQ_ID" : 13599488,
                     "ISO_NUMERIC_CD" : "643",
                     "OKATO_CD" : "45"

                    */
                    columns: [
                        {
                            text: 'Страна',
                            stateId: 'COUNTRY_CD',
                            dataIndex: 'COUNTRY_CD',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Регион',
                            stateId: 'STATE_NM',
                            dataIndex: 'STATE_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Район',
                            stateId: 'DISTRICT_NM',
                            dataIndex: 'DISTRICT_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Город',
                            stateId: 'CITY_NM',
                            dataIndex: 'CITY_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Населенный пункт',
                            stateId: 'SETTLM_NM',
                            dataIndex: 'SETTLM_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Улица',
                            stateId: 'STREET_NAME_TX',
                            dataIndex: 'STREET_NAME_TX',
                            tdCls: 'v-align-middle',
                            flex: 3
                        },
                        {
                            text: 'Дом',
                            stateId: 'HOUSE_NUMBER_TX',
                            dataIndex: 'HOUSE_NUMBER_TX',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Строение/Корпус',
                            stateId: 'BUILDING_NUMBER_TX',
                            dataIndex: 'BUILDING_NUMBER_TX',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Квартира/Офис',
                            stateId: 'BUILDING_NUMBER_TX',
                            dataIndex: 'FLAT_NUMBER_TX',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Система источник',
                            stateId: 'SRC_CD',
                            dataIndex: 'SRC_CD',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Дата изменения',
                            stateId: 'SRC_CHANGE_DT',
                            dataIndex: 'SRC_CHANGE_DT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Пользователь, внесший изменения',
                            stateId: 'SRC_USER_NM',
                            dataIndex: 'SRC_USER_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'selectedDataAddress',
            text: 'Выбрать'
        },
        {
            itemId: 'cancelWindow',
            text: 'Закрыть'
        }
    ]
});