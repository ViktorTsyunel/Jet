Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ViewFSearchControll', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.viewfsearchcontroll',
    config: {
        listen: {
            component: {
                '#cancelWindowSeachClient': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    init: function (tab) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel(),
            dataCh, respObj,
            vmTab = (vm) ? vm.getParent() : false,
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            vmData = (vm) ? vm.getData() : false,
            store = (vmTab) ? vmTab.get('OES321PartyData') : false,
            alertId = (vmTab) ? vmTab.get('alertId') : null,
            vCustSegId = (vmData.CUST_SEQ_ID_1) ? parseInt(vmData.CUST_SEQ_ID_1, 10) : false,
            addressListStore = self.getStore('addressListStore'),
            idDocsListStore = self.getStore('idDocsListStore'),
            contactsListStore = self.getStore('contactsListStore'),
            accountsListStore = self.getStore('accountsListStore'),
            emailsListStore = self.getStore('emailsListStore'),
            linksListStore = self.getStore('linksListStore'),
            params = {
                form: 'CustDetails',
                action: 'readByCustId',
                cust_seq_id: vCustSegId
            },
            addressListStoreParams = {
                form: 'CustAddrList',
                action: 'readByCustId',
                //custSeqId: 179643230
                custSeqId: vCustSegId
            },
            contactsListStoreParams = {
                form: 'CustPhonList',
                action: 'readByCustId',
                custSeqId: vCustSegId
            },
            accountsListStoreParams = {
                form: 'CustAcctList',
                action: 'readByCustId',
                custSeqId: vCustSegId
            },
            emailsListStoreParams = {
                form: 'CustEmailList',
                action: 'readByCustId',
                custSeqId: vCustSegId
            },
            linksListStoreParams = {
                form: 'CustCustList',
                action: 'readByCustId',
                custSeqId: vCustSegId
            },
            idDocsListStoreParams = {
                form: 'CustIdDocList',
                action: 'readByCustId',
                custSeqId: vCustSegId
            };

        // добавим loading обработку:
        // if(!common.globalinit.loadingMaskCmp) {
        //     common.globalinit.loadingMaskCmp = vm.getParent().getParent().getView();
        // }

        // запрос для вкладки плательщик:
        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                if (!Ext.isEmpty(responseObj.data)) {
                    vm.set('tplData', responseObj.data[0]);
                }
            }
        });
        // Подгрузка хранилища все адреса
        addressListStore.load({
            scope: self,
            params: addressListStoreParams
        });
        // Подгрузка хранилища все удостоверяющие документы
        idDocsListStore.load({
            scope: self,
            params: idDocsListStoreParams
        });
        // Подгрузка хранилища контакты
        contactsListStore.load({
            scope: self,
            params: contactsListStoreParams
        });
        // Подгрузка хранилища счета
        accountsListStore.load({
            scope: self,
            params: accountsListStoreParams
        });
        // Подгрузка хранилища электронные адреса
        emailsListStore.load({
            scope: self,
            params: emailsListStoreParams
        });
        // Подгрузка хранилища связи
        linksListStore.load({
            scope: self,
            params: linksListStoreParams
        });
    },

    tableToggle: function (e) {
        var self = this,
            vm = self.getViewModel();

        // Сведения о плательщике
        if (e.target.closest('.payer-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('isPayerInfoExpanded') !== 'none') {
                vm.set('isPayerInfoExpanded', 'none');
                vm.set('payerInfoExpandedClass', 'closed');
            }
            else {
                vm.set('payerInfoExpandedClass', 'opened');
                vm.set('isPayerInfoExpanded', 'table-row');
            }
        }
        // Адрес регистрации
        if (e.target.closest('.registration-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('isRegInfoExpanded') !== 'none') {
                vm.set('isRegInfoExpanded', 'none');
                vm.set('regInfoExpandedClass', 'closed');
            }
            else {
                vm.set('regInfoExpandedClass', 'opened');
                vm.set('isRegInfoExpanded', 'table-row');
            }
        }
        // Адрес местонахождения
        if (e.target.closest('.location-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('isLocInfoExpanded') !== 'none') {
                vm.set('isLocInfoExpanded', 'none');
                vm.set('locInfoExpandedClass', 'closed');
            }
            else {
                vm.set('locInfoExpandedClass', 'opened');
                vm.set('isLocInfoExpanded', 'table-row');
            }
        }
        // Удостоверяющий документ
        if (e.target.closest('.document-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('isDocInfoExpanded') !== 'none') {
                vm.set('isDocInfoExpanded', 'none');
                vm.set('docInfoExpandedClass', 'closed');
            }
            else {
                vm.set('docInfoExpandedClass', 'opened');
                vm.set('isDocInfoExpanded', 'table-row');
            }
        }
        // Все адреса
        if (e.target.closest('.addresses-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('addrInfoExpandedClass') !== 'closed') {
                vm.set('addrInfoExpandedClass', 'closed');
                vm.set('isAddrInfoExpanded', false);
            } else {
                vm.set('addrInfoExpandedClass', 'opened');
                vm.set('isAddrInfoExpanded', true);
            }
        }
        // Все удостоверяющие документы
        if (e.target.closest('.id-documents-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('idDocsInfoExpandedClass') !== 'closed') {
                vm.set('idDocsInfoExpandedClass', 'closed');
                vm.set('isIdDocsInfoExpanded', false);
            } else {
                vm.set('idDocsInfoExpandedClass', 'opened');
                vm.set('isIdDocsInfoExpanded', true);
            }
        }
        // Контакты
        if (e.target.closest('.contacts-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('contactsInfoExpandedClass') !== 'closed') {
                vm.set('contactsInfoExpandedClass', 'closed');
                vm.set('isContactsInfoExpanded', false);
            } else {
                vm.set('contactsInfoExpandedClass', 'opened');
                vm.set('isContactsInfoExpanded', true);
            }
        }
        // accounts
        if (e.target.closest('.accounts-info') && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('accountsExpandedClass') !== 'closed') {
                vm.set('accountsExpandedClass', 'closed');
                vm.set('isAccountsExpanded', false);
            } else {
                vm.set('accountsExpandedClass', 'opened');
                vm.set('isAccountsExpanded', true);
            }
        }
        // Электронные адреса
        if (e.target.closest('.emails-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('emailsInfoExpandedClass') !== 'closed') {
                vm.set('emailsInfoExpandedClass', 'closed');
                vm.set('isEmailsInfoExpanded', false);
            } else {
                vm.set('emailsInfoExpandedClass', 'opened');
                vm.set('isEmailsInfoExpanded', true);
            }
        }
        // Связи
        if (e.target.closest('.links-info') !== -1 && e.target.className.indexOf('arrow-ico') !== -1) {
            if (vm.get('linksInfoExpandedClass') !== 'closed') {
                vm.set('linksInfoExpandedClass', 'closed');
                vm.set('isLinksInfoExpanded', false);
            } else {
                vm.set('linksInfoExpandedClass', 'opened');
                vm.set('isLinksInfoExpanded', true);
            }
        }
    },

    addressListStoreReady: function (store) {
        var self = this,
            vm = self.getViewModel(),
            storeCount = store.getCount();

        vm.set('allAddressesCount', storeCount);
        if (storeCount > 0) {
            vm.set('addrInfoExpandedClass', 'opened');
            vm.set('isAddrInfoExpanded', true);
        }
    },

    idDocsListStoreReady: function (store) {
        var self = this,
            vm = self.getViewModel(),
            storeCount = store.getCount();

        vm.set('allIdDocsCount', storeCount);
        if (storeCount > 0) {
            vm.set('idDocsInfoExpandedClass', 'opened');
            vm.set('isIdDocsInfoExpanded', true);
        }
    },

    contactsListStoreReady: function (store) {
        var self = this,
            vm = self.getViewModel(),
            storeCount = store.getCount();

        vm.set('allContactsCount', storeCount);
        if (storeCount > 0) {
            vm.set('contactsInfoExpandedClass', 'opened');
            vm.set('isContactsInfoExpanded', true);
        }
    },

    accountsListStoreReady: function (store) {
        var self = this,
            vm = self.getViewModel(),
            storeCount = store.getCount();

        vm.set('allAccounts', storeCount);
        if (storeCount > 0) {
            vm.set('accountsExpandedClass', 'opened');
            vm.set('isAccountsExpanded', true);
        }
    },

    emailsListStoreReady: function (store) {
        var self = this,
            vm = self.getViewModel(),
            storeCount = store.getCount();

        vm.set('allEmailsCount', storeCount);
        if (storeCount > 0) {
            vm.set('emailsInfoExpandedClass', 'opened');
            vm.set('isEmailsInfoExpanded', true);
        }
    },

    linksListStoreReady: function (store) {
        var self = this,
            vm = self.getViewModel(),
            storeCount = store.getCount();

        vm.set('allLinksCount', storeCount);
        if (storeCount > 0) {
            vm.set('linksInfoExpandedClass', 'opened');
            vm.set('isLinksInfoExpanded', true);
        }
    },

    onClickCancelWindow: function (button, eOpts) {
        var window = this.getView(),
            controller = this;
        window.close();
    }
});
