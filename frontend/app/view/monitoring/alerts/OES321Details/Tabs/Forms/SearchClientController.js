Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SearchClientController', {
    extend: 'AML.view.main.BaseFormController', // BaseListController
    alias: 'controller.oes321detailspaneltabssearchclient',
    requires: [
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#selectItemSeachClinet': {
                    click: 'onSelectItemSeachClinet'
                },
                '#linkItemSeachClinet': {
                    click: 'onLinkItemSeachClinet'
                },
                '#cancelWindowSeachClinet': {
                    click: 'onClickCancelWindow'
                },
                '#recForPreview': {
                    click: 'openRecordForReview'
                }
            }
        }
    },

    init: function () {
        var self = this,
            tu, fio, kd,
            window = this.getView(),
            vm = self.getViewModel(),
            tabVM = (vm) ? vm.getParent() : false,
            store = (tabVM) ? tabVM.get('OES321PartyData') : false;
        if (store) {
            // Если TU=1, то Тип клиента = ЮЛ, если TU=0,2,4, то Тип клиента=ФЛ, если TU=3, то Тип клиента=ИП.
            if (!store.get('TU')) {
                tu = 0;
            } else {
                tu = parseInt(store.get('TU'), 10);
            }
            switch (true) {
                case (tu === 1): // Только для ЮЛ[ TU=1 ], ИП[ TU=3 ].
                    vm.set('record.tu', tu);
                    if (store.get('RG') && store.get('RG') !== '' && store.get('RG') !== '0') {
                        vm.set('record.rg', Ext.util.Format.trim(store.get('RG')));
                        vm.set('IS_CHECK.IS_RG', true);
                    }
                    if (tu && tu === 1 && store.get('SD') && store.get('SD') !== '' && store.get('SD') !== '0') {
                        vm.set('record.okpo', Ext.util.Format.trim(store.get('SD')));
                        vm.set('IS_CHECK.IS_SD_OKPO', true);
                    }
                    break;
                case  (tu === 3):
                    vm.set('record.tu', tu);
                    if (store.get('RG') && store.get('RG') !== '' && store.get('RG') !== '0') {
                        vm.set('record.rg', Ext.util.Format.trim(store.get('RG')));
                        vm.set('IS_CHECK.IS_RG', true);
                    }
                    if (tu && tu === 1 && store.get('SD') && store.get('SD') !== '' && store.get('SD') !== '0') {
                        vm.set('record.okpo', Ext.util.Format.trim(store.get('SD')));
                        vm.set('IS_CHECK.IS_SD_OKPO', true);
                    }
                    if (tu && tu === 3) {
                        vm.set('record.okpo', '');
                        vm.set('IS_CHECK.IS_SD_OKPO', false);
                    }
                    if (store.get('NAMEU') && store.get('NAMEU') !== '' && store.get('NAMEU') !== '0') {
                        vm.set('record.last_nm', Ext.util.Format.trim(store.get('NAMEU').split(' ')[0]));
                        if (store.get('NAMEU').split(' ').length >= 2) {
                            vm.set('record.first_nm', Ext.util.Format.trim(store.get('NAMEU').split(' ')[1]));
                        }
                        fio = store.get('NAMEU').split(' ');
                        if (fio.length > 2) {
                            fio.shift();
                            fio.shift();
                            var newFio = fio.map(function (val) {
                                return Ext.util.Format.trim(val);
                            });
                            vm.set('record.midl_nm', newFio.join(' '));
                        }
                        vm.set('IS_CHECK.IS_FIO', true);
                    }
                    break;
                case (tu === 0 || tu === 2 || tu === 4): // TU=0,2,4, то Тип клиента=ФЛ
                    vm.set('record.tu', 2);
                    if (tu === 3) vm.set('record.tu', 3);
                    if (store.get('NAMEU') && store.get('NAMEU') !== '' && store.get('NAMEU') !== '0') {
                        vm.set('record.last_nm', Ext.util.Format.trim(store.get('NAMEU').split(' ')[0]));
                        if (store.get('NAMEU').split(' ').length >= 2) {
                            vm.set('record.first_nm', Ext.util.Format.trim(store.get('NAMEU').split(' ')[1]));
                        }
                        fio = store.get('NAMEU').split(' ');
                        if (fio.length > 2) {
                            fio.shift();
                            fio.shift();
                            var newFioNUI = fio.map(function (val) {
                                return Ext.util.Format.trim(val);
                            });
                            vm.set('record.midl_nm', newFioNUI.join(' '));
                        }
                        vm.set('IS_CHECK.IS_FIO', true);
                    }
                    break;
            }
            // Только для ФЛ[ TU=0,2,4 ], ИП[ TU=3 ].
            if ((tu === 0 || tu === 2 || tu === 4 || tu === 3)) {
                if (new Date('2099', '0', '1', '0', '0', '0').toDateString() !== store.get('GR').toDateString() && store.get('GR') && store.get('GR') !== ''
                    && store.get('NAMEU') && store.get('NAMEU') !== '' && store.get('NAMEU') !== '0') {
                    vm.set('record.gr', store.get('GR'));
                }
                if (store.get('VD1') !== '' && store.get('VD1') !== '0') {
                    vm.set('record.vd1', Ext.util.Format.trim(store.get('VD1')));
                }
                if (store.get('SD') !== '' && store.get('SD') !== '0') {
                    vm.set('record.series', Ext.util.Format.trim(store.get('SD')));
                }
            }

            kd = parseInt(store.get('KD'), 10);
            if (kd !== '' && kd > 0) {
                vm.set('record.kd', kd);
                vm.set('IS_CHECK.IS_KD', true);
            } else {
                // Только для ФЛ[ TU=0,2,4 ], ИП[ TU=3 ].
                if (tu === 0 || tu === 2 || tu === 4 || tu === 3) {
                    // «Виды документов по 321-П». По умолчанию – 21 - Паспорт гражданина РФ.
                    vm.set('record.kd', 21);
                } else {
                    vm.set('record.kd', 0);
                }
            }

            vm.set('record.searchType', 'ALL');

            if (store.get('ND') && store.get('ND') !== '' && store.get('ND') !== '0') {
                vm.set('record.nd', Ext.util.Format.trim(store.get('ND')));
                vm.set('IS_CHECK.IS_ND', true);
            }
            if (store.get('ACC_B') && store.get('ACC_B') !== '' && store.get('ACC_B') !== '0') {
                vm.set('record.acc_b', Ext.util.Format.trim(store.get('ACC_B')));
                vm.set('IS_CHECK.IS_ACC_B', true);
            }
            if (store.get('CUST_SEQ_ID') && store.get('CUST_SEQ_ID') !== '' && store.get('CUST_SEQ_ID') !== '0') {
                vm.set('record.cust_seq_id', store.get('CUST_SEQ_ID'));
                vm.set('IS_CHECK.IS_CUST_SEQ_ID', true);
            }
        }
    },

    changeDisabledIsTU: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel(),
            tabVM = (vm) ? vm.getParent() : false,
            store = (tabVM) ? tabVM.get('OES321PartyData') : false;

        window.lookupReference('csf_is_fio').setDisabled(false);
        window.lookupReference('csf_is_rg').setDisabled(false);
        window.lookupReference('csf_is_okpo').setDisabled(false);
        if (store) {
            if (store.get('NAMEU') && store.get('NAMEU') !== '' && store.get('NAMEU') !== '0') {
                vm.set('IS_CHECK.IS_FIO', true);
            }
            if (store.get('RG') && store.get('RG') !== '' && store.get('RG') !== '0') {
                vm.set('IS_CHECK.IS_RG', true);
            }
            if (store.get('SD') && store.get('SD') !== '' && store.get('SD') !== '0') {
                vm.set('IS_CHECK.IS_SD_OKPO', true);
            }
        }
        if (newValue === 1) { // ЮЛ
            vm.set('IS_CHECK.IS_FIO', false);
            window.lookupReference('csf_is_fio').setDisabled(true);
            window.lookupReference('csf_last_nm').setDisabled(true);

            window.lookupReference('csf_first_nm').setDisabled(true);
            window.lookupReference('csf_first_nm_l').setStyle('color', '#b3b3b3');
            window.lookupReference('csf_midl_nm').setDisabled(true);
            window.lookupReference('csf_midl_nm_l').setStyle('color', '#b3b3b3');
            window.lookupReference('csf_gr').setDisabled(true);
            window.lookupReference('csf_dateb_nm_l').setStyle('color', '#b3b3b3');

            vm.set('IS_CHECK.IS_KD', false);
            window.lookupReference('csf_is_kd').setDisabled(true);
            window.lookupReference('csf_series_l').setStyle('color', '#b3b3b3');
            window.lookupReference('csf_vd1_l').setStyle('color', '#b3b3b3');

            window.lookupReference('csf_gr').setDisabled(true);
        } else if (newValue === 2) {
            vm.set('IS_CHECK.IS_RG', false);
            vm.set('IS_CHECK.IS_SD_OKPO', false);
            window.lookupReference('csf_is_fio').setDisabled(false);
            window.lookupReference('csf_is_kd').setDisabled(false);
            window.lookupReference('csf_rg').setDisabled(true);
            window.lookupReference('csf_okpo').setDisabled(true);
            window.lookupReference('csf_is_rg').setDisabled(true);
            window.lookupReference('csf_is_okpo').setDisabled(true);
        } else if (newValue === 3) {
            vm.set('IS_CHECK.IS_SD_OKPO', false);
            window.lookupReference('csf_is_fio').setDisabled(false);
            window.lookupReference('csf_is_kd').setDisabled(false);
            window.lookupReference('csf_rg').setDisabled(false);
            window.lookupReference('csf_okpo').setDisabled(true);
            window.lookupReference('csf_is_okpo').setDisabled(true);
        }
    },

    changeDisabledIsND: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel();

        window.lookupReference('csf_nd').setDisabled(true);
        if (newValue) {
            window.lookupReference('csf_nd').setDisabled(false);
        }
    },

    changeDisabledIsRG: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel();

        window.lookupReference('csf_rg').setDisabled(true);
        if (newValue) {
            window.lookupReference('csf_rg').setDisabled(false);
        }
    },

    changeDisabledIsSDOKPO: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel();

        window.lookupReference('csf_okpo').setDisabled(true);
        if (newValue) {
            window.lookupReference('csf_okpo').setDisabled(false);
        }
        if (window.lookupReference('csf_tu').value === 3) window.lookupReference('csf_okpo').setDisabled(true);
    },

    changeDisabledIsACCB: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel();

        window.lookupReference('csf_acc_b').setDisabled(true);
        if (newValue) {
            window.lookupReference('csf_acc_b').setDisabled(false);
        }
    },

    changeDisabledIsFIO: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel();
        window.lookupReference('csf_last_nm').setDisabled(true);
        window.lookupReference('csf_first_nm').setDisabled(true);
        window.lookupReference('csf_first_nm_l').setStyle('color', '#b3b3b3');
        window.lookupReference('csf_midl_nm').setDisabled(true);
        window.lookupReference('csf_midl_nm_l').setStyle('color', '#b3b3b3');
        window.lookupReference('csf_gr').setDisabled(true);
        window.lookupReference('csf_dateb_nm_l').setStyle('color', '#b3b3b3');
        if (newValue) {
            window.lookupReference('csf_last_nm').setDisabled(false);
            window.lookupReference('csf_first_nm_l').setStyle('color', 'black');
            window.lookupReference('csf_first_nm').setDisabled(false);
            window.lookupReference('csf_midl_nm_l').setStyle('color', 'black');
            window.lookupReference('csf_midl_nm').setDisabled(false);
            window.lookupReference('csf_dateb_nm_l').setStyle('color', 'black');
            window.lookupReference('csf_gr').setDisabled(false);
        }
    },

    changeDisabledIsKD: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel();

        window.lookupReference('csf_kd').setDisabled(true);
        window.lookupReference('csf_series').setDisabled(true);
        window.lookupReference('csf_series_l').setStyle('color', '#b3b3b3');
        window.lookupReference('csf_vd1').setDisabled(true);
        window.lookupReference('csf_vd1_l').setStyle('color', '#b3b3b3');
        if (newValue) {
            window.lookupReference('csf_kd').setDisabled(false);
            window.lookupReference('csf_series').setDisabled(false);
            window.lookupReference('csf_series_l').setStyle('color', 'black');
            window.lookupReference('csf_vd1').setDisabled(false);
            window.lookupReference('csf_vd1_l').setStyle('color', 'black');
        }
    },

    changeDisabledIsCUSTSEQID: function (field, newValue, oldValue, eOpts) {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel();

        window.lookupReference('csf_cust_seq_id').setDisabled(true);
        if (newValue) {
            window.lookupReference('csf_cust_seq_id').setDisabled(false);
        }
    },

    onSelectItemSeachClinet: function () {
        var self = this,
            window = this.getView(),
            chooseSelection = self.lookupReference('searchClientGrid').getSelectionModel().getSelection(),
            vm = self.getViewModel(),
            storeOES321Details = vm.getStore('OES321Details'),
            data, fieldInto, setVal,
            vmTab = (vm) ? vm.getParent() : false,
            parentView = (vmTab) ? vmTab.getView() : false,
            refName = (parentView) ? parentView.reference : false,
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            dateFields = [
                // 'GR',
                'VD3',
                'VD6',
                'VD7',
                'MC2',
                'MC3'
            ],
            ZERO_DATE = (new Date(2099, 0, 1)).getTime();

        if (refName === 'main-search-operations-form') {
            if (vm && chooseSelection.length == 1) {
                data = chooseSelection[0];
                if (vmTab) {
                    var checkRuleClientLoad = window.lookupReference('checkHowRuleClientLoad').getValue()['checkHowRuleClientLoad'];
                    if (!common.globalinit.loadingMaskCmp) {
                        common.globalinit.loadingMaskCmp = self.getView();
                    }

                    vmTab.set('record.payerNameOperSearch', data.get('NAMEU'));
                    if(data.get('ADR_COUNTRY_CD')) {
                        if (data.get('ADR_COUNTRY_CD') === 'РОССИЯ') {
                            vmTab.set('record.payerCountryOperSearch', 'Y');
                        } else {
                            vmTab.set('record.payerCountryOperSearch', 'N');
                        }
                    } else {
                        vmTab.set('record.payerCountryOperSearch', 'Y');
                    }
                    window.close();
                }
            } else if (chooseSelection.length <= 0) {
                Ext.Msg.alert('Внимание!', 'Нет выделенного значения.');
                return false;
            } else if (chooseSelection.length > 1) {
                Ext.Msg.alert('Внимание!', 'Выделенно больше 1 значения.');
                return false;
            }
        } else {
            if (vm && chooseSelection.length == 1) {
                data = chooseSelection[0];
                if (vmTab) {
                    var checkRuleClientLoad = window.lookupReference('checkHowRuleClientLoad').getValue()['checkHowRuleClientLoad'];
                    if (!common.globalinit.loadingMaskCmp) {
                        common.globalinit.loadingMaskCmp = self.getView();
                    }
                    if (numberTab) {
                        var fieldArray = data.data,
                            tabVmStore = vmTab.getStore('OES321PartyStore') ? vmTab.getStore('OES321PartyStore') : storeOES321Details,
                            tabData = vmTab.get('OES321PartyData') ? vmTab.get('OES321PartyData').getData() : vmTab.get('OES321DetailsTable'),
                            record;
                        for (var cField in fieldArray) {
                            setVal = '';
                            fieldInto = '';
                            setVal = fieldArray[cField];
                            if (Ext.Array.indexOf(dateFields, cField) !== -1) {
                                if (setVal) {
                                    setVal = (setVal) ? new Date(setVal) : new Date('2099', '0', '1', '0', '0', '0');
                                    fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + cField);
                                } else {
                                    setVal = new Date('2099', '0', '1', '0', '0', '0');
                                    fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + cField);
                                }
                            } else {
                                fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + cField);
                                setVal = (setVal) ? setVal : '0';
                                if (cField.toUpperCase() === 'CUST_SEQ_ID') {
                                    tabData[cField] = parseInt(setVal, 10) ? parseInt(setVal, 10) : null;
                                }
                            }
                            if ((fieldInto || (Ext.Array.indexOf(dateFields, cField) !== -1)) && tabVmStore) {
                                if (checkRuleClientLoad && checkRuleClientLoad === 1) {
                                    fieldInto.setValue(setVal);
                                    tabData[cField] = setVal;
                                } else if (checkRuleClientLoad && checkRuleClientLoad === 2) {
                                    if (!fieldInto.getValue() ||
                                        (Ext.Array.indexOf(dateFields, cField) !== -1) ||
                                        fieldInto.getValue().toString() === '0' ||
                                        (fieldInto.getValue().getTime && fieldInto.getValue().getTime() == ZERO_DATE)) {
                                        fieldInto.setValue(setVal);
                                        tabData[cField] = setVal;
                                    }
                                }
                            }
                        }
                        tabVmStore.getAt(0).set(tabData);
                        record = tabVmStore.getAt(0);
                        record.set(tabData);
                        window.close();
                    } else {
                        return false;
                    }
                }
            } else if (chooseSelection.length <= 0) {
                Ext.Msg.alert('Внимание!', 'Нет выделенного значения.');
                return false;
            } else if (chooseSelection.length > 1) {
                Ext.Msg.alert('Внимание!', 'Выделенно больше 1 значения.');
                return false;
            }
        }
    },

    onLinkItemSeachClinet: function () {
        var self = this,
            window = this.getView(),
            chooseSelection = self.lookupReference('searchClientGrid').getSelectionModel().getSelection(),
            vm = self.getViewModel(),
            dataCh, respObj,
            vmTab = (vm) ? vm.getParent() : false,
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            vmData = (vm) ? vm.getData() : false,
            store = (vmTab) ? vmTab.get('OES321PartyData') : false,
            data = {
                form: 'OES321Party',
                action: 'update'
            };

        if (vm && chooseSelection.length == 1) {
            dataCh = chooseSelection[0];
            if (store && dataCh.get('CUST_SEQ_ID') && dataCh.get('CUST_SEQ_ID') !== '' && dataCh.get('CUST_SEQ_ID') > 0) {
                data.oes321Id = store.get('OES_321_ID');
                data.block_nb = store.get('BLOCK_NB');
                data.data = [{
                    CUST_SEQ_ID: parseInt(dataCh.get('CUST_SEQ_ID'), 10),
                    _id_: store.get('OES_321_ID') + "|" + store.get('BLOCK_NB')
                }];

                // добавим loading обработки:
                if (!common.globalinit.loadingMaskCmp) {
                    common.globalinit.loadingMaskCmp = self.getView();
                }
                if (numberTab) {
                    Ext.Ajax.request({
                        url: common.globalinit.ajaxUrl,
                        method: common.globalinit.ajaxMethod,
                        headers: common.globalinit.ajaxHeaders,
                        timeout: common.globalinit.ajaxTimeOut,
                        params: Ext.encode(data),
                        success: function (response) {
                            var responseObj = Ext.JSON.decode(response.responseText);
                            if (responseObj.message) {
                                Ext.Msg.alert('', responseObj.message);
                            }
                            if (responseObj.success) {
                                store.set('CUST_SEQ_ID', parseInt(dataCh.get('CUST_SEQ_ID'), 10));
                                Ext.Msg.alert('Внимание!', 'Привязка успешно осуществлена.');
                            }
                        }
                    });
                    window.close();
                } else {
                    return false;
                }
            } else if (!dataCh.get('CUST_SEQ_ID') || dataCh.get('CUST_SEQ_ID') == '' || dataCh.get('CUST_SEQ_ID') === 0) {
                Ext.Msg.alert('Внимание!', 'Нет соответствующих данных для привязки.');
                return false;
            }
        } else if (chooseSelection.length <= 0) {
            Ext.Msg.alert('Внимание!', 'Нет выделенного значения.');
            return false;
        } else if (chooseSelection.length > 1) {
            Ext.Msg.alert('Внимание!', 'Выделенно больше 1 значения.');
            return false;
        }
    },

    onClickSearch: function () {
        var self = this,
            window = this.getView(),
            form = window.down('form'),
            vm = window.getViewModel(),
            vmData = (vm) ? vm.getData() : false,
            oes321ID = vm.get('oes_321_id'),
            data = {
                form: 'OESCustSearch',
                action: 'search',
                oes_321_id: oes321ID
            };

        // проверка данных фильтра поиска
        if (!(data = this.getDataForm(vmData, data, window))) {
            return false;
        }

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(data),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                var storeSearchResult = self.getReferences()['searchClientGrid'].getStore();
                if (storeSearchResult) {
                    storeSearchResult.removeAll();
                }
                if (responseObj.success) {
                    storeSearchResult = self.getReferences()['searchClientGrid'].getStore();
                    if (storeSearchResult) {
                        storeSearchResult.add(responseObj.data);
                    }
                }
            }
        });
    },

    getDataForm: function (vmData, data, window) {
        var record = (vmData) ? vmData.record : false,
            field;

        if (vmData) {
            // if (!record.searchType) {
            if (!vmData.searchTypeVal) {
                field = window.lookupReference('csf_searchType');
                field.markInvalid('нужно заполнить');
            }
            // if (!record.tu) {
            if (!vmData.tuVal) {
                field = window.lookupReference('csf_tu');
                field.markInvalid('нужно заполнить');
            }
            data.searchType = vmData.searchTypeVal;
            data.tu = vmData.tuVal;
            if (vmData.IS_ND) {
                if (!record.nd || record.nd === '') {
                    field = window.lookupReference('csf_nd');
                    field.markInvalid('нужно заполнить');
                }
                data.nd = record.nd;
            }
            if (vmData.IS_RG) {
                if (!record.rg || record.rg === '') {
                    field = window.lookupReference('csf_rg');
                    field.markInvalid('нужно заполнить');
                }
                data.rg = record.rg;
            }
            if (vmData.IS_SD_OKPO) {
                if (!record.okpo || record.okpo === '') {
                    field = window.lookupReference('csf_okpo');
                    field.markInvalid('нужно заполнить');
                }
                data.okpo = record.okpo;
            }
            if (vmData.IS_ACC_B === true) {
                if (!record.acc_b || record.acc_b === '') {
                    field = window.lookupReference('csf_acc_b');
                    field.markInvalid('нужно заполнить');
                }
                data.acc_b = record.acc_b;
            }
            if (vmData.IS_FIO === true) {
                if (!record.last_nm || record.last_nm === '') {
                    field = window.lookupReference('csf_last_nm');
                    field.markInvalid('нужно заполнить');
                }
                if (!record.first_nm || record.first_nm === '') {
                    field = window.lookupReference('csf_first_nm');
                    field.markInvalid('нужно заполнить');
                }
                data.last_nm = record.last_nm;
                data.first_nm = record.first_nm;
                (record.midl_nm) ? data.midl_nm = record.midl_nm : delete data.midl_nm;
                (record.gr) ? data.gr = record.gr : delete data.gr;
            }
            if (vmData.IS_KD === true) {
                if (!record.vd1 || record.vd1 === '') {
                    field = window.lookupReference('csf_vd1');
                    field.markInvalid('нужно заполнить');
                }
                data.vd1 = record.vd1;
                (record.series) ? data.series = record.series : delete data.series;
                (record.kd) ? data.kd = record.kd : delete data.kd;
            }
            if (vmData.IS_CUST_SEQ_ID === true) {
                if (!record.cust_seq_id || record.cust_seq_id === '') {
                    field = window.lookupReference('csf_cust_seq_id');
                    field.markInvalid('нужно заполнить');
                }
                data.cust_seq_id = parseInt(record.cust_seq_id, 10);
            }
            if (field) {
                return false;
            }
        }

        return data;
    },

    onClickCancelWindow: function (button, eOpts) {
        var window = this.getView(),
            controller = this;
        window.close();
    },

    openRecordForReview: function (record, parTabVM) {
        var self = this,
            vNAMEU_1 = (record.data.NAMEU ? record.data.NAMEU : '0'),
            vTU_1 = (record.data.TU ? record.data.TU : '0'),
            vVP_1 = (record.data.VP ? record.data.VP : '0'),
            vCARD_B_1 = (record.data.CARD_B ? record.data.CARD_B : '0'),
            vPR_1 = (record.data.PR ? record.data.PR : '0'),
            vND_1 = (record.data.ND ? record.data.ND : '0'),
            vGR_1 = (record.data.GR ? record.data.GR : ''),
            vRG_1 = (record.data.RG ? record.data.RG : '0'),
            vBP_1 = (record.data.BP ? record.data.BP : '0'),
            vKODCR_1_1 = (record.data.KODCR_1 ? record.data.KODCR_1 : '0'),
            vKODCR_2_1 = (record.data.KODCR_2 ? record.data.KODCR_2 : '0'),
            vAMR_S_1 = (record.data.AMR_S ? record.data.AMR_S : '0'),
            vAMR_R_1 = (record.data.AMR_R ? record.data.AMR_R : '0'),
            vAMR_G_1 = (record.data.AMR_G ? record.data.AMR_G : '0'),
            vAMR_U_1 = (record.data.AMR_U ? record.data.AMR_U : '0'),
            vAMR_D_1 = (record.data.AMR_D ? record.data.AMR_D : '0'),
            vAMR_K_1 = (record.data.AMR_K ? record.data.AMR_K : '0'),
            vAMR_O_1 = (record.data.AMR_O ? record.data.AMR_O : '0'),
            vKODCN_1_1 = (record.data.KODCN_1 ? record.data.KODCN_1 : '0'),
            vKODCN_2_1 = (record.data.KODCN_2 ? record.data.KODCN_2 : '0'),
            vADRESS_S_1 = (record.data.ADRESS_S ? record.data.ADRESS_S : '0'),
            vADRESS_R_1 = (record.data.ADRESS_R ? record.data.ADRESS_R : '0'),
            vADRESS_G_1 = (record.data.ADRESS_G ? record.data.ADRESS_G : '0'),
            vADRESS_U_1 = (record.data.ADRESS_U ? record.data.ADRESS_U : '0'),
            vADRESS_D_1 = (record.data.ADRESS_D ? record.data.ADRESS_D : '0'),
            vADRESS_K_1 = (record.data.ADRESS_K ? record.data.ADRESS_K : '0'),
            vADRESS_O_1 = (record.data.ADRESS_O ? record.data.ADRESS_O : '0'),
            vKD_1 = (record.data.KD ? record.data.KD : '0'),
            vSD_1 = (record.data.SD ? record.data.SD : '0'),
            vVD1_1 = (record.data.VD1 ? record.data.VD1 : '0'),
            vVD2_1 = (record.data.VD2 ? record.data.VD2 : '0'),
            vVD3_1 = (record.data.VD3 ? record.data.VD3 : '01.01.2099'),
            vVD4_1 = (record.data.VD4 ? record.data.VD4 : '0'),
            vVD5_1 = (record.data.VD5 ? record.data.VD5 : '0'),
            vVD6_1 = (record.data.VD6 ? record.data.VD6 : '01.01.2099'),
            vVD7_1 = (record.data.VD7 ? record.data.VD7 : '01.01.2099'),
            vMC1_1 = (record.data.MC1 ? record.data.MC1 : '0'),
            vMC2_1 = (record.data.MC2 ? record.data.MC2 : '01.01.2099'),
            vMC3_1 = (record.data.MC3 ? record.data.MC3 : '01.01.2099'),
            vcustSeqId_1 = (record.data.CUST_SEQ_ID ? record.data.CUST_SEQ_ID : '');


        if (record.data.ROW_TYPE_CD === 'CUST') {
            if (self) {
                var form = Ext.widget({
                    xtype: 'oes321detailspanel-tabs-forms-viewfsearch',
                    // maxHeight: "600",
                    viewModel: {
                        data: {
                            CUST_SEQ_ID_1: vcustSeqId_1
                        },
                        parent: parTabVM
                    }
                }).show();

            } else {
                Ext.Msg.alert('Внимание!', 'Проблемы с функционалом предпросмотра Клиентов.');
                return false;
            }
        } else if (record.data.ROW_TYPE_CD === 'OES') {
            if (self) {
                var form = Ext.widget({
                    xtype: 'app-oes321detailspanel-viewclientfromsearch',
                    // maxHeight: constrain.dom.clientHeight - 100,
                    maxHeight: '800',
                    viewModel: {
                        data: {
                            NAMEU_1: vNAMEU_1,
                            TU_1: vTU_1,
                            VP_1: vVP_1,
                            CARD_B_1: vCARD_B_1,
                            PR_1: vPR_1,
                            ND_1: vND_1,
                            GR_1: vGR_1,
                            RG_1: vRG_1,
                            BP_1: vBP_1,
                            KODCR_1_1: vKODCR_1_1,
                            KODCR_2_1: vKODCR_2_1,
                            AMR_S_1: vAMR_S_1,
                            AMR_R_1: vAMR_R_1,
                            AMR_G_1: vAMR_G_1,
                            AMR_U_1: vAMR_U_1,
                            AMR_D_1: vAMR_D_1,
                            AMR_K_1: vAMR_K_1,
                            AMR_O_1: vAMR_O_1,
                            KODCN_1_1: vKODCN_1_1,
                            KODCN_2_1: vKODCN_2_1,
                            ADRESS_S_1: vADRESS_S_1,
                            ADRESS_R_1: vADRESS_R_1,
                            ADRESS_G_1: vADRESS_G_1,
                            ADRESS_U_1: vADRESS_U_1,
                            ADRESS_D_1: vADRESS_D_1,
                            ADRESS_K_1: vADRESS_K_1,
                            ADRESS_O_1: vADRESS_O_1,
                            KD_1: vKD_1,
                            SD_1: vSD_1,
                            VD1_1: vVD1_1,
                            VD2_1: vVD2_1,
                            VD3_1: vVD3_1,
                            VD4_1: vVD4_1,
                            VD5_1: vVD5_1,
                            VD6_1: vVD6_1,
                            VD7_1: vVD7_1,
                            MC1_1: vMC1_1,
                            MC2_1: vMC2_1,
                            MC3_1: vMC3_1
                        }
                    }
                }).show();
            } else {
                Ext.Msg.alert('Внимание!', 'Проблемы с функционалом предпросмотра ОЭС.');
                return false;
            }
        }
    }
});


