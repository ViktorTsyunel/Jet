Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ViewClientFromSearchModel', {
    extend: 'Ext.data.Model',
    alias: 'model.viewclientfromsearchmodel',
    idProperty: '_id_',
    fields: [
        {
            // ID
            identifier: true,
            name: '_id_',
            type: 'string'
        },
        {
            // Тип клиента
            name: 'TU',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Найдено в
            name: 'ROW_TYPE_NM',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Наименование/ФИО
            name: 'NAMEU',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // ИНН
            name: 'ND',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // ОГРН
            name: 'RG',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // ОКПО
            name: 'SD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата рег./рожд.
            name: 'GR',
            // type: 'string',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // Адрес
            name: 'ADDR_TEXT',
            type: 'string',
            allowNull: false,
            allowBlank: false
        },
        {
            // Серия документа
            name: 'SD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер документа
            name: 'VD1',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // ID клиента
            name: 'CUST_SEQ_ID',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Статус ОЭС
            name: 'RV_STATUS_NM',
            type: 'string',
            allowNull: false,
            allowBlank: false
        },
        {
            // Ответсвенный
            name: 'OWNER_ID',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Место рождения
            name: 'BP',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код места рег./жит.
            name: 'KODCR',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCR_1'), record.get('KODCR_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCR_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCR', record.get('KODCR'), record.get('KODCR_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCR_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCR', record.get('KODCR'), record.get('KODCR_1'));
            }
        },
        {
            // OKATO
            name: 'AMR_S',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Rayon Oblast
            name: 'AMR_R',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Gorod
            name: 'AMR_G',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Ул.
            name: 'AMR_U',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Dom
            name: 'AMR_D',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Korpus
            name: 'AMR_K',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Kvartira
            name: 'AMR_O',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код места нахождения
            name: 'KODCN',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCN_1'), record.get('KODCN_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCN_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCN', record.get('KODCN'), record.get('KODCN_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCN_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCN', record.get('KODCN'), record.get('KODCN_1'));
            }
        },
        {
            // Код места нахождения
            name: 'KODCN_B',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCN_B_1'), record.get('KODCN_B_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCN_B_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCN_B', record.get('KODCN_B'), record.get('KODCN_B_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCN_B_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCN_B', record.get('KODCN_B'), record.get('KODCN_B_1'));
            }
        },
        {
            // Код места нахождения
            name: 'KODCN_R',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCN_R_1'), record.get('KODCN_R_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCN_R_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCN_R', record.get('KODCN_R'), record.get('KODCN_R_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCN_R_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCN_R', record.get('KODCN_R'), record.get('KODCN_R_1'));
            }
        },
        {
            // OKATO
            name: 'ADRESS_S',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Rayon
            name: 'ADRESS_R',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Gorod
            name: 'ADRESS_G',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Ulica
            name: 'ADRESS_U',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Dom
            name: 'ADRESS_D',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // korpus
            name: 'ADRESS_K',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // kvartira
            name: 'ADRESS_O',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Документ, удостоверяющий личность
            name: 'KD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Орган, выдавший документ
            name: 'VD2',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата выдачи
            name: 'VD3',
            // type: 'string',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код вида документа
            name: 'VD4',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Серия и номер
            name: 'VD5',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Срок действия с
            name: 'VD6',
            // type: 'string',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // Срок действия по
            name: 'VD7',
            // type: 'string',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер
            name: 'MC1',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Срок пребывания с
            name: 'MC2',
            // type: 'string',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // по
            name: 'MC3',
            // type: 'string',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // по
            name: 'KD_TX',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // по
            name: 'TRXN_DT',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // по
            name: 'SRC_CD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // по
            name: 'ROW_TYPE_CD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        }
    ]
});