Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SearchClientModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.oes321detailspaneltabssearchclientvm',
	data: {
		title: 'Поиск клиента',
        CUST_SEQ_ID: null,
        IS_SEARCH_OPERATION: false,
        record: {
            tu: null, // Тип клиента. Возможные значения: ЮЛ, ФЛ, ИП.
            searchType: null, //Область поиска
            nd:null, // ИНН
            rg: null, // ОГРН
            okpo: null, //ОКПО
            kd: null, // Вид документа
            series: null, // Серия документа
            vd1: null, // Номер документ
            first_nm: null, // Имя
            last_nm: null, // Фамилия
            midl_nm: null, // Отчество
            gr: null, // Дата рождения
            acc_b: null, // Номер счета
            cust_seq_id: null // ID клиента
        },
        IS_CHECK: {
            IS_ND: false,
            IS_RG: false,
            IS_SD_OKPO: false,
            IS_ACC_B: false,
            IS_FIO: false,
            IS_KD: false,
            IS_CUST_SEQ_ID: false
        }
	},
    formulas: {
        // Flags
        IS_ND: {
            bind: {
                value: '{IS_CHECK.IS_ND}'
            },
            get: function (dataObj) {
                return dataObj.value;
            },
            set: function (value) {
                this.set('IS_CHECK.IS_ND', value);
            }
        },
        IS_RG: { // Только для ЮЛ[ TU=1 ], ИП[ TU=3 ].
            bind: {
                value: '{IS_CHECK.IS_RG}'
            },
            get: function (dataObj) {
                return dataObj.value;
            },
            set: function (value) {
                this.set('IS_CHECK.IS_RG', value);
            }
        },
        IS_SD_OKPO: { // Только для ЮЛ[ TU=1 ].
            bind: {
                value: '{IS_CHECK.IS_SD_OKPO}'
            },
            get: function (dataObj) {
                return dataObj.value;
            },
            set: function (value) {
                this.set('IS_CHECK.IS_SD_OKPO', value);
            }
        },
        IS_ACC_B: {
            bind: {
                value: '{IS_CHECK.IS_ACC_B}'
            },
            get: function (dataObj) {
                return dataObj.value;
            },
            set: function (value) {
                this.set('IS_CHECK.IS_ACC_B', value);
            }
        },
        IS_FIO: {
            bind: {
                value: '{IS_CHECK.IS_FIO}'
            },
            get: function (dataObj) {
                return dataObj.value;
            },
            set: function (value) {
                this.set('IS_CHECK.IS_FIO', value);
            }
        },
        IS_KD: {
            bind: {
                value: '{IS_CHECK.IS_KD}'
            },
            get: function (dataObj) {
                return dataObj.value;
            },
            set: function (value) {
                this.set('IS_CHECK.IS_KD', value);
            }
        },
        IS_CUST_SEQ_ID: {
            bind: {
                value: '{IS_CHECK.IS_CUST_SEQ_ID}'
            },
            get: function (dataObj) {
                return dataObj.value;
            },
            set: function (value) {
                this.set('IS_CHECK.IS_CUST_SEQ_ID', value);
            }
        },
        tuVal: {
            bind: {
                tuV: '{record.tu}'
            },
            get: function(data){
                return data.tuV ? data.tuV : '1';
            },
            set: function (value) {
                this.set('record.tu', value);
            }
        },
        searchTypeVal: {
            bind: {
                stV: '{record.searchType}'
            },
            get: function(data){
                return data.stV ? data.stV : 'ALL';
            },
            set: function (value) {
                this.set('record.searchType', value);
            }
        }
    },
    stores: {
        // Справочник Тип клиента
        typeClientStore: {
            fields: ['id', 'text'],
            data: [
                {id: 1, text: 'ЮЛ'},
                {id: 2, text: 'ФЛ'},
                {id: 3, text: 'ИП'}
            ]
        },
        // Справочник Область поиска
        searchAreaStore: {
            fields: ['id', 'text'],
            data: [
                {id: 'ALL', text: 'Везде'},
                {id: 'CUST', text: 'Справочник клиентов'},
                {id: 'OES', text: 'Сообщения ОЭС'}
            ]
        },

        DOCTYPEStoreSearchClient: {
            type: 'chained',
            source: 'DOCTYPEREF'
        },
        
        TUViewStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'участник отсутствует'},
                {id: 1, text: 'ЮЛ'},
                {id: 2, text: 'ФЛ'},
                {id: 3, text: 'ИП'},
                {id: 4, text: 'Участник не определен'}
            ]
        }
    }
});
