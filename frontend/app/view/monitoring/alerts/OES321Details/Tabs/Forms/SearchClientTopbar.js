Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SearchClientTopbar', {
    extend: 'Ext.container.Container',
    xtype: 'oes321detailspanel-tabs-search-client-topbar',
    reestablishView: false,
    reference: 'ref-search-client-topbar',
    items: [
        {
            xtype: 'form',
            isFilters: true,
            formBind: true,
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'column'
                    },
                    flex: 1,
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 100,
                        margin: '3px 3px 0 3px',
                        flex: 1
                    },
                    items: [
                        {
                            columnWidth: 0.14,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Тип клиента',
                                            name: 'tu',
                                            reference: 'csf_tu',
                                            editable: false,
                                            queryMode: 'local',
                                            forceSelection: true,
                                            multiSelect: false,
                                            matchFieldWidth: false,
                                            displayField: 'text',
                                            valueField: 'id',
                                            bind: {
                                                store: '{typeClientStore}',
                                                // value: '{record.tu}'
                                                value: '{tuVal}'
                                            },
                                            listeners : {
                                                change: 'changeDisabledIsTU'
                                            }
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            height: 25
                                        },
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Область поиска',
                                            name: 'searchType',
                                            reference: 'csf_searchType',
                                            editable: false,
                                            queryMode: 'local',
                                            forceSelection: true,
                                            multiSelect: false,
                                            matchFieldWidth: false,
                                            displayField: 'text',
                                            valueField: 'id',
                                            bind: {
                                                store: '{searchAreaStore}',
                                                // value: '{record.searchType}'
                                                value: '{searchTypeVal}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            columnWidth: 0.01,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            }
                        },
                        {
                            columnWidth: 0.1,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'checkboxfield',
                                    name: 'is_nd',
                                    boxLabel: 'ИНН',
                                    inputValue: '1',
                                    bind: {
                                        value: '{IS_ND}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    },
                                    listeners : {
                                        change: 'changeDisabledIsND'
                                    }
                                },
                                {
                                    xtype: 'checkboxfield',
                                    name: 'is_rg',
                                    reference: 'csf_is_rg',
                                    boxLabel: 'ОГРН',
                                    inputValue: '1',
                                    bind: {
                                        value: '{IS_RG}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    },
                                    listeners : {
                                        change: 'changeDisabledIsRG'
                                    }
                                },
                                {
                                    xtype: 'checkboxfield',
                                    name: 'is_okpo',
                                    reference: 'csf_is_okpo',
                                    boxLabel: 'ОКПО',
                                    inputValue: '1',
                                    bind: {
                                        value: '{IS_SD_OKPO}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    },
                                    listeners : {
                                        change: 'changeDisabledIsSDOKPO'
                                    }
                                },
                                {
                                    xtype: 'checkboxfield',
                                    name: 'is_acc_b',
                                    boxLabel: 'Номер счета',
                                    inputValue: '1',
                                    bind: {
                                        value: '{IS_ACC_B}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    },
                                    listeners : {
                                        change: 'changeDisabledIsACCB'
                                    }
                                }
                            ]
                        },
                        {
                            columnWidth: 0.15,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'nd',
                                    reference: 'csf_nd',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.nd}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'rg',
                                    reference: 'csf_rg',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.rg}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'okpo',
                                    reference: 'csf_okpo',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.okpo}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'acc_b',
                                    reference: 'csf_acc_b',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.acc_b}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                }
                            ]
                        },
                        {
                            columnWidth: 0.01,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            }
                        },
                        {
                            columnWidth: 0.1,
                            xtype: 'fieldcontainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'checkboxfield',
                                    name: 'is_fio',
                                    reference: 'csf_is_fio',
                                    boxLabel: 'Фамилия',
                                    inputValue: '1',
                                    bind: {
                                        value: '{IS_FIO}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    },
                                    listeners : {
                                        change: 'changeDisabledIsFIO'
                                    }
                                },
                                {
                                    xtype: 'label',
                                    reference: 'csf_first_nm_l',
                                    // style: 'text-align: right;',
                                    text: 'Имя',
                                    margin: '7 0 2 0'
                                },
                                {
                                    xtype: 'label',
                                    // style: 'text-align: right;',
                                    reference: 'csf_midl_nm_l',
                                    text: 'Отчество',
                                    margin: '8 0 1 0'

                                },
                                {
                                    xtype: 'label',
                                    // style: 'text-align: right;',
                                    reference: 'csf_dateb_nm_l',
                                    text: 'Дата рождения',
                                    margin: '9 0 0 0'
                                }
                            ]
                        },
                        {
                            columnWidth: 0.15,
                            xtype: 'fieldcontainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'last_nm',
                                    reference: 'csf_last_nm',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.last_nm}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'first_nm',
                                    reference: 'csf_first_nm',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.first_nm}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'midl_nm',
                                    reference: 'csf_midl_nm',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.midl_nm}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'datefield',
                                    name: 'gr',
                                    reference: 'csf_gr',
                                    label: false,
                                    disabled: true,
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    bind: {
                                        value: '{record.gr}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                }
                            ]
                        },
                        {
                            columnWidth: 0.01,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            }
                        },
                        {
                            columnWidth: 0.1,
                            xtype: 'fieldcontainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'checkboxfield',
                                    name: 'is_kd',
                                    reference: 'csf_is_kd',
                                    boxLabel: 'Вид документа',
                                    inputValue: '1',
                                    bind: {
                                        value: '{IS_KD}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    },
                                    listeners : {
                                        change: 'changeDisabledIsKD'
                                    }
                                },
                                {
                                    xtype: 'label',
                                    // style: 'text-align: right;',
                                    reference: 'csf_series_l',
                                    text: 'Серия',
                                    margin: '7 0 2 0'
                                },
                                {
                                    xtype: 'label',
                                    // style: 'text-align: right;',
                                    text: 'Номер',
                                    reference: 'csf_vd1_l',
                                    margin: '8 0 5 0'

                                },
                                {
                                    xtype: 'checkboxfield',
                                    name: 'is_cust_seq_id',
                                    boxLabel: 'ID клиента',
                                    inputValue: '1',
                                    bind: {
                                        value: '{IS_CUST_SEQ_ID}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    },
                                    listeners : {
                                        change: 'changeDisabledIsCUSTSEQID'
                                    }
                                }
                            ]
                        },
                        {
                            columnWidth: 0.15,
                            xtype: 'fieldcontainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    reference: 'csf_kd',
                                    flex: 1,
                                    name: 'kd',
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{DOC_TYPE_CD} - {DOC_TYPE_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayField: 'DOC_TYPE_NM',
                                    valueField: 'DOC_TYPE_CD',
                                    forceSelection: true,
                                    disabled: true,
                                    bind: {
                                        store: '{DOCTYPEStoreSearchClient}',
                                        readOnly: '{IS_SEARCH_OPERATION}',
                                        value: '{record.kd}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'series',
                                    reference: 'csf_series',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.series}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'vd1',
                                    reference: 'csf_vd1',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.vd1}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'cust_seq_id',
                                    reference: 'csf_cust_seq_id',
                                    label: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.cust_seq_id}',
                                        readOnly: '{IS_SEARCH_OPERATION}'
                                    }
                                }
                            ]
                        },
                        {
                            columnWidth: 0.08,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'buttongroup',
                                    layout: 'fit',
                                    height: 100,
                                    margin: '3px 0 0 7px',
                                    items: [
                                        {
                                            scale: 'large',
                                            width: 100,
                                            iconCls: 'icon-search',
                                            listeners: {
                                                click: 'onClickSearch'
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            defaults: {
                margin: '1px 3px 1px 3px'
            },
            items: [
                {
                    xtype: 'label',
                    text: 'Результат поиска'
                }
            ]
        }
    ]
});