Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.AddressSelectionController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oes321detailspaneltabsaddressselection',
    requires: [
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#selectedDataAddress': {
                    click: 'onCopyDataToTab'
                },
                '#cancelWindow': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            store = vm.getStore('AddressListStoreForms');
        if (vm && vm.get('CUST_SEQ_ID')) {
            // загрузка справочника:
            store.load({
                scope: self,
                params: {
                    custSeqId: vm.get('CUST_SEQ_ID')
                },
                callback: function(data) {
                    if (data.length <= 0) {
                        Ext.Msg.alert('Внимание!', 'Нет данных для обработки.');
                        setTimeout(function () {
                            window.close();
                        }, 500);
                    }
                }
            });
        }
    },

    onCopyDataToTab: function() {
        var self = this,
            window = this.getView(),
            addressSelection = self.lookupReference('addressSelectionGrid').getSelectionModel().getSelection(),
            vm = self.getViewModel(),
            data, fieldInto, intoKey, setVal,
            typeBlock = (vm) ? vm.get('typeBlock') : false,
            objKey = {
                KODCR_1: 'KODCN_1',
                KODCR_2: 'KODCN_2',
                AMR_S: 'ADRESS_S',
                AMR_R: 'ADRESS_R',
                AMR_G: 'ADRESS_G',
                AMR_U: 'ADRESS_U',
                AMR_D: 'ADRESS_D',
                AMR_K: 'ADRESS_K',
                AMR_O: 'ADRESS_O'
            },
            keysValue = {
                KODCR_1: 'ISO_NUMERIC_CD',
                KODCR_2: '00',
                AMR_S: 'OKATO_CD',
                AMR_R: 'DISTRICT_NM',
                AMR_G: 'CITY_NM',
                AMR_U: 'STREET_NAME_TX',
                AMR_D: 'HOUSE_NUMBER_TX',
                AMR_K: 'BUILDING_NUMBER_TX',
                AMR_O: 'FLAT_NUMBER_TX'
            },
            defVal = '0',
            vmTab = (vm) ? vm.getParent() : false,
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            store = (vmTab) ? vmTab.get('OES321PartyData') : false;

        if (vmTab && addressSelection.length == 1) {
            data = addressSelection[0];

            if (typeBlock && numberTab) {
                Ext.Object.each(objKey, function (key, value, myself) {
                    if (typeBlock == 'register') {
                        intoKey = key;
                        store.set('KODCR', (data.get('ISO_NUMERIC_CD')) ? data.get('ISO_NUMERIC_CD') + '00' : '0');

                    } else if (typeBlock == 'visit') {
                        intoKey = value;
                        store.set('KODCN', (data.get('ISO_NUMERIC_CD')) ? data.get('ISO_NUMERIC_CD') + '00' : '0');
                    }
                    fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + intoKey);

                    switch (true) {
                        case (key === 'KODCR_2'):
                            setVal = (data.get(keysValue[key])) ? data.get(keysValue[key]) : keysValue[key];
                            break;
                        default:
                            setVal = (data.get(keysValue[key])) ? data.get(keysValue[key]) : defVal;
                            break;
                    }

                    fieldInto.setValue(setVal);
                });
                window.close();
            } else {
                return false;
            }
        } else if (addressSelection.length <= 0) {
            Ext.Msg.alert('Внимание!', 'Нет выделенного значения.');
            return false;
        } else if (addressSelection.length > 1) {
            Ext.Msg.alert('Внимание!', 'Выделенно больше 1 значения.');
            return false;
        }
    },

    onClickCancelWindow: function (button, eOpts) {
        var window = this.getView();

        window.close();
    }
});
