Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SelectDocumentModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.oes321detailspaneltabsselectdocumentvm',
	data: {
		title: 'Выбор документа',
        CUST_SEQ_ID: null,
        typeBlock: null
	},
    stores: {
        // Справочник
        DocumentListStoreForms: {
            //model: 'AML.model.OES321Party',
            // CUST_ADDR_SEQ_ID: 13599488
            fields: [
                {
                    name: 'ISSUE_DT',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                },
                {
                    name: 'END_DT',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                }
            ],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustIdDocList',
                    action: 'readByCustId'
                }
            }
        }
    }
});
