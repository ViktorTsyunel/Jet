Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ChoosingRepresentativeController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oes321detailspaneltabschoosingrepresentative',

    requires: [
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#selectedDataCust': {
                    click: 'onCopyDataToTab'
                },
                '#cancelWindow': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    init: function () {
        var self = this,
            window = this.getView(),
            vm = self.getViewModel(),
            store = vm.getStore('ChoosingListStoreForms');
        if (vm && vm.get('CUST_SEQ_ID')) {
            // загрузка справочника: "form":"CustIdDocList","action":"readByCustId","custSeqId":179529031
            store.load({
                scope: self,
                params: {
                    custSeqId: vm.get('CUST_SEQ_ID')
                },
                callback: function(data, respond) {
                    if (data.length <= 0) {
                        Ext.Msg.alert('Внимание!', 'Нет данных для обработки.');
                        setTimeout(function () {
                            window.close();
                        }, 500);
                    }
                }
            });
        }
    },

    onCopyDataToTab: function() {
        var self = this,
            window = this.getView(),
            chooseSelection = self.lookupReference('chooseSelectionGrid').getSelectionModel().getSelection(),
            vm = self.getViewModel(),
            data, store, respObj,
            vmTab = (vm) ? vm.getParent() : false,
            defVal = '0',
            oes321ID = vm.get('oes_321_id'),
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            dateFields =[
                'GR',
                'VD3',
                'VD6',
                'VD7',
                'MC2',
                'MC3'
            ],
            systemFields = [
                'ADDRESS_TEXT',
                'KD_TX',
                'TRXN_DT',
                'SRC_CD',
                'CUST_SEQ_ID',
                'RV_STATUS_NM',
                'ROW_TYPE_CD',
                'ROW_TYPE_NM'
            ];
 
        if (vm && chooseSelection.length == 1) {
            data = chooseSelection[0];
            if (vmTab && data.get('RLTD_CUST_SEQ_ID') && data.get('RLTD_CUST_SEQ_ID') !== '' && data.get('RLTD_CUST_SEQ_ID') > 0) {
                store = vmTab.get('OES321PartyData');
                // добавим loading обработки:
                if(!common.globalinit.loadingMaskCmp) {
                    common.globalinit.loadingMaskCmp = self.getView();
                }
                if (numberTab) {
                    Ext.Ajax.request({
                        url: common.globalinit.ajaxUrl,
                        method: common.globalinit.ajaxMethod,
                        headers: common.globalinit.ajaxHeaders,
                        timeout: common.globalinit.ajaxTimeOut,
                        params: Ext.encode({
                            form: 'OESCustSearch',
                            action: 'search',
                            searchType: 'CUST',
                            cust_seq_id: data.get('RLTD_CUST_SEQ_ID'),
                            oes_321_id: oes321ID
                        }),
                        success: function (response) {
                            var responseObj = Ext.JSON.decode(response.responseText),
                                setVal, fieldInto, setValY, setValM, setValD, fieldInto2;
                            if (responseObj.message) {
                                Ext.Msg.alert('', responseObj.message);
                            }
                            var resp = responseObj.data[0];
                            for (var paramF in resp) {
                                if (Ext.Array.indexOf(systemFields, paramF) !== -1) {
                                    continue;
                                }
                                fieldInto = '';
                                setVal = '';
                                setVal = resp[paramF];
                                if (Ext.Array.indexOf(dateFields, paramF) !== -1) {
                                    fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + paramF);
                                    setVal = (setVal) ? setVal.substring(0, 10): setVal;
                                    setValY = (setVal) ? setVal.substring(0, 4): setVal;
                                    setValM = (setVal) ? setVal.substring(5, 7): setVal;
                                    setValD = (setVal) ? setVal.substring(8): setVal;
                                    setVal = (setVal) ? new Date(setValY, setValM, setValD, '0', '0', '0') : new Date('2099', '0', '1', '0', '0', '0');
                                } else if (paramF === 'KODCR' || paramF === 'KODCN') {
                                    fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + paramF + '_1');
                                    fieldInto2 = vmTab.getView().lookupReference('t' + numberTab + '_' + paramF + '_2');
                                    if (setVal.length == 5) {
                                        store.set(paramF, (setVal ? setVal : '0'));
                                        var setVal2 = setVal.substr(3, 5);
                                        setVal = setVal.substr(0, 3);
                                        fieldInto.setValue(setVal);
                                        fieldInto2.setValue(setVal2);
                                        continue;
                                    }
                                } else {
                                    fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + paramF);
                                    setVal = (setVal) ? setVal : '0';
                                }
                                if (fieldInto) {fieldInto.setValue(setVal);}
                            }
                         }
                    });
                    window.close();
                } else {
                    return false;
                }
            }
        } else if (chooseSelection.length <= 0) {
            Ext.Msg.alert('Внимание!', 'Нет выделенного значения.');
            return false;
        } else if (chooseSelection.length > 1) {
            Ext.Msg.alert('Внимание!', 'Выделенно больше 1 значения.');
            return false;
        }
    },

    onClickCancelWindow: function (button, eOpts) {
        var window = this.getView(),
            controller = this;
        window.close();
    }
});
