Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ChoosingRepresentative', {
    extend: 'Ext.window.Window',
    xtype: 'oes321detailspanel-tabs-choosing-representative-form',
    controller: 'oes321detailspaneltabschoosingrepresentative',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oes321detailspaneltabschoosingrepresentativevm'
    },
    requires: [
        'Ext.layout.container.Fit',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ChoosingRepresentativeController',
        'AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ChoosingRepresentativeModel'
    ],
    bind: {
        title: '{title}'
    },
    width: "80%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        /**
         * Обрабатываем событие отрисовки
         * @param window
         */
        render: function (window) {
            if (Ext.isIE8) {
                window.setWidth(Math.round(document.body.clientWidth / 100 * 80));
            }
        },
        resize: function (wnd) {
            wnd.center();
        }
    },
            items: [
                {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{ChoosingListStoreForms}'
                    },
                    listeners : {
                        itemdblclick: function(grid, rowIndex, columnIndex, e) {
                            var view = (grid.up()) ? ((grid.up().up()) ? ((grid.up().up().up()) ? grid.up().up().up() : false) : false) : false,
                                controller = (view) ? view.getController() : false;
                            if (controller) {
                                controller.onCopyDataToTab();
                            }
                        }
                    },
                    reference: 'chooseSelectionGrid',
                    /*
                     {
                         "RLSHP_TP_NM":"Мать",
                         "JOB_TITL_NM":"",
                         "FULL_NM":"БАННОВА ИРИНА МАНСУРОВНА",
                         "BIRTH_DT":"",
                         "RLSHP_REASON_TX":"",
                         "STATUS_TX":"Неизвестен",
                         "START_DT":"",
                         "END_DT":"",
                         "END_REASON_TX":"",
                         "SRC_CD":"EKSR|362120191",
                         "SRC_CHANGE_DT":"",
                         "SRC_USER_NM":"",
                         "CUST_CUST_SEQ_ID":379438
                     }

                     {
                         "RLSHP_TP_NM":"Руководитель организации",
                         "JOB_TITL_NM":"ИНДИВИДУАЛЬНЫЙ ПРЕДПРИНИМАТЕЛЬ ",
                         "FULL_NM":"БАЙГУЛОВА ЕЛЕНА АНАТОЛЬЕВНА",
                         "BIRTH_DT":"",
                         "RLSHP_REASON_TX":"",
                         "STATUS_TX":"Неизвестен",
                         "START_DT":"18.08.1998",
                         "END_DT":"",
                         "END_REASON_TX":"",
                         "SRC_CD":"EKSP|260215920",
                         "SRC_CHANGE_DT":"",
                         "SRC_USER_NM":"",
                         "CUST_CUST_SEQ_ID":645105
                     }
                     */
                    columns: [
                        {
                            text: 'Вид связи',
                            stateId: 'RLSHP_TP_NM',
                            dataIndex: 'RLSHP_TP_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Должность',
                            stateId: 'JOB_TITL_NM',
                            dataIndex: 'JOB_TITL_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Наименование лица',
                            stateId: 'FULL_NM',
                            dataIndex: 'FULL_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Дата рег./ рожд.',
                            stateId: 'BIRTH_DT',
                            dataIndex: 'BIRTH_DT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Основание связи',
                            stateId: 'BIRTH_DT',
                            dataIndex: 'BIRTH_DT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Статус',
                            stateId: 'STATUS_TX',
                            dataIndex: 'STATUS_TX',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            // xtype: 'datecolumn',
                            // format: 'd.m.Y',
                            text: 'Начало действия',
                            stateId: 'START_DT',
                            dataIndex: 'START_DT',
                            tdCls: 'v-align-middle',
                            minWidth: 115,
                            flex: 1
                        },
                        {
                            // xtype: 'datecolumn',
                            // format: 'd.m.Y',
                            text: 'Окончание действия',
                            stateId: 'END_DT',
                            dataIndex: 'END_DT',
                            tdCls: 'v-align-middle',
                            minWidth: 115,
                            flex: 1
                        },
                        {
                            text: 'Причина прекращения',
                            stateId: 'END_REASON_TX',
                            dataIndex: 'END_REASON_TX',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Система источник',
                            stateId: 'SRC_CD',
                            dataIndex: 'SRC_CD',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Дата изменения',
                            stateId: 'SRC_CHANGE_DT',
                            dataIndex: 'SRC_CHANGE_DT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Пользователь, внесший изменения',
                            stateId: 'SRC_USER_NM',
                            dataIndex: 'SRC_USER_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'selectedDataCust',
            text: 'Выбрать'
        },
        {
            itemId: 'cancelWindow',
            text: 'Закрыть'
        }
    ]
});