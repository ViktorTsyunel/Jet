Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SearchClient', {
    extend: 'Ext.window.Window',
    xtype: 'oes321detailspanel-tabs-search-client-form',
    controller: 'oes321detailspaneltabssearchclient',
    iconCls: 'icon-report',
    requires: [
        'AML.view.monitoring.alerts.OES321Details.Tabs.Forms.SearchClientController',
        'AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ViewClientFromSearchModel'
    ],
    viewModel: {
        type: 'oes321detailspaneltabssearchclientvm'
    },
    bind: {
        title: '{title}'
    },
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        /**
         * Обрабатываем событие отрисовки
         * @param window
         */
        render: function (window) {
            if (Ext.isIE8) {
                window.setWidth(Math.round(document.body.clientWidth / 100 * 80));
            }
        },
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            tbar: [
                {
                    xtype: 'oes321detailspanel-tabs-search-client-topbar',
                    flex: 1
                }
            ],
            items: [
                {
                    xtype: 'gridpanel',
                    reference: 'searchClientGrid',
                    selModel: {
                        mode: 'SINGLE'
                    },
                    flex: 1,
                    store: {
                        model: 'AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ViewClientFromSearchModel'
                    },
                    listeners: {
                        itemdblclick: function (grid, rowIndex) {
                            var window = this.getView(),
                                rvm = window.up().up().up(),
                                vm = rvm.getViewModel(),
                                vmTab = (vm) ? vm.getParent() : false,
                                view = (grid.up()) ? ((grid.up().up()) ? ((grid.up().up().up()) ? grid.up().up().up() : false) : false) : false,
                                controller = (view) ? view.getController() : false;
                            if (controller) {
                                controller.openRecordForReview(rowIndex, vmTab);
                            }
                        },
                        select: function (grid, record) {
                            var window = this.getView(),
                                rvm = (window) ? window.up().up().up() : false,
                                radioGr = (rvm) ? rvm.lookupReference('checkHowRuleClientLoad') : false,
                                valFirstCheck1 = {checkHowRuleClientLoad: 1},
                                valFirstCheck2 = {checkHowRuleClientLoad: 2};

                            if (record.get('ROW_TYPE_CD') === 'OES') {
                                if (radioGr) {
                                    radioGr.setValue(valFirstCheck1);
                                }
                            } else {
                                if (radioGr) {
                                    radioGr.setValue(valFirstCheck2);
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            text: 'Тип клиента',
                            stateId: 'TU',
                            dataIndex: 'TU',
                            tdCls: 'v-align-middle',
                            flex: 1,
                            renderer: function (value) {
                                switch (true) {
                                    case (value == 1):
                                        return "ЮЛ";
                                    //break;
                                    case (value == 0):
                                    case (value == 2):
                                    case (value == 4):
                                        return "ФЛ";
                                    //break;
                                    case (value == 3):
                                        return "ИП";
                                    //break;
                                }
                            }
                        },
                        {
                            text: 'Найдено в',
                            stateId: 'ROW_TYPE_NM',
                            dataIndex: 'ROW_TYPE_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Наименование/ФИО',
                            stateId: 'NAMEU',
                            dataIndex: 'NAMEU',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'ИНН',
                            stateId: 'ND',
                            dataIndex: 'ND',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'ОГРН',
                            stateId: 'RG',
                            dataIndex: 'RG',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'ОКПО',
                            stateId: 'SD',
                            dataIndex: 'SD',
                            tdCls: 'v-align-middle',
                            flex: 1,
                            renderer: function (value, metaData, record) {
                                switch (true) {
                                    case (record.get('TU') == 1):
                                        return value;
                                    case (record.get('TU') == 0):
                                    case (record.get('TU') == 2):
                                    case (record.get('TU') == 4):
                                    case (record.get('TU') == 3):
                                        return "";
                                }
                            }
                        },
                        {
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            dateFormat: 'c',
                            text: 'Дата рег./рожд.',
                            stateId: 'GR',
                            dataIndex: 'GR',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Адрес',
                            stateId: 'ADDR_TEXT',
                            dataIndex: 'ADDR_TEXT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Документ',
                            stateId: 'KD',
                            dataIndex: 'KD',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Серия документа',
                            stateId: 'SD',
                            dataIndex: 'SD',
                            tdCls: 'v-align-middle',
                            flex: 1,
                            renderer: function (value, metaData, record) {
                                switch (true) {
                                    case (record.get('TU') == 1):
                                        return "";
                                    case (record.get('TU') == 0):
                                    case (record.get('TU') == 2):
                                    case (record.get('TU') == 4):
                                    case (record.get('TU') == 3):
                                        return value;
                                }
                            }
                        },
                        {
                            text: 'Номер документа',
                            stateId: 'VD1',
                            dataIndex: 'VD1',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            dateFormat: 'c',
                            text: 'Дата посл.изм./Дата опер.',
                            stateId: 'TRXN_DT',
                            dataIndex: 'TRXN_DT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Система-источник',
                            stateId: 'SRC_CD',
                            dataIndex: 'SRC_CD',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'ID клиента',
                            stateId: 'CUST_SEQ_ID',
                            dataIndex: 'CUST_SEQ_ID',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Статус ОЭС',
                            stateId: 'RV_STATUS_NM',
                            dataIndex: 'RV_STATUS_NM',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Ответственный',
                            stateId: 'OWNER_ID',
                            dataIndex: 'OWNER_ID',
                            tdCls: 'v-align-middle',
                            flex: 1
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'linkItemSeachClinet',
            text: 'Привязать',
            bind: {
                hidden: '{IS_SEARCH_OPERATION}'
            }
        },
        {
            xtype: 'container',
            flex: 1
        },
        {
            xtype: 'container',
            bind: {
                hidden: '{IS_SEARCH_OPERATION}'
            },
            items: [
                {
                    xtype: 'radiogroup',
                    reference: 'checkHowRuleClientLoad',
                    bind: {
                        value: '{switchItem}'
                    },
                    defaults: {
                        name: 'checkHowRuleClientLoad',
                        padding: '0 20px 0 10px'
                    },
                    fieldLabel: 'Записать поля ОЭС',
                    labelAlign: 'top',
                    columns: 1,
                    items: [
                        {
                            boxLabel: 'Все',
                            inputValue: 1,
                            checked: true
                        },
                        {
                            boxLabel: 'Незаполненные',
                            inputValue: 2
                        }
                    ]
                }
            ]
        },
        {
            itemId: 'selectItemSeachClinet',
            text: 'Выбрать'
        },
        {
            itemId: 'cancelWindowSeachClinet',
            text: 'Закрыть'
        }
    ]
});