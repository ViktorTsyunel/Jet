Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.Forms.ChoosingRepresentativeModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.oes321detailspaneltabschoosingrepresentativevm',
	data: {
		title: 'Выбор представителя',
        CUST_SEQ_ID: null
	},
    stores: {
        // {"form":"CustCustList","action":"readByCustId","custSeqId":164870927}
        // Справочник Представителей
        ChoosingListStoreForms: {
            fields: [
                {
                    name: 'GR',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                },
                {
                    name: 'VD3',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                },
                {
                    name: 'VD6',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                },
                {
                    name: 'VD7',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                },
                {
                    name: 'MC2',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                },
                {
                    name: 'MC3',
                    type: 'date',
                    dateFormat: 'd.m.Y'
                }
            ],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustCustList',
                    action: 'readByCustId'
                }
            }
        }
    }
});
