Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.OperationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.oes321detailsoperationtab',

    toggleCreditInstitution: function (btn) {
        var vm = this.getViewModel();

        btn.pressed ? btn.removeCls('closed') && btn.addCls('opened') : btn.removeCls('opened') && btn.addCls('closed');
        vm.set('showCreditInstitutionContent', btn.pressed);
    },

    toggleFilialKO: function (btn) {
        var vm = this.getViewModel();

        btn.pressed ? btn.removeCls('closed') && btn.addCls('opened') : btn.removeCls('opened') && btn.addCls('closed');
        vm.set('showFilialKOContent', btn.pressed);
    },

    TBChange: function(combo, newValue, oldValue) {
        var vm = this.getViewModel();

        vm.set('selectedTB', combo.selection.data);
    },

    bPayerChangedHandler: function(combo, newValue, oldValue) {
        var vm = this.getViewModel(),
            TUHardStore = vm.getParent().getStore('TUHardStore');

        if(newValue != 0) {
            TUHardStore.removeAt(3);
        } else if(TUHardStore.getCount() < 4) {
            TUHardStore.add({id: 4, text: '4 – Участник не определен'});
        }
    },

    checkFieldServerError: function (container, value, eOpts) {
        if (value) {
            var self = this,
                ref,
                store = self.getViewModel().getParent().getStore('OESCheckStore'),
                fieldsCheck = [],
                msgFields = {};

            store.each(function (item) {
                ref = 'base_' + item.data.COLUMN_NM;
                if (item.data.COLUMN_NM === 'DESCR_1' || item.data.COLUMN_NM === 'DESCR_2') {
                    ref = 'base_DESCR_CONCAT';
                }
                if (item.data.COLUMN_NM === 'PRIM_1' || item.data.COLUMN_NM === 'PRIM_2') {
                    ref = 'base_PRIM_CONCAT';
                }
                if (item.data.TABLE_NM === 'RF_OES_321' && container.getReference() === ref) {
                    if (Ext.Array.indexOf(fieldsCheck, ref) === -1) {
                        container.markInvalid('');
                        fieldsCheck.push(ref);
                    }
                    if (msgFields[ref]) {
                        msgFields[ref] = msgFields.blockTab + "<br/>" + item.data.MSG_TX;
                    } else {
                        msgFields[ref] = item.data.MSG_TX;
                    }
                }
            });
            Ext.Array.each(fieldsCheck, function(item) {
                container.markInvalid(msgFields[item]);
            });
        }
    }
});
