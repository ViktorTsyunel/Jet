Ext.define('AML.view.monitoring.alerts.OES321Details.Tabs.BehalfController', {
    extend: 'AML.view.monitoring.alerts.OES321Details.PanelController',
    alias: 'controller.oes321detailsbehalftab',

    init: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            OES321PartyStore = vm.getStore('OES321PartyStore');

        // добавим loading обработку:
        if(!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = vm.getParent().getParent().getView();
        }

        // загрузка информации от имени и по поручению:
        OES321PartyStore.load({
            params: {
                form: 'OES321Party',
                action: 'read',
                oes321Id: vm.get('oes321Id'),
                block_nb: 4
            },
            callback: function(records, operation, success) {
                vm.set('OES321PartyData', this.getAt(0));
            }
        });
    },

    checkFieldServerError: function (container, value, eOpts) {
        var ref = container.getReference(),
            fieldsCombo = [
                't4_KODCR_2',
                't4_KODCN_2'
            ],
            fieldsComboSlice = [
                't4_KODCR',
                't4_KODCN'
            ];
        if (value) {
            var self = this,
                objMsg = self.getViewModel().getParent().getView().lookupController().objMsg;
            if (ref && Ext.Array.indexOf(fieldsCombo, ref) !== -1) {
                ref = ref.slice(0,-2);
            }
            if (objMsg && ref && objMsg[ref]) {
                container.markInvalid(objMsg[ref]);
                if (ref && Ext.Array.indexOf(fieldsComboSlice, ref) !== -1) {
                    var field = container.previousSibling('combobox').getActionEl();
                    field.el.addCls(Ext.baseCSSPrefix + 'form-invalid-field');
                    field.el.addCls(Ext.baseCSSPrefix + 'form-invalid-field-default');
                    field.up().el.addCls(Ext.baseCSSPrefix + 'form-text-wrap-invalid');
                }
            }
        }
    }
});
