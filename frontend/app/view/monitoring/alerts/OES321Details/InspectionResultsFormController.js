Ext.define('AML.view.settings.InspectionResultsFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inspectionresultsform',

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            OESCheckStore = vm.getStore('OESCheckStore');
    },

    cancelForm: function () {
        this.getView().close();
    },

    clickGoError: function (container, cmp, tr, rowIndex, e, record, cmpGrid, eOpts, operation) {
        var text  = cmp.textContent || cmp.innerText;
        var contentText = (text) ? text.replace(new RegExp(/^(\s|\u00A0)+/g), '') : false;
        if (contentText !== false) {
            var self = this,
                field,
                tabTmp,
                refField,
                vmForm = self.getViewModel(),
                vm = vmForm.getParent(),
                vmView = vm.getView(),
                oes321DetailsTabPanel = vmView.down('tabpanel'),
                vmTab,
                btn1,
                btn2,
                viewTab,
                fieldsCombo = [
                    't0_KODCR',
                    't0_KODCN',
                    't0_KODCN_B',
                    't0_KODCN_R',
                    't1_KODCR',
                    't1_KODCN',
                    't2_KODCR',
                    't2_KODCN',
                    't3_KODCR',
                    't3_KODCN',
                    't3_KODCN_B',
                    't3_KODCN_R',
                    't4_KODCR',
                    't4_KODCN'
                ],
                fieldsPrim = [
                    'base_PRIM_1',
                    'base_PRIM_2'
                ],
                fieldsDescr = [
                    'base_DESCR_1',
                    'base_DESCR_2'
                ],
                fieldsFromFTab = [
                    'base_NUMB_S',
                    'base_REGN',
                    'base_BIK_S',
                    'base_ND_KO',
                    'base_KTU_S',
                    'base_TEL'
                ],
                fieldsFromSTab = [
                    'base_NUMBF_SS',
                    'base_BIK_SS',
                    'base_KTU_SS'
                ];

            switch(true)
            {
                case (record.get('TABLE_NM') === 'RF_OES_321'):
                    oes321DetailsTabPanel.setActiveTab(0);
                    tabTmp = 'app-oes321detailspanel-operationtab';
                    refField = 'base_' + record.get('COLUMN_NM');
                    break;
                case record.get('BLOCK_CD') === '0':
                    oes321DetailsTabPanel.setActiveTab(1);
                    tabTmp = 'app-oes321detailspanel-payertab';
                    refField = 't0_' + record.get('COLUMN_NM');
                    break;
                case record.get('BLOCK_CD') === '1':
                    oes321DetailsTabPanel.setActiveTab(4);
                    tabTmp = 'app-oes321detailspanel-representativetab';
                    refField = 't1_' + record.get('COLUMN_NM');
                    break;
                case record.get('BLOCK_CD') === '2':
                    oes321DetailsTabPanel.setActiveTab(5);
                    tabTmp = 'app-oes321detailspanel-destinationtab';
                    refField = 't2_' + record.get('COLUMN_NM');
                    break;
                case record.get('BLOCK_CD') === '3':
                    oes321DetailsTabPanel.setActiveTab(2);
                    tabTmp = 'app-oes321detailspanel-recipienttab';
                    refField = 't3_' + record.get('COLUMN_NM');
                    break;
                case record.get('BLOCK_CD') === '4':
                    oes321DetailsTabPanel.setActiveTab(3);
                    tabTmp = 'app-oes321detailspanel-behalftab';
                    refField = 't4_' + record.get('COLUMN_NM');
                    break;
            }
            if (refField && Ext.Array.indexOf(fieldsCombo, refField) !== -1) {
                refField = refField + '_1';
            }
            if (refField && Ext.Array.indexOf(fieldsPrim, refField) !== -1) {
                refField = 'base_PRIM_CONCAT';
            }
            if (refField && Ext.Array.indexOf(fieldsDescr, refField) !== -1) {
                refField = 'base_DESCR_CONCAT';
            }
            if (tabTmp) {
                vmTab = oes321DetailsTabPanel.down(tabTmp).getViewModel();
                viewTab = vmTab.getView();
            } else {
                return;
            }
            if(refField && Ext.Array.indexOf(fieldsFromFTab, refField) !== -1) {
                btn1 = viewTab.lookupReference('buttomExpand1');
                self.changeButtomPressed(btn1, vmTab);
            }
            if(refField && Ext.Array.indexOf(fieldsFromSTab, refField) !== -1) {
                btn1 = viewTab.lookupReference('buttomExpand1');
                btn2 = viewTab.lookupReference('buttomExpand2');
                self.changeButtomPressed(btn1, vmTab);
                self.changeButtomPressed(btn2, vmTab);
            }
            if (tabTmp) {
                vmTab = oes321DetailsTabPanel.down(tabTmp).getViewModel();
                viewTab = vmTab.getView();
                field = viewTab.lookupReference(refField);
                field.focus();
            }
            self.closeView();
        }
    },

    changeButtomPressed: function (btn, vm) {
        btn.pressed = true;
        var stCont;
        if (btn.reference == 'buttomExpand1') {
            stCont = 'showCreditInstitutionContent';
        } else if (btn.reference == 'buttomExpand2') {
            stCont = 'showFilialKOContent';
        }
        btn.pressed ? btn.removeCls('closed') && btn.addCls('opened') : btn.removeCls('opened') && btn.addCls('closed');
        vm.set(stCont, true);
    }
});
