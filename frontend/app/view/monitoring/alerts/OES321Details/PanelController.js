Ext.define('AML.view.monitoring.alerts.OES321Details.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.oes321detailspanel',

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            alertId = vm.get('alertId'),
            oes321DetailsTabPanel = self.getView().down('tabpanel'),
            store = vm.getParent().getStore('alertsStore') || Ext.StoreManager.lookup('alertsStore'),
            storeOES321Details = vm.getStore('OES321Details'),
            recLoad = (store) ? store.findRecord('REVIEW_ID', alertId) : null,
            data = (recLoad) ? recLoad.getData() : null,
            defDate = new Date('2099', '0', '1', '0', '0', '0');

        vm.set('fieldsMap', {
            aboutCustMap: {
                TU: 0,
                VP: '0',
                CARD_B: '0',
                PR: '0',
                NAMEU: '0',
                ND: '0',
                GR: defDate,
                RG: '0',
                BP: '0'
            },
            registerMap: {
                KODCR: '0',
                KODCR_1: '0',
                KODCR_2: '00',
                AMR_S: '0',
                AMR_R: '0',
                AMR_G: '0',
                AMR_U: '0',
                AMR_D: '0',
                AMR_K: '0',
                AMR_O: '0'
            },
            visitMap: {
                KODCN: '0',
                KODCN_1: '0',
                KODCN_2: '00',
                ADRESS_S: '0',
                ADRESS_R: '0',
                ADRESS_G: '0',
                ADRESS_U: '0',
                ADRESS_D: '0',
                ADRESS_K: '0',
                ADRESS_O: '0'
            },
            personalityMap: {
                KD: '0',
                SD: '0',
                VD1: '0',
                VD2: '0',
                VD3: defDate
            },
            rightToLiveMap: {
                VD4: '0',
                VD5: '0',
                VD6: defDate,
                VD7: defDate
            },
            migrationCardMap: {
                MC1: '0',
                MC2: defDate,
                MC3: defDate
            },
            partyBankMap: {
                NAME_B: '0',
                BIK_B: '0',
                KODCN_B: '0',
                KODCN_B_1: '0',
                KODCN_B_2: '00',
                ACC_COR_B: '0',
                ACC_B: '0'
            },
            bankCorrespondentMap: {
                NAME_R: '0',
                BIK_R: '0',
                KODCN_R: '0',
                KODCN_R_1: '0',
                KODCN_R_2: '00'
            },
            bankCartMap: {
                NAME_IS: '0',
                BIK_IS: '0'
            }
        });
        if (this.getView().up('app-monitoring-alerts')) {
            self.parentCtrl = this.getView().up('app-monitoring-alerts').getController();
        } else if (this.getView().up('main-search-operations-form')) {
            self.parentCtrl = this.getView().up('main-search-operations-form').getController();
        } else {
            self.parentCtrl = this.getView().getController();
        }
        // загрузили данные для обработки rows NextPrevious
        if (!vm.get('SEARCH_OPERATION_VIEW')) {
            vm.nextPreviousAlert = self.parentCtrl.getNextPreviousAlerts(alertId, store);
        }
        // установка деталей операции по текущей выбранной записи из списка операций:
        vm.set('detailsTable', data);

        // добавим loading обработку:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = vm.getParent().getParent().getView();
        }

        // загрузка информации об ОЭС:
        storeOES321Details.load({
            scope: self,
            params: {
                form: 'OES321Details',
                action: 'readByOesId',
                oes321Id: vm.get('oes321Id')
            },
            callback: function (records, operation, success) {
                !Ext.isEmpty(records) && vm.set('OES321DetailsTable', records[0].getData());
                var sumeField, sumField, sumConField;

                sumeField = self.getViewModel().get('OES321DetailsTable.SUME');
                sumField = self.getViewModel().get('OES321DetailsTable.SUM');
                sumConField = self.getViewModel().get('OES321DetailsTable.SUM_CON');

                if(sumeField){
                    self.getViewModel().set('OES321DetailsTable.SUME', Ext.util.Format.number(String(parseFloat(sumeField.toString().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'))
                }
                if(sumField){
                    self.getViewModel().set('OES321DetailsTable.SUM', Ext.util.Format.number(String(parseFloat(sumField.toString().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'))
                }
                if(sumConField){
                    self.getViewModel().set('OES321DetailsTable.SUM_CON', Ext.util.Format.number(String(parseFloat(sumConField.toString().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'))
                }
            }
        });
        if((vm.get('OES321DetailsTable.OES_READONLY_FL') === 'Y')
            || vm.get('OES_READONLY_FL_CONVERSION_MANUAL')
            || vm.get('Alerts_READONLY')
            || (vm.get('detailsTable.CHECK_STATUS') === 'S')
            || !vm.get('detailsTable.ALLOW_ALERT_EDITING')) {
// Добавление вкладок:
            oes321DetailsTabPanel.add([
                {
                    xtype: 'app-oes321detailspanel-operationtab',
                    tooltip: 'Сведения об операции (Alt + 1)'
                },
                {
                    xtype: 'app-oes321detailspanel-payertab',
                    title: 'Плательщик',
                    tooltip: 'Плательщик (Alt + 2)'
                },
                {
                    xtype: 'app-oes321detailspanel-recipienttab',
                    title: 'Получатель',
                    tooltip: 'Получатель (Alt + 3)'
                },
                {
                    xtype: 'app-oes321detailspanel-behalftab',
                    title: 'От имени и по поруч.',
                    tooltip: 'От имени и по поруч. (Alt + 4)'
                },
                {
                    xtype: 'app-oes321detailspanel-representativetab',
                    title: 'Предст. плат-ка',
                    tooltip: 'Предст. плат-ка (Alt + 5)'
                },
                {
                    xtype: 'app-oes321detailspanel-destinationtab',
                    title: 'Предст. получ-ля',
                    tooltip: 'Предст. получ-ля (Alt + 6)'
                }
            ]);
        } else {

            // Добавление вкладок:
            oes321DetailsTabPanel.add([
                {
                    xtype: 'app-oes321detailspanel-operationtab',
                    // id: 'oes321detailspanel-operationtab',
                    tooltip: 'Сведения об операции (Alt + 1)'
                },
                {
                    xtype: 'app-oes321detailspanel-payertab',
                    // id: 'oes321detailspanel-payertab',
                    title: 'Плательщик',
                    tooltip: 'Плательщик (Alt + 2)',
                    iconCls: 'icon-button-menu-down',
                    menuTab: {
                        items: [
                            {
                                text: 'Поиск клиента',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-app-oes321detailspanel-payertab',
                                handler: 'openFormSearchClient',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                text: 'Очистить все',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-app-oes321detailspanel-payertab',
                                handler: 'cleanTab',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'app-oes321detailspanel-recipienttab',
                    // id: 'oes321detailspanel-recipienttab',
                    title: 'Получатель',
                    tooltip: 'Получатель (Alt + 3)',
                    iconCls: 'icon-button-menu-down',
                    menuTab: {
                        //hidden: false,
                        //floating: true,
                        items: [
                            {
                                text: 'Поиск клиента',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-recipienttab',
                                handler: 'openFormSearchClient',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                text: 'Очистить все',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-recipienttab',
                                handler: 'cleanTab',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'app-oes321detailspanel-behalftab',
                    // id: 'oes321detailspanel-behalftab',
                    title: 'От имени и по поруч.',
                    tooltip: 'От имени и по поруч. (Alt + 4)',
                    iconCls: 'icon-button-menu-down',
                    menuTab: {
                        //hidden: false,
                        //floating: true,
                        items: [
                            {
                                text: 'Поиск клиента',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-behalftab',
                                handler: 'openFormSearchClient',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                text: 'Очистить все',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-behalftab',
                                handler: 'cleanTab',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'app-oes321detailspanel-representativetab',
                    // id: 'oes321detailspanel-representativetab',
                    title: 'Предст. плат-ка',
                    tooltip: 'Предст. плат-ка (Alt + 5)',
                    iconCls: 'icon-button-menu-down',
                    menuTab: {
                        //hidden: false,
                        //floating: true,
                        items: [
                            {
                                text: 'Поиск клиента',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-representativetab',
                                handler: 'openFormSearchClient',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                text: 'Выбрать представителя',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-representativetab',
                                handler: 'openFormChoosingRepresentative',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                text: 'Очистить все',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-representativetab',
                                handler: 'cleanTab',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'app-oes321detailspanel-destinationtab',
                    // id: 'oes321detailspanel-destinationtab',
                    title: 'Предст. получ-ля',
                    tooltip: 'Предст. получ-ля (Alt + 6)',
                    iconCls: 'icon-button-menu-down',
                    menuTab: {
                        //hidden: false,
                        //floating: true,
                        items: [
                            {
                                text: 'Поиск клиента',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-destinationtab',
                                handler: 'openFormSearchClient',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                text: 'Выбрать представителя',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-destinationtab',
                                handler: 'openFormChoosingRepresentative',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            },
                            {
                                text: 'Очистить все',
                                scale: 'small',
                                iconCls: 'alert-details-button',
                                tabRef: 'ref-oes321detailspanel-destinationtab',
                                handler: 'cleanTab',
                                bind: {
                                    hidden: '{OES_READONLY_FL_CONVERSION}'
                                }
                            }
                        ]
                    }
                }
            ]);
        }

        // первичное заполнение obj данными серверных ошибок
        self.storeMsgLoad();

        oes321DetailsTabPanel.setActiveTab(0);
    },

    objMsg: {}, // объект ошибок "Результаты контроля"

    storeMsgLoad: function (putErrors, closePanel) {
        var self = this,
            vm = self.getViewModel(),
            store = vm.getStore('OESCheckStore'),
            blockTab,
            fieldsCheck = [],
            putErrors = putErrors || false,
            closePanel = closePanel || false,
            msgFields = {},
            ref,
            fieldsCombo2 = [
                't0_KODCR_2',
                't0_KODCN_2',
                't0_KODCN_B_2',
                't0_KODCN_R_2',
                't1_KODCR_2',
                't1_KODCN_2',
                't2_KODCR_2',
                't2_KODCN_2',
                't3_KODCR_2',
                't3_KODCN_2',
                't3_KODCN_B_2',
                't3_KODCN_R_2',
                't4_KODCR_2',
                't4_KODCN_2'
            ],
            fieldsComboSlice = [
                't0_KODCR',
                't0_KODCN',
                't0_KODCN_B',
                't0_KODCN_R',
                't1_KODCR',
                't1_KODCN',
                't2_KODCR',
                't2_KODCN',
                't3_KODCR',
                't3_KODCN',
                't3_KODCN_B',
                't3_KODCN_R',
                't4_KODCR',
                't4_KODCN'
            ];
        store.load({
            params: {
                oesId: vm.get('oes321Id')
            },
            callback: function (records, operation, success) {
                store.each(function (item) {
                    switch (true) {
                        case (item.data.TABLE_NM === 'RF_OES_321'):
                            blockTab = 'base';
                            break;
                        case item.data.BLOCK_CD === '0':
                            blockTab = 't0';
                            break;
                        case item.data.BLOCK_CD === '1':
                            blockTab = 't1';
                            break;
                        case item.data.BLOCK_CD === '2':
                            blockTab = 't2';
                            break;
                        case item.data.BLOCK_CD === '3':
                            blockTab = 't3';
                            break;
                        case item.data.BLOCK_CD === '4':
                            blockTab = 't4';
                            break;
                        default:
                            break;
                    }
                    blockTab += '_' + item.data.COLUMN_NM;
                    if (Ext.Array.indexOf(fieldsCheck, blockTab) === -1) {
                        fieldsCheck.push(blockTab);
                    }
                    if (msgFields[blockTab]) {
                        msgFields[blockTab] = msgFields[blockTab] + '<br/>' + item.data.MSG_TX;
                    } else {
                        msgFields[blockTab] = item.data.MSG_TX;
                    }
                });

                self.objMsg = {};
                Ext.Array.each(fieldsCheck, function (item) {
                    self.objMsg[item] = msgFields[item];
                });
                if (putErrors) {
                    Ext.Array.each(self.getView().query('field'), function (field) {
                        // отбиваем правила(ошибки) с сервера
                        ref = field.getReference() || false;
                        if (ref && Ext.Array.indexOf(fieldsCombo2, ref) !== -1) {
                            ref = ref.slice(0, -2);
                        }
                        if (ref && ref === 'base_PRIM_CONCAT') {
                            ref = 'base_PRIM_1';
                        }
                        if (ref && ref === 'base_DESCR_CONCAT') {
                            ref = 'base_DESCR_1';
                        }
                        if (ref && self.objMsg[ref] !== undefined) {
                            field.markInvalid(self.objMsg[ref]);
                            if (ref && Ext.Array.indexOf(fieldsComboSlice, ref) !== -1 && field.previousSibling('combobox').getActionEl()) {
                                field.previousSibling('combobox').getActionEl().el.addCls(Ext.baseCSSPrefix + 'form-invalid-field');
                                field.previousSibling('combobox').getActionEl().el.addCls(Ext.baseCSSPrefix + 'form-invalid-field-default');
                                field.previousSibling('combobox').getActionEl().up().el.addCls(Ext.baseCSSPrefix + 'form-text-wrap-invalid');
                            }

                        } else {
                            if (ref && Ext.Array.indexOf(fieldsComboSlice, ref) !== -1 && field.previousSibling('combobox').getActionEl()) {
                                field.previousSibling('combobox').getActionEl().el.removeCls(Ext.baseCSSPrefix + 'form-invalid-field');
                                field.previousSibling('combobox').getActionEl().el.removeCls(Ext.baseCSSPrefix + 'form-invalid-field-default');
                                field.previousSibling('combobox').getActionEl().up().el.removeCls(Ext.baseCSSPrefix + 'form-text-wrap-invalid');
                            }
                            if (field.isVisible()) {
                                field.isValid();
                                field.validate();
                            }
                        }
                    });
                }

                if (closePanel) {
                    var alertId = self.getViewModel().get('alertId'),
                        grid = self.parentCtrl.lookupReference('alertsGrid') || self.parentCtrl.lookupReference('alertsOperSearchGrid'),
                        gridStore = grid.getStore(),
                        vmP = self.parentCtrl.getViewModel(),
                        selectedAlertIndex = Ext.Array.indexOf(gridStore, vmP.get('selectedAlert')) !== -1 ? Ext.Array.indexOf(gridStore, vmP.get('selectedAlert')) : 0;
                    // выбираем заново строку из грида для обновления подвала детали:
                    if (alertId !== null && alertId > 0) {
                        if (gridStore.data.items.length > 0) {
                            Ext.Array.forEach(gridStore.data.items, function (item, index) {
                                if (parseInt(item.id, 10) === alertId) {
                                    selectedAlertIndex = index;
                                }
                            })
                        }
                    }
                    if (selectedAlertIndex >= 0) {
                        grid.getView().focusRow(gridStore.getAt(selectedAlertIndex));
                        grid.getView().setSelection(gridStore.getAt(selectedAlertIndex));
                    }
                }
                if (vm.getParent().getView().loadMask) {
                    vm.getParent().getView().setLoading(false)
                }
            }
        });
    },

    loadStoreCheck: function (that, storeOESCheck, vm, checkIsValid) {
        if (vm.get('oes321Id')) {
            var oes321Id = (vm) ? vm.get('oes321Id') : null,
                BATCH_DATE = (vm) ? vm.get('BATCH_DATE') : null;
            Ext.Ajax.request({
                url: common.globalinit.ajaxUrl,
                method: common.globalinit.ajaxMethod,
                headers: common.globalinit.ajaxHeaders,
                timeout: common.globalinit.ajaxTimeOut,
                params: Ext.JSON.encode({
                    form: 'OES321Details',
                    action: 'readCheckStatus',
                    oes321Id: oes321Id,
                    BATCH_DATE: BATCH_DATE
                }),
                success: function (response) {
                    var data = Ext.JSON.decode(response.responseText).data;
                    if (data.length > 0) {
                        vm.set('OES321DetailsTable.CHECK_STATUS', data[0].CHECK_STATUS);
                        vm.set('OES321DetailsTable.CHECK_DT', data[0].CHECK_DT);
                    }
                    vm.getParent().getView().setLoading(false);
                }
            });
        }

    },

    updateStrMsg: function (store, checkIsValid) {
        var self = this,
            vm,
            tabTmp,
            checkIsValid = checkIsValid || false,
            fieldValid = true,
            viewForm = self.getViewModel().getParent().getView(),
            fieldsCheck = [],
            fieldsCombo = [
                't0_KODCR',
                't0_KODCN',
                't0_KODCN_B',
                't0_KODCN_R',
                't1_KODCR',
                't1_KODCN',
                't2_KODCR',
                't2_KODCN',
                't3_KODCR',
                't3_KODCN',
                't3_KODCN_B',
                't3_KODCN_R',
                't4_KODCR',
                't4_KODCN'
            ];

        store.each(function (item) {
            var blockTab = '',
                ref,
                strMsgTmp,
                strMsg;
            switch (true) {
                case (item.data.TABLE_NM === 'RF_OES_321'):
                    tabTmp = 'app-oes321detailspanel-operationtab';
                    blockTab = 'base';
                    break;
                case item.data.BLOCK_CD === '0':
                    tabTmp = 'app-oes321detailspanel-payertab';
                    blockTab = 't0';
                    break;
                case item.data.BLOCK_CD === '1':
                    tabTmp = 'app-oes321detailspanel-representativetab';
                    blockTab = 't1';
                    break;
                case item.data.BLOCK_CD === '2':
                    tabTmp = 'app-oes321detailspanel-destinationtab';
                    blockTab = 't2';
                    break;
                case item.data.BLOCK_CD === '3':
                    tabTmp = 'app-oes321detailspanel-recipienttab';
                    blockTab = 't3';
                    break;
                case item.data.BLOCK_CD === '4':
                    tabTmp = 'app-oes321detailspanel-behalftab';
                    blockTab = 't4';
                    break;
                default:
                    break;
            }
            if (tabTmp) {
                vm = self.getView().down(tabTmp).getViewModel();
                ref = blockTab + '_' + item.data.COLUMN_NM;
                blockTab += '_' + item.data.COLUMN_NM + '_msgtxt';

                if (checkIsValid && ref && Ext.Array.indexOf(fieldsCombo, ref) === -1) {
                    if (ref && vm.getView().lookupReference(ref + '_1') && !vm.getView().lookupReference(ref + '_1').isValid()) {
                        fieldValid = false;
                    }
                    if (ref && vm.getView().lookupReference(ref + '_2') && !vm.getView().lookupReference(ref + '_2').isValid()) {
                        fieldValid = false;
                    }
                } else if (checkIsValid && ref && vm.getView().lookupReference(ref) && !vm.getView().lookupReference(ref).isValid()) {
                    fieldValid = false;
                }
                if (Ext.Array.indexOf(fieldsCheck, blockTab) === -1) {
                    if (fieldValid) {
                        vm.set(blockTab, '');
                    }
                    fieldsCheck.push(blockTab);
                }
                if (fieldValid) { //  && ref && vm.getView().lookupReference(ref)
                    strMsgTmp = vm.get(blockTab);
                    strMsg = (strMsgTmp) ? (strMsgTmp + '<br/>' + item.data.MSG_TX) : item.data.MSG_TX;
                    vm.set(blockTab, '');
                    vm.set(blockTab, strMsg);
                    //vm.getView().lookupReference(ref).markInvalid(strMsg);
                }
                tabTmp = null;
                fieldValid = true;
            }
        });
        if (viewForm.loadMask) {
            viewForm.setLoading(false)
        }
    },

    openControlResult: function (btn) {
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-settings-inspectionresults-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistorylog-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
           Ext.widget({
                xtype: 'app-settings-inspectionresults-form',
                id: 'app-settings-inspectionresults-form',
                viewModel: {
                    data: {
                        isOpenReadOnly: true
                    },
                    parent: this.getViewModel()
                }
            }).show();
        }
    },

    // костыль для нормального отображения ширины при переключении табов:
    detailsTabPanelReadyHandler: function (tabPanel) {
        tabPanel.setWidth(tabPanel.getWidth());
    },

    detailsTabPanelChangeHandler: function (tabPanel, newCard, oldCard) {
        var self = this,
            vm = self.getViewModel();

        vm.set('activeTabName', newCard.title);
    },

    showAlertReasonsBtn: null, // для удаления старой кнопки, при перерисовке tpl

    detailsPanelReadyHandler: function () {
        this.showAlertReasonsBtn && this.showAlertReasonsBtn.destroy(); // если уже есть кнопка, то удаляем

        this.showAlertReasonsBtn = Ext.create({
            xtype: 'button',
            renderTo: Ext.get('showAlertReasonsBtn'),
            text: '',
            cls: 'icon-applications',
            handler: 'showAlertReasons',
            scope: this.parentCtrl
        });
    },

    savedTabCounter: 1,

    applyChanges: function () {
        var self = this,
            seachTab,
            vm = self.getViewModel(),
            oes321DetailsTabPanel = self.getView().down('tabpanel'),
            storeOES321Details = vm.getStore('OES321Details'),
            oes321DetailsTabs = oes321DetailsTabPanel && oes321DetailsTabPanel.items.getRange(),
            isValid = true,
            lastTab = -1,
            formTabs = vm.getParent().getView(),
            operationtab = this.getView().down('app-oes321detailspanel-operationtab'),
            descrField = operationtab.down('#DESCR_CONCAT_field'),
            primField = operationtab.down('#PRIM_CONCAT_field'),
            fieldsCombo = [
                't0_KODCR_2',
                't0_KODCN_2',
                't0_KODCN_B_2',
                't0_KODCN_R_2',
                't1_KODCR_2',
                't1_KODCN_2',
                't2_KODCR_2',
                't2_KODCN_2',
                't3_KODCR_2',
                't3_KODCN_2',
                't3_KODCN_B_2',
                't3_KODCN_R_2',
                't4_KODCR_2',
                't4_KODCN_2'
            ],
            BATCH_DATE = new Date();
        vm.set('BATCH_DATE', BATCH_DATE);
        if (formTabs.loadMask) {
            formTabs.setLoading(false);
        }
        formTabs.setLoading('Обработка...');

        /* TODO добавить в проверку || operationtab.getViewModel().get('DESCR_CONCAT') !== descrField.getValue() когда будет убрано ограничение на длину DESCR_2 */
        if (operationtab.getViewModel().get('DESCR_CONCAT') == '' || descrField.getValue() == '') {
            oes321DetailsTabPanel.setActiveTab(operationtab);
            descrField.isValid();
            isValid = false;
        }
        /* TODO добавить в проверку || operationtab.getViewModel().get('PRIM_CONCAT') !== descrField.getValue() когда будет убрано ограничение на длину PRIM_2 */
        if (operationtab.getViewModel().get('PRIM_CONCAT') == '' || primField.getValue() == '') {
            oes321DetailsTabPanel.setActiveTab(operationtab);
            primField.isValid();
            isValid = false;
        }

        Ext.Array.each(this.getView().query('field'), function (field) {
            if (field.isVisible() && !field.isValid() && !field.validate()) {
                isValid = false;
                seachTab = field.up();
                for (var i = 0, len = 5; i < len; i++) {
                    if (Ext.isDefined(seachTab.tabP) && seachTab.tabP >= 0 && seachTab.tabP < 6) {
                        oes321DetailsTabPanel.setActiveTab(seachTab.tabP);
                    }
                    seachTab = seachTab.up();
                }
            }
        });

        // обновляем и проставление результатов проверки в табы если нет ошибок валидации отправляем на сохранение
        if (isValid) {
            Ext.Array.forEach(oes321DetailsTabs, function (tab, i) {
                var tabVM = (tab.getTdType() === 'app-oes321detailspanel-operationtab') ? tab.getViewModel().getParent() : tab.getViewModel(),
                    tabStore = tabVM.getStore('OES321PartyStore') ? tabVM.getStore('OES321PartyStore') : storeOES321Details,
                    tabData = tabVM.get('OES321PartyData') ? tabVM.get('OES321PartyData').getData() : tabVM.get('OES321DetailsTable'),
                    boolVal,
                    record;


                Ext.Object.each(tabData, function (key, value, myTabData) {
                    // исключаем из обработки данные для полей которые обновляються по запросу "обновление результатов проверки ОЭС"
                    if (key === 'CHECK_DT' || key === 'CHECK_STATUS') {
                        delete tabData[key];
                    }
                    // отработка если изменения в base_OES_321_ORG_ID подтягиваем изменения из TBStore
                    if (key === 'OES_321_ORG_ID') {
                        if (tabData.OES_321_ORG_ID !== tabStore.getAt(0).get('OES_321_ORG_ID')) {
                            var addParamsTB = ['NUMBF_S', 'BIK_S', 'KTU_S', 'REGN', 'ND_KO', 'TEL', 'BRANCH_FL', 'NUMBF_SS', 'BIK_SS', 'KTU_SS'],
                                storeTB = self.getView().down('app-oes321detailspanel-operationtab').getViewModel().getStore('TBStore'),
                                index = (storeTB) ? storeTB.find('OES_321_ORG_ID', tabData.OES_321_ORG_ID) : -1,
                                dataTB = (storeTB && index >= 0) ? storeTB.getAt(index).getData() || [] : [];
                            Ext.Object.each(dataTB, function (name, val) {
                                if (Ext.Array.indexOf(addParamsTB, name) !== -1) {
                                    if (name === 'BRANCH_FL') {
                                        if (val) {
                                            boolVal = '1';
                                        } else {
                                            boolVal = '0';
                                        }
                                        tabData['BRANCH'] = boolVal;
                                    } else {
                                        tabData[name] = val;
                                    }
                                }
                            })
                        }
                    }
                });

                // запишем изменения в хранилище:
                tabStore.getAt(0).set(tabData);
                record = tabStore.getAt(0);
                record.set(tabData);

                if (tabStore.isChanges()) {
                    lastTab = i;
                }
            });
            Ext.Array.forEach(oes321DetailsTabs, function (tab, i, tabs) {
                var tabVM = (tab.getTdType() === 'app-oes321detailspanel-operationtab') ? tab.getViewModel().getParent() : tab.getViewModel(),
                    tabStore = tabVM.getStore('OES321PartyStore') ? tabVM.getStore('OES321PartyStore') : storeOES321Details,
                    tabData = tabVM.get('OES321PartyData') ? tabVM.get('OES321PartyData').getData() : tabVM.get('OES321DetailsTable');
                if (lastTab > -1) {
                    self.saveData(tabStore, tabData, lastTab, i, tabs.length);
                }
            });
            if (lastTab === -1) {
                Ext.Array.each(this.getView().query('field'), function (field) {
                    // отбиваем правила(ошибки) с сервера
                    var ref = field.getReference();
                    if (ref && Ext.Array.indexOf(fieldsCombo, ref) !== -1) {
                        ref = ref.slice(0, -2);
                    }
                    if (ref && self.objMsg[ref] !== undefined) {
                        field.markInvalid(self.objMsg[ref]);
                    }
                });
                formTabs.setLoading(false);
            }
        } else {
            formTabs.setLoading(false);
        }
    },

    saveData: function (store, viewModelData, lastTab, numberTab, tabsLength) {
        var self = this,
            closePanel = true,
            putErrors = false,
            vm = self.getViewModel(),
            alertId = viewModelData.REVIEW_ID,
            storeOESCheck = vm.getStore('OESCheckStore');

        if (store.isChanges()) {
            self.savedTabCounter++;
        }

        store.sync({
            params: {
                action: 'update',
                oes321Id: viewModelData.OES_321_ID,
                block_nb: viewModelData.BLOCK_NB,
                BATCH_DATE: vm.get('BATCH_DATE')
            },
            callback: function (options, response) {
                vm.set('flagSave', true);
                setTimeout(function () {
                    if (typeof options.operations[0].success !== 'undefined' && options.operations[0].success) {
                        self.savedTabCounter--;
                        if (self.savedTabCounter === 0 && self.getView()) {
                            closePanel = false;
                            if (vm.get('goRows') === 1 || vm.get('goRows') === -1) {
                                // отрабтка перехода по стрелкам
                                self.parentCtrl = self.getView().up('app-monitoring-alerts').getController();
                                self.afterSaveReloadList(vm, alertId);
                                self.parentCtrl.loadToTabsAlert(vm.get('goRows'), vm.nextPreviousAlert);
                            } else {
                                if (vm.get('copyOpenedOESFlag')) {
                                    vm.set('copyOpenedOESFlag', null);
                                    self.copyOESAJAX(true);
                                }
                                self.parentCtrl.selectRowGrid(alertId);
                                self.useFooter(self);
                                self.afterSaveReloadList(vm, alertId);
                                self.getView().close();
                                self.parentCtrl.getView().setLoading(false);
                            }
                        }
                    }
                    // обновляем и проставление результатов проверки в табы
                    if (parseInt(lastTab, 10) === parseInt(numberTab, 10)) {
                        if (closePanel) {
                            // обновление результатов проверки ОЭС
                            self.loadStoreCheck(self, storeOESCheck, vm, true);
                            putErrors = true;
                            self.storeMsgLoad(putErrors, closePanel);
                        }
                    }

                }, 1);
            }
        });
    },

    rejectChanges: function () {
        var self = this,
            storeOES321Details = self.getViewModel().getStore('OES321Details'),
            oes321DetailsTabs = self.getView().down('tabpanel') && self.getView().down('tabpanel').items.getRange();

        Ext.Array.forEach(oes321DetailsTabs, function (tab) {
            var tabVM = (tab.getTdType() === 'app-oes321detailspanel-operationtab') ? tab.getViewModel().getParent() : tab.getViewModel(),
                tabStore = tabVM.getStore('OES321PartyStore') ? tabVM.getStore('OES321PartyStore') : storeOES321Details;

            tabStore.rejectChanges();
            // обновить данные в vm:
            tabVM.set(tabVM.get('OES321PartyData') ? 'OES321PartyData' : 'OES321DetailsTable', tabStore.getAt(0).getData());
        });
    },

    closePanel: function () {
        var self = this,
            vm = self.getViewModel(),
            storeOES321Details = vm.getStore('OES321Details'),
            oes321DetailsTabs = self.getView().down('tabpanel') && self.getView().down('tabpanel').items.getRange(),
            alertId = vm.get('alertId'),
            oes321Id = vm.get('oes321Id'),
            parentAlertId = vm.get('parentAlertId'),
            parentOES321ID = vm.get('parentOESID'),
            parentCtrl = self.parentCtrl,
            parentVm = parentCtrl.getViewModel(),
            xTypeView,
            xTypeModel;

        // отпработка выхода из режима просмотра при переходе из истории изменений
        if (vm.get('historyAlert')) {
            this.showHistoryActionBack();
            xTypeView = vm.get('xTypeView');
            if (xTypeView === 'app-alerts-oes321detailspanel') {
                // xTypeView = 'app-alerts-oes321detailspanel';
                xTypeModel = 'oes321detailspanelviewmodel';
            }
            if (xTypeView === 'app-alerts-alertdetailspanel') {
                // xTypeView = 'app-alerts-alertdetailspanel';
                xTypeModel = 'alertdetailspanelviewmodel';
            }
            parentCtrl.getView().down('app-alerts-oes321detailspanel').destroy();
            parentCtrl.getView().add({
                xtype: xTypeView,
                viewModel: {
                    type: xTypeModel,
                    data: {
                        alertId: parentAlertId,
                        oes321Id: parentOES321ID,
                        historyAlert: false
                    }
                }
            });
            return;
        }

        if (this.isChangeDetails(oes321DetailsTabs, storeOES321Details) || self.getViewModel().get('flagComboSave')) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    self.savedTabCounter = self.savedTabCounter - 1;
                    self.applyChanges();
                },
                fnNo: function () {
                    // вернуть изменения в хранилище:
                    self.rejectChanges();
                    self.getView().close();
                    self.parentCtrl.selectRowGrid(alertId);
                    if (parentVm.get('flagSave')) {
                        self.afterSaveReloadList(parentVm, alertId);
                    }
                    self.useFooter(self);
                }
            });
        } else {
            if (self.getViewModel().get('flagSave')) {
                if (alertId) {
                    self.afterSaveReloadList(self.getViewModel(), alertId);
                }
            }
            self.getView().close();
            if (self.parentCtrl) {
                self.parentCtrl.selectRowGrid(alertId);
            }
            self.useFooter(self);
        }
    },

    // выбор и передача фокуса записи в гриде
    selectRowGrid: function (alertId) {
        var self = this,
            dataArr = [],
            grid = self.lookupReference('alertsGrid') || self.lookupReference('alertsOperSearchGrid'),
            gridStore = grid.getStore(),
            vm = self.getViewModel(),
            selectedAlertIndex = 0,
            alertId = alertId || vm.get('selectedAlert.REVIEW_ID') || vm.get('selectedOSAlert.REVIEW_ID'),
            view = grid.getView();

        if (alertId) {
            Ext.Array.forEach(gridStore.getRange(), function (item, index) {
                dataArr.push(item.get('REVIEW_ID'));
            });
            selectedAlertIndex = Ext.Array.indexOf(dataArr, alertId) != -1 ? Ext.Array.indexOf(dataArr, alertId) : 0;
        }
        // выставляем select
        view.focusRow(gridStore.getAt(selectedAlertIndex));
        view.setSelection(gridStore.getAt(selectedAlertIndex));
    },

    isChangeDetails: function (oes321DetailsTabs, storeOES321Details) {
        var isChanges = false;
        // запишем изменения в модель: пока не нашел решения проверки на изменение данных
        Ext.Array.forEach(oes321DetailsTabs, function (tab) {
            var tabVM = tab.getViewModel(),
                tabStore = tabVM.getStore('OES321PartyStore') ? tabVM.getStore('OES321PartyStore') : storeOES321Details,
                tabData = tabVM.get('OES321PartyData') ? tabVM.get('OES321PartyData').getData() : tabVM.get('OES321DetailsTable'),
                record,
                changeStore;
            // убираем так как влияют на проверку на изменение
            Ext.Object.each(tabData, function (key, value, myTabData) {
                // исключаем из обработки данные для полей которые обновляються по запросу "обновление результатов проверки ОЭС"
                if (key === 'CHECK_DT' || key === 'CHECK_STATUS') {
                    delete tabData[key];
                }
            });

            changeStore = common.util.component.deepCloneStore(tabStore);
            record = changeStore.getAt(0);
            record.set(tabData);
            if (changeStore.isChanges()) {
                isChanges = true;
            }

            if (tabStore.isChanges()) {
                isChanges = true;
            }
        });

        return isChanges;
    },

    afterSaveReloadList: function (vm, alertId) {
        vm.set('flagSave', false);
        // переподкачка операции в гриде:
        // if (!Ext.isEmpty(alertId) && !vm.getView().up('main-search-operations-form')) {
        if (!Ext.isEmpty(alertId)) {
            if (vm.getParent().getView().getController().type === 'alerts') {
                vm.getParent().getView().getController().refreshAlertsList({
                    alertIds: [alertId],
                    addRecords: true
                });
            }
            if (vm.getView().getController().type === 'alerts') {
                vm.getView().getController().refreshAlertsList({
                    alertIds: [alertId],
                    addRecords: true
                });
            }
            // vm.getParent().getView().getController().refreshAlertsList({
        }
    },

    useFooter: function (self) {
        var toggleBBarBtn = self.parentCtrl.lookupReference('toggleBBarBtn') || self.parentCtrl.lookupReference('toggleBBarOperSearchBtn'),
            monitoringAlertsTopbar = self.parentCtrl.lookupReference('monitoringAlertsTopbar') || self.parentCtrl.lookupReference('main-search-operations-topbar');
        // проверяем был ли открыт подвал
        if (monitoringAlertsTopbar.reestablishView) {
            toggleBBarBtn.setPressed(true);
            monitoringAlertsTopbar.reestablishView = false;
        }
    },

    rebuildOKATOStore: function (store) {
        // своя сортировка
        var item0 = store.findRecord('_ID_', '0', 0, false, false, true),
            item00 = store.findRecord('_ID_', '00', 0, false, false, true),
            records = [],
            addItem0 = item0,
            addItem00 = item00;
        store.remove(item0);
        store.remove(item00);
        store.sort('OKATO_OBJ_NM', 'ASC');
        store.sorters.clear();
        store.each(function (record) {
            records.push(record);
        });
        store.removeAll(true);
        store.add([item0, item00]);
        Ext.each(records, function (record) {
            store.add(record);
        });

        return store;
    },

    combiField1ChangeBlur: function (field) {
        field.nextSibling('textfield').isValid();
        field.getActionEl().el.removeCls(Ext.baseCSSPrefix + 'form-invalid-field');
        field.getActionEl().el.removeCls(Ext.baseCSSPrefix + 'form-invalid-field-default');
        field.getActionEl().up().el.removeCls(Ext.baseCSSPrefix + 'form-text-wrap-invalid');
        field.isValid();
    },

    combiField2ChangeBlur: function (field) {
        var field1 = field.previousSibling('combobox');
        field1.getActionEl().el.removeCls(Ext.baseCSSPrefix + 'form-invalid-field');
        field1.getActionEl().el.removeCls(Ext.baseCSSPrefix + 'form-invalid-field-default');
        field1.getActionEl().up().el.removeCls(Ext.baseCSSPrefix + 'form-text-wrap-invalid');
    },

    showHistoryAction: function () {
        var vm = this.getViewModel(),
            vSOV = (vm) ? vm.get('OES_READONLY_FL_CONVERSION_MANUAL') : false,
            SEARCH_OPERATION_VIEW = (vSOV) ? true : false;
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-settings-inspectionresults-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistorylog-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
            Ext.create({
                xtype: 'app-monitoring-alerts-documenthistory-form',
                id: 'app-monitoring-alerts-documenthistory-form',
                viewModel: {
                    xtype: 'app-monitoring-alerts-documenthistory-form',
                    data: {
                        isOpenReadOnly: true,
                        alertId: vm.get('alertId'),
                        SEARCH_OPERATION_VIEW: SEARCH_OPERATION_VIEW
                    },
                    parent: vm
                }
            }).show();
        }
    },

    showHistoryActionBack: function () {
        var vm = this.getViewModel(),
            vSOV = (vm) ? vm.get('OES_READONLY_FL_CONVERSION_MANUAL') : false,
            SEARCH_OPERATION_VIEW = (vSOV) ? true : false,
            parentAlertId = (vm) ? parseInt(vm.get('parentAlertId'), 10) : null,
            alertId = (vm) ? parseInt(vm.get('alertId'), 10) : null;
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-documenthistory-form');

        if (!isOpenedWindow) {
            Ext.create({
                xtype: 'app-monitoring-alerts-documenthistory-form',
                id: 'app-monitoring-alerts-documenthistory-form',
                viewModel: {
                    xtype: 'app-monitoring-alerts-documenthistory-form',
                    data: {
                        isOpenReadOnly: true,
                        alertId: alertId ? alertId : parentAlertId,
                        SEARCH_OPERATION_VIEW: SEARCH_OPERATION_VIEW
                    },
                    back_parent: vm.getParent()
                    //parent: vm
                }
            }).show();
        }
    },

    showHistoryLogAction: function () {
        var vm = this.getViewModel(),
            vSOV = (vm) ? vm.get('OES_READONLY_FL_CONVERSION_MANUAL') : false,
            SEARCH_OPERATION_VIEW = (vSOV) ? true : false;
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-settings-inspectionresults-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistorylog-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
            Ext.create({
                xtype: 'app-monitoring-alerts-documenthistorylog-form',
                id: 'app-monitoring-alerts-documenthistorylog-form',
                viewModel: {
                    xtype: 'app-monitoring-alerts-documenthistorylog-form',
                    data: {
                        isOpenReadOnly: true,
                        alertId: this.getViewModel().get('alertId'),
                        oes321Id: this.getViewModel().get('oes321Id'),
                        SEARCH_OPERATION_VIEW: SEARCH_OPERATION_VIEW
                    }
                }
            }).show();
        }
    },

    editAttachAction: function () {
        var vm = this.getViewModel(),
            vSOV = (vm) ? vm.get('OES_READONLY_FL_CONVERSION_MANUAL') : false,
            SEARCH_OPERATION_VIEW = (vSOV) ? true : false;

        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-settings-inspectionresults-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistorylog-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
            Ext.widget({
                xtype: 'app-monitoring-alerts-attachments-form',
                id: 'app-monitoring-alerts-attachments-form',
                viewModel: {
                    data: {
                        isOpenReadOnly: false,
                        SEARCH_OPERATION_VIEW: SEARCH_OPERATION_VIEW
                    },
                    parent: this.getViewModel()
                }
            }).show();
        }
    },

    editNoteAction: function () {
        var vm = this.getViewModel(),
            vSOV = (vm) ? vm.get('OES_READONLY_FL_CONVERSION_MANUAL') : false,
            SEARCH_OPERATION_VIEW = (vSOV) ? true : false;
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-settings-inspectionresults-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-documenthistorylog-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
            Ext.widget({
                xtype: 'app-monitoring-alerts-narrative-form',
                id: 'app-monitoring-alerts-narrative-form',
                viewModel: {
                    data: {
                        isOpenReadOnly: false,
                        SEARCH_OPERATION_VIEW: SEARCH_OPERATION_VIEW
                    },
                    parent: this.getViewModel()
                }
            }).show();
        }
    },

    refreshDetailsTable: function (data) {
        var self = this,
            vm = self.getViewModel();

        // установка деталей операции по текущей выбранной записи из списка операций:
        if (vm && data) {
            vm.set('detailsTable', data);
        }
    },

    downloadFile: function (fileId) {
        common.util.Downloader.get({
            url: common.globalinit.ajaxAlertGetAttachmentUrl,
            params: {
                form: 'AlertGetAttachment',
                action: 'getByAttchId',
                attchId: fileId
            }
        });
    },

    loadPageNextAlert: function () {
        this.goLoadAlert(1);
    },

    loadPagePreviousAlert: function () {
        this.goLoadAlert(-1);
    },

    goLoadAlert: function (to) {
        var self = this,
            vm = self.getViewModel();
        if (!vm || !vm.nextPreviousAlert || Ext.isEmpty(vm.nextPreviousAlert)) {
            return false;
        }
        vm.set('goRows', false);

        if (to === 1 && (!vm.nextPreviousAlert.nextAlertId || !vm.nextPreviousAlert.nextOES321Id || !vm.nextPreviousAlert.nextSelectedAlertIndex)) {
            Ext.Msg.alert('Внимание!', 'Переход к следующему ОЭС невозможен. Сообщение отсутствует.');
            return false;
        } else if (to === -1 && (!vm.nextPreviousAlert.previousAlertId || !vm.nextPreviousAlert.previousOES321Id || (!vm.nextPreviousAlert.previousSelectedAlertIndex && vm.nextPreviousAlert.previousSelectedAlertIndex !== 0))) {
            Ext.Msg.alert('Внимание!', 'Переход к предыдущему ОЭС невозможен. Сообщение отсутствует.');
            return false;
        }

        vm.getParent().getView().loadMask;

        // проверяем были ли изменения
        var storeOES321Details = self.getViewModel().getStore('OES321Details'),
            oes321DetailsTabs = self.getView().down('tabpanel') && self.getView().down('tabpanel').items.getRange(),
            alertId = self.getViewModel().get('alertId'),
            vmCtrl = self.parentCtrl.getViewModel();

        if (this.isChangeDetails(oes321DetailsTabs, storeOES321Details)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    vm.set('goRows', to);
                    self.savedTabCounter = self.savedTabCounter - 1;
                    self.applyChanges();
                    //self.afterSaveReloadList(vm, vm.get('alertId'));
                },
                fnNo: function () {
                    // вернуть изменения в хранилище:
                    self.rejectChanges();
                    self.parentCtrl = self.getView().up('app-monitoring-alerts').getController();
                    self.parentCtrl.loadToTabsAlert(to, vm.nextPreviousAlert);
                    if (vmCtrl.get('flagSave')) {
                        self.afterSaveReloadList(vmCtrl, alertId);
                    }
                }
            });
        } else {
            self.afterSaveReloadList(vmCtrl, alertId);
            self.parentCtrl = self.getView().up('app-monitoring-alerts').getController();
            self.parentCtrl.loadToTabsAlert(to, vm.nextPreviousAlert);
        }
    },

    openFormAddressSelection: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            OES321PartyStore = (vm) ? vm.getStore('OES321PartyStore') : false,
            getCustSeqIdTab = (OES321PartyStore) ? OES321PartyStore.getAt(0).get('CUST_SEQ_ID') : false;

        if (getCustSeqIdTab) {
            var form = Ext.widget({
                xtype: 'oes321detailspanel-tabs-address-selection-form',
                viewModel: {
                    data: {
                        CUST_SEQ_ID: getCustSeqIdTab,
                        typeBlock: (btn.config && btn.config.typeBlock) ? btn.config.typeBlock : null
                    },
                    parent: this.getViewModel()
                }
            }).show();
        } else {
            Ext.Msg.alert('Внимание!', 'Участник операции не привязан к справочнику клиентов. Выбор адреса недоступен. Попробуйте привязать участника '
                + vm.get('title') + ' к справочнику клиентов, воспользовавшись сервисом поиска клиента на вкладке '
                + vm.get('title'));
            return false;
        }
    },

    openFormSelectDocument: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            OES321PartyStore = (vm) ? vm.getStore('OES321PartyStore') : false,
            getCustSeqIdTab = (OES321PartyStore) ? OES321PartyStore.getAt(0).get('CUST_SEQ_ID') : false;

        /** TODO delete */
        //getCustSeqIdTab = 179545377;

        if (getCustSeqIdTab) {
            var form = Ext.widget({
                xtype: 'oes321detailspanel-tabs-select-document-form',
                viewModel: {
                    data: {
                        CUST_SEQ_ID: getCustSeqIdTab,
                        typeBlock: (btn.config && btn.config.typeBlock) ? btn.config.typeBlock : null
                    },
                    parent: this.getViewModel()
                }
            }).show();
        } else {
            Ext.Msg.alert('Внимание!', 'Участник операции не привязан к справочнику клиентов. Выбор документа недоступен. Попробуйте привязать участника '
                + vm.get('title') + ' к справочнику клиентов, воспользовавшись сервисом поиска клиента на вкладке '
                + vm.get('title'));
            return false;
        }
    },

    openFormChoosingRepresentative: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            tabRef = (btn.config && btn.config.tabRef) ? btn.config.tabRef : false,
            tbPayer = (tabRef) ? self.getView().lookupReference('ref-app-oes321detailspanel-payertab') : false,
            tbRecipient = (tabRef) ? self.getView().lookupReference('ref-oes321detailspanel-recipienttab') : false,
            tab = (tabRef) ? self.getView().lookupReference(tabRef) : false,
            vmTab = (tab) ? tab.getViewModel() : false,
            vmTbPayer = (tbPayer) ? tbPayer.getViewModel() : false,
            vmTbRecipient = (tbRecipient) ? tbRecipient.getViewModel() : false,
            tabVM = (vmTab) ? vmTab.get('tabVM') : false,
            // storeTab = (vmTab) ? vmTab.getStore('OES321PartyStore') : false,
            storeTabPayer = (vmTbPayer) ? vmTbPayer.getStore('OES321PartyStore') : false,
            storeTabRecipient = (vmTbRecipient) ? vmTbRecipient.getStore('OES321PartyStore') : false,
            storeNeed = (tabVM === '1') ? storeTabPayer : storeTabRecipient,
            storeNeedFirst = (storeNeed) ? storeNeed.getAt(0) : false,
            getCustSeqIdTab = (storeNeedFirst) ? storeNeedFirst.get('CUST_SEQ_ID') : false,
            vOes_321_id = vm.get('es321Id');

        /** TODO delete */
        // d04aPar_@528
        //getCustSeqIdTab = 179529031;

        if (getCustSeqIdTab) {
            var form = Ext.widget({
                xtype: 'oes321detailspanel-tabs-choosing-representative-form',
                viewModel: {
                    data: {
                        CUST_SEQ_ID: getCustSeqIdTab,
                        oes_321_id: vOes_321_id
                    },
                    parent: vmTab
                }
            }).show();
        } else {
            nameTab = (tabVM === '1') ? 'Плательщик' : 'Получатель';
            Ext.Msg.alert('Внимание!', 'Участник операции не привязан к справочнику клиентов. Выбор представителя недоступен. Попробуйте привязать участника '
                + nameTab + ' к справочнику клиентов, воспользовавшись сервисом поиска клиента на вкладке ' + nameTab);
            return false;
        }
    },

    openFormSearchClient: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            tabRef = (btn.config && btn.config.tabRef) ? btn.config.tabRef : false,
            tab = (tabRef) ? self.getView().lookupReference(tabRef) : false,
            vmTab = (tab) ? tab.getViewModel() : false,
            tabVM = (vmTab) ? vmTab.get('tabVM') : false,
            storeTab = (vm) ? vm.getStore('OES321PartyStore') : false,
            getCustSeqIdTab = (storeTab) ? storeTab.getAt(0).get('CUST_SEQ_ID') : false,
            vOes_321_id = vm.get('oes321Id');

        /** TODO delete */
        //getCustSeqIdTab = 179529031;

        if (vm && vmTab) {
            var form = Ext.widget({
                xtype: 'oes321detailspanel-tabs-search-client-form',
                minHeight: 600,
                width: '100%',
                height: 700,
                viewModel: {
                    data: {
                        CUST_SEQ_ID: getCustSeqIdTab,
                        oes_321_id: vOes_321_id
                    },
                    parent: vmTab
                }
            }).show();
        } else {
            Ext.Msg.alert('Внимание!', 'Упс, Поиск');
            return false;
        }
    },

    cleanBlock: function (btn) {
        var self = this,
            items = [],
            vmTab = self.getViewModel(),
            vmTabParent = (vmTab) ? vmTab.getParent() : false,
            fieldsMap = (vmTabParent) ? vmTabParent.get('fieldsMap') : false,
            typeBlock = (btn.config && btn.config.typeBlock) ? btn.config.typeBlock : false,
            store = (vmTab) ? vmTab.getStore('OES321PartyStore') : false;

        if (fieldsMap && typeBlock && store) {
            switch (true) {
                case (typeBlock === 'about-cust'):
                    items = [fieldsMap.aboutCustMap];
                    break;
                case (typeBlock === 'register'):
                    items = [fieldsMap.registerMap];
                    break;
                case (typeBlock === 'visit'):
                    items = [fieldsMap.visitMap];
                    break;
                case (typeBlock === 'personality'):
                    items = [fieldsMap.personalityMap];
                    break;
                case (typeBlock === 'right-to-live'):
                    items = [fieldsMap.rightToLiveMap];
                    break;
                case (typeBlock === 'migration-card'):
                    items = [fieldsMap.migrationCardMap];
                    break;
                case (typeBlock === 'party-bank'):
                    items = [fieldsMap.partyBankMap];
                    break;
                case (typeBlock === 'bank-correspondent'):
                    items = [fieldsMap.bankCorrespondentMap];
                    break;
                case (typeBlock === 'bank-cart'):
                    items = [fieldsMap.bankCartMap];
                    break;
            }

            this.cleanBlockRun(store, items);
        }
    },

    cleanTab: function (btn) {
        var self = this,
            items = [],
            vm = self.getViewModel(),
            tabRef = (btn.config && btn.config.tabRef) ? btn.config.tabRef : false,
            vTab = self.getView(),
            tab = (tabRef) ? vTab.lookupReference(tabRef) : false,
            vmTab = (tab) ? tab.getViewModel() : false,
            tabVM = (vmTab) ? vmTab.get('tabVM') : false,
            store = (vmTab) ? vmTab.getStore('OES321PartyStore') : false,
            fieldsMap = (vm.get('fieldsMap')) ? vm.get('fieldsMap') : false,
            mapA = (fieldsMap) ? [
                    fieldsMap.aboutCustMap,
                    fieldsMap.registerMap,
                    fieldsMap.visitMap,
                    fieldsMap.personalityMap,
                    fieldsMap.rightToLiveMap,
                    fieldsMap.migrationCardMap,
                    fieldsMap.partyBankMap,
                    fieldsMap.bankCorrespondentMap,
                    fieldsMap.bankCartMap,
                    {NOTE: ''}
                ] : [],
            mapB = (fieldsMap) ? [
                    fieldsMap.aboutCustMap,
                    fieldsMap.registerMap,
                    fieldsMap.visitMap,
                    fieldsMap.personalityMap,
                    fieldsMap.rightToLiveMap,
                    fieldsMap.migrationCardMap,
                    {NOTE: ''}
                ] : [];

        if (vmTab && tab && store) {
            switch (true) {
                case (tabVM === '0'): // 'Плательщик'
                case (tabVM === '3'): // 'Получатель'
                    items = mapA;
                    break;
                case (tabVM === '4'): // 'От имени и по поруч.'
                case (tabVM === '1'): // 'Предст. плат-ка'
                case (tabVM === '2'): // 'Предст. получ-ля'
                    items = mapB;
                    break;
            }

            this.cleanBlockRun(store, items);
        }
    },

    cleanBlockRun: function (store, itemsBlock) {
        Ext.Array.forEach(itemsBlock, function (obj, i) {
            Ext.Object.each(obj, function (key, value, myself) {
                store.getAt(0).set(key, value)
            });
        });
    },

    copyOfLocationOrRegistration: function (btn) {
        var self = this,
            fieldFrom, fieldInto, fromKey, intoKey,
            vmTab = self.getViewModel(),
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            tu = (numberTab) ? vmTab.getView().lookupReference('t' + numberTab + '_TU') : false,
            tuValue = (tu) ? parseInt(tu.getValue(), 10) : -1,
            typeBlock = (btn.config && btn.config.typeBlock) ? btn.config.typeBlock : false,
            objKey = {
                KODCR_1: 'KODCN_1',
                KODCR_2: 'KODCN_2',
                AMR_S: 'ADRESS_S',
                AMR_R: 'ADRESS_R',
                AMR_G: 'ADRESS_G',
                AMR_U: 'ADRESS_U',
                AMR_D: 'ADRESS_D',
                AMR_K: 'ADRESS_K',
                AMR_O: 'ADRESS_O'
            },
            keyCheck = [
                'KODCR',
                'KODCN',
                'KODCR_1',
                'KODCN_1',
                'KODCR_2',
                'KODCN_2'
            ],
            store = (vmTab) ? vmTab.getStore('OES321PartyStore').getAt(0) : false;

        if (typeBlock && numberTab && tuValue >= 0) {
            Ext.Object.each(objKey, function (key, value, myself) {
                if (typeBlock == 'register') {
                    // copy of месте нахождения/пребывания
                    fromKey = value;
                    intoKey = key;
                    store.set('KODCR', store.get('KODCN'));
                } else if (typeBlock == 'visit') {
                    // copy of месте регистрации/жительства
                    fromKey = key;
                    intoKey = value;
                    store.set('KODCN', store.get('KODCR'));
                }

                fieldFrom = vmTab.getView().lookupReference('t' + numberTab + '_' + fromKey);
                fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + intoKey);
                if (Ext.Array.indexOf(keyCheck, fieldInto) === -1 || (Ext.Array.indexOf(keyCheck, fieldInto) !== -1 && (tuValue !== 2 || tuValue !== 3))) {
                    fieldInto.setValue(fieldFrom.getValue());
                    var strName = 'OES321PartyData.' + key;
                    vmTab.set(strName, fieldFrom.getValue());
                }
            });
        } else {
            return false;
        }
    },

    copyOfDPP: function (btn) {
        var self = this,
            fieldFrom, fieldInto, fieldVD5, fieldSD, fieldVD1,
            typeBlock = (btn.config && btn.config.typeBlock) ? btn.config.typeBlock : false,
            vmTab = self.getViewModel(),
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            VD4 = (numberTab) ? parseInt(vmTab.getView().lookupReference('t' + numberTab + '_VD4').getValue(), 10) : false,
            objKeyPersonality = {
                KD: 'VD4',
                VD3: 'VD6',
                VD2: '0',
                VD4: VD4
            },
            objKeyMigrationCard = {
                MC1: 'VD5',
                MC2: 'VD6',
                MC3: 'VD7'
            },
            store = (vmTab) ? vmTab.getStore('OES321PartyStore').getAt(0) : false,
            strName;

        if (store && typeBlock && numberTab) {
            if (typeBlock === 'personality') {
                Ext.Object.each(objKeyPersonality, function (key, value, myself) {
                    fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + key);
                    fieldFrom = vmTab.getView().lookupReference('t' + numberTab + '_' + value);

                    switch (true) {
                        case (key === 'VD2'):
                            fieldInto.setValue(value);
                            strName = 'OES321PartyData.' + key;
                            vmTab.set(strName, value);
                            break;
                        case (key === 'VD4'):
                            /*
                             • если VD4=21, то SD=первые 4 цифры от VD5, остальное в  VD1
                             • если VD4<>21, то SD=0, значение VD1=VD5
                             */
                            fieldVD5 = vmTab.getView().lookupReference('t' + numberTab + '_VD5');
                            fieldSD = vmTab.getView().lookupReference('t' + numberTab + '_SD');
                            fieldVD1 = vmTab.getView().lookupReference('t' + numberTab + '_VD1');
                            if (value === 21) {
                                fieldSD.setValue(fieldVD5.getValue().substring(0, 4));
                                fieldVD1.setValue(fieldVD5.getValue().substring(4));
                                strName = 'OES321PartyData.SD',
                                    strNameAdd = 'OES321PartyData.VD1';
                                vmTab.set(strName, fieldVD5.getValue().substring(0, 4));
                                vmTab.set(strNameAdd, fieldVD5.getValue().substring(4));
                            } else {
                                fieldSD.setValue('0');
                                fieldVD1.setValue(fieldVD5.getValue());
                                strName = 'OES321PartyData.VD5';
                                vmTab.set(strName, fieldVD5.getValue());
                            }
                            break;
                        default:
                            fieldInto.setValue(fieldFrom.getValue());
                            strName = 'OES321PartyData.' + key;
                            vmTab.set(strName, fieldFrom.getValue());
                            break;
                    }

                });
            } else if (typeBlock === 'migration-card') {
                // только для случаев, когда в VDX4 стоит 39
                if (VD4 === 39) {
                    Ext.Object.each(objKeyMigrationCard, function (key, value, myself) {
                        fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + key);
                        fieldFrom = vmTab.getView().lookupReference('t' + numberTab + '_' + value);

                        fieldInto.setValue(fieldFrom.getValue());
                        strName = 'OES321PartyData.' + key;
                        vmTab.set(strName, fieldFrom.getValue());
                    });
                } else {
                    Ext.Msg.alert('Внимание!', 'Данный документ не является миграционной картой.');
                    return false;
                }
            }
        }
    },

    copyOfDUL: function (btn) {
        var self = this,
            fieldFrom, fieldInto,
            objKey = {
                VD4: 'KD',
                VD6: 'VD3',
                VD7: new Date('2099', '0', '1', '0', '0', '0'),
                VD5: '0'
            },
            vmTab = self.getViewModel(),
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            SD = (vmTab) ? vmTab.getView().lookupReference('t' + numberTab + '_SD').getValue() : false,
            VD1 = (vmTab) ? vmTab.getView().lookupReference('t' + numberTab + '_VD1').getValue() : false,
            store = (vmTab) ? vmTab.getStore('OES321PartyStore').getAt(0) : false,
            strName;

        if (vmTab && numberTab) {
            Ext.Object.each(objKey, function (key, value, myself) {
                fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + key);
                fieldFrom = vmTab.getView().lookupReference('t' + numberTab + '_' + value);

                switch (true) {
                    case (key === 'VD7'):
                        fieldInto.setValue(value);
                        strName = 'OES321PartyData.' + key;
                        vmTab.set(strName, value);
                        break;
                    case (key === 'VD5'):
                        if (!(store.get('SD') === '0' && store.get('VD1') === '0')) {
                            fieldInto.setValue((SD && (SD !== '0') ? SD : '') + ((VD1 && VD1 !== '0') ? VD1 : ''));
                            strName = 'OES321PartyData.' + key;
                            vmTab.set(strName, (SD && (SD !== '0') ? SD : '') + ((VD1 && VD1 !== '0') ? VD1 : ''));
                        } else {
                            fieldInto.setValue(value);
                            strName = 'OES321PartyData.' + key;
                            vmTab.set(strName, value);
                        }
                        break;
                    default:
                        fieldInto.setValue(fieldFrom.getValue());
                        strName = 'OES321PartyData.' + key;
                        vmTab.set(strName, fieldFrom.getValue());
                        break;
                }
            });
        }
    },

    copyOfMK: function (btn) {
        var self = this,
            fieldFrom, fieldInto,
            objKey = {
                VD4: 39,
                VD6: 'MC2',
                VD7: 'MC3',
                VD5: 'MC1'
            },
            vmTab = self.getViewModel(),
            numberTab = (vmTab) ? vmTab.get('tabVM') : false,
            store = (vmTab) ? vmTab.getStore('OES321PartyStore').getAt(0) : false,
            strName;

        if (vmTab && numberTab) {
            Ext.Object.each(objKey, function (key, value, myself) {
                fieldInto = vmTab.getView().lookupReference('t' + numberTab + '_' + key);
                fieldFrom = vmTab.getView().lookupReference('t' + numberTab + '_' + value);

                switch (true) {
                    case (key === 'VD4'):
                        fieldInto.setValue(value);
                        strName = 'OES321PartyData.' + key;
                        vmTab.set(strName, value);
                        break;
                    default:
                        fieldInto.setValue(fieldFrom.getValue());
                        strName = 'OES321PartyData.' + key;
                        vmTab.set(strName, fieldFrom.getValue());
                        break;
                }
            });
        }
    },

    copyOpenedOES: function () {
        var self = this,
            vm = self.getViewModel(),
            storeOES321Details = vm.getStore('OES321Details'),
            oes321DetailsTabs = self.getView().down('tabpanel') && self.getView().down('tabpanel').items.getRange(),
            alertId = vm.get('alertId'),
            oes321Id = vm.get('oes321Id'),
            msgText,
            type = AML.app.msg._getType('question');


        if (alertId && oes321Id) {
            msgText = 'Вы действительно хотите создать копию операции № ' + alertId + ', ОЭС № ' + oes321Id + '?';

        } else {
            msgText = 'Вы действительно хотите создать копию операции?';
        }
        Ext.Msg.show({
            title: 'Внимание!',
            msg: msgText,
            buttonText: {
                yes: 'Создать', no: 'Отмена'
            },
            icon: type.icon,
            buttons: Ext.Msg.YESNO,
            fn: function (answer) {
                if (answer === 'yes') {
                    if (self.isChangeDetails(oes321DetailsTabs, storeOES321Details) || self.getViewModel().get('flagComboSave')) {
                        AML.app.msg.confirm({
                            type: 'warning',
                            message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                            fnYes: function () {
                                self.savedTabCounter = self.savedTabCounter - 1;
                                vm.set('copyOpenedOESFlag', true);
                                self.applyChanges();
                            },
                            fnNo: function () {
                                self.copyOESAJAX(false);
                            }
                        });
                    } else {
                        self.copyOESAJAX(false);
                    }
                }
            }
        });
    },

    copyOESAJAX: function (editOES) {
        var self = this,
            vm = self.getViewModel(),
            alertId = vm.get('alertId'),
            actvyTypeCds = ['RF_OESCOPY'],
            params;

        if(!editOES) {
            self.getView().close();
        }

        params = {
            form: 'AlertAction',
            action: 'execById',
            actvyTypeCds: actvyTypeCds,
            alertIds: [alertId]
        };

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = this.getView();
        }
        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('Внимание!', responseObj.message);
                }
                if (responseObj.success && responseObj.alertIds && (responseObj.alertIds.length >= 1)) {
                    var viewAppMain = Ext.getCmp('app-main-id'),
                        alertsListVM = (self.parentCtrl) ? self.parentCtrl.getViewModel() : false,
                        alertsList = (alertsListVM) ? alertsListVM.getView() : false,
                        alertsListStore = (alertsListVM) ? alertsListVM.getStore('alertsStore') : false,
                        vmAppMain = (viewAppMain) ? viewAppMain.getViewModel() : false;

                    // фиксируем id созданой записи и подгружаем грид для нее
                    if (vmAppMain) {
                        vmAppMain.set('createAlertReviewId', responseObj.alertIds[0]);
                        vmAppMain.set('createAlertADD', true);
                        vmAppMain.set('newCountAlert', 0);
                    }

                    if (alertsListStore) {
                        alertsList.getViewModel().getStore('alertsStore').load({
                            alertIds: responseObj.alertIds[0],
                            addRecords: true,
                            //scope: self,
                            callback: function () {
                                // прогружаем грид
                                AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                            }
                        });
                    } else {
                        // прогружаем грид
                        AML.app.page.show('app-monitoring-alerts', Ext.getCmp('pageContainer'));
                    }
                }
            }
        });
    }
});
