Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.Tabs.RepresentativePayer', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-alertdetailspanel-representativepayertab',
    controller: 'alertdetailsrepresentativepayertab',
    viewModel: {
        type: 'representativepayertabviewmodel'
    },
    bind: {
        title: '{title}'
    },
    width: '100%',
    padding: '5px',
    items: [
        {
            xtype: 'container',
            listeners: {
                el: {
                    click: 'tableToggle'
                }
            },
            bind: {
                html: '<table class="payer-info">'+
                '<tr>'+
                '<th colspan="4" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Сведения об участнике</span> <div style="margin-left: 240px;" class="arrow-ico {payerInfoExpandedClass}" tabindex="0">-</div></th>'+
                '</tr>'+
                '<tr>'+
                '<td style="padding: 0;" colspan="4"><hr></td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right; width: 176px;"><b>Тип клиента</b></td>'+
                '<td>{tplData.CUST_TYPE_CD}</td>'+
                '<td style="text-align: right"><b>Резидент</b></td>'+
                '<td style="width: 45%">{tplData.RESIDENT_FL}</td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right"><b>Наименование</b></td>'+
                '<td>{tplData.CUST_NM}</td>'+
                '<td style="text-align: right"><b>Пол</b></td>'+
                '<td>{tplData.GENDER_CD}</td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right"><b>ИНН</b></td>'+
                '<td>{tplData.INN_NB}</td>'+
                '<td style="text-align: right"><b>Дата рождения</b></td>'+
                '<td>{tplData.BIRTH_DT}</td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right"><b>КПП</b></td>'+
                '<td>{tplData.KPP_NB}</td>'+
                '<td style="text-align: right"><b>Место рождения</b></td>'+
                '<td>{tplData.BIRTHPLACE_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right"><b>ОКВЭД</b></td>'+
                '<td>{tplData.OKVED_NB}</td>'+
                '<td style="text-align: right"><b>Гражданство/Страна регистрации</b></td>'+
                '<td>{tplData.CITIZENSHIP_CD}</td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right"><b>ОКПО</b></td>'+
                '<td>{tplData.OKPO_NB}</td>'+
                '<td style="text-align: right"><b>Дата регистрации</b></td>'+
                '<td>{tplData.REG_DT}</td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right"><b>ФИО ИП</b></td>'+
                '<td>{tplData.CUST_FL_NM}</td>'+
                '<td style="text-align: right"><b>Регистрационный номер</b></td>'+
                '<td>{tplData.OGRN_NB}</td>'+
                '</tr>'+
                '<tr style="display: {isPayerInfoExpanded}">'+
                '<td style="text-align: right"><b></b></td>'+
                '<td></td>'+
                '<td style="text-align: right"><b>Регистрирующий орган</b></td>'+
                '<td>{tplData.REG_ORG_NM}</td>'+
                '</tr>'+
                '</table>'+
                '<br>'+
                '<table class="registration-info">'+
                '<tr>'+
                '<th colspan="4" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Адрес регистрации</span> <div style="margin-left: 240px;" class="arrow-ico {regInfoExpandedClass}" tabindex="0">-</div></th>'+
                '</tr>'+
                '<tr>'+
                '<td style="padding: 0;" colspan="4"><hr></td>'+
                '</tr>'+
                '<tr style="display: {isRegInfoExpanded}">'+
                '<td style="text-align: right; width: 176px;"><b>Страна</b></td>'+
                '<td>{tplData.ADR_COUNTRY_CD}</td>'+
                '<td style="text-align: right"><b>ОКАТО</b></td>'+
                '<td style="width: 45%">{tplData.ADR_OKATO_CD}</td>'+
                '</tr>'+
                '<tr style="display: {isRegInfoExpanded}">'+
                '<td style="text-align: right"><b>Регион</b></td>'+
                '<td>{tplData.ADR_STATE_NM}</td>'+
                '<td style="text-align: right"><b>Улица</b></td>'+
                '<td>{tplData.ADR_STREET_NAME_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isRegInfoExpanded}">'+
                '<td style="text-align: right"><b>Район</b></td>'+
                '<td>{tplData.ADR_DISTRICT_NM}</td>'+
                '<td style="text-align: right"><b>Дом</b></td>'+
                '<td>{tplData.ADR_HOUSE_NUMBER_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isRegInfoExpanded}">'+
                '<td style="text-align: right"><b>Город</b></td>'+
                '<td>{tplData.ADR_CITY_NM}</td>'+
                '<td style="text-align: right"><b>Строение/Корпус</b></td>'+
                '<td>{tplData.ADR_BUILDING_NUMBER_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isRegInfoExpanded}">'+
                '<td style="text-align: right"><b>Населенный пункт</b></td>'+
                '<td>{tplData.ADR_SETTLM_NM}</td>'+
                '<td style="text-align: right"><b>Офис/Квартира</b></td>'+
                '<td>{tplData.ADR_FLAT_NUMBER_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isRegInfoExpanded}">'+
                '<td style="text-align: right"><b>Адрес текстом</b></td>'+
                '<td>{tplData.ADR_UNSTRUCT_TX}</td>'+
                '<td style="text-align: right"><b></b></td>'+
                '<td></td>'+
                '</tr>'+
                '</table>'+
                '<br>'+
                '<table class="location-info">'+
                '<tr>'+
                '<th colspan="4" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Адрес местонахождения</span> <div style="margin-left: 240px;" class="arrow-ico {locInfoExpandedClass}" tabindex="0">-</div></th>'+
                '</tr>'+
                '<tr>'+
                '<td style="padding: 0;" colspan="4"><hr></td>'+
                '</tr>'+
                '<tr style="display: {isLocInfoExpanded}">'+
                '<td style="text-align: right; width: 176px;"><b>Страна</b></td>'+
                '<td>{tplData.ADH_COUNTRY_CD}</td>'+
                '<td style="text-align: right"><b>ОКАТО</b></td>'+
                '<td style="width: 45%">{tplData.ADH_OKATO_CD}</td>'+
                '</tr>'+
                '<tr style="display: {isLocInfoExpanded}">'+
                '<td style="text-align: right"><b>Регион</b></td>'+
                '<td>{tplData.ADH_STATE_NM}</td>'+
                '<td style="text-align: right"><b>Улица</b></td>'+
                '<td>{tplData.ADH_STREET_NAME_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isLocInfoExpanded}">'+
                '<td style="text-align: right"><b>Район</b></td>'+
                '<td>{tplData.ADH_DISTRICT_NM}</td>'+
                '<td style="text-align: right"><b>Дом</b></td>'+
                '<td>{tplData.ADH_HOUSE_NUMBER_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isLocInfoExpanded}">'+
                '<td style="text-align: right"><b>Город</b></td>'+
                '<td>{tplData.ADH_CITY_NM}</td>'+
                '<td style="text-align: right"><b>Строение/Корпус</b></td>'+
                '<td>{tplData.ADH_BUILDING_NUMBER_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isLocInfoExpanded}">'+
                '<td style="text-align: right"><b>Населенный пункт</b></td>'+
                '<td>{tplData.ADH_SETTLM_NM}</td>'+
                '<td style="text-align: right"><b>Офис/Квартира</b></td>'+
                '<td>{tplData.ADH_FLAT_NUMBER_TX}</td>'+
                '</tr>'+
                '<tr style="display: {isLocInfoExpanded}">'+
                '<td style="text-align: right"><b>Адрес текстом</b></td>'+
                '<td>{tplData.ADH_UNSTRUCT_TX}</td>'+
                '<td style="text-align: right"><b></b></td>'+
                '<td></td>'+
                '</tr>'+
                '</table>'+
                '<br>'+
                '<table class="document-info">'+
                '<tr>'+
                '<th colspan="4" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Удостоверяющий документ</span> <div style="margin-left: 240px;" class="arrow-ico {docInfoExpandedClass}" tabindex="0">-</div></th>'+
                '</tr>'+
                '<tr>'+
                '<td style="padding: 0;" colspan="4"><hr></td>'+
                '</tr>'+
                '<tr style="display: {isDocInfoExpanded}">'+
                '<td style="text-align: right; width: 176px;"><b>Тип документа</b></td>'+
                '<td>{tplData.DOC_TYPE_CD}</td>'+
                '<td style="text-align: right"><b></b></td>'+
                '<td style="width: 45%"></td>'+
                '</tr>'+
                '<tr style="display: {isDocInfoExpanded}">'+
                '<td style="text-align: right"><b>Серия</b></td>'+
                '<td>{tplData.DOCUMENT_SERIES}</td>'+
                '<td style="text-align: right"><b>Номер</b></td>'+
                '<td>{tplData.DOCUMENT_NO}</td>'+
                '</tr>'+
                '<tr style="display: {isDocInfoExpanded}">'+
                '<td style="text-align: right"><b>Орган, выдавший документ</b></td>'+
                '<td>{tplData.DOCUMENT_ISSUED_DPT_TX}</td>'+
                '<td style="text-align: right"><b>Дата выдачи</b></td>'+
                '<td>{tplData.DOCUMENT_ISSUED_DT}</td>'+
                '</tr>'+
                '<tr style="display: {isDocInfoExpanded}">'+
                '<td style="text-align: right"><b>Код подразделения</b></td>'+
                '<td>{tplData.DOCUMENT_ISSUED_DPT_CD}</td>'+
                '<td style="text-align: right"><b>Срок действия</b></td>'+
                '<td>{tplData.DOCUMENT_END_DT}</td>'+
                '</tr>'+
                '</table>'
            }
        },
        {
            xtype: 'container',
            listeners: {
                el: {
                    click: 'tableToggle'
                }
            },
            bind: {
                html: '<br><table class="addresses-info">' +
                '<tr>' +
                '<th colspan="2" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Все адреса ({allAddressesCount})</span> <div style="margin-left: 240px;" class="arrow-ico {addrInfoExpandedClass}" tabindex="0">-</div></th>' +
                '</tr>' +
                '<tr>'+
                '<td style="padding: 0;" colspan="2"><hr></td>'+
                '</tr>'+
                '</table>'
            }
        },
        {
            xtype: 'gridpanel',
            hidden: true,
            bind: {
                store: '{addressListStore}',
                hidden: '{!isAddrInfoExpanded}'
            },
            columns: {
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        text: 'Тип адреса',
                        dataIndex: 'USAGE_CD'
                    },
                    {
                        text: 'Страна',
                        dataIndex: 'COUNTRY_CD'
                    },
                    {
                        text: 'Регион',
                        dataIndex: 'STATE_NM'
                    },
                    {
                        text: 'Район',
                        dataIndex: 'DISTRICT_NM'
                    },
                    {
                        text: 'Город',
                        dataIndex: 'CITY_NM'
                    },
                    {
                        text: 'Населенный пункт',
                        dataIndex: 'SETTLM_NM'
                    },
                    {
                        text: 'Улица',
                        dataIndex: 'STREET_NAME_TX'
                    },
                    {
                        text: 'Дом',
                        dataIndex: 'HOUSE_NUMBER_TX'
                    },
                    {
                        text: 'Строение/Корпус',
                        dataIndex: 'BUILDING_NUMBER_TX'
                    },
                    {
                        text: 'Квартира/Офис',
                        dataIndex: 'FLAT_NUMBER_TX'
                    },
                    {
                        text: 'Статус',
                        dataIndex: 'STATUS_TX'
                    },
                    {
                        text: 'Начало действия',
                        dataIndex: 'START_DT'
                    },
                    {
                        text: 'Окончание действия',
                        dataIndex: 'END_DT'
                    },
                    {
                        text: 'Система источник',
                        dataIndex: 'SRC_SYS_CD'
                    },
                    {
                        text: 'Дата последнего изменения',
                        dataIndex: 'SRC_CHANGE_DT'
                    },
                    {
                        text: 'Пользователь, внесший изменения',
                        dataIndex: 'SRC_USER_NM'
                    },
                    {
                        text: '-',
                        dataIndex: 'CUST_ADDR_SEQ_ID'
                    }
                ]
            }
        },
        {
            xtype: 'container',
            listeners: {
                el: {
                    click: 'tableToggle'
                }
            },
            bind: {
                html: '<br><table class="id-documents-info">' +
                '<tr>' +
                '<th colspan="2" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Все удостоверяющие документы ({allIdDocsCount})</span> <div style="margin-left: 240px;" class="arrow-ico {idDocsInfoExpandedClass}" tabindex="0">-</div></th>' +
                '</tr>' +
                '<tr>'+
                '<td style="padding: 0;" colspan="2"><hr></td>'+
                '</tr>'+
                '</table>'
            }
        },
        {
            xtype: 'gridpanel',
            hidden: true,
            bind: {
                store: '{idDocsListStore}',
                hidden: '{!isIdDocsInfoExpanded}'
            },
            columns: {
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        text: 'Тип документа',
                        dataIndex: 'DOC_TYPE_CD'
                    },
                    {
                        text: 'Статус',
                        dataIndex: 'STATUS_TX'
                    },
                    {
                        text: 'Серия',
                        dataIndex: 'SERIES_ID'
                    },
                    {
                        text: 'Номер',
                        dataIndex: 'NO_ID'
                    },
                    {
                        text: 'Дата выдачи',
                        dataIndex: 'ISSUE_DT'
                    },
                    {
                        text: 'Дата окончания',
                        dataIndex: 'END_DT'
                    },
                    {
                        text: 'Кем выдан',
                        dataIndex: 'ISSUED_BY_TX'
                    },
                    {
                        text: 'Код подразделения',
                        dataIndex: 'ISSUE_DPT_CD'
                    },
                    {
                        text: 'Система источник',
                        dataIndex: 'SRC_SYS_CD'
                    },
                    {
                        text: 'Дата последнего изменения',
                        dataIndex: 'SRC_CHANGE_DT'
                    },
                    {
                        text: 'Пользователь, внесший изменения',
                        dataIndex: 'SRC_USER_NM'
                    },
                    {
                        text: '-',
                        dataIndex: 'CUST_DOC_SEQ_ID'
                    }
                ]
            }
        },
        //-------------------------------------------------------
        {
            xtype: 'container',
            listeners: {
                el: {
                    click: 'tableToggle'
                }
            },
            bind: {
                html: '<br><table class="accounts-info">' +
                '<tr>' +
                '<th style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Счета ({allAccounts})</span> <div  style="margin-left: 240px;" class="arrow-ico {accountsExpandedClass}"  tabindex="0">-</div></th>' +
                '</tr>' +
                '<tr>'+
                '<td style="padding: 0;" colspan="2"><hr></td>'+
                '</tr>'+
                '</table>'
            }
        },
        {
            xtype: 'gridpanel',
            hidden: true,
            bind: {
                store: '{accountsListStore}',
                hidden: '{!isAccountsExpanded}'
            },
            columns: {
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        text: 'Номер счета',
                        dataIndex: 'ACCT_NB'
                        // renderer: function (value) {
                        //     return value.replace(/(\d{4}(?!\s))/g, '$1 ');
                        //     // return value.replace(/\b(\d{4})(\d{4})(\d{4})(\d{4})\b/, '$1 $2 $3 $4');
                        // }
                    },
                    {
                        text: 'Валюта',
                        dataIndex: 'ACCT_CORRENCY_CD'
                    },
                    {
                        text: 'Статус',
                        dataIndex: 'ACCT_STATUS_CD'
                    },
                    {
                        text: 'Дата открытия счета',
                        dataIndex: 'ACCT_OPEN_DT',
                        renderer: function (value) {
                            if (value) {
                                var a = (value) ? value.toString().split(' ') : null,
                                    aa = (a) ? a[0] : null,
                                    d = (aa) ? aa.split('-') : null,
                                    delem1, delem2, dlen1, dlen2, delem3,
                                    dd, mm;
                                delem1 = (d) ? d[2] : null;
                                dlen1 = (delem1) ? delem1.length : null;
                                delem2 = (d) ? d[1] : null;
                                dlen2 = (delem2) ? delem2.length : null;
                                delem3 = (d) ? d[0] : null;

                                dd = (dlen1 === 1) ? ('0' + delem1) : delem1;
                                mm = (dlen2 === 1) ? ('0' + delem2) : delem2;

                                return (dd + '.' + mm + '.' + delem3);
                            }
                        }
                    },
                    {
                        text: 'Дата закрытия счета',
                        dataIndex: 'ACCT_CLOSE_DT',
                        renderer: function (value) {
                            if (value) {
                                var a = (value) ? value.toString().split(' ') : null,
                                    aa = (a) ? a[0] : null,
                                    d = (aa) ? aa.split('-') : null,
                                    delem1, delem2, dlen1, dlen2, delem3,
                                    dd, mm;
                                delem1 = (d) ? d[2] : null;
                                dlen1 = (delem1) ? delem1.length : null;
                                delem2 = (d) ? d[1] : null;
                                dlen2 = (delem2) ? delem2.length : null;
                                delem3 = (d) ? d[0] : null;

                                dd = (dlen1 === 1) ? ('0' + delem1) : delem1;
                                mm = (dlen2 === 1) ? ('0' + delem2) : delem2;

                                return (dd + '.' + mm + '.' + delem3);
                            }
                        }
                    },
                    {
                        text: 'Тербанк',
                        dataIndex: 'ACCT_TB_NM'
                    },
                    {
                        text: 'Отделение',
                        dataIndex: 'ACCT_OSB_NM'
                    },
                    {
                        text: 'Дата первой операции',
                        dataIndex: 'ACCT_FIRST_DT',
                        renderer: function (value) {
                            if (value) {
                                var a = (value) ? value.toString().split(' ') : null,
                                    aa = (a) ? a[0] : null,
                                    d = (aa) ? aa.split('-') : null,
                                    delem1, delem2, dlen1, dlen2, delem3,
                                    dd, mm;
                                delem1 = (d) ? d[2] : null;
                                dlen1 = (delem1) ? delem1.length : null;
                                delem2 = (d) ? d[1] : null;
                                dlen2 = (delem2) ? delem2.length : null;
                                delem3 = (d) ? d[0] : null;

                                dd = (dlen1 === 1) ? ('0' + delem1) : delem1;
                                mm = (dlen2 === 1) ? ('0' + delem2) : delem2;

                                return (dd + '.' + mm + '.' + delem3);
                            }
                        }
                    },
                    {
                        text: 'Дата первого зачисления ГОЗ',
                        dataIndex: 'ACCT_GOZ_FIRST_DT',
                        renderer: function (value) {
                            if (value) {
                                var a = (value) ? value.toString().split(' ') : null,
                                    aa = (a) ? a[0] : null,
                                    d = (aa) ? aa.split('-') : null,
                                    delem1, delem2, dlen1, dlen2, delem3,
                                    dd, mm;
                                delem1 = (d) ? d[2] : null;
                                dlen1 = (delem1) ? delem1.length : null;
                                delem2 = (d) ? d[1] : null;
                                dlen2 = (delem2) ? delem2.length : null;
                                delem3 = (d) ? d[0] : null;

                                dd = (dlen1 === 1) ? ('0' + delem1) : delem1;
                                mm = (dlen2 === 1) ? ('0' + delem2) : delem2;

                                return (dd + '.' + mm + '.' + delem3);
                            }
                        }
                    },
                    {
                        text: 'Дата последней операции',
                        dataIndex: '',
                        renderer: function (value) {
                            if (value) {
                                var a = (value) ? value.toString().split(' ') : null,
                                    aa = (a) ? a[0] : null,
                                    d = (aa) ? aa.split('-') : null,
                                    delem1, delem2, dlen1, dlen2, delem3,
                                    dd, mm;
                                delem1 = (d) ? d[2] : null;
                                dlen1 = (delem1) ? delem1.length : null;
                                delem2 = (d) ? d[1] : null;
                                dlen2 = (delem2) ? delem2.length : null;
                                delem3 = (d) ? d[0] : null;

                                dd = (dlen1 === 1) ? ('0' + delem1) : delem1;
                                mm = (dlen2 === 1) ? ('0' + delem2) : delem2;

                                return (dd + '.' + mm + '.' + delem3);
                            }
                        }
                    },
                    {
                        text: 'Система источник',
                        dataIndex: 'SRC_SYS_CD'
                    }
                ]
            }
        },
        //-------------------------------------------------------
        {
            xtype: 'container',
            listeners: {
                el: {
                    click: 'tableToggle'
                }
            },
            bind: {
                html: '<br><table class="contacts-info">' +
                '<tr>' +
                '<th colspan="2" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Контакты ({allContactsCount})</span> <div style="margin-left: 240px;" class="arrow-ico {contactsInfoExpandedClass}" tabindex="0">-</div></th>' +
                '</tr>' +
                '<tr>'+
                '<td style="padding: 0;" colspan="2"><hr></td>'+
                '</tr>'+
                '</table>'
            }
        },
        {
            xtype: 'gridpanel',
            hidden: true,
            bind: {
                store: '{contactsListStore}',
                hidden: '{!isContactsInfoExpanded}'
            },
            columns: {
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        text: 'Тип контакта',
                        dataIndex: 'USAGE_CD'
                    },
                    {
                        text: 'Статус',
                        dataIndex: 'STATUS_TX'
                    },
                    {
                        text: 'Номер',
                        dataIndex: 'PHON_NB'
                    },
                    {
                        text: 'Дата начала действия',
                        dataIndex: 'START_DT'
                    },
                    {
                        text: 'Дата окончания действия',
                        dataIndex: 'END_DT'
                    },
                    {
                        text: 'Система источник',
                        dataIndex: 'SRC_CD'
                    },
                    {
                        text: 'Дата последнего изменения',
                        dataIndex: 'SRC_CHANGE_DT'
                    },
                    {
                        text: 'Пользователь, внесший изменения',
                        dataIndex: 'SRC_USER_NM'
                    },
                    {
                        text: '-',
                        dataIndex: 'CUST_PHON_SEQ_ID'
                    }
                ]
            }
        },
        {
            xtype: 'container',
            listeners: {
                el: {
                    click: 'tableToggle'
                }
            },
            bind: {
                html: '<br><table class="emails-info">' +
                '<tr>' +
                '<th colspan="2" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Электронные адреса ({allEmailsCount})</span> <div style="margin-left: 240px;" class="arrow-ico {emailsInfoExpandedClass}" tabindex="0">-</div></th>' +
                '</tr>' +
                '<tr>'+
                '<td style="padding: 0;" colspan="2"><hr></td>'+
                '</tr>'+
                '</table>'
            }
        },
        {
            xtype: 'gridpanel',
            hidden: true,
            bind: {
                store: '{emailsListStore}',
                hidden: '{!isEmailsInfoExpanded}'
            },
            columns: {
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        text: 'Статус',
                        dataIndex: 'STATUS_TX'
                    },
                    {
                        text: 'Email',
                        dataIndex: 'EMAIL_ADDR_TX'
                    },
                    {
                        text: 'Дата начала действия',
                        dataIndex: 'START_DT'
                    },
                    {
                        text: 'Дата окончания действия',
                        dataIndex: 'END_DT'
                    },
                    {
                        text: 'Система источник',
                        dataIndex: 'SRC_SYS_CD'
                    },
                    {
                        text: 'Дата последнего изменения',
                        dataIndex: 'SRC_CHANGE_DT'
                    },
                    {
                        text: 'Пользователь, внесший изменения',
                        dataIndex: 'SRC_USER_NM'
                    },
                    {
                        text: '-',
                        dataIndex: 'CUST_EMAIL_SEQ_ID'
                    }
                ]
            }
        },
        {
            xtype: 'container',
            listeners: {
                el: {
                    click: 'tableToggle'
                }
            },
            bind: {
                html: '<br><table class="links-info">' +
                '<tr>' +
                '<th colspan="2" style="text-align: left;padding: 0 5px;width: 250px;"><span style="text-decoration: underline">Связи ({allLinksCount})</span> <div style="margin-left: 240px;" class="arrow-ico {linksInfoExpandedClass}" tabindex="0">-</div></th>' +
                '</tr>' +
                '<tr>'+
                '<td style="padding: 0;" colspan="2"><hr></td>'+
                '</tr>'+
                '</table>'
            }
        },
        {
            xtype: 'gridpanel',
            hidden: true,
            bind: {
                store: '{linksListStore}',
                hidden: '{!isLinksInfoExpanded}'
            },
            columns: {
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        text: 'Вид связи',
                        dataIndex: 'RLSHP_TP_NM'
                    },
                    {
                        text: 'Должность',
                        dataIndex: 'JOB_TITL_NM'
                    },
                    {
                        text: 'Наименование лица',
                        dataIndex: 'FULL_NM'
                    },
                    {
                        text: 'Дата рождения/регистрации',
                        dataIndex: 'BIRTH_DT'
                    },
                    {
                        text: 'Основание связи',
                        dataIndex: 'RLSHP_REASON_TX'
                    },
                    {
                        text: 'Статус',
                        dataIndex: 'STATUS_TX'
                    },
                    {
                        text: 'Начало действия',
                        dataIndex: 'START_DT'
                    },
                    {
                        text: 'Окончание действия',
                        dataIndex: 'END_DT'
                    },
                    {
                        text: 'Причина прекращения',
                        dataIndex: 'END_REASON_TX'
                    },
                    {
                        text: 'Система источник',
                        dataIndex: 'SRC_CD'
                    },
                    {
                        text: 'Дата последнего изменения',
                        dataIndex: 'SRC_CHANGE_DT'
                    },
                    {
                        text: 'Пользователь, внесший изменения',
                        dataIndex: 'SRC_USER_NM'
                    },
                    {
                        text: '-',
                        dataIndex: 'CUST_CUST_SEQ_ID'
                    }
                ]
            }
        }
    ]
});