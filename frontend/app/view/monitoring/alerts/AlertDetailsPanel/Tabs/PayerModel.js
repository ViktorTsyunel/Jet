Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.Tabs.PayerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.payertabviewmodel',
    data: {
        title: 'Плательщик',
        tplData: {},
        allAddressesCount: 0,
        allIdDocsCount: 0,
        allContactsCount: 0,
        allAccounts: 0,
        accountsExpandedClass: 'closed',
        isAccountsExpanded: false,
        allEmailsCount: 0,
        allLinksCount: 0,
        isPayerInfoExpanded: '',
        isRegInfoExpanded: '',
        isLocInfoExpanded: '',
        isDocInfoExpanded: '',
        isAddrInfoExpanded: false,
        isIdDocsInfoExpanded: false,
        isContactsInfoExpanded: false,
        isEmailsInfoExpanded: false,
        isLinksInfoExpanded: false,
        payerInfoExpandedClass: 'opened',
        locInfoExpandedClass: 'opened',
        docInfoExpandedClass: 'opened',
        addrInfoExpandedClass: 'closed',
        idDocsInfoExpandedClass: 'closed',
        contactsInfoExpandedClass: 'closed',
        emailsInfoExpandedClass: 'closed',
        linksInfoExpandedClass: 'closed',
        regInfoExpandedClass: 'opened'
    },
    stores: {
        // Таблица все адреса
        addressListStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                load: 'addressListStoreReady'
            },
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustAddrList',
                    action: 'readByAlertId'
                }
            },
            sorters: [
                {
                    property: 'USAGE_CD',
                    direction: 'DESC'
                }
            ]
        },
        // Таблица все удостоверяющие документы
        idDocsListStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                load: 'idDocsListStoreReady'
            },
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustIdDocList',
                    action: 'readByAlertId'
                }
            }
        },
        // Таблица счетов
        accountsListStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                load: 'accountsListStoreReady'
            },
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustAcctList',
                    action: 'readByAlertId'
                }
            }
        },
        // Таблица контакты
        contactsListStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                load: 'contactsListStoreReady'
            },
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustPhonList',
                    action: 'readByAlertId'
                }
            }
        },
        // Таблица электронные адреса
        emailsListStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                load: 'emailsListStoreReady'
            },
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustEmailList',
                    action: 'readByAlertId'
                }
            }
        },
        // Таблица связи
        linksListStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                load: 'linksListStoreReady'
            },
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'CustCustList',
                    action: 'readByAlertId'
                }
            }
        }
    }
});