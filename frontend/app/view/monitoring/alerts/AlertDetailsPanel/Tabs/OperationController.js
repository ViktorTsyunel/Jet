Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.Tabs.OperationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertdetailsoperationtab',

    init: function(tab) {
        var self = this,
            vm = self.getViewModel(),
            params = tab.config.AjaxParams;

        // добавим loading обработку:
        if(!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = vm.getParent().getParent().getView();
        }

        // запрос для вкладки операция:
        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                if(!Ext.isEmpty(responseObj.data)) {
                    !tab.isDestroyed && vm.set('tplData', responseObj.data[0]);
                }
            }
        });
    },

    tableToggle: function (e) {
        var self = this,
            vm = self.getViewModel();

        // Сведения об операции
        if(e.target.parentNode.className.indexOf('main-info') !== -1 && e.target.className.indexOf('arrow-ico') !==-1)
        {
            if (vm.get('isMainInfoExpanded') !== 'none') {
                vm.set('isMainInfoExpanded', 'none');
                vm.set('mainInfoExpandedClass', 'closed');
            }
            else {
                vm.set('mainInfoExpandedClass', 'opened');
                vm.set('isMainInfoExpanded', 'table-row');
            }
        }
        // Дополнительная информация
        if(e.target.parentNode.className.indexOf('additional-info') !== -1 && e.target.className.indexOf('arrow-ico') !==-1)
        {
            if (vm.get('isAdditionalInfoExpanded') !== 'none') {
                vm.set('isAdditionalInfoExpanded', 'none');
                vm.set('addInfoExpandedClass', 'closed');
            }
            else {
                vm.set('addInfoExpandedClass', 'opened');
                vm.set('isAdditionalInfoExpanded', 'table-row');
            }
        }
    }
});
