Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.Tabs.Operation', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-alertdetailspanel-operationtab',
    controller: 'alertdetailsoperationtab',
    viewModel: {
        type: 'operationtabviewmodel'
    },
    padding: '5px',
    initComponent: function () {
        var me = this;
        this.listeners = {
            el: {
                click: {
                    fn: 'tableToggle',
                    scope: me.lookupController()
                }
            }
        };
        me.callParent(arguments);
    },
    bind: {
        title: '{title}',
        html: '<table class="operation-info">'+
        '<tr>'+
        '<th class="main-info" colspan="8" style="text-align: left;padding: 1 5px;min-width: 205px;"><span style="text-decoration: underline">Сведения об операции</span> <div class="arrow-ico {mainInfoExpandedClass}" tabindex="0">-</div></th>'+
        '</tr>'+
        '<tr>'+
        '<td style="padding: 0;" colspan="8"><hr></td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}">'+
        '<td style="text-align: right;min-width: 202px;"><b>Дата операции</b></td>'+
        '<td style="min-width: 138px;">{tplData.TRXN_DT}</td>'+
        '<td style="text-align: right;min-width: 195px;"><b>Номер платежного документа</b></td>'+
        '<td style="min-width: 132px;">{tplData.TRXN_DOC_ID}</td>'+
        '<td style="text-align: right;min-width: 157px;"><b>Дата платежного документа</b></td>'+
        '<td style="min-width: 175px;">{tplData.TRXN_DOC_DT}</td>'+
        '<td style="text-align: right;min-width: 90px;"><b>Категория операции</b></td>'+
        '<td>{tplData.OP_CAT_CD}</td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}">'+
        '<td style="text-align: right;"><b>Сумма в валюте</b></td>'+
        '<td>{tplData.TRXN_CRNCY_AM:number("0,000.00")}</td>'+
        '<td style="text-align: right;"><b>Сумма в рублях</b></td>'+
        '<td>{tplData.TRXN_BASE_AM:number("0,000.00")}</td>'+
        '<td style="text-align: right;"><b>Валюта</b></td>'+
        '<td>{tplData.TRXN_CRNCY_CD}</td>'+
        '<td style="text-align: right;"><b>Курс</b></td>'+
        '<td>{colorTRXN_CRNCY_RATE_AM}</td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}">'+
        '<td style="text-align: right;"><b>Назначение платежа</b></td>'+
        '<td colspan="7">{colorTRXN_DESC}</td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td><b style="color: blue;">Плательщик</b></td>'+
        '<td colspan="7">{colorORIG_NM}</td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>ИНН</b></td>'+
        '<td>{tplData.ORIG_INN_NB}</td>'+
        '<td style="text-align: right"><b>КПП</b></td>'+
        '<td>{tplData.ORIG_KPP_NB}</td>'+
        '<td style="text-align: right"><b>Расчетный счет</b></td>'+
        '<td>{tplData.ORIG_ACCT_NB}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>Банк плательщика</b></td>'+
        '<td>{tplData.SEND_INSTN_NM}</td>'+
        '<td style="text-align: right"><b>Страна</b></td>'+
        '<td>{tplData.SEND_INSTN_CNTRY_CD}</td>'+
        '<td style="text-align: right"><b>Филиал КГРКО</b></td>'+
        // '<td></td>'+
       '<td>{tplData.SEND_KGRKO_ID} {tplData.SEND_KGRKO_NM}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>БИК/SWIFT</b></td>'+
        '<td>{tplData.SEND_INSTN_ID}</td>'+
        '<td style="text-align: right"><b>Корр. счет</b></td>'+
        '<td>{tplData.SEND_INSTN_ACCT_ID}</td>'+
        '<td style="text-align: right"><b>Дебет</b></td>'+
        '<td>{tplData.DEBIT_CD}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr>'+
        '<td colspan="8"></td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td><b style="color: blue;">Получатель</b></td>'+
        '<td colspan="7">{colorBENEF_NM}</td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>ИНН</b></td>'+
        '<td>{tplData.BENEF_INN_NB}</td>'+
        '<td style="text-align: right"><b>КПП</b></td>'+
        '<td>{tplData.BENEF_KPP_NB}</td>'+
        '<td style="text-align: right"><b>Расчетный счет</b></td>'+
        '<td>{tplData.BENEF_ACCT_NB}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>Банк получателя</b></td>'+
        '<td>{tplData.RCV_INSTN_NM}</td>'+
        '<td style="text-align: right"><b>Страна</b></td>'+
        '<td>{tplData.RCV_INSTN_CNTRY_CD}</td>'+
        '<td style="text-align: right"><b>Филиал КГРКО</b></td>'+
        // '<td></td>'+
        '<td>{tplData.RCV_KGRKO_ID} {tplData.RCV_KGRKO_NM}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr style="display: {isMainInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>БИК/SWIFT</b></td>'+
        '<td>{tplData.RCV_INSTN_ID}</td>'+
        '<td style="text-align: right"><b>Корр. счет</b></td>'+
        '<td>{tplData.RCV_INSTN_ACCT_ID}</td>'+
        '<td style="text-align: right"><b>Кредит</b></td>'+
        '<td>{tplData.CREDIT_CD}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr>'+
        '<td colspan="8">'+
        '</td>'+
        '</tr>'+
        '<tr>'+
        '<th class="additional-info" colspan="8" style="text-align: left;padding: 1 5px;min-width: 205px;"><span style="text-decoration: underline">Дополнительная информация</span> <div class="arrow-ico {addInfoExpandedClass}" tabindex="0">-</div></th>'+
        '</tr>'+
        '<tr>'+
        '<td style="padding: 0;" colspan="8"><hr></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}">'+
        '<td style="text-align: right"><b>Вид операции (ЦАС НСИ)</b></td>'+
        '<td colspan="7">{tplData.NSI_OP_ID}</td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}">'+
        '<td style="text-align: right"><b>Валюта конверсии</b></td>'+
        '<td>{tplData.TRXN_CONV_CRNCY_CD}</td>'+
        '<td style="text-align: right"><b>Вид операции ГОЗ (списание)</b></td>'+
        '<td>{tplData.ORIG_GOZ_OP_CD}</td>'+
        '<td style="text-align: right;"><b>Вид операции ГОЗ (зачисление)</b></td>'+
        '<td><span style="white-space: nowrap;">{tplData.BENEF_GOZ_OP_CD}</span></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}">'+
        '<td style="text-align: right"><b>Сумма конверсии</b></td>'+
        '<td>{tplData.TRXN_CONV_AM:number("0,000.00")}</td>'+
        '<td style="text-align: right;"><b>Сумма конверсии в рублях</b></td>'+
        '<td><span style="white-space: nowrap;">{tplData.TRXN_CONV_BASE_AM:number("0,000.00")}</span></td>'+
        '<td style="text-align: right;"><b>Кассовые символы</b></td>'+
        '<td>{tplData.TRXN_CASH_SYMBOLS_TX}</td>'+
        '<td style="text-align: right;"><b></b></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}" class="blue">'+
        '<td colspan="8"><b style="color: blue;">Банковская карта</b></td>'+
        //'<td colspan="7"></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>Номер</b></td>'+
        '<td>{tplData.BANK_CARD_ID_NB}</td>'+
        '<td style="text-align: right"><b>Дата и время операции</b></td>'+
        '<td>{tplData.BANK_CARD_TRXN_DT}</td>'+
        '<td style="text-align: right"><b>Терминал</b></td>'+
        '<td>{tplData.ATM_ID}</td>'+
        '<td style="text-align: right"><b>Точка обслуживания</b></td>'+
        '<td>{tplData.MERCHANT_NO}</td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}">'+
        '<td colspan="8"></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}" class="blue">'+
        '<td colspan="8"><b style="color: blue;">Банк, зарегистрировавший операцию</b></td>'+
        //'<td colspan="7"></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>Номер ТБ</b></td>'+
        '<td>{tplData.TB_ID}</td>'+
        '<td style="text-align: right"><b>Номер ОСБ</b></td>'+
        '<td>{tplData.OSB_ID}</td>'+
        '<td style="text-align: right"><b>Операция совершена в</b></td>'+
        '<td>{tplData.TRXN_ORG_DESC}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}" class="blue">'+
        '<td style="text-align: right"><b>Наименование ТБ</b></td>'+
        '<td>{tplData.TB_NM}</td>'+
        '<td style="text-align: right"><b>Наименование ОСБ</b></td>'+
        '<td>{tplData.OSB_NM}</td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '<td style="text-align: right"></td>'+
        '<td></td>'+
        '</tr>'+
        '<tr>'+
        '<td colspan="8"></td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}" class="blue">'+
        '<td><b style="color: blue;">Система источник</b></td>'+
        '<td colspan="7">{isCANCELED_FL}</td>'+
        '</tr>'+
        '<tr style="display: {isAdditionalInfoExpanded}" class="blue">'+
        '<td style="text-align: right;min-width: 210px;"><b>Наименование</b></td>'+
        '<td style="min-width: 138px;">{tplData.SRC_SYS_CD}</td>'+
        '<td style="text-align: right;min-width: 195px;"><b>Дата последнего изменения</b></td>'+
        '<td style="min-width: 132px;">{tplData.SRC_CHANGE_DT}</td>'+
        '<td style="text-align: right;min-width: 157px;"><b>Пользователь, внесший изменения</b></td>'+
        '<td style="min-width: 175px;">{tplData.SRC_USER_NM}</td>'+
        '<td style="text-align: right;min-width: 90px;"><b>Код операции</b></td>'+
        '<td>{tplData.SRC_CD}</td>'+
        '</tr>'+
        '</table>'
    }
});