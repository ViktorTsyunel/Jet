Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.Tabs.OperationModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.operationtabviewmodel',
    data: {
        title: 'Операция',
        tplData: {},
        isMainInfoExpanded: '',
        isAdditionalInfoExpanded: '',
        mainInfoExpandedClass: 'opened',
        addInfoExpandedClass: 'opened'
    },
    formulas: {
        colorTRXN_DESC: {
            bind: {
                value: '{tplData}'
            },
            get: function (data){
                var text = data.value.TRXN_DESC,
                    lexeme = data.value.TRXN_DESC_LEX;

                return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
            }
        },
        colorTRXN_CRNCY_RATE_AM: {
            bind: {
                value: '{tplData}'
            },
            get: function (data){
                var curr = data.value.TRXN_CRNCY_RATE_AM,
                    redFl = data.value.TRXN_CRNCY_OLD_RATE_FL;

                return redFl==='Y' ? '<span class="lexeme red">'+curr+'</span>' : curr;
            }
        },
        colorORIG_NM: {
            bind: {
                value: '{tplData}'
            },
            get: function (data){
                var text = data.value.ORIG_NM,
                    lexeme = data.value.ORIG_LEX;

                return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
            }
        },
        colorBENEF_NM: {
            bind: {
                value: '{tplData}'
            },
            get: function (data){
                var text = data.value.BENEF_NM,
                    lexeme = data.value.BENEF_LEX;

                return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
            }
        },
        isCANCELED_FL: {
            bind: {
                value: '{tplData}'
            },
            get: function (data){
                var isCanсeled = data.value.CANCELED_FL;

                return isCanсeled==='Y' ? '<span class="lexeme red">Операция удалена</span>' : '';
            }
        }
    }
});