Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.Panel', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-alerts-alertdetailspanel',
    controller: 'alertdetailspanel',
    viewModel: {
        type: 'alertdetailspanelviewmodel'
    },
    bind: {
        title: '{title} №: {alertId}'
    },
    // reference: 'alertDetailsPanel',
    region: 'east',
    width: '100%',
    header: true,
    scrollable: true,
    split: false,
    cls: 'app-alert-detailspanel',
    bodyStyle: {
        padding: '0 20px 0 10px'
    },
    tools: [
        {
            type: 'close',
            tooltip: 'Закрыть (Alt + з)',
            tabIndex: 0,
            callback: 'closePanel'
        }
    ],
    initComponent: function () {
        // var parentCtrl = this.up('app-monitoring-alerts').getController();
        var parentCtrl;
        if (this.up('app-monitoring-alerts')) {
            parentCtrl = this.up('app-monitoring-alerts').getController();
        } else if (this.up('main-search-operations-form')) {
            parentCtrl = this.up('main-search-operations-form').getController();
        }

        this.tbar = [
            {
                xtype: 'buttongroup',
                flex: 1,
                layout: {
                    type: 'hbox'
                },
                defaults: {
                    margin: '1px 3px 1px 3px'
                },
                bind: {
                    hidden: '{SEARCH_OPERATION_VIEW}'
                },
                items: [
                    {
                        xtype: 'button',
                        reference: 'subjectToControlBtn',
                        text: 'Подлежит <span style="text-decoration:underline;">к</span>онтролю',
                        tooltip: 'Подлежит контролю (Alt + к)',
                        bind: {
                            disabled: '{hideButtons}'
                        },
                        margin: '1px 3px 1px 1px',
                        iconCls: 'icon-document-exclamation',
                        handler: 'subjectToControlAction',
                        scope: parentCtrl
                    },
                    {
                        xtype: 'button',
                        reference: 'cancelInvalidTokenBtn',
                        text: 'Отменить (неверная <span style="text-decoration:underline;">л</span>ексема)',
                        tooltip: 'Отменить (неверная лексема) (Alt + л)',
                        bind: {
                            disabled: '{hideButtons}'
                        },
                        iconCls: 'icon-document-minus',
                        handler: 'cancelInvalidTokenAction',
                        scope: parentCtrl
                    },
                    {
                        xtype: 'button',
                        reference: 'editCancelBtn',
                        text: '<span style="text-decoration:underline;">О</span>тменить',
                        tooltip: 'Отменить (Alt + о)',
                        bind: {
                            disabled: '{hideButtons}'
                        },
                        iconCls: 'icon-document-minus',
                        handler: 'editCancelAction',
                        scope: parentCtrl
                    },
                    {
                        xtype: 'button',
                        text: '<span style="text-decoration:underline;">Д</span>ействия',
                        tooltip: 'Действия (Alt + д)',
                        bind: {
                            disabled: '{hideButtons}'
                        },
                        iconCls: 'icon-gear',
                        handler: 'editAction',
                        scope: parentCtrl
                    },
                    {
                        xtype: 'button',
                        text: 'Пр<span style="text-decoration:underline;">и</span>крепить',
                        tooltip: 'Прикрепить (Alt + и)',
                        bind: {
                            disabled: '{hideButtons}'
                        },
                        iconCls: 'icon-document-plus',
                        handler: 'attachAction',
                        scope: parentCtrl
                    },
                    {
                        xtype: 'button',
                        text: '<span style="text-decoration:underline;">Э</span>кспорт',
                        tooltip: 'Экспорт (Alt + э)',
                        bind: {
                            disabled: '{hideButtons}'
                        },
                        iconCls: 'icon-export',
                        handler: 'exportAction',
                        scope: parentCtrl
                    },
                    {
                        xtype: 'tbfill'
                    },
                    {
                        xtype: 'button',
                        text: '',
                        tooltip: 'За<span style="text-decoration:underline;">м</span>етки (Alt + м)',
                        iconCls: 'icon-notebook-pencil',
                        handler: 'editNoteAction'
                    },
                    {
                        xtype: 'button',
                        text: '',
                        tooltip: '<span style="text-decoration:underline;">В</span>ложения (Alt + в)',
                        iconCls: 'icon-paper-clip',
                        handler: 'editAttachAction'
                    },
                    {
                        xtype: 'button',
                        text: '',
                        tooltip: 'И<span style="text-decoration:underline;">с</span>тория изменений (Alt + с)',
                        iconCls: 'icon-document-list',
                        handler: 'showHistoryAction'
                    }
                ]
            }
        ];

        this.callParent(arguments);
    },
    items: [
        {
            xtype: 'container',
            margin: '10px 0 0 0',
            layout: 'hbox',
            defaults: {
                border: 0
            },
            items: [
                {
                    xtype: 'button',
                    width: 20,
                    height: 20,
                    margin: '5px 10px 0 20px',
                    padding: 1,
                    text: '',
                    cls: 'app-details-table-tgl-btn closed',
                    tabIndex: 0,
                    enableToggle: true,
                    listeners: {
                        toggle: 'toggleDetailsTable'
                    },
                    bind: {
                        hidden: '{SEARCH_OPERATION_VIEW}'
                    }
                },
                {
                    xtype: 'panel',
                    flex: 1,
                    bind: {
                        data: '{detailsTable}',
                        hidden: '{SEARCH_OPERATION_VIEW}'
                    },
                    listeners: {
                        afterlayout: 'detailsPanelReadyHandler'
                    },
                    tpl: [
                        '<table class="details-table">',
                        '<tr>',
                        '<td><b>Номер операции</b></td>',
                        '<td>{REVIEW_ID}</td>',
                        '<td style="text-align: right"><b>Статус операции</b></td>',
                        '<td>{RV_STATUS_NM}</td>',
                        '<td><b>Основной код вида операции</b> <span style="color:red;font-weight: bold;font-size: 14px;">{OPOK_NB}</span></td>',
                        '<td style="text-align: right"><b>Доп. код вида операции</b> <span style="color:red;font-weight: bold;font-size: 14px;">{ADD_OPOKS_TX}</span></td>',
                        '<td style="text-align: right"><b>Ответственный</b> {RV_OWNER_ID}</td>',
                        '</tr>',
                        '<tr>',
                        '<td style="padding: 0;" colspan="7"><hr></td>',
                        '</tr>',
                        '<tpl if="isTableExpanded">',
                        '<tr>',
                        '<td><b>Дата создания</b></td>',
                        '<td>{RV_CREAT_TS:date("d.m.Y")}</td>',
                        '<td style="text-align: right"><table><tr><td><b>Причина выявления</b></td><td style="padding: 0;"><div id="showAlertReasonsBtn"></div></td></tr></table></td>',
                        '<td colspan="2">{EXPLANATION_TX}</td>',
                        '<td style="text-align: right"><b>Изменения в операции</b> {TRXN_CHANGE}</td>',
                        '<td style="text-align: right"><b>Изменения в выявлении</b> {OPOK_CHANGE}</td>',
                        '</tr>',
                        '<tr>',
                        '<td><b>Срок обработки</b></td>',
                        '<td>{RV_DUE_DT:date("d.m.Y")}</td>',
                        '<td style="text-align: right"><b>Комментарий</b></td>',
                        '<td colspan="2">{RV_NOTE_TX}</td>',
                        '<td style="text-align: right"><b>Несколько сообщений</b> {RV_SCRTY_DESC}</td>',
                        '<td></td>',
                        '</tr>',
                        '<tr>',
                        '<td><b>Филиал КГРКО сообщения</b></td>',
                        '<td colspan="2">{RV_KGRKO_ID} {RV_KGRKO_NM}</td>',
                        '<td></td>',
                        '</tr>',
                        '</tpl>',
                        '</table><br>'
                    ]
                }
            ]
        },
        {
            xtype: 'tabpanel',
            reference: 'detailsTabPanel',
            listeners: {
                boxready: 'detailsTabPanelReadyHandler'
            }
        }
    ]
});