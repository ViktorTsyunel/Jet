Ext.define('AML.view.monitoring.alerts.Forms.DocumentFieldsLogModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.alertdocumentfieldslogformvm',
	data: {
		title: 'История изменений',
        BATCH_DATE: null,
        MODIFIED_BY: null,
        changedFDs: true
	},
    formulas: {
    filters: {
        bind: {
            changedFDs: '{changedFDs}'
        },
        get: function (data) {
            var store = this.getStore('alertFieldsLog'),
                filters = [],
                changedFDs = data.changedFDs;

            if (changedFDs) {
                filters.push({
                    property: 'CHANGED_FLAG',
                    value: true
                });
            }
            store && store.clearFilter();
            return filters;
        }
    }
},
    stores: {
        // Справочник История изменений
        alertFieldsLog: {
            model: 'AML.model.AlertFieldsLog',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'ORDER_NB',
                    direction: 'ASC'
                }
            ],
            filters: '{filters}',
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    transform: {
                        fn: function (data) {
                            if (Ext.isArray(data.data)) {
                                Ext.Array.forEach(data.data, function (i) {
                                    if (i.CHANGED_FLAG !== undefined) {
                                        i.CHANGED_FLAG = ((''+i.CHANGED_FLAG) === 'Y');
                                    }
                                });
                            }
                            return data;
                        },
                        scope: this
                    },
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function (data, request) {
                            var actionMap = {
                                create: 'create',
                                update: 'update',
                                destroy: 'delete',
                                read: 'read'
                            };
                            request.setParam('action', actionMap[request.getAction()] || 'read');
                            Ext.Array.forEach(data, function (i) {
                                if (i.CHANGED_FLAG !== undefined) {
                                    i.CHANGED_FLAG = (i.CHANGED_FLAG ? 'Y' : 'N');
                                }
                            });
                            return data;
                        },
                        scope: this
                    },
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'OES321ChangeDetails',
                    action: 'read',
                    batch_date: '{BATCH_DATE}',
                    oes_321_id: '{OES_321_ID}'
                }
            }
        }
    }
});
