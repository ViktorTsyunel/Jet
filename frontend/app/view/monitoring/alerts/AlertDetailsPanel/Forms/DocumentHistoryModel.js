Ext.define('AML.view.monitoring.alerts.Forms.DocumentHistoryModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.alertdocumenthistoryformvm',
	data: {
		title: 'История изменений'
	},
    stores: {
        // Справочник История изменений
        alertHistory: {
            model: 'AML.model.AlertHistory',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            sorters: [
                {
                    property: 'START_DT',
                    direction: 'DESC'
                }
            ]
        }
    }
});
