Ext.define('AML.view.monitoring.alerts.Forms.DocumentFieldsLog', {
    extend: 'Ext.window.Window',
    xtype: 'app-monitoring-alerts-documentfieldslog-form',
    controller: 'alertdocumentfieldslogform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertdocumentfieldslogformvm'
    },
    bind: {
        title: 'Изменение от {BATCH_DATE}. Пользователь {MODIFIED_BY}'
    },
    width: "70%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype:'checkbox',
                    name: 'dsp_changedOnly',
                    labelSeparator: '',
                    boxLabel: 'Показывать только измененные поля',
                    fieldLabel: '',
                    checked: true,
                    bind: {
                        value: '{changedFDs}'
                    }
                },
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{alertFieldsLog}'
                    },
                    columns: [
                        {
                            text: '№ п/п',
                            tooltip: '№ п/п',
                            stateId: 'ORDER_NB',
                            dataIndex: 'ORDER_NB',
                            tdCls: 'v-align-middle',
                            minWidth: 60,
                            flex: 0.2
                        },
                        {
                            text: 'Вкладка',
                            tooltip: 'Вкладка',
                            stateId: 'BLOCK_TX',
                            dataIndex: 'BLOCK_TX',
                            tdCls: 'v-align-middle',
                            minWidth: 150,
                            flex: 0.2
                        },
                        {
                            text: 'Наименование',
                            tooltip: 'Наименование',
                            stateId: 'FIELD_NM',
                            dataIndex: 'FIELD_NM',
                            tdCls: 'v-align-middle',
                            minWidth: 200,
                            flex: 0.5
                        },
                        {
                            text: 'Код',
                            tooltip: 'Код',
                            stateId: 'FIELD_CD',
                            dataIndex: 'FIELD_CD',
                            tdCls: 'v-align-middle',
                            minWidth: 150,
                            flex: 0.2
                        },
                        {
                            text: 'Старое значение',
                            tooltip: 'Старое значение',
                            stateId: 'OLD_VALUE',
                            dataIndex: 'OLD_VALUE',
                            tdCls: 'v-align-middle',
                            minWidth: 225,
                            cellWrap: true,
                            flex: 0.5,
                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                if (record.data.CHANGED_FLAG === true) {
                                    metaData.tdStyle = 'background: #fb654b;font-weight:bold;';
                                }
                                return value;
                            }
                        },
                        {
                            text: 'Новое значение',
                            tooltip: 'Новое значение',
                            stateId: 'NEW_VALUE',
                            dataIndex: 'NEW_VALUE',
                            tdCls: 'v-align-middle',
                            minWidth: 225,
                            cellWrap: true,
                            flex: 0.5,
                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                if (record.data.CHANGED_FLAG === true) {
                                    metaData.tdStyle = 'background: #fb654b;font-weight:bold;';
                                }
                                return value;
                            }
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});