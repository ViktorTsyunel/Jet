Ext.define('AML.view.monitoring.alerts.Forms.DocumentHistoryLogController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.alertdocumenthistorylogform',

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            alertHistoryStore = vm.getStore('alertHistoryLog');

        // загрузка справочника История изменений:
        alertHistoryStore.load({
            scope: self,
            params: {
                oes_321_id: vm.get('oes321Id')
            }
        });
    },

    // открываем детали
    clickOpenHistoryLogDetailsOES: function (container, cmp, tr, rowIndex, e, record) {
        Ext.create({
            xtype: 'app-monitoring-alerts-documentfieldslog-form',
            viewModel: {
                xtype: 'app-monitoring-alerts-documentfieldslog-form',
                data: {
                    isOpenReadOnly: false,
                    OES_321_ID: record.get('OES_321_ID'),
                    BATCH_DATE: record.get('BATCH_DATE'),
                    MODIFIED_BY: record.get('MODIFIED_BY')
                }
            }
        }).show();
    }
});
