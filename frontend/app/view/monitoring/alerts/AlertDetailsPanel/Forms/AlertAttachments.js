Ext.define('AML.view.monitoring.alerts.Forms.AlertAttachments', {
    extend: 'Ext.window.Window',
    xtype: 'app-monitoring-alerts-attachments-form',
    controller: 'alertalertattachmentsform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertalertattachmentsformvm'
    },
    bind: {
        title: '{title}'
    },
    width: "80%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    reference: 'alertAttachmentsGrid',
                    maxHeight: 600,
                    scrollable: 'vertical',
                    bind: {
                        store: '{alertAttachments}'
                    },
                    listeners: {
                        selectionchange: 'selectedRowHandler'
                    },
                    plugins: {
                        ptype: 'rowediting',
                        errorSummary: false,
                        clicksToEdit: 2,
                        listeners: {
                            beforeedit: 'beforeEditRow',
                            edit: 'afterEditRow'
                        }
                    },
                    tbar: [
                        {
                            //itemId: 'create',
                            text: 'Добавить',
                            iconCls: 'icon-plus',
                            bind: {
                                disabled: '{hideButtons}'
                            },
                            handler: 'createAttachment'
                        },
                        {
                            reference: 'alertAttachmentsRemoveBtn',
                            text: 'Удалить',
                            bind: {
                                disabled: '{hideButtons}'
                            },
                            iconCls: 'icon-minus',
                            handler: 'removeAttachment'
                        }
                    ],
                    columns: [
                        {
                            xtype: 'templatecolumn',
                            tpl: ['<span class="f-ico-{FILE_EXT}"></span> {ATTCH_NM}'],
                            text: 'Наименование файла',
                            tooltip: 'Наименование файла',
                            stateId: 'ATTCH_NM',
                            dataIndex: 'ATTCH_NM',
                            tdCls: 'v-align-middle',
                            flex: 1,
                            editor: {
                                xtype: 'textfield'
                            }
                        },
                        {
                            xtype: 'templatecolumn',
                            tpl: ['<tpl if="FILE_NM"><span class="download-arrow-ico"></span> {FILE_NM}</tpl>'],
                            text: 'Файл',
                            tooltip: 'Файл',
                            stateId: 'FILE_NM',
                            dataIndex: 'FILE_NM',
                            tdCls: 'v-align-middle',
                            flex: 1,
                            listeners: {
                                click: 'clickHandler'
                            }
                        },
                        {
                            text: 'Размер',
                            tooltip: 'Размер',
                            stateId: 'FILE_SIZE',
                            dataIndex: 'FILE_SIZE',
                            tdCls: 'v-align-middle',
                            minWidth: 65,
                            flex: 1
                        },
                        {
                            text: 'Комментарий',
                            tooltip: 'Комментарий',
                            stateId: 'CMMNT',
                            dataIndex: 'CMMNT',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Кто создал',
                            tooltip: 'Кто создал',
                            stateId: 'CREATED_BY',
                            dataIndex: 'CREATED_BY',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            xtype: 'datecolumn',
                            format: 'd.m.Y H:i:s',
                            text: 'Когда создал',
                            tooltip: 'Когда создал',
                            stateId: 'CREATED_DATE',
                            dataIndex: 'CREATED_DATE',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Кто изменил',
                            tooltip: 'Кто изменил',
                            stateId: 'MODIFIED_BY',
                            dataIndex: 'MODIFIED_BY',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            xtype: 'datecolumn',
                            format: 'd.m.Y H:i:s',
                            text: 'Когда изменил',
                            tooltip: 'Когда изменил',
                            stateId: 'MODIFIED_DATE',
                            dataIndex: 'MODIFIED_DATE',
                            tdCls: 'v-align-middle',
                            flex: 1
                        }
                    ]
                }
            ],
            buttons: [
                /*{
                 itemId: 'saveForm',
                 text: 'Применить'
                 },*/
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});