Ext.define('AML.view.monitoring.alerts.Forms.AlertNarrativeController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.alertalertnarrativeform',

    init: function () {
        var self = this,
            vm = self.getViewModel();

        self.loadRichEditData({
            form: 'AlertNarrative',
            action: 'readByAlertId',
            alertId: parseInt(vm.get('alertId'), 10)
        }, self.setRichEditData());

        // if (self.getViewModel().get('Alerts_READONLY')) {
        //     vm.set('isRichEditorEnabled', false);
        //     self.lookupReference('richEditor').setDisabled(true);
        // }
    },

    loadRichEditData: function(AjaxParams, responseFn) {
        var self = this,
            vm = self.getViewModel();

        // включить индикатор обработки запроса:
        common.globalinit.loadingMaskCmp = self.getView();
        // получение текущего варианта заметок:
        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            params: Ext.JSON.encode(AjaxParams),
            timeout: common.globalinit.ajaxTimeOut,
            success: responseFn
        });
    },

    setRichEditData: function () {
        var self = this,
            vm = self.getViewModel(),
            saveFormBtn = self.lookupReference('saveFormBtn');

        return function (response) { // обертка для привязки контекста this
            var responseObj = Ext.JSON.decode(response.responseText);
            if (responseObj.data) {
                responseObj.data[0] && vm.set('richEditData', responseObj.data[0]);
                responseObj.data[0] && vm.set('richEditDataInitial', Ext.clone(responseObj.data[0]));
            }
            saveFormBtn.disable();
        };
    },

    richEditDataUpdated: function () {
        var self = this,
            vm = self.getViewModel();

        return function (response) { // обертка для привязки контекста this
            self.loadRichEditData({
                form: 'AlertNarrative',
                action: 'readByAlertId',
                alertId: parseInt(vm.get('alertId'), 10)
            }, self.setRichEditData());
        };
    },

    textAreaDblClickHandler: function(event, target) {
        var self = this,
            vm = self.getViewModel(),
            saveFormBtn = self.lookupReference('saveFormBtn');

        return function () { // обертка для привязки контекста this
            vm.set('isRichEditorEnabled', true);
            saveFormBtn.enable();
        };
    },

    richEditorBlurHandler: function() {
        var self = this,
            vm = self.getViewModel(),
            saveFormBtn = self.lookupReference('saveFormBtn');

        return function () { // обертка для привязки контекста this
            vm.set('isRichEditorEnabled', false);

            if(Ext.Object.equals(vm.get('richEditData'), vm.get('richEditDataInitial'))) {
                saveFormBtn.disable();
            }
        };
    },

    saveForm: function() {
        var self = this,
            vm = self.getViewModel();
            alertsGridVm = vm.getParent().getParent(),
            alertsGridCtrl = alertsGridVm && alertsGridVm.getView().getController();
        if(!Ext.Object.equals(vm.get('richEditData'), vm.get('richEditDataInitial'))) {
            self.loadRichEditData({
                form: 'AlertNarrative',
                action: 'updateByAlertId',
                data: [{
                    _id_: parseInt(vm.get('alertId'), 10),
                    NOTE_TX: vm.get('richEditData.NOTE_TX')
                }]
            }, self.richEditDataUpdated());
            // переподкачка списка операций:
            if(!Ext.isEmpty(vm.get('alertId')) && alertsGridVm.getView().reference !== 'main-search-operations-form') {
                alertsGridCtrl.refreshAlertsList({
                    alertIds: [parseInt(vm.get('alertId'), 10)],
                    addRecords: true
                });
            }
        }
    },

    cancelForm: function() {
        var self = this,
            vm = self.getViewModel(),
            closeWndFn = function(){self.getView().close();};

        if(!Ext.Object.equals(vm.get('richEditData'), vm.get('richEditDataInitial'))) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Вы хотите сохранить сделанные изменения?',
                fnYes: function() {
                    // сохраняем изменения
                    self.loadRichEditData({
                        form: 'AlertNarrative',
                        action: 'updateByAlertId',
                        data: [{
                            _id_: parseInt(vm.get('alertId'), 10),
                            NOTE_TX: vm.get('richEditData.NOTE_TX')
                        }]
                    }, closeWndFn);
                },
                fnNo: function() {
                    closeWndFn();
                }
            });
        } else {
            closeWndFn();
        }
    }
});
