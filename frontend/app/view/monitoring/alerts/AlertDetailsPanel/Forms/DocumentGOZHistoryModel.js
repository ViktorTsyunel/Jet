Ext.define('AML.view.monitoring.alerts.Forms.DocumentGOZHistoryModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.alertdocumentgozhistoryformvm',
	data: {
		title: 'История изменений кода ГОЗ'
	},
    stores: {
        // Справочник История изменений кода ГОЗ
        alertGOZHistory: {
            model: 'AML.model.AlertGOZHistory',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            sorters: [
                {
                    property: 'CHANGE_DT',
                    direction: 'ASC'
                }
            ]
        }
    }
});
