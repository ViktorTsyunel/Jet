Ext.define('AML.view.monitoring.alerts.Forms.AlertNarrativeModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.alertalertnarrativeformvm',
	data: {
		title: 'Заметки',
        isRichEditorEnabled: false,
        richEditData: null
	},
	formulas: {
		hideButtons: {
			bind: {
				SEARCH_OPERATION_VIEW: '{SEARCH_OPERATION_VIEW}',
				Alerts_READONLY: '{Alerts_READONLY}'
			},
			get: function (dataObject) {
                var vmParent = this.getParent(),
                    viewVM = (vmParent) ? vmParent.getView() : false,
                    refView = (viewVM) ? viewVM.up('main-search-operations-form') : false;
                return dataObject.SEARCH_OPERATION_VIEW || dataObject.Alerts_READONLY || refView;
			}
		}
	}
});
