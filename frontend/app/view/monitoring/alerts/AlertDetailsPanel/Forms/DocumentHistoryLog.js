Ext.define('AML.view.monitoring.alerts.Forms.DocumentHistoryLog', {
    extend: 'Ext.window.Window',
    xtype: 'app-monitoring-alerts-documenthistorylog-form',
    controller: 'alertdocumenthistorylogform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertdocumenthistorylogformvm'
    },
    bind: {
        title: '{title}'
    },
    width: "70%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{alertHistoryLog}'
                    },
                    columns: [
                        {
                            text: 'Дата и время',
                            tooltip: 'Дата и время',
                            stateId: 'BATCH_DATE',
                            dataIndex: 'BATCH_DATE',
                            tdCls: 'v-align-middle',
                            minWidth: 120,
                            flex: 0.2
                        },
                        {
                            text: 'Пользователь',
                            tooltip: 'Пользователь',
                            stateId: 'MODIFIED_BY',
                            dataIndex: 'MODIFIED_BY',
                            tdCls: 'v-align-middle',
                            minWidth: 100,
                            flex: 1
                        },
                        {
                            text: 'Измененные поля',
                            tooltip: 'Измененные поля',
                            stateId: 'FIELD_LIST',
                            dataIndex: 'FIELD_LIST',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            flex: 2
                        },
                        {
                            text: 'Действие',
                            tooltip: 'Действие',
                            stateId: 'ACTVY_TYPE_NM',
                            dataIndex: 'ACTVY_TYPE_NM',
                            tdCls: 'v-align-middle',
                            minWidth: 100,
                            flex: 0.5
                        },
                        {
                            xtype: 'templatecolumn',
                            text: 'Детали',
                            tooltip: 'Детали',
                            tpl:['<a href="#">Подробно</a>'],
                            tdCls: 'v-align-middle',
                            minWidth: 75,
                            flex: 0.2,
                            listeners: {
                                click: 'clickOpenHistoryLogDetailsOES'
                            }
                        },
                        {
                            text: 'IP адрес',
                            tooltip: 'IP адрес',
                            stateId: 'IP_ADDRESS',
                            dataIndex: 'IP_ADDRESS',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            cellWrap: true,
                            flex: 0.2
                        }
                    ]
                }
            ],
            buttons: [
                /*{
                 itemId: 'saveForm',
                 text: 'Применить'
                 },*/
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});