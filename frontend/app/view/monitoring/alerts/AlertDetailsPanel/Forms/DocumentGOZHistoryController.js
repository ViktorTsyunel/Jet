Ext.define('AML.view.monitoring.alerts.Forms.DocumentGOZHistoryController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.alertdocumentgozhistoryform',

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            alertHistoryStore = vm.getStore('alertGOZHistory');

        // загрузка справочника История изменений:
        alertHistoryStore.load({
            scope: self,
            params: {
                opCatCd: vm.get('opCatCd'),
                trxnId: vm.get('trxnId')
            }
        });
    }
});
