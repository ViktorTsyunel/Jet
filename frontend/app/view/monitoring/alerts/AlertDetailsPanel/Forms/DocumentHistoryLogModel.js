Ext.define('AML.view.monitoring.alerts.Forms.DocumentHistoryLogModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.alertdocumenthistorylogformvm',
	data: {
		title: 'История изменений ОЭС'
	},
    stores: {
        // Справочник История изменений
        alertHistoryLog: {
            model: 'AML.model.AlertHistoryLog',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            sorters: [
                {
                    property: 'batch_date',
                    direction: 'DESC'
                }
            ]
        }
    }
});
