Ext.define('AML.view.monitoring.alerts.Forms.AlertAttachmentsModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.alertalertattachmentsformvm',
	data: {
		title: 'Вложения'
	},
    formulas: {
        hideButtons: {
            bind: {
                SEARCH_OPERATION_VIEW: '{SEARCH_OPERATION_VIEW}',
                Alerts_READONLY: '{Alerts_READONLY}'
            },
            get: function (dataObject) {
                var vmParent = this.getParent(),
                    viewVM = (vmParent) ? vmParent.getView() : false,
                    refView = (viewVM) ? viewVM.up('main-search-operations-form') : false;
                return dataObject.SEARCH_OPERATION_VIEW || dataObject.Alerts_READONLY || refView;
                // return dataObject.SEARCH_OPERATION_VIEW;
            }
        }
    },
    stores: {
        // Справочник Вложения
        alertAttachments: {
            model: 'AML.model.AlertAttachments',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            sorters: [
                {
                    property: 'MODIFIED_DATE',
                    direction: 'DESC'
                }
            ]
        }
    }
});
