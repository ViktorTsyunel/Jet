Ext.define('AML.view.monitoring.alerts.Forms.AlertNarrative', {
    extend: 'Ext.window.Window',
    xtype: 'app-monitoring-alerts-narrative-form',
    controller: 'alertalertnarrativeform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertalertnarrativeformvm'
    },
    bind: {
        title: '{title}'
    },
    width: "60%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            tbar: [
                '->',
                {
                    xtype: 'displayfield',
                    margin: '0 10px 0 0',
                    labelWidth: 150,
                    fieldLabel: '<b>Последнее обновление</b>',
                    bind: {
                        value: '{richEditData.MODIFIED_DATE} ({richEditData.MODIFIED_BY})'
                    }
                }
            ],
            items: [
                {
                    xtype: 'htmleditor',
                    reference: 'richEditor',
                    height: 400,
                    readOnly: true,
                    enableSourceEdit: false,
                    listeners: {
                        initialize: function (editor) { //привязать события на IFRAME
                            var frame = editor.getWin(),
                                editorCtrl = editor.lookupController();

                            if(Ext.isIE8) { // багфикс для IE8
                                frame.onfocus = editorCtrl.textAreaDblClickHandler();
                            } else {
                                frame.ondblclick = editorCtrl.textAreaDblClickHandler();
                            }
                            //frame.onblur = editorCtrl.richEditorBlurHandler();
                            if (this.up().up().getViewModel().get('Alerts_READONLY') || this.up().up().getViewModel().get('SEARCH_OPERATION_VIEW')) {
                                editor.setDisabled(true);
                            }
                        }
                    },
                    bind: {
                        readOnly: '{!isRichEditorEnabled}',
                        disabled: '{hideButtons}',
                        value: '{richEditData.NOTE_TX}'
                    }
                }
            ],
            buttons: [
                {
                    //itemId: 'saveForm',
                    reference: 'saveFormBtn',
                    disabled: true,
                    text: 'Применить',
                    bind: {
                      hidden: '{hideButtons}'
                    },
                    handler: 'saveForm'
                },
                {
                    //itemId: 'cancelForm',
                    text: 'Отмена',
                    handler: 'cancelForm'
                }
            ]
        }
    ]
});