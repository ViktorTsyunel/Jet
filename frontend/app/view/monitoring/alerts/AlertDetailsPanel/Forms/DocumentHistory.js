Ext.define('AML.view.monitoring.alerts.Forms.DocumentHistory', {
    extend: 'Ext.window.Window',
    xtype: 'app-monitoring-alerts-documenthistory-form',
    controller: 'alertdocumenthistoryform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertdocumenthistoryformvm'
    },
    bind: {
        title: '{title}'
    },
    width: "80%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{alertHistory}'
                    },
                    columns: [
                        {
                            xtype: 'datecolumn',
                            format: 'd.m.Y H:i:s',
                            text: 'Дата и время',
                            tooltip: 'Дата и время',
                            stateId: 'START_DT',
                            dataIndex: 'START_DT',
                            tdCls: 'v-align-middle',
                            minWidth: 115,
                            flex: 1
                        },
                        {
                            text: 'Действие',
                            tooltip: 'Действие',
                            stateId: 'ACTVY_TYPE_NM',
                            dataIndex: 'ACTVY_TYPE_NM',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            flex: 1
                        },
                        {
                            text: 'Пользователь',
                            tooltip: 'Пользователь',
                            stateId: 'OWNER_ID',
                            dataIndex: 'OWNER_ID',
                            tdCls: 'v-align-middle',
                            minWidth: 100,
                            flex: 1
                        },
                        {
                            text: 'Новый статус',
                            tooltip: 'Новый статус',
                            stateId: 'NEW_STATUS_TX',
                            dataIndex: 'NEW_STATUS_TX',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            flex: 1
                        },
                        {
                            text: 'Комментарий',
                            tooltip: 'Комментарий',
                            stateId: 'CMMNT',
                            dataIndex: 'CMMNT',
                            tdCls: 'v-align-middle',
                            minWidth: 255,
                            cellWrap: true,
                            flex: 1
                        },
                        {
                            xtype: 'templatecolumn',
                            tpl: ['<span class="f-ico-{FILE_EXT}"></span> <span <tpl if="DELETED_FL === \'Y\'">style="color:grey"</tpl>>{ATTCH_NM}</span>'],
                            text: 'Наименование файла',
                            tooltip: 'Наименование файла',
                            stateId: 'ATTCH_NM',
                            dataIndex: 'ATTCH_NM',
                            tdCls: 'v-align-middle',
                            minWidth: 90,
                            flex: 1
                        },
                        {
                            xtype: 'templatecolumn',
                            tpl: ['<tpl if="FILE_NM"><span class="download-arrow-ico"></span> <span <tpl if="DELETED_FL === \'Y\'">style="color:grey"</tpl>>{FILE_NM}</span></tpl>'],
                            text: 'Файл',
                            tooltip: 'Файл',
                            stateId: 'FILE_NM',
                            dataIndex: 'FILE_NM',
                            tdCls: 'v-align-middle',
                            minWidth: 90,
                            flex: 1,
                            listeners: {
                                click: 'clickHandler'
                            }
                        },
                        {
                            text: 'Размер',
                            tooltip: 'Размер',
                            stateId: 'FILE_SIZE',
                            dataIndex: 'FILE_SIZE',
                            tdCls: 'v-align-middle',
                            minWidth: 65,
                            flex: 1
                        },
                        {
                            text: 'Сообщение (ОЭС)',
                            tooltip: 'Сообщение (ОЭС)',
                            stateId: 'OES_DESC',
                            dataIndex: 'OES_DESC',
                            tdCls: 'v-align-middle',
                            minWidth: 100,
                            flex: 1,
                            listeners: {
                                click: 'clickOpenDetailsOES'
                            }
                        }
                    ]
                }
            ],
            buttons: [
                /*{
                 itemId: 'saveForm',
                 text: 'Применить'
                 },*/
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});