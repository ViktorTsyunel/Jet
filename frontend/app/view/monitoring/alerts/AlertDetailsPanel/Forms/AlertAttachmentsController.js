Ext.define('AML.view.monitoring.alerts.Forms.AlertAttachmentsController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.alertalertattachmentsform',

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            alertAttachmentsStore = vm.getStore('alertAttachments');
        if (vm && alertAttachmentsStore && vm.get('alertId')) {
            // загрузка справочника Вложения:
            alertAttachmentsStore.load({
                scope: self,
                params: {
                    alertId: parseInt(vm.get('alertId'), 10)
                }
            });
        }
    },

    clickHandler: function (cmp, itemCell, indexRow, indexCell, e, record, itemRow) {
        var self = this,
            parentCtrl = self.getViewModel().getParent().getView().getController();

        if (e.target.className.indexOf('download-arrow-ico') !== -1) {
            parentCtrl.downloadFile(record.getData().ATTCH_SEQ_ID);
        }
    },

    attachActionCallback: function () {
        var self = this,
            vm = self.getViewModel(),
            alertAttachmentsStore = vm.getStore('alertAttachments'),
            alertsGridVm = (vm.getParent()) ? vm.getParent().getParent() : false,
            alertsGridCtrl = alertsGridVm && alertsGridVm.getView().getController();

        // обвновляем записи:
        return function () { // обертка для привязки контекста this
            // переподкачка списка операций:
            if(alertAttachmentsStore && alertsGridCtrl && !Ext.isEmpty(vm.get('alertId'))) {
                // загрузка справочника Вложения:
                alertAttachmentsStore.load({
                    scope: self,
                    params: {
                        alertId: parseInt(vm.get('alertId'), 10)
                    }
                });
                if (alertsGridVm.getView().reference !== 'main-search-operations-form') {
                    alertsGridCtrl.refreshAlertsList({
                        alertIds: [parseInt(vm.get('alertId'), 10)],
                        addRecords: true
                    });
                }
            }
            // убираем attach он мешает отработки при очеродности вызовов
            if (alertsGridVm) {
                alertsGridVm.set('attachActionCallback', null);
            }
        };
    },

    createAttachment: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            alertsGridVm = vm.getParent().getParent(),
            alertsGridCtrl = alertsGridVm && alertsGridVm.getView().getController();

        alertsGridVm.set('attachActionCallback', self.attachActionCallback());
        alertsGridCtrl.attachAction();
    },

    removeAttachment: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            alertAttachmentsStore = vm.getStore('alertAttachments'),
            alertAttachmentsGrid = this.lookupReference('alertAttachmentsGrid'),
            gridSelectionModel = alertAttachmentsGrid && alertAttachmentsGrid.getSelectionModel();

        alertAttachmentsStore.remove(gridSelectionModel.getSelection());

        alertAttachmentsStore.sync({
            params: {
                action: 'delete'
            }
        });
    },

    selectedRowHandler: function (selection, records) {
        var alertAttachmentsRemoveBtn = this.lookupReference('alertAttachmentsRemoveBtn'),
            readOnly = this.getViewModel().get('Alerts_READONLY'),
            readOnlyView = this.getViewModel().get('SEARCH_OPERATION_VIEW'),
            vmParent = this.getViewModel().getParent(),
            viewVM = (vmParent) ? vmParent.getView() : false,
            refView = (viewVM) ? viewVM.up('main-search-operations-form') : false;
        if(selection.getCount() > 0 && !readOnly && !readOnlyView && !refView) {
            alertAttachmentsRemoveBtn.enable();
        } else {
            alertAttachmentsRemoveBtn.disable();
        }
    },

    beforeEditRow: function (editor, context) {
        if(context.field !== 'ATTCH_NM') {
            return false;
        }
    },

    afterEditRow: function (editor, context) {
        var self = this,
            vm = self.getViewModel(),
            alertAttachmentsStore = vm.getStore('alertAttachments');

        alertAttachmentsStore.sync({
            params: {
                action: 'update'
            }
        });
    }
});
