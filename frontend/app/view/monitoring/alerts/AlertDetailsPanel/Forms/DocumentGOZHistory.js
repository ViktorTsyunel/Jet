Ext.define('AML.view.monitoring.alerts.Forms.DocumentGOZHistory', {
    extend: 'Ext.window.Window',
    xtype: 'app-monitoring-alerts-documentgozhistory-form',
    controller: 'alertdocumentgozhistoryform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertdocumentgozhistoryformvm'
    },
    bind: {
        title: '{title}'
    },
    width: "70%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{alertGOZHistory}'
                    },
                    columns: [
                        {
                            text: 'Дата и время',
                            tooltip: 'Дата и время',
                            stateId: 'CHANGE_DT',
                            dataIndex: 'CHANGE_DT',
                            tdCls: 'v-align-middle',
                            minWidth: 120,
                            flex: 0.2
                        },
                        {
                            text: 'Тип записи',
                            tooltip: 'Тип записи',
                            stateId: 'RECORD_TYPE_CD',
                            dataIndex: 'RECORD_TYPE_CD',
                            tdCls: 'v-align-middle',
                            minWidth: 100,
                            flex: 1
                        },
                        {
                            text: 'Старый код',
                            tooltip: 'Старый код',
                            stateId: 'OLD_ORIG_GOZ_OP_CD',
                            dataIndex: 'OLD_ORIG_GOZ_OP_CD',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            flex: 2
                        },
                        {
                            text: 'Новый код',
                            tooltip: 'Новый код',
                            stateId: 'NEW_ORIG_GOZ_OP_CD',
                            dataIndex: 'NEW_ORIG_GOZ_OP_CD',
                            tdCls: 'v-align-middle',
                            minWidth: 100,
                            flex: 0.5
                        },
                        {
                            text: 'Причина изменения',
                            tooltip: 'Причина изменения',
                            stateId: 'CHANGE_REASON_TX',
                            dataIndex: 'CHANGE_REASON_TX',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            cellWrap: true,
                            flex: 0.2
                        },
                        {
                            text: 'Пользователь',
                            tooltip: 'Пользователь',
                            stateId: 'SRC_USER_NM',
                            dataIndex: 'SRC_USER_NM',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            cellWrap: true,
                            flex: 0.2
                        },
                        {
                            text: 'Система-источник',
                            tooltip: 'Система-источник',
                            stateId: 'SRC_CD',
                            dataIndex: 'SRC_CD',
                            tdCls: 'v-align-middle',
                            minWidth: 125,
                            cellWrap: true,
                            flex: 0.2
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});