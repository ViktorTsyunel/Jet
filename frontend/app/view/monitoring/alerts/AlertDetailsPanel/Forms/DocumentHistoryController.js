Ext.define('AML.view.monitoring.alerts.Forms.DocumentHistoryController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.alertdocumenthistoryform',

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            alertHistoryStore = vm.getStore('alertHistory');

        // загрузка справочника История изменений:
        alertHistoryStore.load({
            scope: self,
            params: {
                alertId: parseInt(vm.get('alertId'), 10)
            }
        });
    },

    clickHandler: function (cmp, itemCell, indexRow, indexCell, e, record, itemRow) {
        var self = this,
            // parentCtrl = self.getViewModel().getParent().getView().down('app-alerts-oes321detailspanel').getController();
            parentCtrl = self.getViewModel().getParent().getView().getController();

        if (e.target.className.indexOf('download-arrow-ico') !== -1) {
            parentCtrl.downloadFile(record.getData().ATTCH_SEQ_ID);
        }
    },

    // открываем детали сообщения
    clickOpenDetailsOES: function (container, cmp, tr, rowIndex, e, record) {
        var self = this,
            vmD = self.getViewModel(),
            //self.getViewModel().getParent().get('selectedAlerts')[0].get('REVIEW_ID')
            // parentC = self.getViewModel().getView().lookupController(true).lookupReference('alertsList') || self.getViewModel().getView().lookupController(true).lookupReference('main-search-operations-form'),
            // parentCtrl = parentC.getController(),
            vm = vmD.getParent().getParent() ? vmD.getParent().getParent() : vmD.back_parent,
            //vm = (vmD.getParent().getParent()) ? vmD.getParent().getParent() : vmD.bparent,
            parentCtrl = vm.getView().getController(),
            toggleBBarBtn = parentCtrl.lookupReference('toggleBBarBtn'),
            monitoringAlertsTopbar = parentCtrl.lookupReference('monitoringAlertsTopbar'),
            oes321Id = record.get('OES_321_ID'),
            mainGrid = vm.get('alertsGrid') || vm.get('alertsOperSearchGrid'),
            selectRec = (mainGrid) ? mainGrid.selection : false,
            reviewId = (selectRec) ? selectRec.get('REVIEW_ID') : false,

            text  = cmp.textContent || cmp.innerText,
            contentText = (text) ? text.replace(new RegExp(/^(\s|\u00A0)+/g), '') : false,
            parentRecordVM, parentID, parentOES321ID, xTypeView, xTypeModel, xTypeM, SEARCH_OPERATION_VIEW = false,
            parentGet, selectedRecA, selectedRec;
        if(!reviewId) {
            parentGet = (vmD) ? vmD.getParent() : false;
            selectedRecA = (parentGet) ? parentGet.get('selectedAlerts') : false;
            selectedRec = (selectedRecA) ? selectedRecA[0] : false;
            if (mainGrid) {
                mainGrid.selection = selectedRec;
            }
            reviewId = (selectedRec) ? selectedRec.get('REVIEW_ID') : false;
        }

        if (vm.get('SEARCH_OPERATION_VIEW')) {
            return;
        }
        if (contentText !== '') {
            self.closeView();
            xTypeM = 'app-alerts-oes321detailspanel';
            xTypeModel = 'oes321detailspanelviewmodel';
            if (parentCtrl.getView().down('app-alerts-oes321detailspanel')) {
                xTypeView = 'app-alerts-oes321detailspanel';
               // xTypeModel = 'oes321detailspanelviewmodel';
                parentRecordVM = parentCtrl.getView().down('app-alerts-oes321detailspanel').getViewModel();
                parentID =  parentRecordVM.get('alertId');
                parentOES321ID = parentRecordVM.get('oes321Id');
            }
            if (parentCtrl.getView().down('app-alerts-alertdetailspanel')) {
                xTypeView = 'app-alerts-alertdetailspanel';
               // xTypeModel = 'alertdetailspanelviewmodel';
                parentRecordVM = parentCtrl.getView().down('app-alerts-alertdetailspanel').getViewModel();
                parentID =  parentRecordVM.get('alertId');
                parentOES321ID = parentRecordVM.get('oes321Id');
            }
            if (parentCtrl.getView().getReference() === 'main-search-operations-form') {
                SEARCH_OPERATION_VIEW = true;
            }
            parentCtrl.getView().down(xTypeView).destroy();
            parentCtrl.getView().add({
                xtype: xTypeM,
                viewModel: {
                    // parentCtrl: parentRecordVM,
                    type: xTypeModel,
                    data: {
                        alertId: reviewId,
                        parentAlertId: parentID,
                        parentOESID: parentOES321ID,
                        oes321Id: oes321Id,
                        OES_READONLY_FL_CONVERSION_MANUAL: true,
                        OES_READONLY_FL_CONVERSION: true,
                        historyAlert: true,
                        xTypeView: xTypeView,
                        SEARCH_OPERATION_VIEW: SEARCH_OPERATION_VIEW
                    },
                    parent: vm
                }
            });
        }
    }
});
