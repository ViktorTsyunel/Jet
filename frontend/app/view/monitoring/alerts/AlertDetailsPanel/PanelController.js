Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertdetailspanel',
    requires: [
        'AML.view.monitoring.alerts.Alerts.Controller',
        'AML.view.main.SearchOperationsController'
    ],
    init: function() {
        var self = this,
            vm = self.getViewModel(),
            alertId = vm.get('alertId'),
            detailsTabPanel = self.lookupReference('detailsTabPanel'),
            store = vm.getParent().getStore('alertsStore'),
            data = store.findRecord('REVIEW_ID', alertId) ? store.findRecord('REVIEW_ID', alertId).getData() : store.getData(),
            prefSelectedRecord = vm.get('prefSelectedRecord'),
            codeSelectedRecord = vm.get('codeSelectedRecord');

        // устанавливаем родительский контроллер для вызова общих действий над операцией:
        if (this.getView().up('app-monitoring-alerts')) {
            self.parentCtrl = this.getView().up('app-monitoring-alerts').getController();
        } else if (this.getView().up('main-search-operations-form')) {
            self.parentCtrl =this.getView().up('main-search-operations-form').getController();
        }
        // установка деталей операции по текущей выбранной записи из списка операций:
        !Ext.isEmpty(data) && vm.set('detailsTable', data);
        // Добавление вкладок:
        if (vm.get('SEARCH_OPERATION_VIEW')) {
            detailsTabPanel.add([
                {
                    xtype: 'app-alertdetailspanel-operationtab',
                    AjaxParams: {
                        form: 'TrxnDetails',
                        action: 'readByTrxnId',
                        opCatCd: prefSelectedRecord,
                        trxnId: codeSelectedRecord
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-payertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByTrxnId',
                        opCatCd: prefSelectedRecord,
                        trxnId: codeSelectedRecord,
                        partyCd: 'ORIG'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'ORIG'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'ORIG'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'ORIG'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'ORIG'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'ORIG'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'ORIG'
                        }
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-recievertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByTrxnId',
                        opCatCd: prefSelectedRecord,
                        trxnId: codeSelectedRecord,
                        partyCd: 'BENEF'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'BENEF'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'BENEF'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'BENEF'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'BENEF'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'BENEF'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'BENEF'
                        }
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-representativepayertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByTrxnId',
                        opCatCd: prefSelectedRecord,
                        trxnId: codeSelectedRecord,
                        partyCd: 'SCND_ORIG'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_ORIG'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_ORIG'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_ORIG'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_ORIG'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_ORIG'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_ORIG'
                        }
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-representativereceivertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByTrxnId',
                        opCatCd: prefSelectedRecord,
                        trxnId: codeSelectedRecord,
                        partyCd: 'SCND_BENEF'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_BENEF'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_BENEF'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_BENEF'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_BENEF'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_BENEF'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action:"readByTrxnId",
                            opCatCd:prefSelectedRecord,
                            trxnId:codeSelectedRecord,
                            partyCd: 'SCND_BENEF'
                        }
                    }
                }
            ]);
        } else {
            detailsTabPanel.add([
                {
                    xtype: 'app-alertdetailspanel-operationtab',
                    AjaxParams: {
                        form: 'TrxnDetails',
                        action: 'readByAlertId',
                        alertId: alertId
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-payertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByAlertId',
                        alertId: alertId,
                        partyCd: 'ORIG'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'ORIG'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'ORIG'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'ORIG'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'ORIG'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'ORIG'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'ORIG'
                        }
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-recievertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByAlertId',
                        alertId: alertId,
                        partyCd: 'BENEF'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'BENEF'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'BENEF'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'BENEF'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'BENEF'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'BENEF'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'BENEF'
                        }
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-representativepayertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByAlertId',
                        alertId: alertId,
                        partyCd: 'SCND_ORIG'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_ORIG'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_ORIG'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_ORIG'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_ORIG'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_ORIG'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_ORIG'
                        }
                    }
                },
                {
                    xtype: 'app-alertdetailspanel-representativereceivertab',
                    AjaxParams: {
                        form: 'TrxnParty',
                        action: 'readByAlertId',
                        alertId: alertId,
                        partyCd: 'SCND_BENEF'
                    },
                    AjaxStoresParams: {
                        addressList: {
                            form: 'CustAddrList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_BENEF'
                        },
                        idDocsList: {
                            form: 'CustIdDocList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_BENEF'
                        },
                        contactsList: {
                            form: 'CustPhonList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_BENEF'
                        },
                        accountsList: {
                            form: 'CustAcctList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_BENEF'
                        },
                        emailsList: {
                            form: 'CustEmailList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_BENEF'
                        },
                        linksList: {
                            form: 'CustCustList',
                            action: 'readByAlertId',
                            alertId: alertId,
                            partyCd: 'SCND_BENEF'
                        }
                    }
                }
            ]);
        }

        detailsTabPanel.setActiveTab(0);
    },

    // костыль для нормального отображения ширины при переключении табов:
    detailsTabPanelReadyHandler: function (tabPanel) {
        tabPanel.setWidth(tabPanel.getWidth());
    },

    showAlertReasonsBtn: null, // для удаления старой кнопки, при перерисовке tpl
    detailsPanelReadyHandler: function () {
        this.showAlertReasonsBtn && this.showAlertReasonsBtn.destroy(); // если уже есть кнопка, то удаляем

        this.showAlertReasonsBtn = Ext.create({
            xtype: 'button',
            renderTo: Ext.get('showAlertReasonsBtn'),
            text: '',
            cls: 'icon-applications',
            handler: 'showAlertReasons',
            scope: this.parentCtrl
        });
    },

    toggleDetailsTable: function (btn) {
        var vm = this.getViewModel(),
            detailsTable = Ext.apply(Ext.clone(vm.get('detailsTable')), {isTableExpanded:  btn.pressed});


        btn.pressed ? btn.removeCls('closed') && btn.addCls('opened') : btn.removeCls('opened') && btn.addCls('closed');
        vm.set('detailsTable', detailsTable);
    },

    refreshDetailsTable: function (data) {
        var self = this,
            vm = self.getViewModel();

        // установка деталей операции по текущей выбранной записи из списка операций:
        vm.set('detailsTable', data);
    },

    showHistoryAction: function () {
        var vm = this.getViewModel(),
            isOpenedWindow = Ext.getCmp('app-monitoring-alerts-documenthistory-form')
                || Ext.getCmp('app-monitoring-alerts-attachments-form')
                || Ext.getCmp('app-monitoring-alerts-narrative-form')
                || Ext.getCmp('app-alerts-alertaddattachment-form')
                || Ext.getCmp('app-alerts-alertactionform-form');
        if (!isOpenedWindow) {
            Ext.create({
                xtype: 'app-monitoring-alerts-documenthistory-form',
                id: 'app-monitoring-alerts-documenthistory-form',
                viewModel: {
                    xtype: 'app-monitoring-alerts-documenthistory-form',
                    data: {
                        isOpenReadOnly: true,
                        alertId: this.getViewModel().get('alertId')
                    },
                    parent: vm
                }
            }).show();
        }
    },

    editAttachAction: function () {
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
            Ext.widget({
                xtype: 'app-monitoring-alerts-attachments-form',
                id: 'app-monitoring-alerts-attachments-form',
                viewModel: {
                    data: {
                        isOpenReadOnly: false
                    },
                    parent: this.getViewModel()
                }
            }).show();
        }
    },

    editNoteAction: function () {
        var isOpenedWindow = Ext.getCmp('app-monitoring-alerts-documenthistory-form')
            || Ext.getCmp('app-monitoring-alerts-attachments-form')
            || Ext.getCmp('app-monitoring-alerts-narrative-form')
            || Ext.getCmp('app-alerts-alertaddattachment-form')
            || Ext.getCmp('app-alerts-alertactionform-form');

        if (!isOpenedWindow) {
            Ext.widget({
                xtype: 'app-monitoring-alerts-narrative-form',
                id: 'app-monitoring-alerts-narrative-form',
                viewModel: {
                    data: {
                        isOpenReadOnly: false
                    },
                    parent: this.getViewModel()
                }
            }).show();
        }
    },

    downloadFile: function (fileId) {
        common.util.Downloader.get({
            url: common.globalinit.ajaxAlertGetAttachmentUrl,
            params: {
                form: 'AlertGetAttachment',
                action: 'getByAttchId',
                attchId: fileId
            }
        });
    },

    useFooter: function (self) {
        var toggleBBarBtn = self.parentCtrl.lookupReference('toggleBBarBtn') || self.parentCtrl.lookupReference('toggleBBarOperSearchBtn'),
            monitoringAlertsTopbar = self.parentCtrl.lookupReference('monitoringAlertsTopbar') || self.parentCtrl.lookupReference('main-search-operations-topbar');
        // проверяем был ли открыт подвал
        if (monitoringAlertsTopbar.reestablishView) {
            toggleBBarBtn.setPressed(true);
            monitoringAlertsTopbar.reestablishView = false;
        }
    },

    closePanel: function () {
        var self = this,
            vm = self.getViewModel(),
            alertId = vm.get('alertId'),
            oes321Id = vm.get('oes321Id'),
            parentAlertId = vm.get('parentAlertId'),
            parentOES321ID = vm.get('parentOESID'),
            parentCtrl = self.parentCtrl,
            xTypeView,
            xTypeModel;

        // отпработка выхода из режима просмотра при переходе из истории изменений
        if (vm.get('historyAlert')) {
            this.showHistoryAction();
            if (parentCtrl.getView().down('app-alerts-oes321detailspanel')) {
                xTypeView = 'app-alerts-oes321detailspanel';
                xTypeModel = 'oes321detailspanelviewmodel';
            }
            if (parentCtrl.getView().down('app-alerts-alertdetailspanel')) {
                xTypeView = 'app-alerts-alertdetailspanel';
                xTypeModel = 'alertdetailspanelviewmodel';
            }
            parentCtrl.getView().down(xTypeView).destroy();
            parentCtrl.getView().add({
                xtype: xTypeView,
                viewModel: {
                    type: xTypeModel,
                    data: {
                        alertId: parentAlertId,
                        oes321Id: parentOES321ID,
                        historyAlert: false
                    }
                }
            });
            return;
        }

        self.getView().close();
        if (alertId) self.parentCtrl.selectRowGrid(alertId);
        self.useFooter(self);
    }
});
