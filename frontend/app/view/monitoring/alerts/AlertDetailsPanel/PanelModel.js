Ext.define('AML.view.monitoring.alerts.AlertDetailsPanel.PanelModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alertdetailspanelviewmodel',
    data: {
        title: 'Детали операции',
        detailsTable: {},
        historyAlert: false,
        SEARCH_OPERATION_VIEW: false
    },
    formulas: {
        hideButtons: {
            bind: {
                SEARCH_OPERATION_VIEW: '{SEARCH_OPERATION_VIEW}',
                Alerts_READONLY: '{Alerts_READONLY}',
                ALLOW_ALERT_EDITING: '{detailsTable.ALLOW_ALERT_EDITING}'
            },
            get: function (dataObject) {
                var vmParent = this.getParent(),
                    viewVM = (vmParent) ? vmParent.getView() : false,
                    refView = (viewVM) ? viewVM.reference : false;
                return dataObject.SEARCH_OPERATION_VIEW || dataObject.Alerts_READONLY || refView === 'main-search-operations-form';
                // return dataObject.SEARCH_OPERATION_VIEW || dataObject.ALLOW_ALERT_EDITING === 0;
            }
        }
    }
});