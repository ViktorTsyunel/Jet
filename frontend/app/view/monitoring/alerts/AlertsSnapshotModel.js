/**
 * Модель представления для причин выявления
 */
Ext.define('AML.view.monitoring.alerts.AlertsSnapshotModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alertssnapshotformviewmodel',
    data: {
        record: {
            column_name: null,
            column_value: null
        }
    },
    formulas: {
        title: function (get) {
            var title = 'Правила выявления для операции';
            return title;
        }
    },
    stores: {
        ruleSnapshot: {
            model: 'AML.model.RULESnapshot',
            type: 'tree',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty:function (data) {
                        return data.data || data.children;
                    },
                    messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'AlertRuleSnapshot',
                    action: 'read',
                    alertId: []
                }
            }
        }
    }
});