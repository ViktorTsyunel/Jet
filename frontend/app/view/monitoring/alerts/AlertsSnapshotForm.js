/**
 * Форма с информацией о причинах выявления
 */
Ext.define('AML.view.monitoring.alerts.AlertsSnapshotForm', {
    extend: 'Ext.window.Window',
	requires: [
		'AML.view.monitoring.alerts.AlertsSnapshotController',
		'AML.view.monitoring.alerts.AlertsSnapshotModel',
		'Ext.layout.container.Fit',
		'Ext.tree.Column',
		'Ext.tree.Panel'
	],
	xtype: 'app-alerts-alertssnapshotform-form',
    controller: 'alertssnapshot',
    iconCls: 'icon-report',
    viewModel: {
        type: 'alertssnapshotformviewmodel'
    },
    bind: {
        title: '{title}'
    },
    border: false,
    width: 1024,
    height: 740,
    scrollable: 'vertical',
    layout: {
        type: 'fit',
        align: 'stretch'
    },
    bodyStyle: {
        padding: 0
    },
    items: [
        {
            xtype: 'treepanel',
            bind: {
                store: '{ruleSnapshot}'
            },
	        flex: 1,
            rootVisible: false,
            border: true,
            rowLines: true,
            cls: 'aml-tree-panel',
            columns: [
                {
                    xtype: 'treecolumn',
                    text: 'Параметр',
                    dataIndex: 'COLUMN_NAME',
                    locked: true,
                    width: 290
                },
                {
                    text: 'Значение параметра',
                    dataIndex: 'COLUMN_VALUE_TO_WRAP',
	                cellWrap: true,
	                flex: 1
                }
            ]
        }
    ],
    buttons: [
        {
            itemId: 'cancelForm',
            text: 'Закрыть'
        }
    ]
});