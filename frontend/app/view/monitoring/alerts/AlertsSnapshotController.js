/**
 * Контроллер представления для информации о причинах выявления
 */
Ext.define('AML.view.monitoring.alerts.AlertsSnapshotController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.alertssnapshot',
    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                'app-alerts-alertssnapshotform-form': {
                    render: function (window) {
                        var self = this,
                            vm = this.getViewModel(),
                            store = self.getStore('ruleSnapshot'),
                            selectedAlerts = vm.get('selectedAlerts'),
                            alertIds = [];

                        for(var i=0;i < selectedAlerts.length;i++) {
                            alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
                        }

                        store.getProxy().setExtraParam('alertId', alertIds[0]);
                        store.load();
                    }
                },
                '#cancelForm': {
                    click: 'onClickCancelForm'
                }
            }
        }
    },
    onClickCancelForm: function () {
        var win = this.getView();
        win.close();
    }
});
