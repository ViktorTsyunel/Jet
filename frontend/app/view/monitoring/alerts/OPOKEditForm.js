/**
 * Форма редактирования кодов ОПОК
 */
Ext.define('AML.view.monitoring.alerts.OPOKEditForm', {
    extend: 'Ext.window.Window',
    requires: [
        'AML.view.monitoring.alerts.OPOKEditController',
        'AML.view.monitoring.alerts.OPOKEditModel',
        'Ext.layout.container.Fit'
    ],
    xtype: 'app-alerts-editopokform-form',
    controller: 'opokedit',
    iconCls: 'icon-report',
    viewModel: {
        type: 'opokeditformviewmodel'
    },
    bind: {
        title: '{title}'
    },
    width: 360,
    //width: (Ext.isIE8 ? 369 : '25%'),
    //height: (Ext.isIE8 ? 179 : '21%'),
    layout: {
        type: 'vbox'
    },
    items: [
        {
            xtype: 'toolbar',
            layout: {
                type: 'vbox'
            },
            vertical: true,
            store: 'OPOKEdit',
            /*bind: {
                title: '{opokedit}'
            },*/
            collapsible: false,
            queryMode: 'local',
            items: [
                {
                    xtype: 'combobox',
                    name: 'OPOK_NB',
                    store: 'OPOK',
                    queryMode: 'local',
                    displayField: 'OPOK_NB',
                    editable: false,
                    valueField: 'OPOK_NB',
                    labelWidth: 153,
                    columnWidth: 49,
                    padding: '5px 5px 5px 5px',
                    fieldLabel: 'Вид операции',
                    id: 'OPOK_NB',
                    triggerClear: true,
                    flex: 1,
                    listeners: {
                        scope: this,
                        change: function (field, newValue, oldValue, eOpts) {
                            if (newValue) {
                                var store = Ext.StoreManager.lookup('OPOKEdit').getAt(0);
                                store.set('OPOK_NB', newValue)
                            }
                        }
                    }
                },
                {
                    xtype: 'fieldset',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    collapsible: false,
                    border: false,
                    padding: '5px 5px 5px 5px',
                    defaults: {
                       // anchor: '100%',
                        labelWidth: 153
                    },
                    items: [
                        {
                            xtype: 'fieldcontainer',
                            layout: {
                                type: 'hbox'
                            },
                            fieldLabel: 'Дополнительный вид операций',
                            defaults: {
                                labelAlign: 'left',
                                labelWidth: 153
                            },
                            flex: 2,
                            items: [
                                {
                                    name: 'ADD_OPOKS_TX',
                                    xtype: 'tagfield',
                                    store: 'OPOK',
                                    queryMode: 'local',
                                    valueField: 'OPOK_NB',
                                    displayField: 'OPOK_NB',
                                    id: 'ADD_OPOKS_TX'
                                }
                            ]
                        }
                    ]
                }
            ]
        }],
    buttons: [
        {
            itemId: 'saveData',
            iconCls: 'icon-save-data',
            text: 'Применить'
        },
        {
            itemId: 'cancelForm',
            iconCls: 'icon-backtothe-primitive',
            text: 'Закрыть'
        }
    ]
});