/**
 * Контроллер представления формы редактирования ОПОК
 */
Ext.define('AML.view.monitoring.alerts.OPOKEditController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.opokedit',
    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                'app-alerts-editopokform-form': {
                    afterRender: function (window) {
                        var store = Ext.data.StoreManager.lookup('OPOKEdit').getAt(0);
                        Ext.getCmp('OPOK_NB').setValue(store.get('OPOK_NB'));
                        Ext.getCmp('ADD_OPOKS_TX').setValue(store.get('ADD_OPOKS_TX'));
                    }
                },
                '#saveData': {
                    click: 'onClickSaveData'
                },
                '#cancelForm': {
                    click: 'onClickCancelForm'
                }
            }
        }
    },
    onClickSaveData: function (window) {
        var self = this,
            win = this.getView(),
            vm = this.getViewModel(),
            opokRule = Ext.StoreManager.lookup('OPOKEdit').getAt(0);

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.JSON.encode({
                form: 'Alerts',
                action: 'opokedit',
                id: opokRule.get('id'),
                OPOK_NB: Ext.getCmp('OPOK_NB').getValue(),
                ADD_OPOKS_TX: Ext.getCmp('ADD_OPOKS_TX').getValue(),
                where: opokRule.get('where'),
                filters: opokRule.get('filters')
            }),
            success: function (response) {
                var respmess = Ext.JSON.decode(response.responseText).message,
                    hide = Ext.JSON.decode(response.responseText).hide,
                    dataIds = Ext.JSON.decode(response.responseText).data,
                    alertsStore = vm.getParent().getStore('alertsStore');
                if (respmess.length > 0) {
                    AML.app.msg.toast({
                        type: 'info',
                        message: respmess
                    });
                }
                //обновляем записи, пришедшие с сервера (новые - добавляем)
                for (i = 0; i < dataIds.length; i++) {
                    var tmpRec = alertsStore.getById(dataIds[i].id);
                    if (tmpRec) {
                        // запись существует - обновляем!
                        tmpRec.load();
                        Ext.Msg.alert('loaded!', dataIds[i].id);
                    } else {
                        // запись не существует - добавляем!
                        Ext.Ajax.request({
                            url: common.globalinit.ajaxUrl,
                            method: common.globalinit.ajaxMethod,
                            headers: common.globalinit.ajaxHeaders,
                            timeout: common.globalinit.ajaxTimeOut,
                            params: Ext.JSON.encode({
                                form: 'Alerts',
                                action: 'scope',
                                id: dataIds[i].id,
                                where: opokRule.get('where'),
                                filters: opokRule.get('filters')
                            }),
                            success: function (response) {
                                var data = Ext.JSON.decode(response.responseText).data,
                                    store = vm.getParent().getStore('alertsStore'),
                                    record = store.add(data)[0],
                                    form = Ext.widget('app-monitoring-alerts'),
                                    formViewModel = form.getViewModel();
                                formViewModel.set('record', record);
                                record.load();
                            }
                        })
                    }
                    // удаляем записи и локального хранилища, непрошедшие серверные фильтры
                    for (var j = 0; j < hide.length; j++) {
                        var hideRec = alertsStore.getById(hide[j].id);
                        alertsStore.remove(hideRec);
                        //todo -- когда начнете удалеть алерты надо будет переписать этот код
                        // тут, ничего себе!, очищается массив удаленных объектов и синхронизация ничего не удалит
                        // мне сказал это сделать Дубренский Дмитрий Валерьевич!
                        //alertsStore.remove(alertsStore.getRemovedRecords('Alerts'));
                        //todo что???
                    }
                }
            }
        });
    },

    onClickCancelForm: function () {
        this.getView().close();
    }
});
