/**
 * Модель представления формы редактирования кодов ОПОК
 */
Ext.define('AML.view.monitoring.alerts.OPOKEditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.opokeditformviewmodel',
    data: {
        title: 'Редактирование кодов ОПОК',
        record: {
            opok_nb: null,
            add_opoks_tx: null
        }
    }
});