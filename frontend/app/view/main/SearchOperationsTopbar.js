Ext.define('AML.view.main.SearchOperationsTopbar', {
    extend: 'Ext.container.Container',
    requires: [
        'AML.view.monitoring.alerts.OES321Details.PanelController',
        'AML.view.monitoring.alerts.AlertActionForm.KGRKOFilials'
    ],
    xtype: 'main-search-operations-topbar',
    reestablishView: false,
    reference: 'main-search-operations-topbar',
    items: [
        {
            xtype: 'fieldcontainer',
            isFilters: true,
            hidden: false,
            bind: {
                hidden: '{!showSearchOperFilter}'
            },
            layout: {
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'column'
                    },
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 100,
                        margin: '3 3 0 3',
                        flex: 1
                    },
                    items: [
                        {
                            width: 150,
                            xtype: 'fieldcontainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Дата операции',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 20
                                    },
                                    items: [
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'от',
                                            name: 'dtFromOperSearch',
                                            reference: 'dtFromOperSearch',
                                            bind: '{record.dtFromOperSearch}',
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: false,
                                            listeners: {
                                                scope: this,
                                                change: function (field, newValue, oldValue, eOpts) {
                                                    var toDate = field.up('fieldset').down('datefield[name=dtToOperSearch]');
                                                    toDate.setMinValue(newValue);
                                                    if (Ext.Date.diff(newValue, toDate.getValue(), Ext.Date.DAY) < 0) {
                                                        toDate.setValue(newValue);
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'до',
                                            name: 'dtToOperSearch',
                                            reference: 'dtToOperSearch',
                                            bind: '{record.dtToOperSearch}',
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: true
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 150,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Сумма операции',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 20
                                    },
                                    items: [
                                        {
                                            name: 'amountFromOperSearch',
                                            reference: 'amountFromOperSearch',
                                            xtype: 'textfield',
                                            fieldLabel: 'от',
                                            bind: '{record.amountFromOperSearch}',
                                            enableKeyEvents: true,
                                            maskRe: new RegExp('[0-9,.]'),
                                            //preventMark: false,
                                            value: 600000,
                                            validateOnChange: false,
                                            validateOnBlur: false,
                                            allowBlank: false,
                                            listeners: {
                                                dblclick : {
                                                    fn: 'onDbClickAmountFrom',
                                                    element: 'el'
                                                },
                                                afterrender: function (container, value, eOpts) {
                                                    if (value) {
                                                        container.clearInvalid();
                                                        if (container.nextSibling('textfield').getValue()) {
                                                            container.nextSibling('textfield').clearInvalid();
                                                            if (parseFloat(container.nextSibling('textfield').getRawValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) < parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))) {
                                                                container.markInvalid('Значение "от" не может быть больше "до"');
                                                            }
                                                        }
                                                    }
                                                    container.setValue(Ext.util.Format.number(String(parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                                },
                                                blur: function (container, value, eOpts) {
                                                    if (value) {
                                                        container.clearInvalid();
                                                        if (container.nextSibling('textfield').getValue()) {
                                                            container.nextSibling('textfield').clearInvalid();
                                                            if (parseFloat(container.nextSibling('textfield').getRawValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) < parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))) {
                                                                container.markInvalid('Значение "от" не может быть больше "до"');
                                                            }
                                                        }
                                                    }
                                                    container.setValue(Ext.util.Format.number(String(parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                                }
                                            }
                                        },
                                        {
                                            name: 'amountToOperSearch',
                                            reference: 'amountToOperSearch',
                                            xtype: 'textfield',
                                            fieldLabel: 'до',
                                            bind: '{record.amountToOperSearch}',
                                            maskRe: new RegExp('[0-9,.]'),
                                            validateOnChange: false,
                                            validateOnBlur: false,
                                            listeners: {
                                                dblclick : {
                                                    fn: 'onDbClickAmountTo',
                                                    element: 'el'
                                                },
                                                blur: function (container, value, eOpts) {
                                                    if (value) {
                                                        container.clearInvalid();
                                                        if (container.previousSibling('textfield').getValue()) {
                                                            container.previousSibling('textfield').clearInvalid();
                                                            if (parseFloat(container.previousSibling('textfield').getRawValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) > parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))) {
                                                                container.markInvalid('Значение "от" не может быть больше "до"');
                                                            }
                                                        }
                                                        container.setValue(Ext.util.Format.number(String(parseFloat(container.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'))), '0,000.00'));
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 305,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Назначение платежа',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 100
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    name: 'standartConditionOperSearch',
                                                    reference: 'standartConditionOperSearch',
                                                    xtype: 'combobox',
                                                    fieldLabel: 'Станд. условия',
                                                    labelWidth: 100,
                                                    store: 'OPOKCriteriaT2',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{CRIT_CD} - {CRIT_NM}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayTpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '{CRIT_CD} - {CRIT_NM}',
                                                        '</tpl>'
                                                    ),
                                                    // displayField: 'CRIT_NM',
                                                    valueField: 'CRIT_CD',
                                                    triggerClear: true,
                                                    multiSelect: false,
                                                    bind: '{record.standartConditionOperSearch}',
                                                    flex: 1,
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function (c, newValue) {
                                                            var value = newValue ? c.findRecordByValue(newValue).get('NOTE_TX') : '';
                                                            if (c.tooltip) {
                                                                c.tooltip.update(value);
                                                            } else {
                                                                c.tooltip = Ext.create('Ext.tip.ToolTip', {
                                                                    target: c.getEl(),
                                                                    html: value
                                                                });
                                                            }
                                                            c.tooltip.setDisabled(!value);
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    border: 0,
                                                    align: 'right',
                                                    margin: '0 0 5 0',
                                                    iconCls: 'icon-report',
                                                    style: 'background:none;',
                                                    handler: 'openStandartConditionForm'
                                                }
                                            ]
                                        },
                                        {
                                            labelWidth: 100,
                                            name: 'conditionOperSearch',
                                            reference: 'conditionOperSearch',
                                            xtype: 'textfield',
                                            fieldLabel: 'Условия:',
                                            bind: '{record.conditionOperSearch}'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            width: 212,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Подразделение',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 35
                                    },
                                    items: [
                                        {
                                            name: 'tbIdsOperSearch',
                                            reference: 'tbIdsOperSearch',
                                            xtype: 'combobox',
                                            store: 'TB',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                                '<tpl if="id &gt; 0">{id} - {label}' +
                                                '<tpl else>{label}</tpl>' +
                                                '</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'ТБ',
                                            displayField: 'label',
                                            valueField: 'id',
                                            triggerClear: true,
                                            multiSelect: true,
                                            matchFieldWidth: false,
                                            listeners: {
                                                scope: this,
                                                change: function (field, newValue, oldValue, eOpts) {
                                                    var osbIds = field.up('fieldset').down('combobox[name=osbIdsOperSearch]'),
                                                        tbosb = Ext.data.StoreManager.lookup('TBOSB');
                                                    tbosb.clearFilter();
                                                    osbIds.clearValue();
                                                    if (newValue) {
                                                        tbosb.filterBy(function (record) {
                                                            return Ext.Array.indexOf(newValue, record.get("tb_id")) !== -1;
                                                        }, this);
                                                    }
                                                    osbIds.setDisabled(!newValue || tbosb.getData().length < 1);
                                                }
                                            },
                                            bind: '{record.tbIdsOperSearch}'
                                        },
                                        {
                                            name: 'osbIdsOperSearch',
                                            reference: 'osbIdsOperSearch',
                                            xtype: 'combobox',
                                            store: 'TBOSB',
                                            disabled: true,
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'ОСБ',
                                            displayField: 'label',
                                            valueField: 'id',
                                            triggerClear: true,
                                            multiSelect: true,
                                            matchFieldWidth: false,
                                            bind: '{record.osbIdsOperSearch}'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            width: 420,
                            title: 'Операция',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'left',
                                labelWidth: 130
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 130
                                    },
                                    items: [
                                        {
                                            name: 'notCodeOperSearch',
                                            reference: 'notCodeOperSearch',
                                            xtype: 'combobox',
                                            store: 'OPOK',
                                            padding: '0 5 5 0',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'Не выявлена по коду',
                                            displayField: 'OPOK_NB',
                                            valueField: 'OPOK_NB',
                                            triggerClear: true,
                                            multiSelect: false,
                                            bind: '{record.notCodeOperSearch}',
                                            flex: 0.6,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                }
                                            }
                                        },
                                        {
                                            name: 'forBetweenOperSearch',
                                            reference: 'forBetweenOperSearch',
                                            xtype: 'combobox',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{all}</div>',
                                                '</tpl>'
                                            ),
                                            tooltip: {
                                                text: 'Учитывается только для межфилиальных операций'
                                            },
                                            fieldLabel: '',
                                            labelWidth: 110,
                                            displayField: 'label',
                                            valueField: 'id',
                                            triggerClear: true,
                                            multiSelect: false,
                                            bind: {
                                                value: '{record.forBetweenOperSearch}',
                                                store: '{forBetweenStore}'
                                            },
                                            flex: 0.4,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            padding: '0 0 5 0',
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                },
                                                change: function(c) {
                                                    var tooltip = null;
                                                    tooltip = new Ext.ToolTip({
                                                        target: c.getEl(),
                                                        anchor: 'top',
                                                        anchorToTarget: true,
                                                        //html: c.getRawValue(),
                                                        listeners: {
                                                            scope: this,
                                                            beforeshow: function(tip) {
                                                                tip.setHtml(c.getRawValue());
                                                            }
                                                        }
                                                    });
                                                    return tooltip;
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 130
                                    },
                                    items: [
                                        {
                                            name: 'categoryOperSearch',
                                            reference: 'categoryOperSearch',
                                            xtype: 'combobox',
                                            fieldLabel: 'Категория',
                                            width: 210,
                                            padding: '0 5 5 0',
                                            editable: false,
                                            queryMode: 'local',
                                            triggerClear: true,
                                            multiSelect: true,
                                            displayField: 'label',
                                            valueField: 'iddb',
                                            bind: {
                                                value: '{record.categoryOperSearch}',
                                                store: '{categoryViewStore}'
                                            },
                                            // flex: 1,
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                '</tpl>'
                                            ),
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                },
                                                change: function(c) {
                                                    var tooltip = new Ext.ToolTip({
                                                        target: c.getEl(),
                                                        html: c.getRawValue()
                                                    });
                                                    return tooltip;
                                                }
                                            }
                                        },
                                        {
                                            name: 'systemOperSearch',
                                            reference: 'systemOperSearch',
                                            xtype: 'combobox',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                '</tpl>'
                                            ),
                                            fieldLabel: 'Система-источник',
                                            labelWidth: 110,
                                            displayField: 'label',
                                            valueField: 'label',
                                            triggerClear: true,
                                            multiSelect: true,
                                            bind: {
                                                value: '{record.systemOperSearch}',
                                                store: '{systemViewStore}'
                                            },
                                            flex: 1,
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            padding: '0 0 5 0',
                                            listeners: {
                                                expand: function (field) {
                                                    field.getPicker().refresh();
                                                },
                                                change: function(c) {
                                                    var tooltip = new Ext.ToolTip({
                                                        target: c.getEl(),
                                                        html: c.getRawValue()
                                                    });
                                                    return tooltip;
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'column'
                    },
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 100,
                        margin: '3 3 5 3',
                        flex: 1
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox'
                            },
                            defaults: {
                                labelAlign: 'left',
                                labelWidth: 100,
                                margin: 1
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Плательщик',
                                    height: 75,
                                    createTitleCmp: function() {
                                        var me  = this,
                                            cfg = {
                                                xtype: 'component',
                                                html: me.title,
                                                ui: me.ui,
                                                // cls: me.baseCls + '-header-text',
                                                style:'background:none; font-size: 11px; line-height: 14px; font-family: tahoma, arial, verdana, sans-serif; color: #333; vertical-align:middle',
                                                id: me.id + '-legendTitle'
                                        };
                                        var titleCmp = {
                                            xtype: 'container',
                                            layout: 'hbox',
                                            style:'background:none;',
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    iconCls: 'icon-button-menu-down',
                                                    margin: 0,
                                                    text: '',
                                                    border: false,
                                                    plain: true,
                                                    style:'background:none;',
                                                    layout: 'vbox',

                                                    layoutConfig: {
                                                        align: 'stretch'//,
                                                        // pack: 'start'
                                                    },
                                                    forceLayout: true,
                                                    arrowVisible: false,
                                                    flex: 1,
                                                    menu: {
                                                        xtype: 'menu',
                                                        items: [
                                                            {
                                                                text: 'Условия на плательщика',
                                                                scale: 'small',
                                                                handler: 'checkPayerCondition'
                                                            },
                                                            {
                                                                text: 'Условия на получателя',
                                                                scale: 'small',
                                                                handler: 'checkPayerCondition'
                                                            },
                                                            {
                                                                text: 'Условия на Плательщика или Получателя',
                                                                scale: 'small',
                                                                handler: 'checkPayerCondition'
                                                            },
                                                            {
                                                                text: 'Условия и на Плательщика и на Получателя',
                                                                scale: 'small',
                                                                handler: 'checkPayerCondition'
                                                            }
                                                        ]
                                                    }
                                                }, cfg
                                            ]
                                        };

                                        return (me.titleCmp = Ext.widget(titleCmp));
                                    },
                                    layout: {
                                        type: 'vbox',
                                        align: 'left'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            defaults: {
                                                labelAlign: 'right',
                                                align: 'left'
                                            },
                                            items: [
                                                {
                                                    name: 'payerNameOperSearch',
                                                    reference: 'payerNameOperSearch',
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Наименование',
                                                    labelWidth: 85,
                                                    bind: {
                                                        value: '{record.payerNameOperSearch}'
                                                    },
                                                    triggerClear: true,
                                                    width: 320
                                                },
                                                {
                                                    name: 'payerCountryOperSearch',
                                                    reference: 'payerCountryOperSearch',
                                                    xtype: 'combobox',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    fieldLabel: 'Страна',
                                                    labelWidth: 50,
                                                    width: 130,
                                                    displayField: 'label',
                                                    valueField: 'iddb',
                                                    triggerClear: true,
                                                    multiSelect: false,
                                                    bind: {
                                                        value: '{record.payerCountryOperSearch}',
                                                        store: '{payerCountryViewStore}'
                                                    },
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    padding: '0 0 5 0',
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function(c) {
                                                            var tooltip = new Ext.ToolTip({
                                                                target: c.getEl(),
                                                                html: c.getRawValue()
                                                            });
                                                            return tooltip;
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'right',
                                                align: 'left'
                                            },
                                            items: [
                                                {
                                                    name: 'payerINNOperSearch',
                                                    reference: 'payerINNOperSearch',
                                                    xtype: 'textfield',
                                                    hideTrigger: true,
                                                    allowBlank: true,
                                                    fieldLabel: 'ИНН',
                                                    labelWidth: 30,
                                                    bind: {
                                                        value: '{record.payerINNOperSearch}'
                                                    },
                                                    width: 130,
                                                    padding: '0 0 5 0'
                                                },
                                                {
                                                    xtype: 'button',
                                                    border: 0,
                                                    // width: 20,
                                                    align: 'right',
                                                    margin: '0 5 5 0',
                                                    iconCls : 'icon-report',
                                                    style:'background:none;',
                                                    handler: 'openClientFormByINN'
                                                },
                                                {
                                                    // name: 'someName',
                                                    // reference: 'someName',
                                                    xtype: 'combobox',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{WATCH_LIST_CD} - {WATCH_LIST_NM}</div>',
                                                        '</tpl>'
                                                    ),
                                                    fieldLabel: 'Перечень',
                                                    labelWidth: 55,
                                                    width: 174,
                                                    // displayField: 'WATCH_LIST_NM',
                                                    displayTpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '{WATCH_LIST_CD} - {WATCH_LIST_NM}',
                                                        '</tpl>'
                                                    ),
                                                    valueField: 'WATCH_LIST_CD',
                                                    triggerClear: true,
                                                    multiSelect: false,
                                                    bind: {
                                                        value: '{record.oes321TypeSender}',
                                                        store: '{oes321types}'
                                                    },
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    padding: '0 0 5 0'
                                                },
                                                {
                                                    xtype: 'button',
                                                    border: 0,
                                                    // width: 20,
                                                    align: 'right',
                                                    margin: '0 5 5 0',
                                                    iconCls : 'icon-report',
                                                    style:'background:none;',
                                                    handler: 'openOPOKType'
                                                },
                                                {
                                                    xtype: 'checkbox',
                                                    fieldLabel: 'Исключить',
                                                    labelWidth: 77,
                                                    checked: false,
                                                    value: false,
                                                    bind: {
                                                        value: '{record.excludeSenderInn}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    title: 'Получатель',
                                    margin: '0 0 1 0',
                                    autoHeight:true,
                                    defaultType: 'textfield',
                                    hidden: true,
                                    reference: 'payerSelectNameAddBlocks',
                                    border: 1,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'right',
                                        align: 'left'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            defaults: {
                                                labelAlign: 'right',
                                                align: 'left'
                                            },
                                            items: [
                                                {
                                                    name: 'payerNameOperSearchAdd',
                                                    reference: 'payerNameOperSearchAdd',
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Наименование',
                                                    labelWidth: 85,
                                                    triggerClear: true,
                                                    bind: '{record.payerNameOperSearchAdd}',
                                                    width: 320
                                                },
                                                {
                                                    name: 'payerCountryOperSearchAdd',
                                                    reference: 'payerCountryOperSearchAdd',
                                                    xtype: 'combobox',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    fieldLabel: 'Страна',
                                                    labelWidth: 50,
                                                    width: 130,
                                                    displayField: 'label',
                                                    valueField: 'iddb',
                                                    triggerClear: true,
                                                    multiSelect: false,
                                                    bind: {
                                                        value: '{record.payerCountryOperSearchAdd}',
                                                        store: '{payerCountryViewStore}'
                                                    },
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    padding: '0 0 5 0',
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function(c) {
                                                            var tooltip = new Ext.ToolTip({
                                                                target: c.getEl(),
                                                                html: c.getRawValue()
                                                            });
                                                            return tooltip;
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'right',
                                                align: 'left'
                                            },
                                            items: [
                                                {
                                                    name: 'payerINNOperSearchAdd',
                                                    reference: 'payerINNOperSearchAdd',
                                                    xtype: 'textfield',
                                                    hideTrigger: true,
                                                    allowBlank: true,
                                                    fieldLabel: 'ИНН',
                                                    labelWidth: 30,
                                                    bind: '{record.payerINNOperSearchAdd}',
                                                    width: 130,
                                                    padding: '0 0 5 0'
                                                },
                                                {
                                                    xtype: 'button',
                                                    border: 0,
                                                    // width: 20,
                                                    align: 'right',
                                                    margin: '0 5 5 0',
                                                    iconCls : 'icon-report',
                                                    style:'background:none;',
                                                    handler: 'openClientFormByINN'
                                                },
                                                {
                                                    // name: 'someName',
                                                    // reference: 'someName',
                                                    xtype: 'combobox',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{WATCH_LIST_CD} - {WATCH_LIST_NM}</div>',
                                                        '</tpl>'
                                                    ),
                                                    fieldLabel: 'Перечень',
                                                    labelWidth: 55,
                                                    width: 174,
                                                    // displayField: 'WATCH_LIST_NM',
                                                    displayTpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '{WATCH_LIST_CD} - {WATCH_LIST_NM}',
                                                        '</tpl>'
                                                    ),
                                                    valueField: 'WATCH_LIST_CD',
                                                    triggerClear: true,
                                                    multiSelect: false,
                                                    bind: {
                                                        value: '{record.oes321TypeTaker}',
                                                        store: '{oes321types}'
                                                    },
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    padding: '0 0 5 0'
                                                },
                                                {
                                                    xtype: 'button',
                                                    border: 0,
                                                    // width: 20,
                                                    align: 'right',
                                                    margin: '0 5 5 0',
                                                    iconCls : 'icon-report',
                                                    style:'background:none;',
                                                    handler: 'openOPOKType'
                                                },
                                                {
                                                    xtype: 'checkbox',
                                                    fieldLabel: 'Исключить',
                                                    labelWidth: 77,
                                                    checked: false,
                                                    value: false,
                                                    bind: {
                                                        value: '{record.excludeTakerInn}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox'
                            },
                            defaults: {
                                labelAlign: 'left',
                                labelWidth: 90,
                                margin: '3 3 0 3',
                                flex: 1
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Счет плательщика',
                                    reference: 'payerAccountOperSearch',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 90
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'left',
                                                labelWidth: 100
                                            },
                                            items: [
                                                {
                                                    name: 'payerStandartConditions',
                                                    reference: 'payerStandartConditions',
                                                    xtype: 'combobox',
                                                    fieldLabel: 'Станд. условия',
                                                    labelWidth: 100,
                                                    store: 'OPOKCriteriaT1',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{CRIT_CD} - {CRIT_NM}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayTpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '{CRIT_CD} - {CRIT_NM}',
                                                        '</tpl>'
                                                    ),
                                                    // displayField: 'CRIT_NM',
                                                    valueField: 'CRIT_CD',
                                                    triggerClear: true,
                                                    multiSelect: false,
                                                    padding: '0 5 5 0',
                                                    bind: '{record.payerStandartConditions}',
                                                    width: 325,
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function(c, newValue) {
                                                            var value = newValue ? c.findRecordByValue(newValue).get('NOTE_TX') : '';
                                                            if (c.tooltip) {
                                                                c.tooltip.update(value);
                                                            } else {
                                                                c.tooltip = Ext.create('Ext.tip.ToolTip',{
                                                                    target: c.getEl(),
                                                                    html: value
                                                                });
                                                            }
                                                            c.tooltip.setDisabled(!value);
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    border: 0,
                                                    // width: 20,
                                                    align: 'right',
                                                    margin: '0 5 5 0',
                                                    iconCls : 'icon-report',
                                                    style:'background:none;',
                                                    handler: 'openStandartConditionRO1'
                                                },
                                                {
                                                    name: 'payerAccountNumber',
                                                    reference: 'payerAccountNumber',
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Номер счета',
                                                    labelWidth: 100,
                                                    flex: 1,
                                                    padding: '0 0 5 0',
                                                    // triggerClear: true,
                                                    bind: '{record.payerAccountNumber}'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'left',
                                                labelWidth: 90
                                            },
                                            items: [
                                                {
                                                    name: 'payerConditions',
                                                    reference: 'payerConditions',
                                                    xtype: 'textfield',
                                                    triggerClear: true,
                                                    fieldLabel: 'Условия',
                                                    labelWidth: 100,
                                                    bind: '{record.payerConditions}',
                                                    width: 350,
                                                    padding: '0 5 5 0'
                                                },
                                                {
                                                    name: 'payerRegionSearch',
                                                    reference: 'payerRegionSearch',
                                                    xtype: 'combobox',
                                                    labelWidth: 100,
                                                    fieldLabel: 'Область поиска',
                                                    store: 'OPOKRuleAccountSearchArea',
                                                    queryMode: 'local',
                                                    valueField: 'id',
                                                    displayField: 'label',
                                                    editable: false,
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    triggerClear: true,
                                                    multiSelect: true,
                                                    bind: {
                                                        value: '{record.payerRegionSearch}'
                                                    },
                                                    // flex: 1,
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    padding: '0 0 5 0',
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function(c) {
                                                            var tooltip = new Ext.ToolTip({
                                                                target: c.getEl(),
                                                                html: c.getRawValue()
                                                            });
                                                            return tooltip;
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    title: 'Счет получателя',
                                    hidden: true,
                                    reference: 'payerSelectAccountAddBlocks',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        labelAlign: 'left',
                                        labelWidth: 90
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'left',
                                                labelWidth: 100
                                            },
                                            items: [
                                                {
                                                    name: 'payerStandartConditionsAdd',
                                                    reference: 'payerStandartConditionsAdd',
                                                    xtype: 'combobox',
                                                    fieldLabel: 'Станд. условия',
                                                    labelWidth: 100,
                                                    store: 'OPOKCriteriaT1',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{CRIT_CD} - {CRIT_NM}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayTpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '{CRIT_CD} - {CRIT_NM}',
                                                        '</tpl>'
                                                    ),
                                                    // displayField: 'CRIT_NM',
                                                    valueField: 'CRIT_CD',
                                                    triggerClear: true,
                                                    multiSelect: false,
                                                    padding: '0 5 5 0',
                                                    bind: '{record.payerStandartConditionsAdd}',
                                                    width: 325,
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function(c, newValue) {
                                                            var frec = newValue ? c.findRecordByValue(newValue) : '',
                                                                value = frec ? frec.get('NOTE_TX') : '';
                                                            if (c.tooltip) {
                                                                c.tooltip.update(value);
                                                            } else {
                                                                c.tooltip = Ext.create('Ext.tip.ToolTip',{
                                                                    target: c.getEl(),
                                                                    html: value
                                                                });
                                                            }
                                                            c.tooltip.setDisabled(!value);
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    border: 0,
                                                    // width: 20,
                                                    align: 'right',
                                                    margin: '0 5 5 0',
                                                    iconCls : 'icon-report',
                                                    style:'background:none;',
                                                    handler: 'openStandartConditionRO1'
                                                },
                                                {
                                                    name: 'payerAccountNumberAdd',
                                                    reference: 'payerAccountNumberAdd',
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Номер счета',
                                                    labelWidth: 100,
                                                    flex: 1,
                                                    padding: '0 0 5 0',
                                                    // triggerClear: true,
                                                    bind: '{record.payerAccountNumberAdd}'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            defaults: {
                                                labelAlign: 'left',
                                                labelWidth: 90
                                            },
                                            items: [
                                                {
                                                    name: 'payerConditionsAdd',
                                                    reference: 'payerConditionsAdd',
                                                    xtype: 'textfield',
                                                    triggerClear: true,
                                                    fieldLabel: 'Условия',
                                                    labelWidth: 100,
                                                    bind: '{record.payerConditionsAdd}',
                                                    width: 350,
                                                    padding: '0 5 5 0'
                                                },
                                                {
                                                    name: 'payerRegionSearchAdd',
                                                    reference: 'payerRegionSearchAdd',
                                                    xtype: 'combobox',
                                                    labelWidth: 100,
                                                    fieldLabel: 'Область поиска',
                                                    store: 'OPOKRuleAccountSearchArea',
                                                    queryMode: 'local',
                                                    valueField: 'id',
                                                    displayField: 'label',
                                                    editable: false,
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    triggerClear: true,
                                                    multiSelect: true,
                                                    bind: {
                                                        value: '{record.payerRegionSearchAdd}'
                                                    },
                                                    flex: 1,
                                                    forceSelection: true,
                                                    matchFieldWidth: false,
                                                    padding: '0 0 5 0',
                                                    listeners: {
                                                        expand: function (field) {
                                                            field.getPicker().refresh();
                                                        },
                                                        change: function(c) {
                                                            var tooltip = new Ext.ToolTip({
                                                                target: c.getEl(),
                                                                html: c.getRawValue()
                                                            });
                                                            return tooltip;
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Количество операций',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                labelAlign: 'left',
                                labelWidth: 100
                            },
                            items: [
                                {
                                    name: 'countOperationsAll',
                                    reference: 'countOperationsAll',
                                    xtype: 'label',
                                    padding: '0 5 5 0',
                                    bind: {
                                        text: 'Всего: {allCount}'
                                    }
                                },
                                {
                                    name: 'countOperationsFound',
                                    reference: 'countOperationsFound',
                                    xtype: 'label',
                                    padding: '0 5 5 0',
                                    bind: {
                                        text: 'Выявленных: {filteredCount}'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    align: 'right',
                                    margin: '0 0 7 0',
                                    text: 'Подсчитать',
                                    handler: 'countFilterOperSearchToggle'
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'buttongroup',
            layout: {
                type: 'hbox',
                align: 'right',
                pack: 'right'
            },
            defaults: {
                margin: '1px 3px 1px 3px'
            },
            items: [
                {
                    xtype: 'button',
                    reference: 'subjectToControlBtn',
                    text: 'Подлежит контролю по коду',
                    tooltip: 'Подлежит контролю по коду',
                    bind: {
                        disabled: '{!selectedOSAlert}',
                        hidden: '{Trxns_READONLY}'
                    },
                    margin: '1px 3px 1px 1px',
                    iconCls: 'icon-document-exclamation',
                    handler: 'subjectToControlActionOperSearch'
                },
                {
                    xtype: 'button',
                    text: 'Указать филиалы КГРКО',
                    tooltip: 'Указать филиалы КГРКО',
                    bind: {
                        disabled: '{!selectedOSAlert}',
                        hidden: '{Trxns_READONLY}'
                    },
                    iconCls: 'icon-border-color',
                    margin: '1px 1px 1px 3px',
                    handler: 'setFilialsKGRKO'
                },
                {
                    xtype: 'tbfill'
                },
                {
                    xtype: 'button',
                    text: 'Показать',
                    tooltip: 'Показать (Alt + т)',
                    margin: '1px 1px 1px 3px',
                    iconCls: 'icon-document-search',
                    handler: 'showSearchOperList'
                },
                {
                    xtype: 'button',
                    text: '>>',
                    tooltip: 'Загрузить следующую страницу',
                    margin: '1px 1px 1px 3px',
                    reference: 'readNextListPageBtn',
                    handler: 'readNextListPageOperSearch'
                },
                {
                    xtype: 'button',
                    text: '>>|',
                    tooltip: 'Загрузить оставшиеся страницы',
                    margin: '1px 1px 1px 3px',
                    reference: 'readLastListPageBtn',
                    handler: 'readLastListPageOperSearch'
                },
                {
                    xtype: 'button',
                    text: 'Детали',
                    enableToggle: true,
                    reference: 'toggleBBarOperSearchBtn',
                    bind: {
                        disabled: '{!selectedOSAlert}'
                    },
                    listeners: {
                        toggle: 'toggleBBarOperSearch'
                    }
                }
            ]
        }
    ]
});