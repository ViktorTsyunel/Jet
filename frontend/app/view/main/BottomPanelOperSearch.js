Ext.define('AML.view.main.BottomPanelOperSearch', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-view-main-bottompanelopersearch',

    items: [
        {
            width: '100%',
            layout: 'hbox',
            defaults: {
                border: 0,
                padding: 3
            },
            bind: {
                hidden: '{!footerRecord.EXPLANATION_TX}'
            },
            items: [
                {
                    layout:'hbox',
                    minWidth: 200,
                    flex: 0.15,
                    items: [
                        {
                            xtype: 'container',
                            html: 'Причина выявления:&nbsp&nbsp'
                        },
                        {
                            xtype: 'button',
                            text: '',
                            iconCls: 'icon-available',
                            margin: '0 5 5 0',
                            style:'background:none;',
                            handler: 'showAlertReasons'
                        }
                    ]
                },
                {
                    flex: 0.85,
                    bind: {
                        html: '{footerRecord.EXPLANATION_TX}'
                    }
                }
            ]
        },
        {
            width: '100%',
            itemId: 'bottomLinkOSItem',
            html: '<div class="emptyContainer"><div class="emptyText">Нет данных для отображения</div></div>',
            bind: {
                html: '<table class="table">' +
                '	<tr>' +
                '		<td class="hr" colspan="4">' +
                '			<hr>' +
                '		</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Назначение платежа:</td>' +
                '		<td class="valueCol3" colspan="3">{coloredTRXN_DESC}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Вид операции (ЦАС НСИ):</td>' +
                '		<td class="valueCol3" colspan="3">{footerRecord.NSI_OP}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="hr" colspan="4">' +
                '			<hr>' +
                '		</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="labelCol2" colspan="2">Плательщик</td>' +
                '		<td class="labelCol2" colspan="2">Получатель</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Наименование:</td>' +
                '		<td class="value">{coloredORIG_NM}</td>' +
                '		<td class="label">Наименование:</td>' +
                '		<td class="value">{coloredBENEF_NM}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">ИНН:</td>' +
                '		<td class="value">{footerRecord.ORIG_INN_NB}</td>' +
                '		<td class="label">ИНН:</td>' +
                '		<td class="value">{footerRecord.BENEF_INN_NB}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Счет:</td>' +
                '		<td class="value">{footerRecord.ORIG_ACCT_NB}</td>' +
                '		<td class="label">Счет:</td>' +
                '		<td class="value">{footerRecord.BENEF_ACCT_NB}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Банк:</td>' +
                '		<td class="value">{footerRecord.SEND_INSTN_NM}</td>' +
                '		<td class="label">Банк:</td>' +
                '		<td class="value">{footerRecord.RCV_INSTN_NM}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Корр. счет:</td>' +
                '		<td class="value">{footerRecord.SEND_INSTN_ACCT_ID}</td>' +
                '		<td class="label">Корр. счет:</td>' +
                '		<td class="value">{footerRecord.RCV_INSTN_ACCT_ID}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Счет дебета:</td>' +
                '		<td class="value">{footerRecord.DEBIT_CD}</td>' +
                '		<td class="label">Счет кредита:</td>' +
                '		<td class="value">{footerRecord.CREDIT_CD}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Адрес:</td>' +
                '		<td class="value">{footerRecord.ORIG_ADDR_TX}</td>' +
                '		<td class="label">Адрес:</td>' +
                '		<td class="value">{footerRecord.BENEF_ADDR_TX}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Удостоверяющий документ:</td>' +
                '		<td class="value">{footerRecord.ORIG_IDDOC_TX}</td>' +
                '		<td class="label">Удостоверяющий документ:</td>' +
                '		<td class="value">{footerRecord.BENEF_IDDOC_TX}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Дата регистрации:</td>' +
                '		<td class="value">{footerRecord.ORIG_REG_DT}</td>' +
                '		<td class="label">Дата регистрации:</td>' +
                '		<td class="value">{footerRecord.BENEF_REG_DT}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label"><div><div style="display:inline;">Вид операции ГОЗ&nbsp</div><div style="display:inline-block; min-width:15px; height:15px;" class="icon-goz"><a id="payerGOZHistory" href="#">&nbsp&nbsp</a></div></div></td>' +
                '		<td class="value">{footerRecord.ORIG_GOZ_OP_CD}</td>' +
                '		<td class="label"><div><div style="display:inline;">Вид операции ГОЗ&nbsp</div><div style="display:inline-block; min-width:15px; height:15px;" class="icon-goz"><a id="payerGOZHistory" href="#">&nbsp&nbsp</a></div></div></td>' +
                '		<td class="value">{footerRecord.BENEF_GOZ_OP_CD}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Филиал КГРКО:</td>' +
                '		<td class="value">{footerRecord.SEND_KGRKO_ID} {footerRecord.SEND_KGRKO_NM}</td>' +
                '		<td class="label">Филиал КГРКО:</td>' +
                '		<td class="value">{footerRecord.RCV_KGRKO_ID} {footerRecord.RCV_KGRKO_NM}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="hr" colspan="4">' +
                '			<hr>' +
                '		</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Тербанк:</td>' +
                '		<td class="value">{footerRecord.TB_ID} {footerRecord.TB_NM}</td>' +
                '		<td class="label">Отделение:</td>' +
                '		<td class="value">{footerRecord.OSB_ID} {footerRecord.OSB_NM}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Филиал КГРКО сообщения:</td>' +
                '		<td class="value">{footerRecord.RV_KGRKO_ID} {footerRecord.RV_KGRKO_NM}</td>' +
                '		<td class="label">Операция совершена:</td>' +
                '		<td class="value">{footerRecord.TRXN_ORG_DESC}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Платежный документ:</td>' +
                '		<td class="value">{footerRecord.TRXN_DOC}</td>' +
                '		<td class="label">Система-источник:</td>' +
                '		<td class="value">{footerRecord.SRC_SYS_CD}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="hr" colspan="4">' +
                '			<hr>' +
                '		</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Несколько сообщений:</td>' +
                '		<td class="valueCol3" colspan="3">{footerRecord.RV_SCRTY_DESC}</td>' +
                '	</tr>' +
                '	<tr>' +
                '		<td class="label">Изменения в операции:</td>' +
                '		<td class="value">{footerRecord.TRXN_CHANGE}</td>' +
                '		<td class="label">Изменения в выявлении:</td>' +
                '		<td class="value">{footerRecord.OPOK_CHANGE}</td>' +
                '	</tr>' +
                '</table>'
            },
            listeners: {
                click: {
                    element: 'el',
                    preventDefault: true,
                    fn: function (e, target) {
                        var el = Ext.fly(target),
                            componentView = Ext.ComponentQuery.query('#bottomLinkOSItem')[0],
                            mainComponent = (componentView) ? componentView.up('main-search-operations-form') : null,
                            mainViewModel = (mainComponent) ? mainComponent.getViewModel() : null,
                            opCatCd = (mainViewModel) ? mainViewModel.get('footerRecord.RV_OP_CAT_CD') : null,
                            trxnId = (mainViewModel) ? mainViewModel.get('footerRecord.RV_TRXN_SEQ_ID') : null,
                            mainController = (mainComponent) ? mainComponent.controller : null;
                        if (el.dom.id === "reciverGOZHistory" || el.dom.id === "payerGOZHistory") {
                            if(mainController && opCatCd && trxnId) {
                                mainController.openGOZChanges(opCatCd, trxnId);
                            }
                        }
                    }
                }
            }
        }
    ]
});