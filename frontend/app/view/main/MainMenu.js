Ext.define('AML.view.main.MainMenu', {
	extend: 'Ext.toolbar.Toolbar',
	requires: [
		'AML.view.main.MainMenuController',
		'AML.view.main.SearchOperations',
		'AML.view.main.SearchOperationsController',
		'AML.view.main.SearchOperationsTopbar',
		'AML.view.main.SearchOperationsModel',
		'AML.view.main.BottomPanelOperSearch',
		'Ext.menu.Separator',
		'AML.view.oes.OES321TypeOrg.List',
		'AML.view.oes.OES321ListsOrg.List'
	],
	xtype: 'app-main-menu',
	controller: 'mainmenu',
	border: false,
	padding: '3 5 3 5',
	items: [
		{
			text: 'Главная',
			hidden: true
		},
		{
			text: 'Мониторинг',
			iconCls: 'icon-radar',
			menu: {
				items: [
					{
						text: 'Список выявленных операций',
						iconCls: 'icon-bell',
						isPage: true,
						bind: {
							hidden: '{!menu.Alerts}'
						},
						pageName: 'app-monitoring-alerts'
					},
					{
						text: 'Поиск операций',
						iconCls: 'icon-document-search',
						isPage: true,
						hidden: true,
						bind: {
							hidden: '{!menu.Trxns}'
						},
						pageName: 'main-search-operations-form'
					},
					{
						text: 'Создание ОЭС',
						itemId: 'creationOESMainB',
						iconCls: 'icon-document-plus',
						hidden: true,
						isWindow: true,
						bind: {
							hidden: '{!menu.CreateOES}'
						},
						// windowName: 'app-monitoring-create-createalertform'
						handler: 'checkAndCreateNewOES'
					},
					{
						text: 'Отчеты',
						id: 'reportsMainB',
						iconCls: 'icon-report',
						hidden: true,
						isWindow: true,
						menu: {
							defaults: {
								iconCls: 'icon-report',
								isMenuButton: true
							},
							id: 'menuDynCreate'
						},
						listeners: {
							beforerender: function() {
								var store = Ext.create('Ext.data.Store', {
									model: 'AML.model.Reports',
									autoLoad: true,
									listeners: {
										load: function(store,records,success,operation,opts) {
											var toolbar = Ext.getCmp('menuDynCreate');
											var mainMenu = Ext.getCmp('reportsMainB');

											if (records.length !== 0) {
												mainMenu.show();
											}
											var menu = Ext.create('Ext.menu.Menu');
											store.each(function(record) {
												menu.add({
													xtype: 'button',
													text: record.data.REP_NM,
													hidden: false,
													value: record.data.REP_ID
												});
												toolbar.add({
													text: record.data.REP_NM,
													value: record.data.REP_ID,
													listeners: {
														click: function (field) {
															var urlVal = window.location.protocol + '//' + window.location.host + common.globalinit.ajaxUrlReports + '?rep_id=' + field.getValue(),
																win = window.open(urlVal, '_blank');
															if (win) {
																common.globalinit.pagesHaveToClose.push(win);
																win.focus();
															}
														}
													}
												});
											});

										}
									}
								});
							}
						}
					}
				]
			}
		},
		{
			text: 'Администрирование',
			itemId: 'Admin',
			hidden: true,
			bind: {
				hidden: '{!AdminMenu}'
			},
			iconCls: 'icon-gear',
			menu: {
				items: [
					{
						text: 'Пользователи',
						hidden: true,
						iconCls: 'icon-users'
					},
					{
						text: 'Обязательный контроль',
						hidden: true,
						bind: {
							hidden: '{!OPOKControlMenu}'
						},
						iconCls: 'icon-reports',
						menu: {
							defaults: {
								iconCls: 'icon-report',
								isMenuButton: true
							},
							items: [
								{
									text: 'Коды ОПОК',
									hidden: true,
									bind: {
										hidden: '{!menu.OPOK}'
									},
									isPage: true,
									pageName: 'app-settings-opok'
								},
								{
									text: 'Стандартные условия выявления',
									hidden: true,
									bind: {
										hidden: '{!menu.OPOKCriteria}'
									},
									isPage: true,
									pageName: 'app-settings-opokcriteria'
								},
								{
									text: 'Правила выявления',
									hidden: true,
									bind: {
										hidden: '{!menu.OPOKRule}'
									},
									isPage: true,
									pageName: 'app-settings-opokrule'
								},
								{
									xtype: 'menuseparator',
									itemId: 'OPOKTestSeparator',
									hidden: true,
									bind: {
										hidden: '{!menu.OPOKTest}'
									}
								},
								{
									text: 'Тестирование правил выявления',
									hidden: true,
									bind: {
										hidden: '{!menu.OPOKTest}'
									},
									iconCls: 'icon-report-arrow',
									isWidget: true,
									widgetName: 'app-settings-opoktest'
								}
							]
						}
					},
					{
						text: 'Назначение ответственных',
						hidden: true,
						bind: {
							hidden: '{!AssignMenu}'
						},
						iconCls: 'icon-reports',
						menu: {
							defaults: {
								iconCls: 'icon-report',
								isMenuButton: true
							},
							items: [
								{
									text: 'Правила назначения ответственных',
									hidden: true,
									bind: {
										hidden: '{!menu.AssignRule}'
									},
									isPage: true,
									pageName: 'app-assign-assignrule'
								},
								{
									text: 'Замещения ответственных',
									hidden: true,
									bind: {
										hidden: '{!menu.AssignReplacement}'
									},
									isPage: true,
									pageName: 'app-assign-assignreplacement'
								},
								{
									text: 'Лимиты назначения',
									hidden: true,
									bind: {
										hidden: '{!menu.AssignLimit}'
									},
									isPage: true,
									pageName: 'app-assign-assignlimit'
								}
							]
						}
					},
					{
						text: 'ОЭС',
						hidden: true,
						bind: {
							hidden: '{!OESMenu}'
						},
						iconCls: 'icon-reports',
						menu: {
							defaults: {
								iconCls: 'icon-report',
								isMenuButton: true
							},
							items: [
								{
									text: 'Информация о КО',
									hidden: true,
									bind: {
										hidden: '{!menu.OES321Org}'
									},
									isPage: true,
									pageName: 'app-oes-oes321org'
								},
								{
									text: 'КО участники операций',
									hidden: true,
									bind: {
										hidden: '{!menu.OES321PartyOrg}'
									},
									isPage: true,
									pageName: 'app-oes-oes321partyorg'
								},
								{
									text: 'Правила проверки',
									hidden: true,
									bind: {
										hidden: '{!menu.OESCheckRule}'
									},
									isPage: true,
									pageName: 'app-oes-oescheckrule'
								}
							]
						}
					},
					{
						text: 'Перечни лиц и организаций',
						hidden: true,
						bind: {
							hidden: '{!WLMenu}'
						},
						iconCls: 'icon-reports',
						menu: {
							defaults: {
								iconCls: 'icon-report',
								isMenuButton: true
							},
							items: [
								{
									text: 'Виды перечней',
									hidden: true,
									bind: {
										hidden: '{!menu.WLType}'
									},
									isPage: true,
									pageName: 'app-oes-oes321typeorg'
								},
								{
									text: 'Перечни организаций',
									hidden: true,
									bind: {
										hidden: '{!menu.WLOrg}'
									},
									isPage: true,
									pageName: 'app-oes-oes321listsorg'
								}
							]
						}
					},
					{
						text: 'Импорт данных',
                        hidden: true,
                        bind: {
                            hidden: '{!menu.ImpType}'
                        },
                        iconCls: 'icon-reports',
                        isPage: true,
                        pageName: 'app-typeimport-list'
					}
				]
			}
		},
		{
			text: 'О системе',
			iconCls: 'icon-exclamation',
			isWindow: true,
			windowName: 'app-main-about'
		},
		'->',
		{
			text: 'Помощь',
			iconCls: 'icon-question'
		}
	]
});
