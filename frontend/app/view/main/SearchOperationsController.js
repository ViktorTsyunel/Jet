Ext.define('AML.view.main.SearchOperationsController', {
    // extend: 'AML.view.main.BaseFormController', // BaseListController
    extend: 'Ext.app.ViewController',
    alias: 'controller.mainsearchoperations',
    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                'main-search-operations-form': {
                    /**
                     * Обрабатываем событие отрисовки
                     * @param panel
                     */
                    afterrender: function (panel) {
                        var self = this;

                        Ext.getBody().addKeyMap({
                            eventName: 'keydown',
                            binding: [
                                {
                                    // Вешаем hotkey ESC для закрытия подвала
                                    key: Ext.event.Event.ESC,
                                    ctrl: false,
                                    shift: false,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        toggleBBarBtn.setPressed(false);
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + 1 на вкладку "Сведения об операции"
                                    key: '1',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        //'oes321detailspanel-operationtab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(0);
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + 2 на вкладку "Плательщик"
                                    key: '2',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-payertab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(1);
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + 3 на вкладку "Получатель"
                                    key: '3',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-recipienttab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(2);
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + 4 на вкладку "От имени и по поруч."
                                    key: '4',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-behalftab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(3);

                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + 5 на вкладку "Предст. плат-ка"
                                    key: '5',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        //'oes321detailspanel-representativetab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(4);
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + 6 на вкладку "Предст. Получ-ля"
                                    key: '6',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                            return false;
                                        }
                                        // 'oes321detailspanel-destinationtab'
                                        self.getView().down('app-alerts-oes321detailspanel').down('tabpanel').setActiveTab(5);
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + В на кнопку "Вложения"
                                    key: 'd',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            // if (_vmpanel.get('detailsTable.ALLOW_ALERT_EDITING')) {
                                            _panel.controller.editAttachAction();
                                            // }
                                            return;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel
                                            && _panel.controller
                                            && !_panel.getViewModel().get('SEARCH_OPERATION_VIEW')) {
                                            _panel.controller.editAttachAction();
                                        }

                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + З на кнопку "Закрыть (детали)"
                                    key: 'p',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        if (self.getView().down('app-alerts-oes321detailspanel')) {
                                            self.getView().down('app-alerts-oes321detailspanel').controller.closePanel();
                                            return;
                                        }
                                        if (self.getView().down('app-alerts-alertdetailspanel')) {
                                            self.getView().down('app-alerts-alertdetailspanel').controller.closePanel();
                                            return;
                                        }
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + M на кнопку "Заметки (детали операции)"
                                    key: 'v',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            // if (_vmpanel.get('detailsTable.ALLOW_ALERT_EDITING')) {
                                            _panel.controller.editNoteAction();
                                            // }
                                            return;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel
                                            && _panel.controller
                                            && !_panel.getViewModel().get('SEARCH_OPERATION_VIEW')) {
                                            _panel.controller.editNoteAction();
                                        }
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + С на кнопку "История изменений"
                                    key: 'c',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        var _panel, _vmpanel;
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            // if (_vmpanel.get('detailsTable.ALLOW_ALERT_EDITING')) {
                                            _panel.controller.showHistoryAction();
                                            // }
                                            return;
                                        }
                                        _panel = self.getView().down('app-alerts-alertdetailspanel');
                                        if (_panel
                                            && _panel.controller
                                            && !_panel.getViewModel().get('SEARCH_OPERATION_VIEW')) {
                                            _panel.controller.showHistoryAction();
                                        }
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + Е на кнопку "История изменений ОЭС"
                                    key: 't',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function (keyCode, e) {
                                        if (panel.isHidden()) {
                                            return false;
                                        }
                                        e.stopEvent();
                                        var _panel, _vmpanel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-alertdetailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        _vmpanel = (_panel) ? _panel.getViewModel() : false;
                                        if (_panel && _vmpanel) {
                                            // if (_vmpanel.get('detailsTable.ALLOW_ALERT_EDITING')) {
                                            _panel.controller.showHistoryLogAction();
                                            // }
                                        }
                                    }
                                },
                                {
                                    // Вешаем hotkey Alt + Р на кнопку "Результат контроля"
                                    key: 'h',
                                    ctrl: false,
                                    shift: false,
                                    alt: true,
                                    fn: function () {
                                        var _panel;
                                        if (self.getView()
                                            && self.getView().down('app-alerts-alertdetailspanel')) {
                                            return false;
                                        }
                                        _panel = self.getView().down('app-alerts-oes321detailspanel');
                                        if (_panel) {
                                            _panel.controller.openControlResult();
                                        }
                                    }
                                }
                            ]
                        });
                    }
                },
                'gridpanel': {
                    /**
                     * Обрабатываем событие после отрисовки сетки
                     * @param grid
                     */
                    afterrender: function (grid) {
                        var self = this,
                            toggleBBarBtn = self.lookupReference('toggleBBarOperSearchBtn'),
                            monitoringAlertsTopbar = self.lookupReference('main-search-operations-topbar'),
                            vm = this.getViewModel();
                        // добавляем пункты в контекстное меню:
                        grid.showSearchAlertBtn = Ext.create('Ext.Action', {
                            iconCls: 'alert-details-button',
                            text: 'Детали загруженной операции',
                            viewModel: {
                                parent: vm
                            },
                            disabled: false,
                            handler: function (widget, event) {
                                // если подвал открыт запоминаем активную запись
                                if (toggleBBarBtn.pressed) {
                                    monitoringAlertsTopbar.reestablishView = true;
                                }
                                // закрываем нижную панель детали операции:
                                toggleBBarBtn.setPressed(false);

                                var prefSelectedRecord = vm.get('selectedOSAlert.OP_CAT_CD');
                                var codeSelectedRecord = vm.get('selectedOSAlert.TRXN_SEQ_ID');
                                if (!prefSelectedRecord || !codeSelectedRecord) {
                                    Ext.Msg.alert('', 'Выбранная операция содержит некорректный код!');
                                }
                                // mainController.detailsTRXNbyTRXN(prefSelectedRecord, codeSelectedRecord, true);
                                self.getView().add({
                                    xtype: 'app-alerts-alertdetailspanel',
                                    viewModel: {
                                        parent: vm,
                                        data: {
                                            alertId: vm.get('selectedOSAlert.REVIEW_ID'),
                                            SEARCH_OPERATION_VIEW: true,
                                            prefSelectedRecord: prefSelectedRecord,
                                            codeSelectedRecord: codeSelectedRecord
                                        }
                                    }
                                });
                            }
                        });
                        grid.showAlertDetailsBtn = Ext.create('Ext.Action', {
                            iconCls: 'alert-details-button',
                            text: 'Детали выявленной операции',
                            viewModel: {
                                parent: vm
                            },
                            disabled: false,
                            bind: {
                                disabled: {
                                    bindTo: '{!selectedOSAlert.REVIEW_ID}',
                                    deep: true
                                }
                            },
                            handler: function (widget, event) {
                                // если подвал открыт запоминаем активную запись
                                if (toggleBBarBtn.pressed) {
                                    monitoringAlertsTopbar.reestablishView = true;
                                }
                                // закрываем нижную панель детали операции:
                                toggleBBarBtn.setPressed(false);

                                // mainController.detailsTRXNbyALERTID(vm.get('selectedOSAlert.REVIEW_ID'), false);
                                self.getView().add({
                                    xtype: 'app-alerts-alertdetailspanel',
                                    viewModel: {
                                        parent: vm,
                                        data: {
                                            alertId: vm.get('selectedOSAlert.REVIEW_ID'),
                                            SEARCH_OPERATION_VIEW: false
                                        }
                                    }
                                });
                            }
                        });
                        grid.showOES321DetailsBtn = Ext.create('Ext.Action', {
                            iconCls: 'oes321-details-button',
                            text: 'Детали сообщения',
                            viewModel: {
                                parent: vm
                            },
                            bind: {
                                disabled: '{!selectedOSAlert.OES_321_ID}'
                            },
                            handler: function (widget, event) {
                                // если подвал открыт запоминаем активную запись
                                if (toggleBBarBtn.pressed) {
                                    monitoringAlertsTopbar.reestablishView = true;
                                }
                                // закрываем нижную панель детали операции:
                                toggleBBarBtn.setPressed(false);

                                // mainController.detailsOES321(vm.get('selectedOSAlert.REVIEW_ID'), vm.get('selectedOSAlert.OES_321_ID'), true);
                                self.getView().add({
                                    xtype: 'app-alerts-oes321detailspanel',
                                    viewModel: {
                                        parent: vm,
                                        type: 'oes321detailspanelviewmodel',
                                        data: {
                                            alertId: vm.get('selectedOSAlert.REVIEW_ID'),
                                            oes321Id: vm.get('selectedOSAlert.OES_321_ID'),
                                            SEARCH_OPERATION_VIEW: true
                                        }
                                    }
                                });
                            }
                        });
                        grid.contextMenu = Ext.create('Ext.menu.Menu', {
                            items: [
                                grid.showSearchAlertBtn,
                                grid.showAlertDetailsBtn,
                                grid.showOES321DetailsBtn
                            ]
                        });
                        // устанавливаем фокус на грид:
                        grid.getView().focus();
                    },
                    /**
                     * Отображаем кастомное контекстное меню
                     * @param view
                     * @param rec
                     * @param node
                     * @param index
                     * @param e
                     * @returns {boolean}
                     */
                    itemcontextmenu: function (view, rec, node, index, e) {
                        e.stopEvent();
                        if (view.panel && view.panel.contextMenu) {
                            if (!view.up('app-alerts-alertdetailspanel')) {
                                view.panel.contextMenu.showAt(e.getXY());
                            }
                        }
                        return false;
                    },
                    /**
                     * Обрабатываем событие нажатия кнопок клавиатуры
                     * @param table
                     * @param record
                     * @param tr
                     * @param rowIndex
                     * @param e
                     * @param eOpts
                     */
                    rowkeydown: function (table, record, tr, rowIndex, e, eOpts) {
                        var self = this,
                            vm = this.getViewModel(),
                            toggleBBarBtn = self.lookupReference('toggleBBarOperSearchBtn'),
                            monitoringAlertsTopbar = self.lookupReference('main-search-operations-topbar');

                        switch (e.getKey()) {
                            case e.SPACE:
                                // ставим флаг открытия футера
                                if (toggleBBarBtn.pressed) {
                                    monitoringAlertsTopbar.reestablishView = true;
                                }
                                // закрываем нижную панель детали операции:
                                toggleBBarBtn.setPressed(false);
                                // открываем правую панель ОЭС - Редактирование - Сведения об операции:
                                if (!Ext.isEmpty(record.getData().OES_321_ID)) {
                                    if (!self.getView().down('app-alerts-oes321detailspanel')) {
                                        self.getView().add({
                                            xtype: 'app-alerts-oes321detailspanel',
                                            viewModel: {
                                                parent: vm,
                                                type: 'oes321detailspanelviewmodel',
                                                data: {
                                                    alertId: vm.get('selectedOSAlert.REVIEW_ID'),
                                                    oes321Id: vm.get('selectedOSAlert.OES_321_ID'),
                                                    SEARCH_OPERATION_VIEW: true
                                                }
                                            }
                                        });
                                    }
                                } else if (!Ext.isEmpty(record.getData().REVIEW_ID)) {
                                    if (!self.getView().down('app-alerts-alertdetailspanel')) {
                                        // открываем правую панель детали операции:
                                        self.getView().add({
                                            xtype: 'app-alerts-alertdetailspanel',
                                            viewModel: {
                                                parent: vm,
                                                data: {
                                                    alertId: vm.get('selectedOSAlert.REVIEW_ID'),
                                                    SEARCH_OPERATION_VIEW: false
                                                }
                                            }
                                        });
                                    }
                                } else {
                                    if (!self.getView().down('app-alerts-alertdetailspanel')) {
                                        // открываем правую панель детали операции:
                                        self.getView().add({
                                            xtype: 'app-alerts-alertdetailspanel',
                                            viewModel: {
                                                parent: vm,
                                                data: {
                                                    alertId: vm.get('selectedOSAlert.REVIEW_ID'),
                                                    SEARCH_OPERATION_VIEW: true,
                                                    prefSelectedRecord: vm.get('selectedOSAlert.OP_CAT_CD'),
                                                    codeSelectedRecord: vm.get('selectedOSAlert.TRXN_SEQ_ID')
                                                }
                                            }
                                        });
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    },

    init: function () {
        var vm = this.getViewModel();
        // vm.bind('{search}', function () {
        //     vm.set('filteredCount', '');
        // }, this, {deep: true});
        vm.bind('{record}', function () {
            var count = vm.get('allCount');
            if (count || count === 0) {
                vm.set('allCount', '');
                vm.set('filteredCount', '');
            }
        }, this, {deep: true});
    },

    // ==================================================================================
    editCancelAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.editAction(true);
    },

    editAction: function (isCancelAction) {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        var self = this,
            vm = self.getViewModel(),
            fields = {
                payerINNOperSearch: self.lookupReference('payerINNOperSearch'),
                payerINNOperSearchAdd: self.lookupReference('payerINNOperSearchAdd'),
                dtFromOperSearch: self.lookupReference('dtFromOperSearch'),
                dtToOperSearch: self.lookupReference('dtToOperSearch'),
                amountFromOperSearch: self.lookupReference('amountFromOperSearch'),
                amountToOperSearch: self.lookupReference('amountToOperSearch'),
                standartConditionOperSearch: self.lookupReference('standartConditionOperSearch'),
                conditionOperSearch: self.lookupReference('conditionOperSearch'),
                tbIdsOperSearch: self.lookupReference('tbIdsOperSearch'),
                osbIdsOperSearch: self.lookupReference('osbIdsOperSearch'),
                notCodeOperSearch: self.lookupReference('notCodeOperSearch'),
                categoryOperSearch: self.lookupReference('categoryOperSearch'),
                systemOperSearch: self.lookupReference('systemOperSearch'),
                payerNameOperSearch: self.lookupReference('payerNameOperSearch'),
                payerCountryOperSearch: self.lookupReference('payerCountryOperSearch'),
                payerNameOperSearchAdd: self.lookupReference('payerNameOperSearchAdd'),
                payerCountryOperSearchAdd: self.lookupReference('payerCountryOperSearchAdd'),
                payerStandartConditions: self.lookupReference('payerStandartConditions'),
                payerAccountNumber: self.lookupReference('payerAccountNumber'),
                payerConditions: self.lookupReference('payerConditions'),
                payerRegionSearch: self.lookupReference('payerRegionSearch'),
                payerStandartConditionsAdd: self.lookupReference('payerStandartConditionsAdd'),
                payerAccountNumberAdd: self.lookupReference('payerAccountNumberAdd'),
                payerConditionsAdd: self.lookupReference('payerConditionsAdd'),
                payerRegionSearchAdd: self.lookupReference('payerRegionSearchAdd'),
                checkPayerCondition: self.lookupReference('checkPayerCondition'),
                scopeFilters: vm.get('scopeFilters')
            },
            searchRaw = {
                payerINNOperSearch: '',
                payerINNOperSearchAdd: '',
                dtFromOperSearch: '',
                dtToOperSearch: '',
                amountFromOperSearch: '',
                amountToOperSearch: '',
                standartConditionOperSearch: '',
                conditionOperSearch: '',
                tbIdsOperSearch: '',
                osbIdsOperSearch: '',
                notCodeOperSearch: '',
                categoryOperSearch: '',
                systemOperSearch: '',
                payerNameOperSearch: '',
                payerCountryOperSearch: '',
                payerNameOperSearchAdd: '',
                payerCountryOperSearchAdd: '',
                payerStandartConditions: '',
                payerAccountNumber: '',
                payerConditions: '',
                payerRegionSearch: '',
                payerStandartConditionsAdd: '',
                payerAccountNumberAdd: '',
                payerConditionsAdd: '',
                payerRegionSearchAdd: '',
                checkPayerCondition: ''
            },
            applyFiltersBtn = self.lookupReference('toggleSearchOperFiltersBtn'),
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel,
            search = {};

        if (applyFiltersBtn.pressed) {
            search = {
                dtFrom: fields.dtFromOperSearch.getValue(),
                dtTo: fields.dtToOperSearch.getValue(),
                tbIds: fields.tbIdsOperSearch.getValue(),
                osbIds: fields.osbIdsOperSearch.getValue(),
                amountFrom: parseFloat(fields.amountFromOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountToOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                lexPurposeCrit: fields.standartConditionOperSearch.getValue(),
                lexPurposeAdd: fields.conditionOperSearch.getValue(),
                notOpok: fields.notCodeOperSearch.getValue(),
                opCat: fields.categoryOperSearch.getValue(),
                srcSys: fields.systemOperSearch.getValue(),
                partyType: fields.checkPayerCondition,
                custNm: fields.payerNameOperSearch.getValue(),
                custInn: fields.payerINNOperSearch.getValue(),
                custResident: fields.payerCountryOperSearch.getValue(),
                acctCrit: fields.payerStandartConditions.getValue(),
                acctAdd: fields.payerConditions.getValue(),
                acctNb: fields.payerAccountNumber.getValue(),
                acctArea: fields.payerRegionSearch.getValue(),
                cust2Nm: fields.payerNameOperSearchAdd.getValue(),
                cust2Inn: fields.payerINNOperSearchAdd.getValue(),
                cust2Resident: fields.payerCountryOperSearchAdd.getValue(),
                acct2Crit: fields.payerStandartConditionsAdd.getValue(),
                acct2Add: fields.payerConditionsAdd.getValue(),
                acct2Nb: fields.payerAccountNumberAdd.getValue(),
                acct2Area: fields.payerRegionSearchAdd.getValue(),
                custWatchList: vm.get('record.oes321TypeSender'),
                cust2WatchList: vm.get('record.oes321TypeTaker'),
                custNotWatchListFl: +vm.get('record.excludeSenderInn'),
                cust2NotWatchListFl: +vm.get('record.excludeTakerInn')
            };

            for (var key in search) {
                if (Ext.isEmpty(search[key])) {
                    delete search[key];
                }
            }
        }

        // searchRaw.scopeFilters = fields.scopeFilters.getRawValue() ? ('"' + fields.scopeFilters.getRawValue() + '"; ') : '';
        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }

        Ext.widget({
            xtype: 'app-alerts-alertactionform-form',
            viewModel: {
                type: 'alertactionformviewmodel',
                parent: vm,
                data: {
                    allAlertsCnt: self.getStore('alertsStore').getTotalCount(),
                    selectedAlertsCnt: self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelected().length ? self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelected().length : +(alertDetailsModel.getData().REVIEW_ID !== null),
                    selectedAlerts: self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection() : [alertDetailsModel],
                    isCancel: (typeof isCancelAction === 'boolean' && isCancelAction),
                    search: search,
                    // scopeFilters: fields.scopeFilters.getValue(),
                    // searchRaw: searchRaw,
                    isAlertDetailsPanel: ((alertDetailsPanel && alertDetailsPanel.getViewModel().get('alertId') !== null) || (oes321DetailsPanel && oes321DetailsPanel.getViewModel().get('alertId') !== null)) // если открыта детальная панель операции
                }
            }
        }).show();
    },

    attachAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        var self = this,
            vm = self.getViewModel(),
            selectedAlertsCnt = parseInt(self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelected().length),
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel;

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
            selectedAlertsCnt = 1;
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
            selectedAlertsCnt = 1;
        }

        switch (true) {
            case selectedAlertsCnt === 0:
                Ext.Msg.alert('', 'Нет выделенной операции');
                break;
            case selectedAlertsCnt > 1:
                Ext.Msg.alert('', 'Выделено более одной операции');
                break;
            case selectedAlertsCnt === 1:
                Ext.widget({
                    xtype: 'app-alerts-alertaddattachment-form',
                    viewModel: {
                        type: 'alertaddattachmentformviewmodel',
                        parent: this.getViewModel(),
                        //parent: vm,
                        data: {
                            selectedAlerts: self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection() : [alertDetailsModel]
                        }
                    }
                }).show();
                break;
        }
    },

    checkIfGridSelected: function () {
        var self = this,
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel,
            selectedAlertsCnt = self.getView().lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length;

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        if (alertDetailsModel.getData().REVIEW_ID === null && !selectedAlertsCnt) {
            Ext.Msg.alert('', 'Пожалуйста, выберите операцию из списка.');
            return false;
        }
        return true;
    },

    subjectToControlAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.execByIdAction(['RF_OES']);
    },

    cancelInvalidTokenAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.execByIdAction(['RF_CANCEL']);
    },

    exportAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.execByIdAction(['RF_EXPORT']);
    },

    toSendAction: function () {
        // если пользователь не выбрал операцию в списке выводим сообщение, отменяем действие:
        if (!this.checkIfGridSelected()) {
            return;
        }
        this.execByIdAction(['RF_READY']);
    },

    execByIdAction: function (actvyTypeCds) {
        var vm = this.getViewModel(),
            self = this,
            store = this.getStore('alertsStore'),
            fields = {
                payerINNOperSearch: self.lookupReference('payerINNOperSearch'),
                payerINNOperSearchAdd: self.lookupReference('payerINNOperSearchAdd'),
                dtFromOperSearch: self.lookupReference('dtFromOperSearch'),
                dtToOperSearch: self.lookupReference('dtToOperSearch'),
                amountFromOperSearch: self.lookupReference('amountFromOperSearch'),
                amountToOperSearch: self.lookupReference('amountToOperSearch'),
                standartConditionOperSearch: self.lookupReference('standartConditionOperSearch'),
                conditionOperSearch: self.lookupReference('conditionOperSearch'),
                tbIdsOperSearch: self.lookupReference('tbIdsOperSearch'),
                osbIdsOperSearch: self.lookupReference('osbIdsOperSearch'),
                notCodeOperSearch: self.lookupReference('notCodeOperSearch'),
                categoryOperSearch: self.lookupReference('categoryOperSearch'),
                systemOperSearch: self.lookupReference('systemOperSearch'),
                payerNameOperSearch: self.lookupReference('payerNameOperSearch'),
                payerCountryOperSearch: self.lookupReference('payerCountryOperSearch'),
                payerNameOperSearchAdd: self.lookupReference('payerNameOperSearchAdd'),
                payerCountryOperSearchAdd: self.lookupReference('payerCountryOperSearchAdd'),
                payerStandartConditions: self.lookupReference('payerStandartConditions'),
                payerAccountNumber: self.lookupReference('payerAccountNumber'),
                payerConditions: self.lookupReference('payerConditions'),
                payerRegionSearch: self.lookupReference('payerRegionSearch'),
                payerStandartConditionsAdd: self.lookupReference('payerStandartConditionsAdd'),
                payerAccountNumberAdd: self.lookupReference('payerAccountNumberAdd'),
                payerConditionsAdd: self.lookupReference('payerConditionsAdd'),
                payerRegionSearchAdd: self.lookupReference('payerRegionSearchAdd'),
                checkPayerCondition: self.lookupReference('checkPayerCondition'),
                scopeFilters: vm.get('scopeFilters')
            },
            search = {},
            applyFiltersBtn = self.lookupReference('toggleSearchOperFiltersBtn'),
            // alertDetailsPanel =  self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel,
            alertIds = [],
            alertId,
            nextAlertId = 0,
            selectedAlertIndex2 = 0,
            nextSelectedAlertIndex = 0,
            refleshFooter = false,
            params;

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        var selectedAlerts = self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection() : [alertDetailsModel];

        for (var i = 0; i < selectedAlerts.length; i++) {
            alertIds.push(selectedAlerts[i].get('REVIEW_ID'));
            if (actvyTypeCds[0] === 'RF_CANCEL' || actvyTypeCds[0] === 'RF_READY' || actvyTypeCds[0] === 'RF_EXPORT') {
                refleshFooter = true;
                alertId = selectedAlerts[i].get('REVIEW_ID');
                if (alertId !== null && alertId > 0) {
                    if (store.data.items.length > 0) {
                        Ext.Array.forEach(store.data.items, function (item, index) {
                            if (selectedAlertIndex2 === 1) {
                                nextAlertId = item.id;
                                // выставляем индекс следущей записи с учетом сдвижки
                                nextSelectedAlertIndex = index - 1;
                                selectedAlertIndex2 = 2;
                            }
                            if (item.id === alertId) {
                                selectedAlertIndex2 = 1;
                            }
                        })
                    }
                }
            }
        }

        params = {
            form: 'AlertAction',
            action: 'execById',
            actvyTypeCds: (actvyTypeCds && actvyTypeCds.shift) ? actvyTypeCds : '',
            alertIds: alertIds,
            cmmntId: (actvyTypeCds && actvyTypeCds.shift && Ext.Array.indexOf(actvyTypeCds, 'RF_CANCEL') !== -1) ? 101 : ''
        };

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }

        if (applyFiltersBtn.pressed) {
            search = {
                dtFrom: fields.dtFromOperSearch.getValue(),
                dtTo: fields.dtToOperSearch.getValue(),
                tbIds: fields.tbIdsOperSearch.getValue(),
                osbIds: fields.osbIdsOperSearch.getValue(),
                amountFrom: parseFloat(fields.amountFromOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountToOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                lexPurposeCrit: fields.standartConditionOperSearch.getValue(),
                lexPurposeAdd: fields.conditionOperSearch.getValue(),
                notOpok: fields.notCodeOperSearch.getValue(),
                opCat: fields.categoryOperSearch.getValue(),
                srcSys: fields.systemOperSearch.getValue(),
                partyType: fields.checkPayerCondition,
                custNm: fields.payerNameOperSearch.getValue(),
                custInn: fields.payerINNOperSearch.getValue(),
                custResident: fields.payerCountryOperSearch.getValue(),
                acctCrit: fields.payerStandartConditions.getValue(),
                acctAdd: fields.payerConditions.getValue(),
                acctNb: fields.payerAccountNumber.getValue(),
                acctArea: fields.payerRegionSearch.getValue(),
                cust2Nm: fields.payerNameOperSearchAdd.getValue(),
                cust2Inn: fields.payerINNOperSearchAdd.getValue(),
                cust2Resident: fields.payerCountryOperSearchAdd.getValue(),
                acct2Crit: fields.payerStandartConditionsAdd.getValue(),
                acct2Add: fields.payerConditionsAdd.getValue(),
                acct2Nb: fields.payerAccountNumberAdd.getValue(),
                acct2Area: fields.payerRegionSearchAdd.getValue(),
                custWatchList: vm.get('record.oes321TypeSender'),
                cust2WatchList: vm.get('record.oes321TypeTaker'),
                custNotWatchListFl: +vm.get('record.excludeSenderInn'),
                cust2NotWatchListFl: +vm.get('record.excludeTakerInn')
            };

            for (var skey in search) {
                if (Ext.isEmpty(search[skey])) {
                    delete search[skey];
                }
            }
            Ext.apply(params, {
                filters: search
            });
        }

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            params: Ext.encode(params),
            timeout: common.globalinit.ajaxTimeOut,
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                // переподкачка списка операций:
                var options = {
                    alertIds: responseObj.alertIds,
                    filters: search,
                    //where: fields.scopeFilters.getValue(),
                    addRecords: true,
                    refleshFooter: refleshFooter,
                    nextSelectedAlertIndex: nextSelectedAlertIndex
                };
                if (!Ext.isEmpty(responseObj.alertIds)) {
                    self.refreshAlertsList(options);
                }
            }
        });
    },

    refreshAlertsList: function (options) {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertsStore'),
            // alertDetailsPanelCtrl = self.lookupReference('alertDetailsPanel') && self.lookupReference('alertDetailsPanel').getController(),
            alertDetailsPanelCtrl = self.getView().down('app-alerts-alertdetailspanel') && self.getView().down('app-alerts-alertdetailspanel').getController(),
            oes321DetailsPanelCtrl = self.oes321DetailsPanel && self.oes321DetailsPanel.getController(),
            toggleBBarBtn = self.lookupReference('toggleBBarBtn'),
            trxnIds = (options) ? options.alertIds : null,
            trxnIdsLength = (trxnIds) ? trxnIds.length : null,
            params = {
                form: 'TrxnList',
                action: 'read',
                trxnIds: trxnIds
            },
            alertId,
            fields = {
                payerINNOperSearch: self.lookupReference('payerINNOperSearch'),
                payerINNOperSearchAdd: self.lookupReference('payerINNOperSearchAdd'),
                dtFromOperSearch: self.lookupReference('dtFromOperSearch'),
                dtToOperSearch: self.lookupReference('dtToOperSearch'),
                amountFromOperSearch: self.lookupReference('amountFromOperSearch'),
                amountToOperSearch: self.lookupReference('amountToOperSearch'),
                standartConditionOperSearch: self.lookupReference('standartConditionOperSearch'),
                conditionOperSearch: self.lookupReference('conditionOperSearch'),
                tbIdsOperSearch: self.lookupReference('tbIdsOperSearch'),
                osbIdsOperSearch: self.lookupReference('osbIdsOperSearch'),
                notCodeOperSearch: self.lookupReference('notCodeOperSearch'),
                categoryOperSearch: self.lookupReference('categoryOperSearch'),
                systemOperSearch: self.lookupReference('systemOperSearch'),
                forBetweenOperSearch: self.lookupReference('forBetweenOperSearch'),
                payerNameOperSearch: self.lookupReference('payerNameOperSearch'),
                payerCountryOperSearch: self.lookupReference('payerCountryOperSearch'),
                payerNameOperSearchAdd: self.lookupReference('payerNameOperSearchAdd'),
                payerCountryOperSearchAdd: self.lookupReference('payerCountryOperSearchAdd'),
                payerStandartConditions: self.lookupReference('payerStandartConditions'),
                payerAccountNumber: self.lookupReference('payerAccountNumber'),
                payerConditions: self.lookupReference('payerConditions'),
                payerRegionSearch: self.lookupReference('payerRegionSearch'),
                payerStandartConditionsAdd: self.lookupReference('payerStandartConditionsAdd'),
                payerAccountNumberAdd: self.lookupReference('payerAccountNumberAdd'),
                payerConditionsAdd: self.lookupReference('payerConditionsAdd'),
                payerRegionSearchAdd: self.lookupReference('payerRegionSearchAdd'),
                checkPayerCondition: vm.get('record.checkPayerCondition')
            },
            search = {};
        //applyFiltersBtn = self.lookupReference('applyFiltersBtn');

        search = {
            dtFrom: fields.dtFromOperSearch.getValue(),
            dtTo: fields.dtToOperSearch.getValue(),
            tbIds: fields.tbIdsOperSearch.getValue(),
            osbIds: fields.osbIdsOperSearch.getValue(),
            amountFrom: fields.amountFromOperSearch.getValue() ? parseFloat(fields.amountFromOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
            amountTo: fields.amountToOperSearch.getValue() ? parseFloat(fields.amountToOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
            lexPurposeCrit: fields.standartConditionOperSearch.getValue(),
            lexPurposeAdd: fields.conditionOperSearch.getValue(),
            notOpok: fields.notCodeOperSearch.getValue(),
            notOpokTp: parseInt(fields.forBetweenOperSearch.getValue(), 10),
            opCat: fields.categoryOperSearch.getValue(),
            srcSys: fields.systemOperSearch.getValue(),
            partyType: fields.checkPayerCondition,
            custNm: fields.payerNameOperSearch.getValue(),
            custInn: fields.payerINNOperSearch.getValue(),
            custResident: fields.payerCountryOperSearch.getValue(),
            acctCrit: fields.payerStandartConditions.getValue(),
            acctAdd: fields.payerConditions.getValue(),
            acctNb: fields.payerAccountNumber.getValue(),
            acctArea: fields.payerRegionSearch.getValue(),
            cust2Nm: fields.payerNameOperSearchAdd.getValue(),
            cust2Inn: fields.payerINNOperSearchAdd.getValue(),
            cust2Resident: fields.payerCountryOperSearchAdd.getValue(),
            acct2Crit: fields.payerStandartConditionsAdd.getValue(),
            acct2Add: fields.payerConditionsAdd.getValue(),
            acct2Nb: fields.payerAccountNumberAdd.getValue(),
            acct2Area: fields.payerRegionSearchAdd.getValue(),
            custWatchList: vm.get('record.oes321TypeSender'),
            cust2WatchList: vm.get('record.oes321TypeTaker'),
            custNotWatchListFl: +vm.get('record.excludeSenderInn'),
            cust2NotWatchListFl: +vm.get('record.excludeTakerInn')
        };

        for (var tkey in search) {
            if (Ext.isEmpty(search[tkey])) {
                delete search[tkey];
            }
        }
        if(trxnIdsLength === 0) {
            Ext.apply(params, {
                filters: search,
                cancelFetchId: options.cancelFetchId,
                where: fields.scopeFilters
            });
        }

        // Ext.apply(params, {
        //     cancelFetchId: options.cancelFetchId,
        //     where: fields.scopeFilters
        // });

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }
        if (options.alertIds) {
            // if (options.alertIds && statusOptions === 'NW') {
            var prevNextRecs = this.getNextPreviousAlertIDs(options.alertIds[0], store);
            if (prevNextRecs.nextSelectedAlertIndex && options.nextSelectedAlertIndex !== (prevNextRecs.nextSelectedAlertIndex - 1)) {
                options.nextSelectedAlertIndex = (prevNextRecs.nextSelectedAlertIndex - 1);
            }
        }
        store.load({
            params: params,
            addRecords: options.addRecords ? true : false,
            scope: self,
            callback: function (records, operation, success) {
                var responseText = Ext.decode(operation.getResponse().responseText),
                    store = this.getStore('alertsStore');

                // переподкачка статистики:
                //self.getStatistics();

                // если данные по операции не пришли после обновления списка, то закрываем панель:
                if (alertDetailsPanelCtrl) {
                    // обновляем правую панель детали операции, если есть данные для обновления и она открыта:
                    alertDetailsPanelCtrl && !Ext.isEmpty(records) && alertDetailsPanelCtrl.refreshDetailsTable(records[0].data);
                    if (alertDetailsPanelCtrl.getView()) {
                        alertDetailsPanelCtrl && Ext.isEmpty(records) && alertDetailsPanelCtrl.getView().close();
                    }
                }
                if (oes321DetailsPanelCtrl) {
                    // обновляем правую панель детали операции, если есть данные для обновления и она открыта:
                    oes321DetailsPanelCtrl && !Ext.isEmpty(records) && oes321DetailsPanelCtrl.refreshDetailsTable(records[0].data);
                    if (oes321DetailsPanelCtrl.getView()) {
                        oes321DetailsPanelCtrl && Ext.isEmpty(records) && oes321DetailsPanelCtrl.getView().close();
                    }
                }

                // если данные по операции не пришли после обновления списка, то закрываем нижную панель детали операции:
                vm.get('totalCount') <= 0 && toggleBBarBtn.setPressed(false);

                if (options.refleshFooterAct) {
                    for (var i = 0; i < store.length; i++) {
                        alertId = store[i].get('_id_');
                        if (alertId !== null && alertId > 0) {
                            if (store.data.items.length > 0) {
                                Ext.Array.forEach(store.data.items, function (item, index) {
                                    if (parseInt(item.id, 10) === parseInt(options.alertId, 10)) {
                                        options.refleshFooterAct = false;
                                    }
                                })
                            }
                        }
                    }
                }
                // перегружаем подвал при условии что была операция (отменить(неверная лексема))
                if (!alertDetailsPanelCtrl && !oes321DetailsPanelCtrl && (options.refleshFooter || options.refleshFooterAct) && vm.getView().down('grid').store.totalCount > 0) {
                    if (options.nextSelectedAlertIndex >= 0) {
                        // перепроверка открытия подвала для деталей операции
                        var monitoringAlertsTopbar = self.lookupReference('monitoringAlertsTopbar');
                        // проверяем был ли открыт подвал
                        if (monitoringAlertsTopbar.reestablishView && !self.getView().down('app-alerts-oes321detailspanel')) {
                            toggleBBarBtn.setPressed(true);
                            monitoringAlertsTopbar.reestablishView = false;
                        }
                        self.lookupReference('alertsOperSearchGrid').getView().focusRow(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                        self.lookupReference('alertsOperSearchGrid').getView().setSelection(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                    }
                }
                // set cancelFetchId
                if (responseText && responseText.fetchId) {
                    vm.set('fetchId', responseText.fetchId);
                }
                // if (!responseText.data.length && (options.alertId || options.alertIds)) {
                // if (options.oldIds) {
                //     var deletedRecord;
                //     if (options.oldIds.length) {
                //         for (var m = 0; m < options.oldIds.length; m++) {
                //             deletedRecord = store.findRecord('_id_', options.oldIds[m]);
                //             if (deletedRecord) {
                //                 store.remove(deletedRecord);
                //             }
                //         }
                //     }
                // }
                if (store.getCount()) {
                    if (trxnIds) {
                        var nextRecord = store.findRecord('_id_', trxnIds[0]);
                        if (nextRecord) {
                            self.lookupReference('alertsOperSearchGrid').getView().focusRow(nextRecord);
                            self.lookupReference('alertsOperSearchGrid').getView().setSelection(nextRecord);
                        }
                    }
                }
            }
        });
    },


    onLoadOperSearchAlerts: function (store, records, successful, operation) {
        var self = this,
            grid = self.lookupReference('alertsOperSearchGrid'),
            gridStore = grid.getStore(),
            vm = self.getViewModel(),
            selectedAlertIndex = gridStore.indexOf(vm.get('selectedOSAlert')) != -1 ? gridStore.indexOf(vm.get('selectedOSAlert')) : 0,
            responseObj = operation.getResponse() ? Ext.JSON.decode(operation.getResponse().responseText) : '',
            grandTotal = '...',
            viewAppMain = Ext.getCmp('app-main-id'),
            vmAppMain = (viewAppMain) ? viewAppMain.getViewModel() : false,
            toggleBBarBtn = self.lookupReference('toggleBBarBtn'),
            reviewId = (vmAppMain) ? vmAppMain.get('createAlertReviewId') : false,
            oes321Id = (vmAppMain) ? vmAppMain.get('createAlertOES321Id') : false,
            reviewId2 = (vmAppMain) ? vmAppMain.get('createAlertReviewId2') : false,
            oes321Id2 = (vmAppMain) ? vmAppMain.get('createAlertOES321Id2') : false,
            oes321ADD = (vmAppMain) ? vmAppMain.get('createAlertADD') : false;

        // для отображения количества загруженных записей в футере:
        if (responseObj.grandTotal || responseObj.grandTotal === 0 || responseObj.grandTotal === null) {
            if (responseObj.grandTotal === 0) {
                grandTotal = '0';
            } else if (responseObj.grandTotal > 0) {
                grandTotal = responseObj.grandTotal;
            }
            vm.set('grandTotal', grandTotal);
            vm.set('totalCount', responseObj.totalCount);
            // для порционной подрузки списка:
            vm.set('fetchId', responseObj.fetchId ? responseObj.fetchId : '');
            if (responseObj.grandTotal === 0) {
                // закрываем нижную панель детали операции:
                toggleBBarBtn.setPressed(false);
            }
        }

        // удаление filtered записей:
        if (responseObj) {
            if (!Ext.isEmpty(responseObj.filtered) && !Ext.isEmpty(responseObj.filtered[0])) {
                store.remove(store.queryBy(function (record) {
                    return Ext.Array.contains(responseObj.filtered, record.get('REVIEW_ID'));
                }).getRange());
                // по выполнению операции обновить количество операций(totalCount):
                vm.set('totalCount', vm.get('totalCount') - responseObj.filtered.length);
            }
            if (!Ext.isEmpty(responseObj.filtered) && !Ext.isEmpty(responseObj.filtered[0])) {
                store.remove(store.queryBy(function (record) {
                    return Ext.Array.contains(responseObj.filtered, record.get('REVIEW_ID'));
                }).getRange());
                // по выполнению операции обновить количество операций(totalCount):
                vm.set('totalCount', vm.get('totalCount') - responseObj.filtered.length);
            }
            // выбираем заново строку из грида для обновления подвала детали:
            if ((selectedAlertIndex >= 0) && responseObj.data && (gridStore.data.length > 0)) {
                if (!gridStore.getAt(selectedAlertIndex)) {
                    selectedAlertIndex = 0;
                }
                grid.getView().focusRow(gridStore.getAt(selectedAlertIndex));
                grid.getView().setSelection(gridStore.getAt(selectedAlertIndex));
            }
        }
    },

    onLoadFiltersCollection: function (store) {
        var self = this,
            vm = this.getViewModel(),
            defaultFilter = store.findRecord('IS_DEFAULT', 1),
            alertsStore = this.getStore('alertsStore');

        if (defaultFilter) {
            vm.set('scopeFilter', defaultFilter.get('_id_'));
            alertsStore.load({
                params: {
                    where: defaultFilter.get('_id_')
                }
            });
        } else {
            alertsStore.load();
        }
    },

    closePanel: function () {
        var self = this;

        self.getView().close();
    },

    onDbClickAmountFrom: function () {
        var self = this,
            vm = this.getViewModel(),
            valTo;

        if (vm.get('search.amountToOperSearch') && vm.get('search.amountToOperSearch') !== '') {
            valTo = Ext.util.Format.number(parseFloat(vm.get('search.amountToOperSearch').replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')), '0,000.00');
            vm.set('search.amountFromOperSearch', valTo);
        }
    },

    onDbClickAmountTo: function () {
        var self = this,
            vm = this.getViewModel(),
            valFrom;

        if (vm.get('search.amountFromOperSearch') && vm.get('search.amountFromOperSearch') !== '') {
            valFrom = Ext.util.Format.number(parseFloat(vm.get('search.amountFromOperSearch').replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')), '0,000.00');
            vm.set('search.amountToOperSearch', valFrom);
        }
    },

    countFilterOperSearchToggle: function (btn) {
        var self = this,
            vm = self.getViewModel(),
            fields = {
                payerINNOperSearch: self.lookupReference('payerINNOperSearch'),
                payerINNOperSearchAdd: self.lookupReference('payerINNOperSearchAdd'),
                dtFromOperSearch: self.lookupReference('dtFromOperSearch'),
                dtToOperSearch: self.lookupReference('dtToOperSearch'),
                amountFromOperSearch: self.lookupReference('amountFromOperSearch'),
                amountToOperSearch: self.lookupReference('amountToOperSearch'),
                standartConditionOperSearch: self.lookupReference('standartConditionOperSearch'),
                conditionOperSearch: self.lookupReference('conditionOperSearch'),
                tbIdsOperSearch: self.lookupReference('tbIdsOperSearch'),
                osbIdsOperSearch: self.lookupReference('osbIdsOperSearch'),
                notCodeOperSearch: self.lookupReference('notCodeOperSearch'),
                categoryOperSearch: self.lookupReference('categoryOperSearch'),
                systemOperSearch: self.lookupReference('systemOperSearch'),
                forBetweenOperSearch: self.lookupReference('forBetweenOperSearch'),
                payerNameOperSearch: self.lookupReference('payerNameOperSearch'),
                payerCountryOperSearch: self.lookupReference('payerCountryOperSearch'),
                payerNameOperSearchAdd: self.lookupReference('payerNameOperSearchAdd'),
                payerCountryOperSearchAdd: self.lookupReference('payerCountryOperSearchAdd'),
                payerStandartConditions: self.lookupReference('payerStandartConditions'),
                payerAccountNumber: self.lookupReference('payerAccountNumber'),
                payerConditions: self.lookupReference('payerConditions'),
                payerRegionSearch: self.lookupReference('payerRegionSearch'),
                payerStandartConditionsAdd: self.lookupReference('payerStandartConditionsAdd'),
                payerAccountNumberAdd: self.lookupReference('payerAccountNumberAdd'),
                payerConditionsAdd: self.lookupReference('payerConditionsAdd'),
                payerRegionSearchAdd: self.lookupReference('payerRegionSearchAdd'),
                checkPayerCondition: vm.get('record.checkPayerCondition')
            },
            search = {
                dtFrom: fields.dtFromOperSearch.getValue(),
                dtTo: fields.dtToOperSearch.getValue(),
                tbIds: fields.tbIdsOperSearch.getValue(),
                osbIds: fields.osbIdsOperSearch.getValue(),
                amountFrom: parseFloat(fields.amountFromOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                amountTo: parseFloat(fields.amountToOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')),
                lexPurposeCrit: fields.standartConditionOperSearch.getValue(),
                lexPurposeAdd: fields.conditionOperSearch.getValue(),
                notOpok: fields.notCodeOperSearch.getValue(),
                notOpokTp: parseInt(fields.forBetweenOperSearch.getValue(), 10),
                opCat: fields.categoryOperSearch.getValue(),
                srcSys: fields.systemOperSearch.getValue(),
                partyType: fields.checkPayerCondition,
                custNm: fields.payerNameOperSearch.getValue(),
                custInn: fields.payerINNOperSearch.getValue(),
                custResident: fields.payerCountryOperSearch.getValue(),
                acctCrit: fields.payerStandartConditions.getValue(),
                acctAdd: fields.payerConditions.getValue(),
                acctNb: fields.payerAccountNumber.getValue(),
                acctArea: fields.payerRegionSearch.getValue(),
                cust2Nm: fields.payerNameOperSearchAdd.getValue(),
                cust2Inn: fields.payerINNOperSearchAdd.getValue(),
                cust2Resident: fields.payerCountryOperSearchAdd.getValue(),
                acct2Crit: fields.payerStandartConditionsAdd.getValue(),
                acct2Add: fields.payerConditionsAdd.getValue(),
                acct2Nb: fields.payerAccountNumberAdd.getValue(),
                acct2Area: fields.payerRegionSearchAdd.getValue(),
                custWatchList: vm.get('record.oes321TypeSender'),
                cust2WatchList: vm.get('record.oes321TypeTaker'),
                custNotWatchListFl: +vm.get('record.excludeSenderInn'),
                cust2NotWatchListFl: +vm.get('record.excludeTakerInn')
            };

        Ext.Object.each(search, function (key, value) {
            if (Ext.isEmpty(value)) {
                delete search[key];
            }
        });

        // добавим loading обработки:
        if (!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }
        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode({
                form: 'TrxnList',
                action: 'count',
                filters: search
            }),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                vm.set('filteredCount', responseObj.countAlerts || 0);
                vm.set('allCount', responseObj.count || 0);
            },
            failure: function (error) {
                Ext.Msg.alert('', 'Превышено максимальное время запроса!');
            }
        })
    },

    toggleSearchOperFilters: function (btn) {
        var applyFiltersBtn = this.lookupReference('toggleSearchOperFiltersBtn');
        this.getViewModel().set('showSearchOperFilter', btn.pressed);
        applyFiltersBtn.setPressed(btn.pressed);
    },

    showSearchOperList: function () {
        var self = this,
            window = this.getView(),
            grid = self.lookupReference('alertsOperSearchGrid'),
            gridStore = grid.getStore(),
            //form = window.down('main-search-operations-topbar'),
            vm = window.getViewModel(),
            //vmData = (vm) ? vm.getData() : false,
            data = {
                form: 'TrxnList',
                action: 'read'
            },
            fields = {
                payerINNOperSearch: self.lookupReference('payerINNOperSearch'),
                payerINNOperSearchAdd: self.lookupReference('payerINNOperSearchAdd'),
                dtFromOperSearch: self.lookupReference('dtFromOperSearch'),
                dtToOperSearch: self.lookupReference('dtToOperSearch'),
                amountFromOperSearch: self.lookupReference('amountFromOperSearch'),
                amountToOperSearch: self.lookupReference('amountToOperSearch'),
                standartConditionOperSearch: self.lookupReference('standartConditionOperSearch'),
                conditionOperSearch: self.lookupReference('conditionOperSearch'),
                tbIdsOperSearch: self.lookupReference('tbIdsOperSearch'),
                osbIdsOperSearch: self.lookupReference('osbIdsOperSearch'),
                notCodeOperSearch: self.lookupReference('notCodeOperSearch'),
                categoryOperSearch: self.lookupReference('categoryOperSearch'),
                systemOperSearch: self.lookupReference('systemOperSearch'),
                forBetweenOperSearch: self.lookupReference('forBetweenOperSearch'),
                payerNameOperSearch: self.lookupReference('payerNameOperSearch'),
                payerCountryOperSearch: self.lookupReference('payerCountryOperSearch'),
                payerNameOperSearchAdd: self.lookupReference('payerNameOperSearchAdd'),
                payerCountryOperSearchAdd: self.lookupReference('payerCountryOperSearchAdd'),
                payerStandartConditions: self.lookupReference('payerStandartConditions'),
                payerAccountNumber: self.lookupReference('payerAccountNumber'),
                payerConditions: self.lookupReference('payerConditions'),
                payerRegionSearch: self.lookupReference('payerRegionSearch'),
                payerStandartConditionsAdd: self.lookupReference('payerStandartConditionsAdd'),
                payerAccountNumberAdd: self.lookupReference('payerAccountNumberAdd'),
                payerConditionsAdd: self.lookupReference('payerConditionsAdd'),
                payerRegionSearchAdd: self.lookupReference('payerRegionSearchAdd'),
                checkPayerCondition: vm.get('record.checkPayerCondition')
            },
            msgTrigger = false,
            toggleBBarOperSearchBtn;

        var search = {
            dtFrom: fields.dtFromOperSearch.getValue(),
            dtTo: fields.dtToOperSearch.getValue(),
            tbIds: fields.tbIdsOperSearch.getValue(),
            osbIds: fields.osbIdsOperSearch.getValue(),
            amountFrom: fields.amountFromOperSearch.getValue() ? parseFloat(fields.amountFromOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
            amountTo: fields.amountToOperSearch.getValue() ? parseFloat(fields.amountToOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
            lexPurposeCrit: fields.standartConditionOperSearch.getValue(),
            lexPurposeAdd: fields.conditionOperSearch.getValue(),
            notOpok: fields.notCodeOperSearch.getValue(),
            notOpokTp: parseInt(fields.forBetweenOperSearch.getValue(), 10),
            opCat: fields.categoryOperSearch.getValue(),
            srcSys: fields.systemOperSearch.getValue(),
            partyType: fields.checkPayerCondition,
            custNm: fields.payerNameOperSearch.getValue(),
            custInn: fields.payerINNOperSearch.getValue(),
            custResident: fields.payerCountryOperSearch.getValue(),
            acctCrit: fields.payerStandartConditions.getValue(),
            acctAdd: fields.payerConditions.getValue(),
            acctNb: fields.payerAccountNumber.getValue(),
            acctArea: fields.payerRegionSearch.getValue(),
            cust2Nm: fields.payerNameOperSearchAdd.getValue(),
            cust2Inn: fields.payerINNOperSearchAdd.getValue(),
            cust2Resident: fields.payerCountryOperSearchAdd.getValue(),
            acct2Crit: fields.payerStandartConditionsAdd.getValue(),
            acct2Add: fields.payerConditionsAdd.getValue(),
            acct2Nb: fields.payerAccountNumberAdd.getValue(),
            acct2Area: fields.payerRegionSearchAdd.getValue(),
            custWatchList: vm.get('record.oes321TypeSender'),
            cust2WatchList: vm.get('record.oes321TypeTaker'),
            custNotWatchListFl: +vm.get('record.excludeSenderInn'),
            cust2NotWatchListFl: +vm.get('record.excludeTakerInn')
        };
        var dateFrom = search.dtFrom,
            dateTo = search.dtTo;
        if (!dateFrom) {
            fields.dtFromOperSearch.markInvalid('нужно заполнить');
            return;
        }
        if (!dateTo) {
            dateTo = new Date();
        }
        var timeDiff = Math.abs(dateTo.getTime() - dateFrom.getTime()),
            diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)),
            valPayer = vm.get('record.checkPayerCondition');

        if (dateFrom > dateTo) {
            Ext.Msg.alert('', 'Дата отперации От должна быть больше Даты операции До!');
            return false;
        } else {

            if ((!search.custInn) || (search.custInn.indexOf('%') !== -1) || (search.custInn.indexOf('_') !== -1)) {
                if ((diffDays > 7) || (!search.amountFrom) || (search.amountFrom < 600000)) {
                    msgTrigger = true;
                }
            }
            if ((valPayer === 'O|B' || valPayer === 'O&B') && (!search.cust2Inn || (search.cust2Inn.indexOf('%') !== -1) || (search.cust2Inn.indexOf('_') !== -1))) {
                if ((diffDays > 7) || (!search.amountFrom) || (search.amountFrom < 600000)) {
                    msgTrigger = true;
                }
            }
            if (!search.acctNb || (search.acctNb.indexOf('%') !== -1) || (search.acctNb.indexOf('_') !== -1)) {
                if ((diffDays > 7) || (!search.amountFrom) || (search.amountFrom < 600000)) {
                    msgTrigger = true;
                }
            }
            if ((valPayer === 'O|B' || valPayer === 'O&B') && (!search.acct2Nb || (search.acct2Nb.indexOf('%') !== -1) || (search.acct2Nb.indexOf('_') !== -1))) {
                if ((diffDays > 7) || (!search.amountFrom) || (search.amountFrom < 600000)) {
                    msgTrigger = true;
                }
            }
            if ((search.acctArea.indexOf('НУ') !== -1) || (search.acctArea.indexOf('НП') !== -1)) {
                if ((diffDays > 7) || (!search.amountFrom) || (search.amountFrom < 600000)) {
                    msgTrigger = true;
                }
            }
            if ((valPayer === 'O|B' || valPayer === 'O&B') && ((search.acct2Area.indexOf('НУ') !== -1) || (search.acct2Area.indexOf('НП') !== -1))) {
                if ((diffDays > 7) || (!search.amountFrom) || (search.amountFrom < 600000)) {
                    msgTrigger = true;
                }
            }
        }
        if (msgTrigger) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Будет выполнена загрузка большого количества операций Это может занять много времени и потребовать значительных ресурсов Вашего компьютера. Вы уверены?',
                fnYes: function () {
                    for (key in search) {
                        if (Ext.isEmpty(search[key])) {
                            delete search[key];
                        }
                    }
                    Ext.apply(data, {
                        filters: search
                    });

                    // добавим loading обработки:
                    if (!common.globalinit.loadingMaskCmp) {
                        common.globalinit.loadingMaskCmp = self.getView();
                    }

                    Ext.Ajax.request({
                        url: common.globalinit.ajaxUrl,
                        method: common.globalinit.ajaxMethod,
                        headers: common.globalinit.ajaxHeaders,
                        timeout: common.globalinit.ajaxTimeOut,
                        params: Ext.encode(data),
                        success: function (response) {
                            vm.set('allCount', '');
                            vm.set('filteredCount', '');

                            var responseObj = Ext.JSON.decode(response.responseText);
                            if (responseObj.message) {
                                Ext.Msg.alert('', responseObj.message);
                            }
                            if (responseObj.totalCount === 0) {
                                Ext.Msg.alert('', 'Операций, соответствующих введеным данным не найдено!');
                                vm.set('showBBarOperSearch', false);
                                toggleBBarOperSearchBtn = self.lookupReference('toggleBBarOperSearchBtn');
                                if (toggleBBarOperSearchBtn.pressed) {
                                    toggleBBarOperSearchBtn.setPressed(!toggleBBarOperSearchBtn.pressed);
                                }
                            }
                            // загрузка данных в грид
                            var storeSearchResult = self.getReferences()['alertsOperSearchGrid'].getStore();
                            if (storeSearchResult) {
                                storeSearchResult.removeAll();
                            }
                            if (responseObj.success) {
                                storeSearchResult = self.getReferences()['alertsOperSearchGrid'].getStore();
                                if (storeSearchResult) {
                                    storeSearchResult.add(responseObj.data);
                                }
                            }
                            // для отображения количества загруженных записей в футере:
                            if (responseObj.grandTotal || responseObj.grandTotal === 0 || responseObj.grandTotal === null) {
                                if (responseObj.grandTotal === 0) {
                                    grandTotal = '0';
                                } else if (responseObj.grandTotal > 0) {
                                    grandTotal = responseObj.grandTotal;
                                } else if (responseObj.grandTotal === null) {
                                    grandTotal = '...'
                                }
                                vm.set('grandTotal', grandTotal);
                                vm.set('totalCount', responseObj.totalCount);

                                // для порционной подрузки списка:
                                vm.set('fetchId', responseObj.fetchId ? responseObj.fetchId : '');
                            }
                            self.selectRowGrid();
                        },
                        failure: function (error) {
                            Ext.Msg.alert('', 'Превышено максимальное время запроса!');
                        }
                    })
                },
                fnNo: function () {
                    return false;
                }
            });
        }
        if (!msgTrigger) {
            for (key in search) {
                if (Ext.isEmpty(search[key])) {
                    delete search[key];
                }
            }
            Ext.apply(data, {
                filters: search
            });

            // добавим loading обработки:
            if (!common.globalinit.loadingMaskCmp) {
                common.globalinit.loadingMaskCmp = self.getView();
            }

            Ext.Ajax.request({
                url: common.globalinit.ajaxUrl,
                method: common.globalinit.ajaxMethod,
                headers: common.globalinit.ajaxHeaders,
                timeout: common.globalinit.ajaxTimeOut,
                params: Ext.encode(data),
                success: function (response) {
                    vm.set('allCount', '');
                    vm.set('filteredCount', '');

                    var responseObj = Ext.JSON.decode(response.responseText);
                    if (responseObj.message) {
                        Ext.Msg.alert('', responseObj.message);
                    }
                    if (responseObj.totalCount === 0) {
                        Ext.Msg.alert('', 'Операций, соответствующих введеным данным не найдено!');
                        vm.set('showBBarOperSearch', false);
                        toggleBBarOperSearchBtn = self.lookupReference('toggleBBarOperSearchBtn');
                        if (toggleBBarOperSearchBtn.pressed) {
                            toggleBBarOperSearchBtn.setPressed(!toggleBBarOperSearchBtn.pressed);
                        }
                    }
                    // загрузка данных в грид
                    var storeSearchResult = self.getReferences()['alertsOperSearchGrid'].getStore();
                    if (storeSearchResult) {
                        storeSearchResult.removeAll();
                    }
                    if (responseObj.success) {
                        storeSearchResult = self.getReferences()['alertsOperSearchGrid'].getStore();
                        if (storeSearchResult) {
                            storeSearchResult.add(responseObj.data);
                        }
                    }
                    // для отображения количества загруженных записей в футере:
                    if (responseObj.grandTotal || responseObj.grandTotal === 0 || responseObj.grandTotal === null) {
                        if (responseObj.grandTotal === 0) {
                            grandTotal = '0';
                        } else if (responseObj.grandTotal > 0) {
                            grandTotal = responseObj.grandTotal;
                        } else if (responseObj.grandTotal === null) {
                            grandTotal = '...'
                        }
                        vm.set('grandTotal', grandTotal);
                        vm.set('totalCount', responseObj.totalCount);

                        // для порционной подрузки списка:
                        vm.set('fetchId', responseObj.fetchId ? responseObj.fetchId : '');
                    }
                    // перегружаем подвал при условии что была операция (отменить(неверная лексема))
                    // if ((options.refleshFooter || options.refleshFooterAct) && vm.getView().down('grid').store.totalCount > 0) {
                    //     if (options.nextSelectedAlertIndex >= 0) {
                    //         // перепроверка открытия подвала для деталей операции
                    //         var monitoringAlertsTopbar = self.lookupReference('monitoringAlertsTopbar');
                    //         // проверяем был ли открыт подвал
                    //         if (monitoringAlertsTopbar.reestablishView && !self.getView().down('app-alerts-oes321detailspanel')) {
                    //             toggleBBarBtn.setPressed(true);
                    //             monitoringAlertsTopbar.reestablishView = false;
                    //         }
                    //         self.lookupReference('alertsGrid').getView().focusRow(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                    //         self.lookupReference('alertsGrid').getView().setSelection(this.getStore('alertsStore').getAt(options.nextSelectedAlertIndex));
                    //     }
                    // }
                    self.selectRowGrid();
                },
                failure: function (error) {
                    Ext.Msg.alert('', 'Превышено максимальное время запроса!');
                }
            })
        }
    },

    checkPayerCondition: function (btn) {
        var vmMain = btn.up('main-search-operations-form').getViewModel(),
            fieldSetPayer = btn.up('fieldset'),
            fSetPayer = Ext.getCmp(fieldSetPayer.id + '-legendTitle').el.dom,
            titlePayerName = '', titleAccountPayer = '', checkPayerCondition = 0,
            payerSelectAddBlocks = this.lookupReference('payerSelectNameAddBlocks'),
            fieldSetPayerAccount = this.lookupReference('payerSelectAccountAddBlocks'),
            fieldSetPayerAccCh = this.lookupReference('payerAccountOperSearch');

        switch (btn.text) {
            case 'Условия на плательщика':
                titlePayerName = 'Плательщик';
                titleAccountPayer = 'Счет плательщика';
                checkPayerCondition = 'O';
                if (payerSelectAddBlocks) payerSelectAddBlocks.hide();
                if (fieldSetPayerAccount) fieldSetPayerAccount.hide();
                break;
            case 'Условия на получателя':
                titlePayerName = 'Получатель';
                titleAccountPayer = 'Счет получателя';
                checkPayerCondition = 'B';
                if (payerSelectAddBlocks) payerSelectAddBlocks.hide();
                if (fieldSetPayerAccount) fieldSetPayerAccount.hide();
                break;
            case 'Условия на Плательщика или Получателя':
                titlePayerName = 'Плательщик или Получатель';
                titleAccountPayer = 'Счет плательщика или получателя';
                checkPayerCondition = 'O|B';
                if (payerSelectAddBlocks) payerSelectAddBlocks.hide();
                if (fieldSetPayerAccount) fieldSetPayerAccount.hide();
                break;
            case 'Условия и на Плательщика и на Получателя':
                titlePayerName = 'Плательщик';
                titleAccountPayer = 'Счет плательщика';
                if (vmMain) vmMain.set('selectTypePayer', false);
                checkPayerCondition = 'O&B';
                if (payerSelectAddBlocks) payerSelectAddBlocks.show();
                if (fieldSetPayerAccount) fieldSetPayerAccount.show();
                break;
            default:

        }
        if (vmMain) vmMain.set('record.checkPayerCondition', checkPayerCondition);
        if (fieldSetPayer) fSetPayer.innerHTML = titlePayerName;
        if (fieldSetPayerAccCh) fieldSetPayerAccCh.setTitle(titleAccountPayer);
    },

    readNextListPageOperSearch: function (btn) {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertsStore'),
            readLastListPageBtn = self.lookupReference('readLastListPageBtn'),
            params = {
                form: 'TrxnList',
                action: 'readNext',
                fetchId: vm.get('fetchId')
            };

        // если fetchId пустой (были загружены все операции с сервера):
        if (Ext.isEmpty(params.fetchId)) {
            Ext.Msg.alert('', 'Все операции уже загружены!');
            return;
        }

        if (btn.disabled !== true) {
            btn.disable(true);
            readLastListPageBtn.disable(true);
            store.load({
                params: params,
                addRecords: true,
                scope: self,
                callback: function (records, operation, success) {
                    // переподкачка статистики:
                    // self.getStatistics();
                    btn.enable(true);
                    readLastListPageBtn.enable(true);
                }
            });
        }
    },

    /**
     * Получаем статистику
     */
    getStatistics: function () {
        var ajax = Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            async: false,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.JSON.encode({
                form: 'TrxnStatistics',
                action: 'read'
            })
        });
        this.getViewModel().set('statistics', Ext.JSON.decode(ajax.responseText).data[0]);
    },

    readLastListPageOperSearch: function (btn) {
        var self = this,
            vm = this.getViewModel(),
            store = self.getStore('alertsStore'),
            readNextListPageBtn = self.lookupReference('readNextListPageBtn'),
            params = {
                form: 'TrxnList',
                action: 'readLast',
                fetchId: vm.get('fetchId')
            };

        // если fetchId пустой (были загружены все операции с сервера):
        if (Ext.isEmpty(params.fetchId)) {
            Ext.Msg.alert('', 'Все операции уже загружены!');
            return;
        }

        if (vm.get('grandTotal') === '...' || ((vm.get('grandTotal') - vm.get('totalCount')) > 2000)) {
            Ext.Msg.confirm('', 'Будет выполнена загрузка большого количества операций. Это может занять много времени и потребовать значительных ресурсов Вашего компьютера. Вы уверены?', function (answer) {
                if ((answer === 'yes') && (btn.disabled !== true)) {
                    btn.disable(true);
                    readNextListPageBtn.disable(true);
                    store.load({
                        params: params,
                        addRecords: true,
                        scope: self,
                        callback: function (records, operation, success) {
                            // переподкачка статистики:
                            // self.getStatistics();
                            if (success) {
                                btn.enable(false);
                                readNextListPageBtn.enable(false);
                            }
                        }
                    });
                }
            });
        } else if ((vm.get('grandTotal') - vm.get('totalCount')) <= 2000) {
            if (btn.disabled !== true) {
                btn.disable(true);
                readNextListPageBtn.disable(true);
                store.load({
                    params: params,
                    addRecords: true,
                    scope: self,
                    callback: function (records, operation, success) {
                        // переподкачка статистики:
                        // self.getStatistics();
                        if (success) {
                            btn.enable(false);
                            readNextListPageBtn.enable(false);
                        }
                    }
                });
            }
        }
    },

    toggleBBarOperSearch: function (btn) {
        var self = this,
            alertId = (self.getViewModel().get('selectedOSAlert')) ? self.getViewModel().get('selectedOSAlert').get('REVIEW_ID') : false;

        this.getViewModel().set('showBBarOperSearch', btn.pressed);
        if (alertId) {
            self.selectRowGrid(alertId);
        }
    },

    selectRowGrid: function (alertId) {
        var self = this,

            dataArr = [],
            grid = self.lookupReference('alertsOperSearchGrid'),
            gridStore = grid.getStore(),
            vm = self.getViewModel(),
            selectedAlertIndex3 = 0,
            alertId = alertId || vm.get('selectedOSAlert.REVIEW_ID'),
            view = grid.getView();

        if (gridStore.getCount() > 0) {
            if (alertId) {
                Ext.Array.forEach(gridStore.getRange(), function (item, index) {
                    dataArr.push(item.get('REVIEW_ID'));
                });
                selectedAlertIndex3 = Ext.Array.indexOf(dataArr, alertId) != -1 ? Ext.Array.indexOf(dataArr, alertId) : 0;
            }
            // выставляем select
            view.focusRow(gridStore.getAt(selectedAlertIndex3));
            view.setSelection(gridStore.getAt(selectedAlertIndex3));
        }
    },

    openClientFormByINN: function (btn) {
        var vm = this.getViewModel(),
            sourceField = btn.up().down('textfield'),
            sourceFieldValue = sourceField ? sourceField.getValue() : '';

        if (!sourceFieldValue) {
            Ext.Msg.alert('', 'Не указано значение в поле "ИНН" !');
            return;
        }

        var form = Ext.widget({
            xtype: 'oes321detailspanel-tabs-search-client-form',
            minHeight: 600,
            height: 700,
            width: '100%',
            viewModel: {
                data: {
                    IS_SEARCH_OPERATION: true,
                    IS_CHECK: {
                        IS_ND: true,
                        IS_CUST_SEQ_ID: false
                    },
                    record: {
                        nd: sourceFieldValue,
                        cust_seq_id: null
                    }
                },
                parent: vm
            }
        }).show();
    },

    openOPOKType: function (btn) {
        var sourceField = btn.up().down('combobox'),
            sourceFieldValue = sourceField ? sourceField.getValue() : '',
            opoktype = (sourceField) ? sourceField.getStore() : false;

        if (!sourceFieldValue) {
            Ext.Msg.alert('', 'Не указано значение в поле "Перечень" !');
            return;
        }

        if (opoktype) {
            Ext.create({
                xtype: 'app-oes-oes321listsorg-dialog-list',
                viewModel: {
                    xtype: 'app-oes-oes321listsorg-dialog-list',
                    data: {
                        isOpenReadOnly: true,
                        idOrg: sourceFieldValue
                    }
                }
            }).show();
        }
    },

    openStandartConditionCommon: function (btn, criteriaStore, readOnly) {
        var self = this,
            opok1 = Ext.data.StoreManager.lookup(criteriaStore),
            valField = btn.up().down('combobox').getValue(), record;

        if (valField) {
            if (opok1) {
                record = opok1.findRecord('CRIT_CD', valField, 0, false, false, true);
                if (record) {
                    var form = Ext.widget({
                        xtype: 'app-settings-opokcriteria-form',
                        viewModel: {
                            data: {
                                record: record,
                                isOpenReadOnly: readOnly
                            },
                            parent: self.getViewModel()
                        }
                    }).show();
                }
            }
        } else {
            Ext.Msg.alert('', 'Не выбрано значение в поле "Стандартное условие" !');
            return;
        }
    },

    openStandartCondition: function (btn) {
        this.openStandartConditionCommon(btn, 'OPOKCriteriaT1', false);
    },

    openStandartConditionRO1: function (btn) {
        this.openStandartConditionCommon(btn, 'OPOKCriteriaT1', true);
    },

    openStandartConditionForm: function (btn) {
        this.openStandartConditionCommon(btn, 'OPOKCriteriaT2', true);
    },

    subjectToControlActionOperSearch: function (btn) {
        if (!this.checkGridSelection()) {
            return;
        }
        var self = this,
            vm = self.getViewModel(),
            fields = {
                payerINNOperSearch: self.lookupReference('payerINNOperSearch'),
                payerINNOperSearchAdd: self.lookupReference('payerINNOperSearchAdd'),
                dtFromOperSearch: self.lookupReference('dtFromOperSearch'),
                dtToOperSearch: self.lookupReference('dtToOperSearch'),
                amountFromOperSearch: self.lookupReference('amountFromOperSearch'),
                amountToOperSearch: self.lookupReference('amountToOperSearch'),
                standartConditionOperSearch: self.lookupReference('standartConditionOperSearch'),
                conditionOperSearch: self.lookupReference('conditionOperSearch'),
                tbIdsOperSearch: self.lookupReference('tbIdsOperSearch'),
                osbIdsOperSearch: self.lookupReference('osbIdsOperSearch'),
                notCodeOperSearch: self.lookupReference('notCodeOperSearch'),
                categoryOperSearch: self.lookupReference('categoryOperSearch'),
                systemOperSearch: self.lookupReference('systemOperSearch'),
                forBetweenOperSearch: self.lookupReference('forBetweenOperSearch'),
                payerNameOperSearch: self.lookupReference('payerNameOperSearch'),
                payerCountryOperSearch: self.lookupReference('payerCountryOperSearch'),
                payerNameOperSearchAdd: self.lookupReference('payerNameOperSearchAdd'),
                payerCountryOperSearchAdd: self.lookupReference('payerCountryOperSearchAdd'),
                payerStandartConditions: self.lookupReference('payerStandartConditions'),
                payerAccountNumber: self.lookupReference('payerAccountNumber'),
                payerConditions: self.lookupReference('payerConditions'),
                payerRegionSearch: self.lookupReference('payerRegionSearch'),
                payerStandartConditionsAdd: self.lookupReference('payerStandartConditionsAdd'),
                payerAccountNumberAdd: self.lookupReference('payerAccountNumberAdd'),
                payerConditionsAdd: self.lookupReference('payerConditionsAdd'),
                payerRegionSearchAdd: self.lookupReference('payerRegionSearchAdd'),
                checkPayerCondition: vm.get('record.checkPayerCondition')
            },
            search = {
                dtFrom: fields.dtFromOperSearch.getValue(),
                dtTo: fields.dtToOperSearch.getValue(),
                tbIds: fields.tbIdsOperSearch.getValue(),
                osbIds: fields.osbIdsOperSearch.getValue(),
                amountFrom: fields.amountFromOperSearch.getValue() ? parseFloat(fields.amountFromOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
                amountTo: fields.amountToOperSearch.getValue() ? parseFloat(fields.amountToOperSearch.getValue().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.')) : null,
                lexPurposeCrit: fields.standartConditionOperSearch.getValue(),
                lexPurposeAdd: fields.conditionOperSearch.getValue(),
                notOpok: fields.notCodeOperSearch.getValue(),
                notOpokTp: parseInt(fields.forBetweenOperSearch.getValue(), 10),
                opCat: fields.categoryOperSearch.getValue(),
                srcSys: fields.systemOperSearch.getValue(),
                partyType: fields.checkPayerCondition,
                custNm: fields.payerNameOperSearch.getValue(),
                custInn: fields.payerINNOperSearch.getValue(),
                custResident: fields.payerCountryOperSearch.getValue(),
                acctCrit: fields.payerStandartConditions.getValue(),
                acctAdd: fields.payerConditions.getValue(),
                acctNb: fields.payerAccountNumber.getValue(),
                acctArea: fields.payerRegionSearch.getValue(),
                cust2Nm: fields.payerNameOperSearchAdd.getValue(),
                cust2Inn: fields.payerINNOperSearchAdd.getValue(),
                cust2Resident: fields.payerCountryOperSearchAdd.getValue(),
                acct2Crit: fields.payerStandartConditionsAdd.getValue(),
                acct2Add: fields.payerConditionsAdd.getValue(),
                acct2Nb: fields.payerAccountNumberAdd.getValue(),
                acct2Area: fields.payerRegionSearchAdd.getValue(),
                custWatchList: vm.get('record.oes321TypeSender'),
                cust2WatchList: vm.get('record.oes321TypeTaker'),
                custNotWatchListFl: +vm.get('record.excludeSenderInn'),
                cust2NotWatchListFl: +vm.get('record.excludeTakerInn')
            },
            // alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            alertDetailsPanel = self.getView().down('app-alerts-alertdetailspanel'),
            oes321DetailsPanel = self.oes321DetailsPanel;

        Ext.Object.each(search, function (key, value) {
            if (Ext.isEmpty(value)) {
                delete search[key];
            }
        });
        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        Ext.widget({
            xtype: 'app-alerts-alertactionform-operationsearchform',
            viewModel: {
                type: 'alertactionoperationsearchformviewmodel',
                parent: vm,
                data: {
                    allAlertsCnt: self.getStore('alertsStore') ? self.getStore('alertsStore').getData().length : 0,
                    selectedAlertsCnt: self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelected().length ? self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelected().length : +(alertDetailsModel.getData().REVIEW_ID !== null),
                    selectedAlerts: self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection() : [alertDetailsModel],
                    isCancel: (typeof isCancelAction === 'boolean' && isCancelAction),
                    search: search,
                    scopeFilters: fields.scopeFilters,
                    isAlertDetailsPanel: ((alertDetailsPanel && alertDetailsPanel.getViewModel().get('alertId') !== null) || (oes321DetailsPanel && oes321DetailsPanel.getViewModel().get('alertId') !== null)) // если открыта детальная панель операции
                }
            }
        }).show();
    },

    // получаем Next and Previous => REVIEW_ID(alertID) и OES321ID для деталий сообщения
    getNextPreviousAlerts: function (alertId, store) {
        var selectedAlertIndexNP = false,
            nextAlertId,
            nextOES321Id,
            nextSelectedAlertIndex,
            previousAlertId,
            previousOES321Id,
            previousSelectedAlertIndex,
            dataNext,
            dataPrevious,
            thisIndex,
            thisAlertId,
            thisOES321Id,
            thisData;

        if (alertId !== null && alertId > 0 && store.data.items.length > 0) {
            Ext.Array.forEach(store.data.items, function (item, index) {
                if (selectedAlertIndexNP && store.findRecord('REVIEW_ID', parseInt(item.id, 10))) {
                    // обработка next or previous
                    dataNext = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                    if (!nextOES321Id && dataNext.OES321ID) {
                        nextAlertId = parseInt(item.id, 10);
                        nextOES321Id = dataNext.OES321ID;
                        nextSelectedAlertIndex = index;
                    }
                } else {
                    if (parseInt(item.id, 10) === parseInt(alertId, 10)) {
                        selectedAlertIndexNP = true;
                        thisIndex = index;
                        thisAlertId = parseInt(item.id, 10);
                        thisData = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                        thisOES321Id = thisData.OES321ID;
                    } else {
                        if (!thisOES321Id && item.id && store.findRecord('REVIEW_ID', parseInt(item.id, 10))) {
                            dataPrevious = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                            if (dataPrevious.OES321ID) {
                                previousAlertId = parseInt(item.id, 10);
                                previousOES321Id = dataPrevious.OES321ID;
                                previousSelectedAlertIndex = index;
                            }
                        }
                    }
                }
            });
        }

        return {
            thisIndex: thisIndex,
            thisAlertId: thisAlertId,
            thisOES321Id: thisOES321Id,
            nextAlertId: nextAlertId,
            nextOES321Id: nextOES321Id,
            nextSelectedAlertIndex: nextSelectedAlertIndex,
            previousAlertId: previousAlertId,
            previousOES321Id: previousOES321Id,
            previousSelectedAlertIndex: previousSelectedAlertIndex
        };
    },

    // получаем Next and Previous => REVIEW_ID(alertID) для деталий сообщения
    getNextPreviousAlertIDs: function (alertId, store) {
        var selectedAlertIndexNPA = false,
            nextAlertId,
            nextOES321Id,
            nextSelectedAlertIndex,
            previousAlertId,
            previousOES321Id,
            previousSelectedAlertIndex,
            dataNext,
            dataPrevious,
            thisIndex,
            thisAlertId,
            thisOES321Id,
            thisData;

        if (alertId !== null && alertId > 0 && store.data.items.length > 0) {
            Ext.Array.forEach(store.data.items, function (item, index) {
                if (parseInt(item.id, 10) === parseInt(alertId, 10)) {
                    selectedAlertIndexNPA = true;
                    thisIndex = index;
                    thisAlertId = parseInt(item.id, 10);
                    thisData = store.findRecord('REVIEW_ID', parseInt(item.id, 10)).getData();
                    thisOES321Id = thisData.OES321ID;

                    if (store.getAt(thisIndex + 1)) {
                        dataNext = store.getAt(thisIndex + 1).getData();
                        if (dataNext) {
                            nextAlertId = parseInt(dataNext._id_, 10);
                            nextOES321Id = dataNext.OES321ID;
                            nextSelectedAlertIndex = thisIndex + 1;
                        }
                    }
                    if (store.getAt(thisIndex - 1)) {
                        dataPrevious = store.getAt(thisIndex - 1).getData();
                        if (dataPrevious) {
                            previousAlertId = parseInt(dataPrevious._id_, 10);
                            previousOES321Id = dataPrevious.OES321ID;
                            previousSelectedAlertIndex = thisIndex - 1;
                        }
                    }
                }

            });
        }

        return {
            thisIndex: thisIndex,
            thisAlertId: thisAlertId,
            thisOES321Id: thisOES321Id,
            nextAlertId: nextAlertId,
            nextOES321Id: nextOES321Id,
            nextSelectedAlertIndex: nextSelectedAlertIndex,
            previousAlertId: previousAlertId,
            previousOES321Id: previousOES321Id,
            previousSelectedAlertIndex: previousSelectedAlertIndex
        };
    },

    checkGridSelection: function () {
        var self = this,
            alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            oes321DetailsPanel = self.oes321DetailsPanel,
            selectedAlertsCnt = self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length;

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        if (alertDetailsModel.getData().REVIEW_ID === null && !selectedAlertsCnt) {
            Ext.Msg.alert('', 'Пожалуйста, выберите операцию из списка.');
            return false;
        }
        return true;
    },

    showAlertReasons: function () {
        var self = this,
            vm = self.getViewModel(),
            grid = this.lookupReference('alertsOperSearchGrid'),
            alertDetailsPanel = self.lookupReference('alertDetailsPanel'),
            oes321DetailsPanel = self.oes321DetailsPanel;

        grid.getView().scrollRowIntoView(grid.getStore().indexOf(vm.get('selectedAlert')));

        // если открыта детальная панель операции, то создать модель для selectedAlerts с выбранным alertId:
        var alertDetailsModel = Ext.create('AML.model.Alerts');
        if (alertDetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', alertDetailsPanel.getViewModel().get('alertId'));
        }
        if (oes321DetailsPanel) {
            alertDetailsModel.set('REVIEW_ID', oes321DetailsPanel.getViewModel().get('alertId'));
        }
        Ext.widget({
            xtype: 'app-alerts-alertssnapshotform-form',
            viewModel: {
                type: 'alertssnapshotformviewmodel',
                parent: vm,
                data: {
                    selectedAlerts: self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection().length ? self.lookupReference('alertsOperSearchGrid').getSelectionModel().getSelection() : [alertDetailsModel]
                }
            }
        }).show();
    },

    setFilialsKGRKO: function () {
        var self = this,
            vm = self.getViewModel(),
            selectedAlerts = vm.get('selectedAlert'),
            selectedGrid = (self) ? self.lookupReference('alertsOperSearchGrid') : false,
            selectedGridModel = (selectedGrid) ? selectedGrid.getSelectionModel() : false,
            selectedGridSelect = (selectedGridModel) ? selectedGridModel.getSelected() : false,
            selectedAlertsCnt = (selectedGridSelect) ? selectedGridSelect.length : false,
            params, opCatCd, trxnSeqId;

        if (selectedAlertsCnt === 0) {
            Ext.Msg.alert('', 'Не выбрано ни одной операции!');
            return;
        }

        if (selectedAlertsCnt > 1) {
            Ext.Msg.alert('', 'Выделено более одной операции!');
            return;
        }
        if (selectedAlertsCnt === 1) {
            selectedAlerts = (vm) ? vm.get('selectedOSAlert') : false;
        }

        opCatCd = (selectedAlerts) ? selectedAlerts.get('OP_CAT_CD') : null;
        trxnSeqId = (selectedAlerts) ? selectedAlerts.get('TRXN_SEQ_ID') : null;
        params = {
            form: 'TrxnExtra',
            action: 'read',
            op_cat_cd: opCatCd,
            trxn_seq_id: (trxnSeqId) ? parseInt(trxnSeqId, 10) : null
        };

        for (var key in params) {
            if (Ext.isEmpty(params[key])) {
                delete params[key];
            }
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(params),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText), rec, rec2,
                    storeOSB = Ext.data.StoreManager.lookup('OES321Org');
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                if (responseObj.success) {
                    var dataResp = (responseObj.data) ? responseObj.data[0] : false;
                    if (dataResp) {
                        var double_kgrko_fl = (dataResp.SEND_OES_ORG_ID) ? 1 : 0,
                            double_kgrko_fl2 = (dataResp.RCV_OES_ORG_ID) ? 1 : 0,
                            SEND_ID = (dataResp.SEND_OES_ORG_ID) ? dataResp.SEND_OES_ORG_ID : null,
                            RCV_ID = (dataResp.RCV_OES_ORG_ID) ? dataResp.RCV_OES_ORG_ID : null,
                            CREATED_BY = (dataResp.CREATED_BY) ? dataResp.CREATED_BY : null,
                            CREATED_DATE = (dataResp.CREATED_DATE) ? dataResp.CREATED_DATE : null,
                            MODIFIED_BY = (dataResp.MODIFIED_BY) ? dataResp.MODIFIED_BY : null,
                            MODIFIED_DATE = (dataResp.MODIFIED_DATE) ? dataResp.MODIFIED_DATE : null;
                        if (double_kgrko_fl && SEND_ID) {
                            rec = (storeOSB) ? storeOSB.findRecord('OES_321_ORG_ID', SEND_ID, 0, false, false, true) : null;
                        }
                        if (double_kgrko_fl2 && RCV_ID) {
                            rec2 = (storeOSB) ? storeOSB.findRecord('OES_321_ORG_ID', RCV_ID, 0, false, false, true) : null;
                        }
                        Ext.widget({
                            xtype: 'app-alerts-alertactionform-kgrkofilials',
                            viewModel: {
                                type: 'alertactionkgrkofilialsviewmodel',
                                parentVM: vm,
                                data: {
                                    record: {
                                        double_kgrko_fl: double_kgrko_fl,
                                        double_kgrko_fl2: double_kgrko_fl2,
                                        tbIDForPayerKGRKOFilials: (rec) ? rec.get('TB_CODE') : null,
                                        tbOsbForPayerKGRKOFilials: (rec) ? rec.get('OSB_CODE') : null,
                                        tbIDForPayerKGRKOFilials2: (rec2) ? rec2.get('TB_CODE') : null,
                                        tbOsbForPayerKGRKOFilials2: (rec2) ? rec2.get('OSB_CODE') : null,
                                        CREATED_BY: CREATED_BY,
                                        CREATED_DATE: CREATED_DATE,
                                        MODIFIED_BY: MODIFIED_BY,
                                        MODIFIED_DATE: MODIFIED_DATE,
                                        oes_321_org_id: SEND_ID,
                                        oes_321_org_id2: RCV_ID
                                    },
                                    control2KGRKO: true,
                                    control2KGRKO2: true
                                }
                            }
                        }).show();
                    }
                }
            }
        });
    },

    openGOZChanges: function (opCatCd, trxnId) {
        var vm = this.getViewModel();

        Ext.create({
            xtype: 'app-monitoring-alerts-documentgozhistory-form',
            viewModel: {
                xtype: 'app-monitoring-alerts-documentgozhistory-form',
                data: {
                    isOpenReadOnly: true,
                    opCatCd: opCatCd,
                    trxnId: trxnId
                }
            }
        }).show();
    }
});