Ext.define('AML.view.main.Main', {
	extend: 'Ext.container.Container',
	requires: [
		'AML.view.main.MainController',
		'AML.view.main.MainViewModel',
		'AML.view.main.MainMenu',
		'Ext.layout.container.Border',
		'Ext.container.Container',
		'Ext.layout.container.HBox',
		'Ext.Component',
		'Ext.layout.container.VBox',
		'Ext.panel.Panel',
		'Ext.layout.container.Fit',
	],
	xtype: 'app-main',
	reference: 'app-main',
	id: 'app-main-id',
	controller: 'main',
	viewModel: {
		type: 'main'
	},
	layout: {
		type: 'border'
	},
	style: {
		background: '#fff'
	},
	items: [
		{
			xtype: 'container',
			cls: 'app-header',
			height: 50,
			region: 'north',
			layout: {
				type: "hbox",
				align: "middle"
			},
			items: [
				{
					xtype: 'component',
					cls: 'app-header-logo'
				},
				{
					xtype: 'component',
					cls: 'app-header-logo-fccm'
				},
				{
					xtype: 'container',
					cls: 'app-header-title',
					height: 50,
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'component',
							cls: 'app-header-title-aml',
							hidden: true,
							bind: {
								html: '{app.name}'
							}
						},
						{
							xtype: 'component',
							cls: 'app-header-title-module',
							hidden: true,
							html: 'Система отлова'
						}
					],
					flex: 1
				},
				{
					xtype: 'container',
					cls: 'app-header-user',
					height: 50,
					width: 200,
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'component',
							cls: 'app-header-user-name',
							bind: {
								html: '{user.name}'
							}
						},
						{
							xtype: 'component',
							cls: 'app-header-user-role',
							bind: {
								html: '{user.role}'
							}
						}
					],
					flex: 1
				}
			]
		},
		{
			xtype: 'app-main-menu',
			region: 'north'
		},
		{
			id: 'pageContainer',
			xtype: 'panel',
			reference: 'mainContainer',
			region: 'center',
			layout: 'fit',
			hidden: true
		}
	]
});
