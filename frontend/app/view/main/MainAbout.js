Ext.define('AML.view.main.MainAbout', {
	requires: [
		'Ext.layout.container.Fit',
		'Ext.container.Container'
	],
	extend: 'Ext.window.Window',
	xtype: 'app-main-about',
	closeAction: 'hide',
	width: 450,
	y: 200,
	bodyStyle: {
		background: '#fff',
		padding: '10px 20px 10px 20px'
	},
	iconCls: 'icon-exclamation',
	layout: 'fit',
	viewModel: {
		// todo: вынести данные во внешнюю модель/хранилище
		data: {
			title: 'О системе',
			// fixme: написать нормальное содержание раздела "О системе"
			text: 'Компания обладает необходимыми лицензиями и сертификатами для осуществления своей деятельности, в том числе, лицензиями Федеральной службы по техническому и экспортному контролю РФ, ФСБ России, Министерства обороны РФ, Свидетельствами СРО по строительству и проектированию и рядом других.  Система управления качеством компании сертифицирована на соответствие международному стандарту ISO 9001:2000 с 2004 года, ISO 9001:2008 с 2009 года.' +
			'<br><br>' +
			'&copy; 2014 - 2015 Инфосистемы Джет. Все права защищены.'
		}
	},
	bind: {
		title: '{title}'
	},
	items: [
		{
			xtype: 'container',
			bind: {
				html: '{text}'
			}
		}
	],
	listeners: {
		close: function () {
			// Чистим метку о последнем открытом окне
			AML.app.window.clearLast();
		}
	}
 });