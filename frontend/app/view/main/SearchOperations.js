Ext.define('AML.view.main.SearchOperations', {
    extend: 'Ext.Panel',
    requires: [
        'AML.view.main.SearchOperationsController',
        'AML.view.main.SearchOperationsModel',
        'Ext.Array',
        'Ext.button.Button',
        'Ext.container.ButtonGroup',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.Label',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Date',
        'Ext.grid.column.Number',
        'Ext.grid.column.Template',
        'Ext.grid.plugin.RowEditing',
        'Ext.layout.container.Border',
        'Ext.layout.container.HBox',
        'Ext.layout.container.Table',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        'Ext.toolbar.Fill',
        'Ext.data.StoreManager'
    ],
    xtype: 'main-search-operations-form',
    controller: 'mainsearchoperations',
    accessObject: 'Trxns',
    viewModel: {
        type: 'mainsearchoperationsvm'
    },
    layout: 'border',
    border: false,
    cls: 'icon-document-search',
    reference: 'main-search-operations-form',
    items: [
        {
            xtype: 'gridpanel',
            region: 'center',
            reference: 'alertsOperSearchGrid',
            bind: {
                store: {
                    bindTo: '{alertsStore}',
                    deep: true
                },
                // selection: {
                //     bindTo: '{selectedOSAlert}',
                //     deep: true
                // },
                hidden: '{menu.Alerts}' ? false : true
            },
            listeners: {
                selectionChange: function (selModel, record, index, options) {
                    var numbSelecRows = this.down('#numberSelectedRowsOperSearch');
                    numbSelecRows.removeAll();
                    if (selModel.getCount() > 1) {
                        numbSelecRows.add({
                            xtype: 'container',
                            bind: {
                                html: '<div class="counter">Кол-во выделенных операций: <b>' + selModel.getCount() + '</b></div>'
                            }
                        });
                    }
                },
                afterrender: function () {
                    //Get the first header td element
                    var elemetM = this.getView().headerCt.getHeaderAtIndex(0).el;//down('div');
                    elemetM.down('span').down('span').down('span').setStyle('display', 'inline-block');
                    var insertedElemToMain = elemetM.down('span').down('span').insertFirst({
                        tag: 'div',
                        cls: 'arrow-ico-closed',
                        style: 'width: 10px; height: 10px; cursor: hand; cursor: pointer;vertical-align: middle; display:inline-block'//float: right;
                    });
                    //
                    insertedElemToMain.on('click', function () {
                        var self = this,
                            parentGrid = self.getParent().getParent().getParent().getParent().getParent().getParent().getParent().getParent(),
                            vmParentM = parentGrid.component.up().getViewModel();
                        if (self.getAttributes()['class'].indexOf('arrow-ico-closed') !== -1) {
                            self.removeCls('arrow-ico-closed');
                            self.addCls('arrow-ico-opened');
                            //открыть все колонки
                            vmParentM.set('hideColumn', false);
                        } else if (self.getAttributes()['class'].indexOf('arrow-ico-opened') !== -1) {
                            self.removeCls('arrow-ico-opened');
                            self.addCls('arrow-ico-closed');
                            //скрыть все колонки
                            vmParentM.set('hideColumn', true);
                        }
                    });
                }
            },
            stateful: false,
            stateId: 'alerts',
            header: {
                xtype: 'toolbar',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'label',
                        style: {
                            fontWeight: 'bold'
                        },
                        bind: {
                            text: 'Поиск операций'
                        }
                    },
                    '->',
                    {
                        xtype: 'button',
                        text: 'Показать фильтры',
                        reference: 'toggleSearchOperFiltersBtn',
                        tooltip: 'Показать фильтры',
                        enableToggle: true,
                        pressed: true,
                        listeners: {
                            toggle: 'toggleSearchOperFilters'
                        }
                    }
                ]
            },
            tbar: [
                {
                    xtype: 'main-search-operations-topbar',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    flex: 1
                }
            ],
            bbar: [
                '->',
                {
                    xtype: 'container',
                    id: 'numberSelectedRowsOperSearch'
                    // bind: {
                    //     // hidden: '{OES_READONLY_FL_CONVERSION}'
                    // }
                },
                {
                    xtype: 'container',
                    bind: {
                        html: '<div class="counter">Загружено <b>{totalCount}</b> из <b>{grandTotal}</b></div>'
                    }
                },
                {
                    xtype: 'container',
                    bind: {
                        html: '<div class="counter">Всего: <b>{allCount}</b></div>',
                        hidden: '{!allCount}'
                    }
                },
                {
                    xtype: 'container',
                    bind: {
                        html: '<div class="counter">Выявленных: <b>{filteredCount}</b></div>',
                        hidden: '{!filteredCount}'
                    }
                },
                '->'
            ],
            columns: [
                {
                    tooltip: 'Открыть колонки',
                    width: 20,
                    align: 'center',
                    menuDisabled: true
                },
                {
                    text: 'Номер операции',
                    tooltip: 'Номер операции',
                    stateId: 'REVIEW_ID',
                    dataIndex: 'REVIEW_ID',
                    minWidth: 120,
                    align: 'left',
                    flex: 1
                },
                {
                    text: 'Статус операции',
                    tooltip: 'Статус операции',
                    stateId: 'RV_STATUS_NM',
                    dataIndex: 'RV_STATUS_NM',
                    xtype: 'templatecolumn',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl if="RV_STATUS_HIGHLIGHT == 1">',
                        '<span><b>{RV_STATUS_NM}</b></span>',
                        '<tpl else>',
                        '<span>{RV_STATUS_NM}</span>',
                        '</tpl>'
                    ),
                    width: 150,
                    align: 'left',
                    bind: {
                        hidden: '{hideColumn}'
                    }
                },
                {
                    text: 'Вид операции',
                    tooltip: 'Вид операции',
                    stateId: 'OPOK_NB',
                    dataIndex: 'OPOK_NB',
                    flex: 0.5,
                    minWidth: 50,
                    align: 'left',
                    bind: {
                        hidden: '{hideColumn}'
                    }
                },
                {
                    text: 'Доп. вид операции',
                    stateId: 'ADD_OPOKS_TX',
                    dataIndex: 'ADD_OPOKS_TX',
                    tooltip: 'Дополнительный вид операции',
                    flex: 0.5,
                    minWidth: 50,
                    align: 'left',
                    bind: {
                        hidden: '{hideColumn}'
                    }
                },
                {
                    text: 'Статус сообщения',
                    tooltip: 'Статус сообщения',
                    stateId: 'CHECK_STATUS',
                    dataIndex: 'CHECK_STATUS',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        switch (value) {
                            case 'W':
                                metaData.tdStyle = 'background:yellow;font-weight:bold;';
                                return 'Есть предупреждения';
                                break;
                            case 'E':
                                metaData.tdStyle = 'background:red;font-weight:bold;';
                                return 'Требует доработки';
                                break;
                            case 'O':
                                metaData.tdStyle = 'background:green;font-weight:bold;';
                                return 'Контроль пройден';
                                break;
                            case 'S':
                                return 'Отправлено';
                                break;
                            case null:
                                if (record.getData().OES321ID) {
                                    return 'Не проверялось';
                                } else {
                                    return '';
                                }
                                break;
                            default:
                                return '';
                                break;
                        }
                    },
                    minWidth: 160,
                    flex: 1,
                    align: 'left',
                    bind: {
                        hidden: '{hideColumn}'
                    }
                },
                {
                    text: 'Ответственный',
                    tooltip: 'Ответственный',
                    stateId: 'RV_OWNER_ID',
                    dataIndex: 'RV_OWNER_ID',
                    width: 100,
                    align: 'left',
                    bind: {
                        hidden: '{hideColumn}'
                    }
                },
                {
                    text: 'Комментарий',
                    tooltip: 'Комментарий',
                    stateId: 'RV_NOTE_TX',
                    dataIndex: 'RV_NOTE_TX',
                    renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                        if (record.data.RV_NOTE_TX) {
                            metadata.tdAttr = metadata.tdAttr + ' data-qtip="' + Ext.util.Format.stripTags(Ext.util.Format.stripScripts(record.data.RV_NOTE_TX)) + '"';
                        }
                        return Ext.util.Format.stripScripts(value);
                    },
                    flex: 3,
                    align: 'left',
                    minWidth: 250,
                    bind: {
                        hidden: '{hideColumn}'
                    }
                },
                {
                    text: 'Назначение платежа',
                    tooltip: 'Назначение платежа',
                    stateId: 'TRXN_DESC',
                    dataIndex: 'TRXN_DESC',
                    minWidth: 150,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Сумма в рублях',
                    tooltip: 'Сумма в рублях',
                    stateId: 'TRXN_BASE_AM',
                    dataIndex: 'TRXN_BASE_AM',
                    xtype: 'numbercolumn',
                    format: '0.00',
                    align: 'right',
                    minWidth: 150,
                    flex: 1
                },
                {
                    text: 'Сумма в валюте',
                    tooltip: 'Сумма в валюте',
                    stateId: 'TRXN_CRNCY_AM',
                    dataIndex: 'TRXN_CRNCY_AM',
                    xtype: 'numbercolumn',
                    format: '0.00',
                    align: 'right',
                    minWidth: 150,
                    flex: 1
                },
                {
                    text: 'Валюта',
                    tooltip: 'Валюта',
                    stateId: 'TRXN_CRNCY_CD',
                    dataIndex: 'TRXN_CRNCY_CD',
                    align: 'left',
                    minWidth: 50,
                    flex: 0.5
                },
                {
                    text: 'Дата операции',
                    tooltip: 'Дата операции',
                    stateId: 'TRXN_DT',
                    dataIndex: 'TRXN_DT',
                    xtype: 'datecolumn',
                    format: 'd.m.Y',
                    align: 'left',
                    minWidth: 100,
                    flex: 1
                },
                {
                    text: 'ТБ',
                    tooltip: 'ТБ',
                    stateId: 'TB_NM',
                    dataIndex: 'TB_NM',
                    align: 'left',
                    minWidth: 150,
                    flex: 1
                },
                {
                    text: 'Система-источник',
                    tooltip: 'Система-источник',
                    stateId: 'SRC_SYS_CD',
                    dataIndex: 'SRC_SYS_CD',
                    align: 'left',
                    minWidth: 50,
                    flex: 1
                },
                {
                    text: 'Номер документа',
                    tooltip: 'Номер документа',
                    stateId: 'TRXN_DOC_ID',
                    dataIndex: 'TRXN_DOC_ID',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Межфилиальная операция',
                    tooltip: 'Межфилиальная операция',
                    stateId: '',
                    dataIndex: 'RENDER_ATTACHMENTS',
                    flex: 0.5,
                    renderer: function (value, metadata, record) {
                        if (record.data.IS_INTER_KGRKO && !record.data.HAS_ATTACHMENTS) {
                            metadata.tdCls = 'icon-document-kgrko';
                            record.data.RENDER_ATTACHMENTS = 1;
                        }
                        if (!record.data.IS_INTER_KGRKO && !record.data.HAS_ATTACHMENTS) {
                            metadata.tdCls = '';
                            record.data.RENDER_ATTACHMENTS = 0;
                        }
                        return '';
                    }
                },
                {
                    xtype: 'datecolumn',
                    format: 'd.m.Y',
                    text: 'Дата документа',
                    tooltip: 'Дата документа',
                    stateId: 'TRXN_DOC_DT',
                    dataIndex: 'TRXN_DOC_DT',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Наименование плательщика',
                    tooltip: 'Наименование плательщика',
                    stateId: 'ORIG_NM',
                    dataIndex: 'ORIG_NM',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'ИНН плательщика',
                    tooltip: 'ИНН плательщика',
                    stateId: 'ORIG_INN_NB',
                    dataIndex: 'ORIG_INN_NB',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Счет плательщика',
                    tooltip: 'Счет плательщика',
                    stateId: 'ORIG_ACCT_NB',
                    dataIndex: 'ORIG_ACCT_NB',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Счет дебета',
                    tooltip: 'Счет дебета',
                    stateId: 'DEBIT_CD',
                    dataIndex: 'DEBIT_CD',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Банк плательщика',
                    tooltip: 'Банк плательщика',
                    stateId: 'SEND_INSTN',
                    dataIndex: 'SEND_INSTN',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'БИК банка',
                    tooltip: 'БИК банка',
                    stateId: 'SEND_INSTN_BIK',
                    dataIndex: 'SEND_INSTN_BIK',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Корр.счет банка',
                    tooltip: 'Корр.счет банка',
                    stateId: 'SEND_INSTN_ACCT_ID',
                    dataIndex: 'SEND_INSTN_ACCT_ID',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Наименование получателя',
                    tooltip: 'Наименование получателя',
                    stateId: 'BENEF_NM',
                    dataIndex: 'BENEF_NM',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'ИНН получателя',
                    tooltip: 'ИНН получателя',
                    stateId: 'BENEF_INN_NB',
                    dataIndex: 'BENEF_INN_NB',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Счет получателя',
                    tooltip: 'Счет получателя',
                    stateId: 'BENEF_ACCT_NB',
                    dataIndex: 'BENEF_ACCT_NB',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Счет кредита',
                    tooltip: 'Счет кредита',
                    stateId: 'CREDIT_CD',
                    dataIndex: 'CREDIT_CD',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Банк получателя',
                    tooltip: 'Банк получателя',
                    stateId: 'RCV_INSTN',
                    dataIndex: 'RCV_INSTN',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'БИК банка',
                    tooltip: 'БИК банка',
                    stateId: 'RCV_INSTN_BIK',
                    dataIndex: 'RCV_INSTN_BIK',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Корр.счет банка',
                    tooltip: 'Корр.счет банка',
                    stateId: 'RCV_INSTN_ACCT_ID',
                    dataIndex: 'RCV_INSTN_ACCT_ID',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'ID операции',
                    tooltip: 'ID операции',
                    stateId: '_id_',
                    dataIndex: '_id_',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left'
                },
                {
                    text: 'Вид операции (ЦАС НСИ)',
                    tooltip: 'Вид операции (ЦАС НСИ)',
                    stateId: 'NSI_OP',
                    dataIndex: 'NSI_OP',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left',
                    hidden: true
                    // bind: {
                    //     hidden: '{hideColumn}'
                    // }
                },
                {
                    text: 'Дата загрузки',
                    tooltip: 'Дата загрузки',
                    stateId: 'DATA_DUMP_DT',
                    dataIndex: 'DATA_DUMP_DT',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left',
                    hidden: true
                    // bind: {
                    //     hidden: '{hideColumn}'
                    // }
                },
                {
                    text: 'Дата изменения в системе источнике',
                    tooltip: 'Дата изменения в системе источнике',
                    stateId: 'ORIG_REG_DT',
                    dataIndex: 'ORIG_REG_DT',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left',
                    hidden: true
                    // bind: {
                    //     hidden: '{hideColumn}'
                    // }
                },
                {
                    text: 'Код в системе-источнике',
                    tooltip: 'Код в системе-источнике',
                    stateId: 'SRC_CD',
                    dataIndex: 'SRC_CD',
                    minWidth: 100,
                    flex: 0.5,
                    align: 'left',
                    hidden: true
                    // bind: {
                    //     hidden: '{hideColumn}'
                    // }
                },
                {
                    text: 'Вид операции ГОЗ (списание)',
                    tooltip: 'Вид операции ГОЗ (списание)',
                    stateId: 'ORIG_GOZ_OP_CD',
                    dataIndex: 'ORIG_GOZ_OP_CD',
                    flex: 1,
                    hidden: true,
                    // bind: {
                    //     hidden: '{hideColumn}'
                    // }
                    renderer: function(value, metaData) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    }
                },
                {
                    text: 'Вид операции ГОЗ (зачисление)',
                    tooltip: 'Вид операции ГОЗ (зачисление)',
                    stateId: 'BENEF_GOZ_OP_CD',
                    dataIndex: 'BENEF_GOZ_OP_CD',
                    flex: 1,
                    hidden: true,
                    // bind: {
                    //     hidden: '{hideColumn}'
                    // }
                    renderer: function(value, metaData) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    }
                }
            ]
        },
        {
            xtype: 'app-view-main-bottompanelopersearch',
            isFooter: true,
            region: 'south',
            height: '55%',
            maxHeight: 450,
            minHeight: 130,
            collapsed: true,
            collapsible: false,
            collapseMode: 'mini',
            header: false,
            resizable: true,
            cls: 'app-opoktest-footer',
            split: false,
            layout: {
                type: 'vbox'
            },
            bodyStyle: {
                padding: '5px'
            },
            defaults: {
                bodyStyle: {
                    padding: '3px',
                    border: 0
                }
            },
            scrollable: true,
            bind: {
                collapsed: '{!showBBarOperSearch}'
            }
        }
    ]
});