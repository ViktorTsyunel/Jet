Ext.define('AML.view.main.MainViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.main',
	data: {
		app: {
			name: 'AML'
		},
		user: {
			name: '',
			role: ''
		},
        currentUser: null,
        menu: {
            AssignRule: false,
            AssignReplacement: false,
            AssignLimit: false,
            OPOK: false,
            OPOKCriteria: false,
            OPOKRule: false,
            OPOKTest: false,
            OES321Org: false,
            OES321PartyOrg: false
        },
        OPOKControlMenu: null,
        AssignMenu: null,
        OESMenu: null,
        createAlertReviewId: null,
        createAlertOES321Id: null,
        ImpType: true
	},
    formulas: {
        OPOKControlMenu: {
            bind: {
                OPOK: '{menu.OPOK}',
                OPOKCriteria: '{menu.OPOKCriteria}',
                OPOKRule: '{menu.OPOKRule}',
                OPOKTest: '{menu.OPOKTest}'
            },
            get: function(data){
                return data.OPOK
                    || data.OPOKCriteria
                    || data.OPOKTest
                    || data.OPOKRule;
            }
        },
        AssignMenu: {
            bind: {
                AssignRule: '{menu.AssignRule}',
                AssignReplacement: '{menu.AssignReplacement}',
                AssignLimit: '{menu.AssignLimit}'
            },
            get: function(data){
                return data.AssignRule
                    || data.AssignLimit
                    || data.AssignReplacement;
            }
        },
        OESMenu: {
            bind: {
                OES321Org: '{menu.OES321Org}',
                OES321PartyOrg: '{menu.OES321PartyOrg}',
                OESCheckRule: '{menu.OESCheckRule}'
            },
            get: function(data){
                return data.OES321Org
                    || data.OES321PartyOrg
                    || data.OESCheckRule;
            }
        },
        AdminMenu: {
            bind: {
                OPOKControlMenu: '{OPOKControlMenu}',
                AssignMenu: '{AssignMenu}',
                ImpType: '{ImpType}'

            },
            get: function(data){
                return data.OPOKControlMenu
                    || data.AssignMenu
                    || data.ImpType;
            }
        }
    }
});