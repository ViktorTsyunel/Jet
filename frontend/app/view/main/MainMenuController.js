Ext.define('AML.view.main.MainMenuController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.mainmenu',
    config: {
        listen: {
            component: {
                'app-main-menu [isPage]': {
                    click: 'showPage'
                },
                'app-main-menu [isWindow]': {
                    click: 'showWindow'
                },
                'app-main-menu [isWidget]': {
                    click: 'showWidget'
                },
                'app-main-menu': {
                    beforerender: 'setVisible'
                }
            }
        }
    },
    /**
     * Отображаем страницу
     * @param cfg
     */
    showPage: function (cfg) {
        AML.app.page.show(cfg.pageName, Ext.getCmp('pageContainer'));
    },
    /**
     * Отображаем окно
     * @param cfg
     */
    showWindow: function (cfg) {
        AML.app.window.show(cfg.windowName);
    },
    /**
     * Отображаем widget
     * @param cfg
     */
    showWidget: function (cfg) {
        var widget = Ext.widget(cfg.widgetName);
        widget.show();
    },
    /**
    * Создание ОЭС, но предварительно проверяем не открыта ли старая ОЭС
    */
   checkAndCreateNewOES: function () {
        Ext.create({
            xtype: 'app-monitoring-create-createalertform',
            viewModel: {
                xtype: 'viewmodel.createalertformvm',
                data: {
                    record: {
                        currentUser: null,
                        opok_nb: null,
                        tb_id: null,
                        osb_id: null,
                        // due_dt: null,
                        // owner_id: null,
                        note_tx: null
                    }
                }
            }
        }).show();

        if(this.getView().up().down('app-alerts-oes321detailspanel')) {
            this.getView().up().down('app-alerts-oes321detailspanel').controller.closePanel();
        }
    },

    /**
     * Делаем видимыми пункты меню в соответствии с доступными объектами
     * @param cfg
     */
    setVisible: function (cfg) {
        var store = Ext.getStore('ObjectsAccess');
        var vm = this.getViewModel();
        store.each(function(record){
            //присваиваем роли в соотв. с правами доступа
            vm.set('menu.' + record.getId(), true);
            if (record.getId() === 'WLType' || record.getId() === 'WLOrg') {
                vm.set('WLMenu', true);
            }
            //для Alerts + mode "T" создаем доп. роль
            vm.set(record.getId() + '_READONLY', false);
            var i = store.findExact('id', record.getId());
            if (i > -1) {
                if (store.getAt(i).get('mode') == 'R' || store.getAt(i).get('mode') == 'T') vm.set(record.getId() + '_READONLY', true);
            }
        });
    }
});
