Ext.define('AML.view.main.MainController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.main',
	requires: [
		'Ext.JSON'
	],
	config: {
		listen: {
			store: {
				'*': {
					beforeload: 'storeBeforeload',
					load: 'storeLoad'
				}
			}
		}
	},
	init: function () {
		// Загружаем страницу открытую в прошлый раз или по умолчанию
		AML.app.page.show(null, Ext.getCmp('pageContainer'), 'app-monitoring-alerts');
		// Загружаем окно не закрытое в прошлый раз
		AML.app.window.show(null);
		this.callParent(arguments);
	},
	/**
	 * Выполняем действие перед загрузкой хранилища
	 * @param store
	 * @param operation
	 * @param eOpts
	 */
	// todo: подумать - а нужно ли это!
	storeBeforeload: function (store, operation, eOpts) {
	},
	/**
	 * Выполняем действие во время загрузки хранилища
	 * @param store
	 * @param records
	 * @param successful
	 * @param eOpts
	 */
	// todo: подумать - а нужно ли это!
	storeLoad: function (store, records, successful, eOpts) {
		/*if (!successful) {
			if (store.proxy) {
				var message = eOpts.error.response && Ext.JSON.decode(eOpts.error.response.responseText).message || eOpts.error || 'Ошибка: сервер не ответил.';
				if (!store.proxy.isRedirectToLogin(message)) {
					AML.app.msg.toast({
						type: 'error',
						message: message
					});
				}
			}
		}*/
	}
});
