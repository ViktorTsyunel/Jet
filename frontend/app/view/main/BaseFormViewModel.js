Ext.define('AML.view.main.BaseFormViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.baseformviewmodel',
	data: {
		record: null,  // запись с данными для отображения/редактирования (ссылка на строку store)
		isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
	}
});
