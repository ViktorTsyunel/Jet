Ext.define('AML.view.main.BaseListController', {
    requires: [
        'Ext.data.StoreManager',
        'AML.model.OPOK'
    ],
    extend: 'Ext.app.ViewController',
    alias: 'controller.baselist',
    childForm: null, // xtype формы редактирования, открываемой из списка
    IdColumn: null, //name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя
    accessObject: null, // код объекта доступа для данной формы
    isReadOnly: false, //флаг, что форма открыта в режиме "только чтение"
    doRemove: function (store, records) { //функция удаления выделенных во view записей из store
        // на всякий случай: отменяем локальные изменения в store (чтобы при синхронизации были только наши удаления)
        store.rejectChanges();
        // удаляем из store
        store.remove(records);
        // синхронизируем БД со store
        store.sync({
            failure: store.syncFailure
        });
    },
    setReadOnly: function (flag) { //функция перевода формы в режим "только чтение"*/
        // все кнопки делаем disable/enable
        var grid = this.getView();
        if (!grid) return;

        var buttons = Ext.Array.forEach(grid.query('button'), function(item, i, arr) { item.setDisabled(flag); });
        this.isReadOnly = flag;

        // делаем disable кнопке "Удалить", если нет выделенных строк
        if (flag==false && grid.getSelection() && grid.getSelection().length == 0) grid.down('#remove').setDisabled(true);
    },
    config: {
        listen: {
            component: {
                'gridpanel': {
                    rowkeydown: function (table, record, tr, rowIndex, e, eOpts) {
                        switch (e.getKey()) {
                            case e.ENTER:
                            case e.SPACE:
                                this.onClickOpenForm(record);
                                break;
                            case e.BACKSPACE:
                            case e.DELETE:
                                this.onClickRemove();
                                break;
                        }
                    }
                },
                '#': {
                    beforerender: 'onBeforeRender',
                    selectionchange: 'onSelectionChange',
                    rowdblclick: 'onDBClickOpenForm'
                },
                '#create': {
                    click: 'onClickCreate'
                },
                '#remove': {
                    click: 'onClickRemove'
                }
            }
        }
    },
    onBeforeRender: function(grid, eOpts) {
        // определим права пользователя на текущий объект
        if (!this.accessObject) return;

        var store = Ext.getStore('ObjectsAccess');
        if (!store) return;

        var i = store.findExact('id', this.accessObject);

        // если нет никакого доступа - не показываем форму
        if (i == -1) {
            grid.setHidden(true);
            return;
        }

        // если доступ на чтение - переводим форму в режим "только чтение"
        if (store.getAt(i).get('mode') == 'R') this.setReadOnly(true);
        if ((this.accessObject == 'OPOKCriteria') && (store.getAt(i).get('mode') == 'T')) this.getView().down('#remove').setDisabled(true);
    },
    onSelectionChange: function (model, selected, eOpts) {
        // определим права пользователя на текущий объект
        if (!this.accessObject) return;

        var store = Ext.getStore('ObjectsAccess');
        if (!store) return;

        var i = store.findExact('id', this.accessObject);

        if (!this.isReadOnly) {
            var grid = this.getView();
            grid.down('#remove').setDisabled(!selected.length);
        }
        if ((store.getAt(i).get('mode') == 'T') && (this.accessObject == 'OPOKCriteria')) {
            var grid = this.getView();
            grid.down('#remove').setDisabled(true);
        }
    },
    onDBClickOpenForm: function (grid, record, index, eOpts) {
        var formType = this.childForm;

        if (formType) {
            var form = Ext.widget({
                xtype: formType,
                viewModel: {
                    data: {
                        record: record,
                        isOpenReadOnly: this.isReadOnly
                    },
                    parent: this.getViewModel()
                }
            }).show();
        }
    },
    onClickCreate: function (button, event, eOpts) {
        var grid = this.getView();

        if (grid) {
            var model, record;
            var store = grid.getStore();

            // определим model записи, которую надо создать
            if (store && store.getSource) { store = store.getSource(); }
            if (store) { model = store.getModel().getName(); }

            // создаем новую запись
            if (model) {
                record = Ext.create(model, {});
                grid.store.insert(0, record);
                grid.getSelectionModel().select(0);
                grid.getView().focusRow(0);
                // открываем окно редактирования для нее
                grid.fireEvent('rowdblclick', grid, record);
            }
        }
    },
    /**
     * Открытие формы
     */
    onClickOpenForm: function (record) {
        var grid = this.getView();
        grid.fireEvent('rowdblclick', grid, record);
    },
    /**
     * Удаление
     */
    onClickRemove: function (button, event, eOpts) {
        var grid = this.getView();
        if (!grid) return false;

        // определим права пользователя на текущий объект
        if (!this.accessObject) return false;

        var storeObjAccess = Ext.getStore('ObjectsAccess');
        if (!storeObjAccess) return false;

        var indexPosition = storeObjAccess.findExact('id', this.accessObject);
        if (indexPosition === -1) {
            return false;
        }
        if (this.isReadOnly
            || (storeObjAccess.getAt(indexPosition).get('mode') === 'T')
            || (storeObjAccess.getAt(indexPosition).get('mode') === 'R')) {
            return false;
        }

        // получим выделенные (удаляемые) записи
        var sm = grid.getSelectionModel();
        if (!sm) return false;

        var records = sm.getSelection();
        if (!records || records.length == 0) return false;

        // определим store, из которого надо удалить записи (с учетом возможного chained store)
        var store = grid.getStore();
        if (store && store.getSource) store = store.getSource();
        if (!store) return false;

        // сформируем перечень кодов удаляемых записей - укажем их в запрашиваемом у пользователя подтверждении удаления
        var codes;
        if (this.IdColumn) {
            var i;
            for (i = 0; i < records.length; ++i) {
                if (!codes) { codes = records[i].data[this.IdColumn].toString(); }
                else { codes = codes.concat(', ').concat(records[i].data[this.IdColumn].toString()); }
            }
        }

        var msg;
        if (codes) { msg = 'Вы действительно хотите удалить ' + (records.length > 1 ? 'записи с кодами: ' + codes + ' (' + records.length.toString() + ' шт.)': 'запись с кодом: ' + codes) + ' ?'; }
        else { msg = 'Вы действительно хотите удалить ' + (records.length > 1 ? ' записей: ' + records.length.toString() : 'выделенную запись') + ' ?'; }

        // запрашиваем подтверждение и удаляем
        fnRemove = this.doRemove;
        AML.app.msg.confirm({
            message: msg,
            fnYes: function() {
                // удаляем записи из БД (синхронным запросом - чтобы пользователь не мог, например, удалить второй раз то же самое)
                var async = Ext.Ajax.getAsync();
                Ext.Ajax.setAsync(false);

                fnRemove(store, records);

                Ext.Ajax.setAsync(async);
            }
        });
    },
    /**
     * Обновляем хранилище
     */
    onStoreRefresh: function () {
        // определяем: какой store надо загрузить (с учетом возможного chained store)
        var grid = this.getView();
        if (grid) {
            var store = grid.getStore();
            if (store && store.getSource) { store = store.getSource(); }
            if (store) { store.load(); }
        }
    }
});
