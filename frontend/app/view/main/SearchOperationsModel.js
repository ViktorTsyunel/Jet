Ext.define('AML.view.main.SearchOperationsModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.mainsearchoperationsvm',
	data: {
		title: 'Поиск операций',
        footerRecord: null,
        showFilter: false,
        showBBar: false,
        selectedOSAlert: null,
        scopeFilters: '9001',
        newCountAlert: 0,
        newRvTrxnSeqIDs: [],
        filteredCount: '',
        allCount: '',
        hideColumn: true,
        record: {
            payerINNOperSearch: null, // ИНН Плательщика
            payerINNOperSearchAdd: null, // ИНН Получателя
            dtFromOperSearch: null,
            dtToOperSearch: null,
            amountFromOperSearch: 600000,
            amountToOperSearch: null,
            standartConditionOperSearch: null,
            conditionOperSearch: null,
            tbIdsOperSearch: null,
            osbIdsOperSearch: null,
            notCodeOperSearch: null,
            categoryOperSearch: null,
            systemOperSearch: null,
            forBetweenOperSearch: null,
            payerNameOperSearch: null,
            payerCountryOperSearch: null,
            payerNameOperSearchAdd: null,
            payerCountryOperSearchAdd: null,
            payerStandartConditions: null,
            payerAccountNumber: null,
            payerConditions: null,
            payerRegionSearch: null,
            payerStandartConditionsAdd: null,
            payerAccountNumberAdd: null,
            payerConditionsAdd: null,
            payerRegionSearchAdd: null,
            checkPayerCondition: 'O',
            oes321TypeSender: null,
            oes321TypeTaker: null,
            excludeSenderInn: false,
            excludeTakerInn: false
        },
        statistics: {
            "WORK_QTY": 0,
            "NEARDUE_QTY": 0,
            "OVERDUE_QTY": 0
        },
        fetchId: null
	},
    formulas: {
        footerRecord: function (get) {
            return get('selectedOSAlert');
        },
        coloredTRXN_DESC: function (get) {
            var text = get('footerRecord.TRXN_DESC'),
                lexeme = get('footerRecord.TRXN_DESC_LEX');
            return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
        },
        coloredORIG_NM: function (get) {
            var text = get('footerRecord.ORIG_NM'),
                lexeme = get('footerRecord.ORIG_LEX');
            return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
        },
        coloredBENEF_NM: function (get) {
            var text = get('footerRecord.BENEF_NM'),
                lexeme = get('footerRecord.BENEF_LEX');
            return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
        },
        RV_TRXN_SEQ_ID: function(get) {
            return get('selectedOSAlert.RV_TRXN_SEQ_ID') <= 0;
        },
        selectedOSAlert: {
            bind: {
                bindTo: '{alertsOperSearchGrid.selection}',
                deep: true
            },
            get: function(record) {
                return record;
            },
            set: function(record) {
                if(!record.isModel) {
                    var sRec = this.get('records').getById(record);
                }
                if(sRec) {
                    this.set('selectedOSAlert', sRec);
                } else {
                    this.set('selectedOSAlert', record);
                }
            }
        }
    },
    stores: {
	    // Виды перечней лиц и организаций
        oes321types: {
            model: 'AML.model.OES321TypeOrg',
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true
        },
        // Выявленные операции
        alertsStore: {
            model: 'AML.model.Alerts',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: false,
            listeners: {
                load: 'onLoadOperSearchAlerts'
            },
            sorters: [
                {
                    property: 'SORT_KEY',
                    direction: 'ASC'
                }
            ]
        },

        // Справочник Тип клиента
        typeClientStore: {
            fields: ['id', 'text'],
            data: [
                {id: 1, text: 'ЮЛ'},
                {id: 2, text: 'ФЛ'},
                {id: 3, text: 'ИП'}
            ]
        },
        // Справочник Область поиска
        searchAreaStore: {
            fields: ['id', 'text'],
            data: [
                {id: 'ALL', text: 'Везде'},
                {id: 'CUST', text: 'Справочник клиентов'},
                {id: 'OES', text: 'Сообщения ОЭС'}
            ]
        },

        DOCTYPEStoreSearchClient: {
            type: 'chained',
            source: 'DOCTYPEREF'
        },
        
        TUViewStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'участник отсутствует'},
                {id: 1, text: 'ЮЛ'},
                {id: 2, text: 'ФЛ'},
                {id: 3, text: 'ИП'},
                {id: 4, text: 'Участник не определен'}
            ]
        },

        categoryViewStore: {
            fields: ['id', 'iddb', 'label'],
            data: [
                {id: 0, iddb: 'CT', label: 'Нал.'},
                {id: 1, iddb: 'WT', label: 'Б/нал.'},
                {id: 2, iddb: 'BOT', label: 'Внутр.'}
            ]
        },

        forBetweenStore: {
            fields: ['id', 'all', 'label'],
            data: [
                {id: 0, all: '0 - хотя бы в одном из филиалов', label: 'хотя бы в одном из филиалов'},
                {id: 1, all: '1 - в филиале по дебету', label: 'в филиале по дебету'},
                {id: 2, all: '2 - в филиале по кредиту', label: 'в филиале по кредиту'}
            ]
        },

        systemViewStore: {
            fields: ['id', 'label'],
            data: [
                {id: 0, label: 'EKS'},
                {id: 1, label: 'BCK'},
                {id: 2, label: 'NAV'},
                {id: 3, label: 'GAT'}
            ]
        },

        payerCountryViewStore: {
            fields: ['id', 'iddb', 'label'],
            data: [
                {id: 0, iddb: 'Y', label: 'РФ'},
                {id: 1, iddb: 'N', label: 'не РФ'}
            ]
        },

        // Справочник стандартных условий выявления (таблица)
        opokcriteria: {
            model: 'AML.model.OPOKCriteria',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'CRIT_CD',
                    direction: 'ASC'
                }
            ],
            filters: [
                {
                    property: 'CRIT_TP_CD',
                    value: 1
                }
            ]
        }
    }
});
