Ext.define('AML.view.main.BaseFormController', {
    requires: [
        'Ext.data.StoreManager'
    ],
    extend: 'Ext.app.ViewController',
    alias: 'controller.baseform',
    accessObject: null, // код объекта доступа для данной формы
    isReadOnly: false, //флаг, что форма открыта в режиме "только чтение"
    forceClose: false, //внутренний флаг, что форму следует принудительно закрыть (т. к. к ней нет доступа)
    // функция определяет: есть ли изменения (т. е. требуется ли сохранение)
    isChanges: function(window) {
        var record = window.getViewModel().get('record');
        if (!record) return false;

        if (record.store && record.store.isChanges()) { return true; }
        else { return false; }
    },
    // функция, возвращающая предупреждение для пользователя перед сохранением данных (если возвращает null - без предупреждения)
    getSaveConfirmText: function(form) {
        return null;
    },
    //функция сохранения редактировавшейся записи
    doSave: function (store, record, window, closeFlag, fnCallback) {
        //store.sync({
        record.save({
            success: function (batch, options) {
                //options.setRecords(options.getResultSet().records)
                if (closeFlag) window.close();
            },
            scope: window,
            callback: fnCallback,
            failure: store.syncFailure
        });
    },
    //функция отмены изменений в редактировавшейся записи
    doReject: function (store, record) {
        store.rejectChanges();
    },
    // функция сохранения данных формы
    saveForm: function (window, controller, closeFlag, fnCallback) {
        if (!window || !controller) return;

        var form = window.down('form').getForm();
        if (!form || !form.isValid()) return;

        // получим редактировавшуюся запись и соответствующий store
        var record = window.getViewModel().get('record');
        if (!record) return;

        var store = record.store;
        if (!store) return;

        // если надо - спрашиваем подтверждение перед сохранением у пользователя
        var msg = controller.getSaveConfirmText(form);
        fnSave = controller.doSave;

        if (msg) {
            AML.app.msg.confirm({
                type: 'warning',
                message: msg,
                fnYes: function() {
                    // сохраняем изменения синхронным запросом - чтобы пользователь не мог, например, сохранить второй раз то же самое
                    var async = Ext.Ajax.getAsync();
                    Ext.Ajax.setAsync(false);

                    fnSave.call(controller, store, record, window, closeFlag, fnCallback);

                    Ext.Ajax.setAsync(async);
                }
            });
        }
        // если подтверждение не нужно - молча сохраняем изменившиеся данные
        else {
            var async = Ext.Ajax.getAsync();
            Ext.Ajax.setAsync(false);

            fnSave.call(controller, store, record, window, closeFlag, fnCallback);

            Ext.Ajax.setAsync(async);
        }
    },
    setReadOnly: function (flag) { //функция перевода формы в режим "только чтение"*/
        var window = this.getView();
        if (!window) return;

        // все кнопки, не являющиеся табами, кроме Закрыть, делаем disable/enable
        var buttons = Ext.Array.forEach(window.query('button'), function(item, i, arr) { if (item.xtype && item.xtype == 'tab') {} else { item.setDisabled(flag); } });
        (window.down('#cancelForm')) ? window.down('#cancelForm').setDisabled(false) : {};

        // все элементы делаем read only/не readonly
        window.down('form').cascade(function (f) {
            f.setReadOnly && f.setReadOnly(flag);
        });

        this.isReadOnly = flag;
    },
    config: {
        listen: {
            component: {
                '#': {
                    show: 'onShowForm',
                    close: 'onCloseForm',
                    beforerender: 'onBeforeRender',
                    /**
                     * Обрабатываем событие отрисовки
                     * @param window
                     */
                    render: function (window) {
                        var me = this;
                        window.getEl().on('keypress', function (e) {
                            if (e.getKey() == e.ESC) {
                                me.onClickCancelForm();
                            }
                        });
                        if (Ext.isIE8) {
                            window.setWidth(Math.round(document.body.clientWidth / 100 * 80));
                        }
                    }
                },
                '#saveForm': {
                    click: 'onClickSaveForm'
                },
                '#cancelForm': {
                    click: 'onClickCancelForm'
                }
            }
        }
    },
    /**
     * Метод для события отображения формы
     * @param window
     * @param eOpts
     */
    onShowForm: function (window, eOpts) {
        if (this.forceClose) window.close();
    },
    /**
     * Метод для события закрытия формы
     * @param window
     * @param eOpts
     */
    onCloseForm: function (window, eOpts) {
        // если есть несохраненные изменения  - откатываем их
        if (window && this.isChanges(window)) {
            // определим store, в котором надо откатить несохраненные изменения (если они есть)
            var record = window.getViewModel().get('record');
            if (!record) return;

            var store = record.store;
            if (!store) return;

            // откатываем изменения
            this.doReject(store, record);
        }
    },
    onBeforeRender: function(window, eOpts) {
        // определим и отработаем права пользователя на текущий объект
        if (this.accessObject) {
            var store = Ext.getStore('ObjectsAccess');

            if (store) {
                var i = store.findExact('id', this.accessObject);

                // если нет никакого доступа - не показываем форму
                if (i == -1) {
                    this.forceClose = true;
                    return;
                }
                // если доступ на чтение - переводим форму в режим "только чтение"
                if (store.getAt(i).get('mode') == 'R') {
                    this.setReadOnly(true);
                    return;
                }
            }
        }
        // проверим режим открытия формы, если "только чтение" - переводим форму в этот режим
        var isOpenReadOnly = window.getViewModel().getData().isOpenReadOnly;
        if (isOpenReadOnly === true) this.setReadOnly(true);
    },
    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveForm: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView();
        if (!window || !this.isChanges(window)) return;

        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false);
    },
    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelForm: function (button, eOpts) {
        var window = this.getView(),
            controller = this,
            fnSaveForm = this.saveForm;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    fnSaveForm(window, controller, true);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    },
    /**
     * проверка дат
     * @param window
     * @param startRef
     * @param endRef
     * @returns {boolean}
     */
    valideteDateForm: function (window, startRef, endRef) {
        var start = window.lookupReference(startRef).getValue(),
            end = window.lookupReference(endRef).getValue();

        if (!start || !end) {
            return false;
        }
        if (start <= end) {
            return false;
        }

        return true;
    }
});
