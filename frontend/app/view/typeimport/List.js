Ext.define('AML.view.typeimport.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-typeimport-list',
    controller: 'typeimportList',
    viewModel: {
        type: 'typeimportlistvmdl'
    },
    bind: {
        store: '{ImpType}',
        title: '{title}'
    },
    stateful: true,
    stateId: 'typeimportList',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    columns: [
        {
            text: 'Код',
            identifier: true,
            stateId: 'IMP_TYPE_CD',
            dataIndex: 'IMP_TYPE_CD',
            width: 110
        },
        {
            text: 'Наименование',
            stateId: 'IMP_TYPE_NM',
            dataIndex: 'IMP_TYPE_NM',
            renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                if (record.get('IMP_TYPE_NM')) {
                    metadata.tdAttr = metadata.tdAttr + ' data-qtip="' + Ext.util.Format.stripTags(Ext.util.Format.stripScripts(record.get('IMP_TYPE_NM'))) + '"';
                }
                return Ext.util.Format.stripScripts(value);
            },
            flex: 1
        },
        {
            text: 'Имя файла',
            stateId: 'FILE_NM',
            dataIndex: 'FILE_NM',
            renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                if (record.get('FILE_NM')) {
                    metadata.tdAttr = metadata.tdAttr + ' data-qtip="' + Ext.util.Format.stripTags(Ext.util.Format.stripScripts(record.get('FILE_NM'))) + '"';
                }
                return Ext.util.Format.stripScripts(value);
            },
            flex: 1
        },
        {
            xtype: 'booleancolumn',
            text: 'Активно?',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            width: 70
        },
        {
            xtype: 'datecolumn',
            text: 'Дата актуальности',
            stateId: 'ACTUAL_DT',
            dataIndex: 'ACTUAL_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Контроль даты актуальности',
            stateId: 'CONTROL_DT',
            dataIndex: 'CONTROL_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Время последней загрузки',
            stateId: 'LOAD_DT',
            dataIndex: 'LOAD_DT',
            format: 'd.m.Y H:i:s',
            flex: 0.75
        },
        // Статус последней загрузки (LOAD_STATUS_TX - текст в поле, LOAD_STATUS - код - влияет на цвет поля).
        // Возможные значения: Успех ('O' – зеленый цвет), Ошибка ('E' – красный цвет), Предупреждение ('W' - желтый цвет)
        {
            text: 'Статус сообщения',
            tooltop: 'Статус сообщения',
            stateId: 'LOAD_STATUS',
            dataIndex: 'LOAD_STATUS',
            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                switch (value) {
                    case 'W':
                        metaData.tdStyle = 'background:yellow;font-weight:bold;';
                        break;
                    case 'E':
                        metaData.tdStyle = 'background:red;font-weight:bold;';
                        break;
                    case 'O':
                        metaData.tdStyle = 'background:green;font-weight:bold;';
                        break;
                }
                return record.get('LOAD_STATUS_TX');
            },
            flex: 1
        },
        {
            text: 'Текст ошибки',
            stateId: 'ERR_MSG_TX',
            dataIndex: 'ERR_MSG_TX',
            renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                if (record.get('ERR_MSG_TX')) {
                    metadata.tdAttr = metadata.tdAttr + ' data-qtip="' + Ext.util.Format.stripTags(Ext.util.Format.stripScripts(record.get('ERR_MSG_TX'))) + '"';
                }
                return Ext.util.Format.stripScripts(value);
            },
            flex: 2
        },
        {
            text: 'Загружено записей',
            stateId: 'LOAD_QTY',
            dataIndex: 'LOAD_QTY',
            flex: 0.5
        },
        {
            text: 'Контроль количества записей',
            stateId: 'CONTROL_QTY',
            dataIndex: 'CONTROL_QTY',
            flex: 0.5
        },
        {
            text: 'Примечание',
            stateId: 'NOTE_TX',
            dataIndex: 'NOTE_TX',
            renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                if (record.get('NOTE_TX')) {
                    metadata.tdAttr = metadata.tdAttr + ' data-qtip="' + Ext.util.Format.stripTags(Ext.util.Format.stripScripts(record.get('NOTE_TX'))) + '"';
                }
                return Ext.util.Format.stripScripts(value);
            },
            flex: 2
        },
        {
            text: 'Кто загружал',
            stateId: 'LOADED_BY',
            dataIndex: 'LOADED_BY',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'import',
            text: 'Загрузить',
            bind: {
                hidden: '{IMPTYPE_READONLY_FL_CONVERSION}'
            },
            iconCls: 'icon-import',
            handler: 'attachAction'
        },
        '-',
        {
            itemId: 'files',
            text: 'Файлы',
            bind: {
                hidden: '{IMPTYPE_READONLY_FL_CONVERSION}'
            },
            iconCls: 'icon-document-text',
            handler: 'impTypeFiles'
        },
        '-',
        {
            itemId: 'session',
            text: 'Последняя загрузка',
            bind: {
                hidden: '{IMPTYPE_READONLY_FL_CONVERSION}'
            },
            iconCls: 'folder-in',
            handler: 'impTypeSession'
        },
        '-',
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            flex: 0.5,
            checked: true,
            value: true,
            bind: {
                value: '{activeFL}'
            }
        }
    ],
    listeners: {
    'afterrender': function (elem) {
        var self = this;
        // текущие права
        if (!self.controller.accessObject) return;

        var storeAccess = Ext.getStore('ObjectsAccess');
        if (!storeAccess) return;

        var i = storeAccess.findExact('id', self.controller.accessObject);

        // если доступ на чтение
        if (storeAccess.getAt(i).get('mode') === 'R') {
            elem.down('#import').setDisabled(true);
        }
    }
}
});
