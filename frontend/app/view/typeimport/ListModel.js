Ext.define('AML.view.typeimport.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.typeimportlistvmdl',
    data: {
        title: 'Виды импорта',
        activeFL: true
    },
    formulas: {
        IMPTYPE_READONLY_FL_CONVERSION: {
            bind: {
                IMPTYPE_READONLY: '{ImpType_READONLY}'
            },
            get: function (dataObject) {
                return dataObject.IMPTYPE_READONLY === null;
            }
        },
        filters: {
            bind: {
                activeFL: '{activeFL}'
            },
            get: function(data) {
                var store = this.getStore('ImpType'),
                    filters = [],
                    ACTIVE_FL = data.activeFL;

                if (ACTIVE_FL) {
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Виды импорта
        ImpType: {
            model: 'AML.model.ImpType',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            filters: '{filters}'
        }
    }
});
