Ext.define('AML.view.typeimport.AddImportAttachmentForm.Form', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.form.field.File',
        'Ext.form.action.DirectSubmit',
        'Ext.form.field.Hidden'
    ],
    xtype: 'app-typeimport-addimportattachment-form',
    controller: 'addimportattachmentform',
    iconCls: 'icon-import',
    viewModel: {
        type: 'addimportattachmentformviewmodel'
    },
    bind: {
        title: '{title}'
    },
    width: 600,
    height: 280,
    scrollable: 'vertical',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    initComponent: function () {
        var self = this;

        self.items = [
            {
                xtype: 'form',
                reference: 'fileImportUploadForm',
                border: 0,
                defaults: {
                    flex: 1,
                    width: '100%'
                },
                layout: {
                    type: 'vbox'
                },
                api: {
                    submit: self.getController().onClickSubmitAttachForm
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'impType',
                        bind: '{impType}'
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'fileName',
                        bind: '{filePath}'
                    },
                    {
                        xtype: 'filefield',
                        reference: 'fileImportUploadInput',
                        name: 'upload_file',
                        fieldLabel: 'Файл',
                        labelWidth: 35,
                        buttonText: 'Выбрать',
                        msgTarget: 'side',
                        buttonOnly: false,
                        allowBlank: false,
                        listeners: {
                            change: 'onInputFileChange',
                            afterrender: 'fileFieldAfterRender'
                        }
                    },
                    {
                        xtype: 'datefield',
                        name: 'dateAct',
                        reference: 'ImpTypeDateAct',
                        margin: '5px 0px 5px 0px',
                        flex: 1,
                        labelWidth: 110,
                        format: 'd.m.Y',
                        startDay: 1,
                        submitFormat: 'Y-m-d H:i:s',
                        submitValue: true,
                        fieldLabel: 'Дата актуальности',
                        value: new Date(),
                        // allowBlank: false,
                        dateFormat: 'Y-m-d H:i:s',
                        bind: {
                            hidden: '{hideImpTypeDateAct}'
                        }
                    },
                    {
                        xtype: 'combobox',
                        reference: 'ImpTypeOwnerId',
                        name: 'ownerId',
                        store: 'Owners',
                        // editable: false,
                        queryMode: 'local',
                        tpl: Ext.create('Ext.XTemplate',
                            '<tpl for=".">',
                            '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                            '</tpl>'
                        ),
                        fieldLabel: 'Ответственный',
                        displayField: 'OWNER_DSPLY_NM',
                        matchFieldWidth: false,
                        valueField: 'OWNER_ID',
                        triggerClear: false,
                        typeAhead: true,
                        // allowBlank: false,
                        labelWidth: 110,
                        margin: '5px 0px 5px 0px',
                        flex: 1,
                        submitValue: true,
                        bind: {
                            hidden: '{hideImpTypeOwnerId}'
                        },
                        listeners: {
                            expand: function (field) {
                                field.getStore().filter([{property: 'IS_ALLOW_ALERT_EDITING', value: 'Y'}]);
                                field.getPicker().refresh();
                            }
                        }
                    },
                    {
                        xtype: 'textareafield',
                        grow: true,
                        name: 'noteTx',
                        reference: 'comment',
                        fieldLabel: 'Комментарий',
                        labelAlign: 'top',
                        allowBlank: true
                    }
                ]
            }
        ];

        self.callParent(arguments);
    },

    buttons: [
        {
            itemId: 'attachForm',
            iconCls: 'icon-save-data',
            text: 'Загрузить'
        },
        {
            itemId: 'cancelAttachForm',
            iconCls: 'icon-backtothe-primitive',
            text: 'Отмена'
        }
    ]
});