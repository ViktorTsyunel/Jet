Ext.define('AML.view.typeimport.AddImportAttachmentForm.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.addimportattachmentformviewmodel',
    data: {
        title: null,
        formName: 'AddImportAttachment',
        impTypeSel: null
    },
    formulas: {
        impType: {
            bind: {
                impType: '{impTypeSel}'
            },
            get: function (data) {
                return data.impType;
            }
        }
    }
});