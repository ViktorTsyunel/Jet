Ext.define('AML.view.typeimport.AddImportAttachmentForm.FormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.addimportattachmentform',

    requires: [
        'Ext.util.KeyMap',
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#attachForm': {
                    click: 'onClickAttachForm'
                },
                '#cancelAttachForm': {
                    click: 'onClickCancelAttachForm'
                }
            }
        }
    },
    onClickAttachForm: function () {
        var self = this,
            vm = self.getViewModel(),
            hideImpTypeDateAct = vm.get('hideImpTypeDateAct'),
            ImpTypeDateActAllowBlank = vm.get('ImpTypeDateActAllowBlank'),
            hideImpTypeOwnerId = vm.get('hideImpTypeOwnerId'),
            ImpTypeOwnerIdAllowBlank = vm.get('ImpTypeOwnerIdAllowBlank'),
            fileUploadInput = this.lookupReference('fileImportUploadInput'),
            // comment = this.lookupReference('comment'),
            ImpTypeDateAct = this.lookupReference('ImpTypeDateAct'),
            ImpTypeOwnerId = this.lookupReference('ImpTypeOwnerId'),
            form = this.lookupReference('fileImportUploadForm');

        if(!hideImpTypeDateAct && ImpTypeDateActAllowBlank && Ext.isEmpty(ImpTypeDateAct.getValue())) {
            ImpTypeDateAct.markInvalid('Заполните обязательные поля!');
            isInvalid = true;
            return;
        }
        if(!hideImpTypeOwnerId && ImpTypeOwnerIdAllowBlank && Ext.isEmpty(ImpTypeOwnerId.getValue())) {
            ImpTypeOwnerId.markInvalid('Заполните обязательные поля!');
            isInvalid = true;
            return;
        }
        if (Ext.isEmpty(fileUploadInput.getValue())) {
            fileUploadInput.markInvalid('Заполните обязательные поля!');
            isInvalid = true;
            return;
        }
        /** fileUploadInput.getValue() */
        vm.set('filePath', fileUploadInput.lastValue);

        form.submit({
            scope: self,
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: {
                "Content-Type": null
            }
        });
    },

    onClickSubmitAttachForm: function(form, success, me) {
        var self = me.scope,
            vm = self.getViewModel(),
            files = form['upload_file'].files,
            fileUploadInput = self.lookupReference('fileImportUploadInput'),
            ajaxOptions,
            finalCallbackSuccess,
            finalCallbackFailure,
            totalSize = 0;

        Ext.Array.each(files, function(f) {
            totalSize += f.size;
        });

        if (totalSize > me.files_size * 1024 * 1024) {
            Ext.Msg.alert('','Суммарный размер файлов больше ' + me.files_size + 'Мб!');
            me.failure();
            return;
        }

        finalCallbackSuccess = function(response) {
            var result = Ext.decode(response.responseText),
                //callbackFn = vm.get('attachActionCallback'),
                gridVm = (vm.getParent()) ? vm.getParent() : false,
                store = (gridVm) ? gridVm.getStore('ImpType') : false;
                gridCtrl = gridVm && gridVm.getView().getController();

            // переподкачка списка:
            if (store) {
                store.reload();
            }
            //if(!result.success) {
                // реинициализация множественного выбора файлов:
                // self.fileFieldAfterRender(fileUploadInput);
            //}
            /*// обратный вызов функции для выполнения произвольных действий вне формы:
            callbackFn && callbackFn();*/
            // закрываем форму
            self.getView().close();

            if (result.success == true && result.message) {
                Ext.Msg.alert(' ', result.message);
            }
        };

        finalCallbackFailure = function() {
            // реинициализация множественного выбора файлов:
            // self.fileFieldAfterRender(fileUploadInput);
        };

        ajaxOptions = {
            url: common.globalinit.ajaxImportAddAttachmentUrl,
            timeout: common.globalinit.ajaxTimeOut,
            method: me.getMethod(),
            headers: me.headers,
            form: form,
            isUpload: true,
            success: finalCallbackSuccess,
            failure: finalCallbackFailure
        };

        // добавим loading обработки:
        if(!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request(ajaxOptions);
    },

    onInputFileChange: function (fileInput, value) {
        var newValue = value.split('\\'),
            files = fileInput.fileInputEl.dom.files,
            fileNames = [],
            vm = this.getViewModel(),
            impMultiType = (vm) ? vm.get('impTypeMultiAttach') : null;

        if(!impMultiType && files.length > 1) {
            Ext.Msg.alert('', 'Для данного вида импорта недоступна возможность загрузки более 1 файла!');
            fileInput.setRawValue('');
            return;
        }

        Ext.Array.each(files, function(f) {
            fileNames.push(f.name);
        });

        if(fileNames.length)
            fileInput.setRawValue(fileNames.join(', '));
        else
            fileInput.setRawValue(newValue[newValue.length-1]);
    },

    fileFieldAfterRender: function (cmp) {
        var vm = this.getViewModel(),
            multiV = (vm) ? vm.get('impTypeMultiAttach') : false,
            multiValue = (multiV) ? 'multiple' : false;
        if (Ext.isIE8 || Ext.isIE9) {
            cmp.inputEl.set({ disabled: 'disabled' });
        }
        cmp.fileInputEl.set({
            // multiple: ''
            multiple: multiValue
        });
        cmp.fileInputEl.focus();
    },

    onClickCancelAttachForm: function () {
        this.getView().close();
    }
});
