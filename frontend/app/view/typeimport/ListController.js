Ext.define('AML.view.typeimport.ListController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.typeimportList',
    childForm: null,
    IdColumn: null,
    isReadOnly: true,
    accessObject: 'ImpType', //код объекта доступа для данной формы,

    attachAction: function () {
        var self = this,
            vm = self.getViewModel(),
            selectedCnt = parseInt(self.getView().getSelectionModel().getSelected().length),
            impTypeSel = (selectedCnt == 1) ? self.getView().getSelectionModel().getSelected().items[0].get('IMP_TYPE_CD') : false,
            impTypeFields = (selectedCnt == 1) ? self.getView().getSelectionModel().getSelected().items[0].get('PARAMS_TX') : false,
            impTypeMultiAttach = (selectedCnt == 1) ? self.getView().getSelectionModel().getSelected().items[0].get('MULTIPLE_FILES_FL') : false,
            impTypeTitle = (selectedCnt == 1) ? ('Загрузка данных. ' + self.getView().getSelectionModel().getSelected().items[0].get('IMP_TYPE_NM')) : 'Загрузка данных.',
            objNeededFields = (impTypeFields) ? Ext.JSON.decode(impTypeFields) : null,
            dateActValue = (objNeededFields) ? objNeededFields.dateAct : null,
            ownerIdValue = (objNeededFields) ? objNeededFields.ownerId : null;

        if (impTypeSel) {
            Ext.widget({
                xtype: 'app-typeimport-addimportattachment-form',
                viewModel: {
                    type: 'addimportattachmentformviewmodel',
                    parent: vm,
                    data: {
                        impTypeSel: impTypeSel,
                        title: impTypeTitle,
                        hideImpTypeDateAct: (dateActValue) ? false : true,
                        ImpTypeDateActAllowBlank: (dateActValue) ? (dateActValue) : null,
                        hideImpTypeOwnerId: (ownerIdValue) ? false : true,
                        ImpTypeOwnerIdAllowBlank: (ownerIdValue) ? (ownerIdValue) : null,
                        impTypeMultiAttach: (impTypeMultiAttach === 'Y') ? true : false
                    }
                }
            }).show();
        } else {
            Ext.Msg.alert('', 'Пожалуйста, выберите вид импорта из списка.');
            return false;
        }
    },

    impTypeFiles: function () {
        var self = this,
            vm = self.getViewModel();
        Ext.widget({
            xtype: 'app-typeimport-imptyperead-imptypereadlist',
            minHeight: 600,
            width: '100%',
            height: 700,
            viewModel: {
                parent: vm
            }
        }).show();
    },

    impTypeSession: function () {
        var self = this,
            window = this.getView(),
            //form = window.down('form'),
            vm = window.getViewModel(),
            selectedCnt = parseInt(self.getView().getSelectionModel().getSelected().length, 10),
            impTypeSel = (selectedCnt == 1) ? self.getView().getSelectionModel().getSelected().items[0].get('IMP_TYPE_CD') : false,
            impMulti = (selectedCnt == 1) ? self.getView().getSelectionModel().getSelected().items[0].get('MULTIPLE_FILES_FL') : false,
            storeOwners = Ext.data.StoreManager.lookup('Owners'),
            currentUser = (storeOwners) ? storeOwners.findRecord('IS_CURRENT', true) : null,
            data = {
                form: 'ImpSession',
                action: 'readLatest',
                loaded_by: (currentUser) ? currentUser.get('OWNER_ID') : null,
                imp_type_cd: impTypeSel
            };

        if (impTypeSel && impMulti === 'Y') {

            // добавим loading обработки:
            if (!common.globalinit.loadingMaskCmp) {
                common.globalinit.loadingMaskCmp = self.getView();
            }

            Ext.Ajax.request({
                url: common.globalinit.ajaxUrl,
                method: common.globalinit.ajaxMethod,
                headers: common.globalinit.ajaxHeaders,
                timeout: common.globalinit.ajaxTimeOut,
                params: Ext.encode(data),
                success: function (response) {
                    var responseObj = Ext.JSON.decode(response.responseText), startDC, endDC;
                    if (responseObj.message) {
                        Ext.Msg.alert('', responseObj.message);
                    }

                    if (responseObj.success) {
                        Ext.widget({
                            xtype: 'app-typeimport-imptypesession-imptypesessionlist',
                            minHeight: 600,
                            width: '100%',
                            height: 700,
                            viewModel: {
                                type: 'imptypesessionlistvm',
                                data: {
                                    record: {
                                        end_dt: (responseObj) ? responseObj.end_dt : null,
                                        extra_params_tx: (responseObj) ? responseObj.extra_params_tx : null,
                                        note_tx: (responseObj) ? responseObj.note_tx : null,
                                        imp_type_cd: (responseObj) ? responseObj.imp_type_cd : null,
                                        loaded_by: (responseObj) ? responseObj.loaded_by : null,
                                        start_dt: (responseObj) ? responseObj.start_dt : null
                                    },
                                    foundData: (responseObj) ? responseObj.data : null,
                                    loadImpType: (responseObj) ? responseObj.totalCount: null
                                },
                                parent: vm
                            }
                        }).show();
                    }
                }
            });
        } else {
            Ext.Msg.alert('', 'Информация о загруженных файлах не доступна для выбранного типа импорта.');
            return false;
        }
    },

    convertDateToFormatString: function (dateString) {
        if (dateString) {
            var a = dateString.split(' '),
                d = a[0].split('-'),
                c = a[1].split(':'),
                dd, mm, hh, minuts, mss;
            dd = (d[2].length === 1) ? ('0' + d[2]) : d[2];
            mm = (d[1].length === 1) ? ('0' + d[1]) : d[1];
            hh = (c[0].length === 1) ? ('0' + c[0]) : c[0];
            minuts = (c[1].length === 1) ? ('0' + c[1]) : c[1];
            mss = (c[2].length === 1) ? ('0' + c[2]) : c[2];

            return (dd + '.' + mm + '.' + d[0] + ' ' + hh + ':' + minuts + ':' + mss);
        }
        return null;
    }
});
