Ext.define('AML.view.typeimport.ImpTypeSession.ImpTypeSessionListModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.imptypesessionlistvm',
	data: {
		title: 'Информация о загруженных файлах в последней сессии',
        loadImpType: null,
        record: {
            end_dt: null,
            extra_params_tx: null,
            note_tx: null,
            imp_type_cd: null,
            loaded_by: null,
            start_dt: null
        }
	},
    formulas: {
    },
    stores: {
    }
});
