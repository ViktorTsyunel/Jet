Ext.define('AML.view.typeimport.ImpTypeSession.ImpTypeSessionForm', {
    extend: 'Ext.window.Window',
    xtype: 'app-typeimport-imptyperead-imptypesessionform',
    controller: 'imptypesessionformcontroller',
    viewModel: {
        type: 'imptypesessionformvm'
    },
    // cls: 'app-oes321detailspanel-viewclientfromsearch',
    layout: 'vbox',
    padding: 5,
    defaults: {
        width: '100%',
        border: 0
    },
    // tabP: 1,
    reference: 'ref-app-typeimport-imptypesession-imptypesessionform',
    items: [

    ],
    buttons: [
        {
            itemId: 'cancelWindow',
            text: 'Закрыть'
        }
    ]
});