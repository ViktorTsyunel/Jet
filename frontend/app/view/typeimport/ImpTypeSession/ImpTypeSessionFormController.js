Ext.define('AML.view.typeimport.ImpTypeSession.ImpTypeSessionFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.imptypesessionformcontroller',
    config: {
        listen: {
            component: {
                '#cancelWindow': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    onClickCancelWindow: function () {
        var window = this.getView();
        window.close();
    }
});