Ext.define('AML.view.typeimport.ImpTypeSession.ImpTypeSessionFormModel', {
    extend: 'Ext.data.Model',
    alias: 'model.imptypesessionformvm',
    idProperty: '_id_',
    fields: [
        {
            // ID
            identifier: true,
            name: '_id_',
            type: 'string'
        },
        {
            // Код подразделения
            name: 'DPT_ID',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Система-источник
            name: 'SYS_CD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Файл
            name: 'FILE_NM',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Статус
            name: 'LOAD_STATUS',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Ошибка
            name: 'ERR_MSG_TX',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Кол-во загруженных записей
            name: 'LOAD_QTY',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата данных в файле
            name: 'DATA_DT',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер файла в системе источнике
            name: 'ORDER_NB',
            type: 'string',
            allowNull: false,
            allowBlank: false
        },
        {
            // Имя архива
            name: 'ARCHIVE_NM',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Пользователь
            name: 'LOADED_BY',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата загрузки
            name: 'LOAD_DT',
            type: 'date',
            dateFormat: 'c',
            allowNull: true,
            allowBlank: true
        }
    ]
});