Ext.define('AML.view.typeimport.ImpTypeSession.ImpTypeSessionListController', {
    extend: 'AML.view.main.BaseFormController', // BaseListController
    alias: 'controller.imptypesessionlistcontroller',
    requires: [
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#impTypeSessionClose': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    onClickCancelWindow: function () {
        var window = this.getView();
        window.close();
    }
});


