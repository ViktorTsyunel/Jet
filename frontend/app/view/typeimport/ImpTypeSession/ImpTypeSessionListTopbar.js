Ext.define('AML.view.typeimport.ImpTypeSession.ImpTypeSessionListTopbar', {
    extend: 'Ext.container.Container',
    xtype: 'app-typeimport-imptypesession-imptypesessionlisttopbar',
    reestablishView: false,
    reference: 'ref-imptype-read-topbar',
    defaults: {
        xtype: 'container',
        layout: {
            type: 'vbox'
        }
    },
    items: [
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                margin: '1px 3px 1px 3px',
                xtype: 'displayfield',
                labelAlign: 'right',
                labelWidth: 150,
                fieldStyle: 'font-weight: bold;'
            },
            items: [
                {
                    fieldLabel: 'Вид импорта',
                    labelWidth: 82,
                    bind: {
                        value: '{record.imp_type_cd}'
                    }
                },
                {
                    fieldLabel: 'Пользователь',
                    bind: {
                        value: '{record.loaded_by}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                margin: '1px 3px 1px 3px',
                xtype: 'displayfield',
                labelAlign: 'right',
                fieldStyle: 'font-weight: bold;'
            },
            items: [
                {
                    fieldLabel: 'Дата начала загрузки',
                    labelWidth: 131,
                    bind: {
                        value: '{record.start_dt}'
                    },
                    listeners: {
                        afterrender: function (field) {
                            var doc = field.up('app-typeimport-imptypesession-imptypesessionlist'),
                                vmDoc = (doc) ? doc.getViewModel() : false,
                                docData = (vmDoc) ? vmDoc.getData() : false,
                                rec = (docData) ? docData.record : false,
                                stValue = (rec) ? rec['start_dt'] : false;
                            if (stValue) {
                                var a = stValue.toString().split(' '),
                                    d = a[0].split('-'),
                                    c = a[1].split(':'),
                                    dd, mm, hh, minuts, mss;
                                dd = (d[2].length === 1) ? ('0' + d[2]) : d[2];
                                mm = (d[1].length === 1) ? ('0' + d[1]) : d[1];
                                hh = (c[0].length === 1) ? ('0' + c[0]) : c[0];
                                minuts = (c[1].length === 1) ? ('0' + c[1]) : c[1];
                                mss = (c[2].length === 1) ? ('0' + c[2]) : c[2];
                                field.setValue(dd + '.' + mm + '.' + d[0] + ' ' + hh + ':' + minuts + ':' + mss);
                            }
                        }
                    }
                },
                {
                    fieldLabel: 'Дата окончания загрузки',
                    labelWidth: 150,
                    bind: {
                        value: '{record.end_dt}'
                    },
                    listeners: {
                        afterrender: function (field) {
                            var doc = field.up('app-typeimport-imptypesession-imptypesessionlist'),
                                vmDoc = (doc) ? doc.getViewModel() : false,
                                docData = (vmDoc) ? vmDoc.getData() : false,
                                rec = (docData) ? docData.record : false,
                                stValue = (rec) ? rec['end_dt'] : false;
                            if (stValue) {
                                var a = stValue.toString().split(' '),
                                    d = a[0].split('-'),
                                    c = a[1].split(':'),
                                    dd, mm, hh, minuts, mss;
                                dd = (d[2].length === 1) ? ('0' + d[2]) : d[2];
                                mm = (d[1].length === 1) ? ('0' + d[1]) : d[1];
                                hh = (c[0].length === 1) ? ('0' + c[0]) : c[0];
                                minuts = (c[1].length === 1) ? ('0' + c[1]) : c[1];
                                mss = (c[2].length === 1) ? ('0' + c[2]) : c[2];

                                field.setValue(dd + '.' + mm + '.' + d[0] + ' ' + hh + ':' + minuts + ':' + mss);
                            }
                        }
                    }
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                margin: '1px 3px 1px 3px',
                xtype: 'displayfield',
                labelAlign: 'right',
                fieldStyle: 'font-weight: bold;'
            },
            items: [
                {
                    fieldLabel: 'Доп. параметры',
                    labelWidth: 100,
                    bind: {
                        value: '{record.extra_params_tx}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                margin: '1px 3px 1px 3px',
                xtype: 'displayfield',
                labelAlign: 'right',
                fieldStyle: 'font-weight: bold;'
            },
            items: [
                {
                    fieldLabel: 'Комментарий',
                    labelWidth: 85,
                    bind: {
                        value: '{record.note_tx}'
                    }
                }
            ]
        },
        {
            xtype : 'menuseparator',
            width : '100%'
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            defaults: {
                margin: '1px 3px 1px 3px'
            },
            items: [
                {
                    xtype: 'label',
                    text: 'Результат поиска'
                }
            ]
        }
    ]
});