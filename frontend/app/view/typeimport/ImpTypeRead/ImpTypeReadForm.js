Ext.define('AML.view.typeimport.ImpTypeRead.ImpTypeReadForm', {
    extend: 'Ext.window.Window',
    xtype: 'app-typeimport-imptyperead-imptypereadform',
    controller: 'imptypereadformcontroller',
    viewModel: {
        type: 'imptypereadformvm'
    },
    // cls: 'app-oes321detailspanel-viewclientfromsearch',
    layout: 'vbox',
    padding: 5,
    defaults: {
        width: '100%',
        border: 0
    },
    // tabP: 1,
    reference: 'ref-app-typeimport-imptyperead-imptypereadform',
    items: [

    ],
    buttons: [
        {
            itemId: 'cancelWindowSeachClient',
            text: 'Закрыть'
        }
    ]
});