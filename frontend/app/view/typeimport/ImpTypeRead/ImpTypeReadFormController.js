Ext.define('AML.view.typeimport.ImpTypeRead.ImpTypeReadFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.imptypereadformcontroller',
    config: {
        listen: {
            component: {
                '#cancelWindowSeachClient': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    onClickCancelWindow: function () {
        var window = this.getView();
        window.close();
    }
});