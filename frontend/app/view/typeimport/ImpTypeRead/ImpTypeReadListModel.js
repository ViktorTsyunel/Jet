Ext.define('AML.view.typeimport.ImpTypeRead.ImpTypeReadListModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.imptypereadlistvm',
	data: {
		title: 'Импорт данных',
        loadImpType: null,
        record: {
            impTypeTypes: null,
            impTypeTb: null,
            impTypeDateFrom: null,
            impTypeOwner: null,
            impTypeDateTo: null
        }
	},
    formulas: {
        filters: {
            bind: {
                MULTIPLE_FILES_FL: '{MULTIPLE_FILES_FL}'
            },
            get: function(data) {
                var store = this.getStore('ImpTypeSelect'),
                    filters = [],
                    MULTIPLE_FILES_FL = data.MULTIPLE_FILES_FL;

                if (MULTIPLE_FILES_FL) {
                    filters.push({
                        property: 'MULTIPLE_FILES_FL',
                        value: 'Y'
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Виды импорта
        ImpTypeSelect: {
            model: 'AML.model.ImpType',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            filters: '{filters}'
        }
    }
});
