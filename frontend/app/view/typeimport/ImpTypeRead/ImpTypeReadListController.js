Ext.define('AML.view.typeimport.ImpTypeRead.ImpTypeReadListController', {
    extend: 'AML.view.main.BaseFormController', // BaseListController
    alias: 'controller.imptypereadlistcontroller',
    requires: [
        'Ext.data.StoreManager'
    ],
    config: {
        listen: {
            component: {
                '#impTypeCancelWindow': {
                    click: 'onClickCancelWindow'
                }
            }
        }
    },

    onClickSearchImpType: function() {
        var self = this,
            window = this.getView(),
            //form = window.down('form'),
            vm = window.getViewModel(),
            vmData = (vm) ? vm.getData() : false,
            //impTypeTypes = vm.get('impTypeTypes'),
            data = {
                form: 'ImpFiles',
                action: 'read'
        };

        // проверка данных фильтра поиска
        if (!(data = this.getDataForm(vmData, data, window))) {
            return false;
        }

        // добавим loading обработки:
        if(!common.globalinit.loadingMaskCmp) {
            common.globalinit.loadingMaskCmp = self.getView();
        }

        Ext.Ajax.request({
            url: common.globalinit.ajaxUrl,
            method: common.globalinit.ajaxMethod,
            headers: common.globalinit.ajaxHeaders,
            timeout: common.globalinit.ajaxTimeOut,
            params: Ext.encode(data),
            success: function (response) {
                var responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.message) {
                    Ext.Msg.alert('', responseObj.message);
                }
                var storeSearchResult = self.getReferences()['impTypeGrid'].getStore();
                if (storeSearchResult) {
                    storeSearchResult.removeAll();
                }
                if (responseObj.success) {
                    storeSearchResult = self.getReferences()['impTypeGrid'].getStore();
                    if (storeSearchResult) {
                        storeSearchResult.add(responseObj.data);
                    }
                    if(responseObj.totalCount) {
                        vm.set('loadImpType', responseObj.totalCount);
                    } else {
                        vm.set('loadImpType', 0);
                    }
                }
            }
        });
    },

    getDataForm: function(vmData, data, window) {
        var record = (vmData) ? vmData.record : false,
            field;

        if (vmData) {
            // if (!record.searchType) {
            if (!vmData.impTypeTypes) {
                field = window.lookupReference('impTypeTypes');
                field.markInvalid('нужно заполнить');
            }
            if (field) {
                return false;
            }
            // data.IMP_TYPE_CD = record.impTypeTypes;
            data.imp_type_cd = record.impTypeTypes;
            if (vmData.impTypeTb) {
                // data.TB_CD = record.impTypeTb;
                data.tb_cd = record.impTypeTb;
            }
            if (record.impTypeDateFrom) {
                // data.DATA_DT_FROM = record.impTypeDateFrom;
                data.data_dt_from = record.impTypeDateFrom;
            }
            if (record.impTypeDateTo) {
                // data.DATA_DT_TO = record.impTypeDateTo;
                data.data_dt_to = record.impTypeDateTo;
            }
            if (vmData.impTypeOwner) {
                // data.LOADED_BY = record.impTypeOwner;
                data.loaded_by = record.impTypeOwner;
            }
        }

        return data;
    },

    onClickCancelWindow: function () {
        var window = this.getView();
        window.close();
    }
});


