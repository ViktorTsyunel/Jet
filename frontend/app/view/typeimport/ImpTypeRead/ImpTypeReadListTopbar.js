Ext.define('AML.view.typeimport.ImpTypeRead.ImpTypeReadListTopbar', {
    extend: 'Ext.container.Container',
    xtype: 'app-typeimport-imptyperead-imptypereadlisttopbar',
    reestablishView: false,
    reference: 'ref-imptype-read-topbar',
    items: [
        {
            xtype: 'form',
            isFilters: true,
            formBind: true,
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'column'
                    },
                    flex: 1,
                    defaults: {
                        labelAlign: 'left',
                        labelWidth: 200,
                        margin: '3px 3px 3px 3px',
                        flex: 1
                    },
                    items: [
                        {
                            columnWidth: 0.33,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Вид импорта',
                                            name: 'impTypeTypes',
                                            reference: 'impTypeTypes',
                                            editable: false,
                                            queryMode: 'local',
                                            forceSelection: true,
                                            multiSelect: false,
                                            matchFieldWidth: true,
                                            displayField: 'IMP_TYPE_CD',
                                            labelWidth: 100,
                                            style: 'text-align: right;',
                                            valueField: 'IMP_TYPE_CD',
                                            allowBlank: false,
                                            bind: {
                                                store: '{ImpTypeSelect}',
                                                value: '{record.impTypeTypes}'
                                            },
                                            listeners: {
                                                expand: function (field) {
                                                    field.getStore().filter([{property: 'MULTIPLE_FILES_FL', value: 'Y'}]);
                                                    field.getPicker().refresh();
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            reference: 'impTypeTb',
                                            flex: 1,
                                            name: 'impTypeTb',
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                                '<tpl if="id &gt; 0">{id} - {label}' +
                                                '<tpl else>{label}</tpl>' +
                                                '</div>',
                                                '</tpl>'
                                            ),
                                            style: 'text-align: right;',
                                            fieldLabel: 'Тербанк',
                                            displayField: 'label',
                                            labelWidth: 100,
                                            valueField: 'id',
                                            triggerClear: true,
                                            store: 'TB',
                                            allowBlank: true,
                                            bind: {
                                                value: '{record.impTypeTb}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            columnWidth: 0.33,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    name: 'impTypeDateFrom',
                                    reference: 'impTypeDateFrom',
                                    flex: 1,
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    labelWidth: 150,
                                    triggerClear: true,
                                    submitFormat: 'Y-m-d H:i:s',
                                    // submitValue: true,
                                    style: 'text-align: right;',
                                    fieldLabel: 'Дата данных в файле, с',
                                    allowBlank: true,
                                    dateFormat: 'Y-m-d H:i:s',
                                    bind: {
                                        value: '{record.impTypeDateFrom}'
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'impTypeOwner',
                                    name: 'impTypeOwner',
                                    store: 'Owners',
                                    // editable: false,
                                    triggerClear: true,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    style: 'text-align: right;',
                                    fieldLabel: 'Пользователь',
                                    displayField: 'OWNER_DSPLY_NM',
                                    valueField: 'OWNER_ID',
                                    typeAhead: true,
                                    allowBlank: true,
                                    labelWidth: 150,
                                    matchFieldWidth: false,
                                    // margin: '5px 0px 5px 0px',
                                    flex: 1,
                                    submitValue: true,
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().filter([{property: 'IS_ALLOW_ALERT_EDITING', value: 'Y'}]);
                                            field.getPicker().refresh();
                                        }
                                    },
                                    bind: {
                                        value: '{record.impTypeOwner}'
                                    }
                                }
                            ]
                        },
                        {
                            columnWidth: 0.33,
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    name: 'impTypeDateTo',
                                    reference: 'impTypeDateTo',
                                    flex: 1,
                                    labelWidth: 40,
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    submitFormat: 'Y-m-d H:i:s',
                                    style: 'text-align: right;',
                                    submitValue: true,
                                    fieldLabel: 'по',
                                    allowBlank: true,
                                    triggerClear: true,
                                    dateFormat: 'Y-m-d H:i:s',
                                    bind: {
                                        value: '{record.impTypeDateTo}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    columnWidth: 0.08,
                    xtype: 'container',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'buttongroup',
                            layout: 'fit',
                            height: 75,
                            margin: '0 0 0 0',
                            items: [
                                {
                                    scale: 'large',
                                    width: 75,
                                    iconCls: 'icon-search',
                                    listeners: {
                                        click: 'onClickSearchImpType'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },
            defaults: {
                margin: '1px 3px 1px 3px'
            },
            items: [
                {
                    xtype: 'label',
                    text: 'Результат поиска'
                }
            ]
        }
    ]
});