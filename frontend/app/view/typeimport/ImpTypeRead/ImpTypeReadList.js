Ext.define('AML.view.typeimport.ImpTypeRead.ImpTypeReadList', {
    extend: 'Ext.window.Window',
    xtype: 'app-typeimport-imptyperead-imptypereadlist',
    controller: 'imptypereadlistcontroller',
    iconCls: 'icon-report',
    requires: [
        'AML.view.typeimport.ImpTypeRead.ImpTypeReadListController',
        'AML.view.typeimport.ImpTypeRead.ImpTypeReadFormModel'
    ],
    viewModel: {
        type: 'imptypereadlistvm'
    },
    bind: {
        title: '{title}'
    },
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        /**
         * Обрабатываем событие отрисовки
         * @param window
         */
        render: function (window) {
            if (Ext.isIE8) {
                window.setWidth(Math.round(document.body.clientWidth / 100 * 95));
            }
        },
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            tbar: [
                {
                    xtype: 'app-typeimport-imptyperead-imptypereadlisttopbar',
                    flex: 1
                }
            ],
            items: [
                {
                    xtype: 'gridpanel',
                    reference: 'impTypeGrid',
                    selModel: {
                        mode: 'SINGLE'
                    },
                    flex: 1,
                    store: {
                        model: 'AML.view.typeimport.ImpTypeRead.ImpTypeReadFormModel'
                    },
                    columns: [
                        {
                            text: 'Код подразделения',
                            tooltip: 'Код подразделения',
                            stateId: 'DPT_ID',
                            dataIndex: 'DPT_ID',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Система-источник',
                            tooltip: 'Система-источник',
                            stateId: 'SYS_CD',
                            dataIndex: 'SYS_CD',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Файл',
                            stateId: 'FILE_NM',
                            dataIndex: 'FILE_NM',
                            tdCls: 'v-align-middle',
                            flex: 2,
                            renderer: function(value, metaData) {
                                metaData.tdAttr = 'data-qtip="' + value + '"';
                                return value;
                            }
                        },
                        {
                            text: 'Статус',
                            stateId: 'LOAD_STATUS',
                            dataIndex: 'LOAD_STATUS',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Ошибка',
                            stateId: 'ERR_MSG_TX',
                            dataIndex: 'ERR_MSG_TX',
                            tdCls: 'v-align-middle',
                            flex: 2.5
                        },
                        {
                            text: 'Кол-во загруженных записей',
                            tooltip: 'Кол-во загруженных записей',
                            stateId: 'LOAD_QTY',
                            dataIndex: 'LOAD_QTY',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Дата данных в файле',
                            tooltip: 'Дата данных в файле',
                            stateId: 'DATA_DT',
                            dataIndex: 'DATA_DT',
                            tdCls: 'v-align-middle',
                            flex: 1,
                            renderer: function (value) {
                                if (value) {
                                    var fValue = Ext.Date.format(new Date(value),'d-m-Y h:m:s'),
                                        a = (fValue) ? fValue.toString().split(' ') : null,
                                        aa = (a) ? a[0] : null,
                                        d = (aa) ? aa.split('-') : null,
                                        delem1, delem2, dlen1, dlen2, delem3,
                                        dd, mm;
                                    delem1 = (d) ? d[0] : null;
                                    dlen1 = (delem1) ? delem1.length : null;
                                    delem2 = (d) ? d[1] : null;
                                    dlen2 = (delem2) ? delem2.length : null;
                                    delem3 = (d) ? d[2] : null;

                                    dd = (dlen1 === 1) ? ('0' + delem1) : delem1;
                                    mm = (dlen2 === 1) ? ('0' + delem2) : delem2;

                                    return (dd + '.' + mm + '.' + delem3);
                                }
                            }
                        },
                        {
                            text: 'Номер файла в системе источнике',
                            tooltip: 'Номер файла в системе источнике',
                            stateId: 'ORDER_NB',
                            dataIndex: 'ORDER_NB',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Имя архива',
                            stateId: 'ARCHIVE_NM',
                            dataIndex: 'ARCHIVE_NM',
                            tdCls: 'v-align-middle',
                            flex: 0.5
                        },
                        {
                            text: 'Пользователь',
                            stateId: 'LOADED_BY',
                            dataIndex: 'LOADED_BY',
                            tdCls: 'v-align-middle',
                            flex: 1
                        },
                        {
                            text: 'Дата загрузки',
                            stateId: 'LOAD_DT',
                            dataIndex: 'LOAD_DT',
                            tdCls: 'v-align-middle',
                            flex: 1,
                            renderer: function (value) {
                                if (value) {
                                    var fValue = Ext.Date.format(new Date(value),'d-m-Y h:m:s'),
                                        a = (fValue) ? fValue.toString().split(' ') : null,
                                        aa = (a) ? a[0] : null,
                                        d = (aa) ? aa.split('-') : null,
                                        delem1, delem2, dlen1, dlen2, delem3,
                                        dd, mm;
                                    delem1 = (d) ? d[0] : null;
                                    dlen1 = (delem1) ? delem1.length : null;
                                    delem2 = (d) ? d[1] : null;
                                    dlen2 = (delem2) ? delem2.length : null;
                                    delem3 = (d) ? d[2] : null;

                                    dd = (dlen1 === 1) ? ('0' + delem1) : delem1;
                                    mm = (dlen2 === 1) ? ('0' + delem2) : delem2;

                                    return (dd + '.' + mm + '.' + delem3);
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            xtype: 'container',
            bind: {
                hidden: '{!loadImpType}',
                html: '<div class="counter">Кол-во загруженных записей: <b>{loadImpType}</b></div>'
            }
        },
        {
            xtype: 'container',
            flex: 1
        },
        {
            itemId: 'impTypeCancelWindow',
            text: 'Закрыть'
        }
    ]
});