Ext.define('AML.view.assign.AssignReplacementForm', {
    extend: 'Ext.window.Window',
    requires: [
        'AML.view.settings.OPOKFormController',
        'AML.view.main.BaseFormViewModel',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox'
    ],
    xtype: 'app-assign-assignreplacement-form',
    controller: 'assignreplacementform',
    iconCls: 'icon-report',
    viewModel: {
        data: {
            record: null,
            selectedOwner: null,
            selectedNewOwner: null
        }
    },
    width: 850,
    title: 'Запись замещения ответственного',
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            defaults: {
                labelWidth: 150
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: {
                                        type: 'owners'
                                    },
                                    editable: true,
                                    typeAhead: true,
                                    typeAheadDelay: 500,
                                    forceSelection: true,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Замещаемый',
                                    displayField: 'OWNER_DSPLY_NM',
                                    matchFieldWidth: false,
                                    valueField: 'id',
                                    triggerClear: true,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.OWNER_SEQ_ID}',
                                        selection: '{selectedOwner}'
                                    },
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().filter([{property: "IS_ALLOW_ALERT_EDITING", value: "Y"}]);
                                            field.getPicker().refresh();
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: {
                                        type: 'owners'
                                    },
                                    editable: true,
                                    typeAhead: true,
                                    typeAheadDelay: 500,
                                    forceSelection: true,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Замещающий',
                                    displayField: 'OWNER_DSPLY_NM',
                                    matchFieldWidth: false,
                                    valueField: 'id',
                                    triggerClear: true,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.NEW_OWNER_SEQ_ID}',
                                        selection: '{selectedNewOwner}'
                                    },
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().filter([{property: "IS_ALLOW_ALERT_EDITING", value: "Y"}]);
                                            field.getPicker().refresh();
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Период замещения',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults:{
                                flex: 1,
                                xtype: 'datefield',
                                labelWidth: 130,
                                format: 'd.m.Y',
                                startDay: 1
                            },
                            items: [
                                {
                                    fieldLabel: 'Начало',
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.START_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                        }
                                                    }
                                                    container.nextSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    fieldLabel: 'Окончание',
                                    allowBlank: false,
                                    margin: '0 0 0 10',
                                    bind: {
                                        value: '{record.END_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                }
                                                container.previousSibling('datefield').focus();
                                                container.focus();
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Активно?',
                            margin: '0 0 0 30',
                            allowBlank: false,
                            bind: {
                                value: '{record.ACTIVE_FL}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Аудит',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто создал',
                                    bind: {
                                        value: '{record.CREATED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда создал',
                                    bind: {
                                        value: '{record.CREATED_DATE:date("d.m.Y")}'
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто изменил',
                                    displayField: 'MODIFIED_BY',
                                    bind: {
                                        value: '{record.MODIFIED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда изменил',
                                    bind: {
                                        value: '{record.MODIFIED_DATE:date("d.m.Y")}'
                                    }
                                }

                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveForm',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});