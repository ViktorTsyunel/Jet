Ext.define('AML.view.assign.AssignRuleController', {
    requires: [
        'AML.model.AssignRule',
        'Ext.data.StoreManager'
    ],
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.assignrule',
    childForm: 'app-assign-assignrule-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'RULE_SEQ_ID', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'AssignRule' //код объекта доступа для данной формы

});
