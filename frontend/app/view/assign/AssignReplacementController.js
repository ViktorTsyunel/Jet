Ext.define('AML.view.assign.AssignReplacementController', {
    requires: [
        'AML.model.AssignReplacement',
        'Ext.data.StoreManager'
    ],
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.assignreplacement',
    childForm: 'app-assign-assignreplacement-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'REPLACEMENT_SEQ_ID', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'AssignReplacement', //код объекта доступа для данной формы

    applyAssignReplacementFilter: function () {
        var self = this,
            vm = this.getViewModel(),
            filters = [],
            activeCB = vm.get('activeCB'),
            dateFrom = vm.get('dateFrom'),
            dateTo = vm.get('dateTo'),
            store = this.getStore('assignreplacement');

        if (!store.isLoaded()) return;

        if (activeCB) {
            filters.push({
                property: 'ACTIVE_FL',
                value: true
            });
        }

        /*if (dateFrom && dateTo) {
            filters.push({
                filterFn: function(item) {
                    return Ext.Date.between(item.get('START_DT'), dateFrom, dateTo);
                }
            });
        }*/

        store.load({
            scope: self,
            params: {
                form: 'AssignReplacement',
                action: 'read',
                startDt: dateFrom,
                endDt: dateTo
            }
        });

        store.clearFilter();
        store.filter(filters);
    }
});
