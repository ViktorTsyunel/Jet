Ext.define('AML.view.assign.AssignRule', {
    extend: 'Ext.grid.Panel',
    requires: [
        'AML.view.assign.AssignRuleController',
        'Ext.data.StoreManager',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox'
    ],
    xtype: 'app-assign-assignrule',
    controller: 'assignrule',
    viewModel: {
        type: 'assignrulevmdl'
    },
    bind: {
        store: '{assignrule}',
        title: '{title}'
    },
    stateful: true,
    stateId: 'assignrule',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    columns: [
        {
            text: 'Номер правила',
            identifier: true,
            stateId: 'RULE_SEQ_ID',
            dataIndex: 'RULE_SEQ_ID',
            flex: 0.5
        },
        {
            text: 'Ответственный',
            stateId: 'OWNER_SEQ_ID',
            dataIndex: 'OWNER_SEQ_ID',
            renderer: function (value) {
                var Owners = Ext.data.StoreManager.lookup('Owners'),
                    rec;
                if (Owners && value) {
                    rec = Owners.getById(value);
                }
                return (rec && rec.get('OWNER_DSPLY_NM')) || '';
            },
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Вторая очередь?',
            stateId: 'ORDER2_FL',
            dataIndex: 'ORDER2_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Любой алерт?',
            stateId: 'ANY_FL',
            dataIndex: 'ANY_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            text: 'Код ОПОК',
            stateId: 'OPOK_NB',
            dataIndex: 'OPOK_NB',
            flex: 0.5
        },
        {
            text: 'Тербанк',
            stateId: 'TB_ID',
            dataIndex: 'TB_ID',
            renderer: function (value) {
                var TB = Ext.data.StoreManager.lookup('TB'),
                    rec;
                if (TB && value) {
                    rec = TB.getById(value);
                }
                return (rec && rec.get('TB_NAME')) || '';
            },
            flex: 0.5
        },
        {
            text: 'ОСБ',
            stateId: 'OSB_ID',
            dataIndex: 'OSB_ID',
            renderer: function (value, metaData, record) {
                var TBOSB = Ext.data.StoreManager.lookup('TBOSB'),
                    rec;
                if (TBOSB && value) {
                    rec = TBOSB.getById(record.get('TB_ID') + '-' + value);
                }
                return (rec && rec.get('ORG_NM')) || '';
            },
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Активно?',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Начало действия',
            stateId: 'START_DT',
            dataIndex: 'START_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Окончание действия',
            stateId: 'END_DT',
            dataIndex: 'END_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто создал',
            stateId: 'CREATED_BY',
            dataIndex: 'CREATED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда создал',
            stateId: 'CREATED_DATE',
            dataIndex: 'CREATED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто изменил',
            stateId: 'MODIFIED_BY',
            dataIndex: 'MODIFIED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда изменил',
            stateId: 'MODIFIED_DATE',
            dataIndex: 'MODIFIED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        },
        '-',
        {
            xtype: 'combobox',
            store: 'Owners',
            editable: true,
            typeAhead: true,
            typeAheadDelay: 500,
            forceSelection: true,
            queryMode: 'local',
            tpl: Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                '</tpl>'
            ),
            fieldLabel: 'Ответственный',
            labelWidth: 80,
            matchFieldWidth: false,
            displayField: 'OWNER_DSPLY_NM',
            valueField: 'id',
            triggerClear: true,
            // multiSelect: true,
            minChars: 1,
            margin: '0 10 0 0',
            flex: 1,
            bind: {
                value: '{selectedOwner}'
            },
            listeners: {
                expand: function (field) {
                    field.getStore().filter([
                        {
                            property: 'IS_ALLOW_ALERT_EDITING',
                            value: 'Y'
                        }
                        ]);
                    field.getPicker().refresh();
                }
            }
        },
        {
            xtype: 'combobox',
            store: 'TB',
            editable: false,
            queryMode: 'local',
            tpl: Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                '<tpl if="id &gt; 0">{id} - {label}' +
                '<tpl else>{label}</tpl>' +
                '</div>',
                '</tpl>'
            ),
            fieldLabel: 'Тербанк',
            labelWidth: 50,
            displayField: 'label',
            valueField: 'id',
            triggerClear: true,
            multiSelect: true,
            margin: '0 10 0 0',
            flex: 1,
            bind: {
                value: '{selectedTB}'
            }
        },
        {
            xtype: 'combobox',
            store: 'TBOSB',
            disabled: true,
            editable: false,
            queryMode: 'local',
            tpl: Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                '</tpl>'
            ),
            fieldLabel: 'ОСБ',
            labelWidth: 60,
            displayField: 'label',
            valueField: 'id',
            triggerClear: true,
            multiSelect: true,
            margin: '0 10 0 0',
            flex: 1,
            bind: {
                value: '{selectedOSB}',
                disabled: '{isOSBFieldDisabled}'
            }
        },
        {
            xtype: 'combobox',
            store: 'OPOK',
            //disabled: true,
            tpl: Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                '<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                '</tpl>'
            ),
            matchFieldWidth: false,
            fieldLabel: 'Код ОПОК',
            labelWidth: 60,
            displayField: 'OPOK_NB',
            valueField: 'OPOK_NB',
            forceSelection: true,
            multiSelect: true,
            editable: false,
            queryMode: 'local',
            tooltip: {
                title: 'По коду ОПОК',
                text: 'Код операции, подлежащей обязательному контролю в соответствии с перечнем РФМ: 1001-1008, 3001, 3011, 3021, 4001-4007, 5001-5007, 6001, 7001, 8001, 9001'
            },
            flex: 1,
            bind: {
                value: '{selectedCode}'
            },
            triggerClear: true
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            flex: 0.5,
            checked: true,
            value: true,
            bind: {
                value: '{activeCB}'
            }
        }
    ]
});
