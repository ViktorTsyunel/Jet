Ext.define('AML.view.assign.AssignRuleFormViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.assignruleformviewmodel',
    data: {
        title: 'Правило назначения ответственного',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        isOSBFieldDisabled2: {
            bind: {
                TB_ID: '{record.TB_ID}'
            },
            get: function (data) {
                var osbIds = data.TB_ID,
                    tbosb = this.getParent().getStore('tbosbassrule'); // || Ext.data.StoreManager.lookup('TBOSB');

                tbosb.clearFilter();
                tbosb.filterBy(function (record) {
                    return Ext.Array.indexOf([osbIds], record.get("tb_id")) !== -1;
                }, this);

                return !tbosb.getCount();

            }
        }
    }
});
