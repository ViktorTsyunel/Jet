Ext.define('AML.view.assign.AssignRuleViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.assignrulevmdl',
    data: {
        title: 'Правила назначения ответственных',
        selectedOwner: null,
        selectedTB: null,
        selectedOSB: null,
        selectedCode: null,
        activeCB: true,
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        isOSBFieldDisabled: {
            bind: {
                selectedTB: '{selectedTB}'
            },
            get: function (data) {
                var osbIds = data.selectedTB,
                    tbosb = Ext.data.StoreManager.lookup('TBOSB'),
                    disabled;

                if(osbIds) {
                    tbosb.clearFilter();
                    tbosb.filterBy(function (record) {
                        return Ext.Array.indexOf(osbIds, record.get("tb_id")) !== -1;
                    }, this);
                }
                disabled = !tbosb.getCount();
                disabled && this.set('selectedOSB', null);
                return disabled;
            }
        },
        filters: {
            bind: {
                activeCB: '{activeCB}',
                selectedOwner: '{selectedOwner}',
                selectedTB: '{selectedTB}',
                selectedOSB: '{selectedOSB}',
                selectedCode: '{selectedCode}'
            },
            get: function(data) {
                var store = this.getStore('assignrule'),
                    filters = [],
                    activeCB = data.activeCB,
                    ownerIDs = data.selectedOwner,
                    tbIDs = data.selectedTB || [],
                    osbIDs = data.selectedOSB || [],
                    codeIDs = data.selectedCode || [];

                if (activeCB) {
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                if (ownerIDs) {
                    filters.push({
                        property: 'OWNER_SEQ_ID',
                        value: ownerIDs
                    });
                }
                if (tbIDs.length) {
                    filters.push({
                        filterFn: function(item) {
                            return Ext.Array.indexOf(tbIDs, item.get('TB_ID')) !== -1;
                        }
                    });
                }
                if (osbIDs.length) {
                    filters.push({
                        filterFn: function(item) {
                            return Ext.Array.indexOf(osbIDs, item.get('TB_ID') + '-' + item.get('OSB_ID')) !== -1;
                        }
                    });
                }

                if (codeIDs.length) {
                    filters.push({
                        filterFn: function(item) {
                            return Ext.Array.indexOf(codeIDs, item.get('OPOK_NB')) !== -1;
                        }
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник правил назначения ответственных
        assignrule: {
            model: 'AML.model.AssignRule',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OWNER_SEQ_ID',
                    direction: 'DESC'
                }
            ],
            filters: '{filters}'
        },
        tbosbassrule: {
            model: 'AML.model.TBOSB',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'TBOSB',
                    action: 'read'
                }
            },
            sorters: [
                {
                    property: 'ORG_NM',
                    direction: 'ASC'
                }
            ]
        }
    }
});
