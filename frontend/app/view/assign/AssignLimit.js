Ext.define('AML.view.assign.AssignLimit', {
    extend: 'Ext.grid.Panel',
    requires: [
        'AML.view.assign.AssignLimitController',
        'Ext.data.StoreManager',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox'
    ],
    xtype: 'app-assign-assignlimit',
    controller: 'assignlimit',
    viewModel: {
        type: 'assignlimitvmdl'
    },
    bind: {
        store: '{assignlimit}'
    },
    title: 'Лимиты назначения',
    stateful: true,
    stateId: 'assignlimit',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    columns: [
        {
            text: 'Номер лимита',
            identifier: true,
            stateId: 'LIMIT_SEQ_ID',
            dataIndex: 'LIMIT_SEQ_ID',
            flex: 0.5
        },
        {
            text: 'Вид лимита',
            stateId: 'LIMIT_TP_CD',
            dataIndex: 'LIMIT_TP_CD',
            renderer: function (value) {
                var LimitTypes = Ext.data.StoreManager.lookup('LimitTypes'),
                    rec;
                if (LimitTypes && value) {
                    rec = LimitTypes.findRecord('LIMIT_TP_CD', value);
                }
                return (rec && rec.get('LIMIT_TP_NM')) || '';
            },
            flex: 0.5
        },
        {
            text: 'Роль',
            stateId: 'ROLE_CD',
            dataIndex: 'ROLE_CD',
            renderer: function (value) {
                var Roles = Ext.data.StoreManager.lookup('Roles'),
                    rec;
                if (Roles && value) {
                    rec = Roles.getById(value);
                }
                return (rec && rec.get('ROLE_NM')) || '';
            },
            flex: 0.5
        },
        {
            text: 'Ответственный',
            stateId: 'OWNER_SEQ_ID',
            dataIndex: 'OWNER_SEQ_ID',
            renderer: function (value) {
                var Owners = Ext.data.StoreManager.lookup('Owners'),
                    rec;
                if (Owners && value) {
                    rec = Owners.getById(value);
                }
                return (rec && rec.get('OWNER_DSPLY_NM')) || '';
            },
            flex: 0.5
        },
        {
            text: 'Величина лимита первой очереди распределения',
            stateId: 'ORDER1_LIMIT_QT',
            dataIndex: 'ORDER1_LIMIT_QT',
            flex: 0.5
        },
        {
            text: 'Лимит полный',
            stateId: 'FULL_LIMIT_QT',
            dataIndex: 'FULL_LIMIT_QT',
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Активность замещения',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Начало действия лимита',
            stateId: 'START_DT',
            dataIndex: 'START_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Окончание действия лимита',
            stateId: 'END_DT',
            dataIndex: 'END_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто создал',
            stateId: 'CREATED_BY',
            dataIndex: 'CREATED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда создал',
            stateId: 'CREATED_DATE',
            dataIndex: 'CREATED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто изменил',
            stateId: 'MODIFIED_BY',
            dataIndex: 'MODIFIED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда изменил',
            stateId: 'MODIFIED_DATE',
            dataIndex: 'MODIFIED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        },
        '-',
        {
            xtype: 'combobox',
            store: 'LimitTypes',
            editable: false,
            queryMode: 'local',
            tpl: Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{LIMIT_TP_NM}</div>',
                '</tpl>'
            ),
            fieldLabel: 'Вид лимита',
            labelWidth: 80,
            width: 250,
            matchFieldWidth: true,
            displayField: 'LIMIT_TP_NM',
            valueField: 'LIMIT_TP_CD',
            triggerClear: true,
            multiSelect: true,
            margin: '0 10 0 0',
            bind: {
                value: '{limitTP}'
            },
            listeners: {
                change: {
                    fn: 'applyAssignLimitFilter',
                    buffer: 555
                }
            }
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            checked: true,
            value: true,
            bind: {
                value: '{activeCB}'
            },
            listeners: {
                change: {
                    fn: 'applyAssignLimitFilter',
                    buffer: 555
                }
            }
        }
    ]
});
