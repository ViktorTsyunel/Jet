Ext.define('AML.view.assign.AssignRuleForm', {
    extend: 'Ext.window.Window',
    requires: [
        'AML.view.settings.OPOKFormController',
        'AML.view.main.BaseFormViewModel',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox'
    ],
    xtype: 'app-assign-assignrule-form',
    controller: 'assignruleform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'assignruleformviewmodel'
    },
    width: 850,
    bind: {
        title: '{title}'
    },
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            defaults: {
                labelWidth: 150
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults: {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'Owners',
                                    editable: true,
                                    typeAhead: true,
                                    typeAheadDelay: 500,
                                    forceSelection: true,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Ответственный',
                                    displayField: 'OWNER_DSPLY_NM',
                                    valueField: 'id',
                                    triggerClear: true,
                                    allowBlank: false,
                                    matchFieldWidth: false,
                                    bind: {
                                        value: '{record.OWNER_SEQ_ID}'
                                    },
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().filter([{property: "IS_ALLOW_ALERT_EDITING", value: "Y"}])
                                            field.getPicker().refresh();
                                        }
                                    }
                                },
                                {
                                    name: 'OPOK_NB',
                                    xtype: 'combobox',
                                    store: 'OPOK',
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                        '</tpl>'
                                    ),
                                    matchFieldWidth: false,
                                    displayField: 'OPOK_NB',
                                    valueField: 'OPOK_NB',
                                    //bind: '{record.OPOK_NB}',
                                    fieldLabel: 'Код ОПОК',
                                    triggerClear: true,
                                    bind: {
                                        value: '{record.OPOK_NB}'
                                    }
                                },
                                {
                                    xtype: 'checkboxfield',
                                    fieldLabel: 'Любой алерт?',
                                    bind: {
                                        value: '{record.ANY_FL}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'TB',
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="id &gt; 0">{id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Тербанк',
                                    displayField: 'label',
                                    valueField: 'id',
                                    triggerClear: true,
                                    bind: {
                                        value: '{record.TB_ID}'
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    //disabled: true,
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'ОСБ',
                                    displayField: 'label',
                                    valueField: 'OSB_NB',
                                    triggerClear: true,
                                    forceSelection: true,
                                    bind: {
                                        store: '{tbosbassrule}',
                                        value: '{record.OSB_ID}',
                                        disabled: '{isOSBFieldDisabled2}'
                                    }
                                },
                                {
                                    xtype: 'checkboxfield',
                                    fieldLabel: 'Вторая очередь?',
                                    bind: {
                                        value: '{record.ORDER2_FL}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Период действия',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults: {
                                flex: 1,
                                xtype: 'datefield',
                                labelWidth: 130,
                                format: 'd.m.Y',
                                startDay: 1
                            },
                            items: [
                                {
                                    fieldLabel: 'Начало действия',
                                    bind: {
                                        value: '{record.START_DT}'
                                    },
                                    reference: 'startDateAssRule',
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.validateValue(false);
                                                            container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                        }
                                                    }
                                                    container.nextSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.validateValue(false);
                                                            container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    fieldLabel: 'Окончание действия',
                                    margin: '0 0 0 10',
                                    bind: {
                                        value: '{record.END_DT}'
                                    },
                                    reference: 'endDateAssRule',
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                }
                                                container.previousSibling('datefield').focus();
                                                container.focus();
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Активно?',
                            margin: '0 0 0 30',
                            bind: {
                                value: '{record.ACTIVE_FL}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Аудит',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults: {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            defaults: {
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто создал',
                                    bind: {
                                        value: '{record.CREATED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда создал',
                                    bind: {
                                        value: '{record.CREATED_DATE:date("d.m.Y")}'
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            defaults: {
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто изменил',
                                    bind: {
                                        value: '{record.MODIFIED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда изменил',
                                    bind: {
                                        value: '{record.MODIFIED_DATE:date("d.m.Y")}'
                                    }
                                }

                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveFormAssRule',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelFormAssRule',
                    disabled: false,
                    text: 'Закрыть'
                }
            ]
        }
    ],
    listeners: {
        'afterrender': function (elem) {
            var self = this;
            // текущие права
            if (!self.controller.accessObject) {
                return;
            }
            var storeAccess = Ext.getStore('ObjectsAccess');
            if (!storeAccess) {
                return;
            }
            var i = storeAccess.findExact('id', self.controller.accessObject);

            // если доступ на чтение
            if (storeAccess.getAt(i).get('mode') === 'R') {
                elem.query('field').forEach(function (field) {
                    field.setReadOnly(true);
                });
                elem.down('#saveFormAssRule').setDisabled(true);
            }
            elem.down('#cancelFormAssRule').setDisabled(false);
        }
    }
});