Ext.define('AML.view.assign.AssignLimitFormViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.assignlimitformvmdl',
	data: {
		title: 'Редактировать лимит назначения',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
	},
    formulas: {
        isRoleFieldDisabled: {
            bind: {
                LIMIT_TP_CD: '{record.LIMIT_TP_CD}'
            },
            get: function (data) {
                var disabled = (data.LIMIT_TP_CD !== 'R');
                disabled && this.set('record.ROLE_CD', null);
                return disabled;
            }
        },
        isOwnerFieldDisabled: {
            bind: {
                LIMIT_TP_CD: '{record.LIMIT_TP_CD}'
            },
            get: function (data) {
                var disabled = ( data.LIMIT_TP_CD !== 'U');
                disabled && this.set('record.OWNER_SEQ_ID', null);
                return disabled;
            }
        }
    }
});
