Ext.define('AML.view.assign.AssignReplacementFormController', {
    requires: [
        'Ext.data.StoreManager'
    ],
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.assignreplacementform',

    doSave: function (store, record, window, closeFlag) {
        var vm = this.getViewModel();

        record.set('OWNER_DSPLY_NM', vm.get('selectedOwner.OWNER_DSPLY_NM'));
        record.set('NEW_OWNER_DSPLY_NM', vm.get('selectedNewOwner.OWNER_DSPLY_NM'));

        this.callParent(arguments);
    },

    // функция, возвращающая предупреждение для пользователя перед сохранением данных (если возвращает null - без предупреждения)
    getSaveConfirmText: function(form) {
        // Если поля "Замещаемый" и "Замещающий" равны:
        if(form.getFields().items[0].value === form.getFields().items[1].value)
            return 'Замещаемый сотрудник совпадает с замещающим. Вы уверены?';

        // Если поле "Начало" периода замещения больше поля "Окончание":
        if(new Date(form.getFields().items[2].value) > new Date(form.getFields().items[3].value))
            return 'Дата начала периода замещения больше даты окончания. Вы уверены?';

        return null;
    }
});
