Ext.define('AML.view.assign.AssignReplacement', {
    extend: 'Ext.grid.Panel',
    requires: [
        'AML.view.assign.AssignReplacementController',
        'Ext.data.StoreManager',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox'
    ],
    xtype: 'app-assign-assignreplacement',
    controller: 'assignreplacement',
    viewModel: {
        type: 'assignreplacementvmdl'
    },
    bind: {
        store: '{assignreplacement}'
    },
    title: 'Замещение ответственных',
    stateful: true,
    stateId: 'assignreplacement',
    columns: [
        {
            text: 'Номер замещения',
            identifier: true,
            stateId: 'REPLACEMENT_SEQ_ID',
            dataIndex: 'REPLACEMENT_SEQ_ID',
            flex: 0.5
        },
        {
            text: 'Замещаемый пользователь',
            stateId: 'OWNER_DSPLY_NM',
            dataIndex: 'OWNER_DSPLY_NM',
            flex: 0.5
        },
        {
            text: 'Замещающий пользователь',
            stateId: 'NEW_OWNER_DSPLY_NM',
            dataIndex: 'NEW_OWNER_DSPLY_NM',
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Активность замещения',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Начало периода замещения',
            stateId: 'START_DT',
            dataIndex: 'START_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Окончание периода замещения',
            stateId: 'END_DT',
            dataIndex: 'END_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто создал',
            stateId: 'CREATED_BY',
            dataIndex: 'CREATED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда создал',
            stateId: 'CREATED_DATE',
            dataIndex: 'CREATED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто изменил',
            stateId: 'MODIFIED_BY',
            dataIndex: 'MODIFIED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда изменил',
            stateId: 'MODIFIED_DATE',
            dataIndex: 'MODIFIED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        },
        '-',
        {
            xtype: 'datefield',
            itemId: 'dateFrom',
            labelWidth: 40,
            labelAlign: 'right',
            format: 'd.m.Y',
            startDay: 1,
            fieldLabel: 'Дата с',
            allowBlank: false,
            bind: {
                value: '{dateFrom}'
            },
            listeners: {
                validitychange: function (container, value, eOpts) {
                    if (value) {
                        if (container.nextSibling('datefield').getValue()) {
                            if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                    container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                }
                            }
                            container.nextSibling('datefield').focus();
                            container.focus();
                        }
                    }
                },
                blur: function (container, value, eOpts) {
                    if (value) {
                        if (container.nextSibling('datefield').getValue()) {
                            if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                    container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            xtype: 'datefield',
            itemId: 'dateTo',
            labelAlign: 'right',
            labelWidth: 20,
            format: 'd.m.Y',
            startDay: 1,
            fieldLabel: 'по',
            allowBlank: false,
            bind: {
                value: '{dateTo}'
            },
            listeners: {
                validitychange: function (container, value, eOpts) {
                    if (value) {
                        if (container.previousSibling('datefield').getValue() && container.getValue()) {
                            if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                            }
                        }
                        container.previousSibling('datefield').focus();
                        container.focus();
                    }
                },
                blur: function (container, value, eOpts) {
                    if (value) {
                        if (container.previousSibling('datefield').getValue() && container.getValue()) {
                            if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                            }
                        }
                    }
                }
            }
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            width: 130,
            checked: true,
            value: true,
            bind: {
                value: '{activeCB}'
            }
        },
        '-',
        {
            xtype: 'button',
            text: 'Показать',
            margin: '1px 1px 1px 3px',
            iconCls: 'icon-document-search',
            handler: 'applyAssignReplacementFilter',
            id: 'applyAssignReplacementFilterButtom',
            disabled: false
        }
    ],
    listeners: {
        'afterrender': function (elem) {
            var self = this;
            // текущие права
            if (!self.controller.accessObject) {
                return;
            }
            var storeAccess = Ext.getStore('ObjectsAccess');
            if (!storeAccess) {
                return;
            }
            var i = storeAccess.findExact('id', self.controller.accessObject);

            // если доступ на чтение
            if (storeAccess.getAt(i).get('mode') === 'R') {
                elem.down('#create').setDisabled(true);
                elem.down('#remove').setDisabled(true);
            }
            elem.down('#dateFrom').setDisabled(false);
            elem.down('#dateTo').setDisabled(false);
            elem.down('#applyAssignReplacementFilterButtom').setDisabled(false);
        }
    }
});
