Ext.define('AML.view.assign.AssignLimitViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.assignlimitvmdl',
	data: {
		title: 'Лимиты назначения',
        activeCB: true,
        limitTP: [],
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
	},
    stores: {
        // Справочник Лимиты назначения
        assignlimit: {
            model: 'AML.model.AssignLimit',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'LIMIT_SEQ_ID',
                    direction: 'DESC'
                }
            ],
            filters: [
                {
                    property: 'ACTIVE_FL',
                    value: true
                }
            ]
        }
    },
    formulas: {
        isRoleFieldDisabled: {
            bind: {
                LIMIT_TP_CD: '{record.LIMIT_TP_CD}'
            },
            get: function (data) {
                var disabled = (data.LIMIT_TP_CD !== 'R');
                disabled && this.set('record.ROLE_CD', null);
                return disabled;
            }
        },
        isOwnerFieldDisabled: {
            bind: {
                LIMIT_TP_CD: '{record.LIMIT_TP_CD}'
            },
            get: function (data) {
                var disabled = ( data.LIMIT_TP_CD !== 'U');
                disabled && this.set('record.OWNER_SEQ_ID', null);
                return disabled;
            }
        }
    }
});
