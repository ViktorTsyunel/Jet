Ext.define('AML.view.assign.AssignLimitController', {
    requires: [
        'AML.model.AssignLimit',
        'Ext.data.StoreManager'
    ],
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.assignlimit',
    childForm: 'app-assign-assignlimit-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'LIMIT_SEQ_ID', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'AssignLimit', //код объекта доступа для данной формы

    applyAssignLimitFilter: function () {
        var self = this,
            vm = this.getViewModel(),
            filters = [],
            activeCB = vm.get('activeCB'),
            limitTP = vm.get('limitTP'),
            store = this.getStore('assignlimit');

        if (!store.isLoaded()) return;

        if (activeCB) {
            filters.push({
                property: 'ACTIVE_FL',
                value: true
            });
        }
        if (limitTP.length) {
            filters.push({
                filterFn: function(item) {
                    return Ext.Array.indexOf(limitTP, item.get('LIMIT_TP_CD')) !== -1;
                }
            });
        }

        store.clearFilter();
        store.filter(filters);
    }
});
