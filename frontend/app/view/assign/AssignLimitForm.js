Ext.define('AML.view.assign.AssignLimitForm', {
    extend: 'Ext.window.Window',
    requires: [
        'AML.view.settings.OPOKFormController',
        'AML.view.main.BaseFormViewModel',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox'
    ],
    xtype: 'app-assign-assignlimit-form',
    controller: 'assignlimitform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'assignlimitformvmdl'
    },
    width: 850,
    bind: {
     title: '{title}'
     },
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            defaults: {
                labelWidth: 150
            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'LimitTypes',
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{LIMIT_TP_NM}</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Вид лимита',
                                    matchFieldWidth: true,
                                    displayField: 'LIMIT_TP_NM',
                                    valueField: 'LIMIT_TP_CD',
                                    triggerClear: true,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.LIMIT_TP_CD}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'Roles',
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{ROLE_NM} ({ROLE_CD})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Роль',
                                    displayField: 'ROLE_NM',
                                    valueField: 'ROLE_CD',
                                    triggerClear: true,
                                    allowBlank: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.ROLE_CD}',
                                        disabled: '{isRoleFieldDisabled}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'Owners',
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{name} ({login})</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Пользователь (ответственный)',
                                    displayField: 'OWNER_DSPLY_NM',
                                    valueField: 'id',
                                    triggerClear: true,
                                    allowBlank: false,
                                    disabled: true,
                                    bind: {
                                        value: '{record.OWNER_SEQ_ID}',
                                        disabled: '{isOwnerFieldDisabled}'                                    },
                                    listeners: {
                                        expand: function (field) {
                                            field.getStore().addFilter([
                                                {
                                                property: 'IS_ALLOW_ALERT_EDITING',
                                                value: 'Y'
                                                }
                                                ]);
                                            field.getPicker().refresh();
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Лимит первой очереди',
                                    bind: {
                                        value: '{record.ORDER1_LIMIT_QT}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Лимит полный',
                                    bind: {
                                        value: '{record.FULL_LIMIT_QT}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Период действия',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults:{
                                flex: 1,
                                xtype: 'datefield',
                                labelWidth: 130,
                                format: 'd.m.Y',
                                startDay: 1
                            },
                            items: [
                                {
                                    fieldLabel: 'Начало',
                                    allowBlank: true,
                                    bind: {
                                        value: '{record.START_DT}'
                                    },
                                    reference: 'startDateLimit',
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.validateValue(false);
                                                            container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                        }
                                                    }
                                                    container.nextSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.validateValue(false);
                                                            container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    fieldLabel: 'Окончание',
                                    allowBlank: true,
                                    margin: '0 0 0 10',
                                    bind: {
                                        value: '{record.END_DT}'
                                    },
                                    reference: 'endDateLimit',
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                }
                                                container.previousSibling('datefield').focus();
                                                container.focus();
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Активно?',
                            margin: '0 0 0 30',
                            allowBlank: false,
                            bind: {
                                value: '{record.ACTIVE_FL}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Аудит',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто создал',
                                    bind: {
                                        value: '{record.CREATED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда создал',
                                    bind: {
                                        value: '{record.CREATED_DATE:date("d.m.Y")}'
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто изменил',
                                    bind: {
                                        value: '{record.MODIFIED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда изменил',
                                    bind: {
                                        value: '{record.MODIFIED_DATE:date("d.m.Y")}'
                                    }
                                }

                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveFormLimit',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelFormLimit',
                    text: 'Закрыть'
                }
            ]
        }
    ],
    listeners: {
        'afterrender': function (elem) {
            var self = this;
            // текущие права
            if (!self.controller.accessObject) {
                return;
            }
            var storeAccess = Ext.getStore('ObjectsAccess');
            if (!storeAccess) {
                return;
            }
            var i = storeAccess.findExact('id', self.controller.accessObject);

            // если доступ на чтение
            if (storeAccess.getAt(i).get('mode') === 'R') {
                elem.query('field').forEach(function (field) {
                    field.setReadOnly(true);
                });
                elem.down('#saveFormLimit').setDisabled(true);
            }
            elem.down('#cancelFormLimit').setDisabled(false);
        }
    }
});