Ext.define('AML.view.assign.AssignLimitFormController', {
    requires: [
        'Ext.data.StoreManager'
    ],
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.assignlimitform',
    //код объекта доступа для данной формы
    accessObject: 'AssignLimit',
    config: {
        listen: {
            component: {
                '#saveFormLimit': {
                    click: 'onClickSaveFormLimit'
                },
                '#cancelFormLimit': {
                    click: 'onClickCancelFormLimit'
                }
            }
        }
    },

    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveFormLimit: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView();
        if (!window || !this.isChanges(window)) return;

        // валидация даты старта и окончания операции
        if (this.valideteDateForm(window, 'startDateLimit', 'endDateLimit')) {
            return;
        }

        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false);
    },

    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelFormLimit: function (button, eOpts) {
        var self = this,
            window = this.getView(),
            controller = this,
            fnSaveForm = this.saveForm;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    // валидация даты старта и окончания операции
                    if (self.valideteDateForm(window, 'startDateLimit', 'endDateLimit')) {
                        return;
                    }
                    fnSaveForm(window, controller, true);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    }
});
