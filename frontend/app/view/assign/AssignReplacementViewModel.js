Ext.define('AML.view.assign.AssignReplacementViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.assignreplacementvmdl',
	data: {
		title: 'Замещение ответственных',
        activeCB: true,
        dateFrom: Ext.Date.getFirstDateOfMonth(new Date()),
        dateTo: Ext.Date.getLastDateOfMonth(new Date()),
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
	},
    stores: {
        // Справочник Лимиты назначения
        assignreplacement: {
            model: 'AML.model.AssignReplacement',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OWNER_SEQ_ID',
                    direction: 'DESC'
                }
            ],
            filters: [
                {
                    property: 'ACTIVE_FL',
                    value: true
                }
            ],
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    transform: {
                        fn: function (data) {
                            if (Ext.isArray(data.data)) {
                                Ext.Array.forEach(data.data, function (i) {
                                    if (i.ACTIVE_FL != undefined) {
                                        i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
                                    }
                                });
                            }
                            return data;
                        },
                        scope: this
                    },
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function (data, request) {
                            var actionMap = {
                                create: 'create',
                                update: 'update',
                                destroy: 'delete',
                                read: 'read'
                            };
                            request.setParam('action', actionMap[request.getAction()] || 'read');
                            Ext.Array.forEach(data, function (i) {
                                if (i.ACTIVE_FL != undefined) {
                                    i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
                                }
                            });
                            return data;
                        },
                        scope: this
                    },
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'AssignReplacement',
                    action: 'read',
                    startDt: '{dateFrom}',
                    endDt: '{dateTo}'
                }
            }
        }
    }
});
