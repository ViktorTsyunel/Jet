Ext.define('AML.view.settings.OPOKTestController', {
	requires: [
		'Ext.Ajax',
		'Ext.JSON',
		'Ext.data.StoreManager'
	],
	extend: 'Ext.app.ViewController',
    alias: 'controller.opoktest',
	config: {
		listen: {
			component: {
				'app-settings-opoktest': {
					/**
					 * Обрабатываем событие закрытия окна
					 */
					close: function () {
						// Чистим метку о последнем открытом окне
						AML.app.window.clearLast();
						// Чистим хранилище
						var testData = Ext.data.StoreManager.lookup('OPOKTest');
						testData.loadData([], false);
						// Чистим фильтры для правил выявления (combobox)
						var OPOKRuleField = Ext.data.StoreManager.lookup('OPOKRuleField');
						OPOKRuleField.removeFilter('_id_');
						OPOKRuleField.addFilter({
							property: 'VERS_STATUS_CD',
							operator: '!=',
							value: 'ARC'
						});
					},
					/**
					 * Обрабатываем событие отрисовки
					 * @param window
					 */
					render: function (window) {
						var me = this;
						window.getEl().on('keypress', function (e) {
							if (e.getKey() == e.ESC) {
								var footer = window.down('panel[isFooter=true]');
								// Закрываем подвал если он открыт
								if (!footer.collapsed) {
									footer.collapse();
								// в ином случае закрываем форму
								} else {
									me.onClickCancelForm();
								}
							}
						});
						if (Ext.isIE8) {
							window.setWidth(Math.round(document.body.clientWidth / 100 * 95));
							window.setHeight(Math.round(document.body.clientHeight / 100 * 95));
						}
					}
				},
				'gridpanel': {
					/**
					 * Обрабатываем событие выбора записи таблицы
					 * @param sm
					 * @param selected
					 * @param eOpts
					 */
					selectionchange: function (sm, selected, eOpts) {
						if (selected.length) {
							this.getViewModel().set('footerRecord', selected[0]);
						}
					},
					/**
					 * Обрабатываем событие двойного клика по записи таблицы
					 * @param grid
					 * @param record
					 * @param index
					 * @param eOpts
					 */
					rowdblclick: function (grid, record, index, eOpts) {
						this.footerPanelExpand();
					},
					/**
					 * Обрабатываем событие нажатия кнопок клавиатуры
					 * @param table
					 * @param record
					 * @param tr
					 * @param rowIndex
					 * @param e
					 * @param eOpts
					 */
					rowkeydown: function (table, record, tr, rowIndex, e, eOpts) {
						switch (e.getKey()) {
							case e.ENTER:
							case e.SPACE:
								this.footerPanelExpand();
								break;
						}
					}
				},
				'panel[isFooter=true]': {
					/**
					 * Обрабатываем событие изменения размера подвала
					 * @param panel
					 * @param width
					 * @param height
					 * @param oldWidth
					 * @param oldHeight
					 * @param eOpts
					 */
					resize: function (panel, width, height, oldWidth, oldHeight, eOpts) {
						var grid = panel.ownerCt.down('gridpanel');
						if (height > 0 && grid.selection) {
							grid.getView().scrollRowIntoView(grid.store.indexOf(grid.selection));
						}
					}
				}
			}
		}
	},
	/**
	 * Запускаем тест правил выявления
	 * @param button
	 * @param event
	 * @param eOpts
	 */
	onClickSearch: function (button, event, eOpts) {
		var filter = [],
			me = this,
			toolbar = button.up('toolbar'),
			form = toolbar.down('form').getForm(),
			isRule = toolbar.down('tabpanel').getActiveTab().isRule,
			dtFrom = toolbar.down('datefield[name=dtFrom]').getValue(),
			dtTo = toolbar.down('datefield[name=dtTo]').getValue(),
			tbId = toolbar.down('combobox[name=tbId]').getValue(),
			ruleId = null,
			opok = null,
			ruleQuery = null,
			ruleStatus = null,
			criteriaStatus = null;
		button.setDisabled(true);
		if (isRule) {
			ruleId = toolbar.down('combobox[name=ruleId]').getValue();
		} else {
			opok = toolbar.down('combobox[name=opok]').getValue();
			ruleQuery = toolbar.down('combobox[name=ruleQuery]').getValue();
			ruleStatus = toolbar.down('combobox[name=ruleStatus]').getValue();
			criteriaStatus = toolbar.down('combobox[name=criteriaStatus]').getValue();
		}
		filter.push(
			{
				property: 'par_sdate',
				value: Ext.Date.format(dtFrom, 'Y-m-d H:i:s')
			},
			{
				property: 'par_edate',
				value: Ext.Date.format(dtTo, 'Y-m-d H:i:s')
			},
			{
				property: 'par_tb_id',
				value: tbId
			},
			{
				property: 'par_rule_seq_id',
				value: ruleId
			},
			{
				property: 'par_opok_nb',
				value: opok
			},
			{
				property: 'par_query_cd',
				value: ruleQuery
			},
			{
				property: 'par_vers_status_cd',
				value: ruleStatus
			},
			{
				property: 'par_crit_status_cd',
				value: criteriaStatus
			}
		);
		if (!isRule || form.isValid()) {
			var gridView = toolbar.up('gridpanel').view;
			gridView.mask('Считаем...');
			Ext.Ajax.request({
				url: common.globalinit.ajaxUrl,
				method: common.globalinit.ajaxMethod,
				headers: common.globalinit.ajaxHeaders,
				timeout: common.globalinit.ajaxTimeOut,
				params: Ext.JSON.encode({
					form: 'OPOKTest',
					action: 'estimate',
					filter: filter
				}),
				success: function (response) {
					var count = Ext.JSON.decode(response.responseText).count;
					if (count > 1000000) {
						AML.app.msg.confirm({
							type: 'warning',
							message: 'Выявление операций с данными параметрами может потребовать значительного времени. Примерное количество проверяемых операций: <b>' + count + '</b>.<br>Выполнить выявление?',
							fnYes: function () {
								me.loadStoreOPOKTest(button, filter);
							}
						});
					} else {
						me.loadStoreOPOKTest(button, filter);
					}
				},
				callback: function (options, success, response) {
					gridView.unmask();
					button.setDisabled(false);
				}
			});
		} else {
			button.setDisabled(false);
		}
	},
	/**
	 * Загружаем результат поиска
	 * @param button
	 * @param filter
	 */
	loadStoreOPOKTest: function (button, filter) {
		var me = this;
		button.setDisabled(true);
		Ext.data.StoreManager.lookup('OPOKTest').load({
			params: {
				filter: filter
			},
			callback: function (options, success, response) {
				if (success.success && success.getResultSet().count > 0) {
					me.setCounter(success.getResultSet().count, success.getResultSet().total);
					me.showCounter();
				} else {
					me.setCounter(0, 0);
				}
				button.setDisabled(false);
			}
		});
	},
	/**
	 * Устанавливаем значения счетчика
	 * @param load
	 * @param total
	 */
	setCounter: function (load, total) {
		var viewModel = this.getViewModel();
		viewModel.set('count.load', load);
		viewModel.set('count.total', total);
	},
	/**
	 * Показываем счетчик
	 */
	showCounter: function () {
		var view = this.getView();
		view.down('[isTabCount=true]').show();
	},
	/**
	 * Раскрываем подвал
	 */
	footerPanelExpand: function () {
		var panel = this.getView().down('panel[isFooter=true]');
		if (panel.collapsed) {
			panel.expand();
		}
	},
	/**
	 * Закрываем форму
	 * @param button
	 * @param eOpts
	 */
	onClickCancelForm: function (button, eOpts) {
		var win = this.getView();
		win.close();
	}
});
