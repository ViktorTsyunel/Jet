Ext.define('AML.view.assign.OPOKViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.opokvmdl',
	data: {
		title: 'Коды ОПОК',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
	}
});
