Ext.define('AML.view.settings.OPOKCriteriaForm', {
    extend: 'Ext.window.Window',
	requires: [
		'AML.view.settings.OPOKCriteriaFormController',
		'AML.view.main.BaseFormViewModel',
		'Ext.form.Panel',
		'Ext.form.FieldContainer',
		'Ext.layout.container.HBox',
		'Ext.form.field.Text',
		'Ext.form.field.ComboBox',
		'Ext.tab.Panel',
		'Ext.layout.container.VBox',
		'Ext.form.field.TextArea',
		'Ext.form.field.Checkbox',
		'Ext.button.Button'
	],
	xtype: 'app-settings-opokcriteria-form',
	controller: 'opokcriteriaform',
	iconCls: 'icon-report',
	viewModel: {
		type: 'baseformviewmodel',
		formulas: {
            buttonDisabled: function (get) {
                return get('isOpenReadOnly') || !get('record.TEST_CRIT_TX');
            }
        }
    },
	/*bind: {
		title: '{title}'
	},*/
	title: 'Стандартное условие выявления',
	closable: false,
	tools: [
		{
			type: 'close',
			callback: 'onClickCancelForm'
		}
	],
	items: [
        {
            xtype: 'form',
	        layout: {
		        type: 'vbox',
				align: 'stretch'
	        },
	        border: false,
	        defaults: {
		        labelWidth: 150
	        },
	        items: [
		        {
			        xtype: 'fieldcontainer',
			        layout: 'hbox',
			        defaults: {
				        labelWidth: 150
			        },
			        items: [
				        {
					        xtype: 'textfield',
					        bind: '{record.CRIT_CD}',
					        fieldLabel: 'Код условия',
					        allowBlank: false,
					        flex: 1
				        },
				        {
					        xtype: 'combobox',
					        store: 'OPOKCriteriaTypes',
					        editable: false,
					        queryMode: 'local',
							tpl: Ext.create('Ext.XTemplate',
								'<tpl for=".">',
								'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
								'</tpl>'
							),
					        displayField: 'label',
					        valueField: 'id',
					        bind: '{record.CRIT_TP_CD}',
					        fieldLabel: 'Тип условия',
					        labelAlign: 'right',
					        allowBlank: false,
					        flex: 1
				        }
			        ]
		        },
		        {
			        xtype: 'textfield',
			        bind: '{record.CRIT_NM}',
			        fieldLabel: 'Наименование',
			        allowBlank: false
		        },
		        {
			        xtype: 'tabpanel',
			        plain: true,
			        bodyPadding: 10,
			        activeTab: 0,
			        items: [
				        {
					        title: 'Действующие условия',
					        layout: {
						        type: 'vbox',
						        align: 'stretch'
					        },
					        defaults: {
						        labelWidth: 140
					        },
					        items: [
						        {
							        xtype: 'textarea',
							        name: 'CRIT_TX',
							        bind: {
								        value: '{record.CRIT_TX}',
								        originalValue: '{record.CRIT_TX}'
							        },
							        fieldLabel: 'Текст условия',
							        allowBlank: false
						        },
						        {
							        xtype: 'textarea',
							        name: 'SUBTLE_CRIT_TX',
							        bind: {
								        value: '{record.SUBTLE_CRIT_TX}',
								        originalValue: '{record.SUBTLE_CRIT_TX}'
							        },
							        fieldLabel: 'Уточняющее условие'
						        },
						        {
							        xtype: 'fieldcontainer',
							        layout: {
								        type: 'hbox'
							        },
							        items: [
								        {
									        xtype: 'checkbox',
									        name: 'NULL_FL',
									        labelWidth: 280,
									        labelAlign: 'right',
									        bind: {
										        value: '{record.NULL_FL}',
										        originalValue: '{record.NULL_FL}'
									        },
									        fieldLabel: 'Неизвестное значение?',
									        flex: 1
								        },
								        {
									        xtype: 'button',
									        align: 'right',
									        text: 'Копировать в тестовые',
									        handler: 'onClickCopyActiveToTest'
								        }
							        ]
						        }
					        ]
				        },
				        {
					        title: 'Тестовые условия',
					        layout: {
						        type: 'vbox',
						        align: 'stretch'
					        },
					        defaults: {
						        labelWidth: 140
					        },
					        items: [
						        {
							        xtype: 'textarea',
							        bind: '{record.TEST_CRIT_TX}',
							        fieldLabel: 'Текст условия'
						        },
						        {
							        xtype: 'textarea',
							        bind: '{record.TEST_SUBTLE_CRIT_TX}',
							        fieldLabel: 'Уточняющее условие'
						        },
								{
									xtype: 'fieldcontainer',
									layout: {
										type: 'hbox'
									},
									items: [
								        {
									        xtype: 'checkbox',
									        labelWidth: 280,
									        labelAlign: 'right',
									        bind: '{record.TEST_NULL_FL}',
									        fieldLabel: 'Неизвестное значение?',
									        flex: 1
								        },
										{
											xtype: 'button',
											itemId: 'clickTestToActive',
											align: 'right',
											text: 'Сделать действующими',
											disabled: true,
											bind: {
												disabled: '{buttonDisabled}'
											},
											handler: 'onClickMoveTestToActive'
										}
									]
								}
					        ]
				        }
			        ]
		        },
		        {
			        xtype: 'fieldcontainer',
			        layout: 'hbox',
			        defaults: {
				        labelWidth: 150
			        },
			        padding: '5 0 0 0',
			        items: [
				        {
					        xtype: 'combobox',
					        editable: false,
					        store: 'OPOKCriteriaUses',
					        queryMode: 'local',
					        tpl: Ext.create('Ext.XTemplate',
						        '<tpl for=".">',
						        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
						        '</tpl>'
					        ),
					        displayField: 'label',
					        valueField: 'id',
					        bind: '{record.CRIT_USE_CD}',
					        fieldLabel: 'Вид использования',
					        allowBlank: false,
					        flex: 1
				        },
				        {
					        xtype: 'checkbox',
					        labelAlign: 'right',
							bind: '{record.ACTIVE_FL}',
					        fieldLabel: 'Активно?',
					        allowBlank: false,
					        flex: 1,
							listeners: {
								afterrender: function (field, newValue) {
										var accessObj = "OPOKCriteria";
										var storeAccess = Ext.getStore('ObjectsAccess');
										if (!storeAccess) return;

										var i = storeAccess.findExact('id', accessObj);

										// если доступ на тестирование
										if (storeAccess.getAt(i).get('mode') == 'T') {
											field.disable();
										}
								}
							}
				        }
			        ]
		        },
		        {
			        xtype: 'textarea',
			        bind: '{record.NOTE_TX}',
			        fieldLabel: 'Примечание'
		        }
	        ],
	        buttons: [
		        {
			        itemId: 'saveForm',
			        text: 'Применить'
		        },
		        {
			        itemId: 'cancelForm',
			        text: 'Закрыть'
		        }
	        ]
        }
    ],
	listeners: {
	'afterrender': function (elem) {
		var accessObj = "OPOKCriteria";
		var storeAccess = Ext.getStore('ObjectsAccess');
		if (!storeAccess) return;

		var i = storeAccess.findExact('id', accessObj);

		// если доступ на тестирование
		if (storeAccess.getAt(i).get('mode') == 'T') {
			elem.query('tabpanel')[0].query('field').filter(function(field) {
				if (field.name === 'CRIT_TX' || field.name === 'SUBTLE_CRIT_TX' || field.name === 'NULL_FL') {
					field.setDisabled(true);
				}
			});
			elem.down('#clickTestToActive').setDisabled(true);
		}
	}
}
});