Ext.define('AML.view.settings.OPOKRuleFormController', {
    requires: [
        'Ext.Ajax',
        'Ext.Array',
        'Ext.JSON',
        'Ext.data.StoreManager'
    ],
    extend: 'AML.view.main.BaseFormController', //'Ext.app.ViewController',
    alias: 'controller.opokruleform',
    accessObject: 'OPOKRule', //код объекта доступа для данной формы

    init: function() {
        Ext.data.StoreManager.lookup('OPOKCriteria').load();
    },

    // метод, возвращающий предупреждение для пользователя перед сохранением данных (если возвращает null - без предупреждения)
    getSaveConfirmText: function(form) {
        var verStatus = form.findField('VERS_STATUS_CD');

        if (verStatus.value == 'ACT') {
            return verStatus.originalValue != verStatus.value
                ? 'Вы делаете эту версию правила действующей.<br>Вы уверены?'
                : 'Вы вносите изменения в действующую версию правила.<br>Вы уверены?';
        }
        else if (verStatus.originalValue == 'ACT' && verStatus.originalValue != verStatus.value) {
            return 'Вы делаете действующую версию правила недействующей.<br>Вы уверены?';
        }
        else {
            return null;
        }
    },
    /*doSave: function (store, record, window, closeFlag) { //функция сохранения редактировавшейся записи
        store.sync({
            success: function (batch, options) {
                // Проверяем ответ на наличие блока _ids_
                // и если он присутствует обновляем хранилище
                if (this.reader.rawData._ids_) {
                    var opokRule = Ext.StoreManager.lookup('OPOKRule');
                    if(opokRule) opokRule.load();
                }
                if (closeFlag) window.close();
            },
            failure: store.syncFailure
        });
    },*/
    config: {
        listen: {
            component: {
                /*'app-settings-opokrule-form': {
                 show: 'onShowForm',
                 close: 'onCloseForm',
                 /**
                 * Обрабатываем событие отрисовки
                 * @param window
                 *
                 render: function (window) {
                 var me = this;
                 window.getEl().on('keypress', function (e) {
                 if (e.getKey() == e.ESC) {
                 me.onClickCancelForm();
                 }
                 });
                 if (Ext.isIE8) {
                 window.setWidth(Math.round(document.body.clientWidth / 100 * 90));
                 }
                 }
                 },*/
                '#OP_CAT_CD': {
                    change: 'onChangeOP_CAT_CD'
                },
                '#createVersion': {
                    afterrender: function (button, eOpts) {
                        // Если создаём новую запись, вырубаем кнопку создания версии
                        if (!button.disabled) {
                            button.setDisabled(this.getViewModel().get('record').phantom);
                        }
                    },
                    click: 'onClickCreateVersion'
                },
                '#runTest': {
                    afterrender: function (button, eOpts) {
                        // Если создаём новую запись, вырубаем кнопку тестирования
                        if (!button.disabled) {
                            button.setDisabled(this.getViewModel().get('record').phantom);
                        }
                    },
                    click: 'onClickRunTest'
                },
                '#saveFormRule': {
                    click: 'onClickSaveFormRule'
                },
                '#cancelFormRule': {
                    click: 'onClickCancelFormRule'
                }
            }
        }
    },
    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveFormRule: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView();
        if (!window || !this.isChanges(window)) return;

        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false, this.callbackRuleFormRule);
    },
    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelFormRule: function (button, eOpts) {
        var window = this.getView(),
            controller = this,
            fnCallbackRuleFormRule = this.callbackRuleFormRule,
            fnSaveForm = this.saveForm;

        var accessObj = "OPOKRule";
        var storeAccess = Ext.getStore('ObjectsAccess');
        if (!storeAccess) return;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            var i = storeAccess.findExact('id', accessObj);
            // если доступ на тестирование
            if ((storeAccess.getAt(i).get('mode') == 'T') && (window.getViewModel().get('VERS_STATUS_CD') != 'TEST')) {
                Ext.Msg.alert('Внимание!','Правило было изменено, но у Вас недостаточно прав для сохранения изменений!');
                window.close();
                return false;
            }
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    fnSaveForm(window, controller, true, fnCallbackRuleFormRule);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    },

    callbackRuleFormRule: function (records, operation, success) {
        var self = this,
            id = records.get('RULE_SEQ_ID'),
            vm = self.getViewModel(),
            panel = self.lookupReferenceHolder(true).lookupReference('appSettingsOpokrule'),
            vmMain = (panel) ? panel.getViewModel() : null;
            /*view = vm.getView(),
            controler = self.controller,
            controlerPanel = panel.controller;
            form = self.down('form').getForm(),*/
        if (vmMain) {
            var store = vmMain.getStore('oporule');
            store.reload({
                callback: function () {
                    if (vm) {
                        var records = store.getById(id);
                        vm.set('record', records);
                    } else {
                        // закрытие формы - передача фокуса записи в гриде
                        var index = store.find('RULE_SEQ_ID', id);
                        panel.getView().focusRow(index);
                        panel.getView().setSelection(index);
                    }
                }
            });


        }
    },

    /*onShowForm: function (window, eOpts) {
     },
     onCloseForm: function (window, eOpts) {
     var opokRule = Ext.StoreManager.lookup('OPOKRule');
     if (opokRule.isChanges()) {
     opokRule.rejectChanges();
     }
     },*/
    onChangeOP_CAT_CD: function (combo, newValue, oldValue, eOpts) {
        var win = this.getView(),
            form = win.down('form').getForm(),
            wt = (newValue == 'WT'),
            vocLexemeSearchArea = Ext.data.StoreManager.lookup('OPOKRuleLexemeSearchArea');
        win.down('#fieldSetRecipient').setDisabled(!wt);
        form.findField('DBT_CDT_CD').setDisabled(wt);
        form.findField('ACCT_REVERSE_FL').setDisabled(!wt);
        form.findField('LEXEME_ADD_CUST2_TX').setDisabled(!wt);
        form.findField('LEXEME2_ADD_CUST2_TX').setDisabled(!wt);
        var LEXEME_AREA_TX = form.findField('LEXEME_AREA_TX');
        if (wt) {
            vocLexemeSearchArea.removeFilter('id');
        } else {
            LEXEME_AREA_TX.setValue(
                Ext.Array.filter(LEXEME_AREA_TX.getValue(), function(i) {
                    return i != 'У2'
                })
            );
            vocLexemeSearchArea.addFilter({
                property: 'id',
                operator: '!=',
                value: 'У2'
            });
        }
    },
    onClickRunTest: function (button, eOpts) {
        var tester = Ext.widget('app-settings-opoktest'),
            ruleCombo = tester.down('rulefield combobox'),
            OPOKRuleField = Ext.data.StoreManager.lookup('OPOKRuleField');
        OPOKRuleField.removeFilter('VERS_STATUS_CD');
        OPOKRuleField.addFilter({
            property: '_id_',
            value: this.getViewModel().get('record').id
        });
        ruleCombo.setValue(this.getViewModel().get('record').id);
        ruleCombo.setDisabled(true);
        tester.down('rulefield button').hide();
        tester.down('tabpanel').getTabBar().items.get(1).setDisabled(true);
        tester.show();
    },

    onClickCreateVersion: function (button, eOpts) {
        var self = this;
            win = this.getView(),
            recordId = win.getViewModel().get('record').id,
            vm = self.getViewModel().getParent();
            grid = vm.getView();
        AML.app.msg.confirm({
            message: 'Вы действительно хотите создать новую версию данного правила?',
            fnYes: function () {
                Ext.Ajax.request({
                    url: common.globalinit.ajaxUrl,
                    method: common.globalinit.ajaxMethod,
                    headers: common.globalinit.ajaxHeaders,
                    timeout: common.globalinit.ajaxTimeOut,
                    params: Ext.JSON.encode({
                        form: 'OPOKRule',
                        action: 'create_version',
                        "_id_": recordId
                    }),
                    success: function (response) {
                        var getDecodeData = Ext.JSON.decode(response.responseText),
                            getData = (getDecodeData) ? getDecodeData.data : false,
                            store = Ext.data.StoreManager.lookup('OPOKRule'),
                            record = (store) ? store.add(getData)[0] : false,
                            form = Ext.widget('app-settings-opokrule-form'),
                            formViewModel = form.getViewModel();

                        record.data = Ext.JSON.decode(response.responseText).data[0];
                        formViewModel.setData(Ext.apply(formViewModel.getData(), {record: record}));
                        grid.getStore().reload({
                            callback: function(){
                                grid.getView().refresh();
                            }
                        });
                        win.close();
                        setTimeout(function () {
                            form.show();
                        }, 500);
                    }
                });
            }
        });
    },

    openOPOKType: function (btn) {
        var sourceField = btn.up().down('combobox'),
            sourceFieldValue = sourceField ? sourceField.getValue() : '',
            opoktype = (sourceField) ? sourceField.getStore() : false,
            record;

        if (!sourceFieldValue) {
            Ext.Msg.alert('', 'Не указано значение в поле "Перечень" !');
            return;
        }

        if (opoktype) {
            record = opoktype.findRecord('WATCH_LIST_CD', sourceFieldValue, 0, false, false, true);
            if (record) {
                Ext.create({
                    xtype: 'app-oes-oes321listsorg-dialog-list',
                    viewModel: {
                        xtype: 'app-oes-oes321listsorg-dialog-list',
                        data: {
                            isOpenReadOnly: true,
                            idOrg: sourceFieldValue
                        }
                    }
                }).show();
            }
        }
    },

    parseISO8601: function (dateStringInRange) {
        var isoExp = /^\s*(\d{4})-(\d\d)-(\d\d)\s*$/,
            date = new Date(NaN), month,
            parts = isoExp.exec(dateStringInRange);

        if (parts) {
            month = +parts[2];
            date.setFullYear(parts[1], month - 1, parts[3]);
            if (month != date.getMonth() + 1) {
                date.setTime(NaN);
            }
        }
        return date;
    }
});