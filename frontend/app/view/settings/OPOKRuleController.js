Ext.define('AML.view.settings.OPOKRuleController', {
    requires: [
        'AML.model.OPOKRule',
        'Ext.data.StoreManager'
    ],
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.opokrule',
    childForm: 'app-settings-opokrule-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'RULE_CD', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'OPOKRule', //код объекта доступа для данной формы
    /**
     * Фильтры
     */
    onSelectFilterStatus: function (combo, record, eOpts) {
        var store = this.getStore('oporule');
        if (record.id) {
            store.addFilter({
                property: 'VERS_STATUS_CD',
                value: record.id
            });
        } else {
            store.addFilter({
                property: 'VERS_STATUS_CD',
                operator: '!=',
                value: 'ARC'
            });
        }
    },
    onClickFilterTriggerStatus: function (field, trigger, eOpts) {
        field.clearValue();
        this.getStore('oporule').addFilter({
            property: 'VERS_STATUS_CD',
            operator: '!=',
            value: 'ARC'
        });
    }//,
    /**
     * Обновляем хранилище
     */
    /*onStoreRefresh: function () {
        this.getStore('oporule').load();
     }*/
});
