Ext.define('AML.view.settings.OPOKTestViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.opoktestviewmodel',
	data: {
		title: 'Тестирование правил выявления',
		footerRecord: null,
		count: {
			load: 0,
			total: 0
		}
	},
	formulas: {
		coloredTRXN_DESC: function (get) {
			var text = get('footerRecord.TRXN_DESC'),
				lexeme = get('footerRecord.TRXN_DESC_LEX');
			return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
		},
		coloredORIG_NM: function (get) {
			var text = get('footerRecord.ORIG_NM'),
				lexeme = get('footerRecord.ORIG_LEX');
			return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
		},
		coloredBENEF_NM: function (get) {
			var text = get('footerRecord.BENEF_NM'),
				lexeme = get('footerRecord.BENEF_LEX');
			return (text && lexeme ? AML.app.lexeme.colored(text, lexeme) : text);
		}
	}
});
