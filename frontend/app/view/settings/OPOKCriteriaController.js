Ext.define('AML.view.settings.OPOKCriteriaController', {
	requires: [
		'AML.model.OPOKCriteria',
		'Ext.data.StoreManager'
	],
	extend: 'AML.view.main.BaseListController',
    alias: 'controller.opokcriteria',
	childForm: 'app-settings-opokcriteria-form', /*xtype формы редактирования, открываемой из списка*/
	IdColumn: 'CRIT_CD', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
	accessObject: 'OPOKCriteria', //код объекта доступа для данной формы
	/*config: {
		listen: {
			component: {
				'gridpanel': {
					rowkeydown: function (table, record, tr, rowIndex, e, eOpts) {
						switch (e.getKey()) {
							case e.ENTER:
							case e.SPACE:
								this.onClickOpenForm(record);
								break;
							case e.BACKSPACE:
							case e.DELETE:
								this.onClickRemove();
								break;
						}
					}
				},
				'app-settings-opokcriteria': {
					selectionchange: 'onSelectionChange',
					rowdblclick: 'onDBClickOpenForm'
				},
				'#create': {
					click: 'onClickCreate'
				},
				'#remove': {
					click: 'onClickRemove'
				}
			}
		}
	},
	onSelectionChange: function (model, selected, eOpts) {
		var grid = this.getView();
		grid.down('#remove').setDisabled(!selected.length);
	},
	onDBClickOpenForm: function (grid, record, index, eOpts) {
		var form = Ext.widget('app-settings-opokcriteria-form'),
			formViewModel = form.getViewModel();
		formViewModel.setData(Ext.apply(formViewModel.getData(), {record: record}));
		form.show();
	},
	onClickCreate: function (button, event, eOpts) {
		var grid = this.getView(),
			record = Ext.create('AML.model.OPOKCriteria', {});
		grid.store.insert(0, record);
		grid.getSelectionModel().select(0);
		grid.getView().focusRow(0);
		grid.fireEvent('rowdblclick', grid, record);
	},*/
	/**
	 * Открытие формы
	 */
	/*onClickOpenForm: function (record) {
		var grid = this.getView();
		grid.fireEvent('rowdblclick', grid, record);
	},*/
	/**
	 * Удаление
	 */
	/*onClickRemove: function (button, event, eOpts) {
		var grid = this.getView(),
			sm = grid.getSelectionModel(),
			codes = sm.getSelection().map(function (v) {
				return v.data.CRIT_CD;
			}).join(', ');
		AML.app.msg.confirm({
			message: ('Вы уверены что хотите удалить ' + (sm.getCount() > 1 ? 'условия: ' + codes : 'условие: ' + codes) + '?'),
			fnYes: function () {
				grid.store.remove(sm.getSelection());
				var opokCriteria = Ext.StoreManager.lookup('OPOKCriteria');
				opokCriteria.sync({
					failure: opokCriteria.syncFailure
				});
			}
		});
	},*/
	/**
	 * Фильтры
	 */
	onSelectFilterCRIT_TP_CD: function (combo, record, eOpts) {
		var store = this.getStore('opokcriteria');
		if (record.id > 0) {
			store.addFilter({
				property: 'CRIT_TP_CD',
				value: record.id
			});
		} else {
			store.removeFilter('CRIT_TP_CD');
		}
	},
	onChangeFilterCRIT_NM: function (field, event, eOpts) {
		var store = this.getStore('opokcriteria');
		if (field.value) {
			store.addFilter({
				property: 'CRIT_NM',
				value: new RegExp(field.value, "i")
			});
		} else {
			store.removeFilter('CRIT_NM');
		}
	},
	onSelectFilterCRIT_USE_CD: function (combo, record, eOpts) {
		var store = this.getStore('opokcriteria');
		if (record.id) {
			store.addFilter({
				property: 'CRIT_USE_CD',
				value: record.id
			});
		} else {
			store.removeFilter('CRIT_USE_CD');
		}
	},
	onChangeFilterACTIVE_FL: function (field, newValue, oldValue, eOpts) {
		var store = this.getStore('opokcriteria');
		if (field.value) {
			store.addFilter({
				property: 'ACTIVE_FL',
				value: true
			});
		} else {
			store.removeFilter('ACTIVE_FL');
		}
	},
	onClickFilterTriggerCRIT_TP_CD: function (field, trigger, eOpts) {
		field.clearValue();
		this.getStore('opokcriteria').removeFilter('CRIT_TP_CD');
	},
	onClickFilterTriggerCRIT_USE_CD: function (field, trigger, eOpts) {
		field.clearValue();
		this.getStore('opokcriteria').removeFilter('CRIT_USE_CD');
	}//,
	/**
	 * Обновляем хранилище
	 */
	/*onStoreRefresh: function () {
		Ext.data.StoreManager.lookup('OPOKCriteria').load();
	}*/
});
