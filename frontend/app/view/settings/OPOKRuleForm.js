Ext.define('AML.view.settings.OPOKRuleForm', {
    extend: 'Ext.window.Window',
    requires: [
        'AML.view.settings.OPOKRuleFormController',
        'AML.view.settings.OPOKRuleFormViewModel',
        'Ext.form.Panel',
        'Ext.form.FieldContainer',
        'Ext.layout.container.HBox',
        'Ext.form.field.ComboBox',
        'Ext.layout.container.VBox',
        'Ext.form.field.TextArea',
        'Ext.form.field.Checkbox',
        'Ext.form.FieldSet',
        'Ext.layout.container.Anchor',
        'Ext.form.field.Number',
        'Ext.layout.container.Column',
        'Ext.tab.Panel',
        'Ext.form.field.Tag',
        'Ext.form.field.Text',
        'Ext.form.field.Date',
        'AML.ux.form.field.Criteria',
        'Ext.button.Button',
        'Ext.container.Container'
    ],
    xtype: 'app-settings-opokrule-form',
    controller: 'opokruleform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'opokruleformviewmodel'
    },
    bind: {
        title: '{title}'
    },
    width: (Ext.isIE8 ? 1280 : '90%'),
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelFormRule'
        }
    ],
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'fieldcontainer',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Основные данные',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            collapsible: false,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                align: 'middle'
                                            },
                                            defaults: {
                                                labelAlign: 'right',
                                                labelWidth: 80
                                            },
                                            items: [
                                                {
                                                    xtype: 'label',
                                                    text: 'Категория: ',
                                                    flex: 0.3
                                                },
                                                {
                                                    itemId: 'OP_CAT_CD',
                                                    name: 'OP_CAT_CD',
                                                    xtype: 'combobox',
                                                    store: 'OPOKRuleOperationCategory',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayField: 'label',
                                                    valueField: 'id',
                                                    bind: '{record.OP_CAT_CD}',
                                                    fieldLabel: '',
                                                    allowBlank: false,
                                                    flex: 0.7
                                                },
                                                {
                                                    name: 'DBT_CDT_CD',
                                                    xtype: 'combobox',
                                                    store: 'OPOKRuleInOut',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayField: 'label',
                                                    valueField: 'id',
                                                    bind: '{record.DBT_CDT_CD}',
                                                    fieldLabel: ' ',
                                                    labelSeparator: null,
                                                    labelWidth: 10,
                                                    triggerClear: true,
                                                    flex: 0.5
                                                },
                                                {
                                                    name: 'QUERY_CD',
                                                    xtype: 'combobox',
                                                    store: 'OPOKRuleQuery',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayField: 'label',
                                                    valueField: 'id',
                                                    bind: '{record.QUERY_CD}',
                                                    fieldLabel: 'Запрос',
                                                    labelWidth: 80,
                                                    allowBlank: false,
                                                    flex: 0.75
                                                },
                                                {
                                                    name: 'VERS_STATUS_CD',
                                                    reference: 'statusComboBoxItem',
                                                    xtype: 'combobox',
                                                    store: 'OPOKRuleStatus',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayField: 'label',
                                                    valueField: 'id',
                                                    bind: {
                                                        value: '{record.VERS_STATUS_CD}',
                                                        originalValue: '{record.VERS_STATUS_CD}'
                                                    },
                                                    fieldLabel: 'Статус',
                                                    labelWidth: 60,
                                                    allowBlank: false,
                                                    flex: 0.75
                                                }
                                            ],
                                            flex: 1
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                align: 'middle'
                                            },
                                            defaults: {
                                                labelAlign: 'right',
                                                labelWidth: 80
                                            },
                                            items: [
                                                {
                                                    xtype: 'label',
                                                    text: 'Код ОПОК: ',
                                                    flex: 0.3
                                                },
                                                {
                                                    name: 'OPOK_NB',
                                                    xtype: 'combobox',
                                                    store: 'OPOK',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; ' + ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10) <= 8) ? 'width: 800px;' : 'max-width: 800px;') + ' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
                                                        '</tpl>'
                                                    ),
                                                    matchFieldWidth: false,
                                                    displayField: 'OPOK_NB',
                                                    valueField: 'OPOK_NB',
                                                    bind: '{record.OPOK_NB}',
                                                    // fieldLabel: 'Код ОПОК',
                                                    fieldLabel: '',
                                                    allowBlank: false,
                                                    flex: 0.7
                                                },
                                                {
                                                    xtype: 'label',
                                                    text: 'Приоритет:',
                                                    style: 'text-align: right',
                                                    flex: 0.3
                                                },
                                                {
                                                    name: 'PRIORITY_NB',
                                                    xtype: 'numberfield',
                                                    bind: {
                                                        value: '{record.PRIORITY_NB}'
                                                    },
                                                    fieldLabel: ' ',
                                                    labelSeparator: null,
                                                    labelWidth: 1,
                                                    allowBlank: false,
                                                    flex: 0.2
                                                },
                                                {
                                                    name: 'TB_ID',
                                                    xtype: 'combobox',
                                                    store: 'TB',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {label}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayField: 'label',
                                                    valueField: 'id',
                                                    bind: '{record.TB_ID}',
                                                    fieldLabel: 'Тербанк',
                                                    allowBlank: false,
                                                    flex: 1.5
                                                }
                                            ],
                                            flex: 1
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                align: 'middle'
                                            },
                                            defaults: {
                                                labelAlign: 'right',
                                                labelWidth: 80
                                            },
                                            items: [
                                                {
                                                    xtype: 'label',
                                                    text: 'Межфилиальные операции подлежат контролю: ',
                                                    flex: 1.3
                                                },
                                                {
                                                    name: 'KGRKO_PARTY_CD',
                                                    xtype: 'combobox',
                                                    store: 'OPOKRuleKGRKO',
                                                    editable: false,
                                                    queryMode: 'local',
                                                    tpl: Ext.create('Ext.XTemplate',
                                                        '<tpl for=".">',
                                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{all}</div>',
                                                        '</tpl>'
                                                    ),
                                                    displayField: 'all',
                                                    valueField: 'id',
                                                    bind: '{record.KGRKO_PARTY_CD}',
                                                    fieldLabel: ' ',
                                                    labelSeparator: null,
                                                    labelWidth: 1,
                                                    allowBlank: false,
                                                    flex: 1.7,
                                                    listeners: {
                                                        afterrender: function (combo) {
                                                            var mainForm = combo.up('app-settings-opokrule-form'),
                                                                vmMF = (mainForm) ? mainForm.getViewModel() : false,
                                                                recData = (vmMF) ? vmMF.get('record') : false,
                                                                valueCombo = (recData) ? recData.get('KGRKO_PARTY_CD') : false;
                                                            if (!valueCombo) {
                                                                combo.setValue(0);
                                                            }
                                                        }
                                                    }
                                                }
                                            ],
                                            flex: 1
                                        }
                                    ],
                                    flex: 2
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    // layout: {
                                    // 	type: 'anchor'
                                    // },
                                    layout: 'fit',
                                    defaults: {
                                        labelAlign: 'right'
                                    },
                                    items: [
                                        {
                                            name: 'NOTE_TX',
                                            xtype: 'textarea',
                                            bind: {
                                                value: '{record.NOTE_TX}'
                                            },
                                            fieldLabel: 'Примечание',
                                            anchor: '100%',
                                            margin: '0 0 5 0'
                                        }
                                    ],
                                    flex: 1
                                }
                            ]
                        },
                        {
                            xtype: 'tabpanel',
                            plain: true,
                            bodyPadding: 10,
                            activeTab: 0,
                            minTabWidth: 150,
                            items: [
                                {
                                    title: 'Счета',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'column'
                                            },
                                            items: [
                                                {
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 3 0 0',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    defaults: {
                                                        labelAlign: 'right',
                                                        labelWidth: 150
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Плательщик/Клиент',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: 'Счёт',
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT_CRIT_CD',
                                                                                    xtype: 'criteriafield',
                                                                                    store: 'OPOKCriteriaT1',
                                                                                    triggerClear: true,
                                                                                    flex: 1
                                                                                },
                                                                                {
                                                                                    name: 'ACCT_NULL_FL',
                                                                                    xtype: 'checkbox',
                                                                                    bind: {
                                                                                        value: '{record.ACCT_NULL_FL}'
                                                                                    },
                                                                                    fieldLabel: 'Неизвестный счет',
                                                                                    labelWidth: 120,
                                                                                    listeners: {
                                                                                        afterrender: function (field) {
                                                                                            var doc = field.up('app-settings-opokrule-form'),
                                                                                                vmDoc = (doc) ? doc.getViewModel() : false,
                                                                                                docData = (vmDoc) ? vmDoc.getData() : false,
                                                                                                rec = (docData) ? docData.record : false,
                                                                                                recData = (rec) ? rec.getData() : false,
                                                                                                fValue = (recData) ? recData['ACCT_NULL_FL'] : false;
                                                                                            if (fValue === 'Y'
                                                                                                || fValue === 'true' || fValue === true) {
                                                                                                field.setValue(true);
                                                                                            } else if (fValue === 'N'
                                                                                                || fValue === 'false' || fValue === false) {
                                                                                                field.setValue(true);
                                                                                                field.setValue(false);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT_ADD_CRIT_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.ACCT_ADD_CRIT_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Доп. условия',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT_SUBTLE_CRIT_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.ACCT_SUBTLE_CRIT_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Уточняющие условия',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        }
                                                                    ],
                                                                    flex: 1
                                                                },
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: 'Область поиска',
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT_AREA_TX',
                                                                                    xtype: 'tagfield',
                                                                                    store: 'OPOKRuleAccountSearchArea',
                                                                                    queryMode: 'local',
                                                                                    valueField: 'id',
                                                                                    displayField: 'label',
                                                                                    bind: '{record.ACCT_AREA_TX}',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        }
                                                                    ],
                                                                    flex: 1
                                                                },
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: '',
                                                                    layout: {
                                                                        type: 'hbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'combobox',
                                                                            editable: false,
                                                                            queryMode: 'local',
                                                                            tpl: Ext.create('Ext.XTemplate',
                                                                                '<tpl for=".">',
                                                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{WATCH_LIST_CD} - {WATCH_LIST_NM}</div>',
                                                                                '</tpl>'
                                                                            ),
                                                                            fieldLabel: 'Перечень',
                                                                            labelWidth: 55,
                                                                            // displayField: 'WATCH_LIST_NM',
                                                                            displayTpl: Ext.create('Ext.XTemplate',
                                                                                '<tpl for=".">',
                                                                                '{WATCH_LIST_CD} - {WATCH_LIST_NM}',
                                                                                '</tpl>'
                                                                            ),
                                                                            valueField: 'WATCH_LIST_CD',
                                                                            triggerClear: true,
                                                                            multiSelect: false,
                                                                            bind: {
                                                                                value: '{record.CUST_WATCH_LIST_CD}',
                                                                                store: '{oes321types}'
                                                                            },
                                                                            forceSelection: true,
                                                                            matchFieldWidth: false,
                                                                            padding: '5 5 5 5',
                                                                            flex: 1.5
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            border: 0,
                                                                            // width: 20,
                                                                            align: 'right',
                                                                            margin: '0 5 5 0',
                                                                            iconCls: 'icon-report',
                                                                            style: 'background:none;',
                                                                            handler: 'openOPOKType'
                                                                        },
                                                                        {
                                                                            name: 'CUST_NOT_WATCH_LIST_FL',
                                                                            xtype: 'checkbox',
                                                                            fieldLabel: 'Исключить',
                                                                            labelWidth: 80,
                                                                            bind: {
                                                                                value: '{record.CUST_NOT_WATCH_LIST_FL}'
                                                                            },
                                                                            padding: '5 5 5 5',
                                                                            flex: 0.5,
                                                                            listeners: {
                                                                                afterrender: function (field) {
                                                                                    var doc = field.up('app-settings-opokrule-form'),
                                                                                        vmDoc = (doc) ? doc.getViewModel() : false,
                                                                                        docData = (vmDoc) ? vmDoc.getData() : false,
                                                                                        rec = (docData) ? docData.record : false,
                                                                                        recData = (rec) ? rec.getData() : false,
                                                                                        fValue = (recData) ? recData['CUST_NOT_WATCH_LIST_FL'] : false;
                                                                                    if (fValue === 'Y'
                                                                                        || fValue === 'true' || fValue === true) {
                                                                                        field.setValue(true);
                                                                                    } else if (fValue === 'N'
                                                                                        || fValue === 'false' || fValue === false) {
                                                                                        field.setValue(true);
                                                                                        field.setValue(false);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                },
                                                {
                                                    itemId: 'fieldSetRecipient',
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 0 0 3',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    defaults: {
                                                        labelAlign: 'right',
                                                        labelWidth: 150
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Получатель',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: 'Счёт',
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT2_CRIT_CD',
                                                                                    xtype: 'criteriafield',
                                                                                    store: 'OPOKCriteriaT1',
                                                                                    triggerClear: true,
                                                                                    flex: 1
                                                                                },
                                                                                {
                                                                                    name: 'ACCT2_NULL_FL',
                                                                                    xtype: 'checkbox',
                                                                                    bind: {
                                                                                        value: '{record.ACCT2_NULL_FL}'
                                                                                    },
                                                                                    fieldLabel: 'Неизвестный счет',
                                                                                    labelWidth: 120,
                                                                                    listeners: {
                                                                                        afterrender: function (field) {
                                                                                            var doc = field.up('app-settings-opokrule-form'),
                                                                                                vmDoc = (doc) ? doc.getViewModel() : false,
                                                                                                docData = (vmDoc) ? vmDoc.getData() : false,
                                                                                                rec = (docData) ? docData.record : false,
                                                                                                recData = (rec) ? rec.getData() : false,
                                                                                                fValue = (recData) ? recData['ACCT2_NULL_FL'] : false;
                                                                                            if (fValue === 'Y'
                                                                                                || fValue === 'true' || fValue === true) {
                                                                                                field.setValue(true);
                                                                                            } else if (fValue === 'N'
                                                                                                || fValue === 'false' || fValue === false) {
                                                                                                field.setValue(true);
                                                                                                field.setValue(false);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT2_ADD_CRIT_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.ACCT2_ADD_CRIT_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Доп. условия',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT2_SUBTLE_CRIT_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.ACCT2_SUBTLE_CRIT_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Уточняющие условия',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        }
                                                                    ],
                                                                    flex: 1
                                                                },
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: 'Область поиска',
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'ACCT2_AREA_TX',
                                                                                    xtype: 'tagfield',
                                                                                    store: 'OPOKRuleAccountSearchArea',
                                                                                    queryMode: 'local',
                                                                                    valueField: 'id',
                                                                                    displayField: 'label',
                                                                                    bind: '{record.ACCT2_AREA_TX}',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        }
                                                                    ],
                                                                    flex: 1
                                                                },
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: '',
                                                                    layout: {
                                                                        type: 'hbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'combobox',
                                                                            editable: false,
                                                                            queryMode: 'local',
                                                                            tpl: Ext.create('Ext.XTemplate',
                                                                                '<tpl for=".">',
                                                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{WATCH_LIST_CD} - {WATCH_LIST_NM}</div>',
                                                                                '</tpl>'
                                                                            ),
                                                                            fieldLabel: 'Перечень',
                                                                            labelWidth: 55,
                                                                            // displayField: 'WATCH_LIST_NM',
                                                                            displayTpl: Ext.create('Ext.XTemplate',
                                                                                '<tpl for=".">',
                                                                                '{WATCH_LIST_CD} - {WATCH_LIST_NM}',
                                                                                '</tpl>'
                                                                            ),
                                                                            valueField: 'WATCH_LIST_CD',
                                                                            triggerClear: true,
                                                                            multiSelect: false,
                                                                            bind: {
                                                                                value: '{record.CUST2_WATCH_LIST_CD}',
                                                                                store: '{oes321types}'
                                                                            },
                                                                            forceSelection: true,
                                                                            matchFieldWidth: false,
                                                                            padding: '5 5 5 5',
                                                                            flex: 1.5
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            border: 0,
                                                                            // width: 20,
                                                                            align: 'right',
                                                                            margin: '0 5 5 0',
                                                                            iconCls: 'icon-report',
                                                                            style: 'background:none;',
                                                                            handler: 'openOPOKType'
                                                                        },
                                                                        {
                                                                            name: 'CUST2_NOT_WATCH_LIST_FL',
                                                                            xtype: 'checkbox',
                                                                            fieldLabel: 'Исключить',
                                                                            labelWidth: 80,
                                                                            bind: {
                                                                                value: '{record.CUST2_NOT_WATCH_LIST_FL}'
                                                                            },
                                                                            padding: '5 5 5 5',
                                                                            flex: 0.5,
                                                                            listeners: {
                                                                                afterrender: function (field) {
                                                                                    var doc = field.up('app-settings-opokrule-form'),
                                                                                        vmDoc = (doc) ? doc.getViewModel() : false,
                                                                                        docData = (vmDoc) ? vmDoc.getData() : false,
                                                                                        rec = (docData) ? docData.record : false,
                                                                                        recData = (rec) ? rec.getData() : false,
                                                                                        fValue = (recData) ? recData['CUST2_NOT_WATCH_LIST_FL'] : false;
                                                                                    if (fValue === 'Y'
                                                                                        || fValue === 'true' || fValue === true) {
                                                                                        field.setValue(true);
                                                                                    } else if (fValue === 'N'
                                                                                        || fValue === 'false' || fValue === false) {
                                                                                        field.setValue(true);
                                                                                        field.setValue(false);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                pack: 'center'
                                            },
                                            items: [
                                                {
                                                    name: 'ACCT_REVERSE_FL',
                                                    xtype: 'checkbox',
                                                    bind: {
                                                        value: '{record.ACCT_REVERSE_FL}'
                                                    },
                                                    fieldLabel: 'Обратные условия на счета',
                                                    labelWidth: 170,
                                                    labelAlign: 'right',
                                                    allowBlank: false,
                                                    listeners: {
                                                        afterrender: function (field) {
                                                            var doc = field.up('app-settings-opokrule-form'),
                                                                vmDoc = (doc) ? doc.getViewModel() : false,
                                                                docData = (vmDoc) ? vmDoc.getData() : false,
                                                                rec = (docData) ? docData.record : false,
                                                                recData = (rec) ? rec.getData() : false,
                                                                fValue = (recData) ? recData['ACCT_REVERSE_FL'] : false;
                                                            if (fValue === 'Y'
                                                                || fValue === 'true' || fValue === true) {
                                                                field.setValue(true);
                                                            } else if (fValue === 'N'
                                                                || fValue === 'false' || fValue === false) {
                                                                field.setValue(true);
                                                                field.setValue(false);
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    title: 'Лексемы',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'column'
                                            },
                                            items: [
                                                {
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 3 0 0',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    defaults: {
                                                        labelAlign: 'right',
                                                        labelWidth: 150
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Первый блок',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'LEXEME_CRIT_CD',
                                                                            xtype: 'criteriafield',
                                                                            store: 'OPOKCriteriaT2',
                                                                            triggerClear: true,
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'LEXEME_AREA_TX',
                                                                            xtype: 'tagfield',
                                                                            store: 'OPOKRuleLexemeSearchArea',
                                                                            queryMode: 'local',
                                                                            valueField: 'id',
                                                                            displayField: 'label',
                                                                            bind: '{record.LEXEME_AREA_TX}',
                                                                            fieldLabel: 'Область поиска',
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: 'Дополнительные лексемы',
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'LEXEME_ADD_PURPOSE_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.LEXEME_ADD_PURPOSE_TX}'
                                                                                    },
                                                                                    fieldLabel: 'В назначении платежа',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'LEXEME_ADD_CUST_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.LEXEME_ADD_CUST_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Участник 1',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'LEXEME_ADD_CUST2_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.LEXEME_ADD_CUST2_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Участник 2',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                },
                                                {
                                                    itemId: 'fieldSetRecipient',
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 0 0 3',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    defaults: {
                                                        labelAlign: 'right',
                                                        labelWidth: 150
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Второй блок',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'LEXEME2_CRIT_CD',
                                                                            xtype: 'criteriafield',
                                                                            store: 'OPOKCriteriaT2',
                                                                            triggerClear: true,
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'LEXEME2_AREA_TX',
                                                                            xtype: 'tagfield',
                                                                            store: 'OPOKRuleLexemeSearchArea',
                                                                            queryMode: 'local',
                                                                            valueField: 'id',
                                                                            displayField: 'label',
                                                                            bind: '{record.LEXEME2_AREA_TX}',
                                                                            fieldLabel: 'Область поиска',
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldset',
                                                                    title: 'Дополнительные лексемы',
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                    collapsible: false,
                                                                    defaults: {
                                                                        anchor: '100%',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'LEXEME2_ADD_PURPOSE_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.LEXEME2_ADD_PURPOSE_TX}'
                                                                                    },
                                                                                    fieldLabel: 'В назначении платежа',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'LEXEME2_ADD_CUST_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.LEXEME2_ADD_CUST_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Участник 1',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            xtype: 'fieldcontainer',
                                                                            layout: {
                                                                                type: 'hbox'
                                                                            },
                                                                            defaults: {
                                                                                labelAlign: 'right',
                                                                                labelWidth: 150
                                                                            },
                                                                            items: [
                                                                                {
                                                                                    name: 'LEXEME2_ADD_CUST2_TX',
                                                                                    xtype: 'textarea',
                                                                                    bind: {
                                                                                        value: '{record.LEXEME2_ADD_CUST2_TX}'
                                                                                    },
                                                                                    fieldLabel: 'Участник 2',
                                                                                    flex: 1
                                                                                }
                                                                            ]
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    title: 'Другое',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'column'
                                            },
                                            items: [
                                                {
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 3 0 0',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Вид операции ЦАС НСИ',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'OPTP_CRIT_CD',
                                                                            xtype: 'criteriafield',
                                                                            store: 'OPOKCriteriaT3',
                                                                            triggerClear: true,
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'OPTP_ADD_CRIT_TX',
                                                                            xtype: 'textarea',
                                                                            bind: {
                                                                                value: '{record.OPTP_ADD_CRIT_TX}'
                                                                            },
                                                                            fieldLabel: 'Дополнительные условия',
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                },
                                                {
                                                    itemId: 'fieldSetRecipient',
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 0 0 3',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    defaults: {
                                                        labelAlign: 'right',
                                                        labelWidth: 150
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Прочие условия',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'SRC_CRIT_TX',
                                                                            xtype: 'textarea',
                                                                            bind: {
                                                                                value: '{record.SRC_CRIT_TX}'
                                                                            },
                                                                            fieldLabel: 'Системы-источники',
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'START_DT',
                                                                            xtype: 'datefield',
                                                                            bind: {
                                                                                value: '{record.START_DT}'
                                                                            },
                                                                            format: 'd.m.Y',
                                                                            startDay: 1,
                                                                            fieldLabel: 'Дата операции с',
                                                                            width: 250,
                                                                            listeners: {
                                                                                render: function (field) {
                                                                                    var doc = field.up('app-settings-opokrule-form'),
                                                                                        vmDoc = (doc) ? doc.getViewModel() : false,
                                                                                        docData = (vmDoc) ? vmDoc.getData() : false,
                                                                                        rec = (docData) ? docData.record : false,
                                                                                        recData = (rec) ? rec.getData() : false,
                                                                                        fValue = (recData) ? recData['START_DT'] : false;
                                                                                    if (typeof fValue === 'string') {
                                                                                        var a = fValue.split(' '),
                                                                                            d = a[0].split('-'),
                                                                                            fDateToRec = new Date(d[0], (d[1] - 1), d[2]);
                                                                                        rec.set('START_DT', fDateToRec);

                                                                                    }
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            name: 'END_DT',
                                                                            xtype: 'datefield',
                                                                            bind: {
                                                                                value: '{record.END_DT}'
                                                                            },
                                                                            format: 'd.m.Y',
                                                                            startDay: 1,
                                                                            fieldLabel: 'по',
                                                                            labelWidth: 25,
                                                                            width: 125,
                                                                            listeners: {
                                                                                render: function (field) {
                                                                                    var doc = field.up('app-settings-opokrule-form'),
                                                                                        vmDoc = (doc) ? doc.getViewModel() : false,
                                                                                        docData = (vmDoc) ? vmDoc.getData() : false,
                                                                                        rec = (docData) ? docData.record : false,
                                                                                        recData = (rec) ? rec.getData() : false,
                                                                                        fValue = (recData) ? recData['END_DT'] : false;
                                                                                    if (typeof fValue === 'string') {
                                                                                        var a = fValue.split(' '),
                                                                                            d = a[0].split('-'),
                                                                                            fDateToRec = new Date(d[0], (d[1] - 1), d[2]);
                                                                                        rec.set('END_DT', fDateToRec);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'column'
                                            },
                                            items: [
                                                {
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 3 0 0',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Дополнительное SQL условие',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'CUSTOM_CRIT_CD',
                                                                            xtype: 'criteriafield',
                                                                            store: 'OPOKCriteriaT4',
                                                                            triggerClear: true,
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'CUSTOM_CRIT_SQL_TX',
                                                                            xtype: 'textarea',
                                                                            bind: {
                                                                                value: '{record.CUSTOM_CRIT_SQL_TX}'
                                                                            },
                                                                            fieldLabel: 'SQL',
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'CUSTOM_CRIT_PARAM_TX',
                                                                            xtype: 'textfield',
                                                                            bind: {
                                                                                value: '{record.CUSTOM_CRIT_PARAM_TX}'
                                                                            },
                                                                            fieldLabel: 'Параметры',
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                },
                                                {
                                                    itemId: 'fieldSetRecipient',
                                                    xtype: 'fieldcontainer',
                                                    columnWidth: 0.50,
                                                    margin: '0 0 0 3',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    defaults: {
                                                        labelAlign: 'right',
                                                        labelWidth: 150
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'fieldset',
                                                            title: 'Пояснение к выявлению',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            collapsible: false,
                                                            defaults: {
                                                                anchor: '100%',
                                                                labelWidth: 150
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'EXPLANATION_CRIT_CD',
                                                                            xtype: 'criteriafield',
                                                                            store: 'OPOKCriteriaT5',
                                                                            triggerClear: true,
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'fieldcontainer',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    defaults: {
                                                                        labelAlign: 'right',
                                                                        labelWidth: 150
                                                                    },
                                                                    items: [
                                                                        {
                                                                            name: 'EXPLANATION_SQL_TX',
                                                                            xtype: 'textarea',
                                                                            bind: {
                                                                                value: '{record.EXPLANATION_SQL_TX}'
                                                                            },
                                                                            fieldLabel: 'SQL',
                                                                            flex: 1
                                                                        }
                                                                    ]
                                                                }
                                                            ],
                                                            flex: 1
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'createVersion',
                    text: 'Создать новую версию'
                },
                {
                    itemId: 'runTest',
                    text: 'Протестировать'
                },
                {
                    xtype: 'container',
                    width: '16px',
                    items: [
                        {
                            xtype: 'button',
                            hidden: true,
                            bind: {
                                iconCls: 'icon-exclamation-{record.ERROR.color}',
                                hidden: '{!record.ERROR.type}'
                            },
                            handler: function () {
                                var error = this.bind.hidden.owner._data.record.data.ERROR;
                                Ext.Msg.alert('Ошибка (' + (error.type == 1 ? 'действующие' : 'тестовые') + ' стандартные условия)', error.message);
                            }
                        }
                    ]
                },
                '->',
                {
                    itemId: 'saveFormRule',
                    text: 'Применить',
                    listeners: {
                        'click': function (e) {
                            var formObj = e.up('app-settings-opokrule-form'),
                                accessObj = "OPOKRule";

                            var storeAccess = Ext.getStore('ObjectsAccess');
                            if (!storeAccess) return;

                            var i = storeAccess.findExact('id', accessObj);

                            // если нет никакого доступа
                            if (i == -1) {
                                Ext.Msg.alert('Внимание!', 'Нет прав на изменение/удаление действующих правил выявления');
                                return false;
                            }
                            // если доступ на тестирование
                            if ((storeAccess.getAt(i).get('mode') == 'T') && (formObj.getViewModel().data.record.get('VERS_STATUS_CD') == "ACT")) {
                                Ext.Msg.alert('Внимание!', 'Нет прав на перевод правила выявления в статус: Действующее');
                                var cb = formObj.lookupReference('statusComboBoxItem');
                                cb.setValue(cb.originalValue);
                                return false;
                            }
                        }
                    }
                },
                {
                    itemId: 'cancelFormRule',
                    text: 'Закрыть',
                    disabled: false,
                    readOnly: false
                }
            ]
        }
    ],
    listeners: {
        'afterrender': function (elem) {
            var self = this;
            // текущие права
            if (!self.controller.accessObject) return;

            var storeAccess = Ext.getStore('ObjectsAccess');
            if (!storeAccess) return;

            var i = storeAccess.findExact('id', self.controller.accessObject);

            // если доступ на тестирование и статус Действительна
            if ((storeAccess.getAt(i).get('mode') == 'T') && (self.getViewModel().data.record.get('VERS_STATUS_CD') == "ACT")) {
                elem.query('field').forEach(function (field) {
                    field.setReadOnly(true);
                });
                elem.down('#saveFormRule').setDisabled(true);
            }
            elem.down('#cancelFormRule').setDisabled(false);
        }
    }
});