Ext.define('AML.view.settings.OPOKRuleFormViewModel', {
	extend: 'AML.view.main.BaseFormViewModel',
	alias: 'viewmodel.opokruleformviewmodel',
	formulas: {
		title: function (get) {
			var record = get('record'); 	
			return (!record.phantom ? ('Правило выявления №: ' + record.data.RULE_CD + ', версия: ' + record.data.VERS_NUMBER) : 'Новое правило выявления');
		}
	},
	stores: {
		oes321types: {
			model: 'AML.model.OES321TypeOrg',
			remoteSort: false,
			remoteFilter: false,
			autoLoad: true
		}
	}
});
