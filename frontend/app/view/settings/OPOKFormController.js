Ext.define('AML.view.settings.OPOKFormController', {
    requires: [
        'Ext.data.StoreManager',
        'AML.model.OPOKDetails'
    ],
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.opokform',
    accessObject: 'OPOK', //код объекта доступа для данной формы
    // функция определяет: есть ли изменения (т. е. требуется ли сохранение)
    isChanges: function(window) {
        var store;

        store = Ext.StoreManager.lookup('OPOK');
        if (store && store.isChanges()) return true;

        store = Ext.StoreManager.lookup('OPOKDetails');
        if (store && store.isChanges()) return true;

        return false;
    },
    //функция отмены изменений в редактировавшейся записи
    doReject: function (store, record) {
        var stor;

        stor = Ext.StoreManager.lookup('OPOK');
        if (stor && stor.isChanges()) stor.rejectChanges();

        stor = Ext.StoreManager.lookup('OPOKDetails');
        if (stor && stor.isChanges()) stor.rejectChanges();
    },
    // функция сохранения данных формы
    saveForm: function (window, controller, closeFlag) {
        var sD,
            eD,
            itemI,
            returnError = false;

        if (!window || !controller) return;

        var form = window.down('form').getForm();
        if (!form || !form.isValid()) return;

        var opok = Ext.StoreManager.lookup('OPOK');
        if (!opok) return;

        var opokDetails = Ext.StoreManager.lookup('OPOKDetails');
        if (!opokDetails) return;

        var record = window.getViewModel().get('record');
        if (!record) return;

        // если это создание нового кода ОПОК - отправляем особый запрос, подкачиваем занового store деталей и ЗАКРЫВАЕМ ОКНО!
        if (record.phantom) {

            // валидация даты старта и окончания операции
            if (form.findField('START_DT').getValue() && form.findField('END_DT').getValue()) {
                if (form.findField('START_DT').getValue() > form.findField('END_DT').getValue()) {
                    form.findField('START_DT').validateValue(false);
                    form.findField('END_DT').validateValue(false);
                    return;
                }
            }
            // сохраняем изменения синхронным запросом - чтобы пользователь не мог, например, сохранить второй раз то же самое
            var async = Ext.Ajax.getAsync();
            Ext.Ajax.setAsync(false);

            //opok.sync({
            record.save({
                // Формируем dataDetails если создаётся новая запись
                params: ({dataDetails:
                    [{
                        RECORD_SEQ_ID: null,
                        OPOK_NB: form.findField('OPOK_NB').getValue(),
                        LIMIT_AM: form.findField('LIMIT_AM').getValue(),
                        START_DT: Ext.Date.format(form.findField('START_DT').getValue(), 'Y-m-d H:i:s'),
                        END_DT: Ext.Date.format(form.findField('END_DT').getValue(), 'Y-m-d H:i:s'),
                        ACTIVE_FL: 'Y'
                    }]
                }),
                success: function () { opokDetails.load(); window.close();},
                failure: opok.syncFailure
            });

            Ext.Ajax.setAsync(async);
        } else {
            // проверка пересичения дат.
            var dates = opokDetails.query('OPOK_NB', form.findField('OPOK_NB').getValue(), false, false, true);
            dates.each(function(selfObjItem, selfI, selfTotal) {
                itemI = selfI;
                sD = selfObjItem.get('START_DT') ? selfObjItem.get('START_DT') : new Date('01/01/1970');
                eD = selfObjItem.get('END_DT') ? selfObjItem.get('END_DT') : new Date('01/01/2199');
                dates.each(function(objItem, i, total) {
                    if (itemI !== i) {
                        var sD_objItem = objItem.get('START_DT') ? objItem.get('START_DT') : new Date('01/01/1970'),
                            eD_objItem = objItem.get('END_DT') ? objItem.get('END_DT') : new Date('01/01/2199');
                        if (sD >= sD_objItem && sD <= eD_objItem) {
                            returnError = true;
                        }
                        if (eD >= sD_objItem && eD <= eD_objItem) {
                            returnError = true;
                        }
                        if (sD >= sD_objItem && eD <= eD_objItem) {
                            returnError = true;
                        }
                        if (sD <= sD_objItem && eD >= eD_objItem) {
                            returnError = true;
                        }
                    }
                })
            });
            if (returnError) {
                Ext.Msg.alert('Ошибка', 'Периоды действия пересекаются! Запись не создана! Возникла системная ошибка. Обратитесь в службу технической поддержки.');
                return;
            }

            if (opok.isChanges()) {
                // сохраняем изменения синхронным запросом - чтобы пользователь не мог, например, сохранить второй раз то же самое
                var async = Ext.Ajax.getAsync();
                Ext.Ajax.setAsync(false);

                //opok.sync({
                record.save({
                    success: function () {
                        if (opokDetails.isChanges()) {
                            opokDetails.sync({
                                success: function() {
                                    if (closeFlag) window.close();
                                },
                                failure: opokDetails.syncFailure
                            });
                        } else {
                            if (closeFlag) window.close();
                        }
                    },
                    failure: opok.syncFailure
                });

                Ext.Ajax.setAsync(async);
            } else if (opokDetails.isChanges()) {
                // сохраняем изменения синхронным запросом - чтобы пользователь не мог, например, сохранить второй раз то же самое
                var async = Ext.Ajax.getAsync();
                Ext.Ajax.setAsync(false);

                opokDetails.sync({
                    success: function() {
                        if (closeFlag) window.close();
                    },
                    failure: opokDetails.syncFailure
                });

                Ext.Ajax.setAsync(async);
            }
        }
    },
    config: {
        listen: {
            component: {
                /*'app-settings-opok-form': {
                 show: 'onShowForm',
                 close: 'onCloseForm',
                 /**
                 * Обрабатываем событие отрисовки
                 * @param window
                 *
                 render: function (window) {
                 var me = this;
                 window.getEl().on('keypress', function (e) {
                 if (e.getKey() == e.ESC) {
                 me.onClickCancelForm();
                 }
                 });
                 if (Ext.isIE8) {
                 window.setWidth(Math.round(document.body.clientWidth / 100 * 80));
                 }
                 }
                 },*/
                '#OPOK_NB': {
                    change: 'onChangeOPOK_NB'
                },
                'gridpanel': {
                    selectionchange: 'onSelectionChangeDetail',
                    /**
                     * Обрабатываем события нажатия кнопок для таблицы
                     * @param table
                     * @param record
                     * @param tr
                     * @param rowIndex
                     * @param e
                     * @param eOpts
                     */
                    rowkeydown: function (table, record, tr, rowIndex, e, eOpts) {
                        if (this.isReadOnly) return;
                        switch (e.getKey()) {
                            case e.BACKSPACE:
                            case e.DELETE:
                                var recordsInStore = table.store.data.length,
                                    recordsSelected = table.getSelectionModel().selected.length;
                                if (recordsSelected && recordsSelected < recordsInStore) {
                                    this.onClickRemoveDetail();
                                }
                                break;
                        }
                    },
                    /**
                     * Событие перед редактированием записи таблицы
                     * @param editor
                     * @param context
                     * @param eOpts
                     */
                    beforeedit: function (editor, context, eOpts) {
                        // запрещаем редактирование в режиме "только чтение"
                        if (this.isReadOnly) return false;
                        context.grid.down('#createDetail').setDisabled(true);
                    },
                    /**
                     * Событие отмены редактирования записи таблицы
                     * @param editor
                     * @param context
                     * @param eOpts
                     */
                    canceledit: function (editor, context, eOpts) {
                        if (context.record.phantom) {
                            context.record.drop();
                        }
                        context.grid.down('#createDetail').setDisabled(false);
                    },
                    /**
                     * Событие редактирования записи таблицы
                     * @param editor
                     * @param context
                     * @param eOpts
                     */
                    edit: function (editor, context, eOpts) {
                        context.grid.down('#createDetail').setDisabled(false);
                    }
                },
                '#createDetail': {
                    click: 'onClickCreateDetail'
                },
                '#removeDetail': {
                    click: 'onClickRemoveDetail'
                }/*,
                 '#saveForm': {
                 click: 'onClickSaveForm'
                 },
                 '#cancelForm': {
                 click: 'onClickCancelForm'
                 }*/
            }
        }
    },
    /**
     * Метод для события изменения выбранных строк таблицы
     * @param model
     * @param selected
     * @param eOpts
     */
    onSelectionChangeDetail: function (model, selected, eOpts) {
        if (this.isReadOnly) return;
        var recordsInStore = model.store.data.length,
            recordsSelected = selected.length;
        // Скрыавем кнопку удаления если выбрано равное или большее кол-во записей чем есть в хранилище, а так же 0
        this.getView().down('#removeDetail').setDisabled(recordsSelected < 1 || recordsSelected >= recordsInStore);
    },
    /**
     * Метод для события отображения формы
     * @param window
     * @param eOpts
     */
    onShowForm: function (window, eOpts) {
        var opok_w = window.down('#OPOK_NB'),
            opok_r = window.down('#OPOK_NB_R'),
            newOPOKDetails = window.down('#newOPOKDetails'),
            listOPOKDetails = window.down('#listOPOKDetails');
        if (window.getViewModel().get('record').phantom) {
            opok_r.hide();
            listOPOKDetails.hide();
        } else {
            opok_w.hide();
            newOPOKDetails.hide().setDisabled(true);
            // на всякий случай: отменяем в деталях локальные изменения
            var store = Ext.StoreManager.lookup('OPOKDetails');
            if (store) { store.rejectChanges(); }

            // устанавливаем фильтр по коду ОПОК на детали
            store = Ext.StoreManager.lookup('OPOKDetailsGrid');
            if (store) {
                store.addFilter({
                    property: 'OPOK_NB',
                    value: window.getViewModel().get('record').data.OPOK_NB,
                    operator: '='
                });
            }
        }

        this.callParent(arguments); // закрываем форму, если на нее нет прав
    },
    /**
     * Метод для события закрытия формы
     * @param window
     * @param eOpts
     */
    onCloseForm: function (window, eOpts) {
        /*var opok = Ext.StoreManager.lookup('OPOK'),
         opokDetails = Ext.StoreManager.lookup('OPOKDetails');
         if (opok.isChanges()) {
         opok.rejectChanges();
         }
         if (opokDetails.isChanges()) {
         opokDetails.rejectChanges();
         }*/
        this.callParent(arguments);

        // дополнительно: сбрасываем фильтр по коду ОПОК с деталей и отменяем в деталях локальные изменения
        var store = Ext.StoreManager.lookup('OPOKDetailsGrid');
        if (store) { store.clearFilter(); }

        store = Ext.StoreManager.lookup('OPOKDetails');
        if (store) { store.rejectChanges();	}
    },
    /**
     * Метод для события изменения поля OPOK_NB
     * @param field
     * @param newValue
     * @param oldValue
     * @param eOpts
     */
    onChangeOPOK_NB: function (field, newValue, oldValue, eOpts) {
        var win = this.getView();
        if (win.getViewModel().get('record').phantom) {
            field.up('form').down('#PRIORITY_NB').setValue(newValue);
            field.up('form').down('#ORDER_NB').setValue(newValue);
        }
    },
    /**
     * Создаем запись пороговой суммы
     * @param button
     * @param event
     * @param eOpts
     */
    onClickCreateDetail: function (button, event, eOpts) {
        if (this.isReadOnly) return;

        var opok_nb = this.getView().getViewModel().get('record').data.OPOK_NB,
            grid = this.getView().down('#listOPOKDetails');
        grid.editingPlugin.cancelEdit();
        var record = Ext.create('AML.model.OPOKDetails', {
            OPOK_NB: opok_nb
        });
        grid.editingPlugin.startEdit(grid.store.insert(0, record)[0]);
    },
    /**
     * Удаляем запись пороговой суммы
     * @param button
     * @param event
     * @param eOpts
     */
    onClickRemoveDetail: function (button, event, eOpts) {
        if (this.isReadOnly) return;

        var grid = this.getView().down('#listOPOKDetails'),
            sm = grid.getSelectionModel();
        grid.editingPlugin.cancelEdit();
        grid.store.remove(sm.getSelection());
    },
    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    /*onClickSaveForm: function (button, eOpts) {
     var win = this.getView(),
     form = win.down('form').getForm(),
     opok = Ext.StoreManager.lookup('OPOK'),
     opokPhantom = win.getViewModel().getData().record.phantom,
     opokDirty = win.getViewModel().getData().record.dirty,
     opokChange = (opokDirty || opokPhantom),
     opokDetails = Ext.StoreManager.lookup('OPOKDetails'),
     opokDetailsChange = opokDetails.isChanges();
     if (opokChange || opokDetailsChange) {
     if (!opokChange) {
     opokDetails.sync({
     success: function () {
     },
     failure: opokDetails.syncFailure
     });
     } else if (form.isValid()) {
     opok.sync({
     // Формируем dataDetails если создаётся новая запись
     params: (opokPhantom ? {
     dataDetails: [
     {
     RECORD_SEQ_ID: null,
     OPOK_NB: form.findField('OPOK_NB').getValue(),
     LIMIT_AM: form.findField('LIMIT_AM').getValue(),
     START_DT: Ext.Date.format(form.findField('START_DT').getValue(), 'Y-m-d H:i:s'),
     END_DT: Ext.Date.format(form.findField('END_DT').getValue(), 'Y-m-d H:i:s'),
     ACTIVE_FL: 'Y'
     }
     ]
     } : undefined),
     success: function () {
     // Обновим хранилище opokDetails если запись была новой
     if (opokPhantom) {
     opokDetails.load();
     }
     if (opokDetailsChange) {
     opokDetails.sync({
     success: function () {
     },
     failure: opokDetails.syncFailure
     });
     } else {
     win.close();
     }
     },
     failure: opok.syncFailure
     });
     }
     } else {
     win.close();
     }
     },*/
    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    /*onClickCancelForm: function (button, eOpts) {
     var win = this.getView(),
     change = win.getViewModel().getData().record.store.isChanges(),
     opokDetails = Ext.StoreManager.lookup('OPOKDetails'),
     opokDetailsChange = opokDetails.isChanges();
     if (change || opokDetailsChange) {
     AML.app.msg.confirm({
     type: 'warning',
     message: 'Все изменения будут потеряны!<br>Вы уверены что хотите закрыть форму?',
     fnYes: function () {
     win.close();
     }
     });
     } else {
     win.close();
     }
     },*/
    /**
     * Обновляем хранилище
     */
    onStoreRefresh: function () {
        Ext.data.StoreManager.lookup('OPOKDetails').load();
    }
});
