Ext.define('AML.view.settings.OPOKController', {
	requires: [
		'Ext.data.StoreManager',
		'AML.model.OPOK'
	],
	extend: 'AML.view.main.BaseListController',
    alias: 'controller.opok',
	childForm: 'app-settings-opok-form', //xtype формы редактирования, открываемой из списка
	IdColumn: 'OPOK_NB', //name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя
	accessObject: 'OPOK', //код объекта доступа для данной формы
	doRemove: function (store, records) { //функция удаления выделенных во view записей из store
	            // на всякий случай: отменяем локальные изменения в store (чтобы при синхронизации были только наши удаления)
				store.rejectChanges();				

				store.remove(records);
				store.sync({
					success: function () {
						// Обновляем детали при положительном результате
						var opokDetails = Ext.StoreManager.lookup('OPOKDetails');
						opokDetails.load();
						},
					failure: store.syncFailure
					});
				},
	/*config: {
		listen: {
			component: {
				'gridpanel': {
					rowkeydown: function (table, record, tr, rowIndex, e, eOpts) {
						switch (e.getKey()) {
							case e.ENTER:
							case e.SPACE:
								this.onClickOpenForm(record);
								break;
							case e.BACKSPACE:
							case e.DELETE:
								this.onClickRemove();
								break;
						}
					}
				},
				//'app-settings-opok': {
				'#': {
					beforerender: 'onBeforeRender',
					selectionchange: 'onSelectionChange',
					rowdblclick: 'onDBClickOpenForm'
				},
				'#create': {
					click: 'onClickCreate'
				},
				'#remove': {
					click: 'onClickRemove'
				}
			}
		}
	},
	onBeforeRender: function(grid, eOpts) {
		var tmp = grid.query('.button');
	},
	onSelectionChange: function (model, selected, eOpts) {
		var grid = this.getView();
		grid.down('#remove').setDisabled(!selected.length);
	},
	onDBClickOpenForm: function (grid, record, index, eOpts) {
		var form = Ext.widget('app-settings-opok-form'),
			formViewModel = form.getViewModel();
		formViewModel.setData(Ext.apply(formViewModel.getData(), {record: record}));
		if (!record.phantom) {
			Ext.data.StoreManager.lookup('OPOKDetailsGrid').addFilter({
				property: 'OPOK_NB',
				value: record.data.OPOK_NB
			});
		}
		form.show();
	},
	onClickCreate: function (button, event, eOpts) {
		var grid = this.getView(),
			record = Ext.create('AML.model.OPOK', {});
		grid.store.insert(0, record);
		grid.getSelectionModel().select(0);
		grid.getView().focusRow(0);
		grid.fireEvent('rowdblclick', grid, record);
	},*/
	/**
	 * Открытие формы
	 */
	/*onDBClickOpenForm: function (grid, record, index, eOpts) {
		var formType = this.childForm;
		if (formType) {
			var form = Ext.widget(formType),
				formViewModel = form.getViewModel();
			formViewModel.setData(Ext.apply(formViewModel.getData(), {record: record}));
			if (!record.phantom) {
				Ext.data.StoreManager.lookup('OPOKDetailsGrid').addFilter({
					property: 'OPOK_NB',
					value: record.data.OPOK_NB
				});
			}
			form.show();
			}
	},*/
	 /*onClickOpenForm: function (record) {
		var grid = this.getView();
		grid.fireEvent('rowdblclick', grid, record);
	},*/
	/**
	 * Удаление
	 */
	/*onClickRemove: function (button, event, eOpts) {
		var grid = this.getView(),
			sm = grid.getSelectionModel(),
			codes = sm.getSelection().map(function (v) {
				return v.data.OPOK_NB;
			}).join(', ');
		AML.app.msg.confirm({
			message: ('Вы уверены что хотите удалить ' + (sm.getCount() > 1 ? 'коды ОПОК: ' + codes : 'код ОПОК: ' + codes) + '?'),
			fnYes: function () {
				grid.store.remove(sm.getSelection());
				var opok = Ext.StoreManager.lookup('OPOK'),
					opokDetails = Ext.StoreManager.lookup('OPOKDetails');
				opok.sync({
					success: function () {
						// Обновляем детали при положительном результате
						opokDetails.load();
					},
					failure: opok.syncFailure
				});
			}
		});
	},*/
	/**
	 * Фильтры
	 */
	onChangeFilterOPOK_NB: function (field, event, eOpts) {
		var store = this.getStore('opok');
        if (Ext.isDefined(store) || !Ext.isObject(store)) {
            store = Ext.StoreManager.lookup('OPOK');
        }
		if (field.value) {
			store.addFilter({
				property: 'OPOK_NB',
				value: field.value
			});
		} else {
			store.removeFilter('OPOK_NB');
		}
	},
	onChangeFilterOPOK_NM: function (field, event, eOpts) {
		var store = this.getStore('opok');
        if (Ext.isDefined(store) || !Ext.isObject(store)) {
            store = Ext.StoreManager.lookup('OPOK');
        }
		if (field.value) {
			store.addFilter({
				property: 'OPOK_NM',
				value: new RegExp(field.value, "i")
			});
		} else {
			store.removeFilter('OPOK_NM');
		}
	}//,
	/**
	 * Обновляем хранилище
	 */
	/*onStoreRefresh: function () {
		Ext.data.StoreManager.lookup('OPOK').load();
	}*/
});
