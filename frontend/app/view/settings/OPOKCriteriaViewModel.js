Ext.define('AML.view.assign.OPOKCriteriaViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.opokcriteriavmdl',
	data: {
		title: 'Стандартные условия выявления',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
	},
    stores: {
        // Справочник стандартных условий выявления (таблица)
        opokcriteria: {
            model: 'AML.model.OPOKCriteria',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'CRIT_CD',
                    direction: 'ASC'
                }
            ]
        }
    }
});
