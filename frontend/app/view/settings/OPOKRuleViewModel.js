Ext.define('AML.view.assign.OPOKRuleViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.opokrulevmdl',
	data: {
		title: 'Правила выявления',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
	},
    stores: {
        // Справочник правил выявления (таблица)
        oporule: {
            model: 'AML.model.OPOKRule',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OPOK_NB',
                    direction: 'ASC'
                },
                {
                    property: 'VERS_NUMBER',
                    direction: 'DESC'
                },
                {
                    property: 'PRIORITY_NB',
                    direction: 'ASC'
                }
            ],
            filters: [
                {
                    property: 'VERS_STATUS_CD',
                    operator: '!=',
                    value: 'ARC'
                }
            ]
        }
    }
});
