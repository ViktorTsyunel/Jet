Ext.define('AML.view.settings.OPOKForm', {
	extend: 'Ext.window.Window',
	requires: [
		'AML.view.settings.OPOKFormController',
		'AML.view.main.BaseFormViewModel',
		'Ext.container.Container',
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'Ext.form.Panel',
		'Ext.form.field.Checkbox',
		'Ext.form.field.Date',
		'Ext.form.field.Display',
		'Ext.form.field.Number',
		'Ext.form.field.Text',
		'Ext.form.field.TextArea',
		'Ext.grid.Panel',
		'Ext.grid.column.Date',
		'Ext.grid.column.Number',
		'Ext.grid.plugin.RowEditing',
		'Ext.layout.container.Anchor',
		'Ext.layout.container.HBox',
		'Ext.layout.container.VBox'
	],
	xtype: 'app-settings-opok-form',
	controller: 'opokform',
	iconCls: 'icon-report',
	viewModel: {
		type: 'baseformviewmodel'
	},
	title: 'Код ОПОК',
	/*bind: {
		title: '{title}'
	},*/
	closable: false,
	tools: [
		{
			type: 'close',
			callback: 'onClickCancelForm'
		}
	],
	items: [
		{
			xtype: 'form',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			border: false,
			defaults: {
				labelWidth: 150
			},
			items: [
				{
					xtype: 'fieldset',
					title: 'Основные данные',
					layout: 'anchor',
					collapsible: false,
					defaults: {
						anchor: '100%',
						labelWidth: 150
					},
					items: [
						{
							xtype: 'fieldcontainer',
							layout: 'hbox',
							defaults: {
								labelWidth: 150,
								labelAlign: 'right'
							},
							items: [
								{
									itemId: 'OPOK_NB',
									name: 'OPOK_NB',
									xtype: 'numberfield',
									bind: {
										value: '{record.OPOK_NB}'
									},
									fieldLabel: 'Код ОПОК',
									tooltip: {
										text: 'Код операции, подлежащей обязательному контролю в соответствии с перечнем РФМ: 1001-1008, 3001, 3011, 3021, 4001-4007, 5001-5007, 6001, 7001, 8001, 9001'
									},
									labelAlign: 'left',
									allowBlank: false,
									minValue: 1,
									flex: 1
								},
								{
									itemId: 'OPOK_NB_R',
									xtype: 'displayfield',
									labelAlign: 'left',
									bind: {
										value: '<b>{record.OPOK_NB}</b>'
									},
									fieldLabel: 'Код ОПОК',
									fieldStyle: {
										fontSize: '24px'
									},
									flex: 1
								},
								{
									itemId: 'PRIORITY_NB',
									xtype: 'numberfield',
									bind: {
										value: '{record.PRIORITY_NB}'
									},
									fieldLabel: 'Приоритет',
									tooltip: {
										text: 'Приоритет данного кода ОПОК. Влияет на определении основного и дополнительных кодов ОПОК в случае, если одна и та же операция подпала сразу под несколько кодов ОПОК.' +
										'<br>' +
										'<br>Основной код ОПОК в этом случае - это код ОПОК с наименьшим значением приоритета.' +
										'<br>' +
										'<br>Текущая расстановка приоритетов следующая:' +
										'<br> - 7001 код имеет наивысший приоритет (наименьшее значение приоритета, например 1)' +
										'<br> - 6001 код имеет низший приоритет (наибольшее значение приоритета, например, 9999)' +
										'<br> - остальные коды упорядочиваются по своему коду (значение приоритета равно коду ОПОК'
									},
									flex: 1
								},
								{
									itemId: 'ORDER_NB',
									xtype: 'numberfield',
									bind: {
										value: '{record.ORDER_NB}'
									},
									fieldLabel: 'Номер для сортировки',
									tooltip: {
										text: 'Порядковый номер кодов ОПОК для сортировки оповещений на интерфейсе пользователя.' +
										'<br>' +
										'<br>Оповещения упорядочиваются в соответствии с данным номером для основного кода ОПОК.' +
										'<br>' +
										'<br>Текущий порядок сортировки предлагается выбрать в соответствии с приоритетами кодов ОПОК (см. предыдущее поле).'
									},
									flex: 1
								},
								{
									xtype: 'numberfield',
									bind: {
										value: '{record.GROUP_NB}'
									},
									fieldLabel: 'Групповой код выявления',
									tooltip: {
										text: 'Код, позволяющий сгруппировать правила выявления, относящиеся к разным кодам ОПОК.' +
										'<br>Нужен для 3000-х кодов ОПОК, у которых есть основное условие выявление - связь участника операции со страной из "черного" списка и дополнительное условие, определяющее конкретный код ОПОК - характер операции (3011 - кредит/займ, 3021 - ценные бумаги, 3001 - остальные операции).' +
										'<br>В этой ситуации нужно, чтобы из правил выявления, относящихся к кодам 3001, 3011, 3021, "сработало" только одно, в соответствии с общей настройкой приоритетов, и операция была отнесена к какому-нибудь одному коду ОПОК.' +
										'<br>Для этих трех кодов ОПОК предлагается заполнить данное поле значением "3000"'
									},
									labelWidth: 180,
									flex: 1
								}
							]
						},
						{
							xtype: 'fieldcontainer',
							layout: 'hbox',
							defaults: {
								labelWidth: 150,
								labelAlign: 'right'
							},
							items: [
								{
									xtype: 'textarea',
									bind: {
										value: '{record.OPOK_NM}'
									},
									fieldLabel: 'Наименование',
									tooltip: {
										text: 'Наименование кода ОПОК'
									},
									allowBlank: false,
									labelAlign: 'left',
									flex: 1
								},
								{
									xtype: 'textarea',
									bind: {
										value: '{record.NOTE}'
									},
									fieldLabel: 'Примечание',
									tooltip: {
										text: 'Примечание'
									},
									flex: 1
								}
							]
						},
						{
							xtype: 'textfield',
							bind: {
								value: '{record.OES_PARTIES_TX}'
							},
							fieldLabel: 'Участники в ОЭС',
							tooltip: {
								text: 'Текстовая строка, привязывающая участников операции в Мантасе к блокам участников операции в сообщении в РФМ по форме 321-П' +
								'<br>Имеет вид: 0-ORIG,1-SCND_ORIG,3-ORIG' +
								'<br>Цифрами кодируются номера блоков участников в сообщении по форме 321-П - от 0 до 4' +
								'<br>символьными кодами - участники операции в Мантасе или сам Сбербанк:' +
								'<br>\'ORIG\' - плательщик' +
								'<br>\'SCND_ORIG\' - доп. отправитель платежа (представитель плательщика)' +
								'<br>\'BENEF\' - получатель платежа' +
								'<br>\'SCND_BENEF\' - доп. получатель платежа (представитель получателя)' +
								'<br>\'SBRF\' - Сбербанк РФ' +
								'<br>Примеры:' +
								'<br>0-ORIG,1-SCND_ORIG,3-ORIG - в блоках 0 и 3 указывается плательщик, в блоке 1 - представитель плательщика' +
								'<br>0-ORIG,3-SBRF - в блоке 0 указывается плательщик, в блоке 3 - Сбербанк' +
								'<br>Если данное поле не заполнено, то применяется стандартное правило соответствия:' +
								'<br>0-ORIG,1-SCND_ORIG,2-SCND_BENEF,3-BENE'
							}
						}
					]
				},
				{
					xtype: 'fieldset',
					title: 'Дополнительные данные',
					layout: 'anchor',
					collapsible: false,
					defaults: {
						anchor: '100%'
					},
					items: [
						{
							itemId: 'newOPOKDetails',
							xtype: 'fieldcontainer',
							layout: 'hbox',
							defaults: {
								labelWidth: 150,
								labelAlign: 'right'
							},
							items: [
								{
									itemId: 'LIMIT_AM',
									name: 'LIMIT_AM',
									xtype: 'numberfield',
									labelAlign: 'left',
									fieldLabel: 'Пороговая сумма',
									tooltip: {
										text: 'Пороговая сумма в рублях, начиная с которой сообщения об операциях по данному коду ОПОК должны отправляться в РФМ' +
										'<br>Как правило, 600 тыс. руб).' +
										'<br>Исключения составляют коды:' +
										'<br>4007 - стратегические предприятия, порог = 50 млн. руб.' +
										'<br>6001 - подозрительные операции, без ограничения пороговой суммы?' +
										'<br>7001 - террористы, без ограничения пороговой суммы' +
										'<br>8001 - недвижимость, порог = 3 млн. руб.' +
										'<br>9001 - НКО, порог = 200 тыс. руб.'
									},
									allowBlank: false,
									minValue: 1,
									flex: 1
								},
								{
									itemId: 'START_DT',
									name: 'START_DT',
									xtype: 'datefield',
									format: 'd.m.Y',
									startDay: 1,
									fieldLabel: 'Начало действия',
									tooltip: {
										text: 'Начало действия данного значения пороговой суммы для данного кода ОПОК, если null, то - с начала времен' +
										'<br>Операция подлежит контролю, если сумма у нее больше либо равна пороговой и дата ее проведения принадлежит периоду действия пороговой суммы'
									},
									flex: 1,
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                    container.nextSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        select: function (container, value, eOpts) {
                                            container.nextSibling('datefield').focus();
                                            container.focus();
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                }
                                            }
                                        }
                                    }
								},
								{
									itemId: 'END_DT',
									name: 'END_DT',
									xtype: 'datefield',
									format: 'd.m.Y',
									startDay: 1,
									fieldLabel: 'Окончание действия',
									tooltip: {
										text: 'Окончание действия данного значения пороговой суммы для данного кода ОПОК, если null, то - до конца времен' +
										'<br>Операция подлежит контролю, если сумма у нее больше либо равна пороговой и дата ее проведения принадлежит периоду действия пороговой суммы'
									},
									flex: 1,
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                    container.previousSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        select: function (container, value, eOpts) {
                                            container.previousSibling('datefield').focus();
                                            container.focus();
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                    if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                    }
                                                }
                                            }
                                        }
                                    }
								},
								{
									xtype: 'container',
									flex: 0.1
								}
							]
						},
						{
							itemId: 'listOPOKDetails',
							xtype: 'gridpanel',
							store: 'OPOKDetailsGrid',
							border: true,
							padding: '0 0 5 0',
							frame: false,
							height: 150,
							stateful: true,
							stateId: 'opokdetails',
							tools: [
								{
									type: 'refresh',
									callback: 'onStoreRefresh'
								}
							],
                            listeners: {
                                validateedit: function (editor, context) {
                                    var start = editor.getEditor().items.getAt(1).value || context.record.get('START_DT'),
                                        end = editor.getEditor().items.getAt(2).value || context.record.get('END_DT');

                                    if (!start || !end) {
                                        return true;
                                    }
                                    if (start <= end) {
                                        return true;
                                    }

                                    return false;
                                }
                            },
							columns: [
								{
									xtype: 'numbercolumn',
									format: '0.00',
									text: 'Пороговая сумма',
									stateId: 'LIMIT_AM',
									dataIndex: 'LIMIT_AM',
									flex: 1,
									align: 'right',
									editor: {
										xtype: 'numberfield',
										msgTarget: 'qtip',
										minValue: 0,
										allowBlank: false
									}
								},
								{
									text: 'Начало действия',
									stateId: 'START_DT',
									dataIndex: 'START_DT',
									flex: 1,
									xtype: 'datecolumn',
									format: 'd.m.Y',
									startDay: 1,
									editor: {
										xtype: 'datefield',
										msgTarget: 'none',
										format: 'd.m.Y',
										startDay: 1,
										allowBlank: true,
                                        listeners: {
                                            validitychange: function (container, value, eOpts) {
                                                if (value) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.validateValue(false);
                                                        }
                                                        container.nextSibling('datefield').focus();
                                                        container.focus();
                                                    }
                                                }
                                            },
                                            select: function (container, value, eOpts) {
                                                container.nextSibling('datefield').focus();
                                                container.focus();
                                            },
                                            blur: function (container, value, eOpts) {
                                                if (value) {
                                                    if (container.nextSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                            container.validateValue(false);
                                                        }
                                                    }
                                                }
                                            }
                                        }
									}
								},
								{
									text: 'Окончание действия',
									stateId: 'END_DT',
									dataIndex: 'END_DT',
									flex: 1,
									xtype: 'datecolumn',
									format: 'd.m.Y',
									editor: {
										xtype: 'datefield',
										msgTarget: 'none',
										format: 'd.m.Y',
										startDay: 1,
										allowBlank: true,
                                        listeners: {
                                            validitychange: function (container, value, eOpts) {
                                                if (value) {
                                                    if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                            container.validateValue(false);
                                                        }
                                                        container.previousSibling('datefield').focus();
                                                        container.focus();
                                                    }
                                                }
                                            },
                                            select: function (container, value, eOpts) {
                                                container.previousSibling('datefield').focus();
                                                container.focus();
                                            },
                                            blur: function (container, value, eOpts) {
                                                if (value) {
                                                    if (container.previousSibling('datefield').getValue() && container.getValue()) {
                                                        if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                            container.validateValue(false);
                                                        }
                                                    }
                                                }
                                            }
                                        }
									}
								},
								{
									text: 'Учитывать?',
									stateId: 'ACTIVE_FL',
									dataIndex: 'ACTIVE_FL',
									flex: 1,
									renderer: function (value) {
										return (value ? 'Да' : 'Нет');
									},
									editor: {
										xtype: 'checkbox',
										allowBlank: false
									}
								}
							],
							tbar: [
								{
									itemId: 'createDetail',
									text: 'Добавить',
									iconCls: 'icon-plus'
								},
								{
									itemId: 'removeDetail',
									text: 'Удалить',
									iconCls: 'icon-minus',
									disabled: true
								}
							],
							plugins: {
								ptype: 'rowediting',
								errorSummary: false,
								clicksToEdit: 2
							}
						}
					]
				}
			],
			buttons: [
				{
					itemId: 'saveForm',
					text: 'Применить'
				},
				{
					itemId: 'cancelForm',
					text: 'Закрыть'
				}
			]
		}
	]
});