Ext.define('AML.view.settings.OPOKRule', {
	extend: 'Ext.grid.Panel',
	requires: [
		'AML.view.settings.OPOKRuleController',
		'Ext.data.StoreManager',
		'Ext.form.field.ComboBox',
		'Ext.grid.column.Template',
		'Ext.toolbar.Fill',
		'Ext.toolbar.Separator'
	],
	xtype: 'app-settings-opokrule',
	reference: 'appSettingsOpokrule',
	controller: 'opokrule',
    viewModel: {
        type: 'opokrulevmdl'
    },
    bind: {
        store: '{oporule}',
        title: '{title}'
    },
	stateful: true,
	stateId: 'opokrule',
	tools: [
		{
			type: 'refresh',
			callback: 'onStoreRefresh'
		}
	],
	cls: 'app-opokrule-grid',
	viewConfig: {
		stripeRows: false,
		getRowClass: function (record) {
			var error = record.get('ERROR');
			if (error.type) {
				return error.color;
			}
		}
	},
	columns: [
		{
			text: '№ (версия)',
			identifier: true,
			stateId: 'RULE_CD',
			dataIndex: 'RULE_CD',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				return value + ' (' + record.data.VERS_NUMBER + ')';
			},
			width: 80
		},
		{
			text: 'Код ОПОК',
			stateId: 'OPOK_NB',
			dataIndex: 'OPOK_NB',
			width: 80
		},
		{
			text: 'Статус',
			stateId: 'VERS_STATUS_CD',
			dataIndex: 'VERS_STATUS_CD',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				var status = Ext.data.StoreManager.lookup('OPOKRuleStatus');
				return (status && value && status.getById(value).get('label')) || 'Нет';
			},
			flex: 1
		},
		{
			text: 'Категория операции',
			stateId: 'OP_CAT_CD',
			dataIndex: 'OP_CAT_CD',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				var category = Ext.data.StoreManager.lookup('OPOKRuleOperationCategory'),
					direction = Ext.data.StoreManager.lookup('OPOKRuleInOut');
				if (value == 'CT') {
					return ((category && value && category.getById(value).get('label')) || 'Нет') +
						(record.data.DBT_CDT_CD
							? ' (' + (direction && record.data.DBT_CDT_CD && direction.getById(record.data.DBT_CDT_CD).get('label')) + ')'
							: '');
				} else {
					return (category && value && category.getById(value).get('label')) || 'Нет';
				}
			},
			flex: 1.5
		},
		{
			text: 'Запрос',
			stateId: 'QUERY_CD',
			dataIndex: 'QUERY_CD',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				var query = Ext.data.StoreManager.lookup('OPOKRuleQuery');
				return (query && value && query.getById(value).get('label')) || 'Нет';
			},
			flex: 1
		},
		{
			text: 'Приоритет',
			stateId: 'PRIORITY_NB',
			dataIndex: 'PRIORITY_NB',
			sortable: false,
			width: 90
		},
		{
			text: 'Тербанк',
			stateId: 'TB_ID',
			dataIndex: 'TB_ID',
			renderer: function (value) {
				var tb = Ext.data.StoreManager.lookup('TB');
				return (tb && tb.getById(value).get('label')) || 'Нет';
			},
			flex: 2
		},
		{
			text: 'Межфилиальные операции подлежат контролю',
			stateId: 'KGRKO_PARTY_CD',
			dataIndex: 'KGRKO_PARTY_CD',
			renderer: function (value) {
				switch(value) {
					case 0:
						return "в обоих филиалах";
					case 1:
						return "в филиале по дебету";
					case 2:
						return "в филиале по кредиту";
					default:
						return "";
				}
			},
			width: 150
		},
		{
			text: 'Примечание',
			stateId: 'NOTE_TX',
			dataIndex: 'NOTE_TX',
			sortable: false,
			flex: 3
		},
		{
			text: 'Ошибка',
			stateId: 'ERROR',
			dataIndex: 'ERROR',
			sortable: false,
			xtype: 'templatecolumn',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl if="ERROR.type">',
					'<div class="icon icon-bullet-{ERROR.color}" data-qtip="{ERROR.message}"></div>',
				'</tpl>'
			),
			width: 55
		}
	],
	tbar: [
		{
			itemId: 'create',
			text: 'Добавить',
			iconCls: 'icon-plus'
		},
		{
			itemId: 'remove',
			text: 'Удалить',
			iconCls: 'icon-minus',
			disabled: true,
			listeners: {
				'click': function (e, t, r) {
					var recObj = e.up().up(),
						recController = e.up().up().controller,
						accessObj = "OPOKRule";

					var storeAccess = Ext.getStore('ObjectsAccess');
					if (!storeAccess) return;

					var i = storeAccess.findExact('id', accessObj);

					// если нет никакого доступа
					if (i == -1) {
						Ext.Msg.alert('Внимание!', 'Нет прав на изменение/удаление действующих правил выявления');
						return false;
					}
					var selectedArr = recObj.getSelection();
					// если доступ на тестирование
					if (storeAccess.getAt(i).get('mode') == 'T') {
						if (selectedArr.length > 1) {
							for (var k = 0; k < selectedArr.length; k++) {
								if (selectedArr[k].get('VERS_STATUS_CD') == "ACT") {
									Ext.Msg.alert('Внимание!', 'Нет прав на изменение/удаление действующих правил выявления');
									return false;
								}
							}
						} else if (selectedArr.length == 1) {
							if (selectedArr[0].get('VERS_STATUS_CD') == "ACT") {
								Ext.Msg.alert('Внимание!', 'Нет прав на изменение/удаление действующих правил выявления');
								return false;
							}
						}
					}
				}
			}
		},
		'-',
		{
			xtype: 'combobox',
			store: 'OPOKRuleStatus',
			editable: false,
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
				'</tpl>'
			),
			displayField: 'label',
			valueField: 'id',
			emptyText: 'Выберите статус',
			fieldLabel: 'Показать правила со статусом',
			labelWidth: 165,
			labelAlign: 'right',
			listeners: {
				select: 'onSelectFilterStatus'
			},
			triggerClear: true,
			triggerClearHandler: 'onClickFilterTriggerStatus',
			flex: 0.35
		},
		'->'
	]
});
