Ext.define('AML.view.settings.OPOKCriteria', {
	extend: 'Ext.grid.Panel',
	requires: [
		'AML.view.settings.OPOKCriteriaController',
		'Ext.data.StoreManager',
		'Ext.form.field.ComboBox',
		'Ext.form.field.Text',
		'Ext.form.field.Checkbox'
	],
	xtype: 'app-settings-opokcriteria',
	controller: 'opokcriteria',
    viewModel: {
        type: 'opokcriteriavmdl'
    },
    bind: {
        store: '{opokcriteria}',
        title: '{title}'
    },
	stateful: true,
	stateId: 'opokcriteria',
	tools: [
		{
			type: 'refresh',
			callback: 'onStoreRefresh'
		}
	],
	columns: [
		{
			text: 'Код условия',
			identifier: true,
			stateId: 'CRIT_CD',
			dataIndex: 'CRIT_CD',
			flex: 0.5
		},
		{
			text: 'Тип условия',
			stateId: 'CRIT_TP_CD',
			dataIndex: 'CRIT_TP_CD',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				var types = Ext.data.StoreManager.lookup('OPOKCriteriaTypes');
				return (types && value && types.getById(value).get('label')) || 'Нет';
			},
			flex: 1
		},
		{
			text: 'Наименование',
			stateId: 'CRIT_NM',
			dataIndex: 'CRIT_NM',
			flex: 2
		},
		{
			text: 'Вид использования',
			stateId: 'CRIT_USE_CD',
			dataIndex: 'CRIT_USE_CD',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				var uses = Ext.data.StoreManager.lookup('OPOKCriteriaUses');
				return (uses && value && uses.getById(value).get('label')) || 'Нет';
			},
			flex: 0.5
		},
		{
			text: 'Активно?',
			stateId: 'ACTIVE_FL',
			dataIndex: 'ACTIVE_FL',
			renderer: function(value) {
				return (value ? 'Да' : 'Нет');
			},
			flex: 0.5
		}
	],
	tbar: [
		{
			itemId: 'create',
			text: 'Добавить',
			iconCls: 'icon-plus'
		},
		{
			itemId: 'remove',
			text: 'Удалить',
			iconCls: 'icon-minus',
			disabled: true
		},
		'-',
		{
			itemId: 'filterCRIT_TP_CD',
			xtype: 'combobox',
			store: 'OPOKCriteriaTypes',
			editable: false,
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
				'</tpl>'
			),
			displayField: 'label',
			valueField: 'id',
			emptyText: 'Выберите тип',
			fieldLabel: 'Тип условия',
			labelWidth: 70,
			labelAlign: 'right',
			listeners: {
				select: 'onSelectFilterCRIT_TP_CD'
			},
			triggerClear: true,
			triggerClearHandler: 'onClickFilterTriggerCRIT_TP_CD',
			flex: 1
		},
		{
			itemId: 'filterCRIT_NM',
			xtype: 'textfield',
			fieldLabel: 'Поиск',
			labelWidth: 40,
			labelAlign: 'right',
			emptyText: 'По наименованию',
			listeners: {
				change: 'onChangeFilterCRIT_NM'
			},
			triggerClear: true,
			flex: 0.5
		},
		{
			itemId: 'filterCRIT_USE_CD',
			xtype: 'combobox',
			store: 'OPOKCriteriaUses',
			editable: false,
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
				'</tpl>'
			),
			displayField: 'label',
			valueField: 'id',
			emptyText: 'Выберите вид',
			fieldLabel: 'Вид использования',
			labelWidth: 110,
			labelAlign: 'right',
			listeners: {
				select: 'onSelectFilterCRIT_USE_CD'
			},
			triggerClear: true,
			triggerClearHandler: 'onClickFilterTriggerCRIT_USE_CD',
			flex: 0.6
		},
		{
			itemId: 'filterACTIVE_FL',
			xtype: 'checkbox',
			fieldLabel: 'Только активные',
			labelWidth: 100,
			labelAlign: 'right',
			listeners: {
				change: 'onChangeFilterACTIVE_FL'
			},
			flex: 0.5
		},
		'->'
	]
});
