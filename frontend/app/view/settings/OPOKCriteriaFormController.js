Ext.define('AML.view.settings.OPOKCriteriaFormController', {
	requires: [
		'Ext.data.StoreManager'
	],
	extend: 'AML.view.main.BaseFormController',
    alias: 'controller.opokcriteriaform',
	accessObject: 'OPOKCriteria', //код объекта доступа для данной формы
	/*config: {
		listen: {
			component: {
				'app-settings-opokcriteria-form': {
					show: 'onShowForm',
					close: 'onCloseForm',
					/**
					 * Обрабатываем событие отрисовки
					 * @param window
					 *
					render: function (window) {
						var me = this;
						window.getEl().on('keypress', function (e) {
							if (e.getKey() == e.ESC) {
								me.onClickCancelForm();
							}
						});
						if (Ext.isIE8) {
							window.setWidth(Math.round(document.body.clientWidth / 100 * 80));
						}
					}
				},
				'#saveForm': {
					click: 'onClickSaveForm'
				},
				'#cancelForm': {
					click: 'onClickCancelForm'
				}
			}
		}
	},*/
	// метод, возвращающий предупреждение для пользователя перед сохранением данных (если возвращает null - без предупреждения)
	getSaveConfirmText: function(form) {
		var CRIT_TX = form.findField('CRIT_TX'),
			SUBTLE_CRIT_TX = form.findField('SUBTLE_CRIT_TX'),
			NULL_FL = form.findField('NULL_FL');
			
		if (CRIT_TX.value != CRIT_TX.originalValue || 
			SUBTLE_CRIT_TX.value != SUBTLE_CRIT_TX.originalValue || 
			NULL_FL.value != NULL_FL.originalValue) {
			return 'Вы вносите изменения в действующие условия выявления.<br>Вы уверены?';			
		}
		else {
			return null;
		}
	},
	/**
	 * Копируем действующие условия в тестовые
	 * @param button
	 * @param eOpts
	 */
	onClickCopyActiveToTest: function (button, eOpts) {
		var criteria = this.getViewModel().get('record');
		AML.app.msg.confirm({
			type: 'warning',
			message: 'Вы копируете действующие условия в тестовые. В результате текущие тестовые условия будут заменены.<br>Вы уверены?',
			fnYes: function () {
				criteria.set('TEST_CRIT_TX', criteria.get('CRIT_TX'));
				criteria.set('TEST_SUBTLE_CRIT_TX', criteria.get('SUBTLE_CRIT_TX'));
				criteria.set('TEST_NULL_FL', criteria.get('NULL_FL'));
				button.up('tabpanel').setActiveTab(1);
			}
		});
	},
	/**
	 * Переносим тестовые условия в действующие
	 * @param button
	 * @param eOpts
	 */
	onClickMoveTestToActive: function (button, eOpts) {
		var criteria = this.getViewModel().get('record');

		var accessObj = "OPOKCriteria";
		var storeAccess = Ext.getStore('ObjectsAccess');
		if (!storeAccess) return;

		var i = storeAccess.findExact('id', accessObj);

		// если доступ на тестирование
		if (storeAccess.getAt(i).get('mode') == 'T') {
			Ext.Msg.alert('Внимание!','У Вас недостаточно прав для выполнения данного действия!');
			return false;
		}
		AML.app.msg.confirm({
			type: 'warning',
			message: 'Вы делаете тестовые условия действующими. В результате текущие действующие условия будут заменены.<br>Вы уверены?',
			fnYes: function () {
				criteria.set('CRIT_TX', criteria.get('TEST_CRIT_TX'));
				criteria.set('SUBTLE_CRIT_TX', criteria.get('TEST_SUBTLE_CRIT_TX'));
				criteria.set('NULL_FL', criteria.get('TEST_NULL_FL'));
				button.up('tabpanel').setActiveTab(0);
				criteria.set('TEST_CRIT_TX', null);
				criteria.set('TEST_SUBTLE_CRIT_TX', null);
				criteria.set('TEST_NULL_FL', null);
			}
		});
	}//,
	/**
	 * Метод для события отображения формы
	 * @param window
	 * @param eOpts
	 *
	onShowForm: function (window, eOpts) {
		// todo: наверное этот метод не пригодится...
	},*/
	/**
	 * Метод для события закрытия формы
	 * @param window
	 * @param eOpts
	 *
	onCloseForm: function (window, eOpts) {
		var opokCriteria = Ext.StoreManager.lookup('OPOKCriteria');
		if (opokCriteria.isChanges()) {
			opokCriteria.rejectChanges();
		}
	},*/
	/**
	 * Сохраняем форму
	 * @param button
	 * @param eOpts
	 */
	/*onClickSaveForm: function (button, eOpts) {
		var win = this.getView(),
			form = win.down('form').getForm(),
			opokCriteria = Ext.StoreManager.lookup('OPOKCriteria'),
			opokCriteriaChange = (win.getViewModel().getData().record.dirty || win.getViewModel().getData().record.phantom);
		if (opokCriteriaChange) {
			if (form.isValid()) {
				var CRIT_TX = form.findField('CRIT_TX'),
					SUBTLE_CRIT_TX = form.findField('SUBTLE_CRIT_TX'),
					NULL_FL = form.findField('NULL_FL');
				if (
					CRIT_TX.value != CRIT_TX.originalValue
					|| SUBTLE_CRIT_TX.value != SUBTLE_CRIT_TX.originalValue
					|| NULL_FL.value != NULL_FL.originalValue
				) {
					AML.app.msg.confirm({
						type: 'warning',
						message: 'Вы вносите изменения в действующие условия.<br>Вы уверены?',
						fnYes: function () {
							opokCriteria.sync({
								success: function () {
									win.close();
								},
								failure: opokCriteria.syncFailure
							});
						}
					});
				} else {
					opokCriteria.sync({
						success: function () {
							win.close();
						},
						failure: opokCriteria.syncFailure
					});
				}
			}
		} else{
			win.close();
		}
	},*/
	/**
	 * Отменяем изменения в форме и закрываем её
	 * @param button
	 * @param eOpts
	 */
	/*onClickCancelForm: function (button, eOpts) {
		var win = this.getView(),
			change = win.getViewModel().getData().record.store.isChanges();
		if (change) {
			AML.app.msg.confirm({
				type: 'warning',
				message: 'Все изменения будут потеряны!<br>Вы уверены что хотите закрыть форму?',
				fnYes: function () {
					win.close();
				}
			});
		} else {
			win.close();
		}
	}*/
});
