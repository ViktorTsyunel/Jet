Ext.define('AML.view.settings.OPOK', {
	extend: 'Ext.grid.Panel',
	requires: [
		'AML.view.settings.OPOKController',
		'Ext.form.field.Text',
		'Ext.grid.column.Date',
		'Ext.grid.column.Number',
		'Ext.toolbar.Fill',
		'Ext.toolbar.Separator'
	],
	xtype: 'app-settings-opok',
	controller: 'opok',
    viewModel: {
        type: 'opokvmdl'
    },
    bind: {
        title: '{title}'
    },
    store: {type: 'opokgrid'},
    stateful: true,
	stateId: 'opok',
	tools: [
		{
			type: 'refresh',
			callback: 'onStoreRefresh'
		}
	],
	columns: [
		{
			text: 'Код ОПОК',
			stateId: 'OPOK_NB',
			dataIndex: 'OPOK_NB',
			flex: 0.5,
			editor: {
				allowBlank: false
			}
		},
		{
			text: 'Наименование',
			stateId: 'OPOK_NM',
			dataIndex: 'OPOK_NM',
			flex: 2,
			editor: {
				allowBlank: false
			}
		},
		{
			text: 'Приоритет',
			stateId: 'PRIORITY_NB',
			dataIndex: 'PRIORITY_NB',
			flex: 0.5,
			editor: {
				allowBlank: true
			}
		},
		{
			text: 'Пороговая сумма',
			stateId: 'LIMIT_AM',
			dataIndex: 'LIMIT_AM',
			flex: 1,
			align: 'right',
			xtype: 'numbercolumn',
			format: '0.00',
			editor: {
				allowBlank: true
			}
		},
		{
			text: 'Начало действия',
			stateId: 'START_DT',
			dataIndex: 'START_DT',
			flex: 1,
			xtype: 'datecolumn',
			format: 'd.m.Y',
			editor: {
				allowBlank: true
			}
		},
		{
			text: 'Номер для сортировки',
			stateId: 'ORDER_NB',
			dataIndex: 'ORDER_NB',
			flex: 1,
			editor: {
				allowBlank: true
			}
		},
		{
			text: 'Групповой код выявления',
			stateId: 'GROUP_NB',
			dataIndex: 'GROUP_NB',
			flex: 1,
			editor: {
				allowBlank: true
			}
		},
		{
			text: 'Участники в ОЭС',
			stateId: 'OES_PARTIES_TX',
			dataIndex: 'OES_PARTIES_TX',
			flex: 1,
			editor: {
				allowBlank: true
			}
		}
	],
	tbar: [
		{
			itemId: 'create',
			text: 'Добавить',
			iconCls: 'icon-plus'
		},
		{
			itemId: 'remove',
			text: 'Удалить',
			iconCls: 'icon-minus',
			disabled: true
		},
		'-',
		{
			itemId: 'filterOPOK_NB',
			xtype: 'textfield',
			fieldLabel: 'Поиск',
			tooltip: {
				title: 'По коду ОПОК',
				text: 'Код операции, подлежащей обязательному контролю в соответствии с перечнем РФМ: 1001-1008, 3001, 3011, 3021, 4001-4007, 5001-5007, 6001, 7001, 8001, 9001'
			},
			labelWidth: 40,
			labelAlign: 'right',
			emptyText: 'По коду ОПОК',
			listeners: {
				change: 'onChangeFilterOPOK_NB'
			},
			triggerClear: true,
			flex: 0.5
		},
		{
			itemId: 'filterOPOK_NM',
			xtype: 'textfield',
			tooltip: {
				title: 'По наименованию',
				text: 'Наименование кода ОПОК'
			},
			emptyText: 'По наименованию',
			listeners: {
				change: 'onChangeFilterOPOK_NM'
			},
			triggerClear: true,
			flex: 0.5
		},
		'->'
	]
});
