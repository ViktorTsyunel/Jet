Ext.define('AML.view.settings.OPOKTest', {
	extend: 'Ext.window.Window',
	requires: [
		'AML.ux.form.field.Rule',
		'AML.view.settings.OPOKTestController',
		'AML.view.settings.OPOKTestViewModel',
		'Ext.Component',
		'Ext.container.ButtonGroup',
		'Ext.container.Container',
		'Ext.data.Store',
		'Ext.form.FieldContainer',
		'Ext.form.Panel',
		'Ext.form.field.ComboBox',
		'Ext.form.field.Date',
		'Ext.grid.Panel',
		'Ext.grid.column.Date',
		'Ext.grid.column.Number',
		'Ext.layout.container.Anchor',
		'Ext.layout.container.Border',
		'Ext.layout.container.Fit',
		'Ext.layout.container.HBox',
		'Ext.layout.container.Table',
		'Ext.layout.container.VBox',
		'Ext.panel.Panel',
		'Ext.tab.Panel'
	],
	xtype: 'app-settings-opoktest',
	controller: 'opoktest',
	iconCls: 'icon-report-arrow',
	viewModel: {
		type: 'opoktestviewmodel'
	},
	bind: {
		title: '{title}'
	},
	closable: false,
	tools: [
		{
			type: 'close',
			callback: 'onClickCancelForm'
		}
	],
	width: (Ext.isIE8 ? 1280 : '95%'),
	height: (Ext.isIE8 ? 600 : '95%'),
	bodyStyle: {
		padding: 0
	},
	items: [
		{
			xtype: 'panel',
			layout: 'border',
			border: false,
			items: [
				{
					xtype: 'gridpanel',
					region: 'center',
					selModel: {
						mode: 'SINGLE'
					},
					store: 'OPOKTestGrid',
					border: false,
					frame: false,
					stateful: true,
					stateId: 'opoktest',
					columns: [
						{
							text: 'ID операции',
							dataIndex: 'TRXN_SEQ_ID',
							stateId: 'TRXN_SEQ_ID',
							width: 80,
							align: 'center'
						},
						{
							text: 'Нал/Бнал',
							dataIndex: 'OP_CAT_CD',
							stateId: 'OP_CAT_CD',
							width: 50,
							align: 'center'
						},
						{
							text: 'Виды оп.',
							dataIndex: 'OPOK_LIST',
							stateId: 'OPOK_LIST',
							width: 40,
							align: 'center'
						},
						{
							text: 'Дата операции',
							dataIndex: 'TRXN_DT',
							stateId: 'TRXN_DT',
							width: 80,
							align: 'center',
							xtype: 'datecolumn',
							format: 'd.m.Y'
						},
						{
							text: 'Сумма в рублях',
							dataIndex: 'TRXN_BASE_AM',
							stateId: 'TRXN_BASE_AM',
							width: 95,
							align: 'right',
							xtype: 'numbercolumn',
							format: '0.00'
						},
						{
							text: 'Сумма в валюте',
							dataIndex: 'TRXN_CRNCY_AM',
							stateId: 'TRXN_CRNCY_AM',
							width: 95,
							align: 'right',
							xtype: 'numbercolumn',
							format: '0.00'
						},
						{
							text: 'Валюта',
							dataIndex: 'TRXN_CRNCY_CD',
							stateId: 'TRXN_CRNCY_CD',
							width: 35,
							align: 'center'
						},
						{
							text: 'Назначение платежа',
							dataIndex: 'TRXN_DESC',
							stateId: 'TRXN_DESC',
							width: 300,
							align: 'left'
						},
						{
							text: 'Наименование плательщика',
							dataIndex: 'ORIG_NM',
							stateId: 'ORIG_NM',
							width: 300,
							align: 'left'
						},
						{
							text: 'ИНН плательщика',
							dataIndex: 'ORIG_INN_NB',
							stateId: 'ORIG_INN_NB',
							width: 80,
							align: 'center'
						},
						{
							text: 'Счет плательщика',
							dataIndex: 'ORIG_ACCT_NB',
							stateId: 'ORIG_ACCT_NB',
							width: 110,
							align: 'left'
						},
						{
							text: 'Клиент? (плательщик)',
							dataIndex: 'ORIG_OWN_FL',
							stateId: 'ORIG_OWN_FL',
							width: 40,
							align: 'center',
							renderer: function (value) {
								return (value === true ? 'Да' : '');
							}
						},
						{
							text: 'Счет дебета ',
							dataIndex: 'DEBIT_CD',
							stateId: 'DEBIT_CD',
							width: 110,
							align: 'left'
						},
						{
							text: 'Банк плательщика',
							dataIndex: 'SEND_INSTN_NM',
							stateId: 'SEND_INSTN_NM',
							width: 150,
							align: 'left'
						},
						{
							text: 'БИК банка плательщика',
							dataIndex: 'SEND_INSTN_ID',
							stateId: 'SEND_INSTN_ID',
							width: 80,
							align: 'center'
						},
						{
							text: 'Корр. счет банка плательщика',
							dataIndex: 'SEND_INSTN_ACCT_ID',
							stateId: 'SEND_INSTN_ACCT_ID',
							width: 110,
							align: 'left'
						},
						{
							text: 'Получатель',
							dataIndex: 'BENEF_NM',
							stateId: 'BENEF_NM',
							width: 300,
							align: 'left'
						},
						{
							text: 'ИНН получателя',
							dataIndex: 'BENEF_INN_NB',
							stateId: 'BENEF_INN_NB',
							width: 80,
							align: 'center'
						},
						{
							text: 'Счет получателя',
							dataIndex: 'BENEF_ACCT_NB',
							stateId: 'BENEF_ACCT_NB',
							width: 110,
							align: 'left'
						},
						{
							text: 'Клиент? (получатель)',
							dataIndex: 'BENEF_OWN_FL',
							stateId: 'BENEF_OWN_FL',
							width: 40,
							align: 'center',
							renderer: function (value) {
								return (value === true ? 'Да' : '');
							}
						},
						{
							text: 'Счет кредита',
							dataIndex: 'CREDIT_CD',
							stateId: 'CREDIT_CD',
							width: 110,
							align: 'left'
						},
						{
							text: 'Банк получателя',
							dataIndex: 'RCV_INSTN_NM',
							stateId: 'RCV_INSTN_NM',
							width: 150,
							align: 'left'
						},
						{
							text: 'БИК банка получателя',
							dataIndex: 'RCV_INSTN_ID',
							stateId: 'RCV_INSTN_ID',
							width: 80,
							align: 'center'
						},
						{
							text: 'Корр. счет банка получателя',
							dataIndex: 'RCV_INSTN_ACCT_ID',
							stateId: 'RCV_INSTN_ACCT_ID',
							width: 110,
							align: 'left'
						},
						{
							text: 'Причина выявления',
							dataIndex: 'EXPLANATION_TX',
							stateId: 'EXPLANATION_TX',
							width: 200,
							align: 'left'
						},
						{
							text: 'Вид операции (ЦАС НСИ)',
							dataIndex: 'NSI_OP',
							stateId: 'NSI_OP',
							width: 200,
							align: 'left'
						},
						{
							text: 'Номинал ценн. бумаги',
							dataIndex: 'SCRTY_BASE_AM',
							stateId: 'SCRTY_BASE_AM',
							width: 95,
							align: 'right',
							xtype: 'numbercolumn',
							format: '0.00'
						},
						{
							text: 'Номер ценной бумаги',
							dataIndex: 'SCRTY_ID',
							stateId: 'SCRTY_ID',
							width: 95,
							align: 'center'
						},
						{
							text: 'Вид ценн. бумаги',
							dataIndex: 'SCRTY_TYPE_CD',
							stateId: 'SCRTY_TYPE_CD',
							width: 40,
							align: 'center'
						},
						{
							text: 'ID ценн. бумаги',
							dataIndex: 'TRXN_SCRTY_SEQ_ID',
							stateId: 'TRXN_SCRTY_SEQ_ID',
							width: 40,
							align: 'center'
						},
						{
							text: '№ТБ',
							dataIndex: 'TB_ID',
							stateId: 'TB_ID',
							width: 40,
							align: 'center'
						},
						{
							text: '№ОСБ',
							dataIndex: 'OSB_ID',
							stateId: 'OSB_ID',
							width: 45,
							align: 'center'
						},
						{
							text: 'Дата загрузки ',
							dataIndex: 'DATA_DUMP_DT',
							stateId: 'DATA_DUMP_DT',
							width: 80,
							align: 'center',
							xtype: 'datecolumn',
							format: 'd.m.Y'
						},
						{
							text: 'Система-источник',
							dataIndex: 'SRC_SYS_CD',
							stateId: 'SRC_SYS_CD',
							width: 40,
							align: 'center'
						},
						{
							text: 'Дата изменения в источнике',
							dataIndex: 'SRC_CHANGE_DT',
							stateId: 'SRC_CHANGE_DT',
							width: 80,
							align: 'center',
							xtype: 'datecolumn',
							format: 'd.m.Y'
						},
						{
							text: 'Пользователь',
							dataIndex: 'SRC_USER_NM',
							stateId: 'SRC_USER_NM',
							width: 80,
							align: 'left'
						},
						{
							text: 'Код в системе источнике',
							dataIndex: 'SRC_CD',
							stateId: 'SRC_CD',
							width: 110,
							align: 'left'
						}
					],
					tbar: [
						{
							xtype: 'buttongroup',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							height: 100,
							style: {
								background: '#eee',
								margin: '0px 2px 0px 0px',
								padding: '7px 7px 2px 7px'
							},
							items: [
								{
									xtype: 'form',
									layout: {
										type: 'hbox',
										align: 'stretch'
									},
									bodyStyle: {
										background: '#eee'
									},
									flex: 1,
									frame: false,
									border: false,
									items: [
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'anchor'
											},
											width: 300,
											background: '#fff',
											border: true,
											items: [
												{
													xtype: 'fieldcontainer',
													layout: {
														type: 'vbox',
														align: 'stretch'
													},
													padding: '2px 0px 0px 0px',
													items: [
														{
															xtype: 'datefield',
															fieldLabel: 'Дата с',
															name: 'dtFrom',
															flex: 1,
															labelWidth: 50,
															labelAlign: 'right',
															value: new Date(),
															format: 'd.m.Y',
															startDay: 1,
															allowBlank: false,
															listeners: {
																scope: this,
																change: function (field, newValue, oldValue, eOpts) {
																	var toDate = field.up('fieldcontainer').down('datefield[name=dtTo]');
																	toDate.setMinValue(newValue);
																	if (Ext.Date.diff(newValue, toDate.getValue(), Ext.Date.DAY) < 0) {
																		toDate.setValue(newValue);
																	}
																}
															}
														},
														{
															xtype: 'datefield',
															fieldLabel: 'по',
															name: 'dtTo',
															flex: 1,
															labelWidth: 50,
															labelAlign: 'right',
															value: new Date(),
															format: 'd.m.Y',
															startDay: 1,
															allowBlank: false,
															minValue: new Date()
														},
														{
															name: 'tbId',
															xtype: 'combobox',
															store: 'TB',
															editable: false,
															queryMode: 'local',
															tpl: Ext.create('Ext.XTemplate',
																'<tpl for=".">',
																'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
																'<tpl if="id &gt; 0">{id} - {label}' +
																'<tpl else>{label}</tpl>' +
																'</div>',
																'</tpl>'
															),
															displayField: 'label',
															valueField: 'id',
															fieldLabel: 'Тербанк',
															labelWidth: 50,
															labelAlign: 'right',
															allowBlank: false,
															value: 0,
															flex: 1
														}
													]
												}
											]
										},
										{
											xtype: 'container',
											width: 5
										},
										{
											xtype: 'fieldcontainer',
											layout: {
												type: 'anchor'
											},
											flex: 1,
											background: '#fff',
											border: true,
											items: [
												{
													xtype: 'tabpanel',
													plain: true,
													bodyPadding: 10,
													bodyStyle: {
														background: '#eee'
													},
													activeTab: 0,
													minTabWidth: 150,
													flex: 1,
													defaults: {
														bodyStyle: {
															background: '#eee'
														}
													},
													listeners: {
														afterrender: function (panel) {
															var bar = panel.tabBar;
															bar.insert(2, [
																{
																	xtype: 'component',
																	isTabCount: true,
																	hidden: true,
																	cls: 'app-opoktest-tabcount',
																	bind: {
																		html: '<div class="text">Кол-во выявленных операций: <b>{count.total}</b>, загружено: <b>{count.load}</b></div>'
																	},
																	flex: 1
																}
															]);
														}
													},
													items: [
														{
															title: 'Выявление по правилу',
															isRule: true,
															layout: {
																type: 'vbox',
																align: 'stretch'
															},
															items: [
																{
																	xtype: 'fieldcontainer',
																	layout: {
																		type: 'hbox',
																		align: 'stretch'
																	},
																	defaults: {
																		padding: '1px 5px 1px 5px'
																	},
																	items: [
																		{
																			name: 'ruleId',
																			xtype: 'rulefield',
																			store: 'OPOKRuleField',
																			triggerClear: true,
																			emptyText: 'Выберите правило',
																			flex: 1
																		}
																	]
																}
															]
														},
														{
															title: 'Выявление по параметрам',
															isRule: false,
															layout: {
																type: 'vbox',
																align: 'stretch'
															},
															items: [
																{
																	xtype: 'fieldcontainer',
																	layout: {
																		type: 'hbox',
																		align: 'stretch'
																	},
																	defaults: {
																		padding: '1px 5px 1px 5px'
																	},
																	items: [
																		{
																			name: 'opok',
																			xtype: 'combobox',
																			store: 'OPOKGrid',
																			editable: false,
																			queryMode: 'local',
																			tpl: Ext.create('Ext.XTemplate',
																				'<tpl for=".">',
																				'<div class="x-boundlist-item" style="overflow: hidden; '+ ((Ext.browser.is.IE && parseInt(Ext.browser.version.version, 10)<=8) ? 'width: 800px;' : 'max-width: 800px;') +' white-space: nowrap; text-overflow: ellipsis;">{OPOK_NB} - {OPOK_NM}</div>',
																				'</tpl>'
																			),
																			matchFieldWidth: false,
																			displayField: 'OPOK_NB',
																			valueField: 'OPOK_NB',
																			emptyText: 'Код ОПОК',
																			tooltip: 'Код ОПОК',
																			labelAlign: 'right',
																			triggerClear: true,
																			flex: 1
																		},
																		{
																			name: 'ruleQuery',
																			xtype: 'combobox',
																			store: 'OPOKRuleQuery',
																			editable: false,
																			queryMode: 'local',
																			tpl: Ext.create('Ext.XTemplate',
																				'<tpl for=".">',
																				'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
																				'</tpl>'
																			),
																			displayField: 'label',
																			valueField: 'id',
																			emptyText: 'Запрос',
																			tooltip: 'Вид запроса',
																			labelAlign: 'right',
																			triggerClear: true,
																			flex: 1
																		},
																		{
																			name: 'ruleStatus',
																			xtype: 'combobox',
																			store: Ext.create('Ext.data.Store', {
																				fields: [
																					'id',
																					'label'
																				],
																				data: [
																					{
																						"id": 'ACT',
																						"label": 'Действующее'
																					},
																					{
																						"id": 'TEST',
																						"label": 'В тестировании'
																					}
																				]
																			}),
																			editable: false,
																			queryMode: 'local',
																			tpl: Ext.create('Ext.XTemplate',
																				'<tpl for=".">',
																				'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
																				'</tpl>'
																			),
																			displayField: 'label',
																			valueField: 'id',
																			emptyText: 'Статус правила',
																			tooltip: 'Статус правила выявления',
																			labelAlign: 'right',
																			value: 'ACT',
																			flex: 1
																		},
																		{
																			name: 'criteriaStatus',
																			xtype: 'combobox',
																			store: Ext.create('Ext.data.Store', {
																				fields: [
																					'id',
																					'label'
																				],
																				data: [
																					{
																						"id": 'ACT',
																						"label": 'Действующее'
																					},
																					{
																						"id": 'TEST',
																						"label": 'В тестировании'
																					}
																				]
																			}),
																			editable: false,
																			queryMode: 'local',
																			tpl: Ext.create('Ext.XTemplate',
																				'<tpl for=".">',
																				'<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{label}</div>',
																				'</tpl>'
																			),
																			displayField: 'label',
																			valueField: 'id',
																			emptyText: 'Статус ст. усл.',
																			tooltip: 'Статус стандартного условия',
																			labelAlign: 'right',
																			value: 'ACT',
																			flex: 1
																		}
																	]
																}
															]
														}
													]
												}
											]
										}
									]
								}
							],
							flex: 1
						},
						{
							xtype: 'buttongroup',
							layout: 'fit',
							height: 100,
							items: [
								{
									scale: 'large',
									width: 100,
									iconCls: 'icon-search',
									listeners: {
										click: 'onClickSearch'
									}
								}
							]
						}
					]
				},
				{
					xtype: 'panel',
					isFooter: true,
					region: 'south',
					height: '30%',
					minHeight: 130,
					maxHeight: 330,
					collapsed: true,
					collapsible: true,
					collapseMode: 'mini',
					header: false,
					cls: 'app-opoktest-footer',
					split: true,
					layout: {
						type: 'table',
						columns: 4
					},
					bodyStyle: {
						padding: '5px'
					},
					defaults: {
						bodyStyle: {
							padding: '3px',
							border: 0
						}
					},
					scrollable: true,
					html: '<div class="emptyContainer"><div class="emptyText">Нет данных для отображения</div></div>',
					bind: {
						html:
							'<table class="table">' +
							'	<tr>' +
							'		<td class="label">Причина выявления:</td>' +
							'		<td class="valueCol3" colspan="3">{footerRecord.EXPLANATION_TX}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="hr" colspan="4">' +
							'			<hr>' +
							'		</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Назначение платежа:</td>' +
							'		<td class="valueCol3" colspan="3">{coloredTRXN_DESC}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Код операции:</td>' +
							'		<td class="valueCol3" colspan="3">--</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="hr" colspan="4">' +
							'			<hr>' +
							'		</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="labelCol2" colspan="2">Плательщик</td>' +
							'		<td class="labelCol2" colspan="2">Получатель</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Наименование:</td>' +
							'		<td class="value">{coloredORIG_NM}</td>' +
							'		<td class="label">Наименование:</td>' +
							'		<td class="value">{coloredBENEF_NM}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">ИНН:</td>' +
							'		<td class="value">{footerRecord.ORIG_INN_NB}</td>' +
							'		<td class="label">ИНН:</td>' +
							'		<td class="value">{footerRecord.BENEF_INN_NB}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Счет:</td>' +
							'		<td class="value">{footerRecord.ORIG_ACCT_NB}</td>' +
							'		<td class="label">Счет:</td>' +
							'		<td class="value">{footerRecord.BENEF_ACCT_NB}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Банк:</td>' +
							'		<td class="value">{footerRecord.SEND_INSTN_NM}</td>' +
							'		<td class="label">Банк:</td>' +
							'		<td class="value">{footerRecord.RCV_INSTN_NM}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Корр. счет:</td>' +
							'		<td class="value">{footerRecord.SEND_INSTN_ACCT_ID}</td>' +
							'		<td class="label">Корр. счет:</td>' +
							'		<td class="value">{footerRecord.RCV_INSTN_ACCT_ID}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Счет дебета:</td>' +
							'		<td class="value">{footerRecord.DEBIT_CD}</td>' +
							'		<td class="label">Счет кредита:</td>' +
							'		<td class="value">{footerRecord.CREDIT_CD}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Адрес:</td>' +
							'		<td class="value">--</td>' +
							'		<td class="label">Адрес:</td>' +
							'		<td class="value">--</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Удостоверяющий документ:</td>' +
							'		<td class="value">--</td>' +
							'		<td class="label">Удостоверяющий документ:</td>' +
							'		<td class="value">--</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="hr" colspan="4">' +
							'			<hr>' +
							'		</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Номер филиала:</td>' +
							'		<td class="value">{footerRecord.TB_ID}</td>' +
							'		<td class="label">Номер отделения:</td>' +
							'		<td class="value">{footerRecord.OSB_ID}</td>' +
							'	</tr>' +
							'	<tr>' +
							'		<td class="label">Наименование филиала:</td>' +
							'		<td class="value">--</td>' +
							'		<td class="label">Наименование отделения:</td>' +
							'		<td class="value">--</td>' +
							'	</tr>' +
							'</table>'
					}
				}
			]
		}
	]
});