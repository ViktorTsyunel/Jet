Ext.define('AML.view.oes.OES321PartyOrg.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-oes-oes321partyorg',
    controller: 'oes321partyorg',
    viewModel: {
        type: 'oes321partyorgvmdl'
    },
    bind: {
        store: '{oes321partyorg}',
        title: '{title}'
    },
    stateful: true,
    stateId: 'oes321partyorg',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    reference: 'viewOes321partyorg',
    columns: [
        {
            text: 'ID записи',
            identifier: true,
            stateId: 'OES_321_P_ORG_ID',
            dataIndex: 'OES_321_P_ORG_ID',
            flex: 0.5
        },
        {
            text: 'Код ТБ',
            stateId: 'TB_CODE',
            dataIndex: 'TB_CODE',
            flex: 0.5
        },
        {
            text: 'Код ОСБ',
            stateId: 'OSB_CODE',
            dataIndex: 'OSB_CODE',
            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.data.OSB_CODE === 0) {
                    return "";
                } else {
                    return record.data.OSB_CODE;
                }
            },
            flex: 0.5
        },
        {
            text: 'Участник',
            stateId: 'NAMEU',
            dataIndex: 'NAMEU',
            flex: 2
        },
        {
            text: 'ОКПО',
            stateId: 'SD',
            dataIndex: 'SD',
            flex: 0.5
        },
        {
            text: 'Рег.номер',
            stateId: 'RG',
            dataIndex: 'RG',
            flex: 0.5
        },
        {
            text: 'ИНН',
            stateId: 'ND',
            dataIndex: 'ND',
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Активно?',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Начало действия',
            stateId: 'START_DT',
            dataIndex: 'START_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Окончание действия',
            stateId: 'END_DT',
            dataIndex: 'END_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто создал',
            stateId: 'CREATED_BY',
            dataIndex: 'CREATED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда создал',
            stateId: 'CREATED_DATE',
            dataIndex: 'CREATED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто изменил',
            stateId: 'MODIFIED_BY',
            dataIndex: 'MODIFIED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда изменил',
            stateId: 'MODIFIED_DATE',
            dataIndex: 'MODIFIED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        },
        '-',
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            flex: 0.5,
            checked: true,
            value: true,
            bind: {
                value: '{activeCB}'
            }
        }
    ]
});
