Ext.define('AML.view.oes.OES321PartyOrg.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321partyorgformvm',
    data: {
        title: 'КО участники операций',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    stores: {
        // поле Признак владельца банковской карты
        CARDBHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'без БК'},
                {id: 1, text: 'клиент Банка'},
                {id: 2, text: 'не клиент Банка'},
                {id: 3, text: 'иное'}
            ]
        },
        // поле Признак участника
        PRHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'участник является ФЛ или ЮЛ, имеющим регистрацию или место жительства в государстве, не участвующем в международном сотрудничестве в сфере ПОД/ФТ'},
                {id: 1, text: 'участник является ФЛ или ЮЛ, имеющим место нахождения в государстве, не участвующем в международном сотрудничестве в сфере ПОД/ФТ'},
                {id: 2, text: 'если участник операции является лицом, владеющим счетом в банке, зарегистрированном в государстве, не участвующем в международном сотрудничестве в сфере ПОД/ФТ'}
            ]
        },
        // поле Признак выгодоприобретателя
        VPHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'нет выгодоприобретателя'},
                {id: 1, text: 'идентификация завершена'},
                {id: 2, text: 'идентификация не завершена'}
            ]
        },
        // поле Тип сообщения
        ACTIONHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 1, text: 'добавление новой записи'},
                {id: 2, text: 'исправление записи'},
                {id: 3, text: 'запрос замены записи'},
                {id: 4, text: 'запрос удаления записи'}
            ]
        },
        // поле Тип участника для Вкладок плательщик, представитель плательщика, представитель получателя, получатель, от имени и по поручению
        TUHardStore: {
            fields: ['id', 'text'],
            data: [
                {id: 0, text: 'участник отсутствует'},
                {id: 1, text: 'ЮЛ'},
                {id: 2, text: 'ФЛ'},
                {id: 3, text: 'ИП'},
                {id: 4, text: 'Участник не определен'}
            ]
        },
        // Код по OKATO AMR_S
        OKATOStoreAmrS: {
            fields: [{
                name: 'OKATO_CD',
                type: 'string'
            }],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'REGION_REF',
                    action: 'read'
                }
            },
            listeners: {
                load: 'rebuildOKATOStore'
            }
        },
        // Код по OKATO ADRESS_S
        OKATOStoreAdressS: {
            fields: [{
                name: 'OKATO_CD',
                type: 'string'
            }],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'REGION_REF',
                    action: 'read'
                }
            },
            listeners: {
                load: 'rebuildOKATOStore'
            }
        },
        // Справочник типов ДУЛ
        DOCTYPEStore: {
            fields: [],
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: {
                property: 'DOC_TYPE_CD',
                direction: 'ASC'
            },
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: common.globalinit.proxyType.ajax,
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                extraParams: {
                    form: 'DOCTYPE_REF',
                    action: 'read'
                }
            }
        }
    }
});
