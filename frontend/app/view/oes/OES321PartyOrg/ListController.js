Ext.define('AML.view.oes.OES321PartyOrg.ListController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oes321partyorg',
    childForm: 'app-oes-oes321partyorg-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'OES_321_P_ORG_ID', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'OES321PartyOrg' //код объекта доступа для данной формы
});
