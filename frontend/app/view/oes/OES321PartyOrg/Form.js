Ext.define('AML.view.oes.OES321PartyOrg.Form', {
    extend: 'Ext.window.Window',
    xtype: 'app-oes-oes321partyorg-form',
    controller: 'oes321partyorgform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oes321partyorgformvm'
    },
    width: Ext.getBody().getViewSize().width*0.8,
    height:Ext.getBody().getViewSize().height,
    bind: {
        title: '{title}'
    },
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    listeners: {
        afterrender: function (form) {
            form.focus();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            autoHeight:true,
            autoScroll:true,
            items: [
                {
                    xtype: 'fieldset',
                    title: '<b>Общие сведения об участнике</b>',
                    border: 1,
                    padding: 10,
                    margin: '10px 5px 0 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'TB',
                                    name: 'TB_CODE',
                                    editable: false,
                                    queryMode: 'local',
                                    labelWidth: 160,
                                    matchFieldWidth: false,
                                    listConfig: {
                                      width: 320
                                    },
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="id &gt; 0">{id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Код тер.банка (TB_CODE)',
                                    displayField: 'id',
                                    valueField: 'id',
                                    triggerClear: true,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.TB_CODE}'
                                    },
                                    listeners: {
                                        scope: this,
                                        change: function (field, newValue, oldValue, eOpts) {
                                            var tbosboesField = field.up('window').down('combobox[name=OSB_CODE]'),
                                                tbosbStore = tbosboesField.getStore(),
                                                tbCode = parseInt(field.up('window').getViewModel().data.record.get('TB_CODE'), 10),
                                                tb = Ext.data.StoreManager.lookup('TB'),
                                                record = tb.findRecord('id', newValue);
                                            if (record && (newValue != tbCode)) {
                                                field.up('window').getViewModel().set('selectedTBName', record.get('TB_NAME'))
                                            }
                                            tbosbStore.clearFilter();
                                            if (newValue != tbCode) {
                                                tbosboesField.clearValue();
                                            }
                                            tbosbStore.filterBy(function (record) {
                                                return record.get("tb_id") === newValue;
                                            }, this);
                                        }
                                    }
                                    // listeners: {
                                    //     change: function (combo, newValue, oldValue) {
                                    //         if (!oldValue || newValue == oldValue) { return; }
                                    //         var vm = combo.lookupViewModel();
                                    //         var selection = vm.get('record.OSB_SELECTION');
                                    //         if (!selection || selection.get('tb_id')!=newValue) {
                                    //             vm.set('record.OSB_SELECTION', null);
                                    //         }
                                    //     }
                                    // }
                                },
                                {flex: 0.1},
                                {
                                    xtype: 'combobox',
                                    // store: {
                                    //     type: 'tbosb'
                                    // },
                                    store: 'TBOSB',
                                    name: 'OSB_CODE',
                                    editable: true,
                                    queryMode: 'local',
                                    labelWidth: 160,
                                    matchFieldWidth: false,
                                    listConfig: {
                                        width: 320
                                    },
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="osb_id &gt; 0">{osb_id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Код ОСБ (OSB_CODE)',
                                    displayField: 'osb_id',
                                    valueField: 'osb_id',
                                    triggerClear: true,
                                    allowBlank: true,
                                    bind: {
                                        // selection: '{record.OSB_SELECTION}',
                                        value: '{record.OSB_CODE}'
                                    },
                                    listeners: {
                                        scope: this,
                                        change: function (tbField, newValue, oldValue, eOpts) {
                                            var tbStore = tbField.getStore(),
                                                tbTBField = tbField.up('window').down('combobox[name=TB_CODE]'),
                                                tbCode = parseInt(tbTBField.getValue(), 10),
                                                record = tbStore.findRecord('id', (tbCode + "-" + newValue));
                                            if (record && (newValue != tbCode)) {
                                                tbField.up('window').getViewModel().set('selectedTBName', record.get('label'))
                                            }
                                            tbStore.filterBy(function (record) {
                                                return record.get("tb_id") === tbTBField.value;
                                            }, this);
                                        }
                                    }
                                    // listeners: {
                                    //     'expand' : function(combo) {
                                    //         var store = combo.getStore();
                                    //         var value = combo.lookupViewModel().get('record.TB_CODE');
                                    //         store.clearFilter(true);
                                    //         store.setFilters([{property:'tb_id', value:value}]);
                                    //     }
                                    // }
                                },
                                {flex: 0.1}
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    allowBlank: false,
                                    width: 255,
                                    labelWidth: 160,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "Тип участника операции<br>В случае если участник находится на обслуживании в Банке (филиале Банка), представляющем сведения, или участником является Банк (филиал Банка), представляющий сведения (B_PAYER <>0 (Признак банка плательщика)):<br>•	ЮЛ, филиал ЮЛ (Значение для списка: 1 - ЮЛ);<br>•	ФЛ (Значение для списка: 2 - ФЛ);<br>•	ИП (Значение для списка: 3 – ИП).<br>В ином случае (B_PAYER = '0'):<br>•	ЮЛ, филиал ЮЛ (Значение для списка: 1 - ЮЛ);<br>•	ФЛ (Значение для списка: 2 - ФЛ);<br>•	ИП (Значение для списка: 3 - ИП);<br>•	Невозможно установить тип участника операции (Значение для списка: 4 – не определен)."
                                    },
                                    editable: false,
                                    queryMode: 'local',
                                    forceSelection: true,
                                    multiSelect: false,
                                    matchFieldWidth: false,
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{id}',
                                        '</tpl>'
                                    ],
                                    displayField: 'text',
                                    fieldLabel: 'Тип участника (TU)',
                                    valueField: 'id',
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        store: '{TUHardStore}',
                                        value: '{record.TU}'
                                    }
                                },
                                {flex: 0.1},
                                {
                                    xtype: 'combobox',
                                    width: 340,
                                    labelWidth: 250,
                                    labelAlign: 'right',
                                    fieldLabel: 'Признак выгодоприобретателя (VP)',
                                    tooltip: {
                                        text: "Признак выгодоприобретателя:<br>•	По операции есть выгодоприобретатель, и идентификация его завершена (значение 1 – идентификация завершена);<br>•	По операции есть выгодоприобретатель, и идентификация его на дату направления сообщения не завершена (значение 2 – идентификация не завершена);<br>•	В иных случаях (значение 0 – нет выгодоприобретателя)."
                                    },
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{id}',
                                        '</tpl>'
                                    ],
                                    displayField: 'text',
                                    valueField: 'id',
                                    forceSelection: true,
                                    multiSelect: false,
                                    matchFieldWidth: false,
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        store: '{VPHardStore}',
                                        value: '{record.VP}'
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    width: 450,
                                    labelWidth: 300,
                                    labelAlign: 'right',
                                    tooltip: {
                                        text: "В случае проведения операции с использованием карты:<br>•	Владелец банковской карты - клиент Банка, представляющего сведения и являющегося эмитентом банковской карты (значение для списка: 1 – клиент Банка);<br>•	Владелец банковской карты - не клиент Банка, представляющего сведения (значение для списка: 2 – не клиент Банка);<br>•	В ином случае: (значение для списка: 3 – иное).<br>В случае если операция проводится без использования банковской карты - (значение для списка: 0 – без БК)."
                                    },
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{id}',
                                        '</tpl>'
                                    ],
                                    displayField: 'text',
                                    valueField: 'id',
                                    forceSelection: true,
                                    multiSelect: false,
                                    matchFieldWidth: false,
                                    allowBlank: false,
                                    fieldLabel: 'Признак владельца банковской карты (CARD_B)',
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        store: '{CARDBHardStore}',
                                        value: '{record.CARD_B}'
                                    }
                                },
                                {flex: 0.1}
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    width: '100%',
                                    labelWidth: 160,
                                    labelAlign: 'right',
                                    fieldLabel: 'Признак участника (PRU)',
                                    tooltip: {
                                        text: "Признак участника операции - 6 цифр, заполняется для 3000-х и 7000-х кодов.</br>3000-е коды: 1 разряд - содержит 1, 2 или 3 - признак адреса регистрации, адреса нахождения или счета в банке соответственно, в государстве, не участвующем в противодействии легализации доходов</br>2-4 разряды - содержат код по ОКСМ страны, не участвующей в противодействии легализации доходов</br>5-6 разряды - содержат порядковый номер территории в соответствии с Перечнем несотрудничающих государств (для территорий, отсутствующих в ОКСМ, иначе - 00)</br>7000-е коды: 1 разряд - содержит 1, 2 или 3:</br>1 - участник - лицо, причастное к терроризму</br>2 - участник - ЮЛ, находящееся в собственности лица, причастного к терроризму</br>3 - участник - ФЛ/ЮЛ, действующее от имени или по указанию лица, причастного к терроризму</br>2-6 разряды - номер ФЛ/ЮЛ в соответствии с Перечнем лиц, причастных к терроризму"
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.PR}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    width: '100%',
                                    labelWidth: 250,
                                    labelAlign: 'right',
                                    fieldLabel: 'Наименование ЮЛ или ФИО ФЛ (NAMEU)',
                                    tooltip: {
                                        text: "Для ЮЛ - наименование ЮЛ.<br>Если участником операции является филиал ЮЛ - наименование филиала ЮЛ и наименование ЮЛ.<br>Для ФЛ, индивидуального предпринимателя (далее – ИП) - фамилия, имя, отчество (при наличии последнего) полностью, в указанном порядке, с разделением слов символом 'пробел'.<br>Если значение поля «тип участника операции» = 4 (невозможно установить тип участника операции), указывается значение поля \"Плательщик\" или иного соответствующего ему поля из расчетного документа."
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.NAMEU}'
                                    },
                                    listeners: {
                                        afterrender: function (field) {
                                            field.focus();
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    width: 400,
                                    labelWidth: 250,
                                    labelAlign: 'right',
                                    fieldLabel: 'ИНН (ND)',
                                    tooltip: {
                                        text: "Идентификационный номер участника операции.<br>Для резидента:<br>ЮЛ – ИНН – 10 символов, при отсутствии ИНН у ЮЛ на законном основании – 0;<br>ФЛ, ИП – ИНН (при его наличии) – 12 символов, при отсутствии ИНН у ФЛ – 0.<br>Для нерезидента:<br>ЮЛ (филиала ЮЛ) – ИНН (при его наличии) – 10 символов, при отсутствии ИНН – код иностранной организации (КИО) – 5 символов;<br>ФЛ, ИП – ИНН (при его наличии) – 12 символов, при отсутствии ИНН у ФЛ, ИП – 0."
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.ND}'
                                    }
                                },
                                {flex: 0.1},
                                {
                                    xtype: 'datefield',
                                    allowBlank: false,
                                    width: 460,
                                    labelWidth: 290,
                                    labelAlign: 'right',
                                    fieldLabel: 'Дата рег./ рожд. (GR)',
                                    tooltip: {
                                        text: "Для ЮЛ - резидента: дата регистрации ЮЛ.<br>Для ЮЛ - нерезидента: если участником операции является филиал (представительство) иностранного ЮЛ, аккредитованный (аккредитованное) на территории РФ – дата выдачи свидетельства об аккредитации (без учета процедур продления аккредитации);<br>в ином случае – дата регистрации ЮЛ по месту учреждения и регистрации.<br>Для ФЛ, ИП - дата рождения в соответствии с документом, удостоверяющим личность."
                                    },
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.GR}'
                                    }
                                },
                                {flex: 1}
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    width: 400,
                                    labelWidth: 250,
                                    labelAlign: 'right',
                                    fieldLabel: 'Рег. номер (RG)',
                                    tooltip: {
                                        text: "Для ЮЛ - резидента: основной государственный регистрационный номер (далее – ОГРН).<br>Для ЮЛ - нерезидента:<br>Если участником операции является филиал (представительство) иностранного ЮЛ, аккредитованный (аккредитованное) на территории Российской Федерации (далее - РФ) - номер свидетельства об аккредитации филиала иностранного ЮЛ (номер свидетельства об аккредитации представительства иностранного ЮЛ) (присваивается федеральным государственным учреждением Государственная регистрационная палата при Министерстве юстиции РФ (далее - Государственная регистрационная палата при Минюсте России);<br>в ином случае - регистрационный номер ЮЛ по месту учреждения и регистрации.<br>Для ФЛ – 0.<br>Для ИП – гражданина РФ, иностранного гражданина или лица без гражданства – основной государственный регистрационный номер индивидуального предпринимателя (далее – ОГРНИП) (при его наличии). При отсутствии ОГРНИП – регистрационный номер по месту учреждения и регистрации."
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.RG}'
                                    }
                                },
                                {flex: 0.1},
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    labelWidth: 250,
                                    labelAlign: 'right',
                                    fieldLabel: 'Место рождения (BP)',
                                    tooltip: {
                                        text: "Для ЮЛ - заполняется значением '0'.<br>Для ФЛ, ИП - место рождения в соответствии с документом, удостоверяющим личность."
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.BP}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretchmax'
                    },
                    margin: '0 5px 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: '<b>Информация о месте регистрации/жительства</b>',
                            flex: 1,
                            border: 1,
                            padding: 10,
                            margin: '10px 0 0 0',
                            defaults: {
                                padding: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            fieldLabel: 'Код места рег./жит. (KODCR)',
                                            tooltip: {
                                                text: "Для ЮЛ - код места регистрации.<br>Если участником операции является филиал ЮЛ, указывается код места регистрации головной организации.<br>Для ФЛ, ИП - код места жительства.<br>Для стран и территорий, включенных в ОКСМ, 1 - 3 разряды содержат цифровой код страны регистрации (проживания) в соответствии с ОКСМ.<br>При регистрации (жительстве) на территории, которая не участвует в международном сотрудничестве в сфере ПОД/ФТ и не включена в ОКСМ, разряды 1 - 3 содержат цифровой код по ОКСМ страны, в которой находится территория, в разрядах 4 - 5 указывается порядковый номер этой территории в соответствии с Перечнем несотрудничающих государств, в ином случае разряды 4 - 5 содержат ‘00’."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.KODCR}'
                                            },
                                            listeners: {
                                                afterrender: function (field) {
                                                    field.focus();
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            tooltip: {
                                                text: "Код субъекта РФ по ОКАТО. Для лиц, зарегистрированных в иностранном государстве – ‘00’."
                                            },
                                            editable: true,
                                            queryMode: 'local',
                                            tpl: [
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                                                '</tpl>'
                                            ],
                                            displayTpl: [
                                                '<tpl for=".">',
                                                '{OKATO_CD}',
                                                '</tpl>'
                                            ],
                                            fieldLabel: 'Код по ОКАТО (AMR_S)',
                                            displayField: 'OKATO_CD',
                                            valueField: 'OKATO_CD',
                                            forceSelection: true,
                                            triggerClear: true,
                                            allowBlank: false,
                                            bind: {
                                                store: '{OKATOStoreAmrS}',
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.AMR_S}'
                                            },
                                            listeners: {
                                                afterrender: function (field) {
                                                    field.focus();
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            fieldLabel: 'Район/Область (AMR_R)',
                                            tooltip: {
                                                text: "Район (регион) республиканского и областного значения. Для лиц, зарегистрированных в иностранном государстве, в данном поле проставляется субъект территориального деления иностранного государства (при наличии)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.AMR_R}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            fieldLabel: 'Город/Нас. пункт (AMR_G)',
                                            tooltip: {
                                                text: "Населенный пункт (город, ПГТ, сельский населенный пункт и т.п.)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.AMR_G}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            fieldLabel: 'Ул. (AMR_U)',
                                            tooltip: {
                                                text: "Наименование улицы."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.AMR_U}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'column'
                                    },
                                    padding: 0,
                                    defaults: {
                                        border: 0,
                                        margin: '5px 0 0 0'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            width: 250,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            fieldLabel: 'Д. (AMR_D)',
                                            tooltip: {
                                                text: "Номер дома (номер владения)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.AMR_D}'
                                            }
                                        },
                                        {columnWidth: 0.5},
                                        {
                                            xtype: 'textfield',
                                            width: 250,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            fieldLabel: 'Корпус/Стр. (AMR_K)',
                                            tooltip: {
                                                text: "Номер корпуса (номер строения)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.AMR_K}'
                                            }
                                        },
                                        {columnWidth: 0.5},
                                        {
                                            xtype: 'textfield',
                                            width: 250,
                                            labelWidth: 180,
                                            labelAlign: 'right',
                                            fieldLabel: 'Оф./Кв. (AMR_O)',
                                            tooltip: {
                                                text: "Номер офиса (номер квартиры)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.AMR_O}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {width: 10},
                        {
                            xtype: 'fieldset',
                            title: '<b>Информация о месте нахождения/пребывания</b>',
                            flex: 1,
                            border: 1,
                            padding: 10,
                            margin: '10px 0 0 0',
                            defaults: {
                                padding: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Код места нах./Гражданство (KODCN)',
                                            tooltip: {
                                                text: "Для ЮЛ - код места нахождения.<br>Если участником операции является филиал ЮЛ, указывается код места нахождения филиала ЮЛ.<br>Для стран и территорий, включенных в ОКСМ, разряды 1 - 3 содержат цифровой код страны нахождения в соответствии с ОКСМ.<br>При нахождении ЮЛ (филиала ЮЛ) на территории, которая не участвует в международном сотрудничестве в сфере ПОД/ФТ  и не включена в ОКСМ, разряды 1 - 3 содержат цифровой код по ОКСМ страны, в которой находится территория, в разрядах 4 - 5 указывается порядковый номер этой территории в соответствии с Перечнем несотрудничающих государств, в ином случае разряды 4 - 5 содержат ‘00’.<br>Для ФЛ, ИП - гражданство.<br>Разряды 1 - 3 поля содержат код страны в соответствии с ОКСМ. Для лиц без гражданства, разряды 1-3 заполняются значением ‘000’.<br>Разряды 4 – 5 содержат: для иностранных публичных должностных лиц (далее ИПДЛ) – ‘01’; для родственников ИПДЛ – ‘02’; в ином случае – ‘00’."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.KODCN}'
                                            },
                                            listeners: {
                                                afterrender: function (field) {
                                                    field.focus();
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Код по ОКАТО (ADRESS_S)',
                                            tooltip: {
                                                text: "Код субъекта РФ по ОКАТО.<br>Для лиц, находящихся в иностранном государстве – ‘00’."
                                            },
                                            editable: true,
                                            queryMode: 'local',
                                            tpl: [
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                                                '</tpl>'
                                            ],
                                            displayTpl: [
                                                '<tpl for=".">',
                                                '{OKATO_CD}',
                                                '</tpl>'
                                            ],
                                            displayField: 'OKATO_CD',
                                            valueField: 'OKATO_CD',
                                            forceSelection: true,
                                            triggerClear: true,
                                            allowBlank: false,
                                            bind: {
                                                store: '{OKATOStoreAdressS}',
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ADRESS_S}'
                                            },
                                            listeners: {
                                                afterrender: function (field) {
                                                    field.focus();
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Район/Область (ADRESS_R)',
                                            tooltip: {
                                                text: "Район (регион) республиканского и областного значения. Для лиц, зарегистрированных в иностранном государстве, в данном поле проставляется субъект территориального деления иностранного государства (при наличии)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ADRESS_R}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Город/Нас. пункт (ADRESS_G)',
                                            tooltip: {
                                                text: "Населенный пункт (город, ПГТ, сельский населенный пункт и т.п.)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ADRESS_G}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Ул. (ADRESS_U)',
                                            tooltip: {
                                                text: "Наименование улицы."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ADRESS_U}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'column'
                                    },
                                    padding: 0,
                                    defaults: {
                                        border: 0,
                                        margin: '5px 0 0 0'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            width: 250,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Д. (ADRESS_D)',
                                            tooltip: {
                                                text: "Номер дома (номер владения)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ADRESS_D}'
                                            }
                                        },
                                        {columnWidth: 0.5},
                                        {
                                            xtype: 'textfield',
                                            width: 250,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Корпус/Стр. (ADRESS_K)',
                                            tooltip: {
                                                text: "Номер корпуса (номер строения)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ADRESS_K}'
                                            }
                                        },
                                        {columnWidth: 0.5},
                                        {
                                            xtype: 'textfield',
                                            width: 250,
                                            labelWidth: 190,
                                            labelAlign: 'right',
                                            fieldLabel: 'Оф./Кв. (ADRESS_O)',
                                            tooltip: {
                                                text: "Номер офиса (номер квартиры)."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ADRESS_O}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: '<b>Документ, удостоверяющий личность</b>',
                    border: 1,
                    padding: 10,
                    margin: '10px 5px 0 0',
                    defaults: {
                        padding: '5px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    flex: 1,
                                    labelWidth: 260,
                                    labelAlign: 'right',
                                    fieldLabel: 'Документ, удостоверяющий личность (KD)',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - код вида документа, удостоверяющего личность (Справочник кодов видов документов см. в Приложении 10 к Положению № 321-П)."
                                    },
                                    editable: false,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{DOC_TYPE_CD} - {DOC_TYPE_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{DOC_TYPE_CD}',
                                        '</tpl>'
                                    ],
                                    displayField: 'OKATO_OBJ_NM',
                                    valueField: 'DOC_TYPE_CD',
                                    forceSelection: true,
                                    matchFieldWidth: false,
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        store: '{DOCTYPEStore}',
                                        value: '{record.KD}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 0.5,
                                    labelWidth: 200,
                                    labelAlign: 'right',
                                    fieldLabel: 'ОКПО/Серия документа (SD)',
                                    tooltip: {
                                        text: "Для ЮЛ - код ОКПО, при отсутствии у ЮЛ кода ОКПО на законном основании – ‘0’;<br>Для ФЛ, ИП - серия документа, удостоверяющего личность.<br>Если в серии присутствуют римские цифры, то при заполнении данного показателя используются заглавные буквы латинского алфавита (при этом для обозначения цифры 5 – V); русские буквы в серии передаются заглавными русскими буквами. Если в документе отсутствует серия, в поле проставляется ‘0’."
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.SD}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 0.5,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    fieldLabel: 'Номер документа (VD1)',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - номер документа, удостоверяющего личность. При заполнении пробелы не допускаются."
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.VD1}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 2,
                                    labelWidth: 260,
                                    labelAlign: 'right',
                                    fieldLabel: 'Орган, выдавший документ (VD2)',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - наименование органа, выдавшего документ, удостоверяющий личность ФЛ, ИП и код подразделения (если имеется)."
                                    },
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.VD2}'
                                    }
                                },
                                {
                                    xtype: 'datefield',
                                    width: 340,
                                    labelWidth: 170,
                                    labelAlign: 'right',
                                    fieldLabel: 'Дата выдачи (VD3)',
                                    tooltip: {
                                        text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - дата выдачи документа, удостоверяющего личность ФЛ, ИП."
                                    },
                                    format: 'd.m.Y',
                                    startDay: 1,
                                    allowBlank: false,
                                    bind: {
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.VD3}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretchmax'
                    },
                    margin: '0 5px 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: '<b>Документ, подтверждающий право на пребывание (проживание) в РФ</b>',
                            flex: 1,
                            border: 1,
                            padding: 10,
                            margin: '10px 0 0 0',
                            defaults: {
                                padding: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            labelWidth: 170,
                                            labelAlign: 'right',
                                            fieldLabel: 'Код вида документа (VD4)',
                                            tooltip: {
                                                text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - гражданина РФ - заполняется значением '0'.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - код вида документа, подтверждающего право на пребывание (проживание) в РФ (Справочник кодов видов документов см. в Приложении 10 к Положению № 321-П)."
                                            },
                                            editable: false,
                                            queryMode: 'local',
                                            tpl: [
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{DOC_TYPE_CD} - {DOC_TYPE_NM}</div>',
                                                '</tpl>'
                                            ],
                                            displayTpl: [
                                                '<tpl for=".">',
                                                '{DOC_TYPE_CD}',
                                                '</tpl>'
                                            ],
                                            displayField: 'OKATO_OBJ_NM',
                                            valueField: 'DOC_TYPE_CD',
                                            forceSelection: true,
                                            matchFieldWidth: false,
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                store: '{DOCTYPEStore}',
                                                value: '{record.VD4}'
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 160,
                                            labelAlign: 'right',
                                            fieldLabel: 'Серия и номер (VD5)',
                                            tooltip: {
                                                text: "Для ЮЛ заполняется значением '0'.<br>Для ФЛ, ИП - гражданина РФ заполняется значением '0'.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - серия и номер документа, подтверждающего право на пребывание (проживание) в РФ.<br>Серия и номер заполняются без пробелов.<br>Для заполнения римских цифр используются заглавные буквы латинского алфавита (при этом для обозначения цифры 5 - V); русские буквы в серии передаются заглавными русскими буквами. Если в документе отсутствует серия, в поле проставляется ‘0’."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.VD5}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'datefield',
                                            width: 270,
                                            labelWidth: 150,
                                            labelAlign: 'right',
                                            fieldLabel: 'Срок действия с (VD6)',
                                            tooltip: {
                                                text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата начала срока действия права на пребывание (проживание) в РФ.<br>Если в документе отсутствует соответствующая дата, в поле проставляется значение 01012099"
                                            },
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.VD6}'
                                            }
                                        },
                                        {
                                            xtype: 'datefield',
                                            flex: 1,
                                            labelWidth: 60,
                                            labelAlign: 'right',
                                            fieldLabel: 'по (VD7)',
                                            tooltip: {
                                                text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата окончания срока действия права на пребывание (проживание) в РФ.<br>Если в документе отсутствует соответствующая дата, в поле проставляется значение 01012099.<br>Формат поля даты - ДДММГГГГ, где ДД - день месяца, ММ - месяц, ГГГГ - год."
                                            },
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.VD7}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {width: 10},
                        {
                            xtype: 'fieldset',
                            title: '<b>Миграционная карта</b>',
                            flex: 1,
                            border: 1,
                            padding: 10,
                            margin: '10px 0 0 0',
                            defaults: {
                                padding: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 170,
                                            labelAlign: 'right',
                                            fieldLabel: 'Номер (MC_1)',
                                            tooltip: {
                                                text: "Для ЮЛ заполняется значением '0' .<br>Для ФЛ, ИП - гражданина РФ заполняется значением '0'.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - номер миграционной карты.<br>При отсутствии миграционной карты – ‘0’."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.MC1}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'datefield',
                                            width: 280,
                                            labelWidth: 170,
                                            labelAlign: 'right',
                                            fieldLabel: 'Срок пребывания с (MC_2)',
                                            tooltip: {
                                                text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата начала срока пребывания в РФ в соответствии с миграционной картой.<br>При отсутствии миграционной карты заполняется значением 01012099."
                                            },
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.MC2}'
                                            }
                                        },
                                        {
                                            xtype: 'datefield',
                                            flex: 1,
                                            labelWidth: 70,
                                            labelAlign: 'right',
                                            fieldLabel: 'по (MC_3)',
                                            tooltip: {
                                                text: "Для ЮЛ заполняется значением 01012099.<br>Для ФЛ, ИП - гражданина РФ заполняется значением 01012099.<br>Для ФЛ, ИП - иностранного гражданина или лица без гражданства - дата окончания срока пребывания в РФ в соответствии с миграционной картой.<br>При отсутствии миграционной карты заполняется значением 01012099."
                                            },
                                            format: 'd.m.Y',
                                            startDay: 1,
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.MC3}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretchmax'
                    },
                    margin: '0 5px 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: '<b>Банковские идентификационные реквизиты КО, обслуживающей участника</b>',
                            flex: 1,
                            border: 1,
                            padding: 10,
                            margin: '10px 0 0 0',
                            defaults: {
                                padding: '5px 0 0 0'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 240,
                                            labelAlign: 'right',
                                            fieldLabel: 'Наименование КО (филиала) (NAME_B)',
                                            tooltip: {
                                                text: "Наименование КО (филиала КО), обслуживающей (обслуживающего) участника операции при ее проведении, или ‘0’ – если участник операции находится на обслуживании в Банке (филиале Банка), представляющем сведения об операции, или участником операции является Банк (филиал Банка), представляющий сведения об этой операции в ФСФМ."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.NAME_B}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            minWidth: 300,
                                            labelWidth: 240,
                                            labelAlign: 'right',
                                            fieldLabel: 'БИК/SWIFTBIC/ non-SWIFT BIC (BIK_B)',
                                            tooltip: {
                                                text: "БИК КО (филиала КО, обслуживающей (обслуживающего) участника операции при ее проведении, или ‘0’ – если участник операции находится на обслуживании в Банке (филиале Банка), представляющем сведения об операции, или участником операции является Банк (филиал Банка), представляющий сведения об этой операции в ФСФМ.<br>Для КО (филиала КО) – резидента: БИК.<br>Для КО (филиала КО) – нерезидента: SWIFT BIC (или non-SWIFT BIC) или ‘0’ – при его отсутствии."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.BIK_B}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 240,
                                            labelAlign: 'right',
                                            fieldLabel: 'Код места нахождения (KODCN_B)',
                                            tooltip: {
                                                text: "Код места нахождения КО (филиала КО, обслуживающей (обслуживающего) участника операции при ее проведении, или ‘0’ – если участник операции находится на обслуживании в Банке (филиале Банка), представляющем сведения об операции, или участником операции является Банк (филиал Банка), представляющий сведения об этой операции в ФСФМ.<br>Для стран и территорий, включенных в ОКСМ, разряды 1 – 3 содержат цифровой код страны в соответствии с ОКСМ.<br>При нахождении КО (филиала КО) на территории, которая не участвует в международном сотрудничестве в сфере ПОД/ФТ и не включена в ОКСМ, разряды 1 – 3 содержат цифровой код по ОКСМ страны, в которой находится территория, в разрядах 4 – 5 указывается порядковый номер этой территории в Перечне несотрудничающих государств, в ином случае разряды 4 – 5 содержат ‘00’."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.KODCN_B}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 240,
                                            labelAlign: 'right',
                                            fieldLabel: 'Номер корр. счета (ACC_COR_B)',
                                            tooltip: {
                                                text: "Номер корреспондентского счета, используемого при проведении операции, или ‘0’ – если операция осуществляется без использования такого счета."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ACC_COR_B}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        border: 0
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            labelWidth: 240,
                                            labelAlign: 'right',
                                            fieldLabel: 'Номер счета (ACC_B)',
                                            tooltip: {
                                                text: "Номер счета участника операции в обслуживающей (обслуживающем) его КО (филиале КО), используемый при проведении операции, или ‘0’ – если операция осуществляется без использования счета участника операции."
                                            },
                                            allowBlank: false,
                                            bind: {
                                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                value: '{record.ACC_B}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {width: 10},
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: '<b>Банковские идентификационные реквизиты корреспондента</b>',
                                    flex: 1,
                                    height: 102,
                                    border: 1,
                                    margin: '10px 0 0 0',
                                    defaults: {
                                        padding: '5px 0 0 0'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            defaults: {
                                                border: 0
                                            },
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    flex: 1,
                                                    labelWidth: 350,
                                                    labelAlign: 'right',
                                                    fieldLabel: 'Наименование КО (филиала) – корреспондента (NAME_R)',
                                                    tooltip: {
                                                        text: "Наименование КО (филиала КО), передавшей (передавшего) (принявшей (принявшего) от) КО (филиала КО), представляющей (представляющего) сведения об операции, документы, являющиеся основанием для проведения операции, со стороны лица, совершающего операцию с ДС, или ‘0’ – если лицо, совершающее операцию с ДС, находится на обслуживании в Банке (филиале Банка), представляющем сведения об операции, или лицом, совершающим операцию с ДС, является Банк (филиал Банка), представляющий сведения об операции в ФСФМ."
                                                    },
                                                    allowBlank: false,
                                                    bind: {
                                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                        value: '{record.NAME_R}'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            defaults: {
                                                border: 0
                                            },
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    flex: 1,
                                                    minWidth: 300,
                                                    labelWidth: 235,
                                                    labelAlign: 'right',
                                                    fieldLabel: 'БИК/SWIFTBIC/ non-SWIFT BIC (BIK_R)',
                                                    tooltip: {
                                                        text: "БИК КО (филиала КО), представляющей (представляющего) сведения об операции, документы, являющиеся основанием для проведения операции, со стороны лица, совершающего операцию с ДС, или ‘0’ – если лицо, совершающее операцию с ДС, находится на обслуживании в Банке (филиале Банка), представляющем сведения об операции, или лицом, совершающим операцию с ДС, является Банк (филиал Банка), представляющий сведения об операции в ФСФМ.<br>Для КО (филиала КО) – резидента: БИК.<br>Для КО (филиала КО) – нерезидента: SWIFT BIC (или non-SWIFT BIC) или ‘0’ – при его отсутствии."
                                                    },
                                                    allowBlank: false,
                                                    bind: {
                                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                        value: '{record.BIK_R}'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            defaults: {
                                                border: 0
                                            },
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    flex: 1,
                                                    labelWidth: 235,
                                                    labelAlign: 'right',
                                                    fieldLabel: 'Код места нахождения (KODCN_R)',
                                                    tooltip: {
                                                        text: "Код места нахождения КО (филиала КО), передавшей (передавшего) (принявшей (принявшего) от) КО (филиала КО), представляющей (представляющего) сведения об операции, документы, являющиеся основанием для проведения операции, со стороны лица, совершающего операцию с ДС, или ‘0’ – если лицо, совершающее операцию с ДС, находится на обслуживании в Банке (филиале Банка), представляющем сведения об операции, или лицом, совершающим операцию с ДС, является Банк (филиал Банка), представляющий сведения об операции в ФСФМ. <br>Разряды 1 – 3 содержат цифровой код страны в соответствии с ОКСМ.<br>При нахождении КО на территории, которая не участвует в международном сотрудничестве в сфере ПОД/ФТ и не включена в ОКСМ, разряды 1 – 3 содержат цифровой код по ОКСМ страны, в которой находится территория, в разрядах 4 – 5 указывается порядковый номер этой территории в Перечне несотрудничающих государств, в ином случае разряды 4 – 5 содержат ‘00’."
                                                    },
                                                    allowBlank: false,
                                                    bind: {
                                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                        value: '{record.KODCN_R}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    title: '<b>Данные о банковской карте</b>',
                                    flex: 1,
                                    height: 80,
                                    border: 1,
                                    margin: '10px 0 0 0',
                                    defaults: {
                                        padding: '5px 0 0 0'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            defaults: {
                                                border: 0
                                            },
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    flex: 1,
                                                    labelWidth: 265,
                                                    labelAlign: 'right',
                                                    fieldLabel: 'Наименование банка-эмитента (NAME_IS_B)',
                                                    tooltip: {
                                                        text: "Наименование КО - эмитента банковской карты.<br>В ином случае – ‘0’."
                                                    },
                                                    allowBlank: false,
                                                    bind: {
                                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                        value: '{record.NAME_IS}'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            defaults: {
                                                border: 0
                                            },
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    flex: 1,
                                                    labelWidth: 265,
                                                    labelAlign: 'right',
                                                    fieldLabel: 'БИК банка-эмитента (BIK_IS_B)',
                                                    tooltip: {
                                                        text: "Для КО эмитента банковской карты – резидента: БИК.<br>Для КО эмитента банковской карты – нерезидента: SWIFT (или non-SWIFT BIC) (код КО нерезидента).<br>В ином случае – ‘0’."
                                                    },
                                                    allowBlank: false,
                                                    bind: {
                                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                                        value: '{record.BIK_IS}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    margin: '0 5px 0 0',
                    items: [
                        {
                            xtype: 'fieldset',
                            title: '<b>Период действия</b>',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults:{
                                flex: 1,
                                xtype: 'datefield',
                                labelWidth: 130,
                                format: 'd.m.Y',
                                startDay: 1
                            },
                            items: [
                                {
                                    fieldLabel: 'Начало действия',
                                    reference: 'startDatePartyOrg',
                                    bind: {
                                        value: '{record.START_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                    container.nextSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    fieldLabel: 'Окончание действия',
                                    reference: 'endDatePartyOrg',
                                    margin: '0 0 0 10',
                                    bind: {
                                        value: '{record.END_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                    container.validateValue(false);
                                                    container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                }
                                                container.previousSibling('datefield').focus();
                                                container.focus();
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                    container.validateValue(false);
                                                    container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                }
                                            }
                                        }
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Активно?',
                            margin: '0 30 0 30',
                            bind: {
                                value: '{record.ACTIVE_FL}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    margin: '0 5px 0 0',
                    items: [
                        {
                            xtype: 'fieldset',
                            title: '<b>Аудит</b>',
                            layout: {
                                type: 'hbox',
                                align: 'left',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults:{
                                flex: 1,
                                xtype: 'container',
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                width: 350
                            },
                            items: [
                                {
                                    defaults: {
                                        flex: 1,
                                        xtype: 'displayfield',
                                        labelAlign: 'left',
                                        width: 245,
                                        align: 'left'
                                    },
                                    items: [
                                        {
                                            fieldLabel: 'Кто создал',
                                            labelWidth: 75,
                                            bind: {
                                                value: '{record.CREATED_BY}'
                                            }
                                        },
                                        {
                                            fieldLabel: 'Когда создал',
                                            labelWidth: 80,
                                            bind: {
                                                value: '{record.CREATED_DATE}'
                                            },
                                            renderer: function (value, field) {
                                                return Ext.Date.format(value,'d.m.Y H:i:s');
                                            }
                                        },
                                        {
                                            xtype: 'container',
                                            flex: 0.1
                                        },
                                        {
                                            fieldLabel: 'Кто изменил',
                                            labelWidth: 80,
                                            bind: {
                                                value: '{record.MODIFIED_BY}'
                                            }
                                        },
                                        {
                                            fieldLabel: 'Когда изменил',
                                            labelWidth: 90,
                                            bind: {
                                                value: '{record.MODIFIED_DATE}'
                                            },
                                            renderer: function (value, field) {
                                                return Ext.Date.format(value, 'd.m.Y H:i:s');
                                            }
                                        }

                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    margin: '10px 5px 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            width: '100%',
                            labelWidth: 80,
                            labelAlign: 'right',
                            fieldLabel: '<b>Примечание</b>',
                            tooltip: {
                                text: ""
                            },
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                value: '{record.NOTE}'
                            }
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveFormPartyOrg',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть',
                    disabled: false,
                    readOnly: false
                }
            ]
        }
    ]
});