Ext.define('AML.view.oes.OES321PartyOrg.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321partyorgvmdl',
    data: {
        title: 'КО участники операций',
        activeCB: true,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        filters: {
            bind: {
                activeCB: '{activeCB}'
            },
            get: function(data) {
                var store = this.getStore('oes321partyorg'),
                    filters = [],
                    activeCB = data.activeCB;

                if (activeCB) {
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник КО участники операций
        oes321partyorg: {
            model: 'AML.model.OES321PartyOrg',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OES_321_P_ORG_ID',
                    direction: 'ASC'
                }
            ],
            filters: '{filters}'
        }
    }
});
