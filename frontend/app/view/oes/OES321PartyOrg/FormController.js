Ext.define('AML.view.oes.OES321PartyOrg.FormController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.oes321partyorgform',
    config: {
        listen: {
            component: {
                '#saveFormPartyOrg': {
                    click: 'onClickSaveFormPartyOrg'
                },
                '#cancelForm': {
                    click: 'onClickCancelFormPartyOrg'
                }
            }
        }
    },
    callbackOESPartyOrg: function (records, operation, success) {
        var vm = this.getViewModel(),
            controler = this.controller,
            oldRecord = vm.get('record'),
            form = this.down('form').getForm(),
            view = vm.getView(),
            recData = records.getData(),
            id = records.get('OES_321_P_ORG_ID'),
            store = vm.getParent().getStore('oes321partyorg');
        store.load();
        var newRecord = store.getById(id),
            record = newRecord.getData(),
            newRecordSync = store.sync();
        vm.set('record', newRecord);
    },
    callbackRefreshGridOESPartyOrg: function (records, operation, success) {
        var self = this,
            controlerMain = self.lookupController(true),
            viewGrid = controlerMain.lookupReference('viewOes321partyorg'),
            vm = viewGrid.getViewModel(),
            store = vm.getStore('oes321partyorg');
        store.reload();
    },
    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveFormPartyOrg: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView();
        if (!window || !this.isChanges(window)) return;

        // валидация даты старта и окончания операции
        if (this.valideteDateForm(window, 'startDatePartyOrg', 'endDatePartyOrg')) {
            return;
        }

        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false, this.callbackOESPartyOrg);
    },
    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelFormPartyOrg: function (button, eOpts) {
        var self = this,
            window = this.getView(),
            controller = this,
            fnSaveForm = this.saveForm;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    // валидация даты старта и окончания операции
                    if (self.valideteDateForm(window, 'startDatePartyOrg', 'endDatePartyOrg')) {
                        return;
                    }
                    fnSaveForm(window, controller, true, self.callbackRefreshGridOESPartyOrg);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    },
    rebuildOKATOStore: function(store) {
        // своя сортировка
        var item0 = store.findRecord('_ID_', '0', 0, false, false, true),
            item00 = store.findRecord('_ID_', '00', 0, false, false, true),
            records = [],
            addItem0 = item0,
            addItem00 = item00;
        store.remove(item0);
        store.remove(item00);
        store.sort('OKATO_OBJ_NM', 'ASC');
        store.sorters.clear();
        store.each(function(record){
            records.push(record);
        });
        store.removeAll(true);
        store.add([item0, item00]);
        Ext.each(records, function (record) {
            store.add(record);
        })

        return store;
    }
});
