Ext.define('AML.view.oes.OES321ListsOrg.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321listsorgvmdl',
    data: {
        title: 'Перечни организаций',
        activeCB: true,
        idOrg: null,
        innOrg: null,
        nameOrg: null,
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        filters: {
            bind: {
                activeCB: '{activeCB}',
                innCB: '{innOrg}',
                nameCB: '{nameOrg}',
                idnOrg: '{idOrg}'
            },
            get: function (data) {
                var store = this.getStore('oes321listsorg'),
                    filters = [],
                    activeCB = data.activeCB,
                    innCB = data.innCB,
                    nameCB = data.nameCB,
                    idnOrg = data.idnOrg;

                if (activeCB) {
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                if (innCB) {
                    filters.push({
                        property: 'INN_NB',
                        value: innCB
                    });
                }
                if (nameCB) {
                    filters.push({
                        property: 'ORG_NM',
                        value: nameCB
                    });
                }
                if (idnOrg) {
                    filters.push({
                        property: 'WATCH_LIST_CD',
                        value: idnOrg
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник Информация о КО
        oes321typeorg: {
            model: 'AML.model.OES321TypeOrg',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'WATCH_LIST_NM',
                    direction: 'ASC'
                }
            ],
            listeners: {
                load: function (data) {
                    var firstIndex = data.getAt(0).get('WATCH_LIST_CD');
                    var combo = Ext.getCmp('scopeFiltersLists');
                    combo.setValue(firstIndex);
                    var grid = combo.up('app-oes-oes321listsorg');
                    grid.getViewModel().set('idOrg', firstIndex);
                    var store = grid.getStore();
                    store.load({
                        scope: combo.up('app-oes-oes321listsorg').getView(),
                        params: {
                            form: 'WatchListOrg',
                            action: 'read',
                            watch_list_cd: firstIndex,
                            ACTIVE_FL: 'Y'
                        }
                    });

                    store.clearFilter();
                    store.filter([{
                            property: 'watch_list_cd',
                            value: firstIndex
                        },
                        {
                            property: 'ACTIVE_FL',
                            value: 'Y'
                        }]);
                    store.load();
                }
            }
        },

        oes321listsorg: {
            model: 'AML.model.OES321ListsOrg',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'WATCH_LIST_ORG_ID',
                    direction: 'DESC'
                }
            ],
            filters: '{filters}',
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    transform: {
                        fn: function (data) {
                            if (Ext.isArray(data.data)) {
                                Ext.Array.forEach(data.data, function (i) {
                                    if (i.ACTIVE_FL != undefined) {
                                        i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
                                    }
                                });
                            }
                            return data;
                        },
                        scope: this
                    },
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function (data, request) {
                            var actionMap = {
                                create: 'create',
                                update: 'update',
                                destroy: 'delete',
                                read: 'read'
                            };
                            request.setParam('action', actionMap[request.getAction()] || 'read');
                            Ext.Array.forEach(data, function (i) {
                                if (i.ACTIVE_FL != undefined) {
                                    i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
                                }
                            });
                            return data;
                        },
                        scope: this
                    },
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'WatchListOrg',
                    action: 'read',
                    watch_list_cd: '{idOrg}'
                }
            }
        }
    }
});
