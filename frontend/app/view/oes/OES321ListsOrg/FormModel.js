Ext.define('AML.view.oes.OES321ListsOrg.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321listsorgformvm',
    data: {
        title: 'Перечни организаций',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    }
});
