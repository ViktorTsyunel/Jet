Ext.define('AML.view.oes.OES321ListsOrg.Dialog.ListROModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.appoes321listsorgdialoglistvm',
    data: {
        title: 'Перечни организаций',
        idOrg: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        filters: {
            bind: {
                idnOrg: '{idOrg}'
            },
            get: function (data) {
                var store = this.getStore('oes321listsorg'),
                    filters = [],
                    idnOrg = data.idnOrg;

                if (idnOrg) {
                    filters.push({
                        property: 'WATCH_LIST_CD',
                        value: idnOrg
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник Информация о КО
        oes321listsorg: {
            model: 'AML.model.OES321ListsOrg',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'WATCH_LIST_ORG_ID',
                    direction: 'DESC'
                }
            ],
            filters: '{filters}',
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST',
                    update: 'POST',
                    create: 'POST',
                    destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message',
                    totalProperty: 'totalCount'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'WatchListOrg',
                    action: 'read',
                    watch_list_cd: '{idOrg}'
                }
            }
        }
    }
});
