Ext.define('AML.view.oes.OES321ListsOrg.Dialog.ListRO', {
    extend: 'Ext.window.Window',
    xtype: 'app-oes-oes321listsorg-dialog-list',
    controller: 'appoes321listsorgdialoglist',
    iconCls: 'icon-report',
    viewModel: {
        type: 'appoes321listsorgdialoglistvm'
    },
    bind: {
        title: 'Перечни организаций'
    },
    width: "70%",
    closable: true,
    bodyStyle: {
        padding: 0
    },
    listeners: {
        resize: function (wnd) {
            wnd.center();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'gridpanel',
                    maxHeight: 600,
                    scrollable: true,
                    bind: {
                        store: '{oes321listsorg}'
                    },
                    columns: [
                        {
                            text: 'ID элемента перечня',
                            stateId: 'WATCH_LIST_ORG_ID',
                            dataIndex: 'WATCH_LIST_ORG_ID',
                            flex: 0.5
                        },
                        {
                            text: 'Код перечня',
                            stateId: 'WATCH_LIST_CD',
                            dataIndex: 'WATCH_LIST_CD',
                            flex: 0.5
                        },
                        {
                            text: '№ п/п',
                            stateId: 'ORDER_NB',
                            dataIndex: 'ORDER_NB',
                            flex: 1
                        },
                        {
                            text: 'Наименование',
                            stateId: 'ORG_NM',
                            dataIndex: 'ORG_NM',
                            flex: 2
                        },
                        {
                            text: 'Адрес',
                            stateId: 'ADDR_TX',
                            dataIndex: 'ADDR_TX',
                            flex: 0.5
                        },
                        {
                            text: 'ИНН',
                            stateId: 'INN_NB',
                            dataIndex: 'INN_NB',
                            flex: 0.5
                        },
                        {
                            text: 'Номер счета',
                            stateId: 'ACCT_NB',
                            dataIndex: 'ACCT_NB',
                            flex: 1
                        },
                        {
                            xtype: 'booleancolumn',
                            text: 'Активно?',
                            stateId: 'ACTIVE_FL',
                            dataIndex: 'ACTIVE_FL',
                            trueText: 'Да',
                            falseText: 'Нет',
                            flex: 0.5
                        },
                        {
                            text: 'Начало действия',
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            stateId: 'START_DT',
                            dataIndex: 'START_DT',
                            flex: 0.5
                        },
                        {
                            text: 'Окончание действия',
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            stateId: 'END_DT',
                            dataIndex: 'END_DT',
                            flex: 1
                        },
                        {
                            text: 'Примечание',
                            stateId: 'NOTE_TX',
                            dataIndex: 'NOTE_TX',
                            flex: 1
                        },
                        {
                            text: 'Кто создал',
                            stateId: 'CREATED_BY',
                            dataIndex: 'CREATED_BY',
                            flex: 1
                        },
                        {
                            text: 'Когда создал',
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            stateId: 'CREATED_DATE',
                            dataIndex: 'CREATED_DATE',
                            flex: 0.5
                        },
                        {
                            text: 'Кто изменил',
                            stateId: 'MODIFIED_BY',
                            dataIndex: 'MODIFIED_BY',
                            flex: 0.5
                        },
                        {
                            text: 'Когда изменил',
                            stateId: 'MODIFIED_DATE',
                            dataIndex: 'MODIFIED_DATE',
                            xtype: 'datecolumn',
                            format: 'd.m.Y',
                            flex: 0.5
                        },
                        {
                            text: 'Код в источнике',
                            stateId: 'SRC_CD',
                            dataIndex: 'SRC_CD',
                            flex: 0.5
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});