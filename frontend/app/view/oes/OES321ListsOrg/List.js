Ext.define('AML.view.oes.OES321ListsOrg.List', {
    extend: 'Ext.grid.Panel',
    requires: [
        'AML.view.oes.OES321ListsOrg.ListModel',
        'AML.view.oes.OES321ListsOrg.ListController',
        'AML.view.oes.OES321ListsOrg.Form'
    ],
    xtype: 'app-oes-oes321listsorg',
    controller: 'oes321listsorg',
    viewModel: {
        type: 'oes321listsorgvmdl'
    },
    bind: {
        store: '{oes321listsorg}',
        title: '{title}'
    },
    stateful: true,
    stateId: 'oes321listsorg',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    columns: [
        {
            text: 'ID элемента перечня',
            stateId: 'WATCH_LIST_ORG_ID',
            dataIndex: 'WATCH_LIST_ORG_ID',
            flex: 0.5
        },
        {
            text: 'Код перечня',
            stateId: 'WATCH_LIST_CD',
            dataIndex: 'WATCH_LIST_CD',
            flex: 0.5
        },
        {
            text: '№ п/п',
            stateId: 'ORDER_NB',
            dataIndex: 'ORDER_NB',
            flex: 1
        },
        {
            text: 'Наименование',
            stateId: 'ORG_NM',
            dataIndex: 'ORG_NM',
            flex: 2
        },
        {
            text: 'Адрес',
            stateId: 'ADDR_TX',
            dataIndex: 'ADDR_TX',
            flex: 0.5
        },
        {
            text: 'ИНН',
            stateId: 'INN_NB',
            dataIndex: 'INN_NB',
            flex: 0.5
        },
        {
            text: 'Номер счета',
            stateId: 'ACCT_NB',
            dataIndex: 'ACCT_NB',
            flex: 1
        },
        {
            xtype: 'booleancolumn',
            text: 'Активно?',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            text: 'Начало действия',
            xtype: 'datecolumn',
            format: 'd.m.Y',
            stateId: 'START_DT',
            dataIndex: 'START_DT',
            flex: 0.5
        },
        {
            text: 'Окончание действия',
            xtype: 'datecolumn',
            format: 'd.m.Y',
            stateId: 'END_DT',
            dataIndex: 'END_DT',
            flex: 1
        },
        {
            text: 'Примечание',
            stateId: 'NOTE_TX',
            dataIndex: 'NOTE_TX',
            flex: 1
        },
        {
            text: 'Кто создал',
            stateId: 'CREATED_BY',
            dataIndex: 'CREATED_BY',
            flex: 1
        },
        {
            text: 'Когда создал',
            xtype: 'datecolumn',
            format: 'd.m.Y',
            stateId: 'CREATED_DATE',
            dataIndex: 'CREATED_DATE',
            flex: 0.5
        },
        {
            text: 'Кто изменил',
            stateId: 'MODIFIED_BY',
            dataIndex: 'MODIFIED_BY',
            flex: 0.5
        },
        {
            text: 'Когда изменил',
            stateId: 'MODIFIED_DATE',
            dataIndex: 'MODIFIED_DATE',
            xtype: 'datecolumn',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Код в источнике',
            stateId: 'SRC_CD',
            dataIndex: 'SRC_CD',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'//,
            // bind: {
            //     disabled: '{!idOrg}'
            // }
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        },
        '-',
        {
            name: 'scopeFiltersLists',
            reference: 'scopeFiltersLists',
            id: 'scopeFiltersLists',
            xtype: 'combobox',
            editable: false,
            queryMode: 'local',
            tpl: Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{WATCH_LIST_NM}</div>',
                '</tpl>'
            ),
            displayField: 'WATCH_LIST_NM',
            valueField: 'WATCH_LIST_CD',
            autoLoadOnValue: true,
            triggerClear: false,
            bind: {
                store: '{oes321typeorg}',
                value: '{idOrg}'
            },
            flex: 1
        },
        {
            itemId: 'clearListsOrg',
            text: '',
            iconCls: 'icon-backtothe-primitive',
            handler: 'clearOES321ListsOrgFilter'
        },
        {
            xtype: 'textfield',
            name: 'ORG_NAME',
            fieldLabel: 'Наименование',
            labelAlign: 'right',
            allowBlank: true,
            bind: {
                value: '{nameOrg}'
            }
        },
        {
            xtype: 'textfield',
            fieldLabel: 'ИНН',
            labelAlign: 'right',
            allowBlank: true,
            name: 'ORG_INN',
            bind: {
                value: '{innOrg}'
            }
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            flex: 0.5,
            checked: true,
            value: true,
            bind: {
                value: '{activeCB}'
            }
        },
        {
            itemId: 'showListsOrg',
            text: 'Показать',
            iconCls: 'icon-document-search',
            handler: 'applyOES321ListsOrgFilter'
        }
    ],
    listeners: {
        'afterrender': function (elem) {
            var self = this;
            // текущие права
            if (!self.controller.accessObject) return;

            var storeAccess = Ext.getStore('ObjectsAccess');
            if (!storeAccess) return;

            var i = storeAccess.findExact('id', self.controller.accessObject);

            // если доступ на чтение
            if (storeAccess.getAt(i).get('mode') === 'R') {
                elem.down('#create').setDisabled(true);
                elem.down('#remove').setDisabled(true);
            }
            elem.down('#clearListsOrg').setDisabled(false);
            elem.down('#showListsOrg').setDisabled(false);
        }
    }
});
