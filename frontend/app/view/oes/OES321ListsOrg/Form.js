Ext.define('AML.view.oes.OES321ListsOrg.Form', {
    extend: 'Ext.window.Window',
    requires: [
        'AML.view.oes.OES321ListsOrg.FormController',
        'AML.view.oes.OES321ListsOrg.FormModel',
        'AML.view.main.BaseFormViewModel',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox'
    ],
    xtype: 'app-oes-oes321listsorg-form',
    controller: 'oes321listsorgform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oes321listsorgformvm'
    },
    width: 1000,
    bind: {
        title: '{title}'
    },
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Информация об организации',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: '48%'
                    },
                    items: [
                        {
                            defaults: {
                                labelWidth: 180
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'WATCH_LIST_CD',
                                    fieldLabel: 'Код перечня',
                                    readOnly: true,
                                    inputWrapCls: '',
                                    fieldStyle: 'background:none',
                                    bind: {
                                        value: '{record.WATCH_LIST_CD}'
                                    },
                                    listeners: {
                                        afterrender: function (component, eOpts) {
                                            var idValue = this.up('app-oes-oes321listsorg-form').getViewModel().getParent().get('idOrg');
                                            // this.up().up().up().up().getViewModel().getParent().get('idOrg')
                                            component.setValue(idValue);
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'ORG_NM',
                                    fieldLabel: 'Наименование',
                                    // vtype : 'vRegexTextField',
                                    // maskRe: new RegExp('^[a-9]$'),
                                    // enforceMaxLength:true,
                                    maxLength: 255,
                                    bind: {
                                        value: '{record.ORG_NM}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'ADDR_TX',
                                    fieldLabel: 'Адрес',
                                    // allowBlank: true,
                                    maxLength: 255,

                                    bind: {
                                        value: '{record.ADDR_TX}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'ИНН',
                                    hideTrigger: true,
                                    allowBlank: false,
                                    name: 'INN_NB',
                                    // vtype : 'vRegexNumberField',
                                    // maskRe: new RegExp('^[0-9]$'),
                                    // enforceMaxLength:true,
                                    maxLength: 64,
                                    bind: {
                                        value: '{record.INN_NB}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 0.5
                        },
                        {
                            defaults: {
                                labelWidth: 160
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Номер счета',
                                    hideTrigger: true,
                                    allowBlank: true,
                                    vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 64,
                                    bind: {
                                        value: '{record.ACCT_NB}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Код в источнике',
                                    hideTrigger: true,
                                    inputWrapCls: '',
                                    fieldStyle: 'background:none',
                                    readOnly: true,
                                    bind: {
                                        value: '{record.SRC_CD}'
                                    }
                                },
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Примечание',
                                    allowBlank: true,
                                    maxLength: 2000,
                                    height: '100',
                                    bind: {
                                        value: '{record.NOTE_TX}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Период действия',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults:{
                                flex: 1,
                                xtype: 'datefield',
                                labelWidth: 130,
                                format: 'd.m.Y',
                                startDay: 1
                            },
                            items: [
                                {
                                    fieldLabel: 'Начало действия',
                                    reference: 'startDateListsOrg',
                                    bind: {
                                        value: '{record.START_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                    container.nextSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    fieldLabel: 'Окончание действия',
                                    reference: 'endDateListsOrg',
                                    margin: '0 0 0 10',
                                    bind: {
                                        value: '{record.END_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                    container.validateValue(false);
                                                    container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                }
                                                container.previousSibling('datefield').focus();
                                                container.focus();
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                    container.validateValue(false);
                                                    container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                }
                                            }
                                        }
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Активно?',
                            margin: '0 0 0 30',
                            bind: {
                                value: '{record.ACTIVE_FL}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Аудит',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто создал',
                                    bind: {
                                        value: '{record.CREATED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда создал',
                                    bind: {
                                        value: '{record.CREATED_DATE:date("d.m.Y h:m:s")}'
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто изменил',
                                    bind: {
                                        value: '{record.MODIFIED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда изменил',
                                    bind: {
                                        value: '{record.MODIFIED_DATE:date("d.m.Y h:m:s")}'
                                    }
                                }

                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveForm',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});