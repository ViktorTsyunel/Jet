Ext.define('AML.view.oes.OES321ListsOrg.FormController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.oes321listsorgform',
    config: {
        listen: {
            component: {
                '#saveForm': {
                    click: 'onClickSaveFormListsOrg'
                },
                '#cancelForm': {
                    click: 'onClickCancelFormListsOrg'
                }
            }
        }
    },

    init: function () {
        var self = this,
            vm = self.getViewModel(),
            idOrg = vm.get('idOrg');
        if(!idOrg) {
            return false;
        }
    },

    callbackOES321ListsOrg: function (records, operation, success) {
        var vm = this.getViewModel(),
            controler = this.controller,
            oldRecord = vm.get('record'),
            form = this.down('form').getForm(),
            view = vm.getView(),
            recData = records.getData(),
            id = records.get('_id_'),
            store = vm.getParent().getStore('oes321listsorg');
        store.load();
        var newRecord = store.getById(id),
            record = newRecord.getData(),
            newRecordSync = store.sync();
        vm.set('record', newRecord);
    },
    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveFormListsOrg: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView(),
            form = window.down('form'),
            vm = window.getViewModel(),
            record = (vm) ? vm.get('record') : false;

        if (!window || !this.isChanges(window)) return;

        // валидация даты старта и окончания операции
        if (this.valideteDateForm(window, 'startDateListsOrg', 'endDateListsOrg')) {
            return;
        }
        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false, this.callbackOES321ListsOrg);
    },

    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelFormListsOrg: function (button, eOpts) {
        var self = this,
            window = this.getView(),
            controller = this,
            fnSaveForm = this.saveForm,
            form = window.down('form'),
            vm = window.getViewModel(),
            record = (vm) ? vm.get('record') : false;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    // валидация даты старта и окончания операции
                    if (self.valideteDateForm(window, 'startDateListsOrg', 'endDateListsOrg')) {
                        return;
                    }
                    fnSaveForm(window, controller, true, this.callbackOES321ListsOrg);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    }
});
