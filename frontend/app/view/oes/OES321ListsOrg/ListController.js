Ext.define('AML.view.oes.OES321ListsOrg.ListController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oes321listsorg',
    childForm: 'app-oes-oes321listsorg-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'WATCH_LIST_ORG_ID', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'WLOrg', //код объекта доступа для данной формы,

    applyOES321ListsOrgFilter: function () {
        var self = this,
            vm = this.getViewModel(),
            filters = [],
            activeCB = vm.get('activeCB'),
            nameOrg = vm.get('nameOrg'),
            innOrg = vm.get('innOrg'),
            idOrg = vm.getView().lookupReference('scopeFiltersLists').getValue(),
            store = this.getStore('oes321listsorg');

        if (!store.isLoaded()) return;

        if (activeCB) {
            filters.push({
                property: 'ACTIVE_FL',
                value: true
            });
        }

        if (innOrg) {
            filters.push({
                property: 'INN_NB',
                value: innOrg
            });
        }
        if (nameOrg) {
            filters.push({
                property: 'ORG_NM',
                value: nameOrg
            });
        }
        if (idOrg) {
            filters.push({
                property: 'WATCH_LIST_CD',
                value: idOrg
            });
        }
        store.load({
            scope: vm.getView(),
            params: {
                form: 'WatchListOrg',
                action: 'read',
                WATCH_LIST_CD: idOrg,
                ORG_NM: nameOrg,
                INN_NB: innOrg
            }
        });

        store.clearFilter();
        store.filter(filters);
        // store.load();
        // onStoreRefresh();
    },

    clearOES321ListsOrgFilter: function () {
        var self = this,
            vm = this.getViewModel(),
            filters = [],
            activeCB = vm.get('activeCB'),
            nameOrg = vm.get('nameOrg'),
            innOrg = vm.get('innOrg'),
            idOrg = vm.getView().lookupReference('scopeFiltersLists').getValue(),
            store = this.getStore('oes321listsorg');
        vm.set('idOrg', null);
        if (!store.isLoaded()) return;

        if (activeCB) {
            filters.push({
                property: 'ACTIVE_FL',
                value: true
            });
        }

        if (innOrg) {
            filters.push({
                property: 'INN_NB',
                value: null
            });
        }
        if (nameOrg) {
            filters.push({
                property: 'ORG_NM',
                value: null
            });
        }
        if (idOrg) {
            filters.push({
                property: 'watch_list_cd',
                value: null
            });
        }
        store.load({
            scope: vm.getView(),
            params: {
                form: 'WatchListOrg',
                action: 'read',
                watch_list_cd: null,
                ORG_NM: null,
                INN_NB: null
            }
        });

        store.clearFilter();
        store.filter(filters);
        // store.load();
        // onStoreRefresh();
    }
});
