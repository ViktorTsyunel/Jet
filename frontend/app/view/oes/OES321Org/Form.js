Ext.define('AML.view.oes.OES321Org.Form', {
    extend: 'Ext.window.Window',
    xtype: 'app-oes-oes321org-form',
    controller: 'oes321orgform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oes321orgformvm'
    },
    width: 1000,
    bind: {
        title: '{title}'
    },
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Кредитная организация, передающая сведения',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: '48%'
                    },
                    items: [
                        {
                            defaults: {
                                labelWidth: 180
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    store: 'TB',
                                    editable: false,
                                    name: 'TB_CODE',
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="id &gt; 0">{id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Код ТБ (TB_CODE)',
                                    displayField: 'id',
                                    valueField: 'id',
                                    triggerClear: true,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.TB_CODE}'
                                    },
                                    listeners: {
                                        scope: this,
                                        change: function (field, newValue, oldValue, eOpts) {
                                            var tbosboesField = field.up('window').down('combobox[name=OSB_CODE]'),
                                                tbnameField = field.up('window').down('textfield[name=TB_NAME]'),
                                                tbosbStore = tbosboesField.getStore(),
                                                tbCode = parseInt(field.up('window').getViewModel().data.record.get('TB_CODE'), 10),
                                                tb = Ext.data.StoreManager.lookup('TB'),
                                                record = tb.findRecord('id', newValue);
                                            if (record && (newValue != tbCode)) {
                                                field.up('window').getViewModel().set('selectedTBName', record.get('TB_NAME'))
                                            }
                                            tbosbStore.clearFilter();
                                            if (newValue != tbCode) {
                                                tbosboesField.clearValue();
                                            }
                                            if (newValue === null || newValue === '') tbnameField.setValue('');
                                            tbosbStore.filterBy(function (record) {
                                                return record.get("tb_id") === newValue;
                                            }, this);
                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    store: 'TBOSB',
                                    name: 'OSB_CODE',
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: Ext.create('Ext.XTemplate',
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' +
                                        '<tpl if="osb_id &gt; 0">{osb_id} - {label}' +
                                        '<tpl else>{label}</tpl>' +
                                        '</div>',
                                        '</tpl>'
                                    ),
                                    fieldLabel: 'Код ОСБ (OSB_CODE)',
                                    displayField: 'osb_id',
                                    valueField: 'osb_id',
                                    triggerClear: true,
                                    allowBlank: true,
                                    // vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    bind: {
                                        value: '{record.OSB_CODE}'
                                    },
                                    listeners: {
                                        scope: this,
                                        change: function (tbField, newValue, oldValue, eOpts) {
                                            var tbStore = tbField.getStore(),
                                                tbTBField = tbField.up('window').down('combobox[name=TB_CODE]'),
                                                tbCode = parseInt(tbTBField.getValue(), 10),
                                                record = tbStore.findRecord('id', (tbCode + "-" + newValue)),
                                                tb = Ext.data.StoreManager.lookup('TB'),
                                                tb_record = tb.findRecord('id', tbTBField.getValue());
                                            var vm = tbField.up('window').getViewModel();
                                            if (record && (newValue != tbCode)) {
                                                if (oldValue || vm.get('record').dirty) {
                                                    vm.set('selectedTBName', record.get('label'))
                                                }
                                            }
                                            if (newValue === null || newValue === '') {
                                                if (tb_record) {
                                                    vm.set('selectedTBName', tb_record.get('label'))
                                                } else {
                                                    vm.set('selectedTBName', null)
                                                }
                                            }
                                            tbStore.filterBy(function (record) {
                                                return record.get("tb_id") === tbTBField.value;
                                            }, this);
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'TB_NAME',
                                    fieldLabel: 'Имя филиала (TB_NAME)',
                                    allowBlank: true,
                                    bind: {
                                        value: '{selectedTBName}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: '№ филиала (NUMBF_S)',
                                    hideTrigger: true,
                                    allowBlank: false,
                                    vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 4,
                                    bind: {
                                        value: '{record.NUMBF_S}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Рег. номер КО (REGN)',
                                    hideTrigger: true,
                                    vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 4,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.REGN}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 0.5
                        },
                        {
                            defaults: {
                                labelWidth: 160
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'БИК (BIK_S)',
                                    hideTrigger: true,
                                    allowBlank: false,
                                    vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 9,
                                    bind: {
                                        value: '{record.BIK_S}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'ИНН (ND_KO)',
                                    hideTrigger: true,
                                    vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 10,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.ND_KO}'
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    flex: 1,
                                    labelWidth: 160,
                                    labelAlign: 'left',
                                    tooltip: {
                                        text: "Код субъекта РФ по ОКАТО. Для лиц, зарегистрированных в иностранном государстве – ‘00’."
                                    },
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{OKATO_CD}',
                                        '</tpl>'
                                    ],
                                    fieldLabel: 'Код по ОКАТО (KTU_S)',
                                    displayField: 'OKATO_CD',
                                    valueField: 'OKATO_CD',
                                    forceSelection: true,
                                    triggerClear: true,
                                    allowBlank: false,
                                    bind: {
                                        store: '{OKATOStoreKtuS}',
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.KTU_S}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Тел. ответственного (TEL)',
                                    maxLength: 16,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.TEL}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Филиал кредитной организации, предоставляющий сведения, но не передающий самостоятельно ОЭС',
                    layout: {
                        type: 'vbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        width: '100%',
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            defaults:{
                                flex: 1,
                                border: 0
                            },
                            items: [
                                {
                                    xtype: 'checkboxfield',
                                    labelWidth: 310,
                                    fieldLabel: 'Сведения предоставлены филиалом Банка (BRANCH)',
                                    bind: {
                                        value: '{record.BRANCH_FL}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            defaults:{
                                flex: 1,
                                border: 0,
                                labelWidth: 150
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: '№ филиала (NUMBF_SS)',
                                    hideTrigger: true,
                                    vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 4,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.NUMBF_SS}'
                                    }
                                },
                                {flex: 0.1},
                                {
                                    xtype: 'textfield',
                                    labelWidth: 80,
                                    fieldLabel: 'БИК (BIK_SS)',
                                    hideTrigger: true,
                                    vtype : 'vRegexNumberField',
                                    maskRe: new RegExp('^[0-9]$'),
                                    enforceMaxLength:true,
                                    maxLength: 9,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.BIK_SS}'
                                    }
                                },
                                {flex: 0.1},
                                {
                                    xtype: 'combobox',
                                    flex: 1,
                                    labelWidth: 100,
                                    labelAlign: 'left',
                                    tooltip: {
                                        text: "Код субъекта РФ по ОКАТО. Для лиц, зарегистрированных в иностранном государстве – ‘00’."
                                    },
                                    editable: true,
                                    queryMode: 'local',
                                    tpl: [
                                        '<tpl for=".">',
                                        '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{OKATO_CD} - {OKATO_OBJ_NM}</div>',
                                        '</tpl>'
                                    ],
                                    displayTpl: [
                                        '<tpl for=".">',
                                        '{OKATO_CD}',
                                        '</tpl>'
                                    ],
                                    fieldLabel: 'Код по ОКАТО (KTU_SS)',
                                    displayField: 'OKATO_CD',
                                    valueField: 'OKATO_CD',
                                    forceSelection: true,
                                    triggerClear: true,
                                    allowBlank: false,
                                    bind: {
                                        store: '{OKATOStoreKtuSs}',
                                        readOnly: '{OES_READONLY_FL_CONVERSION}',
                                        value: '{record.KTU_SS}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Период действия',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults:{
                                flex: 1,
                                xtype: 'datefield',
                                labelWidth: 130,
                                format: 'd.m.Y',
                                startDay: 1
                            },
                            items: [
                                {
                                    fieldLabel: 'Начало действия',
                                    reference: 'startDateOrg',
                                    bind: {
                                        value: '{record.START_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                    container.nextSibling('datefield').focus();
                                                    container.focus();
                                                }
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.nextSibling('datefield').getValue()) {
                                                    if (container.nextSibling('datefield').getValue() < container.getValue()) {
                                                        container.validateValue(false);
                                                        container.setErrorMsgTxt('Дата начала действия не может быть больше даты окончания.');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    fieldLabel: 'Окончание действия',
                                    reference: 'endDateOrg',
                                    margin: '0 0 0 10',
                                    bind: {
                                        value: '{record.END_DT}'
                                    },
                                    listeners: {
                                        validitychange: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                    container.validateValue(false);
                                                    container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                }
                                                container.previousSibling('datefield').focus();
                                                container.focus();
                                            }
                                        },
                                        blur: function (container, value, eOpts) {
                                            if (value) {
                                                if (container.previousSibling('datefield').getValue() > container.getValue()) {
                                                    container.validateValue(false);
                                                    container.setErrorMsgTxt('Дата окончания действия не может быть меньше даты начала');
                                                }
                                            }
                                        }
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Активно?',
                            margin: '0 0 0 30',
                            bind: {
                                value: '{record.ACTIVE_FL}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Аудит',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: 350
                    },
                    items: [
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто создал',
                                    bind: {
                                        value: '{record.CREATED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда создал',
                                    bind: {
                                        value: '{record.CREATED_DATE}'
                                    },
                                    renderer: function (value, field) {
                                        return Ext.Date.format(value,'d.m.Y');
                                    }
                                }

                            ]
                        },
                        {
                            xtype: 'container',
                            flex: 1
                        },
                        {
                            defaults:{
                                flex: 1,
                                xtype: 'displayfield',
                                labelWidth: 130,
                                width: 245
                            },
                            items: [
                                {
                                    fieldLabel: 'Кто изменил',
                                    bind: {
                                        value: '{record.MODIFIED_BY}'
                                    }
                                },
                                {
                                    fieldLabel: 'Когда изменил',
                                    bind: {
                                        value: '{record.MODIFIED_DATE}'
                                    },
                                    renderer: function (value, field) {
                                        return Ext.Date.format(value,'d.m.Y');
                                    }
                                }

                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveForm',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});