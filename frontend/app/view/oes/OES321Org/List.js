Ext.define('AML.view.oes.OES321Org.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-oes-oes321org',
    controller: 'oes321org',
    viewModel: {
        type: 'oes321orgvmdl'
    },
    bind: {
        store: '{oes321org}',
        title: '{title}'
    },
    stateful: true,
    stateId: 'oes321org',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    columns: [
        {
            text: 'ID записи',
            identifier: true,
            stateId: 'OES_321_ORG_ID',
            dataIndex: 'OES_321_ORG_ID',
            flex: 0.5
        },
        {
            text: 'Код ТБ',
            stateId: 'TB_CODE',
            dataIndex: 'TB_CODE',
            flex: 0.5
        },
        {
            text: 'Код ОСБ',
            stateId: 'OSB_CODE',
            dataIndex: 'OSB_CODE',
            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.data.OSB_CODE === 0) {
                    return "";
                } else {
                    return record.data.OSB_CODE;
                }
            },
            flex: 1
        },
        {
            text: 'Наименование филиала',
            stateId: 'TB_NAME',
            dataIndex: 'TB_NAME',
            flex: 2
        },
        {
            text: 'Рег. номер КО',
            stateId: 'REGN',
            dataIndex: 'REGN',
            flex: 0.5
        },
        {
            text: '№ филиала КО',
            stateId: 'NUMBF_S',
            dataIndex: 'NUMBF_S',
            flex: 0.5
        },
        {
            text: 'БИК КО',
            stateId: 'BIK_S',
            dataIndex: 'BIK_S',
            flex: 1
        },
        {
            text: 'ИНН КО',
            stateId: 'ND_KO',
            dataIndex: 'ND_KO',
            flex: 1
        },
        {
            text: 'Код ОКАТО ТУ',
            stateId: 'KTU_S',
            dataIndex: 'KTU_S',
            flex: 0.5
        },
        {
            text: 'Тел. ответственного',
            stateId: 'TEL',
            dataIndex: 'TEL',
            flex: 1
        },
        {
            text: 'Код ОКАТО ФКО ТУ',
            stateId: 'KTU_SS',
            dataIndex: 'KTU_SS',
            flex: 0.5
        },
        {
            text: 'БИК ФКО',
            stateId: 'BIK_SS',
            dataIndex: 'BIK_SS',
            flex: 1
        },
        {
            text: 'Номер ФКО',
            stateId: 'NUMBF_SS',
            dataIndex: 'NUMBF_SS',
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Активно?',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Сведения предоставлены филиалом Банка (BRANCH)',
            stateId: 'BRANCH_FL',
            dataIndex: 'BRANCH_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Начало действия',
            stateId: 'START_DT',
            dataIndex: 'START_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Окончание действия',
            stateId: 'END_DT',
            dataIndex: 'END_DT',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто создал',
            stateId: 'CREATED_BY',
            dataIndex: 'CREATED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда создал',
            stateId: 'CREATED_DATE',
            dataIndex: 'CREATED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        },
        {
            text: 'Кто изменил',
            stateId: 'MODIFIED_BY',
            dataIndex: 'MODIFIED_BY',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            text: 'Когда изменил',
            stateId: 'MODIFIED_DATE',
            dataIndex: 'MODIFIED_DATE',
            format: 'd.m.Y',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        },
        '-',
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            flex: 0.5,
            checked: true,
            value: true,
            bind: {
                value: '{activeCB}'
            }
        }
    ]
});
