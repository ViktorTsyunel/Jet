Ext.define('AML.view.oes.OES321Org.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321orgformvm',
    data: {
        title: 'Информация о КО',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        selectedTBName: {
            bind: {
                TB_NM:  '{record.TB_NAME}'
            },
            get: function (data) {
                var  nmCode = data.TB_NM;

                if (nmCode) {
                    return nmCode;
                } else {
                    return '';
                }
            },
            set: function (value) {
                if (value) {
                    this.set('record.TB_NAME', value);
                }
            }
        }
    },
    stores: {
        // Код по OKATO KTU_S
        OKATOStoreKtuS: {
            type: 'chained',
            source: 'OKATO'
        },
        // Код по OKATO KTU_SS
        OKATOStoreKtuSs: {
            type: 'chained',
            source: 'OKATO'
        },

        TBOSBOES: {
            model: 'AML.model.TBOSB',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            autoSync: true,
            proxy: {
                url: common.globalinit.ajaxUrl,
                type: 'ajax',
                paramsAsJson: true,
                noCache: false,
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    rootProperty: 'data'
                },
                extraParams: {
                    form: 'TBOSB',
                    action: 'read'
                }
            },
            sorters: [
                {
                    property: 'ORG_NM',
                    direction: 'ASC'
                }
            ]
        }
    }
});
