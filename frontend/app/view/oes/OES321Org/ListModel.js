Ext.define('AML.view.oes.OES321Org.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321orgvmdl',
    data: {
        title: 'Информация о КО',
        activeCB: true,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        filters: {
            bind: {
                activeCB: '{activeCB}'
            },
            get: function(data) {
                var store = this.getStore('oes321org'),
                    filters = [],
                    activeCB = data.activeCB;

                if (activeCB) {
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник Информация о КО
        oes321org: {
            model: 'AML.model.OES321Org',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'OES_321_ORG_ID',
                    direction: 'ASC'
                }
            ],
            filters: '{filters}'
        }
    }
});
