Ext.define('AML.view.oes.OES321Org.ListController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oes321org',
    childForm: 'app-oes-oes321org-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'OES_321_ORG_ID', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'OES321Org' //код объекта доступа для данной формы
});
