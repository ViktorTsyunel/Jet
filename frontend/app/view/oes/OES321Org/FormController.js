Ext.define('AML.view.oes.OES321Org.FormController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.oes321orgform',
    config: {
        listen: {
            component: {
                '#saveForm': {
                    click: 'onClickSaveFormOrg'
                },
                '#cancelForm': {
                    click: 'onClickCancelFormOrg'
                }
            }
        }
    },

    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveFormOrg: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView(),
            form = window.down('form'),
            vm = window.getViewModel(),
            record = (vm) ? vm.get('record') : false;

        if (!window || !this.isChanges(window)) return;

        // валидация даты старта и окончания операции
        if (this.valideteDateForm(window, 'startDateOrg', 'endDateOrg')) {
            return;
        }
        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false, this.callRefreshTBStore);
    },

    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelFormOrg: function (button, eOpts) {
        var self = this,
            window = this.getView(),
            controller = this,
            fnSaveForm = this.saveForm,
            form = window.down('form'),
            vm = window.getViewModel(),
            record = (vm) ? vm.get('record') : false;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    // валидация даты старта и окончания операции
                    if (self.valideteDateForm(window, 'startDateOrg', 'endDateOrg')) {
                        return;
                    }
                    fnSaveForm(window, controller, true, this.callRefreshTBStore);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    },

    /**
     * обновления хранилища TBStore
     * @param respond
     */
    callRefreshTBStore: function(respond) {
        var self = this,
            record,
            vmMain = this.lookupViewModel(true),
            viewMain = vmMain.getView(),
            cMain = viewMain.getController(),
            store = (viewMain.down('app-oes321detailspanel-operationtab')) ? viewMain.down('app-oes321detailspanel-operationtab').getViewModel().getStore('TBStore') : false;
        if (store) {
            var index = store.find('_ID_', respond.getId());
            if (index && index >= 0) {
                record = store.getAt(index);
                record.set(respond.getData());
            }
        }
    },

    rebuildOKATOStoreOrg: function(store) {
        // своя сортировка
        var item0 = store.findRecord('_ID_', '0', 0, false, false, true),
            item00 = store.findRecord('_ID_', '00', 0, false, false, true),
            records = [],
            addItem0 = item0,
            addItem00 = item00;
        store.remove(item0);
        store.remove(item00);
        store.sort('OKATO_OBJ_NM', 'ASC');
        store.sorters.clear();
        store.each(function(record){
            records.push(record);
        });
        store.removeAll(true);
        store.add([item0, item00]);
        Ext.each(records, function (record) {
            store.add(record);
        });

        return store;
    }
});
