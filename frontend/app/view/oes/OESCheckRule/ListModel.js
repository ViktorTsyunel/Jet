Ext.define('AML.view.oes.OESCheckRule.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oescheckrulevmdl',
    data: {
        title: 'Правила проверки ОЭС',
        activeCB: true,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        filters: {
            bind: {
                activeCB: '{activeCB}'
            },
            get: function(data) {
                var store = this.getStore('oescheckrule'),
                    filters = [],
                    activeCB = data.activeCB;

                if (activeCB) {
                    filters.push({
                        property: 'ACTIVE_FL',
                        value: true
                    });
                }
                store && store.clearFilter();
                return filters;
            }
        }
    },
    stores: {
        // Справочник КО участники операций
        oescheckrule: {
            model: 'AML.model.OESCheckRule',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'RULE_SEQ_ID',
                    direction: 'ASC'
                }
            ],
            filters: '{filters}'
        }
    }
});
