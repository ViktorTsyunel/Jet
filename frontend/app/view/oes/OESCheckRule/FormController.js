Ext.define('AML.view.oes.OESCheckRule.FormController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.oescheckruleform',
    requires: [
        'Ext.data.StoreManager'
    ],

    config: {
        listen: {
            component: {
                '#saveForm': {
                    click: 'onClickSaveFormOESCheckRule'
                },
                '#cancelForm': {
                    click: 'onClickCancelFormOESCheckRule'
                }
            }
        }
    },
    callbackOESCheckRule: function (records, operation, success) {
        var vm = this.getViewModel(),
            controler = this.controller,
            oldRecord = vm.get('record'),
            form = this.down('form').getForm(),
            view = vm.getView(),
            recData = records.getData(),
            id = records.get('RULE_SEQ_ID'),
            store = vm.getParent().getStore('oescheckrule');
        store.load();
            var newRecord = store.getById(id),
            record = newRecord.getData(),
            newRecordSync = store.sync();
        vm.set('record', newRecord);
    },
    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveFormOESCheckRule: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView();
        if (!window || !this.isChanges(window)) return;

        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false, this.callbackOESCheckRule);
    },
    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelFormOESCheckRule: function (button, eOpts) {
        var window = this.getView(),
            controller = this,
            fnSaveForm = this.saveForm;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    fnSaveForm(window, controller, true, this.callbackOESCheckRule);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    }
});
