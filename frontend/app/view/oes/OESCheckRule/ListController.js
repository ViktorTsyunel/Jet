Ext.define('AML.view.oes.OESCheckRule.ListController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oescheckrule',
    childForm: 'app-oes-oescheckrule-form', /*xtype формы редактирования, открываемой из списка*/
    IdColumn: 'RULE_SEQ_ID', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'OESCheckRule', //код объекта доступа для данной формы
    /**
     * Фильтры
     */
    onChangeFilterOESColumn: function (field, event, eOpts) {
        var store = this.getStore('oescheckrule');
        if (!Ext.isObject(store)) {
            store = Ext.StoreManager.lookup('oescheckrule');
        }
        if (field.value) {
            store.addFilter({
                property: 'COL_LIST_TX',
                value: new RegExp(field.value, "i")
            });
        } else {
            store.removeFilter('COL_LIST_TX');
        }
    }
});
