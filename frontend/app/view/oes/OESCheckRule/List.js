Ext.define('AML.view.oes.OESCheckRule.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-oes-oescheckrule',
    controller: 'oescheckrule',
    viewModel: {
        type: 'oescheckrulevmdl'
    },
    bind: {
        store: '{oescheckrule}',
        title: '{title}'
    },
    stateful: true,
    stateId: 'oescheckrule',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    columns: [
        {
            text: 'Номер правила',
            identifier: true,
            stateId: 'RULE_SEQ_ID',
            dataIndex: 'RULE_SEQ_ID',
            flex: 0.5
        },
        {
            text: 'Код формы',
            stateId: 'FORM_CD',
            dataIndex: 'FORM_CD',
            flex: 3
        },
        {
            text: 'Описание',
            stateId: 'DESC_TX',
            dataIndex: 'DESC_TX',
            flex: 2
        },
        {
            text: 'Колонки ОЭС',
            stateId: 'COL_LIST_TX',
            dataIndex: 'COL_LIST_TX',
            flex: 0.5
        },
        {
            text: 'Блоки ОЭС',
            stateId: 'BLOCK_LIST_TX',
            dataIndex: 'BLOCK_LIST_TX',
            flex: 0.5
        },
        {
            xtype: 'booleancolumn',
            text: 'Критично?',
            stateId: 'CRITICAL_FL',
            dataIndex: 'CRITICAL_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.2
        },
        {
            xtype: 'booleancolumn',
            text: 'Активно?',
            stateId: 'ACTIVE_FL',
            dataIndex: 'ACTIVE_FL',
            trueText: 'Да',
            falseText: 'Нет',
            flex: 0.2
        },
        {
            text: 'Примечание',
            stateId: 'NOTE_TX',
            dataIndex: 'NOTE_TX',
            flex: 1
        },
        {
            text: 'Ошибка',
            stateId: 'ERR_FL',
            dataIndex: 'ERR_FL',
            xtype: 'templatecolumn',
            tpl: Ext.create('Ext.XTemplate',
                '<tpl if="ERR_FL == true">',
                '<div class="icon icon-bullet-red" data-qtip="{ERR_MESS_TX}">&nbsp;</div>',
                '</tpl>'
            ),
            width: 55
        },
        {
            text: 'Кто создал',
            stateId: 'CREATED_BY',
            dataIndex: 'CREATED_BY',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Когда создал',
            stateId: 'CREATED_DATE',
            dataIndex: 'CREATED_DATE',
            format: 'd.m.Y H:i:s',
            flex: 0.5
        },
        {
            text: 'Кто изменил',
            stateId: 'MODIFIED_BY',
            dataIndex: 'MODIFIED_BY',
            flex: 0.5
        },
        {
            xtype: 'datecolumn',
            text: 'Когда изменил',
            stateId: 'MODIFIED_DATE',
            dataIndex: 'MODIFIED_DATE',
            format: 'd.m.Y H:i:s',
            flex: 0.5
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        },
        '-',
        {
            xtype: 'checkbox',
            fieldLabel: 'Только активные',
            labelWidth: 100,
            width: 125,
            checked: true,
            value: true,
            bind: {
                value: '{activeCB}'
            }
        },
        '-',
        {
            itemId: 'filterOESColumn',
            xtype: 'textfield',
            tooltip: {
                title: 'Колонки ОЭС'
            },
            emptyText: 'Текст Колонки ОЭС',
            listeners: {
                change: 'onChangeFilterOESColumn'
            },
            triggerClear: true,
            flex: 0.5
        },
        '->'
    ]
});
