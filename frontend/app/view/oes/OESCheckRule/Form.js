Ext.define('AML.view.oes.OESCheckRule.Form', {
    extend: 'Ext.window.Window',
    xtype: 'app-oes-oescheckrule-form',
    controller: 'oescheckruleform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oescheckruleformvm'
    },
    maxWidth: Ext.getBody().getViewSize().width*0.8,
    maxHeight:Ext.getBody().getViewSize().height*0.8,
    bind: {
        title: '{title}'
    },
    reference: 'refOEScheckruleForm',
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    listeners: {
        afterrender: function (form) {
            form.focus();
        }
    },
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            autoHeight:true,
            autoScroll:true,
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretchmax'
                    },
                    margin: '0 5px 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            width: 250,
                            labelWidth: 90,
                            flex: 1,
                            labelAlign: 'left',
                            fieldLabel: '<b>Код формы</b>',
                            editable: false,
                            queryMode: 'local',
                            tpl: [
                                '<tpl for=".">',
                                '<div class="x-boundlist-item" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{id} - {text}</div>',
                                '</tpl>'
                            ],
                            displayTpl: [
                                '<tpl for=".">',
                                '{id}',
                                '</tpl>'
                            ],
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            multiSelect: false,
                            matchFieldWidth: false,
                            allowBlank: false,
                            bind: {
                                readOnly: '{OES_READONLY_FL_CONVERSION}',
                                store: '{storeFormCd}',
                                value: '{record.FORM_CD}'
                            },
                            listeners: {
                                afterrender: function (form) {
                                    form.focus();
                                }
                            }
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: '<b>Критично?</b>',
                            labelWidth: 70,
                            width: 120,
                            labelAlign: 'left',
                            margin: '0 30 0 20',
                            bind: {
                                value: '{record.CRITICAL_FL}'
                            }
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: '<b>Активно?</b>',
                            labelWidth: 70,
                            width: 120,
                            labelAlign: 'left',
                            margin: '0 0 0 30',
                            bind: {
                                value: '{record.ACTIVE_FL}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretchmax',
                        pack: 'center'
                    },
                    margin: '10 0 10 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            width: '100%',
                            margin: '0 5 0 0',
                            labelWidth: 90,
                            labelAlign: 'left',
                            fieldLabel: '<b>Описание</b>',
                            validateBlank: true,
                            allowBlank: false,
                            bind: {
                                value: '{record.DESC_TX}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'center'
                    },
                    margin: '0 5px 0 0',
                    height: 200,
                    items: [
                        {
                            xtype: 'textareafield',
                            width: '100%',
                            flex: 1,
                            labelAlign: 'top',
                            margin: '0 3 0 0',
                            fieldLabel: '<b>SQL-проверки</b>',
                            validateBlank: true,
                            allowBlank: false,
                            bind: {
                                value: '{record.CHECK_SQL_TX}'
                            }
                        },
                        {
                            xtype: 'textareafield',
                            width: '100%',
                            flex: 1,
                            labelAlign: 'top',
                            margin: '0 0 0 3',
                            fieldLabel: '<b>SQL-сообщения</b>',
                            validateBlank: true,
                            allowBlank: false,
                            bind: {
                                value: '{record.MSG_SQL_TX}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    margin: '10 0 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            width: '100%',
                            margin: '0 5 0 0',
                            labelWidth: 90,
                            labelAlign: 'left',
                            fieldLabel: '<b>Колонки ОЭС</b>',
                            validateBlank: true,
                            allowBlank: false,
                            bind: {
                                value: '{record.COL_LIST_TX}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    margin: '10 0 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            width: '100%',
                            margin: '0 5 0 0',
                            labelWidth: 90,
                            labelAlign: 'left',
                            fieldLabel: '<b>Блоки ОЭС</b>',
                            bind: {
                                value: '{record.BLOCK_LIST_TX}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox'
                    },
                    margin: '10 0 0 0',
                    defaults: {
                        border: 0
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            labelWidth: 90,
                            margin: '0 5 0 0',
                            labelAlign: 'left',
                            fieldLabel: '<b>Примечание</b>',
                            width: '100%',
                            validateBlank: true,
                            allowBlank: false,
                            bind: {
                                value: '{record.NOTE_TX}'//,
                                // width: '{isWidthErrMessTx}',
                                //margin: '{isMarginErrMessTx}'
                            }
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            inputWrapCls: 'border-disable',
                            // margin: '10 0 0 0',
                            width: 5,
                            height: 30,
                            hidden: true,
                            bind: {
                                hidden: '{isHiddenErrMessTx}',
                                errorMsgTxt: '{record.ERR_MESS_TX}'
                            }//,
                            //errorDismissDelay: 0
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    margin: '0 5 0 0',
                    items: [
                        {
                            xtype: 'fieldset',
                            title: '<b>Аудит</b>',
                            layout: {
                                type: 'hbox',
                                align: 'left',
                                pack: 'center'
                            },
                            padding: 10,
                            flex: 1,
                            defaults:{
                                flex: 1,
                                xtype: 'container',
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                width: 350
                            },
                            items: [
                                {
                                    defaults: {
                                        flex: 1,
                                        xtype: 'displayfield',
                                        labelAlign: 'left',
                                        width: 245,
                                        align: 'left'
                                    },
                                    items: [
                                        {
                                            fieldLabel: 'Кто создал',
                                            labelWidth: 75,
                                            bind: {
                                                value: '{record.CREATED_BY}'
                                            }
                                        },
                                        {
                                            fieldLabel: 'Когда создал',
                                            labelWidth: 80,
                                            bind: {
                                                value: '{record.CREATED_DATE}'
                                            },
                                            renderer: function (value, field) {
                                                return Ext.Date.format(value,'d.m.Y H:i:s');
                                            }
                                        },
                                        {
                                            xtype: 'container',
                                            flex: 0.1
                                        },
                                        {
                                            fieldLabel: 'Кто изменил',
                                            labelWidth: 80,
                                            bind: {
                                                value: '{record.MODIFIED_BY}'
                                            }
                                        },
                                        {
                                            fieldLabel: 'Когда изменил',
                                            labelWidth: 90,
                                            bind: {
                                                value: '{record.MODIFIED_DATE}'
                                            },
                                            renderer: function (value, field) {
                                                return Ext.Date.format(value, 'd.m.Y H:i:s');
                                            }
                                        }

                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveForm',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть',
                    disabled: false
                }
            ]
        }
    ]
});