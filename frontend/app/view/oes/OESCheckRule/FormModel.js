Ext.define('AML.view.oes.OESCheckRule.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oescheckruleformvm',
    data: {
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    formulas: {
        title: function () {
            var title = 'Правило проверки №' + this.get('record').data.RULE_SEQ_ID;
            return title;
        },
        isWidthErrMessTx: {
            bind: {
                ERR_FL: '{record.ERR_FL}'
            },
            get: function(data){
                return (data.ERR_FL) ? '98%' : '100%';
            }
        },
        isMarginErrMessTx: {
            bind: {
                ERR_FL: '{record.ERR_FL}'
            },
            get: function(data){
                return (data.ERR_FL) ? '0 5 0 0' : '0 0 0 0';
            }
        },
        isHiddenErrMessTx: {
            bind: {
                ERR_FL: '{record.ERR_FL}'
            },
            get: function(data){
                return (data.ERR_FL) ? false : true;
            }
        }
    },
    stores: {
        // Код формы
        storeFormCd: {
            fields: ['id', 'text'],
            data: [
                {id: 321, text: '321'}
            ]
        }
    }
});
