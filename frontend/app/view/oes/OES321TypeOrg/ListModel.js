Ext.define('AML.view.oes.OES321TypeOrg.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321typeorgvmdl',
    data: {
        title: 'Виды перечней',
        activeCB: true,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    stores: {
        // Справочник
        oes321typeorg: {
            model: 'AML.model.OES321TypeOrg',
            pageSize: 0,
            remoteSort: false,
            remoteFilter: false,
            autoLoad: true,
            sorters: [
                {
                    property: 'WATCH_LIST_CD',
                    direction: 'ASC'
                }
            ]//,
            // filters: '{filters}'
        }
    }
});
