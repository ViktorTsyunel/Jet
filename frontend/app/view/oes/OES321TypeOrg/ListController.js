Ext.define('AML.view.oes.OES321TypeOrg.ListController', {
    extend: 'AML.view.main.BaseListController',
    alias: 'controller.oes321typeorgc',
    childForm: 'app-oes-oes321typeorg-form', /*xtype формы редактирования, открываемой из списка*/
    // IdColumn: 'WATCH_LIST_CD', /*name колонки в списке (в соответствии с моделью), которая является идентификатором записи для пользователя*/
    accessObject: 'WLType' //код объекта доступа для данной формы
});
