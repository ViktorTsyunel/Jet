Ext.define('AML.view.oes.OES321TypeOrg.FormController', {
    extend: 'AML.view.main.BaseFormController',
    alias: 'controller.oes321typeorgform',
    config: {
        listen: {
            component: {
                '#saveForm': {
                    click: 'onClickSaveFormTypeOrg'
                },
                '#cancelForm': {
                    click: 'onClickCancelFormTypeOrg'
                }
            }
        }
    },

    callbackOES321TypeOrg: function (records, operation, success) {
        var vm = this.getViewModel(),
            controler = this.controller,
            oldRecord = vm.get('record'),
            form = this.down('form').getForm(),
            view = vm.getView(),
            recData = records.getData(),
            id = records.get('_id_'),
            store = vm.getParent().getStore('oes321typeorg');
        store.load();
        var newRecord = store.getById(id),
            record = newRecord ? newRecord.getData() : false,
            newRecordSync = store.sync();
        vm.set('record', newRecord);
    },
    /**
     * Сохраняем форму
     * @param button
     * @param eOpts
     */
    onClickSaveFormTypeOrg: function (button, eOpts) {
        // если нет изменений - ничего не делаем
        var window = this.getView();
        if (!window || !this.isChanges(window)) return;

        // сохраняем изменившиеся данные, окно не закрываем
        this.saveForm(window, this, false, this.callbackOES321TypeOrg);
    },
    /**
     * Закрываем форму, если надо - перед этим сохраняем ее
     * @param button
     * @param eOpts
     */
    onClickCancelFormTypeOrg: function (button, eOpts) {
        var window = this.getView(),
            controller = this,
            fnSaveForm = this.saveForm;

        // если есть несохраненные изменения - запрашиваем о сохранении, сохраняем, закрываем окно, иначе - просто закрываем окно
        if (this.isChanges(window)) {
            AML.app.msg.confirm({
                type: 'warning',
                message: 'Запись была изменена.<br>Вы хотите сохранить сделанные изменения?',
                fnYes: function () {
                    fnSaveForm(window, controller, true, this.callbackOES321TypeOrg);
                },
                fnNo: function () {
                    window.close();
                }
            });
        } else {
            window.close();
        }
    }
});
