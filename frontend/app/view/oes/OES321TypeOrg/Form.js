Ext.define('AML.view.oes.OES321TypeOrg.Form', {
    extend: 'Ext.window.Window',
    requires: [
        'AML.view.oes.OES321TypeOrg.FormController',
        'AML.view.oes.OES321TypeOrg.FormModel'
    ],
    xtype: 'app-oes-oes321typeorg-form',
    controller: 'oes321typeorgform',
    iconCls: 'icon-report',
    viewModel: {
        type: 'oes321typeorgformvm'
    },
    width: 700,
    bind: {
        title: '{title}'
    },
    closable: false,
    tools: [
        {
            type: 'close',
            callback: 'onClickCancelForm'
        }
    ],
    items: [
        {
            xtype: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Информация о перечне лиц и организаций',
                    layout: {
                        type: 'hbox'
                    },
                    padding: 10,
                    flex: 1,
                    defaults:{
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        width: '95%'
                    },
                    items: [
                        {
                            defaults: {
                                labelWidth: 160
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Код перечня',
                                    enforceMaxLength:true,
                                    maxLength: 64,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.WATCH_LIST_CD}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Наименование',
                                    enforceMaxLength:true,
                                    maxLength: 255,
                                    allowBlank: false,
                                    bind: {
                                        value: '{record.WATCH_LIST_NM}'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Примечание',
                                    enforceMaxLength:true,
                                    maxLength: 2000,
                                    allowBlank: true,
                                    bind: {
                                        value: '{record.NOTE_TX}'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    itemId: 'saveForm',
                    text: 'Применить'
                },
                {
                    itemId: 'cancelForm',
                    text: 'Закрыть'
                }
            ]
        }
    ]
});