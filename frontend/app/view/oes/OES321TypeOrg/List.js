Ext.define('AML.view.oes.OES321TypeOrg.List', {
    extend: 'Ext.grid.Panel',
    requires: [
        'AML.view.oes.OES321TypeOrg.ListModel',
        'AML.view.oes.OES321TypeOrg.ListController',
        'AML.view.oes.OES321TypeOrg.Form'
    ],
    xtype: 'app-oes-oes321typeorg',
    controller: 'oes321typeorgc',
    viewModel: {
        type: 'oes321typeorgvmdl'
    },
    bind: {
        store: '{oes321typeorg}',
        title: '{title}'
    },
    stateful: true,
    stateId: 'oes321typeorg',
    tools: [
        {
            type: 'refresh',
            callback: 'onStoreRefresh'
        }
    ],
    columns: [
        {
            text: 'Код перечня',
            stateId: 'WATCH_LIST_CD',
            dataIndex: 'WATCH_LIST_CD',
            flex: 0.5
        },
        {
            text: 'Наименование',
            stateId: 'WATCH_LIST_NM',
            dataIndex: 'WATCH_LIST_NM',
            flex: 0.5
        },
        {
            text: 'Примечание',
            stateId: 'NOTE_TX',
            dataIndex: 'NOTE_TX',
            flex: 1
        }
    ],
    tbar: [
        {
            itemId: 'create',
            text: 'Добавить',
            iconCls: 'icon-plus'
        },
        {
            itemId: 'remove',
            text: 'Удалить',
            iconCls: 'icon-minus',
            disabled: true
        }
    ]
});
