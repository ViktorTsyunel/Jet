Ext.define('AML.view.oes.OES321TypeOrg.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oes321typeorgformvm',
    data: {
        title: 'Информация о КО',
        //record: null,
        isOpenReadOnly: null // флаг, что форму следует открыть в режиме "только чтение"
    },
    stores: {
        // Код по OKATO KTU_S
        OKATOStoreKtuS: {
            type: 'chained',
            source: 'OKATO'
        }
    }
});
