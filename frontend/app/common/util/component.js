/**
 * ускоряет перерисовку при setFieldLabel;
 */
Ext.define('common.util.component', function () {
    return {
        singleton : true,

        setFieldLabel: function (label) {
            label = label || '';

            var me = this,
                separator = me.labelSeparator,
                labelEl = me.labelEl,
                errorWrapEl = me.errorWrapEl,
                sideLabel = (me.labelAlign !== 'top'),
                noLabelCls = me.noLabelCls,
                errorWrapUnderSideLabelCls = me.errorWrapUnderSideLabelCls;

            me.fieldLabel = label;
            if (me.rendered) {
                if (Ext.isEmpty(label) && me.hideEmptyLabel) {
                    me.addCls(noLabelCls);
                    if (sideLabel && errorWrapEl) {
                        errorWrapEl.removeCls(errorWrapUnderSideLabelCls);
                    }
                } else {
                    if (separator) {
                        label = me.trimLabelSeparator() + separator;
                    }
                    labelEl.dom.firstChild.innerHTML = label;
                    me.removeCls(noLabelCls);
                    if (sideLabel && errorWrapEl) {
                        errorWrapEl.addCls(errorWrapUnderSideLabelCls);
                    }
                }
                //me.updateLayout(); убираем тормоза при bind: {fieldLabel}!
            }
        },
        // валидаторы
        regexCountries: /^([0-9]{1,3}|[0]{1}$)/,
        typeNumer: Ext.apply(Ext.form.field.VTypes, {
            //  vtype validation function
            vRegexCountries: function(val, field) {
                return common.util.component.regexCountries.test(val);
            },
            vRegexCountriesText: 'Неверно заполнено поле'
        }),
        regexCountriesTwo: /^([0-9]{1,2}$)/,
        typeNumerTwo: Ext.apply(Ext.form.field.VTypes, {
            //  vtype validation function
            vRegexCountriesTwo: function(val, field) {
                return common.util.component.regexCountriesTwo.test(val);
            },
            vRegexCountriesTwoText: 'Неверно заполнено поле'
        }),
        regexNumberField: /\d/,
        typeNumberField: Ext.apply(Ext.form.field.VTypes, {
            //  vtype validation function
            vRegexNumberField: function(val, field) {
                return common.util.component.regexNumberField.test(val);
            },
            vRegexNumberFieldText: 'Неверно заполнено поле'
        }),

        // обработчики филдов для модели OES321Party
        convertFieldBase: function (value, record, param1, param2) {
            var outputKODCN,
                addZero = '0000';
            if (Ext.isDefined(param1) || Ext.isDefined(param2)) {
                if (param1.length === 1 && param1 === '0') {
                    outputKODCN = '0';
                } else {
                    if (param1.length < 3) {
                        param1 = addZero.substr(0, (3 - param1.length)) + param1;
                    }
                    param1 = param1.substr(0, 3);
                    if (param2.length < 2) {
                        param2 = addZero.substr(0, (2 - param2.length)) + param2;
                    }
                    param2 = param2.substr(0, 2);
                    outputKODCN = param1 + param2;
                }
            } else {
                outputKODCN = value;
            }

            return outputKODCN;
        },
        convertField1: function (value, record, recField, paramBase, paramField2) {
            var kodMod,
                rec,
                addZero = '0000',
                output;
            if (!Ext.isDefined(value)) {
                if (paramBase && paramBase !== '0') { // paramBase: record.get('KODCN')
                    if (paramBase.length > 5) {
                        paramBase = kodcn.substring(0, 5);
                    }
                    if (paramBase.length > 2 && paramBase.length <= 5) {
                        return paramBase.substring(0, paramBase.length - 2)
                    }

                    return '';
                }
                return '0';
            } else {
                output = value;
                if (output !== '0') {
                    /*if (output.length < 3) {
                        output = addZero.substr(0, (3 - output.length)) + output;
                    }*/
                    output = output.substr(0, 3);
                }
            }
            if (!Ext.isDefined(paramField2)) { // paramField2: record.get('KODCN_2')
                if (output !== '0') {
                    kodMod = '00';
                }
            } else {
                kodMod = paramField2;
            }
            if (output === '0') {
                rec = output;
            } else {
                rec = output + kodMod
            }
            setTimeout(function() {
                record.set(recField, rec); // recField: 'KODCN'
            }, 5);

            return output;
        },
        convertField2: function (value, record, recField, paramBase, paramField1) {
            var rec,
                output;
            if (!Ext.isDefined(value)) {
                if (paramBase) {
                    if (paramBase.length === 1 && paramBase === '0') { // paramBase: record.get('KODCN')
                        return '00';
                    } else {
                        if (paramBase.length > 5) {
                            paramBase = paramBase.substring(0, 5);
                        }
                        if (paramBase.length >= 2) {
                            // return paramBase.substr(-2); IE8
                            return paramBase.slice(-(2));
                        } else if (paramBase.length > 0) {
                            return paramBase;
                        }

                        return '00';
                    }
                }
            } else {
                output = value;
            }
            if (paramField1 === '0') { // paramField1: record.get('KODCN_1')
                rec = paramField1;
            } else {
                rec = paramField1 + output;
            }
            setTimeout(function() {
                record.set(recField, rec); // recField: 'KODCN'
            }, 5);

            return output;
        },

        // clone store for change value
        deepCloneStore: function(source) {
            source = Ext.isString(source) ? Ext.data.StoreManager.lookup(source) : source;

            var target = Ext.create(source.$className, {
                model: source.model
            });

            target.add(Ext.Array.map(source.getRange(), function (record) {
                return record.copy();
            }));

            return target;
        }
    };
});