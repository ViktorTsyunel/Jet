Ext.define('common.globalinit', function () {
    return {
        singleton : true,

        // constants
        proxyType: {
            ajax: 'ajax',
            json: 'json'
        },
        //ajaxUrl: '/OFSAAI/solution/am/service.jsp',
         ajaxUrl: 'http://10.31.11.83:7001/OFSAAI/solution/am/service.jsp',
         //ajaxUrlReports: 'http://10.31.11.83:7001/OFSAAI/solution/am/RF_OpenReport.jsp',
        ajaxUrlReports: '/OFSAAI/solution/am/RF_OpenReport.jsp',
        //ajaxAlertAddAttachmentUrl: '/OFSAAI/solution/am/RF_AlertAddAttachment.jsp',
         ajaxAlertAddAttachmentUrl: 'http://10.31.11.83:7001/OFSAAI/solution/am/RF_AlertAddAttachment.jsp',
        //ajaxAlertGetAttachmentUrl: '/OFSAAI/solution/am/RF_AlertGetAttachment.jsp',
         ajaxAlertGetAttachmentUrl: 'http://10.31.11.83:7001/OFSAAI/solution/am/RF_AlertGetAttachment.jsp',
        ajaxImportAddAttachmentUrl: '/OFSAAI/solution/am/RF_ImportAddFile.jsp',
        ajaxMethod: 'POST',
        ajaxTimeOut: 3600000,
        ajaxHeaders: {
            'Content-Type': 'application/json'
        },
        loadingMaskCmp: null,
        pagesHaveToClose: [],
        // methods
        init : function () {
            Ext.Ajax.on('requestexception', this.ajaxHandler);
            Ext.Ajax.on('requestcomplete', this.ajaxHandler);
            Ext.Ajax.on('beforerequest', this.ajaxHandlerBeforeRequest);
            Ext.override('Ext.data.Connection', {timeout: common.globalinit.ajaxTimeOut});
        },

        ajaxHandlerBeforeRequest: function(conn, options, eOpts) {
            // add loading mask for operation
            common.globalinit.loadingMaskCmp && common.globalinit.loadingMaskCmp.setLoading('Обработка...');
            //add session
            options.params = options.params || {};
            if (!options.params.session){
                //options.params.session = common.sessionId;
            }
        },
        ajaxHandler: function (conn, response, request) {
            var resp,
                msg = 'Возникла системная ошибка. Обратитесь в службу технической поддержки.';

            // disable loading mask
            if(common.globalinit.loadingMaskCmp) {
                common.globalinit.loadingMaskCmp.setLoading(false);
                common.globalinit.loadingMaskCmp = null;
            }

            resp = Ext.JSON.decode(response.responseText, true);
            if (!resp) {
                return;
            }

            if (resp.success === false && resp.message) {
                msg = resp.message + '<br>' + msg;
                //<debug>
                //msg = msg + '<br>Код ошибки: ' + resp.err_code || resp.err_id;
                //</debug>
                Ext.Msg.alert('Ошибка',  msg);
            }
        }
    };
});