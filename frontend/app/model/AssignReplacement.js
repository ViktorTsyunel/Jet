Ext.define('AML.model.AssignReplacement', {
	extend: 'Ext.data.Model',
	alias: 'model.AssignReplacement',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
            // ID записи замещения
            name: 'REPLACEMENT_SEQ_ID',
			type: 'int'
		},
        {
            // ID замещаемого пользователя
            name: 'OWNER_SEQ_ID',
			type: 'int'
		},
		{
            // ID замещающего пользователя
            name: 'NEW_OWNER_SEQ_ID',
			type: 'int'
		},
        {
            // Флаг Y/N активности записи замещения
            name: 'ACTIVE_FL',
            defaultValue: true,
			type: 'boolean'
		},
        {
            // Начало периода замещения
            name: 'START_DT',
            defaultValue: new Date(),
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Окончание периода замещения
            name: 'END_DT',
            defaultValue: new Date(),
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Пользователь, создавший данную запись
            name: 'CREATED_BY',
            type: 'string'
        },
        {
            // Дата и время создания данной записи
            name: 'CREATED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Пользователь, изменивший данную запись последним
            name: 'MODIFIED_BY',
            type: 'string'
        },
        {
            // Дата и время последнего изменения данной записи
            name: 'MODIFIED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
	],
	validators: {
		"_id_": 'presence',
        REPLACEMENT_SEQ_ID: 'presence',
        OWNER_SEQ_ID: 'presence',
        NEW_OWNER_SEQ_ID: 'presence',
        ACTIVE_FL: 'presence',
        START_DT: 'presence',
        END_D: 'presence'
    },
    proxy: {
        url: common.globalinit.ajaxUrl,
        type: 'ajax',
        paramsAsJson: true,
        noCache: false,
        actionMethods: {
            read: 'POST',
            update: 'POST',
            create: 'POST',
            destroy: 'POST'
        },
        reader: {
            type: 'json',
            transform: {
                fn: function (data) {
                    if (Ext.isArray(data.data)) {
                        Ext.Array.forEach(data.data, function (i) {
                            if (i.ACTIVE_FL !== undefined) {
                                i.ACTIVE_FL = (i.ACTIVE_FL === 'Y');
                            }
                        });
                    }
                    return data;
                },
                scope: this
            },
            rootProperty: 'data',
            messageProperty: 'message',
            totalProperty: 'totalCount'
        },
        writer: {
            type: 'json',
            transform: {
                fn: function (data, request) {
                    var actionMap = {
                        create: 'create',
                        update: 'update',
                        destroy: 'delete',
                        read: 'read'
                    };
                    request.setParam('action', actionMap[request.getAction()] || 'read');
                    Ext.Array.forEach(data, function (i) {
                        if (i.ACTIVE_FL !== undefined) {
                            i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
                        }
                    });
                    return data;
                },
                scope: this
            },
            allowSingle: false,
            rootProperty: 'data'
        },
        extraParams: {
            form: 'AssignReplacement',
            action: 'read'
        }
    }
});
