Ext.define('AML.model.RULESnapshot', {
    extend: 'Ext.data.Model',
    alias: 'model.RULESnapshot',
    fields: [
        {
            // column_name
            name: 'COLUMN_NAME',
            type: 'string'
        },
        {
            // column_value
            name: 'COLUMN_VALUE',
            type: 'string'
        },
	    {
		    name: 'COLUMN_VALUE_TO_WRAP',
		    convert: function (value, record) {
			    var COLUMN_VALUE = record.get('COLUMN_VALUE');
			    if (COLUMN_VALUE.length > 100) {
				    return COLUMN_VALUE.split(',').join(',&#8203;');
			    }
			    return COLUMN_VALUE;
		    },
		    depends: ['COLUMN_VALUE']
	    }/*,
        {
            name: 'MYNAMEIS2',
            calculate: function (data) {
                /*if (data.COLUMN_NAME == 'SNAPSHOT_ID') {
                    data.COLUMN_NAME = 'Cуррогатный ключ';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'RULE_SEQ_ID') {
                    data.COLUMN_NAME = 'Идентификатор правила';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'RECORD_SEQ_ID') {
                    data.COLUMN_NAME = 'Идентификатор записи деталей ОПОК';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACTUAL_FL') {
                    data.COLUMN_NAME = 'Флаг актуальности данного снимка условий.';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'SNAPSHOT_TIME') {
                    data.COLUMN_NAME = 'Дата и время записи данного снимка условий';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'OPOK_NB') {
                    data.COLUMN_NAME = 'Код ОПОК';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'QUERY_CD') {
                    data.COLUMN_NAME = 'Запрос';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'PRIORITY_NB') {
                    data.COLUMN_NAME = 'Приоритет правила';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'LIMIT_AM') {
                    data.COLUMN_NAME = 'Пороговая сумма в рублях';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'TB_ID') {
                    data.COLUMN_NAME = 'Код территориального банка';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'OP_CAT_CD') {
                    data.COLUMN_NAME = 'Категория операций, к которым применяется правило';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'DBT_CDT_CD') {
                    data.COLUMN_NAME = 'C - зачисления (на счет),D - списания (со счета)';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT_CRIT_TX') {
                    data.COLUMN_NAME = 'Совокупные условия на номер счета в операции';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT_SUBTLE_CRIT_TX') {
                    data.COLUMN_NAME = 'Совокупные уточняющие условия на номер счета в операции';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT_AREA_TX') {
                    data.COLUMN_NAME = 'Откуда следует получать/где следует искать номер счета';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT_NULL_FL') {
                    data.COLUMN_NAME = 'Подпадают также операции, в которых неизвестен';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT2_CRIT_TX') {
                    data.COLUMN_NAME = 'Совокупные условия';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT2_SUBTLE_CRIT_TX') {
                    data.COLUMN_NAME = 'Совокупные уточняющие условия';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT2_AREA_TX') {
                    data.COLUMN_NAME = 'Откуда следует получать/где следует искать';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT2_NULL_FL') {
                    data.COLUMN_NAME = 'Если Y, то подпадают также операции, в которых неизвестен';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'ACCT_REVERSE_FL') {
                    data.COLUMN_NAME = 'Если Y, роперации с обратными условиями на номера счетов';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'LEXEME_PURPOSE_TX') {
                    data.COLUMN_NAME = 'В назначении платежа';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'LEXEME_CUST_TX') {
                    data.COLUMN_NAME = 'В наименовании участника операции (плательщика для безналичных операций)';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'LEXEME_CUST2_TX') {
                    data.COLUMN_NAME = '2 в наименовании участника операции';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'LEXEME2_PURPOSE_TX') {
                    data.COLUMN_NAME = '2 в назначении платежа';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'LEXEME2_CUST_TX') {
                    data.COLUMN_NAME = '2 в наименовании участника операции (плательщика для безналичных операций)';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'LEXEME2_CUST2_TX') {
                    data.COLUMN_NAME = '2 в наименовании участника операции';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'OPTP_CRIT_TX') {
                    data.COLUMN_NAME = 'Совокупные условия на вид операции';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'SRC_CRIT_TX') {
                    data.COLUMN_NAME = 'Перечень систем-источников';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'CUSTOM_CRIT_SQL_TX') {
                    data.COLUMN_NAME = 'SQL-выражение';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'CUSTOM_CRIT_PARAM_TX') {
                    data.COLUMN_NAME = 'Параметр, подставляемый в текст дополнительного условия';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'EXPLANATION_SQL_TX') {
                    data.COLUMN_NAME = 'SQL-выражение, возвращающее поясняющий текст';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'START_DT') {
                    data.COLUMN_NAME = 'Начало действия данного правила выявления';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'END_DT') {
                    data.COLUMN_NAME = 'Окончание действия данного правила выявления';
                    data.qtip = data.COLUMN_NAME;
                }
                if (data.COLUMN_NAME == 'NOTE_TX') {
                    data.COLUMN_NAME = 'Комментарий, раскрывающий логику выявления в данном правиле';
                    data.qtip = data.COLUMN_NAME;
                }
                return data;
            }
        }*/
    ]
});