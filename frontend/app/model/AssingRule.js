Ext.define('AML.model.AssignRule', {
	extend: 'Ext.data.Model',
	alias: 'model.AssignRule',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
            // ID правила назначения
            name: 'RULE_SEQ_ID',
			type: 'int'
		},
        {
            // Ответственный
            name: 'OWNER_SEQ_ID',
			type: 'int'
		},
		{
            // Вторая очередь?
            name: 'ORDER2_FL',
			type: 'boolean'
		},
        {
            // Любой алерт?
            name: 'ANY_FL',
			type: 'boolean'
		},
        {
            // Код ОПОК
            name: 'OPOK_NB',
            allowNull: true,
            type: 'int'
        },
        {
            // Код ТБ
            name: 'TB_ID',
            type: 'int'
        },
        {
            // Код ОСБ
            name: 'OSB_ID',
            type: 'int'
        },
        {
            // Активно?
            name: 'ACTIVE_FL',
            defaultValue: true,
            type: 'boolean'
        },
        {
            // Начало действия
            name: 'START_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Окончание действия
            name: 'END_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Кто создал
            name: 'CREATED_BY',
            type: 'string'
        },
        {
            // Когда создал
            name: 'CREATED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Кто изменил
            name: 'MODIFIED_BY',
            type: 'string'
        },
        {
            // Когда изменил
            name: 'MODIFIED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
	],
	validators: {
		"_id_": 'presence',
		OWNER_SEQ_ID: 'presence',
		ORDER2_FL: 'presence',
		ANY_FL: 'presence',
		ACTIVE_FL: 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ORDER2_FL != undefined) {
								i.ORDER2_FL = (i.ORDER2_FL == 'Y');
							}
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
							}
							if (i.ANY_FL != undefined) {
								i.ANY_FL = (i.ANY_FL == 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ORDER2_FL != undefined) {
							i.ORDER2_FL = (i.ORDER2_FL ? 'Y' : 'N');
						}
						if (i.ANY_FL != undefined) {
							i.ANY_FL = (i.ANY_FL ? 'Y' : 'N');
						}
						if (i.ACTIVE_FL != undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'AssignRule',
			action: 'read'
		}
	}
});
