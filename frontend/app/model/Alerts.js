/**
 * Модель для выявленных операций
 */
Ext.define('AML.model.Alerts', {
	extend: 'Ext.data.Model',
	alias: 'model.Alerts',
	idProperty: '_id_',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'string'
		},
		{
			// Номер операции
			name: 'REVIEW_ID',
			type: 'int',
			allowNull: true,
			allowBlank: true
		},
		{
			// Статус операции
			name: 'RV_STATUS_NM',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Статус сообщения
			name: 'CHECK_STATUS',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Номер сообщения
			name: 'message_number',
			mapping: 'message_number',
			type: 'int',
			allowNull: true,
			allowBlank: true
		},
		{
			// Вид операции
			name: 'OPOK_NB',
			type: 'int',
			allowNull: true,
			allowBlank: true
		},
		{
			// Доп. вид операции
			name: 'ADD_OPOKS_TX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Сумма в рублях
			name: 'TRXN_BASE_AM',
			type: 'number',
			allowNull: true,
			allowBlank: true
		},
		{
			// Сумма в валюте
			name: 'TRXN_CRNCY_AM',
			type: 'number',
			allowNull: true,
			allowBlank: true
		},
		{
			// Валюта
			name: 'TRXN_CRNCY_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Дата операции
			name: 'TRXN_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
			allowNull: true,
			allowBlank: true
		},
		{
			// Обработать к...
			name: 'RV_DUE_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
			allowNull: true,
			allowBlank: true
		},
		{
			// Ответственный
			name: 'RV_OWNER_ID',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Комментарий
			name: 'RV_NOTE_TX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Нал/Бнал
			name: 'OP_CAT_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Приложение
			name: 'attachments',
			mapping: 'attachments',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		// Поля для подвала
		{
			// Причина выявления
			name: 'EXPLANATION_TX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Назначение платежа
			name: 'TRXN_DESC',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Вид операции (ЦАС НСИ)
			name: 'NSI_OP',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Наименование
			name: 'ORIG_NM',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// ИНН
			name: 'ORIG_INN_NB',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Счет
			name: 'ORIG_ACCT_NB',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Банк
			name: 'SEND_INSTN',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Корр. счет
			name: 'SEND_INSTN_ACCT_ID',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Счет дебета
			name: 'DEBIT_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Адрес
			name: 'ORIG_ADDR_TX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Удостоверяющий документ
			name: 'ORIG_IDDOC_TX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Наименование
			name: 'BENEF_NM',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// ИНН
			name: 'BENEF_INN_NB',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Счет
			name: 'BENEF_ACCT_NB',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Банк
			name: 'RCV_INSTN',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Корр. счет
			name: 'RCV_INSTN_ACCT_ID',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Счет кредита
			name: 'CREDIT_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Адрес
			name: 'BENEF_ADDR_TX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Удостоверяющий документ
			name: 'BENEF_IDDOC_TX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Номер ТБ
			name: 'TB_ID',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Наименование ТБ
			name: 'TB_NM',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Номер ОСБ
			name: 'OSB_ID',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Наименование ОСБ
			name: 'OSB_NM',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Ценные бумаги
			name: 'RV_SCRTY_DESC',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Изменения в операции - пока заглушка
			name: 'TRXN_CHANGE',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// Изменения в выявлении - пока заглушка
			name: 'OPOK_CHANGE',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		// Служебные поля
		{
			// подсветка лексем в назначении платежа, невидимо
			name: 'TRXN_DESC_LEX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// подсветка лексем в наим. плательщика, невидимо
			name: 'ORIG_LEX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// подсветка лексем в наим. получателя, невидимо
			name: 'BENEF_LEX',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			// флаг (0/1) подсветки статуса алерта
			name: 'RV_STATUS_HIGHLIGHT',
			type: 'int',
			allowNull: true,
			allowBlank: true
		},
		{
			// признак подстветки срока обработки алерта (обработать к ...) OVERDUE - "совсем красный", NEARDUE - "краснеющий"
			name: 'RV_DUE_DT_HIGHLIGHT',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		// Невидимые полезные поля
		{
			name: 'RV_STATUS_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'RV_OBJECT_TP_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'RV_OP_CAT_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'RV_TRXN_SEQ_ID',
			type: 'int',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'RV_TRXN_SCRTY_SEQ_ID',
			type: 'int',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'RV_REPEAT_NB',
			type: 'int',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'RV_DATA_DUMP_DT',
			type: 'date',
			allowNull: true,
			allowBlank: true,
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			name: 'RV_CLS_DT',
			type: 'date',
			allowNull: true,
			allowBlank: true,
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			name: 'SRC_SYS_CD',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'CANCELED_FL',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'SCRTY_CANCELED_FL',
			type: 'string',
			allowNull: true,
			allowBlank: true
		},
		{
			name: 'TRXN_DOC_DT',
			type: 'date',
			allowNull: true,
			allowBlank: true,
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// флаг возможности редактирования: 0/1 = нельзя/можно
			name: 'ALLOW_ALERT_EDITING',
			type: 'int',
            allowNull: true,
            allowBlank: true
		},
		{
			name: 'RV_CREAT_TS',
			type: 'date',
			allowNull: true,
			allowBlank: true,
			dateFormat: 'Y-m-d H:i:s'
		},
        {
            name: 'DEBIT_CD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            name: 'CREDIT_CD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            name: 'ORIG_NM',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            name: 'BENEF_NM',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер цен. бум
            name: 'SCRTY_NB',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Сумма цен. бум.
            name: 'SCRTY_BASE_AM',
            type: 'number',
            allowNull: true,
            allowBlank: true
        },
        {
            // Вид операции ГОЗ (списание)
            name: 'ORIG_GOZ_OP_CD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Вид операции ГОЗ (зачисление)
            name: 'BENEF_GOZ_OP_CD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
		{
            // Вид операции ГОЗ (зачисление)
            name: 'TRXN_ORG_DESC',
            type: 'string',
            allowNull: true,
            allowBlank: true
        }
	],
	validators: {
		"_id_": 'presence'
	},
    proxy: {
        url: common.globalinit.ajaxUrl,
        type: common.globalinit.proxyType.ajax,
        paramsAsJson: true,
        noCache: false,
        actionMethods: {
            read: 'POST',
            update: 'POST',
            create: 'POST',
            destroy: 'POST'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message',
            totalProperty: 'totalCount'
        },
        extraParams: {
            form: 'AlertList',
            action: 'read'
        }
    }
});