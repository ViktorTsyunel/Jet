Ext.define('AML.model.OPOKaddZero', {
    extend: 'Ext.data.Model',
	alias: 'model.OPOKaddZero',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
			// Код ОПОК
			name: 'OPOK_NB',
			allowNull: true,
			allowBlank: false,
			unique: true,
			type: 'int'
		},
		{
			// Наименование
			name: 'OPOK_NM',
			type: 'string'
		},
		{
			// Приоритет
			name: 'PRIORITY_NB',
			allowNull: true,
			type: 'int'
		},
		{
			// Пороговая сумма
			name: 'LIMIT_AM',
			allowNull: true,
			type: 'number'
		},
		{
			// Начало действия
			name: 'START_DT',
			allowNull: true,
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// Номер для сортировки
			name: 'ORDER_NB',
			allowNull: true,
			type: 'int'
		},
		{
			// Групповой код выявления
			name: 'GROUP_NB',
			allowNull: true,
			type: 'int'
		},
		{
			// Участники в ОЭС
			name: 'OES_PARTIES_TX',
			allowNull: true,
			type: 'string'
		},
		{
			// Примечание
			name: 'NOTE',
			allowNull: true,
			type: 'string'
		}
	],
	validators: {
		"_id_": 'presence',
		OPOK_NB: 'presence',
		OPOK_NM: 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						var records = [
							{
								GROUP_NB: null,
								LIMIT_AM: 100000,
								NOTE: null,
								OES_PARTIES_TX: null,
								OPOK_NB: '0',
								OPOK_NM: 'Пусто',
								ORDER_NB: '0',
								PRIORITY_NB: '0'
							}
						];
						Ext.Array.forEach(data.data, function (record) {
							records.push(record);
						});
					}
					return records;
				},
				scope: this
			}
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OPOK',
			action: 'read'
		}
	}
});
