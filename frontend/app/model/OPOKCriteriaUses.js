Ext.define('AML.model.OPOKCriteriaUses', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKCriteriaUses',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
