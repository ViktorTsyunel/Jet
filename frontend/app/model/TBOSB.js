Ext.define('AML.model.TBOSB', {
	extend: 'Ext.data.Model',
	alias: 'model.TBOSB',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			mapping: '_id_',
			type: 'string'
		},
		{
			// Наименование
			name: 'tb_id',
			mapping: 'TB_NB',
			type: 'int'
		},
        {
            // Наименование
            name: 'osb_id',
            mapping: 'OSB_NB',
            type: 'int'
        },
		{
			// Наименование
			name: 'label',
			mapping: 'ORG_NM',
			type: 'string'
		}
	]
});
