Ext.define('AML.model.AlertAttachments', {
	extend: 'Ext.data.Model',
	alias: 'model.alertattachments',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
        {
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
        {
            // Наименование файла
            name: 'ATTCH_NM',
            type: 'string'
        },
        {
            // Файл
            name: 'FILE_NM',
            type: 'string'
        },
        {
            // Комментарий
            name: 'CMMNT',
            type: 'string'
        },
        {
            // Кто создал
            name: 'CREATED_BY',
            type: 'string'
        },
        {
            // Когда создал
            name: 'CREATED_DATE',
			type: 'date',
            dateFormat: 'Y-m-d H:i:s'
		},
        {
            // Кто изменил
            name: 'MODIFIED_BY',
            type: 'string'
        },
        {
            // Когда изменил
            name: 'MODIFIED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Тип файла (Не отображается, скрытый)
            name: 'CONTENT_TYPE_TX',
            type: 'string'
        },
        {
            // ID файла (Не отображается, скрытый)
            name: 'ATTCH_SEQ_ID',
            type: 'int'
        },
		{
            // Разширение Файла
            name: 'FILE_EXT',
			type: 'string',
            convert: function(value, record) {
                var fileName = record.get('FILE_NM'),
                    fileExt = fileName.substr(fileName.lastIndexOf('.') + 1);

                return fileExt;
            }
		},
		{
			// Размер Файла
			name: 'FILE_SIZE',
			type: 'string'
		}
	],
	validators: {
		"_id_": 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'AlertAttachments',
			action: 'readByAlertId'
		}
	}
});
