Ext.define('AML.model.AlertHistory', {
	extend: 'Ext.data.Model',
	alias: 'model.alerthistory',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
            // Дата и время
            name: 'START_DT',
			type: 'date',
            dateFormat: 'Y-m-d H:i:s'
		},
		{
            // Действие
            name: 'ACTVY_TYPE_NM',
			type: 'string'
		},
		{
            // Пользователь
            name: 'OWNER_ID',
			type: 'string'
		},
		{
            // Новый статус
            name: 'NEW_STATUS_TX',
			type: 'string'
		},
		{
            // Комментарий
            name: 'CMMNT',
			type: 'string'
		},
		{
            // Наименование файла
            name: 'ATTCH_NM',
			type: 'string'
		},
		{
            // Файл
            name: 'FILE_NM',
			type: 'string'
		},
		{
            // Сообщение (ОЭС)
            name: 'OES_DESC',
			type: 'string',
			convert: function(value, record) {
				if (record.get('OES_321_ID')) {
                    return '<span style="text-decoration: underline; cursor: pointer;">' + value + '</span>';
				}
				return value;
			}
		},
		{
			name: 'OES_321_ID',
			tupe: 'int'
		},
		{
            // Тип файла (Не отображается, скрытый)
            name: 'CONTENT_TYPE_TX',
			type: 'string'
		},
		{
            // ID файла (Не отображается, скрытый)
            name: 'ATTCH_SEQ_ID',
			type: 'int'
		},
		{
            // Разширение Файла
            name: 'FILE_EXT',
			type: 'string',
            convert: function(value, record) {
                var fileName = record.get('FILE_NM'),
                    fileExt = fileName.substr(fileName.lastIndexOf('.') + 1);

                return fileExt;
            }
		},
		{
			// Файл удален
			name: 'DELETED_FL',
			type: 'string'
		},
		{
			// Размер Файла
			name: 'FILE_SIZE',
			type: 'string'
		}
	],
	validators: {
		"_id_": 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'AlertHistory',
			action: 'readByAlertId'
		}
	}
});
