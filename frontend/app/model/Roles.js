Ext.define('AML.model.Roles', {
	extend: 'Ext.data.Model',
	alias: 'model.Roles',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'string'
		},
		{
            // Код роли
            name: 'ROLE_CD',
			type: 'string'
		},
        {
            // Название роли
            name: 'ROLE_NM',
            type: 'string'
        }
	],
	validators: {
		"_id_": 'presence',
        ROLE_CD: 'presence'
    }
});
