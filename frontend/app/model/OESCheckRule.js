Ext.define('AML.model.OESCheckRule', {
	extend: 'Ext.data.Model',
	alias: 'model.oescheckrule',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
			// RULE_SEQ_ID ID правила NUMBER(22) Primary:TRUE
			name: 'RULE_SEQ_ID',
			type: 'int',
            allowNull: false,
            allowBlank: false
		},
        {
            // FORM_CD Код формы VARCHAR2(64 CHAR)
            name: 'FORM_CD',
            type: 'string',
            allowNull: false,
            allowBlank: false,
            defaultValue: '0'
        },
        {
            // DESC_TX Описание VARCHAR2(255 CHAR)
            name: 'DESC_TX',
            type: 'string',
            allowNull: false,
            allowBlank: false,
            defaultValue: '0'
        },
        {
            // CHECK_SQL_TX SQL проверки VARCHAR2(2000 CHAR)
            name: 'CHECK_SQL_TX',
            type: 'string',
            allowNull: false,
            allowBlank: false,
            defaultValue: '0'
        },
        {
            // MSG_SQL_TX SQL сообщения VARCHAR2(2000 CHAR)
            name: 'MSG_SQL_TX',
            type: 'string',
            allowNull: false,
            allowBlank: false,
            defaultValue: '0'
        },
        {
            // COL_LIST_TX Колонки ОЭС VARCHAR2(2000 CHAR)
            name: 'COL_LIST_TX',
            type: 'string',
            allowNull: false,
            allowBlank: false,
            defaultValue: '0'
        },
        {
            // BLOCK_LIST_TX Блоки ОЭС VARCHAR2(2000 CHAR)
            name: 'BLOCK_LIST_TX',
            type: 'string'
        },
        {
            // Критично?
            name: 'CRITICAL_FL',
            allowNull: false,
            allowBlank: false,
            defaultValue: true,
            type: 'boolean'
        },
        {
            // Активно?
            name: 'ACTIVE_FL',
            allowNull: false,
            allowBlank: false,
            defaultValue: true,
            type: 'boolean'
        },
        {
            // Есть ошибки?
            name: 'ERR_FL',
            allowNull: false,
            allowBlank: false,
            defaultValue: false,
            type: 'boolean'
        },
        {
            // ERR_MESS_TX Сообщение об ошибке VARCHAR2(2000 CHAR)
            name: 'ERR_MESS_TX',
            type: 'string'
        },
        {
            // NOTE_TX Примечание VARCHAR2(2000 CHAR)
            name: 'NOTE_TX',
            type: 'string'
        },
        {
            // Кто создал
            name: 'CREATED_BY',
            type: 'string'
        },
        {
            // Когда создал
            name: 'CREATED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Кто изменил
            name: 'MODIFIED_BY',
            type: 'string'
        },
        {
            // Когда изменил
            name: 'MODIFIED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
    ],
    proxy: {
        url: common.globalinit.ajaxUrl,
        type: 'ajax',
        paramsAsJson: true,
        noCache: false,
        actionMethods: {
            read: 'POST',
            update: 'POST',
            create: 'POST',
            destroy: 'POST'
        },
        reader: {
            type: 'json',
            transform: {
                fn: function (data) {
                    if (Ext.isArray(data.data)) {
                        Ext.Array.forEach(data.data, function (i) {
                            if (i.ACTIVE_FL != undefined) {
                                i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
                            }
                            if (i.CRITICAL_FL != undefined) {
                                i.CRITICAL_FL = (i.CRITICAL_FL == 'Y');
                            }
                            if (i.ERR_FL != undefined) {
                                i.ERR_FL = (i.ERR_FL == 'Y');
                            }
                        });
                    }
                    return data;
                },
                scope: this
            },
            rootProperty: 'data',
            messageProperty: 'message',
            totalProperty: 'totalCount'
        },
        writer: {
            type: 'json',
            transform: {
                fn: function (data, request) {
                    var actionMap = {
                        create: 'create',
                        update: 'update',
                        destroy: 'delete',
                        read: 'read'
                    };
                    request.setParam('action', actionMap[request.getAction()] || 'read');
                    Ext.Array.forEach(data, function (i) {
                        if (i.ACTIVE_FL != undefined) {
                            i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
                        }
                        if (i.CRITICAL_FL != undefined) {
                            i.CRITICAL_FL = (i.CRITICAL_FL ? 'Y' : 'N');
                        }
                        if (i.ERR_FL != undefined) {
                            i.ERR_FL = (i.ERR_FL ? 'Y' : 'N');
                        }
                    });
                    return data;
                },
                scope: this
            },
            allowSingle: false,
            rootProperty: 'data'
        },
        extraParams: {
            form: 'OESCheckRule',
            action: 'read'
        }
    }
});
