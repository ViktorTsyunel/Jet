Ext.define('AML.model.OES321Party', {
    extend: 'Ext.data.Model',
    alias: 'model.oes321party',
    idProperty: '_id_',
    fields: [
        {
            // ID
            identifier: true,
            name: '_id_',
            type: 'string'
        },
        {
            // Номер счета
            name: 'ACC_B',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер корр. счета
            name: 'ACC_COR_B',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Д.
            name: 'ADRESS_D',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Город/Нас. пункт
            name: 'ADRESS_G',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Корпус/Стр.
            name: 'ADRESS_K',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Оф./Кв.
            name: 'ADRESS_O',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Район/Область
            name: 'ADRESS_R',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код по ОКАТО
            name: 'ADRESS_S',
            type: 'string',
            allowNull: false,
            allowBlank: false
        },
        {
            // Ул.
            name: 'ADRESS_U',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Д.
            name: 'AMR_D',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Город/Нас. пункт
            name: 'AMR_G',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Корпус/Стр.
            name: 'AMR_K',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Оф./Кв.
            name: 'AMR_O',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Район/Область
            name: 'AMR_R',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код по ОКАТО
            name: 'AMR_S',
            type: 'string',
            allowNull: false,
            allowBlank: false
        },
        {
            // Ул.
            name: 'AMR_U',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // БИК/SWIFTBIC/ non-SWIFT BIC
            name: 'BIK_B',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // БИК банка-эмитента
            name: 'BIK_IS',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // БИК/SWIFTBIC/ non-SWIFT BIC
            name: 'BIK_R',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер блока который требуется отобразить
            name: 'BLOCK_NB',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Место рождения
            name: 'BP',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Признак владельца банковской карты
            name: 'CARD_B',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата рег./ рожд.
            name: 'GR',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Документ, удостоверяющий личность
            name: 'KD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код места нахождения
            name: 'KODCN',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCN_1'), record.get('KODCN_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCN_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCN', record.get('KODCN'), record.get('KODCN_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCN_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCN', record.get('KODCN'), record.get('KODCN_1'));
            }
        },
        {
            // Код места нахождения
            name: 'KODCN_B',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCN_B_1'), record.get('KODCN_B_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCN_B_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCN_B', record.get('KODCN_B'), record.get('KODCN_B_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCN_B_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCN_B', record.get('KODCN_B'), record.get('KODCN_B_1'));
            }
        },
        {
            // Код места нахождения
            name: 'KODCN_R',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCN_R_1'), record.get('KODCN_R_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCN_R_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCN_R', record.get('KODCN_R'), record.get('KODCN_R_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCN_R_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCN_R', record.get('KODCN_R'), record.get('KODCN_R_1'));
            }
        },
        {
            // Код места рег./жит.
            name: 'KODCR',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value, record) {
                return common.util.component.convertFieldBase (value, record, record.get('KODCR_1'), record.get('KODCR_2'));
            }
        },
        {
            // Код места рег./жит. регион
            name: 'KODCR_1',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField1 (value, record, 'KODCR', record.get('KODCR'), record.get('KODCR_2'));
            }
        },
        {
            // Код места рег./жит. страна
            name: 'KODCR_2',
            type: 'string',
            persist: false,
            convert: function (value, record) {
                return common.util.component.convertField2 (value, record, 'KODCR', record.get('KODCR'), record.get('KODCR_1'));
            }
        },
        {
            // Номер
            name: 'MC1',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Срок пребывания с
            name: 'MC2',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // по
            name: 'MC3',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Наименование ЮЛ или ФИО ФЛ
            name: 'NAMEU',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Наименование КО (филиала)
            name: 'NAME_B',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Наименование банка-эмитента
            name: 'NAME_IS',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Наименование КО (филиала) - корреспондента
            name: 'NAME_R',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // ИНН
            name: 'ND',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Примечание
            name: 'NOTE',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // OES_321_ID
            name: 'OES_321_ID',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Признак участника
            name: 'PR',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Рег. номер
            name: 'RG',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // ОКПО/Серия документа
            name: 'SD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Тип участника
            name: 'TU',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер документа
            name: 'VD1',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Орган, выдавший документ
            name: 'VD2',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата выдачи
            name: 'VD3',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код вида документа
            name: 'VD4',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Серия и номер
            name: 'VD5',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Срок действия с
            name: 'VD6',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Срок действия по
            name: 'VD7',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Признак выгодоприобретателя
            name: 'VP',
            type: 'string',
            allowNull: true,
            allowBlank: true
        }
    ]
});