Ext.define('AML.model.OPOKCriteria', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKCriteria',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'string'
		},
		{
			// Код условия
			name: 'CRIT_CD',
			type: 'string'
		},
		{
			// Тип условия
			name: 'CRIT_TP_CD',
			type: 'int',
			defaultValue: 1
		},
		{
			// Наименование
			name: 'CRIT_NM',
			type: 'string'
		},
		{
			// Текст условия
			name: 'CRIT_TX',
			type: 'string'
		},
		{
			// Уточняющее условие
			name: 'SUBTLE_CRIT_TX',
			type: 'string'
		},
		{
			// Неизвестное значение?
			name: 'NULL_FL',
			type: 'boolean'
		},
		{
			// Текст условия - тест
			name: 'TEST_CRIT_TX',
			type: 'string'
		},
		{
			// Уточняющее условие - тест
			name: 'TEST_SUBTLE_CRIT_TX',
			type: 'string'
		},
		{
			// Неизвестное значение? - тест
			name: 'TEST_NULL_FL',
			type: 'boolean'
		},
		{
			// Вид использования
			name: 'CRIT_USE_CD',
			type: 'string',
			defaultValue: 'ВЫЯВЛЕНИЕ'
		},
		{
			// Активно?
			name: 'ACTIVE_FL',
			type: 'boolean',
			defaultValue: true
		},
		{
			// Примечание
			name: 'NOTE_TX',
			allowNull: true,
			type: 'string'
		}
	],
	validators: {
		"_id_": 'presence',
		CRIT_CD: 'presence',
		CRIT_TP_CD: 'presence',
		CRIT_NM: 'presence',
		CRIT_TX: 'presence',
		CRIT_USE_CD: 'presence',
		ACTIVE_FL: 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.NULL_FL != undefined) {
								i.NULL_FL = (i.NULL_FL == 'Y');
							}
							if (i.TEST_NULL_FL != undefined) {
								i.TEST_NULL_FL = (i.TEST_NULL_FL == 'Y');
							}
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.NULL_FL != undefined) {
							i.NULL_FL = (i.NULL_FL ? 'Y' : 'N');
						}
						if (i.TEST_NULL_FL != undefined) {
							i.TEST_NULL_FL = (i.TEST_NULL_FL ? 'Y' : 'N');
						}
						if (i.ACTIVE_FL != undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OPOKCriteria',
			action: 'read'
		}
	}
});
