Ext.define('AML.model.OPOKEdit', {
    extend: 'Ext.data.Model',
    alias: 'model.OPOKEdit',
    idProperty: 'id',
    fields: [
        {
            // ID
            identifier: true,
            name: 'id',
            type: 'int'
        },
        {
            // Вид операции
            name: 'OPOK_NB',
            type: 'int',
            allowNull: false,
            allowBlank: false
        },
        {
            // Доп. вид операции
            name: 'ADD_OPOKS_TX',
            type: 'string',
            allowNull: true,
            allowBlank: true
        }
    ]
});
