Ext.define('AML.model.OES321TypeOrg', {
	extend: 'Ext.data.Model',
	alias: 'model.oes321typeorg',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			//
			identifier: true,
			name: '_id_',
			type: 'string'
		},
		{
            // Код перечня
            name: 'WATCH_LIST_CD',
			type: 'string'
		},
        {
            // Наименование
            name: 'WATCH_LIST_NM',
			type: 'string'
		},
		{
			// Примечание
			name: 'NOTE_TX',
			type: 'string'
		},
		{
			// Активно?
			name: 'ACTIVE_FL',
			type: 'string'
		}
	],
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'WatchList',
			action: 'read'
		}
	}
});