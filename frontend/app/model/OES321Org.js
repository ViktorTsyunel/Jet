Ext.define('AML.model.OES321Org', {
	extend: 'Ext.data.Model',
	alias: 'model.oes21org',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
            // Id записи информации о КО
            name: 'OES_321_ORG_ID',
			type: 'int'
		},
        {
            // Код ТБ
            name: 'TB_CODE',
			type: 'int'
		},
		{
			// Код ОСБ
			name: 'OSB_CODE',
			type: 'int'
		},
		{
            // Тербанк
            name: 'TB_NAME',
			type: 'string'
		},
        {
            // Рег. номер КО
            name: 'REGN',
			type: 'string'
		},
        {
            // № филиала КО
            name: 'NUMBF_S',
            type: 'string'
        },
        {
            // БИК КО
            name: 'BIK_S',
            type: 'string'
        },
        {
            // ИНН КО
            name: 'ND_KO',
            type: 'string'
        },
        {
            // Код ОКАТО ТУ
            name: 'KTU_S',
            type: 'string'
        },
        {
            // Тел. ответственного
            name: 'TEL',
            type: 'string'
        },
        {
            // Код ОКАТО ФКО ТУ
            name: 'KTU_SS',
            type: 'string'
        },
        {
            // БИК ФКО
            name: 'BIK_SS',
            type: 'string'
        },
        {
            // Номер ФКО
            name: 'NUMBF_SS',
            type: 'string'
        },
        {
            // Активно?
            name: 'ACTIVE_FL',
            defaultValue: true,
            type: 'boolean'
        },
        {
            // Сведения предоставлены филиалом Банка
            name: 'BRANCH_FL',
            type: 'boolean'
        },
        {
            // Начало действия
            name: 'START_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Окончание действия
            name: 'END_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Кто создал
            name: 'CREATED_BY',
            type: 'string'
        },
        {
            // Когда создал
            name: 'CREATED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Кто изменил
            name: 'MODIFIED_BY',
            type: 'string'
        },
        {
            // Когда изменил
            name: 'MODIFIED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
	],
	validators: {
		"_id_": 'presence',
        OES_321_ORG_ID: 'presence',
		ACTIVE_FL: 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
							}
                            if (i.BRANCH_FL != undefined) {
                                i.BRANCH_FL = (i.BRANCH_FL === '1');
                            }
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ACTIVE_FL != undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
                        if (i.BRANCH_FL != undefined) {
                            i.BRANCH_FL = (i.BRANCH_FL ? '1' : '0');
                        }
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OES321Org',
			action: 'read'
		}
	}
});
