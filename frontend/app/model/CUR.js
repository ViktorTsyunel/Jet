Ext.define('AML.model.CUR', {
	extend: 'Ext.data.Model',
	alias: 'model.CUR',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
			// Код ОПОК
			name: 'OPOK_NB',
			allowNull: true,
			allowBlank: false,
			unique: true,
			type: 'int'
		},
		{
			// Наименование
			name: 'OPOK_NM',
			type: 'string'
		},
		{
			// Приоритет
			name: 'PRIORITY_NB',
			allowNull: true,
			type: 'int'
		},
		{
			// Пороговая сумма
			name: 'LIMIT_AM',
			allowNull: true,
			type: 'number'
		},
		{
			// Начало действия
			name: 'START_DT',
			allowNull: true,
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// Номер для сортировки
			name: 'ORDER_NB',
			allowNull: true,
			type: 'int'
		},
		{
			// Групповой код выявления
			name: 'GROUP_NB',
			allowNull: true,
			type: 'int'
		},
		{
			// Участники в ОЭС
			name: 'OES_PARTIES_TX',
			allowNull: true,
			type: 'string'
		},
		{
			// Примечание
			name: 'NOTE',
			allowNull: true,
			type: 'string'
		},{
			name: 'CUR_NAME',
			allowNull: true,
			type: 'string'
		},{
			name: 'METAL_FLAG',
			allowNull: true,
			type: 'int'
		},{
			name: 'CUR_CODE',
			allowNull: true,
			type: 'string'
		}
	]
});
