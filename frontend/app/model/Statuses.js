Ext.define('AML.model.Statuses', {
	extend: 'Ext.data.Model',
	alias: 'model.Statuses',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			mapping: '_id_',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			mapping: 'CODE_DISP_TX',
			type: 'string'
		}
	]
});
