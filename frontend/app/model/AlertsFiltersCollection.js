/**
 * Модель для справочника фильтров
 */
Ext.define('AML.model.AlertsFiltersCollection', {
	extend: 'Ext.data.Model',
	alias: 'model.AlertsFiltersCollection',
	idProperty: '_id_',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
			// Наименование
			name: 'QUEUE_DISPLAY_NM',
			type: 'string'
		},
		{
			// По умолчанию
			name: 'IS_DEFAULT',
			type: 'int'
		}
	],
	validators: {
		"_id_": 'presence'
	}
});
