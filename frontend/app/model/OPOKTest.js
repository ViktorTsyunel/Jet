Ext.define('AML.model.OPOKTest', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKTest',
	fields: [
		{
			// ID операции
			name: 'TRXN_SEQ_ID',
			type: 'int',
			allowNull: true
		},
		{
			// Нал/Бнал
			name: 'OP_CAT_CD',
			type: 'string',
			allowNull: true
		},
		{
			// Виды оп.
			name: 'OPOK_LIST',
			type: 'string',
			allowNull: true
		},
		{
			// Дата операции
			name: 'TRXN_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
			allowNull: true
		},
		{
			// Сумма в рублях
			name: 'TRXN_BASE_AM',
			type: 'number',
			allowNull: true
		},
		{
			// Сумма в валюте
			name: 'TRXN_CRNCY_AM',
			type: 'number',
			allowNull: true
		},
		{
			// Валюта
			name: 'TRXN_CRNCY_CD',
			type: 'string',
			allowNull: true
		},
		{
			// Назначение платежа
			name: 'TRXN_DESC',
			type: 'string',
			allowNull: true
		},
		{
			// Назначение платежа (лексемы)
			name: 'TRXN_DESC_LEX',
			type: 'string',
			allowNull: true
		},
		{
			// Наименование плательщика
			name: 'ORIG_NM',
			type: 'string',
			allowNull: true
		},
		{
			// Наименование плательщика (лексемы)
			name: 'ORIG_LEX',
			type: 'string',
			allowNull: true
		},
		{
			// ИНН плательщика
			name: 'ORIG_INN_NB',
			type: 'string',
			allowNull: true
		},
		{
			// Счет плательщика
			name: 'ORIG_ACCT_NB',
			type: 'string',
			allowNull: true
		},
		{
			// Плательщик - клиент СБ РФ
			name: 'ORIG_OWN_FL',
			type: 'boolean',
			allowNull: true
		},
		{
			// Счет дебета
			name: 'DEBIT_CD',
			type: 'string',
			allowNull: true
		},
		{
			// Банк плательщика
			name: 'SEND_INSTN_NM',
			type: 'string',
			allowNull: true
		},
		{
			// БИК банка плательщика
			name: 'SEND_INSTN_ID',
			type: 'string',
			allowNull: true
		},
		{
			// Корр. счет банка плательщика
			name: 'SEND_INSTN_ACCT_ID',
			type: 'string',
			allowNull: true
		},
		{
			// Получатель
			name: 'BENEF_NM',
			type: 'string',
			allowNull: true
		},
		{
			// Получатель  (лексемы)
			name: 'BENEF_LEX',
			type: 'string',
			allowNull: true
		},
		{
			// ИНН получателя
			name: 'BENEF_INN_NB',
			type: 'string',
			allowNull: true
		},
		{
			// Счет получателя
			name: 'BENEF_ACCT_NB',
			type: 'string',
			allowNull: true
		},
		{
			// Получатель - клиент СБ РФ
			name: 'BENEF_OWN_FL',
			type: 'boolean',
			allowNull: true
		},
		{
			// Счет кредита
			name: 'CREDIT_CD',
			type: 'string',
			allowNull: true
		},
		{
			// Банк получателя
			name: 'RCV_INSTN_NM',
			type: 'string',
			allowNull: true
		},
		{
			// БИК банка получателя
			name: 'RCV_INSTN_ID',
			type: 'string',
			allowNull: true
		},
		{
			// Корр. счет банка получателя
			name: 'RCV_INSTN_ACCT_ID',
			type: 'string',
			allowNull: true
		},
		{
			// Причина выявления
			name: 'EXPLANATION_TX',
			type: 'string',
			allowNull: true
		},
		{
			// Вид операции (ЦАС НСИ)
			name: 'NSI_OP',
			type: 'string',
			allowNull: true
		},
		{
			// Номинал ценн. бумаги
			name: 'SCRTY_BASE_AM',
			type: 'string',
			allowNull: true
		},
		{
			// Номер ценной бумаги
			name: 'SCRTY_ID',
			type: 'string',
			allowNull: true
		},
		{
			// Вид ценн. бумаги
			name: 'SCRTY_TYPE_CD',
			type: 'string',
			allowNull: true
		},
		{
			// ID ценн. бумаги
			name: 'TRXN_SCRTY_SEQ_ID',
			type: 'int',
			allowNull: true
		},
		{
			// Номер ТБ
			name: 'TB_ID',
			type: 'int',
			allowNull: true
		},
		{
			// Номер ОСБ
			name: 'OSB_ID',
			type: 'int',
			allowNull: true
		},
		{
			// Дата загрузки
			name: 'DATA_DUMP_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
			allowNull: true
		},
		{
			// Система-источник
			name: 'SRC_SYS_CD',
			type: 'string',
			allowNull: true
		},
		{
			// Дата изменения в источнике
			name: 'SRC_CHANGE_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
			allowNull: true
		},
		{
			// Пользователь
			name: 'SRC_USER_NM',
			type: 'string',
			allowNull: true
		},
		{
			// Код в системе источнике
			name: 'SRC_CD',
			type: 'string',
			allowNull: true
		}
	]
});
