Ext.define('AML.model.SearchDirection', {
	extend: 'Ext.data.Model',
	alias: 'model.SearchDirection',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
