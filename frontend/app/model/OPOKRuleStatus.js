Ext.define('AML.model.OPOKRuleStatus', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKRuleStatus',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
