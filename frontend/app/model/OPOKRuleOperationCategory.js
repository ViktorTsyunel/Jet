Ext.define('AML.model.OPOKRuleOperationCategory', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKRuleOperationCategory',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
