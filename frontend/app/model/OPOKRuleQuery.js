Ext.define('AML.model.OPOKRuleQuery', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKRuleQuery',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
