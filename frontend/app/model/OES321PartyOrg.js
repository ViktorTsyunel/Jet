Ext.define('AML.model.OES321PartyOrg', {
	extend: 'Ext.data.Model',
	alias: 'model.oes321partyorg',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
            // Id записи КО участники операций
            name: 'OES_321_P_ORG_ID',
			type: 'int',
			allowNull: false,
			allowBlank: false
		},
		{
			// Код ТБ
			name: 'TB_CODE',
			type: 'int',
			allowNull: false,
			allowBlank: false
		},
		{
			// TU
			name: 'TU',
			type: 'int',
			allowNull: false,
			allowBlank: false
		},
		{
			// PR
			name: 'PR',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// Участник
			name: 'NAMEU',
			type: 'string',
			allowNull: false,
			allowBlank: false
		},
		{
			// KODCR
			name: 'KODCR',
			type: 'string',
            allowNull: false,
            allowBlank: false
		},
		{
			// KODCN
			name: 'KODCN',
			type: 'string',
            allowNull: false,
            allowBlank: false
		},
		{
			// KD
			name: 'KD',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ОКПО
			name: 'SD',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// Рег.номер
			name: 'RG',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ИНН
			name: 'ND',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// VD1
			name: 'VD1',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// VD2
			name: 'VD2',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// VD3
			name: 'VD3',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
            allowNull: false,
            allowBlank: false,
			defaultValue: new Date('2099', '0', '1', '0', '0', '0')
		},
		{
			// VD4
			name: 'VD4',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// VD5
			name: 'VD5',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// VD6
			name: 'VD6',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
            allowNull: false,
            allowBlank: false,
			defaultValue: new Date('2099', '0', '1', '0', '0', '0')
		},
		{
			// VD7
			name: 'VD7',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
            allowNull: false,
            allowBlank: false,
			defaultValue: new Date('2099', '0', '1', '0', '0', '0')
		},
		{
			// MC1
			name: 'MC1',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// MC2
			name: 'MC2',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
            allowNull: false,
            allowBlank: false,
			defaultValue: new Date('2099', '0', '1', '0', '0', '0')
		},
		{
			// MC3
			name: 'MC3',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
            allowNull: false,
            allowBlank: false,
			defaultValue: new Date('2099', '0', '1', '0', '0', '0')
		},
		{
			// Дата регистрации
			name: 'GR',
			type: 'date',
            allowNull: false,
            allowBlank: false,
			dateFormat: 'Y-m-d H:i:s',
			defaultValue: new Date('2099', '0', '1', '0', '0', '0')
		},
		{
			// BP
			name: 'BP',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// VP
			name: 'VP',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ACC_B
			name: 'ACC_B',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ACC_COR_B
			name: 'ACC_COR_B',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// AMR_S
			name: 'AMR_S',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// AMR_R
			name: 'AMR_R',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// AMR_G
			name: 'AMR_G',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// AMR_U
			name: 'AMR_U',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// AMR_D
			name: 'AMR_D',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			//AMR_K
			name: 'AMR_K',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// AMR_O
			name: 'AMR_O',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ADRESS_S
			name: 'ADRESS_S',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ADRESS_R
			name: 'ADRESS_R',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ADRESS_G
			name: 'ADRESS_G',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ADRESS_U
			name: 'ADRESS_U',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ADRESS_D
			name: 'ADRESS_D',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ADRESS_K
			name: 'ADRESS_K',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// ADRESS_O
			name: 'ADRESS_O',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// NAME_B
			name: 'NAME_B',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// KODCN_B
			name: 'KODCN_B',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// BIK_B
			name: 'BIK_B',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// CARD_B
			name: 'CARD_B',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// NAME_IS
			name: 'NAME_IS',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// BIK_IS
			name: 'BIK_IS',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// NAME_R
			name: 'NAME_R',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// KODCN_R
			name: 'KODCN_R',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// BIK_R
			name: 'BIK_R',
			type: 'string',
            allowNull: false,
            allowBlank: false,
			defaultValue: '0'
		},
		{
			// NOTE
			name: 'NOTE',
			type: 'string'
		},
		{
			// Активно?
			name: 'ACTIVE_FL',
            allowNull: false,
            allowBlank: false,
			defaultValue: true,
			type: 'boolean'
		},
		{
			// Начало действия
			name: 'START_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// Окончание действия
			name: 'END_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// Кто создал
			name: 'CREATED_BY',
			type: 'string'
		},
		{
			// Когда создал
			name: 'CREATED_DATE',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// Кто изменил
			name: 'MODIFIED_BY',
			type: 'string'
		},
		{
			// Когда изменил
			name: 'MODIFIED_DATE',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		}
	],
	validators: {
		"_id_": 'presence',
		OES_321_P_ORG_ID: 'presence',
		ACTIVE_FL: 'presence',
		TU: 'presence',
		NAMEU: 'presence',
		KODCR: 'presence',
		KODCN: 'presence',
		KD: 'presence',
		RG: 'presence',
		GR: 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
							}
                            if (i.BRANCH_FL != undefined) {
                                i.BRANCH_FL = (i.BRANCH_FL == 'Y');
                            }
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ACTIVE_FL != undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
                        if (i.BRANCH_FL != undefined) {
                            i.BRANCH_FL = (i.BRANCH_FL ? 'Y' : 'N');
                        }
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OES321PartyOrg',
			action: 'read'
		}
	}
});
