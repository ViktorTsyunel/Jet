Ext.define('AML.model.OPOKRuleInOut', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKRuleInOut',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
