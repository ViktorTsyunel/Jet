Ext.define('AML.model.OES321Details', {
    extend: 'Ext.data.Model',
    alias: 'model.oes321details',
    idProperty: '_id_',
    fields: [
        {
            // ID
            identifier: true,
            name: '_id_',
            type: 'int'
        },
        {
            // Тип сообщения
            name: 'ACTION',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // БИК
            name: 'BIK_S',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // БИК
            name: 'BIK_SS',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Сведения предоставлены филиалом Банка
            name: 'BRANCH',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Признак Банка плательщика
            name: 'B_PAYER',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Признак Банка получателя
            name: 'B_RECIP',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата контроля
            name: 'CHECK_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Валюта проведения
            name: 'CURREN',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Валюта конверсии
            name: 'CURREN_CON',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата ОЭС
            name: 'DATA',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата представления
            name: 'DATE_P',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата платежного документа
            name: 'DATE_PAY_D',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дата выявления
            name: 'DATE_S',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Дополнительная информация
            name: 'DESCR_1',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            defaultValue: '0'
        },
        {
            // Дополнительная информация
            name: 'DESCR_2',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value) {
                return value || '0'
            }
        },
        {
            // Доп. коды вида операции
            name: 'DOP_V',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код по ОКАТО
            name: 'KTU_S',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код по ОКАТО
            name: 'KTU_SS',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Комментарий к сообщению
            name: 'MANUAL_CMNT',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код драгоценного металла
            name: 'METAL',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // ИНН
            name: 'ND_KO',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // № филиала
            name: 'NUMBF_S',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // № филиала
            name: 'NUMBF_SS',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер ОЭС
            name: 'NUMB_P',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер платежного документа
            name: 'NUM_PAY_D',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер OES_321_ID
            name: 'OES_321_ID',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер OES_321_ORG_ID
            name: 'OES_321_ORG_ID',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            // Операция совершается по поручению и от имени
            name: 'PART',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Основание совершения операции
            name: 'PRIM_1',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            defaultValue: '0'
        },
        {
            // Основание совершения операции
            name: 'PRIM_2',
            type: 'string',
            allowNull: true,
            allowBlank: true,
            convert: function (value) {
                return value || '0'
            }
        },
        {
            // Код признаков необычных операций и сделок
            name: 'PRIZ6001',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Объект сообщения
            name: 'PRIZ_SD',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Рег. номер
            name: 'REGN',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Номер операции
            name: 'REVIEW_ID',
            type: 'int',
            allowNull: true,
            allowBlank: true
        },
        {
            //
            name: 'SEND_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true,
            allowBlank: true
        },
        {
            // Сумма в валюте
            name: 'SUM',
            type: 'number',
            allowNull: true,
            allowBlank: true,
            convert: function (value) {
                return parseFloat(value.toString().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'));
            }
        },
        {
            // Сумма в рублях
            name: 'SUME',
            type: 'number',
            allowNull: true,
            allowBlank: true,
            convert: function (value) {
                return parseFloat(value.toString().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'));
            }
        },
        {
            // Сумма в валюте конверсии
            name: 'SUM_CON',
            type: 'number',
            allowNull: true,
            allowBlank: true,
            convert: function (value) {
                return parseFloat(value.toString().replace(new RegExp(' ', 'g'), '').replace(new RegExp(',', 'g'), '.'));
            }
        },
        {
            // Телефон ответственного
            name: 'TEL',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Признак операции, связанной с финансированием терроризма
            name: 'TERROR',
            type: 'string',
            allowNull: true,
            allowBlank: true
        },
        {
            // Код вида операции
            name: 'VO',
            type: 'int',
            allowNull: true,
            allowBlank: true
        }
    ]
});