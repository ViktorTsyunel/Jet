Ext.define('AML.model.AlertGOZHistory',{
	extend:'Ext.data.Model',
	alias:'model.alertgozhistory',
	fields: [
		{
            // Дата и время
            name: 'CHANGE_DT',
			type: 'string'
		},
		{
            name: 'RECORD_TYPE_CD',
			type: 'string'
		},
		{
            name: 'OLD_ORIG_GOZ_OP_CD',
			type: 'string'
		},
		{
            name: 'NEW_ORIG_GOZ_OP_CD',
			type: 'string'
		},
		{
            name: 'CHANGE_REASON_TX',
			type: 'string'
		},
        {
            name: 'SRC_USER_NM',
            type: 'string'
        },
        {
            name: 'SRC_CD',
            type: 'string'
        }
	],
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		extraParams: {
			form: 'GOZChanges',
			action: 'readByTrxnId'
		}
	}
});
