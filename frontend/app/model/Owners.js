Ext.define('AML.model.Owners', {
	extend: 'Ext.data.Model',
	alias: 'model.Owners',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			mapping: '_id_',
			type: 'int'
		},
		{
			// Имя
			name: 'name',
			mapping: 'OWNER_DSPLY_NM',
			type: 'string'
		},
		{
			// Логин
			name: 'login',
			mapping: 'OWNER_ID',
			type: 'string'
		},
		{
			name: 'IS_ALLOW_ALERT_EDITING',
			mapping: 'IS_ALLOW_ALERT_EDITING',
			type: 'string'
		},
		{
			name: 'IS_CURRENT',
			mapping: 'IS_CURRENT',
			type: 'string'
		},
		{
			name: 'IS_SUPERVISOR',
			mapping: 'IS_SUPERVISOR',
			type: 'string'
		}
	]
});
