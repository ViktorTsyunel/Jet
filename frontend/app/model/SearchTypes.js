Ext.define('AML.model.SearchTypes', {
	extend: 'Ext.data.Model',
	alias: 'model.SearchTypes',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
