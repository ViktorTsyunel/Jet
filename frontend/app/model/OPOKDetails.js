Ext.define('AML.model.OPOKDetails', {
	extend: 'Ext.data.Model',
	alias: 'model.opokdetails',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
			// Код ОПОК
			name: 'OPOK_NB',
			type: 'int'
		},
		{
			// Пороговая сумма
			name: 'LIMIT_AM',
			type: 'number'
		},
		{
			// Начало действия
			name: 'START_DT',
			allowNull: true,
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// Окончание действия
			name: 'END_DT',
			allowNull: true,
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
		},
		{
			// Учитывать?
			name: 'ACTIVE_FL',
			type: 'boolean',
			defaultValue: true
		}
	],
	validators: {
		"_id_": 'presence',
		OPOK_NB: 'presence',
		LIMIT_AM: 'presence',
		ACTIVE_FL: 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ACTIVE_FL != undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OPOKDetails',
			action: 'read'
		}
	}	
});
