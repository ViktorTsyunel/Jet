Ext.define('AML.model.ObjectsAccess', {
	extend: 'Ext.data.Model',
	alias: 'model.ObjectsAccess',
	idProperty: 'id',
	fields: [
		{
			// id - код объекта
			identifier: true,
			name: 'id',
			mapping: '_id_',
			type: 'string'
		},
		{
			// Вид доступа: F - полный, R - только чтение, T - тестовый
			name: 'mode',
			mapping: 'ACCESS_MODE',
			type: 'string'
		}
	],
    proxy: {
        url: common.globalinit.ajaxUrl,
        type: 'ajax',
        paramsAsJson: true,
        noCache: false,
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            allowSingle: false,
            rootProperty: 'data'
        },
        extraParams: {
            form: 'ObjectsAccess',
            action: 'read'
        }
    }
});
