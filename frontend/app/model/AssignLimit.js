Ext.define('AML.model.AssignLimit', {
	extend: 'Ext.data.Model',
	alias: 'model.AssignLimit',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
            // ID записи лимита
            name: 'LIMIT_SEQ_ID',
			type: 'int'
		},
        {
            // Вид лимита
            name: 'LIMIT_TP_CD',
            defaultValue: 'D',
            type: 'string'
        },
        {
            // Символьный код роли
            name: 'ROLE_CD',
			type: 'string'
		},
		{
            // ID ответственного (пользователя)
            name: 'OWNER_SEQ_ID',
			type: 'int',
            allowNull: true
		},
        {
            // Величина лимита первой очереди распределения
            name: 'ORDER1_LIMIT_QT',
			type: 'int'
		},
        {
            // Лимит полный
            name: 'FULL_LIMIT_QT',
            type: 'int'
        },
        {
            // Флаг Y/N активности данного лимита
            name: 'ACTIVE_FL',
            defaultValue: true,
            type: 'boolean'
        },
        {
            // Начало действия лимита
            name: 'START_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Окончание действия лимита
            name: 'END_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Пользователь, создавший данную запись
            name: 'CREATED_BY',
            type: 'string'
        },
        {
            // Дата и время создания данной записи
            name: 'CREATED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Пользователь, изменивший данную запись последним
            name: 'MODIFIED_BY',
            type: 'string'
        },
        {
            // Дата и время последнего изменения данной записи
            name: 'MODIFIED_DATE',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
	],
	validators: {
		"_id_": 'presence',
        LIMIT_SEQ_ID: 'presence',
        LIMIT_TP_CD: 'presence',
        ORDER1_LIMIT_QT: 'presence',
        FULL_LIMIT_QT: 'presence',
        ACTIVE_FL: 'presence'
    },
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACTIVE_FL !== undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL === 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ACTIVE_FL !== undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'AssignLimit',
			action: 'read'
		}
	}
});
