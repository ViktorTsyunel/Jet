Ext.define('AML.model.OPOKRuleKGRKO', {
    extend: 'Ext.data.Model',
    alias: 'model.OPOKRuleKGRKO',
    idProperty: 'id',
    fields: [
        {
            identifier: true,
            name: 'id',
            type: 'string'
        },
        {
            name: 'label',
            type: 'string'
        },
        {
            name: 'all',
            type: 'string'
        }
    ]
});
