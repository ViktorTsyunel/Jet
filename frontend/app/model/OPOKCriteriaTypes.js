Ext.define('AML.model.OPOKCriteriaTypes', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKCriteriaTypes',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'int'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		},
		{
			// Описание
			name: 'description',
			type: 'string'
		}
	]
});
