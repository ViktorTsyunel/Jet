Ext.define('AML.model.AlertFieldsLog',{
	extend:'Ext.data.Model',
	alias:'model.alertfieldslog',
	fields: [
		{
			name: 'ORDER_NB',
			type: 'int'
		},
		{
			name: 'BLOCK_TX',
			type: 'string'
		},
		{
            // Дата и время
            name: 'FIELD_NM',
			type: 'string'
		},
		{
            //
            name: 'FIELD_CD',
			type: 'string'
		},
		{
            //
            name: 'OLD_VALUE',
			type: 'string'
		},
		{
            //
            name: 'NEW_VALUE',
			type: 'string'
		},
		{
            //
            name: 'CHANGED_FLAG',
			defaultValue: false,
			type: 'boolean'
		}
	],
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		extraParams: {
			form: 'OES321ChangeDetails',
			action: 'read'
		}
	}
});
