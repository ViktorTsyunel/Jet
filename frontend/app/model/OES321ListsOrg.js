Ext.define('AML.model.OES321ListsOrg', {
	extend: 'Ext.data.Model',
	alias: 'model.oes321listsorg',
	idProperty: '_id_',
	// clientIdProperty: '#clientId#',
	identifier: 'negative',
	fields: [
		{
			// ID
			identifier: true,
			name: '_id_',
			type: 'int'
		},
		{
            // Id записи
            name: 'WATCH_LIST_ORG_ID',
			type: 'int'
		},
        {
            // Код перечня
            name: 'WATCH_LIST_CD',
			type: 'string'
		},
		{
			// № п/п
			name: 'ORDER_NB',
			type: 'int'
		},
		{
            // Наименование
            name: 'ORG_NM',
			type: 'string'
		},
        {
            // Адрес
            name: 'ADDR_TX',
			type: 'string'
		},
        {
            // ИНН
            name: 'INN_NB',
            type: 'string'
        },
        {
            // Номер счета
            name: 'ACCT_NB',
            type: 'string'
        },
        {
			// Активно?
			name: 'ACTIVE_FL',
			defaultValue: true,
			type: 'boolean'
        },
        {
            // Начало действия
            name: 'START_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Окончание действия
            name: 'END_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Примечание
            name: 'NOTE_TX',
            type: 'string'
        },
        {
            // Кто создал
            name: 'CREATED_BY',
            type: 'string'
        },
        {
            // Когда создал
            name: 'CREATED_DATE',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Кто изменил
            name: 'MODIFIED_BY',
            type: 'string'
        },
        {
            // Когда изменил
            name: 'MODIFIED_DATE',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s'
        },
        {
            // Код в источнике
            name: 'SRC_CD',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
	],
	validators: {
		"_id_": 'presence',
		WATCH_LIST_ORG_ID: 'presence',
		ACTIVE_FL: 'presence'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
							}
                            if (i.BRANCH_FL != undefined) {
                                i.BRANCH_FL = (i.BRANCH_FL === '1');
                            }
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ACTIVE_FL != undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
                        if (i.BRANCH_FL != undefined) {
                            i.BRANCH_FL = (i.BRANCH_FL ? '1' : '0');
                        }
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'WatchListOrg',
			action: 'read'
		}
	}
});
