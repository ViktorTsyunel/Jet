Ext.define('AML.model.ReportsFromService', {
	extend: 'Ext.data.Model',
	alias: 'model.ReportsFromService',
    title: 'Отчеты',
    pageSize: 0,
    remoteSort: false,
    remoteFilter: false,
    autoLoad: true,
	fields: [
		{
			// id - код объекта
			name: 'REP_ID',
			mapping: 'REP_ID',
			type: 'string'
		},
		{
			//
			name: 'REP_NM',
			mapping: 'REP_NM',
			type: 'string'
		}
	],
    proxy: {
        url: common.globalinit.ajaxUrlReports,
        type: common.globalinit.proxyType.ajax,
        paramsAsJson: true,
        removeDefaultParams: true,
        limitParam: '',
        pageParam: '',
        startParam: '',
        noCache: false,
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message',
            totalProperty: 'totalCount'
        },
        extraParams: {
           rep_id: '1001'
        }
    }
});
