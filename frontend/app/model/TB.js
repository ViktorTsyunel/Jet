Ext.define('AML.model.TB', {
	extend: 'Ext.data.Model',
	alias: 'model.TB',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			mapping: '_id_',
			type: 'int'
		},
		{
			// Наименование
			name: 'label',
			mapping: 'TB_NAME',
			type: 'string'
		}
	]
});
