Ext.define('AML.model.ImpType', {
	extend: 'Ext.data.Model',
	alias: 'model.ImpType',
    pageSize: 0,
    remoteSort: false,
    remoteFilter: false,
    autoLoad: true,
    fields: [
		{
			name: 'ACTUAL_DT',
			type: 'date',
			dateFormat: 'Y-m-d H:i:s',
			allowNull: true
		},
        {
            name: 'CONTROL_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true
        },
        {
            name: 'LOAD_DT',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s',
            allowNull: true
        }
	],
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL === 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'ImpType',
			action: 'read'
		},
		sorters: [
			{
				property: 'ACTUAL_DT',
				direction: 'DESC'
			}
		]
	}
});
