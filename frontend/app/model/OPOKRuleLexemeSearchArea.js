Ext.define('AML.model.OPOKRuleLexemeSearchArea', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKRuleLexemeSearchArea',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
