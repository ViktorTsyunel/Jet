Ext.define('AML.model.AlertHistoryLog',{
	extend:'Ext.data.Model',
	alias:'model.alerthistorylog',
	fields: [
		{
			name: 'OES_321_ID',
			type: 'int'
		},
		{
            // Дата и время
            name: 'BATCH_DATE',
			type: 'string'
            // type: 'date',
            // dateFormat: 'Y-m-d H:i:s'
		},
		{
            // список полей
            name: 'FIELD_LIST',
			type: 'string'
		},
		{
            // Пользователь
            name: 'MODIFIED_BY',
			type: 'string'
		},
		{
            // действие
            name: 'ACTVY_TYPE_NM',
			type: 'string'
		},
		{
            // айпи
            name: 'IP_ADDRESS',
			type: 'string'
		}
	],
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		extraParams: {
			form: 'OES321Changes',
			action: 'readByOesId'
		}
	}
});
