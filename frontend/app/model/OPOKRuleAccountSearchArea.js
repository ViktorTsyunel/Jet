Ext.define('AML.model.OPOKRuleAccountSearchArea', {
	extend: 'Ext.data.Model',
	alias: 'model.OPOKRuleAccountSearchArea',
	idProperty: 'id',
	fields: [
		{
			// id
			identifier: true,
			name: 'id',
			type: 'string'
		},
		{
			// Наименование
			name: 'label',
			type: 'string'
		}
	]
});
