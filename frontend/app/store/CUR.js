Ext.define('AML.store.CUR', {
	requires: [
		'AML.model.CUR'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.CUR',
	alias: 'store.cur',
	title: 'Cправочник валют, металов',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	storeId: 'CURID',
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: common.globalinit.proxyType.ajax,
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		extraParams: {
			form: 'CUR_REF',
			action: 'read'
		}
	},
    sorters: [
        {
			property: 'CUR_CODE',
			direction: 'ASC'
        }
    ]
});
