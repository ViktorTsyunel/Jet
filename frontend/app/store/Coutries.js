Ext.define('AML.store.Coutries', {
	requires: [
		'AML.model.Coutries'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.Coutries',
	alias: 'store.coutries',
	title: 'Справочник Стран',
	fields: [{
		name: 'ISO_NUMERIC_CD',
		type: 'string'
	}],
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: common.globalinit.proxyType.ajax,
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		extraParams: {
			form: 'CNTRY_NUM_REF',
			action: 'read'
		}
	}
});
