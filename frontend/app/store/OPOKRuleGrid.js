Ext.define('AML.store.OPOKRuleGrid', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokrulegrid',
	title: 'Справочник правил выявления (таблица)',
	source: 'OPOKRule',
	sorters: [
		{
			property: 'OPOK_NB',
			direction: 'ASC'
		},
		{
			property: 'VERS_NUMBER',
			direction: 'DESC'
		},
		{
			property: 'PRIORITY_NB',
			direction: 'ASC'
		}
	],
	filters: [
		{
			property: 'VERS_STATUS_CD',
			operator: '!=',
			value: 'ARC'
		}
	]
});
