Ext.define('AML.store.OPOKCriteriaT5', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokcriteriat5',
	title: 'Справочник стандартных условий выявления 5-го типа',
	source: 'OPOKCriteria',
	sorters: [
		{
			property: 'CRIT_CD',
			direction: 'ASC'
		}
	],
	filters: [
		{
			property: 'CRIT_TP_CD',
			value: 5
		}
	]
});
