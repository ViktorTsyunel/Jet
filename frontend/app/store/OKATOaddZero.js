Ext.define('AML.store.OKATOaddZero', {
	requires: [
		'AML.model.OKATO'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OKATO',
	alias: 'store.OKATOaddZero',
	title: 'Код по OKATO ',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: common.globalinit.proxyType.ajax,
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount',
            transform: {
                fn: function (data) {
                    if (Ext.isArray(data.data)) {
                        var records = [
                            { OKATO_CD: '0', OKATO_OBJ_NM: 'Пусто', _ID_: '0' },
                            { OKATO_CD: '00', OKATO_OBJ_NM: 'Иностранное государство', _ID_: '00' }
                        ];
                        Ext.Array.forEach(data.data, function (record) {
                            if (record.OKATO_CD !== '0' || record.OKATO_CD !== '00') {
                                records.push(record);
                            }
                        });
                    }
                    return records;
                },
                scope: this
            }
		},
		extraParams: {
			form: 'REGION_REF',
			action: 'read'
		}
	}
});
