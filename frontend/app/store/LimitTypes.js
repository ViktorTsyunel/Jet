Ext.define('AML.store.LimitTypes', {
	extend: 'Ext.data.Store',

	alias: 'store.limittypes',
	title: 'Справочник типов лимитов',

	remoteSort: false,
	remoteFilter: false,

	fields: ['LIMIT_TP_CD', 'LIMIT_TP_NM'],
    data: [
        {LIMIT_TP_NM: 'По умолчанию', LIMIT_TP_CD: 'D'},
        {LIMIT_TP_NM: 'Для роли', LIMIT_TP_CD: 'R'},
        {LIMIT_TP_NM: 'Для пользователя', LIMIT_TP_CD: 'U'}
    ]
});
