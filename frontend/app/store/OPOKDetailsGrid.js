Ext.define('AML.store.OPOKDetailsGrid', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokdetailsgrid',
	title: 'Доп. справочник для кодов ОПОК (таблица)',
	source: 'OPOKDetails',
	sorters: [
		{
			property: 'OPOK_NB',
			direction: 'ASC'
		}
	]
});
