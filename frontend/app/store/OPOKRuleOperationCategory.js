Ext.define('AML.store.OPOKRuleOperationCategory', {
	requires: [
		'AML.model.OPOKRuleOperationCategory'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKRuleOperationCategory',
	alias: 'store.opokruleoperationcategory',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			"id": 'WT',
			"label": 'Безналичные'
		},
		{
			"id": 'CT',
			"label": 'Наличные'
		},
		{
			"id": 'BOT',
			"label": 'Внутренние'
		}
	],
	sorters: [
		{
			property: 'id',
			direction: 'ASC'
		}
	]
});
