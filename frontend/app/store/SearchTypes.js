Ext.define('AML.store.SearchTypes', {
	requires: [
		'AML.model.SearchTypes'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.SearchTypes',
	alias: 'store.searchtypes',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			"id": 'ALL',
			"label": 'Везде'
		},
		{
			"id": 'OPOK',
			"label": 'Основной вид оп.'
		},
		{
			"id": 'ADD_OPOK',
			"label": 'Доп. виды оп.'
		}
	]
});
