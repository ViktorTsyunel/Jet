/**
 * Хранилище для выявленных операций
 */
Ext.define('AML.store.Alerts', {
	extend: 'Ext.data.Store',
	requires: [
		'AML.model.Alerts'
	],
	model: 'AML.model.Alerts',
	alias: 'store.alerts',
	title: 'Выявленные операции'
});