Ext.define('AML.store.OPOKTest', {
	requires: [
		'AML.model.OPOKTest',
		'Ext.Array'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKTest',
	alias: 'store.opoktest',
	title: 'Результат тестового поиска операций',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: false,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ORIG_OWN_FL !== undefined && i.ORIG_OWN_FL !== null) {
								i.ORIG_OWN_FL = (i.ORIG_OWN_FL == 'Y');
							}
							if (i.BENEF_OWN_FL !== undefined && i.BENEF_OWN_FL !== null) {
								i.BENEF_OWN_FL = (i.BENEF_OWN_FL == 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OPOKTest',
			action: 'read'
		}
	}
});
