Ext.define('AML.store.OKATO', {
	requires: [
		'AML.model.OKATO'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OKATO',
	alias: 'store.okato',
	title: 'Код по OKATO',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: common.globalinit.proxyType.ajax,
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		extraParams: {
			form: 'REGION_REF',
			action: 'read'
		}
	}
});
