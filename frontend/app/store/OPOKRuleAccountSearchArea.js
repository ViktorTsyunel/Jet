Ext.define('AML.store.OPOKRuleAccountSearchArea', {
	requires: [
		'AML.model.OPOKRuleAccountSearchArea'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKRuleAccountSearchArea',
	alias: 'store.opokruleaccountsearcharea',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			id: 'СУ',
			label: 'Счёт участника'
		},
		{
			id: 'СП',
			label: 'Счёт проводки'
		},
		{
			id: 'НУ',
			label: 'Наименование участника'
		},
		{
			id: 'НП',
			label: 'Назначение платежа'
		}
	],
	sorters: [
		{
			property: 'id',
			direction: 'ASC'
		}
	]
});
