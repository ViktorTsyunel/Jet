Ext.define('AML.store.TB', {
	requires: [
		'AML.model.TB'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.TB',
	alias: 'store.tb',
	title: 'Справочник тербанков',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'TB',
			action: 'read'
		}
	},
    sorters: [
        {
            property: 'TB_ID',
            direction: 'ASC'
        }
    ]
});
