Ext.define('AML.store.SearchDirection', {
	requires: [
		'AML.model.SearchDirection'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.SearchDirection',
	alias: 'store.searchdirection',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			"id": 'ALL',
			"label": 'Везде'
		},
		{
			"id": 'ORIG',
			"label": 'Плательщик'
		},
		{
			"id": 'BENEF',
			"label": 'Получатель'
		}
	]
});
