Ext.define('AML.store.OPOKCriteriaT2', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokcriteriat2',
	title: 'Справочник стандартных условий выявления 2-го типа',
	source: 'OPOKCriteria',
	sorters: [
		{
			property: 'CRIT_CD',
			direction: 'ASC'
		}
	],
	filters: [
		{
			property: 'CRIT_TP_CD',
			value: 2
		}
	]
});
