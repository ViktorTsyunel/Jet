Ext.define('AML.store.Statuses', {
    requires: [
        'AML.model.Statuses'
    ],
    extend: 'Ext.data.Store',
    model: 'AML.model.Statuses',
    alias: 'store.statuses',
    title: 'Справочник статусов операций',
    pageSize: 0,
    remoteSort: false,
    remoteFilter: false,
    autoLoad: true,
    proxy: {
        url: common.globalinit.ajaxUrl,
        type: 'ajax',
        paramsAsJson: true,
        noCache: false,
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            allowSingle: false,
            rootProperty: 'data'
        },
        extraParams: {
            form: 'Statuses',
            action: 'read'
        }
    },
    sorters: [
        {
            property: 'label',
            direction: 'ASC'
        }
    ]
});
