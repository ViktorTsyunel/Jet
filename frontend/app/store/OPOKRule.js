Ext.define('AML.store.OPOKRule', {
	requires: [
		'AML.model.OPOKRule',
		'Ext.Array'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKRule',
	alias: 'store.opokrule',
	title: 'Справочник правил выявления',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true/*,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACCT_NULL_FL != undefined) {
								i.ACCT_NULL_FL = (i.ACCT_NULL_FL == 'Y');
							}
							if (i.ACCT2_NULL_FL != undefined) {
								i.ACCT2_NULL_FL = (i.ACCT2_NULL_FL == 'Y');
							}
							if (i.ACCT_REVERSE_FL != undefined) {
								i.ACCT_REVERSE_FL = (i.ACCT_REVERSE_FL == 'Y');
							}
							if (i.ERR_FL != undefined) {
								i.ERR_FL = (i.ERR_FL == 'Y');
							}
							if (i.TEST_ERR_FL != undefined) {
								i.TEST_ERR_FL = (i.TEST_ERR_FL == 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ACCT_NULL_FL != undefined) {
							i.ACCT_NULL_FL = (i.ACCT_NULL_FL ? 'Y' : 'N');
						}
						if (i.ACCT2_NULL_FL != undefined) {
							i.ACCT2_NULL_FL = (i.ACCT2_NULL_FL ? 'Y' : 'N');
						}
						if (i.ACCT_REVERSE_FL != undefined) {
							i.ACCT_REVERSE_FL = (i.ACCT_REVERSE_FL ? 'Y' : 'N');
						}
						if (i.ERR_FL != undefined) {
							i.ERR_FL = (i.ERR_FL ? 'Y' : 'N');
						}
						if (i.TEST_ERR_FL != undefined) {
							i.TEST_ERR_FL = (i.TEST_ERR_FL ? 'Y' : 'N');
						}
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OPOKRule',
			action: 'read'
		}
	}*/
});
