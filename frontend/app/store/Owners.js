Ext.define('AML.store.Owners', {
	requires: [
		'AML.model.Owners'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.Owners',
	alias: 'store.owners',
	title: 'Справочник ответственных',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.IS_CURRENT != undefined) {
								i.IS_CURRENT = (i.IS_CURRENT == 'Y');
							}
						});
						Ext.Array.forEach(data.data, function (i) {
							if (i.IS_SUPERVISOR != undefined) {
								i.IS_SUPERVISOR = (i.IS_SUPERVISOR == 'Y');
							}
						});
						Ext.Array.forEach(data.data, function (i) {
							if (i.is_allow_alert_editing != undefined) {
								i.is_allow_alert_editing = (i.is_allow_alert_editing == 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'Owners',
			action: 'read'
		}
	}
});
