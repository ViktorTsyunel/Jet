Ext.define('AML.store.OPOKCriteriaGrid', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokcriteriagrid',
	title: 'Справочник стандартных условий выявления (таблица)',
	source: 'OPOKCriteria',
	sorters: [
		{
			property: 'CRIT_CD',
			direction: 'ASC'
		}
	]
});
