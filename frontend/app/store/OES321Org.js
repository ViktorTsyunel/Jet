Ext.define('AML.store.OES321Org', {
    requires: [
        'AML.model.OES321Org'
    ],
    extend: 'Ext.data.Store',
    model: 'AML.model.OES321Org',
    alias: 'store.OES321Org',
    title: 'Справочник филиалов',
    pageSize: 0,
    remoteSort: false,
    remoteFilter: false,
    autoLoad: true,
    proxy: {
        url: common.globalinit.ajaxUrl,
        type: 'ajax',
        paramsAsJson: true,
        noCache: false,
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            allowSingle: false,
            rootProperty: 'data'
        },
        extraParams: {
            form: 'OES321Org',
            action: 'read'
        }
    },
    sorters: [
        {
            property: 'OES_321_ORG_ID',
            direction: 'ASC'
        }
    ]
});
