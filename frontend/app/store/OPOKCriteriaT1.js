Ext.define('AML.store.OPOKCriteriaT1', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokcriteriat1',
	title: 'Справочник стандартных условий выявления 1-го типа',
	source: 'OPOKCriteria',
	sorters: [
		{
			property: 'CRIT_CD',
			direction: 'ASC'
		}
	],
	filters: [
		{
			property: 'CRIT_TP_CD',
			value: 1
		}
	]
});
