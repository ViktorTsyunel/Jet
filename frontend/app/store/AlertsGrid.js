Ext.define('AML.store.AlertsGrid', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.alertsgrid',
	title: 'Выявленные операции (таблица)',
	source: 'Alerts',
	sorters: [
		{
			property: 'number',
			direction: 'ASC'
		}
	]
});
