Ext.define('AML.store.OPOKGrid', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokgrid',
	title: 'Справочник кодов ОПОК (таблица)',
	source: 'OPOK',
	sorters: [
		{
			property: 'OPOK_NB',
			direction: 'ASC'
		}
	]
});
