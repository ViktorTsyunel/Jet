Ext.define('AML.store.OPOKRuleLexemeSearchArea', {
	requires: [
		'AML.model.OPOKRuleLexemeSearchArea'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKRuleLexemeSearchArea',
	alias: 'store.opokrulelexemesearcharea',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			id: 'НП',
			label: 'Назначение платежа'
		},
		{
			id: 'У',
			label: 'Участник операции 1'
		},
		{
			id: 'У2',
			label: 'Участник операции 2'
		}
	],
	sorters: [
		{
			property: 'id',
			direction: 'ASC'
		}
	]
});
