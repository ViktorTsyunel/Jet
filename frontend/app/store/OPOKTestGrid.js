Ext.define('AML.store.OPOKTestGrid', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opoktestgrid',
	title: 'Результат тестового поиска операций (таблица)',
	source: 'OPOKTest',
	sorters: [
		{
			property: 'TRXN_DESC',
			direction: 'ASC'
		}
	]
});
