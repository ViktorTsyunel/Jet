Ext.define('AML.store.TBOSB', {
	requires: [
		'AML.model.TBOSB'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.TBOSB',
	alias: 'store.tbosb',
	title: 'Справочник ОСБ',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'TBOSB',
			action: 'read'
		}
	},
    sorters: [
        {
            property: 'ORG_NM',
            direction: 'ASC'
        }
    ]
});
