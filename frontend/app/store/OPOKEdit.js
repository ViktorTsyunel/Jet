Ext.define('AML.store.OPOKEdit', {
    requires: [
        'AML.model.OPOKEdit',
        'Ext.Array'
    ],
    extend: 'Ext.data.Store',
    model: 'AML.model.OPOKEdit',
    alias: 'store.opokedit',
    title: 'Редактирование кодов ОПОК',
    pageSize: 0,
    proxy: {
        //url: common.globalinit.ajaxUrl,
        type: 'memory',
        paramsAsJson: true,
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message'
        }
    }
});