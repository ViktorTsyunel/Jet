Ext.define('AML.store.OPOKRuleInOut', {
	requires: [
		'AML.model.OPOKRuleInOut'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKRuleInOut',
	alias: 'store.opokruleinout',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			"id": 'C',
			"label": 'Зачисление'
		},
		{
			"id": 'D',
			"label": 'Списание'
		}
	],
	sorters: [
		{
			property: 'id',
			direction: 'ASC'
		}
	]
});
