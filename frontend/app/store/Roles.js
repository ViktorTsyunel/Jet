Ext.define('AML.store.Roles', {
	requires: [
		'AML.model.Roles'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.Roles',
	alias: 'store.roles',
	title: 'Справочник ролей',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'Roles',
			action: 'read'
		}
	}
});
