Ext.define('AML.store.OPOKCriteriaUses', {
	requires: [
		'AML.model.OPOKCriteriaUses'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKCriteriaUses',
	alias: 'store.opokcriteriauses',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			"id": 'ВЫЯВЛЕНИЕ',
			"label": 'Выявление'
		},
		{
			"id": 'ЗАГРУЗКА',
			"label": 'Загрузка'
		}
	],
	sorters: [
		{
			property: 'id',
			direction: 'ASC'
		}
	]
});
