Ext.define('AML.store.OPOKDetails', {
	requires: [
		'AML.model.OPOKDetails',
		'Ext.Array'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKDetails',
	alias: 'store.opokdetails',
	title: 'Доп. справочник для кодов ОПОК',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true/*,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST',
			update: 'POST',
			create: 'POST',
			destroy: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.ACTIVE_FL != undefined) {
								i.ACTIVE_FL = (i.ACTIVE_FL == 'Y');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						create: 'create',
						update: 'update',
						destroy: 'delete',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.ACTIVE_FL != undefined) {
							i.ACTIVE_FL = (i.ACTIVE_FL ? 'Y' : 'N');
						}
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OPOKDetails',
			action: 'read'
		}
	}*/
});
