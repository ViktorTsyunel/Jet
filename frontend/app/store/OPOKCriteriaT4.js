Ext.define('AML.store.OPOKCriteriaT4', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokcriteriat4',
	title: 'Справочник стандартных условий выявления 4-го типа',
	source: 'OPOKCriteria',
	sorters: [
		{
			property: 'CRIT_CD',
			direction: 'ASC'
		}
	],
	filters: [
		{
			property: 'CRIT_TP_CD',
			value: 4
		}
	]
});
