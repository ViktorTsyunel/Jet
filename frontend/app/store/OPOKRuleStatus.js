Ext.define('AML.store.OPOKRuleStatus', {
	requires: [
		'AML.model.OPOKRuleStatus'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKRuleStatus',
	alias: 'store.opokrulestatus',
	proxy: {
		type: 'memory'
	},
	data: [
		{
			"id": 'ACT',
			"label": 'Действующее'
		},
		{
			"id": 'TEST',
			"label": 'В тестировании'
		},
		{
			"id": 'DEV',
			"label": 'В разработке'
		},
		{
			"id": 'ARC',
			"label": 'В архиве'
		}
	],
	sorters: [
		{
			property: 'id',
			direction: 'ASC'
		}
	]
});
