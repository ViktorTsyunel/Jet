Ext.define('AML.store.OPOKaddZero', {
	requires: [
		'AML.model.OPOKaddZero'
	],
	extend: 'Ext.data.Store',
	alias: 'store.OPOKaddZero',
	title: 'Справочник кодов ОПОК',

    model: 'AML.model.OPOKaddZero',
    pageSize: 0,
    remoteSort: false,
    remoteFilter: false,
    autoLoad: true,
    sorters: {
        property: 'OPOK_NB',
        direction: 'ASC'
    }
});
