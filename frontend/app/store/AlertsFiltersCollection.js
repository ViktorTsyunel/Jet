Ext.define('AML.store.AlertsFiltersCollection', {
	requires: [
		'AML.model.AlertsFiltersCollection'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.AlertsFiltersCollection',
	alias: 'store.alertsfilterscollection',
	title: 'Справочник фильтров',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'AlertsFiltersCollection',
			action: 'read'
		}
	}
});
