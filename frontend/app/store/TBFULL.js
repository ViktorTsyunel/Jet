Ext.define('AML.store.TBFULL', {
	requires: [
		'AML.model.TBFULL'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.TBFULL',
	alias: 'store.tbfull',
	title: 'Cправочник Территориальный банк',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: common.globalinit.proxyType.ajax,
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			transform: {
				fn: function (data) {
					if (Ext.isArray(data.data)) {
						Ext.Array.forEach(data.data, function (i) {
							if (i.BRANCH_FL != undefined) {
								i.BRANCH_FL = (i.BRANCH_FL === '1');
							}
						});
					}
					return data;
				},
				scope: this
			},
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		writer: {
			type: 'json',
			transform: {
				fn: function (data, request) {
					var actionMap = {
						update: 'update',
						read: 'read'
					};
					request.setParam('action', actionMap[request.getAction()] || 'read');
					Ext.Array.forEach(data, function (i) {
						if (i.BRANCH_FL != undefined) {
							i.BRANCH_FL = (i.BRANCH_FL ? '1' : '0');
						}
					});
					return data;
				},
				scope: this
			},
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'OES321TB_REF',
			action: 'read'
		}
	}
});
