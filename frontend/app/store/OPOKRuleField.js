Ext.define('AML.store.OPOKRuleField', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokrulefield',
	title: 'Справочник правил выявления (комбобокс)',
	source: 'OPOKRule',
	sorters: [
		{
			property: 'OPOK_NB',
			direction: 'ASC'
		},
		{
			property: 'VERS_NUMBER',
			direction: 'DESC'
		},
		{
			property: 'PRIORITY_NB',
			direction: 'ASC'
		}
	],
	filters: [
		{
			property: 'VERS_STATUS_CD',
			operator: '!=',
			value: 'ARC'
		}
	]
});
