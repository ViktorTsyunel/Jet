Ext.define('AML.store.OPOKRuleKGRKO', {
    requires: [
        'AML.model.OPOKRuleKGRKO'
    ],
    extend: 'Ext.data.Store',
    model: 'AML.model.OPOKRuleKGRKO',
    alias: 'store.opokrulekgrko',
    proxy: {
        type: 'memory'
    },
    data: [
        {
            'id': 0,
            'label': 'в обоих филиалах',
            'all': '0 - в обоих филиалах'
        },
        {
            'id': 1,
            'label': 'в филиале по дебету',
            'all': '1 - в филиале по дебету'
        },
        {
            'id': 2,
            'label': 'в филиале по кредиту',
            'all': '2 - в филиале по кредиту'
        }
    ],
    sorters: [
        {
            property: 'id',
            direction: 'ASC'
        }
    ]
});
