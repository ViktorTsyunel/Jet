Ext.define('AML.store.OPOKCriteria', {
	requires: [
		'AML.model.OPOKCriteria',
		'Ext.Array'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOKCriteria',
	alias: 'store.opokcriteria',
	title: 'Справочник стандартных условий выявления',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true
});
