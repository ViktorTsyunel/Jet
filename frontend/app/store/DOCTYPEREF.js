Ext.define('AML.store.DOCTYPEREF', {
	requires: [
		'AML.model.DOCTYPEREF'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.DOCTYPEREF',
	alias: 'store.doctyperef',
	title: 'Справочник типов ДУЛ',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	sorters: {
		property: 'DOC_TYPE_CD',
		direction: 'ASC'
	},
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: common.globalinit.proxyType.ajax,
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message',
			totalProperty: 'totalCount'
		},
		extraParams: {
			form: 'DOCTYPE_REF',
			action: 'read'
		}
	}
});
