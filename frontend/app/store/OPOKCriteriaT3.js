Ext.define('AML.store.OPOKCriteriaT3', {
	extend: 'Ext.data.ChainedStore',
	alias: 'store.opokcriteriat3',
	title: 'Справочник стандартных условий выявления 3-го типа',
	source: 'OPOKCriteria',
	sorters: [
		{
			property: 'CRIT_CD',
			direction: 'ASC'
		}
	],
	filters: [
		{
			property: 'CRIT_TP_CD',
			value: 3
		}
	]
});
