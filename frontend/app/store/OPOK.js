Ext.define('AML.store.OPOK', {
	requires: [
		'AML.model.OPOK'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.OPOK',
	alias: 'store.opok',
	title: 'Справочник кодов ОПОК',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: true,
	sorters: {
		property: 'OPOK_NB',
		direction: 'ASC'
	}
});
