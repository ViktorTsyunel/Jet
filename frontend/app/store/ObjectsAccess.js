Ext.define('AML.store.ObjectsAccess', {
	requires: [
		'AML.model.ObjectsAccess'
	],
	extend: 'Ext.data.Store',
	model: 'AML.model.ObjectsAccess',
	alias: 'store.objectsaccess',
	title: 'Права доступа к объектам',
	pageSize: 0,
	remoteSort: false,
	remoteFilter: false,
	autoLoad: false,
	proxy: {
		url: common.globalinit.ajaxUrl,
		type: 'ajax',
		paramsAsJson: true,
		noCache: false,
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			rootProperty: 'data',
			messageProperty: 'message'
		},
		writer: {
			type: 'json',
			allowSingle: false,
			rootProperty: 'data'
		},
		extraParams: {
			form: 'ObjectsAccess',
			action: 'read'
		}
	}
});
