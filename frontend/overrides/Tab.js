Ext.define('AML.overrides.Tab', {
	override: 'Ext.tab.Tab',

	/**
	 * Sets this tab's attached card. Usually this is handled automatically by the {@link Ext.tab.Panel} that this Tab
	 * belongs to and would not need to be done by the developer
	 * @param {Ext.Component} card The card to set
	 */
	setCard: function(card) {
		var me = this;

		me.card = card;
		if (card.iconAlign) {
			me.setIconAlign(card.iconAlign);
		}
		if (card.textAlign) {
			me.setTextAlign(card.textAlign);
		}
		me.setText(me.title || card.title);
		me.setIconCls(me.iconCls || card.iconCls);
		me.setIcon(me.icon || card.icon);
		me.setGlyph(me.glyph || card.glyph);
		me.setMenu(me.menuTab || card.menuTab);
		
		if (me.menu) {
			me.menu.on('beforeshow', function () {
				return me.menu.up('tab').active;
			});
		}
	}
});
