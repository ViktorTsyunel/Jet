Ext.define('AML.overrides.Collection', {
	override: 'Ext.util.Collection',
    /**
	 * ���������� ���: �� �����������, ��� � ������ reject'� ������ ��� ����� �� ���� � ���������, ��� ������ ���� ��������� (���� ��������� �������)
	 *
     * This method should be called when an item in this collection has been modified. If
     * the collection is sorted or filtered the result of modifying an item needs to be
     * reflected in the collection. If the item's key is also being modified, it is best
     * to pass the `oldKey` to this same call rather than call `updateKey` separately.
     *
     * @param {Object} item The item that was modified.
     * @param {String[]} [modified] The names of the modified properties of the item.
     * @param {String/Number} [oldKey] Passed if the item's key was also modified.
     * @since 5.0.0
     */
    itemChanged: function (item, modified, oldKey, /* private */ meta) {
        var me = this,
            keyChanged = oldKey === 0 || !!oldKey,
            filtered = me.filtered && me.getAutoFilter(),
            filterChanged = false,
            itemMovement = 0,
            items = me.items,
            last = me.length - 1,
            sorted = me.sorted && last > 0 && me.getAutoSort(), // one or zero items is not really sorted
                                                                // CAN be called on an empty Collection
                                                                // A TreeStore can call afterEdit on a hidden root before
                                                                // any child nodes exist in the store.
            source = me.getSource(),
            toAdd,
            toRemove = 0,
            index,
            itemFiltered = false,
            newIndex,
            wasFiltered = false,
            details, newKey, sortFn;

        // We are owned, we cannot react, inform owning collection.
        if (source && !source.updating) {
            source.itemChanged(item, modified, oldKey, meta);
        }

        // Root Collection has been informed.
        // Change is propagating downward from root.
        else {
            newKey = me.getKey(item);
			
			// ****************************** ����������� ���� - ������ *****************************
			// ���� ��� ������ ��������, ����� ��������� ��������� ������� ������� (���� �������� �������)
			if (meta == "reject" && me.indexOfKey(newKey) == -1) {
				index = -1;

				if (filtered) {
					wasFiltered = (index < 0);
					itemFiltered = me.isItemFiltered(item);
					filterChanged = (wasFiltered !== itemFiltered);
				}
				
				if (!itemFiltered) {
					toAdd = [ item ];
					newIndex = me.length;
				}	
			}	
			else {
			// ****************************** ����������� ���� - ����� *****************************
				if (filtered) {
					index = me.indexOfKey(keyChanged ? oldKey : newKey);
					wasFiltered = (index < 0);
					itemFiltered = me.isItemFiltered(item);
					filterChanged = (wasFiltered !== itemFiltered);
				}

				if (filterChanged) {
					if (itemFiltered) {
						toRemove = [ item ];
						newIndex = -1;
					} else {
						toAdd = [ item ];
						newIndex = me.length; // this will be ignored if sorted
					}
				}
				// If sorted, the newIndex must be reported correctly in the beforeitemchange and itemchange events.
				// Even though splice ignores the parameter and calculates the insertion point
				else if (sorted && !itemFiltered) {
					// If we are sorted and there are 2 or more items make sure this item is at
					// the proper index.
					if (!filtered) {
						// If the filter has not changed we may need to move the item but if
						// there is a filter we have already determined its index.
						index = me.indexOfKey(keyChanged ? oldKey : newKey);
					}

					sortFn = me.getSortFn();

					if (index && sortFn(items[index - 1], items[index]) > 0) {
						// If this item is not the first and the item before it compares as
						// greater-than then item needs to move left since it is less-than
						// item[index - 1].
						itemMovement = -1;

						// We have to bound the binarySearch or else the presence of the
						// out-of-order "item" would break it.
						newIndex = Ext.Array.binarySearch(items, item, 0, index, sortFn);
					}
					else if (index < last && sortFn(items[index], items[index + 1]) > 0) {
						// If this item is not the last and the item after it compares as
						// less-than then item needs to move right since it is greater-than
						// item[index + 1].
						itemMovement = 1;

						// We have to bound the binarySearch or else the presence of the
						// out-of-order "item" would break it.
						newIndex = Ext.Array.binarySearch(items, item, index + 1, sortFn);
					}

					if (itemMovement) {
						toAdd = [ item ];
					}
				}
			}	

            // One may be tempted to avoid this notification when none of our three vars
            // are true, *but* the problem with that is that these three changes we care
            // about are only what this collection cares about. Child collections or
            // outside parties still need to know that the item has changed in some way.
            // We do NOT adjust the newIndex reported here to allow for position *after* the item has been removed
            // We report the "visual" position at which the item would be inserted as if it were new.
            details = {
                item: item,
                key: newKey,
                index: newIndex,

                filterChanged: filterChanged,
                keyChanged: keyChanged,
                indexChanged: !!itemMovement,

                filtered: itemFiltered,
                oldIndex: index,
                newIndex: newIndex,
                wasFiltered: wasFiltered,
                meta: meta
            };

            if (keyChanged) {
                details.oldKey = oldKey;
            }
            if (modified) {
                details.modified = modified;
            }

            me.beginUpdate();

            me.notify('beforeitemchange', [details]);

            if (keyChanged) {
                me.updateKey(item, oldKey);
            }

            if (toAdd || toRemove) {
                // In sorted mode (which is the only time we get here), newIndex is
                // correct but *ignored* by splice since it has to assume that *insert*
                // index values need to be determined internally. In other words, the
                // first argument here is both the remove and insert index but in sorted
                // mode the insert index is calculated by splice.
                me.splice(newIndex, toRemove, toAdd);
            }

            // Ensure that the newIndex always refers to the item the insertion is *before*.
            // Ensure that the oldIndex always refers to the item the insertion was *before*.
            //
            // Before change to "c" to "h":     |   Before change "i" to "d":
            //                                  |
            //      +---+---+---+---+---+---+   |   +---+---+---+---+---+---+
            //      | a | c | e | g | i | k |   |   | a | c | e | g | i | k |
            //      +---+---+---+---+---+---+   |   +---+---+---+---+---+---+
            //        0   1   2   3   4   5     |     0   1   2   3   4   5   
            //            ^           ^         |             ^       ^
            //            |           |         |             |       |
            //        oldIndex    newIndex      |       newIndex     oldIndex
            //                                  |
            // After change to "c" to "h":      |   After change "i" to "d":
            //                                  |
            //      +---+---+---+---+---+---+   |   +---+---+---+---+---+---+
            //      | a | e | g | h | i | k |   |   | a | c | d | e | g | k |
            //      +---+---+---+---+---+---+   |   +---+---+---+---+---+---+
            //        0   1   2   3   4   5     |     0   1   2   3   4   5  
            //            ^       ^             |             ^           ^
            //            |       |             |             |           |
            //      oldIndex    newIndex        |        newIndex     oldIndex
            //
            if (itemMovement > 0) {
                details.newIndex--;
            } else if (itemMovement < 0) {
                details.oldIndex++;
            }

            // Divergence depending on whether the record if filtered out at this level in a chaining hierarchy.
            // Child collections of this collection will not care about filtereditemchange because the record is not in them.
            // Stores however will still need to know because the record *is* in them, just filtered.
            me.notify(itemFiltered ? 'filtereditemchange' : 'itemchange', [details]);

            me.endUpdate();
        }
    }
});
