Ext.define('AML.overrides.TabPanel', {
	override: 'Ext.tab.Panel',

	/**
	 * @protected
	 * Makes sure we have a Tab for each item added to the TabPanel
	 */
	onAdd: function(item, index) {
		var me = this,
			cfg = Ext.apply({}, item.tabConfig),
			tabBar = me.getTabBar(),
			defaultConfig = {
				xtype: 'tab',
				title: item.title,
				icon: item.icon,
				iconCls: item.iconCls,
				glyph: item.glyph,
				ui: tabBar.ui,
				card: item,
				disabled: item.disabled,
				closable: item.closable,
				hidden: item.hidden && !item.hiddenByLayout, // only hide if it wasn't hidden by the layout itself
				tooltip: item.tooltip,
				tabBar: tabBar,
				tabPosition: tabBar.dock,
				rotation: tabBar.getTabRotation(),
                menu: item.menu
			};

		if (item.closeText !== undefined) {
			defaultConfig.closeText = item.closeText;
		}

		cfg = Ext.applyIf(cfg, defaultConfig);

		// Create the correspondiong tab in the tab bar
		item.tab = me.tabBar.insert(index, cfg);

		item.on({
			scope : me,
			enable: me.onItemEnable,
			disable: me.onItemDisable,
			beforeshow: me.onItemBeforeShow,
			iconchange: me.onItemIconChange,
			iconclschange: me.onItemIconClsChange,
			glyphchange: me.onItemGlyphChange,
			titlechange: me.onItemTitleChange
		});

		if (item.isPanel) {
			if (me.removePanelHeader) {
				if (item.rendered) {
					if (item.header) {
						item.header.hide();
					}
				} else {
					item.header = false;
				}
			}
			if (item.isPanel && me.border) {
				item.setBorder(false);
			}
		}

		// Force the view model to be created, see onRender
		if (me.rendered) {
			item.getBind();
		}

		// Ensure that there is at least one active tab. This is only needed when adding tabs via a loader config, i.e., there
		// may be no pre-existing tabs. Note that we need to check if activeTab was explicitly set to `null` in the tabpanel
		// config (which tells the layout not to set an active item), as this is a valid value to mean 'do not set an active tab'.
		if (me.rendered && me.loader && me.activeTab === undefined && me.layout.activeItem !== null) {
			me.setActiveTab(0);
		}
	}
});
