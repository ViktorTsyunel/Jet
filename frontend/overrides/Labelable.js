Ext.define('AML.overrides.Labelable', {
    override: 'Ext.form.Labelable',

    statics: {
        initTip: function (dismissDelay) {
            var tip = this.tip,
                cfg, copy;

            if (tip) {
                return;
            }

            cfg = {
                id: 'ext-form-error-tip',
                //<debug>
                // tell the spec runner to ignore this element when checking if the dom is clean
                sticky: true,
                //</debug>
                ui: 'form-invalid'
            };

            // On Touch devices, tapping the target shows the qtip
            if (Ext.supports.Touch) {
                cfg.anchor = 'top';
                cfg.showDelay = 0;
                cfg.dismissDelay = 0;
                cfg.listeners = {
                    beforeshow: function () {
                        this.minWidth = Ext.fly(this.anchorTarget).getWidth();
                    }
                };
            }
            cfg.listeners = {
                beforeshow: function () {
                    var anchorDismissDelay = Ext.get(this.activeTarget.anchorTarget);
                    if (Ext.isDefined(anchorDismissDelay.component)) {
                        if (Ext.isDefined(anchorDismissDelay.component.errorDismissDelay) && !Ext.isEmpty(anchorDismissDelay.component.errorDismissDelay)) {
                            this.dismissDelay = anchorDismissDelay.component.errorDismissDelay;
                        }
                    } else if (Ext.isDefined(anchorDismissDelay.parent()) && Ext.isDefined(anchorDismissDelay.parent().component)) {
                        if (Ext.isDefined(anchorDismissDelay.parent().component.errorDismissDelay) && !Ext.isEmpty(anchorDismissDelay.parent().component.errorDismissDelay)) {
                            this.dismissDelay = anchorDismissDelay.parent().component.errorDismissDelay;
                        }
                    } else {
                        this.dismissDelay = 5000;
                    }

                },
                beforehide: function () {
                    this.dismissDelay = 5000;
                }
            };

            tip = this.tip = Ext.create('Ext.tip.QuickTip', cfg);
            copy = Ext.apply({}, tip.tagConfig);
            copy.attribute = 'errorqtip';
            tip.setTagConfig(copy);
        }
    }
});