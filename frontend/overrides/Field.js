Ext.define('AML.overrides.Field', {
	requires: [
		'Ext.tip.QuickTipManager'
	],
	override: 'Ext.form.field.Base',
	config: {
		triggerClear: false,
		triggerClearHandler: undefined
	},
	constructor: function (config) {
		if (config.triggerClear) {
			config.triggers = Ext.applyIf(config.triggers || {}, {
				clear: {
					cls: Ext.baseCSSPrefix + 'form-clear-trigger',
					handler: config.triggerClearHandler || function (field) {
						if (field.clearValue) {
							field.clearValue();
							if (field.up('window') && field.up('window').getViewModel().data.record) {
								// field.up('window').getViewModel().data.record.set(field.name, '')
								field.up('window').getViewModel().data.record[field.name] = '';
							}
							if (field.up('app-alerts-oes321detailspanel')) {
								field.up('app-alerts-oes321detailspanel').getViewModel().data.flagComboSave = true
							}
						} else if (field.setValue) {
							field.setValue(null);
						}
					}
				}
			});
		}
		this.callParent(arguments);
	},
	_setOriginalValueFlag: false,
	setOriginalValue: function (value) {
		if (!this._setOriginalValueFlag) {
			this._setOriginalValueFlag = true;
			this.originalValue = this.value;
		}
	},
	msgTarget: 'side',
	labelableRenderTpl: [
		'{beforeLabelTpl}',
		'<label id="{id}-labelEl" data-ref="labelEl" class="{labelCls} {labelCls}-{ui} {labelClsExtra} ',
				'{unselectableCls}" style="{labelStyle}"<tpl if="inputId">',
				' for="{inputId}"</tpl> {labelAttrTpl}>',
			'<span class="{labelInnerCls} {labelInnerCls}-{ui}" style="{labelInnerStyle}">',
			'{beforeLabelTextTpl}',
			'<tpl if="fieldLabel">{fieldLabel}',
				'<tpl if="labelSeparator">{labelSeparator}</tpl>',
			'</tpl>',
			'{afterLabelTextTpl}',
			'</span>',
		'</label>',
		'{afterLabelTpl}',
		'<div id="{id}-bodyEl" data-ref="bodyEl" class="{baseBodyCls} {baseBodyCls}-{ui}<tpl if="fieldBodyCls">',
			' {fieldBodyCls} {fieldBodyCls}-{ui}</tpl> {growCls} {extraFieldBodyCls}"',
			'<tpl if="bodyStyle"> style="{bodyStyle}"</tpl>>',
			'{beforeBodyEl}',
			'{beforeSubTpl}',
			'{[values.$comp.getSubTplMarkup(values)]}',
			'{afterSubTpl}',
			'{afterBodyEl}',
		'</div>',
		'<tpl if="renderError">',
			'<div id="{id}-errorWrapEl" data-ref="errorWrapEl" class="{errorWrapCls} {errorWrapCls}-{ui}',
				' {errorWrapExtraCls}" style="{errorWrapStyle}">',
				'<div role="alert" aria-live="polite" id="{id}-errorEl" data-ref="errorEl" ',
					'class="{errorMsgCls} {invalidMsgCls} {invalidMsgCls}-{ui}" ',
					'data-anchorTarget="{id}-inputEl">',
				'</div>',
			'</div>',
		'</tpl>',
		'<tpl if="renderHelp">',
			'<div id="{id}-helpWrapEl" data-ref="helpWrapEl" class="jet-helpWrapEl">',
				'<div role="help" aria-live="polite" id="{id}-helpEl" data-ref="helpEl" class="jet-helpEl' + (Ext.isIE8 ? ' isIE8' : '') + '">',
				'</div>',
			'</div>',
		'</tpl>',
		{
			disableFormats: true
		}
	],
	getLabelableRenderData: function () {
		var data = this.callParent(arguments);
		data.renderHelp = !(!this.tooltip);
		return data;
	},
	// todo: переименовать tooltip в help
	initComponent: function () {
		this.on('afterrender', this.setHelpQTip);
		this.on('afterrender', this.disableFocusFromLabel);
		this.callParent(arguments);
	},
	disableFocusFromLabel: function() {
        // Отменить установку фокуса на поле при клике на label
        var label = this.labelEl;
		var activeEl = document.activeElement;
        label.on('click', function(e, t, eOpts) {
            if (Ext.isIE10 || Ext.isIE11) {
                activeEl.focus();
			}
            e.preventDefault();
            e.stopEvent();
        });
        if (Ext.isIE10 || Ext.isIE11) {
        	//Костыль: IE10,IE11 не отменяют установку фокуса на поле при отмене пропагации клика на лейбе
            label.on('mousedown', function(e, t, eOpts) {
                activeEl = document.activeElement;
            });
		}
	},
	setHelpQTip: function () {
		var tooltip = this.tooltip;
		if (tooltip) {
			var helpElId = this.getId() + '-helpEl';
			Ext.tip.QuickTipManager.register({
				target: helpElId,
				alwaysOnTop: true,
				trackMouse: tooltip.trackMouse || false,
				dismissDelay: tooltip.dismissDelay || 0,
				hideDelay: tooltip.hideDelay || 0,
				minWidth: tooltip.minWidth || 250,
				maxWidth: tooltip.maxWidth || 500,
				title: (tooltip.title || this.fieldLabel) ? '&nbsp;' + (tooltip.title || this.fieldLabel) + '&nbsp;' : undefined,
				text: tooltip.text || tooltip
			});
		}
	},
	setErrorMsgTxt: function (value) {
		this.markInvalid(value || '');
	},
    setRedBorder: function (value) {
		if (value) {
            var field = this.getActionEl();
            field.el.addCls(Ext.baseCSSPrefix + 'form-invalid-field');
            field.el.addCls(Ext.baseCSSPrefix + 'form-invalid-field-default');
            field.up().el.addCls(Ext.baseCSSPrefix + 'form-text-wrap-invalid');
        }
	}
});
