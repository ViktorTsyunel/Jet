Ext.define('AML.overrides.ComboBox', {
	override: 'Ext.form.field.ComboBox',
	// СНИМАЕТ ПОРЧУ С COMBOBOX'ов В IE
	checkChangeEvents: Ext.isIE ? [
		'change',
		'propertychange',
		'keyup'
	] : [
		'change',
		'input',
		'textInput',
		'keyup',
		'dragdrop'
	]
});
