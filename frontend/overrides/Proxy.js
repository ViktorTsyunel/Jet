Ext.define('AML.overrides.Proxy', {
	override: 'Ext.data.proxy.Proxy',
	listeners: {
		/**
		 * Мутим перенаправление на авторизацию мантаса для неизвестных граждан
		 * @param reader
		 * @param response
		 * @param error
		 * @param eOpts
		 */
		exception: function (reader, response, error, eOpts) {
			if (response.responseText && this.isRedirectToLogin(response.responseText)) {
				window.location.href = '/OFSAAI/prelogin.jsp';
			}
		}
	},
	isRedirectToLogin: function (source) {
		return source.match(/prelogin\.jsp\?loc=/);
	}
});
