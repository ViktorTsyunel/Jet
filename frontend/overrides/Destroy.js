Ext.define('AML.overrides.Destroy', {
	override: 'Ext.data.operation.Destroy',

    doProcess: function(/* resultSet, request, response */) {
		/* ��������� ��� - ��������� ����������� ������� (.SLICE()), �. �. �������� ��������� �� ���� ��������� � �����*/
        var clientRecords = this.getRecords().slice(),  
            clientLen = clientRecords.length,
            i;
        
        for (i = 0; i < clientLen; ++i) {
            clientRecords[i].setErased();
        }
    }
});
