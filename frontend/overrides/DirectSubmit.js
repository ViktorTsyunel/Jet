Ext.define('AML.overrides.DirectSubmit', {
    override: 'Ext.form.action.DirectSubmit',

    doSubmit: function() {
        var me = this,
            form = me.form,
            metadata = me.metadata || form.metadata,
            timeout = me.timeout || form.timeout,
            fn, formInfo, args;

        fn       = me.resolveMethod('submit');
        formInfo = me.buildForm();

        /*args = fn.directCfg.method.getArgs({
         params: formInfo.formEl,
         options: timeout != null ? { timeout: timeout * 1000 } : null,
         metadata: metadata,
         callback: me.onComplete,
         scope: me
         });

         fn.apply(window, args);*/
        if (me.timeout || form.timeout) {
            var options = {
                timeout: me.timeout * 1000 || form.timeout * 1000
            };
        }
        fn.call(window, formInfo.formEl, me.onComplete, me, options);

        me.cleanup(formInfo);
    },

    // Direct actions have already been processed and therefore
    // we can directly set the result; Direct Actions do not have
    // a this.response property.
    processResponse: function(result) {
        return (this.result = result);
    }
});
