Ext.define('AML.overrides.Store', {
	override: 'Ext.data.Store',
	/**
	 * Обработчик ошибок
	 * @param batch
	 * @param options
	 */
	syncFailure: function (batch, options) {
			// если это было неуспешное удаление - восстанавливаем удаленные локально записи
			Ext.each(batch.operations, function (operation) {
				if (operation.getRequest() && operation.getRequest().getAction() == 'destroy') {
					//копируем массив записей, т. к. в процессе восстановления элементы оригинального массива удаляются
					var records = operation.getRecords().slice();
					Ext.each(records, function(item) { item.reject && item.reject(); });
				}
			});	
			
			// показываем ошибки пользователю	
			// если используется record.save, то ошибка в options, если store.sync - в batch.exceptions
			if (options.exception && options.error) {
				Ext.toast(options.error);
			}
			else {
				Ext.each(batch.exceptions, function (operation) {
					if (operation.hasException()) {
						Ext.toast(operation.error);
					}
				});
			}
	},
	/**
	 * Проверка на наличие любых изменений в хранилище
	 * @returns {boolean}
	 */
	isChanges: function () {
		var change = (
			// Все измененные записи
			this.getModifiedRecords().length +
			// Все удаленные записи
			this.getRemovedRecords().length +
			// Все новые записи
			// todo: узнать почему это не getNewRecords()
			this.getRejectRecords().length > 0
		);
		return change;
	}
});
