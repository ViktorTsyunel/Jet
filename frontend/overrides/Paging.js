Ext.define('AML.overrides.Paging', {
	requires: [
		'Ext.String'
	],
	displayMsg: 'Записей на странице {1}, из них отфильтровано {3}, всего {2}',
	override: 'Ext.toolbar.Paging',
	// @private
	updateInfo : function(){
		var me = this,
			displayItem = me.child('#displayItem'),
			store = me.store,
			pageData = me.getPageData(),
			count, msg;

		if (displayItem) {
			count = store.getCount();
			if (count === 0) {
				msg = me.emptyMsg;
			} else {
				msg = Ext.String.format(
					me.displayMsg,
					pageData.fromRecord,
					pageData.toRecord,
					pageData.total,
					count
				);
			}
			displayItem.setText(msg);
		}
	}
});
