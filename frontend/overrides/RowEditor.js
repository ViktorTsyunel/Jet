Ext.define('AML.overrides.RowEditing', {
    override: 'Ext.grid.RowEditor',

    cancelBtnText: 'Отмена',
    saveBtnText: 'Сохранить'
});
