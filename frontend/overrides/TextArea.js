Ext.define('AML.overrides.TextArea', {
	override: 'Ext.form.field.TextArea',
	grow: true,
	growMin: 50,
	growMax: 50
});
