Ext.define('AML.overrides.Window', {
	requires: [
		'Ext.layout.container.Fit'
	],
	override: 'Ext.window.Window',
	resizable: false,
	draggable: false,
	modal: true,
	width: (Ext.isIE8 ? 1280 : '80%'),
	bodyStyle: {
		background: '#fff',
		padding: '10px 20px 10px 20px'
	},
	layout: 'fit'
});
