Ext.define('AML.overrides.Component', {
	override: 'Ext.Component',
	initComponent: function () {
		this.callParent(arguments);
		/**
		 * Лечим багу с маской для компонентов после родительского disable
		 */
		this.on('enable', function (cmp) {
			if (cmp.isMasked()) {
				cmp.unmask();
			}
		});
	}
});
