Ext.define('AML.overrides.Toast', {
	override: 'Ext.window.Toast',
	align: 'br',
	alwaysOnTop: true,
	autoDestroy: true,
	closable: false,
	closeOnMouseOut: true,
	modal: false,
	slideInDuration: 300,
	slideHideDuration: 300,
	autoCloseDelay: 10000,
	width: '25%'
});
