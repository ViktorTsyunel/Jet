Ext.define('AML.overrides.GridColumn', {
    override: 'Ext.grid.column.Column',

    isOnLeftEdge: function(a) {
        return ( a.getXY()[0] - this.getX() <= this.handleWidth)
    },
    isOnRightEdge: function(a) {
        return ( this.getX() + this.getWidth() - a.getXY()[0] <= this.handleWidth)
    },

    onTitleElClick: function(e, t, sortOnClick) {
        var me = this,
            activeHeader,
            prevSibling;

        if (Ext.supports.Touch) {
            prevSibling = me.previousSibling(':not([hidden])');

            if (!me.menuDisabled && me.isOnRightEdge(e, parseInt(me.triggerEl.getStyle('width')))) {
                if (!me.menuDisabled) {
                    activeHeader = me;
                }
            }

            else if (prevSibling && !prevSibling.menuDisabled && me.isOnLeftEdge(e)) {
                activeHeader = prevSibling;
            }
        }
        else {
            activeHeader = me.triggerEl && (e.target === me.triggerEl.dom || t === me.triggerEl || e.within(me.triggerEl)) ? me : null;
        }

        if (sortOnClick !== false && (!activeHeader && !me.isOnLeftEdge(e) && !me.isOnRightEdge(e) || e.getKey())) {
            me.toggleSortState(e.ctrlKey);
        }

        return activeHeader;
    },

    toggleSortState: function(ctrKey) {
        var direction;

        if (ctrKey) {
            switch (this.sortState) {
                case 'ASC':
                    direction = 'DESC';
                    break;
                case 'DESC':
                    direction = 'CLR';
                    break;
                case 'CLR':
                    direction = 'ASC';
                    break;
                default:
                    direction = 'ASC';
                    break;
            }
        }

        if (this.isSortable()) {
            this.sort(direction, ctrKey);
        }
    },

    sort: function(direction, ctrKey) {
        var me = this,
            grid = me.up('tablepanel'),
            store = grid.store;

        Ext.suspendLayouts();
        me.sorting = true;
        if (direction === 'CLR') {
            store.getSorters().removeByKey(me.getSortParam());
            if (store.getSorters().count() < 1) {
                store.sort();
            }
        } else {
            store.sort(me.getSortParam(), direction, ctrKey ? 'append' : 'replace');
        }

        delete me.sorting;
        Ext.resumeLayouts(true);
    }
});
