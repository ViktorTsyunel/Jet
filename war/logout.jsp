<%--
/**
 * This source is part of the Reveleus Software System and is copyrighted by Oracle Financial Services Software Limited.
 *
 * All rights reserved. No part of this work may be reproduced, stored in a retrieval system, adopted or transmitted in any form or by any means, electronic, mechanical, photographic, graphic, optic recording or otherwise, translated in any language or computer language, without the prior written permission of Oracle Financial Services Software Limited.
 *
 * Oracle Financial Services Software Limited.
 * 10-11, SDF I, SEEPZ, Andheri (East),
 * Mumbai - 400 096.
 * India
 *
 * Copyright &copy; 2009 - 2010 Oracle Financial Services Software Limited. All rights reserved.
 */
--%>

<%@ page import = "java.util.*"%>
<%@ page import = "com.iflex.fic.util.*"%>
<%@ page import = "com.iflex.fic.common.*"%>
<%@ page import = "com.iflex.fic.client.*"%>
<%@ page import = "com.iflex.fic.i18n.common.I18NDBProperties"%>
<%@ page import = "com.iflex.fic.global.*"%>
<%@ page import = "com.iflex.fic.i18n.message.client.MessageFramework"%>
<%@ page import = "com.iflex.fic.i18n.common.*"%>

<%@page
import="javax.naming.InitialContext"
import="javax.naming.Context"
import="javax.sql.DataSource"
import="java.sql.SQLException"
import="java.sql.DriverManager"
import="java.sql.Connection"
import="java.sql.CallableStatement"%>

<%@ include file="taginclude.jsp"%>

<%-- /** RevSecInclude.jsp included for Security Penetration Prevention */ --%>
<jsp:include page="/RevSecInclude.jsp" flush="true" />

<jsp:useBean id="CSBeanId" scope="session" class="com.iflex.fic.global.CSBean" />

<script src="commonfunctions.js"></script>

<%		
	Vector v = new Vector<String>();
	v.add("HASHPASS");
	v.add("AUTHENTICATIONTYPE");
	v.add("SSO_ENABLED");
	v.add("SSO_LOGOUT_URL");
	Properties props = SMSServices.getConfiguration(v);
	String access_denied = (String)session.getAttribute("access_denied");
	
	if(access_denied != null && access_denied != "")
	{
		if(access_denied.equalsIgnoreCase("usergroup"))
		{
%>
			<script language="Javascript">
				alert("WARNING: "+"<revi18n:revmessage bundleRef="bundle1" key="SMSME.CHK_USR_RIGHT"/>");
			</script>
<%
		}else if(access_denied.equalsIgnoreCase("usergrouprole"))
		{
%>
			<script language="Javascript">
				alert("WARNING: "+"<revi18n:revmessage bundleRef="bundle1" key="SMSME.CHK_USR_RIGHT"/>");
			</script>
<%
		}else if(access_denied.equalsIgnoreCase("infodom"))
		{
%>
			<script language="Javascript">
				alert("WARNING: "+"<revi18n:revmessage bundleRef="bundle1" key="SMSME.INFODOM_NOT_MAPPED"/>");
				if(window.opener)
				{
					if(!window.opener.closed)
					{
						window.opener.top.close();
					}
				}
			</script>
<%
		} else if(access_denied.equalsIgnoreCase("notauthorizedpage"))
		{
%>
			<script language="Javascript">
				alert("WARNING: "+"<revi18n:revmessage bundleRef="bundle1" key="SMSME.NOT_AUTHORIZED_PAGE"/>");
				if(window.opener)
				{
					if(!window.opener.closed)
					{
						window.opener.top.close();
					}
				}
			</script>
<%
		}
	}
%>

	<script language="Javascript">
		var lcl = '<%=(String)session.getAttribute("lclPostFix")%>';
	</script>

<html>
	<head></head>
	<body>

<%
		String loggedInUserid = (String)session.getAttribute("gsUsrID");
		
		String loginType = "";
		if(props.getProperty("SSO_ENABLED").equals("Y") && props.getProperty("AUTHENTICATIONTYPE").equals("" + GlobalParameters.SSO_AUTHENTICATION_TYPE_SSO_CSSMS)){
			loginType = (String)session.getAttribute("LOGINPAGE");
		}
		
		String msg = SMSServices.logoutUser(loggedInUserid, request.getRemoteAddr());
		if( msg == null) 
		{
%>
			<script language="javascript">
				 MM_openBrowserWindowForMessage("<revi18n:revmessage bundleRef="bundle1" key="SMSME.ERROR"/>"+"<%=msg%>"+'~Error');
			</script>
<%	
		}

		String lsMessage = KeyConstructor.constructCompoundKey("SMSAUDIT.USR_LOGGED_OUT", CommonFunctions.decrypt(loggedInUserid) );
		SMSServices.updateAuditTrail("SMS", loggedInUserid, "SMSAUDIT.LOG_OUT", "SMSAUDIT.LOGOUT", lsMessage);
		
		//Added/Modified by Lakki for SSO integration with OFSAAI
		if(props.getProperty("SSO_ENABLED").equals("Y") && props.getProperty("AUTHENTICATIONTYPE").equals("" + GlobalParameters.SSO_AUTHENTICATION_TYPE_SSO_CSSMS))
		{
			session.removeAttribute("gsUsrID");
			session.removeAttribute("getpass_password");
			session.removeAttribute("userLocale");
			session.removeAttribute("lclPostFix");
			session.removeAttribute("LOGINPAGE");
			session.invalidate();
			
		}else
		{
			session.setAttribute("sessionId","");
			session.setAttribute("cleanupRequired","false");
			/** Invalidate current session, it automatically remove all parameters of it.*/
			session.invalidate();
		}

		Cookie[] cookieSet = request.getCookies();
		if(cookieSet != null)
		{
			for(int i=0; i<cookieSet.length; i++) 
			{
				cookieSet[i].setMaxAge(0);
				response.addCookie(cookieSet[i]);
			}
		}
		
		//������ ������ APEX �� ���������������� IP-������
        Connection conn = null;
	    CallableStatement cs = null;
        try {
	      final String sql = "{ call mantas.rf_pkg_apex_adapter.delete_apex_sessions(?) }";
          String ipAddress = request.getRemoteAddr();

          Context initContext = new InitialContext();
          DataSource ds = (DataSource)initContext.lookup("jdbc/AMINFO");
          conn = ds.getConnection();
    
          cs = conn.prepareCall(sql);
          cs.setString(1, ipAddress);
          cs.execute();
        } 
        catch (Exception e) {} 
        finally {
          try {cs.close();} catch(Exception e) {}
          try {conn.close();} catch (Exception e) {}
		}      				
%>

		<script language="javascript">
			document.cookie = "FIC_SESSION=1;expires=Thu, 01-Jan-1970 00:00:01 GMT;";
		</script>
	
<%  
		//Added/Modified for SSO integration with OFSAAI
		if (loginType =="SSO")
		{
			String ssoLogoutURL = props.getProperty("SSO_LOGOUT_URL").trim();
%>
			<script>
				window.top.location.href = "<%=ssoLogoutURL%>";	
			</script>
<%
		} 
		else if (loginType =="REV")
		{
%>
			<script>
				window.top.location.href = "direct_login.jsp?loc="+lcl;
			</script>
<%	
		}else
		{
%>
			<script>
				window.top.location.href = "prelogin.jsp?loc="+lcl;
			</script>
<%
		}
%>
	</body>
</html>