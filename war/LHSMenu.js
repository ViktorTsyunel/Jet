/**
 * This source is part of the Reveleus Software System and is copyrighted by Oracle Financial Services Software Limited.
 *
 * All rights reserved. No part of this work may be reproduced, stored in a retrieval system, adopted or transmitted in any form or by any means, electronic, mechanical, photographic, graphic, optic recording or otherwise, translated in any language or computer language, without the prior written permission of Oracle Financial Services Software Limited.
 *
 * Oracle Financial Services Software Limited.
 * 10-11, SDF I, SEEPZ, Andheri (East),
 * Mumbai - 400 096.
 * India
 *
 * Copyright � 2009 - 2010 Oracle Financial Services Software Limited. All rights reserved.
 */

/** */
function init() {
	/* Work out which browser the user is using and get a reference */
	/* to its array that holds the Div elements */
	if (navigator.appName == "Microsoft Internet Explorer") {
		garrDoc = document.all.tags('div');
		gblnIE = true;
	}
	else {
		/* Check to see if its Netscape Nav 4.x */
		if (navigator.appVersion.substring(0, 2) == "4.") {
			garrDoc = document.layers;
			gblnNS = true;
		}
		else {
			garrDoc = document.getElementsByTagName('div');
			gblnNS = false;
		}
	}
	isTabloaded = false;
}// init()
function resetSelectedNodes() {
	var coll = document.all.tags("span");
	for (i = 0; i < coll.length; i++) {
		coll[i].className = "menuunselected ";
	}
}
/** */
function createTree() {
	init();
	loadParentArray();
	topPos = 20;
	height = 20;
	stdWidth = 4;
	var CompHTMLStr = "";
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		htmlStr = "<div id=\"" + tempArr[0] + "\" class=\"\"";
		styleStr = "background-image:url(" + tempArr[6] + ")";
		htmlStr = htmlStr + "STYLE=\"visibility:visible;height:" + height + ";width:" + totaltabwidth + "; " + styleStr + ";\">";
		indent = (tempArr[15]) * stdWidth;
		cellwidth = totaltabwidth - indent;
		if (tempArr[19] != '') {
			cellwidth = cellwidth - helpcellwidth;
		}
		var font = tempArr[4].split('~');
		htmlStr = htmlStr + "<table width=\"" + totaltabwidth + "\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" ><tr><td>"; // outer table
		htmlStr = htmlStr + "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr>";
		var parentArr = tempArr[24];
		for ( var depth = 0; depth < tempArr[15] - 2; depth++) {
			if (depth < 0 || (depth != 0 && (parentArr != null && parentArr[20] == '1'))) {
				htmlStr = htmlStr + "<td><img src=\"images/spacer.gif\" width=\"16\"></td>"; // verticle line
			}
			else {
				htmlStr = htmlStr + "<td><img src=\"images/spacer.gif\" class=\"vertline\"></td>"; // verticle line
			}
		}// for
		indent = indent - ((tempArr[15] - 2) * 16) - 1;
		htmlStr = htmlStr + "<td><img src=\"images/spacer.gif\" width=\"1px\"></td>"; // first cell of inner left table for indent
		imgName = "";
		if (tempArr[14] == 'Y') {
			imgName = openImgSrc;
		}
		else {
			if (tempArr[20] == '1') {
				imgName = "lastnode";
			}
			else {
				imgName = "node";
			}// if mid node
		}// if leaf node
		if (x != 0) {
			if (tempArr[14] == 'Y') {
				// for non leaf nodes put a href on this image
				htmlStr = htmlStr + "<td>";
				htmlStr = htmlStr + "<a style=\"text-decoration:none\" href=\"javascript:NodeClick('" + tempArr[0] + "')\" title = '" + tempArr[3] + "'>";
				htmlStr = htmlStr + "<img id=\"img" + tempArr[0] + "\" border=\"0\" src=\"images/spacer.gif\"  class=\"" + imgName + "\">";
				htmlStr = htmlStr + "</a>";
				htmlStr = htmlStr + "</td>"; // second cell of inner left table for +/- image
			}
			else {
				htmlStr = htmlStr + "<td>";
				htmlStr = htmlStr + "<img src=\"images/spacer.gif\"  class=\"" + imgName + "\">";
				htmlStr = htmlStr + "</td>"; // second cell of inner left table for +/- image
			}
		}// if x is not zero
		if (tempArr[10] != '') {
			tempArr[10] = tempArr[10].replace('.gif','');
			tempArr[10] = tempArr[10].replace('images/','');
			htmlStr = htmlStr + "<td>";
			htmlStr = htmlStr + "<img src=\"images/spacer.gif\" class=\"" + tempArr[10] + "\">";
			htmlStr = htmlStr + "</td>";
			// this was the third optional cell of the left inner table for the link image
		}
		htmlStr = htmlStr + "<td width='3'>&nbsp;";
		htmlStr = htmlStr + "</td>";
		htmlStr = htmlStr + "<td nowrap style='font-family:" + font[0] + "; font-size:" + font[1] + "; font-weight:" + font[2] + ";'>";
		if (tempArr[14] == 'Y') {
			htmlStr = htmlStr + "<a style=\"text-decoration:none; color:" + tempArr[5] + "\" href=\"javascript:NodeClick('" + tempArr[0] + "')\" title = '" + tempArr[3] + "'>";
			htmlStr = htmlStr + tempArr[2];
			htmlStr = htmlStr + "</a>";
		}
		else {
			htmlStr = htmlStr + "<a style=\"text-decoration:none; color:" + tempArr[5] + "\" href=\"javascript:onLinkClick('" + tempArr[0] + "')\" title = '" + tempArr[3] + "'><span ID=\"" + tempArr[0] + "_Y\">" + tempArr[2] + "</span></a>";
		}
		htmlStr = htmlStr + "</td>"; // this is for the fourth cell of the left inner table for the link text
		htmlStr = htmlStr + "</tr></table>"; // end of inner left table
		htmlStr = htmlStr + "</td><td align=\"right\" class=\"txtalnrt\">"; // beginning of second cell of outer table
		if (tempArr[19] != '') {
			tempArr[19] = tempArr[19].replace('.gif','');
			tempArr[19] = tempArr[19].replace('images/','');
			htmlStr = htmlStr + "<a style=\"text-decoration:none\" href='" + tempArr[11] + "' target='help'> <img border=0 src=\"images/spacer.gif\" class ='" + tempArr[19] + "'></a>";
		}
		htmlStr = htmlStr + "</td></tr></table>"; // end of outer table
		htmlStr = htmlStr + "</div>";
		topPos = topPos + height;
		tempArr[21] = "V";
		tempArr[22] = "O";
		CompHTMLStr = CompHTMLStr + htmlStr;
	}// for all array items
	document.write(CompHTMLStr);
	loadTab();
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		tempArr[22] = "C";
		recurseCloseNodes(tempArr[0]);
	}
	resetPosition();
}// createTree()
/** */
function loadParentArray() {
	// This funtion sets the reference to the parent array item
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		tempArr[24] = getArrayItem(tempArr[1]);
	}// for
}// loadParentArray()
/** */
function loadTab() {
	// This funtion sets the reference to the Div tag in the array
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		tempArr[23] = getDiv(tempArr[0]);
		tempArr[24] = getArrayItem(tempArr[1]);
	}// for
	isTabloaded = true;
}// loadTab()
/** */
function getArrayItem(NodeID) {
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		if (tempArr[0] == NodeID)
			return tempArr;
	}
}// getArrayItem
/** */
function getDiv(DivID) {
	if (gblnNS) {
		for (x = 0; x < document.layers.length; x++) {
			if (document.layers[x].id == DivID) {
				return document.layers[x];
			}
		}
	}
	else {
		for (x = 0; x < garrDoc.length; x++) {
			if (garrDoc[x].id == DivID) {
				return garrDoc[x];
			}// if
		}// for
	}// else
}// getDiv
/** */
function getImage(DivObj, ImgID) {
	if (gblnNS) {
		for (x = 0; x < DivObj.document.images.length; x++) {
			if (DivObj.document.images[x].id == ImgID) {
				return DivObj.document.images[x];
			}
		}
	}
	else {
		for (x = 0; x < document.images.length; x++) {
			if (document.images[x].id == ImgID) {
				return document.images[x];
			}
		}
	}
}// getDiv
/** */
function NodeClick(DivSection) {
	if (!isTabloaded) {
		loadTab();
	}
	var strAction = "Open";
	/* Work out whether the node being clicked need to be opened or closed */
	arrVal = locateArray(DivSection);
	if (arrVal == null) {
		return;
	}
	if (arrVal[22] == "O") {
		strAction = "Close";
	}
	/* Call the appropriate function to open or close this node */
	if (strAction == "Open") {
		OpenNode(DivSection);
	}
	else {
		CloseNode(DivSection);
	}
}
/** */
function locateArray(DivSection) {
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		if (tempArr[0] == DivSection) {
			return tempArr;
		}
	}// for
	return null;
}// LocateNodes()
/** */
function OpenNode(DivSection) {
	/* Show all nodes that are children of the one being clicked */
	arr = locateArray(DivSection);
	arr[21] = "V";
	arr[22] = "O";
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		if (tempArr[1] == DivSection) {
			tempArr[21] = "V"; // set Visible
			tempArr[22] = "C"; // set Closed
			recurseOpenNodes(tempArr[0]);
		}
	}// for
	/* Now that we have hidden the child nodes we need to move all other Lower nodes up to fill the space */
	resetPosition();
}// OpenNode()
/** */
function recurseOpenNodes(parentNode) {
	// this function recursively closes nodes and sub-nodes of the given nodes
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		if (tempArr[1] == parentNode) {
			tempArr[21] = "H";
			tempArr[22] = "C";
			recurseCloseNodes(tempArr[0]);
		}// if
	}// for
}// recurseCloseNodes()
/** */
function CloseNode(DivSection) {
	/* Hide all nodes that are children of the one being clicked */
	arr = locateArray(DivSection);
	arr[21] = "V";
	arr[22] = "C";
	recurseCloseNodes(DivSection);
	/* Now that we have hidden the child nodes we need to move all other Lower nodes up to fill the space */
	resetPosition();
}// CloseNode()
/** */
function recurseCloseNodes(parentNode) {
	// this function recursively closes nodes and sub-nodes of the given nodes
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		if (tempArr[1] == parentNode) {
			tempArr[21] = "H";
			tempArr[22] = "C";
			recurseCloseNodes(tempArr[0]);
		}// if
	}// for
}// recurseCloseNodes()
/** */
function resetPosition() {
	if (!isTabloaded)
		loadTab();
	topPos = 20;
	for ( var x = 0; x < menuStr.length; x++) {
		var tempArr = menuStr[x];
		if (tempArr[21] == "V") {
			document.getElementById(tempArr[23].id).style.display = 'block';
			tempArr[23].style.top = topPos;
			topPos = topPos + height;
		}
		else {
			document.getElementById(tempArr[23].id).style.display = 'none';
			tempArr[23].style.top = topPos;
		}
		if (tempArr[14] == 'Y') {
			imgID = "img" + tempArr[0];
			var img = getImage(tempArr[23], imgID);
			if (tempArr[22] == "O") {
				// node has children and is OPEN
				img.className = openImgSrc;
				if(tempArr[20] == '1'){
					img.src = "images/spacer.gif";
					img.className = "ftv2mlastnode";
				}
			}
			else {
				// node has children and is CLOSED
				img.className = closedImgSrc;
				if(tempArr[20] == '1'){
					img.src = "images/spacer.gif";
					img.className = "ftv2plastnode";
				}
			}
		}// if node has children
	}// for
}// resetPosition()
/** */
function onLinkClick(lid) {
	resetSelectedNodes();
	var element = document.getElementById(lid + "_Y");
	element.className = "menuselected";
	var menuArr = menuStr;
	if (!isValidForOlap(lid, olapType)) {
		MM_openBrWindowForMessage(SMSME.IVLD_CURR_OLAP_TYPE + '~Error');
		return;
	}
	if (!isValidForRDB(lid, dbType)) {
		MM_openBrWindowForMessage(SMSME.IVLD_CURR_DB_TYPE + '~Error');
		return;
	}
	if (isWindowOpen(lid)) {
		MM_openBrWindowForMessage(SMSME.WIN_ALRDY_OPEN);
		return;
	}

	var isInfodomCheckRequired = false;
	var infodomCheckName = dsn;
	for (var i = 0; i < menuArr.length; i++) {
		var tmpArr = menuArr[i];
		if (tmpArr[0] == lid) {
			var url = tmpArr[7];
			var vtarget = tmpArr[8];
			var params = tmpArr[13];
			var strparam = "?";
			var delim = '';
			for (var x = 0; x < params.length; x++) {
				if (isTemplateParameter(params[x])) {
					var nv = params[x].split('=');
					var pname = nv[0];
					var pval = getTemplateParamName(params[x]);
					if (pval == 'dsn') {
						pval = dsn;
					}
					else if (pval == 'userid') {
						pval = userid;
					}
					else if (pval == 'gsfunctions') {
						pval = gsfunctions;
					}
					else {
						pval = 'TBD';
					}
					strparam += delim + pname + '=' + pval;
				}
				else if(params[x].toUpperCase().indexOf("ISINFODOMCHECKREQUIRED") > -1) {
					var paramArray = params[x].split('=');
					var paramName = paramArray[0];
					var paramValue = paramArray[1];
					if(paramValue.toUpperCase() == "TRUE") {
						isInfodomCheckRequired = true;
					}
				}
				else {
					strparam += delim + params[x];
				}
				delim = '&';
			}

			/** Check if infodom check is required */
			if(isInfodomCheckRequired) {
				for (var x = 0; x < params.length; x++) {
					if(params[x].indexOf("dsn=") > -1) {
						var paramArray = params[x].split('=');
						var paramName = paramArray[0];
						var paramValue = paramArray[1];
						/** check if the current information domain is matching with the configured parameter */
						if(paramValue != dsn) {
							MM_openBrWindowForMessage(SMSME.LINK_NOT_ALLOWED_USER_INFODOM + '~Error');
							return;
						}
					}
				}
			}
			url = url + strparam;
			var features;
		       
			if (url.indexOf('RF/index.html') !== -1) {
 				features = 'top=0,left=0,status=yes,location=yes,scrollbars=yes,resizable=yes,toolbar=yes,addressbar=yes,menubar=no,' + 'width=' + (screen.width - 10) + "," + 'height=' + (screen.height - 200);
			}
			else {
				features = 'top=0,left=0,status=yes,location=no,scrollbars=yes,resizable=yes,toolbar=no,addressbar=yes,menubar=no,' + 'width=' + (screen.width - 10) + "," + 'height=' + (screen.height - 200);
			}
			var windowRef = window.open(url, vtarget, features);
			// Added by Ishwar on 28-Jul-2005, for SCF 86 (CAT Reveleus 526i ST - Win 2K - SQL Server - Ess -Tomcat)
			// Resize to cover full desktop area
			if ((screen.availWidth && screen.availHeight) && (vtarget != "right")) {
				try {
					windowRef.resizeTo(screen.availWidth, screen.availHeight);
				}
				catch (e) {
				}
			}
			windowRef.focus();
			Tracker.put(lid, windowRef);
			removeRefForOtherId(lid, windowRef);
			break;
		}
	}
}
/** */
function isWindowOpen(wid) {
	var windowRef = Tracker.get(wid);
	if (windowRef == null) {
		return false;
	}
	else {
		if (windowRef.closed) {
			return false;
		}
		else {
			for ( var i = 0; i < menuStr.length; i++) {
				var tempMenu = menuStr[i];
				if (tempMenu[0] == wid) {
					if (tempMenu[16] == 'Y') {
						return false;
					}
				}
			}
			return true;
		}
	}
}
/** */
function removeRefForOtherId(wid, windowRef) {
	var keys = Tracker.getKeys();
	for ( var i = 0; i < keys.length; i++) {
		if (wid != keys[i]) {
			if (windowRef == Tracker.get(keys[i])) {
				Tracker.remove(keys[i]);
			}
		}
	}
}
/** */
function removeFromOpenList(wid) {
	Tracker.remove(wid);
}
/** */
function isTemplateParameter(param) {
	var nameval = param.split('=');
	val = nameval[1];
	if (val.charAt(0) == '%' && val.charAt(1) == '{') {
		return true;
	}
	else {
		return false;
	}
}
/** */
function getTemplateParamName(param) {
	var nameval = param.split('=');
	var val = nameval[1];
	var ret = '';
	for ( var i = 2; i < val.length; i++) {
		if (val.charAt(i) == '}') {
			return ret;
		}
		ret += val.charAt(i);
	}
	return ret;
}
/** */
function windowCloseHandle() {
	MM_openBrWindowForMessage(SMSME.NAME_EQ + " " + window.name);
}
/** */
function confirmUnloadLHS(flag) {
	var keys = this.Tracker.getKeys();
	var ret = true;
	if (this.Tracker.getSize() == 0) {
		return true;
	}
	for ( var i = 0; i < this.Tracker.getSize(); i++) {
		var temp = this.Tracker.get(keys[i]);
		if (isToBeClosedOnLogOut(keys[i])) {
			var windowRef = this.Tracker.get(keys[i]);
			if (!windowRef.closed) {
				if (MM_openBrWindowForMessage(replaceSubstring(SMSME.CNF_ACT_OPEN_WANT_CLOSE, "{0}", getMenuName(keys[i])) + "~userInput")) {
					if (windowRef.name == "AMHM") {
						if (windowRef.Tracker != null && windowRef.Tracker.getSize() > 0) {
							for ( var jj = 0; jj < windowRef.Tracker.getSize(); jj++) {
								if (!windowRef.Tracker.valArr[jj].closed) {
									windowRef.Tracker.valArr[jj].close();
								}
							}
						}
					}
					if (windowRef.name == "fsapps") {
						if (windowRef.Tracker != null && windowRef.Tracker.getSize() > 0) {
							for ( var jj = 0; jj < windowRef.Tracker.getSize(); jj++) {
								if (!windowRef.Tracker.valArr[jj].closed) {
									windowRef.Tracker.valArr[jj].close();
								}
							}
						}
					}
					if (windowRef.name == "right") {
						if (windowRef.Tracker != null && windowRef.Tracker.getSize() > 0) {
							for ( var jj = 0; jj < windowRef.Tracker.getSize(); jj++) {
								if (!windowRef.Tracker.valArr[jj].closed) {
									windowRef.Tracker.valArr[jj].close();
								}
							}
						}
					}
					if (windowRef.name.indexOf("DI") != -1) {
						if (windowRef.frames.rhsContent.dlgWin != null && windowRef.frames.rhsContent.dlgWin.getSize() > 0) {
							for ( var jj = 0; jj < windowRef.frames.rhsContent.dlgWin.getSize(); jj++) {
								if (!windowRef.frames.rhsContent.dlgWin.valArr[jj].closed) {
									windowRef.frames.rhsContent.dlgWin.valArr[jj].close();
								}
							}
						}
						
					}
					windowRef.close();
				}
				else {
					if (flag == 'DI' || flag == 'SM') {
						windowRef[i].focus();
						return false;
					}
					else {
						return false;
					}
				}
			}
		}
	}
	return ret;
}
/** */
function isToBeClosedOnLogOut(wid) {
	for ( var i = 0; i < menuStr.length; i++) {
		var tempArr = menuStr[i];
		if (tempArr[0] == wid) {
			if (tempArr[9] == 'Y') {
				return true;
			}
		}
	}
	return false;
}
/** */
function getMenuName(mid) {
	for ( var i = 0; i < menuStr.length; i++) {
		var tempArr = menuStr[i];
		if (tempArr[0] == mid) {
			return tempArr[2];
		}
	}
	return '';
}
/** */
function isValidForOlap(menuId, currOlap) {
	for ( var i = 0; i < menuStr.length; i++) {
		var tempMenu = menuStr[i];
		if (tempMenu[0] == menuId) {
			var olps = tempMenu[17];
			if (olps == '' || olps == null) {
				return true;
			}
			else {
				return isCurrStrTypePresent(olps, currOlap);
			}
		}
	}
}
/** */
function isValidForRDB(menuId, currRDB) {
	for ( var i = 0; i < menuStr.length; i++) {
		var tempMenu = menuStr[i];
		if (tempMenu[0] == menuId) {
			var rdbs = tempMenu[18];
			if (rdbs == '' || rdbs == null) {
				return true;
			}
			else {
				return isCurrStrTypePresent(rdbs, currRDB);
			}
		}
	}
}
/** */
function isCurrStrTypePresent(avStrs, currStr) {
	var avStrArr = avStrs.split(',');
	for ( var i = 0; i < avStrArr.length; i++) {
		if (currStr == avStrArr[i]) {
			return true;
		}
	}
	return false;
}
/** */
function LHSCloseConfirm() {
	return confirmUnloadLHS();
}
function MM_openBrWindowForMessage(argument) {
	var agrType = "";
	var winTitle = "";
	var temp = argument.lastIndexOf("~");
	if (temp != -1) {
		agrType = argument.substr(temp + 1);
	}
	if (agrType == 'Warning') {
		winTitle = "Warning Message";
	}
	else if (agrType == 'Confirm') {
		winTitle = "Warning";
	}
	else if (agrType == 'Error') {
		winTitle = "Error Message";
	}
	else if (agrType == 'userInput') {
		winTitle = "Warning";
	}
	else if (agrType == 'Message') {
		winTitle = "Information";
	}
	else {
		winTitle = "Information";
	}
	var confirmMsgVal = window.showModalDialog("PopupAlert.jsp?winTitle=" + winTitle, argument, "dialogHeight:190px;dialogWidth:686px;status:no;help:no");
	if (confirmMsgVal != null || confirmMsgVal != undefined) {
		return confirmMsgVal;
	}
}